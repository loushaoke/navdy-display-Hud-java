package com.navdy.obd;

final class ECU$1 implements android.os.Parcelable$Creator {
    ECU$1() {
    }
    
    public com.navdy.obd.ECU createFromParcel(android.os.Parcel a) {
        return new com.navdy.obd.ECU(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.obd.ECU[] newArray(int i) {
        return new com.navdy.obd.ECU[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
