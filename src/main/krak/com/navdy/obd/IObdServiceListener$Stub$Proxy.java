package com.navdy.obd;

class IObdServiceListener$Stub$Proxy implements com.navdy.obd.IObdServiceListener {
    private android.os.IBinder mRemote;
    
    IObdServiceListener$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.obd.IObdServiceListener";
    }
    
    public void onConnectionStateChange(int i) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
            a.writeInt(i);
            this.mRemote.transact(1, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void onRawData(String s) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
            a.writeString(s);
            this.mRemote.transact(3, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void onStatusMessage(String s) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
            a.writeString(s);
            this.mRemote.transact(2, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void scannedPids(java.util.List a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
            a0.writeTypedList(a);
            this.mRemote.transact(4, a0, a1, 0);
            a1.readException();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void scannedVIN(String s) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
            a.writeString(s);
            this.mRemote.transact(6, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void supportedPids(java.util.List a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.obd.IObdServiceListener");
            a0.writeTypedList(a);
            this.mRemote.transact(5, a0, a1, 0);
            a1.readException();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
    }
}
