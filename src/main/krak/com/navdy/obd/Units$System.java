package com.navdy.obd;


    public enum Units$System {
        None(0),
        Metric(1),
        US(2);

        private int value;
        Units$System(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class Units$System extends Enum {
//    final private static com.navdy.obd.Units$System[] $VALUES;
//    final public static com.navdy.obd.Units$System Metric;
//    final public static com.navdy.obd.Units$System None;
//    final public static com.navdy.obd.Units$System US;
//    
//    static {
//        None = new com.navdy.obd.Units$System("None", 0);
//        Metric = new com.navdy.obd.Units$System("Metric", 1);
//        US = new com.navdy.obd.Units$System("US", 2);
//        com.navdy.obd.Units$System[] a = new com.navdy.obd.Units$System[3];
//        a[0] = None;
//        a[1] = Metric;
//        a[2] = US;
//        $VALUES = a;
//    }
//    
//    private Units$System(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.obd.Units$System valueOf(String s) {
//        return (com.navdy.obd.Units$System)Enum.valueOf(com.navdy.obd.Units$System.class, s);
//    }
//    
//    public static com.navdy.obd.Units$System[] values() {
//        return $VALUES.clone();
//    }
//}
//