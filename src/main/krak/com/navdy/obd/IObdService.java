package com.navdy.obd;

abstract public interface IObdService extends android.os.IInterface {
    abstract public void addListener(com.navdy.obd.IObdServiceListener arg);
    
    
    abstract public void connect(String arg, com.navdy.obd.IObdServiceListener arg0);
    
    
    abstract public void disconnect();
    
    
    abstract public String getFirmwareVersion();
    
    
    abstract public int getState();
    
    
    abstract public String readPid(int arg);
    
    
    abstract public void removeListener(com.navdy.obd.IObdServiceListener arg);
    
    
    abstract public void reset();
    
    
    abstract public void scanPids(java.util.List arg, int arg0);
    
    
    abstract public void scanSupportedPids();
    
    
    abstract public void scanVIN();
    
    
    abstract public void startRawScan();
    
    
    abstract public boolean updateDeviceFirmware(String arg, String arg0);
}
