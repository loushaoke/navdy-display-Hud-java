package com.navdy.obd;

public class VoltageSettings implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    final public static float DEFAULT_CHARGING_VOLTAGE = 13.1f;
    final public static float DEFAULT_ENGINE_OFF_VOLTAGE = 12.9f;
    final public static float DEFAULT_LOW_BATTERY_VOLTAGE = 12.2f;
    final public float chargingVoltage;
    final public float engineOffVoltage;
    final public float lowBatteryVoltage;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.obd.VoltageSettings$1();
    }
    
    public VoltageSettings() {
        this.lowBatteryVoltage = 12.2f;
        this.engineOffVoltage = 12.9f;
        this.chargingVoltage = 13.1f;
    }
    
    public VoltageSettings(float f, float f0, float f1) {
        this.lowBatteryVoltage = f;
        this.engineOffVoltage = f0;
        this.chargingVoltage = f1;
    }
    
    public VoltageSettings(android.os.Parcel a) {
        this.lowBatteryVoltage = a.readFloat();
        this.engineOffVoltage = a.readFloat();
        this.chargingVoltage = a.readFloat();
    }
    
    public int describeContents() {
        return 0;
    }
    
    public String toString() {
        StringBuffer a = new StringBuffer("VoltageSettings{");
        a.append("lowBatteryVoltage=").append(this.lowBatteryVoltage);
        a.append(", engineOffVoltage=").append(this.engineOffVoltage);
        a.append(", chargingVoltage=").append(this.chargingVoltage);
        a.append((char)125);
        return a.toString();
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeFloat(this.lowBatteryVoltage);
        a.writeFloat(this.engineOffVoltage);
        a.writeFloat(this.chargingVoltage);
    }
}
