package com.navdy.obd;

public class ObdServiceInterface {
    final public static String ACTION_RESCAN = "com.navdy.obd.action.RESCAN";
    final public static String ACTION_START_AUTO_CONNECT = "com.navdy.obd.action.START_AUTO_CONNECT";
    final public static String ACTION_STOP_AUTO_CONNECT = "com.navdy.obd.action.STOP_AUTO_CONNECT";
    final public static String CAN_BUS_MONITOR_ERROR_MESSAGE_BAD_STATE = "BAD_STATE";
    final public static String GENERIC_CAN_BUS_MONITOR_ERROR_MESSAGE = "NO_DATA";
    final public static int MODE_J1939 = 1;
    final public static int MODE_OBD2 = 0;
    final public static String OBD_CLASS_NAME = "com.navdy.obd.ObdService";
    final public static String OBD_PACKAGE_NAME = "com.navdy.obd.app.ObdTestApp";
    
    public ObdServiceInterface() {
    }
    
    public static android.content.ComponentName getObdServiceComponent() {
        return new android.content.ComponentName("com.navdy.obd.app.ObdTestApp", "com.navdy.obd.ObdService");
    }
    
    public static void startObdService(android.content.Context a) {
        android.content.Intent a0 = new android.content.Intent();
        a0.setComponent(com.navdy.obd.ObdServiceInterface.getObdServiceComponent());
        a0.setAction("com.navdy.obd.action.START_AUTO_CONNECT");
        a.startService(a0);
    }
    
    public static void stopObdService(android.content.Context a) {
        android.content.Intent a0 = new android.content.Intent();
        a0.setComponent(com.navdy.obd.ObdServiceInterface.getObdServiceComponent());
        a0.setAction("com.navdy.obd.action.STOP_AUTO_CONNECT");
        a.startService(a0);
    }
}
