package com.navdy.obd;

final class VoltageSettings$1 implements android.os.Parcelable$Creator {
    VoltageSettings$1() {
    }
    
    public com.navdy.obd.VoltageSettings createFromParcel(android.os.Parcel a) {
        return new com.navdy.obd.VoltageSettings(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.obd.VoltageSettings[] newArray(int i) {
        return new com.navdy.obd.VoltageSettings[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
