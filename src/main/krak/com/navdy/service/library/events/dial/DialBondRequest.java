package com.navdy.service.library.events.dial;

final public class DialBondRequest extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.dial.DialBondRequest$DialAction DEFAULT_ACTION;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.dial.DialBondRequest$DialAction action;
    
    static {
        DEFAULT_ACTION = com.navdy.service.library.events.dial.DialBondRequest$DialAction.DIAL_BOND;
    }
    
    private DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest$Builder a) {
        this(a.action);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest$Builder a, com.navdy.service.library.events.dial.DialBondRequest$1 a0) {
        this(a);
    }
    
    public DialBondRequest(com.navdy.service.library.events.dial.DialBondRequest$DialAction a) {
        this.action = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.dial.DialBondRequest && this.equals(this.action, ((com.navdy.service.library.events.dial.DialBondRequest)a).action);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.action == null) ? 0 : this.action.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
