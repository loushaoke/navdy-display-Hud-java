package com.navdy.service.library.events;

final public class LegacyCapability extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.LegacyCapability[] $VALUES;
    final public static com.navdy.service.library.events.LegacyCapability CAPABILITY_COMPACT_UI;
    final public static com.navdy.service.library.events.LegacyCapability CAPABILITY_LOCAL_MUSIC_BROWSER;
    final public static com.navdy.service.library.events.LegacyCapability CAPABILITY_PLACE_TYPE_SEARCH;
    final public static com.navdy.service.library.events.LegacyCapability CAPABILITY_VOICE_SEARCH;
    final public static com.navdy.service.library.events.LegacyCapability CAPABILITY_VOICE_SEARCH_BETA;
    final private int value;
    
    static {
        CAPABILITY_VOICE_SEARCH_BETA = new com.navdy.service.library.events.LegacyCapability("CAPABILITY_VOICE_SEARCH_BETA", 0, 1);
        CAPABILITY_COMPACT_UI = new com.navdy.service.library.events.LegacyCapability("CAPABILITY_COMPACT_UI", 1, 2);
        CAPABILITY_PLACE_TYPE_SEARCH = new com.navdy.service.library.events.LegacyCapability("CAPABILITY_PLACE_TYPE_SEARCH", 2, 3);
        CAPABILITY_LOCAL_MUSIC_BROWSER = new com.navdy.service.library.events.LegacyCapability("CAPABILITY_LOCAL_MUSIC_BROWSER", 3, 4);
        CAPABILITY_VOICE_SEARCH = new com.navdy.service.library.events.LegacyCapability("CAPABILITY_VOICE_SEARCH", 4, 5);
        com.navdy.service.library.events.LegacyCapability[] a = new com.navdy.service.library.events.LegacyCapability[5];
        a[0] = CAPABILITY_VOICE_SEARCH_BETA;
        a[1] = CAPABILITY_COMPACT_UI;
        a[2] = CAPABILITY_PLACE_TYPE_SEARCH;
        a[3] = CAPABILITY_LOCAL_MUSIC_BROWSER;
        a[4] = CAPABILITY_VOICE_SEARCH;
        $VALUES = a;
    }
    
    private LegacyCapability(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.LegacyCapability valueOf(String s) {
        return (com.navdy.service.library.events.LegacyCapability)Enum.valueOf(com.navdy.service.library.events.LegacyCapability.class, s);
    }
    
    public static com.navdy.service.library.events.LegacyCapability[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
