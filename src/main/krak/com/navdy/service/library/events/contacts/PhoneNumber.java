package com.navdy.service.library.events.contacts;

final public class PhoneNumber extends com.squareup.wire.Message {
    final public static String DEFAULT_CUSTOMTYPE = "";
    final public static String DEFAULT_NUMBER = "";
    final public static com.navdy.service.library.events.contacts.PhoneNumberType DEFAULT_NUMBERTYPE;
    final private static long serialVersionUID = 0L;
    final public String customType;
    final public String number;
    final public com.navdy.service.library.events.contacts.PhoneNumberType numberType;
    
    static {
        DEFAULT_NUMBERTYPE = com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_HOME;
    }
    
    private PhoneNumber(com.navdy.service.library.events.contacts.PhoneNumber$Builder a) {
        this(a.number, a.numberType, a.customType);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhoneNumber(com.navdy.service.library.events.contacts.PhoneNumber$Builder a, com.navdy.service.library.events.contacts.PhoneNumber$1 a0) {
        this(a);
    }
    
    public PhoneNumber(String s, com.navdy.service.library.events.contacts.PhoneNumberType a, String s0) {
        this.number = s;
        this.numberType = a;
        this.customType = s0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.contacts.PhoneNumber) {
                com.navdy.service.library.events.contacts.PhoneNumber a0 = (com.navdy.service.library.events.contacts.PhoneNumber)a;
                boolean b0 = this.equals(this.number, a0.number);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.numberType, a0.numberType)) {
                        break label1;
                    }
                    if (this.equals(this.customType, a0.customType)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.number == null) ? 0 : this.number.hashCode()) * 37 + ((this.numberType == null) ? 0 : this.numberType.hashCode())) * 37 + ((this.customType == null) ? 0 : this.customType.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
