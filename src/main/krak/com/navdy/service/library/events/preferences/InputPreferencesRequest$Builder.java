package com.navdy.service.library.events.preferences;

final public class InputPreferencesRequest$Builder extends com.squareup.wire.Message.Builder {
    public Long serial_number;
    
    public InputPreferencesRequest$Builder() {
    }
    
    public InputPreferencesRequest$Builder(com.navdy.service.library.events.preferences.InputPreferencesRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.preferences.InputPreferencesRequest build() {
        return new com.navdy.service.library.events.preferences.InputPreferencesRequest(this, (com.navdy.service.library.events.preferences.InputPreferencesRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.InputPreferencesRequest$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
}
