package com.navdy.service.library.events.location;

final public class LatLong extends com.squareup.wire.Message {
    final public static Double DEFAULT_LATITUDE;
    final public static Double DEFAULT_LONGITUDE;
    final private static long serialVersionUID = 0L;
    final public Double latitude;
    final public Double longitude;
    
    static {
        DEFAULT_LATITUDE = Double.valueOf(0.0);
        DEFAULT_LONGITUDE = Double.valueOf(0.0);
    }
    
    private LatLong(com.navdy.service.library.events.location.LatLong$Builder a) {
        this(a.latitude, a.longitude);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    LatLong(com.navdy.service.library.events.location.LatLong$Builder a, com.navdy.service.library.events.location.LatLong$1 a0) {
        this(a);
    }
    
    public LatLong(Double a, Double a0) {
        this.latitude = a;
        this.longitude = a0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.location.LatLong) {
                com.navdy.service.library.events.location.LatLong a0 = (com.navdy.service.library.events.location.LatLong)a;
                boolean b0 = this.equals(this.latitude, a0.latitude);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.longitude, a0.longitude)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.latitude == null) ? 0 : this.latitude.hashCode()) * 37 + ((this.longitude == null) ? 0 : this.longitude.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
