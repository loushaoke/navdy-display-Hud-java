package com.navdy.service.library.events;

final public class Ping extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public Ping() {
    }
    
    private Ping(com.navdy.service.library.events.Ping$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    Ping(com.navdy.service.library.events.Ping$Builder a, com.navdy.service.library.events.Ping$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.Ping;
    }
    
    public int hashCode() {
        return 0;
    }
}
