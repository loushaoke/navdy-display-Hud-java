package com.navdy.service.library.events.audio;

final public class SpeechRequestStatus$Builder extends com.squareup.wire.Message.Builder {
    public String id;
    public com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType status;
    
    public SpeechRequestStatus$Builder() {
    }
    
    public SpeechRequestStatus$Builder(com.navdy.service.library.events.audio.SpeechRequestStatus a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.id = a.id;
            this.status = a.status;
        }
    }
    
    public com.navdy.service.library.events.audio.SpeechRequestStatus build() {
        return new com.navdy.service.library.events.audio.SpeechRequestStatus(this, (com.navdy.service.library.events.audio.SpeechRequestStatus$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.SpeechRequestStatus$Builder id(String s) {
        this.id = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.SpeechRequestStatus$Builder status(com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType a) {
        this.status = a;
        return this;
    }
}
