package com.navdy.service.library.events.settings;

final public class NetworkStateChange$Builder extends com.squareup.wire.Message.Builder {
    public Boolean cellNetwork;
    public Boolean networkAvailable;
    public Boolean reachability;
    public Boolean wifiNetwork;
    
    public NetworkStateChange$Builder() {
    }
    
    public NetworkStateChange$Builder(com.navdy.service.library.events.settings.NetworkStateChange a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.networkAvailable = a.networkAvailable;
            this.cellNetwork = a.cellNetwork;
            this.wifiNetwork = a.wifiNetwork;
            this.reachability = a.reachability;
        }
    }
    
    public com.navdy.service.library.events.settings.NetworkStateChange build() {
        return new com.navdy.service.library.events.settings.NetworkStateChange(this, (com.navdy.service.library.events.settings.NetworkStateChange$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.settings.NetworkStateChange$Builder cellNetwork(Boolean a) {
        this.cellNetwork = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.NetworkStateChange$Builder networkAvailable(Boolean a) {
        this.networkAvailable = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.NetworkStateChange$Builder reachability(Boolean a) {
        this.reachability = a;
        return this;
    }
    
    public com.navdy.service.library.events.settings.NetworkStateChange$Builder wifiNetwork(Boolean a) {
        this.wifiNetwork = a;
        return this;
    }
}
