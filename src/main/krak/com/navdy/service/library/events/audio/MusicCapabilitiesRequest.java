package com.navdy.service.library.events.audio;

final public class MusicCapabilitiesRequest extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public MusicCapabilitiesRequest() {
    }
    
    private MusicCapabilitiesRequest(com.navdy.service.library.events.audio.MusicCapabilitiesRequest$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicCapabilitiesRequest(com.navdy.service.library.events.audio.MusicCapabilitiesRequest$Builder a, com.navdy.service.library.events.audio.MusicCapabilitiesRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.audio.MusicCapabilitiesRequest;
    }
    
    public int hashCode() {
        return 0;
    }
}
