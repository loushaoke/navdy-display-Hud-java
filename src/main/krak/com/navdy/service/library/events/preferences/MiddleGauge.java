package com.navdy.service.library.events.preferences;

final public class MiddleGauge extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.MiddleGauge[] $VALUES;
    final public static com.navdy.service.library.events.preferences.MiddleGauge SPEEDOMETER;
    final public static com.navdy.service.library.events.preferences.MiddleGauge TACHOMETER;
    final private int value;
    
    static {
        SPEEDOMETER = new com.navdy.service.library.events.preferences.MiddleGauge("SPEEDOMETER", 0, 1);
        TACHOMETER = new com.navdy.service.library.events.preferences.MiddleGauge("TACHOMETER", 1, 2);
        com.navdy.service.library.events.preferences.MiddleGauge[] a = new com.navdy.service.library.events.preferences.MiddleGauge[2];
        a[0] = SPEEDOMETER;
        a[1] = TACHOMETER;
        $VALUES = a;
    }
    
    private MiddleGauge(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.MiddleGauge valueOf(String s) {
        return (com.navdy.service.library.events.preferences.MiddleGauge)Enum.valueOf(com.navdy.service.library.events.preferences.MiddleGauge.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.MiddleGauge[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
