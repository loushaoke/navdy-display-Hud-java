package com.navdy.service.library.events.navigation;

final public class NavigationSessionRouteChange extends com.squareup.wire.Message {
    final public static String DEFAULT_OLDROUTEID = "";
    final public static com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason DEFAULT_REASON;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.navigation.NavigationRouteResult newRoute;
    final public String oldRouteId;
    final public com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason reason;
    
    static {
        DEFAULT_REASON = com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason.NAV_SESSION_OFF_REROUTE;
    }
    
    private NavigationSessionRouteChange(com.navdy.service.library.events.navigation.NavigationSessionRouteChange$Builder a) {
        this(a.oldRouteId, a.newRoute, a.reason);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationSessionRouteChange(com.navdy.service.library.events.navigation.NavigationSessionRouteChange$Builder a, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$1 a0) {
        this(a);
    }
    
    public NavigationSessionRouteChange(String s, com.navdy.service.library.events.navigation.NavigationRouteResult a, com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason a0) {
        this.oldRouteId = s;
        this.newRoute = a;
        this.reason = a0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationSessionRouteChange) {
                com.navdy.service.library.events.navigation.NavigationSessionRouteChange a0 = (com.navdy.service.library.events.navigation.NavigationSessionRouteChange)a;
                boolean b0 = this.equals(this.oldRouteId, a0.oldRouteId);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.newRoute, a0.newRoute)) {
                        break label1;
                    }
                    if (this.equals(this.reason, a0.reason)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.oldRouteId == null) ? 0 : this.oldRouteId.hashCode()) * 37 + ((this.newRoute == null) ? 0 : this.newRoute.hashCode())) * 37 + ((this.reason == null) ? 0 : this.reason.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
