package com.navdy.service.library.events.audio;

final public class VoiceAssistRequest extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_END;
    final private static long serialVersionUID = 0L;
    final public Boolean end;
    
    static {
        DEFAULT_END = Boolean.valueOf(false);
    }
    
    private VoiceAssistRequest(com.navdy.service.library.events.audio.VoiceAssistRequest$Builder a) {
        this(a.end);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    VoiceAssistRequest(com.navdy.service.library.events.audio.VoiceAssistRequest$Builder a, com.navdy.service.library.events.audio.VoiceAssistRequest$1 a0) {
        this(a);
    }
    
    public VoiceAssistRequest(Boolean a) {
        this.end = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.audio.VoiceAssistRequest && this.equals(this.end, ((com.navdy.service.library.events.audio.VoiceAssistRequest)a).end);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.end == null) ? 0 : this.end.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
