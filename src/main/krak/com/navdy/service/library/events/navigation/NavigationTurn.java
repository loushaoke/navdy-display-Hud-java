package com.navdy.service.library.events.navigation;

final public class NavigationTurn extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.navigation.NavigationTurn[] $VALUES;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_EASY_LEFT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_EASY_RIGHT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_END;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_EXIT_LEFT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_EXIT_RIGHT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_EXIT_ROUNDABOUT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_FERRY;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_FOLLOW_ROUTE;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_KEEP_LEFT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_KEEP_RIGHT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_LEFT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_MERGE_LEFT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_MERGE_RIGHT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_MOTORWAY;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_OUT_OF_ROUTE;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_RIGHT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_ROUNDABOUT_E;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_ROUNDABOUT_N;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_ROUNDABOUT_NE;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_ROUNDABOUT_NW;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_ROUNDABOUT_S;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_ROUNDABOUT_SE;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_ROUNDABOUT_SW;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_ROUNDABOUT_W;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_SHARP_LEFT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_SHARP_RIGHT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_START;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_STATE_BOUNDARY;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_STRAIGHT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_UNKNOWN;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_UTURN_LEFT;
    final public static com.navdy.service.library.events.navigation.NavigationTurn NAV_TURN_UTURN_RIGHT;
    final private int value;
    
    static {
        NAV_TURN_START = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_START", 0, 1);
        NAV_TURN_EASY_LEFT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_EASY_LEFT", 1, 2);
        NAV_TURN_EASY_RIGHT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_EASY_RIGHT", 2, 3);
        NAV_TURN_END = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_END", 3, 4);
        NAV_TURN_KEEP_LEFT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_KEEP_LEFT", 4, 5);
        NAV_TURN_KEEP_RIGHT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_KEEP_RIGHT", 5, 6);
        NAV_TURN_LEFT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_LEFT", 6, 7);
        NAV_TURN_OUT_OF_ROUTE = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_OUT_OF_ROUTE", 7, 8);
        NAV_TURN_RIGHT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_RIGHT", 8, 9);
        NAV_TURN_SHARP_LEFT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_SHARP_LEFT", 9, 10);
        NAV_TURN_SHARP_RIGHT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_SHARP_RIGHT", 10, 11);
        NAV_TURN_STRAIGHT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_STRAIGHT", 11, 12);
        NAV_TURN_UTURN_LEFT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_UTURN_LEFT", 12, 13);
        NAV_TURN_UTURN_RIGHT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_UTURN_RIGHT", 13, 14);
        NAV_TURN_ROUNDABOUT_SE = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_ROUNDABOUT_SE", 14, 15);
        NAV_TURN_ROUNDABOUT_E = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_ROUNDABOUT_E", 15, 16);
        NAV_TURN_ROUNDABOUT_NE = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_ROUNDABOUT_NE", 16, 17);
        NAV_TURN_ROUNDABOUT_N = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_ROUNDABOUT_N", 17, 18);
        NAV_TURN_ROUNDABOUT_NW = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_ROUNDABOUT_NW", 18, 19);
        NAV_TURN_ROUNDABOUT_W = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_ROUNDABOUT_W", 19, 20);
        NAV_TURN_ROUNDABOUT_SW = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_ROUNDABOUT_SW", 20, 21);
        NAV_TURN_ROUNDABOUT_S = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_ROUNDABOUT_S", 21, 22);
        NAV_TURN_FERRY = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_FERRY", 22, 23);
        NAV_TURN_STATE_BOUNDARY = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_STATE_BOUNDARY", 23, 24);
        NAV_TURN_FOLLOW_ROUTE = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_FOLLOW_ROUTE", 24, 25);
        NAV_TURN_EXIT_RIGHT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_EXIT_RIGHT", 25, 26);
        NAV_TURN_EXIT_LEFT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_EXIT_LEFT", 26, 27);
        NAV_TURN_MOTORWAY = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_MOTORWAY", 27, 28);
        NAV_TURN_EXIT_ROUNDABOUT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_EXIT_ROUNDABOUT", 28, 29);
        NAV_TURN_UNKNOWN = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_UNKNOWN", 29, 30);
        NAV_TURN_MERGE_LEFT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_MERGE_LEFT", 30, 31);
        NAV_TURN_MERGE_RIGHT = new com.navdy.service.library.events.navigation.NavigationTurn("NAV_TURN_MERGE_RIGHT", 31, 32);
        com.navdy.service.library.events.navigation.NavigationTurn[] a = new com.navdy.service.library.events.navigation.NavigationTurn[32];
        a[0] = NAV_TURN_START;
        a[1] = NAV_TURN_EASY_LEFT;
        a[2] = NAV_TURN_EASY_RIGHT;
        a[3] = NAV_TURN_END;
        a[4] = NAV_TURN_KEEP_LEFT;
        a[5] = NAV_TURN_KEEP_RIGHT;
        a[6] = NAV_TURN_LEFT;
        a[7] = NAV_TURN_OUT_OF_ROUTE;
        a[8] = NAV_TURN_RIGHT;
        a[9] = NAV_TURN_SHARP_LEFT;
        a[10] = NAV_TURN_SHARP_RIGHT;
        a[11] = NAV_TURN_STRAIGHT;
        a[12] = NAV_TURN_UTURN_LEFT;
        a[13] = NAV_TURN_UTURN_RIGHT;
        a[14] = NAV_TURN_ROUNDABOUT_SE;
        a[15] = NAV_TURN_ROUNDABOUT_E;
        a[16] = NAV_TURN_ROUNDABOUT_NE;
        a[17] = NAV_TURN_ROUNDABOUT_N;
        a[18] = NAV_TURN_ROUNDABOUT_NW;
        a[19] = NAV_TURN_ROUNDABOUT_W;
        a[20] = NAV_TURN_ROUNDABOUT_SW;
        a[21] = NAV_TURN_ROUNDABOUT_S;
        a[22] = NAV_TURN_FERRY;
        a[23] = NAV_TURN_STATE_BOUNDARY;
        a[24] = NAV_TURN_FOLLOW_ROUTE;
        a[25] = NAV_TURN_EXIT_RIGHT;
        a[26] = NAV_TURN_EXIT_LEFT;
        a[27] = NAV_TURN_MOTORWAY;
        a[28] = NAV_TURN_EXIT_ROUNDABOUT;
        a[29] = NAV_TURN_UNKNOWN;
        a[30] = NAV_TURN_MERGE_LEFT;
        a[31] = NAV_TURN_MERGE_RIGHT;
        $VALUES = a;
    }
    
    private NavigationTurn(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.navigation.NavigationTurn valueOf(String s) {
        return (com.navdy.service.library.events.navigation.NavigationTurn)Enum.valueOf(com.navdy.service.library.events.navigation.NavigationTurn.class, s);
    }
    
    public static com.navdy.service.library.events.navigation.NavigationTurn[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
