package com.navdy.service.library.events.file;

final public class FileTransferStatus$Builder extends com.squareup.wire.Message.Builder {
    public Integer chunkIndex;
    public com.navdy.service.library.events.file.FileTransferError error;
    public Boolean success;
    public Long totalBytesTransferred;
    public Boolean transferComplete;
    public Integer transferId;
    
    public FileTransferStatus$Builder() {
    }
    
    public FileTransferStatus$Builder(com.navdy.service.library.events.file.FileTransferStatus a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.transferId = a.transferId;
            this.success = a.success;
            this.totalBytesTransferred = a.totalBytesTransferred;
            this.error = a.error;
            this.chunkIndex = a.chunkIndex;
            this.transferComplete = a.transferComplete;
        }
    }
    
    public com.navdy.service.library.events.file.FileTransferStatus build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.file.FileTransferStatus(this, (com.navdy.service.library.events.file.FileTransferStatus$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.file.FileTransferStatus$Builder chunkIndex(Integer a) {
        this.chunkIndex = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferStatus$Builder error(com.navdy.service.library.events.file.FileTransferError a) {
        this.error = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferStatus$Builder success(Boolean a) {
        this.success = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferStatus$Builder totalBytesTransferred(Long a) {
        this.totalBytesTransferred = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferStatus$Builder transferComplete(Boolean a) {
        this.transferComplete = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferStatus$Builder transferId(Integer a) {
        this.transferId = a;
        return this;
    }
}
