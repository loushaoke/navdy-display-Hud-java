package com.navdy.service.library.events.audio;

final public class MusicCollectionRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_COLLECTIONID = "";
    final public static com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    final public static com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE;
    final public static java.util.List DEFAULT_FILTERS;
    final public static com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_GROUPBY;
    final public static Boolean DEFAULT_INCLUDECHARACTERMAP;
    final public static Integer DEFAULT_LIMIT;
    final public static Integer DEFAULT_OFFSET;
    final private static long serialVersionUID = 0L;
    final public String collectionId;
    final public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    final public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    final public java.util.List filters;
    final public com.navdy.service.library.events.audio.MusicCollectionType groupBy;
    final public Boolean includeCharacterMap;
    final public Integer limit;
    final public Integer offset;
    
    static {
        DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
        DEFAULT_FILTERS = java.util.Collections.emptyList();
        DEFAULT_LIMIT = Integer.valueOf(0);
        DEFAULT_OFFSET = Integer.valueOf(0);
        DEFAULT_INCLUDECHARACTERMAP = Boolean.valueOf(false);
        DEFAULT_GROUPBY = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    }
    
    private MusicCollectionRequest(com.navdy.service.library.events.audio.MusicCollectionRequest$Builder a) {
        this(a.collectionSource, a.collectionType, a.filters, a.collectionId, a.limit, a.offset, a.includeCharacterMap, a.groupBy);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicCollectionRequest(com.navdy.service.library.events.audio.MusicCollectionRequest$Builder a, com.navdy.service.library.events.audio.MusicCollectionRequest$1 a0) {
        this(a);
    }
    
    public MusicCollectionRequest(com.navdy.service.library.events.audio.MusicCollectionSource a, com.navdy.service.library.events.audio.MusicCollectionType a0, java.util.List a1, String s, Integer a2, Integer a3, Boolean a4, com.navdy.service.library.events.audio.MusicCollectionType a5) {
        this.collectionSource = a;
        this.collectionType = a0;
        this.filters = com.navdy.service.library.events.audio.MusicCollectionRequest.immutableCopyOf(a1);
        this.collectionId = s;
        this.limit = a2;
        this.offset = a3;
        this.includeCharacterMap = a4;
        this.groupBy = a5;
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.audio.MusicCollectionRequest.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicCollectionRequest) {
                com.navdy.service.library.events.audio.MusicCollectionRequest a0 = (com.navdy.service.library.events.audio.MusicCollectionRequest)a;
                boolean b0 = this.equals(this.collectionSource, a0.collectionSource);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.collectionType, a0.collectionType)) {
                        break label1;
                    }
                    if (!this.equals(this.filters, a0.filters)) {
                        break label1;
                    }
                    if (!this.equals(this.collectionId, a0.collectionId)) {
                        break label1;
                    }
                    if (!this.equals(this.limit, a0.limit)) {
                        break label1;
                    }
                    if (!this.equals(this.offset, a0.offset)) {
                        break label1;
                    }
                    if (!this.equals(this.includeCharacterMap, a0.includeCharacterMap)) {
                        break label1;
                    }
                    if (this.equals(this.groupBy, a0.groupBy)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((((this.collectionSource == null) ? 0 : this.collectionSource.hashCode()) * 37 + ((this.collectionType == null) ? 0 : this.collectionType.hashCode())) * 37 + ((this.filters == null) ? 1 : this.filters.hashCode())) * 37 + ((this.collectionId == null) ? 0 : this.collectionId.hashCode())) * 37 + ((this.limit == null) ? 0 : this.limit.hashCode())) * 37 + ((this.offset == null) ? 0 : this.offset.hashCode())) * 37 + ((this.includeCharacterMap == null) ? 0 : this.includeCharacterMap.hashCode())) * 37 + ((this.groupBy == null) ? 0 : this.groupBy.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
