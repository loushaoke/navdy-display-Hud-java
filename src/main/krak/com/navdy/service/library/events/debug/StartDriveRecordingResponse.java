package com.navdy.service.library.events.debug;

final public class StartDriveRecordingResponse extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.RequestStatus status;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    }
    
    public StartDriveRecordingResponse(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
    }
    
    private StartDriveRecordingResponse(com.navdy.service.library.events.debug.StartDriveRecordingResponse$Builder a) {
        this(a.status);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    StartDriveRecordingResponse(com.navdy.service.library.events.debug.StartDriveRecordingResponse$Builder a, com.navdy.service.library.events.debug.StartDriveRecordingResponse$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.debug.StartDriveRecordingResponse && this.equals(this.status, ((com.navdy.service.library.events.debug.StartDriveRecordingResponse)a).status);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.status == null) ? 0 : this.status.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
