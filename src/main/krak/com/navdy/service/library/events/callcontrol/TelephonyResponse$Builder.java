package com.navdy.service.library.events.callcontrol;

final public class TelephonyResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.callcontrol.CallAction action;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public TelephonyResponse$Builder() {
    }
    
    public TelephonyResponse$Builder(com.navdy.service.library.events.callcontrol.TelephonyResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.action = a.action;
            this.status = a.status;
            this.statusDetail = a.statusDetail;
        }
    }
    
    public com.navdy.service.library.events.callcontrol.TelephonyResponse$Builder action(com.navdy.service.library.events.callcontrol.CallAction a) {
        this.action = a;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.TelephonyResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.callcontrol.TelephonyResponse(this, (com.navdy.service.library.events.callcontrol.TelephonyResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.callcontrol.TelephonyResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.TelephonyResponse$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
