package com.navdy.service.library.events.messaging;

final public class SmsMessageRequest$Builder extends com.squareup.wire.Message.Builder {
    public String id;
    public String message;
    public String name;
    public String number;
    
    public SmsMessageRequest$Builder() {
    }
    
    public SmsMessageRequest$Builder(com.navdy.service.library.events.messaging.SmsMessageRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.number = a.number;
            this.message = a.message;
            this.name = a.name;
            this.id = a.id;
        }
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.messaging.SmsMessageRequest(this, (com.navdy.service.library.events.messaging.SmsMessageRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageRequest$Builder id(String s) {
        this.id = s;
        return this;
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageRequest$Builder message(String s) {
        this.message = s;
        return this;
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageRequest$Builder name(String s) {
        this.name = s;
        return this;
    }
    
    public com.navdy.service.library.events.messaging.SmsMessageRequest$Builder number(String s) {
        this.number = s;
        return this;
    }
}
