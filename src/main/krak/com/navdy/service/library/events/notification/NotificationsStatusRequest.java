package com.navdy.service.library.events.notification;

final public class NotificationsStatusRequest extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.notification.NotificationsState DEFAULT_NEWSTATE;
    final public static com.navdy.service.library.events.notification.ServiceType DEFAULT_SERVICE;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.notification.NotificationsState newState;
    final public com.navdy.service.library.events.notification.ServiceType service;
    
    static {
        DEFAULT_NEWSTATE = com.navdy.service.library.events.notification.NotificationsState.NOTIFICATIONS_ENABLED;
        DEFAULT_SERVICE = com.navdy.service.library.events.notification.ServiceType.SERVICE_ANCS;
    }
    
    public NotificationsStatusRequest(com.navdy.service.library.events.notification.NotificationsState a, com.navdy.service.library.events.notification.ServiceType a0) {
        this.newState = a;
        this.service = a0;
    }
    
    private NotificationsStatusRequest(com.navdy.service.library.events.notification.NotificationsStatusRequest$Builder a) {
        this(a.newState, a.service);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NotificationsStatusRequest(com.navdy.service.library.events.notification.NotificationsStatusRequest$Builder a, com.navdy.service.library.events.notification.NotificationsStatusRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.notification.NotificationsStatusRequest) {
                com.navdy.service.library.events.notification.NotificationsStatusRequest a0 = (com.navdy.service.library.events.notification.NotificationsStatusRequest)a;
                boolean b0 = this.equals(this.newState, a0.newState);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.service, a0.service)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.newState == null) ? 0 : this.newState.hashCode()) * 37 + ((this.service == null) ? 0 : this.service.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
