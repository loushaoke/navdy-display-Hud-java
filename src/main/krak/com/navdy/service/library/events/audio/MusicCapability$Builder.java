package com.navdy.service.library.events.audio;

final public class MusicCapability$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus authorizationStatus;
    public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    public java.util.List collectionTypes;
    public Long serial_number;
    public java.util.List supportedRepeatModes;
    public java.util.List supportedShuffleModes;
    
    public MusicCapability$Builder() {
    }
    
    public MusicCapability$Builder(com.navdy.service.library.events.audio.MusicCapability a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.collectionSource = a.collectionSource;
            this.collectionTypes = com.navdy.service.library.events.audio.MusicCapability.access$000(a.collectionTypes);
            this.authorizationStatus = a.authorizationStatus;
            this.supportedShuffleModes = com.navdy.service.library.events.audio.MusicCapability.access$100(a.supportedShuffleModes);
            this.supportedRepeatModes = com.navdy.service.library.events.audio.MusicCapability.access$200(a.supportedRepeatModes);
            this.serial_number = a.serial_number;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicCapability$Builder authorizationStatus(com.navdy.service.library.events.audio.MusicCapability$MusicAuthorizationStatus a) {
        this.authorizationStatus = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCapability build() {
        return new com.navdy.service.library.events.audio.MusicCapability(this, (com.navdy.service.library.events.audio.MusicCapability$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicCapability$Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource a) {
        this.collectionSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCapability$Builder collectionTypes(java.util.List a) {
        this.collectionTypes = com.navdy.service.library.events.audio.MusicCapability$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCapability$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCapability$Builder supportedRepeatModes(java.util.List a) {
        this.supportedRepeatModes = com.navdy.service.library.events.audio.MusicCapability$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCapability$Builder supportedShuffleModes(java.util.List a) {
        this.supportedShuffleModes = com.navdy.service.library.events.audio.MusicCapability$Builder.checkForNulls(a);
        return this;
    }
}
