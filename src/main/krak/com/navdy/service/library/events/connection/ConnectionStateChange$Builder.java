package com.navdy.service.library.events.connection;

final public class ConnectionStateChange$Builder extends com.squareup.wire.Message.Builder {
    public String remoteDeviceId;
    public com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState state;
    
    public ConnectionStateChange$Builder() {
    }
    
    public ConnectionStateChange$Builder(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.remoteDeviceId = a.remoteDeviceId;
            this.state = a.state;
        }
    }
    
    public com.navdy.service.library.events.connection.ConnectionStateChange build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.connection.ConnectionStateChange(this, (com.navdy.service.library.events.connection.ConnectionStateChange$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.connection.ConnectionStateChange$Builder remoteDeviceId(String s) {
        this.remoteDeviceId = s;
        return this;
    }
    
    public com.navdy.service.library.events.connection.ConnectionStateChange$Builder state(com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState a) {
        this.state = a;
        return this;
    }
}
