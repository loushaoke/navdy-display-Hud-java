package com.navdy.service.library.events.location;

final public class TransmitLocation extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_SENDLOCATION;
    final private static long serialVersionUID = 0L;
    final public Boolean sendLocation;
    
    static {
        DEFAULT_SENDLOCATION = Boolean.valueOf(false);
    }
    
    private TransmitLocation(com.navdy.service.library.events.location.TransmitLocation$Builder a) {
        this(a.sendLocation);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    TransmitLocation(com.navdy.service.library.events.location.TransmitLocation$Builder a, com.navdy.service.library.events.location.TransmitLocation$1 a0) {
        this(a);
    }
    
    public TransmitLocation(Boolean a) {
        this.sendLocation = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.location.TransmitLocation && this.equals(this.sendLocation, ((com.navdy.service.library.events.location.TransmitLocation)a).sendLocation);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.sendLocation == null) ? 0 : this.sendLocation.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
