package com.navdy.service.library.events.audio;

final public class SpeechRequestStatus$SpeechRequestStatusType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType[] $VALUES;
    final public static com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType SPEECH_REQUEST_STARTING;
    final public static com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType SPEECH_REQUEST_STOPPED;
    final private int value;
    
    static {
        SPEECH_REQUEST_STARTING = new com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType("SPEECH_REQUEST_STARTING", 0, 1);
        SPEECH_REQUEST_STOPPED = new com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType("SPEECH_REQUEST_STOPPED", 1, 2);
        com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType[] a = new com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType[2];
        a[0] = SPEECH_REQUEST_STARTING;
        a[1] = SPEECH_REQUEST_STOPPED;
        $VALUES = a;
    }
    
    private SpeechRequestStatus$SpeechRequestStatusType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType valueOf(String s) {
        return (com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType)Enum.valueOf(com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType.class, s);
    }
    
    public static com.navdy.service.library.events.audio.SpeechRequestStatus$SpeechRequestStatusType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
