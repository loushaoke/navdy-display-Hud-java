package com.navdy.service.library.events.navigation;

final public class NavigationRouteResult$Builder extends com.squareup.wire.Message.Builder {
    public String address;
    public Integer duration;
    public Integer duration_traffic;
    public java.util.List geoPoints_OBSOLETE;
    public String label;
    public Integer length;
    public String routeId;
    public java.util.List routeLatLongs;
    public String via;
    
    public NavigationRouteResult$Builder() {
    }
    
    public NavigationRouteResult$Builder(com.navdy.service.library.events.navigation.NavigationRouteResult a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.routeId = a.routeId;
            this.label = a.label;
            this.geoPoints_OBSOLETE = com.navdy.service.library.events.navigation.NavigationRouteResult.access$000(a.geoPoints_OBSOLETE);
            this.length = a.length;
            this.duration = a.duration;
            this.duration_traffic = a.duration_traffic;
            this.routeLatLongs = com.navdy.service.library.events.navigation.NavigationRouteResult.access$100(a.routeLatLongs);
            this.via = a.via;
            this.address = a.address;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult$Builder address(String s) {
        this.address = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.NavigationRouteResult(this, (com.navdy.service.library.events.navigation.NavigationRouteResult$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult$Builder duration(Integer a) {
        this.duration = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult$Builder duration_traffic(Integer a) {
        this.duration_traffic = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult$Builder geoPoints_OBSOLETE(java.util.List a) {
        this.geoPoints_OBSOLETE = com.navdy.service.library.events.navigation.NavigationRouteResult$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult$Builder length(Integer a) {
        this.length = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult$Builder routeId(String s) {
        this.routeId = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult$Builder routeLatLongs(java.util.List a) {
        this.routeLatLongs = com.navdy.service.library.events.navigation.NavigationRouteResult$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult$Builder via(String s) {
        this.via = s;
        return this;
    }
}
