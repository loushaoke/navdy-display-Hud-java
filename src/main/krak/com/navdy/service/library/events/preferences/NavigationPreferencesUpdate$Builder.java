package com.navdy.service.library.events.preferences;

final public class NavigationPreferencesUpdate$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.preferences.NavigationPreferences preferences;
    public Long serial_number;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public NavigationPreferencesUpdate$Builder() {
    }
    
    public NavigationPreferencesUpdate$Builder(com.navdy.service.library.events.preferences.NavigationPreferencesUpdate a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.serial_number = a.serial_number;
            this.preferences = a.preferences;
        }
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferencesUpdate build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.preferences.NavigationPreferencesUpdate(this, (com.navdy.service.library.events.preferences.NavigationPreferencesUpdate$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferencesUpdate$Builder preferences(com.navdy.service.library.events.preferences.NavigationPreferences a) {
        this.preferences = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferencesUpdate$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferencesUpdate$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferencesUpdate$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
