package com.navdy.service.library.events.navigation;

final public class NavigationRouteResponse extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_CONSIDEREDTRAFFIC;
    final public static String DEFAULT_LABEL = "";
    final public static String DEFAULT_REQUESTID = "";
    final public static java.util.List DEFAULT_RESULTS;
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUSDETAIL = "";
    final private static long serialVersionUID = 0L;
    final public Boolean consideredTraffic;
    final public com.navdy.service.library.events.location.Coordinate destination;
    final public String label;
    final public String requestId;
    final public java.util.List results;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String statusDetail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_RESULTS = java.util.Collections.emptyList();
        DEFAULT_CONSIDEREDTRAFFIC = Boolean.valueOf(false);
    }
    
    public NavigationRouteResponse(com.navdy.service.library.events.RequestStatus a, String s, com.navdy.service.library.events.location.Coordinate a0, String s0, java.util.List a1, Boolean a2, String s1) {
        this.status = a;
        this.statusDetail = s;
        this.destination = a0;
        this.label = s0;
        this.results = com.navdy.service.library.events.navigation.NavigationRouteResponse.immutableCopyOf(a1);
        this.consideredTraffic = a2;
        this.requestId = s1;
    }
    
    private NavigationRouteResponse(com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder a) {
        this(a.status, a.statusDetail, a.destination, a.label, a.results, a.consideredTraffic, a.requestId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationRouteResponse(com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder a, com.navdy.service.library.events.navigation.NavigationRouteResponse$1 a0) {
        this(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.navigation.NavigationRouteResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationRouteResponse) {
                com.navdy.service.library.events.navigation.NavigationRouteResponse a0 = (com.navdy.service.library.events.navigation.NavigationRouteResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.statusDetail, a0.statusDetail)) {
                        break label1;
                    }
                    if (!this.equals(this.destination, a0.destination)) {
                        break label1;
                    }
                    if (!this.equals(this.label, a0.label)) {
                        break label1;
                    }
                    if (!this.equals(this.results, a0.results)) {
                        break label1;
                    }
                    if (!this.equals(this.consideredTraffic, a0.consideredTraffic)) {
                        break label1;
                    }
                    if (this.equals(this.requestId, a0.requestId)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.statusDetail == null) ? 0 : this.statusDetail.hashCode())) * 37 + ((this.destination == null) ? 0 : this.destination.hashCode())) * 37 + ((this.label == null) ? 0 : this.label.hashCode())) * 37 + ((this.results == null) ? 1 : this.results.hashCode())) * 37 + ((this.consideredTraffic == null) ? 0 : this.consideredTraffic.hashCode())) * 37 + ((this.requestId == null) ? 0 : this.requestId.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
