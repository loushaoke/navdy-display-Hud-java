package com.navdy.service.library.events.connection;

final public class ConnectionStatus$Builder extends com.squareup.wire.Message.Builder {
    public String remoteDeviceId;
    public com.navdy.service.library.events.connection.ConnectionStatus$Status status;
    
    public ConnectionStatus$Builder() {
    }
    
    public ConnectionStatus$Builder(com.navdy.service.library.events.connection.ConnectionStatus a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.remoteDeviceId = a.remoteDeviceId;
        }
    }
    
    public com.navdy.service.library.events.connection.ConnectionStatus build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.connection.ConnectionStatus(this, (com.navdy.service.library.events.connection.ConnectionStatus$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.connection.ConnectionStatus$Builder remoteDeviceId(String s) {
        this.remoteDeviceId = s;
        return this;
    }
    
    public com.navdy.service.library.events.connection.ConnectionStatus$Builder status(com.navdy.service.library.events.connection.ConnectionStatus$Status a) {
        this.status = a;
        return this;
    }
}
