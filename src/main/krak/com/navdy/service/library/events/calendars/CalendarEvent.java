package com.navdy.service.library.events.calendars;

final public class CalendarEvent extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_ALL_DAY;
    final public static String DEFAULT_CALENDAR_NAME = "";
    final public static Integer DEFAULT_COLOR;
    final public static String DEFAULT_DISPLAY_NAME = "";
    final public static Long DEFAULT_END_TIME;
    final public static String DEFAULT_LOCATION = "";
    final public static Long DEFAULT_START_TIME;
    final private static long serialVersionUID = 0L;
    final public Boolean all_day;
    final public String calendar_name;
    final public Integer color;
    final public com.navdy.service.library.events.destination.Destination destination;
    final public String display_name;
    final public Long end_time;
    final public String location;
    final public Long start_time;
    
    static {
        DEFAULT_START_TIME = Long.valueOf(0L);
        DEFAULT_END_TIME = Long.valueOf(0L);
        DEFAULT_ALL_DAY = Boolean.valueOf(false);
        DEFAULT_COLOR = Integer.valueOf(0);
    }
    
    private CalendarEvent(com.navdy.service.library.events.calendars.CalendarEvent$Builder a) {
        this(a.display_name, a.start_time, a.end_time, a.all_day, a.color, a.location, a.calendar_name, a.destination);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    CalendarEvent(com.navdy.service.library.events.calendars.CalendarEvent$Builder a, com.navdy.service.library.events.calendars.CalendarEvent$1 a0) {
        this(a);
    }
    
    public CalendarEvent(String s, Long a, Long a0, Boolean a1, Integer a2, String s0, String s1, com.navdy.service.library.events.destination.Destination a3) {
        this.display_name = s;
        this.start_time = a;
        this.end_time = a0;
        this.all_day = a1;
        this.color = a2;
        this.location = s0;
        this.calendar_name = s1;
        this.destination = a3;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.calendars.CalendarEvent) {
                com.navdy.service.library.events.calendars.CalendarEvent a0 = (com.navdy.service.library.events.calendars.CalendarEvent)a;
                boolean b0 = this.equals(this.display_name, a0.display_name);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.start_time, a0.start_time)) {
                        break label1;
                    }
                    if (!this.equals(this.end_time, a0.end_time)) {
                        break label1;
                    }
                    if (!this.equals(this.all_day, a0.all_day)) {
                        break label1;
                    }
                    if (!this.equals(this.color, a0.color)) {
                        break label1;
                    }
                    if (!this.equals(this.location, a0.location)) {
                        break label1;
                    }
                    if (!this.equals(this.calendar_name, a0.calendar_name)) {
                        break label1;
                    }
                    if (this.equals(this.destination, a0.destination)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((((this.display_name == null) ? 0 : this.display_name.hashCode()) * 37 + ((this.start_time == null) ? 0 : this.start_time.hashCode())) * 37 + ((this.end_time == null) ? 0 : this.end_time.hashCode())) * 37 + ((this.all_day == null) ? 0 : this.all_day.hashCode())) * 37 + ((this.color == null) ? 0 : this.color.hashCode())) * 37 + ((this.location == null) ? 0 : this.location.hashCode())) * 37 + ((this.calendar_name == null) ? 0 : this.calendar_name.hashCode())) * 37 + ((this.destination == null) ? 0 : this.destination.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
