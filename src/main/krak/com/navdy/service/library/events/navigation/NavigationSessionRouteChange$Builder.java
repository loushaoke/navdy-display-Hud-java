package com.navdy.service.library.events.navigation;

final public class NavigationSessionRouteChange$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.navigation.NavigationRouteResult newRoute;
    public String oldRouteId;
    public com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason reason;
    
    public NavigationSessionRouteChange$Builder() {
    }
    
    public NavigationSessionRouteChange$Builder(com.navdy.service.library.events.navigation.NavigationSessionRouteChange a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.oldRouteId = a.oldRouteId;
            this.newRoute = a.newRoute;
            this.reason = a.reason;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRouteChange build() {
        return new com.navdy.service.library.events.navigation.NavigationSessionRouteChange(this, (com.navdy.service.library.events.navigation.NavigationSessionRouteChange$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRouteChange$Builder newRoute(com.navdy.service.library.events.navigation.NavigationRouteResult a) {
        this.newRoute = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRouteChange$Builder oldRouteId(String s) {
        this.oldRouteId = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionRouteChange$Builder reason(com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason a) {
        this.reason = a;
        return this;
    }
}
