package com.navdy.service.library.events.places;

final public class PlaceType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.places.PlaceType[] $VALUES;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_AIRPORT;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_ATM;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_BANK;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_BAR;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_CALENDAR_EVENT;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_COFFEE;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_CONTACT;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_ENTERTAINMENT;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_GAS;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_GYM;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_HOSPITAL;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_PARK;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_PARKING;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_RESTAURANT;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_SCHOOL;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_STORE;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_TRANSIT;
    final public static com.navdy.service.library.events.places.PlaceType PLACE_TYPE_UNKNOWN;
    final private int value;
    
    static {
        PLACE_TYPE_UNKNOWN = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_UNKNOWN", 0, 0);
        PLACE_TYPE_ATM = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_ATM", 1, 1);
        PLACE_TYPE_BANK = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_BANK", 2, 2);
        PLACE_TYPE_SCHOOL = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_SCHOOL", 3, 3);
        PLACE_TYPE_STORE = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_STORE", 4, 4);
        PLACE_TYPE_GAS = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_GAS", 5, 5);
        PLACE_TYPE_AIRPORT = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_AIRPORT", 6, 6);
        PLACE_TYPE_PARKING = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_PARKING", 7, 7);
        PLACE_TYPE_TRANSIT = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_TRANSIT", 8, 8);
        PLACE_TYPE_BAR = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_BAR", 9, 9);
        PLACE_TYPE_ENTERTAINMENT = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_ENTERTAINMENT", 10, 10);
        PLACE_TYPE_HOSPITAL = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_HOSPITAL", 11, 11);
        PLACE_TYPE_COFFEE = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_COFFEE", 12, 12);
        PLACE_TYPE_RESTAURANT = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_RESTAURANT", 13, 13);
        PLACE_TYPE_GYM = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_GYM", 14, 14);
        PLACE_TYPE_PARK = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_PARK", 15, 15);
        PLACE_TYPE_CONTACT = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_CONTACT", 16, 1001);
        PLACE_TYPE_CALENDAR_EVENT = new com.navdy.service.library.events.places.PlaceType("PLACE_TYPE_CALENDAR_EVENT", 17, 1002);
        com.navdy.service.library.events.places.PlaceType[] a = new com.navdy.service.library.events.places.PlaceType[18];
        a[0] = PLACE_TYPE_UNKNOWN;
        a[1] = PLACE_TYPE_ATM;
        a[2] = PLACE_TYPE_BANK;
        a[3] = PLACE_TYPE_SCHOOL;
        a[4] = PLACE_TYPE_STORE;
        a[5] = PLACE_TYPE_GAS;
        a[6] = PLACE_TYPE_AIRPORT;
        a[7] = PLACE_TYPE_PARKING;
        a[8] = PLACE_TYPE_TRANSIT;
        a[9] = PLACE_TYPE_BAR;
        a[10] = PLACE_TYPE_ENTERTAINMENT;
        a[11] = PLACE_TYPE_HOSPITAL;
        a[12] = PLACE_TYPE_COFFEE;
        a[13] = PLACE_TYPE_RESTAURANT;
        a[14] = PLACE_TYPE_GYM;
        a[15] = PLACE_TYPE_PARK;
        a[16] = PLACE_TYPE_CONTACT;
        a[17] = PLACE_TYPE_CALENDAR_EVENT;
        $VALUES = a;
    }
    
    private PlaceType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.places.PlaceType valueOf(String s) {
        return (com.navdy.service.library.events.places.PlaceType)Enum.valueOf(com.navdy.service.library.events.places.PlaceType.class, s);
    }
    
    public static com.navdy.service.library.events.places.PlaceType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
