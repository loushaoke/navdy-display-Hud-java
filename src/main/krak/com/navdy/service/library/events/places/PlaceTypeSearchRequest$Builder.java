package com.navdy.service.library.events.places;

final public class PlaceTypeSearchRequest$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.places.PlaceType place_type;
    public String request_id;
    
    public PlaceTypeSearchRequest$Builder() {
    }
    
    public PlaceTypeSearchRequest$Builder(com.navdy.service.library.events.places.PlaceTypeSearchRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.request_id = a.request_id;
            this.place_type = a.place_type;
        }
    }
    
    public com.navdy.service.library.events.places.PlaceTypeSearchRequest build() {
        return new com.navdy.service.library.events.places.PlaceTypeSearchRequest(this, (com.navdy.service.library.events.places.PlaceTypeSearchRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.PlaceTypeSearchRequest$Builder place_type(com.navdy.service.library.events.places.PlaceType a) {
        this.place_type = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlaceTypeSearchRequest$Builder request_id(String s) {
        this.request_id = s;
        return this;
    }
}
