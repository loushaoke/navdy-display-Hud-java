package com.navdy.service.library.events.places;

final public class AutoCompleteRequest$Builder extends com.squareup.wire.Message.Builder {
    public Integer maxResults;
    public String partialSearch;
    public Integer searchArea;
    
    public AutoCompleteRequest$Builder() {
    }
    
    public AutoCompleteRequest$Builder(com.navdy.service.library.events.places.AutoCompleteRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.partialSearch = a.partialSearch;
            this.searchArea = a.searchArea;
            this.maxResults = a.maxResults;
        }
    }
    
    public com.navdy.service.library.events.places.AutoCompleteRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.places.AutoCompleteRequest(this, (com.navdy.service.library.events.places.AutoCompleteRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.AutoCompleteRequest$Builder maxResults(Integer a) {
        this.maxResults = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.AutoCompleteRequest$Builder partialSearch(String s) {
        this.partialSearch = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.AutoCompleteRequest$Builder searchArea(Integer a) {
        this.searchArea = a;
        return this;
    }
}
