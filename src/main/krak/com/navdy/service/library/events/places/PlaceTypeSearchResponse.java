package com.navdy.service.library.events.places;

final public class PlaceTypeSearchResponse extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_DESTINATIONS;
    final public static String DEFAULT_REQUEST_ID = "";
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_REQUEST_STATUS;
    final private static long serialVersionUID = 0L;
    final public java.util.List destinations;
    final public String request_id;
    final public com.navdy.service.library.events.RequestStatus request_status;
    
    static {
        DEFAULT_REQUEST_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_DESTINATIONS = java.util.Collections.emptyList();
    }
    
    private PlaceTypeSearchResponse(com.navdy.service.library.events.places.PlaceTypeSearchResponse$Builder a) {
        this(a.request_id, a.request_status, a.destinations);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PlaceTypeSearchResponse(com.navdy.service.library.events.places.PlaceTypeSearchResponse$Builder a, com.navdy.service.library.events.places.PlaceTypeSearchResponse$1 a0) {
        this(a);
    }
    
    public PlaceTypeSearchResponse(String s, com.navdy.service.library.events.RequestStatus a, java.util.List a0) {
        this.request_id = s;
        this.request_status = a;
        this.destinations = com.navdy.service.library.events.places.PlaceTypeSearchResponse.immutableCopyOf(a0);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.places.PlaceTypeSearchResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.PlaceTypeSearchResponse) {
                com.navdy.service.library.events.places.PlaceTypeSearchResponse a0 = (com.navdy.service.library.events.places.PlaceTypeSearchResponse)a;
                boolean b0 = this.equals(this.request_id, a0.request_id);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.request_status, a0.request_status)) {
                        break label1;
                    }
                    if (this.equals(this.destinations, a0.destinations)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.request_id == null) ? 0 : this.request_id.hashCode()) * 37 + ((this.request_status == null) ? 0 : this.request_status.hashCode())) * 37 + ((this.destinations == null) ? 1 : this.destinations.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
