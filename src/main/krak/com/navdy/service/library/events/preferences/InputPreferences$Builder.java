package com.navdy.service.library.events.preferences;

final public class InputPreferences$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity dial_sensitivity;
    public Long serial_number;
    public Boolean use_gestures;
    
    public InputPreferences$Builder() {
    }
    
    public InputPreferences$Builder(com.navdy.service.library.events.preferences.InputPreferences a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
            this.use_gestures = a.use_gestures;
            this.dial_sensitivity = a.dial_sensitivity;
        }
    }
    
    public com.navdy.service.library.events.preferences.InputPreferences build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.preferences.InputPreferences(this, (com.navdy.service.library.events.preferences.InputPreferences$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.InputPreferences$Builder dial_sensitivity(com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity a) {
        this.dial_sensitivity = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.InputPreferences$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.InputPreferences$Builder use_gestures(Boolean a) {
        this.use_gestures = a;
        return this;
    }
}
