package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferences$ObdScanSetting extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting[] $VALUES;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting SCAN_DEFAULT_OFF;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting SCAN_DEFAULT_ON;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting SCAN_OFF;
    final public static com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting SCAN_ON;
    final private int value;
    
    static {
        SCAN_DEFAULT_ON = new com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting("SCAN_DEFAULT_ON", 0, 1);
        SCAN_ON = new com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting("SCAN_ON", 1, 2);
        SCAN_OFF = new com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting("SCAN_OFF", 2, 3);
        SCAN_DEFAULT_OFF = new com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting("SCAN_DEFAULT_OFF", 3, 4);
        com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting[] a = new com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting[4];
        a[0] = SCAN_DEFAULT_ON;
        a[1] = SCAN_ON;
        a[2] = SCAN_OFF;
        a[3] = SCAN_DEFAULT_OFF;
        $VALUES = a;
    }
    
    private DriverProfilePreferences$ObdScanSetting(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting valueOf(String s) {
        return (com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting)Enum.valueOf(com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
