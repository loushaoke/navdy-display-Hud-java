package com.navdy.service.library.events.navigation;

final public class RouteManeuverResponse extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_MANEUVERS;
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUSDETAIL = "";
    final private static long serialVersionUID = 0L;
    final public java.util.List maneuvers;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String statusDetail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_MANEUVERS = java.util.Collections.emptyList();
    }
    
    public RouteManeuverResponse(com.navdy.service.library.events.RequestStatus a, String s, java.util.List a0) {
        this.status = a;
        this.statusDetail = s;
        this.maneuvers = com.navdy.service.library.events.navigation.RouteManeuverResponse.immutableCopyOf(a0);
    }
    
    private RouteManeuverResponse(com.navdy.service.library.events.navigation.RouteManeuverResponse$Builder a) {
        this(a.status, a.statusDetail, a.maneuvers);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    RouteManeuverResponse(com.navdy.service.library.events.navigation.RouteManeuverResponse$Builder a, com.navdy.service.library.events.navigation.RouteManeuverResponse$1 a0) {
        this(a);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.navigation.RouteManeuverResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.RouteManeuverResponse) {
                com.navdy.service.library.events.navigation.RouteManeuverResponse a0 = (com.navdy.service.library.events.navigation.RouteManeuverResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.statusDetail, a0.statusDetail)) {
                        break label1;
                    }
                    if (this.equals(this.maneuvers, a0.maneuvers)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.statusDetail == null) ? 0 : this.statusDetail.hashCode())) * 37 + ((this.maneuvers == null) ? 1 : this.maneuvers.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
