package com.navdy.service.library.events.settings;

final public class ReadSettingsResponse extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_SETTINGS;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.settings.ScreenConfiguration screenConfiguration;
    final public java.util.List settings;
    
    static {
        DEFAULT_SETTINGS = java.util.Collections.emptyList();
    }
    
    private ReadSettingsResponse(com.navdy.service.library.events.settings.ReadSettingsResponse$Builder a) {
        this(a.screenConfiguration, a.settings);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ReadSettingsResponse(com.navdy.service.library.events.settings.ReadSettingsResponse$Builder a, com.navdy.service.library.events.settings.ReadSettingsResponse$1 a0) {
        this(a);
    }
    
    public ReadSettingsResponse(com.navdy.service.library.events.settings.ScreenConfiguration a, java.util.List a0) {
        this.screenConfiguration = a;
        this.settings = com.navdy.service.library.events.settings.ReadSettingsResponse.immutableCopyOf(a0);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.settings.ReadSettingsResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.settings.ReadSettingsResponse) {
                com.navdy.service.library.events.settings.ReadSettingsResponse a0 = (com.navdy.service.library.events.settings.ReadSettingsResponse)a;
                boolean b0 = this.equals(this.screenConfiguration, a0.screenConfiguration);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.settings, a0.settings)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.screenConfiguration == null) ? 0 : this.screenConfiguration.hashCode()) * 37 + ((this.settings == null) ? 1 : this.settings.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
