package com.navdy.service.library.events.obd;

final public class ObdStatusRequest$Builder extends com.squareup.wire.Message.Builder {
    public ObdStatusRequest$Builder() {
    }
    
    public ObdStatusRequest$Builder(com.navdy.service.library.events.obd.ObdStatusRequest a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.obd.ObdStatusRequest build() {
        return new com.navdy.service.library.events.obd.ObdStatusRequest(this, (com.navdy.service.library.events.obd.ObdStatusRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
