package com.navdy.service.library.events.audio;

final public class MusicTrackInfo extends com.squareup.wire.Message {
    final public static String DEFAULT_ALBUM = "";
    final public static String DEFAULT_AUTHOR = "";
    final public static String DEFAULT_COLLECTIONID = "";
    final public static com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    final public static com.navdy.service.library.events.audio.MusicCollectionType DEFAULT_COLLECTIONTYPE;
    final public static Integer DEFAULT_CURRENTPOSITION;
    final public static com.navdy.service.library.events.audio.MusicDataSource DEFAULT_DATASOURCE;
    final public static Integer DEFAULT_DURATION;
    final public static Long DEFAULT_INDEX;
    final public static Boolean DEFAULT_ISNEXTALLOWED;
    final public static Boolean DEFAULT_ISONLINESTREAM;
    final public static Boolean DEFAULT_ISPREVIOUSALLOWED;
    final public static String DEFAULT_NAME = "";
    final public static com.navdy.service.library.events.audio.MusicPlaybackState DEFAULT_PLAYBACKSTATE;
    final public static Integer DEFAULT_PLAYCOUNT;
    final public static com.navdy.service.library.events.audio.MusicRepeatMode DEFAULT_REPEATMODE;
    final public static com.navdy.service.library.events.audio.MusicShuffleMode DEFAULT_SHUFFLEMODE;
    final public static String DEFAULT_TRACKID = "";
    final private static long serialVersionUID = 0L;
    final public String album;
    final public String author;
    final public String collectionId;
    final public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    final public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    final public Integer currentPosition;
    final public com.navdy.service.library.events.audio.MusicDataSource dataSource;
    final public Integer duration;
    final public Long index;
    final public Boolean isNextAllowed;
    final public Boolean isOnlineStream;
    final public Boolean isPreviousAllowed;
    final public String name;
    final public Integer playCount;
    final public com.navdy.service.library.events.audio.MusicPlaybackState playbackState;
    final public com.navdy.service.library.events.audio.MusicRepeatMode repeatMode;
    final public com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode;
    final public String trackId;
    
    static {
        DEFAULT_PLAYBACKSTATE = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE;
        DEFAULT_INDEX = Long.valueOf(0L);
        DEFAULT_DURATION = Integer.valueOf(0);
        DEFAULT_CURRENTPOSITION = Integer.valueOf(0);
        DEFAULT_ISPREVIOUSALLOWED = Boolean.valueOf(false);
        DEFAULT_ISNEXTALLOWED = Boolean.valueOf(false);
        DEFAULT_DATASOURCE = com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE;
        DEFAULT_ISONLINESTREAM = Boolean.valueOf(false);
        DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPE = com.navdy.service.library.events.audio.MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
        DEFAULT_PLAYCOUNT = Integer.valueOf(0);
        DEFAULT_SHUFFLEMODE = com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN;
        DEFAULT_REPEATMODE = com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_UNKNOWN;
    }
    
    public MusicTrackInfo(com.navdy.service.library.events.audio.MusicPlaybackState a, Long a0, String s, String s0, String s1, Integer a1, Integer a2, Boolean a3, Boolean a4, com.navdy.service.library.events.audio.MusicDataSource a5, Boolean a6, com.navdy.service.library.events.audio.MusicCollectionSource a7, com.navdy.service.library.events.audio.MusicCollectionType a8, String s2, String s3, Integer a9, com.navdy.service.library.events.audio.MusicShuffleMode a10, com.navdy.service.library.events.audio.MusicRepeatMode a11) {
        this.playbackState = a;
        this.index = a0;
        this.name = s;
        this.author = s0;
        this.album = s1;
        this.duration = a1;
        this.currentPosition = a2;
        this.isPreviousAllowed = a3;
        this.isNextAllowed = a4;
        this.dataSource = a5;
        this.isOnlineStream = a6;
        this.collectionSource = a7;
        this.collectionType = a8;
        this.collectionId = s2;
        this.trackId = s3;
        this.playCount = a9;
        this.shuffleMode = a10;
        this.repeatMode = a11;
    }
    
    private MusicTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo$Builder a) {
        this(a.playbackState, a.index, a.name, a.author, a.album, a.duration, a.currentPosition, a.isPreviousAllowed, a.isNextAllowed, a.dataSource, a.isOnlineStream, a.collectionSource, a.collectionType, a.collectionId, a.trackId, a.playCount, a.shuffleMode, a.repeatMode);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo$Builder a, com.navdy.service.library.events.audio.MusicTrackInfo$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicTrackInfo) {
                com.navdy.service.library.events.audio.MusicTrackInfo a0 = (com.navdy.service.library.events.audio.MusicTrackInfo)a;
                boolean b0 = this.equals(this.playbackState, a0.playbackState);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.index, a0.index)) {
                        break label1;
                    }
                    if (!this.equals(this.name, a0.name)) {
                        break label1;
                    }
                    if (!this.equals(this.author, a0.author)) {
                        break label1;
                    }
                    if (!this.equals(this.album, a0.album)) {
                        break label1;
                    }
                    if (!this.equals(this.duration, a0.duration)) {
                        break label1;
                    }
                    if (!this.equals(this.currentPosition, a0.currentPosition)) {
                        break label1;
                    }
                    if (!this.equals(this.isPreviousAllowed, a0.isPreviousAllowed)) {
                        break label1;
                    }
                    if (!this.equals(this.isNextAllowed, a0.isNextAllowed)) {
                        break label1;
                    }
                    if (!this.equals(this.dataSource, a0.dataSource)) {
                        break label1;
                    }
                    if (!this.equals(this.isOnlineStream, a0.isOnlineStream)) {
                        break label1;
                    }
                    if (!this.equals(this.collectionSource, a0.collectionSource)) {
                        break label1;
                    }
                    if (!this.equals(this.collectionType, a0.collectionType)) {
                        break label1;
                    }
                    if (!this.equals(this.collectionId, a0.collectionId)) {
                        break label1;
                    }
                    if (!this.equals(this.trackId, a0.trackId)) {
                        break label1;
                    }
                    if (!this.equals(this.playCount, a0.playCount)) {
                        break label1;
                    }
                    if (!this.equals(this.shuffleMode, a0.shuffleMode)) {
                        break label1;
                    }
                    if (this.equals(this.repeatMode, a0.repeatMode)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            int i0 = (this.playbackState == null) ? 0 : this.playbackState.hashCode();
            int i1 = (this.index == null) ? 0 : this.index.hashCode();
            int i2 = (this.name == null) ? 0 : this.name.hashCode();
            int i3 = (this.author == null) ? 0 : this.author.hashCode();
            int i4 = (this.album == null) ? 0 : this.album.hashCode();
            int i5 = (this.duration == null) ? 0 : this.duration.hashCode();
            int i6 = (this.currentPosition == null) ? 0 : this.currentPosition.hashCode();
            int i7 = (this.isPreviousAllowed == null) ? 0 : this.isPreviousAllowed.hashCode();
            int i8 = (this.isNextAllowed == null) ? 0 : this.isNextAllowed.hashCode();
            int i9 = (this.dataSource == null) ? 0 : this.dataSource.hashCode();
            int i10 = (this.isOnlineStream == null) ? 0 : this.isOnlineStream.hashCode();
            int i11 = (this.collectionSource == null) ? 0 : this.collectionSource.hashCode();
            int i12 = (this.collectionType == null) ? 0 : this.collectionType.hashCode();
            int i13 = (this.collectionId == null) ? 0 : this.collectionId.hashCode();
            int i14 = (this.trackId == null) ? 0 : this.trackId.hashCode();
            int i15 = (this.playCount == null) ? 0 : this.playCount.hashCode();
            int i16 = (this.shuffleMode == null) ? 0 : this.shuffleMode.hashCode();
            int i17 = (this.repeatMode == null) ? 0 : this.repeatMode.hashCode();
            int i18 = (i0 * 37 + i1) * 37 + i2;
            i = ((((((((((((((i18 * 37 + i3) * 37 + i4) * 37 + i5) * 37 + i6) * 37 + i7) * 37 + i8) * 37 + i9) * 37 + i10) * 37 + i11) * 37 + i12) * 37 + i13) * 37 + i14) * 37 + i15) * 37 + i16) * 37 + i17;
            this.hashCode = i;
        }
        return i;
    }
}
