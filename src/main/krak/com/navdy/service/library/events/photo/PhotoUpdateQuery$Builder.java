package com.navdy.service.library.events.photo;

final public class PhotoUpdateQuery$Builder extends com.squareup.wire.Message.Builder {
    public String album;
    public String author;
    public String checksum;
    public String identifier;
    public com.navdy.service.library.events.photo.PhotoType photoType;
    public Long size;
    public String track;
    
    public PhotoUpdateQuery$Builder() {
    }
    
    public PhotoUpdateQuery$Builder(com.navdy.service.library.events.photo.PhotoUpdateQuery a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.identifier = a.identifier;
            this.photoType = a.photoType;
            this.size = a.size;
            this.checksum = a.checksum;
            this.album = a.album;
            this.track = a.track;
            this.author = a.author;
        }
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQuery$Builder album(String s) {
        this.album = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQuery$Builder author(String s) {
        this.author = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQuery build() {
        return new com.navdy.service.library.events.photo.PhotoUpdateQuery(this, (com.navdy.service.library.events.photo.PhotoUpdateQuery$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQuery$Builder checksum(String s) {
        this.checksum = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQuery$Builder identifier(String s) {
        this.identifier = s;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQuery$Builder photoType(com.navdy.service.library.events.photo.PhotoType a) {
        this.photoType = a;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQuery$Builder size(Long a) {
        this.size = a;
        return this;
    }
    
    public com.navdy.service.library.events.photo.PhotoUpdateQuery$Builder track(String s) {
        this.track = s;
        return this;
    }
}
