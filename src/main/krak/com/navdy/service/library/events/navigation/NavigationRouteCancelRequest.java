package com.navdy.service.library.events.navigation;

final public class NavigationRouteCancelRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_HANDLE = "";
    final private static long serialVersionUID = 0L;
    final public String handle;
    
    private NavigationRouteCancelRequest(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest$Builder a) {
        this(a.handle);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationRouteCancelRequest(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest$Builder a, com.navdy.service.library.events.navigation.NavigationRouteCancelRequest$1 a0) {
        this(a);
    }
    
    public NavigationRouteCancelRequest(String s) {
        this.handle = s;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.navigation.NavigationRouteCancelRequest && this.equals(this.handle, ((com.navdy.service.library.events.navigation.NavigationRouteCancelRequest)a).handle);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.handle == null) ? 0 : this.handle.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
