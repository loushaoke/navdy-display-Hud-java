package com.navdy.service.library.events.preferences;

final public class NavigationPreferences$Builder extends com.squareup.wire.Message.Builder {
    public Boolean allowAutoTrains;
    public Boolean allowFerries;
    public Boolean allowHOVLanes;
    public Boolean allowHighways;
    public Boolean allowTollRoads;
    public Boolean allowTunnels;
    public Boolean allowUnpavedRoads;
    public Boolean phoneticTurnByTurn;
    public com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic rerouteForTraffic;
    public com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType routingType;
    public Long serial_number;
    public Boolean showTrafficInOpenMap;
    public Boolean showTrafficWhileNavigating;
    public Boolean spokenCameraWarnings;
    public Boolean spokenSpeedLimitWarnings;
    public Boolean spokenTurnByTurn;
    
    public NavigationPreferences$Builder() {
    }
    
    public NavigationPreferences$Builder(com.navdy.service.library.events.preferences.NavigationPreferences a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.serial_number = a.serial_number;
            this.rerouteForTraffic = a.rerouteForTraffic;
            this.routingType = a.routingType;
            this.spokenTurnByTurn = a.spokenTurnByTurn;
            this.spokenSpeedLimitWarnings = a.spokenSpeedLimitWarnings;
            this.showTrafficInOpenMap = a.showTrafficInOpenMap;
            this.showTrafficWhileNavigating = a.showTrafficWhileNavigating;
            this.allowHighways = a.allowHighways;
            this.allowTollRoads = a.allowTollRoads;
            this.allowFerries = a.allowFerries;
            this.allowTunnels = a.allowTunnels;
            this.allowUnpavedRoads = a.allowUnpavedRoads;
            this.allowAutoTrains = a.allowAutoTrains;
            this.allowHOVLanes = a.allowHOVLanes;
            this.spokenCameraWarnings = a.spokenCameraWarnings;
            this.phoneticTurnByTurn = a.phoneticTurnByTurn;
        }
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder allowAutoTrains(Boolean a) {
        this.allowAutoTrains = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder allowFerries(Boolean a) {
        this.allowFerries = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder allowHOVLanes(Boolean a) {
        this.allowHOVLanes = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder allowHighways(Boolean a) {
        this.allowHighways = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder allowTollRoads(Boolean a) {
        this.allowTollRoads = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder allowTunnels(Boolean a) {
        this.allowTunnels = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder allowUnpavedRoads(Boolean a) {
        this.allowUnpavedRoads = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.preferences.NavigationPreferences(this, (com.navdy.service.library.events.preferences.NavigationPreferences$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder phoneticTurnByTurn(Boolean a) {
        this.phoneticTurnByTurn = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder rerouteForTraffic(com.navdy.service.library.events.preferences.NavigationPreferences$RerouteForTraffic a) {
        this.rerouteForTraffic = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder routingType(com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType a) {
        this.routingType = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder showTrafficInOpenMap(Boolean a) {
        this.showTrafficInOpenMap = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder showTrafficWhileNavigating(Boolean a) {
        this.showTrafficWhileNavigating = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder spokenCameraWarnings(Boolean a) {
        this.spokenCameraWarnings = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder spokenSpeedLimitWarnings(Boolean a) {
        this.spokenSpeedLimitWarnings = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences$Builder spokenTurnByTurn(Boolean a) {
        this.spokenTurnByTurn = a;
        return this;
    }
}
