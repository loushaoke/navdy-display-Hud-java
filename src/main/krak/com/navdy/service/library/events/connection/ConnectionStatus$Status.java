package com.navdy.service.library.events.connection;

final public class ConnectionStatus$Status extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.connection.ConnectionStatus$Status[] $VALUES;
    final public static com.navdy.service.library.events.connection.ConnectionStatus$Status CONNECTION_FOUND;
    final public static com.navdy.service.library.events.connection.ConnectionStatus$Status CONNECTION_LOST;
    final public static com.navdy.service.library.events.connection.ConnectionStatus$Status CONNECTION_PAIRED_DEVICES_CHANGED;
    final public static com.navdy.service.library.events.connection.ConnectionStatus$Status CONNECTION_PAIRING;
    final public static com.navdy.service.library.events.connection.ConnectionStatus$Status CONNECTION_SEARCH_FINISHED;
    final public static com.navdy.service.library.events.connection.ConnectionStatus$Status CONNECTION_SEARCH_STARTED;
    final private int value;
    
    static {
        CONNECTION_PAIRING = new com.navdy.service.library.events.connection.ConnectionStatus$Status("CONNECTION_PAIRING", 0, 1);
        CONNECTION_SEARCH_STARTED = new com.navdy.service.library.events.connection.ConnectionStatus$Status("CONNECTION_SEARCH_STARTED", 1, 2);
        CONNECTION_SEARCH_FINISHED = new com.navdy.service.library.events.connection.ConnectionStatus$Status("CONNECTION_SEARCH_FINISHED", 2, 3);
        CONNECTION_FOUND = new com.navdy.service.library.events.connection.ConnectionStatus$Status("CONNECTION_FOUND", 3, 4);
        CONNECTION_LOST = new com.navdy.service.library.events.connection.ConnectionStatus$Status("CONNECTION_LOST", 4, 5);
        CONNECTION_PAIRED_DEVICES_CHANGED = new com.navdy.service.library.events.connection.ConnectionStatus$Status("CONNECTION_PAIRED_DEVICES_CHANGED", 5, 6);
        com.navdy.service.library.events.connection.ConnectionStatus$Status[] a = new com.navdy.service.library.events.connection.ConnectionStatus$Status[6];
        a[0] = CONNECTION_PAIRING;
        a[1] = CONNECTION_SEARCH_STARTED;
        a[2] = CONNECTION_SEARCH_FINISHED;
        a[3] = CONNECTION_FOUND;
        a[4] = CONNECTION_LOST;
        a[5] = CONNECTION_PAIRED_DEVICES_CHANGED;
        $VALUES = a;
    }
    
    private ConnectionStatus$Status(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.connection.ConnectionStatus$Status valueOf(String s) {
        return (com.navdy.service.library.events.connection.ConnectionStatus$Status)Enum.valueOf(com.navdy.service.library.events.connection.ConnectionStatus$Status.class, s);
    }
    
    public static com.navdy.service.library.events.connection.ConnectionStatus$Status[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
