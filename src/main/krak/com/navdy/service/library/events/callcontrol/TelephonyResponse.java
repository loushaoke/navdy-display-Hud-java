package com.navdy.service.library.events.callcontrol;

final public class TelephonyResponse extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.callcontrol.CallAction DEFAULT_ACTION;
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUSDETAIL = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.callcontrol.CallAction action;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String statusDetail;
    
    static {
        DEFAULT_ACTION = com.navdy.service.library.events.callcontrol.CallAction.CALL_ACCEPT;
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    }
    
    public TelephonyResponse(com.navdy.service.library.events.callcontrol.CallAction a, com.navdy.service.library.events.RequestStatus a0, String s) {
        this.action = a;
        this.status = a0;
        this.statusDetail = s;
    }
    
    private TelephonyResponse(com.navdy.service.library.events.callcontrol.TelephonyResponse$Builder a) {
        this(a.action, a.status, a.statusDetail);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    TelephonyResponse(com.navdy.service.library.events.callcontrol.TelephonyResponse$Builder a, com.navdy.service.library.events.callcontrol.TelephonyResponse$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.callcontrol.TelephonyResponse) {
                com.navdy.service.library.events.callcontrol.TelephonyResponse a0 = (com.navdy.service.library.events.callcontrol.TelephonyResponse)a;
                boolean b0 = this.equals(this.action, a0.action);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.status, a0.status)) {
                        break label1;
                    }
                    if (this.equals(this.statusDetail, a0.statusDetail)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.action == null) ? 0 : this.action.hashCode()) * 37 + ((this.status == null) ? 0 : this.status.hashCode())) * 37 + ((this.statusDetail == null) ? 0 : this.statusDetail.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
