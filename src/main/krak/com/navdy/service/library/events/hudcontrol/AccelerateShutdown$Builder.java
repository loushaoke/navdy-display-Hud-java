package com.navdy.service.library.events.hudcontrol;

final public class AccelerateShutdown$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason reason;
    
    public AccelerateShutdown$Builder() {
    }
    
    public AccelerateShutdown$Builder(com.navdy.service.library.events.hudcontrol.AccelerateShutdown a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.reason = a.reason;
        }
    }
    
    public com.navdy.service.library.events.hudcontrol.AccelerateShutdown build() {
        return new com.navdy.service.library.events.hudcontrol.AccelerateShutdown(this, (com.navdy.service.library.events.hudcontrol.AccelerateShutdown$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.hudcontrol.AccelerateShutdown$Builder reason(com.navdy.service.library.events.hudcontrol.AccelerateShutdown$AccelerateReason a) {
        this.reason = a;
        return this;
    }
}
