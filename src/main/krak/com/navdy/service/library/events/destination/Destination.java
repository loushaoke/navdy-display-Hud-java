package com.navdy.service.library.events.destination;

final public class Destination extends com.squareup.wire.Message {
    final public static java.util.List DEFAULT_CONTACTS;
    final public static String DEFAULT_DESTINATIONDISTANCE = "";
    final public static Integer DEFAULT_DESTINATIONICON;
    final public static Integer DEFAULT_DESTINATIONICONBKCOLOR;
    final public static String DEFAULT_DESTINATION_SUBTITLE = "";
    final public static String DEFAULT_DESTINATION_TITLE = "";
    final public static com.navdy.service.library.events.destination.Destination$FavoriteType DEFAULT_FAVORITE_TYPE;
    final public static String DEFAULT_FULL_ADDRESS = "";
    final public static String DEFAULT_IDENTIFIER = "";
    final public static Boolean DEFAULT_IS_RECOMMENDATION;
    final public static Long DEFAULT_LAST_NAVIGATED_TO;
    final public static java.util.List DEFAULT_PHONENUMBERS;
    final public static String DEFAULT_PLACE_ID = "";
    final public static com.navdy.service.library.events.places.PlaceType DEFAULT_PLACE_TYPE;
    final public static com.navdy.service.library.events.destination.Destination$SuggestionType DEFAULT_SUGGESTION_TYPE;
    final private static long serialVersionUID = 0L;
    final public java.util.List contacts;
    final public String destinationDistance;
    final public Integer destinationIcon;
    final public Integer destinationIconBkColor;
    final public String destination_subtitle;
    final public String destination_title;
    final public com.navdy.service.library.events.location.LatLong display_position;
    final public com.navdy.service.library.events.destination.Destination$FavoriteType favorite_type;
    final public String full_address;
    final public String identifier;
    final public Boolean is_recommendation;
    final public Long last_navigated_to;
    final public com.navdy.service.library.events.location.LatLong navigation_position;
    final public java.util.List phoneNumbers;
    final public String place_id;
    final public com.navdy.service.library.events.places.PlaceType place_type;
    final public com.navdy.service.library.events.destination.Destination$SuggestionType suggestion_type;
    
    static {
        DEFAULT_FAVORITE_TYPE = com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_NONE;
        DEFAULT_SUGGESTION_TYPE = com.navdy.service.library.events.destination.Destination$SuggestionType.SUGGESTION_NONE;
        DEFAULT_IS_RECOMMENDATION = Boolean.valueOf(false);
        DEFAULT_LAST_NAVIGATED_TO = Long.valueOf(0L);
        DEFAULT_PLACE_TYPE = com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN;
        DEFAULT_DESTINATIONICON = Integer.valueOf(0);
        DEFAULT_DESTINATIONICONBKCOLOR = Integer.valueOf(0);
        DEFAULT_PHONENUMBERS = java.util.Collections.emptyList();
        DEFAULT_CONTACTS = java.util.Collections.emptyList();
    }
    
    private Destination(com.navdy.service.library.events.destination.Destination$Builder a) {
        this(a.navigation_position, a.display_position, a.full_address, a.destination_title, a.destination_subtitle, a.favorite_type, a.identifier, a.suggestion_type, a.is_recommendation, a.last_navigated_to, a.place_type, a.destinationIcon, a.destinationIconBkColor, a.place_id, a.destinationDistance, a.phoneNumbers, a.contacts);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    Destination(com.navdy.service.library.events.destination.Destination$Builder a, com.navdy.service.library.events.destination.Destination$1 a0) {
        this(a);
    }
    
    public Destination(com.navdy.service.library.events.location.LatLong a, com.navdy.service.library.events.location.LatLong a0, String s, String s0, String s1, com.navdy.service.library.events.destination.Destination$FavoriteType a1, String s2, com.navdy.service.library.events.destination.Destination$SuggestionType a2, Boolean a3, Long a4, com.navdy.service.library.events.places.PlaceType a5, Integer a6, Integer a7, String s3, String s4, java.util.List a8, java.util.List a9) {
        this.navigation_position = a;
        this.display_position = a0;
        this.full_address = s;
        this.destination_title = s0;
        this.destination_subtitle = s1;
        this.favorite_type = a1;
        this.identifier = s2;
        this.suggestion_type = a2;
        this.is_recommendation = a3;
        this.last_navigated_to = a4;
        this.place_type = a5;
        this.destinationIcon = a6;
        this.destinationIconBkColor = a7;
        this.place_id = s3;
        this.destinationDistance = s4;
        this.phoneNumbers = com.navdy.service.library.events.destination.Destination.immutableCopyOf(a8);
        this.contacts = com.navdy.service.library.events.destination.Destination.immutableCopyOf(a9);
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.destination.Destination.copyOf(a);
    }
    
    static java.util.List access$100(java.util.List a) {
        return com.navdy.service.library.events.destination.Destination.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.destination.Destination) {
                com.navdy.service.library.events.destination.Destination a0 = (com.navdy.service.library.events.destination.Destination)a;
                boolean b0 = this.equals(this.navigation_position, a0.navigation_position);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.display_position, a0.display_position)) {
                        break label1;
                    }
                    if (!this.equals(this.full_address, a0.full_address)) {
                        break label1;
                    }
                    if (!this.equals(this.destination_title, a0.destination_title)) {
                        break label1;
                    }
                    if (!this.equals(this.destination_subtitle, a0.destination_subtitle)) {
                        break label1;
                    }
                    if (!this.equals(this.favorite_type, a0.favorite_type)) {
                        break label1;
                    }
                    if (!this.equals(this.identifier, a0.identifier)) {
                        break label1;
                    }
                    if (!this.equals(this.suggestion_type, a0.suggestion_type)) {
                        break label1;
                    }
                    if (!this.equals(this.is_recommendation, a0.is_recommendation)) {
                        break label1;
                    }
                    if (!this.equals(this.last_navigated_to, a0.last_navigated_to)) {
                        break label1;
                    }
                    if (!this.equals(this.place_type, a0.place_type)) {
                        break label1;
                    }
                    if (!this.equals(this.destinationIcon, a0.destinationIcon)) {
                        break label1;
                    }
                    if (!this.equals(this.destinationIconBkColor, a0.destinationIconBkColor)) {
                        break label1;
                    }
                    if (!this.equals(this.place_id, a0.place_id)) {
                        break label1;
                    }
                    if (!this.equals(this.destinationDistance, a0.destinationDistance)) {
                        break label1;
                    }
                    if (!this.equals(this.phoneNumbers, a0.phoneNumbers)) {
                        break label1;
                    }
                    if (this.equals(this.contacts, a0.contacts)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            int i0 = (this.navigation_position == null) ? 0 : this.navigation_position.hashCode();
            int i1 = (this.display_position == null) ? 0 : this.display_position.hashCode();
            int i2 = (this.full_address == null) ? 0 : this.full_address.hashCode();
            int i3 = (this.destination_title == null) ? 0 : this.destination_title.hashCode();
            int i4 = (this.destination_subtitle == null) ? 0 : this.destination_subtitle.hashCode();
            int i5 = (this.favorite_type == null) ? 0 : this.favorite_type.hashCode();
            int i6 = (this.identifier == null) ? 0 : this.identifier.hashCode();
            int i7 = (this.suggestion_type == null) ? 0 : this.suggestion_type.hashCode();
            int i8 = (this.is_recommendation == null) ? 0 : this.is_recommendation.hashCode();
            int i9 = (this.last_navigated_to == null) ? 0 : this.last_navigated_to.hashCode();
            int i10 = (this.place_type == null) ? 0 : this.place_type.hashCode();
            int i11 = (this.destinationIcon == null) ? 0 : this.destinationIcon.hashCode();
            int i12 = (this.destinationIconBkColor == null) ? 0 : this.destinationIconBkColor.hashCode();
            int i13 = (this.place_id == null) ? 0 : this.place_id.hashCode();
            int i14 = (this.destinationDistance == null) ? 0 : this.destinationDistance.hashCode();
            int i15 = (this.phoneNumbers == null) ? 1 : this.phoneNumbers.hashCode();
            int i16 = (this.contacts == null) ? 1 : this.contacts.hashCode();
            int i17 = i0 * 37 + i1;
            i = ((((((((((((((i17 * 37 + i2) * 37 + i3) * 37 + i4) * 37 + i5) * 37 + i6) * 37 + i7) * 37 + i8) * 37 + i9) * 37 + i10) * 37 + i11) * 37 + i12) * 37 + i13) * 37 + i14) * 37 + i15) * 37 + i16;
            this.hashCode = i;
        }
        return i;
    }
}
