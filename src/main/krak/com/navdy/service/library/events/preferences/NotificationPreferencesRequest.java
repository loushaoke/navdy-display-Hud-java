package com.navdy.service.library.events.preferences;

final public class NotificationPreferencesRequest extends com.squareup.wire.Message {
    final public static Long DEFAULT_SERIAL_NUMBER;
    final private static long serialVersionUID = 0L;
    final public Long serial_number;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
    }
    
    private NotificationPreferencesRequest(com.navdy.service.library.events.preferences.NotificationPreferencesRequest$Builder a) {
        this(a.serial_number);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NotificationPreferencesRequest(com.navdy.service.library.events.preferences.NotificationPreferencesRequest$Builder a, com.navdy.service.library.events.preferences.NotificationPreferencesRequest$1 a0) {
        this(a);
    }
    
    public NotificationPreferencesRequest(Long a) {
        this.serial_number = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.preferences.NotificationPreferencesRequest && this.equals(this.serial_number, ((com.navdy.service.library.events.preferences.NotificationPreferencesRequest)a).serial_number);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.serial_number == null) ? 0 : this.serial_number.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
