package com.navdy.service.library.events.audio;

final public class ResumeMusicRequest$Builder extends com.squareup.wire.Message.Builder {
    public ResumeMusicRequest$Builder() {
    }
    
    public ResumeMusicRequest$Builder(com.navdy.service.library.events.audio.ResumeMusicRequest a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.audio.ResumeMusicRequest build() {
        return new com.navdy.service.library.events.audio.ResumeMusicRequest(this, (com.navdy.service.library.events.audio.ResumeMusicRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
