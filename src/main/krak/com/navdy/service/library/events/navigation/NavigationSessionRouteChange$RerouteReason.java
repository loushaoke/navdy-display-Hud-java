package com.navdy.service.library.events.navigation;

final public class NavigationSessionRouteChange$RerouteReason extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason[] $VALUES;
    final public static com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason NAV_SESSION_ARRIVAL_REROUTE;
    final public static com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason NAV_SESSION_FUEL_REROUTE;
    final public static com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason NAV_SESSION_OFF_REROUTE;
    final public static com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason NAV_SESSION_ROUTE_PICKER;
    final public static com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason NAV_SESSION_TRAFFIC_REROUTE;
    final private int value;
    
    static {
        NAV_SESSION_OFF_REROUTE = new com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason("NAV_SESSION_OFF_REROUTE", 0, 1);
        NAV_SESSION_TRAFFIC_REROUTE = new com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason("NAV_SESSION_TRAFFIC_REROUTE", 1, 2);
        NAV_SESSION_ARRIVAL_REROUTE = new com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason("NAV_SESSION_ARRIVAL_REROUTE", 2, 3);
        NAV_SESSION_FUEL_REROUTE = new com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason("NAV_SESSION_FUEL_REROUTE", 3, 4);
        NAV_SESSION_ROUTE_PICKER = new com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason("NAV_SESSION_ROUTE_PICKER", 4, 5);
        com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason[] a = new com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason[5];
        a[0] = NAV_SESSION_OFF_REROUTE;
        a[1] = NAV_SESSION_TRAFFIC_REROUTE;
        a[2] = NAV_SESSION_ARRIVAL_REROUTE;
        a[3] = NAV_SESSION_FUEL_REROUTE;
        a[4] = NAV_SESSION_ROUTE_PICKER;
        $VALUES = a;
    }
    
    private NavigationSessionRouteChange$RerouteReason(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason valueOf(String s) {
        return (com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason)Enum.valueOf(com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason.class, s);
    }
    
    public static com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
