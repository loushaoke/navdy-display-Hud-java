package com.navdy.service.library.events.obd;

final public class ObdStatusResponse extends com.squareup.wire.Message {
    final public static Boolean DEFAULT_ISCONNECTED;
    final public static Integer DEFAULT_SPEED;
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_VIN = "";
    final private static long serialVersionUID = 0L;
    final public Boolean isConnected;
    final public Integer speed;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String vin;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_ISCONNECTED = Boolean.valueOf(false);
        DEFAULT_SPEED = Integer.valueOf(0);
    }
    
    public ObdStatusResponse(com.navdy.service.library.events.RequestStatus a, Boolean a0, String s, Integer a1) {
        this.status = a;
        this.isConnected = a0;
        this.vin = s;
        this.speed = a1;
    }
    
    private ObdStatusResponse(com.navdy.service.library.events.obd.ObdStatusResponse$Builder a) {
        this(a.status, a.isConnected, a.vin, a.speed);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ObdStatusResponse(com.navdy.service.library.events.obd.ObdStatusResponse$Builder a, com.navdy.service.library.events.obd.ObdStatusResponse$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.obd.ObdStatusResponse) {
                com.navdy.service.library.events.obd.ObdStatusResponse a0 = (com.navdy.service.library.events.obd.ObdStatusResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.isConnected, a0.isConnected)) {
                        break label1;
                    }
                    if (!this.equals(this.vin, a0.vin)) {
                        break label1;
                    }
                    if (this.equals(this.speed, a0.speed)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.isConnected == null) ? 0 : this.isConnected.hashCode())) * 37 + ((this.vin == null) ? 0 : this.vin.hashCode())) * 37 + ((this.speed == null) ? 0 : this.speed.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
