package com.navdy.service.library.events.input;

final public class DialSimulationEvent$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction dialAction;
    
    public DialSimulationEvent$Builder() {
    }
    
    public DialSimulationEvent$Builder(com.navdy.service.library.events.input.DialSimulationEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.dialAction = a.dialAction;
        }
    }
    
    public com.navdy.service.library.events.input.DialSimulationEvent build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.input.DialSimulationEvent(this, (com.navdy.service.library.events.input.DialSimulationEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.input.DialSimulationEvent$Builder dialAction(com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction a) {
        this.dialAction = a;
        return this;
    }
}
