package com.navdy.service.library.events.dial;

final public class DialBondRequest$DialAction extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.dial.DialBondRequest$DialAction[] $VALUES;
    final public static com.navdy.service.library.events.dial.DialBondRequest$DialAction DIAL_BOND;
    final public static com.navdy.service.library.events.dial.DialBondRequest$DialAction DIAL_CLEAR_BOND;
    final public static com.navdy.service.library.events.dial.DialBondRequest$DialAction DIAL_REBOND;
    final private int value;
    
    static {
        DIAL_BOND = new com.navdy.service.library.events.dial.DialBondRequest$DialAction("DIAL_BOND", 0, 1);
        DIAL_CLEAR_BOND = new com.navdy.service.library.events.dial.DialBondRequest$DialAction("DIAL_CLEAR_BOND", 1, 2);
        DIAL_REBOND = new com.navdy.service.library.events.dial.DialBondRequest$DialAction("DIAL_REBOND", 2, 3);
        com.navdy.service.library.events.dial.DialBondRequest$DialAction[] a = new com.navdy.service.library.events.dial.DialBondRequest$DialAction[3];
        a[0] = DIAL_BOND;
        a[1] = DIAL_CLEAR_BOND;
        a[2] = DIAL_REBOND;
        $VALUES = a;
    }
    
    private DialBondRequest$DialAction(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.dial.DialBondRequest$DialAction valueOf(String s) {
        return (com.navdy.service.library.events.dial.DialBondRequest$DialAction)Enum.valueOf(com.navdy.service.library.events.dial.DialBondRequest$DialAction.class, s);
    }
    
    public static com.navdy.service.library.events.dial.DialBondRequest$DialAction[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
