package com.navdy.service.library.events.places;

final public class PlacesSearchResponse extends com.squareup.wire.Message {
    final public static String DEFAULT_REQUESTID = "";
    final public static java.util.List DEFAULT_RESULTS;
    final public static String DEFAULT_SEARCHQUERY = "";
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUSDETAIL = "";
    final private static long serialVersionUID = 0L;
    final public String requestId;
    final public java.util.List results;
    final public String searchQuery;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String statusDetail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
        DEFAULT_RESULTS = java.util.Collections.emptyList();
    }
    
    private PlacesSearchResponse(com.navdy.service.library.events.places.PlacesSearchResponse$Builder a) {
        this(a.searchQuery, a.status, a.statusDetail, a.results, a.requestId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PlacesSearchResponse(com.navdy.service.library.events.places.PlacesSearchResponse$Builder a, com.navdy.service.library.events.places.PlacesSearchResponse$1 a0) {
        this(a);
    }
    
    public PlacesSearchResponse(String s, com.navdy.service.library.events.RequestStatus a, String s0, java.util.List a0, String s1) {
        this.searchQuery = s;
        this.status = a;
        this.statusDetail = s0;
        this.results = com.navdy.service.library.events.places.PlacesSearchResponse.immutableCopyOf(a0);
        this.requestId = s1;
    }
    
    static java.util.List access$000(java.util.List a) {
        return com.navdy.service.library.events.places.PlacesSearchResponse.copyOf(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.places.PlacesSearchResponse) {
                com.navdy.service.library.events.places.PlacesSearchResponse a0 = (com.navdy.service.library.events.places.PlacesSearchResponse)a;
                boolean b0 = this.equals(this.searchQuery, a0.searchQuery);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.status, a0.status)) {
                        break label1;
                    }
                    if (!this.equals(this.statusDetail, a0.statusDetail)) {
                        break label1;
                    }
                    if (!this.equals(this.results, a0.results)) {
                        break label1;
                    }
                    if (this.equals(this.requestId, a0.requestId)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.searchQuery == null) ? 0 : this.searchQuery.hashCode()) * 37 + ((this.status == null) ? 0 : this.status.hashCode())) * 37 + ((this.statusDetail == null) ? 0 : this.statusDetail.hashCode())) * 37 + ((this.results == null) ? 1 : this.results.hashCode())) * 37 + ((this.requestId == null) ? 0 : this.requestId.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
