package com.navdy.service.library.events.destination;

final public class RecommendedDestinationsUpdate$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List destinations;
    public Long serial_number;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public RecommendedDestinationsUpdate$Builder() {
    }
    
    public RecommendedDestinationsUpdate$Builder(com.navdy.service.library.events.destination.RecommendedDestinationsUpdate a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.serial_number = a.serial_number;
            this.destinations = com.navdy.service.library.events.destination.RecommendedDestinationsUpdate.access$000(a.destinations);
        }
    }
    
    public com.navdy.service.library.events.destination.RecommendedDestinationsUpdate build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.destination.RecommendedDestinationsUpdate(this, (com.navdy.service.library.events.destination.RecommendedDestinationsUpdate$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.destination.RecommendedDestinationsUpdate$Builder destinations(java.util.List a) {
        this.destinations = com.navdy.service.library.events.destination.RecommendedDestinationsUpdate$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.destination.RecommendedDestinationsUpdate$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.RecommendedDestinationsUpdate$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.destination.RecommendedDestinationsUpdate$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
