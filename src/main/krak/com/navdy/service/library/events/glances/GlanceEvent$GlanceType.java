package com.navdy.service.library.events.glances;

final public class GlanceEvent$GlanceType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.glances.GlanceEvent$GlanceType[] $VALUES;
    final public static com.navdy.service.library.events.glances.GlanceEvent$GlanceType GLANCE_TYPE_CALENDAR;
    final public static com.navdy.service.library.events.glances.GlanceEvent$GlanceType GLANCE_TYPE_EMAIL;
    final public static com.navdy.service.library.events.glances.GlanceEvent$GlanceType GLANCE_TYPE_FUEL;
    final public static com.navdy.service.library.events.glances.GlanceEvent$GlanceType GLANCE_TYPE_GENERIC;
    final public static com.navdy.service.library.events.glances.GlanceEvent$GlanceType GLANCE_TYPE_MESSAGE;
    final public static com.navdy.service.library.events.glances.GlanceEvent$GlanceType GLANCE_TYPE_SOCIAL;
    final private int value;
    
    static {
        GLANCE_TYPE_CALENDAR = new com.navdy.service.library.events.glances.GlanceEvent$GlanceType("GLANCE_TYPE_CALENDAR", 0, 0);
        GLANCE_TYPE_EMAIL = new com.navdy.service.library.events.glances.GlanceEvent$GlanceType("GLANCE_TYPE_EMAIL", 1, 1);
        GLANCE_TYPE_MESSAGE = new com.navdy.service.library.events.glances.GlanceEvent$GlanceType("GLANCE_TYPE_MESSAGE", 2, 2);
        GLANCE_TYPE_SOCIAL = new com.navdy.service.library.events.glances.GlanceEvent$GlanceType("GLANCE_TYPE_SOCIAL", 3, 3);
        GLANCE_TYPE_GENERIC = new com.navdy.service.library.events.glances.GlanceEvent$GlanceType("GLANCE_TYPE_GENERIC", 4, 4);
        GLANCE_TYPE_FUEL = new com.navdy.service.library.events.glances.GlanceEvent$GlanceType("GLANCE_TYPE_FUEL", 5, 5);
        com.navdy.service.library.events.glances.GlanceEvent$GlanceType[] a = new com.navdy.service.library.events.glances.GlanceEvent$GlanceType[6];
        a[0] = GLANCE_TYPE_CALENDAR;
        a[1] = GLANCE_TYPE_EMAIL;
        a[2] = GLANCE_TYPE_MESSAGE;
        a[3] = GLANCE_TYPE_SOCIAL;
        a[4] = GLANCE_TYPE_GENERIC;
        a[5] = GLANCE_TYPE_FUEL;
        $VALUES = a;
    }
    
    private GlanceEvent$GlanceType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent$GlanceType valueOf(String s) {
        return (com.navdy.service.library.events.glances.GlanceEvent$GlanceType)Enum.valueOf(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.class, s);
    }
    
    public static com.navdy.service.library.events.glances.GlanceEvent$GlanceType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
