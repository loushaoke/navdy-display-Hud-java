package com.navdy.service.library.events.file;

final public class FileTransferResponse$Builder extends com.squareup.wire.Message.Builder {
    public String checksum;
    public String destinationFileName;
    public com.navdy.service.library.events.file.FileTransferError error;
    public com.navdy.service.library.events.file.FileType fileType;
    public Integer maxChunkSize;
    public Long offset;
    public Boolean success;
    public Boolean supportsAcks;
    public Integer transferId;
    
    public FileTransferResponse$Builder() {
    }
    
    public FileTransferResponse$Builder(com.navdy.service.library.events.file.FileTransferResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.transferId = a.transferId;
            this.offset = a.offset;
            this.checksum = a.checksum;
            this.success = a.success;
            this.maxChunkSize = a.maxChunkSize;
            this.error = a.error;
            this.fileType = a.fileType;
            this.destinationFileName = a.destinationFileName;
            this.supportsAcks = a.supportsAcks;
        }
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.file.FileTransferResponse(this, (com.navdy.service.library.events.file.FileTransferResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse$Builder checksum(String s) {
        this.checksum = s;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse$Builder destinationFileName(String s) {
        this.destinationFileName = s;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse$Builder error(com.navdy.service.library.events.file.FileTransferError a) {
        this.error = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse$Builder fileType(com.navdy.service.library.events.file.FileType a) {
        this.fileType = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse$Builder maxChunkSize(Integer a) {
        this.maxChunkSize = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse$Builder offset(Long a) {
        this.offset = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse$Builder success(Boolean a) {
        this.success = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse$Builder supportsAcks(Boolean a) {
        this.supportsAcks = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse$Builder transferId(Integer a) {
        this.transferId = a;
        return this;
    }
}
