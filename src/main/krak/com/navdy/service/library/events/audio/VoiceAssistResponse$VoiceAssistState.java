package com.navdy.service.library.events.audio;

final public class VoiceAssistResponse$VoiceAssistState extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState[] $VALUES;
    final public static com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState VOICE_ASSIST_AVAILABLE;
    final public static com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState VOICE_ASSIST_LISTENING;
    final public static com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState VOICE_ASSIST_NOT_AVAILABLE;
    final public static com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState VOICE_ASSIST_STOPPED;
    final private int value;
    
    static {
        VOICE_ASSIST_AVAILABLE = new com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState("VOICE_ASSIST_AVAILABLE", 0, 0);
        VOICE_ASSIST_NOT_AVAILABLE = new com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState("VOICE_ASSIST_NOT_AVAILABLE", 1, 1);
        VOICE_ASSIST_LISTENING = new com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState("VOICE_ASSIST_LISTENING", 2, 2);
        VOICE_ASSIST_STOPPED = new com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState("VOICE_ASSIST_STOPPED", 3, 3);
        com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState[] a = new com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState[4];
        a[0] = VOICE_ASSIST_AVAILABLE;
        a[1] = VOICE_ASSIST_NOT_AVAILABLE;
        a[2] = VOICE_ASSIST_LISTENING;
        a[3] = VOICE_ASSIST_STOPPED;
        $VALUES = a;
    }
    
    private VoiceAssistResponse$VoiceAssistState(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState valueOf(String s) {
        return (com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState)Enum.valueOf(com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState.class, s);
    }
    
    public static com.navdy.service.library.events.audio.VoiceAssistResponse$VoiceAssistState[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
