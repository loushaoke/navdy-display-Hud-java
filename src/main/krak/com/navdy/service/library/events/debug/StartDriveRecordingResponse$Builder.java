package com.navdy.service.library.events.debug;

final public class StartDriveRecordingResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.RequestStatus status;
    
    public StartDriveRecordingResponse$Builder() {
    }
    
    public StartDriveRecordingResponse$Builder(com.navdy.service.library.events.debug.StartDriveRecordingResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
        }
    }
    
    public com.navdy.service.library.events.debug.StartDriveRecordingResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.debug.StartDriveRecordingResponse(this, (com.navdy.service.library.events.debug.StartDriveRecordingResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.debug.StartDriveRecordingResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
}
