package com.navdy.service.library.events.notification;

final public class ShowCustomNotification$Builder extends com.squareup.wire.Message.Builder {
    public String id;
    
    public ShowCustomNotification$Builder() {
    }
    
    public ShowCustomNotification$Builder(com.navdy.service.library.events.notification.ShowCustomNotification a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.id = a.id;
        }
    }
    
    public com.navdy.service.library.events.notification.ShowCustomNotification build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.notification.ShowCustomNotification(this, (com.navdy.service.library.events.notification.ShowCustomNotification$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.notification.ShowCustomNotification$Builder id(String s) {
        this.id = s;
        return this;
    }
}
