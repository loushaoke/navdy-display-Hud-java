package com.navdy.service.library.events.glances;

final public class ClearGlances$Builder extends com.squareup.wire.Message.Builder {
    public ClearGlances$Builder() {
    }
    
    public ClearGlances$Builder(com.navdy.service.library.events.glances.ClearGlances a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.glances.ClearGlances build() {
        return new com.navdy.service.library.events.glances.ClearGlances(this, (com.navdy.service.library.events.glances.ClearGlances$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
