package com.navdy.service.library.events.glances;

final public class GlanceEvent$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List actions;
    public java.util.List glanceData;
    public com.navdy.service.library.events.glances.GlanceEvent$GlanceType glanceType;
    public String id;
    public String language;
    public Long postTime;
    public String provider;
    
    public GlanceEvent$Builder() {
    }
    
    public GlanceEvent$Builder(com.navdy.service.library.events.glances.GlanceEvent a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.glanceType = a.glanceType;
            this.provider = a.provider;
            this.id = a.id;
            this.postTime = a.postTime;
            this.glanceData = com.navdy.service.library.events.glances.GlanceEvent.access$000(a.glanceData);
            this.actions = com.navdy.service.library.events.glances.GlanceEvent.access$100(a.actions);
            this.language = a.language;
        }
    }
    
    public com.navdy.service.library.events.glances.GlanceEvent$Builder actions(java.util.List a) {
        this.actions = com.navdy.service.library.events.glances.GlanceEvent$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.glances.GlanceEvent build() {
        return new com.navdy.service.library.events.glances.GlanceEvent(this, (com.navdy.service.library.events.glances.GlanceEvent$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.glances.GlanceEvent$Builder glanceData(java.util.List a) {
        this.glanceData = com.navdy.service.library.events.glances.GlanceEvent$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.glances.GlanceEvent$Builder glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType a) {
        this.glanceType = a;
        return this;
    }
    
    public com.navdy.service.library.events.glances.GlanceEvent$Builder id(String s) {
        this.id = s;
        return this;
    }
    
    public com.navdy.service.library.events.glances.GlanceEvent$Builder language(String s) {
        this.language = s;
        return this;
    }
    
    public com.navdy.service.library.events.glances.GlanceEvent$Builder postTime(Long a) {
        this.postTime = a;
        return this;
    }
    
    public com.navdy.service.library.events.glances.GlanceEvent$Builder provider(String s) {
        this.provider = s;
        return this;
    }
}
