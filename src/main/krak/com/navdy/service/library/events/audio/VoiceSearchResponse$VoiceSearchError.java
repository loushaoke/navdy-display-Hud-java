package com.navdy.service.library.events.audio;

final public class VoiceSearchResponse$VoiceSearchError extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError[] $VALUES;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError AMBIENT_NOISE;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError FAILED_TO_START;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError NEED_PERMISSION;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError NOT_AVAILABLE;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError NO_RESULTS_FOUND;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError NO_WORDS_RECOGNIZED;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError OFFLINE;
    final public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST;
    final private int value;
    
    static {
        NOT_AVAILABLE = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError("NOT_AVAILABLE", 0, 0);
        NEED_PERMISSION = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError("NEED_PERMISSION", 1, 1);
        OFFLINE = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError("OFFLINE", 2, 2);
        FAILED_TO_START = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError("FAILED_TO_START", 3, 3);
        NO_WORDS_RECOGNIZED = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError("NO_WORDS_RECOGNIZED", 4, 4);
        NO_RESULTS_FOUND = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError("NO_RESULTS_FOUND", 5, 5);
        AMBIENT_NOISE = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError("AMBIENT_NOISE", 6, 6);
        VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError("VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST", 7, 7);
        com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError[] a = new com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError[8];
        a[0] = NOT_AVAILABLE;
        a[1] = NEED_PERMISSION;
        a[2] = OFFLINE;
        a[3] = FAILED_TO_START;
        a[4] = NO_WORDS_RECOGNIZED;
        a[5] = NO_RESULTS_FOUND;
        a[6] = AMBIENT_NOISE;
        a[7] = VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST;
        $VALUES = a;
    }
    
    private VoiceSearchResponse$VoiceSearchError(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError valueOf(String s) {
        return (com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError)Enum.valueOf(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.class, s);
    }
    
    public static com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
