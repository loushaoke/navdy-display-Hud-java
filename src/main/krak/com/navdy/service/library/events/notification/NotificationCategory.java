package com.navdy.service.library.events.notification;

final public class NotificationCategory extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.notification.NotificationCategory[] $VALUES;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_BUSINESS_AND_FINANCE;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_EMAIL;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_ENTERTAINMENT;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_HEALTH_AND_FITNESS;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_INCOMING_CALL;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_LOCATION;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_MISSED_CALL;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_NEWS;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_OTHER;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_SCHEDULE;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_SOCIAL;
    final public static com.navdy.service.library.events.notification.NotificationCategory CATEGORY_VOICE_MAIL;
    final private int value;
    
    static {
        CATEGORY_OTHER = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_OTHER", 0, 0);
        CATEGORY_INCOMING_CALL = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_INCOMING_CALL", 1, 1);
        CATEGORY_MISSED_CALL = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_MISSED_CALL", 2, 2);
        CATEGORY_VOICE_MAIL = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_VOICE_MAIL", 3, 3);
        CATEGORY_SOCIAL = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_SOCIAL", 4, 4);
        CATEGORY_SCHEDULE = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_SCHEDULE", 5, 5);
        CATEGORY_EMAIL = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_EMAIL", 6, 6);
        CATEGORY_NEWS = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_NEWS", 7, 7);
        CATEGORY_HEALTH_AND_FITNESS = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_HEALTH_AND_FITNESS", 8, 8);
        CATEGORY_BUSINESS_AND_FINANCE = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_BUSINESS_AND_FINANCE", 9, 9);
        CATEGORY_LOCATION = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_LOCATION", 10, 10);
        CATEGORY_ENTERTAINMENT = new com.navdy.service.library.events.notification.NotificationCategory("CATEGORY_ENTERTAINMENT", 11, 11);
        com.navdy.service.library.events.notification.NotificationCategory[] a = new com.navdy.service.library.events.notification.NotificationCategory[12];
        a[0] = CATEGORY_OTHER;
        a[1] = CATEGORY_INCOMING_CALL;
        a[2] = CATEGORY_MISSED_CALL;
        a[3] = CATEGORY_VOICE_MAIL;
        a[4] = CATEGORY_SOCIAL;
        a[5] = CATEGORY_SCHEDULE;
        a[6] = CATEGORY_EMAIL;
        a[7] = CATEGORY_NEWS;
        a[8] = CATEGORY_HEALTH_AND_FITNESS;
        a[9] = CATEGORY_BUSINESS_AND_FINANCE;
        a[10] = CATEGORY_LOCATION;
        a[11] = CATEGORY_ENTERTAINMENT;
        $VALUES = a;
    }
    
    private NotificationCategory(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.notification.NotificationCategory valueOf(String s) {
        return (com.navdy.service.library.events.notification.NotificationCategory)Enum.valueOf(com.navdy.service.library.events.notification.NotificationCategory.class, s);
    }
    
    public static com.navdy.service.library.events.notification.NotificationCategory[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
