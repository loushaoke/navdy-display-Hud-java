package com.navdy.service.library.events.glances;

final public class ClearGlances extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public ClearGlances() {
    }
    
    private ClearGlances(com.navdy.service.library.events.glances.ClearGlances$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ClearGlances(com.navdy.service.library.events.glances.ClearGlances$Builder a, com.navdy.service.library.events.glances.ClearGlances$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.glances.ClearGlances;
    }
    
    public int hashCode() {
        return 0;
    }
}
