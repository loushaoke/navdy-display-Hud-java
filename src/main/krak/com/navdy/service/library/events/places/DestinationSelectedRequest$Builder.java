package com.navdy.service.library.events.places;

final public class DestinationSelectedRequest$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.destination.Destination destination;
    public String request_id;
    
    public DestinationSelectedRequest$Builder() {
    }
    
    public DestinationSelectedRequest$Builder(com.navdy.service.library.events.places.DestinationSelectedRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.request_id = a.request_id;
            this.destination = a.destination;
        }
    }
    
    public com.navdy.service.library.events.places.DestinationSelectedRequest build() {
        return new com.navdy.service.library.events.places.DestinationSelectedRequest(this, (com.navdy.service.library.events.places.DestinationSelectedRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.DestinationSelectedRequest$Builder destination(com.navdy.service.library.events.destination.Destination a) {
        this.destination = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.DestinationSelectedRequest$Builder request_id(String s) {
        this.request_id = s;
        return this;
    }
}
