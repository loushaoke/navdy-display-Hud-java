package com.navdy.service.library.events.callcontrol;

final public class PhoneStatusResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.callcontrol.PhoneEvent callStatus;
    public com.navdy.service.library.events.RequestStatus status;
    
    public PhoneStatusResponse$Builder() {
    }
    
    public PhoneStatusResponse$Builder(com.navdy.service.library.events.callcontrol.PhoneStatusResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.callStatus = a.callStatus;
        }
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneStatusResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.callcontrol.PhoneStatusResponse(this, (com.navdy.service.library.events.callcontrol.PhoneStatusResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneStatusResponse$Builder callStatus(com.navdy.service.library.events.callcontrol.PhoneEvent a) {
        this.callStatus = a;
        return this;
    }
    
    public com.navdy.service.library.events.callcontrol.PhoneStatusResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
}
