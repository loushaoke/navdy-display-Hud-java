package com.navdy.service.library.events.audio;

final public class MusicCharacterMap$Builder extends com.squareup.wire.Message.Builder {
    public String character;
    public Integer offset;
    
    public MusicCharacterMap$Builder() {
    }
    
    public MusicCharacterMap$Builder(com.navdy.service.library.events.audio.MusicCharacterMap a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.character = a.character;
            this.offset = a.offset;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicCharacterMap build() {
        return new com.navdy.service.library.events.audio.MusicCharacterMap(this, (com.navdy.service.library.events.audio.MusicCharacterMap$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicCharacterMap$Builder character(String s) {
        this.character = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCharacterMap$Builder offset(Integer a) {
        this.offset = a;
        return this;
    }
}
