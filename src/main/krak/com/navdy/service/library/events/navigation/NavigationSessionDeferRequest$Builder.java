package com.navdy.service.library.events.navigation;

final public class NavigationSessionDeferRequest$Builder extends com.squareup.wire.Message.Builder {
    public Integer delay;
    public Long expireTimestamp;
    public Boolean originDisplay;
    public String requestId;
    public String routeId;
    
    public NavigationSessionDeferRequest$Builder() {
    }
    
    public NavigationSessionDeferRequest$Builder(com.navdy.service.library.events.navigation.NavigationSessionDeferRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.requestId = a.requestId;
            this.routeId = a.routeId;
            this.delay = a.delay;
            this.originDisplay = a.originDisplay;
            this.expireTimestamp = a.expireTimestamp;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.NavigationSessionDeferRequest(this, (com.navdy.service.library.events.navigation.NavigationSessionDeferRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest$Builder delay(Integer a) {
        this.delay = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest$Builder expireTimestamp(Long a) {
        this.expireTimestamp = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest$Builder originDisplay(Boolean a) {
        this.originDisplay = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest$Builder requestId(String s) {
        this.requestId = s;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.NavigationSessionDeferRequest$Builder routeId(String s) {
        this.routeId = s;
        return this;
    }
}
