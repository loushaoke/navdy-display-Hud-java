package com.navdy.service.library.events.preferences;

final public class DriverProfilePreferencesUpdate$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.preferences.DriverProfilePreferences preferences;
    public Long serial_number;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public DriverProfilePreferencesUpdate$Builder() {
    }
    
    public DriverProfilePreferencesUpdate$Builder(com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.serial_number = a.serial_number;
            this.preferences = a.preferences;
        }
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate(this, (com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate$Builder preferences(com.navdy.service.library.events.preferences.DriverProfilePreferences a) {
        this.preferences = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate$Builder serial_number(Long a) {
        this.serial_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
