package com.navdy.service.library.events.places;

final public class DestinationSelectedResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.destination.Destination destination;
    public String request_id;
    public com.navdy.service.library.events.RequestStatus request_status;
    
    public DestinationSelectedResponse$Builder() {
    }
    
    public DestinationSelectedResponse$Builder(com.navdy.service.library.events.places.DestinationSelectedResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.request_id = a.request_id;
            this.request_status = a.request_status;
            this.destination = a.destination;
        }
    }
    
    public com.navdy.service.library.events.places.DestinationSelectedResponse build() {
        return new com.navdy.service.library.events.places.DestinationSelectedResponse(this, (com.navdy.service.library.events.places.DestinationSelectedResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.DestinationSelectedResponse$Builder destination(com.navdy.service.library.events.destination.Destination a) {
        this.destination = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.DestinationSelectedResponse$Builder request_id(String s) {
        this.request_id = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.DestinationSelectedResponse$Builder request_status(com.navdy.service.library.events.RequestStatus a) {
        this.request_status = a;
        return this;
    }
}
