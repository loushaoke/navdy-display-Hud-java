package com.navdy.service.library.events.navigation;

final public class RouteManeuverResponse$Builder extends com.squareup.wire.Message.Builder {
    public java.util.List maneuvers;
    public com.navdy.service.library.events.RequestStatus status;
    public String statusDetail;
    
    public RouteManeuverResponse$Builder() {
    }
    
    public RouteManeuverResponse$Builder(com.navdy.service.library.events.navigation.RouteManeuverResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.statusDetail = a.statusDetail;
            this.maneuvers = com.navdy.service.library.events.navigation.RouteManeuverResponse.access$000(a.maneuvers);
        }
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuverResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.navigation.RouteManeuverResponse(this, (com.navdy.service.library.events.navigation.RouteManeuverResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuverResponse$Builder maneuvers(java.util.List a) {
        this.maneuvers = com.navdy.service.library.events.navigation.RouteManeuverResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuverResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.navigation.RouteManeuverResponse$Builder statusDetail(String s) {
        this.statusDetail = s;
        return this;
    }
}
