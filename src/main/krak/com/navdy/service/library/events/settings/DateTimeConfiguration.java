package com.navdy.service.library.events.settings;

final public class DateTimeConfiguration extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.settings.DateTimeConfiguration$Clock DEFAULT_FORMAT;
    final public static Long DEFAULT_TIMESTAMP;
    final public static String DEFAULT_TIMEZONE = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.settings.DateTimeConfiguration$Clock format;
    final public Long timestamp;
    final public String timezone;
    
    static {
        DEFAULT_TIMESTAMP = Long.valueOf(0L);
        DEFAULT_FORMAT = com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_24_HOUR;
    }
    
    private DateTimeConfiguration(com.navdy.service.library.events.settings.DateTimeConfiguration$Builder a) {
        this(a.timestamp, a.timezone, a.format);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DateTimeConfiguration(com.navdy.service.library.events.settings.DateTimeConfiguration$Builder a, com.navdy.service.library.events.settings.DateTimeConfiguration$1 a0) {
        this(a);
    }
    
    public DateTimeConfiguration(Long a, String s, com.navdy.service.library.events.settings.DateTimeConfiguration$Clock a0) {
        this.timestamp = a;
        this.timezone = s;
        this.format = a0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.settings.DateTimeConfiguration) {
                com.navdy.service.library.events.settings.DateTimeConfiguration a0 = (com.navdy.service.library.events.settings.DateTimeConfiguration)a;
                boolean b0 = this.equals(this.timestamp, a0.timestamp);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.timezone, a0.timezone)) {
                        break label1;
                    }
                    if (this.equals(this.format, a0.format)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.timestamp == null) ? 0 : this.timestamp.hashCode()) * 37 + ((this.timezone == null) ? 0 : this.timezone.hashCode())) * 37 + ((this.format == null) ? 0 : this.format.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
