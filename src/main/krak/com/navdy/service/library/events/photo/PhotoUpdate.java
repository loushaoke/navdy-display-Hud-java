package com.navdy.service.library.events.photo;

final public class PhotoUpdate extends com.squareup.wire.Message {
    final public static String DEFAULT_IDENTIFIER = "";
    final public static okio.ByteString DEFAULT_PHOTO;
    final public static com.navdy.service.library.events.photo.PhotoType DEFAULT_PHOTOTYPE;
    final private static long serialVersionUID = 0L;
    final public String identifier;
    final public okio.ByteString photo;
    final public com.navdy.service.library.events.photo.PhotoType photoType;
    
    static {
        DEFAULT_PHOTO = okio.ByteString.EMPTY;
        DEFAULT_PHOTOTYPE = com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART;
    }
    
    private PhotoUpdate(com.navdy.service.library.events.photo.PhotoUpdate$Builder a) {
        this(a.identifier, a.photo, a.photoType);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PhotoUpdate(com.navdy.service.library.events.photo.PhotoUpdate$Builder a, com.navdy.service.library.events.photo.PhotoUpdate$1 a0) {
        this(a);
    }
    
    public PhotoUpdate(String s, okio.ByteString a, com.navdy.service.library.events.photo.PhotoType a0) {
        this.identifier = s;
        this.photo = a;
        this.photoType = a0;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.photo.PhotoUpdate) {
                com.navdy.service.library.events.photo.PhotoUpdate a0 = (com.navdy.service.library.events.photo.PhotoUpdate)a;
                boolean b0 = this.equals(this.identifier, a0.identifier);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.photo, a0.photo)) {
                        break label1;
                    }
                    if (this.equals(this.photoType, a0.photoType)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.identifier == null) ? 0 : this.identifier.hashCode()) * 37 + ((this.photo == null) ? 0 : this.photo.hashCode())) * 37 + ((this.photoType == null) ? 0 : this.photoType.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
