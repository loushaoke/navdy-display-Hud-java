package com.navdy.service.library.events.settings;

final public class ScreenConfiguration extends com.squareup.wire.Message {
    final public static Integer DEFAULT_MARGINBOTTOM;
    final public static Integer DEFAULT_MARGINLEFT;
    final public static Integer DEFAULT_MARGINRIGHT;
    final public static Integer DEFAULT_MARGINTOP;
    final public static Float DEFAULT_SCALEX;
    final public static Float DEFAULT_SCALEY;
    final private static long serialVersionUID = 0L;
    final public Integer marginBottom;
    final public Integer marginLeft;
    final public Integer marginRight;
    final public Integer marginTop;
    final public Float scaleX;
    final public Float scaleY;
    
    static {
        DEFAULT_MARGINLEFT = Integer.valueOf(0);
        DEFAULT_MARGINTOP = Integer.valueOf(0);
        DEFAULT_MARGINRIGHT = Integer.valueOf(0);
        DEFAULT_MARGINBOTTOM = Integer.valueOf(0);
        DEFAULT_SCALEX = Float.valueOf(1f);
        DEFAULT_SCALEY = Float.valueOf(1f);
    }
    
    private ScreenConfiguration(com.navdy.service.library.events.settings.ScreenConfiguration$Builder a) {
        this(a.marginLeft, a.marginTop, a.marginRight, a.marginBottom, a.scaleX, a.scaleY);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ScreenConfiguration(com.navdy.service.library.events.settings.ScreenConfiguration$Builder a, com.navdy.service.library.events.settings.ScreenConfiguration$1 a0) {
        this(a);
    }
    
    public ScreenConfiguration(Integer a, Integer a0, Integer a1, Integer a2, Float a3, Float a4) {
        this.marginLeft = a;
        this.marginTop = a0;
        this.marginRight = a1;
        this.marginBottom = a2;
        this.scaleX = a3;
        this.scaleY = a4;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.settings.ScreenConfiguration) {
                com.navdy.service.library.events.settings.ScreenConfiguration a0 = (com.navdy.service.library.events.settings.ScreenConfiguration)a;
                boolean b0 = this.equals(this.marginLeft, a0.marginLeft);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.marginTop, a0.marginTop)) {
                        break label1;
                    }
                    if (!this.equals(this.marginRight, a0.marginRight)) {
                        break label1;
                    }
                    if (!this.equals(this.marginBottom, a0.marginBottom)) {
                        break label1;
                    }
                    if (!this.equals(this.scaleX, a0.scaleX)) {
                        break label1;
                    }
                    if (this.equals(this.scaleY, a0.scaleY)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((((((this.marginLeft == null) ? 0 : this.marginLeft.hashCode()) * 37 + ((this.marginTop == null) ? 0 : this.marginTop.hashCode())) * 37 + ((this.marginRight == null) ? 0 : this.marginRight.hashCode())) * 37 + ((this.marginBottom == null) ? 0 : this.marginBottom.hashCode())) * 37 + ((this.scaleX == null) ? 0 : this.scaleX.hashCode())) * 37 + ((this.scaleY == null) ? 0 : this.scaleY.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
