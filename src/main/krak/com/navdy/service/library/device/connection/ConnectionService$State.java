package com.navdy.service.library.device.connection;


public enum ConnectionService$State {
    START(0),
    IDLE(1),
    SEARCHING(2),
    CONNECTING(3),
    RECONNECTING(4),
    CONNECTED(5),
    DISCONNECTING(6),
    DISCONNECTED(7),
    PAIRING(8),
    LISTENING(9),
    DESTROYED(10);

    private int value;
    ConnectionService$State(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class ConnectionService$State extends Enum {
//    final private static com.navdy.service.library.device.connection.ConnectionService$State[] $VALUES;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State CONNECTED;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State CONNECTING;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State DESTROYED;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State DISCONNECTED;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State DISCONNECTING;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State IDLE;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State LISTENING;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State PAIRING;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State RECONNECTING;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State SEARCHING;
//    final public static com.navdy.service.library.device.connection.ConnectionService$State START;
//    
//    static {
//        START = new com.navdy.service.library.device.connection.ConnectionService$State("START", 0);
//        IDLE = new com.navdy.service.library.device.connection.ConnectionService$State("IDLE", 1);
//        SEARCHING = new com.navdy.service.library.device.connection.ConnectionService$State("SEARCHING", 2);
//        CONNECTING = new com.navdy.service.library.device.connection.ConnectionService$State("CONNECTING", 3);
//        RECONNECTING = new com.navdy.service.library.device.connection.ConnectionService$State("RECONNECTING", 4);
//        CONNECTED = new com.navdy.service.library.device.connection.ConnectionService$State("CONNECTED", 5);
//        DISCONNECTING = new com.navdy.service.library.device.connection.ConnectionService$State("DISCONNECTING", 6);
//        DISCONNECTED = new com.navdy.service.library.device.connection.ConnectionService$State("DISCONNECTED", 7);
//        PAIRING = new com.navdy.service.library.device.connection.ConnectionService$State("PAIRING", 8);
//        LISTENING = new com.navdy.service.library.device.connection.ConnectionService$State("LISTENING", 9);
//        DESTROYED = new com.navdy.service.library.device.connection.ConnectionService$State("DESTROYED", 10);
//        com.navdy.service.library.device.connection.ConnectionService$State[] a = new com.navdy.service.library.device.connection.ConnectionService$State[11];
//        a[0] = START;
//        a[1] = IDLE;
//        a[2] = SEARCHING;
//        a[3] = CONNECTING;
//        a[4] = RECONNECTING;
//        a[5] = CONNECTED;
//        a[6] = DISCONNECTING;
//        a[7] = DISCONNECTED;
//        a[8] = PAIRING;
//        a[9] = LISTENING;
//        a[10] = DESTROYED;
//        $VALUES = a;
//    }
//    
//    private ConnectionService$State(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.service.library.device.connection.ConnectionService$State valueOf(String s) {
//        return (com.navdy.service.library.device.connection.ConnectionService$State)Enum.valueOf(com.navdy.service.library.device.connection.ConnectionService$State.class, s);
//    }
//    
//    public static com.navdy.service.library.device.connection.ConnectionService$State[] values() {
//        return $VALUES.clone();
//    }
//}
//