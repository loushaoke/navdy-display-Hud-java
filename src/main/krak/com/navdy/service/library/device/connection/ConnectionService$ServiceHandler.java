package com.navdy.service.library.device.connection;

final public class ConnectionService$ServiceHandler extends android.os.Handler {
    final com.navdy.service.library.device.connection.ConnectionService this$0;
    
    public ConnectionService$ServiceHandler(com.navdy.service.library.device.connection.ConnectionService a, android.os.Looper a0) {
        super(a0);
        this.this$0 = a;
    }
    
    public void handleMessage(android.os.Message a) {
        switch(a.what) {
            case 4: {
                if (com.navdy.service.library.device.connection.ConnectionService.access$000(this.this$0)) {
                    this.this$0.logger.i("stopping/starting listeners");
                    this.this$0.stopListeners();
                    this.this$0.startListeners();
                }
                if (com.navdy.service.library.device.connection.ConnectionService.access$100(this.this$0)) {
                    this.this$0.logger.i("restarting broadcasters");
                    this.this$0.stopBroadcasters();
                    this.this$0.startBroadcasters();
                }
                this.this$0.stopProxyService();
                this.this$0.startProxyService();
                break;
            }
            case 3: {
                com.navdy.service.library.device.connection.Connection$DisconnectCause a0 = com.navdy.service.library.device.connection.Connection$DisconnectCause.values()[a.arg1];
                com.navdy.service.library.device.RemoteDevice a1 = (com.navdy.service.library.device.RemoteDevice)a.obj;
                this.this$0.handleDeviceDisconnect(a1, a0);
                break;
            }
            case 2: {
                this.this$0.heartBeat();
                break;
            }
            case 1: {
                com.navdy.service.library.device.connection.ConnectionService$State a2 = (com.navdy.service.library.device.connection.ConnectionService$State)a.obj;
                if (a2 == this.this$0.state) {
                    break;
                }
                this.this$0.exitState(this.this$0.state);
                this.this$0.state = a2;
                this.this$0.enterState(this.this$0.state);
                break;
            }
            default: {
                this.this$0.logger.e(new StringBuilder().append("Unknown message: ").append(a).toString());
            }
        }
    }
}
