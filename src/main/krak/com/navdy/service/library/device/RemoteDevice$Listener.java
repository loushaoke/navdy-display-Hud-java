package com.navdy.service.library.device;

abstract public interface RemoteDevice$Listener extends com.navdy.service.library.util.Listenable$Listener {
    abstract public void onDeviceConnectFailure(com.navdy.service.library.device.RemoteDevice arg, com.navdy.service.library.device.connection.Connection$ConnectionFailureCause arg0);
    
    
    abstract public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice arg);
    
    
    abstract public void onDeviceConnecting(com.navdy.service.library.device.RemoteDevice arg);
    
    
    abstract public void onDeviceDisconnected(com.navdy.service.library.device.RemoteDevice arg, com.navdy.service.library.device.connection.Connection$DisconnectCause arg0);
    
    
    abstract public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice arg, com.navdy.service.library.events.NavdyEvent arg0);
    
    
    abstract public void onNavdyEventReceived(com.navdy.service.library.device.RemoteDevice arg, byte[] arg0);
}
