package com.navdy.service.library.device.link;

abstract public interface LinkManager$LinkFactory {
    abstract public com.navdy.service.library.device.link.Link build(com.navdy.service.library.device.connection.ConnectionInfo arg);
}
