package com.navdy.service.library.device.connection.tunnel;

public class Tunnel extends com.navdy.service.library.device.connection.ProxyService {
    final static String TAG;
    final private com.navdy.service.library.network.SocketAcceptor acceptor;
    final private com.navdy.service.library.network.SocketFactory connector;
    final java.util.ArrayList transferThreads;
    
    static {
        TAG = com.navdy.service.library.device.connection.tunnel.Tunnel.class.getSimpleName();
    }
    
    public Tunnel(com.navdy.service.library.network.SocketAcceptor a, com.navdy.service.library.network.SocketFactory a0) {
        this.transferThreads = new java.util.ArrayList();
        this.setName(TAG);
        this.acceptor = a;
        this.connector = a0;
    }
    
    public void cancel() {
        try {
            this.acceptor.close();
        } catch(java.io.IOException a) {
            android.util.Log.e(TAG, new StringBuilder().append("close failed:").append(a.getMessage()).toString());
        }
        Object a0 = ((java.util.ArrayList)this.transferThreads.clone()).iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            ((com.navdy.service.library.device.connection.tunnel.TransferThread)((java.util.Iterator)a0).next()).cancel();
        }
    }
    
    public void run() {
        String s = TAG;
        Object[] a = new Object[2];
        a[0] = this.acceptor;
        a[1] = this.connector;
        android.util.Log.d(s, String.format("start: %s -> %s", a));
        while(true) {
            label1: {
                com.navdy.service.library.network.SocketAdapter a0 = null;
                String s0 = null;
                com.navdy.service.library.network.SocketAdapter a1 = null;
                java.io.InputStream a2 = null;
                java.io.OutputStream a3 = null;
                java.io.InputStream a4 = null;
                java.io.OutputStream a5 = null;
                label0: {
                    Exception a6 = null;
                    try {
                        a0 = this.acceptor.accept();
                        s0 = TAG;
                        break label0;
                    } catch(Exception a7) {
                        a6 = a7;
                    }
                    android.util.Log.e(TAG, new StringBuilder().append("accept failed:").append(a6.getMessage()).toString());
                    break label1;
                }
                android.util.Log.v(s0, new StringBuilder().append("accepted connection (").append(a0).append(")").toString());
                try {
                    try {
                        a1 = null;
                        a2 = null;
                        a3 = null;
                        a4 = null;
                        a5 = null;
                        a1 = this.connector.build();
                        a2 = null;
                        a3 = null;
                        a4 = null;
                        a5 = null;
                        a1.connect();
                        String s1 = TAG;
                        a2 = null;
                        a3 = null;
                        a4 = null;
                        a5 = null;
                        StringBuilder a8 = new StringBuilder();
                        a2 = null;
                        a3 = null;
                        a4 = null;
                        a5 = null;
                        android.util.Log.v(s1, a8.append("connected (").append(a1).append(")").toString());
                        String s2 = TAG;
                        a2 = null;
                        a3 = null;
                        a4 = null;
                        a5 = null;
                        Object[] a9 = new Object[2];
                        a9[0] = a0;
                        a9[1] = a1;
                        android.util.Log.d(s2, String.format("starting transfer: %s -> %s", a9));
                        a2 = null;
                        a3 = null;
                        a4 = null;
                        a5 = null;
                        a2 = a0.getInputStream();
                        a3 = null;
                        a4 = null;
                        a5 = null;
                        a5 = a1.getOutputStream();
                        a3 = null;
                        a4 = null;
                        com.navdy.service.library.device.connection.tunnel.TransferThread a10 = new com.navdy.service.library.device.connection.tunnel.TransferThread(this, a2, a5, true);
                        a3 = null;
                        a4 = null;
                        a4 = a1.getInputStream();
                        a3 = null;
                        a3 = a0.getOutputStream();
                        com.navdy.service.library.device.connection.tunnel.TransferThread a11 = new com.navdy.service.library.device.connection.tunnel.TransferThread(this, a4, a3, false);
                        a10.start();
                        a11.start();
                        a10.join();
                        a11.join();
                        a0.close();
                        a1.close();
                        continue;
                    } catch(InterruptedException ignoredException) {
                    }
                } catch(java.io.IOException a12) {
                    android.util.Log.e(TAG, new StringBuilder().append("transfer failed:").append(a12.getMessage()).toString());
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a5);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a3);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                    continue;
                }
            }
            this.cancel();
            return;
        }
    }
}
