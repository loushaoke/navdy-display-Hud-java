package com.navdy.service.library.network;

abstract public interface SocketAdapter extends java.io.Closeable {
    abstract public void close();
    
    
    abstract public void connect();
    
    
    abstract public java.io.InputStream getInputStream();
    
    
    abstract public java.io.OutputStream getOutputStream();
    
    
    abstract public com.navdy.service.library.device.NavdyDeviceId getRemoteDevice();
    
    
    abstract public boolean isConnected();
}
