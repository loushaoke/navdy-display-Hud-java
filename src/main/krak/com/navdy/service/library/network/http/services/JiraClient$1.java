package com.navdy.service.library.network.http.services;

class JiraClient$1 implements okhttp3.Callback {
    final com.navdy.service.library.network.http.services.JiraClient this$0;
    final com.navdy.service.library.network.http.services.JiraClient$ResultCallback val$callback;
    final String val$emailAddress;
    final String val$ticketId;
    
    JiraClient$1(com.navdy.service.library.network.http.services.JiraClient a, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a0, String s, String s0) {
        super();
        this.this$0 = a;
        this.val$callback = a0;
        this.val$emailAddress = s;
        this.val$ticketId = s0;
    }
    
    public void onFailure(okhttp3.Call a, java.io.IOException a0) {
        com.navdy.service.library.network.http.services.JiraClient.access$000().e("onFailure ", (Throwable)a0);
        if (this.val$callback != null) {
            this.val$callback.onError((Throwable)a0);
        }
    }
    
    public void onResponse(okhttp3.Call a, okhttp3.Response a0) {
        try {
            int i = a0.code();
            if (i != 200) {
                if (this.val$callback != null) {
                    this.val$callback.onError((Throwable)new java.io.IOException(new StringBuilder().append("Jira request failed with ").append(i).toString()));
                }
            } else {
                org.json.JSONArray a1 = new org.json.JSONArray(a0.body().string());
                if (a1.length() > 0) {
                    String s = "";
                    int i0 = 0;
                    while(i0 < a1.length()) {
                        org.json.JSONObject a2 = (org.json.JSONObject)a1.get(i0);
                        if (i0 == 0) {
                            s = a2.getString("name");
                        }
                        boolean b = a2.has("emailAddress");
                        {
                            label0: {
                                label1: {
                                    if (!b) {
                                        break label1;
                                    }
                                    String s0 = a2.getString("emailAddress");
                                    com.navdy.service.library.network.http.services.JiraClient.access$000().d(new StringBuilder().append("Email Id ").append(s0).toString());
                                    if (!android.text.TextUtils.equals((CharSequence)s0, (CharSequence)this.val$emailAddress)) {
                                        break label1;
                                    }
                                    if (a2.has("name")) {
                                        break label0;
                                    }
                                }
                                i0 = i0 + 1;
                                continue;
                            }
                            s = a2.getString("name");
                            com.navdy.service.library.network.http.services.JiraClient.access$000().d(new StringBuilder().append("User name ").append(s).toString());
                            break;
                        }
                    }
                    com.navdy.service.library.network.http.services.JiraClient.access$000().d(new StringBuilder().append("Assigning the ticket to User ").append(s).toString());
                    this.this$0.assignTicketToUser(this.val$ticketId, s, this.val$callback);
                }
            }
        } catch(Throwable a3) {
            if (this.val$callback != null) {
                this.val$callback.onError(a3);
            }
        }
    }
}
