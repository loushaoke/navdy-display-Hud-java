package com.navdy.service.library.network;

public class TCPSocketAdapter implements com.navdy.service.library.network.SocketAdapter {
    final private String host;
    final private int port;
    private java.net.Socket socket;
    
    public TCPSocketAdapter(String s, int i) {
        if (s == null) {
            throw new IllegalArgumentException("Host must not be null");
        }
        this.host = s;
        this.port = i;
    }
    
    public TCPSocketAdapter(java.net.Socket a) {
        this.host = null;
        this.port = -1;
        this.socket = a;
    }
    
    public void close() {
        if (this.socket != null) {
            this.socket.close();
        }
        this.socket = null;
    }
    
    public void connect() {
        if (this.host == null) {
            throw new IllegalStateException("Can't connect with accepted socket");
        }
        this.socket = new java.net.Socket(this.host, this.port);
    }
    
    public java.io.InputStream getInputStream() {
        return this.socket.getInputStream();
    }
    
    public java.io.OutputStream getOutputStream() {
        return this.socket.getOutputStream();
    }
    
    public String getRemoteAddress() {
        return (this.isConnected()) ? this.socket.getInetAddress().getHostAddress() : null;
    }
    
    public com.navdy.service.library.device.NavdyDeviceId getRemoteDevice() {
        return (this.isConnected()) ? com.navdy.service.library.device.NavdyDeviceId.UNKNOWN_ID : null;
    }
    
    public boolean isConnected() {
        return this.socket != null && this.socket.isConnected();
    }
}
