package com.navdy.service.library.network.http.services;

public class JiraClient {
    final public static String ASSIGNEE = "assignee";
    final public static String DESCRIPTION = "description";
    final public static String EMAIL_ADDRESS = "emailAddress";
    final public static String ENVIRONMENT = "environment";
    final public static String FIELDS = "fields";
    final public static String ISSUE_TYPE = "issuetype";
    final public static String ISSUE_TYPE_BUG = "Bug";
    final public static String JIRA_API_SEARCH_ASSIGNABLE_USER = "https://navdyhud.atlassian.net/rest/api/2/user/assignable/search";
    final private static String JIRA_ATTACHMENTS = "/attachments/";
    final private static String JIRA_ISSUE_API_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    final private static String JIRA_ISSUE_EDIT_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    final public static String KEY = "key";
    final public static String NAME = "name";
    final public static String PROJECT = "project";
    final public static String SUMMARY = "summary";
    final private static com.navdy.service.library.log.Logger sLogger;
    private String encodedCredentials;
    private okhttp3.OkHttpClient mClient;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.network.http.services.JiraClient.class);
    }
    
    public JiraClient(okhttp3.OkHttpClient a) {
        this.mClient = a;
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public void assignTicketForName(String s, String s0, String s1, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a) {
        String s2 = new StringBuilder().append("https://navdyhud.atlassian.net/rest/api/2/user/assignable/search?issueKey=").append(s).append("&username=").append(s0).toString();
        okhttp3.Request$Builder a0 = new okhttp3.Request$Builder().url(s2).get();
        if (!android.text.TextUtils.isEmpty((CharSequence)this.encodedCredentials)) {
            a0.addHeader("Authorization", new StringBuilder().append("Basic ").append(this.encodedCredentials).toString());
        }
        okhttp3.Request a1 = a0.build();
        this.mClient.newCall(a1).enqueue((okhttp3.Callback)new com.navdy.service.library.network.http.services.JiraClient$1(this, a, s1, s));
    }
    
    public void assignTicketToUser(String s, String s0, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a) {
        label1: {
            Throwable a0 = null;
            label0: {
                try {
                    org.json.JSONObject a1 = new org.json.JSONObject();
                    org.json.JSONObject a2 = new org.json.JSONObject();
                    org.json.JSONObject a3 = new org.json.JSONObject();
                    a3.put("name", s0);
                    a2.put("assignee", a3);
                    a1.put("fields", a2);
                    okhttp3.RequestBody a4 = com.navdy.service.library.network.http.HttpUtils.getJsonRequestBody(a1.toString());
                    okhttp3.Request$Builder a5 = new okhttp3.Request$Builder().url(new StringBuilder().append("https://navdyhud.atlassian.net/rest/api/2/issue//").append(s).toString()).put(a4);
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.encodedCredentials)) {
                        a5.addHeader("Authorization", new StringBuilder().append("Basic ").append(this.encodedCredentials).toString());
                    }
                    okhttp3.Request a6 = a5.build();
                    this.mClient.newCall(a6).enqueue((okhttp3.Callback)new com.navdy.service.library.network.http.services.JiraClient$2(this, a, s0));
                } catch(Throwable a7) {
                    a0 = a7;
                    break label0;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                break label1;
            }
            try {
                a.onError(a0);
            } catch(Throwable a8) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                throw a8;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
        }
    }
    
    public void attachFilesToTicket(String s, java.util.List a, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a0) {
        okhttp3.MultipartBody$Builder a1 = new okhttp3.MultipartBody$Builder().setType(okhttp3.MultipartBody.FORM);
        Object a2 = a.iterator();
        Object a3 = a0;
        while(((java.util.Iterator)a2).hasNext()) {
            com.navdy.service.library.network.http.services.JiraClient$Attachment a4 = (com.navdy.service.library.network.http.services.JiraClient$Attachment)((java.util.Iterator)a2).next();
            java.io.File a5 = new java.io.File(a4.filePath);
            if (!a5.exists()) {
                throw new IllegalArgumentException(new StringBuilder().append("Attachment ").append(a5.getName()).append("File does not exists").toString());
            }
            if (!a5.canRead()) {
                throw new IllegalArgumentException(new StringBuilder().append("Attachment ").append(a5.getName()).append("File cannot be read").toString());
            }
            okhttp3.MediaType a6 = okhttp3.MediaType.parse(a4.mimeType);
            a1.addFormDataPart("file", a5.getName(), okhttp3.RequestBody.create(a6, a5));
        }
        okhttp3.MultipartBody a7 = a1.build();
        okhttp3.Request$Builder a8 = new okhttp3.Request$Builder().url(new StringBuilder().append("https://navdyhud.atlassian.net/rest/api/2/issue/").append(s).append("/attachments/").toString()).addHeader("X-Atlassian-Token", "nocheck").post((okhttp3.RequestBody)a7);
        if (!android.text.TextUtils.isEmpty((CharSequence)this.encodedCredentials)) {
            a8.addHeader("Authorization", new StringBuilder().append("Basic ").append(this.encodedCredentials).toString());
        }
        okhttp3.Request a9 = a8.build();
        this.mClient.newCall(a9).enqueue((okhttp3.Callback)new com.navdy.service.library.network.http.services.JiraClient$4(this, (com.navdy.service.library.network.http.services.JiraClient$ResultCallback)a3, s));
    }
    
    public void setEncodedCredentials(String s) {
        this.encodedCredentials = s;
    }
    
    public void submitTicket(String s, String s0, String s1, String s2, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a) {
        this.submitTicket(s, s0, s1, s2, (String)null, a);
    }
    
    public void submitTicket(String s, String s0, String s1, String s2, String s3, com.navdy.service.library.network.http.services.JiraClient$ResultCallback a) {
        label1: {
            Throwable a0 = null;
            label0: {
                try {
                    org.json.JSONObject a1 = new org.json.JSONObject();
                    org.json.JSONObject a2 = new org.json.JSONObject();
                    org.json.JSONObject a3 = new org.json.JSONObject();
                    org.json.JSONObject a4 = new org.json.JSONObject();
                    a3.put("key", s);
                    a2.put("project", a3);
                    a4.put("name", s0);
                    a2.put("issuetype", a4);
                    a2.put("summary", s1);
                    if (!android.text.TextUtils.isEmpty((CharSequence)s3)) {
                        a2.put("environment", s3);
                    }
                    a2.put("description", s2);
                    a1.put("fields", a2);
                    okhttp3.RequestBody a5 = com.navdy.service.library.network.http.HttpUtils.getJsonRequestBody(a1.toString());
                    okhttp3.Request$Builder a6 = new okhttp3.Request$Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue/").post(a5);
                    if (!android.text.TextUtils.isEmpty((CharSequence)this.encodedCredentials)) {
                        a6.addHeader("Authorization", new StringBuilder().append("Basic ").append(this.encodedCredentials).toString());
                    }
                    okhttp3.Request a7 = a6.build();
                    this.mClient.newCall(a7).enqueue((okhttp3.Callback)new com.navdy.service.library.network.http.services.JiraClient$3(this, a));
                } catch(Throwable a8) {
                    a0 = a8;
                    break label0;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                break label1;
            }
            try {
                a.onError(a0);
            } catch(Throwable a9) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                throw a9;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
        }
    }
}
