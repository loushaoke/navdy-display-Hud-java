package com.navdy.service.library.log;

final public class Logger {
    final public static String ACTION_RELOAD = "com.navdy.service.library.log.action.RELOAD";
    final public static String DEFAULT_TAG = "Navdy";
    private static volatile long logLevelChange;
    private static com.navdy.service.library.log.LogAppender[] sAppenders;
    private volatile boolean[] loggable;
    private long startTime;
    final private String tagName;
    private volatile long timestamp;
    
    static {
        sAppenders = new com.navdy.service.library.log.LogAppender[0];
        logLevelChange = -1L;
    }
    
    public Logger(Class a) {
        this.timestamp = -2L;
        if (a == null) {
            this.tagName = "Navdy";
        } else {
            String s = a.getSimpleName();
            this.tagName = s.substring(0, Math.min(s.length(), 23));
        }
    }
    
    public Logger(String s) {
        this.timestamp = -2L;
        if (s == null) {
            this.tagName = "Navdy";
        } else {
            this.tagName = s.substring(0, Math.min(s.length(), 23));
        }
    }
    
    public static void addAppender(com.navdy.service.library.log.LogAppender a) {
        Throwable a0 = null;
        label3: synchronized(com.navdy.service.library.log.Logger.class) {
            Object a1 = null;
            Object[] a2 = null;
            int i = 0;
            try {
                if (a == null) {
                    throw new IllegalArgumentException();
                }
                com.navdy.service.library.log.LogAppender[] a3 = new com.navdy.service.library.log.LogAppender[sAppenders.length + 1];
                a1 = a;
                a2 = a3;
                i = 0;
            } catch(Throwable a4) {
                a0 = a4;
                break label3;
            }
            while(true) {
                label1: {
                    label2: {
                        label0: try {
                            if (i >= a2.length) {
                                break label1;
                            }
                            if (i >= a2.length - 1) {
                                break label0;
                            }
                            Object a5 = sAppenders[i];
                            a2[i] = a5;
                            break label2;
                        } catch(ArrayIndexOutOfBoundsException | ArrayStoreException | NullPointerException a6) {
                            a0 = a6;
                            break;
                        }
                        a2[i] = a1;
                    }
                    i = i + 1;
                    continue;
                }
                sAppenders = (com.navdy.service.library.log.LogAppender[])a2;
                /*monexit(com.navdy.service.library.log.Logger.class)*/;
                return;
            }
        }
        /*monexit(com.navdy.service.library.log.Logger.class)*/;
        throw a0;
    }
    
    public static void close() {
        int i = 0;
        while(i < sAppenders.length) {
            Object a = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a).close();
            i = i + 1;
        }
    }
    
    public static void flush() {
        int i = 0;
        while(i < sAppenders.length) {
            Object a = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a).flush();
            i = i + 1;
        }
    }
    
    public static void init(com.navdy.service.library.log.LogAppender[] a) {
        if (a != null && a.length != 0) {
            sAppenders = a.clone();
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public static void reloadLogLevels() {
        logLevelChange = System.currentTimeMillis();
    }
    
    public void d(String s) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a).d(this.tagName, s);
            i = i + 1;
        }
    }
    
    public void d(String s, Throwable a) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a0 = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a0).d(this.tagName, s, a);
            i = i + 1;
        }
    }
    
    public void d(Throwable a) {
        this.d("", a);
    }
    
    public void e(String s) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a).e(this.tagName, s);
            i = i + 1;
        }
    }
    
    public void e(String s, Throwable a) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a0 = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a0).e(this.tagName, s, a);
            i = i + 1;
        }
    }
    
    public void e(Throwable a) {
        this.e("", a);
    }
    
    public void i(String s) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a).i(this.tagName, s);
            i = i + 1;
        }
    }
    
    public void i(String s, Throwable a) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a0 = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a0).i(this.tagName, s, a);
            i = i + 1;
        }
    }
    
    public void i(Throwable a) {
        this.i("", a);
    }
    
    public boolean isLoggable(int i) {
        if (this.timestamp < logLevelChange) {
            if (this.loggable == null) {
                this.loggable = new boolean[8];
            }
            int i0 = 2;
            while(i0 <= 7) {
                this.loggable[i0] = android.util.Log.isLoggable(this.tagName, i0);
                i0 = i0 + 1;
            }
            this.timestamp = System.currentTimeMillis();
        }
        return this.loggable[i];
    }
    
    public void logTimeTaken(String s) {
        long j = System.currentTimeMillis();
        long j0 = this.startTime;
        this.d(new StringBuilder().append(s).append(" ").append(j - j0).append(" ms").toString());
    }
    
    public void recordStartTime() {
        this.startTime = System.currentTimeMillis();
    }
    
    public void v(String s) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a).v(this.tagName, s);
            i = i + 1;
        }
    }
    
    public void v(String s, Throwable a) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a0 = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a0).v(this.tagName, s, a);
            i = i + 1;
        }
    }
    
    public void v(Throwable a) {
        this.v("", a);
    }
    
    public void w(String s) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a).w(this.tagName, s);
            i = i + 1;
        }
    }
    
    public void w(String s, Throwable a) {
        int i = 0;
        while(i < sAppenders.length) {
            Object a0 = sAppenders[i];
            ((com.navdy.service.library.log.LogAppender)a0).w(this.tagName, s, a);
            i = i + 1;
        }
    }
    
    public void w(Throwable a) {
        this.w("", a);
    }
}
