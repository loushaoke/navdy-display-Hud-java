package com.navdy.service.library.file;

abstract public class TransferDataSource {
    final public static String TEST_DATA_NAME = "<TESTDATA>";
    private static com.navdy.service.library.log.Logger sLogger;
    protected long mCurOffset;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.file.TransferDataSource.class);
    }
    
    public TransferDataSource() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public static com.navdy.service.library.file.TransferDataSource fileSource(java.io.File a) {
        return new com.navdy.service.library.file.TransferDataSource$FileTransferDataSource(a);
    }
    
    public static com.navdy.service.library.file.TransferDataSource testSource(long j) {
        return new com.navdy.service.library.file.TransferDataSource$TestDataSource(j);
    }
    
    abstract public String checkSum();
    
    
    abstract public String checkSum(long arg);
    
    
    abstract public java.io.File getFile();
    
    
    abstract public String getName();
    
    
    abstract public long length();
    
    
    abstract public int read(byte[] arg);
    
    
    public void seek(long j) {
        this.mCurOffset = j;
    }
}
