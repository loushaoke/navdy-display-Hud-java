package com.navdy.service.library.file;

public class FileTransferSessionManager implements com.navdy.service.library.file.IFileTransferManager {
    final private static int MAX_SESSIONS = 5;
    final private static int SESSION_TIMEOUT_INTERVAL = 60000;
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.content.Context mContext;
    private com.navdy.service.library.file.IFileTransferAuthority mFileTransferAuthority;
    private java.util.HashMap mFileTransferSessionsIdIndexed;
    private java.util.HashMap mFileTransferSessionsPathIndexed;
    private java.util.concurrent.atomic.AtomicInteger mTransferId;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.file.FileTransferSessionManager.class);
    }
    
    public FileTransferSessionManager(android.content.Context a, com.navdy.service.library.file.IFileTransferAuthority a0) {
        this.mTransferId = new java.util.concurrent.atomic.AtomicInteger(0);
        this.mContext = a;
        this.mFileTransferAuthority = a0;
        this.mFileTransferSessionsPathIndexed = new java.util.HashMap();
        this.mFileTransferSessionsIdIndexed = new java.util.HashMap();
    }
    
    final public static String absolutePath(android.content.Context a, String s, String s0) {
        StringBuilder a0 = new StringBuilder();
        Object a1 = (s == null) ? a.getFilesDir() : s;
        return a0.append(a1).append(java.io.File.separator).append(s0).toString();
    }
    
    private void endFileTransferSession(com.navdy.service.library.file.FileTransferSession a, boolean b) {
        RuntimeException a0 = null;
        label0: {
            if (a != null) {
                String s = null;
                String s0 = null;
                synchronized(a) {
                    s = a.mFileName;
                    s0 = a.mDestinationFolder;
                    /*monexit(a)*/;
                }
                String s1 = com.navdy.service.library.file.FileTransferSessionManager.absolutePath(this.mContext, s0, s);
                this.mFileTransferSessionsPathIndexed.remove(s1);
                this.mFileTransferSessionsIdIndexed.remove(Integer.valueOf(a.mTransferId));
                a.endSession(this.mContext, b);
            }
            return;
        }
        while(true) {
            Throwable a2 = null;
            try {
                /*monexit(a)*/;
                a2 = a0;
            } catch(IllegalMonitorStateException | NullPointerException a3) {
                a0 = a3;
                continue;
            }
            throw a2;
        }
    }
    
    private int endSessions(boolean b) {
        long j = System.currentTimeMillis();
        java.util.Iterator a = this.mFileTransferSessionsIdIndexed.keySet().iterator();
        int i = 0;
        Object a0 = a;
        while(((java.util.Iterator)a0).hasNext()) {
            com.navdy.service.library.file.FileTransferSession a1 = (com.navdy.service.library.file.FileTransferSession)this.mFileTransferSessionsIdIndexed.get(((java.util.Iterator)a0).next());
            {
                if (b) {
                    if (a1 == null) {
                        continue;
                    }
                    if (j - a1.getLastActivity() <= 60000L) {
                        continue;
                    }
                }
                ((java.util.Iterator)a0).remove();
                this.endFileTransferSession(a1, false);
                i = i + 1;
            }
        }
        return i;
    }
    
    public static boolean isPullRequest(com.navdy.service.library.events.file.FileType a) {
        return com.navdy.service.library.file.FileTransferSessionManager$1.$SwitchMap$com$navdy$service$library$events$file$FileType[a.ordinal()] != 0;
    }
    
    public static com.navdy.service.library.events.file.FileTransferData prepareFileTransferData(int i, com.navdy.service.library.file.TransferDataSource a, int i0, int i1, long j, long j0) {
        com.navdy.service.library.events.file.FileTransferData a0 = null;
        try {
            boolean b = false;
            long j1 = a.length() - j0;
            if (j1 >= (long)i1) {
                b = false;
            } else {
                i1 = (int)j1;
                b = true;
            }
            byte[] a1 = new byte[i1];
            a.seek(j0);
            a.read(a1);
            com.navdy.service.library.events.file.FileTransferData$Builder a2 = new com.navdy.service.library.events.file.FileTransferData$Builder().transferId(Integer.valueOf(i)).chunkIndex(Integer.valueOf(i0));
            if (b) {
                a2.fileCheckSum(a.checkSum());
            }
            a0 = a2.dataBytes(okio.ByteString.of(a1)).lastChunk(Boolean.valueOf(b)).build();
        } catch(Throwable a3) {
            sLogger.e(new StringBuilder().append("Exception while preparing the chunk data ").append(a3).toString());
            throw a3;
        }
        return a0;
    }
    
    public String absolutePathForTransferId(int i) {
        RuntimeException a = null;
        com.navdy.service.library.file.FileTransferSession a0 = (com.navdy.service.library.file.FileTransferSession)this.mFileTransferSessionsIdIndexed.get(Integer.valueOf(i));
        label0: {
            String s = null;
            if (a0 == null) {
                s = null;
            } else {
                String s0 = null;
                String s1 = null;
                synchronized(a0) {
                    s0 = a0.mFileName;
                    s1 = a0.mDestinationFolder;
                    /*monexit(a0)*/;
                }
                s = com.navdy.service.library.file.FileTransferSessionManager.absolutePath(this.mContext, s1, s0);
            }
            return s;
        }
        while(true) {
            Throwable a2 = null;
            try {
                /*monexit(a0)*/;
                a2 = a;
            } catch(IllegalMonitorStateException | NullPointerException a3) {
                a = a3;
                continue;
            }
            throw a2;
        }
    }
    
    public void endFileTransferSession(int i, boolean b) {
        com.navdy.service.library.file.FileTransferSession a = (com.navdy.service.library.file.FileTransferSession)this.mFileTransferSessionsIdIndexed.get(Integer.valueOf(i));
        if (a != null) {
            this.endFileTransferSession(a, b);
        }
    }
    
    public com.navdy.service.library.events.file.FileType getFileType(int i) {
        com.navdy.service.library.file.FileTransferSession a = (com.navdy.service.library.file.FileTransferSession)this.mFileTransferSessionsIdIndexed.get(Integer.valueOf(i));
        com.navdy.service.library.events.file.FileType a0 = (a == null) ? null : a.getFileType();
        return a0;
    }
    
    public com.navdy.service.library.events.file.FileTransferData getNextChunk(int i) {
        com.navdy.service.library.events.file.FileTransferData a = null;
        com.navdy.service.library.file.FileTransferSession a0 = (com.navdy.service.library.file.FileTransferSession)this.mFileTransferSessionsIdIndexed.get(Integer.valueOf(i));
        label0: if (a0 == null) {
            a = null;
        } else {
            a = a0.getNextChunk();
            label1: {
                if (a == null) {
                    break label1;
                }
                if (!a.lastChunk.booleanValue()) {
                    break label0;
                }
                if (a0.isFlowControlEnabled()) {
                    break label0;
                }
            }
            this.endFileTransferSession(a0, true);
        }
        return a;
    }
    
    public com.navdy.service.library.events.file.FileTransferStatus handleFileTransferData(com.navdy.service.library.events.file.FileTransferData a) {
        com.navdy.service.library.events.file.FileTransferStatus a0 = null;
        com.navdy.service.library.file.FileTransferSession a1 = (com.navdy.service.library.file.FileTransferSession)this.mFileTransferSessionsIdIndexed.get(a.transferId);
        label0: if (a1 == null) {
            a0 = new com.navdy.service.library.events.file.FileTransferStatus$Builder().success(Boolean.valueOf(false)).transferComplete(Boolean.valueOf(false)).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NOT_INITIATED).build();
        } else {
            a0 = a1.appendChunk(this.mContext, a);
            boolean b = a0.transferComplete.booleanValue();
            label1: {
                if (b) {
                    break label1;
                }
                if (a0.error == null) {
                    break label0;
                }
                if (a0.error == com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_NO_ERROR) {
                    break label0;
                }
            }
            this.endFileTransferSession(a1, false);
        }
        return a0;
    }
    
    public com.navdy.service.library.events.file.FileTransferResponse handleFileTransferRequest(com.navdy.service.library.events.file.FileTransferRequest a) {
        com.navdy.service.library.events.file.FileTransferResponse a0 = null;
        label0: {
            Throwable a1 = null;
            if (a != null) {
                try {
                    if (this.mFileTransferAuthority.isFileTypeAllowed(a.fileType)) {
                        if (a.fileType != com.navdy.service.library.events.file.FileType.FILE_TYPE_PERF_TEST) {
                            if (com.navdy.service.library.file.FileTransferSessionManager.isPullRequest(a.fileType)) {
                                com.navdy.service.library.file.FileTransferSession a2 = new com.navdy.service.library.file.FileTransferSession(this.mTransferId.incrementAndGet());
                                String s = this.mFileTransferAuthority.getDirectoryForFileType(a.fileType);
                                String s0 = this.mFileTransferAuthority.getFileToSend(a.fileType);
                                if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                                    a0 = new com.navdy.service.library.events.file.FileTransferResponse$Builder().success(Boolean.valueOf(false)).destinationFileName(a.destinationFileName).fileType(a.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                                    break label0;
                                } else {
                                    java.io.File a3 = new java.io.File(s0);
                                    if (a3.exists()) {
                                        a0 = a2.initPull(this.mContext, a.fileType, s, a3.getName(), ((Boolean)com.squareup.wire.Wire.get(a.supportsAcks, com.navdy.service.library.events.file.FileTransferRequest.DEFAULT_SUPPORTSACKS)).booleanValue());
                                        if (!a0.success.booleanValue()) {
                                            break label0;
                                        }
                                        this.mFileTransferSessionsIdIndexed.put(Integer.valueOf(a2.mTransferId), a2);
                                        break label0;
                                    } else {
                                        a0 = new com.navdy.service.library.events.file.FileTransferResponse$Builder().success(Boolean.valueOf(false)).destinationFileName(a.destinationFileName).fileType(a.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                                        break label0;
                                    }
                                }
                            } else {
                                String s1 = this.mFileTransferAuthority.getDirectoryForFileType(a.fileType);
                                String s2 = com.navdy.service.library.file.FileTransferSessionManager.absolutePath(this.mContext, s1, a.destinationFileName);
                                com.navdy.service.library.file.FileTransferSession a4 = (com.navdy.service.library.file.FileTransferSession)this.mFileTransferSessionsPathIndexed.get(s2);
                                label1: {
                                    if (a4 != null) {
                                        break label1;
                                    }
                                    if (this.mFileTransferSessionsPathIndexed.size() != 5) {
                                        break label1;
                                    }
                                    if (this.endSessions(true) != 0) {
                                        break label1;
                                    }
                                    a0 = new com.navdy.service.library.events.file.FileTransferResponse$Builder().success(Boolean.valueOf(false)).destinationFileName(a.destinationFileName).fileType(a.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_HOST_BUSY).build();
                                    break label0;
                                }
                                this.endFileTransferSession(a4, false);
                                com.navdy.service.library.file.FileTransferSession a5 = new com.navdy.service.library.file.FileTransferSession(this.mTransferId.incrementAndGet());
                                a0 = a5.initFileTransfer(this.mContext, a.fileType, s1, a.destinationFileName, a.fileSize.longValue(), a.offset.longValue(), a.fileDataChecksum, Boolean.TRUE.equals(a.override));
                                if (!a0.success.booleanValue()) {
                                    break label0;
                                }
                                this.mFileTransferSessionsIdIndexed.put(Integer.valueOf(a5.mTransferId), a5);
                                this.mFileTransferSessionsPathIndexed.put(s2, a5);
                                break label0;
                            }
                        } else {
                            com.navdy.service.library.file.FileTransferSession a6 = new com.navdy.service.library.file.FileTransferSession(this.mTransferId.incrementAndGet());
                            a0 = a6.initTestData(this.mContext, a.fileType, "<TESTDATA>", a.fileSize.longValue());
                            if (!a0.success.booleanValue()) {
                                break label0;
                            }
                            this.mFileTransferSessionsIdIndexed.put(Integer.valueOf(a6.mTransferId), a6);
                            break label0;
                        }
                    } else {
                        a0 = new com.navdy.service.library.events.file.FileTransferResponse$Builder().success(Boolean.valueOf(false)).destinationFileName(a.destinationFileName).fileType(a.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).build();
                        break label0;
                    }
                } catch(Throwable a7) {
                    a1 = a7;
                }
            } else {
                a0 = null;
                break label0;
            }
            sLogger.e("Exception in handle file request ", a1);
            a0 = new com.navdy.service.library.events.file.FileTransferResponse$Builder().success(Boolean.valueOf(false)).destinationFileName(a.destinationFileName).fileType(a.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_IO_ERROR).build();
        }
        return a0;
    }
    
    public boolean handleFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus a) {
        boolean b = false;
        if (a != null) {
            com.navdy.service.library.file.FileTransferSession a0 = (com.navdy.service.library.file.FileTransferSession)this.mFileTransferSessionsIdIndexed.get(a.transferId);
            b = a0 != null && a0.handleFileTransferStatus(a);
        } else {
            b = false;
        }
        return b;
    }
    
    public void stop() {
        this.endSessions(false);
    }
}
