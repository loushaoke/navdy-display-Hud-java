package com.navdy.service.library.util;

public class CredentialUtil {
    final public static com.navdy.service.library.log.Logger logger;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.service.library.util.CredentialUtil.class);
    }
    
    public CredentialUtil() {
    }
    
    public static String getCredentials(android.content.Context a, String s) {
        String s0 = null;
        label0: {
            android.content.pm.PackageManager.NameNotFoundException a0 = null;
            try {
                try {
                    s0 = a.getPackageManager().getApplicationInfo(a.getPackageName(), 128).metaData.getString(s);
                    break label0;
                } catch(android.content.pm.PackageManager.NameNotFoundException a1) {
                    a0 = a1;
                }
            } catch(NullPointerException a2) {
                logger.e(new StringBuilder().append("Failed to load meta-data, NullPointer: ").append(a2.getMessage()).toString());
                s0 = "";
                break label0;
            }
            logger.e(new StringBuilder().append("Failed to load meta-data, NameNotFound: ").append(a0.getMessage()).toString());
            s0 = "";
        }
        return s0;
    }
}
