package com.navdy.service.library.util;

final public class MusicDataUtils {
    final public static String ALTERNATE_SEPARATOR = "_";
    final private static String SEPARATOR = " // ";
    
    public MusicDataUtils() {
    }
    
    public static String photoIdentifierFromTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo a) {
        return (a != null) ? (a.index != null) ? a.index.toString() : com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(a) : null;
    }
    
    public static String songIdentifierFromArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse a) {
        return (a != null) ? new StringBuilder().append(String.valueOf(a.name)).append(" // ").append(String.valueOf(a.album)).append(" // ").append(String.valueOf(a.author)).toString().replaceAll("[^A-Za-z0-9_]", "_") : null;
    }
    
    public static String songIdentifierFromTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo a) {
        return com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(a, " // ");
    }
    
    public static String songIdentifierFromTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo a, String s) {
        return (a != null) ? new StringBuilder().append(String.valueOf(a.name)).append(s).append(String.valueOf(a.album)).append(s).append(String.valueOf(a.author)).toString().replaceAll("[^A-Za-z0-9_]", "_") : null;
    }
}
