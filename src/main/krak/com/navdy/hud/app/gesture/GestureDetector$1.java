package com.navdy.hud.app.gesture;

class GestureDetector$1 {
    final static int[] $SwitchMap$com$navdy$service$library$events$input$Gesture;
    
    static {
        $SwitchMap$com$navdy$service$library$events$input$Gesture = new int[com.navdy.service.library.events.input.Gesture.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$input$Gesture;
        com.navdy.service.library.events.input.Gesture a0 = com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_UP;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_HAND_UP.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_DOWN.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_HAND_DOWN.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_FINGER_TO_PINCH.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_HAND_TO_FIST.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_PINCH.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_FIST.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_PINCH_TO_FINGER.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_FIST_TO_HAND.ordinal()] = 10;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_FINGER.ordinal()] = 11;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_PALM.ordinal()] = 12;
        } catch(NoSuchFieldError ignoredException10) {
        }
    }
}
