package com.navdy.hud.app.gesture;

public class MultipleClickGestureDetector implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final public static int DOUBLE_CLICK = 2;
    final private static int MULTIPLE_CLICK_THRESHOLD = 400;
    final private static com.navdy.hud.app.manager.InputManager$CustomKeyEvent MULTIPLIABLE_EVENT;
    final public static int TRIPLE_CLICK = 3;
    final private android.os.Handler handler;
    private com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture listener;
    private int maxAcceptableClickCount;
    private int multipleClickCount;
    final private Runnable multipleClickTrigger;
    
    static {
        MULTIPLIABLE_EVENT = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT;
    }
    
    public MultipleClickGestureDetector(int i, com.navdy.hud.app.gesture.MultipleClickGestureDetector$IMultipleClickKeyGesture a) {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.multipleClickTrigger = (Runnable)new com.navdy.hud.app.gesture.MultipleClickGestureDetector$1(this);
        if (a != null && i > 1) {
            this.maxAcceptableClickCount = i;
            this.listener = a;
            return;
        }
        throw new IllegalArgumentException();
    }
    
    static boolean access$000(com.navdy.hud.app.gesture.MultipleClickGestureDetector a, com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0) {
        return a.processEvent(a0);
    }
    
    private boolean processEvent(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        label0: synchronized(this) {
            this.handler.removeCallbacks(this.multipleClickTrigger);
            if (this.listener != null) {
                boolean b0 = MULTIPLIABLE_EVENT.equals(a);
                if (b0) {
                    this.multipleClickCount = this.multipleClickCount + 1;
                }
                while(this.multipleClickCount >= this.maxAcceptableClickCount) {
                    this.listener.onMultipleClick(this.maxAcceptableClickCount);
                    this.multipleClickCount = this.multipleClickCount - this.maxAcceptableClickCount;
                }
                label1: {
                    label2: {
                        if (!b0) {
                            break label2;
                        }
                        if (this.multipleClickCount <= 0) {
                            break label1;
                        }
                        this.handler.postDelayed(this.multipleClickTrigger, 400L);
                        break label1;
                    }
                    if (this.multipleClickCount <= 1) {
                        if (this.multipleClickCount == 1) {
                            this.listener.onKey(MULTIPLIABLE_EVENT);
                        }
                    } else {
                        this.listener.onMultipleClick(this.multipleClickCount);
                    }
                    this.multipleClickCount = 0;
                    if (a == null) {
                        break label1;
                    }
                    b = this.listener.onKey(a);
                    switch(com.navdy.hud.app.gesture.MultipleClickGestureDetector$2.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                        default: {
                            break;
                        }
                        case 1: case 2: {
                            break label0;
                        }
                    }
                }
                b = true;
            } else {
                b = false;
            }
        }
        /*monexit(this)*/;
        return b;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return this.processEvent(a);
    }
}
