package com.navdy.hud.app.gesture;

class GestureServiceConnector$3 implements Runnable {
    final com.navdy.hud.app.gesture.GestureServiceConnector this$0;
    final String val$command;
    
    GestureServiceConnector$3(com.navdy.hud.app.gesture.GestureServiceConnector a, String s) {
        super();
        this.this$0 = a;
        this.val$command = s;
    }
    
    public void run() {
        try {
            this.this$0.sendCommand(this.val$command);
        } catch(Throwable a) {
            com.navdy.hud.app.gesture.GestureServiceConnector.access$100().e(a);
        }
    }
}
