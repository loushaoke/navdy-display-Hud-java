package com.navdy.hud.app.gesture;

class GestureServiceConnector$5 implements Runnable {
    final com.navdy.hud.app.gesture.GestureServiceConnector this$0;
    final com.navdy.service.library.events.preferences.InputPreferences val$event;
    
    GestureServiceConnector$5(com.navdy.hud.app.gesture.GestureServiceConnector a, com.navdy.service.library.events.preferences.InputPreferences a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        if (this.val$event.use_gestures.booleanValue()) {
            if (com.navdy.hud.app.gesture.GestureServiceConnector.access$200(this.this$0) == null) {
                this.this$0.start();
            } else {
                com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("already running");
            }
        } else if (com.navdy.hud.app.gesture.GestureServiceConnector.access$200(this.this$0) != null) {
            this.this$0.stop();
        } else {
            com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("not running");
        }
    }
}
