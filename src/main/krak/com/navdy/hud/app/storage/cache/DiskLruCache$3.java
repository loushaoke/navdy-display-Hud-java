package com.navdy.hud.app.storage.cache;

class DiskLruCache$3 implements Runnable {
    final com.navdy.hud.app.storage.cache.DiskLruCache this$0;
    final com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry val$entry;
    
    DiskLruCache$3(com.navdy.hud.app.storage.cache.DiskLruCache a, com.navdy.hud.app.storage.cache.DiskLruCache$FileEntry a0) {
        super();
        this.this$0 = a;
        this.val$entry = a0;
    }
    
    public void run() {
        if (!com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.storage.cache.DiskLruCache.access$300(this.this$0), com.navdy.hud.app.storage.cache.DiskLruCache.access$100(this.this$0, this.val$entry.name))) {
            com.navdy.hud.app.storage.cache.DiskLruCache.access$200(this.this$0).v(new StringBuilder().append("file not deleted [").append(this.val$entry.name).append("]").toString());
        }
    }
}
