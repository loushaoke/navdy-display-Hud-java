package com.navdy.hud.app.storage.db;

final class HudDatabase$1 implements android.database.DatabaseErrorHandler {
    HudDatabase$1() {
    }
    
    public void onCorruption(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.storage.db.HudDatabase.access$000().e("**** DB Corrupt ****");
        com.navdy.hud.app.storage.db.HudDatabase.access$102(true);
        com.navdy.hud.app.storage.db.HudDatabase.access$202((Throwable)null);
        label0: try {
            java.util.List a0 = null;
            Throwable a1 = null;
            label1: if (a.isOpen()) {
                try {
                    try {
                        a0 = null;
                        a0 = a.getAttachedDbs();
                    } catch(android.database.sqlite.SQLiteException a2) {
                        a0 = null;
                        com.navdy.hud.app.storage.db.HudDatabase.access$000().i((Throwable)a2);
                        a0 = null;
                    }
                    try {
                        a.close();
                    } catch(android.database.sqlite.SQLiteException a3) {
                        com.navdy.hud.app.storage.db.HudDatabase.access$000().i((Throwable)a3);
                    }
                } catch(Throwable a4) {
                    a1 = a4;
                    break label1;
                }
                if (a0 != null) {
                    Object a5 = a0.iterator();
                    while(((java.util.Iterator)a5).hasNext()) {
                        android.util.Pair a6 = (android.util.Pair)((java.util.Iterator)a5).next();
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.storage.db.HudDatabase.access$300(), (String)a6.second);
                    }
                }
                com.navdy.hud.app.storage.db.HudDatabase.deleteDatabaseFile();
                break label0;
            } else {
                com.navdy.hud.app.storage.db.HudDatabase.deleteDatabaseFile();
                break label0;
            }
            if (a0 != null) {
                Object a7 = a0.iterator();
                while(((java.util.Iterator)a7).hasNext()) {
                    android.util.Pair a8 = (android.util.Pair)((java.util.Iterator)a7).next();
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.storage.db.HudDatabase.access$300(), (String)a8.second);
                }
            }
            com.navdy.hud.app.storage.db.HudDatabase.deleteDatabaseFile();
            throw a1;
        } catch(Throwable a9) {
            com.navdy.hud.app.storage.db.HudDatabase.access$000().e(a9);
        }
    }
}
