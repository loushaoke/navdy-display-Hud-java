package com.navdy.hud.app.storage.db.table;

public class RecentCallsTable {
    final public static String CALL_TIME = "call_time";
    final public static String CALL_TYPE = "call_type";
    final public static String CATEGORY = "category";
    final public static String DEFAULT_IMAGE_INDEX = "def_image";
    final public static String DRIVER_ID = "device_id";
    final public static String NAME = "name";
    final public static String NUMBER = "number";
    final public static String NUMBER_NUMERIC = "number_numeric";
    final public static String NUMBER_TYPE = "number_type";
    final public static String TABLE_NAME = "recent_calls";
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.table.RecentCallsTable.class);
    }
    
    public RecentCallsTable() {
    }
    
    public static void createTable(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.storage.db.table.RecentCallsTable.createTable_2(a);
    }
    
    public static void createTable_2(android.database.sqlite.SQLiteDatabase a) {
        a.execSQL(new StringBuilder().append("CREATE TABLE IF NOT EXISTS ").append("recent_calls").append(" (").append("device_id").append(" TEXT NOT NULL,").append("category").append(" INTEGER NOT NULL,").append("name").append(" TEXT,").append("number").append(" TEXT NOT NULL,").append("number_type").append(" INTEGER NOT NULL,").append("call_time").append(" INTEGER,").append("call_type").append(" INTEGER,").append("def_image").append(" INTEGER,").append("number_numeric").append(" INTEGER").append(");").toString());
        sLogger.v(new StringBuilder().append("createdTable:").append("recent_calls").toString());
        String s = new StringBuilder().append("recent_calls").append("_").append("device_id").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s).append(" ON ").append("recent_calls").append("(").append("device_id").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s).toString());
        String s0 = new StringBuilder().append("recent_calls").append("_").append("category").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s0).append(" ON ").append("recent_calls").append("(").append("category").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s0).toString());
        String s1 = new StringBuilder().append("recent_calls").append("_").append("number").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s1).append(" ON ").append("recent_calls").append("(").append("number").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s1).toString());
        String s2 = new StringBuilder().append("recent_calls").append("_").append("number_type").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s2).append(" ON ").append("recent_calls").append("(").append("number_type").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s2).toString());
        String s3 = new StringBuilder().append("recent_calls").append("_").append("call_type").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s3).append(" ON ").append("recent_calls").append("(").append("call_type").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s3).toString());
        String s4 = new StringBuilder().append("recent_calls").append("_").append("def_image").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s4).append(" ON ").append("recent_calls").append("(").append("def_image").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s4).toString());
        String s5 = new StringBuilder().append("recent_calls").append("_").append("number_numeric").toString();
        a.execSQL(new StringBuilder().append("CREATE INDEX IF NOT EXISTS ").append(s5).append(" ON ").append("recent_calls").append("(").append("number_numeric").append(");").toString());
        sLogger.v(new StringBuilder().append("createdIndex:").append(s5).toString());
    }
    
    public static void upgradeDatabase_2(android.database.sqlite.SQLiteDatabase a) {
        com.navdy.hud.app.storage.db.DatabaseUtil.dropTable(a, "recent_calls", sLogger);
        com.navdy.hud.app.storage.db.table.RecentCallsTable.createTable_2(a);
    }
}
