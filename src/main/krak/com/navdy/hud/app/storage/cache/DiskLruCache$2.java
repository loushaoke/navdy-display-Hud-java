package com.navdy.hud.app.storage.cache;

class DiskLruCache$2 implements Runnable {
    final com.navdy.hud.app.storage.cache.DiskLruCache this$0;
    final String val$name;
    final byte[] val$val;
    
    DiskLruCache$2(com.navdy.hud.app.storage.cache.DiskLruCache a, String s, byte[] a0) {
        super();
        this.this$0 = a;
        this.val$name = s;
        this.val$val = a0;
    }
    
    public void run() {
        label3: {
            java.io.FileOutputStream a = null;
            Throwable a0 = null;
            label1: {
                label2: {
                    try {
                        a = new java.io.FileOutputStream(com.navdy.hud.app.storage.cache.DiskLruCache.access$100(this.this$0, this.val$name));
                        break label2;
                    } catch(Throwable a1) {
                        a0 = a1;
                    }
                    a = null;
                    break label1;
                }
                label0: {
                    try {
                        a.write(this.val$val);
                        com.navdy.service.library.util.IOUtils.fileSync(a);
                        break label0;
                    } catch(Throwable a2) {
                        a0 = a2;
                    }
                    break label1;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                break label3;
            }
            try {
                com.navdy.hud.app.storage.cache.DiskLruCache.access$200(this.this$0).e(a0);
            } catch(Throwable a3) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                throw a3;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        }
    }
}
