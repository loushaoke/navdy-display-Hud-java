package com.navdy.hud.app.storage.db.helper;

public class VinInformationHelper {
    final private static String[] DATASET_ARGS;
    final private static String[] DATASET_PROJECTION;
    final private static String DATASET_WHERE = "vin=?";
    final private static int VIN_INFO_ORDINAL = 0;
    final public static String VIN_SHARED_PREF = "vin";
    final private static String VIN_SHARED_PREF_NAME = "activeVin";
    final private static Object lockObj;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.db.helper.VinInformationHelper.class);
        String[] a = new String[1];
        a[0] = "info";
        DATASET_PROJECTION = a;
        DATASET_ARGS = new String[1];
        lockObj = new Object();
    }
    
    public VinInformationHelper() {
    }
    
    public static void deleteVinInfo(String s) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        synchronized(lockObj) {
            try {
                android.database.sqlite.SQLiteDatabase a0 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (a0 == null) {
                    throw new com.navdy.hud.app.storage.db.DatabaseUtil$DatabaseNotAvailable();
                }
                DATASET_ARGS[0] = s;
                int i = a0.delete("vininfo", "vin=?", DATASET_ARGS);
                sLogger.v(new StringBuilder().append("vin info deleted:").append(s).append(" result=").append(i).toString());
            } catch(Throwable a1) {
                sLogger.e(a1);
            }
            /*monexit(a)*/;
        }
    }
    
    public static String getVinInfo(String s) {
        String s0 = null;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();        label2: synchronized(lockObj) {
            android.database.sqlite.SQLiteDatabase a0 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
            if (a0 == null) {
                throw new com.navdy.hud.app.storage.db.DatabaseUtil$DatabaseNotAvailable();
            }
            DATASET_ARGS[0] = s;
            android.database.Cursor a1 = a0.query("vininfo", DATASET_PROJECTION, "vin=?", DATASET_ARGS, (String)null, (String)null, (String)null);
            label1: {
                label0: {
                    if (a1 == null) {
                        break label0;
                    }
                    try {
                        if (!a1.moveToFirst()) {
                            break label0;
                        }
                        s0 = a1.getString(0);
                        break label1;
                    } catch(Throwable a2) {
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                        throw a2;
                    }
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                /*monexit(a)*/;
                s0 = null;
                break label2;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
            /*monexit(a)*/;
        }
        return s0;
    }
    
    public static android.content.SharedPreferences getVinPreference() {
        return com.navdy.hud.app.HudApplication.getAppContext().getSharedPreferences("activeVin", 4);
    }
    
    public static void storeVinInfo(String s, String s0) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.navdy.hud.app.storage.db.helper.VinInformationHelper.deleteVinInfo(s);
        synchronized(lockObj) {
            try {
                android.database.sqlite.SQLiteDatabase a0 = com.navdy.hud.app.storage.db.HudDatabase.getInstance().getWritableDatabase();
                if (a0 == null) {
                    throw new com.navdy.hud.app.storage.db.DatabaseUtil$DatabaseNotAvailable();
                }
                android.content.ContentValues a1 = new android.content.ContentValues();
                a1.put("vin", s);
                a1.put("info", s0);
                a0.insert("vininfo", (String)null, a1);
                sLogger.v(new StringBuilder().append("added vin info [ ").append(s).append(" , ").append(s0).append("]").toString());
            } catch(Throwable a2) {
                sLogger.e(a2);
            }
            /*monexit(a)*/;
        }
    }
}
