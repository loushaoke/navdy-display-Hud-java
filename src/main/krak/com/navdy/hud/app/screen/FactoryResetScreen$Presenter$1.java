package com.navdy.hud.app.screen;

class FactoryResetScreen$Presenter$1 implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener {
    final com.navdy.hud.app.screen.FactoryResetScreen$Presenter this$0;
    
    FactoryResetScreen$Presenter$1(com.navdy.hud.app.screen.FactoryResetScreen$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void executeItem(int i, int i0) {
        switch(i) {
            case 1: {
                this.this$0.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
                break;
            }
            case 0: {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordShutdown(com.navdy.hud.app.event.Shutdown$Reason.FACTORY_RESET, false);
                com.navdy.hud.app.device.dial.DialManager.getInstance().requestDialForgetKeys();
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.screen.FactoryResetScreen$Presenter$1$1(this), 1);
                break;
            }
        }
    }
    
    public void itemSelected(int i, int i0) {
    }
}
