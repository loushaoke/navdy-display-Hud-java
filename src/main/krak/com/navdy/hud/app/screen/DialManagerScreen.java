package com.navdy.hud.app.screen;

@Layout(R.layout.screen_dial_manager)
public class DialManagerScreen extends com.navdy.hud.app.screen.BaseScreen {
    private static com.navdy.hud.app.screen.DialManagerScreen$Presenter presenter;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.DialManagerScreen.class);
    }
    
    public DialManagerScreen() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static com.navdy.hud.app.screen.DialManagerScreen$Presenter access$102(com.navdy.hud.app.screen.DialManagerScreen$Presenter a) {
        presenter = a;
        return a;
    }
    
    public int getAnimationIn(flow.Flow$Direction a) {
        return -1;
    }
    
    public int getAnimationOut(flow.Flow$Direction a) {
        return -1;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.DialManagerScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING;
    }
    
    public boolean isScanningMode() {
        return presenter != null && presenter.isScanningMode();
    }
}
