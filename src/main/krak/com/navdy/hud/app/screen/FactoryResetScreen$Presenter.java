package com.navdy.hud.app.screen;
import com.navdy.hud.app.R;
import javax.inject.Inject;

public class FactoryResetScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    final private static int CANCEL_POSITION = 1;
    final private static java.util.List CONFIRMATION_CHOICES;
    final private static int FACTORY_RESET_POSITION = 0;
    @Inject
    com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.ChoiceLayout$IListener confirmationListener;
    private com.navdy.hud.app.ui.component.ConfirmationLayout factoryResetConfirmation;
    private android.content.res.Resources resources;
    
    static {
        CONFIRMATION_CHOICES = (java.util.List)new java.util.ArrayList();
        CONFIRMATION_CHOICES.add(com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(R.string.factory_reset_go));
        CONFIRMATION_CHOICES.add(com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(R.string.factory_reset_cancel));
    }
    
    public FactoryResetScreen$Presenter() {
        this.confirmationListener = (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)new com.navdy.hud.app.screen.FactoryResetScreen$Presenter$1(this);
    }
    
    public boolean handleKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return this.factoryResetConfirmation.handleKey(a);
    }
    
    public void onLoad(android.os.Bundle a) {
        this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.updateView();
        super.onLoad(a);
    }
    
    protected void updateView() {
        com.navdy.hud.app.view.FactoryResetView a = (com.navdy.hud.app.view.FactoryResetView)this.getView();
        if (a != null) {
            this.factoryResetConfirmation = a.getConfirmation();
            this.factoryResetConfirmation.screenTitle.setText((CharSequence)this.resources.getString(R.string.factory_reset_screen_title));
            this.factoryResetConfirmation.title1.setVisibility(8);
            this.factoryResetConfirmation.title2.setVisibility(8);
            this.factoryResetConfirmation.title3.setSingleLine(false);
            this.factoryResetConfirmation.title3.setText((CharSequence)this.resources.getString(R.string.factory_reset_description));
            this.factoryResetConfirmation.screenImage.setImageDrawable(this.resources.getDrawable(R.drawable.icon_settings_factory_reset));
            this.factoryResetConfirmation.setChoices(CONFIRMATION_CHOICES, 1, this.confirmationListener);
        }
    }
}
