package com.navdy.hud.app.screen;

final public class FactoryResetScreen$Presenter$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding supertype;
    
    public FactoryResetScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.FactoryResetScreen$Presenter", "members/com.navdy.hud.app.screen.FactoryResetScreen$Presenter", true, com.navdy.hud.app.screen.FactoryResetScreen$Presenter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.screen.FactoryResetScreen$Presenter.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.screen.FactoryResetScreen$Presenter.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public com.navdy.hud.app.screen.FactoryResetScreen$Presenter get() {
        com.navdy.hud.app.screen.FactoryResetScreen$Presenter a = new com.navdy.hud.app.screen.FactoryResetScreen$Presenter();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.screen.FactoryResetScreen$Presenter a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.screen.FactoryResetScreen$Presenter)a);
    }
}
