package com.navdy.hud.app.screen;

final public class DialManagerScreen$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter {
    final private static Class[] INCLUDES;
    final private static String[] INJECTS;
    final private static Class[] STATIC_INJECTIONS;
    
    static {
        String[] a = new String[1];
        a[0] = "members/com.navdy.hud.app.view.DialManagerView";
        INJECTS = a;
        STATIC_INJECTIONS = new Class[0];
        INCLUDES = new Class[0];
    }
    
    public DialManagerScreen$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.screen.DialManagerScreen$Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
