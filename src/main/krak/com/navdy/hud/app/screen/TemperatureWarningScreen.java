package com.navdy.hud.app.screen;

@Layout(R.layout.temperature_warning_lyt)
public class TemperatureWarningScreen extends com.navdy.hud.app.screen.BaseScreen {
    public TemperatureWarningScreen() {
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.TemperatureWarningScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return com.navdy.hud.app.screen.TemperatureWarningScreen.class.getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_TEMPERATURE_WARNING;
    }
}
