package com.navdy.hud.app.screen;
import com.navdy.hud.app.R;
import com.squareup.otto.Subscribe;
import javax.inject.Inject;

public class WelcomeScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    final private static int MESSAGE_TIMEOUT = 2500;
    final public static int MISSING_DEVICES_OFFSET = 100;
    private boolean appConnected;
    private Runnable appLaunchTimeout;
    private boolean appLaunched;
    private Runnable appWaitTimeout;
    @Inject
    com.squareup.otto.Bus bus;
    private boolean connected;
    private com.navdy.service.library.device.NavdyDeviceId connectingDevice;
    @Inject
    com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    private int currentItem;
    private Runnable deviceConnectTimeout;
    private com.navdy.service.library.device.RemoteDeviceRegistry deviceRegistry;
    private boolean dirty;
    private com.navdy.service.library.device.NavdyDeviceId failedConnection;
    private boolean firstUpdate;
    @Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    private android.os.Handler handler;
    private boolean ledUpdated;
    private java.util.concurrent.atomic.AtomicBoolean mGreetingPending;
    @Inject
    com.navdy.hud.app.manager.PairingManager pairingManager;
    @Inject
    com.navdy.hud.app.profile.DriverProfileManager profileManager;
    private boolean searching;
    private com.navdy.hud.app.screen.WelcomeScreen$State state;
    private Runnable stateTimeout;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public WelcomeScreen$Presenter() {
        this.deviceRegistry = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(com.navdy.hud.app.HudApplication.getAppContext());
        this.searching = false;
        this.state = com.navdy.hud.app.screen.WelcomeScreen$State.UNKNOWN;
        this.handler = new android.os.Handler();
        this.dirty = false;
        this.ledUpdated = false;
        this.mGreetingPending = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.deviceConnectTimeout = (Runnable)new com.navdy.hud.app.screen.WelcomeScreen$Presenter$1(this);
        this.appLaunchTimeout = (Runnable)new com.navdy.hud.app.screen.WelcomeScreen$Presenter$2(this);
        this.appWaitTimeout = (Runnable)new com.navdy.hud.app.screen.WelcomeScreen$Presenter$3(this);
        this.stateTimeout = (Runnable)new com.navdy.hud.app.screen.WelcomeScreen$Presenter$4(this);
    }
    
    static boolean access$100(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        return a.connected;
    }
    
    static boolean access$1000(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        return a.searching;
    }
    
    static void access$200(com.navdy.hud.app.screen.WelcomeScreen$Presenter a, com.navdy.hud.app.screen.WelcomeScreen$State a0) {
        a.setState(a0);
    }
    
    static boolean access$300(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        return a.appConnected;
    }
    
    static boolean access$402(com.navdy.hud.app.screen.WelcomeScreen$Presenter a, boolean b) {
        a.appLaunched = b;
        return b;
    }
    
    static Object access$500(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        return a.getView();
    }
    
    static com.navdy.hud.app.screen.WelcomeScreen$State access$600(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        return a.state;
    }
    
    static com.navdy.service.library.device.NavdyDeviceId access$702(com.navdy.hud.app.screen.WelcomeScreen$Presenter a, com.navdy.service.library.device.NavdyDeviceId a0) {
        a.connectingDevice = a0;
        return a0;
    }
    
    static com.navdy.service.library.device.RemoteDeviceRegistry access$800(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        return a.deviceRegistry;
    }
    
    static Object access$900(com.navdy.hud.app.screen.WelcomeScreen$Presenter a) {
        return a.getView();
    }
    
    private com.navdy.hud.app.ui.component.carousel.Carousel$Model buildAddDriverModel(boolean b) {
        android.content.res.Resources a = ((com.navdy.hud.app.view.WelcomeView)this.getView()).getResources();
        int i = b ? R.string.welcome_who_is_driving : R.string.welcome_no_drivers_found;
        com.navdy.hud.app.ui.component.carousel.Carousel$Model a0 = new com.navdy.hud.app.ui.component.carousel.Carousel$Model();
        a0.id = R.id.welcome_menu_add_driver;
        a0.smallImageRes = R.drawable.icon_add_driver;
        a0.largeImageRes = R.drawable.icon_add_driver;
        a0.infoMap = new java.util.HashMap(1);
        a0.infoMap.put(Integer.valueOf(R.id.title), a.getString(i));
        a0.infoMap.put(Integer.valueOf(R.id.subTitle), a.getString(R.string.welcome_add_driver));
        return a0;
    }
    
    private com.navdy.hud.app.ui.component.carousel.Carousel$Model buildDriverModel(com.navdy.service.library.device.NavdyDeviceId a, int i) {
        return this.buildDriverModel(a, i, 0, 0);
    }
    
    private com.navdy.hud.app.ui.component.carousel.Carousel$Model buildDriverModel(com.navdy.service.library.device.NavdyDeviceId a, int i, int i0) {
        return this.buildDriverModel(a, i, i0, 0);
    }
    
    private com.navdy.hud.app.ui.component.carousel.Carousel$Model buildDriverModel(com.navdy.service.library.device.NavdyDeviceId a, int i, int i0, int i1) {
        android.content.res.Resources a0 = ((com.navdy.hud.app.view.WelcomeView)this.getView()).getResources();
        com.navdy.hud.app.ui.component.carousel.Carousel$Model a1 = new com.navdy.hud.app.ui.component.carousel.Carousel$Model();
        a1.id = i;
        String s = a.getDeviceName();
        int i2 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getDriverImageResId(s);
        a1.smallImageRes = i2;
        a1.largeImageRes = i2;
        com.navdy.hud.app.screen.WelcomeScreen$DeviceMetadata a2 = new com.navdy.hud.app.screen.WelcomeScreen$DeviceMetadata(a, this.profileManager.getProfileForId(a));
        a1.extras = a2;
        a1.infoMap = new java.util.HashMap(4);
        a1.infoMap.put(Integer.valueOf(R.id.title), a0.getString(i0));
        com.navdy.hud.app.profile.DriverProfile a3 = a2.driverProfile;
        label1: {
            label0: {
                {
                    if (a3 == null) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)a2.driverProfile.getDriverName())) {
                        break label0;
                    }
                }
                break label1;
            }
            s = a2.driverProfile.getDriverName();
        }
        a1.infoMap.put(Integer.valueOf(R.id.subTitle), s);
        if (i1 != 0) {
            a1.infoMap.put(Integer.valueOf(R.id.message), a0.getString(i1));
        }
        return a1;
    }
    
    private com.navdy.hud.app.ui.component.carousel.Carousel$Model buildSearchingModel() {
        android.content.res.Resources a = ((com.navdy.hud.app.view.WelcomeView)this.getView()).getResources();
        com.navdy.hud.app.ui.component.carousel.Carousel$Model a0 = new com.navdy.hud.app.ui.component.carousel.Carousel$Model();
        a0.id = R.id.welcome_menu_searching;
        a0.smallImageRes = R.drawable.icon_bluetooth_connecting;
        a0.largeImageRes = R.drawable.icon_bluetooth_connecting;
        a0.infoMap = new java.util.HashMap(1);
        a0.infoMap.put(Integer.valueOf(R.id.title), a.getString(R.string.welcome_looking_for_drivers));
        a0.infoMap.put(Integer.valueOf(R.id.subTitle), "");
        return a0;
    }
    
    private void clearCallbacks() {
        this.handler.removeCallbacks(this.deviceConnectTimeout);
        this.handler.removeCallbacks(this.appLaunchTimeout);
        this.handler.removeCallbacks(this.appWaitTimeout);
    }
    
    private void clearLaunchNotification() {
        this.appLaunched = false;
        this.clearCallbacks();
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a.dismissCurrentToast("disconnection#toast");
        a.dismissCurrentToast("connection#toast");
    }
    
    private boolean hasPaired() {
        return this.deviceRegistry.hasPaired();
    }
    
    private java.util.List knownDevices() {
        java.util.List a = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(com.navdy.hud.app.HudApplication.getAppContext()).getPairedConnections();
        java.util.ArrayList a0 = new java.util.ArrayList();
        Object a1 = a.iterator();
        while(((java.util.Iterator)a1).hasNext()) {
            ((java.util.List)a0).add(((com.navdy.service.library.device.connection.ConnectionInfo)((java.util.Iterator)a1).next()).getDeviceId());
        }
        return (java.util.List)a0;
    }
    
    private void sayWelcome(com.navdy.hud.app.profile.DriverProfile a) {
        if (this.mGreetingPending.compareAndSet(true, false)) {
            android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            Object[] a1 = new Object[1];
            a1[0] = a.getFirstName();
            com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(a0.getString(R.string.welcome_welcome_driver, a1), com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_WELCOME_MESSAGE, (String)null);
        }
    }
    
    private void selectDevice(com.navdy.service.library.device.NavdyDeviceId a) {
        com.navdy.hud.app.screen.WelcomeScreen.access$000().i(new StringBuilder().append("Trying to select ").append(a).toString());
        com.navdy.service.library.device.RemoteDevice a0 = this.connectionHandler.getRemoteDevice();
        label1: {
            label0: {
                if (a0 == null) {
                    break label0;
                }
                if (!a0.getDeviceId().equals(a)) {
                    break label0;
                }
                this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.WELCOME);
                break label1;
            }
            this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.CONNECTING);
            this.handler.post((Runnable)new com.navdy.hud.app.screen.WelcomeScreen$Presenter$7(this, a));
        }
        this.setDevice(a);
        this.updateView();
    }
    
    private void setDevice(com.navdy.service.library.device.NavdyDeviceId a) {
        this.connectingDevice = a;
        this.failedConnection = null;
        this.dirty = true;
        this.currentItem = 0;
    }
    
    private void setSearching(boolean b) {
        if (this.searching != b) {
            this.searching = b;
            this.dirty = true;
        }
    }
    
    private void setState(com.navdy.hud.app.screen.WelcomeScreen$State a) {
        if (this.state != a) {
            this.state = a;
            this.pairingManager.setAutoPairing(false);
            this.clearCallbacks();
            this.dirty = true;
            com.navdy.hud.app.screen.WelcomeScreen.access$000().i(new StringBuilder().append("switching to state:").append(a).toString());
            switch(com.navdy.hud.app.screen.WelcomeScreen$1.$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[a.ordinal()]) {
                case 6: {
                    this.connectionHandler.stopSearch();
                    this.bus.post(new com.navdy.hud.app.event.Disconnect());
                    this.pairingManager.setAutoPairing(true);
                    break;
                }
                case 5: case 7: {
                    if (this.connectingDevice == null) {
                        break;
                    }
                    this.failedConnection = this.connectingDevice;
                    this.connectingDevice = null;
                    break;
                }
                case 4: {
                    this.handler.removeCallbacks(this.stateTimeout);
                    this.handler.postDelayed(this.stateTimeout, 2500L);
                    break;
                }
                case 3: {
                    this.handler.postDelayed(this.appLaunchTimeout, 12000L);
                    break;
                }
                case 2: {
                    this.handler.postDelayed(this.deviceConnectTimeout, 5000L);
                    break;
                }
                case 1: {
                    this.mGreetingPending.set(true);
                    this.handler.removeCallbacks(this.stateTimeout);
                    this.handler.postDelayed(this.stateTimeout, 2500L);
                    break;
                }
            }
            this.updateViewState();
        }
    }
    
    private void startSearchIfNeeded() {
        if (this.state == com.navdy.hud.app.screen.WelcomeScreen$State.SEARCHING && this.connectionHandler.serviceConnected() && android.bluetooth.BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            this.connectionHandler.searchForDevices();
        }
    }
    
    private void updatePairingStatusOnLED(boolean b) {
        boolean b0 = this.ledUpdated;
        label1: {
            label0: {
                if (b0) {
                    break label0;
                }
                if (!b) {
                    break label0;
                }
                com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
                this.ledUpdated = true;
                break label1;
            }
            if (this.ledUpdated) {
                com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), false);
                this.ledUpdated = false;
            }
        }
    }
    
    private void updateViewState() {
        com.navdy.hud.app.view.WelcomeView a = (com.navdy.hud.app.view.WelcomeView)this.getView();
        if (a != null) {
            int i = 0;
            switch(com.navdy.hud.app.screen.WelcomeScreen$1.$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[this.state.ordinal()]) {
                case 7: {
                    i = 2;
                    break;
                }
                case 6: {
                    i = 1;
                    break;
                }
                case 1: case 2: case 3: case 5: {
                    i = 3;
                    break;
                }
                default: {
                    i = -1;
                    break;
                }
                case 4: {
                    i = -1;
                }
            }
            if (i != -1) {
                a.setState(i);
            }
        }
    }
    
    public void cancel() {
        this.connectionHandler.stopSearch();
        this.finish();
    }
    
    public void executeItem(int i, int i0) {
        com.navdy.hud.app.screen.WelcomeScreen.access$000().i(new StringBuilder().append("execute: id:").append(i).append(" pos:").append(i0).toString());
        label0: {
            if (i == R.id.welcome_menu_searching) {
                break label0;
            }
            if (i == R.id.welcome_menu_connecting) {
                break label0;
            }
            label1: if (i != R.id.welcome_menu_add_driver) {
                com.navdy.hud.app.view.WelcomeView a = (com.navdy.hud.app.view.WelcomeView)this.getView();
                if (a == null) {
                    break label0;
                }
                com.navdy.hud.app.ui.component.carousel.Carousel$Model a0 = a.carousel.getModel(i0);
                {
                    try {
                        this.selectDevice(((com.navdy.hud.app.screen.WelcomeScreen$DeviceMetadata)a0.extras).deviceId);
                    } catch(Exception ignoredException) {
                        break label1;
                    }
                    break label0;
                }
            } else {
                this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.DOWNLOAD_APP);
                break label0;
            }
            com.navdy.hud.app.screen.WelcomeScreen.access$000().e(new StringBuilder().append("Failed to select device at pos ").append(i0).toString());
        }
    }
    
    public void finish() {
        com.navdy.hud.app.screen.BaseScreen a = this.uiStateManager.getCurrentScreen();
        label1: {
            label0: {
                if (a == null) {
                    break label0;
                }
                if (a.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME) {
                    break label0;
                }
                com.navdy.hud.app.screen.WelcomeScreen.access$000().v(new StringBuilder().append("finish: not switching: ").append(a.getScreen()).toString());
                break label1;
            }
            com.navdy.hud.app.screen.WelcomeScreen.access$000().v(new StringBuilder().append("finish: switching: ").append(a.getScreen()).toString());
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(this.uiStateManager.getDefaultMainActiveScreen()).build());
        }
    }
    
    @Subscribe
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        com.navdy.service.library.device.NavdyDeviceId a0 = null;
        try {
            a0 = new com.navdy.service.library.device.NavdyDeviceId(a.remoteDeviceId);
        } catch(Exception ignoredException) {
            a0 = null;
        }
        com.navdy.hud.app.screen.WelcomeScreen.access$000().v(new StringBuilder().append("connection state change:").append(a.state).append(" for: ").append(a0).toString());
        int i = com.navdy.hud.app.screen.WelcomeScreen$1.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()];
        label0: {
            switch(i) {
                case 5: {
                    this.connectingDevice = a0;
                    this.connected = true;
                    this.appConnected = false;
                    this.updatePairingStatusOnLED(false);
                    this.setDevice(a0);
                    this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.LAUNCHING_APP);
                    break;
                }
                case 4: {
                    if (a0 == null) {
                        break;
                    }
                    if (this.connectingDevice == null) {
                        break;
                    }
                    if (!this.connectingDevice.equals(a0)) {
                        break;
                    }
                    this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.PICKING);
                    break;
                }
                case 3: {
                    this.appConnected = true;
                    this.connectingDevice = a0;
                    this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.WELCOME);
                    break;
                }
                case 2: {
                    this.connectingDevice = a0;
                    break;
                }
                case 1: {
                    this.connected = false;
                    if (a0 != null) {
                        if (!a0.equals(this.connectingDevice)) {
                            break;
                        }
                        this.clearLaunchNotification();
                        this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.PICKING);
                        break;
                    } else {
                        this.startSearchIfNeeded();
                        break;
                    }
                }
                default: {
                    break label0;
                }
            }
            this.updateView();
        }
    }
    
    @Subscribe
    public void onConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus a) {
        com.navdy.service.library.device.NavdyDeviceId a0 = null;
        try {
            a0 = new com.navdy.service.library.device.NavdyDeviceId(a.remoteDeviceId);
        } catch(Exception ignoredException) {
            a0 = null;
        }
        com.navdy.hud.app.screen.WelcomeScreen.access$000().i(new StringBuilder().append("ConnectionStatus: ").append(a).toString());
        int i = com.navdy.hud.app.screen.WelcomeScreen$1.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStatus$Status[a.status.ordinal()];
        label0: {
            switch(i) {
                case 5: {
                    com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.screen.WelcomeScreen$Presenter$5(this), 1);
                    break label0;
                }
                case 3: {
                    if (this.state != com.navdy.hud.app.screen.WelcomeScreen$State.SEARCHING) {
                        break;
                    }
                    this.selectDevice(a0);
                    break;
                }
                case 2: {
                    this.setSearching(false);
                    this.updatePairingStatusOnLED(false);
                    if (this.state != com.navdy.hud.app.screen.WelcomeScreen$State.SEARCHING) {
                        break;
                    }
                    this.connectionHandler.stopSearch();
                    this.finish();
                    break;
                }
                case 1: {
                    this.setSearching(true);
                    this.updatePairingStatusOnLED(true);
                    break;
                }
                default: {
                    break label0;
                }
                case 4: {
                }
            }
            this.updateView();
        }
    }
    
    @Subscribe
    public void onCurrentItemChanged(int i, int i0) {
        com.navdy.hud.app.screen.WelcomeScreen.access$000().i(new StringBuilder().append("onCurrentItemChanged id:").append(i0).append(" pos:").append(i).toString());
        this.currentItem = i;
    }
    
    @Subscribe
    public void onDisconnect(com.navdy.hud.app.event.Disconnect a) {
        this.appLaunched = false;
        this.clearCallbacks();
        if (this.state != com.navdy.hud.app.screen.WelcomeScreen$State.DOWNLOAD_APP) {
            this.finish();
        }
    }
    
    @Subscribe
    public void onDismissToast(com.navdy.hud.app.framework.toast.ToastManager$DismissedToast a) {
        if (this.appLaunched && android.text.TextUtils.equals((CharSequence)"disconnection#toast", (CharSequence)a.name)) {
            com.navdy.hud.app.screen.WelcomeScreen.access$000().v("launching app timeout");
            this.handler.removeCallbacks(this.appLaunchTimeout);
            this.handler.removeCallbacks(this.appWaitTimeout);
            this.handler.postDelayed(this.appWaitTimeout, 5000L);
        }
    }
    
    @Subscribe
    public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated a) {
        com.navdy.hud.app.profile.DriverProfile a0 = this.profileManager.getCurrentProfile();
        if (this.state == com.navdy.hud.app.screen.WelcomeScreen$State.WELCOME) {
            com.navdy.hud.app.screen.WelcomeScreen.access$000().v(new StringBuilder().append("sayWelcome profile[").append(a0.getProfileName()).append("] FN[").append(a0.getFirstName()).append("]").toString());
            this.updateView();
            this.sayWelcome(a0);
            if (a.state == com.navdy.hud.app.event.DriverProfileUpdated$State.UPDATED) {
                this.handler.removeCallbacks(this.stateTimeout);
                this.handler.postDelayed(this.stateTimeout, 2500L);
            }
        }
    }
    
    public void onLoad(android.os.Bundle a) {
        super.onLoad(a);
        this.uiStateManager.enableNotificationColor(false);
        String s = null;
        if (a != null) {
            s = a.getString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION);
        }
        com.navdy.hud.app.screen.WelcomeScreen.access$000().i(new StringBuilder().append("onLoad - action:").append(s).toString());
        this.firstUpdate = true;
        this.bus.register(this);
        this.currentItem = 0;
        this.dirty = true;
        boolean b = com.navdy.hud.app.screen.WelcomeScreen.ACTION_SWITCH_PHONE.equals(s);
        label5: {
            label0: {
                label1: {
                    com.navdy.service.library.device.NavdyDeviceId a0 = null;
                    label4: {
                        if (!b) {
                            break label4;
                        }
                        this.connectingDevice = null;
                        if (this.hasPaired()) {
                            this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.PICKING);
                            break label1;
                        } else {
                            this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.DOWNLOAD_APP);
                            break label1;
                        }
                    }
                    boolean b0 = com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT.equals(s);
                    label2: {
                        label3: {
                            if (b0) {
                                break label3;
                            }
                            if (this.state != com.navdy.hud.app.screen.WelcomeScreen$State.UNKNOWN) {
                                break label2;
                            }
                        }
                        if (this.hasPaired()) {
                            this.setSearching(true);
                            this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.SEARCHING);
                            this.startSearchIfNeeded();
                            break label1;
                        } else {
                            this.setState(com.navdy.hud.app.screen.WelcomeScreen$State.DOWNLOAD_APP);
                            break label1;
                        }
                    }
                    switch(com.navdy.hud.app.screen.WelcomeScreen$1.$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[this.state.ordinal()]) {
                        case 5: {
                            this.startSearchIfNeeded();
                            break label1;
                        }
                        case 1: case 2: case 3: case 4: {
                            a0 = this.connectingDevice;
                            break;
                        }
                        default: {
                            break label1;
                        }
                    }
                    if (a0 == null) {
                        break label0;
                    }
                    this.handler.removeCallbacks(this.stateTimeout);
                    this.handler.postDelayed(this.stateTimeout, 2500L);
                }
                this.updateView();
                break label5;
            }
            this.finish();
        }
    }
    
    @Subscribe
    public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus a) {
        if (a.photoType == com.navdy.service.library.events.photo.PhotoType.PHOTO_DRIVER_PROFILE && !a.alreadyDownloaded) {
            com.navdy.hud.app.view.WelcomeView a0 = (com.navdy.hud.app.view.WelcomeView)this.getView();
            if (a0 != null) {
                a0.carousel.reload();
            }
        }
    }
    
    protected void onUnload() {
        this.updatePairingStatusOnLED(false);
        this.uiStateManager.enableNotificationColor(true);
    }
    
    protected void updateView() {
        com.navdy.hud.app.view.WelcomeView a = (com.navdy.hud.app.view.WelcomeView)this.getView();
        if (a != null) {
            this.updateViewState();
            if (this.dirty) {
                if (this.firstUpdate) {
                    this.firstUpdate = false;
                    this.handler.postDelayed((Runnable)new com.navdy.hud.app.screen.WelcomeScreen$Presenter$6(this), 1000L);
                } else {
                    a.setSearching(this.searching);
                }
                this.dirty = false;
                java.util.ArrayList a0 = new java.util.ArrayList();
                switch(com.navdy.hud.app.screen.WelcomeScreen$1.$SwitchMap$com$navdy$hud$app$screen$WelcomeScreen$State[this.state.ordinal()]) {
                    case 7: {
                        ((java.util.List)a0).add(this.buildAddDriverModel(true));
                        java.util.List a1 = this.knownDevices();
                        com.navdy.service.library.device.NavdyDeviceId a2 = this.connectionHandler.getConnectedDevice();
                        this.currentItem = (a1.size() <= 0) ? 0 : 1;
                        java.util.Iterator a3 = a1.iterator();
                        int i = 1;
                        Object a4 = a3;
                        while(((java.util.Iterator)a4).hasNext()) {
                            int i0 = 0;
                            int i1 = 0;
                            com.navdy.service.library.device.NavdyDeviceId a5 = (com.navdy.service.library.device.NavdyDeviceId)((java.util.Iterator)a4).next();
                            if (a5.equals(this.failedConnection)) {
                                this.currentItem = i;
                                i0 = R.string.welcome_cant_connect;
                                i1 = R.string.welcome_bluetooth_disabled;
                            } else if (a5.equals(a2)) {
                                i0 = R.string.welcome_current_driver;
                                i1 = 0;
                            } else {
                                i0 = R.string.welcome_who_is_driving;
                                i1 = 0;
                            }
                            ((java.util.List)a0).add(this.buildDriverModel(a5, i, i0, i1));
                            i = i + 1;
                        }
                        break;
                    }
                    case 5: {
                        ((java.util.List)a0).add(this.buildSearchingModel());
                        break;
                    }
                    case 4: {
                        ((java.util.List)a0).add(this.buildDriverModel(this.connectingDevice, R.id.welcome_menu_connecting, R.string.welcome_cant_connect, R.string.welcome_bluetooth_disabled));
                        break;
                    }
                    case 2: case 3: {
                        ((java.util.List)a0).add(this.buildDriverModel(this.connectingDevice, R.id.welcome_menu_connecting, R.string.welcome_connecting));
                        break;
                    }
                    case 1: {
                        ((java.util.List)a0).add(this.buildDriverModel(this.connectingDevice, R.id.welcome_menu_connecting, R.string.welcome_welcome));
                        break;
                    }
                }
                a.carousel.setModel((java.util.List)a0, this.currentItem, false);
            }
        }
    }
}
