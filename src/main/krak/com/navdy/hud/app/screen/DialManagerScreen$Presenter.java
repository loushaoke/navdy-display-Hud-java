package com.navdy.hud.app.screen;

public class DialManagerScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    @Inject
    com.squareup.otto.Bus bus;
    private com.navdy.hud.app.device.dial.DialManager dialManager;
    @Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureServiceConnector;
    private android.os.Handler handler;
    private java.util.concurrent.atomic.AtomicBoolean isLedTurnedBlue;
    private boolean scanningMode;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public DialManagerScreen$Presenter() {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.isLedTurnedBlue = new java.util.concurrent.atomic.AtomicBoolean(false);
    }
    
    private void exitScreen() {
        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
            com.navdy.hud.app.screen.DialManagerScreen.access$000().v("go to default screen");
            com.navdy.hud.app.ui.framework.UIStateManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(a.getDefaultMainActiveScreen()).build());
        } else {
            com.navdy.hud.app.screen.DialManagerScreen.access$000().v("go to welcome screen");
            android.os.Bundle a0 = new android.os.Bundle();
            a0.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT);
            this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, a0, false));
        }
    }
    
    public void dismiss() {
        this.exitScreen();
    }
    
    public com.squareup.otto.Bus getBus() {
        return this.bus;
    }
    
    public android.os.Handler getHandler() {
        return this.handler;
    }
    
    public boolean isScanningMode() {
        return this.scanningMode;
    }
    
    public void onLoad(android.os.Bundle a) {
        com.navdy.hud.app.screen.DialManagerScreen.access$000().v("onLoad");
        com.navdy.hud.app.screen.DialManagerScreen.access$102(this);
        this.dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
        this.uiStateManager.enableSystemTray(false);
        this.uiStateManager.enableNotificationColor(false);
        com.navdy.hud.app.screen.DialManagerScreen.access$000().v("systemtray:invisible");
        String s = null;
        if (a != null) {
            s = a.getString("OtaDialNameKey", (String)null);
        }
        this.updateView(s);
        super.onLoad(a);
    }
    
    protected void onUnload() {
        com.navdy.hud.app.screen.DialManagerScreen.access$000().v("onUnload");
        com.navdy.hud.app.screen.DialManagerScreen.access$102((com.navdy.hud.app.screen.DialManagerScreen$Presenter)null);
        this.showPairingLed(false);
        this.uiStateManager.enableSystemTray(true);
        this.uiStateManager.enableNotificationColor(true);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        com.navdy.hud.app.screen.DialManagerScreen.access$000().v("systemtray:visible");
        super.onUnload();
    }
    
    public void showPairingLed(boolean b) {
        if (b) {
            if (this.isLedTurnedBlue.compareAndSet(false, true)) {
                com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), true);
            }
        } else if (this.isLedTurnedBlue.compareAndSet(true, false)) {
            com.navdy.hud.app.device.light.HUDLightUtils.showPairing(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.device.light.LightManager.getInstance(), false);
        }
    }
    
    public void updateView(String s) {
        com.navdy.hud.app.view.DialManagerView a = (com.navdy.hud.app.view.DialManagerView)this.getView();
        if (a != null) {
            if (s == null) {
                if (this.dialManager.isDialConnected()) {
                    com.navdy.hud.app.screen.DialManagerScreen.access$000().v("show dial connected view");
                    a.showDialConnected();
                } else {
                    com.navdy.hud.app.screen.DialManagerScreen.access$000().v("show dial pairing view");
                    this.scanningMode = true;
                    com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
                    a.showDialPairing();
                    this.showPairingLed(true);
                }
            } else {
                com.navdy.hud.app.screen.DialManagerScreen.access$000().v("show searching for dial view");
                this.scanningMode = true;
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
                a.showDialSearching(s);
                this.showPairingLed(true);
            }
        }
    }
}
