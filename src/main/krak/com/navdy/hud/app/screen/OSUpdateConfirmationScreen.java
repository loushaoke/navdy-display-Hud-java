package com.navdy.hud.app.screen;

@Layout(R.layout.screen_update_confirmation)
public class OSUpdateConfirmationScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.OSUpdateConfirmationScreen.class);
    }
    
    public OSUpdateConfirmationScreen() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.OSUpdateConfirmationScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return com.navdy.hud.app.screen.OSUpdateConfirmationScreen.class.getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION;
    }
}
