package com.navdy.hud.app.screen;

public class DialUpdateConfirmationScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    private boolean isReminder;
    @Inject
    com.squareup.otto.Bus mBus;
    
    public DialUpdateConfirmationScreen$Presenter() {
    }
    
    public void finish() {
        this.mBus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
    }
    
    public void install() {
        android.os.Bundle a = new android.os.Bundle();
        a.putInt("PROGRESS_CAUSE", 1);
        this.mBus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS, a, false));
    }
    
    public boolean isReminder() {
        return this.isReminder;
    }
    
    public void onLoad(android.os.Bundle a) {
        super.onLoad(a);
        if (a == null) {
            com.navdy.hud.app.screen.DialUpdateConfirmationScreen.access$000().e("null bundle passed to onLoad()");
            this.isReminder = false;
        } else {
            this.isReminder = a.getBoolean("UPDATE_REMINDER", false);
        }
    }
}
