package com.navdy.hud.app.view;

class GestureVideoCaptureView$3 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State;
    
    static {
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State = new int[com.navdy.hud.app.view.GestureVideoCaptureView$State.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State;
        com.navdy.hud.app.view.GestureVideoCaptureView$State a2 = com.navdy.hud.app.view.GestureVideoCaptureView$State.SWIPE_LEFT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[com.navdy.hud.app.view.GestureVideoCaptureView$State.SWIPE_RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[com.navdy.hud.app.view.GestureVideoCaptureView$State.DOUBLE_TAP_1.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[com.navdy.hud.app.view.GestureVideoCaptureView$State.DOUBLE_TAP_2.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[com.navdy.hud.app.view.GestureVideoCaptureView$State.UPLOADING.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[com.navdy.hud.app.view.GestureVideoCaptureView$State.ALL_DONE.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[com.navdy.hud.app.view.GestureVideoCaptureView$State.UPLOADING_FAILURE.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[com.navdy.hud.app.view.GestureVideoCaptureView$State.NOT_PAIRED.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[com.navdy.hud.app.view.GestureVideoCaptureView$State.SAVING.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException10) {
        }
    }
}
