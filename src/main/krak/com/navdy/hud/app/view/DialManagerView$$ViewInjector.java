package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class DialManagerView$$ViewInjector {
    public DialManagerView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.DialManagerView a0, Object a1) {
        a0.videoContainer = (android.view.ViewGroup)a.findRequiredView(a1, R.id.videoContainer, "field 'videoContainer'");
        a0.videoView = (android.widget.VideoView)a.findRequiredView(a1, R.id.videoView, "field 'videoView'");
        a0.rePairTextView = (android.widget.TextView)a.findRequiredView(a1, R.id.repair_text, "field 'rePairTextView'");
        a0.connectedView = (com.navdy.hud.app.ui.component.ConfirmationLayout)a.findRequiredView(a1, R.id.connectedView, "field 'connectedView'");
    }
    
    public static void reset(com.navdy.hud.app.view.DialManagerView a) {
        a.videoContainer = null;
        a.videoView = null;
        a.rePairTextView = null;
        a.connectedView = null;
    }
}
