package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class NotificationView$$ViewInjector {
    public NotificationView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.NotificationView a0, Object a1) {
        a0.customNotificationContainer = (android.widget.FrameLayout)a.findRequiredView(a1, R.id.customNotificationContainer, "field 'customNotificationContainer'");
        a0.border = (com.navdy.hud.app.ui.component.ShrinkingBorderView)a.findRequiredView(a1, R.id.border, "field 'border'");
        a0.nextNotificationColorView = (com.navdy.hud.app.ui.component.image.ColorImageView)a.findRequiredView(a1, R.id.nextNotificationColorView, "field 'nextNotificationColorView'");
    }
    
    public static void reset(com.navdy.hud.app.view.NotificationView a) {
        a.customNotificationContainer = null;
        a.border = null;
        a.nextNotificationColorView = null;
    }
}
