package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class LearnGestureScreenLayout$$ViewInjector {
    public LearnGestureScreenLayout$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.LearnGestureScreenLayout a0, Object a1) {
        a0.mGestureLearningView = (com.navdy.hud.app.view.GestureLearningView)a.findRequiredView(a1, R.id.gesture_learning_view, "field 'mGestureLearningView'");
        a0.mScrollableTextPresenter = (com.navdy.hud.app.view.ScrollableTextPresenterLayout)a.findRequiredView(a1, R.id.scrollable_text_presenter, "field 'mScrollableTextPresenter'");
        a0.mCameraBlockedMessage = (android.view.ViewGroup)a.findRequiredView(a1, R.id.sensor_blocked_message, "field 'mCameraBlockedMessage'");
        a0.mVideoCaptureInstructionsLayout = (com.navdy.hud.app.view.GestureVideoCaptureView)a.findRequiredView(a1, R.id.capture_instructions_lyt, "field 'mVideoCaptureInstructionsLayout'");
    }
    
    public static void reset(com.navdy.hud.app.view.LearnGestureScreenLayout a) {
        a.mGestureLearningView = null;
        a.mScrollableTextPresenter = null;
        a.mCameraBlockedMessage = null;
        a.mVideoCaptureInstructionsLayout = null;
    }
}
