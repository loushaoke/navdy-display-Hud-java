package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

class MainView$3 implements android.view.ViewTreeObserver$OnGlobalLayoutListener {
    final com.navdy.hud.app.view.MainView this$0;
    
    MainView$3(com.navdy.hud.app.view.MainView a) {
        super();
        this.this$0 = a;
    }
    
    public void onGlobalLayout() {
        this.this$0.getViewTreeObserver().removeOnGlobalLayoutListener((android.view.ViewTreeObserver$OnGlobalLayoutListener)this);
        android.content.res.Resources a = this.this$0.getResources();
        int i = (int)a.getDimension(R.dimen.dashboard_height);
        int i0 = (int)this.this$0.getResources().getDimension(R.dimen.dashboard_box_height);
        com.navdy.hud.app.view.MainView.access$302(this.this$0, i / 2 - i0 / 2);
        ((android.widget.FrameLayout$LayoutParams)this.this$0.splitterView.getLayoutParams()).topMargin = com.navdy.hud.app.view.MainView.access$300(this.this$0) + com.navdy.hud.app.view.MainView.access$400(this.this$0);
        com.navdy.hud.app.view.MainView.access$502(this.this$0, (int)a.getDimension(R.dimen.dashboard_width));
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            com.navdy.hud.app.view.MainView.access$602(this.this$0, (int)a.getDimension(R.dimen.side_panel_width));
        } else {
            com.navdy.hud.app.view.MainView.access$602(this.this$0, (int)((float)com.navdy.hud.app.view.MainView.access$500(this.this$0) * 0.33f));
        }
        this.this$0.uiStateManager.setMainPanelWidth(com.navdy.hud.app.view.MainView.access$500(this.this$0));
        this.this$0.uiStateManager.setSidePanelWidth(com.navdy.hud.app.view.MainView.access$600(this.this$0));
        com.navdy.hud.app.view.MainView.access$700().v(new StringBuilder().append("side panel width=").append(com.navdy.hud.app.view.MainView.access$600(this.this$0)).toString());
        com.navdy.hud.app.view.MainView.access$802(this.this$0, com.navdy.hud.app.view.MainView.access$500(this.this$0) - com.navdy.hud.app.view.MainView.access$600(this.this$0));
        android.util.DisplayMetrics a0 = this.this$0.getResources().getDisplayMetrics();
        com.navdy.hud.app.view.MainView.access$700().v(new StringBuilder().append("Density:").append(a0.density).append(", dpi:").append(a0.densityDpi).append(", width:").append(a0.widthPixels).append(", height:").append(a0.heightPixels).toString());
        com.navdy.hud.app.view.MainView.access$700().v(new StringBuilder().append("onGlobalLayout mainPanelWidth = ").append(com.navdy.hud.app.view.MainView.access$500(this.this$0)).append(" sidePanelWidth = ").append(com.navdy.hud.app.view.MainView.access$600(this.this$0)).append(" rightPanelStart = ").append(com.navdy.hud.app.view.MainView.access$800(this.this$0)).append(" paddingTop = ").append(com.navdy.hud.app.view.MainView.access$300(this.this$0)).append(" mh =").append(i).toString());
        this.this$0.containerView.setX(0.0f);
        com.navdy.hud.app.view.MainView.access$700().v("containerView x: 0");
        ((android.widget.FrameLayout$LayoutParams)this.this$0.containerView.getLayoutParams()).width = com.navdy.hud.app.view.MainView.access$500(this.this$0);
        ((android.widget.FrameLayout$LayoutParams)this.this$0.notificationView.getLayoutParams()).width = com.navdy.hud.app.view.MainView.access$600(this.this$0);
        this.this$0.notificationView.setX((float)com.navdy.hud.app.view.MainView.access$500(this.this$0));
        com.navdy.hud.app.view.MainView.access$700().v(new StringBuilder().append("notifView x: ").append(com.navdy.hud.app.view.MainView.access$500(this.this$0)).toString());
        this.this$0.expandedNotificationView.setX((float)com.navdy.hud.app.view.MainView.access$600(this.this$0));
        com.navdy.hud.app.view.MainView.access$700().v(new StringBuilder().append("expand notifView x: ").append(com.navdy.hud.app.view.MainView.access$600(this.this$0)).toString());
        int i1 = com.navdy.hud.app.view.MainView.access$600(this.this$0) + (int)a.getDimension(R.dimen.expand_notif_width) + (int)a.getDimension(R.dimen.expand_notif_indicator_left_margin);
        this.this$0.notifIndicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.VERTICAL);
        this.this$0.notifIndicator.setX((float)i1);
        com.navdy.hud.app.view.MainView.access$700().v(new StringBuilder().append("notif indicator x: ").append(i1).toString());
        this.this$0.notifScrollIndicator.setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.VERTICAL);
        this.this$0.notifScrollIndicator.setProperties(com.navdy.hud.app.framework.glance.GlanceConstants.scrollingRoundSize, com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorProgressSize, 0, 0, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite, true, com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorPadding, -1, -1);
        this.this$0.notifScrollIndicator.setItemCount(100);
        com.navdy.hud.app.view.MainView.access$900(this.this$0).postDelayed((Runnable)new com.navdy.hud.app.view.MainView$3$1(this), 2000L);
        this.this$0.presenter.createScreens();
        this.this$0.presenter.setInputFocus();
    }
}
