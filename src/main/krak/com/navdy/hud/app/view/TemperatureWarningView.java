package com.navdy.hud.app.view;

public class TemperatureWarningView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener, com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static int WARNING_MESSAGE_MAX_WIDTH = 320;
    private static long lastDismissedTime;
    final private static com.navdy.service.library.log.Logger sLogger;
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    android.widget.ImageView mIcon;
    @InjectView(R.id.title2)
    android.widget.TextView mMainTitleText;
    @Inject
    com.navdy.hud.app.screen.TemperatureWarningScreen$Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    android.widget.ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    android.widget.TextView mScreenTitleText;
    @InjectView(R.id.title1)
    android.widget.TextView mTitle1;
    @InjectView(R.id.title3)
    android.widget.TextView mWarningMessage;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.TemperatureWarningView.class);
        lastDismissedTime = -1L;
    }
    
    public TemperatureWarningView(android.content.Context a) {
        super(a);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public TemperatureWarningView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public static long getLastDismissedTime() {
        return lastDismissedTime;
    }
    
    public void executeItem(int i, int i0) {
        this.mPresenter.finish();
    }
    
    public void itemSelected(int i, int i0) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        lastDismissedTime = android.os.SystemClock.elapsedRealtime();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
        this.mScreenTitleText.setText(R.string.temperature_title);
        this.mMainTitleText.setText(R.string.navdy_too_hot);
        this.mIcon.setImageResource(R.drawable.icon_warning_temperature);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(320);
        this.mTitle1.setVisibility(8);
        this.mWarningMessage.setVisibility(0);
        this.mWarningMessage.setText(R.string.avoid_overheating);
        this.mWarningMessage.setSingleLine(false);
        java.util.ArrayList a = new java.util.ArrayList();
        ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.dismiss), 0));
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
        this.mRightSwipe.setVisibility(0);
        lastDismissedTime = 0L;
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        android.widget.ImageView a0 = this.mRightSwipe;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.mRightSwipe.getVisibility() != 0) {
                        break label1;
                    }
                    if (a.gesture == com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            this.mPresenter.finish();
            b = true;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        com.navdy.hud.app.ui.component.ChoiceLayout a0 = this.mChoiceLayout;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.mChoiceLayout.getVisibility() != 0) {
                        break label1;
                    }
                    if (a == com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            this.mChoiceLayout.executeSelectedItem(true);
            b = true;
        }
        return b;
    }
}
