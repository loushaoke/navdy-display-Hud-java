package com.navdy.hud.app.view;

final public class GestureLearningView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding mBus;
    private dagger.internal.Binding mPresenter;
    
    public GestureLearningView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.GestureLearningView", false, com.navdy.hud.app.view.GestureLearningView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.mPresenter = a.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", com.navdy.hud.app.view.GestureLearningView.class, (this).getClass().getClassLoader());
        this.mBus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.view.GestureLearningView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.mPresenter);
        a0.add(this.mBus);
    }
    
    public void injectMembers(com.navdy.hud.app.view.GestureLearningView a) {
        a.mPresenter = (com.navdy.hud.app.screen.GestureLearningScreen$Presenter)this.mPresenter.get();
        a.mBus = (com.squareup.otto.Bus)this.mBus.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.GestureLearningView)a);
    }
}
