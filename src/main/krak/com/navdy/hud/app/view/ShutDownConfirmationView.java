package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class ShutDownConfirmationView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener, com.navdy.hud.app.manager.InputManager$IInputHandler {
    final public static int DIAL_OTA = 2;
    final public static int INACTIVITY = 0;
    final private static int INACTIVITY_SHUTDOWN_TIMEOUT = 60000;
    final private static int INSTALL_UPDATE_AND_SHUTDOWN_TIMEOUT = 30000;
    final public static int OTA = 1;
    final private static int SHUTDOWN_MESSAGE_MAX_WIDTH = 360;
    final private static int SHUTDOWN_RESET_TIMEOUT = 30000;
    final private static int SHUTDOWN_TIMEOUT = 5000;
    final private static int TAG_DO_NOT_SHUT_DOWN = 2;
    final private static int TAG_INSTALL_AND_SHUT_DOWN = 1;
    final private static int TAG_INSTALL_DIAL_UPDATE = 3;
    final private static int TAG_SHUT_DOWN = 0;
    final private static com.navdy.service.library.log.Logger sLogger;
    private boolean gesturesEnabled;
    private android.os.Handler handler;
    private int initialState;
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    android.widget.ImageView mIcon;
    @InjectView(R.id.title3)
    android.widget.TextView mInfoText;
    boolean mIsUpdateAvailable;
    @InjectView(R.id.leftSwipe)
    android.widget.ImageView mLefttSwipe;
    @InjectView(R.id.title2)
    android.widget.TextView mMainTitleText;
    @Inject
    com.navdy.hud.app.screen.ShutDownScreen$Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    android.widget.ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    android.widget.TextView mScreenTitleText;
    android.animation.ValueAnimator mSecondsAnimator;
    @InjectView(R.id.title4)
    android.widget.TextView mSummaryText;
    @InjectView(R.id.title1)
    android.widget.TextView mTextView1;
    private com.navdy.hud.app.event.Shutdown$Reason shutDownCause;
    private int swipeLeftAction;
    private int swipeRightAction;
    private Runnable timeout;
    String timerMessage;
    private String updateTargetVersion;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.ShutDownConfirmationView.class);
    }
    
    public ShutDownConfirmationView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public ShutDownConfirmationView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ShutDownConfirmationView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.mIsUpdateAvailable = false;
        this.handler = new android.os.Handler();
        this.gesturesEnabled = false;
        this.swipeLeftAction = -1;
        this.swipeRightAction = -1;
        this.timerMessage = null;
        this.updateTargetVersion = "1.3.2884";
        this.shutDownCause = com.navdy.hud.app.event.Shutdown$Reason.INACTIVITY;
        this.initialState = 0;
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
        this.initFromAttributes(a, a0);
    }
    
    static void access$000(com.navdy.hud.app.view.ShutDownConfirmationView a, int i) {
        a.performAction(i);
    }
    
    private void cancelTimeout() {
        sLogger.v("cancelTimeout");
        if (this.timeout != null) {
            this.handler.removeCallbacks(this.timeout);
        }
        if (this.mSecondsAnimator != null) {
            this.mSecondsAnimator.removeAllUpdateListeners();
            this.mSecondsAnimator.removeAllListeners();
            this.mSecondsAnimator.cancel();
        }
    }
    
    private int getTimeout(com.navdy.hud.app.event.Shutdown$Reason a) {
        return (com.navdy.hud.app.view.ShutDownConfirmationView$7.$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason[a.ordinal()] == 3) ? 60000 : 5000;
    }
    
    private void initFromAttributes(android.content.Context a, android.util.AttributeSet a0) {
        android.content.res.TypedArray a1 = a.getTheme().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.ShutdownScreen, 0, 0);
        try {
            this.initialState = a1.getInt(0, 0);
        } catch(Throwable a2) {
            a1.recycle();
            throw a2;
        }
        a1.recycle();
    }
    
    private void performAction(int i) {
        switch(i) {
            case 3: {
                if (!this.mPresenter.isDialFirmwareUpdatePending()) {
                    break;
                }
                this.recordUpdateSelection(true);
                this.mScreenTitleText.setText(R.string.dial_update_started);
                this.mMainTitleText.setText((CharSequence)"");
                this.mInfoText.setVisibility(0);
                java.util.ArrayList a = new java.util.ArrayList();
                ((java.util.List)a).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.shutdown), 0));
                this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
                this.mPresenter.installDialUpdateAndShutDown();
                break;
            }
            case 2: {
                this.recordUpdateSelection(false);
                this.mPresenter.finish();
                break;
            }
            case 1: {
                if (!this.mPresenter.isSoftwareUpdatePending()) {
                    break;
                }
                this.recordUpdateSelection(true);
                this.mPresenter.installAndShutDown();
                break;
            }
            case 0: {
                this.recordUpdateSelection(false);
                this.mPresenter.shutDown();
                break;
            }
        }
    }
    
    private void recordUpdateSelection(boolean b) {
        if (this.mIsUpdateAvailable) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(b, !this.mPresenter.isSoftwareUpdatePending(), this.updateTargetVersion);
        }
    }
    
    private void startCountDown() {
        sLogger.v("startCountDown");
        if (this.timeout != null) {
            this.handler.removeCallbacks(this.timeout);
        }
        if (this.mSecondsAnimator != null) {
            this.mSecondsAnimator.removeAllUpdateListeners();
            this.mSecondsAnimator.removeAllListeners();
            this.mSecondsAnimator.cancel();
        }
        if (this.initialState != 1) {
            if (this.initialState != 2) {
                int i = (this.mSecondsAnimator != null) ? 30000 : this.getTimeout(this.shutDownCause);
                if (this.timeout == null) {
                    this.timeout = (Runnable)new com.navdy.hud.app.view.ShutDownConfirmationView$5(this);
                }
                int[] a = new int[2];
                a[0] = i / 1000;
                a[1] = 0;
                this.mSecondsAnimator = android.animation.ValueAnimator.ofInt(a);
                this.mSecondsAnimator.setDuration((long)i);
                this.mSecondsAnimator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.LinearInterpolator());
                this.mSecondsAnimator.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.ShutDownConfirmationView$6(this));
                this.handler.postDelayed(this.timeout, (long)i);
                this.mSecondsAnimator.start();
            } else {
                if (this.timeout == null) {
                    this.timeout = (Runnable)new com.navdy.hud.app.view.ShutDownConfirmationView$3(this);
                }
                int[] a0 = new int[2];
                a0[0] = 30;
                a0[1] = 0;
                this.mSecondsAnimator = android.animation.ValueAnimator.ofInt(a0);
                this.mSecondsAnimator.setDuration(30000L);
                this.mSecondsAnimator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.LinearInterpolator());
                this.mSecondsAnimator.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.ShutDownConfirmationView$4(this));
                this.handler.postDelayed(this.timeout, 30000L);
                this.mSecondsAnimator.start();
            }
        } else {
            if (this.timeout == null) {
                this.timeout = (Runnable)new com.navdy.hud.app.view.ShutDownConfirmationView$1(this);
            }
            int[] a1 = new int[2];
            a1[0] = 30;
            a1[1] = 0;
            this.mSecondsAnimator = android.animation.ValueAnimator.ofInt(a1);
            this.mSecondsAnimator.setDuration(30000L);
            this.mSecondsAnimator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.LinearInterpolator());
            this.mSecondsAnimator.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.ShutDownConfirmationView$2(this));
            this.handler.postDelayed(this.timeout, 30000L);
            this.mSecondsAnimator.start();
        }
    }
    
    public void executeItem(int i, int i0) {
        this.performAction(i0);
    }
    
    public void itemSelected(int i, int i0) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.cancelTimeout();
        if (this.mPresenter != null) {
            this.mPresenter.dropView((android.view.View)this);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        }
    }
    
    protected void onFinishInflate() {
        String s = null;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.mTextView1.setVisibility(8);
        android.widget.LinearLayout a = (android.widget.LinearLayout)this.findViewById(R.id.infoContainer);
        android.view.ViewGroup$LayoutParams a0 = a.getLayoutParams();
        com.navdy.hud.app.view.MaxWidthLinearLayout a1 = (com.navdy.hud.app.view.MaxWidthLinearLayout)a;
        android.widget.LinearLayout a2 = a1;
        a1.setMaxWidth(360);
        a0.width = -2;
        a2.setLayoutParams(a0);
        if (this.mPresenter == null) {
            s = "1.2.2872";
        } else {
            this.mPresenter.takeView(this);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            if (this.mPresenter.isSoftwareUpdatePending()) {
                this.initialState = 1;
                this.updateTargetVersion = this.mPresenter.getUpdateVersion();
                s = this.mPresenter.getCurrentVersion();
            } else if (this.mPresenter.isDialFirmwareUpdatePending()) {
                this.initialState = 2;
                com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions a3 = com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions();
                this.updateTargetVersion = new StringBuilder().append("1.0.").append(a3.local.incrementalVersion).toString();
                s = new StringBuilder().append("1.0.").append(a3.dial.incrementalVersion).toString();
            } else {
                this.shutDownCause = this.mPresenter.getShutDownCause();
                this.initialState = 0;
                s = "1.2.2872";
            }
        }
        this.gesturesEnabled = false;
        this.swipeLeftAction = -1;
        this.swipeRightAction = -1;
        if (this.initialState != 1) {
            if (this.initialState != 2) {
                this.mIsUpdateAvailable = false;
                this.mScreenTitleText.setText((CharSequence)null);
                this.mMainTitleText.setText(R.string.shutting_down);
                this.mInfoText.setVisibility(0);
                this.setShutdownCause(this.shutDownCause);
                android.content.res.Resources a4 = this.getResources();
                Object[] a5 = new Object[1];
                a5[0] = Integer.valueOf(5);
                String s0 = a4.getString(R.string.powering_off, a5);
                this.mSummaryText.setText((CharSequence)s0);
                this.mSummaryText.setVisibility(0);
                this.mIcon.setImageResource(R.drawable.icon_power);
                java.util.ArrayList a6 = new java.util.ArrayList();
                ((java.util.List)a6).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.shutdown), 0));
                ((java.util.List)a6).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.dont_shutdown), 2));
                this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a6, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
                int i = this.getTimeout(this.shutDownCause);
                sLogger.v(new StringBuilder().append("shutdown cause is ").append(this.shutDownCause).append(" time is ").append(i).toString());
                this.mRightSwipe.setVisibility(8);
                this.mLefttSwipe.setVisibility(8);
                this.startCountDown();
            } else {
                this.mIsUpdateAvailable = true;
                this.mScreenTitleText.setText((CharSequence)"");
                this.mMainTitleText.setText(R.string.dial_update_available);
                android.content.res.Resources a7 = this.getResources();
                Object[] a8 = new Object[1];
                a8[0] = Integer.valueOf(30);
                this.timerMessage = a7.getString(R.string.dial_update_installation_will_begin_in, a8);
                this.mInfoText.setVisibility(0);
                this.mInfoText.setText((CharSequence)this.timerMessage);
                String s1 = this.getResources().getString(R.string.dial_update_will_upgrade_to_from);
                Object[] a9 = new Object[2];
                a9[0] = this.updateTargetVersion;
                a9[1] = s;
                String s2 = String.format(s1, a9);
                this.mSummaryText.setText((CharSequence)s2);
                this.mSummaryText.setVisibility(0);
                this.mIcon.setImageResource(R.drawable.icon_dial_update);
                java.util.ArrayList a10 = new java.util.ArrayList();
                ((java.util.List)a10).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.install), 3));
                ((java.util.List)a10).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.shutdown), 0));
                ((java.util.List)a10).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.dont_shutdown), 2));
                this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a10, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
                this.swipeLeftAction = 3;
                this.swipeRightAction = 2;
                this.gesturesEnabled = true;
                this.mRightSwipe.setVisibility(0);
                this.mLefttSwipe.setVisibility(0);
                this.startCountDown();
            }
        } else {
            this.mIsUpdateAvailable = true;
            this.mScreenTitleText.setVisibility(8);
            this.mMainTitleText.setText(R.string.update_available);
            android.content.res.Resources a11 = this.getResources();
            Object[] a12 = new Object[1];
            a12[0] = Integer.valueOf(30);
            this.timerMessage = a11.getString(R.string.update_installation_will_begin_in, a12);
            this.mInfoText.setVisibility(0);
            this.mInfoText.setText((CharSequence)this.timerMessage);
            String s3 = this.getResources().getString(R.string.update_navdy_will_upgrade_from_to);
            Object[] a13 = new Object[2];
            a13[0] = this.updateTargetVersion;
            a13[1] = s;
            String s4 = String.format(s3, a13);
            this.mSummaryText.setVisibility(0);
            this.mSummaryText.setText((CharSequence)s4);
            this.mIcon.setImageResource(R.drawable.icon_software_update);
            java.util.ArrayList a14 = new java.util.ArrayList();
            ((java.util.List)a14).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.install), 1));
            ((java.util.List)a14).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.shutdown), 0));
            ((java.util.List)a14).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.getContext().getString(R.string.dont_shutdown), 2));
            this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a14, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
            this.swipeLeftAction = 1;
            this.swipeRightAction = 2;
            this.gesturesEnabled = true;
            this.mRightSwipe.setVisibility(0);
            this.mLefttSwipe.setVisibility(0);
            this.startCountDown();
        }
        com.navdy.hud.app.util.ViewUtil.autosize(this.mMainTitleText, 2, 360, R.array.title_sizes);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        boolean b0 = this.gesturesEnabled;
        label0: {
            label1: {
                if (!b0) {
                    break label1;
                }
                if (a.gesture == null) {
                    break label1;
                }
                switch(com.navdy.hud.app.view.ShutDownConfirmationView$7.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()]) {
                    case 2: {
                        if (this.swipeRightAction == -1) {
                            break;
                        }
                        this.executeItem(0, this.swipeRightAction);
                        b = true;
                        break label0;
                    }
                    case 1: {
                        if (this.swipeLeftAction == -1) {
                            break;
                        }
                        this.executeItem(0, this.swipeLeftAction);
                        b = true;
                        break label0;
                    }
                }
            }
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        com.navdy.hud.app.ui.component.ChoiceLayout a0 = this.mChoiceLayout;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.mChoiceLayout.getVisibility() == 0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            switch(com.navdy.hud.app.view.ShutDownConfirmationView$7.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 5: {
                    sLogger.v("power btn click");
                    this.cancelTimeout();
                    this.mPresenter.finish();
                    b = true;
                    break;
                }
                case 4: {
                    sLogger.v("power btn dlb-click");
                    this.cancelTimeout();
                    this.mPresenter.finish();
                    b = true;
                    break;
                }
                case 3: {
                    this.cancelTimeout();
                    this.mChoiceLayout.executeSelectedItem(true);
                    b = true;
                    break;
                }
                case 2: {
                    this.startCountDown();
                    this.mChoiceLayout.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.startCountDown();
                    this.mChoiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = true;
                }
            }
        }
        return b;
    }
    
    public void setShutdownCause(com.navdy.hud.app.event.Shutdown$Reason a) {
        sLogger.d(new StringBuilder().append("Update is ").append((this.mIsUpdateAvailable) ? "" : "not ").append("available, shut down cause:").append(a).toString());
        if (this.mIsUpdateAvailable) {
            this.mInfoText.setVisibility(0);
        } else {
            switch(com.navdy.hud.app.view.ShutDownConfirmationView$7.$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason[a.ordinal()]) {
                case 1: case 2: case 3: {
                    this.mInfoText.setVisibility(0);
                    this.mInfoText.setText((CharSequence)this.getResources().getString(R.string.navdy_inactivity));
                    break;
                }
                default: {
                    this.mInfoText.setVisibility(8);
                }
            }
        }
    }
}
