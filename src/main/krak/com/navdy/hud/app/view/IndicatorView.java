package com.navdy.hud.app.view;

public class IndicatorView extends android.view.View {
    private android.graphics.Paint indicatorPaint;
    private android.graphics.Path indicatorPath;
    private float mCurveRadius;
    private int mIndicatorHeight;
    private com.navdy.hud.app.util.CustomDimension mIndicatorHeightAttribute;
    private float mIndicatorStrokeWidth;
    private int mIndicatorWidth;
    private com.navdy.hud.app.util.CustomDimension mIndicatorWidthAttribute;
    private float mValue;
    
    public IndicatorView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public IndicatorView(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.initFromAttributes(a, a0);
    }
    
    public static void drawIndicatorPath(android.graphics.Path a, float f, float f0, float f1, float f2, float f3, float f4) {
        com.navdy.hud.app.view.IndicatorView.drawIndicatorPath(a, 0.5f, f, f0, f1, f2, f3, f4);
    }
    
    public static void drawIndicatorPath(android.graphics.Path a, float f, float f0, float f1, float f2, float f3, float f4, float f5) {
        a.moveTo(f0, f1);
        double d = Math.atan((double)(f4 / (f3 / 2f)));
        float f6 = (float)(Math.sin(d) * (double)f2);
        float f7 = (float)(Math.cos(d) * (double)f2);
        float f8 = f3 / 2f;
        float f9 = f0 + f8 + (f5 - f3) * f;
        a.lineTo(f9 - f8 - f2, f1);
        a.quadTo(f9 - f8, f1, f9 - f8 + f7, f1 - f6);
        a.lineTo(f9, f1 - f4);
        a.lineTo(f9 + f8 - f7, f1 - f6);
        a.quadTo(f9 + f8, f1, f9 + f8 + f2, f1);
        a.lineTo(f0 + f5, f1);
    }
    
    private void initDrawingTools() {
        this.indicatorPath = new android.graphics.Path();
        this.indicatorPaint = new android.graphics.Paint();
        this.indicatorPaint.setColor(-1);
        this.indicatorPaint.setAntiAlias(true);
        this.indicatorPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.indicatorPaint.setStrokeWidth(this.mIndicatorStrokeWidth);
        int i = this.getPaddingLeft();
        float f = (float)i;
        float f0 = (float)(this.getWidth() - this.getPaddingRight());
        android.graphics.Shader$TileMode a = android.graphics.Shader$TileMode.CLAMP;
        int[] a0 = new int[4];
        a0[0] = 0;
        a0[1] = -1;
        a0[2] = -1;
        a0[3] = 0;
        float[] a1 = new float[4];
        a1[0] = 0.0f;
        a1[1] = 0.45f;
        a1[2] = 0.55f;
        a1[3] = 1f;
        android.graphics.LinearGradient a2 = new android.graphics.LinearGradient(f, 0.0f, f0, 0.0f, a0, a1, a);
        this.indicatorPaint.setShader((android.graphics.Shader)a2);
        com.navdy.hud.app.view.IndicatorView.drawIndicatorPath(this.indicatorPath, this.mValue, (float)i, (float)this.getHeight() - this.mIndicatorStrokeWidth, this.mCurveRadius, (float)this.mIndicatorWidth, (float)this.mIndicatorHeight, (float)(this.getWidth() - this.getPaddingRight() - this.getPaddingLeft()));
    }
    
    private void initFromAttributes(android.content.Context a, android.util.AttributeSet a0) {
        android.content.res.TypedArray a1 = a.getTheme().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.Indicator, 0, 0);
        try {
            this.mIndicatorWidthAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 0, 0.0f);
            this.mIndicatorHeightAttribute = com.navdy.hud.app.util.CustomDimension.getDimension((android.view.View)this, a1, 1, 0.0f);
            this.mCurveRadius = a1.getDimension(2, 10f);
            this.mIndicatorStrokeWidth = a1.getDimension(3, 4f);
            this.mValue = a1.getFloat(4, 0.5f);
        } catch(Throwable a2) {
            a1.recycle();
            throw a2;
        }
        a1.recycle();
    }
    
    public void evaluateDimensions(int i, int i0) {
        this.mIndicatorHeight = (int)this.mIndicatorHeightAttribute.getSize((android.view.View)this, (float)i, 0.0f);
        this.mIndicatorWidth = (int)this.mIndicatorWidthAttribute.getSize((android.view.View)this, (float)i, 0.0f);
    }
    
    public float getCurveRadius() {
        return this.mCurveRadius;
    }
    
    public int getIndicatorHeight() {
        return this.mIndicatorHeight;
    }
    
    public int getIndicatorWidth() {
        return this.mIndicatorWidth;
    }
    
    public com.navdy.hud.app.util.CustomDimension getIndicatorWidthAttribute() {
        return this.mIndicatorWidthAttribute;
    }
    
    public float getStrokeWidth() {
        return this.mIndicatorStrokeWidth;
    }
    
    public float getValue() {
        return this.mValue;
    }
    
    protected void onDraw(android.graphics.Canvas a) {
        a.drawPath(this.indicatorPath, this.indicatorPaint);
    }
    
    protected void onSizeChanged(int i, int i0, int i1, int i2) {
        this.evaluateDimensions(i, i0);
        this.initDrawingTools();
    }
    
    public void setValue(float f) {
        if (f != this.mValue) {
            this.mValue = f;
            this.initDrawingTools();
            this.invalidate();
        }
    }
}
