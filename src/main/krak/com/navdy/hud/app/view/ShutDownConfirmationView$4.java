package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

class ShutDownConfirmationView$4 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.view.ShutDownConfirmationView this$0;
    
    ShutDownConfirmationView$4(com.navdy.hud.app.view.ShutDownConfirmationView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        int i = ((Integer)a.getAnimatedValue()).intValue();
        android.content.res.Resources a0 = this.this$0.getResources();
        Object[] a1 = new Object[1];
        a1[0] = Integer.valueOf(i);
        String s = a0.getString(R.string.dial_update_installation_will_begin_in, a1);
        this.this$0.mInfoText.setText((CharSequence)s);
    }
}
