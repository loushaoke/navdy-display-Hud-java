package com.navdy.hud.app.view;

class MainView$4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.view.MainView this$0;
    final String val$notifId;
    final com.navdy.hud.app.framework.notifications.NotificationType val$type;
    
    MainView$4(com.navdy.hud.app.view.MainView a, String s, com.navdy.hud.app.framework.notifications.NotificationType a0) {
        super();
        this.this$0 = a;
        this.val$notifId = s;
        this.val$type = a0;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        this.this$0.uiStateManager.postNotificationAnimationEvent(false, this.val$notifId, this.val$type, com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND);
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        this.this$0.uiStateManager.postNotificationAnimationEvent(true, this.val$notifId, this.val$type, com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND);
    }
}
