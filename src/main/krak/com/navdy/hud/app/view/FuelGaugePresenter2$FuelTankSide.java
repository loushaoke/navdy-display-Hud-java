package com.navdy.hud.app.view;


public enum FuelGaugePresenter2$FuelTankSide {
    UNKNOWN(0),
    LEFT(1),
    RIGHT(2);

    private int value;
    FuelGaugePresenter2$FuelTankSide(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class FuelGaugePresenter2$FuelTankSide extends Enum {
//    final private static com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide[] $VALUES;
//    final public static com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide LEFT;
//    final public static com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide RIGHT;
//    final public static com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide UNKNOWN;
//    
//    static {
//        UNKNOWN = new com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide("UNKNOWN", 0);
//        LEFT = new com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide("LEFT", 1);
//        RIGHT = new com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide("RIGHT", 2);
//        com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide[] a = new com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide[3];
//        a[0] = UNKNOWN;
//        a[1] = LEFT;
//        a[2] = RIGHT;
//        $VALUES = a;
//    }
//    
//    private FuelGaugePresenter2$FuelTankSide(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide valueOf(String s) {
//        return (com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide)Enum.valueOf(com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide.class, s);
//    }
//    
//    public static com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide[] values() {
//        return $VALUES.clone();
//    }
//}
//