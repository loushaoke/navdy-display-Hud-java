package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class GestureVideoCaptureView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener, com.navdy.hud.app.manager.InputManager$IInputHandler {
    final public static int TAG_EXIT = 0;
    final public static int TAG_NEXT = 1;
    final private static long UPLOAD_TIMEOUT = 300000L;
    final public static com.navdy.service.library.log.Logger sLogger;
    @Inject
    com.squareup.otto.Bus bus;
    private java.util.List emptyChoices;
    private java.util.List exitChoices;
    @Inject
    com.navdy.hud.app.gesture.GestureServiceConnector gestureService;
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @Inject
    com.navdy.hud.app.screen.GestureLearningScreen$Presenter mPresenter;
    private android.os.Handler mainHandler;
    @InjectView(R.id.image)
    android.widget.ImageView mainImageView;
    @InjectView(R.id.title1)
    android.widget.TextView mainText;
    private java.util.List nextAndExitChoices;
    private com.navdy.hud.app.view.GestureVideoCaptureView$State prevState;
    private com.navdy.hud.app.view.GestureVideoCaptureView$RecordingSavingContext recordingToSave;
    private java.util.Set recordingsToUpload;
    private boolean registered;
    @InjectView(R.id.title3)
    android.widget.TextView secondaryText;
    private java.util.concurrent.ExecutorService serialExecutor;
    private String sessionName;
    @InjectView(R.id.sideImage)
    android.widget.ImageView sideImageView;
    private com.navdy.hud.app.view.GestureVideoCaptureView$State state;
    private Runnable uploadTimeoutRunnable;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.GestureVideoCaptureView.class);
    }
    
    public GestureVideoCaptureView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public GestureVideoCaptureView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, -1);
    }
    
    public GestureVideoCaptureView(android.content.Context a, android.util.AttributeSet a0, int i) {
        this(a, a0, i, -1);
    }
    
    public GestureVideoCaptureView(android.content.Context a, android.util.AttributeSet a0, int i, int i0) {
        super(a, a0, i, i0);
        this.registered = false;
        this.recordingsToUpload = (java.util.Set)new java.util.HashSet();
        this.emptyChoices = java.util.Collections.EMPTY_LIST;
        this.mainHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
        this.serialExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
        this.nextAndExitChoices = (java.util.List)new java.util.ArrayList();
        this.nextAndExitChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.next_recording_step), 1));
        this.nextAndExitChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.exit_recording), 0));
        this.exitChoices = (java.util.List)new java.util.ArrayList();
        this.exitChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.exit_recording), 0));
    }
    
    static String access$000(com.navdy.hud.app.view.GestureVideoCaptureView a) {
        return a.sessionName;
    }
    
    static void access$100(com.navdy.hud.app.view.GestureVideoCaptureView a, com.navdy.hud.app.view.GestureVideoCaptureView$State a0) {
        a.setState(a0);
    }
    
    private java.util.List choicesForState(com.navdy.hud.app.view.GestureVideoCaptureView$State a) {
        java.util.List a0 = null;
        switch(com.navdy.hud.app.view.GestureVideoCaptureView$3.$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[a.ordinal()]) {
            case 5: case 6: case 7: case 8: {
                a0 = this.exitChoices;
                break;
            }
            case 1: case 2: case 3: case 4: {
                a0 = this.nextAndExitChoices;
                break;
            }
            default: {
                a0 = this.emptyChoices;
            }
        }
        return a0;
    }
    
    private void createSession() {
        this.gestureService.setCalibrationEnabled(false);
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        if (a.isDefaultProfile()) {
            this.setState(com.navdy.hud.app.view.GestureVideoCaptureView$State.NOT_PAIRED);
        } else {
            long j = System.currentTimeMillis();
            java.text.SimpleDateFormat a0 = new java.text.SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            this.sessionName = new StringBuilder().append(a.getDriverEmail()).append("-").append(((java.text.DateFormat)a0).format(new java.util.Date(j))).toString();
            this.setState(com.navdy.hud.app.view.GestureVideoCaptureView$State.SWIPE_LEFT);
        }
    }
    
    private com.navdy.hud.app.view.GestureVideoCaptureView$State nextState() {
        int i = com.navdy.hud.app.view.GestureVideoCaptureView$3.$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[this.state.ordinal()];
        com.navdy.hud.app.view.GestureVideoCaptureView$State a = null;
        label0: switch(i) {
            case 9: {
                switch(com.navdy.hud.app.view.GestureVideoCaptureView$3.$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[this.prevState.ordinal()]) {
                    case 4: {
                        a = com.navdy.hud.app.view.GestureVideoCaptureView$State.UPLOADING;
                        break label0;
                    }
                    case 3: {
                        a = com.navdy.hud.app.view.GestureVideoCaptureView$State.DOUBLE_TAP_2;
                        break label0;
                    }
                    case 2: {
                        a = com.navdy.hud.app.view.GestureVideoCaptureView$State.DOUBLE_TAP_1;
                        break label0;
                    }
                    case 1: {
                        a = com.navdy.hud.app.view.GestureVideoCaptureView$State.SWIPE_RIGHT;
                        break label0;
                    }
                    default: {
                        a = null;
                        break label0;
                    }
                }
            }
            case 1: case 2: case 3: case 4: {
                a = com.navdy.hud.app.view.GestureVideoCaptureView$State.SAVING;
                break;
            }
            default: {
                a = null;
                break;
            }
            case 5: case 6: case 7: case 8: {
            }
        }
        return a;
    }
    
    private void resetSession() {
        this.gestureService.setCalibrationEnabled(true);
        this.recordingToSave = null;
        this.recordingsToUpload.clear();
        if (this.uploadTimeoutRunnable != null) {
            this.mainHandler.removeCallbacks(this.uploadTimeoutRunnable);
            this.uploadTimeoutRunnable = null;
        }
    }
    
    private void setState(com.navdy.hud.app.view.GestureVideoCaptureView$State a) {
        if (this.state != null) {
            switch(com.navdy.hud.app.view.GestureVideoCaptureView$3.$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[this.state.ordinal()]) {
                case 1: case 2: case 3: case 4: {
                    this.stopRecording();
                    break;
                }
            }
        }
        this.prevState = this.state;
        this.state = a;
        if (this.state != com.navdy.hud.app.view.GestureVideoCaptureView$State.SAVING) {
            this.mainText.setText(this.state.mainText);
            if (this.state.detailText == 0) {
                this.secondaryText.setText((CharSequence)"");
            } else {
                this.secondaryText.setText(this.state.detailText);
            }
        } else {
            this.secondaryText.setText(this.state.detailText);
        }
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, this.choicesForState(this.state), 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
        switch(com.navdy.hud.app.view.GestureVideoCaptureView$3.$SwitchMap$com$navdy$hud$app$view$GestureVideoCaptureView$State[this.state.ordinal()]) {
            case 5: {
                if (this.recordingsToUpload.isEmpty()) {
                    this.mainHandler.post((Runnable)new com.navdy.hud.app.view.GestureVideoCaptureView$1(this));
                    break;
                } else {
                    this.uploadTimeoutRunnable = (Runnable)new com.navdy.hud.app.view.GestureVideoCaptureView$UploadTimeoutRunnable(this, this.sessionName);
                    this.mainHandler.postDelayed(this.uploadTimeoutRunnable, 300000L);
                    break;
                }
            }
            case 1: case 2: case 3: case 4: {
                this.startRecording();
                break;
            }
        }
    }
    
    private void startRecording() {
        sLogger.d("Start recording");
        this.recordingToSave = new com.navdy.hud.app.view.GestureVideoCaptureView$RecordingSavingContext(this, this.sessionName, this.state);
        this.gestureService.startRecordingVideo();
    }
    
    private void stopRecording() {
        sLogger.d(new StringBuilder().append("Stop recording for ").append(this.state.filename).toString());
        this.gestureService.stopRecordingVideo();
    }
    
    public void executeItem(int i, int i0) {
        switch(i0) {
            case 1: {
                this.setState(this.nextState());
                break;
            }
            case 0: {
                this.mPresenter.hideCaptureView();
                break;
            }
        }
    }
    
    public void itemSelected(int i, int i0) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.sideImageView.setVisibility(0);
        this.mainImageView.setVisibility(0);
        this.mainImageView.setImageResource(R.drawable.icon_settings_learning_to_gesture_gray);
        this.findViewById(R.id.title2).setVisibility(8);
        this.findViewById(R.id.title4).setVisibility(8);
        this.secondaryText.setSingleLine(false);
        this.secondaryText.setTextSize(1, 17f);
        this.mChoiceLayout.setHighlightPersistent(true);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        switch(com.navdy.hud.app.view.GestureVideoCaptureView$3.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 3: {
                this.mChoiceLayout.executeSelectedItem(true);
                break;
            }
            case 2: {
                this.mChoiceLayout.moveSelectionRight();
                break;
            }
            case 1: {
                this.mChoiceLayout.moveSelectionLeft();
                break;
            }
        }
        return false;
    }
    
    public void onRecordingSaved(com.navdy.hud.app.gesture.GestureServiceConnector$RecordingSaved a) {
        sLogger.d("Recording saved");
        com.navdy.hud.app.view.GestureVideoCaptureView$RecordingSavingContext a0 = this.recordingToSave;
        if (this.recordingToSave != null) {
            if (this.recordingToSave.sessionName.equals(this.sessionName)) {
                this.recordingToSave = null;
                this.setState(this.nextState());
                String s = com.navdy.hud.app.storage.PathManager.getInstance().getGestureVideosSyncFolder();
                String s0 = new StringBuilder().append(s).append(java.io.File.separator).append(a0.sessionName).toString();
                sLogger.d(new StringBuilder().append("Session Path : ").append(s0).toString());
                sLogger.d(new StringBuilder().append("Video archive name to be saved : ").append(a0.state.filename).toString());
                String s1 = new StringBuilder().append(s0).append(java.io.File.separator).append(a0.state.filename).append(".zip").toString();
                this.recordingsToUpload.add(s1);
                String s2 = this.sessionName;
                this.serialExecutor.submit((Runnable)new com.navdy.hud.app.view.GestureVideoCaptureView$2(this, s0, a, s1, s2));
            } else {
                sLogger.d(new StringBuilder().append("Recording session ").append(this.recordingToSave.sessionName).append(" doesn't match current session ").append(this.sessionName).toString());
                sLogger.d(new StringBuilder().append("Ignore recording ").append(a.path).toString());
            }
        } else {
            sLogger.d(new StringBuilder().append("Ignore recording ").append(a.path).append(" due to missing context.").toString());
        }
    }
    
    public void onUploadFinished(com.navdy.hud.app.service.S3FileUploadService$UploadFinished a) {
        if (a.userTag != null && a.userTag.equals(this.sessionName)) {
            if (a.succeeded) {
                this.recordingsToUpload.remove(a.filePath);
                sLogger.d(new StringBuilder().append("Remaining files to be uploaded: ").append(this.recordingsToUpload.size()).toString());
                if (this.recordingsToUpload.isEmpty() && this.state == com.navdy.hud.app.view.GestureVideoCaptureView$State.UPLOADING) {
                    if (this.uploadTimeoutRunnable != null) {
                        this.mainHandler.removeCallbacks(this.uploadTimeoutRunnable);
                        this.uploadTimeoutRunnable = null;
                    }
                    this.setState(com.navdy.hud.app.view.GestureVideoCaptureView$State.ALL_DONE);
                }
            } else {
                sLogger.d(new StringBuilder().append("Failed to upload: ").append(a.filePath).toString());
                this.setState(com.navdy.hud.app.view.GestureVideoCaptureView$State.UPLOADING_FAILURE);
            }
        }
    }
    
    public void setVisibility(int i) {
        super.setVisibility(i);
        if (i != 0) {
            this.gestureService.stopRecordingVideo();
            this.resetSession();
            this.gestureService.setRecordMode(false);
            if (this.registered) {
                this.bus.unregister(this);
                this.registered = false;
            }
        } else {
            this.gestureService.setRecordMode(true);
            this.createSession();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
            }
        }
    }
}
