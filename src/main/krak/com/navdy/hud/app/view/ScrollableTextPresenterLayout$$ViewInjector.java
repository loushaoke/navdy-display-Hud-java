package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class ScrollableTextPresenterLayout$$ViewInjector {
    public ScrollableTextPresenterLayout$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.ScrollableTextPresenterLayout a0, Object a1) {
        a0.mMainTitleText = (android.widget.TextView)a.findRequiredView(a1, R.id.mainTitle, "field 'mMainTitleText'");
        a0.mMainImageView = (android.widget.ImageView)a.findRequiredView(a1, R.id.mainImage, "field 'mMainImageView'");
        a0.mObservableScrollView = (com.navdy.hud.app.view.ObservableScrollView)a.findRequiredView(a1, R.id.scrollView, "field 'mObservableScrollView'");
        a0.mTitleText = (android.widget.TextView)a.findRequiredView(a1, R.id.title, "field 'mTitleText'");
        a0.mMessageText = (android.widget.TextView)a.findRequiredView(a1, R.id.message, "field 'mMessageText'");
        a0.topScrub = a.findRequiredView(a1, R.id.topScrub, "field 'topScrub'");
        a0.bottomScrub = a.findRequiredView(a1, R.id.bottomScrub, "field 'bottomScrub'");
        a0.mNotificationIndicator = (com.navdy.hud.app.ui.component.carousel.CarouselIndicator)a.findRequiredView(a1, R.id.notifIndicator, "field 'mNotificationIndicator'");
        a0.mNotificationScrollIndicator = (com.navdy.hud.app.ui.component.carousel.ProgressIndicator)a.findRequiredView(a1, R.id.notifScrollIndicator, "field 'mNotificationScrollIndicator'");
        a0.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout)a.findRequiredView(a1, R.id.choiceLayout, "field 'mChoiceLayout'");
    }
    
    public static void reset(com.navdy.hud.app.view.ScrollableTextPresenterLayout a) {
        a.mMainTitleText = null;
        a.mMainImageView = null;
        a.mObservableScrollView = null;
        a.mTitleText = null;
        a.mMessageText = null;
        a.topScrub = null;
        a.bottomScrub = null;
        a.mNotificationIndicator = null;
        a.mNotificationScrollIndicator = null;
        a.mChoiceLayout = null;
    }
}
