package com.navdy.hud.app.view;

class GestureLearningView$1 implements android.view.ViewTreeObserver$OnGlobalLayoutListener {
    final com.navdy.hud.app.view.GestureLearningView this$0;
    
    GestureLearningView$1(com.navdy.hud.app.view.GestureLearningView a) {
        super();
        this.this$0 = a;
    }
    
    public void onGlobalLayout() {
        com.navdy.hud.app.view.GestureLearningView.access$002(this.this$0, (float)(this.this$0.getWidth() / 2));
        float f = this.this$0.mCenterImage.getY();
        float f0 = (float)(this.this$0.mCenterImage.getHeight() / 2);
        float f1 = (float)(this.this$0.mGestureProgressIndicator.getHeight() / 2);
        this.this$0.mGestureProgressIndicator.setY(f + f0 - f1);
        com.navdy.hud.app.view.GestureLearningView.access$102(this.this$0, com.navdy.hud.app.view.GestureLearningView.access$200(this.this$0) + (float)(this.this$0.mCenterImage.getWidth() / 2) - (float)(this.this$0.mGestureProgressIndicator.getWidth() / 2));
        com.navdy.hud.app.view.GestureLearningView.access$302(this.this$0, (float)(this.this$0.mGestureProgressIndicator.getWidth() / 2));
        this.this$0.getViewTreeObserver().removeOnGlobalLayoutListener((android.view.ViewTreeObserver$OnGlobalLayoutListener)this);
    }
}
