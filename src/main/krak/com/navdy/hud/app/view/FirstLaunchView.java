package com.navdy.hud.app.view;

public class FirstLaunchView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static com.navdy.service.library.log.Logger sLogger;
    @InjectView(R.id.firstLaunchLogo)
    public android.widget.ImageView firstLaunchLogo;
    @Inject
    com.navdy.hud.app.screen.FirstLaunchScreen$Presenter presenter;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.FirstLaunchView.class);
    }
    
    public FirstLaunchView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public FirstLaunchView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public FirstLaunchView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        sLogger.v("onAttachToWindow");
        super.onAttachedToWindow();
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        sLogger.v("onDetachToWindow");
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return true;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return true;
    }
}
