package com.navdy.hud.app.view;

class SpeedometerGaugePresenter2$1 implements Runnable {
    final com.navdy.hud.app.view.SpeedometerGaugePresenter2 this$0;
    
    SpeedometerGaugePresenter2$1(com.navdy.hud.app.view.SpeedometerGaugePresenter2 a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.view.SpeedometerGaugePresenter2.access$000(this.this$0).isRunning()) {
            com.navdy.hud.app.view.SpeedometerGaugePresenter2.access$000(this.this$0).cancel();
        }
        android.animation.ValueAnimator a = com.navdy.hud.app.view.SpeedometerGaugePresenter2.access$000(this.this$0);
        int[] a0 = new int[2];
        a0[0] = 255;
        a0[1] = 0;
        a.setIntValues(a0);
        com.navdy.hud.app.view.SpeedometerGaugePresenter2.access$000(this.this$0).setDuration(1000L);
        com.navdy.hud.app.view.SpeedometerGaugePresenter2.access$000(this.this$0).addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.SpeedometerGaugePresenter2$1$1(this));
        com.navdy.hud.app.view.SpeedometerGaugePresenter2.access$000(this.this$0).addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.SpeedometerGaugePresenter2$1$2(this));
        com.navdy.hud.app.view.SpeedometerGaugePresenter2.access$000(this.this$0).start();
    }
}
