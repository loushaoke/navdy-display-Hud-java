package com.navdy.hud.app.view;

class FuelGaugePresenter2$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$view$FuelGaugePresenter2$FuelTankSide;
    
    static {
        $SwitchMap$com$navdy$hud$app$view$FuelGaugePresenter2$FuelTankSide = new int[com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$view$FuelGaugePresenter2$FuelTankSide;
        com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide a0 = com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide.UNKNOWN;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$FuelGaugePresenter2$FuelTankSide[com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide.LEFT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$FuelGaugePresenter2$FuelTankSide[com.navdy.hud.app.view.FuelGaugePresenter2$FuelTankSide.RIGHT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
    }
}
