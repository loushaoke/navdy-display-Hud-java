package com.navdy.hud.app.view;

class GestureLearningView$2 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.view.GestureLearningView this$0;
    
    GestureLearningView$2(com.navdy.hud.app.view.GestureLearningView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)this.this$0.mTipsScroller.getLayoutParams();
        a0.topMargin = ((Integer)a.getAnimatedValue()).intValue();
        this.this$0.mTipsScroller.setLayoutParams((android.view.ViewGroup$LayoutParams)a0);
    }
}
