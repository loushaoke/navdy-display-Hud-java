package com.navdy.hud.app.view;

class SerialValueAnimator$2 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.view.SerialValueAnimator this$0;
    
    SerialValueAnimator$2(com.navdy.hud.app.view.SerialValueAnimator a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        float f = ((Float)a.getAnimatedValue()).floatValue();
        com.navdy.hud.app.view.SerialValueAnimator.access$000(this.this$0).setValue(f);
    }
}
