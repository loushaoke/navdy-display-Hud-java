package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class DialUpdateConfirmationView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.ChoiceLayout$IListener, com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static int UPDATE_MESSAGE_MAX_WIDTH = 380;
    final private static com.navdy.service.library.log.Logger sLogger;
    private boolean isReminder;
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout mChoiceLayout;
    @InjectView(R.id.image)
    android.widget.ImageView mIcon;
    @InjectView(R.id.title3)
    android.widget.TextView mInfoText;
    @InjectView(R.id.leftSwipe)
    android.widget.ImageView mLefttSwipe;
    @InjectView(R.id.title2)
    android.widget.TextView mMainTitleText;
    @Inject
    com.navdy.hud.app.screen.DialUpdateConfirmationScreen$Presenter mPresenter;
    @InjectView(R.id.rightSwipe)
    android.widget.ImageView mRightSwipe;
    @InjectView(R.id.mainTitle)
    android.widget.TextView mScreenTitleText;
    @InjectView(R.id.title4)
    android.widget.TextView mSummaryText;
    @InjectView(R.id.mainSection)
    android.widget.RelativeLayout mainSection;
    private String updateTargetVersion;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.DialUpdateConfirmationView.class);
    }
    
    public DialUpdateConfirmationView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null, 0);
    }
    
    public DialUpdateConfirmationView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public DialUpdateConfirmationView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.updateTargetVersion = "1.0.60";
        this.isReminder = false;
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    public void executeItem(int i, int i0) {
        switch(i) {
            case 1: {
                this.mPresenter.finish();
                if (!this.isReminder) {
                    break;
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(false, true, this.updateTargetVersion);
                break;
            }
            case 0: {
                this.mPresenter.install();
                if (!this.isReminder) {
                    break;
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordUpdatePrompt(true, true, this.updateTargetVersion);
                break;
            }
        }
    }
    
    public void itemSelected(int i, int i0) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        com.navdy.hud.app.screen.BaseScreen a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
        boolean b = a == null || a.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS;
        com.navdy.service.library.log.Logger a0 = sLogger;
        StringBuilder a1 = new StringBuilder().append("enableNotif:").append(b).append(" screen[");
        Object a2 = (a != null) ? a.getScreen() : "none";
        a0.v(a1.append(a2).append("]").toString());
        if (b) {
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        }
        if (this.mPresenter != null) {
            this.mPresenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        String s = null;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        if (this.mPresenter == null) {
            s = "1.0.1";
        } else {
            this.mPresenter.takeView(this);
            this.isReminder = this.mPresenter.isReminder();
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions a = com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions();
            this.updateTargetVersion = new StringBuilder().append("1.0.").append(a.local.incrementalVersion).toString();
            s = new StringBuilder().append("1.0.").append(a.dial.incrementalVersion).toString();
        }
        this.mScreenTitleText.setVisibility(8);
        this.findViewById(R.id.title1).setVisibility(8);
        this.mMainTitleText.setText(R.string.dial_update_ready_to_install);
        com.navdy.hud.app.util.ViewUtil.adjustPadding((android.view.View)this.mainSection, 0, 0, 0, 10);
        com.navdy.hud.app.util.ViewUtil.autosize(this.mMainTitleText, 2, 380, R.array.title_sizes);
        this.mMainTitleText.setVisibility(0);
        this.mIcon.setImageResource(R.drawable.icon_dial_update);
        this.mInfoText.setText(R.string.dial_update_installation_will_take);
        this.mInfoText.setVisibility(0);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(380);
        android.content.res.Resources a0 = this.getContext().getResources();
        String s0 = a0.getString(R.string.dial_update_will_upgrade_to_from);
        Object[] a1 = new Object[2];
        a1[0] = this.updateTargetVersion;
        a1[1] = s;
        String s1 = String.format(s0, a1);
        this.mSummaryText.setText((CharSequence)s1);
        this.mSummaryText.setVisibility(0);
        java.util.ArrayList a2 = new java.util.ArrayList();
        if (this.isReminder) {
            ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a0.getString(R.string.install_now), 0));
            ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a0.getString(R.string.install_later), 1));
        } else {
            ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a0.getString(R.string.install), 0));
            ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a0.getString(R.string.cancel), 1));
        }
        this.mChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a2, 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)this);
        this.mRightSwipe.setVisibility(0);
        this.mLefttSwipe.setVisibility(0);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        com.navdy.service.library.events.input.Gesture a0 = a.gesture;
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                switch(com.navdy.hud.app.view.DialUpdateConfirmationView$1.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()]) {
                    case 2: {
                        this.executeItem(1, 0);
                        b = true;
                        break label0;
                    }
                    case 1: {
                        this.executeItem(0, 0);
                        b = true;
                        break label0;
                    }
                }
            }
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        com.navdy.hud.app.ui.component.ChoiceLayout a0 = this.mChoiceLayout;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.mChoiceLayout.getVisibility() == 0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            switch(com.navdy.hud.app.view.DialUpdateConfirmationView$1.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.mChoiceLayout.executeSelectedItem(true);
                    b = true;
                    break;
                }
                case 2: {
                    this.mChoiceLayout.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.mChoiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = true;
                }
            }
        }
        return b;
    }
}
