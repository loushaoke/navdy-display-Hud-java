package com.navdy.hud.app.view;

public class SerialValueAnimator {
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener animationListener;
    private com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter mAdapter;
    private boolean mAnimationRunning;
    private android.animation.ValueAnimator mAnimator;
    private int mDuration;
    private java.util.Queue mValueQueue;
    
    public SerialValueAnimator(com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter a, int i) {
        this.animationListener = new com.navdy.hud.app.view.SerialValueAnimator$1(this);
        this.mValueQueue = (java.util.Queue)new java.util.LinkedList();
        this.mAdapter = a;
        this.mDuration = i;
        if (a == null) {
            throw new IllegalArgumentException("SerialValueAnimator cannot run without the adapter");
        }
    }
    
    static com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter access$000(com.navdy.hud.app.view.SerialValueAnimator a) {
        return a.mAdapter;
    }
    
    static java.util.Queue access$100(com.navdy.hud.app.view.SerialValueAnimator a) {
        return a.mValueQueue;
    }
    
    static boolean access$202(com.navdy.hud.app.view.SerialValueAnimator a, boolean b) {
        a.mAnimationRunning = b;
        return b;
    }
    
    static android.animation.ValueAnimator access$302(com.navdy.hud.app.view.SerialValueAnimator a, android.animation.ValueAnimator a0) {
        a.mAnimator = a0;
        return a0;
    }
    
    static void access$400(com.navdy.hud.app.view.SerialValueAnimator a, float f) {
        a.animate(f);
    }
    
    private void animate(float f) {
        float[] a = new float[2];
        a[0] = this.mAdapter.getValue();
        a[1] = f;
        this.mAnimator = android.animation.ValueAnimator.ofFloat(a);
        this.mAnimator.setDuration((long)this.mDuration);
        this.mAnimator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.LinearInterpolator());
        this.mAnimator.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.view.SerialValueAnimator$2(this));
        this.mAnimator.addListener((android.animation.Animator$AnimatorListener)this.animationListener);
        this.mAnimator.start();
    }
    
    public void release() {
        this.mValueQueue.clear();
        if (this.mAnimator != null) {
            this.mAnimator.removeAllListeners();
            this.mAnimator.removeAllUpdateListeners();
            this.mAnimator.cancel();
        }
        this.mAnimator = null;
        this.mAnimationRunning = false;
    }
    
    public void setDuration(int i) {
        this.mDuration = i;
    }
    
    public void setValue(float f) {
        if (this.mAdapter.getValue() == f) {
            this.mAdapter.setValue(f);
            this.mAdapter.animationComplete(f);
        } else if (this.mAnimationRunning) {
            this.mValueQueue.add(Float.valueOf(f));
        } else {
            this.mAnimationRunning = true;
            this.animate(f);
        }
    }
}
