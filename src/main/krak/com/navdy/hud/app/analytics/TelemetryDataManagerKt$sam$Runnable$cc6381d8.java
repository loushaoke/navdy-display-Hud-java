package com.navdy.hud.app.analytics;

final class TelemetryDataManagerKt$sam$Runnable$cc6381d8 implements Runnable {
    final private kotlin.jvm.functions.Function0 function;
    
    TelemetryDataManagerKt$sam$Runnable$cc6381d8(kotlin.jvm.functions.Function0 a) {
        this.function = a;
    }
    
    final public void run() {
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(this.function.invoke(), "invoke(...)");
    }
}
