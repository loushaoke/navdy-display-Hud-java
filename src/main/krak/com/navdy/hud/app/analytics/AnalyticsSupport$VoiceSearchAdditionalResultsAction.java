package com.navdy.hud.app.analytics;


public enum AnalyticsSupport$VoiceSearchAdditionalResultsAction {
    ADDITIONAL_RESULTS_TIMEOUT(0),
    ADDITIONAL_RESULTS_LIST(1),
    ADDITIONAL_RESULTS_GO(2);

    private int value;
    AnalyticsSupport$VoiceSearchAdditionalResultsAction(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class AnalyticsSupport$VoiceSearchAdditionalResultsAction extends Enum {
//    final private static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction[] $VALUES;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction ADDITIONAL_RESULTS_GO;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction ADDITIONAL_RESULTS_LIST;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction ADDITIONAL_RESULTS_TIMEOUT;
//    final public String tag;
//    
//    static {
//        ADDITIONAL_RESULTS_TIMEOUT = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction("ADDITIONAL_RESULTS_TIMEOUT", 0, "timeout");
//        ADDITIONAL_RESULTS_LIST = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction("ADDITIONAL_RESULTS_LIST", 1, "list");
//        ADDITIONAL_RESULTS_GO = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction("ADDITIONAL_RESULTS_GO", 2, "go");
//        com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction[] a = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction[3];
//        a[0] = ADDITIONAL_RESULTS_TIMEOUT;
//        a[1] = ADDITIONAL_RESULTS_LIST;
//        a[2] = ADDITIONAL_RESULTS_GO;
//        $VALUES = a;
//    }
//    
//    private AnalyticsSupport$VoiceSearchAdditionalResultsAction(String s, int i, String s0) {
//        super(s, i);
//        this.tag = s0;
//    }
//    
//    public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction valueOf(String s) {
//        return (com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction)Enum.valueOf(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction.class, s);
//    }
//    
//    public static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction[] values() {
//        return $VALUES.clone();
//    }
//}
//