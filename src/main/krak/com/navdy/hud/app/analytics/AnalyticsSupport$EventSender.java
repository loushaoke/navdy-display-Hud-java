package com.navdy.hud.app.analytics;

class AnalyticsSupport$EventSender implements Runnable {
    private com.navdy.hud.app.analytics.Event event;
    
    AnalyticsSupport$EventSender(com.navdy.hud.app.analytics.Event a) {
        this.event = a;
    }
    
    public void run() {
        com.localytics.android.Localytics.tagEvent(this.event.tag, this.event.argMap);
    }
}
