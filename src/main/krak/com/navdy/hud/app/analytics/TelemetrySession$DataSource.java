package com.navdy.hud.app.analytics;

abstract public interface TelemetrySession$DataSource {
    abstract public long absoluteCurrentTime();
    
    
    abstract public com.navdy.hud.app.analytics.RawSpeed currentSpeed();
    
    
    abstract public void interestingEventDetected(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent arg);
    
    
    abstract public boolean isHighAccuracySpeedAvailable();
    
    
    abstract public boolean isVerboseLoggingNeeded();
    
    
    abstract public long totalDistanceTravelledWithMeters();
}
