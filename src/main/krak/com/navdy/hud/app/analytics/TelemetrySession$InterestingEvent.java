package com.navdy.hud.app.analytics;


    public enum TelemetrySession$InterestingEvent {
        NONE(0),
        HARD_BRAKING(1),
        HARD_ACCELERATION(2),
        HIGH_G_STARTED(3),
        HIGH_G_ENDED(4),
        SPEEDING_STARTED(5),
        SPEEDING_STOPPED(6),
        EXCESSIVE_SPEEDING_STARTED(7),
        EXCESSIVE_SPEEDING_STOPPED(8);

        private int value;
        TelemetrySession$InterestingEvent(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class TelemetrySession$InterestingEvent extends Enum {
//    final private static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent[] $VALUES;
//    final public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent EXCESSIVE_SPEEDING_STARTED;
//    final public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent EXCESSIVE_SPEEDING_STOPPED;
//    final public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent HARD_ACCELERATION;
//    final public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent HARD_BRAKING;
//    final public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent HIGH_G_ENDED;
//    final public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent HIGH_G_STARTED;
//    final public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent NONE;
//    final public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent SPEEDING_STARTED;
//    final public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent SPEEDING_STOPPED;
//    
//    static {
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent("NONE", 0);
//        NONE = a;
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a0 = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent("HARD_BRAKING", 1);
//        HARD_BRAKING = a0;
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a1 = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent("HARD_ACCELERATION", 2);
//        HARD_ACCELERATION = a1;
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a2 = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent("HIGH_G_STARTED", 3);
//        HIGH_G_STARTED = a2;
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a3 = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent("HIGH_G_ENDED", 4);
//        HIGH_G_ENDED = a3;
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a4 = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent("SPEEDING_STARTED", 5);
//        SPEEDING_STARTED = a4;
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a5 = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent("SPEEDING_STOPPED", 6);
//        SPEEDING_STOPPED = a5;
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a6 = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent("EXCESSIVE_SPEEDING_STARTED", 7);
//        EXCESSIVE_SPEEDING_STARTED = a6;
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a7 = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent("EXCESSIVE_SPEEDING_STOPPED", 8);
//        EXCESSIVE_SPEEDING_STOPPED = a7;
//        com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent[] a8 = new com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent[9];
//        a8[0] = a;
//        a8[1] = a0;
//        a8[2] = a1;
//        a8[3] = a2;
//        a8[4] = a3;
//        a8[5] = a4;
//        a8[6] = a5;
//        a8[7] = a6;
//        a8[8] = a7;
//        $VALUES = a8;
//    }
//    
//    protected TelemetrySession$InterestingEvent(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent valueOf(String s) {
//        return (com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent)Enum.valueOf(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.class, s);
//    }
//    
//    public static com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent[] values() {
//        return $VALUES.clone();
//    }
//}
//