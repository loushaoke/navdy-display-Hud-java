package com.navdy.hud.app.analytics;

class Event {
    final public static String TAG = "Localytics";
    final private static com.navdy.service.library.log.Logger sLogger;
    public java.util.Map argMap;
    public String tag;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger("Localytics");
    }
    
    public Event(String s, java.util.Map a) {
        this.tag = s;
        this.argMap = a;
    }
    
    public Event(String s, String[] a) {
        this.tag = s;
        this.argMap = (java.util.Map)new java.util.HashMap();
        int i = 0;
        while(i < a.length - 1) {
            String s0 = a[i + 1];
            java.util.Map a0 = this.argMap;
            String s1 = a[i];
            if (s0 == null) {
                s0 = "";
            }
            a0.put(s1, s0);
            i = i + 2;
        }
        if (i < a.length) {
            sLogger.e(new StringBuilder().append("Odd number of event arguments for tag ").append(s).toString());
        }
    }
    
    String getChangedFields(com.navdy.hud.app.analytics.Event a) {
        StringBuilder a0 = new StringBuilder();
        Object a1 = this.argMap.entrySet().iterator();
        while(((java.util.Iterator)a1).hasNext()) {
            Object a2 = ((java.util.Iterator)a1).next();
            String s = (String)((java.util.Map.Entry)a2).getKey();
            String s0 = (String)a.argMap.get(s);
            String s1 = (String)((java.util.Map.Entry)a2).getValue();
            {
                label0: {
                    label1: {
                        if (s1 == null) {
                            break label1;
                        }
                        if (!s1.equals(s0)) {
                            break label0;
                        }
                    }
                    if (s1 != null) {
                        continue;
                    }
                    if (s0 == null) {
                        continue;
                    }
                }
                if (a0.length() == 0) {
                    a0.append("|");
                }
                a0.append(s);
                a0.append("|");
            }
        }
        return a0.toString();
    }
    
    public String toString() {
        return new StringBuilder().append("Event{tag='").append(this.tag).append((char)39).append(", argMap=").append(this.argMap).append((char)125).toString();
    }
}
