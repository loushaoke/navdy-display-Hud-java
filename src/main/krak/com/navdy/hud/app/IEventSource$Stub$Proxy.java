package com.navdy.hud.app;

class IEventSource$Stub$Proxy implements com.navdy.hud.app.IEventSource {
    private android.os.IBinder mRemote;
    
    IEventSource$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public void addEventListener(com.navdy.hud.app.IEventListener a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.hud.app.IEventSource");
            android.os.IBinder a2 = (a == null) ? null : a.asBinder();
            a0.writeStrongBinder(a2);
            this.mRemote.transact(1, a0, a1, 0);
            a1.readException();
        } catch(Throwable a3) {
            a1.recycle();
            a0.recycle();
            throw a3;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.hud.app.IEventSource";
    }
    
    public void postEvent(byte[] a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.hud.app.IEventSource");
            a0.writeByteArray(a);
            this.mRemote.transact(3, a0, (android.os.Parcel)null, 1);
        } catch(Throwable a1) {
            a0.recycle();
            throw a1;
        }
        a0.recycle();
    }
    
    public void postRemoteEvent(String s, byte[] a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.hud.app.IEventSource");
            a0.writeString(s);
            a0.writeByteArray(a);
            this.mRemote.transact(4, a0, (android.os.Parcel)null, 1);
        } catch(Throwable a1) {
            a0.recycle();
            throw a1;
        }
        a0.recycle();
    }
    
    public void removeEventListener(com.navdy.hud.app.IEventListener a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.hud.app.IEventSource");
            android.os.IBinder a2 = (a == null) ? null : a.asBinder();
            a0.writeStrongBinder(a2);
            this.mRemote.transact(2, a0, a1, 0);
            a1.readException();
        } catch(Throwable a3) {
            a1.recycle();
            a0.recycle();
            throw a3;
        }
        a1.recycle();
        a0.recycle();
    }
}
