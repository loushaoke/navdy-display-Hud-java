package com.navdy.hud.app.debug;

class DebugReceiver$6 implements Runnable {
    final com.navdy.hud.app.debug.DebugReceiver this$0;
    
    DebugReceiver$6(com.navdy.hud.app.debug.DebugReceiver a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        label0: {
            java.net.HttpURLConnection a = null;
            java.io.InputStream a0 = null;
            Throwable a1 = null;
            Throwable a2 = null;
            label3: {
                Throwable a3 = null;
                try {
                    com.navdy.service.library.log.Logger a4 = com.navdy.hud.app.debug.DebugReceiver.sLogger;
                    a = null;
                    a0 = null;
                    a4.v("getGoogleHomePage");
                    a = (java.net.HttpURLConnection)new java.net.URL("https://www.google.com").openConnection();
                    a0 = null;
                    a.setRequestMethod("GET");
                    a0 = a.getInputStream();
                    String s = com.navdy.service.library.util.IOUtils.convertInputStreamToString(a0, "UTF-8");
                    java.util.Map a5 = a.getHeaderFields();
                    Object a6 = a5.keySet().iterator();
                    Object a7 = a5;
                    while(((java.util.Iterator)a6).hasNext()) {
                        String s0 = (String)((java.util.Iterator)a6).next();
                        String s1 = (String)((java.util.List)((java.util.Map)a7).get(s0)).get(0);
                        {
                            if (!"null".equals(s0) && s0 != null) {
                                com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("getGoogleHomePage ").append(s0).append(":  ").append(s1).toString());
                                continue;
                            }
                            com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("getGoogleHomePage ").append(s1).toString());
                        }
                    }
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("getGoogleHomePage ").append(s).toString());
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.v(new StringBuilder().append("getGoogleHomePage LEN = ").append(s.length()).toString());
                } catch(Throwable a8) {
                    a1 = a8;
                    break label3;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                if (a == null) {
                    break label0;
                }
                label2: {
                    try {
                        a.disconnect();
                    } catch(Throwable a9) {
                        a3 = a9;
                        break label2;
                    }
                    break label0;
                }
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e(a3);
                break label0;
            }
            try {
                com.navdy.hud.app.debug.DebugReceiver.sLogger.e("getGoogleHomePage", a1);
            } catch(Throwable a10) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                label1: {
                    Throwable a11 = null;
                    if (a == null) {
                        break label1;
                    }
                    try {
                        a.disconnect();
                        break label1;
                    } catch(Throwable a12) {
                        a11 = a12;
                    }
                    com.navdy.hud.app.debug.DebugReceiver.sLogger.e(a11);
                }
                throw a10;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            if (a == null) {
                break label0;
            }
            try {
                a.disconnect();
                break label0;
            } catch(Throwable a13) {
                a2 = a13;
            }
            com.navdy.hud.app.debug.DebugReceiver.sLogger.e(a2);
        }
    }
}
