package com.navdy.hud.app.debug;

final class GestureEngine$$ViewInjector$1 implements android.widget.CompoundButton$OnCheckedChangeListener {
    final com.navdy.hud.app.debug.GestureEngine val$target;
    
    GestureEngine$$ViewInjector$1(com.navdy.hud.app.debug.GestureEngine a) {
        super();
        this.val$target = a;
    }
    
    public void onCheckedChanged(android.widget.CompoundButton a, boolean b) {
        this.val$target.onToggleGesture(b);
    }
}
