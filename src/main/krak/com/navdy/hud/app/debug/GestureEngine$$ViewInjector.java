package com.navdy.hud.app.debug;
import com.navdy.hud.app.R;

public class GestureEngine$$ViewInjector {
    public GestureEngine$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.debug.GestureEngine a0, Object a1) {
        ((android.widget.CompoundButton)a.findRequiredView(a1, R.id.enable_gesture, "method 'onToggleGesture'")).setOnCheckedChangeListener((android.widget.CompoundButton$OnCheckedChangeListener)new com.navdy.hud.app.debug.GestureEngine$$ViewInjector$1(a0));
        ((android.widget.CompoundButton)a.findRequiredView(a1, R.id.enable_discrete_mode, "method 'onToggleDiscreteMode'")).setOnCheckedChangeListener((android.widget.CompoundButton$OnCheckedChangeListener)new com.navdy.hud.app.debug.GestureEngine$$ViewInjector$2(a0));
        ((android.widget.CompoundButton)a.findRequiredView(a1, R.id.enable_preview, "method 'onTogglePreview'")).setOnCheckedChangeListener((android.widget.CompoundButton$OnCheckedChangeListener)new com.navdy.hud.app.debug.GestureEngine$$ViewInjector$3(a0));
    }
    
    public static void reset(com.navdy.hud.app.debug.GestureEngine a) {
    }
}
