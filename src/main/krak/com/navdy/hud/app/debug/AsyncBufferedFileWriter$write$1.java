package com.navdy.hud.app.debug;

final class AsyncBufferedFileWriter$write$1 implements Runnable {
    final String $data;
    final boolean $forceToDisk;
    final com.navdy.hud.app.debug.AsyncBufferedFileWriter this$0;
    
    AsyncBufferedFileWriter$write$1(com.navdy.hud.app.debug.AsyncBufferedFileWriter a, String s, boolean b) {
        super();
        this.this$0 = a;
        this.$data = s;
        this.$forceToDisk = b;
    }
    
    final public void run() {
        this.this$0.getBufferedWriter().write(this.$data);
        if (this.$forceToDisk) {
            this.this$0.getBufferedWriter().flush();
        }
    }
}
