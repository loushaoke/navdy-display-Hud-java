package com.navdy.hud.app.debug;

class DriveRecorder$5 implements Runnable {
    final com.navdy.hud.app.debug.DriveRecorder this$0;
    
    DriveRecorder$5(com.navdy.hud.app.debug.DriveRecorder a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.debug.DriveRecorder.access$600(this.this$0).size() <= com.navdy.hud.app.debug.DriveRecorder.access$200(this.this$0) + 1) {
            if (com.navdy.hud.app.debug.DriveRecorder.access$700(this.this$0)) {
                com.navdy.hud.app.debug.DriveRecorder.access$202(this.this$0, 0);
                com.navdy.hud.app.debug.DriveRecorder.access$400(this.this$0).post(com.navdy.hud.app.debug.DriveRecorder.access$500(this.this$0));
            } else {
                com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Done playing the recorded data. Stopping the playback");
                this.this$0.stopPlayback();
            }
        } else {
            Object a = com.navdy.hud.app.debug.DriveRecorder.access$600(this.this$0).get(com.navdy.hud.app.debug.DriveRecorder.access$200(this.this$0));
            long j = ((Long)((java.util.Map.Entry)a).getKey()).longValue();
            com.navdy.hud.app.obd.ObdManager.getInstance().injectObdData((java.util.List)((java.util.Map.Entry)a).getValue(), (java.util.List)((java.util.Map.Entry)a).getValue());
            if (com.navdy.hud.app.debug.DriveRecorder.access$600(this.this$0).size() > com.navdy.hud.app.debug.DriveRecorder.access$200(this.this$0) + 1) {
                long j0 = ((Long)((java.util.Map.Entry)com.navdy.hud.app.debug.DriveRecorder.access$600(this.this$0).get(com.navdy.hud.app.debug.DriveRecorder.access$204(this.this$0))).getKey()).longValue();
                com.navdy.hud.app.debug.DriveRecorder.access$400(this.this$0).postDelayed(com.navdy.hud.app.debug.DriveRecorder.access$500(this.this$0), j0 - j);
            }
        }
    }
}
