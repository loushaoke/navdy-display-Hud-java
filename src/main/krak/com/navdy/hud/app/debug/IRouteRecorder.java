package com.navdy.hud.app.debug;

abstract public interface IRouteRecorder extends android.os.IInterface {
    abstract public boolean isPaused();
    
    
    abstract public boolean isPlaying();
    
    
    abstract public boolean isRecording();
    
    
    abstract public boolean isStopped();
    
    
    abstract public void pausePlayback();
    
    
    abstract public void prepare(String arg, boolean arg0);
    
    
    abstract public boolean restartPlayback();
    
    
    abstract public void resumePlayback();
    
    
    abstract public void startPlayback(String arg, boolean arg0, boolean arg1);
    
    
    abstract public String startRecording(String arg);
    
    
    abstract public void stopPlayback();
    
    
    abstract public void stopRecording();
}
