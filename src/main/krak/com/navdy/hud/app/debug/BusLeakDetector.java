package com.navdy.hud.app.debug;

public class BusLeakDetector {
    final private static String EMPTY = "";
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.debug.BusLeakDetector singleton;
    private java.util.HashMap registrationMap;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.debug.BusLeakDetector.class);
        singleton = new com.navdy.hud.app.debug.BusLeakDetector();
    }
    
    public BusLeakDetector() {
        this.registrationMap = new java.util.HashMap(100);
        sLogger.v("::ctor::");
    }
    
    public static com.navdy.hud.app.debug.BusLeakDetector getInstance() {
        return singleton;
    }
    
    public void addReference(Class a) {
        synchronized(this) {
            Integer a0 = (Integer)this.registrationMap.get(a);
            if (a0 != null) {
                this.registrationMap.put(a, Integer.valueOf(a0.intValue() + 1));
            } else {
                this.registrationMap.put(a, Integer.valueOf(1));
            }
        }
        /*monexit(this)*/;
    }
    
    public void printReferences() {
        synchronized(this) {
            com.navdy.service.library.log.Logger a = sLogger;
            a.v(new StringBuilder().append("::printReferences: [").append(this.registrationMap.size()).append("]").toString());
            Object a0 = this.registrationMap.entrySet().iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                String s = null;
                Object a1 = ((java.util.Iterator)a0).next();
                int i = ((Integer)((java.util.Map.Entry)a1).getValue()).intValue();
                switch(i) {
                    case 4: {
                        s = " ********";
                        break;
                    }
                    case 3: {
                        s = " ****";
                        break;
                    }
                    case 2: {
                        s = " **";
                        break;
                    }
                    case 1: {
                        s = "";
                        break;
                    }
                    default: {
                        s = " ****************";
                    }
                }
                sLogger.v(new StringBuilder().append(((Class)((java.util.Map.Entry)a1).getKey()).getName()).append(" = ").append(i).append(s).toString());
            }
        }
        /*monexit(this)*/;
    }
    
    public void removeReference(Class a) {
        synchronized(this) {
            Integer a0 = (Integer)this.registrationMap.get(a);
            if (a0 != null) {
                Integer a1 = Integer.valueOf(a0.intValue() - 1);
                if (a1.intValue() != 0) {
                    this.registrationMap.put(a, a1);
                } else {
                    this.registrationMap.remove(a);
                }
            }
        }
        /*monexit(this)*/;
    }
}
