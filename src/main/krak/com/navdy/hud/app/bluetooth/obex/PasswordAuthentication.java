package com.navdy.hud.app.bluetooth.obex;

final public class PasswordAuthentication {
    final private byte[] mPassword;
    private byte[] mUserName;
    
    public PasswordAuthentication(byte[] a, byte[] a0) {
        if (a != null) {
            this.mUserName = new byte[a.length];
            System.arraycopy(a, 0, this.mUserName, 0, a.length);
        }
        this.mPassword = new byte[a0.length];
        System.arraycopy(a0, 0, this.mPassword, 0, a0.length);
    }
    
    public byte[] getPassword() {
        return this.mPassword;
    }
    
    public byte[] getUserName() {
        return this.mUserName;
    }
}
