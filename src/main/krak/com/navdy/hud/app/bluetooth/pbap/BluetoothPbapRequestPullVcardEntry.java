package com.navdy.hud.app.bluetooth.pbap;

final class BluetoothPbapRequestPullVcardEntry extends com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest {
    final private static String TAG = "BTPbapReqPullVcardEntry";
    final private static String TYPE = "x-bt/vcard";
    final private byte mFormat;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList mResponse;
    
    public BluetoothPbapRequestPullVcardEntry(String s, long j, byte a) {
        this.mHeaderSet.setHeader(1, s);
        this.mHeaderSet.setHeader(66, "x-bt/vcard");
        int i = a;
        int i0 = a;
        i0 = a;
        if (i != 0 && i0 != 1) {
            i0 = 0;
        }
        com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters a0 = new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters();
        if (j != 0L) {
            a0.add((byte)6, j);
        }
        a0.add((byte)7, (byte)i0);
        a0.addToHeaderSet(this.mHeaderSet);
        this.mFormat = (byte)i0;
    }
    
    protected void checkResponseCode(int i) {
        android.util.Log.v("BTPbapReqPullVcardEntry", "checkResponseCode");
        if (this.mResponse.getCount() == 0) {
            if (i != 196 && i != 198) {
                throw new java.io.IOException(new StringBuilder().append("Invalid response received:").append(i).toString());
            }
            android.util.Log.v("BTPbapReqPullVcardEntry", "Vcard Entry not found");
        }
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardEntry getVcard() {
        return this.mResponse.getFirst();
    }
    
    protected void readResponse(java.io.InputStream a) {
        android.util.Log.v("BTPbapReqPullVcardEntry", "readResponse");
        int i = this.mFormat;
        this.mResponse = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapVcardList(a, (byte)i);
    }
}
