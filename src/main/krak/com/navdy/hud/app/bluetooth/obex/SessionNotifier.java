package com.navdy.hud.app.bluetooth.obex;

abstract public interface SessionNotifier {
    abstract public com.navdy.hud.app.bluetooth.obex.ObexSession acceptAndOpen(com.navdy.hud.app.bluetooth.obex.ServerRequestHandler arg);
    
    
    abstract public com.navdy.hud.app.bluetooth.obex.ObexSession acceptAndOpen(com.navdy.hud.app.bluetooth.obex.ServerRequestHandler arg, com.navdy.hud.app.bluetooth.obex.Authenticator arg0);
}
