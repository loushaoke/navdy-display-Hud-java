package com.navdy.hud.app.bluetooth.vcard;

abstract public class VCardParser {
    public VCardParser() {
    }
    
    abstract public void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter arg);
    
    
    abstract public void cancel();
    
    
    abstract public void parse(java.io.InputStream arg);
    
    
    public void parse(java.io.InputStream a, com.navdy.hud.app.bluetooth.vcard.VCardInterpreter a0) {
        this.addInterpreter(a0);
        this.parse(a);
    }
    
    abstract public void parseOne(java.io.InputStream arg);
}
