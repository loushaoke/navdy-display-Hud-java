package com.navdy.hud.app.bluetooth.vcard;

class VCardEntry$ToStringIterator implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator {
    private StringBuilder mBuilder;
    private boolean mFirstElement;
    final com.navdy.hud.app.bluetooth.vcard.VCardEntry this$0;
    
    private VCardEntry$ToStringIterator(com.navdy.hud.app.bluetooth.vcard.VCardEntry a) {
        super();
        this.this$0 = a;
    }
    
    VCardEntry$ToStringIterator(com.navdy.hud.app.bluetooth.vcard.VCardEntry a, com.navdy.hud.app.bluetooth.vcard.VCardEntry$1 a0) {
        this(a);
    }
    
    public boolean onElement(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement a) {
        if (!this.mFirstElement) {
            this.mBuilder.append(", ");
            this.mFirstElement = false;
        }
        this.mBuilder.append("[").append((a).toString()).append("]");
        return true;
    }
    
    public void onElementGroupEnded() {
        this.mBuilder.append("\n");
    }
    
    public void onElementGroupStarted(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel a) {
        this.mBuilder.append(new StringBuilder().append(a.toString()).append(": ").toString());
        this.mFirstElement = true;
    }
    
    public void onIterationEnded() {
        this.mBuilder.append("]]\n");
    }
    
    public void onIterationStarted() {
        this.mBuilder = new StringBuilder();
        this.mBuilder.append(new StringBuilder().append("[[hash: ").append((this.this$0).hashCode()).append("\n").toString());
    }
    
    public String toString() {
        return this.mBuilder.toString();
    }
}
