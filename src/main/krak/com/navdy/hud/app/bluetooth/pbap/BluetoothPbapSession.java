package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapSession implements android.os.Handler$Callback {
    final public static int ACTION_LISTING = 14;
    final public static int ACTION_PHONEBOOK_SIZE = 16;
    final public static int ACTION_VCARD = 15;
    final public static int AUTH_REQUESTED = 8;
    final public static int AUTH_TIMEOUT = 9;
    final private static String PBAP_UUID = "0000112f-0000-1000-8000-00805f9b34fb";
    final public static int REQUEST_COMPLETED = 3;
    final public static int REQUEST_FAILED = 4;
    final private static int RFCOMM_CONNECTED = 1;
    final private static int RFCOMM_FAILED = 2;
    final public static int SESSION_CONNECTED = 6;
    final public static int SESSION_CONNECTING = 5;
    final public static int SESSION_DISCONNECTED = 7;
    final private static String TAG = "BTPbapSession";
    final private android.bluetooth.BluetoothAdapter mAdapter;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession$RfcommConnectThread mConnectThread;
    final private android.bluetooth.BluetoothDevice mDevice;
    final private android.os.HandlerThread mHandlerThread;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession mObexSession;
    final private android.os.Handler mParentHandler;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest mPendingRequest;
    final private android.os.Handler mSessionHandler;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexTransport mTransport;
    
    public BluetoothPbapSession(android.bluetooth.BluetoothDevice a, android.os.Handler a0) {
        this.mPendingRequest = null;
        this.mAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
        if (this.mAdapter == null) {
            throw new NullPointerException("No Bluetooth adapter in the system");
        }
        this.mDevice = a;
        this.mParentHandler = a0;
        this.mConnectThread = null;
        this.mTransport = null;
        this.mObexSession = null;
        this.mHandlerThread = new android.os.HandlerThread("PBAP session handler", 10);
        this.mHandlerThread.start();
        this.mSessionHandler = new android.os.Handler(this.mHandlerThread.getLooper(), (android.os.Handler$Callback)this);
    }
    
    static android.bluetooth.BluetoothAdapter access$000(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession a) {
        return a.mAdapter;
    }
    
    static android.bluetooth.BluetoothDevice access$100(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession a) {
        return a.mDevice;
    }
    
    static android.os.Handler access$200(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession a) {
        return a.mSessionHandler;
    }
    
    private void startObexSession() {
        android.util.Log.d("BTPbapSession", "startObexSession");
        this.mObexSession = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexSession((com.navdy.hud.app.bluetooth.obex.ObexTransport)this.mTransport);
        this.mObexSession.start(this.mSessionHandler);
    }
    
    private void startRfcomm() {
        android.util.Log.d("BTPbapSession", "startRfcomm");
        if (this.mConnectThread == null && this.mObexSession == null) {
            this.mParentHandler.obtainMessage(5).sendToTarget();
            this.mConnectThread = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapSession$RfcommConnectThread(this);
            this.mConnectThread.start();
        }
    }
    
    private void stopObexSession() {
        android.util.Log.d("BTPbapSession", "stopObexSession");
        label1: {
            Throwable a = null;
            label0: {
                try {
                    if (this.mObexSession != null) {
                        this.mObexSession.stop();
                    }
                } catch(Throwable a0) {
                    a = a0;
                    break label0;
                }
                this.mObexSession = null;
                break label1;
            }
            try {
                android.util.Log.e("BTPbapSession", "", a);
            } catch(Throwable a1) {
                this.mObexSession = null;
                throw a1;
            }
            this.mObexSession = null;
        }
    }
    
    private void stopRfcomm() {
        android.util.Log.d("BTPbapSession", "stopRfcomm");
        if (this.mConnectThread != null) {
            try {
                this.mConnectThread.join();
            } catch(Throwable ignoredException) {
            }
            this.mConnectThread = null;
        }
        if (this.mTransport != null) {
            try {
                this.mTransport.close();
            } catch(Throwable ignoredException0) {
            }
            this.mTransport = null;
        }
    }
    
    public void abort() {
        android.util.Log.d("BTPbapSession", "abort");
        if (this.mPendingRequest != null) {
            this.mParentHandler.obtainMessage(4, this.mPendingRequest).sendToTarget();
            this.mPendingRequest = null;
        }
        if (this.mObexSession != null) {
            this.mObexSession.abort();
        }
    }
    
    public boolean handleMessage(android.os.Message a) {
        boolean b = false;
        android.util.Log.d("BTPbapSession", new StringBuilder().append("Handler: msg: ").append(a.what).toString());
        int i = a.what;
        label0: {
            switch(i) {
                case 106: {
                    this.setAuthResponse((String)null);
                    this.mParentHandler.obtainMessage(9).sendToTarget();
                    break;
                }
                case 105: {
                    this.mParentHandler.obtainMessage(8).sendToTarget();
                    this.mSessionHandler.sendMessageDelayed(this.mSessionHandler.obtainMessage(106), 30000L);
                    break;
                }
                case 104: {
                    this.mParentHandler.obtainMessage(4, a.obj).sendToTarget();
                    break;
                }
                case 103: {
                    this.mParentHandler.obtainMessage(3, a.obj).sendToTarget();
                    break;
                }
                case 102: {
                    this.mParentHandler.obtainMessage(7).sendToTarget();
                    this.stopRfcomm();
                    break;
                }
                case 101: {
                    this.stopObexSession();
                    this.mParentHandler.obtainMessage(7).sendToTarget();
                    if (this.mPendingRequest == null) {
                        break;
                    }
                    this.mParentHandler.obtainMessage(4, this.mPendingRequest).sendToTarget();
                    this.mPendingRequest = null;
                    break;
                }
                case 100: {
                    this.mParentHandler.obtainMessage(6).sendToTarget();
                    if (this.mPendingRequest == null) {
                        break;
                    }
                    this.mObexSession.schedule(this.mPendingRequest);
                    this.mPendingRequest = null;
                    break;
                }
                case 2: {
                    this.mConnectThread = null;
                    this.mParentHandler.obtainMessage(7).sendToTarget();
                    if (this.mPendingRequest == null) {
                        break;
                    }
                    this.mParentHandler.obtainMessage(4, this.mPendingRequest).sendToTarget();
                    this.mPendingRequest = null;
                    break;
                }
                case 1: {
                    this.mConnectThread = null;
                    this.mTransport = (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapObexTransport)a.obj;
                    this.startObexSession();
                    break;
                }
                default: {
                    b = false;
                    break label0;
                }
            }
            b = true;
        }
        return b;
    }
    
    public boolean makeRequest(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequest a) {
        boolean b = false;
        android.util.Log.v("BTPbapSession", new StringBuilder().append("makeRequest: ").append((a).getClass().getSimpleName()).toString());
        if (this.mPendingRequest == null) {
            if (this.mObexSession != null) {
                b = this.mObexSession.schedule(a);
            } else {
                this.mPendingRequest = a;
                this.startRfcomm();
                b = true;
            }
        } else {
            android.util.Log.w("BTPbapSession", "makeRequest: request already queued, exiting");
            b = false;
        }
        return b;
    }
    
    public boolean setAuthResponse(String s) {
        android.util.Log.d("BTPbapSession", new StringBuilder().append("setAuthResponse key=").append(s).toString());
        this.mSessionHandler.removeMessages(106);
        return this.mObexSession != null && this.mObexSession.setAuthReply(s);
    }
    
    public void start() {
        android.util.Log.d("BTPbapSession", "start");
        this.startRfcomm();
    }
    
    public void stop() {
        android.util.Log.d("BTPbapSession", "Stop");
        this.stopObexSession();
        this.stopRfcomm();
        if (this.mHandlerThread.isAlive()) {
            boolean b = this.mHandlerThread.quit();
            android.util.Log.d("BTPbapSession", new StringBuilder().append("Stopped handler thread:").append(b).append(" id[").append(System.identityHashCode(this.mHandlerThread)).append("]").toString());
        } else {
            android.util.Log.d("BTPbapSession", new StringBuilder().append("handler thread not running id[").append(System.identityHashCode(this.mHandlerThread)).append("]").toString());
        }
    }
}
