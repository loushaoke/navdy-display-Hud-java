package com.navdy.hud.app.bluetooth.obex;

final public class ApplicationParameter {
    private byte[] mArray;
    private int mLength;
    private int mMaxLength;
    
    public ApplicationParameter() {
        this.mMaxLength = 1000;
        this.mArray = new byte[this.mMaxLength];
        this.mLength = 0;
    }
    
    public void addAPPHeader(byte a, byte a0, byte[] a1) {
        int i = this.mLength + a0 + 2;
        int i0 = this.mMaxLength;
        int i1 = a;
        int i2 = a0;
        if (i > i0) {
            byte[] a2 = new byte[this.mLength + i2 * 4];
            System.arraycopy(this.mArray, 0, a2, 0, this.mLength);
            this.mArray = a2;
            this.mMaxLength = this.mLength + i2 * 4;
        }
        byte[] a3 = this.mArray;
        int i3 = this.mLength;
        this.mLength = i3 + 1;
        int i4 = (byte)i1;
        a3[i3] = (byte)i4;
        byte[] a4 = this.mArray;
        int i5 = this.mLength;
        this.mLength = i5 + 1;
        int i6 = (byte)i2;
        a4[i5] = (byte)i6;
        System.arraycopy(a1, 0, this.mArray, this.mLength, i2);
        this.mLength = this.mLength + i2;
    }
    
    public byte[] getAPPparam() {
        byte[] a = new byte[this.mLength];
        System.arraycopy(this.mArray, 0, a, 0, this.mLength);
        return a;
    }
}
