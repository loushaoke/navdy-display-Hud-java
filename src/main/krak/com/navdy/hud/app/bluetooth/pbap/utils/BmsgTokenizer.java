package com.navdy.hud.app.bluetooth.pbap.utils;

final public class BmsgTokenizer {
    final private java.util.regex.Matcher mMatcher;
    final private int mOffset;
    private int mPos;
    final private String mStr;
    
    public BmsgTokenizer(String s) {
        this(s, 0);
    }
    
    public BmsgTokenizer(String s, int i) {
        this.mPos = 0;
        this.mStr = s;
        this.mOffset = i;
        this.mMatcher = java.util.regex.Pattern.compile("(([^:]*):(.*))?\r\n").matcher((CharSequence)s);
        this.mPos = this.mMatcher.regionStart();
    }
    
    public com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer$Property next() {
        return this.next(false);
    }
    
    public com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer$Property next(boolean b) {
        boolean b0 = false;
        while(true) {
            com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer$Property a = null;
            this.mMatcher.region(this.mPos, this.mMatcher.regionEnd());
            if (this.mMatcher.lookingAt()) {
                this.mPos = this.mMatcher.end();
                if (this.mMatcher.group(1) != null) {
                    b0 = true;
                }
                if (!b0) {
                    continue;
                }
                a = new com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer$Property(this.mMatcher.group(2), this.mMatcher.group(3));
            } else {
                if (!b) {
                    throw new java.text.ParseException("Property or empty line expected", this.pos());
                }
                a = null;
            }
            return a;
        }
    }
    
    public int pos() {
        return this.mPos + this.mOffset;
    }
    
    public String remaining() {
        return this.mStr.substring(this.mPos);
    }
}
