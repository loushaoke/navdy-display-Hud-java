package com.navdy.hud.app.bluetooth.vcard;

public class VCardComposer {
    final private static boolean DEBUG = false;
    final public static String FAILURE_REASON_FAILED_TO_GET_DATABASE_INFO = "Failed to get database information";
    final public static String FAILURE_REASON_NOT_INITIALIZED = "The vCard composer object is not correctly initialized";
    final public static String FAILURE_REASON_NO_ENTRY = "There's no exportable in the database";
    final public static String FAILURE_REASON_UNSUPPORTED_URI = "The Uri vCard composer received is not supported by the composer.";
    final private static String LOG_TAG = "VCardComposer";
    final public static String NO_ERROR = "No error";
    final private static String SHIFT_JIS = "SHIFT_JIS";
    final private static String UTF_8 = "UTF-8";
    final private static String[] sContactsProjection;
    final private static java.util.Map sImMap;
    final private String mCharset;
    final private android.content.ContentResolver mContentResolver;
    private android.net.Uri mContentUriForRawContactsEntity;
    private android.database.Cursor mCursor;
    private boolean mCursorSuppliedFromOutside;
    private String mErrorReason;
    private boolean mFirstVCardEmittedInDoCoMoCase;
    private int mIdColumn;
    private boolean mInitDone;
    final private boolean mIsDoCoMo;
    private com.navdy.hud.app.bluetooth.vcard.VCardPhoneNumberTranslationCallback mPhoneTranslationCallback;
    private boolean mTerminateCalled;
    final private int mVCardType;
    
    static {
        sImMap = (java.util.Map)new java.util.HashMap();
        sImMap.put(Integer.valueOf(0), "X-AIM");
        sImMap.put(Integer.valueOf(1), "X-MSN");
        sImMap.put(Integer.valueOf(2), "X-YAHOO");
        sImMap.put(Integer.valueOf(6), "X-ICQ");
        sImMap.put(Integer.valueOf(7), "X-JABBER");
        sImMap.put(Integer.valueOf(3), "X-SKYPE-USERNAME");
        String[] a = new String[1];
        a[0] = "_id";
        sContactsProjection = a;
    }
    
    public VCardComposer(android.content.Context a) {
        this(a, com.navdy.hud.app.bluetooth.vcard.VCardConfig.VCARD_TYPE_DEFAULT, (String)null, true);
    }
    
    public VCardComposer(android.content.Context a, int i) {
        this(a, i, (String)null, true);
    }
    
    public VCardComposer(android.content.Context a, int i, String s) {
        this(a, i, s, true);
    }
    
    public VCardComposer(android.content.Context a, int i, String s, boolean b) {
        this(a, a.getContentResolver(), i, s, b);
    }
    
    public VCardComposer(android.content.Context a, int i, boolean b) {
        this(a, i, (String)null, b);
    }
    
    public VCardComposer(android.content.Context a, android.content.ContentResolver a0, int i, String s, boolean b) {
        this.mErrorReason = "No error";
        this.mTerminateCalled = true;
        this.mVCardType = i;
        this.mContentResolver = a0;
        this.mIsDoCoMo = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isDoCoMo(i);
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s = "UTF-8";
        }
        boolean b0 = !com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(i) || !"UTF-8".equalsIgnoreCase(s);
        boolean b1 = this.mIsDoCoMo;
        label0: {
            label1: {
                label2: {
                    if (b1) {
                        break label2;
                    }
                    if (!b0) {
                        break label1;
                    }
                }
                if ("SHIFT_JIS".equalsIgnoreCase(s)) {
                    this.mCharset = s;
                    break label0;
                } else if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                    this.mCharset = "SHIFT_JIS";
                    break label0;
                } else {
                    this.mCharset = s;
                    break label0;
                }
            }
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                this.mCharset = "UTF-8";
            } else {
                this.mCharset = s;
            }
        }
        android.util.Log.d("VCardComposer", new StringBuilder().append("Use the charset \"").append(this.mCharset).append("\"").toString());
    }
    
    private void closeCursorIfAppropriate() {
        if (!this.mCursorSuppliedFromOutside && this.mCursor != null) {
            try {
                this.mCursor.close();
            } catch(android.database.sqlite.SQLiteException a) {
                android.util.Log.e("VCardComposer", new StringBuilder().append("SQLiteException on Cursor#close(): ").append(a.getMessage()).toString());
            }
            this.mCursor = null;
        }
    }
    
    private String createOneEntryInternal(String s, java.lang.reflect.Method a) {
        Object a0 = null;
        Throwable a1 = null;
        java.util.HashMap a2 = new java.util.HashMap();
        label1: {
            android.net.Uri a3 = null;
            String[] a4 = null;
            String s0 = null;
            try {
                a0 = null;
                a3 = this.mContentUriForRawContactsEntity;
                a0 = null;
                a4 = new String[1];
            } catch(NullPointerException | OutOfMemoryError a5) {
                a1 = a5;
                break label1;
            }
            a4[0] = s;
            label0: {
                label10: {
                    label5: {
                        label3: try {
                            label9: {
                                IllegalAccessException a6 = null;
                                label6: {
                                    IllegalArgumentException a7 = null;
                                    label8: {
                                        java.lang.reflect.InvocationTargetException a8 = null;
                                        label7: if (a == null) {
                                            a0 = null;
                                            a0 = android.provider.ContactsContract$RawContacts.newEntityIterator(this.mContentResolver.query(a3, (String[])null, "contact_id=?", a4, (String)null));
                                            break label9;
                                        } else {
                                            try {
                                                try {
                                                    try {
                                                        a0 = null;
                                                        Object[] a9 = new Object[5];
                                                        a9[0] = this.mContentResolver;
                                                        a9[1] = a3;
                                                        a9[2] = "contact_id=?";
                                                        a9[3] = a4;
                                                        a9[4] = null;
                                                        a0 = a.invoke(null, a9);
                                                    } catch(IllegalArgumentException a10) {
                                                        a7 = a10;
                                                        break label8;
                                                    }
                                                } catch(java.lang.reflect.InvocationTargetException a11) {
                                                    a8 = a11;
                                                    break label7;
                                                }
                                            } catch(IllegalAccessException a12) {
                                                a6 = a12;
                                                break label6;
                                            }
                                            break label9;
                                        }
                                        a0 = null;
                                        android.util.Log.e("VCardComposer", "InvocationTargetException has been thrown: ", (Throwable)a8);
                                        throw new RuntimeException("InvocationTargetException has been thrown");
                                    }
                                    a0 = null;
                                    android.util.Log.e("VCardComposer", new StringBuilder().append("IllegalArgumentException has been thrown: ").append(a7.getMessage()).toString());
                                    a0 = null;
                                    break label9;
                                }
                                a0 = null;
                                android.util.Log.e("VCardComposer", new StringBuilder().append("IllegalAccessException has been thrown: ").append(a6.getMessage()).toString());
                                a0 = null;
                            }
                            label4: {
                                if (a0 != null) {
                                    break label4;
                                }
                                android.util.Log.e("VCardComposer", "EntityIterator is null");
                                break label5;
                            }
                            boolean b = ((android.content.EntityIterator)a0).hasNext();
                            label2: {
                                if (b) {
                                    break label2;
                                }
                                android.util.Log.w("VCardComposer", new StringBuilder().append("Data does not exist. contactId: ").append(s).toString());
                                break label3;
                            }
                            while(((android.content.EntityIterator)a0).hasNext()) {
                                Object a13 = ((android.content.Entity)((android.content.EntityIterator)a0).next()).getSubValues().iterator();
                                while(((java.util.Iterator)a13).hasNext()) {
                                    android.content.ContentValues a14 = ((android.content.Entity$NamedContentValues)((java.util.Iterator)a13).next()).values;
                                    String s1 = a14.getAsString("mimetype");
                                    if (s1 != null) {
                                        Object a15 = ((java.util.Map)a2).get(s1);
                                        if (a15 == null) {
                                            java.util.ArrayList a16 = new java.util.ArrayList();
                                            ((java.util.Map)a2).put(s1, a16);
                                            a15 = a16;
                                        }
                                        ((java.util.List)a15).add(a14);
                                    }
                                }
                            }
                            break label10;
                        } catch(Throwable a17) {
                            a1 = a17;
                            break label1;
                        }
                        if (a0 == null) {
                            s0 = "";
                            break label0;
                        } else {
                            ((android.content.EntityIterator)a0).close();
                            s0 = "";
                            break label0;
                        }
                    }
                    if (a0 == null) {
                        s0 = "";
                        break label0;
                    } else {
                        ((android.content.EntityIterator)a0).close();
                        s0 = "";
                        break label0;
                    }
                }
                if (a0 != null) {
                    ((android.content.EntityIterator)a0).close();
                }
                s0 = this.buildVCard((java.util.Map)a2);
            }
            return s0;
        }
        if (a0 != null) {
            ((android.content.EntityIterator)a0).close();
        }
        throw a1;
    }
    
    private boolean initInterCursorCreationPart(android.net.Uri a, String[] a0, String s, String[] a1, String s0) {
        boolean b = false;
        this.mCursorSuppliedFromOutside = false;
        this.mCursor = this.mContentResolver.query(a, a0, s, a1, s0);
        if (this.mCursor != null) {
            b = true;
        } else {
            android.util.Log.e("VCardComposer", String.format("Cursor became null unexpectedly", new Object[0]));
            this.mErrorReason = "Failed to get database information";
            b = false;
        }
        return b;
    }
    
    private boolean initInterFirstPart(android.net.Uri a) {
        boolean b = false;
        if (a == null) {
            a = android.provider.ContactsContract$RawContactsEntity.CONTENT_URI;
        }
        this.mContentUriForRawContactsEntity = a;
        if (this.mInitDone) {
            android.util.Log.e("VCardComposer", "init() is already called");
            b = false;
        } else {
            b = true;
        }
        return b;
    }
    
    private boolean initInterLastPart() {
        this.mInitDone = true;
        this.mTerminateCalled = false;
        return true;
    }
    
    private boolean initInterMainPart() {
        boolean b = false;
        int i = this.mCursor.getCount();
        label2: {
            label0: {
                label1: {
                    if (i == 0) {
                        break label1;
                    }
                    if (this.mCursor.moveToFirst()) {
                        break label0;
                    }
                }
                this.closeCursorIfAppropriate();
                b = false;
                break label2;
            }
            this.mIdColumn = this.mCursor.getColumnIndex("_id");
            b = this.mIdColumn >= 0;
        }
        return b;
    }
    
    public String buildVCard(java.util.Map a) {
        String s = null;
        if (a != null) {
            com.navdy.hud.app.bluetooth.vcard.VCardBuilder a0 = new com.navdy.hud.app.bluetooth.vcard.VCardBuilder(this.mVCardType, this.mCharset);
            a0.appendNameProperties((java.util.List)a.get("vnd.android.cursor.item/name")).appendNickNames((java.util.List)a.get("vnd.android.cursor.item/nickname")).appendPhones((java.util.List)a.get("vnd.android.cursor.item/phone_v2"), this.mPhoneTranslationCallback).appendEmails((java.util.List)a.get("vnd.android.cursor.item/email_v2")).appendPostals((java.util.List)a.get("vnd.android.cursor.item/postal-address_v2")).appendOrganizations((java.util.List)a.get("vnd.android.cursor.item/organization")).appendWebsites((java.util.List)a.get("vnd.android.cursor.item/website"));
            if ((this.mVCardType & 8388608) == 0) {
                a0.appendPhotos((java.util.List)a.get("vnd.android.cursor.item/photo"));
            }
            a0.appendNotes((java.util.List)a.get("vnd.android.cursor.item/note")).appendEvents((java.util.List)a.get("vnd.android.cursor.item/contact_event")).appendIms((java.util.List)a.get("vnd.android.cursor.item/im")).appendSipAddresses((java.util.List)a.get("vnd.android.cursor.item/sip_address")).appendRelation((java.util.List)a.get("vnd.android.cursor.item/relation"));
            s = a0.toString();
        } else {
            android.util.Log.e("VCardComposer", "The given map is null. Ignore and return empty String");
            s = "";
        }
        return s;
    }
    
    public String createOneEntry() {
        return this.createOneEntry((java.lang.reflect.Method)null);
    }
    
    public String createOneEntry(java.lang.reflect.Method a) {
        if (this.mIsDoCoMo && !this.mFirstVCardEmittedInDoCoMoCase) {
            this.mFirstVCardEmittedInDoCoMoCase = true;
        }
        String s = this.createOneEntryInternal(this.mCursor.getString(this.mIdColumn), a);
        if (!this.mCursor.moveToNext()) {
            android.util.Log.e("VCardComposer", "Cursor#moveToNext() returned false");
        }
        return s;
    }
    
    protected void finalize() {
        try {
            if (!this.mTerminateCalled) {
                android.util.Log.e("VCardComposer", "finalized() is called before terminate() being called");
            }
        } catch(Throwable a) {
            (this).finalize();
            throw a;
        }
        (this).finalize();
    }
    
    public int getCount() {
        int i = 0;
        if (this.mCursor != null) {
            i = this.mCursor.getCount();
        } else {
            android.util.Log.w("VCardComposer", "This object is not ready yet.");
            i = 0;
        }
        return i;
    }
    
    public String getErrorReason() {
        return this.mErrorReason;
    }
    
    public boolean init() {
        return this.init((String)null, (String[])null);
    }
    
    public boolean init(android.database.Cursor a) {
        boolean b = false;
        if (this.initInterFirstPart((android.net.Uri)null)) {
            this.mCursorSuppliedFromOutside = true;
            this.mCursor = a;
            b = this.initInterMainPart() && this.initInterLastPart();
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean init(android.net.Uri a, String s, String[] a0, String s0) {
        return this.init(a, sContactsProjection, s, a0, s0, (android.net.Uri)null);
    }
    
    public boolean init(android.net.Uri a, String s, String[] a0, String s0, android.net.Uri a1) {
        return this.init(a, sContactsProjection, s, a0, s0, a1);
    }
    
    public boolean init(android.net.Uri a, String[] a0, String s, String[] a1, String s0, android.net.Uri a2) {
        boolean b = false;
        if ("com.android.contacts".equals(a.getAuthority())) {
            b = this.initInterFirstPart(a2) && this.initInterCursorCreationPart(a, a0, s, a1, s0) && this.initInterMainPart() && this.initInterLastPart();
        } else {
            this.mErrorReason = "The Uri vCard composer received is not supported by the composer.";
            b = false;
        }
        return b;
    }
    
    public boolean init(String s, String[] a) {
        return this.init(android.provider.ContactsContract$Contacts.CONTENT_URI, sContactsProjection, s, a, (String)null, (android.net.Uri)null);
    }
    
    public boolean initWithRawContactsEntityUri(android.net.Uri a) {
        return this.init(android.provider.ContactsContract$Contacts.CONTENT_URI, sContactsProjection, (String)null, (String[])null, (String)null, a);
    }
    
    public boolean isAfterLast() {
        boolean b = false;
        if (this.mCursor != null) {
            b = this.mCursor.isAfterLast();
        } else {
            android.util.Log.w("VCardComposer", "This object is not ready yet.");
            b = false;
        }
        return b;
    }
    
    public void setPhoneNumberTranslationCallback(com.navdy.hud.app.bluetooth.vcard.VCardPhoneNumberTranslationCallback a) {
        this.mPhoneTranslationCallback = a;
    }
    
    public void terminate() {
        this.closeCursorIfAppropriate();
        this.mTerminateCalled = true;
    }
}
