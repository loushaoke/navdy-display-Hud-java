package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry {
    final private static int DEFAULT_ORGANIZATION_TYPE = 1;
    final private static String LOG_TAG = "vCard";
    final private static java.util.List sEmptyList;
    final private static java.util.Map sImMap;
    final private android.accounts.Account mAccount;
    private java.util.List mAndroidCustomDataList;
    private com.navdy.hud.app.bluetooth.vcard.VCardEntry$AnniversaryData mAnniversary;
    private com.navdy.hud.app.bluetooth.vcard.VCardEntry$BirthdayData mBirthday;
    private java.util.Date mCallTime;
    private int mCallType;
    private java.util.List mChildren;
    private java.util.List mEmailList;
    private java.util.List mImList;
    final private com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData mNameData;
    private java.util.List mNicknameList;
    private java.util.List mNoteList;
    private java.util.List mOrganizationList;
    private java.util.List mPhoneList;
    private java.util.List mPhotoList;
    private java.util.List mPostalList;
    private java.util.List mSipList;
    final private int mVCardType;
    private java.util.List mWebsiteList;
    
    static {
        sImMap = (java.util.Map)new java.util.HashMap();
        sImMap.put("X-AIM", Integer.valueOf(0));
        sImMap.put("X-MSN", Integer.valueOf(1));
        sImMap.put("X-YAHOO", Integer.valueOf(2));
        sImMap.put("X-ICQ", Integer.valueOf(6));
        sImMap.put("X-JABBER", Integer.valueOf(7));
        sImMap.put("X-SKYPE-USERNAME", Integer.valueOf(3));
        sImMap.put("X-GOOGLE-TALK", Integer.valueOf(5));
        sImMap.put("X-GOOGLE TALK", Integer.valueOf(5));
        sEmptyList = java.util.Collections.unmodifiableList((java.util.List)new java.util.ArrayList(0));
    }
    
    public VCardEntry() {
        this(-1073741824);
    }
    
    public VCardEntry(int i) {
        this(i, (android.accounts.Account)null);
    }
    
    public VCardEntry(int i, android.accounts.Account a) {
        this.mNameData = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData();
        this.mVCardType = i;
        this.mAccount = a;
    }
    
    private void addCallTime(int i, java.util.Date a) {
        this.mCallType = i;
        this.mCallTime = a;
    }
    
    private void addEmail(int i, String s, String s0, boolean b) {
        if (this.mEmailList == null) {
            this.mEmailList = (java.util.List)new java.util.ArrayList();
        }
        this.mEmailList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry$EmailData(s, i, s0, b));
    }
    
    private void addIm(int i, String s, String s0, int i0, boolean b) {
        if (this.mImList == null) {
            this.mImList = (java.util.List)new java.util.ArrayList();
        }
        this.mImList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry$ImData(i, s, s0, i0, b));
    }
    
    private void addNewOrganization(String s, String s0, String s1, String s2, int i, boolean b) {
        if (this.mOrganizationList == null) {
            this.mOrganizationList = (java.util.List)new java.util.ArrayList();
        }
        this.mOrganizationList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData(s, s0, s1, s2, i, b));
    }
    
    private void addNickName(String s) {
        if (this.mNicknameList == null) {
            this.mNicknameList = (java.util.List)new java.util.ArrayList();
        }
        this.mNicknameList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry$NicknameData(s));
    }
    
    private void addNote(String s) {
        if (this.mNoteList == null) {
            this.mNoteList = (java.util.List)new java.util.ArrayList(1);
        }
        this.mNoteList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry$NoteData(s));
    }
    
    private void addPhone(int i, String s, String s0, boolean b) {
        if (this.mPhoneList == null) {
            this.mPhoneList = (java.util.List)new java.util.ArrayList();
        }
        StringBuilder a = new StringBuilder();
        String s1 = s.trim();
        if (i != 6 && !com.navdy.hud.app.bluetooth.vcard.VCardConfig.refrainPhoneNumberFormatting(this.mVCardType)) {
            int i0 = s1.length();
            boolean b0 = false;
            int i1 = 0;
            while(i1 < i0) {
                int i2 = s1.charAt(i1);
                label0: {
                    label5: {
                        label6: {
                            if (i2 == 112) {
                                break label6;
                            }
                            if (i2 != 80) {
                                break label5;
                            }
                        }
                        a.append((char)44);
                        b0 = true;
                        break label0;
                    }
                    label3: {
                        label4: {
                            if (i2 == 119) {
                                break label4;
                            }
                            if (i2 != 87) {
                                break label3;
                            }
                        }
                        a.append((char)59);
                        b0 = true;
                        break label0;
                    }
                    label1: {
                        label2: {
                            if (48 > i2) {
                                break label2;
                            }
                            if (i2 <= 57) {
                                break label1;
                            }
                        }
                        if (i1 != 0) {
                            break label0;
                        }
                        if (i2 != 43) {
                            break label0;
                        }
                    }
                    a.append((char)i2);
                }
                i1 = i1 + 1;
            }
            if (b0) {
                s1 = a.toString();
            } else {
                int i3 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPhoneNumberFormat(this.mVCardType);
                s1 = com.navdy.hud.app.bluetooth.vcard.VCardUtils$PhoneNumberUtilsPort.formatNumber(a.toString(), i3);
            }
        }
        com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhoneData a0 = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhoneData(s1, i, s0, b);
        this.mPhoneList.add(a0);
    }
    
    private void addPhotoBytes(String s, byte[] a, boolean b) {
        if (this.mPhotoList == null) {
            this.mPhotoList = (java.util.List)new java.util.ArrayList(1);
        }
        com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhotoData a0 = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhotoData(s, a, b);
        this.mPhotoList.add(a0);
    }
    
    private void addPostal(int i, java.util.List a, String s, boolean b) {
        if (this.mPostalList == null) {
            this.mPostalList = (java.util.List)new java.util.ArrayList(0);
        }
        this.mPostalList.add(com.navdy.hud.app.bluetooth.vcard.VCardEntry$PostalData.constructPostalData(a, i, s, b, this.mVCardType));
    }
    
    private void addSip(String s, int i, String s0, boolean b) {
        if (this.mSipList == null) {
            this.mSipList = (java.util.List)new java.util.ArrayList();
        }
        this.mSipList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry$SipData(s, i, s0, b));
    }
    
    public static com.navdy.hud.app.bluetooth.vcard.VCardEntry buildFromResolver(android.content.ContentResolver a) {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry.buildFromResolver(a, android.provider.ContactsContract$Contacts.CONTENT_URI);
    }
    
    public static com.navdy.hud.app.bluetooth.vcard.VCardEntry buildFromResolver(android.content.ContentResolver a, android.net.Uri a0) {
        return null;
    }
    
    private String buildSinglePhoneticNameFromSortAsParam(java.util.Map a) {
        String s = null;
        Object a0 = a.get("SORT-AS");
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (((java.util.Collection)a0).size() != 0) {
                        break label0;
                    }
                }
                s = null;
                break label2;
            }
            if (((java.util.Collection)a0).size() > 1) {
                android.util.Log.w("vCard", new StringBuilder().append("Incorrect multiple SORT_AS parameters detected: ").append(java.util.Arrays.toString(((java.util.Collection)a0).toArray())).toString());
            }
            java.util.List a1 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue((String)((java.util.Collection)a0).iterator().next(), this.mVCardType);
            StringBuilder a2 = new StringBuilder();
            Object a3 = a1.iterator();
            while(((java.util.Iterator)a3).hasNext()) {
                a2.append((String)((java.util.Iterator)a3).next());
            }
            s = a2.toString();
        }
        return s;
    }
    
    private String constructDisplayName() {
        String s = null;
        label1: if (android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1300(this.mNameData))) {
            if (this.mNameData.emptyStructuredName()) {
                if (this.mNameData.emptyPhoneticStructuredName()) {
                    java.util.List a = this.mEmailList;
                    label3: {
                        if (a == null) {
                            break label3;
                        }
                        if (this.mEmailList.size() <= 0) {
                            break label3;
                        }
                        s = com.navdy.hud.app.bluetooth.vcard.VCardEntry$EmailData.access$1500((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EmailData)this.mEmailList.get(0));
                        break label1;
                    }
                    java.util.List a0 = this.mPhoneList;
                    label2: {
                        if (a0 == null) {
                            break label2;
                        }
                        if (this.mPhoneList.size() <= 0) {
                            break label2;
                        }
                        s = com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhoneData.access$1600((com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhoneData)this.mPhoneList.get(0));
                        break label1;
                    }
                    java.util.List a1 = this.mPostalList;
                    label0: {
                        if (a1 == null) {
                            break label0;
                        }
                        if (this.mPostalList.size() <= 0) {
                            break label0;
                        }
                        s = ((com.navdy.hud.app.bluetooth.vcard.VCardEntry$PostalData)this.mPostalList.get(0)).getFormattedAddress(this.mVCardType);
                        break label1;
                    }
                    java.util.List a2 = this.mOrganizationList;
                    s = null;
                    if (a2 != null) {
                        int i = this.mOrganizationList.size();
                        s = null;
                        if (i > 0) {
                            s = ((com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData)this.mOrganizationList.get(0)).getFormattedString();
                        }
                    }
                } else {
                    s = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(this.mVCardType, com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$500(this.mNameData), com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$600(this.mNameData), com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$700(this.mNameData));
                }
            } else {
                s = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(this.mVCardType, com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1200(this.mNameData), com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1000(this.mNameData), com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1100(this.mNameData), com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$900(this.mNameData), com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$800(this.mNameData));
            }
        } else {
            s = com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1300(this.mNameData);
        }
        if (s == null) {
            s = "";
        }
        return s;
    }
    
    private void handleAndroidCustomProperty(java.util.List a) {
        if (this.mAndroidCustomDataList == null) {
            this.mAndroidCustomDataList = (java.util.List)new java.util.ArrayList();
        }
        this.mAndroidCustomDataList.add(com.navdy.hud.app.bluetooth.vcard.VCardEntry$AndroidCustomData.constructAndroidCustomData(a));
    }
    
    private void handleNProperty(java.util.List a, java.util.Map a0) {
        this.tryHandleSortAsName(a0);
        if (a != null) {
            int i = a.size();
            if (i >= 1) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a1 = null;
                String s = null;
                if (i > 5) {
                    i = 5;
                }
                switch(i) {
                    case 5: {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$802(this.mNameData, (String)a.get(4));
                    }
                    case 4: {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$902(this.mNameData, (String)a.get(3));
                    }
                    case 3: {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1002(this.mNameData, (String)a.get(2));
                    }
                    case 2: {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1102(this.mNameData, (String)a.get(1));
                    }
                    default: {
                        a1 = this.mNameData;
                        s = (String)a.get(0);
                    }
                }
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1202(a1, s);
            }
        }
    }
    
    private void handleOrgValue(int i, java.util.List a, java.util.Map a0, boolean b) {
        String s = null;
        String s0 = null;
        String s1 = this.buildSinglePhoneticNameFromSortAsParam(a0);
        if (a == null) {
            a = sEmptyList;
        }
        int i0 = a.size();
        switch(i0) {
            case 1: {
                s = (String)a.get(0);
                s0 = null;
                break;
            }
            case 0: {
                s = "";
                s0 = null;
                break;
            }
            default: {
                s = (String)a.get(0);
                StringBuilder a1 = new StringBuilder();
                Object a2 = a;
                int i1 = 1;
                while(i1 < i0) {
                    if (i1 > 1) {
                        a1.append((char)32);
                    }
                    a1.append((String)((java.util.List)a2).get(i1));
                    i1 = i1 + 1;
                }
                s0 = a1.toString();
            }
        }
        if (this.mOrganizationList != null) {
            Object a3 = this.mOrganizationList.iterator();
            while(true) {
                if (((java.util.Iterator)a3).hasNext()) {
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a4 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData)((java.util.Iterator)a3).next();
                    if (com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData.access$100(a4) != null) {
                        continue;
                    }
                    if (com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData.access$200(a4) != null) {
                        continue;
                    }
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData.access$102(a4, s);
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData.access$202(a4, s0);
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData.access$302(a4, b);
                    break;
                } else {
                    this.addNewOrganization(s, s0, (String)null, s1, i, b);
                    break;
                }
            }
        } else {
            this.addNewOrganization(s, s0, (String)null, s1, i, b);
        }
    }
    
    private void handlePhoneticNameFromSound(java.util.List a) {
        label0: if (android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$500(this.mNameData)) && android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$600(this.mNameData)) && android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$700(this.mNameData)) && a != null) {
            int i = a.size();
            if (i >= 1) {
                Object a0 = null;
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a1 = null;
                String s = null;
                if (i > 3) {
                    i = 3;
                }
                int i0 = ((String)a.get(0)).length();
                label1: {
                    boolean b = false;
                    if (i0 <= 0) {
                        a0 = a;
                        break label1;
                    }
                    a0 = a;
                    int i1 = 1;
                    while(true) {
                        if (i1 >= i) {
                            b = true;
                            break;
                        } else {
                            if (((String)((java.util.List)a0).get(i1)).length() <= 0) {
                                i1 = i1 + 1;
                                continue;
                            }
                            b = false;
                            break;
                        }
                    }
                    if (!b) {
                        break label1;
                    }
                    String[] a2 = ((String)((java.util.List)a0).get(0)).split(" ");
                    int i2 = a2.length;
                    if (i2 != 3) {
                        if (i2 != 2) {
                            com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$702(this.mNameData, (String)((java.util.List)a0).get(0));
                            break label0;
                        } else {
                            com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$502(this.mNameData, a2[0]);
                            com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$702(this.mNameData, a2[1]);
                            break label0;
                        }
                    } else {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$502(this.mNameData, a2[0]);
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$602(this.mNameData, a2[1]);
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$702(this.mNameData, a2[2]);
                        break label0;
                    }
                }
                switch(i) {
                    case 3: {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$602(this.mNameData, (String)((java.util.List)a0).get(2));
                    }
                    case 2: {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$702(this.mNameData, (String)((java.util.List)a0).get(1));
                    }
                    default: {
                        a1 = this.mNameData;
                        s = (String)((java.util.List)a0).get(0);
                    }
                }
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$502(a1, s);
            }
        }
    }
    
    private void handleSipCase(String s, java.util.Collection a) {
        label0: if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            boolean b = false;
            int i = 0;
            boolean b0 = s.startsWith("sip:");
            label1: {
                if (!b0) {
                    break label1;
                }
                String s0 = s.substring(4);
                if (s0.length() == 0) {
                    break label0;
                }
                s = s0;
            }
            String s1 = null;
            if (a == null) {
                b = false;
                i = -1;
            } else {
                java.util.Iterator a0 = a.iterator();
                s1 = null;
                i = -1;
                b = false;
                Object a1 = a0;
                while(((java.util.Iterator)a1).hasNext()) {
                    String s2 = (String)((java.util.Iterator)a1).next();
                    String s3 = s2.toUpperCase();
                    if (s3.equals("PREF")) {
                        b = true;
                    } else if (s3.equals("HOME")) {
                        i = 1;
                    } else if (s3.equals("WORK")) {
                        i = 2;
                    } else if (i < 0) {
                        if (s3.startsWith("X-")) {
                            s2 = s2.substring(2);
                        }
                        s1 = s2;
                        i = 0;
                    }
                }
            }
            if (i < 0) {
                i = 3;
            }
            this.addSip(s, i, s1, b);
        }
    }
    
    private void handleTitleValue(String s) {
        if (this.mOrganizationList != null) {
            Object a = this.mOrganizationList.iterator();
            while(true) {
                if (((java.util.Iterator)a).hasNext()) {
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData)((java.util.Iterator)a).next();
                    if (com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData.access$400(a0) != null) {
                        continue;
                    }
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$OrganizationData.access$402(a0, s);
                    break;
                } else {
                    this.addNewOrganization((String)null, (String)null, s, (String)null, 1, false);
                    break;
                }
            }
        } else {
            this.addNewOrganization((String)null, (String)null, s, (String)null, 1, false);
        }
    }
    
    private void iterateOneList(java.util.List a, com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator a0) {
        if (a != null && a.size() > 0) {
            a0.onElementGroupStarted(((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement)a.get(0)).getEntryLabel());
            Object a1 = a.iterator();
            Object a2 = a0;
            while(((java.util.Iterator)a1).hasNext()) {
                ((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator)a2).onElement((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement)((java.util.Iterator)a1).next());
            }
            ((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator)a2).onElementGroupEnded();
        }
    }
    
    private String listToString(java.util.List a) {
        String s = null;
        int i = a.size();
        if (i <= 1) {
            s = (i != 1) ? "" : (String)a.get(0);
        } else {
            StringBuilder a0 = new StringBuilder();
            Object a1 = a.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                a0.append((String)((java.util.Iterator)a1).next());
                if (i - 1 < 0) {
                    a0.append(";");
                }
            }
            s = a0.toString();
        }
        return s;
    }
    
    private void tryHandleSortAsName(java.util.Map a) {
        boolean b = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType);
        label0: {
            label1: {
                if (!b) {
                    break label1;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$500(this.mNameData))) {
                    break label0;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$600(this.mNameData))) {
                    break label0;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$700(this.mNameData))) {
                    break label0;
                }
            }
            Object a0 = a.get("SORT-AS");
            if (a0 != null && ((java.util.Collection)a0).size() != 0) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a1 = null;
                String s = null;
                if (((java.util.Collection)a0).size() > 1) {
                    android.util.Log.w("vCard", new StringBuilder().append("Incorrect multiple SORT_AS parameters detected: ").append(java.util.Arrays.toString(((java.util.Collection)a0).toArray())).toString());
                }
                java.util.List a2 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue((String)((java.util.Collection)a0).iterator().next(), this.mVCardType);
                int i = a2.size();
                if (i > 3) {
                    i = 3;
                }
                switch(i) {
                    case 3: {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$602(this.mNameData, (String)a2.get(2));
                    }
                    case 2: {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$702(this.mNameData, (String)a2.get(1));
                    }
                    default: {
                        a1 = this.mNameData;
                        s = (String)a2.get(0);
                    }
                }
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$502(a1, s);
            }
        }
    }
    
    public void addChild(com.navdy.hud.app.bluetooth.vcard.VCardEntry a) {
        if (this.mChildren == null) {
            this.mChildren = (java.util.List)new java.util.ArrayList();
        }
        this.mChildren.add(a);
    }
    
    public void addProperty(com.navdy.hud.app.bluetooth.vcard.VCardProperty a) {
        String s = a.getName();
        java.util.Map a0 = a.getParameterMap();
        java.util.List a1 = a.getValueList();
        byte[] a2 = a.getByteValue();
        label0: {
            label13: {
                label14: {
                    if (a1 == null) {
                        break label14;
                    }
                    if (a1.size() != 0) {
                        break label13;
                    }
                }
                if (a2 == null) {
                    break label0;
                }
            }
            String s0 = (a1 == null) ? null : this.listToString(a1).trim();
            if (!s.equals("VERSION")) {
                if (s.equals("FN")) {
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1302(this.mNameData, s0);
                } else if (s.equals("NAME")) {
                    if (android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1300(this.mNameData))) {
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1302(this.mNameData, s0);
                    }
                } else if (s.equals("N")) {
                    this.handleNProperty(a1, a0);
                } else if (s.equals("SORT-STRING")) {
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$1402(this.mNameData, s0);
                } else {
                    boolean b = s.equals("NICKNAME");
                    label11: {
                        label12: {
                            if (b) {
                                break label12;
                            }
                            if (!s.equals("X-NICKNAME")) {
                                break label11;
                            }
                        }
                        this.addNickName(s0);
                        break label0;
                    }
                    if (s.equals("SOUND")) {
                        Object a3 = a0.get("TYPE");
                        if (a3 != null && ((java.util.Collection)a3).contains("X-IRMC-N")) {
                            this.handlePhoneticNameFromSound(com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue(s0, this.mVCardType));
                        }
                    } else if (s.equals("ADR")) {
                        boolean b0 = false;
                        Object a4 = a1.iterator();
                        Object a5 = a0;
                        Object a6 = a1;
                        while(true) {
                            if (((java.util.Iterator)a4).hasNext()) {
                                if (android.text.TextUtils.isEmpty((CharSequence)(String)((java.util.Iterator)a4).next())) {
                                    continue;
                                }
                                b0 = false;
                                break;
                            } else {
                                b0 = true;
                                break;
                            }
                        }
                        if (!b0) {
                            int i = 0;
                            boolean b1 = false;
                            Object a7 = ((java.util.Map)a5).get("TYPE");
                            String s1 = null;
                            if (a7 == null) {
                                i = -1;
                                b1 = false;
                            } else {
                                java.util.Iterator a8 = ((java.util.Collection)a7).iterator();
                                s1 = null;
                                Object a9 = a8;
                                i = -1;
                                b1 = false;
                                while(((java.util.Iterator)a9).hasNext()) {
                                    String s2 = (String)((java.util.Iterator)a9).next();
                                    String s3 = s2.toUpperCase();
                                    if (s3.equals("PREF")) {
                                        b1 = true;
                                    } else if (s3.equals("HOME")) {
                                        s1 = null;
                                        i = 1;
                                    } else {
                                        if (!s3.equals("WORK") && !s3.equalsIgnoreCase("COMPANY")) {
                                            if (s3.equals("PARCEL")) {
                                                continue;
                                            }
                                            if (s3.equals("DOM")) {
                                                continue;
                                            }
                                            if (s3.equals("INTL")) {
                                                continue;
                                            }
                                            if (i >= 0) {
                                                continue;
                                            }
                                            if (s3.startsWith("X-")) {
                                                s1 = s2.substring(2);
                                                i = 0;
                                                continue;
                                            } else {
                                                s1 = s2;
                                                i = 0;
                                                continue;
                                            }
                                        }
                                        s1 = null;
                                        i = 2;
                                    }
                                }
                            }
                            if (i < 0) {
                                i = 1;
                            }
                            this.addPostal(i, (java.util.List)a6, s1, b1);
                        }
                    } else if (s.equals("EMAIL")) {
                        int i0 = 0;
                        boolean b2 = false;
                        Object a10 = a0.get("TYPE");
                        String s4 = null;
                        if (a10 == null) {
                            i0 = -1;
                            b2 = false;
                        } else {
                            java.util.Iterator a11 = ((java.util.Collection)a10).iterator();
                            s4 = null;
                            Object a12 = a11;
                            i0 = -1;
                            b2 = false;
                            while(((java.util.Iterator)a12).hasNext()) {
                                String s5 = (String)((java.util.Iterator)a12).next();
                                String s6 = s5.toUpperCase();
                                if (s6.equals("PREF")) {
                                    b2 = true;
                                } else if (s6.equals("HOME")) {
                                    i0 = 1;
                                } else if (s6.equals("WORK")) {
                                    i0 = 2;
                                } else if (s6.equals("CELL")) {
                                    i0 = 4;
                                } else if (i0 < 0) {
                                    if (s6.startsWith("X-")) {
                                        s5 = s5.substring(2);
                                    }
                                    s4 = s5;
                                    i0 = 0;
                                }
                            }
                        }
                        if (i0 < 0) {
                            i0 = 3;
                        }
                        this.addEmail(i0, s0, s4, b2);
                    } else if (s.equals("ORG")) {
                        Object a13 = null;
                        Object a14 = null;
                        boolean b3 = false;
                        Object a15 = a0.get("TYPE");
                        if (a15 == null) {
                            a13 = a0;
                            a14 = a1;
                            b3 = false;
                        } else {
                            Object a16 = ((java.util.Collection)a15).iterator();
                            a13 = a0;
                            a14 = a1;
                            b3 = false;
                            while(((java.util.Iterator)a16).hasNext()) {
                                if (((String)((java.util.Iterator)a16).next()).equals("PREF")) {
                                    b3 = true;
                                }
                            }
                        }
                        this.handleOrgValue(1, (java.util.List)a14, (java.util.Map)a13, b3);
                    } else if (s.equals("TITLE")) {
                        this.handleTitleValue(s0);
                    } else if (!s.equals("ROLE")) {
                        boolean b4 = false;
                        boolean b5 = s.equals("PHOTO");
                        label10: {
                            if (b5) {
                                break label10;
                            }
                            if (s.equals("LOGO")) {
                                break label10;
                            }
                            if (s.equals("TEL")) {
                                String s7 = null;
                                boolean b6 = false;
                                if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
                                    if (s0.startsWith("sip:")) {
                                        s7 = null;
                                        b6 = true;
                                    } else if (s0.startsWith("tel:")) {
                                        s7 = s0.substring(4);
                                        b6 = false;
                                    } else {
                                        s7 = s0;
                                        b6 = false;
                                    }
                                } else {
                                    s7 = s0;
                                    b6 = false;
                                }
                                if (b6) {
                                    this.handleSipCase(s0, (java.util.Collection)a0.get("TYPE"));
                                    break label0;
                                } else {
                                    int i1 = 0;
                                    String s8 = null;
                                    boolean b7 = false;
                                    if (s0.length() == 0) {
                                        break label0;
                                    }
                                    Object a17 = a0.get("TYPE");
                                    Object a18 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPhoneTypeFromStrings((java.util.Collection)a17, s7);
                                    if (a18 instanceof Integer) {
                                        i1 = ((Integer)a18).intValue();
                                        s8 = null;
                                    } else {
                                        s8 = a18.toString();
                                        i1 = 0;
                                    }
                                    label9: {
                                        label7: {
                                            label8: {
                                                if (a17 == null) {
                                                    break label8;
                                                }
                                                if (((java.util.Collection)a17).contains("PREF")) {
                                                    break label7;
                                                }
                                            }
                                            b7 = false;
                                            break label9;
                                        }
                                        b7 = true;
                                    }
                                    this.addPhone(i1, s7, s8, b7);
                                    break label0;
                                }
                            } else if (s.equals("X-IRMC-CALL-DATETIME")) {
                                int i2 = 0;
                                if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                                    break label0;
                                }
                                Object a19 = a0.get("TYPE");
                                label5: {
                                    label6: {
                                        if (a19 == null) {
                                            break label6;
                                        }
                                        if (((java.util.Collection)a19).size() <= 0) {
                                            i2 = 0;
                                            break label5;
                                        }
                                    }
                                    Object a20 = ((java.util.Collection)a19).iterator().next();
                                    if (a20 instanceof String) {
                                        String s9 = (String)a20;
                                        i2 = ("DIALED".equalsIgnoreCase(s9)) ? 11 : ("MISSED".equalsIgnoreCase(s9)) ? 12 : ("RECEIVED".equalsIgnoreCase(s9)) ? 10 : 0;
                                    } else {
                                        i2 = 0;
                                    }
                                }
                                java.util.Date a21 = com.navdy.hud.app.util.DateUtil.parseIrmcDateStr(s0);
                                if (a21 == null) {
                                    break label0;
                                }
                                this.addCallTime(i2, a21);
                                break label0;
                            } else if (s.equals("X-SKYPE-PSTNNUMBER")) {
                                boolean b8 = false;
                                Object a22 = a0.get("TYPE");
                                label4: {
                                    label2: {
                                        label3: {
                                            if (a22 == null) {
                                                break label3;
                                            }
                                            if (((java.util.Collection)a22).contains("PREF")) {
                                                break label2;
                                            }
                                        }
                                        b8 = false;
                                        break label4;
                                    }
                                    b8 = true;
                                }
                                this.addPhone(7, s0, (String)null, b8);
                                break label0;
                            } else if (sImMap.containsKey(s)) {
                                int i3 = 0;
                                boolean b9 = false;
                                int i4 = ((Integer)sImMap.get(s)).intValue();
                                Object a23 = a0.get("TYPE");
                                if (a23 == null) {
                                    i3 = -1;
                                    b9 = false;
                                } else {
                                    Object a24 = ((java.util.Collection)a23).iterator();
                                    i3 = -1;
                                    b9 = false;
                                    while(((java.util.Iterator)a24).hasNext()) {
                                        String s10 = (String)((java.util.Iterator)a24).next();
                                        if (s10.equals("PREF")) {
                                            b9 = true;
                                        } else if (i3 < 0) {
                                            if (s10.equalsIgnoreCase("HOME")) {
                                                i3 = 1;
                                            } else if (s10.equalsIgnoreCase("WORK")) {
                                                i3 = 2;
                                            }
                                        }
                                    }
                                }
                                if (i3 < 0) {
                                    i3 = 1;
                                }
                                this.addIm(i4, (String)null, s0, i3, b9);
                                break label0;
                            } else if (s.equals("NOTE")) {
                                this.addNote(s0);
                                break label0;
                            } else if (s.equals("URL")) {
                                if (this.mWebsiteList == null) {
                                    this.mWebsiteList = (java.util.List)new java.util.ArrayList(1);
                                }
                                this.mWebsiteList.add(new com.navdy.hud.app.bluetooth.vcard.VCardEntry$WebsiteData(s0));
                                break label0;
                            } else if (s.equals("BDAY")) {
                                this.mBirthday = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$BirthdayData(s0);
                                break label0;
                            } else if (s.equals("ANNIVERSARY")) {
                                this.mAnniversary = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$AnniversaryData(s0);
                                break label0;
                            } else if (s.equals("X-PHONETIC-FIRST-NAME")) {
                                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$702(this.mNameData, s0);
                                break label0;
                            } else if (s.equals("X-PHONETIC-MIDDLE-NAME")) {
                                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$602(this.mNameData, s0);
                                break label0;
                            } else if (s.equals("X-PHONETIC-LAST-NAME")) {
                                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData.access$502(this.mNameData, s0);
                                break label0;
                            } else if (s.equals("IMPP")) {
                                if (!s0.startsWith("sip:")) {
                                    break label0;
                                }
                                this.handleSipCase(s0, (java.util.Collection)a0.get("TYPE"));
                                break label0;
                            } else if (s.equals("X-SIP")) {
                                if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                                    break label0;
                                }
                                this.handleSipCase(s0, (java.util.Collection)a0.get("TYPE"));
                                break label0;
                            } else {
                                if (!s.equals("X-ANDROID-CUSTOM")) {
                                    break label0;
                                }
                                this.handleAndroidCustomProperty(com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructListFromValue(s0, this.mVCardType));
                                break label0;
                            }
                        }
                        Object a25 = a0.get("VALUE");
                        label1: {
                            if (a25 == null) {
                                break label1;
                            }
                            if (((java.util.Collection)a25).contains("URL")) {
                                break label0;
                            }
                        }
                        Object a26 = a0.get("TYPE");
                        String s11 = null;
                        if (a26 == null) {
                            b4 = false;
                        } else {
                            java.util.Iterator a27 = ((java.util.Collection)a26).iterator();
                            s11 = null;
                            Object a28 = a27;
                            b4 = false;
                            while(((java.util.Iterator)a28).hasNext()) {
                                String s12 = (String)((java.util.Iterator)a28).next();
                                if ("PREF".equals(s12)) {
                                    b4 = true;
                                } else if (s11 == null) {
                                    s11 = s12;
                                }
                            }
                        }
                        this.addPhotoBytes(s11, a2, b4);
                    }
                }
            }
        }
    }
    
    public void consolidateFields() {
        this.mNameData.displayName = this.constructDisplayName();
    }
    
    public java.util.ArrayList constructInsertOperations(android.content.ContentResolver a, java.util.ArrayList a0) {
        if (a0 == null) {
            a0 = new java.util.ArrayList();
        }
        if (!this.isIgnorable()) {
            int i = a0.size();
            android.content.ContentProviderOperation$Builder a1 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$RawContacts.CONTENT_URI);
            if (this.mAccount == null) {
                a1.withValue("account_name", null);
                a1.withValue("account_type", null);
            } else {
                a1.withValue("account_name", this.mAccount.name);
                a1.withValue("account_type", this.mAccount.type);
            }
            a0.add(a1.build());
            a0.size();
            this.iterateAllData((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator)new com.navdy.hud.app.bluetooth.vcard.VCardEntry$InsertOperationConstrutor(this, (java.util.List)a0, i));
            a0.size();
        }
        return a0;
    }
    
    final public String getBirthday() {
        String s = (this.mBirthday == null) ? null : com.navdy.hud.app.bluetooth.vcard.VCardEntry$BirthdayData.access$1800(this.mBirthday);
        return s;
    }
    
    public java.util.Date getCallTime() {
        return this.mCallTime;
    }
    
    public int getCallType() {
        return this.mCallType;
    }
    
    final public java.util.List getChildlen() {
        return this.mChildren;
    }
    
    public String getDisplayName() {
        if (this.mNameData.displayName == null) {
            this.mNameData.displayName = this.constructDisplayName();
        }
        return this.mNameData.displayName;
    }
    
    final public java.util.List getEmailList() {
        return this.mEmailList;
    }
    
    final public java.util.List getImList() {
        return this.mImList;
    }
    
    final public com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData getNameData() {
        return this.mNameData;
    }
    
    final public java.util.List getNickNameList() {
        return this.mNicknameList;
    }
    
    final public java.util.List getNotes() {
        return this.mNoteList;
    }
    
    final public java.util.List getOrganizationList() {
        return this.mOrganizationList;
    }
    
    final public java.util.List getPhoneList() {
        return this.mPhoneList;
    }
    
    final public java.util.List getPhotoList() {
        return this.mPhotoList;
    }
    
    final public java.util.List getPostalList() {
        return this.mPostalList;
    }
    
    final public java.util.List getWebsiteList() {
        return this.mWebsiteList;
    }
    
    public boolean isIgnorable() {
        com.navdy.hud.app.bluetooth.vcard.VCardEntry$IsIgnorableIterator a = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$IsIgnorableIterator(this, (com.navdy.hud.app.bluetooth.vcard.VCardEntry$1)null);
        this.iterateAllData((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator)a);
        return a.getResult();
    }
    
    final public void iterateAllData(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator a) {
        a.onIterationStarted();
        a.onElementGroupStarted(this.mNameData.getEntryLabel());
        a.onElement((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement)this.mNameData);
        a.onElementGroupEnded();
        this.iterateOneList(this.mPhoneList, a);
        this.iterateOneList(this.mEmailList, a);
        this.iterateOneList(this.mPostalList, a);
        this.iterateOneList(this.mOrganizationList, a);
        this.iterateOneList(this.mImList, a);
        this.iterateOneList(this.mPhotoList, a);
        this.iterateOneList(this.mWebsiteList, a);
        this.iterateOneList(this.mSipList, a);
        this.iterateOneList(this.mNicknameList, a);
        this.iterateOneList(this.mNoteList, a);
        this.iterateOneList(this.mAndroidCustomDataList, a);
        if (this.mBirthday != null) {
            a.onElementGroupStarted(this.mBirthday.getEntryLabel());
            a.onElement((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement)this.mBirthday);
            a.onElementGroupEnded();
        }
        if (this.mAnniversary != null) {
            a.onElementGroupStarted(this.mAnniversary.getEntryLabel());
            a.onElement((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement)this.mAnniversary);
            a.onElementGroupEnded();
        }
        a.onIterationEnded();
    }
    
    public String toString() {
        com.navdy.hud.app.bluetooth.vcard.VCardEntry$ToStringIterator a = new com.navdy.hud.app.bluetooth.vcard.VCardEntry$ToStringIterator(this, (com.navdy.hud.app.bluetooth.vcard.VCardEntry$1)null);
        this.iterateAllData((com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElementIterator)a);
        return a.toString();
    }
}
