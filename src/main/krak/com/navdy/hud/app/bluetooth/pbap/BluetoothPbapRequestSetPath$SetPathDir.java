package com.navdy.hud.app.bluetooth.pbap;


    public enum BluetoothPbapRequestSetPath$SetPathDir {
        ROOT(0),
    UP(1),
    DOWN(2);

        private int value;
        BluetoothPbapRequestSetPath$SetPathDir(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class BluetoothPbapRequestSetPath$SetPathDir extends Enum {
//    final private static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir[] $VALUES;
//    final public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir DOWN;
//    final public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir ROOT;
//    final public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir UP;
//    
//    static {
//        ROOT = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir("ROOT", 0);
//        UP = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir("UP", 1);
//        DOWN = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir("DOWN", 2);
//        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir[] a = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir[3];
//        a[0] = ROOT;
//        a[1] = UP;
//        a[2] = DOWN;
//        $VALUES = a;
//    }
//    
//    private BluetoothPbapRequestSetPath$SetPathDir(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir valueOf(String s) {
//        return (com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir)Enum.valueOf(com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir.class, s);
//    }
//    
//    public static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir[] values() {
//        return $VALUES.clone();
//    }
//}
//