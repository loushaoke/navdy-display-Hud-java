package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntryCounter implements com.navdy.hud.app.bluetooth.vcard.VCardInterpreter {
    private int mCount;
    
    public VCardEntryCounter() {
    }
    
    public int getCount() {
        return this.mCount;
    }
    
    public void onEntryEnded() {
        this.mCount = this.mCount + 1;
    }
    
    public void onEntryStarted() {
    }
    
    public void onPropertyCreated(com.navdy.hud.app.bluetooth.vcard.VCardProperty a) {
    }
    
    public void onVCardEnded() {
    }
    
    public void onVCardStarted() {
    }
}
