package com.navdy.hud.app.bluetooth.vcard;

public class VCardUtils {
    final private static String LOG_TAG = "vCard";
    final private static int[] sEscapeIndicatorsV30;
    final private static int[] sEscapeIndicatorsV40;
    final private static java.util.Map sKnownImPropNameMap_ItoS;
    final private static java.util.Map sKnownPhoneTypeMap_StoI;
    final private static java.util.Map sKnownPhoneTypesMap_ItoS;
    final private static java.util.Set sMobilePhoneLabelSet;
    final private static java.util.Set sPhoneTypesUnknownToContactsSet;
    final private static java.util.Set sUnAcceptableAsciiInV21WordSet;
    
    static {
        sKnownPhoneTypesMap_ItoS = (java.util.Map)new java.util.HashMap();
        sKnownPhoneTypeMap_StoI = (java.util.Map)new java.util.HashMap();
        sKnownPhoneTypesMap_ItoS.put(Integer.valueOf(9), "CAR");
        sKnownPhoneTypeMap_StoI.put("CAR", Integer.valueOf(9));
        sKnownPhoneTypesMap_ItoS.put(Integer.valueOf(6), "PAGER");
        sKnownPhoneTypeMap_StoI.put("PAGER", Integer.valueOf(6));
        sKnownPhoneTypesMap_ItoS.put(Integer.valueOf(11), "ISDN");
        sKnownPhoneTypeMap_StoI.put("ISDN", Integer.valueOf(11));
        sKnownPhoneTypeMap_StoI.put("HOME", Integer.valueOf(1));
        sKnownPhoneTypeMap_StoI.put("WORK", Integer.valueOf(3));
        sKnownPhoneTypeMap_StoI.put("CELL", Integer.valueOf(2));
        sKnownPhoneTypeMap_StoI.put("OTHER", Integer.valueOf(7));
        sKnownPhoneTypeMap_StoI.put("CALLBACK", Integer.valueOf(8));
        sKnownPhoneTypeMap_StoI.put("COMPANY-MAIN", Integer.valueOf(10));
        sKnownPhoneTypeMap_StoI.put("RADIO", Integer.valueOf(14));
        sKnownPhoneTypeMap_StoI.put("TTY-TDD", Integer.valueOf(16));
        sKnownPhoneTypeMap_StoI.put("ASSISTANT", Integer.valueOf(19));
        sKnownPhoneTypeMap_StoI.put("VOICE", Integer.valueOf(7));
        sPhoneTypesUnknownToContactsSet = (java.util.Set)new java.util.HashSet();
        sPhoneTypesUnknownToContactsSet.add("MODEM");
        sPhoneTypesUnknownToContactsSet.add("MSG");
        sPhoneTypesUnknownToContactsSet.add("BBS");
        sPhoneTypesUnknownToContactsSet.add("VIDEO");
        sKnownImPropNameMap_ItoS = (java.util.Map)new java.util.HashMap();
        sKnownImPropNameMap_ItoS.put(Integer.valueOf(0), "X-AIM");
        sKnownImPropNameMap_ItoS.put(Integer.valueOf(1), "X-MSN");
        sKnownImPropNameMap_ItoS.put(Integer.valueOf(2), "X-YAHOO");
        sKnownImPropNameMap_ItoS.put(Integer.valueOf(3), "X-SKYPE-USERNAME");
        sKnownImPropNameMap_ItoS.put(Integer.valueOf(5), "X-GOOGLE-TALK");
        sKnownImPropNameMap_ItoS.put(Integer.valueOf(6), "X-ICQ");
        sKnownImPropNameMap_ItoS.put(Integer.valueOf(7), "X-JABBER");
        sKnownImPropNameMap_ItoS.put(Integer.valueOf(4), "X-QQ");
        sKnownImPropNameMap_ItoS.put(Integer.valueOf(8), "X-NETMEETING");
        String[] a = new String[5];
        a[0] = "MOBILE";
        a[1] = "\u643a\u5e2f\u96fb\u8a71";
        a[2] = "\u643a\u5e2f";
        a[3] = "\u30b1\u30a4\u30bf\u30a4";
        a[4] = "\uff79\uff72\uff80\uff72";
        sMobilePhoneLabelSet = (java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a));
        Character[] a0 = new Character[7];
        a0[0] = Character.valueOf((char)91);
        a0[1] = Character.valueOf((char)93);
        a0[2] = Character.valueOf((char)61);
        a0[3] = Character.valueOf((char)58);
        a0[4] = Character.valueOf((char)46);
        a0[5] = Character.valueOf((char)44);
        a0[6] = Character.valueOf((char)32);
        sUnAcceptableAsciiInV21WordSet = (java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a0));
        int[] a1 = new int[4];
        a1[0] = 58;
        a1[1] = 59;
        a1[2] = 44;
        a1[3] = 32;
        sEscapeIndicatorsV30 = a1;
        int[] a2 = new int[2];
        a2[0] = 59;
        a2[1] = 58;
        sEscapeIndicatorsV40 = a2;
    }
    
    private VCardUtils() {
    }
    
    public static boolean appearsLikeAndroidVCardQuotedPrintable(String s) {
        boolean b = false;
        int i = s.length() % 3;
        label1: if (s.length() < 2) {
            b = false;
        } else {
            label0: {
                if (i == 1) {
                    break label0;
                }
                if (i == 0) {
                    break label0;
                }
                b = false;
                break label1;
            }
            int i0 = 0;
            while(true) {
                if (i0 >= s.length()) {
                    b = true;
                    break;
                } else {
                    int i1 = s.charAt(i0);
                    if (i1 != 61) {
                        b = false;
                        break;
                    } else {
                        i0 = i0 + 3;
                    }
                }
            }
        }
        return b;
    }
    
    public static boolean areAllEmpty(String[] a) {
        boolean b = false;
        if (a != null) {
            int i = a.length;
            int i0 = 0;
            while(true) {
                if (i0 >= i) {
                    b = true;
                    break;
                } else {
                    if (android.text.TextUtils.isEmpty((CharSequence)a[i0])) {
                        i0 = i0 + 1;
                        continue;
                    }
                    b = false;
                    break;
                }
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public static java.util.List constructListFromValue(String s, int i) {
        java.util.ArrayList a = new java.util.ArrayList();
        StringBuilder a0 = new StringBuilder();
        int i0 = s.length();
        int i1 = 0;
        while(i1 < i0) {
            int i2 = s.charAt(i1);
            label0: {
                String s0 = null;
                label1: {
                    label2: {
                        if (i2 != 92) {
                            break label2;
                        }
                        if (i1 < i0 - 1) {
                            break label1;
                        }
                    }
                    if (i2 != 59) {
                        a0.append((char)i2);
                        break label0;
                    } else {
                        ((java.util.List)a).add(a0.toString());
                        a0 = new StringBuilder();
                        break label0;
                    }
                }
                int i3 = s.charAt(i1 + 1);
                if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(i)) {
                    s0 = com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V40.unescapeCharacter((char)i3);
                } else if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(i)) {
                    s0 = com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V30.unescapeCharacter((char)i3);
                } else {
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion21(i)) {
                        android.util.Log.w("vCard", "Unknown vCard type");
                    }
                    s0 = com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21.unescapeCharacter((char)i3);
                }
                if (s0 == null) {
                    a0.append((char)i2);
                } else {
                    a0.append(s0);
                    i1 = i1 + 1;
                }
            }
            i1 = i1 + 1;
        }
        ((java.util.List)a).add(a0.toString());
        return (java.util.List)a;
    }
    
    public static String constructNameFromElements(int i, String s, String s0, String s1) {
        return com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(i, s, s0, s1, (String)null, (String)null);
    }
    
    public static String constructNameFromElements(int i, String s, String s0, String s1, String s2, String s3) {
        boolean b = false;
        StringBuilder a = new StringBuilder();
        String[] a0 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.sortNameElements(i, s, s0, s1);
        if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
            b = true;
        } else {
            a.append(s2);
            b = false;
        }
        int i0 = a0.length;
        int i1 = 0;
        while(i1 < i0) {
            String s4 = a0[i1];
            if (!android.text.TextUtils.isEmpty((CharSequence)s4)) {
                if (b) {
                    b = false;
                } else {
                    a.append((char)32);
                }
                a.append(s4);
            }
            i1 = i1 + 1;
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)s3)) {
            if (!b) {
                a.append((char)32);
            }
            a.append(s3);
        }
        return a.toString();
    }
    
    public static boolean containsOnlyAlphaDigitHyphen(java.util.Collection a) {
        boolean b = false;
        if (a != null) {
            Object a0 = a.iterator();
            label4: while(true) {
                if (((java.util.Iterator)a0).hasNext()) {
                    String s = (String)((java.util.Iterator)a0).next();
                    if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                        continue label4;
                    }
                    int i = s.length();
                    int i0 = 0;
                    while(true) {
                        if (i0 >= i) {
                            continue label4;
                        }
                        int i1 = s.codePointAt(i0);
                        label0: {
                            label3: {
                                if (97 > i1) {
                                    break label3;
                                }
                                if (i1 < 123) {
                                    break label0;
                                }
                            }
                            label2: {
                                if (65 > i1) {
                                    break label2;
                                }
                                if (i1 < 91) {
                                    break label0;
                                }
                            }
                            label1: {
                                if (48 > i1) {
                                    break label1;
                                }
                                if (i1 < 58) {
                                    break label0;
                                }
                            }
                            if (i1 != 45) {
                                break;
                            }
                        }
                        i0 = s.offsetByCodePoints(i0, 1);
                    }
                    b = false;
                    break;
                } else {
                    b = true;
                    break;
                }
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public static boolean containsOnlyAlphaDigitHyphen(String[] a) {
        return a == null || com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyAlphaDigitHyphen((java.util.Collection)java.util.Arrays.asList((Object[])a));
    }
    
    public static boolean containsOnlyNonCrLfPrintableAscii(java.util.Collection a) {
        boolean b = false;
        if (a != null) {
            Object a0 = a.iterator();
            label0: while(true) {
                if (((java.util.Iterator)a0).hasNext()) {
                    String s = (String)((java.util.Iterator)a0).next();
                    if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                        continue label0;
                    }
                    int i = s.length();
                    int i0 = 0;
                    while(true) {
                        if (i0 >= i) {
                            continue label0;
                        }
                        int i1 = s.codePointAt(i0);
                        if (32 > i1) {
                            break;
                        }
                        if (i1 > 126) {
                            break;
                        }
                        i0 = s.offsetByCodePoints(i0, 1);
                    }
                    b = false;
                    break;
                } else {
                    b = true;
                    break;
                }
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public static boolean containsOnlyNonCrLfPrintableAscii(String[] a) {
        return a == null || com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii((java.util.Collection)java.util.Arrays.asList((Object[])a));
    }
    
    public static boolean containsOnlyPrintableAscii(java.util.Collection a) {
        boolean b = false;
        if (a != null) {
            Object a0 = a.iterator();
            while(true) {
                if (((java.util.Iterator)a0).hasNext()) {
                    String s = (String)((java.util.Iterator)a0).next();
                    if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                        continue;
                    }
                    if (com.navdy.hud.app.bluetooth.vcard.VCardUtils$TextUtilsPort.isPrintableAsciiOnly((CharSequence)s)) {
                        continue;
                    }
                    b = false;
                    break;
                } else {
                    b = true;
                    break;
                }
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public static boolean containsOnlyPrintableAscii(String[] a) {
        return a == null || com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii((java.util.Collection)java.util.Arrays.asList((Object[])a));
    }
    
    public static boolean containsOnlyWhiteSpaces(java.util.Collection a) {
        boolean b = false;
        if (a != null) {
            Object a0 = a.iterator();
            label0: while(true) {
                if (((java.util.Iterator)a0).hasNext()) {
                    String s = (String)((java.util.Iterator)a0).next();
                    if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                        continue label0;
                    }
                    int i = s.length();
                    int i0 = 0;
                    while(true) {
                        if (i0 >= i) {
                            continue label0;
                        }
                        if (!Character.isWhitespace(s.codePointAt(i0))) {
                            break;
                        }
                        i0 = s.offsetByCodePoints(i0, 1);
                    }
                    b = false;
                    break;
                } else {
                    b = true;
                    break;
                }
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public static boolean containsOnlyWhiteSpaces(String[] a) {
        return a == null || com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyWhiteSpaces((java.util.Collection)java.util.Arrays.asList((Object[])a));
    }
    
    final public static String convertStringCharset(String s, String s0, String s1) {
        boolean b = s0.equalsIgnoreCase(s1);
        label1: {
            if (b) {
                break label1;
            }
            java.nio.ByteBuffer a = java.nio.charset.Charset.forName(s0).encode(s);
            byte[] a0 = new byte[a.remaining()];
            a.get(a0);
            label0: {
                String s2 = null;
                try {
                    s2 = new String(a0, s1);
                } catch(java.io.UnsupportedEncodingException ignoredException) {
                    break label0;
                }
                s = s2;
                break label1;
            }
            android.util.Log.e("vCard", new StringBuilder().append("Failed to encode: charset=").append(s1).toString());
            s = null;
        }
        return s;
    }
    
    final public static com.navdy.hud.app.bluetooth.vcard.VCardParser getAppropriateParser(int i) {
        com.navdy.hud.app.bluetooth.vcard.VCardParser a = null;
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion21(i)) {
            a = new com.navdy.hud.app.bluetooth.vcard.VCardParser_V21();
        } else if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(i)) {
            a = new com.navdy.hud.app.bluetooth.vcard.VCardParser_V30();
        } else {
            if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(i)) {
                throw new com.navdy.hud.app.bluetooth.vcard.exception.VCardException("Version is not specified");
            }
            a = new com.navdy.hud.app.bluetooth.vcard.VCardParser_V40();
        }
        return a;
    }
    
    public static int getPhoneNumberFormat(int i) {
        return (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isJapaneseDevice(i)) ? 2 : 1;
    }
    
    public static Object getPhoneTypeFromStrings(java.util.Collection a, String s) {
        boolean b = false;
        boolean b0 = false;
        int i = 0;
        if (s == null) {
            s = "";
        }
        String s0 = null;
        if (a == null) {
            b = false;
            b0 = false;
            i = -1;
        } else {
            java.util.Iterator a0 = a.iterator();
            s0 = null;
            i = -1;
            b = false;
            b0 = false;
            Object a1 = a0;
            while(((java.util.Iterator)a1).hasNext()) {
                String s1 = (String)((java.util.Iterator)a1).next();
                if (s1 != null) {
                    String s2 = s1.toUpperCase();
                    if (s2.equals("PREF")) {
                        b0 = true;
                    } else if (s2.equals("FAX")) {
                        b = true;
                    } else {
                        boolean b1 = s2.startsWith("X-");
                        label3: {
                            label2: {
                                {
                                    if (!b1) {
                                        break label3;
                                    }
                                    if (i < 0) {
                                        break label2;
                                    }
                                }
                                break label3;
                            }
                            s1 = s1.substring(2);
                        }
                        if (s1.length() != 0) {
                            Integer a2 = (Integer)sKnownPhoneTypeMap_StoI.get(s1.toUpperCase());
                            if (a2 == null) {
                                if (i < 0) {
                                    s0 = s1;
                                    i = 0;
                                }
                            } else {
                                int i0 = a2.intValue();
                                int i1 = s.indexOf("@");
                                label0: {
                                    label1: {
                                        if (i0 != 6) {
                                            break label1;
                                        }
                                        if (i1 <= 0) {
                                            break label1;
                                        }
                                        if (i1 < s.length() - 1) {
                                            break label0;
                                        }
                                    }
                                    if (i >= 0 && i != 0 && i != 7) {
                                        continue;
                                    }
                                }
                                i = a2.intValue();
                            }
                        }
                    }
                }
            }
        }
        if (i < 0) {
            i = b0 ? 12 : 1;
        }
        if (b) {
            if (i != 1) {
                if (i != 3) {
                    if (i == 7) {
                        i = 13;
                    }
                } else {
                    i = 4;
                }
            } else {
                i = 5;
            }
        }
        Object a3 = (i != 0) ? Integer.valueOf(i) : s0;
        return a3;
    }
    
    public static String getPhoneTypeString(Integer a) {
        return (String)sKnownPhoneTypesMap_ItoS.get(a);
    }
    
    public static String getPropertyNameForIm(int i) {
        return (String)sKnownImPropNameMap_ItoS.get(Integer.valueOf(i));
    }
    
    public static String guessImageType(byte[] a) {
        String s = null;
        label2: if (a != null) {
            int i = a.length;
            label4: {
                if (i < 3) {
                    break label4;
                }
                int i0 = a[0];
                if (i0 != 71) {
                    break label4;
                }
                int i1 = a[1];
                if (i1 != 73) {
                    break label4;
                }
                int i2 = a[2];
                if (i2 != 70) {
                    break label4;
                }
                s = "GIF";
                break label2;
            }
            int i3 = a.length;
            label3: {
                if (i3 < 4) {
                    break label3;
                }
                int i4 = a[0];
                if (i4 != -119) {
                    break label3;
                }
                int i5 = a[1];
                if (i5 != 80) {
                    break label3;
                }
                int i6 = a[2];
                if (i6 != 78) {
                    break label3;
                }
                int i7 = a[3];
                if (i7 != 71) {
                    break label3;
                }
                s = "PNG";
                break label2;
            }
            int i8 = a.length;
            label0: {
                label1: {
                    if (i8 < 2) {
                        break label1;
                    }
                    int i9 = a[0];
                    if (i9 != -1) {
                        break label1;
                    }
                    int i10 = a[1];
                    if (i10 == -40) {
                        break label0;
                    }
                }
                s = null;
                break label2;
            }
            s = "JPEG";
        } else {
            s = null;
        }
        return s;
    }
    
    public static boolean isMobilePhoneLabel(String s) {
        boolean b = false;
        boolean b0 = "_AUTO_CELL".equals(s);
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!sMobilePhoneLabelSet.contains(s)) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static boolean isV21Word(String s) {
        boolean b = false;
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            b = true;
        } else {
            int i = s.length();
            int i0 = 0;
            while(true) {
                if (i0 >= i) {
                    b = true;
                    break;
                } else {
                    int i1 = s.codePointAt(i0);
                    if (32 <= i1 && i1 <= 126) {
                        java.util.Set a = sUnAcceptableAsciiInV21WordSet;
                        int i2 = (char)i1;
                        if (!a.contains(Character.valueOf((char)i2))) {
                            i0 = s.offsetByCodePoints(i0, 1);
                            continue;
                        }
                    }
                    b = false;
                    break;
                }
            }
        }
        return b;
    }
    
    public static boolean isValidInV21ButUnknownToContactsPhoteType(String s) {
        return sPhoneTypesUnknownToContactsSet.contains(s);
    }
    
    public static String parseQuotedPrintable(String s, boolean b, String s0, String s1) {
        String[] a = null;
        byte[] a0 = null;
        String s2 = null;
        StringBuilder a1 = new StringBuilder();
        int i = s.length();
        int i0 = 0;
        while(i0 < i) {
            int i1 = s.charAt(i0);
            label2: {
                label0: {
                    if (i1 != 61) {
                        break label0;
                    }
                    if (i0 >= i - 1) {
                        break label0;
                    }
                    int i2 = s.charAt(i0 + 1);
                    label1: {
                        if (i2 == 32) {
                            break label1;
                        }
                        if (i2 != 9) {
                            break label0;
                        }
                    }
                    a1.append((char)i2);
                    i0 = i0 + 1;
                    break label2;
                }
                a1.append((char)i1);
            }
            i0 = i0 + 1;
        }
        String s3 = a1.toString();
        if (b) {
            a = s3.split("\r\n");
        } else {
            StringBuilder a2 = new StringBuilder();
            int i3 = s3.length();
            java.util.ArrayList a3 = new java.util.ArrayList();
            int i4 = 0;
            while(i4 < i3) {
                int i5 = s3.charAt(i4);
                if (i5 != 10) {
                    if (i5 != 13) {
                        a2.append((char)i5);
                    } else {
                        a3.add(a2.toString());
                        a2 = new StringBuilder();
                        if (i4 < i3 - 1) {
                            int i6 = s3.charAt(i4 + 1);
                            if (i6 == 10) {
                                i4 = i4 + 1;
                            }
                        }
                    }
                } else {
                    a3.add(a2.toString());
                    a2 = new StringBuilder();
                }
                i4 = i4 + 1;
            }
            String s4 = a2.toString();
            if (s4.length() > 0) {
                a3.add(s4);
            }
            a = (String[])a3.toArray((Object[])new String[0]);
        }
        StringBuilder a4 = new StringBuilder();
        int i7 = a.length;
        int i8 = 0;
        while(i8 < i7) {
            String s5 = a[i8];
            if (s5.endsWith("=")) {
                s5 = s5.substring(0, s5.length() - 1);
            }
            a4.append(s5);
            i8 = i8 + 1;
        }
        String s6 = a4.toString();
        if (android.text.TextUtils.isEmpty((CharSequence)s6)) {
            android.util.Log.w("vCard", "Given raw string is empty.");
        }
        try {
            a0 = s6.getBytes(s0);
        } catch(java.io.UnsupportedEncodingException ignoredException) {
            android.util.Log.w("vCard", new StringBuilder().append("Failed to decode: ").append(s0).toString());
            a0 = s6.getBytes();
        }
        try {
            a0 = com.navdy.hud.app.bluetooth.vcard.VCardUtils$QuotedPrintableCodecPort.decodeQuotedPrintable(a0);
        } catch(com.navdy.hud.app.bluetooth.vcard.VCardUtils$DecoderException ignoredException0) {
            android.util.Log.e("vCard", "DecoderException is thrown.");
        }
        try {
            s2 = new String(a0, s1);
        } catch(java.io.UnsupportedEncodingException ignoredException1) {
            android.util.Log.e("vCard", new StringBuilder().append("Failed to encode: charset=").append(s1).toString());
            s2 = new String(a0);
        }
        return s2;
    }
    
    public static String[] sortNameElements(int i, String s, String s0, String s1) {
        String[] a = new String[3];
        switch(com.navdy.hud.app.bluetooth.vcard.VCardConfig.getNameOrderType(i)) {
            case 8: {
                String[] a0 = new String[1];
                a0[0] = s;
                boolean b = com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a0);
                label0: {
                    label1: {
                        if (!b) {
                            break label1;
                        }
                        String[] a1 = new String[1];
                        a1[0] = s1;
                        if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a1)) {
                            break label0;
                        }
                    }
                    a[0] = s;
                    a[1] = s0;
                    a[2] = s1;
                    break;
                }
                a[0] = s1;
                a[1] = s0;
                a[2] = s;
                break;
            }
            case 4: {
                a[0] = s0;
                a[1] = s1;
                a[2] = s;
                break;
            }
            default: {
                a[0] = s1;
                a[1] = s0;
                a[2] = s;
            }
        }
        return a;
    }
    
    public static String toHalfWidthString(String s) {
        String s0 = null;
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s0 = null;
        } else {
            StringBuilder a = new StringBuilder();
            int i = s.length();
            int i0 = 0;
            while(i0 < i) {
                int i1 = s.charAt(i0);
                String s1 = com.navdy.hud.app.bluetooth.vcard.JapaneseUtils.tryGetHalfWidthText((char)i1);
                if (s1 == null) {
                    a.append((char)i1);
                } else {
                    a.append(s1);
                }
                i0 = s.offsetByCodePoints(i0, 1);
            }
            s0 = a.toString();
        }
        return s0;
    }
    
    private static String toStringAsParamValue(String s, int[] a) {
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s = "";
        }
        StringBuilder a0 = new StringBuilder();
        int i = s.length();
        boolean b = false;
        int i0 = 0;
        while(i0 < i) {
            int i1 = s.codePointAt(i0);
            if (i1 >= 32 && i1 != 34) {
                a0.appendCodePoint(i1);
                int i2 = a.length;
                int i3 = 0;
                while(i3 < i2) {
                    {
                        if (i1 != a[i3]) {
                            i3 = i3 + 1;
                            continue;
                        }
                        b = true;
                        break;
                    }
                }
            }
            i0 = s.offsetByCodePoints(i0, 1);
        }
        String s0 = a0.toString();
        boolean b0 = s0.isEmpty();
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    String[] a1 = new String[1];
                    a1[0] = s0;
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyWhiteSpaces(a1)) {
                        break label0;
                    }
                }
                s0 = "";
                break label2;
            }
            if (b) {
                s0 = new StringBuilder().append((char)34).append(s0).append((char)34).toString();
            }
        }
        return s0;
    }
    
    public static String toStringAsV30ParamValue(String s) {
        return com.navdy.hud.app.bluetooth.vcard.VCardUtils.toStringAsParamValue(s, sEscapeIndicatorsV30);
    }
    
    public static String toStringAsV40ParamValue(String s) {
        return com.navdy.hud.app.bluetooth.vcard.VCardUtils.toStringAsParamValue(s, sEscapeIndicatorsV40);
    }
}
