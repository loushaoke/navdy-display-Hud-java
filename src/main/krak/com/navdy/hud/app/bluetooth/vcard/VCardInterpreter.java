package com.navdy.hud.app.bluetooth.vcard;

abstract public interface VCardInterpreter {
    abstract public void onEntryEnded();
    
    
    abstract public void onEntryStarted();
    
    
    abstract public void onPropertyCreated(com.navdy.hud.app.bluetooth.vcard.VCardProperty arg);
    
    
    abstract public void onVCardEnded();
    
    
    abstract public void onVCardStarted();
}
