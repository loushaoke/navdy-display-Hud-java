package com.navdy.hud.app.bluetooth.pbap;

public class BluetoothPbapCard {
    final public String N;
    final public String firstName;
    final public String handle;
    final public String lastName;
    final public String middleName;
    final public String prefix;
    final public String suffix;
    
    public BluetoothPbapCard(String s, String s0) {
        this.handle = s;
        this.N = s0;
        String[] a = s0.split(";", 5);
        this.lastName = (a.length >= 1) ? a[0] : null;
        this.firstName = (a.length >= 2) ? a[1] : null;
        this.middleName = (a.length >= 3) ? a[2] : null;
        this.prefix = (a.length >= 4) ? a[3] : null;
        this.suffix = (a.length >= 5) ? a[4] : null;
    }
    
    public static String jsonifyVcardEntry(com.navdy.hud.app.bluetooth.vcard.VCardEntry a) {
        org.json.JSONObject a0 = new org.json.JSONObject();
        try {
            com.navdy.hud.app.bluetooth.vcard.VCardEntry$NameData a1 = a.getNameData();
            a0.put("formatted", a1.getFormatted());
            a0.put("family", a1.getFamily());
            a0.put("given", a1.getGiven());
            a0.put("middle", a1.getMiddle());
            a0.put("prefix", a1.getPrefix());
            a0.put("suffix", a1.getSuffix());
        } catch(org.json.JSONException ignoredException) {
        }
        try {
            org.json.JSONArray a2 = new org.json.JSONArray();
            java.util.List a3 = a.getPhoneList();
            if (a3 != null) {
                Object a4 = a3.iterator();
                while(((java.util.Iterator)a4).hasNext()) {
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhoneData a5 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhoneData)((java.util.Iterator)a4).next();
                    org.json.JSONObject a6 = new org.json.JSONObject();
                    a6.put("type", a5.getType());
                    a6.put("number", a5.getNumber());
                    a6.put("label", a5.getLabel());
                    a6.put("is_primary", a5.isPrimary());
                    a2.put(a6);
                }
                a0.put("phones", a2);
            }
        } catch(org.json.JSONException ignoredException0) {
        }
        try {
            org.json.JSONArray a7 = new org.json.JSONArray();
            java.util.List a8 = a.getEmailList();
            if (a8 != null) {
                Object a9 = a8.iterator();
                while(((java.util.Iterator)a9).hasNext()) {
                    com.navdy.hud.app.bluetooth.vcard.VCardEntry$EmailData a10 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$EmailData)((java.util.Iterator)a9).next();
                    org.json.JSONObject a11 = new org.json.JSONObject();
                    a11.put("type", a10.getType());
                    a11.put("address", a10.getAddress());
                    a11.put("label", a10.getLabel());
                    a11.put("is_primary", a10.isPrimary());
                    a7.put(a11);
                }
                a0.put("emails", a7);
            }
        } catch(org.json.JSONException ignoredException1) {
        }
        return a0.toString();
    }
    
    public String toString() {
        org.json.JSONObject a = new org.json.JSONObject();
        try {
            a.put("handle", this.handle);
            a.put("N", this.N);
            a.put("lastName", this.lastName);
            a.put("firstName", this.firstName);
            a.put("middleName", this.middleName);
            a.put("prefix", this.prefix);
            a.put("suffix", this.suffix);
        } catch(org.json.JSONException ignoredException) {
        }
        return a.toString();
    }
}
