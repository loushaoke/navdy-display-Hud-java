package com.navdy.hud.app.bluetooth.vcard;

abstract public interface VCardEntry$EntryElementIterator {
    abstract public boolean onElement(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement arg);
    
    
    abstract public void onElementGroupEnded();
    
    
    abstract public void onElementGroupStarted(com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel arg);
    
    
    abstract public void onIterationEnded();
    
    
    abstract public void onIterationStarted();
}
