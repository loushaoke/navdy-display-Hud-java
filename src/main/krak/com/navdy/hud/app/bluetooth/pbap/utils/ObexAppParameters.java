package com.navdy.hud.app.bluetooth.pbap.utils;

final public class ObexAppParameters {
    final private java.util.HashMap mParams;
    
    public ObexAppParameters() {
        this.mParams = new java.util.HashMap();
    }
    
    public ObexAppParameters(byte[] a) {
        this.mParams = new java.util.HashMap();
        if (a != null) {
            int i = 0;
            while(i < a.length && a.length - i >= 2) {
                int i0 = i + 1;
                int i1 = a[i];
                int i2 = i0 + 1;
                int i3 = a[i0];
                if (a.length - i2 - i3 < 0) {
                    break;
                }
                byte[] a0 = new byte[i3];
                System.arraycopy(a, i2, a0, 0, i3);
                this.add((byte)i1, a0);
                i = i2 + i3;
            }
        }
    }
    
    public static com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters fromHeaderSet(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters a0 = null;
        try {
            a0 = new com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters((byte[])a.getHeader(76));
        } catch(java.io.IOException ignoredException) {
            a0 = null;
        }
        return a0;
    }
    
    public void add(byte a, byte a0) {
        byte[] a1 = java.nio.ByteBuffer.allocate(1).put(a0).array();
        this.mParams.put(Byte.valueOf(a), a1);
    }
    
    public void add(byte a, int i) {
        byte[] a0 = java.nio.ByteBuffer.allocate(4).putInt(i).array();
        this.mParams.put(Byte.valueOf(a), a0);
    }
    
    public void add(byte a, long j) {
        byte[] a0 = java.nio.ByteBuffer.allocate(8).putLong(j).array();
        this.mParams.put(Byte.valueOf(a), a0);
    }
    
    public void add(byte a, String s) {
        byte[] a0 = s.getBytes();
        this.mParams.put(Byte.valueOf(a), a0);
    }
    
    public void add(byte a, short a0) {
        byte[] a1 = java.nio.ByteBuffer.allocate(2).putShort(a0).array();
        this.mParams.put(Byte.valueOf(a), a1);
    }
    
    public void add(byte a, byte[] a0) {
        this.mParams.put(Byte.valueOf(a), a0);
    }
    
    public void addToHeaderSet(com.navdy.hud.app.bluetooth.obex.HeaderSet a) {
        if (this.mParams.size() > 0) {
            a.setHeader(76, this.getHeader());
        }
    }
    
    public boolean exists(byte a) {
        return this.mParams.containsKey(Byte.valueOf(a));
    }
    
    public byte getByte(byte a) {
        int i = 0;
        byte[] a0 = (byte[])this.mParams.get(Byte.valueOf(a));
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.length >= 1) {
                        break label0;
                    }
                }
                i = 0;
                break label2;
            }
            i = java.nio.ByteBuffer.wrap(a0).get();
        }
        return (byte)i;
    }
    
    public byte[] getByteArray(byte a) {
        return (byte[])this.mParams.get(Byte.valueOf(a));
    }
    
    public byte[] getHeader() {
        java.util.Iterator a = this.mParams.entrySet().iterator();
        int i = 0;
        Object a0 = a;
        while(((java.util.Iterator)a0).hasNext()) {
            i = i + (((byte[])((java.util.Map.Entry)((java.util.Iterator)a0).next()).getValue()).length + 2);
        }
        byte[] a1 = new byte[i];
        java.util.Iterator a2 = this.mParams.entrySet().iterator();
        int i0 = 0;
        Object a3 = a2;
        while(((java.util.Iterator)a3).hasNext()) {
            Object a4 = ((java.util.Iterator)a3).next();
            int i1 = ((byte[])((java.util.Map.Entry)a4).getValue()).length;
            int i2 = i0 + 1;
            int i3 = ((Byte)((java.util.Map.Entry)a4).getKey()).byteValue();
            a1[i0] = (byte)i3;
            int i4 = i2 + 1;
            int i5 = (byte)i1;
            int i6 = (byte)i5;
            a1[i2] = (byte)i6;
            System.arraycopy(((java.util.Map.Entry)a4).getValue(), 0, a1, i4, i1);
            i0 = i4 + i1;
        }
        return a1;
    }
    
    public int getInt(byte a) {
        int i = 0;
        byte[] a0 = (byte[])this.mParams.get(Byte.valueOf(a));
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.length >= 4) {
                        break label0;
                    }
                }
                i = 0;
                break label2;
            }
            i = java.nio.ByteBuffer.wrap(a0).getInt();
        }
        return i;
    }
    
    public short getShort(byte a) {
        int i = 0;
        byte[] a0 = (byte[])this.mParams.get(Byte.valueOf(a));
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.length >= 2) {
                        break label0;
                    }
                }
                i = 0;
                break label2;
            }
            i = java.nio.ByteBuffer.wrap(a0).getShort();
        }
        return (short)i;
    }
    
    public String getString(byte a) {
        byte[] a0 = (byte[])this.mParams.get(Byte.valueOf(a));
        return (a0 != null) ? new String(a0) : null;
    }
    
    public String toString() {
        return this.mParams.toString();
    }
}
