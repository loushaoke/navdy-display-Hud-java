package com.navdy.hud.app.bluetooth.vcard;

final public class VCardParser_V21 extends com.navdy.hud.app.bluetooth.vcard.VCardParser {
    final static java.util.Set sAvailableEncoding;
    final static java.util.Set sKnownPropertyNameSet;
    final static java.util.Set sKnownTypeSet;
    final static java.util.Set sKnownValueSet;
    final private com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21 mVCardParserImpl;
    
    static {
        String[] a = new String[21];
        a[0] = "BEGIN";
        a[1] = "END";
        a[2] = "LOGO";
        a[3] = "PHOTO";
        a[4] = "LABEL";
        a[5] = "FN";
        a[6] = "TITLE";
        a[7] = "SOUND";
        a[8] = "VERSION";
        a[9] = "TEL";
        a[10] = "EMAIL";
        a[11] = "TZ";
        a[12] = "GEO";
        a[13] = "NOTE";
        a[14] = "URL";
        a[15] = "BDAY";
        a[16] = "ROLE";
        a[17] = "REV";
        a[18] = "UID";
        a[19] = "KEY";
        a[20] = "MAILER";
        sKnownPropertyNameSet = java.util.Collections.unmodifiableSet((java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a)));
        String[] a0 = new String[50];
        a0[0] = "DOM";
        a0[1] = "INTL";
        a0[2] = "POSTAL";
        a0[3] = "PARCEL";
        a0[4] = "HOME";
        a0[5] = "WORK";
        a0[6] = "PREF";
        a0[7] = "VOICE";
        a0[8] = "FAX";
        a0[9] = "MSG";
        a0[10] = "CELL";
        a0[11] = "PAGER";
        a0[12] = "BBS";
        a0[13] = "MODEM";
        a0[14] = "CAR";
        a0[15] = "ISDN";
        a0[16] = "VIDEO";
        a0[17] = "AOL";
        a0[18] = "APPLELINK";
        a0[19] = "ATTMAIL";
        a0[20] = "CIS";
        a0[21] = "EWORLD";
        a0[22] = "INTERNET";
        a0[23] = "IBMMAIL";
        a0[24] = "MCIMAIL";
        a0[25] = "POWERSHARE";
        a0[26] = "PRODIGY";
        a0[27] = "TLX";
        a0[28] = "X400";
        a0[29] = "GIF";
        a0[30] = "CGM";
        a0[31] = "WMF";
        a0[32] = "BMP";
        a0[33] = "MET";
        a0[34] = "PMB";
        a0[35] = "DIB";
        a0[36] = "PICT";
        a0[37] = "TIFF";
        a0[38] = "PDF";
        a0[39] = "PS";
        a0[40] = "JPEG";
        a0[41] = "QTIME";
        a0[42] = "MPEG";
        a0[43] = "MPEG2";
        a0[44] = "AVI";
        a0[45] = "WAVE";
        a0[46] = "AIFF";
        a0[47] = "PCM";
        a0[48] = "X509";
        a0[49] = "PGP";
        sKnownTypeSet = java.util.Collections.unmodifiableSet((java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a0)));
        String[] a1 = new String[4];
        a1[0] = "INLINE";
        a1[1] = "URL";
        a1[2] = "CONTENT-ID";
        a1[3] = "CID";
        sKnownValueSet = java.util.Collections.unmodifiableSet((java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a1)));
        String[] a2 = new String[5];
        a2[0] = "7BIT";
        a2[1] = "8BIT";
        a2[2] = "QUOTED-PRINTABLE";
        a2[3] = "BASE64";
        a2[4] = "B";
        sAvailableEncoding = java.util.Collections.unmodifiableSet((java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a2)));
    }
    
    public VCardParser_V21() {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21();
    }
    
    public VCardParser_V21(int i) {
        this.mVCardParserImpl = new com.navdy.hud.app.bluetooth.vcard.VCardParserImpl_V21(i);
    }
    
    public void addInterpreter(com.navdy.hud.app.bluetooth.vcard.VCardInterpreter a) {
        this.mVCardParserImpl.addInterpreter(a);
    }
    
    public void cancel() {
        this.mVCardParserImpl.cancel();
    }
    
    public void parse(java.io.InputStream a) {
        this.mVCardParserImpl.parse(a);
    }
    
    public void parseOne(java.io.InputStream a) {
        this.mVCardParserImpl.parseOne(a);
    }
}
