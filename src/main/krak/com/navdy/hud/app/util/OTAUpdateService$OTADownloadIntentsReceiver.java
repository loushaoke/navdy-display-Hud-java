package com.navdy.hud.app.util;

public class OTAUpdateService$OTADownloadIntentsReceiver extends android.content.BroadcastReceiver {
    private android.content.SharedPreferences sharedPreferences;
    
    public OTAUpdateService$OTADownloadIntentsReceiver(android.content.SharedPreferences a) {
        this.sharedPreferences = a;
    }
    
    static android.content.SharedPreferences access$800(com.navdy.hud.app.util.OTAUpdateService$OTADownloadIntentsReceiver a) {
        return a.sharedPreferences;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        String s = a0.getStringExtra("path");
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.OTAUpdateService$OTADownloadIntentsReceiver$1(this, s, a), 1);
    }
}
