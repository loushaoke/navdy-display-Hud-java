package com.navdy.hud.app.util;

public class ConversionUtil {
    final public static int MAX_KMPL = 45;
    final public static int MAX_MPG = 100;
    
    public ConversionUtil() {
    }
    
    public static double convertLpHundredKmToKMPL(double d) {
        return (d > 0.0) ? Math.min(100.0 / d, 45.0) : 0.0;
    }
    
    public static double convertLpHundredKmToMPG(double d) {
        return (d > 0.0) ? Math.min(235.214 / d, 100.0) : 0.0;
    }
}
