package com.navdy.hud.app.util;

class CrashReporter$6 implements Runnable {
    final com.navdy.hud.app.util.CrashReporter this$0;
    final Throwable val$throwable;
    
    CrashReporter$6(com.navdy.hud.app.util.CrashReporter a, Throwable a0) {
        super();
        this.this$0 = a;
        this.val$throwable = a0;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.util.CrashReporter.access$1100(this.this$0).saveException(this.val$throwable);
        } catch(Throwable a) {
            com.navdy.hud.app.util.CrashReporter.access$400().e(a);
        }
    }
}
