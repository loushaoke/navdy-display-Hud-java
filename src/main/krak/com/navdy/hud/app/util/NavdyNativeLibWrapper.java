package com.navdy.hud.app.util;

public class NavdyNativeLibWrapper {
    private static boolean loaded;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.NavdyNativeLibWrapper.class);
    }
    
    public NavdyNativeLibWrapper() {
    }
    
    native public static void crashSegv();
    
    
    native public static void crashSegvOnNewThread();
    
    
    public static void loadlibrary() {
        label0: synchronized(com.navdy.hud.app.util.NavdyNativeLibWrapper.class) {
        boolean b = loaded;
        //label0: {
        //    Throwable a = null;
            if (b) {
                break label0;
            }
       //     try {
                loaded = true;
                sLogger.v("loading navdy lib");
                System.loadLibrary("Navdy");
                sLogger.v("loaded navdy lib");
                break label0;
       //     } catch(Throwable a0) {
       //         a = a0;
        //    }
        //    try {
        //        sLogger.e(a);
        //    } catch(Throwable a1) {
        //        /*monexit(com.navdy.hud.app.util.NavdyNativeLibWrapper.class)*/;
        //        throw a1;
        //    }
        }
        /*monexit(com.navdy.hud.app.util.NavdyNativeLibWrapper.class)*/;
    }
    
    native public static synchronized void startCpuHog();
    
    
    native public static synchronized void stopCpuHog();
}
