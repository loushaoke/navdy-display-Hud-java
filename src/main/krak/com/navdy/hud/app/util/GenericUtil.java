package com.navdy.hud.app.util;
import com.navdy.hud.app.R;

public class GenericUtil {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.GenericUtil.class);
    }
    
    public GenericUtil() {
    }
    
    public static void checkNotOnMainThread() {
        if (android.os.Looper.myLooper() == android.os.Looper.getMainLooper()) {
            throw new RuntimeException("cannot call on mainthread");
        }
    }
    
    public static void clearInputMethodManagerFocusLeak() {
        android.view.inputmethod.InputMethodManager a = (android.view.inputmethod.InputMethodManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("input_method");
        try {
            java.lang.reflect.Method a0 = android.view.inputmethod.InputMethodManager.class.getDeclaredMethod("finishInputLocked", new Class[0]);
            a0.setAccessible(true);
            sLogger.v("calling finishInputLocked");
            a0.invoke(a, new Object[0]);
            sLogger.v("called finishInputLocked");
        } catch(Throwable a1) {
            sLogger.v("finishInputLocked", a1);
        }
    }
    
    public static int getDrawableResourceIdFromName(String s) {
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        return a.getResources().getIdentifier(s, "drawable", a.getPackageName());
    }
    
    public static String getErrorMessage(Throwable a) {
        String s = null;
        if (a != null) {
            s = a.getMessage();
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                s = a.toString();
            }
        } else {
            s = com.navdy.hud.app.HudApplication.getAppContext().getResources().getString(R.string.operation_failed);
        }
        return s;
    }
    
    public static int getIdsResourceIdFromName(String s) {
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        return a.getResources().getIdentifier(s, "id", a.getPackageName());
    }
    
    public static int getStringResourceIdFromName(String s) {
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        return a.getResources().getIdentifier(s, "string", a.getPackageName());
    }
    
    public static int indexOf(byte[] a, byte[] a0, int i, int i0) {
        int i1 = a0.length;
        label2: {
            label0: {
                label1: {
                    if (i1 == 0) {
                        break label1;
                    }
                    if (i0 - i + 1 >= a0.length) {
                        break label0;
                    }
                }
                i = -1;
                break label2;
            }
            int i2 = a.length - a0.length;
            if (i0 >= i2) {
                i0 = i2;
            }
            while(true) {
                if (i > i0) {
                    i = -1;
                    break;
                } else {
                    boolean b = false;
                    int i3 = 0;
                    while(true) {
                        if (i3 >= a0.length) {
                            b = true;
                            break;
                        } else {
                            int i4 = a[i + i3];
                            int i5 = a0[i3];
                            if (i4 == i5) {
                                i3 = i3 + 1;
                                continue;
                            }
                            b = false;
                            break;
                        }
                    }
                    if (b) {
                        break;
                    }
                    i = i + 1;
                }
            }
        }
        return i;
    }
    
    public static boolean isClientiOS() {
        boolean b = false;
        com.navdy.service.library.events.DeviceInfo a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.platform == com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static boolean isMainThread() {
        return android.os.Looper.myLooper() == android.os.Looper.getMainLooper();
    }
    
    public static String normalizeToFilename(String s) {
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            s = s.replaceAll("[^a-zA-Z0-9\\._]+", "");
        }
        return s;
    }
    
    public static String removePunctuation(String s) {
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            s = s.replaceAll("\\p{Punct}+", " ");
        }
        return s;
    }
    
    public static void sleep(int i) {
        try {
            Thread.sleep((long)i);
        } catch(Throwable ignoredException) {
        }
    }
}
