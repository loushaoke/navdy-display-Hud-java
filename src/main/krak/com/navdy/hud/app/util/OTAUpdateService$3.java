package com.navdy.hud.app.util;

class OTAUpdateService$3 implements Runnable {
    final com.navdy.hud.app.util.OTAUpdateService this$0;
    final com.navdy.hud.app.util.OTAUpdateService$OTAFailedException val$exception;
    
    OTAUpdateService$3(com.navdy.hud.app.util.OTAUpdateService a, com.navdy.hud.app.util.OTAUpdateService$OTAFailedException a0) {
        super();
        this.this$0 = a;
        this.val$exception = a0;
    }
    
    public void run() {
        com.navdy.hud.app.util.CrashReporter.getInstance().reportOTAFailure(this.val$exception);
    }
}
