package com.navdy.hud.app.util;

class OTAUpdateService$OTADownloadIntentsReceiver$1 implements Runnable {
    final com.navdy.hud.app.util.OTAUpdateService$OTADownloadIntentsReceiver this$0;
    final android.content.Context val$context;
    final String val$path;
    
    OTAUpdateService$OTADownloadIntentsReceiver$1(com.navdy.hud.app.util.OTAUpdateService$OTADownloadIntentsReceiver a, String s, android.content.Context a0) {
        super();
        this.this$0 = a;
        this.val$path = s;
        this.val$context = a0;
    }
    
    public void run() {
        java.io.File a = new java.io.File(this.val$path);
        if (com.navdy.hud.app.util.OTAUpdateService.isOtaFile(a.getName())) {
            String s = com.navdy.hud.app.util.OTAUpdateService$OTADownloadIntentsReceiver.access$800(this.this$0).getString("last_update_file_received", (String)null);
            String s0 = a.getAbsolutePath();
            com.navdy.hud.app.util.OTAUpdateService$OTADownloadIntentsReceiver.access$800(this.this$0).edit().putString("last_update_file_received", s0).remove("do_not_prompt").remove("retry_count").remove("verified").commit();
            if (!s0.equals(s)) {
                if (s != null) {
                    com.navdy.service.library.util.IOUtils.deleteFile(this.val$context, s);
                    com.navdy.hud.app.util.OTAUpdateService.access$000().d(new StringBuilder().append("Delete the old update file ").append(s).append(", new update :").append(s0).toString());
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordDownloadOTAUpdate(com.navdy.hud.app.util.OTAUpdateService$OTADownloadIntentsReceiver.access$800(this.this$0));
            }
            com.navdy.hud.app.util.OTAUpdateService.startServiceToVerifyUpdate();
        }
    }
}
