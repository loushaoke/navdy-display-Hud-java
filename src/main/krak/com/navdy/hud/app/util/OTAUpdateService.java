package com.navdy.hud.app.util;

public class OTAUpdateService extends android.app.Service {
    final public static int CAPTURE_GROUP_FROM_VERSION = 2;
    final public static int CAPTURE_GROUP_TARGET_VERSION = 3;
    final public static String COMMAND_CHECK_UPDATE_ON_REBOOT = "CHECK_UPDATE";
    final public static String COMMAND_DO_NOT_PROMPT = "DO_NOT_PROMPT";
    final public static String COMMAND_INSTALL_UPDATE = "INSTALL_UPDATE";
    final public static String COMMAND_INSTALL_UPDATE_REBOOT_QUIET = "INSTALL_UPDATE_REBOOT_QUIET";
    final public static String COMMAND_INSTALL_UPDATE_SHUTDOWN = "INSTALL_UPDATE_SHUTDOWN";
    final public static String COMMAND_VERIFY_UPDATE = "VERIFY_UPDATE";
    final public static String DO_NOT_PROMPT = "do_not_prompt";
    final public static String EXTRA_COMMAND = "COMMAND";
    final private static String FAILED_OTA_FILE = "failed_ota_file";
    final private static long MAX_FILE_SIZE = 2147483648L;
    final public static int MAX_RETRY_COUNT = 3;
    final private static int MAX_ZIP_ENTRIES = 1000;
    final public static String RECOVERY_LAST_INSTALL_FILE_PATH = "/cache/recovery/last_install";
    final private static String RECOVERY_LAST_LOG_FILE_PATH = "/cache/recovery/last_log";
    final public static String RETRY_COUNT = "retry_count";
    final public static String UPDATE_FILE = "last_update_file_received";
    final private static String UPDATE_FROM_VERSION = "update_from_version";
    final public static String VERIFIED = "verified";
    final public static String VERSION_PREFIX = "1.0.";
    final private static java.util.regex.Pattern sFileNamePattern;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static String swVersion;
    @Inject
    com.squareup.otto.Bus bus;
    private boolean installing;
    @Inject
    android.content.SharedPreferences sharedPreferences;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.OTAUpdateService.class);
        sFileNamePattern = java.util.regex.Pattern.compile("^navdy_ota_(([0-9]+)_to_)?([0-9]+)\\.zip");
    }
    
    public OTAUpdateService() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static java.io.File access$100(android.content.Context a, android.content.SharedPreferences a0) {
        return com.navdy.hud.app.util.OTAUpdateService.checkUpdateFile(a, a0);
    }
    
    static void access$200(com.navdy.hud.app.util.OTAUpdateService a, android.content.Context a0, java.io.File a1, String s) {
        a.installPackage(a0, a1, s);
    }
    
    static boolean access$302(com.navdy.hud.app.util.OTAUpdateService a, boolean b) {
        a.installing = b;
        return b;
    }
    
    static boolean access$400(com.navdy.hud.app.util.OTAUpdateService a, java.io.File a0) {
        return a.checkIfIncrementalUpdatesFailed(a0);
    }
    
    static String access$500() {
        return swVersion;
    }
    
    static String access$502(String s) {
        swVersion = s;
        return s;
    }
    
    static String access$600(android.content.SharedPreferences a) {
        return com.navdy.hud.app.util.OTAUpdateService.getUpdateVersion(a);
    }
    
    static int access$700(String s) {
        return com.navdy.hud.app.util.OTAUpdateService.extractIncrementalVersion(s);
    }
    
    private boolean checkIfIncrementalUpdatesFailed(java.io.File a) {
        boolean b = false;
        java.io.File a0 = new java.io.File("/cache/recovery/last_install");
        boolean b0 = a0.exists();
        label0: {
            java.io.IOException a1 = null;
            if (b0) {
                try {
                    String s = com.navdy.service.library.util.IOUtils.convertFileToString(a0.getAbsolutePath());
                    if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                        b = true;
                        break label0;
                    } else {
                        String[] a2 = s.split("\n");
                        if (a2.length < 2) {
                            b = true;
                            break label0;
                        } else {
                            String s0 = a2[0];
                            sLogger.d(new StringBuilder().append("Last installed update was ").append(s0).toString());
                            String s1 = new java.io.File(s0).getName();
                            boolean b1 = Integer.parseInt(a2[1]) != 0;
                            if (!b1) {
                                this.reportOTAFailure(s1);
                            }
                            sLogger.d(new StringBuilder().append("Last install succeeded: ").append(b1 ? "1" : "0").toString());
                            int i = com.navdy.hud.app.util.OTAUpdateService.extractFromVersion(s1);
                            boolean b2 = i != -1;
                            String s2 = this.sharedPreferences.getString("update_from_version", "");
                            if (!android.text.TextUtils.isEmpty((CharSequence)s2)) {
                                com.navdy.hud.app.analytics.AnalyticsSupport.recordOTAInstallResult(b2, s2, b1);
                                this.sharedPreferences.edit().remove("update_from_version").commit();
                            }
                            if (b2) {
                                int i0 = 0;
                                sLogger.d("Last install was an incremental update");
                                String s3 = android.os.Build.VERSION.INCREMENTAL;
                                try {
                                    i0 = Integer.parseInt(s3);
                                } catch(NumberFormatException ignoredException) {
                                    sLogger.e(new StringBuilder().append("Cannot parse the incremental version of the build :").append(android.os.Build.VERSION.INCREMENTAL).toString());
                                    i0 = 0;
                                }
                                if (i0 != i) {
                                    b = true;
                                    break label0;
                                } else {
                                    sLogger.d(new StringBuilder().append("The last installed incremental from version is same as current version :").append(i0).toString());
                                    if (b1) {
                                        b = true;
                                        break label0;
                                    } else {
                                        sLogger.e("Last incremental update has failed");
                                        this.bus.post(new com.navdy.hud.app.util.OTAUpdateService$IncrementalOTAFailureDetected());
                                        if (a == null) {
                                            b = true;
                                            break label0;
                                        } else if (a.getAbsolutePath().equals(s0)) {
                                            sLogger.d("Current update is the same as the one failed during last install");
                                            b = false;
                                            break label0;
                                        } else {
                                            b = true;
                                            break label0;
                                        }
                                    }
                                }
                            } else {
                                b = true;
                                break label0;
                            }
                        }
                    }
                } catch(java.io.IOException a3) {
                    a1 = a3;
                }
            } else {
                b = true;
                break label0;
            }
            sLogger.e(new StringBuilder().append("Error reading the file :").append(a0.getAbsolutePath()).toString(), (Throwable)a1);
            b = true;
        }
        return b;
    }
    
    private static java.io.File checkUpdateFile(android.content.Context a, android.content.SharedPreferences a0) {
        java.io.File a1 = null;
        sLogger.d("Checking for update file");
        swVersion = null;
        String s = a0.getString("last_update_file_received", (String)null);
        label1: if (s != null) {
            int i = 0;
            a1 = new java.io.File(s);
            boolean b = a1.isFile();
            label2: {
                label3: {
                    if (!b) {
                        break label3;
                    }
                    if (!a1.exists()) {
                        break label3;
                    }
                    if (a1.canRead()) {
                        break label2;
                    }
                }
                sLogger.e("Invalid update file");
                a1 = null;
                break label1;
            }
            try {
                i = Integer.parseInt(android.os.Build.VERSION.INCREMENTAL);
            } catch(NumberFormatException ignoredException) {
                sLogger.e(new StringBuilder().append("Cannot parse the incremental version of the build :").append(android.os.Build.VERSION.INCREMENTAL).toString());
                i = 0;
            }
            sLogger.d(new StringBuilder().append("Incremental version of the build on the device:").append(i).toString());
            int i0 = com.navdy.hud.app.util.OTAUpdateService.extractIncrementalVersion(a1.getName());
            int i1 = com.navdy.hud.app.util.OTAUpdateService.extractFromVersion(a1.getName());
            sLogger.d(new StringBuilder().append("Update from version ").append(i1).toString());
            label0: {
                if (i1 == -1) {
                    break label0;
                }
                if (i == 0) {
                    break label0;
                }
                if (i1 == i) {
                    break label0;
                }
                sLogger.e(new StringBuilder().append("Update from version mismatch ").append(i1).append(" ").append(i).toString());
                com.navdy.hud.app.util.OTAUpdateService.clearUpdate(a1, a, a0);
                a1 = null;
                break label1;
            }
            if (i0 > i) {
                if (a0.getInt("retry_count", 0) >= 3) {
                    sLogger.d("Maximum retries. Deleting the update file.");
                    com.navdy.hud.app.util.OTAUpdateService.clearUpdate(a1, a, a0);
                    a1 = null;
                }
            } else {
                sLogger.e(new StringBuilder().append("Already up to date :").append(i).toString());
                com.navdy.hud.app.util.OTAUpdateService.clearUpdate(a1, a, a0);
                a1 = null;
            }
        } else {
            sLogger.e("No update available");
            a1 = null;
        }
        return a1;
    }
    
    private void cleanupOldFiles() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.OTAUpdateService$4(this), 1);
    }
    
    public static void clearUpdate(java.io.File a, android.content.Context a0, android.content.SharedPreferences a1) {
        if (a != null) {
            sLogger.d("Clearing the update");
            com.navdy.service.library.util.IOUtils.deleteFile(a0, a.getAbsolutePath());
        }
        a1.edit().remove("last_update_file_received").remove("retry_count").remove("do_not_prompt").remove("verified").commit();
    }
    
    private static int extractFromVersion(String s) {
        int i = 0;
        java.util.regex.Matcher a = sFileNamePattern.matcher((CharSequence)s);
        boolean b = a.find();
        label0: {
            NumberFormatException a0 = null;
            label1: if (b) {
                if (a.groupCount() <= 0) {
                    i = -1;
                    break label0;
                } else {
                    String s0 = a.group(2);
                    if (s0 != null) {
                        try {
                            i = Integer.parseInt(s0);
                        } catch(NumberFormatException a1) {
                            a0 = a1;
                            break label1;
                        }
                        break label0;
                    } else {
                        i = -1;
                        break label0;
                    }
                }
            } else {
                i = -1;
                break label0;
            }
            sLogger.e(new StringBuilder().append("Cannot parse the file to retrieve the from version").append(a0).toString());
            a0.printStackTrace();
            i = -1;
        }
        return i;
    }
    
    private static int extractIncrementalVersion(String s) {
        int i = 0;
        java.util.regex.Matcher a = sFileNamePattern.matcher((CharSequence)s);
        boolean b = a.find();
        label0: {
            label1: {
                NumberFormatException a0 = null;
                if (!b) {
                    break label1;
                }
                if (a.groupCount() <= 0) {
                    break label1;
                }
                String s0 = a.group(3);
                try {
                    i = Integer.parseInt(s0);
                    break label0;
                } catch(NumberFormatException a1) {
                    a0 = a1;
                }
                sLogger.e(new StringBuilder().append("Cannot parse the file to retrieve the target version").append(a0).toString());
                a0.printStackTrace();
            }
            i = -1;
        }
        return i;
    }
    
    public static String getCurrentVersion() {
        return com.navdy.hud.app.util.OTAUpdateService.shortVersion("1.3.3051-corona");
    }
    
    public static String getIncrementalUpdateVersion(android.content.SharedPreferences a) {
        String s = null;
        String s0 = a.getString("last_update_file_received", (String)null);
        if (s0 != null) {
            int i = com.navdy.hud.app.util.OTAUpdateService.extractIncrementalVersion(new java.io.File(s0).getName());
            s = null;
            if (i > 0) {
                s = String.valueOf(i);
            }
        } else {
            s = null;
        }
        return s;
    }
    
    public static String getSWUpdateVersion() {
        return swVersion;
    }
    
    private static String getUpdateVersion(android.content.SharedPreferences a) {
        String s = a.getString("last_update_file_received", (String)null);
        String s0 = null;
        label0: {
            java.io.InputStream a0 = null;
            java.util.zip.ZipFile a1 = null;
            Throwable a2 = null;
            if (s == null) {
                break label0;
            }
            java.io.File a3 = new java.io.File(s);
            label1: {
                java.util.zip.ZipFile a4 = null;
                java.io.InputStream a5 = null;
                label2: {
                    {
                        label7: {
                            java.io.InputStream a6 = null;
                            label6: {
                                label3: try {
                                    java.io.IOException a7 = null;
                                    a0 = null;
                                    a1 = null;
                                    label5: {
                                        label4: {
                                            try {
                                                a0 = null;
                                                a1 = null;
                                                a4 = new java.util.zip.ZipFile(a3);
                                                break label4;
                                            } catch(java.io.IOException a8) {
                                                a7 = a8;
                                            }
                                            a4 = null;
                                            a6 = null;
                                            s0 = null;
                                            break label5;
                                        }
                                        try {
                                            try {
                                                a5 = null;
                                                s0 = null;
                                                a6 = null;
                                                java.util.zip.ZipEntry a9 = a4.getEntry("META-INF/com/android/metadata");
                                                a5 = null;
                                                a5 = null;
                                                s0 = null;
                                                a6 = null;
                                                java.util.Properties a10 = new java.util.Properties();
                                                a5 = null;
                                                s0 = null;
                                                a6 = null;
                                                a5 = a4.getInputStream(a9);
                                                s0 = null;
                                                a6 = a5;
                                                a10.load(a5);
                                                s0 = null;
                                                a6 = a5;
                                                s0 = a10.getProperty("version-name");
                                                a6 = a5;
                                                s0 = com.navdy.hud.app.util.OTAUpdateService.shortVersion(s0);
                                                break label3;
                                            } catch(java.io.IOException a11) {
                                                a7 = a11;
                                            }
                                        } catch(Throwable a12) {
                                            a2 = a12;
                                            break label2;
                                        }
                                    }
                                    com.navdy.service.library.log.Logger a13 = sLogger;
                                    a0 = a6;
                                    a1 = a4;
                                    a13.w("Error extracting version-name from ota metadata ", (Throwable)a7);
                                    break label6;
                                } catch(Throwable a14) {
                                    a2 = a14;
                                    break label1;
                                }
                                com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a5);
                                com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a4);
                                break label7;
                            }
                            com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a6);
                            com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a4);
                        }
                        if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                            break label0;
                        }
                        s0 = new StringBuilder().append("1.0.").append(com.navdy.hud.app.util.OTAUpdateService.getIncrementalUpdateVersion(a)).toString();
                        break label0;
                    }
                }
                a0 = a5;
                a1 = a4;
            }
            com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
            com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a1);
            throw a2;
        }
        return s0;
    }
    
    private void installPackage(android.content.Context a, java.io.File a0, String s) {
        com.navdy.hud.app.analytics.AnalyticsSupport.recordShutdown(com.navdy.hud.app.event.Shutdown$Reason.OTA, false);
        label0: {
            java.lang.reflect.InvocationTargetException a1 = null;
            try {
                try {
                    Class[] a2 = new Class[3];
                    a2[0] = android.content.Context.class;
                    a2[1] = java.io.File.class;
                    a2[2] = String.class;
                    java.lang.reflect.Method a3 = android.os.RecoverySystem.class.getMethod("installPackage", a2);
                    Object[] a4 = new Object[3];
                    a4[0] = a;
                    a4[1] = a0;
                    a4[2] = s;
                    a3.invoke(null, a4);
                    break label0;
                } catch(java.lang.reflect.InvocationTargetException a5) {
                    a1 = a5;
                }
            } catch(Exception a6) {
                sLogger.e(new StringBuilder().append("error invoking Recovery.installPackage(): ").append(a6).toString());
                android.os.RecoverySystem.installPackage(a, a0);
                break label0;
            }
            Throwable a7 = a1.getCause();
            if (a7 instanceof java.io.IOException) {
                throw (java.io.IOException)a7;
            }
            sLogger.e(new StringBuilder().append("unexpected exception from RecoverySystem.installPackage()").append(a7).toString());
        }
    }
    
    public static boolean isOTAUpdateVerified(android.content.SharedPreferences a) {
        return a.getBoolean("verified", false);
    }
    
    public static boolean isOtaFile(String s) {
        boolean b = false;
        java.util.regex.Matcher a = sFileNamePattern.matcher((CharSequence)s);
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.find()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static boolean isUpdateAvailable() {
        return !android.text.TextUtils.isEmpty((CharSequence)swVersion);
    }
    
    private void reportOTAFailure(String s) {
        if (s.equals(this.sharedPreferences.getString("failed_ota_file", ""))) {
            sLogger.v(new StringBuilder().append("update failure already reported for ").append(s).toString());
        } else {
            String s0 = null;
            String s1 = null;
            try {
                s0 = new StringBuilder().append("\n========== /cache/recovery/last_install\n").append(com.navdy.service.library.util.IOUtils.convertFileToString("/cache/recovery/last_install")).toString();
            } catch(java.io.IOException a) {
                sLogger.e("exception reading /cache/recovery/last_install", (Throwable)a);
                s0 = "\n*** failed to read /cache/recovery/last_install";
            }
            try {
                s1 = new StringBuilder().append("\n========== /cache/recovery/last_log\n").append(com.navdy.service.library.util.IOUtils.convertFileToString("/cache/recovery/last_log")).toString();
            } catch(java.io.IOException a0) {
                sLogger.e("exception reading /cache/recovery/last_log", (Throwable)a0);
                s1 = "\n*** failed to read /cache/recovery/last_log";
            }
            com.navdy.hud.app.util.OTAUpdateService$OTAFailedException a1 = new com.navdy.hud.app.util.OTAUpdateService$OTAFailedException(s0, s1);
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.OTAUpdateService$3(this, a1), 1);
            this.sharedPreferences.edit().putString("failed_ota_file", s).commit();
        }
    }
    
    public static String shortVersion(String s) {
        String s0 = (s == null) ? null : s.split("-")[0];
        return s0;
    }
    
    public static void startServiceToVerifyUpdate() {
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.Intent a0 = new android.content.Intent(a, com.navdy.hud.app.util.OTAUpdateService.class);
        a0.putExtra("COMMAND", "VERIFY_UPDATE");
        a.startService(a0);
    }
    
    private void update(String s) {
        if (!this.installing) {
            this.installing = true;
            this.bus.post(new com.navdy.hud.app.util.OTAUpdateService$InstallingUpdate());
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.OTAUpdateService$1(this, s), 1);
        }
    }
    
    public static boolean verifyOTAZipFile(java.io.File a, int i, long j) {
        boolean b = false;
        label10: {
            java.util.zip.ZipFile a0 = null;
            label5: {
                label4: {
                    label3: {
                        label9: {
                            label6: {
                                java.util.zip.ZipFile a1 = null;
                                Throwable a2 = null;
                                try {
                                    label0: {
                                        java.io.IOException a3 = null;
                                        label8: {
                                            java.io.File a4 = null;
                                            String s = null;
                                            label7: {
                                                try {
                                                    a1 = null;
                                                    a4 = a.getParentFile();
                                                    s = a4.getCanonicalPath();
                                                    a0 = new java.util.zip.ZipFile(a);
                                                    break label7;
                                                } catch(java.io.IOException a5) {
                                                    a3 = a5;
                                                }
                                                a0 = null;
                                                break label8;
                                            }
                                            try {
                                                try {
                                                    Object a6 = a0.entries();
                                                    int i0 = 0;
                                                    long j0 = 0L;
                                                    label2: {
                                                        label1: {
                                                            java.util.zip.ZipEntry a7 = null;
                                                            String s0 = null;
                                                            while(true) {
                                                                if (!((java.util.Enumeration)a6).hasMoreElements()) {
                                                                    break label3;
                                                                }
                                                                a7 = (java.util.zip.ZipEntry)((java.util.Enumeration)a6).nextElement();
                                                                i0 = i0 + 1;
                                                                if (!a7.isDirectory()) {
                                                                    long j1 = a7.getSize();
                                                                    if (j1 > 0L) {
                                                                        j0 = j0 + j1;
                                                                    }
                                                                }
                                                                if (i0 > i) {
                                                                    break label2;
                                                                }
                                                                if (j0 > j) {
                                                                    break label1;
                                                                }
                                                                s0 = new java.io.File(a4, a7.getName()).getCanonicalPath();
                                                                if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                                                                    break;
                                                                }
                                                                if (!s0.startsWith(s)) {
                                                                    break;
                                                                }
                                                            }
                                                            sLogger.d(new StringBuilder().append("OTA zip failed verification : Zip entry tried to write outside destination ").append(a7.getName()).append(" , Canonical path ").append(s0).toString());
                                                            break label4;
                                                        }
                                                        sLogger.d(new StringBuilder().append("OTA zip file failed verification : Exceeded max uncompressed size ").append(j).toString());
                                                        break label5;
                                                    }
                                                    sLogger.d(new StringBuilder().append("OTA zip file failed verification : Too many entries ").append(i0).toString());
                                                    break label6;
                                                } catch(java.io.IOException a8) {
                                                    a3 = a8;
                                                }
                                            } catch(Throwable a9) {
                                                a2 = a9;
                                                break label0;
                                            }
                                        }
                                        com.navdy.service.library.log.Logger a10 = sLogger;
                                        a1 = a0;
                                        a10.e(new StringBuilder().append("Exception while verifying the OTA package ").append(a3).toString());
                                        a3.printStackTrace();
                                        break label9;
                                    }
                                    a1 = a0;
                                } catch(Throwable a11) {
                                    a2 = a11;
                                }
                                com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a1);
                                throw a2;
                            }
                            com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
                            b = false;
                            break label10;
                        }
                        com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
                        b = false;
                        break label10;
                    }
                    com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
                    b = true;
                    break label10;
                }
                com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
                b = false;
                break label10;
            }
            com.navdy.service.library.util.IOUtils.closeObject((java.io.Closeable)a0);
            b = false;
        }
        return b;
    }
    
    public boolean bVerifyUpdate(java.io.File a, android.content.SharedPreferences a0) {
        boolean b = false;
        label0: {
            Exception a1 = null;
            try {
                java.io.IOException a2 = null;
                try {
                    android.os.RecoverySystem.verifyPackage(a, (android.os.RecoverySystem$ProgressListener)new com.navdy.hud.app.util.OTAUpdateService$5(this), (java.io.File)null);
                    com.navdy.hud.app.util.OTAUpdateService.verifyOTAZipFile(a, 1000, 2147483648L);
                    b = true;
                    break label0;
                } catch(java.io.IOException a3) {
                    a2 = a3;
                }
                a1 = a2;
            } catch(java.security.GeneralSecurityException a4) {
                a1 = a4;
            }
            sLogger.e(new StringBuilder().append("Exception while verifying the update package ").append(a1).toString());
            b = false;
        }
        return b;
    }
    
    public void checkForUpdate() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.OTAUpdateService$2(this), 1);
    }
    
    public android.os.IBinder onBind(android.content.Intent a) {
        return null;
    }
    
    public void onCreate() {
        super.onCreate();
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
    }
    
    public int onStartCommand(android.content.Intent a, int i, int i0) {
        super.onStartCommand(a, i, i0);
        String s = a.getStringExtra("COMMAND");
        sLogger.d(new StringBuilder().append("OTA Update service started command[").append(s).append("]").toString());
        if (a.hasExtra("COMMAND")) {
            int i1 = 0;
            switch(s.hashCode()) {
                case 1870386340: {
                    i1 = (s.equals("DO_NOT_PROMPT")) ? 3 : -1;
                    break;
                }
                case 1813668687: {
                    i1 = (s.equals("VERIFY_UPDATE")) ? 4 : -1;
                    break;
                }
                case 227671624: {
                    i1 = (s.equals("INSTALL_UPDATE_SHUTDOWN")) ? 1 : -1;
                    break;
                }
                case -1672139796: {
                    i1 = (s.equals("INSTALL_UPDATE_REBOOT_QUIET")) ? 0 : -1;
                    break;
                }
                case -2036275187: {
                    i1 = (s.equals("INSTALL_UPDATE")) ? 2 : -1;
                    break;
                }
                default: {
                    i1 = -1;
                }
            }
            switch(i1) {
                case 4: {
                    this.checkForUpdate();
                    this.cleanupOldFiles();
                    break;
                }
                case 3: {
                    this.sharedPreferences.edit().putBoolean("do_not_prompt", true).commit();
                    break;
                }
                case 2: {
                    this.update((String)null);
                    break;
                }
                case 1: {
                    this.update("--shutdown_after");
                    break;
                }
                case 0: {
                    this.update("--reboot_quiet_after");
                    break;
                }
                default: {
                    sLogger.e(new StringBuilder().append("onStartCommand() invalid command: ").append(s).toString());
                }
            }
        }
        return 2;
    }
}
