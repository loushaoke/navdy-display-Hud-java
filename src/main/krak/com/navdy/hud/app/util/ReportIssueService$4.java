package com.navdy.hud.app.util;

final class ReportIssueService$4 implements java.util.Comparator {
    ReportIssueService$4() {
    }
    
    public int compare(java.io.File a, java.io.File a0) {
        return Long.compare(a.lastModified(), a0.lastModified());
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((java.io.File)a, (java.io.File)a0);
    }
}
