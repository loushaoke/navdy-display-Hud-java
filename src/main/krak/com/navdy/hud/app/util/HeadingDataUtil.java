package com.navdy.hud.app.util;

final public class HeadingDataUtil {
    final private int HEADING_EXPIRE_INTERVAL;
    private double heading;
    private double lastHeading;
    private long lastHeadingSampleTime;
    
    public HeadingDataUtil() {
        this.HEADING_EXPIRE_INTERVAL = 3000;
    }
    
    final public int getHEADING_EXPIRE_INTERVAL() {
        return this.HEADING_EXPIRE_INTERVAL;
    }
    
    final public double getHeading() {
        return this.heading;
    }
    
    final public double getLastHeading() {
        return this.lastHeading;
    }
    
    final public long getLastHeadingSampleTime() {
        return this.lastHeadingSampleTime;
    }
    
    final public void reset() {
        this.lastHeading = 0.0;
        this.lastHeadingSampleTime = android.os.SystemClock.elapsedRealtime();
    }
    
    final public void setHeading(double d) {
        com.navdy.hud.app.manager.SpeedManager a = com.navdy.hud.app.manager.SpeedManager.getInstance();
        long j = android.os.SystemClock.elapsedRealtime();
        label0: if (a.getCurrentSpeed() >= 1) {
            double d0 = this.lastHeading;
            int i = (d0 < 0.0) ? -1 : (d0 == 0.0) ? 0 : 1;
            label1: {
                if (i == 0) {
                    break label1;
                }
                if (Math.abs(this.heading - this.lastHeading) < (double)120) {
                    break label1;
                }
                if (j - this.lastHeadingSampleTime <= (long)this.HEADING_EXPIRE_INTERVAL) {
                    break label0;
                }
            }
            this.lastHeading = d;
            this.lastHeadingSampleTime = android.os.SystemClock.elapsedRealtime();
            this.heading = d;
        }
    }
    
    final public void setLastHeading(double d) {
        this.lastHeading = d;
    }
    
    final public void setLastHeadingSampleTime(long j) {
        this.lastHeadingSampleTime = j;
    }
}
