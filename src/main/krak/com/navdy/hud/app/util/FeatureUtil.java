package com.navdy.hud.app.util;

final public class FeatureUtil {
    final public static String FEATURE_ACTION = "com.navdy.hud.app.feature";
    final public static String FEATURE_FUEL_ROUTING = "com.navdy.hud.app.feature.fuelRouting";
    final public static String FEATURE_GESTURE_COLLECTOR = "com.navdy.hud.app.feature.gestureCollector";
    final public static String FEATURE_GESTURE_ENGINE = "com.navdy.hud.app.feature.gestureEngine";
    final public static String FEATURE_VOICE_SEARCH_ADDITIONAL_RESULTS_PROPERTY = "persist.voice.search.additional.results";
    final public static String FEATURE_VOICE_SEARCH_LIST_PROPERTY = "persist.sys.voicesearch.list";
    final private static String GESTURE_CALIBRATED_PROPERTY = "persist.sys.swiped.calib";
    final private static com.navdy.service.library.log.Logger sLogger;
    private java.util.HashSet featuresEnabledMap;
    private android.content.BroadcastReceiver receiver;
    private android.content.SharedPreferences sharedPreferences;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.FeatureUtil.class);
    }
    
    public FeatureUtil(android.content.SharedPreferences a) {
        super();
        boolean b = false;
        this.featuresEnabledMap = new java.util.HashSet();
        this.receiver = new com.navdy.hud.app.util.FeatureUtil$1(this);
        this.sharedPreferences = a;
        boolean b0 = !android.text.TextUtils.isEmpty((CharSequence)com.navdy.hud.app.util.os.SystemProperties.get("persist.sys.swiped.calib", ""));
        boolean b1 = a.getBoolean("gesture.engine", true);
        label2: {
            label0: {
                label1: {
                    if (b1) {
                        break label1;
                    }
                    if (!b0) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.voicesearch.list", false);
        com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.voice.search.additional.results", true);
        if (b) {
            this.featuresEnabledMap.add(com.navdy.hud.app.util.FeatureUtil$Feature.GESTURE_ENGINE);
            sLogger.v("enabled gesture engine");
            if (this.isGestureCollectorInstalled()) {
                this.featuresEnabledMap.add(com.navdy.hud.app.util.FeatureUtil$Feature.GESTURE_COLLECTOR);
                sLogger.v("enabled gesture collector");
            }
        } else {
            sLogger.v("not enabled gesture engine");
        }
        this.featuresEnabledMap.add(com.navdy.hud.app.util.FeatureUtil$Feature.FUEL_ROUTING);
        android.content.IntentFilter a0 = new android.content.IntentFilter("com.navdy.hud.app.feature");
        com.navdy.hud.app.HudApplication.getAppContext().registerReceiver(this.receiver, a0);
    }
    
    static com.navdy.hud.app.util.FeatureUtil$Feature access$000(com.navdy.hud.app.util.FeatureUtil a, String s) {
        return a.getFeatureFromName(s);
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static android.content.SharedPreferences access$200(com.navdy.hud.app.util.FeatureUtil a) {
        return a.sharedPreferences;
    }
    
    static boolean access$300(com.navdy.hud.app.util.FeatureUtil a) {
        return a.isGestureCollectorInstalled();
    }
    
    static void access$400(com.navdy.hud.app.util.FeatureUtil a, com.navdy.hud.app.util.FeatureUtil$Feature a0, boolean b) {
        a.setFeature(a0, b);
    }
    
    private com.navdy.hud.app.util.FeatureUtil$Feature getFeatureFromName(String s) {
        com.navdy.hud.app.util.FeatureUtil$Feature a = null;
        if (s != null) {
            int i = 0;
            switch(s.hashCode()) {
                case -23581364: {
                    i = (s.equals("com.navdy.hud.app.feature.gestureEngine")) ? 1 : -1;
                    break;
                }
                case -1149811389: {
                    i = (s.equals("com.navdy.hud.app.feature.gestureCollector")) ? 0 : -1;
                    break;
                }
                case -1256323375: {
                    i = (s.equals("com.navdy.hud.app.feature.fuelRouting")) ? 2 : -1;
                    break;
                }
                default: {
                    i = -1;
                }
            }
            switch(i) {
                case 2: {
                    a = com.navdy.hud.app.util.FeatureUtil$Feature.FUEL_ROUTING;
                    break;
                }
                case 1: {
                    a = com.navdy.hud.app.util.FeatureUtil$Feature.GESTURE_ENGINE;
                    break;
                }
                case 0: {
                    a = com.navdy.hud.app.util.FeatureUtil$Feature.GESTURE_COLLECTOR;
                    break;
                }
                default: {
                    a = null;
                }
            }
        } else {
            a = null;
        }
        return a;
    }
    
    private boolean isGestureCollectorInstalled() {
        boolean b = false;
        try {
            com.navdy.hud.app.HudApplication.getAppContext().getPackageManager().getPackageInfo("com.navdy.collector", 1);
            b = true;
        } catch(Throwable a) {
            if (a instanceof android.content.pm.PackageManager$NameNotFoundException) {
                sLogger.v("gesture collector not found");
            } else {
                sLogger.v("gesture collector not enabled", a);
            }
            b = false;
        }
        return b;
    }
    
    private void setFeature(com.navdy.hud.app.util.FeatureUtil$Feature a, boolean b) {
        synchronized(this) {
            if (b) {
                this.featuresEnabledMap.add(a);
                sLogger.v(new StringBuilder().append("enabled featured:").append(a).toString());
            } else {
                this.featuresEnabledMap.remove(a);
                sLogger.v(new StringBuilder().append("disabled featured:").append(a).toString());
            }
        }
        /*monexit(this)*/;
    }
    
    public boolean isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil$Feature a) {
        boolean b = false;
        synchronized(this) {
            b = this.featuresEnabledMap.contains(a);
        }
        /*monexit(this)*/;
        return b;
    }
}
