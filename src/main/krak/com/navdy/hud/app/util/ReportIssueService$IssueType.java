package com.navdy.hud.app.util;
import com.navdy.hud.app.R;


public enum ReportIssueService$IssueType {
    INEFFICIENT_ROUTE_ETA_TRAFFIC(0),
    INEFFICIENT_ROUTE_SELECTED(1),
    ROAD_CLOSED(2),
    WRONG_DIRECTION(3),
    ROAD_NAME(4),
    ROAD_CLOSED_PERMANENT(5),
    OTHER(6);

    private int value;
    ReportIssueService$IssueType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class ReportIssueService$IssueType extends Enum {
//    final private static com.navdy.hud.app.util.ReportIssueService$IssueType[] $VALUES;
//    final public static com.navdy.hud.app.util.ReportIssueService$IssueType INEFFICIENT_ROUTE_ETA_TRAFFIC;
//    final public static com.navdy.hud.app.util.ReportIssueService$IssueType INEFFICIENT_ROUTE_SELECTED;
//    final public static com.navdy.hud.app.util.ReportIssueService$IssueType OTHER;
//    final public static com.navdy.hud.app.util.ReportIssueService$IssueType ROAD_CLOSED;
//    final public static com.navdy.hud.app.util.ReportIssueService$IssueType ROAD_CLOSED_PERMANENT;
//    final public static com.navdy.hud.app.util.ReportIssueService$IssueType ROAD_NAME;
//    final public static com.navdy.hud.app.util.ReportIssueService$IssueType WRONG_DIRECTION;
//    private int issueTypeCode;
//    private int messageStringResource;
//    private int titleStringResource;
//    
//    static {
//        INEFFICIENT_ROUTE_ETA_TRAFFIC = new com.navdy.hud.app.util.ReportIssueService$IssueType("INEFFICIENT_ROUTE_ETA_TRAFFIC", 0, R.string.eta_inaccurate_traffic, 0, 107);
//        INEFFICIENT_ROUTE_SELECTED = new com.navdy.hud.app.util.ReportIssueService$IssueType("INEFFICIENT_ROUTE_SELECTED", 1, R.string.route_inefficient, 0, 106);
//        ROAD_CLOSED = new com.navdy.hud.app.util.ReportIssueService$IssueType("ROAD_CLOSED", 2, R.string.road_closed, R.string.road_closed_message, 103);
//        WRONG_DIRECTION = new com.navdy.hud.app.util.ReportIssueService$IssueType("WRONG_DIRECTION", 3, R.string.wrong_direction, 0, 102);
//        ROAD_NAME = new com.navdy.hud.app.util.ReportIssueService$IssueType("ROAD_NAME", 4, R.string.road_name, 0, 104);
//        ROAD_CLOSED_PERMANENT = new com.navdy.hud.app.util.ReportIssueService$IssueType("ROAD_CLOSED_PERMANENT", 5, R.string.permanent_closure, 0, 108);
//        OTHER = new com.navdy.hud.app.util.ReportIssueService$IssueType("OTHER", 6, R.string.other_navigation_issue, 0, 105);
//        com.navdy.hud.app.util.ReportIssueService$IssueType[] a = new com.navdy.hud.app.util.ReportIssueService$IssueType[7];
//        a[0] = INEFFICIENT_ROUTE_ETA_TRAFFIC;
//        a[1] = INEFFICIENT_ROUTE_SELECTED;
//        a[2] = ROAD_CLOSED;
//        a[3] = WRONG_DIRECTION;
//        a[4] = ROAD_NAME;
//        a[5] = ROAD_CLOSED_PERMANENT;
//        a[6] = OTHER;
//        $VALUES = a;
//    }
//    
//    private ReportIssueService$IssueType(String s, int i, int i0, int i1, int i2) {
//        super(s, i);
//        this.titleStringResource = i0;
//        this.messageStringResource = i1;
//        this.issueTypeCode = i2;
//    }
//    
//    public static com.navdy.hud.app.util.ReportIssueService$IssueType valueOf(String s) {
//        return (com.navdy.hud.app.util.ReportIssueService$IssueType)Enum.valueOf(com.navdy.hud.app.util.ReportIssueService$IssueType.class, s);
//    }
//    
//    public static com.navdy.hud.app.util.ReportIssueService$IssueType[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getIssueTypeCode() {
//        return this.issueTypeCode;
//    }
//    
//    public int getMessageStringResource() {
//        return this.messageStringResource;
//    }
//    
//    public int getTitleStringResource() {
//        return this.titleStringResource;
//    }
//}
//