package com.navdy.hud.app.util;

class ScreenConductor$1 implements android.view.animation.Animation$AnimationListener {
    final com.navdy.hud.app.util.ScreenConductor this$0;
    final int val$animIn;
    final com.navdy.hud.app.screen.BaseScreen val$newScreen;
    final com.navdy.hud.app.screen.BaseScreen val$oldScreen;
    
    ScreenConductor$1(com.navdy.hud.app.util.ScreenConductor a, com.navdy.hud.app.screen.BaseScreen a0, int i, com.navdy.hud.app.screen.BaseScreen a1) {
        super();
        this.this$0 = a;
        this.val$oldScreen = a0;
        this.val$animIn = i;
        this.val$newScreen = a1;
    }
    
    public void onAnimationEnd(android.view.animation.Animation a) {
        this.val$oldScreen.onAnimationOutEnd();
        if (this.val$animIn == -1) {
            this.this$0.uiStateManager.postScreenAnimationEvent(false, this.val$newScreen, this.val$oldScreen);
        }
    }
    
    public void onAnimationRepeat(android.view.animation.Animation a) {
    }
    
    public void onAnimationStart(android.view.animation.Animation a) {
        this.val$oldScreen.onAnimationOutStart();
        if (this.val$animIn == -1) {
            this.this$0.uiStateManager.postScreenAnimationEvent(true, this.val$newScreen, this.val$oldScreen);
        }
    }
}
