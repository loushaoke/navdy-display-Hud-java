package com.navdy.hud.app.util;

final public class ReportIssueService$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding driveRecorder;
    private dagger.internal.Binding gestureService;
    private dagger.internal.Binding mHttpManager;
    
    public ReportIssueService$$InjectAdapter() {
        super("com.navdy.hud.app.util.ReportIssueService", "members/com.navdy.hud.app.util.ReportIssueService", false, com.navdy.hud.app.util.ReportIssueService.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.mHttpManager = a.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.hud.app.util.ReportIssueService.class, (this).getClass().getClassLoader());
        this.gestureService = a.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.util.ReportIssueService.class, (this).getClass().getClassLoader());
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.util.ReportIssueService.class, (this).getClass().getClassLoader());
        this.driveRecorder = a.requestBinding("com.navdy.hud.app.debug.DriveRecorder", com.navdy.hud.app.util.ReportIssueService.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.util.ReportIssueService get() {
        com.navdy.hud.app.util.ReportIssueService a = new com.navdy.hud.app.util.ReportIssueService();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.mHttpManager);
        a0.add(this.gestureService);
        a0.add(this.bus);
        a0.add(this.driveRecorder);
    }
    
    public void injectMembers(com.navdy.hud.app.util.ReportIssueService a) {
        a.mHttpManager = (com.navdy.service.library.network.http.IHttpManager)this.mHttpManager.get();
        a.gestureService = (com.navdy.hud.app.gesture.GestureServiceConnector)this.gestureService.get();
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.driveRecorder = (com.navdy.hud.app.debug.DriveRecorder)this.driveRecorder.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.util.ReportIssueService)a);
    }
}
