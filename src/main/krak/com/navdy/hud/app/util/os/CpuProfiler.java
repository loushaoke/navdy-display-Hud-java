package com.navdy.hud.app.util.os;

public class CpuProfiler {
    final private static com.navdy.hud.app.util.os.CpuProfiler$CpuUsage HIGH_CPU;
    final private static String[] HIGH_CPU_THREAD_NAME_PATTERN;
    final private static int HIGH_CPU_USAGE_THRESHOLD = 85;
    final private static int INITIAL_PERIODIC_INTERVAL;
    final private static int LOW_NICE_PRIORITY = 19;
    final private static String NAVDY_PROCESS_NAME = "com.navdy.hud.app";
    final private static com.navdy.hud.app.util.os.CpuProfiler$CpuUsage NORMAL_CPU;
    final private static int NORMAL_CPU_USAGE_THRESHOLD = 70;
    final private static int PERIODIC_INTERVAL;
    final private static int PROCESS_CPU_USAGE_THRESHOLD = 20;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.util.os.CpuProfiler singleton;
    private com.squareup.otto.Bus bus;
    private android.os.Handler handler;
    private com.navdy.service.library.util.SystemUtils$ProcessCpuInfo highCpuThreadInfo;
    private int highCpuThreadOrigPrio;
    private boolean highCpuUsage;
    private Runnable periodicRunnable;
    private Runnable periodicRunnableBk;
    private boolean running;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.os.CpuProfiler.class);
        HIGH_CPU = new com.navdy.hud.app.util.os.CpuProfiler$CpuUsage(com.navdy.hud.app.util.os.CpuProfiler$Usage.HIGH);
        NORMAL_CPU = new com.navdy.hud.app.util.os.CpuProfiler$CpuUsage(com.navdy.hud.app.util.os.CpuProfiler$Usage.NORMAL);
        INITIAL_PERIODIC_INTERVAL = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(45L);
        PERIODIC_INTERVAL = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(10L);
        String[] a = new String[2];
        a[0] = "NAVDY-HOGGER-";
        a[1] = "Thread-";
        HIGH_CPU_THREAD_NAME_PATTERN = a;
        singleton = new com.navdy.hud.app.util.os.CpuProfiler();
    }
    
    private CpuProfiler() {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.periodicRunnable = (Runnable)new com.navdy.hud.app.util.os.CpuProfiler$1(this);
        this.periodicRunnableBk = (Runnable)new com.navdy.hud.app.util.os.CpuProfiler$2(this);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }
    
    static Runnable access$000(com.navdy.hud.app.util.os.CpuProfiler a) {
        return a.periodicRunnableBk;
    }
    
    static boolean access$100(com.navdy.hud.app.util.os.CpuProfiler a) {
        return a.running;
    }
    
    static com.navdy.service.library.util.SystemUtils$ProcessCpuInfo access$1000(com.navdy.hud.app.util.os.CpuProfiler a) {
        return a.highCpuThreadInfo;
    }
    
    static Runnable access$1100(com.navdy.hud.app.util.os.CpuProfiler a) {
        return a.periodicRunnable;
    }
    
    static int access$1200() {
        return PERIODIC_INTERVAL;
    }
    
    static android.os.Handler access$1300(com.navdy.hud.app.util.os.CpuProfiler a) {
        return a.handler;
    }
    
    static boolean access$200(com.navdy.hud.app.util.os.CpuProfiler a) {
        return a.highCpuUsage;
    }
    
    static boolean access$202(com.navdy.hud.app.util.os.CpuProfiler a, boolean b) {
        a.highCpuUsage = b;
        return b;
    }
    
    static com.navdy.hud.app.util.os.CpuProfiler$CpuUsage access$300() {
        return HIGH_CPU;
    }
    
    static com.squareup.otto.Bus access$400(com.navdy.hud.app.util.os.CpuProfiler a) {
        return a.bus;
    }
    
    static void access$500(com.navdy.hud.app.util.os.CpuProfiler a, com.navdy.service.library.util.SystemUtils$CpuInfo a0) {
        a.takeCorrectiveAction(a0);
    }
    
    static void access$600(com.navdy.hud.app.util.os.CpuProfiler a, String s, int i, int i0, int i1, java.util.ArrayList a0) {
        a.printCpuInfo(s, i, i0, i1, a0);
    }
    
    static com.navdy.service.library.log.Logger access$700() {
        return sLogger;
    }
    
    static com.navdy.hud.app.util.os.CpuProfiler$CpuUsage access$800() {
        return NORMAL_CPU;
    }
    
    static void access$900(com.navdy.hud.app.util.os.CpuProfiler a, com.navdy.service.library.util.SystemUtils$CpuInfo a0) {
        a.revertCorrectiveAction(a0);
    }
    
    public static com.navdy.hud.app.util.os.CpuProfiler getInstance() {
        return singleton;
    }
    
    private com.navdy.service.library.util.SystemUtils$ProcessCpuInfo getKnownHighCpuThread(com.navdy.service.library.util.SystemUtils$CpuInfo a) {
        Object a0 = a.getList().iterator();
        label1: while(true) {
            com.navdy.service.library.util.SystemUtils$ProcessCpuInfo a1 = null;
            if (((java.util.Iterator)a0).hasNext()) {
                a1 = (com.navdy.service.library.util.SystemUtils$ProcessCpuInfo)((java.util.Iterator)a0).next();
                if (!android.text.TextUtils.equals((CharSequence)a1.getProcessName(), (CharSequence)"com.navdy.hud.app")) {
                    continue label1;
                }
                String s = a1.getThreadName();
                if (s == null) {
                    continue label1;
                }
                int i = 0;
                while(true) {
                    if (i >= HIGH_CPU_THREAD_NAME_PATTERN.length) {
                        continue label1;
                    }
                    boolean b = s.startsWith(HIGH_CPU_THREAD_NAME_PATTERN[i]);
                    label0: {
                        if (!b) {
                            break label0;
                        }
                        if (a1.getCpu() >= 20) {
                            break;
                        }
                        sLogger.v(new StringBuilder().append("Thread found but cpu usage < threshold").append(a1.getThreadName()).append(" ").append(a1.getCpu()).append("%").toString());
                    }
                    i = i + 1;
                }
                sLogger.v(new StringBuilder().append("high cpu thread found ").append(a1.getThreadName()).append(" ").append(a1.getCpu()).append("%").toString());
            } else {
                a1 = null;
            }
            return a1;
        }
    }
    
    private void printCpuInfo(String s, int i, int i0, int i1, java.util.ArrayList a) {
        sLogger.v(new StringBuilder().append("[CPU] ").append(s).append(" cpu=").append(i).append("% user=").append(i0).append("% system=").append(i1).append("%").toString());
        Object a0 = a.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            com.navdy.service.library.util.SystemUtils$ProcessCpuInfo a1 = (com.navdy.service.library.util.SystemUtils$ProcessCpuInfo)((java.util.Iterator)a0).next();
            if (!"top".equals(a1.getProcessName())) {
                sLogger.v(new StringBuilder().append("[CPU] ").append(s).append(" ").append(a1.getCpu()).append("% process=").append(a1.getProcessName()).append(" thread=").append(a1.getThreadName()).append(" tid=").append(a1.getTid()).append(" pid=").append(a1.getPid()).append("").toString());
            }
        }
    }
    
    private void revertCorrectiveAction(com.navdy.service.library.util.SystemUtils$CpuInfo a) {
        try {
            sLogger.i("revertCorrectiveAction");
            if (this.highCpuThreadInfo == null) {
                sLogger.i("revertCorrectiveAction high cpu thread was not detected previously");
            } else {
                try {
                    android.os.Process.setThreadPriority(this.highCpuThreadInfo.getTid(), this.highCpuThreadOrigPrio);
                    sLogger.i(new StringBuilder().append("revertCorrectiveAction high cpu thread priority reduced from 19 to ").append(this.highCpuThreadOrigPrio).toString());
                } catch(Throwable a0) {
                    sLogger.e(a0);
                }
                this.highCpuThreadInfo = null;
                this.highCpuThreadOrigPrio = 0;
            }
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
    
    private void takeCorrectiveAction(com.navdy.service.library.util.SystemUtils$CpuInfo a) {
        label0: try {
            Throwable a0 = null;
            sLogger.i("takeCorrectiveAction");
            com.navdy.service.library.util.SystemUtils$ProcessCpuInfo a1 = this.getKnownHighCpuThread(a);
            if (a1 == null) {
                sLogger.i("takeCorrectiveAction high cpu thread not found");
                break label0;
            } else if (a1.getTid() == 0) {
                sLogger.i("takeCorrectiveAction high cpu thread tid is not valid");
                break label0;
            } else {
                int i = android.os.Process.getThreadPriority(a1.getTid());
                sLogger.v(new StringBuilder().append("takeCorrectiveAction: high cpu thread, priority[").append(i).append("]").toString());
                if (this.highCpuThreadInfo != null) {
                    sLogger.i("takeCorrectiveAction high cpu thread, priority already reduced");
                    break label0;
                } else {
                    this.highCpuThreadInfo = a1;
                    this.highCpuThreadOrigPrio = i;
                    try {
                        android.os.Process.setThreadPriority(this.highCpuThreadInfo.getTid(), 19);
                        sLogger.i(new StringBuilder().append("takeCorrectiveAction high cpu thread, priority changed from ").append(i).append(" to ").append(android.os.Process.getThreadPriority(a1.getTid())).toString());
                        break label0;
                    } catch(Throwable a2) {
                        a0 = a2;
                    }
                }
            }
            sLogger.e(a0);
            this.highCpuThreadInfo = null;
            this.highCpuThreadOrigPrio = 0;
        } catch(Throwable a3) {
            sLogger.e(a3);
        }
    }
    
    public void start() {
        synchronized(this) {
            if (this.running) {
                sLogger.v("already running");
            } else {
                this.handler.removeCallbacks(this.periodicRunnable);
                this.handler.postDelayed(this.periodicRunnable, (long)INITIAL_PERIODIC_INTERVAL);
                this.running = true;
                sLogger.v("running");
            }
        }
        /*monexit(this)*/;
    }
    
    public void stop() {
        synchronized(this) {
            if (this.running) {
                this.handler.removeCallbacks(this.periodicRunnable);
                this.running = false;
                sLogger.v("stopped");
            } else {
                sLogger.v("not running");
            }
        }
        /*monexit(this)*/;
    }
}
