package com.navdy.hud.app;

abstract public interface IEventListener extends android.os.IInterface {
    abstract public void onEvent(byte[] arg);
}
