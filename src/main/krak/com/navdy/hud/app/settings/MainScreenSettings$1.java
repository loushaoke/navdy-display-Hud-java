package com.navdy.hud.app.settings;

final class MainScreenSettings$1 implements android.os.Parcelable$Creator {
    MainScreenSettings$1() {
    }
    
    public com.navdy.hud.app.settings.MainScreenSettings createFromParcel(android.os.Parcel a) {
        return new com.navdy.hud.app.settings.MainScreenSettings(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.hud.app.settings.MainScreenSettings[] newArray(int i) {
        return new com.navdy.hud.app.settings.MainScreenSettings[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
