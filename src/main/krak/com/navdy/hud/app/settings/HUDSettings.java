package com.navdy.hud.app.settings;

public class HUDSettings {
    final private static String ADAPTIVE_AUTOBRIGHTNESS_PROP = "persist.sys.autobright_adaptive";
    final public static String AUTO_BRIGHTNESS = "screen.auto_brightness";
    final public static String AUTO_BRIGHTNESS_ADJUSTMENT = "screen.auto_brightness_adj";
    final public static String BRIGHTNESS = "screen.brightness";
    final public static float BRIGHTNESS_SCALE = 255f;
    final public static String GESTURE_ENGINE = "gesture.engine";
    final public static String GESTURE_PREVIEW = "gesture.preview";
    final public static String LED_BRIGHTNESS = "screen.led_brightness";
    final public static String MAP_ANIMATION_MODE = "map.animation.mode";
    final public static String MAP_SCHEME = "map.scheme";
    final public static String MAP_TILT = "map.tilt";
    final public static String MAP_ZOOM = "map.zoom";
    final public static boolean USE_ADAPTIVE_AUTOBRIGHTNESS;
    private static com.navdy.service.library.log.Logger sLogger;
    private android.content.SharedPreferences preferences;
    
    static {
        USE_ADAPTIVE_AUTOBRIGHTNESS = com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.autobright_adaptive", true);
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.settings.HUDSettings.class);
    }
    
    public HUDSettings(android.content.SharedPreferences a) {
        this.preferences = a;
    }
    
    private Object get(String s) {
        return this.preferences.getAll().get(s);
    }
    
    private void set(android.content.SharedPreferences$Editor a, String s, String s0) {
        Object a0 = this.get(s);
        if (a0 instanceof String) {
            a.putString(s, s0);
        } else if (a0 instanceof Boolean) {
            a.putBoolean(s, Boolean.valueOf(s0).booleanValue());
        }
    }
    
    public java.util.List availableSettings() {
        java.util.ArrayList a = new java.util.ArrayList();
        ((java.util.List)a).addAll((java.util.Collection)this.preferences.getAll().keySet());
        return (java.util.List)a;
    }
    
    public java.util.List readSettings(java.util.List a) {
        java.util.ArrayList a0 = new java.util.ArrayList();
        java.util.Map a1 = this.preferences.getAll();
        Object a2 = a;
        Object a3 = a1;
        int i = 0;
        while(i < ((java.util.List)a2).size()) {
            String s = (String)((java.util.List)a2).get(i);
            Object a4 = ((java.util.Map)a3).get(s);
            if (a4 != null) {
                ((java.util.List)a0).add(new com.navdy.service.library.events.settings.Setting(s, a4.toString()));
            }
            i = i + 1;
        }
        return (java.util.List)a0;
    }
    
    public void updateSettings(java.util.List a) {
        android.content.SharedPreferences$Editor a0 = this.preferences.edit();
        Object a1 = a;
        Object a2 = a0;
        int i = 0;
        while(i < ((java.util.List)a1).size()) {
            com.navdy.service.library.events.settings.Setting a3 = (com.navdy.service.library.events.settings.Setting)((java.util.List)a1).get(i);
            if (a3.value.equals("-1")) {
                int i0 = 0;
                String s = a3.key;
                switch(s.hashCode()) {
                    case 134000933: {
                        i0 = (s.equals("map.zoom")) ? 1 : -1;
                        break;
                    }
                    case 133816335: {
                        i0 = (s.equals("map.tilt")) ? 0 : -1;
                        break;
                    }
                    default: {
                        i0 = -1;
                    }
                }
                switch(i0) {
                    case 1: {
                        this.set((android.content.SharedPreferences$Editor)a2, a3.key, String.valueOf(16.5f));
                        break;
                    }
                    case 0: {
                        this.set((android.content.SharedPreferences$Editor)a2, a3.key, String.valueOf(60f));
                        break;
                    }
                }
            } else {
                this.set((android.content.SharedPreferences$Editor)a2, a3.key, a3.value);
            }
            i = i + 1;
        }
        ((android.content.SharedPreferences$Editor)a2).apply();
    }
}
