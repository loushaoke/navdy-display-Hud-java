package com.navdy.hud.app.receiver;

public class LogLevelReceiver extends android.content.BroadcastReceiver {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.receiver.LogLevelReceiver.class);
    }
    
    public LogLevelReceiver() {
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        try {
            if (a0.getAction().equals("com.navdy.service.library.log.action.RELOAD")) {
                sLogger.i("Forcing loggers to reload cached log levels");
                com.navdy.service.library.log.Logger.reloadLogLevels();
            }
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
}
