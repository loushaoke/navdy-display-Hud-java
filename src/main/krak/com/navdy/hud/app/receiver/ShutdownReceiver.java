package com.navdy.hud.app.receiver;

public class ShutdownReceiver extends android.content.BroadcastReceiver {
    public ShutdownReceiver() {
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        com.navdy.hud.app.HudApplication.getApplication().shutdown();
    }
}
