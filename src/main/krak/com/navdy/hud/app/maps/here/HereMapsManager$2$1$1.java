package com.navdy.hud.app.maps.here;

class HereMapsManager$2$1$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager$2$1 this$2;
    final com.here.android.mpa.odml.MapPackage val$mapPackage;
    
    HereMapsManager$2$1$1(com.navdy.hud.app.maps.here.HereMapsManager$2$1 a, com.here.android.mpa.odml.MapPackage a0) {
        super();
        this.this$2 = a;
        this.val$mapPackage = a0;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapsManager.access$2400(this.this$2.this$1.this$0, this.val$mapPackage, java.util.EnumSet.of((Enum)com.here.android.mpa.odml.MapPackage$InstallationState.INSTALLED, (Enum)com.here.android.mpa.odml.MapPackage$InstallationState.PARTIALLY_INSTALLED));
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("initMapLoader: map package count:").append(com.navdy.hud.app.maps.here.HereMapsManager.access$2500(this.this$2.this$1.this$0)).toString());
        com.navdy.hud.app.maps.here.HereMapsManager.access$2602(this.this$2.this$1.this$0, true);
    }
}
