package com.navdy.hud.app.maps.here;

class HereMapsManager$10 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$10(com.navdy.hud.app.maps.here.HereMapsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapsManager.access$300(this.this$0).setTrafficInfoVisible(false);
        com.here.android.mpa.mapping.MapTrafficLayer a = com.navdy.hud.app.maps.here.HereMapsManager.access$300(this.this$0).getMapTrafficLayer();
        a.setDisplayFilter(com.here.android.mpa.mapping.TrafficEvent$Severity.NORMAL);
        a.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer$RenderLayer.ONROUTE, false);
        a.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer$RenderLayer.FLOW, false);
        a.setEnabled(com.here.android.mpa.mapping.MapTrafficLayer$RenderLayer.INCIDENT, false);
    }
}
