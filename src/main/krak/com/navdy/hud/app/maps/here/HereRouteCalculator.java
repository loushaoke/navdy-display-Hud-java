package com.navdy.hud.app.maps.here;

public class HereRouteCalculator {
    private static java.util.concurrent.atomic.AtomicLong ID;
    final private static int PROGRESS_INTERVAL_MS = 200;
    private com.here.android.mpa.routing.CoreRouter coreRouter;
    private com.navdy.service.library.log.Logger logger;
    private String tag;
    private boolean verbose;
    
    static {
        ID = new java.util.concurrent.atomic.AtomicLong(1L);
    }
    
    public HereRouteCalculator(com.navdy.service.library.log.Logger a, boolean b) {
        this.logger = a;
        this.verbose = b;
        this.tag = new StringBuilder().append("[").append(ID.getAndIncrement()).append("]").toString();
    }
    
    static boolean access$000(com.navdy.hud.app.maps.here.HereRouteCalculator a, com.navdy.service.library.events.navigation.NavigationRouteRequest a0) {
        return a.isRouteCancelled(a0);
    }
    
    static String access$100(com.navdy.hud.app.maps.here.HereRouteCalculator a) {
        return a.tag;
    }
    
    static com.navdy.service.library.log.Logger access$200(com.navdy.hud.app.maps.here.HereRouteCalculator a) {
        return a.logger;
    }
    
    private boolean isRouteCancelled(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        boolean b = false;
        if (a != null) {
            String s = com.navdy.hud.app.maps.here.HereRouteManager.getActiveRouteCalcId();
            if (android.text.TextUtils.equals((CharSequence)a.requestId, (CharSequence)s)) {
                b = false;
            } else {
                this.logger.v(new StringBuilder().append("route request [").append(a.requestId).append("] is not active anymore, current [").append(s).append("]").toString());
                b = true;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void calculateRoute(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0, java.util.List a1, com.here.android.mpa.common.GeoCoordinate a2, boolean b, com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener a3, int i, com.here.android.mpa.routing.RouteOptions a4, boolean b0) {
        this.calculateRoute(a, a0, a1, a2, b, a3, i, a4, true, true, b0);
    }
    
    public void calculateRoute(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0, java.util.List a1, com.here.android.mpa.common.GeoCoordinate a2, boolean b, com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener a3, int i, com.here.android.mpa.routing.RouteOptions a4, boolean b0, boolean b1, boolean b2) {
        Object a5 = null;
        this.coreRouter = new com.here.android.mpa.routing.CoreRouter();
        com.here.android.mpa.routing.RoutePlan a6 = new com.here.android.mpa.routing.RoutePlan();
        a4.setRouteCount(i);
        a6.setRouteOptions(a4);
        a6.addWaypoint(new com.here.android.mpa.routing.RouteWaypoint(a0));
        if (a1 == null) {
            a5 = a3;
        } else {
            Object a7 = a1.iterator();
            a5 = a3;
            while(((java.util.Iterator)a7).hasNext()) {
                a6.addWaypoint(new com.here.android.mpa.routing.RouteWaypoint((com.here.android.mpa.common.GeoCoordinate)((java.util.Iterator)a7).next()));
            }
        }
        a6.addWaypoint(new com.here.android.mpa.routing.RouteWaypoint(a2));
        com.here.android.mpa.routing.DynamicPenalty a8 = new com.here.android.mpa.routing.DynamicPenalty();
        if (b) {
            this.logger.v(new StringBuilder().append(this.tag).append("Factoring traffic in route calc[ONLINE]").toString());
            a8.setTrafficPenaltyMode(com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL);
            this.coreRouter.setConnectivity(com.here.android.mpa.routing.CoreRouter$Connectivity.ONLINE);
        } else {
            this.logger.v(new StringBuilder().append(this.tag).append("Not factoring traffic in route calc[OFFLINE]").toString());
            a8.setTrafficPenaltyMode(com.here.android.mpa.routing.Route$TrafficPenaltyMode.DISABLED);
            this.coreRouter.setConnectivity(com.here.android.mpa.routing.CoreRouter$Connectivity.OFFLINE);
        }
        this.coreRouter.setDynamicPenalty(a8);
        this.logger.i(new StringBuilder().append(this.tag).append("Generating route with ").append(a6.getWaypointCount()).append(" waypoints").toString());
        long j = android.os.SystemClock.elapsedRealtime();
        this.coreRouter.calculateRoute(a6, (com.here.android.mpa.routing.Router$Listener)new com.navdy.hud.app.maps.here.HereRouteCalculator$1(this, (com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener)a5, b2, a, b1, b0, b, a0, j));
    }
    
    public void cancel() {
        label1: {
            Throwable a = null;
            label0: {
                try {
                    if (this.coreRouter != null) {
                        this.coreRouter.cancel();
                    }
                } catch(Throwable a0) {
                    a = a0;
                    break label0;
                }
                this.coreRouter = null;
                break label1;
            }
            try {
                this.logger.e(this.tag, a);
            } catch(Throwable a1) {
                this.coreRouter = null;
                throw a1;
            }
            this.coreRouter = null;
        }
    }
    
    public com.navdy.service.library.events.navigation.NavigationRouteResult getRouteResultFromRoute(String s, com.here.android.mpa.routing.Route a, String s0, String s1, String s2, boolean b) {
        java.util.List a0 = null;
        java.util.ArrayList a1 = new java.util.ArrayList();
        if (b) {
            long j = android.os.SystemClock.elapsedRealtime();
            java.util.List a2 = a.getRouteGeometry();
            long j0 = android.os.SystemClock.elapsedRealtime();
            this.logger.v(new StringBuilder().append(this.tag).append("getting geometry took ").append(j0 - j).append(" ms").toString());
            this.logger.v(new StringBuilder().append(this.tag).append("points before simplification: ").append(a2.size()).toString());
            a0 = com.navdy.hud.app.maps.util.RouteUtils.simplify(a2);
            this.logger.v(new StringBuilder().append(this.tag).append("points after simplification: ").append(a0.size() / 2).toString());
            com.navdy.service.library.events.location.Coordinate a3 = new com.navdy.service.library.events.location.Coordinate(Double.valueOf(((com.here.android.mpa.common.GeoCoordinate)a2.get(0)).getLatitude()), Double.valueOf(((com.here.android.mpa.common.GeoCoordinate)a2.get(0)).getLongitude()), (Float)null, (Double)null, (Float)null, (Float)null, (Long)null, (String)null);
            a1 = new java.util.ArrayList();
            ((java.util.List)a1).add(a3);
        } else {
            this.logger.v(new StringBuilder().append(this.tag).append(" poly line calc off").toString());
            a0 = null;
        }
        int i = a.getTta(com.here.android.mpa.routing.Route$TrafficPenaltyMode.OPTIMAL, 268435455).getDuration();
        int i0 = a.getLength();
        int i1 = a.getTta(com.here.android.mpa.routing.Route$TrafficPenaltyMode.DISABLED, 268435455).getDuration();
        com.navdy.service.library.events.navigation.NavigationRouteResult a4 = new com.navdy.service.library.events.navigation.NavigationRouteResult(s, s1, (java.util.List)a1, Integer.valueOf(i0), Integer.valueOf(i1), Integer.valueOf(i), a0, s0, s2);
        this.logger.v(new StringBuilder().append(this.tag).append("route id[").append(s).append("] via[").append(s0).append("] length[").append(i0).append("] duration[").append(i).append("] freeFlowDuration[").append(i1).append("]").toString());
        return a4;
    }
}
