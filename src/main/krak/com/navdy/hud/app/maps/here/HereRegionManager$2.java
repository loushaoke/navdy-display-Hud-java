package com.navdy.hud.app.maps.here;

class HereRegionManager$2 implements com.here.android.mpa.search.ResultListener {
    final com.navdy.hud.app.maps.here.HereRegionManager this$0;
    
    HereRegionManager$2(com.navdy.hud.app.maps.here.HereRegionManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onCompleted(com.here.android.mpa.search.Location a, com.here.android.mpa.search.ErrorCode a0) {
        try {
            if (a0 == com.here.android.mpa.search.ErrorCode.NONE) {
                if (a != null) {
                    com.here.android.mpa.search.Address a1 = a.getAddress();
                    if (a1 != null) {
                        com.navdy.hud.app.maps.MapEvents$RegionEvent a2 = new com.navdy.hud.app.maps.MapEvents$RegionEvent(a1.getState(), a1.getCountryName());
                        if (!a2.equals(com.navdy.hud.app.maps.here.HereRegionManager.access$200(this.this$0))) {
                            com.navdy.hud.app.maps.here.HereRegionManager.access$100().v(new StringBuilder().append("").append(a2).toString());
                            com.navdy.hud.app.maps.here.HereRegionManager.access$202(this.this$0, a2);
                            this.this$0.bus.post(com.navdy.hud.app.maps.here.HereRegionManager.access$200(this.this$0));
                        }
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereRegionManager.access$100().e("reverseGeoCode:onComplete: address is null");
                }
            } else {
                com.navdy.hud.app.maps.here.HereRegionManager.access$100().e(new StringBuilder().append("reverseGeoCode:onComplete: Error[").append(a0.name()).append("]").toString());
            }
        } catch(Throwable a3) {
            com.navdy.hud.app.maps.here.HereRegionManager.access$100().e(a3);
            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(a3);
        }
    }
    
    public void onCompleted(Object a, com.here.android.mpa.search.ErrorCode a0) {
        this.onCompleted((com.here.android.mpa.search.Location)a, a0);
    }
}
