package com.navdy.hud.app.maps.util;
import com.navdy.hud.app.R;

public class DistanceConverter$Distance {
    public com.navdy.service.library.events.navigation.DistanceUnit unit;
    public float value;
    
    public DistanceConverter$Distance() {
    }
    
    void clear() {
        this.value = 0.0f;
        this.unit = null;
    }
    
    public String getFormattedExtendedDistanceUnit() {
        String s = null;
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        switch(com.navdy.hud.app.maps.util.DistanceConverter$1.$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[this.unit.ordinal()]) {
            case 4: {
                float f = this.value;
                s = (((f > 1f) ? 1 : (f == 1f) ? 0 : -1) != 0) ? a.getString(R.string.unit_miles_ext) : a.getString(R.string.unit_miles_ext_singular);
                break;
            }
            case 3: {
                float f0 = this.value;
                s = (((f0 > 1f) ? 1 : (f0 == 1f) ? 0 : -1) != 0) ? a.getString(R.string.unit_kilometers_ext) : a.getString(R.string.unit_kilometers_ext_singular);
                break;
            }
            case 2: {
                float f1 = this.value;
                s = (((f1 > 1f) ? 1 : (f1 == 1f) ? 0 : -1) != 0) ? a.getString(R.string.unit_feet_ext) : a.getString(R.string.unit_feet_ext_singular);
                break;
            }
            case 1: {
                float f2 = this.value;
                s = (((f2 > 1f) ? 1 : (f2 == 1f) ? 0 : -1) != 0) ? a.getString(R.string.unit_meters_ext) : a.getString(R.string.unit_meters_ext_singular);
                break;
            }
            default: {
                s = "";
            }
        }
        return s;
    }
}
