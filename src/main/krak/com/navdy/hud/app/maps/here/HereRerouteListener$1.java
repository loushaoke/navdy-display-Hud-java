package com.navdy.hud.app.maps.here;

class HereRerouteListener$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereRerouteListener this$0;
    
    HereRerouteListener$1(com.navdy.hud.app.maps.here.HereRerouteListener a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereRerouteListener.access$002(this.this$0, com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).getCurrentRouteId());
        com.navdy.hud.app.maps.here.HereRerouteListener.access$300(this.this$0).i(new StringBuilder().append(com.navdy.hud.app.maps.here.HereRerouteListener.access$200(this.this$0)).append(" reroute-begin: id=").append(com.navdy.hud.app.maps.here.HereRerouteListener.access$000(this.this$0)).toString());
        com.navdy.hud.app.maps.here.HereRerouteListener.access$402(this.this$0, true);
        if (com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).getNavigationSessionPreference().spokenTurnByTurn && !com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).hasArrived() && !com.navdy.hud.app.maps.MapSettings.isTtsRecalculationDisabled()) {
            com.navdy.hud.app.maps.here.HereRerouteListener.access$500(this.this$0).post(com.navdy.hud.app.maps.here.HereRerouteListener.access$100(this.this$0).TTS_REROUTING);
        }
        com.navdy.hud.app.maps.here.HereRerouteListener.access$500(this.this$0).post(com.navdy.hud.app.maps.here.HereRerouteListener.REROUTE_STARTED);
        com.navdy.hud.app.maps.here.HereRerouteListener.access$500(this.this$0).post(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.EMPTY_MANEUVER_DISPLAY);
    }
}
