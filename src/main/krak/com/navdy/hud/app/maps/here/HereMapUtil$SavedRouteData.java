package com.navdy.hud.app.maps.here;

public class HereMapUtil$SavedRouteData {
    final public boolean isGasRoute;
    final public com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest;
    
    public HereMapUtil$SavedRouteData(com.navdy.service.library.events.navigation.NavigationRouteRequest a, boolean b) {
        this.navigationRouteRequest = a;
        this.isGasRoute = b;
    }
}
