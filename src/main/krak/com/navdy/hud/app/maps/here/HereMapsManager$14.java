package com.navdy.hud.app.maps.here;

class HereMapsManager$14 {
    final static int[] $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType = new int[com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType;
        com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType a0 = com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.ROUTING_FASTEST;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType[com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.ROUTING_SHORTEST.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
