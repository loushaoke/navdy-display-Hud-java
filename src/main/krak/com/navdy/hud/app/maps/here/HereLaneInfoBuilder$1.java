package com.navdy.hud.app.maps.here;

class HereLaneInfoBuilder$1 {
    final static int[] $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction;
    final static int[] $SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType;
    
    static {
        $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction = new int[com.here.android.mpa.guidance.LaneInformation$Direction.values().length];
        int[] a = $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction;
        com.here.android.mpa.guidance.LaneInformation$Direction a0 = com.here.android.mpa.guidance.LaneInformation$Direction.STRAIGHT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation$Direction.RIGHT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_RIGHT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_LEFT.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_LEFT.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation$Direction.LEFT.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_LEFT.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction[com.here.android.mpa.guidance.LaneInformation$Direction.U_TURN_RIGHT.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException7) {
        }
        $SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType = new int[com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType;
        com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType a2 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType[com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$here$HereLaneInfoBuilder$DirectionType[com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT_RIGHT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException10) {
        }
    }
}
