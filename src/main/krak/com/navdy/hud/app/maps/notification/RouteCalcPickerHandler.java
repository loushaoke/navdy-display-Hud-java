package com.navdy.hud.app.maps.notification;

public class RouteCalcPickerHandler implements com.navdy.hud.app.ui.component.destination.IDestinationPicker {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent event;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.notification.RouteCalcPickerHandler.class);
    }
    
    public RouteCalcPickerHandler(com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a) {
        this.event = a;
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public void onDestinationPickerClosed() {
    }
    
    public boolean onItemClicked(int i, int i0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a) {
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getNavigationView().cleanupMapOverviewRoutes();
        if (i0 != 0) {
            sLogger.v("map-route- onItemClicked new route");
            int i1 = this.event.response.results.size();
            if (i0 < i1) {
                com.navdy.service.library.events.navigation.NavigationRouteResult a0 = (com.navdy.service.library.events.navigation.NavigationRouteResult)this.event.response.results.get(i0);
                com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a1 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(a0.routeId);
                if (a1 != null) {
                    a.doNotAddOriginalRoute = true;
                    com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.notification.RouteCalcPickerHandler$1(this, a0, a1), 20);
                } else {
                    sLogger.w(new StringBuilder().append("map-route- route not found in cache:").append(a0.routeId).toString());
                }
            } else {
                sLogger.w(new StringBuilder().append("map-route- invalid pos:").append(i0).append(" len=").append(i1).toString());
            }
        } else {
            sLogger.v("map-route- onItemClicked current route");
        }
        return true;
    }
    
    public boolean onItemSelected(int i, int i0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a) {
        return false;
    }
}
