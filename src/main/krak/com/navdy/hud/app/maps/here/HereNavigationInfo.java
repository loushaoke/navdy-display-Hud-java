package com.navdy.hud.app.maps.here;

public class HereNavigationInfo {
    com.navdy.hud.app.maps.MapEvents$ManeuverDisplay arrivedManeuverDisplay;
    com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason currentRerouteReason;
    com.here.android.mpa.common.GeoCoordinate destination;
    com.navdy.hud.app.maps.MapEvents$DestinationDirection destinationDirection;
    String destinationDirectionStr;
    int destinationIconId;
    public String destinationIdentifier;
    String destinationLabel;
    com.navdy.service.library.device.NavdyDeviceId deviceId;
    boolean firstManeuverShown;
    long firstManeuverShownTime;
    boolean hasArrived;
    boolean ignoreArrived;
    boolean lastManeuver;
    long lastManeuverPostTime;
    com.navdy.service.library.events.navigation.NavigationRouteRequest lastRequest;
    String lastRouteId;
    long lastTrafficRerouteTime;
    com.here.android.mpa.routing.Maneuver maneuverAfterCurrent;
    int maneuverAfterCurrentIconid;
    com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState maneuverState;
    com.here.android.mpa.mapping.MapMarker mapDestinationMarker;
    com.here.android.mpa.mapping.MapRoute mapRoute;
    com.navdy.service.library.events.navigation.NavigationRouteRequest navigationRouteRequest;
    com.here.android.mpa.routing.Route route;
    String routeId;
    com.here.android.mpa.routing.RouteOptions routeOptions;
    int simulationSpeed;
    com.here.android.mpa.common.GeoCoordinate startLocation;
    String streetAddress;
    int trafficRerouteCount;
    boolean trafficRerouteOnce;
    java.util.List waypoints;
    
    public HereNavigationInfo() {
        this.maneuverAfterCurrentIconid = -1;
    }
    
    void clear() {
        if (this.navigationRouteRequest == null) {
            this.lastRequest = null;
            this.lastRouteId = null;
        } else {
            this.lastRequest = this.navigationRouteRequest;
            this.lastRouteId = this.routeId;
        }
        this.navigationRouteRequest = null;
        this.routeId = null;
        this.route = null;
        this.simulationSpeed = -1;
        this.streetAddress = null;
        this.destinationLabel = null;
        this.destinationIdentifier = null;
        this.lastManeuverPostTime = 0L;
        this.deviceId = null;
        this.destinationDirection = null;
        this.destinationDirectionStr = null;
        this.destinationIconId = -1;
        this.maneuverAfterCurrent = null;
        this.maneuverAfterCurrentIconid = -1;
        this.hasArrived = false;
        this.ignoreArrived = false;
        this.lastManeuver = false;
        this.arrivedManeuverDisplay = null;
        this.destination = null;
        this.trafficRerouteCount = 0;
        this.lastTrafficRerouteTime = 0L;
        this.currentRerouteReason = null;
        this.trafficRerouteOnce = false;
        this.firstManeuverShown = false;
        this.firstManeuverShownTime = 0L;
        this.routeOptions = null;
        this.startLocation = null;
    }
    
    void copy(com.navdy.hud.app.maps.here.HereNavigationInfo a) {
        this.clear();
        if (a != null) {
            this.navigationRouteRequest = a.navigationRouteRequest;
            if (this.navigationRouteRequest != null) {
                this.destination = new com.here.android.mpa.common.GeoCoordinate(this.navigationRouteRequest.destination.latitude.doubleValue(), this.navigationRouteRequest.destination.longitude.doubleValue());
            }
            this.routeId = a.routeId;
            this.routeOptions = a.routeOptions;
            this.route = a.route;
            this.simulationSpeed = a.simulationSpeed;
            this.streetAddress = a.streetAddress;
            this.destinationLabel = a.destinationLabel;
            this.destinationIdentifier = a.destinationIdentifier;
            this.deviceId = a.deviceId;
            this.destinationDirection = a.destinationDirection;
            this.destinationDirectionStr = a.destinationDirectionStr;
            this.destinationIconId = a.destinationIconId;
            this.maneuverAfterCurrent = a.maneuverAfterCurrent;
            this.maneuverAfterCurrentIconid = a.maneuverAfterCurrentIconid;
            this.lastRequest = null;
            this.lastRouteId = null;
            this.currentRerouteReason = null;
            this.trafficRerouteOnce = false;
            this.startLocation = a.startLocation;
        }
    }
    
    com.navdy.service.library.events.navigation.NavigationRouteRequest getLastNavigationRequest() {
        com.navdy.service.library.events.navigation.NavigationRouteRequest a = this.lastRequest;
        this.lastRequest = null;
        return a;
    }
    
    String getLastRouteId() {
        String s = this.lastRouteId;
        this.lastRouteId = null;
        return s;
    }
    
    long getLastTrafficRerouteTime() {
        return this.lastTrafficRerouteTime;
    }
    
    int getTrafficReRouteCount() {
        return this.trafficRerouteCount;
    }
    
    void incrTrafficRerouteCount() {
        this.trafficRerouteCount = this.trafficRerouteCount + 1;
    }
    
    void setLastTrafficRerouteTime(long j) {
        this.lastTrafficRerouteTime = j;
    }
}
