package com.navdy.hud.app.maps.here;

class HereSpeedWarningManager$4 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit;
    
    static {
        $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit = new int[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit;
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a0 = com.navdy.hud.app.manager.SpeedManager$SpeedUnit.MILES_PER_HOUR;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.KILOMETERS_PER_HOUR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
    }
}
