package com.navdy.hud.app.maps.here;

class NavdyTrafficRerouteManager$4 implements Runnable {
    final com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager this$0;
    
    NavdyTrafficRerouteManager$4(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        label0: {
            Throwable a = null;
            label1: {
                label21: {
                    label19: {
                        label17: {
                            label15: {
                                label13: {
                                    label11: {
                                        label9: {
                                            label7: {
                                                label5: {
                                                    label22: {
                                                        label3: try {
                                                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$400(this.this$0));
                                                            boolean b = com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext());
                                                            label20: {
                                                                if (b) {
                                                                    break label20;
                                                                }
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$0);
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("skip traffic check, no n/w");
                                                                break label21;
                                                            }
                                                            boolean b0 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).isNavigationModeOn();
                                                            label18: {
                                                                if (b0) {
                                                                    break label18;
                                                                }
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$0).dismissReroute();
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("skip traffic check, not navigating");
                                                                break label19;
                                                            }
                                                            boolean b1 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).isRerouting();
                                                            label16: {
                                                                if (!b1) {
                                                                    break label16;
                                                                }
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("skip traffic check, rerouting");
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$0).dismissReroute();
                                                                break label17;
                                                            }
                                                            boolean b2 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).hasArrived();
                                                            label14: {
                                                                if (!b2) {
                                                                    break label14;
                                                                }
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("skip traffic check, arrival mode on");
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$0).dismissReroute();
                                                                break label15;
                                                            }
                                                            boolean b3 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).isOnGasRoute();
                                                            label12: {
                                                                if (!b3) {
                                                                    break label12;
                                                                }
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("skip traffic check, on gas route");
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$0).dismissReroute();
                                                                break label13;
                                                            }
                                                            boolean b4 = com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isLimitBandwidthModeOn();
                                                            label10: {
                                                                if (!b4) {
                                                                    break label10;
                                                                }
                                                                if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).hasTrafficRerouteOnce()) {
                                                                    break label10;
                                                                }
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("user has already selected faster route, no-op in limited b/w condition");
                                                                break label11;
                                                            }
                                                            com.here.android.mpa.routing.Route a0 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getCurrentRoute();
                                                            label8: {
                                                                if (a0 != null) {
                                                                    break label8;
                                                                }
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$0).dismissReroute();
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().v("skip traffic check, no current route");
                                                                break label9;
                                                            }
                                                            com.navdy.hud.app.maps.here.HereMapsManager a1 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
                                                            com.here.android.mpa.common.GeoCoordinate a2 = a1.getLocationFixManager().getLastGeoCoordinate();
                                                            label6: {
                                                                if (a2 != null) {
                                                                    break label6;
                                                                }
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$0);
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().e("skip traffic check, no start point");
                                                                break label7;
                                                            }
                                                            com.here.android.mpa.common.GeoCoordinate a3 = a0.getDestination();
                                                            label4: {
                                                                if (a3 != null) {
                                                                    break label4;
                                                                }
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$0);
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().e("skip traffic check, no end point");
                                                                break label5;
                                                            }
                                                            boolean b5 = com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$000(this.this$0).shouldCalculateFasterRoute();
                                                            label2: {
                                                                if (!b5) {
                                                                    break label2;
                                                                }
                                                                com.here.android.mpa.routing.RouteOptions a4 = a1.getRouteOptions();
                                                                a4.setRouteType(com.here.android.mpa.routing.RouteOptions$Type.FASTEST);
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$200(this.this$0).cancel();
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$400(this.this$0), 45000L);
                                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$200(this.this$0).calculateRoute((com.navdy.service.library.events.navigation.NavigationRouteRequest)null, a2, (java.util.List)null, a3, true, (com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener)new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$4$1(this), 1, a4, true, false, false);
                                                                break label3;
                                                            }
                                                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().e("skip traffic check, time not right");
                                                            break label22;
                                                        } catch(Throwable a5) {
                                                            a = a5;
                                                            break label1;
                                                        }
                                                        if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                                                            break label0;
                                                        }
                                                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                                                        break label0;
                                                    }
                                                    if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                                                        break label0;
                                                    }
                                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                                                    break label0;
                                                }
                                                if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                                                    break label0;
                                                }
                                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                                                break label0;
                                            }
                                            if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                                                break label0;
                                            }
                                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                                            break label0;
                                        }
                                        if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                                            break label0;
                                        }
                                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                                        break label0;
                                    }
                                    if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                                        break label0;
                                    }
                                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                                    break label0;
                                }
                                if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                                    break label0;
                                }
                                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                                break label0;
                            }
                            if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                                break label0;
                            }
                            com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                            break label0;
                        }
                        if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                            break label0;
                        }
                        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                        break label0;
                    }
                    if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                        break label0;
                    }
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                    break label0;
                }
                if (!com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                    break label0;
                }
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                break label0;
            }
            try {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$0);
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().e(a);
            } catch(Throwable a6) {
                if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                    com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
                }
                throw a6;
            }
            if (com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$800(this.this$0)) {
                com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$0).postDelayed(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$900(this.this$0), (long)(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$700(this.this$0).getRerouteInterval() / 2));
            }
        }
    }
}
