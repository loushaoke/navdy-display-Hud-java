package com.navdy.hud.app.maps.here;

class NavdyTrafficRerouteManager$4$1 implements com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener {
    final com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$4 this$1;
    
    NavdyTrafficRerouteManager$4$1(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$4 a) {
        super();
        this.this$1 = a;
    }
    
    public void error(com.here.android.mpa.routing.RoutingError a, Throwable a0) {
        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$1.this$0).removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$400(this.this$1.this$0));
        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$600(this.this$1.this$0);
        if (com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled()) {
            com.navdy.hud.app.framework.voice.TTSUtils.debugShowFasterRouteToast("Faster route not found [Error]", (a0 == null) ? (a == null) ? "" : a.name() : a0.toString());
        }
        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$100().e(new StringBuilder().append("error occured:").append(a).toString(), a0);
    }
    
    public void postSuccess(java.util.ArrayList a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager$4$1$1(this, a), 2);
    }
    
    public void preSuccess() {
        com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$500(this.this$1.this$0).removeCallbacks(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager.access$400(this.this$1.this$0));
    }
    
    public void progress(int i) {
    }
}
