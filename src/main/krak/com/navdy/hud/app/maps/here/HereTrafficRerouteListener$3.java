package com.navdy.hud.app.maps.here;

class HereTrafficRerouteListener$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereTrafficRerouteListener this$0;
    
    HereTrafficRerouteListener$3(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$200(this.this$0) == null) {
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).e("confirmReroute: no route available");
        } else {
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$300(this.this$0).clearNavManeuver();
            com.here.android.mpa.guidance.NavigationManager$Error a = com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$000(this.this$0).setRoute(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$200(this.this$0));
            if (a != com.here.android.mpa.guidance.NavigationManager$Error.NONE) {
                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).e(new StringBuilder().append("confirmReroute: traffic route could not be set:").append(a).toString());
            } else {
                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$300(this.this$0).updateMapRoute(com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$200(this.this$0), com.navdy.service.library.events.navigation.NavigationSessionRouteChange$RerouteReason.NAV_SESSION_TRAFFIC_REROUTE, (String)null, (String)null, false, true);
                com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$100(this.this$0).e("confirmReroute: set");
            }
            com.navdy.hud.app.maps.here.HereTrafficRerouteListener.access$400(this.this$0);
        }
    }
}
