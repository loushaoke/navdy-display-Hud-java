package com.navdy.hud.app.maps.here;

class HereLocationFixManager$5 implements Runnable {
    final com.navdy.hud.app.maps.here.HereLocationFixManager this$0;
    final com.navdy.service.library.events.debug.StartDriveRecordingEvent val$event;
    
    HereLocationFixManager$5(com.navdy.hud.app.maps.here.HereLocationFixManager a, com.navdy.service.library.events.debug.StartDriveRecordingEvent a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        try {
            if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$1000(this.this$0)) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().v("already recording");
            } else {
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$1402(this.this$0, this.val$event.label);
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$1002(this.this$0, true);
                java.io.File a = com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(com.navdy.hud.app.maps.here.HereLocationFixManager.access$1400(this.this$0));
                if (!a.exists()) {
                    a.mkdirs();
                }
                java.io.File a0 = new java.io.File(a, new StringBuilder().append(com.navdy.hud.app.maps.here.HereLocationFixManager.access$1400(this.this$0)).append(".here").toString());
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$1502(this.this$0, new java.io.FileOutputStream(a0));
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().v(new StringBuilder().append("created recording file [").append(com.navdy.hud.app.maps.here.HereLocationFixManager.access$1400(this.this$0)).append("] path:").append(a0.getAbsolutePath()).toString());
            }
        } catch(Throwable a1) {
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().e(a1);
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$1002(this.this$0, false);
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$1402(this.this$0, (String)null);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)com.navdy.hud.app.maps.here.HereLocationFixManager.access$1500(this.this$0));
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$1502(this.this$0, (java.io.FileOutputStream)null);
        }
    }
}
