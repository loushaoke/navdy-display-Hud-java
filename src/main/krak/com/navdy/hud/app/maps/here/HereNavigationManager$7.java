package com.navdy.hud.app.maps.here;

class HereNavigationManager$7 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    final boolean val$clearPrevious;
    final com.here.android.mpa.routing.Maneuver val$maneuverAfterNext;
    final com.here.android.mpa.routing.Maneuver val$nextManeuver;
    final com.here.android.mpa.routing.Maneuver val$previous;
    
    HereNavigationManager$7(com.navdy.hud.app.maps.here.HereNavigationManager a, com.here.android.mpa.routing.Maneuver a0, com.here.android.mpa.routing.Maneuver a1, com.here.android.mpa.routing.Maneuver a2, boolean b) {
        super();
        this.this$0 = a;
        this.val$nextManeuver = a0;
        this.val$maneuverAfterNext = a1;
        this.val$previous = a2;
        this.val$clearPrevious = b;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.maps.here.HereNavigationManager.access$1000(this.this$0, this.val$nextManeuver, this.val$maneuverAfterNext, this.val$previous, this.val$clearPrevious);
        } catch(Throwable a) {
            com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.e("updateNavigationInfoInternal", a);
        }
    }
}
