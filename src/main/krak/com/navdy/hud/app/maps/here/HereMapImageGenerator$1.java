package com.navdy.hud.app.maps.here;

class HereMapImageGenerator$1 implements com.here.android.mpa.common.OnScreenCaptureListener {
    final com.navdy.hud.app.maps.here.HereMapImageGenerator this$0;
    final com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams val$mapGeneratorParams;
    
    HereMapImageGenerator$1(com.navdy.hud.app.maps.here.HereMapImageGenerator a, com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams a0) {
        super();
        this.this$0 = a;
        this.val$mapGeneratorParams = a0;
    }
    
    public void onScreenCaptured(android.graphics.Bitmap a) {
        try {
            com.navdy.hud.app.maps.here.HereMapImageGenerator.access$000(this.this$0);
            com.navdy.hud.app.maps.here.HereMapImageGenerator.access$100().e(new StringBuilder().append("onScreenCaptured bitmap:").append(a).toString());
            com.navdy.hud.app.maps.here.HereMapImageGenerator.access$200(this.this$0, this.val$mapGeneratorParams, a);
        } catch(Throwable a0) {
            com.navdy.hud.app.maps.here.HereMapImageGenerator.access$100().e("onScreenCaptured", a0);
        }
    }
}
