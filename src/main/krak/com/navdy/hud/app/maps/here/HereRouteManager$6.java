package com.navdy.hud.app.maps.here;

final class HereRouteManager$6 implements Runnable {
    final com.navdy.service.library.events.navigation.RouteManeuverRequest val$routeManeuverRequest;
    
    HereRouteManager$6(com.navdy.service.library.events.navigation.RouteManeuverRequest a) {
        super();
        this.val$routeManeuverRequest = a;
    }
    
    public void run() {
        label2: try {
            long j = 0L;
            com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a = null;
            String s = this.val$routeManeuverRequest.routeId;
            label0: {
                label1: {
                    if (s == null) {
                        break label1;
                    }
                    j = android.os.SystemClock.elapsedRealtime();
                    a = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(this.val$routeManeuverRequest.routeId);
                    if (a != null) {
                        break label0;
                    }
                }
                com.navdy.hud.app.maps.here.HereRouteManager.access$1800().sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.RouteManeuverResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, (String)null, (java.util.List)null));
                break label2;
            }
            com.navdy.service.library.events.navigation.NavigationRouteRequest a0 = a.routeRequest;
            String s0 = null;
            if (a0 != null) {
                s0 = a.routeRequest.streetAddress;
            }
            java.util.List a1 = a.route.getManeuvers();
            java.util.ArrayList a2 = new java.util.ArrayList(20);
            int i = a1.size();
            Object a3 = a1;
            com.here.android.mpa.routing.Maneuver a4 = null;
            com.here.android.mpa.routing.Maneuver a5 = null;
            int i0 = 0;
            while(i0 < i) {
                com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a6 = null;
                int i1 = 0;
                if (a5 != null) {
                    a4 = a5;
                }
                a5 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a3).get(i0);
                int i2 = i - 1;
                com.here.android.mpa.routing.Maneuver a7 = null;
                if (i0 < i2) {
                    a7 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a3).get(i0 + 1);
                }
                if (i0 != 0) {
                    a6 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverDisplay(a5, a7 == null, (long)a5.getDistanceToNextManeuver(), s0, a4, a.routeRequest, a7, true, false, (com.navdy.hud.app.maps.MapEvents$DestinationDirection)null);
                    if (a7 == null) {
                        i1 = 0;
                    } else {
                        long j0 = a5.getStartTime().getTime();
                        i1 = (int)((a7.getStartTime().getTime() - j0) / 1000L);
                    }
                } else {
                    a6 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getStartManeuverDisplay(a5, a7);
                    i1 = 0;
                }
                ((java.util.List)a2).add(new com.navdy.service.library.events.navigation.RouteManeuver(a6.currentRoad, a6.navigationTurn, a6.pendingRoad, Float.valueOf(a6.distance), a6.distanceUnit, Integer.valueOf(i1)));
                i0 = i0 + 1;
            }
            long j1 = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HereRouteManager.access$100().v(new StringBuilder().append("Time to build maneuver list:").append(j1 - j).toString());
            com.navdy.hud.app.maps.here.HereRouteManager.access$1800().sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.RouteManeuverResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, (String)null, (java.util.List)a2));
        } catch(Throwable a8) {
            com.navdy.hud.app.maps.here.HereRouteManager.access$1800().sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.RouteManeuverResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, a8.toString(), (java.util.List)null));
            com.navdy.hud.app.maps.here.HereRouteManager.access$100().e(a8);
        }
    }
}
