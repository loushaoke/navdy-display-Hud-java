package com.navdy.hud.app.maps.here;

class HereLaneInfoListener$4 {
    final static int[] $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction;
    final static int[] $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState;
    final static int[] $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn;
    
    static {
        $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn = new int[com.here.android.mpa.routing.Maneuver$Turn.values().length];
        int[] a = $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn;
        com.here.android.mpa.routing.Maneuver$Turn a0 = com.here.android.mpa.routing.Maneuver$Turn.KEEP_RIGHT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.LIGHT_RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.QUITE_RIGHT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.HEAVY_RIGHT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.KEEP_LEFT.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.LIGHT_LEFT.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.QUITE_LEFT.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[com.here.android.mpa.routing.Maneuver$Turn.HEAVY_LEFT.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException6) {
        }
        $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction = new int[com.here.android.mpa.guidance.LaneInformation$Direction.values().length];
        int[] a1 = $SwitchMap$com$here$android$mpa$guidance$LaneInformation$Direction;
        com.here.android.mpa.guidance.LaneInformation$Direction a2 = com.here.android.mpa.guidance.LaneInformation$Direction.UNDEFINED;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException7) {
        }
        $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState = new int[com.here.android.mpa.guidance.LaneInformation$RecommendationState.values().length];
        int[] a3 = $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState;
        com.here.android.mpa.guidance.LaneInformation$RecommendationState a4 = com.here.android.mpa.guidance.LaneInformation$RecommendationState.NOT_RECOMMENDED;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[com.here.android.mpa.guidance.LaneInformation$RecommendationState.HIGHLY_RECOMMENDED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[com.here.android.mpa.guidance.LaneInformation$RecommendationState.RECOMMENDED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$here$android$mpa$guidance$LaneInformation$RecommendationState[com.here.android.mpa.guidance.LaneInformation$RecommendationState.NOT_AVAILABLE.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException11) {
        }
    }
}
