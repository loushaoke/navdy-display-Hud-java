package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

class HereMapsConfigurator {
    final private static com.navdy.hud.app.maps.here.HereMapsConfigurator sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private volatile boolean isAlreadyConfigured;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapsConfigurator.class);
        sInstance = new com.navdy.hud.app.maps.here.HereMapsConfigurator();
    }
    
    HereMapsConfigurator() {
    }
    
    public static com.navdy.hud.app.maps.here.HereMapsConfigurator getInstance() {
        return sInstance;
    }
    
    private void removeOldHereMapsFolders(java.util.List a, String s) {
        Object a0 = a.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            String s0 = (String)((java.util.Iterator)a0).next();
            if (!android.text.TextUtils.equals((CharSequence)s0, (CharSequence)s)) {
                com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), new java.io.File(s0));
            }
        }
    }
    
    void removeMWConfigFolder() {
        String s = com.navdy.hud.app.storage.PathManager.getInstance().getLatestHereMapsConfigPath();
        com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), new java.io.File(s));
    }
    
    void updateMapsConfig() {
        synchronized(this) {
            if (this.isAlreadyConfigured) {
                sLogger.w("MWConfig is already configured");
            } else {
                com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
                android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
                String s = com.navdy.hud.app.storage.PathManager.getInstance().getLatestHereMapsConfigPath();
                sLogger.i("MWConfig: starting");
                java.util.List a0 = com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsConfigDirs();
                com.navdy.hud.app.util.PackagedResource a1 = new com.navdy.hud.app.util.PackagedResource("mwconfig_client", s);
                try {
                    long j = android.os.SystemClock.elapsedRealtime();
                    a1.updateFromResources(a, R.raw.mwconfig_hud, R.raw.mwconfig_hud_md5);
                    this.removeOldHereMapsFolders(a0, s);
                    long j0 = android.os.SystemClock.elapsedRealtime();
                    sLogger.i(new StringBuilder().append("MWConfig: time took ").append(j0 - j).toString());
                    this.isAlreadyConfigured = true;
                } catch(Throwable a2) {
                    sLogger.e("MWConfig error", a2);
                }
                com.navdy.service.library.util.IOUtils.checkIntegrity(a, s, R.raw.here_mwconfig_integrity);
            }
        }
        /*monexit(this)*/;
    }
}
