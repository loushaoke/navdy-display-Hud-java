package com.navdy.hud.app.maps.here;

class HereMapController$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    final com.here.android.mpa.mapping.Map$Animation val$animation;
    final com.here.android.mpa.common.GeoCoordinate val$center;
    final float val$orientation;
    final float val$tilt;
    final double val$zoom;
    
    HereMapController$3(com.navdy.hud.app.maps.here.HereMapController a, com.here.android.mpa.common.GeoCoordinate a0, com.here.android.mpa.mapping.Map$Animation a1, double d, float f, float f0) {
        super();
        this.this$0 = a;
        this.val$center = a0;
        this.val$animation = a1;
        this.val$zoom = d;
        this.val$orientation = f;
        this.val$tilt = f0;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapController.access$000(this.this$0).setCenter(this.val$center, this.val$animation, this.val$zoom, this.val$orientation, this.val$tilt);
    }
}
