package com.navdy.hud.app.maps.here;

class HereMapCameraManager$1$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapCameraManager$1 this$1;
    
    HereMapCameraManager$1$1(com.navdy.hud.app.maps.here.HereMapCameraManager$1 a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        if (this.this$1.this$0.isManualZoom()) {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v("unlocking dial zoom:manual");
            float f = (com.navdy.hud.app.maps.here.HereMapCameraManager.access$600(this.this$1.this$0)) ? 10000f : (float)com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$1.this$0).getZoomLevel();
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$702(this.this$1.this$0, new com.navdy.service.library.events.preferences.LocalPreferences$Builder(com.navdy.hud.app.maps.here.HereMapCameraManager.access$700(this.this$1.this$0)).manualZoomLevel(Float.valueOf(f)).build());
            com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().updateLocalPreferences(com.navdy.hud.app.maps.here.HereMapCameraManager.access$700(this.this$1.this$0));
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v("unlocking dial zoom:manual pref saved");
        } else {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v("unlocking dial zoom:auto");
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$500(this.this$1.this$0);
        }
    }
}
