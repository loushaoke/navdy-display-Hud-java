package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

public class HereRouteManager {
    final private static String EMPTY_STR = "";
    final public static long MAX_ALLOWED_NAV_DISPLAY_POSITION_DIFF = 160934L;
    final private static double MAX_DISTANCE = 15000000.0;
    final private static int MAX_ONLINE_ROUTE_CALCULATION_TIME;
    final private static int MAX_ROUTES;
    final private static String MAX_ROUTES_PROP = "persist.sys.routes.max";
    final private static boolean VERBOSE = false;
    private static volatile String activeGeoCalcId;
    private static com.navdy.service.library.events.navigation.NavigationRouteRequest activeGeoRouteRequest;
    private static volatile String activeRouteCalcId;
    private static com.navdy.hud.app.maps.here.HereRouteCalculator activeRouteCalculator;
    private static boolean activeRouteCancelledByUser;
    private static com.here.android.mpa.search.GeocodeRequest activeRouteGeocodeRequest;
    private static int activeRouteProgress;
    private static com.navdy.service.library.events.navigation.NavigationRouteRequest activeRouteRequest;
    final private static com.squareup.otto.Bus bus;
    final private static android.content.Context context;
    private static android.os.Handler handler;
    final private static com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager;
    final private static Object lockObj;
    final private static com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler;
    final private static java.util.HashSet pendingRouteRequestIds;
    final private static java.util.Queue pendingRouteRequestQueue;
    private static com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent routeCalculationEvent;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRouteManager.class);
        hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        context = com.navdy.hud.app.HudApplication.getAppContext();
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        MAX_ROUTES = com.navdy.hud.app.util.os.SystemProperties.getInt("persist.sys.routes.max", 3);
        MAX_ONLINE_ROUTE_CALCULATION_TIME = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(20L);
        mapsEventHandler = com.navdy.hud.app.maps.MapsEventHandler.getInstance();
        lockObj = new Object();
        pendingRouteRequestQueue = (java.util.Queue)new java.util.LinkedList();
        pendingRouteRequestIds = new java.util.HashSet();
        bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }
    
    public HereRouteManager() {
    }
    
    static com.navdy.hud.app.maps.here.HereMapsManager access$000() {
        return hereMapsManager;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.navdy.service.library.events.navigation.NavigationRouteRequest access$1002(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        activeGeoRouteRequest = a;
        return a;
    }
    
    static void access$1100(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0, java.util.List a1, com.here.android.mpa.common.GeoCoordinate a2, boolean b) {
        com.navdy.hud.app.maps.here.HereRouteManager.routeSearch(a, a0, a1, a2, b);
    }
    
    static int access$1200() {
        return MAX_ONLINE_ROUTE_CALCULATION_TIME;
    }
    
    static void access$1300(com.navdy.service.library.events.navigation.NavigationRouteRequest a, java.util.ArrayList a0, boolean b) {
        com.navdy.hud.app.maps.here.HereRouteManager.returnSuccessResponse(a, a0, b);
    }
    
    static android.os.Handler access$1400() {
        return handler;
    }
    
    static boolean access$1500() {
        return activeRouteCancelledByUser;
    }
    
    static void access$1600(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0, java.util.List a1, com.here.android.mpa.common.GeoCoordinate a2, boolean b) {
        com.navdy.hud.app.maps.here.HereRouteManager.routeSearchNoTraffic(a, a0, a1, a2, b);
    }
    
    static com.navdy.hud.app.maps.here.HereRouteCalculator access$1702(com.navdy.hud.app.maps.here.HereRouteCalculator a) {
        activeRouteCalculator = a;
        return a;
    }
    
    static com.navdy.hud.app.maps.MapsEventHandler access$1800() {
        return mapsEventHandler;
    }
    
    static android.content.Context access$200() {
        return context;
    }
    
    static void access$300(com.navdy.service.library.events.RequestStatus a, com.navdy.service.library.events.navigation.NavigationRouteRequest a0, String s) {
        com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(a, a0, s);
    }
    
    static Object access$400() {
        return lockObj;
    }
    
    static com.navdy.service.library.events.navigation.NavigationRouteRequest access$500() {
        return activeRouteRequest;
    }
    
    static java.util.HashSet access$600() {
        return pendingRouteRequestIds;
    }
    
    static void access$700(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0) {
        com.navdy.hud.app.maps.here.HereRouteManager.handleRouteRequest(a, a0);
    }
    
    static String access$800() {
        return activeGeoCalcId;
    }
    
    static String access$802(String s) {
        activeGeoCalcId = s;
        return s;
    }
    
    static com.here.android.mpa.search.GeocodeRequest access$902(com.here.android.mpa.search.GeocodeRequest a) {
        activeRouteGeocodeRequest = a;
        return a;
    }
    
    public static String buildChangeRouteTTS(String s) {
        return com.navdy.hud.app.maps.here.HereRouteManager.buildRouteTTS(R.string.route_change_msg, s);
    }
    
    public static String buildRouteTTS(int i, String s) {
        String s0 = null;
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            String s1 = null;
            String s2 = null;
            com.navdy.hud.app.maps.here.HereNavigationManager a = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            com.navdy.hud.app.maps.here.HereRouteCache a1 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance();
            if (s == null) {
                s = a.getCurrentRouteId();
            }
            com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a2 = a1.getRoute(s);
            if (a2 == null) {
                s1 = "";
                s2 = "";
            } else {
                s2 = a2.routeResult.via;
                if (Boolean.TRUE.equals(a.getGeneratePhoneticTTS())) {
                    s2 = new StringBuilder().append("\u001b\\tn=address\\").append(s2).append("\u001b\\tn=normal\\").toString();
                }
                java.util.Date a3 = com.navdy.hud.app.maps.here.HereMapUtil.getRouteTtaDate(a2.route);
                StringBuilder a4 = new StringBuilder();
                String s3 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper().formatTime12Hour(a3, a4, false);
                s1 = new StringBuilder().append(s3).append(" ").append(a4.toString()).toString();
            }
            switch(i) {
                case R.string.route_start_msg: {
                    String s4 = null;
                    com.here.android.mpa.routing.RouteOptions a5 = a.getRouteOptions();
                    if (a5 == null) {
                        s4 = "";
                    } else {
                        switch(com.navdy.hud.app.maps.here.HereRouteManager$8.$SwitchMap$com$here$android$mpa$routing$RouteOptions$Type[a5.getRouteType().ordinal()]) {
                            case 2: {
                                s4 = com.navdy.hud.app.maps.notification.RouteCalculationNotification.fastestRoute;
                                break;
                            }
                            case 1: {
                                s4 = com.navdy.hud.app.maps.notification.RouteCalculationNotification.shortestRoute;
                                break;
                            }
                            default: {
                                s4 = "";
                            }
                        }
                    }
                    Object[] a6 = new Object[3];
                    a6[0] = s4;
                    a6[1] = s2;
                    a6[2] = s1;
                    s0 = a0.getString(R.string.route_start_msg, a6);
                    break;
                }
                case R.string.route_change_msg: {
                    Object[] a7 = new Object[2];
                    a7[0] = s2;
                    a7[1] = s1;
                    s0 = a0.getString(R.string.route_change_msg, a7);
                    break;
                }
                default: {
                    s0 = "";
                }
            }
        } else {
            s0 = "";
        }
        return s0;
    }
    
    public static String buildStartRouteTTS() {
        return com.navdy.hud.app.maps.here.HereRouteManager.buildRouteTTS(R.string.route_start_msg, (String)null);
    }
    
    public static boolean cancelActiveRouteRequest() {
        boolean b = false;
        if (android.text.TextUtils.isEmpty((CharSequence)activeRouteCalcId)) {
            sLogger.v("no active route id");
            b = false;
        } else {
            b = com.navdy.hud.app.maps.here.HereRouteManager.handleRouteCancelRequest(new com.navdy.service.library.events.navigation.NavigationRouteCancelRequest(activeRouteCalcId), true);
        }
        return b;
    }
    
    private static void cancelCurrentGeocodeRequest() {
        synchronized(lockObj) {
            com.here.android.mpa.search.GeocodeRequest a0 = activeRouteGeocodeRequest;
            if (a0 != null) {
                try {
                    activeRouteGeocodeRequest.cancel();
                    sLogger.v(new StringBuilder().append("geo: geocode request cancelled:").append(activeGeoCalcId).toString());
                    if (activeGeoRouteRequest != null) {
                        com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, activeGeoRouteRequest, (String)null);
                    }
                } catch(Throwable a1) {
                    sLogger.e(a1);
                }
                activeRouteGeocodeRequest = null;
                activeGeoCalcId = null;
                activeGeoRouteRequest = null;
            }
            /*monexit(a)*/;
        }
    }
    
    public static void clearActiveRouteCalc() {
        synchronized(lockObj) {
            com.navdy.service.library.log.Logger a0 = sLogger;
            a0.v("clearActiveRouteCalc");
            activeRouteCalculator = null;
            activeRouteCalcId = null;
            activeRouteRequest = null;
            activeRouteProgress = 0;
            activeRouteCancelledByUser = false;
            routeCalculationEvent = null;
            int i = pendingRouteRequestQueue.size();
            if (i > 0) {
                sLogger.v(new StringBuilder().append("pending route queue size:").append(i).toString());
                com.navdy.service.library.events.navigation.NavigationRouteRequest a1 = (com.navdy.service.library.events.navigation.NavigationRouteRequest)pendingRouteRequestQueue.remove();
                pendingRouteRequestIds.remove(a1.requestId);
                handler.post((Runnable)new com.navdy.hud.app.maps.here.HereRouteManager$7(a1));
            }
            /*monexit(a)*/;
        }
    }
    
    public static void clearActiveRouteCalc(String s) {
        synchronized(lockObj) {
            String s0 = activeRouteCalcId;
            if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)s0)) {
                com.navdy.hud.app.maps.here.HereRouteManager.clearActiveRouteCalc();
            }
            /*monexit(a)*/;
        }
    }
    
    public static String getActiveRouteCalcId() {
        return activeRouteCalcId;
    }
    
    public static boolean handleRouteCancelRequest(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest a, boolean b) {
        boolean b0 = false;
        label0: try {
            Object a0 = null;
            Throwable a1 = null;
            sLogger.v(new StringBuilder().append("routeCancelRequest:").append(a.handle).toString());
            if (a.handle != null) {
                synchronized(lockObj) {
                    com.here.android.mpa.search.GeocodeRequest a2 = activeRouteGeocodeRequest;
                    label8: {
                        label6: {
                            label4: {
                                label2: {
                                    boolean b1 = false;
                                    try {
                                        label7: {
                                            if (a2 == null) {
                                                break label7;
                                            }
                                            b1 = false;
                                            if (!android.text.TextUtils.equals((CharSequence)a.handle, (CharSequence)activeGeoCalcId)) {
                                                break label7;
                                            }
                                            b1 = false;
                                            com.navdy.hud.app.maps.here.HereRouteManager.cancelCurrentGeocodeRequest();
                                            sLogger.v(new StringBuilder().append("routeCancelRequest: sent navrouteresponse-geocode cancel [").append(a.handle).append("]").toString());
                                            com.navdy.service.library.events.navigation.NavigationRouteResponse a3 = new com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder().requestId(a.handle).status(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED).destination(new com.navdy.service.library.events.location.Coordinate$Builder().latitude(Double.valueOf(0.0)).longitude(Double.valueOf(0.0)).build()).build();
                                            bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a3));
                                            break label8;
                                        }
                                        com.navdy.hud.app.maps.here.HereRouteCalculator a4 = activeRouteCalculator;
                                        label5: {
                                            if (a4 != null) {
                                                break label5;
                                            }
                                            com.navdy.service.library.log.Logger a5 = sLogger;
                                            b1 = false;
                                            a5.v(new StringBuilder().append("no active route calculation:").append(a.handle).toString());
                                            break label6;
                                        }
                                        b1 = false;
                                        boolean b2 = android.text.TextUtils.equals((CharSequence)a.handle, (CharSequence)activeRouteCalcId);
                                        label3: {
                                            if (b2) {
                                                break label3;
                                            }
                                            com.navdy.service.library.log.Logger a6 = sLogger;
                                            b1 = false;
                                            a6.v(new StringBuilder().append("invalid active route id:").append(a.handle).toString());
                                            break label4;
                                        }
                                        boolean b3 = activeRouteCancelledByUser;
                                        label1: {
                                            if (!b3) {
                                                break label1;
                                            }
                                            com.navdy.service.library.log.Logger a7 = sLogger;
                                            b1 = false;
                                            a7.v(new StringBuilder().append("request already cancelled:").append(a.handle).toString());
                                            break label2;
                                        }
                                        if (b) {
                                            com.navdy.service.library.log.Logger a8 = sLogger;
                                            b1 = false;
                                            a8.v("send cancel to client");
                                            bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a));
                                        }
                                        activeRouteCancelledByUser = true;
                                        com.navdy.hud.app.maps.here.HereRouteCalculator a9 = activeRouteCalculator;
                                        b1 = true;
                                        a9.cancel();
                                        sLogger.v("called cancel");
                                        sLogger.v(new StringBuilder().append("routeCancelRequest: sent navrouteresponse cancel [").append(a.handle).append("]").toString());
                                        com.navdy.service.library.events.navigation.NavigationRouteResponse a10 = new com.navdy.service.library.events.navigation.NavigationRouteResponse$Builder().requestId(a.handle).status(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED).destination(new com.navdy.service.library.events.location.Coordinate$Builder().latitude(Double.valueOf(0.0)).longitude(Double.valueOf(0.0)).build()).build();
                                        bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a10));
                                    } catch(Throwable a11) {
                                        if (!b1) {
                                            sLogger.v(new StringBuilder().append("may be pending route exists: ").append(a.handle).toString());
                                            com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a12 = new com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent();
                                            a12.pendingNavigationRequestId = a.handle;
                                            a12.abortOriginDisplay = b;
                                            a12.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.ABORT_NAVIGATION;
                                            bus.post(a12);
                                        }
                                        throw a11;
                                    }
                                    /*monexit(a0)*/;
                                    b0 = true;
                                    break label0;
                                }
                                sLogger.v(new StringBuilder().append("may be pending route exists: ").append(a.handle).toString());
                                com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a13 = new com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent();
                                a13.pendingNavigationRequestId = a.handle;
                                a13.abortOriginDisplay = b;
                                a13.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.ABORT_NAVIGATION;
                                bus.post(a13);
                                /*monexit(a0)*/;
                                b0 = false;
                                break label0;
                            }
                            sLogger.v(new StringBuilder().append("may be pending route exists: ").append(a.handle).toString());
                            com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a14 = new com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent();
                            a14.pendingNavigationRequestId = a.handle;
                            a14.abortOriginDisplay = b;
                            a14.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.ABORT_NAVIGATION;
                            bus.post(a14);
                            /*monexit(a0)*/;
                            b0 = false;
                            break label0;
                        }
                        sLogger.v(new StringBuilder().append("may be pending route exists: ").append(a.handle).toString());
                        com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a15 = new com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent();
                        a15.pendingNavigationRequestId = a.handle;
                        a15.abortOriginDisplay = b;
                        a15.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(a15);
                        /*monexit(a0)*/;
                        b0 = false;
                        break label0;
                    }
                    sLogger.v(new StringBuilder().append("may be pending route exists: ").append(a.handle).toString());
                    com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a16 = new com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent();
                    a16.pendingNavigationRequestId = a.handle;
                    a16.abortOriginDisplay = b;
                    a16.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.ABORT_NAVIGATION;
                    bus.post(a16);
                    /*monexit(a0)*/;
                    b0 = true;
                    break label0;
                }
            } else {
                sLogger.v("routeCancelRequest:null handle");
                b0 = false;
                break label0;
            }

        } catch(Throwable a20) {
            sLogger.e(a20);
            b0 = false;
        }
        return b0;
    }
    
    public static void handleRouteManeuverRequest(com.navdy.service.library.events.navigation.RouteManeuverRequest a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereRouteManager$6(a), 2);
    }
    
    public static void handleRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereRouteManager$1(a), 19);
    }
    
    private static void handleRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0) {
        com.here.android.mpa.common.GeoCoordinate a1 = null;
        long j = android.os.SystemClock.elapsedRealtime();
        java.util.ArrayList a2 = new java.util.ArrayList();
        if (a.waypoints != null) {
            Object a3 = a.waypoints.iterator();
            while(((java.util.Iterator)a3).hasNext()) {
                com.navdy.service.library.events.location.Coordinate a4 = (com.navdy.service.library.events.location.Coordinate)((java.util.Iterator)a3).next();
                ((java.util.List)a2).add(new com.here.android.mpa.common.GeoCoordinate(a4.latitude.doubleValue(), a4.longitude.doubleValue()));
            }
        }
        com.navdy.service.library.events.location.Coordinate a5 = a.destinationDisplay;
        label13: {
            label11: {
                label12: {
                    if (a5 == null) {
                        break label12;
                    }
                    if (a.destinationDisplay.latitude.doubleValue() == 0.0) {
                        break label12;
                    }
                    if (a.destinationDisplay.longitude.doubleValue() == 0.0) {
                        break label12;
                    }
                    if (a.destination.latitude == a.destinationDisplay.latitude) {
                        break label12;
                    }
                    if (a.destination.longitude != a.destinationDisplay.longitude) {
                        break label11;
                    }
                }
                sLogger.v("display/nav distance check pass: no display pos");
                a1 = new com.here.android.mpa.common.GeoCoordinate(a.destination.latitude.doubleValue(), a.destination.longitude.doubleValue());
                break label13;
            }
            long j0 = (long)new com.here.android.mpa.common.GeoCoordinate(a.destinationDisplay.latitude.doubleValue(), a.destinationDisplay.longitude.doubleValue()).distanceTo(new com.here.android.mpa.common.GeoCoordinate(a.destination.latitude.doubleValue(), a.destination.longitude.doubleValue()));
            if (j0 < 160934L) {
                sLogger.v(new StringBuilder().append("display/nav distance check pass:").append(j0).toString());
                a1 = new com.here.android.mpa.common.GeoCoordinate(a.destination.latitude.doubleValue(), a.destination.longitude.doubleValue());
            } else {
                a1 = new com.here.android.mpa.common.GeoCoordinate(a.destinationDisplay.latitude.doubleValue(), a.destinationDisplay.longitude.doubleValue());
                sLogger.e(new StringBuilder().append("display/nav distance check failed:").append(j0).toString());
                com.navdy.hud.app.analytics.AnalyticsSupport.recordBadRoutePosition(new StringBuilder().append(a.destinationDisplay.latitude).append(",").append(a.destinationDisplay.longitude).toString(), new StringBuilder().append(a.destination.latitude).append(",").append(a.destination.longitude).toString(), a.streetAddress);
            }
        }
        sLogger.v(new StringBuilder().append("handleRouteRequest start=").append(a0).append(", end=").append(a1).toString());
        boolean b = Boolean.TRUE.equals(a.geoCodeStreetAddress);
        label2: {
            boolean b0 = false;
            label9: {
                label10: {
                    if (b) {
                        break label10;
                    }
                    double d = a0.distanceTo(a1);
                    if (!(d > 15000000.0)) {
                        b0 = false;
                        break label9;
                    }
                    sLogger.e(new StringBuilder().append("distance between start and endpoint:").append(d).append(" max:").append(15000000.0).toString());
                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, a, context.getString(R.string.long_drive_error));
                    break label2;
                }
                boolean b1 = android.text.TextUtils.isEmpty((CharSequence)a.streetAddress);
                label8: {
                    if (b1) {
                        break label8;
                    }
                    b0 = true;
                    break label9;
                }
                com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_REQUEST, a, context.getString(R.string.unknown_address));
                break label2;
            }
            boolean b2 = hereMapsManager.isEngineOnline();
            sLogger.v(new StringBuilder().append("maps engine online:").append(b2).toString());
            label7: {
                label6: {
                    if (b0) {
                        break label6;
                    }
                    sLogger.v("geocoding not set");
                    com.navdy.hud.app.maps.here.HereRouteManager.routeSearch(a, a0, (java.util.List)a2, a1, b2);
                    break label7;
                }
                sLogger.v(new StringBuilder().append("Street Address:").append(a.streetAddress).toString());
                synchronized(lockObj) {
                    boolean b3 = com.navdy.hud.app.maps.here.HereRouteManager.isRouteCalcInProgress();
                    label5: {
                        if (!b3) {
                            break label5;
                        }
                        Boolean a7 = a.cancelCurrent;
                        label3: {
                            label4: {
                                if (a7 == null) {
                                    break label4;
                                }
                                if (a.cancelCurrent.booleanValue()) {
                                    break label3;
                                }
                            }
                            com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, a, activeRouteCalcId);
                            /*monexit(a6)*/;
                            break label2;
                        }
                        if (activeRouteCalculator != null) {
                            sLogger.v(new StringBuilder().append("geo:route cancelling current request :").append(activeRouteCalcId).toString());
                            activeRouteCalculator.cancel();
                            activeRouteCalculator = null;
                            com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, activeRouteRequest, activeRouteCalcId);
                        }
                    }
                    boolean b4 = com.navdy.hud.app.maps.here.HereRouteManager.isRouteCalcGeocodeRequestInProgress();
                    label1: {
                        if (!b4) {
                            break label1;
                        }
                        Boolean a8 = a.cancelCurrent;
                        label0: {
                            if (a8 == null) {
                                break label0;
                            }
                            if (!a.cancelCurrent.booleanValue()) {
                                break label0;
                            }
                            com.navdy.hud.app.maps.here.HereRouteManager.cancelCurrentGeocodeRequest();
                            break label1;
                        }
                        com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, a, activeGeoCalcId);
                        /*monexit(a6)*/;
                        break label2;
                    }
                    activeGeoCalcId = a.requestId;
                    activeGeoRouteRequest = a;
                    activeRouteGeocodeRequest = com.navdy.hud.app.maps.here.HerePlacesManager.geoCodeStreetAddress(a0, a.streetAddress, 100000, (com.navdy.hud.app.maps.here.HerePlacesManager$GeoCodeCallback)new com.navdy.hud.app.maps.here.HereRouteManager$2(a, a0, (java.util.List)a2, b2));
                    if (activeRouteGeocodeRequest != null) {
                        sLogger.v("geocode request triggered");
                    } else {
                        activeRouteGeocodeRequest = null;
                        activeGeoCalcId = null;
                        sLogger.v("geocode request failed");
                    }
                    /*monexit(a6)*/;
                }
            }
            long j1 = android.os.SystemClock.elapsedRealtime();
            sLogger.v(new StringBuilder().append("handleRouteRequest- time-1 =").append(j1 - j).toString());
        }
    }
    
    private static boolean isRouteCalcGeocodeRequestInProgress() {
        boolean b = false;
        synchronized(lockObj) {
            com.here.android.mpa.search.GeocodeRequest a0 = activeRouteGeocodeRequest;
            if (a0 == null) {
                /*monexit(a)*/;
                b = false;
            } else {
                sLogger.i(new StringBuilder().append("route calculation geocode already in progress:").append(activeRouteCalcId).toString());
                /*monexit(a)*/;
                b = true;
            }
        }
        return b;
    }
    
    private static boolean isRouteCalcInProgress() {
        boolean b = false;
        synchronized(lockObj) {
            com.navdy.hud.app.maps.here.HereRouteCalculator a0 = activeRouteCalculator;
            if (a0 == null) {
                /*monexit(a)*/;
                b = false;
            } else {
                sLogger.i(new StringBuilder().append("route calculation already in progress:").append(activeRouteCalcId).toString());
                /*monexit(a)*/;
                b = true;
            }
        }
        return b;
    }
    
    public static boolean isUIShowingRouteCalculation() {
        return com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification("navdy#route#calc#notif") != null;
    }
    
    public static void printRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        try {
            java.util.List a0 = a.routeAttributes;
            String s = null;
            if (a0 != null) {
                int i = a0.size();
                s = null;
                if (i > 0) {
                    StringBuilder a1 = new StringBuilder();
                    Object a2 = a0.iterator();
                    while(((java.util.Iterator)a2).hasNext()) {
                        a1.append(((com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute)((java.util.Iterator)a2).next()).name());
                        a1.append(",");
                    }
                    s = a1.toString();
                }
            }
            sLogger.v(new StringBuilder().append("NavigationRouteRequest label[").append(a.label).append("] local[").append(a.originDisplay).append("] streetAddress[").append(a.streetAddress).append("] destination_id[").append(a.destination_identifier).append("] autonav[").append(a.autoNavigate).append("] geoCode[").append(a.geoCodeStreetAddress).append("] dest_coordinate[").append(a.destination).append("] favType[").append(a.destinationType).append("] requestId[").append(a.requestId).append("]").append("] display_coordinate[").append(a.destinationDisplay).append("] route attrs[").append(s).append("]").toString());
        } catch(Throwable a3) {
            sLogger.e(a3);
        }
    }
    
    private static void returnErrorResponse(com.navdy.service.library.events.RequestStatus a, com.navdy.service.library.events.navigation.NavigationRouteRequest a0, String s) {
        boolean b = false;
        Throwable a1 = null;
        label1: {
            label0: {
                label3: try {
                    boolean b0 = false;
                    com.navdy.service.library.log.Logger a2 = sLogger;
                    b = true;
                    a2.e(new StringBuilder().append(a).append(": ").append(s).toString());
                    com.navdy.service.library.events.navigation.NavigationRouteResponse a3 = new com.navdy.service.library.events.navigation.NavigationRouteResponse(a, s, a0.destination, (String)null, (java.util.List)null, Boolean.valueOf(false), a0.requestId);
                    com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient((com.squareup.wire.Message)a3);
                    synchronized(lockObj) {
                        com.navdy.service.library.events.RequestStatus a5 = com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS;
                        label2: {
                            if (a != a5) {
                                break label2;
                            }
                            b0 = false;
                            /*monexit(a4)*/;
                            break label0;
                        }
                        if (routeCalculationEvent != null) {
                            com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a6 = routeCalculationEvent;
                            b0 = true;
                            a6.response = a3;
                            routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.STOPPED;
                            bus.post(routeCalculationEvent);
                        }
                        b0 = true;
                        /*monexit(a4)*/;
                        break label3;
                    }
                } catch(Throwable a11) {
                    try {
                        sLogger.e(a11);
                    } catch(Throwable a12) {
                        a1 = a12;
                        break label1;
                    }
                    if (!b) {
                        break label0;
                    }
                    com.navdy.hud.app.maps.here.HereRouteManager.clearActiveRouteCalc();
                    break label0;
                }
                com.navdy.hud.app.maps.here.HereRouteManager.clearActiveRouteCalc();
            }
            return;
        }
        if (b) {
            com.navdy.hud.app.maps.here.HereRouteManager.clearActiveRouteCalc();
        }
        throw a1;
    }
    
    private static void returnStatusResponse(String s, int i, String s0) {
        try {
            sLogger.v(new StringBuilder().append("handle[").append(s).append("] progress [").append(i).append("]").toString());
            com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.NavigationRouteStatus(s, Integer.valueOf(i), s0));
        } catch(Throwable a) {
            sLogger.e(a);
        }
    }
    
    private static void returnSuccessResponse(com.navdy.service.library.events.navigation.NavigationRouteRequest a, java.util.ArrayList a0, boolean b) {
        label1: {
            Throwable a1 = null;
            label0: {
                try {
                    com.navdy.service.library.log.Logger a2 = null;
                    com.navdy.service.library.events.navigation.NavigationRouteResponse a3 = new com.navdy.service.library.events.navigation.NavigationRouteResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS, "", a.destination, a.label, (java.util.List)a0, Boolean.valueOf(b), a.requestId);
                    if (!b) {
                        com.navdy.hud.app.framework.voice.TTSUtils.debugShowNoTrafficToast();
                    }
                    com.navdy.hud.app.maps.MapsEventHandler.getInstance().sendEventToClient((com.squareup.wire.Message)a3);
                    synchronized(lockObj) {
                        com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent a5 = routeCalculationEvent;
                        if (a5 != null) {
                            routeCalculationEvent.response = a3;
                            routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.STOPPED;
                            bus.post(routeCalculationEvent);
                        }
                        /*monexit(a4)*/;
                        a2 = sLogger;
                    }
                    a2.v("sent route response");
                    if (a.autoNavigate != null && a.autoNavigate.booleanValue()) {
                        sLogger.i(new StringBuilder().append("start auto-navigation to route ").append(a.label).toString());
                        com.navdy.service.library.events.navigation.NavigationSessionRequest a10 = new com.navdy.service.library.events.navigation.NavigationSessionRequest$Builder().newState(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED).label(a.label).routeId(((com.navdy.service.library.events.navigation.NavigationRouteResult)a0.get(0)).routeId).simulationSpeed(Integer.valueOf(0)).originDisplay(Boolean.valueOf(a.originDisplay != null && a.originDisplay.booleanValue())).build();
                        bus.post(a10);
                        bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a10));
                    }
                    com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance().trackCalculationWithTraffic(b);
                } catch(Throwable a11) {
                    a1 = a11;
                    break label0;
                }
                com.navdy.hud.app.maps.here.HereRouteManager.clearActiveRouteCalc();
                break label1;
            }
            try {
                sLogger.e(a1);
            } catch(Throwable a12) {
                com.navdy.hud.app.maps.here.HereRouteManager.clearActiveRouteCalc();
                throw a12;
            }
            com.navdy.hud.app.maps.here.HereRouteManager.clearActiveRouteCalc();
        }
    }
    
    private static void routeSearch(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0, java.util.List a1, com.here.android.mpa.common.GeoCoordinate a2, boolean b) {
            label4: synchronized(lockObj) {
            label6: try {
                boolean b0 = com.navdy.hud.app.maps.here.HereRouteManager.isRouteCalcGeocodeRequestInProgress();
                label8: {
                    if (!b0) {
                        break label8;
                    }
                    Boolean a4 = a.cancelCurrent;
                    label7: {
                        if (a4 == null) {
                            break label7;
                        }
                        if (!a.cancelCurrent.booleanValue()) {
                            break label7;
                        }
                        com.navdy.hud.app.maps.here.HereRouteManager.cancelCurrentGeocodeRequest();
                        break label8;
                    }
                    com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, a, activeGeoCalcId);
                    /*monexit(a3)*/;
                    break label4;
                }
                boolean b1 = com.navdy.hud.app.maps.here.HereRouteManager.isRouteCalcInProgress();
                label5: {
                    if (b1) {
                        break label5;
                    }
                    /*monexit(a3)*/;
                    break label6;
                }
                Boolean a5 = a.cancelCurrent;
                label1: {
                    label2: {
                        label3: {
                            if (a5 == null) {
                                break label3;
                            }
                            if (a.cancelCurrent.booleanValue()) {
                                break label2;
                            }
                        }
                        com.navdy.hud.app.maps.here.HereRouteManager.returnErrorResponse(com.navdy.service.library.events.RequestStatus.REQUEST_ALREADY_IN_PROGRESS, a, activeRouteCalcId);
                        break label1;
                    }
                    pendingRouteRequestQueue.add(a);
                    pendingRouteRequestIds.add(a.requestId);
                    sLogger.v("add to pending Queue");
                    boolean b2 = activeRouteCancelledByUser;
                    label0: {
                        if (b2) {
                            break label0;
                        }
                        sLogger.v(new StringBuilder().append("cancelling current request :").append(activeRouteCalcId).toString());
                        activeRouteCancelledByUser = true;
                        activeRouteCalculator.cancel();
                        break label1;
                    }
                    sLogger.v("cancellation in progress");
                    /*monexit(a3)*/;
                    break label4;
                }
                /*monexit(a3)*/;
                break label4;
            } catch(Throwable a6) {
                Throwable a7 = a6;
            }
            if (b) {
                com.navdy.hud.app.maps.here.HereRouteManager.routeSearchTraffic(a, a0, a1, a2, true);
            } else {
                com.navdy.hud.app.maps.here.HereRouteManager.routeSearchNoTraffic(a, a0, a1, a2, true);
            }
        }
    }
    
    private static void routeSearchNoTraffic(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0, java.util.List a1, com.here.android.mpa.common.GeoCoordinate a2, boolean b) {
        Object a3 = null;
        Throwable a4 = null;
        com.navdy.hud.app.maps.here.HereRouteCalculator a5 = new com.navdy.hud.app.maps.here.HereRouteCalculator(sLogger, false);
        com.here.android.mpa.routing.RouteOptions a6 = hereMapsManager.getRouteOptions();
        label1: {
            Object a7 = null;
            Throwable a8 = null;
            label0: {
                if (b) {
                    activeRouteCalcId = a.requestId;
                    activeRouteRequest = a;
                    activeRouteProgress = 0;
                    activeRouteCalculator = a5;
                    com.navdy.hud.app.maps.here.HereRouteManager.returnStatusResponse(activeRouteCalcId, activeRouteProgress, activeRouteRequest.requestId);
                    synchronized(lockObj) {
                        routeCalculationEvent = new com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent();
                        routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.STARTED;
                        routeCalculationEvent.start = a0;
                        routeCalculationEvent.waypoints = a1;
                        routeCalculationEvent.end = a2;
                        routeCalculationEvent.request = a;
                        routeCalculationEvent.routeOptions = a6;
                        bus.post(routeCalculationEvent);
                        /*monexit(a3)*/;
                    }
                } else {
                    synchronized(lockObj) {
                        com.navdy.hud.app.maps.here.HereRouteCalculator a10 = activeRouteCalculator;
                        if (a10 != null) {
                            sLogger.v("replaced active route calc with no traffic");
                            activeRouteCalculator = a5;
                        }
                        /*monexit(a7)*/;
                    }
                }
                sLogger.v("no-traffic route calc started");
                a5.calculateRoute(a, a0, a1, a2, false, (com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener)new com.navdy.hud.app.maps.here.HereRouteManager$5(a), MAX_ROUTES, a6, true);
                return;
            }
        }
    }
    
    private static void routeSearchTraffic(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.here.android.mpa.common.GeoCoordinate a0, java.util.List a1, com.here.android.mpa.common.GeoCoordinate a2, boolean b) {
        com.navdy.hud.app.maps.here.HereRouteCalculator a3 = new com.navdy.hud.app.maps.here.HereRouteCalculator(sLogger, false);
        label0: {
            Throwable a4 = null;
            if (!b) {
                break label0;
            }
            synchronized(lockObj) {
                activeRouteCalcId = a.requestId;
                activeRouteRequest = a;
                activeRouteProgress = 0;
                activeRouteCalculator = a3;
                com.navdy.hud.app.maps.here.HereRouteManager.returnStatusResponse(activeRouteCalcId, activeRouteProgress, activeRouteRequest.requestId);
                /*monexit(a5)*/;
                break label0;
            }

        }
        com.navdy.hud.app.maps.here.HereRouteManager$3 a9 = new com.navdy.hud.app.maps.here.HereRouteManager$3(a3);
        sLogger.v("traffic route calc started");
        handler.postDelayed((Runnable)a9, (long)MAX_ONLINE_ROUTE_CALCULATION_TIME);
        com.here.android.mpa.routing.RouteOptions a10 = hereMapsManager.getRouteOptions();
        synchronized(lockObj) {
            routeCalculationEvent = new com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent();
            routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.STARTED;
            routeCalculationEvent.start = a0;
            routeCalculationEvent.waypoints = a1;
            routeCalculationEvent.end = a2;
            routeCalculationEvent.request = a;
            routeCalculationEvent.routeOptions = a10;
            bus.post(routeCalculationEvent);
            /*monexit(a11)*/;
        }
        a3.calculateRoute(a, a0, a1, a2, true, (com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener)new com.navdy.hud.app.maps.here.HereRouteManager$4(a, (Runnable)a9, a0, a1, a2), MAX_ROUTES, a10, true);
    }
    
    public static void startNavigationLookup(com.navdy.hud.app.framework.destinations.Destination a) {
        Object a0 = null;
        Throwable a1 = null;
        label0: {
            if (a != null) {
                synchronized(lockObj) {
                    routeCalculationEvent = new com.navdy.hud.app.maps.MapEvents$RouteCalculationEvent();
                    routeCalculationEvent.state = com.navdy.hud.app.maps.MapEvents$RouteCalculationState.FINDING_NAV_COORDINATES;
                    routeCalculationEvent.lookupDestination = a;
                    routeCalculationEvent.routeOptions = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getRouteOptions();
                    sLogger.v("startNavigationLookup sent nav_coordinate_lookup ");
                    bus.post(routeCalculationEvent);
                    /*monexit(a0)*/;
                }
            } else {
                sLogger.e("startNavigationLookup null destination");
            }
            return;
        }

    }
}
