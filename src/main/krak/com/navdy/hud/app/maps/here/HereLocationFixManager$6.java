package com.navdy.hud.app.maps.here;

class HereLocationFixManager$6 implements Runnable {
    final com.navdy.hud.app.maps.here.HereLocationFixManager this$0;
    
    HereLocationFixManager$6(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        try {
            if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$1000(this.this$0)) {
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$1002(this.this$0, false);
                com.navdy.service.library.util.IOUtils.fileSync(com.navdy.hud.app.maps.here.HereLocationFixManager.access$1500(this.this$0));
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)com.navdy.hud.app.maps.here.HereLocationFixManager.access$1500(this.this$0));
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$1502(this.this$0, (java.io.FileOutputStream)null);
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().v(new StringBuilder().append("stopped recording file [").append(com.navdy.hud.app.maps.here.HereLocationFixManager.access$1400(this.this$0)).append("]").toString());
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$1402(this.this$0, (String)null);
            } else {
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().v("not recording");
            }
        } catch(Throwable a) {
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().e(a);
        }
    }
}
