package com.navdy.hud.app.maps.here;

class HereMapsManager$1$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager$1 this$1;
    final long val$t1;
    
    HereMapsManager$1$1(com.navdy.hud.app.maps.here.HereMapsManager$1 a, long j) {
        super();
        this.this$1 = a;
        this.val$t1 = j;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapsManager.access$200(this.this$1.this$0).onResume();
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(":initializing maps config");
        com.navdy.hud.app.maps.here.HereMapsConfigurator.getInstance().updateMapsConfig();
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("creating geopos");
        com.here.android.mpa.common.GeoPosition a = new com.here.android.mpa.common.GeoPosition(new com.here.android.mpa.common.GeoCoordinate(37.802086, -122.419015));
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("created geopos");
        if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
            long j = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HereLaneInfoBuilder.init();
            com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("time to init HereLaneInfoBuilder [").append(android.os.SystemClock.elapsedRealtime() - j).append("]").toString());
        }
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("engine initialized refcount:").append(com.navdy.hud.app.maps.here.HereMapsManager.access$200(this.this$1.this$0).getResourceReferenceCount()).toString());
        try {
            com.navdy.hud.app.maps.here.HereMapsManager.access$302(this.this$1.this$0, new com.here.android.mpa.mapping.Map());
        } catch(Throwable a0) {
            if (a0.getMessage().contains((CharSequence)"Invalid configuration file. Check MWConfig!")) {
                com.navdy.hud.app.maps.here.HereMapsManager.access$100().e("MWConfig is corrupted! Cleaning up and restarting HUD app.");
                com.navdy.hud.app.maps.here.HereMapsConfigurator.getInstance().removeMWConfigFolder();
            }
            com.navdy.hud.app.maps.here.HereMapsManager.access$400(this.this$1.this$0).post((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$1$1$1(this, a0));
        }
        com.navdy.hud.app.maps.here.HereMapsManager.access$300(this.this$1.this$0).setProjectionMode(com.here.android.mpa.mapping.Map$Projection.MERCATOR);
        com.navdy.hud.app.maps.here.HereMapsManager.access$300(this.this$1.this$0).setTrafficInfoVisible(true);
        com.navdy.hud.app.maps.here.HereMapsManager.access$502(this.this$1.this$0, new com.navdy.hud.app.maps.here.HereMapController(com.navdy.hud.app.maps.here.HereMapsManager.access$300(this.this$1.this$0), com.navdy.hud.app.maps.here.HereMapController$State.NONE));
        com.navdy.hud.app.maps.here.HereMapsManager.access$600(this.this$1.this$0, new com.here.android.mpa.common.GeoCoordinate(37.802086, -122.419015));
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("setting default map attributes");
        com.navdy.hud.app.maps.here.HereMapsManager.access$700(this.this$1.this$0);
        com.navdy.hud.app.maps.here.HereMapsManager.access$800(this.this$1.this$0, com.navdy.hud.app.maps.here.HereMapsManager.access$300(this.this$1.this$0));
        com.navdy.hud.app.maps.here.HereMapsManager.access$902(this.this$1.this$0, com.here.android.mpa.common.PositioningManager.getInstance());
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MAP-ENGINE-INIT-2 took [").append(android.os.SystemClock.elapsedRealtime() - this.val$t1).append("]").toString());
        long j0 = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.maps.here.HereMapsManager.access$400(this.this$1.this$0).post((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$1$1$2(this, j0));
    }
}
