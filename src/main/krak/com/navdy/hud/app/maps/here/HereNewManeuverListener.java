package com.navdy.hud.app.maps.here;

class HereNewManeuverListener extends com.here.android.mpa.guidance.NavigationManager$NewInstructionEventListener {
    final private com.squareup.otto.Bus bus;
    private boolean hasNewRoute;
    final private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    final private com.navdy.service.library.log.Logger logger;
    final private com.navdy.hud.app.maps.here.HereNavController navController;
    private com.here.android.mpa.routing.Maneuver navManeuver;
    final private String tag;
    final private boolean verbose;
    
    HereNewManeuverListener(com.navdy.service.library.log.Logger a, String s, boolean b, com.navdy.hud.app.maps.here.HereNavController a0, com.navdy.hud.app.maps.here.HereNavigationManager a1, com.squareup.otto.Bus a2) {
        this.logger = a;
        this.tag = s;
        this.verbose = b;
        this.navController = a0;
        this.hereNavigationManager = a1;
        this.bus = a2;
    }
    
    static com.navdy.hud.app.maps.here.HereNavController access$000(com.navdy.hud.app.maps.here.HereNewManeuverListener a) {
        return a.navController;
    }
    
    static com.here.android.mpa.routing.Maneuver access$102(com.navdy.hud.app.maps.here.HereNewManeuverListener a, com.here.android.mpa.routing.Maneuver a0) {
        a.navManeuver = a0;
        return a0;
    }
    
    static String access$200(com.navdy.hud.app.maps.here.HereNewManeuverListener a) {
        return a.tag;
    }
    
    static com.navdy.service.library.log.Logger access$300(com.navdy.hud.app.maps.here.HereNewManeuverListener a) {
        return a.logger;
    }
    
    static boolean access$400(com.navdy.hud.app.maps.here.HereNewManeuverListener a) {
        return a.verbose;
    }
    
    static boolean access$500(com.navdy.hud.app.maps.here.HereNewManeuverListener a) {
        return a.hasNewRoute;
    }
    
    static boolean access$502(com.navdy.hud.app.maps.here.HereNewManeuverListener a, boolean b) {
        a.hasNewRoute = b;
        return b;
    }
    
    static com.navdy.hud.app.maps.here.HereNavigationManager access$600(com.navdy.hud.app.maps.here.HereNewManeuverListener a) {
        return a.hereNavigationManager;
    }
    
    static com.squareup.otto.Bus access$700(com.navdy.hud.app.maps.here.HereNewManeuverListener a) {
        return a.bus;
    }
    
    void clearNavManeuver() {
        this.navManeuver = null;
    }
    
    com.here.android.mpa.routing.Maneuver getNavManeuver() {
        return this.navManeuver;
    }
    
    public void onNewInstructionEvent() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereNewManeuverListener$1(this), 20);
    }
    
    void setNewRoute() {
        this.logger.v("setNewRoute");
        this.hasNewRoute = true;
        if (this.logger.isLoggable(2)) {
            this.logger.v("setNewRoute: send empty maneuver");
        }
        this.bus.post(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.EMPTY_MANEUVER_DISPLAY);
    }
}
