package com.navdy.hud.app.maps;


public enum NavigationMode {
    MAP(0),
    MAP_ON_ROUTE(1),
    TBT_ON_ROUTE(2);

    private int value;
    NavigationMode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class NavigationMode extends Enum {
//    final private static com.navdy.hud.app.maps.NavigationMode[] $VALUES;
//    final public static com.navdy.hud.app.maps.NavigationMode MAP;
//    final public static com.navdy.hud.app.maps.NavigationMode MAP_ON_ROUTE;
//    final public static com.navdy.hud.app.maps.NavigationMode TBT_ON_ROUTE;
//    
//    static {
//        MAP = new com.navdy.hud.app.maps.NavigationMode("MAP", 0);
//        MAP_ON_ROUTE = new com.navdy.hud.app.maps.NavigationMode("MAP_ON_ROUTE", 1);
//        TBT_ON_ROUTE = new com.navdy.hud.app.maps.NavigationMode("TBT_ON_ROUTE", 2);
//        com.navdy.hud.app.maps.NavigationMode[] a = new com.navdy.hud.app.maps.NavigationMode[3];
//        a[0] = MAP;
//        a[1] = MAP_ON_ROUTE;
//        a[2] = TBT_ON_ROUTE;
//        $VALUES = a;
//    }
//    
//    private NavigationMode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.maps.NavigationMode valueOf(String s) {
//        return (com.navdy.hud.app.maps.NavigationMode)Enum.valueOf(com.navdy.hud.app.maps.NavigationMode.class, s);
//    }
//    
//    public static com.navdy.hud.app.maps.NavigationMode[] values() {
//        return $VALUES.clone();
//    }
//}
//