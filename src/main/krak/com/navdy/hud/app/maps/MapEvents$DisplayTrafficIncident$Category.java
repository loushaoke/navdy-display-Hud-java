package com.navdy.hud.app.maps;


public enum MapEvents$DisplayTrafficIncident$Category {
    ACCIDENT(0),
    CLOSURE(1),
    ROADWORKS(2),
    CONGESTION(3),
    FLOW(4),
    UNDEFINED(5),
    OTHER(6);

    private int value;
    MapEvents$DisplayTrafficIncident$Category(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MapEvents$DisplayTrafficIncident$Category extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category ACCIDENT;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category CLOSURE;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category CONGESTION;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category FLOW;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category OTHER;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category ROADWORKS;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category UNDEFINED;
//    
//    static {
//        ACCIDENT = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category("ACCIDENT", 0);
//        CLOSURE = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category("CLOSURE", 1);
//        ROADWORKS = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category("ROADWORKS", 2);
//        CONGESTION = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category("CONGESTION", 3);
//        FLOW = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category("FLOW", 4);
//        UNDEFINED = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category("UNDEFINED", 5);
//        OTHER = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category("OTHER", 6);
//        com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category[] a = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category[7];
//        a[0] = ACCIDENT;
//        a[1] = CLOSURE;
//        a[2] = ROADWORKS;
//        a[3] = CONGESTION;
//        a[4] = FLOW;
//        a[5] = UNDEFINED;
//        a[6] = OTHER;
//        $VALUES = a;
//    }
//    
//    private MapEvents$DisplayTrafficIncident$Category(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category.class, s);
//    }
//    
//    public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Category[] values() {
//        return $VALUES.clone();
//    }
//}
//