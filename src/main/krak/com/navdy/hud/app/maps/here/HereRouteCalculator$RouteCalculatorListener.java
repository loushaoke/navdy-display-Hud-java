package com.navdy.hud.app.maps.here;

abstract public interface HereRouteCalculator$RouteCalculatorListener {
    abstract public void error(com.here.android.mpa.routing.RoutingError arg, Throwable arg0);
    
    
    abstract public void postSuccess(java.util.ArrayList arg);
    
    
    abstract public void preSuccess();
    
    
    abstract public void progress(int arg);
}
