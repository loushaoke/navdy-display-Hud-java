package com.navdy.hud.app.maps.here;

class HerePlacesManager$4$1 implements com.navdy.hud.app.maps.here.HerePlacesManager$PlacesSearchCallback {
    final com.navdy.hud.app.maps.here.HerePlacesManager$4 this$0;
    final com.here.android.mpa.common.GeoCoordinate val$geoPosition;
    final long val$l1;
    final int val$maxResults;
    final int val$searchArea;
    final String val$searchQuery;
    
    HerePlacesManager$4$1(com.navdy.hud.app.maps.here.HerePlacesManager$4 a, String s, com.here.android.mpa.common.GeoCoordinate a0, int i, int i0, long j) {
        super();
        this.this$0 = a;
        this.val$searchQuery = s;
        this.val$geoPosition = a0;
        this.val$maxResults = i;
        this.val$searchArea = i0;
        this.val$l1 = j;
    }
    
    public void result(com.here.android.mpa.search.ErrorCode a, com.here.android.mpa.search.DiscoveryResultPage a0) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HerePlacesManager$4$1$1(this, a, a0), 19);
    }
}
