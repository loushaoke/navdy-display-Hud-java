package com.navdy.hud.app.framework.phonecall;

class CallNotification$4 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    
    static {
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState = new int[com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState;
        com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a2 = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IDLE;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.MISSED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.ENDED.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.FAILED.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.REJECTED.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.CANCELLED.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IN_PROGRESS.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.RINGING.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.DIALING.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException10) {
        }
    }
}
