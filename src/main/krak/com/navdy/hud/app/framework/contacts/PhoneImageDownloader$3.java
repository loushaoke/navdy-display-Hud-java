package com.navdy.hud.app.framework.contacts;

class PhoneImageDownloader$3 implements Runnable {
    final com.navdy.hud.app.framework.contacts.PhoneImageDownloader this$0;
    final com.navdy.service.library.events.photo.PhotoResponse val$event;
    
    PhoneImageDownloader$3(com.navdy.hud.app.framework.contacts.PhoneImageDownloader a, com.navdy.service.library.events.photo.PhotoResponse a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$100().v("storing photo");
        label6: {
            java.io.FileOutputStream a = null;
            java.io.FileOutputStream a0 = null;
            Throwable a1 = null;
            label0: {
                label1: {
                    label5: {
                        byte[] a2 = null;
                        String s = null;
                        com.navdy.hud.app.profile.DriverProfile a3 = null;
                        java.io.File a4 = null;
                        String s0 = null;
                        label3: {
                            try {
                                a2 = this.val$event.photo.toByteArray();
                                android.graphics.Bitmap a5 = android.graphics.BitmapFactory.decodeByteArray(a2, 0, a2.length);
                                s = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$200(this.this$0, this.val$event.identifier, this.val$event.photoType);
                                label4: {
                                    if (a5 != null) {
                                        break label4;
                                    }
                                    com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$500(this.this$0).post(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus(this.val$event.identifier, this.val$event.photoType, false, false));
                                    a = null;
                                    a0 = null;
                                    break label5;
                                }
                                a3 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
                                boolean b = android.text.TextUtils.equals((CharSequence)a3.getProfileName(), (CharSequence)com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$300(this.this$0));
                                label2: {
                                    if (!b) {
                                        break label2;
                                    }
                                    a4 = new java.io.File(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$400(this.this$0, a3, this.val$event.photoType), s);
                                    a0 = new java.io.FileOutputStream(a4);
                                    break label3;
                                }
                                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$100().w(new StringBuilder().append("photo cannot save [").append(this.val$event.photoType).append("] id[").append(this.val$event.identifier).append("] profile changed from [").append(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$300(this.this$0)).append("] to [").append(a3.getProfileName()).append("]").toString());
                                a = null;
                                a0 = null;
                                break label5;
                            } catch(Throwable a6) {
                                a1 = a6;
                            }
                            a = null;
                            a0 = null;
                            break label0;
                        }
                        try {
                            a0.write(a2);
                            s0 = com.navdy.service.library.util.IOUtils.hashForBytes(a2);
                            a = new java.io.FileOutputStream(new java.io.File(com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$400(this.this$0, a3, this.val$event.photoType), new StringBuilder().append(s).append(".md5").toString()));
                        } catch(Throwable a7) {
                            a1 = a7;
                            break label1;
                        }
                        try {
                            a.write(s0.getBytes());
                            com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$100().v(new StringBuilder().append("photo saved[").append(this.val$event.photoType).append("] id[").append(this.val$event.identifier).append("]").toString());
                            com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().invalidate(a4);
                            com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$500(this.this$0).post(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus(this.val$event.identifier, this.val$event.photoType, true, false));
                        } catch(Throwable a8) {
                            a1 = a8;
                            break label0;
                        }
                    }
                    com.navdy.service.library.util.IOUtils.fileSync(a0);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                    com.navdy.service.library.util.IOUtils.fileSync(a);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                    com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$600(this.this$0);
                    break label6;
                }
                a = null;
            }
            try {
                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$100().e(a1);
                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$500(this.this$0).post(new com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus(this.val$event.identifier, this.val$event.photoType, false, false));
            } catch(Throwable a9) {
                com.navdy.service.library.util.IOUtils.fileSync(a0);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                com.navdy.service.library.util.IOUtils.fileSync(a);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$600(this.this$0);
                throw a9;
            }
            com.navdy.service.library.util.IOUtils.fileSync(a0);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            com.navdy.service.library.util.IOUtils.fileSync(a);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
            com.navdy.hud.app.framework.contacts.PhoneImageDownloader.access$600(this.this$0);
        }
    }
}
