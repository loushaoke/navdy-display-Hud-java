package com.navdy.hud.app.framework.glance;
import com.navdy.hud.app.R;

public class GlanceConstants {
    final public static String AT_SEPARATOR = "@";
    final public static int AUDIO_FEEDBACK_FADE_OUT = 300;
    final public static String CALENDAR_PREFIX = "glance_cal_";
    final public static String COLON_SEPARATOR = ":";
    final public static int DEFAULT_TIMEOUT = 25000;
    final public static int DELETE_ALL_GLANCE_TIMEOUT = 1000;
    final public static Object DELETE_ALL_VIEW_TAG;
    final public static String EMAIL_PREFIX = "glance_email_";
    final public static String EMPTY = "";
    final public static String FUEL_PREFIX = "glance_fuel_";
    final public static String GENERIC_PREFIX = "glance_gen_";
    final public static String GMAIL_SEPARATOR = "\u2022";
    final public static String INVALID_APPLE_MAIL_GLANCE_BODY = "this message has no content";
    final public static String MESSAGE_PREFIX = "glance_msg_";
    final public static String NEWLINE = "\n";
    final public static String PERIOD = ".";
    final public static int SCROLL_DISCARD_EVENTS_THRESHOLD = 750;
    final public static int SCROLL_PROGRESS_INDICATOR_ITEM_COUNT = 100;
    final public static int SECONDS_IN_MINUTE = 60000;
    final public static String SOCIAL_PREFIX = "glance_soc_";
    final public static String SPACE = " ";
    final public static int TAG_ADD_FUEL = 7;
    final public static int TAG_BACK = 5;
    final public static int TAG_CALL = 3;
    final public static int TAG_DISMISS = 6;
    final public static int TAG_DISMISS_ADD_FUEL = 8;
    final public static int TAG_QUICK = 4;
    final public static int TAG_READ = 2;
    final public static int TAG_REPLY = 1;
    final public static int TIME_UPDATE_INTERVAL = 30000;
    final public static String am;
    final public static String amMarker;
    final public static String back;
    public static java.util.List backChoice;
    public static java.util.List backChoice2;
    final public static int calendarNormalMargin;
    final public static int calendarNowMargin;
    public static java.util.List calendarOptions;
    final public static int calendarTimeMargin;
    final public static int calendarpmMarker;
    final public static String call;
    final public static int choiceItemDefaultPadding;
    final public static int choiceItemLargePadding;
    final public static int choiceItemSmallPadding;
    final public static int choiceTextLarge;
    final public static int choiceTextSmall;
    final public static int colorAppleCalendar;
    final public static int colorAppleMail;
    final public static int colorDeleteAll;
    final public static int colorFacebook;
    final public static int colorFacebookMessenger;
    final public static int colorFasterRoute;
    final public static int colorFuelLevel;
    final public static int colorGeneric;
    final public static int colorGoogleCalendar;
    final public static int colorGoogleHangout;
    final public static int colorGoogleInbox;
    final public static int colorGoogleMail;
    final public static int colorIMessage;
    final public static int colorPhoneCall;
    final public static int colorSlack;
    final public static int colorSms;
    final public static int colorTrafficDelay;
    final public static int colorTwitter;
    final public static int colorWhatsapp;
    final public static int colorWhite;
    final public static String dismiss;
    final public static String done;
    final public static String facebook;
    public static java.util.List fuelChoices;
    public static java.util.List fuelChoicesNoRoute;
    final public static String fuelNotificationSubtitle;
    final public static String fuelNotificationTitle;
    final public static String glancesCannotDelete;
    final public static String glancesDismissAll;
    final public static String hour;
    final public static String message;
    final public static int messageHeightBound;
    private static int messageTitleHeight;
    final public static int messageTitleMarginBottom;
    final public static int messageTitleMarginTop;
    final public static int messageWidthBound;
    final public static String minute;
    public static java.util.List noMessage;
    public static java.util.List noNumberandNoReplyBack;
    final public static String notification;
    final public static String nowStr;
    public static java.util.List numberAndNoReplyBack;
    public static java.util.List numberAndReplyBack_1;
    final public static String pm;
    final public static String pmMarker;
    final public static String questionMark;
    final public static String quick;
    final public static String read;
    final public static String reply;
    final private static java.util.ArrayList replyBackMessages;
    final public static String replySent;
    final public static int scrollingIndicatorCircleFocusSize;
    final public static int scrollingIndicatorCircleSize;
    final public static int scrollingIndicatorHeight;
    final public static int scrollingIndicatorLeftPadding;
    final public static int scrollingIndicatorPadding;
    final public static int scrollingIndicatorParentHeight;
    final public static int scrollingIndicatorParentWidth;
    final public static int scrollingIndicatorProgressSize;
    final public static int scrollingIndicatorSize;
    final public static int scrollingIndicatorWidth;
    final public static int scrollingRoundSize;
    final public static String senderUnknown;
    final public static String starts;
    final public static String view;
    
    static {
        DELETE_ALL_VIEW_TAG = new Object();
        numberAndReplyBack_1 = (java.util.List)new java.util.ArrayList(3);
        numberAndNoReplyBack = (java.util.List)new java.util.ArrayList(3);
        noNumberandNoReplyBack = (java.util.List)new java.util.ArrayList(2);
        calendarOptions = (java.util.List)new java.util.ArrayList(2);
        noMessage = (java.util.List)new java.util.ArrayList(1);
        backChoice = (java.util.List)new java.util.ArrayList(1);
        backChoice2 = (java.util.List)new java.util.ArrayList(1);
        fuelChoices = (java.util.List)new java.util.ArrayList(3);
        fuelChoicesNoRoute = (java.util.List)new java.util.ArrayList(2);
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        reply = a.getString(R.string.reply);
        read = a.getString(R.string.read);
        call = a.getString(R.string.call);
        quick = a.getString(R.string.quick);
        back = a.getString(R.string.back);
        dismiss = a.getString(R.string.dismiss);
        done = a.getString(R.string.done);
        view = a.getString(R.string.view);
        senderUnknown = a.getString(R.string.caller_unknown);
        replySent = a.getString(R.string.reply_sent);
        notification = a.getString(R.string.notification);
        facebook = a.getString(R.string.facebook);
        nowStr = a.getString(R.string.now);
        glancesCannotDelete = a.getString(R.string.glances_cannot_delete);
        glancesDismissAll = a.getString(R.string.glances_dismiss);
        starts = a.getString(R.string.starts);
        hour = a.getString(R.string.hr);
        minute = a.getString(R.string.min);
        message = a.getString(R.string.message);
        fuelNotificationTitle = a.getString(R.string.fuel_low);
        fuelNotificationSubtitle = a.getString(R.string.add_to_trip);
        questionMark = a.getString(R.string.question_mark);
        am = a.getString(R.string.am).toUpperCase();
        pm = a.getString(R.string.pm).toUpperCase();
        amMarker = a.getString(R.string.am_marker);
        pmMarker = a.getString(R.string.pm_marker);
        colorGoogleCalendar = a.getColor(R.color.google_calendar);
        colorGoogleMail = a.getColor(R.color.google_mail);
        colorGoogleHangout = a.getColor(R.color.google_hangout);
        colorSlack = a.getColor(R.color.slack);
        colorWhatsapp = a.getColor(R.color.whatsapp);
        colorFacebookMessenger = a.getColor(R.color.facebook_messenger);
        colorFacebook = a.getColor(R.color.facebook);
        colorTwitter = a.getColor(R.color.twitter);
        colorIMessage = a.getColor(R.color.imessage);
        colorAppleMail = a.getColor(R.color.apple_mail);
        colorAppleCalendar = a.getColor(R.color.apple_calendar);
        colorSms = a.getColor(R.color.sms);
        colorGeneric = a.getColor(R.color.generic);
        colorDeleteAll = a.getColor(R.color.delete_all);
        colorPhoneCall = a.getColor(R.color.phone_call);
        colorFasterRoute = a.getColor(R.color.faster_route);
        colorFuelLevel = a.getColor(R.color.fuel_level);
        colorTrafficDelay = a.getColor(R.color.traffic_delay);
        colorWhite = a.getColor(17170443);
        colorGoogleInbox = a.getColor(R.color.google_inbox);
        int i = a.getColor(R.color.glance_back);
        int i0 = a.getColor(R.color.glance_dismiss);
        int i1 = a.getColor(R.color.glance_ok_blue);
        int i2 = a.getColor(R.color.glance_ok_blue);
        int i3 = a.getColor(R.color.call_dial);
        a.getColor(R.color.glance_msg);
        int i4 = a.getColor(R.color.glance_ok_blue);
        numberAndReplyBack_1.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(1, R.drawable.icon_glances_reply, i2, R.drawable.icon_glances_reply, -16777216, reply, i2));
        numberAndReplyBack_1.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(2, R.drawable.icon_glances_read, i1, R.drawable.icon_glances_read, -16777216, read, i1));
        numberAndReplyBack_1.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(6, R.drawable.icon_glances_dismiss, i0, R.drawable.icon_glances_dismiss, -16777216, dismiss, i0));
        numberAndNoReplyBack.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(3, R.drawable.icon_call, i3, R.drawable.icon_call, -16777216, call, i3));
        numberAndNoReplyBack.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(2, R.drawable.icon_glances_read, i1, R.drawable.icon_glances_read, -16777216, read, i1));
        numberAndNoReplyBack.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(6, R.drawable.icon_glances_dismiss, i0, R.drawable.icon_glances_dismiss, -16777216, dismiss, i0));
        noNumberandNoReplyBack.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(2, R.drawable.icon_glances_read, i1, R.drawable.icon_glances_read, -16777216, read, i1));
        noNumberandNoReplyBack.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(6, R.drawable.icon_glances_dismiss, i0, R.drawable.icon_glances_dismiss, -16777216, dismiss, i0));
        calendarOptions.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(2, R.drawable.icon_glances_read, i1, R.drawable.icon_glances_read, -16777216, read, i1));
        calendarOptions.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(6, R.drawable.icon_glances_dismiss, i0, R.drawable.icon_glances_dismiss, -16777216, dismiss, i0));
        noMessage.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(6, R.drawable.icon_glances_dismiss, i0, R.drawable.icon_glances_dismiss, -16777216, dismiss, i0));
        backChoice.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a.getString(R.string.back), 5));
        backChoice2.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(5, R.drawable.icon_glances_back, i, R.drawable.icon_glances_back, i, a.getString(R.string.back), i));
        fuelChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(7, R.drawable.icon_glances_add, i4, R.drawable.icon_glances_add, -16777216, a.getString(R.string.add_fuel_stop), i4));
        fuelChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(2, R.drawable.icon_glances_read, i1, R.drawable.icon_glances_read, -16777216, read, i1));
        fuelChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(8, R.drawable.icon_glances_dismiss, i0, R.drawable.icon_glances_dismiss, -16777216, dismiss, i0));
        fuelChoicesNoRoute.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(2, R.drawable.icon_glances_read, i1, R.drawable.icon_glances_read, -16777216, read, i1));
        fuelChoicesNoRoute.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(8, R.drawable.icon_glances_dismiss, i0, R.drawable.icon_glances_dismiss, -16777216, dismiss, i0));
        choiceTextLarge = (int)a.getDimension(R.dimen.message_options_text_large);
        choiceTextSmall = (int)a.getDimension(R.dimen.message_options_text_small);
        choiceItemLargePadding = (int)a.getDimension(R.dimen.message_options_item_padding_large);
        choiceItemDefaultPadding = (int)a.getDimension(R.dimen.message_options_item_padding_default);
        choiceItemSmallPadding = (int)a.getDimension(R.dimen.message_options_item_padding_small);
        calendarNormalMargin = (int)a.getDimension(R.dimen.glance_calendar_normal_margin);
        calendarNowMargin = (int)a.getDimension(R.dimen.glance_calendar_now_margin);
        calendarTimeMargin = (int)a.getDimension(R.dimen.glance_calendar_time_margin);
        calendarpmMarker = (int)a.getDimension(R.dimen.glance_calendar_pm_marker);
        messageWidthBound = (int)a.getDimension(R.dimen.glance_message_width_bound);
        messageHeightBound = (int)a.getDimension(R.dimen.glance_message_height_bound);
        messageTitleMarginTop = (int)a.getDimension(R.dimen.glance_message_title_top);
        messageTitleMarginBottom = (int)a.getDimension(R.dimen.glance_message_title_bottom);
        scrollingIndicatorSize = (int)a.getDimension(R.dimen.glance_scrolling_indicator_size);
        scrollingRoundSize = (int)a.getDimension(R.dimen.glance_scrolling_round_size);
        scrollingIndicatorWidth = (int)a.getDimension(R.dimen.glance_scrolling_indicator_width);
        scrollingIndicatorHeight = (int)a.getDimension(R.dimen.glance_scrolling_indicator_height);
        scrollingIndicatorParentWidth = (int)a.getDimension(R.dimen.glance_scrolling_indicator_parent_width);
        scrollingIndicatorParentHeight = (int)a.getDimension(R.dimen.glance_scrolling_indicator_parent_height);
        scrollingIndicatorProgressSize = (int)a.getDimension(R.dimen.glance_scrolling_indicator_size);
        scrollingIndicatorCircleSize = (int)a.getDimension(R.dimen.glance_scrolling_indicator_circle_size);
        scrollingIndicatorCircleFocusSize = (int)a.getDimension(R.dimen.glance_scrolling_indicator_focus_circle_size);
        scrollingIndicatorPadding = (int)a.getDimension(R.dimen.glance_scrolling_indicator_padding);
        scrollingIndicatorLeftPadding = (int)a.getDimension(R.dimen.glance_scrolling_indicator_left_padding);
        replyBackMessages = new java.util.ArrayList();
        replyBackMessages.add(a.getString(R.string.reply_text_msg_1));
        replyBackMessages.add(a.getString(R.string.reply_text_msg_2));
        replyBackMessages.add(a.getString(R.string.reply_text_msg_3));
        replyBackMessages.add(a.getString(R.string.reply_text_msg_4));
        replyBackMessages.add(a.getString(R.string.reply_text_msg_5));
        replyBackMessages.add(a.getString(R.string.reply_text_msg_6));
    }
    
    public GlanceConstants() {
    }
    
    public static boolean areMessageCanned() {
        boolean b = false;
        com.navdy.service.library.events.glances.CannedMessagesUpdate a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getCannedMessages();
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.cannedMessage == null) {
                        break label1;
                    }
                    if (a.cannedMessage.size() > 0) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static java.util.List getCannedMessages() {
        Object a = null;
        com.navdy.service.library.events.glances.CannedMessagesUpdate a0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getCannedMessages();
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.cannedMessage == null) {
                        break label1;
                    }
                    if (a0.cannedMessage.size() > 0) {
                        break label0;
                    }
                }
                a = replyBackMessages;
                break label2;
            }
            a = a0.cannedMessage;
        }
        return (java.util.List)a;
    }
    
    public static int getMessageTitleHeight() {
        return messageTitleHeight;
    }
    
    public static void setMessageTitleHeight(int i) {
        messageTitleHeight = i;
    }
}
