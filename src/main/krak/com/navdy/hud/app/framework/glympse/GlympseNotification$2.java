package com.navdy.hud.app.framework.glympse;

class GlympseNotification$2 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$glympse$GlympseNotification$Type;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    
    static {
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a0 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$framework$glympse$GlympseNotification$Type = new int[com.navdy.hud.app.framework.glympse.GlympseNotification$Type.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$framework$glympse$GlympseNotification$Type;
        com.navdy.hud.app.framework.glympse.GlympseNotification$Type a2 = com.navdy.hud.app.framework.glympse.GlympseNotification$Type.SENT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glympse$GlympseNotification$Type[com.navdy.hud.app.framework.glympse.GlympseNotification$Type.READ.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glympse$GlympseNotification$Type[com.navdy.hud.app.framework.glympse.GlympseNotification$Type.OFFLINE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException4) {
        }
    }
}
