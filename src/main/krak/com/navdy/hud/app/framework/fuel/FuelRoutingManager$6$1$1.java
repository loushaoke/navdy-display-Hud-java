package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$6$1$1 implements Runnable {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$6$1 this$2;
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$navigationRouteRequest;
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem val$optimal;
    final java.util.ArrayList val$outgoingResults;
    
    FuelRoutingManager$6$1$1(com.navdy.hud.app.framework.fuel.FuelRoutingManager$6$1 a, com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem a0, java.util.ArrayList a1, com.navdy.service.library.events.navigation.NavigationRouteRequest a2) {
        super();
        this.this$2 = a;
        this.val$optimal = a0;
        this.val$outgoingResults = a1;
        this.val$navigationRouteRequest = a2;
    }
    
    public void run() {
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$2202(this.this$2.this$1.this$0, this.val$optimal.gasStation);
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$602(this.this$2.this$1.this$0, false);
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$202(this.this$2.this$1.this$0, this.val$outgoingResults);
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$502(this.this$2.this$1.this$0, this.val$navigationRouteRequest);
        double d = (double)Math.round(this.val$optimal.gasStation.getLocation().getCoordinate().distanceTo(com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$2300(this.this$2.this$1.this$0).getLocationFixManager().getLastGeoCoordinate()) * 10.0 / 1609.34) / 10.0;
        String s = this.val$optimal.gasStation.getLocation().getAddress().getText().split("<br/>")[0];
        java.util.ArrayList a = new java.util.ArrayList(1);
        if (this.this$2.this$1.val$fuelLevel != -1) {
            ((java.util.List)a).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.FUEL_LEVEL.name(), String.valueOf(com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$800(this.this$2.this$1.this$0).getFuelLevel())));
        }
        ((java.util.List)a).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_NAME.name(), this.val$optimal.gasStation.getName()));
        ((java.util.List)a).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_ADDRESS.name(), s));
        ((java.util.List)a).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.FuelConstants.GAS_STATION_DISTANCE.name(), String.valueOf(d)));
        com.navdy.service.library.events.glances.GlanceEvent a0 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().glanceType(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_FUEL).id("low#fuel#level").postTime(Long.valueOf(System.currentTimeMillis())).provider("com.navdy.fuel").glanceData((java.util.List)a).build();
        com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$2400(this.this$2.this$1.this$0).post(a0);
        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().isLoggable(2)) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$000().v("state is now LOW_FUEL, posting low fuel glance");
        }
    }
}
