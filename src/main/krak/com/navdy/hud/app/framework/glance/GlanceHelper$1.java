package com.navdy.hud.app.framework.glance;

class GlanceHelper$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
    final static int[] $SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType;
    
    static {
        $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp = new int[com.navdy.hud.app.framework.glance.GlanceApp.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp;
        com.navdy.hud.app.framework.glance.GlanceApp a0 = com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_HANGOUT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.SLACK.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.WHATS_APP.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK_MESSENGER.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MESSAGE.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FACEBOOK.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.TWITTER.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_SOCIAL.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.IMESSAGE.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_CALENDAR.ordinal()] = 10;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.APPLE_CALENDAR.ordinal()] = 11;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_CALENDAR.ordinal()] = 12;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_MAIL.ordinal()] = 13;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.APPLE_MAIL.ordinal()] = 14;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC_MAIL.ordinal()] = 15;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GOOGLE_INBOX.ordinal()] = 16;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.SMS.ordinal()] = 17;
        } catch(NoSuchFieldError ignoredException15) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.FUEL.ordinal()] = 18;
        } catch(NoSuchFieldError ignoredException16) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[com.navdy.hud.app.framework.glance.GlanceApp.GENERIC.ordinal()] = 19;
        } catch(NoSuchFieldError ignoredException17) {
        }
        $SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType = new int[com.navdy.service.library.events.glances.GlanceEvent$GlanceType.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType;
        com.navdy.service.library.events.glances.GlanceEvent$GlanceType a2 = com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_EMAIL;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException18) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType[com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_CALENDAR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException19) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType[com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_MESSAGE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException20) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType[com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_SOCIAL.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException21) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$glances$GlanceEvent$GlanceType[com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_FUEL.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException22) {
        }
    }
}
