package com.navdy.hud.app.framework.connection;

final class ConnectionNotification$2 implements com.navdy.hud.app.framework.toast.IToastCallback {
    com.navdy.hud.app.ui.component.image.InitialsImageView driverImage;
    final java.io.File val$imagePath;
    final com.navdy.hud.app.profile.DriverProfile val$profile;
    
    ConnectionNotification$2(java.io.File a, com.navdy.hud.app.profile.DriverProfile a0) {
        super();
        this.val$imagePath = a;
        this.val$profile = a0;
    }
    
    public void executeChoiceItem(int i, int i0) {
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public void onStart(com.navdy.hud.app.view.ToastView a) {
        this.driverImage = a.getMainLayout().screenImage;
        this.driverImage.setTag("connection#toast");
        com.navdy.hud.app.framework.connection.ConnectionNotification.access$000(this.driverImage, "connection#toast", this.val$imagePath, this.val$profile);
    }
    
    public void onStop() {
        this.driverImage.setTag(null);
        this.driverImage = null;
    }
}
