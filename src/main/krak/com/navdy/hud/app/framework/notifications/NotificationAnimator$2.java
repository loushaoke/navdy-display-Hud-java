package com.navdy.hud.app.framework.notifications;

final class NotificationAnimator$2 implements Runnable {
    final android.view.ViewGroup val$parent;
    final android.view.View val$view;
    
    NotificationAnimator$2(android.view.ViewGroup a, android.view.View a0) {
        super();
        this.val$parent = a;
        this.val$view = a0;
    }
    
    public void run() {
        this.val$parent.removeView(this.val$view);
    }
}
