package com.navdy.hud.app.framework.notifications;

class NotificationManager$3 implements java.util.Comparator {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    
    NotificationManager$3(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        super();
        this.this$0 = a;
    }
    
    public int compare(com.navdy.hud.app.framework.notifications.NotificationManager$Info a, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0) {
        int i = a.priority - a0.priority;
        if (i == 0) {
            i = (int)(a0.uniqueCounter - a.uniqueCounter);
        }
        return i;
    }
    
    public int compare(Object a, Object a0) {
        return this.compare((com.navdy.hud.app.framework.notifications.NotificationManager$Info)a, (com.navdy.hud.app.framework.notifications.NotificationManager$Info)a0);
    }
}
