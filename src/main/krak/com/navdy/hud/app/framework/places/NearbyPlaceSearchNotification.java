package com.navdy.hud.app.framework.places;
import com.navdy.hud.app.R;

public class NearbyPlaceSearchNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.ui.component.ChoiceLayout2$IListener {
    final private static float ICON_SCALE = 1.38f;
    final private static int N_OFFLINE_RESULTS = 7;
    final private static long TIMEOUT = 10000L;
    final private static java.util.Map choicesMapping;
    final private static com.navdy.service.library.log.Logger logger;
    final private static String searchAgain;
    final private static int searchColor;
    final private static int secondaryColor;
    final private com.squareup.otto.Bus bus;
    @InjectView(R.id.choiceLayout)
    com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState currentState;
    final private android.os.Handler handler;
    @InjectView(R.id.iconColorView)
    com.navdy.hud.app.ui.component.image.IconColorImageView iconColorView;
    @InjectView(R.id.iconSide)
    com.navdy.hud.app.ui.component.image.IconColorImageView iconSide;
    private android.animation.ObjectAnimator loadingAnimator;
    final private com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
    final private com.navdy.service.library.events.places.PlaceType placeType;
    final private String requestId;
    @InjectView(R.id.resultsCount)
    android.widget.TextView resultsCount;
    @InjectView(R.id.resultsLabel)
    android.widget.TextView resultsLabel;
    private java.util.List returnedDestinations;
    final private com.navdy.hud.app.manager.SpeedManager speedManager;
    @InjectView(R.id.iconSpinner)
    android.widget.ImageView statusBadge;
    @InjectView(R.id.subTitle)
    android.widget.TextView subTitle;
    final private Runnable timeoutForFailure;
    @InjectView(R.id.title)
    android.widget.TextView title;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification.class);
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        String s = a.getString(R.string.dismiss);
        int i = a.getColor(R.color.glance_dismiss);
        int i0 = a.getColor(R.color.glance_ok_blue);
        com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a0 = new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, s, i);
        com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a1 = new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.retry, R.drawable.icon_glances_retry, i0, R.drawable.icon_glances_retry, -16777216, a.getString(R.string.retry), i0);
        java.util.ArrayList a2 = new java.util.ArrayList();
        ((java.util.List)a2).add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, a.getString(R.string.cancel), i));
        java.util.ArrayList a3 = new java.util.ArrayList();
        ((java.util.List)a3).add(a1);
        ((java.util.List)a3).add(a0);
        java.util.ArrayList a4 = new java.util.ArrayList();
        ((java.util.List)a4).add(a1);
        ((java.util.List)a4).add(a0);
        int i1 = a.getColor(R.color.glance_ok_blue);
        java.util.ArrayList a5 = new java.util.ArrayList();
        ((java.util.List)a5).add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(R.id.dismiss, R.drawable.icon_glances_read, i1, R.drawable.icon_glances_read, -16777216, a.getString(R.string.view), i1));
        ((java.util.List)a5).add(a0);
        secondaryColor = a.getColor(R.color.place_type_search_secondary_color);
        choicesMapping = (java.util.Map)new java.util.HashMap(4);
        choicesMapping.put(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.SEARCHING, a2);
        choicesMapping.put(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.ERROR, a3);
        choicesMapping.put(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.NO_RESULTS, a4);
        choicesMapping.put(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.SEARCH_COMPLETE, a5);
        searchColor = a.getColor(R.color.mm_search);
        searchAgain = a.getString(R.string.search_again);
    }
    
    public NearbyPlaceSearchNotification(com.navdy.service.library.events.places.PlaceType a) {
        this.timeoutForFailure = (Runnable)new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$1(this);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.requestId = java.util.UUID.randomUUID().toString();
        this.placeType = a;
    }
    
    static com.navdy.hud.app.framework.notifications.INotificationController access$000(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        return a.controller;
    }
    
    static void access$100(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        a.stopLoadingAnimation();
    }
    
    static android.animation.ObjectAnimator access$1000(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        return a.loadingAnimator;
    }
    
    static void access$200(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        a.goToFailedState();
    }
    
    static com.navdy.service.library.events.places.PlaceType access$300(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        return a.placeType;
    }
    
    static com.navdy.service.library.log.Logger access$400() {
        return logger;
    }
    
    static com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState access$500(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        return a.currentState;
    }
    
    static Runnable access$600(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        return a.timeoutForFailure;
    }
    
    static android.os.Handler access$700(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        return a.handler;
    }
    
    static void access$800(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a, java.util.List a0) {
        a.goToSuccessfulState(a0);
    }
    
    static void access$900(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        a.goToZeroResultsState();
    }
    
    private void dismissNotification() {
        this.notificationManager.removeNotification("navdy#place#type#search#notif");
    }
    
    private com.here.android.mpa.search.CategoryFilter getHereCategoryFilter(com.navdy.service.library.events.places.PlaceType a) {
        com.here.android.mpa.search.CategoryFilter a0 = new com.here.android.mpa.search.CategoryFilter();
        switch(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$5.$SwitchMap$com$navdy$service$library$events$places$PlaceType[a.ordinal()]) {
            case 7: {
                a0.add("hospital-health-care-facility");
                break;
            }
            case 6: {
                a0.add("atm-bank-exchange");
                break;
            }
            case 5: {
                a0.add("coffee-tea");
                break;
            }
            case 4: {
                a0.add("shopping");
                break;
            }
            case 3: {
                String[] a1 = com.navdy.hud.app.maps.here.HerePlacesManager.HERE_PLACE_TYPE_RESTAURANT;
                int i = a1.length;
                int i0 = 0;
                while(i0 < i) {
                    a0.add(a1[i0]);
                    i0 = i0 + 1;
                }
                break;
            }
            case 2: {
                a0.add("parking-facility");
                break;
            }
            case 1: {
                a0.add("petrol-station");
                break;
            }
        }
        return a0;
    }
    
    private void goToFailedState() {
        this.iconColorView.setIcon(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType).iconRes, secondaryColor, (android.graphics.Shader)null, 1.38f);
        this.subTitle.setText(R.string.place_type_search_failed);
        this.statusBadge.setImageResource(R.drawable.icon_badge_alert);
        this.statusBadge.setVisibility(0);
        this.iconSide.setVisibility(8);
        this.currentState = com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.ERROR;
        this.choiceLayout.setChoices((java.util.List)choicesMapping.get(this.currentState), 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
    }
    
    private void goToSuccessfulState(java.util.List a) {
        android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.iconColorView.setIcon(0, secondaryColor, (android.graphics.Shader)null, 1.38f);
        this.resultsCount.setVisibility(0);
        this.resultsCount.setText((CharSequence)String.valueOf(a.size()));
        this.resultsLabel.setVisibility(0);
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder a1 = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        int i = a1.iconRes;
        int i0 = a0.getColor(a1.colorRes);
        if (i != 0) {
            this.statusBadge.setVisibility(8);
            this.iconSide.setIcon(i, i0, (android.graphics.Shader)null, 0.5f);
            this.iconSide.setVisibility(0);
        }
        this.currentState = com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.SEARCH_COMPLETE;
        this.choiceLayout.setChoices((java.util.List)choicesMapping.get(this.currentState), 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
        this.returnedDestinations = a;
    }
    
    private void goToZeroResultsState() {
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.iconColorView.setIcon(0, secondaryColor, (android.graphics.Shader)null, 1.38f);
        this.resultsCount.setVisibility(0);
        this.resultsCount.setText((CharSequence)a.getString(R.string.zero));
        this.resultsLabel.setVisibility(0);
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder a0 = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        int i = a0.iconRes;
        int i0 = a.getColor(a0.colorRes);
        if (i != 0) {
            this.statusBadge.setVisibility(8);
            this.iconSide.setIcon(i, i0, (android.graphics.Shader)null, 0.5f);
            this.iconSide.setVisibility(0);
        }
        this.currentState = com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.NO_RESULTS;
        this.choiceLayout.setChoices((java.util.List)choicesMapping.get(this.currentState), 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
    }
    
    private void launchPicker() {
        logger.v("launch picker screen");
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.res.Resources a0 = a.getResources();
        android.os.Bundle a1 = new android.os.Bundle();
        a1.putBoolean("PICKER_SHOW_DESTINATION_MAP", true);
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder a2 = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        a1.putInt("PICKER_DESTINATION_ICON", (a2 == null) ? -1 : a2.destinationIcon);
        a1.putString("PICKER_LEFT_TITLE", a0.getString(R.string.quick_search));
        a1.putInt("PICKER_LEFT_ICON", R.drawable.icon_mm_search_2);
        a1.putInt("PICKER_LEFT_ICON_BKCOLOR", android.support.v4.content.ContextCompat.getColor(a, R.color.mm_search));
        a1.putInt("PICKER_INITIAL_SELECTION", 1);
        int i = android.support.v4.content.ContextCompat.getColor(a, a2.colorRes);
        int i0 = android.support.v4.content.ContextCompat.getColor(a, R.color.icon_bk_color_unselected);
        com.navdy.hud.app.ui.component.destination.DestinationParcelable a3 = new com.navdy.hud.app.ui.component.destination.DestinationParcelable(R.id.search_again, a.getString(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType).titleRes), searchAgain, false, (String)null, true, (String)null, 0.0, 0.0, 0.0, 0.0, R.drawable.icon_mm_search_2, 0, searchColor, i0, com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType.NONE, (com.navdy.service.library.events.places.PlaceType)null);
        java.util.List a4 = com.navdy.hud.app.maps.util.DestinationUtil.convert(a, this.returnedDestinations, i, i0, false);
        a4.add(0, a3);
        com.navdy.hud.app.ui.component.destination.DestinationParcelable[] a5 = new com.navdy.hud.app.ui.component.destination.DestinationParcelable[a4.size()];
        a4.toArray((Object[])a5);
        a1.putParcelableArray("PICKER_DESTINATIONS", (android.os.Parcelable[])a5);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#place#type#search#notif", true, com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER, a1, new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$2(this));
    }
    
    private void performOfflineSearch() {
        com.navdy.hud.app.maps.here.HerePlacesManager.handleCategoriesRequest(this.getHereCategoryFilter(this.placeType), 7, (com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener)new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$3(this), true);
    }
    
    private void recordSelection(int i, int i0) {
        switch(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$5.$SwitchMap$com$navdy$hud$app$framework$places$NearbyPlaceSearchNotification$PlaceTypeSearchState[this.currentState.ordinal()]) {
            case 4: {
                if (i0 != R.id.dismiss) {
                    break;
                }
                if (i != 0) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchResultsDismiss();
                    break;
                } else {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchResultsView();
                    break;
                }
            }
            case 3: {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchNoResult(i0 == R.id.retry);
                break;
            }
            case 1: {
                if (i0 != R.id.dismiss) {
                    break;
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchDismiss();
                break;
            }
        }
    }
    
    private void sendRequest() {
        this.currentState = com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.SEARCHING;
        com.navdy.service.library.events.places.PlaceTypeSearchRequest a = new com.navdy.service.library.events.places.PlaceTypeSearchRequest$Builder().request_id(this.requestId).place_type(this.placeType).build();
        boolean b = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isAppConnected();
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!com.navdy.hud.app.framework.network.NetworkStateManager.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
                    break label0;
                }
                logger.v("performing online quick search");
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a));
                break label1;
            }
            logger.v("performing offline quick search");
            this.performOfflineSearch();
        }
        this.handler.postDelayed(this.timeoutForFailure, 10000L);
    }
    
    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            this.statusBadge.setImageResource(R.drawable.loader_circle);
            this.statusBadge.setVisibility(0);
            this.iconSide.setVisibility(8);
            android.widget.ImageView a = this.statusBadge;
            android.util.Property a0 = android.view.View.ROTATION;
            float[] a1 = new float[1];
            a1[0] = 360f;
            this.loadingAnimator = android.animation.ObjectAnimator.ofFloat(a, a0, a1);
            this.loadingAnimator.setDuration(500L);
            this.loadingAnimator.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$4(this));
        }
        if (!this.loadingAnimator.isRunning()) {
            logger.v("started loading animation");
            this.loadingAnimator.start();
        }
    }
    
    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            logger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.statusBadge.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
        this.recordSelection(a.pos, a.id);
        switch(a.id) {
            case R.id.retry: {
                this.startLoadingAnimation();
                this.sendRequest();
                break;
            }
            case R.id.dismiss: {
                if (this.currentState != com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.SEARCH_COMPLETE) {
                    this.dismissNotification();
                    break;
                } else if (a.pos == 0) {
                    this.launchPicker();
                    break;
                } else {
                    this.dismissNotification();
                    break;
                }
            }
        }
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return 0;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#place#type#search#notif";
    }
    
    public int getTimeout() {
        return 0;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.PLACE_TYPE_SEARCH;
    }
    
    public android.view.View getView(android.content.Context a) {
        android.view.View a0 = android.view.LayoutInflater.from(a).inflate(R.layout.notification_place_type_search, (android.view.ViewGroup)null);
        butterknife.ButterKnife.inject(this, a0);
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder a1 = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        this.title.setText((CharSequence)a.getString(a1.titleRes));
        this.iconColorView.setIcon(a1.iconRes, a.getResources().getColor(a1.colorRes), (android.graphics.Shader)null, 1.38f);
        this.choiceLayout.setVisibility(0);
        this.choiceLayout.setChoices((java.util.List)choicesMapping.get(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.SEARCHING), 0, (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)this);
        return a0;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return false;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a) {
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        if (this.controller != null) {
            if (this.currentState != com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.SEARCH_COMPLETE) {
                b = false;
            } else if (com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$5.$SwitchMap$com$navdy$service$library$events$input$Gesture[a.gesture.ordinal()] != 0) {
                logger.v("show route picker:gesture");
                this.launchPicker();
                b = true;
            } else {
                b = false;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.controller != null) {
            switch(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$5.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.choiceLayout.executeSelectedItem();
                    b = true;
                    break;
                }
                case 2: {
                    this.choiceLayout.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.choiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onPlaceTypeSearchResponse(com.navdy.service.library.events.places.PlaceTypeSearchResponse a) {
        logger.v(new StringBuilder().append("PlaceTypeSearchResponse:").append(a.request_status).toString());
        this.handler.removeCallbacks(this.timeoutForFailure);
        this.stopLoadingAnimation();
        if (android.text.TextUtils.equals((CharSequence)a.request_id, (CharSequence)this.requestId)) {
            if (this.currentState != com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$PlaceTypeSearchState.ERROR) {
                switch(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification$5.$SwitchMap$com$navdy$service$library$events$RequestStatus[a.request_status.ordinal()]) {
                    case 3: {
                        this.goToSuccessfulState(a.destinations);
                        break;
                    }
                    case 2: {
                        this.goToZeroResultsState();
                        break;
                    }
                    case 1: {
                        this.goToFailedState();
                        break;
                    }
                }
            } else {
                logger.w("received response after place type search has already failed, no-op");
            }
        } else {
            logger.w("received wrong request_id on the PlaceTypeSearchResponse, no-op");
        }
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        this.controller = a;
        this.bus.register(this);
        this.startLoadingAnimation();
        this.sendRequest();
    }
    
    public void onStop() {
        this.controller = null;
        this.bus.unregister(this);
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
    }
    
    public boolean supportScroll() {
        return false;
    }
}
