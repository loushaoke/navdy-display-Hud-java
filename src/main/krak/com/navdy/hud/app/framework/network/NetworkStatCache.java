package com.navdy.hud.app.framework.network;

public class NetworkStatCache {
    private com.navdy.hud.app.framework.network.DnsCache dnsCache;
    private java.util.HashMap networkStatMap;
    private java.util.HashMap networkStatMapBoot;
    
    NetworkStatCache(com.navdy.hud.app.framework.network.DnsCache a) {
        this.networkStatMap = new java.util.HashMap();
        this.networkStatMapBoot = new java.util.HashMap();
        this.dnsCache = a;
    }
    
    private void dumpBootStat(com.navdy.service.library.log.Logger a) {
        if (this.networkStatMapBoot.size() != 0) {
            java.util.Iterator a0 = this.networkStatMapBoot.values().iterator();
            a.v(new StringBuilder().append("dump-boot total endpoints:").append(this.networkStatMapBoot.size()).toString());
            Object a1 = a0;
            while(((java.util.Iterator)a1).hasNext()) {
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a2 = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo)((java.util.Iterator)a1).next();
                String s = this.dnsCache.getHostnamefromIP(a2.destIP);
                if (s == null) {
                    s = a2.destIP;
                }
                a.v(new StringBuilder().append("dump-boot  dest[").append(s).append("] tx[").append(a2.txBytes).append("] rx[").append(a2.rxBytes).append("]").toString());
            }
        }
    }
    
    private void dumpSessionStat(com.navdy.service.library.log.Logger a, boolean b) {
        if (this.networkStatMap.size() != 0) {
            java.util.Iterator a0 = this.networkStatMap.keySet().iterator();
            a.v(new StringBuilder().append("dump-session total connections:").append(this.networkStatMap.size()).toString());
            Object a1 = a0;
            while(((java.util.Iterator)a1).hasNext()) {
                int i = ((Integer)((java.util.Iterator)a1).next()).intValue();
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a2 = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo)this.networkStatMap.get(Integer.valueOf(i));
                String s = this.dnsCache.getHostnamefromIP(a2.destIP);
                if (s == null) {
                    s = a2.destIP;
                }
                a.v(new StringBuilder().append("dump-session fd[").append(i).append("] dest[").append(s).append("] tx[").append(a2.txBytes).append("] rx[").append(a2.rxBytes).append("]").toString());
            }
            if (b) {
                this.clear();
            }
        }
    }
    
    public void addStat(String s, int i, int i0, int i1) {
        synchronized(this) {
            com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo)this.networkStatMap.get(Integer.valueOf(i1));
            if (a != null) {
                a.txBytes = a.txBytes + i;
                a.rxBytes = a.rxBytes + i0;
            } else {
                java.util.HashMap a0 = this.networkStatMap;
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a1 = new com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo(i, i0, s);
                a0.put(Integer.valueOf(i1), a1);
            }
            if (s != null) {
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a2 = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo)this.networkStatMapBoot.get(s);
                if (a2 != null) {
                    a2.txBytes = a2.txBytes + i;
                    a2.rxBytes = a2.rxBytes + i0;
                } else {
                    this.networkStatMapBoot.put(s, new com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo(i, i0, s));
                }
            }
        }
        /*monexit(this)*/;
    }
    
    public void clear() {
        synchronized(this) {
            this.networkStatMap.clear();
        }
        /*monexit(this)*/;
    }
    
    public void dump(com.navdy.service.library.log.Logger a, boolean b) {
        synchronized(this) {
            this.dumpSessionStat(a, b);
            this.dumpBootStat(a);
        }
        /*monexit(this)*/;
    }
    
    public java.util.List getBootStat() {
        java.util.List a = null;
        synchronized(this) {
            a = this.getStat(this.networkStatMapBoot.values().iterator());
        }
        /*monexit(this)*/;
        return a;
    }
    
    public java.util.List getSessionStat() {
        java.util.List a = null;
        synchronized(this) {
            a = this.getStat(this.networkStatMap.values().iterator());
        }
        /*monexit(this)*/;
        return a;
    }
    
    public java.util.List getStat(java.util.Iterator a) {
        java.util.ArrayList a0 = null;
        synchronized(this) {
            a0 = new java.util.ArrayList();
            Object a1 = a;
            while(((java.util.Iterator)a1).hasNext()) {
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a2 = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo)((java.util.Iterator)a1).next();
                String s = this.dnsCache.getHostnamefromIP(a2.destIP);
                if (s == null) {
                    s = a2.destIP;
                }
                ((java.util.List)a0).add(new com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo(a2.txBytes, a2.rxBytes, s));
            }
        }
        /*monexit(this)*/;
        return (java.util.List)a0;
    }
}
