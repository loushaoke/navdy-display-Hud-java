package com.navdy.hud.app.framework.contacts;
import com.navdy.hud.app.R;

public class ContactUtil {
    final private static String EMPTY = "";
    private static String HOME_STR;
    private static java.util.HashSet MARKER_CHARS;
    private static String MOBILE_STR;
    final public static String SPACE = " ";
    final private static char SPACE_CHAR = (char)32;
    private static String WORK_STR;
    private static StringBuilder builder;
    private static android.os.Handler handler;
    
    static {
        builder = new StringBuilder();
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        WORK_STR = a.getString(R.string.work);
        MOBILE_STR = a.getString(R.string.mobile);
        HOME_STR = a.getString(R.string.home);
        MARKER_CHARS = new java.util.HashSet();
        MARKER_CHARS.add(Character.valueOf((char)8206));
        MARKER_CHARS.add(Character.valueOf((char)8234));
        MARKER_CHARS.add(Character.valueOf((char)8236));
        MARKER_CHARS.add(Character.valueOf((char)8206));
    }
    
    public ContactUtil() {
    }
    
    static void access$000(String s, String s0, boolean b, com.navdy.hud.app.ui.component.image.InitialsImageView a, com.squareup.picasso.Transformation a0, com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback a1, java.io.File a2, boolean b0, com.navdy.hud.app.framework.contacts.PhoneImageDownloader a3, android.graphics.Bitmap a4) {
        com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(s, s0, b, a, a0, a1, a2, b0, a3, a4);
    }
    
    static android.os.Handler access$100() {
        return handler;
    }
    
    public static com.navdy.service.library.events.contacts.PhoneNumberType convertNumberType(com.navdy.hud.app.framework.contacts.NumberType a) {
        com.navdy.service.library.events.contacts.PhoneNumberType a0 = null;
        if (a != null) {
            switch(com.navdy.hud.app.framework.contacts.ContactUtil$3.$SwitchMap$com$navdy$hud$app$framework$contacts$NumberType[a.ordinal()]) {
                case 4: {
                    a0 = com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_HOME;
                    break;
                }
                case 3: {
                    a0 = com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_MOBILE;
                    break;
                }
                case 1: case 2: {
                    a0 = com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_WORK;
                    break;
                }
                default: {
                    a0 = com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_OTHER;
                }
            }
        } else {
            a0 = null;
        }
        return a0;
    }
    
    public static java.util.List fromContacts(java.util.List a) {
        java.util.ArrayList a0 = null;
        if (a != null) {
            a0 = new java.util.ArrayList(a.size());
            Object a1 = a.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                ((java.util.List)a0).add(new com.navdy.hud.app.framework.contacts.Contact((com.navdy.service.library.events.contacts.Contact)((java.util.Iterator)a1).next()));
            }
        } else {
            a0 = null;
        }
        return (java.util.List)a0;
    }
    
    public static java.util.List fromPhoneNumbers(java.util.List a) {
        java.util.ArrayList a0 = null;
        if (a != null) {
            a0 = new java.util.ArrayList(a.size());
            Object a1 = a.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                ((java.util.List)a0).add(new com.navdy.hud.app.framework.contacts.Contact((com.navdy.service.library.events.contacts.PhoneNumber)((java.util.Iterator)a1).next()));
            }
        } else {
            a0 = null;
        }
        return (java.util.List)a0;
    }
    
    public static String getFirstName(String s) {
        String s0 = null;
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s0 = "";
        } else {
            s0 = s.trim();
            int i = s0.indexOf(" ");
            if (i > 0) {
                s0 = s0.substring(0, i);
            }
        }
        return s0;
    }
    
    public static String getInitials(String s) {
        String s0 = null;
        synchronized(com.navdy.hud.app.framework.contacts.ContactUtil.class) {
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                s0 = "";
            } else {
                String s1 = com.navdy.hud.app.util.GenericUtil.removePunctuation(s).trim();
                int i = s1.indexOf(" ");
                if (i <= 0) {
                    if (s1.length() <= 0) {
                        s0 = "";
                    } else {
                        int i0 = s1.charAt(0);
                        s0 = String.valueOf((char)i0).toUpperCase();
                    }
                } else {
                    String s2 = s1.substring(0, i).trim().replaceAll("[\\u202A]", "");
                    String s3 = s1.substring(i + 1).trim().replaceAll("[\\u202A]", "");
                    builder.setLength(0);
                    if (!android.text.TextUtils.isEmpty((CharSequence)s2)) {
                        StringBuilder a = builder;
                        int i1 = s2.charAt(0);
                        a.append((char)i1);
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s3)) {
                        StringBuilder a0 = builder;
                        int i2 = s3.charAt(0);
                        a0.append((char)i2);
                    }
                    s0 = builder.toString().toUpperCase();
                }
            }
        }
        /*monexit(com.navdy.hud.app.framework.contacts.ContactUtil.class)*/;
        return s0;
    }
    
    public static com.navdy.hud.app.framework.contacts.NumberType getNumberType(com.navdy.service.library.events.contacts.PhoneNumberType a) {
        com.navdy.hud.app.framework.contacts.NumberType a0 = null;
        if (a != null) {
            switch(com.navdy.hud.app.framework.contacts.ContactUtil$3.$SwitchMap$com$navdy$service$library$events$contacts$PhoneNumberType[a.ordinal()]) {
                case 3: {
                    a0 = com.navdy.hud.app.framework.contacts.NumberType.WORK;
                    break;
                }
                case 2: {
                    a0 = com.navdy.hud.app.framework.contacts.NumberType.MOBILE;
                    break;
                }
                case 1: {
                    a0 = com.navdy.hud.app.framework.contacts.NumberType.HOME;
                    break;
                }
                default: {
                    a0 = com.navdy.hud.app.framework.contacts.NumberType.OTHER;
                }
            }
        } else {
            a0 = null;
        }
        return a0;
    }
    
    public static long getPhoneNumber(String s) {
        long j = 0L;
        try {
            java.util.Locale a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale();
            j = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(s, a.getCountry()).getNationalNumber();
        } catch(Throwable ignoredException) {
            j = -1L;
        }
        return j;
    }
    
    public static String getPhoneType(com.navdy.hud.app.framework.contacts.NumberType a) {
        String s = null;
        if (a != null) {
            switch(com.navdy.hud.app.framework.contacts.ContactUtil$3.$SwitchMap$com$navdy$hud$app$framework$contacts$NumberType[a.ordinal()]) {
                case 4: {
                    s = HOME_STR;
                    break;
                }
                case 3: {
                    s = MOBILE_STR;
                    break;
                }
                case 1: case 2: {
                    s = WORK_STR;
                    break;
                }
                default: {
                    s = null;
                }
            }
        } else {
            s = null;
        }
        return s;
    }
    
    public static boolean isDisplayNameValid(String s, String s0, long j) {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
        label0: {
            label4: {
                if (!b0) {
                    break label4;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    break label4;
                }
                b = false;
                break label0;
            }
            boolean b1 = android.text.TextUtils.isEmpty((CharSequence)s);
            label3: {
                if (b1) {
                    break label3;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    break label3;
                }
                b = true;
                break label0;
            }
            if (s0.equalsIgnoreCase(s)) {
                b = false;
            } else {
                label2: {
                    int i = 0;
                    label1: {
                        try {
                            java.util.Locale a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale();
                            long j0 = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(s, a.getCountry()).getNationalNumber();
                            i = (j0 < j) ? -1 : (j0 == j) ? 0 : 1;
                            break label1;
                        } catch(Throwable ignoredException) {
                        }
                        break label2;
                    }
                    if (i == 0) {
                        b = false;
                        break label0;
                    }
                }
                b = true;
            }
        }
        return b;
    }
    
    public static boolean isImageAvailable(String s, String s0) {
        boolean b = false;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
        label0: {
            label1: {
                if (!b0) {
                    break label1;
                }
                if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    b = false;
                    break label0;
                }
            }
            b = !android.text.TextUtils.isEmpty((CharSequence)s0);
        }
        return b;
    }
    
    public static boolean isValidNumber(String s) {
        boolean b = false;
        try {
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                b = false;
            } else {
                java.util.Locale a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale();
                com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(s, a.getCountry());
                b = true;
            }
        } catch(Throwable ignoredException) {
            b = false;
        }
        return b;
    }
    
    public static String sanitizeString(String s) {
        if (s != null) {
            int i = s.length();
            char[] a = s.toCharArray();
            boolean b = false;
            int i0 = 0;
            while(i0 < a.length) {
                if (MARKER_CHARS.contains(Character.valueOf((char)(boolean)a[i0]))) {
                    a[i0] = (char)32;
                    b = true;
                }
                i0 = i0 + 1;
            }
            s = b ? new String(a, 0, i).trim() : s.trim();
        }
        return s;
    }
    
    public static void setContactPhoto(String s, String s0, boolean b, com.navdy.hud.app.ui.component.image.InitialsImageView a, com.squareup.picasso.Transformation a0, com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback a1) {
        a.setTag(null);
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
        label1: {
            label0: {
                if (!b0) {
                    break label0;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    break label0;
                }
                a.setImage(R.drawable.icon_user_numberonly, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                break label1;
            }
            if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                a.setImage(R.drawable.icon_user_numberonly, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
            } else {
                if (!com.navdy.hud.app.framework.contacts.ContactUtil.isDisplayNameValid(s, s0, com.navdy.hud.app.framework.contacts.ContactUtil.getPhoneNumber(s0))) {
                    s = null;
                }
                com.navdy.hud.app.framework.contacts.PhoneImageDownloader a2 = com.navdy.hud.app.framework.contacts.PhoneImageDownloader.getInstance();
                java.io.File a3 = a2.getImagePath(s0, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT);
                android.graphics.Bitmap a4 = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(a3);
                if (a4 == null) {
                    com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.contacts.ContactUtil$1(a1, a3, s, s0, b, a, a0, a2), 1);
                } else {
                    a.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                    a.setImageBitmap(a4);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(s, s0, b, a, a0, a1, a3, false, a2, a4);
                }
            }
        }
    }
    
    private static void setContactPhoto(String s, String s0, boolean b, com.navdy.hud.app.ui.component.image.InitialsImageView a, com.squareup.picasso.Transformation a0, com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback a1, java.io.File a2, boolean b0, com.navdy.hud.app.framework.contacts.PhoneImageDownloader a3, android.graphics.Bitmap a4) {
        label0: {
            label1: {
                if (a4 != null) {
                    break label1;
                }
                if (!a1.isContextValid()) {
                    break label0;
                }
                if (b0) {
                    com.navdy.hud.app.framework.contacts.ContactUtil.updateInitials(com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT, (String)null, a);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setImage(a2, a, a0);
                } else if (s == null) {
                    a.setImage(R.drawable.icon_user_numberonly, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                } else {
                    com.navdy.hud.app.framework.contacts.ContactImageHelper a5 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
                    a.setImage(a5.getResourceId(a5.getContactImageIndex(s0)), com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(s), com.navdy.hud.app.ui.component.image.InitialsImageView$Style.LARGE);
                }
            }
            if (b) {
                a3.clearPhotoCheckEntry(s0, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT);
                a3.submitDownload(s0, com.navdy.hud.app.framework.contacts.PhoneImageDownloader$Priority.HIGH, com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT, s);
            }
        }
    }
    
    private static void setImage(java.io.File a, com.navdy.hud.app.ui.component.image.InitialsImageView a0, com.squareup.picasso.Transformation a1) {
        com.navdy.hud.app.util.picasso.PicassoUtil.getInstance().load(a).fit().transform(a1).into((android.widget.ImageView)a0, (com.squareup.picasso.Callback)new com.navdy.hud.app.framework.contacts.ContactUtil$2(a0, a));
    }
    
    public static java.util.List toContacts(java.util.List a) {
        java.util.ArrayList a0 = null;
        if (a != null) {
            a0 = new java.util.ArrayList(a.size());
            Object a1 = a.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                com.navdy.hud.app.framework.contacts.Contact a2 = (com.navdy.hud.app.framework.contacts.Contact)((java.util.Iterator)a1).next();
                com.navdy.service.library.events.contacts.Contact$Builder a3 = new com.navdy.service.library.events.contacts.Contact$Builder().name(a2.name).number(a2.number).numberType(com.navdy.hud.app.framework.contacts.ContactUtil.convertNumberType(a2.numberType));
                String s = (a2.numberType != com.navdy.hud.app.framework.contacts.NumberType.OTHER) ? null : a2.numberTypeStr;
                ((java.util.List)a0).add(a3.label(s).build());
            }
        } else {
            a0 = null;
        }
        return (java.util.List)a0;
    }
    
    public static java.util.List toPhoneNumbers(java.util.List a) {
        java.util.ArrayList a0 = null;
        if (a != null) {
            a0 = new java.util.ArrayList(a.size());
            Object a1 = a.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                com.navdy.hud.app.framework.contacts.Contact a2 = (com.navdy.hud.app.framework.contacts.Contact)((java.util.Iterator)a1).next();
                com.navdy.service.library.events.contacts.PhoneNumber$Builder a3 = new com.navdy.service.library.events.contacts.PhoneNumber$Builder().number(a2.number).numberType(com.navdy.hud.app.framework.contacts.ContactUtil.convertNumberType(a2.numberType));
                String s = (a2.numberType != com.navdy.hud.app.framework.contacts.NumberType.OTHER) ? null : a2.numberTypeStr;
                ((java.util.List)a0).add(a3.customType(s).build());
            }
        } else {
            a0 = null;
        }
        return (java.util.List)a0;
    }
    
    private static void updateInitials(com.navdy.hud.app.ui.component.image.InitialsImageView$Style a, String s, com.navdy.hud.app.ui.component.image.InitialsImageView a0) {
        a0.setInitials(s, a);
    }
}
