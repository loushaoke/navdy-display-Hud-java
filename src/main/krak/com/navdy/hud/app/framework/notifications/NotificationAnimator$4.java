package com.navdy.hud.app.framework.notifications;

final class NotificationAnimator$4 implements Runnable {
    final Runnable val$endAction;
    final android.view.View val$expandedNotifView;
    final com.navdy.hud.app.framework.notifications.NotificationManager$Info val$notificationObj;
    final com.navdy.hud.app.view.NotificationView val$notificationView;
    
    NotificationAnimator$4(android.view.View a, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0, com.navdy.hud.app.view.NotificationView a1, Runnable a2) {
        super();
        this.val$expandedNotifView = a;
        this.val$notificationObj = a0;
        this.val$notificationView = a1;
        this.val$endAction = a2;
    }
    
    public void run() {
        this.val$expandedNotifView.setVisibility(8);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().setExpandedStack(false);
        if (this.val$notificationObj != null && this.val$notificationObj.startCalled) {
            com.navdy.hud.app.framework.notifications.NotificationAnimator.access$000().v("calling onExpandedNotificationEvent-collapse");
            this.val$notificationObj.notification.onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode.COLLAPSE);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().setNotificationColor();
            this.val$notificationView.showNextNotificationColor();
        }
        if (this.val$endAction != null) {
            this.val$endAction.run();
        }
    }
}
