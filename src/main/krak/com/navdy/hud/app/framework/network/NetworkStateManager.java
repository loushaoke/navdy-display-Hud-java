package com.navdy.hud.app.framework.network;

public class NetworkStateManager {
    final private static String NAVDY_NETWORK_DOWN_INTENT = "com.navdy.hud.NetworkDown";
    final private static String NAVDY_NETWORK_UP_INTENT = "com.navdy.hud.NetworkUp";
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.framework.network.NetworkStateManager singleton;
    @Inject
    com.squareup.otto.Bus bus;
    private int countDown;
    private android.os.Handler handler;
    private com.navdy.service.library.events.settings.NetworkStateChange lastPhoneNetworkState;
    private Runnable networkCheck;
    private volatile boolean networkStateInitialized;
    private Runnable triggerNetworkCheck;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.network.NetworkStateManager.class);
        singleton = new com.navdy.hud.app.framework.network.NetworkStateManager();
    }
    
    private NetworkStateManager() {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.countDown = 3;
        this.triggerNetworkCheck = (Runnable)new com.navdy.hud.app.framework.network.NetworkStateManager$1(this);
        this.networkCheck = (Runnable)new com.navdy.hud.app.framework.network.NetworkStateManager$2(this);
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.bus.register(this);
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            this.handler.post(this.triggerNetworkCheck);
        } else {
            this.networkStateInitialized = true;
            this.bus.post(new com.navdy.hud.app.framework.network.NetworkStateManager$HudNetworkInitialized());
        }
    }
    
    static boolean access$000(com.navdy.hud.app.framework.network.NetworkStateManager a) {
        return a.networkStateInitialized;
    }
    
    static boolean access$002(com.navdy.hud.app.framework.network.NetworkStateManager a, boolean b) {
        a.networkStateInitialized = b;
        return b;
    }
    
    static Runnable access$100(com.navdy.hud.app.framework.network.NetworkStateManager a) {
        return a.networkCheck;
    }
    
    static com.navdy.service.library.log.Logger access$200() {
        return sLogger;
    }
    
    static int access$300(com.navdy.hud.app.framework.network.NetworkStateManager a) {
        return a.countDown;
    }
    
    static int access$306(com.navdy.hud.app.framework.network.NetworkStateManager a) {
        int i = a.countDown - 1;
        a.countDown = i;
        return i;
    }
    
    static com.navdy.service.library.events.settings.NetworkStateChange access$400(com.navdy.hud.app.framework.network.NetworkStateManager a) {
        return a.lastPhoneNetworkState;
    }
    
    static Runnable access$500(com.navdy.hud.app.framework.network.NetworkStateManager a) {
        return a.triggerNetworkCheck;
    }
    
    static android.os.Handler access$600(com.navdy.hud.app.framework.network.NetworkStateManager a) {
        return a.handler;
    }
    
    public static com.navdy.hud.app.framework.network.NetworkStateManager getInstance() {
        return singleton;
    }
    
    public static boolean isConnectedToCellNetwork(android.content.Context a) {
        android.net.NetworkInfo a0 = ((android.net.ConnectivityManager)a.getSystemService("connectivity")).getNetworkInfo(0);
        return a0 != null && a0.isConnected();
    }
    
    public static boolean isConnectedToNetwork(android.content.Context a) {
        boolean b = false;
        android.net.NetworkInfo a0 = ((android.net.ConnectivityManager)a.getSystemService("connectivity")).getActiveNetworkInfo();
        sLogger.v(new StringBuilder().append("setEngineOnlineState active network=").append((a0 == null) ? " no active network " : a0.getTypeName()).toString());
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.isConnectedOrConnecting()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static boolean isConnectedToWifi(android.content.Context a) {
        boolean b = false;
        android.net.NetworkInfo a0 = ((android.net.ConnectivityManager)a.getSystemService("connectivity")).getNetworkInfo(1);
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                if (a0.isConnected()) {
                    b = true;
                    break label0;
                }
            }
            b = false;
        }
        return b;
    }
    
    public com.navdy.service.library.events.settings.NetworkStateChange getLastPhoneNetworkState() {
        return this.lastPhoneNetworkState;
    }
    
    public boolean isNetworkStateInitialized() {
        return this.networkStateInitialized;
    }
    
    public void networkAvailable() {
        if (this.networkStateInitialized) {
            if (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isNetworkDisabled()) {
                sLogger.i("network is permanently disabled");
                this.networkNotAvailable();
            } else {
                android.content.Intent a = new android.content.Intent("com.navdy.hud.NetworkUp");
                android.os.UserHandle a0 = android.os.Process.myUserHandle();
                com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(a, a0);
                sLogger.v("DummyNetNetworkFactory:sent:com.navdy.hud.NetworkUp");
            }
        } else {
            sLogger.v("network not initialized yet");
        }
    }
    
    public void networkNotAvailable() {
        if (this.networkStateInitialized) {
            android.content.Intent a = new android.content.Intent("com.navdy.hud.NetworkDown");
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(a, a0);
            sLogger.v("DummyNetNetworkFactory:sent:com.navdy.hud.NetworkDown");
        } else {
            sLogger.v("network not initialized yet");
        }
    }
    
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        sLogger.d(new StringBuilder().append("ConnectionStateChange - ").append(a.state).toString());
        if (com.navdy.hud.app.framework.network.NetworkStateManager$3.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()] != 0) {
            this.lastPhoneNetworkState = null;
        }
    }
    
    public void onNetworkStateChange(com.navdy.service.library.events.settings.NetworkStateChange a) {
        try {
            this.lastPhoneNetworkState = a;
            sLogger.v(new StringBuilder().append("NetworkStateChange available[").append(a.networkAvailable).append("] cell[").append(a.cellNetwork).append("] wifi[").append(a.wifiNetwork).append("] reach[").append(a.reachability).append("]").toString());
            if (Boolean.TRUE.equals(a.networkAvailable)) {
                sLogger.v("DummyNetNetworkFactory:phone n/w avaialble");
                this.networkAvailable();
            } else {
                sLogger.v("DummyNetNetworkFactory:phone n/w lost");
                this.networkNotAvailable();
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
}
