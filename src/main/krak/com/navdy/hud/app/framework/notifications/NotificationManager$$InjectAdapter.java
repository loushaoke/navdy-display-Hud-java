package com.navdy.hud.app.framework.notifications;

final public class NotificationManager$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding uiStateManager;
    
    public NotificationManager$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.framework.notifications.NotificationManager", false, com.navdy.hud.app.framework.notifications.NotificationManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.framework.notifications.NotificationManager.class, (this).getClass().getClassLoader());
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.framework.notifications.NotificationManager.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.uiStateManager);
        a0.add(this.bus);
    }
    
    public void injectMembers(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        a.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get();
        a.bus = (com.squareup.otto.Bus)this.bus.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.framework.notifications.NotificationManager)a);
    }
}
