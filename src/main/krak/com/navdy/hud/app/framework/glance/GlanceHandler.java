package com.navdy.hud.app.framework.glance;

public class GlanceHandler {
    final private static String EMPTY = "";
    final private static java.text.SimpleDateFormat dateFormat1;
    final private static java.text.SimpleDateFormat dateFormat2;
    final private static java.text.SimpleDateFormat dateFormat3;
    final private static java.text.SimpleDateFormat dateFormat4;
    final private static java.text.SimpleDateFormat dateFormat5;
    final private static Object lockObj;
    final private static com.navdy.hud.app.framework.glance.GlanceHandler sInstance;
    final public static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.framework.glance.GlanceTracker glanceTracker;
    private java.util.HashMap messagesSent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceHandler.class);
        sInstance = new com.navdy.hud.app.framework.glance.GlanceHandler();
        lockObj = new Object();
        dateFormat1 = new java.text.SimpleDateFormat("hh:mm a");
        dateFormat2 = new java.text.SimpleDateFormat("hh:mm");
        dateFormat3 = new java.text.SimpleDateFormat("MMM d, hh:mm a");
        dateFormat4 = new java.text.SimpleDateFormat("h");
        dateFormat5 = new java.text.SimpleDateFormat("h a");
    }
    
    private GlanceHandler() {
        this.glanceTracker = new com.navdy.hud.app.framework.glance.GlanceTracker();
        this.messagesSent = new java.util.HashMap();
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }
    
    static void access$000(com.navdy.hud.app.framework.glance.GlanceHandler a, com.navdy.service.library.events.glances.GlanceEvent a0) {
        a.handleGlancEventInternal(a0);
    }
    
    private void clearAllMessage() {
        synchronized(this.messagesSent) {
            this.messagesSent.clear();
            /*monexit(a)*/;
        }
    }
    
    private long extractTime(String s) {
        String s0 = null;
        String s1 = null;
        long j = 0L;
        int i = s.indexOf("\u2013");
        if (i == -1) {
            s0 = com.navdy.hud.app.framework.contacts.ContactUtil.sanitizeString(s);
            s1 = null;
        } else {
            String s2 = s.substring(i + 1).trim();
            String s3 = s.substring(0, i).trim();
            s1 = com.navdy.hud.app.framework.contacts.ContactUtil.sanitizeString(s2);
            s0 = com.navdy.hud.app.framework.contacts.ContactUtil.sanitizeString(s3);
        }        label1: synchronized(lockObj) {
            java.util.Calendar a0 = java.util.Calendar.getInstance();
            label5: {
                try {
                    java.util.Calendar a1 = java.util.Calendar.getInstance();
                    a1.setTime(dateFormat1.parse(s0));
                    a0.set(10, a1.get(10));
                    a0.set(12, a1.get(12));
                    a0.set(13, a1.get(13));
                    a0.set(14, 0);
                    a0.set(9, a1.get(9));
                    java.util.Date a2 = a0.getTime();
                    sLogger.v(new StringBuilder().append("event-time-date = ").append(a2.toString()).toString());
                    j = a2.getTime();
                } catch(Throwable ignoredException) {
                    break label5;
                }
                /*monexit(a)*/;
                break label1;
            }
            label4: {
                try {
                    java.util.Calendar a3 = java.util.Calendar.getInstance();
                    a3.setTime(dateFormat2.parse(s0));
                    a0.set(10, a3.get(10));
                    a0.set(12, a3.get(12));
                    a0.set(13, a3.get(13));
                    a0.set(14, 0);
                    if (s1 != null) {
                        try {
                            java.util.Calendar a4 = java.util.Calendar.getInstance();
                            a4.setTime(dateFormat1.parse(s1));
                            a0.set(9, a4.get(9));
                        } catch(Throwable ignoredException0) {
                        }
                    }
                    java.util.Date a5 = a0.getTime();
                    sLogger.v(new StringBuilder().append("event-time-date = ").append(a5.toString()).toString());
                    j = a5.getTime();
                } catch(Throwable ignoredException1) {
                    break label4;
                }
                /*monexit(a)*/;
                break label1;
            }
            label3: {
                try {
                    java.util.Calendar a6 = java.util.Calendar.getInstance();
                    a6.setTime(dateFormat3.parse(s0));
                    a0.set(10, a6.get(10));
                    a0.set(12, a6.get(12));
                    a0.set(13, a6.get(13));
                    a0.set(14, 0);
                    a0.set(9, a6.get(9));
                    java.util.Date a7 = a0.getTime();
                    sLogger.v(new StringBuilder().append("event-time-date = ").append(a7.toString()).toString());
                    j = a7.getTime();
                } catch(Throwable ignoredException2) {
                    break label3;
                }
                /*monexit(a)*/;
                break label1;
            }
            label2: {
                try {
                    java.util.Calendar a8 = java.util.Calendar.getInstance();
                    a8.setTime(dateFormat4.parse(s0));
                    a0.set(10, a8.get(10));
                    a0.set(12, 0);
                    a0.set(13, 0);
                    a0.set(14, 0);
                    if (s1 != null) {
                        try {
                            java.util.Calendar a9 = java.util.Calendar.getInstance();
                            a9.setTime(dateFormat5.parse(s1));
                            a0.set(9, a9.get(9));
                        } catch(Throwable ignoredException3) {
                        }
                    }
                    java.util.Date a10 = a0.getTime();
                    sLogger.v(new StringBuilder().append("event-time-date = ").append(a10.toString()).toString());
                    j = a10.getTime();
                } catch(Throwable ignoredException4) {
                    break label2;
                }
                /*monexit(a)*/;
                break label1;
            }
            label0: {
                try {
                    java.util.Calendar a11 = java.util.Calendar.getInstance();
                    a11.setTime(dateFormat5.parse(s0));
                    a0.set(10, a11.get(10));
                    a0.set(12, a11.get(12));
                    a0.set(13, a11.get(13));
                    a0.set(14, 0);
                    a0.set(9, a11.get(9));
                    java.util.Date a12 = a0.getTime();
                    sLogger.v(new StringBuilder().append("event-time-date = ").append(a12.toString()).toString());
                    j = a12.getTime();
                    break label0;
                } catch(Throwable ignoredException5) {
                }
                /*monexit(a)*/;
                j = 0L;
                break label1;
            }
            /*monexit(a)*/;
        }
        return j;
    }
    
    public static com.navdy.hud.app.framework.glance.GlanceHandler getInstance() {
        return sInstance;
    }
    
    private void handleGlancEventInternal(com.navdy.service.library.events.glances.GlanceEvent a) {
        com.navdy.hud.app.framework.glance.GlanceHandler.printGlance(a);
        boolean b = android.text.TextUtils.isEmpty((CharSequence)a.id);
        label2: {
            Object a0 = null;
            com.navdy.hud.app.framework.glance.GlanceNotification a1 = null;
            String s = null;
            label3: {
                label4: {
                    if (b) {
                        break label4;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)a.provider)) {
                        break label4;
                    }
                    if (a.glanceType == null) {
                        break label4;
                    }
                    if (a.glanceData == null) {
                        break label4;
                    }
                    if (a.glanceData.size() != 0) {
                        break label3;
                    }
                }
                sLogger.v("invalid glance event");
                break label2;
            }
            com.navdy.hud.app.framework.glance.GlanceApp a2 = com.navdy.hud.app.framework.glance.GlanceHelper.getGlancesApp(a);
            if (a2 == null) {
                a2 = com.navdy.hud.app.framework.glance.GlanceApp.GENERIC;
            }
            java.util.Map a3 = com.navdy.hud.app.framework.glance.GlanceHelper.buildDataMap(a);
            switch(com.navdy.hud.app.framework.glance.GlanceHandler$3.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[a2.ordinal()]) {
                case 12: case 13: case 14: case 15: {
                    String s0 = (String)a3.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_BODY.name());
                    String s1 = (String)a3.get(com.navdy.service.library.events.glances.EmailConstants.EMAIL_SUBJECT.name());
                    boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s0);
                    a0 = null;
                    if (!b0) {
                        break;
                    }
                    boolean b1 = android.text.TextUtils.isEmpty((CharSequence)s1);
                    a0 = null;
                    if (!b1) {
                        break;
                    }
                    sLogger.v(new StringBuilder().append("glance message does not exist:").append(a2).toString());
                    break label2;
                }
                case 11: {
                    boolean b2 = android.text.TextUtils.isEmpty((CharSequence)(String)a3.get(com.navdy.service.library.events.glances.GenericConstants.GENERIC_MESSAGE.name()));
                    a0 = null;
                    if (!b2) {
                        break;
                    }
                    sLogger.v(new StringBuilder().append("glance message does not exist:").append(a2).toString());
                    break label2;
                }
                case 8: case 9: case 10: {
                    boolean b3 = android.text.TextUtils.isEmpty((CharSequence)(String)a3.get(com.navdy.service.library.events.glances.SocialConstants.SOCIAL_MESSAGE.name()));
                    a0 = null;
                    if (!b3) {
                        break;
                    }
                    sLogger.v(new StringBuilder().append("glance message does not exist:").append(a2).toString());
                    break label2;
                }
                case 4: case 5: case 6: case 7: {
                    boolean b4 = android.text.TextUtils.isEmpty((CharSequence)(String)a3.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name()));
                    a0 = null;
                    if (!b4) {
                        break;
                    }
                    sLogger.v(new StringBuilder().append("glance message does not exist:").append(a2).toString());
                    break label2;
                }
                case 1: case 2: case 3: {
                    String s2 = null;
                    boolean b5 = false;
                    java.util.List a4 = null;
                    boolean b6 = android.text.TextUtils.isEmpty((CharSequence)(String)a3.get(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_BODY.name()));
                    label1: {
                        if (!b6) {
                            break label1;
                        }
                        sLogger.v(new StringBuilder().append("glance message does not exist:").append(a2).toString());
                        break label2;
                    }
                    String s3 = com.navdy.hud.app.framework.glance.GlanceHelper.getNumber(a2, a3);
                    String s4 = com.navdy.hud.app.framework.glance.GlanceHelper.getFrom(a2, a3);
                    if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(s3)) {
                        sLogger.v(new StringBuilder().append("hasNumber:").append(s3).toString());
                        if (s4 != null) {
                            s2 = s4;
                            b5 = true;
                        } else {
                            s2 = s3;
                            b5 = true;
                        }
                    } else {
                        s2 = null;
                        b5 = false;
                        s3 = s4;
                    }
                    a0 = null;
                    if (s3 == null) {
                        break;
                    }
                    com.navdy.hud.app.framework.recentcall.RecentCall a5 = new com.navdy.hud.app.framework.recentcall.RecentCall(s2, com.navdy.hud.app.framework.recentcall.RecentCall$Category.MESSAGE, s3, com.navdy.hud.app.framework.contacts.NumberType.OTHER, new java.util.Date(), com.navdy.hud.app.framework.recentcall.RecentCall$CallType.INCOMING, -1, 0L);
                    com.navdy.hud.app.framework.recentcall.RecentCallManager a6 = com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance();
                    boolean b7 = a6.handleNewCall(a5);
                    Object a7 = null;
                    if (b7) {
                        a4 = (java.util.List)a7;
                    } else {
                        a4 = a6.getContactsFromId(s3);
                        sLogger.v(new StringBuilder().append("recent call contact list found [").append(s3).append("]").toString());
                    }
                    sLogger.v(new StringBuilder().append("recent call id[").append(s3).append("]").toString());
                    if (b5) {
                        a0 = a4;
                        break;
                    } else if (b7) {
                        sLogger.v(new StringBuilder().append("got Number:").append(a5.number).toString());
                        java.util.ArrayList a8 = new java.util.ArrayList();
                        java.util.Iterator a9 = a.glanceData.iterator();
                        a0 = a4;
                        Object a10 = a9;
                        while(((java.util.Iterator)a10).hasNext()) {
                            com.navdy.service.library.events.glances.KeyValue a11 = (com.navdy.service.library.events.glances.KeyValue)((java.util.Iterator)a10).next();
                            if (!android.text.TextUtils.equals((CharSequence)a11.key, (CharSequence)com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name())) {
                                ((java.util.List)a8).add(a11);
                            }
                        }
                        ((java.util.List)a8).add(new com.navdy.service.library.events.glances.KeyValue(com.navdy.service.library.events.glances.MessageConstants.MESSAGE_FROM_NUMBER.name(), a5.number));
                        com.navdy.service.library.events.glances.GlanceEvent a12 = new com.navdy.service.library.events.glances.GlanceEvent$Builder().id(a.id).actions(a.actions).postTime(a.postTime).glanceType(a.glanceType).provider(a.provider).glanceData((java.util.List)a8).build();
                        a3 = com.navdy.hud.app.framework.glance.GlanceHelper.buildDataMap(a12);
                        a = a12;
                        break;
                    } else {
                        sLogger.v(new StringBuilder().append("no Number yet:").append(s3).toString());
                        a0 = a4;
                        break;
                    }
                }
                default: {
                    a0 = null;
                }
            }
            if (com.navdy.hud.app.framework.glance.GlanceHelper.usesMessageLayout(a2)) {
                boolean b8 = com.navdy.hud.app.framework.glance.GlanceHelper.needsScrollLayout(com.navdy.hud.app.framework.glance.GlanceHelper.getGlanceMessage(a2, a3));
                a1 = b8 ? new com.navdy.hud.app.framework.glance.GlanceNotification(a, a2, com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_GLANCE_MESSAGE, a3) : new com.navdy.hud.app.framework.glance.GlanceNotification(a, a2, a3);
                s = new StringBuilder().append("posted glance [").append(a.id).append("] scroll[").append(b8).append("]").toString();
            } else if (com.navdy.hud.app.framework.glance.GlanceHelper.isCalendarApp(a2)) {
                long j = 0L;
                String s5 = (String)a3.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name());
                label0: {
                    Throwable a13 = null;
                    if (s5 == null) {
                        j = 0L;
                        break label0;
                    } else {
                        try {
                            j = Long.parseLong(s5);
                            break label0;
                        } catch(Throwable a14) {
                            a13 = a14;
                        }
                    }
                    sLogger.e(a13);
                    j = 0L;
                }
                if (j == 0L) {
                    String s6 = (String)a3.get(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME_STR.name());
                    sLogger.v(new StringBuilder().append("event-time-str =").append(s6).toString());
                    if (!android.text.TextUtils.isEmpty((CharSequence)s6)) {
                        j = this.extractTime(s6);
                    }
                    if (j != 0L) {
                        a3.put(com.navdy.service.library.events.glances.CalendarConstants.CALENDAR_TIME.name(), String.valueOf(j));
                    }
                }
                a1 = new com.navdy.hud.app.framework.glance.GlanceNotification(a, a2, a3);
                s = new StringBuilder().append("posted glance [").append(a.id).append("] time[").append(j).append("]").toString();
            } else {
                a1 = new com.navdy.hud.app.framework.glance.GlanceNotification(a, a2, a3);
                s = new StringBuilder().append("posted glance [").append(a.id).append("]").toString();
            }
            long j0 = this.glanceTracker.isNotificationSeen(a, a2, a3);
            if (j0 <= 0L) {
                if (a0 != null) {
                    java.util.ArrayList a15 = new java.util.ArrayList();
                    Object a16 = ((java.util.List)a0).iterator();
                    while(((java.util.Iterator)a16).hasNext()) {
                        ((java.util.List)a15).add(new com.navdy.hud.app.framework.contacts.Contact((com.navdy.service.library.events.contacts.Contact)((java.util.Iterator)a16).next()));
                    }
                    a1.setContactList((java.util.List)a15);
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification((com.navdy.hud.app.framework.notifications.INotification)a1);
                sLogger.v(s);
            } else {
                sLogger.v(new StringBuilder().append("glance already seen [").append(a.id).append("] at [").append(j0).append("] now [").append(android.os.SystemClock.elapsedRealtime()).append("]").toString());
            }
        }
    }
    
    private static void printGlance(com.navdy.service.library.events.glances.GlanceEvent a) {
        if (a != null) {
            sLogger.v(new StringBuilder().append("[glance-event] id[").append(a.id).append("] type[").append(a.glanceType).append("] provider[").append(a.provider).append("] data-size [").append((a.glanceData != null) ? a.glanceData.size() : 0).append("] action-size [").append((a.actions != null) ? a.actions.size() : 0).append("]").toString());
            if (a.glanceData != null && a.glanceData.size() > 0) {
                Object a0 = a.glanceData.iterator();
                while(((java.util.Iterator)a0).hasNext()) {
                    com.navdy.service.library.events.glances.KeyValue a1 = (com.navdy.service.library.events.glances.KeyValue)((java.util.Iterator)a0).next();
                    sLogger.v(new StringBuilder().append("[glance-event] data key[").append(a1.key).append("] val[").append(a1.value).append("]").toString());
                }
            }
            if (a.actions != null && a.actions.size() > 0) {
                Object a2 = a.actions.iterator();
                while(((java.util.Iterator)a2).hasNext()) {
                    com.navdy.service.library.events.glances.GlanceEvent$GlanceActions a3 = (com.navdy.service.library.events.glances.GlanceEvent$GlanceActions)((java.util.Iterator)a2).next();
                    sLogger.v(new StringBuilder().append("[glance-event] action[").append(a3).append("]").toString());
                }
            }
        } else {
            sLogger.v("null glance event");
        }
    }
    
    public void addMessage(String s, String s0) {
        java.util.HashMap a = null;
        Throwable a0 = null;
        label0: {
            if (s != null && s0 != null) {
                synchronized(this.messagesSent) {
                    this.messagesSent.put(s, s0);
                    /*monexit(a)*/;
                }
            }
            return;
        }
        while(true) {
            try {
                /*monexit(a)*/;
            } catch(IllegalMonitorStateException | NullPointerException a2) {
                Throwable a3 = a2;
                a0 = a3;
                continue;
            }
            throw a0;
        }
    }
    
    public void clearState() {
        this.glanceTracker.clearState();
        this.clearAllMessage();
    }
    
    public void onGlanceEvent(com.navdy.service.library.events.glances.GlanceEvent a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.glance.GlanceHandler$1(this, a), 14);
    }
    
    public void onMessage(com.navdy.service.library.events.messaging.SmsMessageResponse a) {
        try {
            if (a.status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(false, "Android", com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
                String s = this.removeMessage(a.id);
                if (s == null) {
                    sLogger.e(new StringBuilder().append("sms message with id:").append(a.id).append(" not found").toString());
                } else {
                    com.navdy.hud.app.framework.glance.GlanceHandler.getInstance().sendSmsFailedNotification(a.number, s, a.name);
                }
            } else {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(true, "Android", com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    public String removeMessage(String s) {
        java.util.HashMap a = null;
        Throwable a0 = null;
        label0: {
            String s0 = null;
            if (s != null) {
                synchronized(this.messagesSent) {
                    s0 = (String)this.messagesSent.remove(s);
                    /*monexit(a)*/;
                }
            } else {
                s0 = null;
            }
            return s0;
        }
        while(true) {
            try {
                /*monexit(a)*/;
            } catch(IllegalMonitorStateException | NullPointerException a2) {
                Throwable a3 = a2;
                a0 = a3;
                continue;
            }
            throw a0;
        }
    }
    
    public void restoreState() {
        this.glanceTracker.restoreState();
    }
    
    public void saveState() {
        this.glanceTracker.saveState();
    }
    
    public boolean sendMessage(String s, String s0, String s1) {
        boolean b = false;
        try {
            if (com.navdy.hud.app.util.GenericUtil.isClientiOS()) {
                if (com.navdy.hud.app.framework.twilio.TwilioSmsManager.getInstance().sendSms(s, s0, (com.navdy.hud.app.framework.twilio.TwilioSmsManager$Callback)new com.navdy.hud.app.framework.glance.GlanceHandler$2(this, s, s0, s1)) != com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.SUCCESS) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(false, "iOS", com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
                    b = false;
                } else {
                    sLogger.v("sms-twilio queued");
                    b = true;
                }
            } else {
                String s2 = java.util.UUID.randomUUID().toString();
                this.addMessage(s2, s0);
                com.navdy.service.library.events.messaging.SmsMessageRequest a = new com.navdy.service.library.events.messaging.SmsMessageRequest(s, s0, s1, s2);
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a));
                sLogger.v("sms sent");
                b = true;
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
            b = false;
        }
        return b;
    }
    
    public void sendSmsFailedNotification(String s, String s0, String s1) {
        com.navdy.hud.app.framework.message.SmsNotification a = new com.navdy.hud.app.framework.message.SmsNotification(com.navdy.hud.app.framework.message.SmsNotification$Mode.Failed, java.util.UUID.randomUUID().toString(), s, s0, s1);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification((com.navdy.hud.app.framework.notifications.INotification)a);
    }
    
    public void sendSmsSuccessNotification(String s, String s0, String s1, String s2) {
        com.navdy.hud.app.framework.message.SmsNotification a = new com.navdy.hud.app.framework.message.SmsNotification(com.navdy.hud.app.framework.message.SmsNotification$Mode.Success, s, s0, s1, s2);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification((com.navdy.hud.app.framework.notifications.INotification)a);
    }
}
