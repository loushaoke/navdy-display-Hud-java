package com.navdy.hud.app.framework.music;

public class MusicCollection {
    public java.util.List collections;
    public com.navdy.service.library.events.audio.MusicCollectionInfo musicCollectionInfo;
    public java.util.List tracks;
    
    public MusicCollection() {
        this.collections = (java.util.List)new java.util.ArrayList();
        this.tracks = (java.util.List)new java.util.ArrayList();
    }
}
