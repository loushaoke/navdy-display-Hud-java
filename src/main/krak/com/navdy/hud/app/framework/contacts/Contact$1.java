package com.navdy.hud.app.framework.contacts;

final class Contact$1 implements android.os.Parcelable$Creator {
    Contact$1() {
    }
    
    public com.navdy.hud.app.framework.contacts.Contact createFromParcel(android.os.Parcel a) {
        return new com.navdy.hud.app.framework.contacts.Contact(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.hud.app.framework.contacts.Contact[] newArray(int i) {
        return new com.navdy.hud.app.framework.contacts.Contact[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
