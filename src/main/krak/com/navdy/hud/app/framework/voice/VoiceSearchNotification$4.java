package com.navdy.hud.app.framework.voice;

class VoiceSearchNotification$4 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    private boolean canceled;
    final com.navdy.hud.app.framework.voice.VoiceSearchNotification this$0;
    
    VoiceSearchNotification$4(com.navdy.hud.app.framework.voice.VoiceSearchNotification a) {
        super();
        this.this$0 = a;
        this.canceled = false;
    }
    
    public void onAnimationCancel(android.animation.Animator a) {
        this.canceled = true;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (!this.canceled) {
            com.navdy.hud.app.framework.voice.VoiceSearchNotification.access$500(this.this$0).setStartDelay(33L);
            com.navdy.hud.app.framework.voice.VoiceSearchNotification.access$500(this.this$0).start();
        }
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        this.this$0.imageProcessing.setRotation(0.0f);
        this.canceled = false;
    }
}
