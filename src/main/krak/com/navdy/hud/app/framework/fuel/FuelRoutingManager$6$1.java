package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$6$1 implements com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback {
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager$6 this$1;
    
    FuelRoutingManager$6$1(com.navdy.hud.app.framework.fuel.FuelRoutingManager$6 a) {
        super();
        this.this$1 = a;
    }
    
    public void onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$6$1$2(this, a), 21);
    }
    
    public void onOptimalRouteCalculationComplete(com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem a, java.util.ArrayList a0, com.navdy.service.library.events.navigation.NavigationRouteRequest a1) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$6$1$1(this, a, a0, a1), 21);
    }
}
