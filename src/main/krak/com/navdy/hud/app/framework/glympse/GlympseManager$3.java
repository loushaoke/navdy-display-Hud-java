package com.navdy.hud.app.framework.glympse;

class GlympseManager$3 implements com.glympse.android.api.GEventListener {
    final com.navdy.hud.app.framework.glympse.GlympseManager this$0;
    final com.glympse.android.api.GTicket val$ticket;
    
    GlympseManager$3(com.navdy.hud.app.framework.glympse.GlympseManager a, com.glympse.android.api.GTicket a0) {
        super();
        this.this$0 = a;
        this.val$ticket = a0;
    }
    
    public void eventsOccurred(com.glympse.android.api.GGlympse a, int i, int i0, Object a0) {
        if (com.navdy.hud.app.framework.glympse.GlympseManager.access$600(this.this$0, i0, 8192)) {
            String s = ((com.glympse.android.api.GTicket)a0).getId();
            String s0 = ((com.glympse.android.api.GTicket)a0).getProperty(1L, "navdy_contact_uuid").getString();
            com.navdy.hud.app.framework.glympse.GlympseManager.access$200().v(new StringBuilder().append("ticket added, ticket id is ").append(s).append("; contact uuid is ").append(s0).toString());
            if (!android.text.TextUtils.isEmpty((CharSequence)s0) && com.navdy.hud.app.framework.glympse.GlympseManager.access$900(this.this$0).containsKey(s0)) {
                com.navdy.hud.app.framework.glympse.GlympseManager.access$1000(this.this$0, (com.navdy.hud.app.framework.contacts.Contact)com.navdy.hud.app.framework.glympse.GlympseManager.access$900(this.this$0).get(s0));
            }
        }
        if (com.navdy.hud.app.framework.glympse.GlympseManager.access$600(this.this$0, i0, 16384)) {
            String s1 = ((com.glympse.android.api.GTicket)a0).getId();
            String s2 = ((com.glympse.android.api.GTicket)a0).getProperty(1L, "navdy_contact_uuid").getString();
            com.navdy.hud.app.framework.glympse.GlympseManager.access$200().v(new StringBuilder().append("ticket updated, ticket id is ").append(s1).append("; contact uuid is ").append(s2).toString());
            if (com.navdy.hud.app.framework.glympse.GlympseManager.access$900(this.this$0).containsKey(s2)) {
                com.navdy.hud.app.framework.contacts.Contact a1 = (com.navdy.hud.app.framework.contacts.Contact)com.navdy.hud.app.framework.glympse.GlympseManager.access$900(this.this$0).remove(s2);
                com.navdy.hud.app.framework.glympse.GlympseManager.access$1100(this.this$0, a1);
                this.val$ticket.removeListener((com.glympse.android.api.GEventListener)this);
                if (s2 == null) {
                    s2 = "";
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordGlympseViewed(s2);
                com.navdy.hud.app.framework.glympse.GlympseManager.access$200().v(new StringBuilder().append("Glympse viewed ticket id is ").append(s1).append(" contact[").append(a1.name).append("] number[").append(a1.number).append("] e164[").append(com.navdy.hud.app.util.PhoneUtil.convertToE164Format(a1.number)).append("]").toString());
            }
        }
    }
}
