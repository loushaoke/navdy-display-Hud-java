package com.navdy.hud.app.framework.glance;

public class GlanceViewCache {
    final private static boolean VERBOSE = false;
    private static java.util.ArrayList bigCalendarViewCache;
    private static java.util.ArrayList bigGlanceMessageSingleViewCache;
    private static java.util.ArrayList bigGlanceMessageViewCache;
    private static java.util.ArrayList bigMultiTextViewCache;
    private static java.util.ArrayList bigTextViewCache;
    private static java.util.HashMap cachedViewMap;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static java.util.ArrayList smallGlanceMessageViewCache;
    private static java.util.ArrayList smallImageViewCache;
    private static java.util.ArrayList smallSignViewCache;
    private static java.util.HashSet viewSet;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.glance.GlanceViewCache.class);
        smallGlanceMessageViewCache = new java.util.ArrayList(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_GLANCE_MESSAGE.cacheSize);
        bigGlanceMessageViewCache = new java.util.ArrayList(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_GLANCE_MESSAGE.cacheSize);
        bigGlanceMessageSingleViewCache = new java.util.ArrayList(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_GLANCE_MESSAGE_SINGLE.cacheSize);
        smallImageViewCache = new java.util.ArrayList(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_IMAGE.cacheSize);
        bigTextViewCache = new java.util.ArrayList(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_TEXT.cacheSize);
        bigMultiTextViewCache = new java.util.ArrayList(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_MULTI_TEXT.cacheSize);
        smallSignViewCache = new java.util.ArrayList(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_SIGN.cacheSize);
        bigCalendarViewCache = new java.util.ArrayList(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_CALENDAR.cacheSize);
        cachedViewMap = new java.util.HashMap();
        viewSet = new java.util.HashSet();
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_GLANCE_MESSAGE, smallGlanceMessageViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_GLANCE_MESSAGE, bigGlanceMessageViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_GLANCE_MESSAGE_SINGLE, bigGlanceMessageSingleViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_IMAGE, smallImageViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_TEXT, bigTextViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_MULTI_TEXT, bigMultiTextViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.SMALL_SIGN, smallSignViewCache);
        cachedViewMap.put(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_CALENDAR, bigCalendarViewCache);
    }
    
    public GlanceViewCache() {
    }
    
    public static void clearCache() {
        smallGlanceMessageViewCache.clear();
        bigGlanceMessageViewCache.clear();
        bigGlanceMessageSingleViewCache.clear();
        smallImageViewCache.clear();
        bigTextViewCache.clear();
        bigMultiTextViewCache.clear();
        smallSignViewCache.clear();
        bigCalendarViewCache.clear();
        viewSet.clear();
    }
    
    public static android.view.View getView(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType a, android.content.Context a0) {
        java.util.ArrayList a1 = (java.util.ArrayList)cachedViewMap.get(a);
        int i = a1.size();
        android.view.View a2 = null;
        if (i > 0) {
            a2 = (android.view.View)a1.remove(0);
            viewSet.remove(a2);
            if (a2.getParent() != null) {
                sLogger.e(new StringBuilder().append(":-/ view already has parent:").append(a).toString());
            }
        }
        if (a2 != null) {
            if (sLogger.isLoggable(2)) {
                sLogger.v(new StringBuilder().append(" reusing cache for ").append(a).toString());
            }
        } else {
            a2 = android.view.LayoutInflater.from(a0).inflate(a.layoutId, (android.view.ViewGroup)null);
            if (sLogger.isLoggable(2)) {
                sLogger.v(new StringBuilder().append(" creating view for ").append(a).toString());
            }
        }
        return a2;
    }
    
    public static void putView(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType a, android.view.View a0) {
        if (a0 != null) {
            if (viewSet.contains(a0)) {
                sLogger.e(new StringBuilder().append(":-/ view already in cache:").append(a).toString());
            } else {
                if (a0.getParent() != null) {
                    sLogger.e(new StringBuilder().append(":-/ view already has parent:").append(a).toString());
                }
                a0.setTranslationX(0.0f);
                a0.setTranslationY(0.0f);
                a0.setTag(null);
                java.util.ArrayList a1 = (java.util.ArrayList)cachedViewMap.get(a);
                if (a1.size() < a.cacheSize) {
                    a1.add(a0);
                    viewSet.add(a0);
                }
                if (sLogger.isLoggable(2)) {
                    sLogger.v(new StringBuilder().append(" putView cache for ").append(a).toString());
                }
            }
        }
    }
    
    public static boolean supportScroll(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType a) {
        return com.navdy.hud.app.framework.glance.GlanceViewCache$1.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[a.ordinal()] != 0;
    }
}
