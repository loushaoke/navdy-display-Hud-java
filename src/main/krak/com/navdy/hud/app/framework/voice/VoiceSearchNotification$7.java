package com.navdy.hud.app.framework.voice;

class VoiceSearchNotification$7 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal;
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchError;
    final static int[] $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState;
    final static int[] $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType;
    final static int[] $SwitchMap$com$navdy$service$library$events$input$Gesture;
    
    static {
        $SwitchMap$com$navdy$service$library$events$input$Gesture = new int[com.navdy.service.library.events.input.Gesture.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$input$Gesture;
        com.navdy.service.library.events.input.Gesture a0 = com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$input$Gesture[com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a2 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.SELECT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal = new int[com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal;
        com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal a4 = com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.FAILED;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.IDLE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.RESULTS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.LISTENING.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.STARTING.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$voice$VoiceSearchNotification$VoiceSearchStateInternal[com.navdy.hud.app.framework.voice.VoiceSearchNotification$VoiceSearchStateInternal.SEARCHING.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException9) {
        }
        $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchError = new int[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.values().length];
        int[] a5 = $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchError;
        com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError a6 = com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError.NO_RESULTS_FOUND;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException10) {
        }
        $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState = new int[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.values().length];
        int[] a7 = $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState;
        com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState a8 = com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_STARTING;
        try {
            a7[a8.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_SEARCHING.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException12) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_LISTENING.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_ERROR.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchState.VOICE_SEARCH_SUCCESS.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException15) {
        }
        $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType = new int[com.navdy.service.library.events.destination.Destination$FavoriteType.values().length];
        int[] a9 = $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType;
        com.navdy.service.library.events.destination.Destination$FavoriteType a10 = com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_HOME;
        try {
            a9[a10.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException16) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType[com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_WORK.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException17) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType[com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_CUSTOM.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException18) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$destination$Destination$FavoriteType[com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_NONE.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException19) {
        }
    }
}
