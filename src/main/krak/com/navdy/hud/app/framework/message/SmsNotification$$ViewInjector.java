package com.navdy.hud.app.framework.message;
import com.navdy.hud.app.R;

public class SmsNotification$$ViewInjector {
    public SmsNotification$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.framework.message.SmsNotification a0, Object a1) {
        a0.title = (android.widget.TextView)a.findRequiredView(a1, R.id.title, "field 'title'");
        a0.subtitle = (android.widget.TextView)a.findRequiredView(a1, R.id.subtitle, "field 'subtitle'");
        a0.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)a.findRequiredView(a1, R.id.choice_layout, "field 'choiceLayout'");
        a0.notificationUserImage = (com.navdy.hud.app.ui.component.image.InitialsImageView)a.findRequiredView(a1, R.id.notification_user_image, "field 'notificationUserImage'");
        a0.sideImage = (android.widget.ImageView)a.findRequiredView(a1, R.id.badge, "field 'sideImage'");
    }
    
    public static void reset(com.navdy.hud.app.framework.message.SmsNotification a) {
        a.title = null;
        a.subtitle = null;
        a.choiceLayout = null;
        a.notificationUserImage = null;
        a.sideImage = null;
    }
}
