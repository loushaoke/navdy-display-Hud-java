package com.navdy.hud.app.framework.music;

public class OutlineTextView extends android.widget.TextView {
    final private static com.navdy.service.library.log.Logger sLogger;
    java.lang.reflect.Field currentTextColorField;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.music.OutlineTextView.class);
    }
    
    public OutlineTextView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public OutlineTextView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public OutlineTextView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        try {
            this.currentTextColorField = android.widget.TextView.class.getDeclaredField("mCurTextColor");
            this.currentTextColorField.setAccessible(true);
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
    
    private void setCurrentTextColor(Integer a) {
        try {
            if (this.currentTextColorField != null) {
                this.currentTextColorField.set(this, a);
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    public void onDraw(android.graphics.Canvas a) {
        int i = this.getCurrentTextColor();
        android.text.TextPaint a0 = this.getPaint();
        a0.setStyle(android.graphics.Paint$Style.STROKE);
        a0.setStrokeJoin(android.graphics.Paint$Join.MITER);
        a0.setStrokeMiter(1f);
        this.setCurrentTextColor(Integer.valueOf(-16777216));
        a0.setStrokeWidth(4f);
        super.onDraw(a);
        a0.setStyle(android.graphics.Paint$Style.FILL);
        this.setCurrentTextColor(Integer.valueOf(i));
        super.onDraw(a);
    }
}
