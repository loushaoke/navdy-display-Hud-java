package com.navdy.hud.app.framework.music;

final public class MusicDetailsView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding presenter;
    
    public MusicDetailsView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.framework.music.MusicDetailsView", false, com.navdy.hud.app.framework.music.MusicDetailsView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.presenter = a.requestBinding("com.navdy.hud.app.framework.music.MusicDetailsScreen$Presenter", com.navdy.hud.app.framework.music.MusicDetailsView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.presenter);
    }
    
    public void injectMembers(com.navdy.hud.app.framework.music.MusicDetailsView a) {
        a.presenter = (com.navdy.hud.app.framework.music.MusicDetailsScreen$Presenter)this.presenter.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.framework.music.MusicDetailsView)a);
    }
}
