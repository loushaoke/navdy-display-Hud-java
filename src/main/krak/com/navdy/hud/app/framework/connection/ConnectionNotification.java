package com.navdy.hud.app.framework.connection;
import com.navdy.hud.app.R;

public class ConnectionNotification {
    final private static int CONNECTED_TIMEOUT = 2000;
    final public static String CONNECT_ID = "connection#toast";
    final public static String DISCONNECT_ID = "disconnection#toast";
    final private static String EMPTY = "";
    final private static int NOT_CONNECTED_TIMEOUT = 30000;
    final private static int TAG_CONNECT = 0;
    final private static int TAG_DISMISS = 1;
    final private static int TAG_DONT_LAUNCH = 3;
    final private static int TAG_LAUNCH_APP = 2;
    final private static java.util.ArrayList appClosedChoices;
    final private static java.util.ArrayList appClosedLaunchFailedChoices;
    final private static int appNotConnectedWidth;
    final private static com.squareup.otto.Bus bus;
    final private static String connect;
    final private static String connectedTitle;
    final private static int contentWidth;
    final private static java.util.ArrayList disconnectedChoices;
    final private static String disconnectedTitle;
    final private static String dismiss;
    private static android.os.Handler handler;
    final private static String iphoneAppClosedLaunchFailedMessage;
    final private static String iphoneAppClosedMessage1;
    final private static String iphoneAppClosedMessage2;
    final private static String iphoneAppClosedTitle;
    final private static String launch;
    private static com.squareup.picasso.Transformation roundTransformation;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.connection.ConnectionNotification.class);
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        appClosedChoices = new java.util.ArrayList(2);
        appClosedLaunchFailedChoices = new java.util.ArrayList(1);
        disconnectedChoices = new java.util.ArrayList(2);
        roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        connectedTitle = a.getString(R.string.phone_connected);
        disconnectedTitle = a.getString(R.string.phone_disconnected);
        iphoneAppClosedTitle = a.getString(R.string.app_disconnected);
        iphoneAppClosedMessage1 = a.getString(R.string.app_disconnected_title);
        iphoneAppClosedMessage2 = a.getString(R.string.app_disconnected_msg);
        iphoneAppClosedLaunchFailedMessage = a.getString(R.string.app_disconnected_msg_launch_failed);
        connect = a.getString(R.string.connect);
        dismiss = a.getString(R.string.dismiss);
        launch = a.getString(R.string.launch);
        disconnectedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(connect, 0));
        disconnectedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(dismiss, 1));
        appClosedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(launch, 2));
        appClosedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(dismiss, 3));
        appClosedLaunchFailedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(dismiss, 3));
        contentWidth = (int)a.getDimension(R.dimen.toast_phone_disconnected_width);
        appNotConnectedWidth = (int)a.getDimension(R.dimen.toast_app_disconnected_width);
        bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }
    
    public ConnectionNotification() {
    }
    
    static void access$000(com.navdy.hud.app.ui.component.image.InitialsImageView a, String s, java.io.File a0, com.navdy.hud.app.profile.DriverProfile a1) {
        com.navdy.hud.app.framework.connection.ConnectionNotification.loadDriveImagefromProfile(a, s, a0, a1);
    }
    
    static com.squareup.otto.Bus access$100() {
        return bus;
    }
    
    static com.squareup.picasso.Transformation access$200() {
        return roundTransformation;
    }
    
    static android.os.Handler access$300() {
        return handler;
    }
    
    static com.navdy.service.library.log.Logger access$400() {
        return sLogger;
    }
    
    private static String getName(com.navdy.hud.app.profile.DriverProfile a, com.navdy.service.library.device.NavdyDeviceId a0) {
        String s = a.getDriverName();
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s = a0.getDeviceName();
        }
        if (s == null) {
            s = "";
        }
        return s;
    }
    
    private static void loadDriveImagefromProfile(com.navdy.hud.app.ui.component.image.InitialsImageView a, String s, java.io.File a0, com.navdy.hud.app.profile.DriverProfile a1) {
        android.graphics.Bitmap a2 = com.navdy.hud.app.util.picasso.PicassoUtil.getBitmapfromCache(a0);
        if (a2 == null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.connection.ConnectionNotification$3(a0, a, s, a1), 1);
        } else {
            a.setInitials((String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
            a.setImageBitmap(a2);
        }
    }
    
    public static void showConnectedToast() {
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        com.navdy.service.library.device.NavdyDeviceId a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
        if (a0 != null) {
            java.io.File a1 = a.getDriverImageFile();
            boolean b = com.navdy.hud.app.util.picasso.PicassoUtil.isImageAvailableInCache(a1);
            String s = com.navdy.hud.app.framework.connection.ConnectionNotification.getName(a, a0);
            String s0 = a0.getDeviceName();
            android.os.Bundle a2 = new android.os.Bundle();
            a2.putInt("13", 2000);
            if (b) {
                a2.putString("9", a1.getAbsolutePath());
            } else {
                int i = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getDriverImageResId(a0.getDeviceName());
                String s1 = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(s);
                a2.putInt("8", i);
                a2.putString("10", s1);
            }
            a2.putInt("11", R.drawable.icon_alert_phone_connected);
            a2.putString("1", connectedTitle);
            a2.putInt("1_1", R.style.ToastMainTitle);
            String s2 = a.getDriverName();
            if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
                a2.putString("4", s0);
                a2.putInt("5", R.style.Glances_1);
            } else {
                a2.putString("4", s2);
                a2.putInt("5", R.style.Glances_1);
                a2.putString("6", s0);
                a2.putInt("7", R.style.Toast_1);
            }
            a2.putInt("16", contentWidth);
            com.navdy.hud.app.framework.connection.ConnectionNotification$2 a3 = null;
            if (!b) {
                a3 = new com.navdy.hud.app.framework.connection.ConnectionNotification$2(a1, a);
            }
            com.navdy.hud.app.framework.toast.ToastManager a4 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            a4.dismissCurrentToast("disconnection#toast");
            a4.clearPendingToast("disconnection#toast");
            a4.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("connection#toast", a2, (com.navdy.hud.app.framework.toast.IToastCallback)a3, true, true));
        }
    }
    
    public static void showDisconnectedToast(boolean b) {
        com.navdy.service.library.device.NavdyDeviceId a = null;
        boolean b0 = false;
        boolean b1 = false;
        com.navdy.hud.app.manager.RemoteDeviceManager a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        com.navdy.hud.app.service.ConnectionHandler a1 = a0.getConnectionHandler();
        com.navdy.service.library.events.DeviceInfo a2 = a0.getRemoteDeviceInfo();
        android.os.Bundle a3 = new android.os.Bundle();
        a3.putInt("13", 30000);
        a3.putInt("11", R.drawable.icon_type_phone_disconnected);
        a3.putInt("1_1", R.style.ToastMainTitle);
        a3.putBoolean("19", true);
        label0: {
            label1: {
                label2: {
                    if (b) {
                        break label2;
                    }
                    if (a2 == null) {
                        break label1;
                    }
                    if (!a1.isAppClosed()) {
                        break label1;
                    }
                }
                a3.putString("1", iphoneAppClosedTitle);
                a3.putInt("8", R.drawable.icon_navdyapp);
                a3.putInt("16", appNotConnectedWidth);
                if (a1.isAppLaunchAttempted()) {
                    sLogger.d("App launch has already been attempted but app is not launched yet");
                    a3.putParcelableArrayList("20", appClosedLaunchFailedChoices);
                    a3.putString("6", iphoneAppClosedLaunchFailedMessage);
                    a3.putInt("7", R.style.Toast_3);
                    a = null;
                    b0 = false;
                    break label0;
                } else {
                    sLogger.d("App launch is not attempted yet so showing the app launch prompt");
                    a3.putParcelableArrayList("20", appClosedChoices);
                    a3.putString("6", iphoneAppClosedMessage1);
                    a3.putInt("7", R.style.Toast_3);
                    a = null;
                    b0 = false;
                    break label0;
                }
            }
            com.navdy.service.library.events.DeviceInfo a4 = a1.getLastConnectedDeviceInfo();
            if (a4 != null) {
                sLogger.v("last connected device found");
                a = new com.navdy.service.library.device.NavdyDeviceId(a4.deviceId);
            } else {
                sLogger.v("last connected device not found");
                com.navdy.service.library.device.connection.ConnectionInfo a5 = com.navdy.service.library.device.RemoteDeviceRegistry.getInstance(com.navdy.hud.app.HudApplication.getAppContext()).getLastPairedDevice();
                if (a5 != null) {
                    a = a5.getDeviceId();
                    sLogger.v(new StringBuilder().append("last paired device found:").append(a).toString());
                } else {
                    sLogger.v("no last paired device found");
                    a3.putInt("8", R.drawable.icon_user_numberonly);
                    a = null;
                }
            }
            a3.putString("1", disconnectedTitle);
            a3.putString("4", disconnectedTitle);
            a3.putParcelableArrayList("20", disconnectedChoices);
            a3.putInt("16", contentWidth);
            b0 = true;
        }
        com.navdy.hud.app.profile.DriverProfile a6 = null;
        java.io.File a7 = null;
        if (a == null) {
            b1 = false;
        } else {
            String s = null;
            a6 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getProfileForId(a);
            String s0 = a.getDeviceName();
            if (a6 != null) {
                s = a6.getDriverName();
                sLogger.v(new StringBuilder().append("driver profile found:").append(a).toString());
                a7 = a6.getDriverImageFile();
                boolean b2 = com.navdy.hud.app.util.picasso.PicassoUtil.isImageAvailableInCache(a7);
                String s1 = com.navdy.hud.app.framework.connection.ConnectionNotification.getName(a6, a);
                if (b2) {
                    a3.putString("9", a7.getAbsolutePath());
                    b1 = false;
                    a7 = null;
                } else {
                    int i = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getDriverImageResId(s0);
                    String s2 = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(s1);
                    a3.putInt("8", i);
                    a3.putString("10", s2);
                    b1 = true;
                }
            } else {
                sLogger.v(new StringBuilder().append("driver profile not found:").append(a).toString());
                a3.putInt("8", R.drawable.icon_user_numberonly);
                b1 = false;
                a7 = null;
                s = null;
            }
            if (b0) {
                if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                    a3.putString("4", s0);
                    a3.putInt("5", R.style.Glances_1);
                } else {
                    a3.putString("4", s);
                    a3.putInt("5", R.style.Glances_1);
                    a3.putString("6", s0);
                    a3.putInt("7", R.style.Toast_1);
                }
            }
        }
        com.navdy.hud.app.framework.connection.ConnectionNotification$1 a8 = new com.navdy.hud.app.framework.connection.ConnectionNotification$1(b1, a7, a6);
        com.navdy.hud.app.framework.toast.ToastManager a9 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a9.dismissCurrentToast(a9.getCurrentToastId());
        a9.clearAllPendingToast();
        a9.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("disconnection#toast", a3, (com.navdy.hud.app.framework.toast.IToastCallback)a8, true, true));
    }
}
