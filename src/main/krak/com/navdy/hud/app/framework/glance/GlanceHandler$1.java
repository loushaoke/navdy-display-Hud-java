package com.navdy.hud.app.framework.glance;

class GlanceHandler$1 implements Runnable {
    final com.navdy.hud.app.framework.glance.GlanceHandler this$0;
    final com.navdy.service.library.events.glances.GlanceEvent val$event;
    
    GlanceHandler$1(com.navdy.hud.app.framework.glance.GlanceHandler a, com.navdy.service.library.events.glances.GlanceEvent a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.framework.glance.GlanceHandler.access$000(this.this$0, this.val$event);
        } catch(Throwable a) {
            com.navdy.hud.app.framework.glance.GlanceHandler.sLogger.e("handleGlancEventInternal", a);
        }
    }
}
