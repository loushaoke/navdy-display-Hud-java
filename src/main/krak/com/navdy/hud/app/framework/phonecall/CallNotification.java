package com.navdy.hud.app.framework.phonecall;
import com.navdy.hud.app.R;

public class CallNotification implements com.navdy.hud.app.framework.notifications.INotification, com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback {
    final public static String CALL_NOTIFICATION_TOAST_ID = "incomingcall#toast";
    final private static String EMPTY = "";
    final private static int END_NOTIFICATION_TIMEOUT = 3000;
    final private static int FAILED_NOTIFICATION_TIMEOUT = 10000;
    final private static float IMAGE_SCALE = 0.5f;
    final private static int IN_CALL_NOTIFICATION_TIMEOUT = 30000;
    final private static int MISSED_NOTIFICATION_TIMEOUT = 10000;
    final private static int TAG_ACCEPT = 101;
    final private static int TAG_CANCEL = 105;
    final private static int TAG_DISMISS = 107;
    final private static int TAG_END = 106;
    final private static int TAG_IGNORE = 102;
    final private static int TAG_MUTE = 103;
    final private static int TAG_UNMUTE = 104;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private String activeCall;
    private java.util.ArrayList activeCallChoices;
    private java.util.ArrayList activeCallMutedChoices;
    private java.util.ArrayList activeCallNoMuteChoices;
    private com.squareup.otto.Bus bus;
    final private String callAccept;
    final private String callCancel;
    final private String callDialing;
    final private String callDismiss;
    final private String callEnd;
    final private String callEnded;
    final private String callFailed;
    final private String callIgnore;
    private com.navdy.hud.app.framework.phonecall.CallManager callManager;
    final private String callMissed;
    final private String callMute;
    final private String callMuted;
    private android.widget.ImageView callStatusImage;
    final private String callUnmute;
    private com.navdy.hud.app.ui.component.image.InitialsImageView callerImage;
    private android.widget.TextView callerName;
    private android.widget.TextView callerStatus;
    final private String callerUnknown;
    private com.navdy.hud.app.ui.component.ChoiceLayout2 choiceLayout;
    private com.navdy.hud.app.ui.component.ChoiceLayout2$IListener choiceListener;
    private android.view.ViewGroup container;
    private int contentWidth;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private java.util.ArrayList dialingCallChoices;
    final private int dismisColor;
    private Runnable durationRunnable;
    final private int endColor;
    private java.util.ArrayList endedCallChoices;
    private android.view.ViewGroup extendedContainer;
    private android.widget.TextView extendedTitle;
    private java.util.ArrayList failedCallChoices;
    private android.os.Handler handler;
    final private String incomingCall;
    private java.util.ArrayList incomingCallChoices;
    private java.util.ArrayList missedCallChoices;
    final private int muteColor;
    private android.content.res.Resources resources;
    private com.squareup.picasso.Transformation roundTransformation;
    final private int unmuteColor;
    final private int unselectedColor;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.phonecall.CallNotification.class);
    }
    
    CallNotification(com.navdy.hud.app.framework.phonecall.CallManager a, com.squareup.otto.Bus a0) {
        this.incomingCallChoices = new java.util.ArrayList(2);
        this.activeCallChoices = new java.util.ArrayList(2);
        this.activeCallNoMuteChoices = new java.util.ArrayList(1);
        this.activeCallMutedChoices = new java.util.ArrayList(2);
        this.dialingCallChoices = new java.util.ArrayList(1);
        this.failedCallChoices = new java.util.ArrayList(1);
        this.endedCallChoices = new java.util.ArrayList(1);
        this.missedCallChoices = new java.util.ArrayList(1);
        this.roundTransformation = new com.makeramen.RoundedTransformationBuilder().oval(true).build();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.durationRunnable = (Runnable)new com.navdy.hud.app.framework.phonecall.CallNotification$1(this);
        this.choiceListener = (com.navdy.hud.app.ui.component.ChoiceLayout2$IListener)new com.navdy.hud.app.framework.phonecall.CallNotification$2(this);
        this.callManager = a;
        this.bus = a0;
        this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.callEnded = this.resources.getString(R.string.call_ended);
        this.callDialing = this.resources.getString(R.string.call_dialing);
        this.callMuted = this.resources.getString(R.string.call_muted);
        this.callerUnknown = this.resources.getString(R.string.caller_unknown);
        this.callAccept = this.resources.getString(R.string.call_accept);
        this.callIgnore = this.resources.getString(R.string.call_ignore);
        this.callMute = this.resources.getString(R.string.call_mute);
        this.callEnd = this.resources.getString(R.string.call_end);
        this.callUnmute = this.resources.getString(R.string.call_unmute);
        this.callCancel = this.resources.getString(R.string.call_cancel);
        this.callDismiss = this.resources.getString(R.string.call_dismiss);
        this.callFailed = this.resources.getString(R.string.call_failed);
        this.callMissed = this.resources.getString(R.string.call_missed);
        this.incomingCall = this.resources.getString(R.string.incoming_call);
        this.activeCall = this.resources.getString(R.string.phone_call_duration);
        this.muteColor = this.resources.getColor(R.color.call_mute);
        this.unmuteColor = this.resources.getColor(R.color.call_unmute);
        this.endColor = this.resources.getColor(R.color.call_end);
        this.dismisColor = this.resources.getColor(R.color.glance_dismiss);
        this.unselectedColor = -16777216;
        this.contentWidth = (int)this.resources.getDimension(R.dimen.toast_app_disconnected_width);
        this.incomingCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.callAccept, 101));
        this.incomingCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(this.callIgnore, 102));
        this.activeCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(103, R.drawable.icon_glances_mute, this.muteColor, R.drawable.icon_glances_mute, this.unselectedColor, this.callMute, this.muteColor));
        this.activeCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(106, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.activeCallNoMuteChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(106, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.activeCallMutedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(104, R.drawable.icon_glances_unmute, this.unmuteColor, R.drawable.icon_glances_unmute, this.unselectedColor, this.callUnmute, this.unmuteColor));
        this.activeCallMutedChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(106, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.dialingCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(105, R.drawable.icon_glances_endcall, this.endColor, R.drawable.icon_glances_endcall, this.unselectedColor, this.callEnd, this.endColor));
        this.failedCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(107, R.drawable.icon_glances_dismiss, this.dismisColor, R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
        this.endedCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(107, R.drawable.icon_glances_dismiss, this.dismisColor, R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
        this.missedCallChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2$Choice(107, R.drawable.icon_glances_dismiss, this.dismisColor, R.drawable.icon_glances_dismiss, this.unselectedColor, this.callDismiss, this.dismisColor));
    }
    
    static android.widget.TextView access$000(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.extendedTitle;
    }
    
    static void access$100(com.navdy.hud.app.framework.phonecall.CallNotification a, android.widget.TextView a0) {
        a.setCallDuration(a0);
    }
    
    static com.navdy.hud.app.ui.component.ChoiceLayout2$IListener access$1000(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.choiceListener;
    }
    
    static com.navdy.hud.app.ui.component.ChoiceLayout2 access$1100(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.choiceLayout;
    }
    
    static java.util.ArrayList access$1200(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.activeCallChoices;
    }
    
    static com.navdy.hud.app.framework.notifications.INotificationController access$1300(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.controller;
    }
    
    static com.squareup.otto.Bus access$1400(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.bus;
    }
    
    static com.squareup.picasso.Transformation access$1500(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.roundTransformation;
    }
    
    static Runnable access$200(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.durationRunnable;
    }
    
    static android.os.Handler access$300(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.handler;
    }
    
    static com.navdy.hud.app.framework.phonecall.CallManager access$400(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.callManager;
    }
    
    static void access$500(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        a.dismissNotification();
    }
    
    static String access$600(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.callMuted;
    }
    
    static android.widget.TextView access$700(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.callerStatus;
    }
    
    static android.widget.ImageView access$800(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.callStatusImage;
    }
    
    static java.util.ArrayList access$900(com.navdy.hud.app.framework.phonecall.CallNotification a) {
        return a.activeCallMutedChoices;
    }
    
    private boolean checkIfCollapseRequired() {
        boolean b = false;
        if (this.isAlive()) {
            b = false;
        } else if (this.controller == null) {
            b = false;
        } else if (this.controller.isExpandedWithStack()) {
            sLogger.v("collapse required");
            this.controller.collapseNotification(false, false);
            b = true;
        } else {
            b = false;
        }
        return b;
    }
    
    public static void clearToast() {
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a.dismissCurrentToast("incomingcall#toast");
        a.clearAllPendingToast();
        a.disableToasts(false);
    }
    
    private void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#phone#call#notif");
        this.callManager.state = com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState.IDLE;
    }
    
    private String getCallerNumber() {
        return this.callManager.number;
    }
    
    private int getTimeout(com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a) {
        int i = 0;
        if (a != null) {
            switch(com.navdy.hud.app.framework.phonecall.CallNotification$4.$SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[a.ordinal()]) {
                case 7: case 9: {
                    i = 30000;
                    break;
                }
                case 4: {
                    i = 10000;
                    break;
                }
                case 3: {
                    i = 3000;
                    break;
                }
                case 2: {
                    i = 10000;
                    break;
                }
                default: {
                    i = 0;
                }
            }
        } else {
            i = 0;
        }
        return i;
    }
    
    private boolean isTimeoutRequired(com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a) {
        boolean b = false;
        if (a != null) {
            switch(com.navdy.hud.app.framework.phonecall.CallNotification$4.$SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[a.ordinal()]) {
                case 7: {
                    com.navdy.hud.app.framework.notifications.INotificationController a0 = this.controller;
                    label0: {
                        if (a0 == null) {
                            break label0;
                        }
                        if (this.controller.isExpandedWithStack()) {
                            b = false;
                            break;
                        }
                    }
                    b = true;
                    break;
                }
                case 2: case 3: case 4: case 9: {
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    private void setCallDuration(android.widget.TextView a) {
        long j = this.callManager.callStartMs;
        if (j > 0L) {
            long j0 = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(android.os.SystemClock.elapsedRealtime() - j);
            if (j0 >= 1L) {
                android.content.res.Resources a0 = this.resources;
                Object[] a1 = new Object[1];
                a1[0] = Long.valueOf(j0);
                a.setText((CharSequence)a0.getString(R.string.phone_call_duration_min, a1));
            } else {
                a.setText((CharSequence)this.activeCall);
            }
        } else {
            a.setText((CharSequence)this.activeCall);
        }
    }
    
    private boolean setCaller() {
        boolean b = false;
        String s = this.getCaller();
        if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(s)) {
            this.callerName.setText((CharSequence)com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(s));
            b = true;
        } else {
            this.callerName.setText((CharSequence)s);
            b = false;
        }
        return b;
    }
    
    private void switchToExpandedMode() {
        if (this.controller != null) {
            this.controller.stopTimeout(true);
            this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
            this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2);
            if (!this.controller.isExpandedWithStack()) {
                this.controller.expandNotification(true);
            }
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public boolean expandNotification() {
        boolean b = false;
        label0: if (this.controller != null) {
            Object a = this.choiceLayout.getTag();
            java.util.ArrayList a0 = this.activeCallChoices;
            label1: {
                if (a == a0) {
                    break label1;
                }
                if (a == this.activeCallMutedChoices) {
                    break label1;
                }
                if (a != this.activeCallNoMuteChoices) {
                    b = false;
                    break label0;
                }
            }
            this.switchToExpandedMode();
            b = true;
        } else {
            b = false;
        }
        return b;
    }
    
    public String getCaller() {
        return (android.text.TextUtils.isEmpty((CharSequence)this.callManager.contact)) ? (android.text.TextUtils.isEmpty((CharSequence)this.callManager.number)) ? this.callerUnknown : this.callManager.number : this.callManager.contact;
    }
    
    public int getColor() {
        return com.navdy.hud.app.framework.glance.GlanceConstants.colorPhoneCall;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        this.extendedContainer = (android.view.ViewGroup)com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_MULTI_TEXT, a);
        this.extendedTitle = (android.widget.TextView)this.extendedContainer.findViewById(R.id.title1);
        this.extendedTitle.setTextAppearance(a, R.style.glance_title_1);
        ((android.widget.TextView)this.extendedContainer.findViewById(R.id.title2)).setVisibility(8);
        this.setCallDuration(this.extendedTitle);
        this.extendedTitle.setVisibility(0);
        this.handler.removeCallbacks(this.durationRunnable);
        this.handler.postDelayed(this.durationRunnable, java.util.concurrent.TimeUnit.MINUTES.toMillis(1L));
        return this.extendedContainer;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#phone#call#notif";
    }
    
    public int getTimeout() {
        return 0;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.PHONE_CALL;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_phonecall, (android.view.ViewGroup)null);
            this.callerName = (android.widget.TextView)this.container.findViewById(R.id.callerName);
            this.callerStatus = (android.widget.TextView)this.container.findViewById(R.id.callerStatus);
            this.callerImage = (com.navdy.hud.app.ui.component.image.InitialsImageView)this.container.findViewById(R.id.callerImage);
            this.callStatusImage = (android.widget.ImageView)this.container.findViewById(R.id.callStatusImage);
            this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        android.animation.AnimatorSet a = new android.animation.AnimatorSet();
        if (b) {
            android.animation.Animator[] a0 = new android.animation.Animator[3];
            android.widget.TextView a1 = this.callerName;
            android.util.Property a2 = android.view.View.ALPHA;
            float[] a3 = new float[2];
            a3[0] = 0.0f;
            a3[1] = 1f;
            a0[0] = android.animation.ObjectAnimator.ofFloat(a1, a2, a3);
            android.widget.TextView a4 = this.callerStatus;
            android.util.Property a5 = android.view.View.ALPHA;
            float[] a6 = new float[2];
            a6[0] = 0.0f;
            a6[1] = 1f;
            a0[1] = android.animation.ObjectAnimator.ofFloat(a4, a5, a6);
            com.navdy.hud.app.ui.component.ChoiceLayout2 a7 = this.choiceLayout;
            android.util.Property a8 = android.view.View.ALPHA;
            float[] a9 = new float[2];
            a9[0] = 0.0f;
            a9[1] = 1f;
            a0[2] = android.animation.ObjectAnimator.ofFloat(a7, a8, a9);
            a.playTogether(a0);
        } else {
            android.animation.Animator[] a10 = new android.animation.Animator[3];
            android.widget.TextView a11 = this.callerName;
            android.util.Property a12 = android.view.View.ALPHA;
            float[] a13 = new float[2];
            a13[0] = 1f;
            a13[1] = 0.0f;
            a10[0] = android.animation.ObjectAnimator.ofFloat(a11, a12, a13);
            android.widget.TextView a14 = this.callerStatus;
            android.util.Property a15 = android.view.View.ALPHA;
            float[] a16 = new float[2];
            a16[0] = 1f;
            a16[1] = 0.0f;
            a10[1] = android.animation.ObjectAnimator.ofFloat(a14, a15, a16);
            com.navdy.hud.app.ui.component.ChoiceLayout2 a17 = this.choiceLayout;
            android.util.Property a18 = android.view.View.ALPHA;
            float[] a19 = new float[2];
            a19[0] = 1f;
            a19[1] = 0.0f;
            a10[2] = android.animation.ObjectAnimator.ofFloat(a17, a18, a19);
            a.playTogether(a10);
        }
        return a;
    }
    
    public boolean isAlive() {
        boolean b = false;
        switch(com.navdy.hud.app.framework.phonecall.CallNotification$4.$SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[this.callManager.state.ordinal()]) {
            case 7: case 8: case 9: {
                b = true;
                break;
            }
            default: {
                b = false;
                break;
            }
            case 1: case 2: case 3: case 4: case 5: case 6: {
                b = false;
            }
        }
        return b;
    }
    
    public boolean isContextValid() {
        return this.controller != null;
    }
    
    public boolean isPurgeable() {
        return !this.isAlive();
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
        if (a == com.navdy.hud.app.ui.framework.UIStateManager$Mode.COLLAPSE && this.controller != null) {
            this.updateState();
        }
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        boolean b0 = false;
        switch(com.navdy.hud.app.framework.phonecall.CallNotification$4.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 3: {
                this.choiceLayout.executeSelectedItem();
                b = false;
                b0 = true;
                break;
            }
            case 2: {
                b = this.choiceLayout.moveSelectionRight();
                b0 = true;
                break;
            }
            case 1: {
                b = this.choiceLayout.moveSelectionLeft();
                b0 = true;
                break;
            }
            default: {
                b = false;
                b0 = false;
            }
        }
        if (this.controller != null && b && this.isTimeoutRequired(this.callManager.state) && this.getTimeout(this.callManager.state) > 0) {
            this.controller.resetTimeout();
        }
        return b0;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
        if (a == com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND) {
            com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a0 = this.callManager.state;
            if (this.isTimeoutRequired(a0)) {
                if (this.controller != null) {
                    sLogger.v(new StringBuilder().append("set timeout:").append(a0).toString());
                    this.controller.stopTimeout(true);
                    this.controller.startTimeout(this.getTimeout(a0));
                }
            } else if (this.controller != null) {
                sLogger.v(new StringBuilder().append("reset timeout:").append(a0).toString());
                this.controller.stopTimeout(true);
            }
        }
    }
    
    public void onPhotoDownload(com.navdy.hud.app.framework.contacts.PhoneImageDownloader$PhotoDownloadStatus a) {
        if (this.controller != null && !a.alreadyDownloaded && a.photoType == com.navdy.service.library.events.photo.PhotoType.PHOTO_CONTACT && this.callManager.phoneStatus != com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE && android.text.TextUtils.equals((CharSequence)this.callManager.number, (CharSequence)a.sourceIdentifier)) {
            com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
        }
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        this.controller = a;
        this.bus.register(this);
        this.updateState();
        if (!a.isExpandedWithStack()) {
            this.container.setTranslationX(0.0f);
            this.container.setTranslationY(0.0f);
            this.container.setAlpha(1f);
            this.callerName.setAlpha(1f);
            this.callerStatus.setAlpha(1f);
            this.choiceLayout.setAlpha(1f);
        }
    }
    
    public void onStop() {
        this.handler.removeCallbacks(this.durationRunnable);
        this.bus.unregister(this);
        this.controller = null;
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
        if (this.extendedContainer != null) {
            android.view.ViewGroup a = (android.view.ViewGroup)this.extendedContainer.getParent();
            if (a != null) {
                a.removeView((android.view.View)this.extendedContainer);
            }
            com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache$ViewType.BIG_MULTI_TEXT, (android.view.View)this.extendedContainer);
            this.extendedContainer = null;
        }
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
        this.updateState();
    }
    
    public void showIncomingCallToast() {
        String s = this.getCaller();
        String s0 = this.getCallerNumber();
        if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
            s0 = null;
        }
        android.os.Bundle a = new android.os.Bundle();
        a.putString("1", this.incomingCall);
        a.putInt("1_1", R.style.ToastMainTitle);
        a.putInt("11", R.drawable.icon_call_green);
        a.putParcelableArrayList("20", this.incomingCallChoices);
        a.putBoolean("19", true);
        a.putInt("16", this.contentWidth);
        label1: {
            label0: {
                if (s0 == null) {
                    break label0;
                }
                if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)s0)) {
                    break label0;
                }
                a.putString("4", s);
                a.putInt("5", R.style.Glances_1);
                a.putString("6", com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(s0));
                a.putInt("7", R.style.Toast_1);
                break label1;
            }
            if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(s)) {
                a.putString("4", com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(s0));
            } else {
                a.putString("4", s);
            }
            a.putInt("5", R.style.Glances_1);
        }
        com.navdy.hud.app.framework.phonecall.CallNotification$3 a0 = new com.navdy.hud.app.framework.phonecall.CallNotification$3(this, s, s0);
        com.navdy.hud.app.framework.toast.ToastManager a1 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        com.navdy.hud.app.framework.phonecall.CallNotification.clearToast();
        a1.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("incomingcall#toast", a, (com.navdy.hud.app.framework.toast.IToastCallback)a0, false, true, true));
    }
    
    public boolean supportScroll() {
        return false;
    }
    
    void updateState() {
        com.navdy.hud.app.framework.phonecall.CallManager$CallNotificationState a = this.callManager.state;
        sLogger.v(new StringBuilder().append("call state:").append(a).toString());
        if (!this.checkIfCollapseRequired()) {
            switch(com.navdy.hud.app.framework.phonecall.CallNotification$4.$SwitchMap$com$navdy$hud$app$framework$phonecall$CallManager$CallNotificationState[a.ordinal()]) {
                case 9: {
                    this.setCaller();
                    this.callerStatus.setText((CharSequence)this.callDialing);
                    this.callStatusImage.setVisibility(4);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, true, this.callerImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
                    this.choiceLayout.setChoices((java.util.List)this.dialingCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.dialingCallChoices);
                    this.choiceLayout.setVisibility(0);
                    break;
                }
                case 7: {
                    if (this.setCaller()) {
                        this.callerStatus.setText((CharSequence)"");
                    } else {
                        String s = this.getCallerNumber();
                        if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(s)) {
                            this.callerStatus.setText((CharSequence)com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(s));
                        }
                    }
                    this.callStatusImage.setImageResource(R.drawable.icon_badge_call);
                    this.callStatusImage.setVisibility(0);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
                    label2: if (this.controller.isExpandedWithStack()) {
                        this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                        this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2);
                    } else {
                        com.navdy.service.library.events.DeviceInfo a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
                        label0: {
                            label1: {
                                if (a0 == null) {
                                    break label1;
                                }
                                if (a0.platform != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android) {
                                    break label0;
                                }
                            }
                            this.choiceLayout.setChoices((java.util.List)this.activeCallNoMuteChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallNoMuteChoices);
                            break label2;
                        }
                        if (this.callManager.callMuted) {
                            this.choiceLayout.setChoices((java.util.List)this.activeCallMutedChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallMutedChoices);
                        } else {
                            this.choiceLayout.setChoices((java.util.List)this.activeCallChoices, 0, this.choiceListener, 0.5f);
                            this.choiceLayout.setTag(this.activeCallChoices);
                        }
                    }
                    this.choiceLayout.setVisibility(0);
                    break;
                }
                case 6: {
                    this.dismissNotification();
                    break;
                }
                case 5: {
                    this.dismissNotification();
                    break;
                }
                case 4: {
                    this.setCaller();
                    this.callerStatus.setText((CharSequence)this.callFailed);
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices((java.util.List)this.failedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.failedCallChoices);
                    this.callStatusImage.setImageResource(R.drawable.icon_call_red);
                    this.callStatusImage.setVisibility(0);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
                    break;
                }
                case 3: {
                    this.setCaller();
                    this.callerStatus.setText((CharSequence)this.callEnded);
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices((java.util.List)this.endedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.endedCallChoices);
                    this.callStatusImage.setImageResource(R.drawable.icon_badge_callend);
                    this.callStatusImage.setVisibility(0);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
                    break;
                }
                case 2: {
                    this.setCaller();
                    this.callerStatus.setText((CharSequence)this.callMissed);
                    this.choiceLayout.setVisibility(0);
                    this.choiceLayout.setChoices((java.util.List)this.missedCallChoices, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(this.missedCallChoices);
                    this.callStatusImage.setImageResource(R.drawable.icon_badge_callend);
                    this.callStatusImage.setVisibility(0);
                    com.navdy.hud.app.framework.contacts.ContactUtil.setContactPhoto(this.callManager.contact, this.callManager.number, false, this.callerImage, this.roundTransformation, (com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback)this);
                    break;
                }
                case 1: {
                    if (!com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isCurrentNotificationId("navdy#phone#call#notif")) {
                        break;
                    }
                    this.dismissNotification();
                    break;
                }
            }
            if (this.isTimeoutRequired(a)) {
                if (this.controller != null) {
                    sLogger.v(new StringBuilder().append("set timeout:").append(a).toString());
                    this.controller.stopTimeout(true);
                    this.controller.startTimeout(this.getTimeout(a));
                }
            } else if (this.controller != null) {
                sLogger.v(new StringBuilder().append("reset timeout:").append(a).toString());
                this.controller.stopTimeout(true);
            }
        }
    }
}
