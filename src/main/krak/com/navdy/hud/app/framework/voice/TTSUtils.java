package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;

public class TTSUtils {
    final public static String DEBUG_GPS_CALIBRATION_IMU_DONE = "Ublox IMU Calibration Done";
    final public static String DEBUG_GPS_CALIBRATION_SENSOR_DONE = "Ublox Sensor Calibration Done";
    final public static String DEBUG_GPS_CALIBRATION_SENSOR_NOT_NEEDED = "Ublox Sensor Calibration Not Needed";
    final public static String DEBUG_GPS_CALIBRATION_STARTED = "Ublox Calibration started";
    final private static String DEBUG_GPS_SWITCH_TOAST_ID = "debug-tts-gps-switch";
    final public static String DEBUG_TTS_FASTER_ROUTE_AVAILABLE = "Faster route is available";
    final public static String DEBUG_TTS_TRAFFIC_NOT_USED_IN_ROUTE_CALC = "Traffic Not used for Route Calculation";
    final public static String DEBUG_TTS_UBLOX_DR_ENDED = "Dead Reckoning has Stopped";
    final public static String DEBUG_TTS_UBLOX_DR_STARTED = "Dead Reckoning has Started";
    final public static String DEBUG_TTS_UBLOX_HAS_LOCATION_FIX = "Ublox Has Location Fix";
    final public static String DEBUG_TTS_USING_GPS_SPEED = "Using GPS RawSpeed";
    final public static String DEBUG_TTS_USING_OBD_SPEED = "Using OBD RawSpeed";
    final public static String TTS_AUTOMATIC_ZOOM;
    final public static String TTS_DIAL_BATTERY_EXTREMELY_LOW;
    final public static String TTS_DIAL_BATTERY_LOW;
    final public static String TTS_DIAL_BATTERY_VERY_LOW;
    final public static String TTS_DIAL_CONNECTED;
    final public static String TTS_DIAL_DISCONNECTED;
    final public static String TTS_DIAL_FORGOTTEN;
    final public static String TTS_FUEL_GAUGE_ADDED;
    final public static String TTS_FUEL_RANGE_GAUGE_ADDED;
    final public static String TTS_GLANCES_DISABLED;
    final public static String TTS_HEADING_GAUGE_ADDED;
    final public static String TTS_MANUAL_ZOOM;
    final public static String TTS_NAV_STOPPED;
    final public static String TTS_PHONE_BATTERY_LOW;
    final public static String TTS_PHONE_BATTERY_VERY_LOW;
    final public static String TTS_PHONE_OFFLINE;
    final public static String TTS_RPM_GAUGE_ADDED;
    final public static String TTS_TBT_DISABLED;
    final public static String TTS_TBT_ENABLED;
    final public static String TTS_TRAFFIC_DISABLED;
    final public static String TTS_TRAFFIC_ENABLED;
    final private static com.squareup.otto.Bus bus;
    final private static boolean isDebugTTSEnabled;
    
    static {
        bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        TTS_GLANCES_DISABLED = a.getString(R.string.glances_disabled);
        TTS_DIAL_DISCONNECTED = a.getString(R.string.tts_dial_disconnected);
        TTS_DIAL_CONNECTED = a.getString(R.string.tts_dial_connected);
        TTS_DIAL_FORGOTTEN = a.getString(R.string.tts_dial_forgotten);
        TTS_TRAFFIC_DISABLED = a.getString(R.string.tts_traffic_disabled);
        TTS_TRAFFIC_ENABLED = a.getString(R.string.tts_traffic_enabled);
        TTS_NAV_STOPPED = a.getString(R.string.tts_nav_stopped);
        TTS_TBT_DISABLED = a.getString(R.string.tts_tbt_disabled);
        TTS_TBT_ENABLED = a.getString(R.string.tts_tbt_enabled);
        TTS_FUEL_RANGE_GAUGE_ADDED = a.getString(R.string.tts_fuel_range_gauge_added);
        TTS_FUEL_GAUGE_ADDED = a.getString(R.string.tts_fuel_gauge_added);
        TTS_RPM_GAUGE_ADDED = a.getString(R.string.tts_rpm_gauge_added);
        TTS_HEADING_GAUGE_ADDED = a.getString(R.string.tts_heading_gauge_added);
        TTS_DIAL_BATTERY_VERY_LOW = a.getString(R.string.tts_dial_battery_very_low);
        TTS_DIAL_BATTERY_LOW = a.getString(R.string.tts_dial_battery_low);
        TTS_DIAL_BATTERY_EXTREMELY_LOW = a.getString(R.string.tts_dial_battery_extremely_low);
        TTS_PHONE_BATTERY_VERY_LOW = a.getString(R.string.tts_phone_battery_very_low);
        TTS_PHONE_BATTERY_LOW = a.getString(R.string.tts_phone_battery_low);
        TTS_PHONE_OFFLINE = a.getString(R.string.phone_no_network);
        TTS_MANUAL_ZOOM = a.getString(R.string.tts_manual_zoom);
        TTS_AUTOMATIC_ZOOM = a.getString(R.string.tts_auto_zoon);
        isDebugTTSEnabled = new java.io.File("/sdcard/debug_tts").exists();
    }
    
    public TTSUtils() {
    }
    
    public static void debugShowDREnded() {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_gpserror);
            a.putString("4", "Dead Reckoning has Stopped");
            a.putInt("5", R.style.Glances_1);
            a.putString("17", "Dead Reckoning has Stopped");
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-tts-ublox-dr-end", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowDRStarted(String s) {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_gpserror);
            a.putString("2", s);
            a.putInt("3", R.style.Glances_1);
            a.putString("4", "Dead Reckoning has Started");
            a.putInt("5", R.style.Glances_1);
            a.putString("17", "Dead Reckoning has Started");
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-tts-ublox-dr-start", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowFasterRouteToast(String s, String s0) {
        if (isDebugTTSEnabled) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 7000);
            a.putInt("8", R.drawable.icon_open_nav_disable_traffic);
            a.putString("4", s);
            a.putInt("5", R.style.Glances_1);
            a.putString("6", s0);
            a.putInt("7", R.style.Glances_2);
            a.putString("17", "Faster route is available");
            a.putBoolean("12", true);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-tts-faster-route", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowGotUbloxFix() {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_gpserror);
            a.putString("4", "Ublox Has Location Fix");
            a.putInt("5", R.style.Glances_1);
            a.putString("17", "Ublox Has Location Fix");
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-tts-ublox-fix", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowGpsCalibrationStarted() {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_gpserror);
            a.putString("2", "Ublox Calibration started");
            a.putInt("3", R.style.Glances_1);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-gps-calibration_start", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowGpsImuCalibrationDone() {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_gpserror);
            a.putString("2", "Ublox IMU Calibration Done");
            a.putInt("3", R.style.Glances_1);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-gps-calibration_imu", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowGpsReset(String s) {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_gpserror);
            a.putString("4", s);
            a.putInt("5", R.style.Glances_1);
            a.putInt("16", (int)com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimension(R.dimen.no_glances_width));
            a.putString("17", s);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-tts-gps-reset", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowGpsSensorCalibrationDone() {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_gpserror);
            a.putString("2", "Ublox Sensor Calibration Done");
            a.putInt("3", R.style.Glances_1);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-gps-calibration_sensor", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowGpsSensorCalibrationNotNeeded() {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_gpserror);
            a.putString("2", "Ublox Sensor Calibration Not Needed");
            a.putInt("3", R.style.Glances_1);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-gps-calibration_not_needed", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowGpsSwitch(String s, String s0) {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_gpserror);
            a.putString("4", s);
            a.putInt("5", R.style.Glances_1);
            a.putString("6", s0);
            a.putInt("7", R.style.Glances_2);
            a.putInt("16", (int)com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimension(R.dimen.no_glances_width));
            a.putString("17", s);
            com.navdy.hud.app.framework.toast.ToastManager a0 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            a0.dismissCurrentToast("debug-tts-gps-switch");
            a0.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-tts-gps-switch", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowNoTrafficToast() {
        if (isDebugTTSEnabled) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_open_nav_disable_traffic);
            a.putString("4", "Traffic Not used for Route Calculation");
            a.putInt("5", R.style.Glances_1);
            a.putString("17", "Traffic Not used for Route Calculation");
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-tts-no-traffic", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static void debugShowSpeedInput(String s) {
        if (isDebugTTSEnabled && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            android.os.Bundle a = new android.os.Bundle();
            a.putInt("13", 3000);
            a.putInt("8", R.drawable.icon_mm_dash);
            a.putString("4", s);
            a.putInt("5", R.style.Glances_1);
            a.putString("17", s);
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("debug-tts-speed-input", a, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
        }
    }
    
    public static boolean isDebugTTSEnabled() {
        return isDebugTTSEnabled;
    }
    
    public static void sendSpeechRequest(String s, com.navdy.service.library.events.audio.SpeechRequest$Category a, String s0) {
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            String s1 = java.util.Locale.getDefault().toLanguageTag();
            com.navdy.service.library.events.audio.SpeechRequest a0 = new com.navdy.service.library.events.audio.SpeechRequest$Builder().words(s).category(a).id(s0).sendStatus(Boolean.valueOf(false)).language(s1).build();
            bus.post(new com.navdy.hud.app.event.LocalSpeechRequest(a0));
        }
    }
    
    public static void sendSpeechRequest(String s, String s0) {
        com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(s, com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_NOTIFICATION, s0);
    }
}
