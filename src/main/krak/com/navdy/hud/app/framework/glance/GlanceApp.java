package com.navdy.hud.app.framework.glance;
import com.navdy.hud.app.R;


public enum GlanceApp {
    FUEL(0),
    GOOGLE_CALENDAR(1),
    GOOGLE_MAIL(2),
    GOOGLE_HANGOUT(3),
    SLACK(4),
    WHATS_APP(5),
    FACEBOOK_MESSENGER(6),
    FACEBOOK(7),
    TWITTER(8),
    IMESSAGE(9),
    APPLE_CALENDAR(10),
    APPLE_MAIL(11),
    SMS(12),
    GENERIC(13),
    GOOGLE_INBOX(14),
    GENERIC_MAIL(15),
    GENERIC_CALENDAR(16),
    GENERIC_MESSAGE(17),
    GENERIC_SOCIAL(18);

    private int value;
    GlanceApp(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class GlanceApp extends Enum {
//    final private static com.navdy.hud.app.framework.glance.GlanceApp[] $VALUES;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp APPLE_CALENDAR;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp APPLE_MAIL;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp FACEBOOK;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp FACEBOOK_MESSENGER;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp FUEL;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp GENERIC;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp GENERIC_CALENDAR;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp GENERIC_MAIL;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp GENERIC_MESSAGE;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp GENERIC_SOCIAL;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp GOOGLE_CALENDAR;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp GOOGLE_HANGOUT;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp GOOGLE_INBOX;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp GOOGLE_MAIL;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp IMESSAGE;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp SLACK;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp SMS;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp TWITTER;
//    final public static com.navdy.hud.app.framework.glance.GlanceApp WHATS_APP;
//    int color;
//    boolean defaultPhotoBasedOnId;
//    int mainIcon;
//    com.navdy.hud.app.framework.notifications.NotificationType notificationType;
//    int sideIcon;
//    
//    static {
//        FUEL = new com.navdy.hud.app.framework.glance.GlanceApp("FUEL", 0, com.navdy.hud.app.framework.glance.GlanceConstants.colorFuelLevel, R.drawable.icon_car_alert, R.drawable.icon_glance_fuel_low, false, com.navdy.hud.app.framework.notifications.NotificationType.LOW_FUEL);
//        GOOGLE_CALENDAR = new com.navdy.hud.app.framework.glance.GlanceApp("GOOGLE_CALENDAR", 1, com.navdy.hud.app.framework.glance.GlanceConstants.colorGoogleCalendar, R.drawable.icon_glance_google_calendar, -1, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        GOOGLE_MAIL = new com.navdy.hud.app.framework.glance.GlanceApp("GOOGLE_MAIL", 2, com.navdy.hud.app.framework.glance.GlanceConstants.colorGoogleMail, R.drawable.icon_glance_gmail, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        GOOGLE_HANGOUT = new com.navdy.hud.app.framework.glance.GlanceApp("GOOGLE_HANGOUT", 3, com.navdy.hud.app.framework.glance.GlanceConstants.colorGoogleHangout, R.drawable.icon_glance_hangout, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        SLACK = new com.navdy.hud.app.framework.glance.GlanceApp("SLACK", 4, com.navdy.hud.app.framework.glance.GlanceConstants.colorSlack, R.drawable.icon_glance_slack, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        WHATS_APP = new com.navdy.hud.app.framework.glance.GlanceApp("WHATS_APP", 5, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhatsapp, R.drawable.icon_glance_whatsapp, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        FACEBOOK_MESSENGER = new com.navdy.hud.app.framework.glance.GlanceApp("FACEBOOK_MESSENGER", 6, com.navdy.hud.app.framework.glance.GlanceConstants.colorFacebookMessenger, R.drawable.icon_glance_facebook_messenger, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        FACEBOOK = new com.navdy.hud.app.framework.glance.GlanceApp("FACEBOOK", 7, com.navdy.hud.app.framework.glance.GlanceConstants.colorFacebook, R.drawable.icon_glance_facebook, R.drawable.icon_glance_large_generic, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        TWITTER = new com.navdy.hud.app.framework.glance.GlanceApp("TWITTER", 8, com.navdy.hud.app.framework.glance.GlanceConstants.colorTwitter, R.drawable.icon_glance_twitter, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        IMESSAGE = new com.navdy.hud.app.framework.glance.GlanceApp("IMESSAGE", 9, com.navdy.hud.app.framework.glance.GlanceConstants.colorIMessage, R.drawable.icon_message_blue, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        APPLE_CALENDAR = new com.navdy.hud.app.framework.glance.GlanceApp("APPLE_CALENDAR", 10, com.navdy.hud.app.framework.glance.GlanceConstants.colorAppleCalendar, R.drawable.icon_glance_calendar, -1, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        APPLE_MAIL = new com.navdy.hud.app.framework.glance.GlanceApp("APPLE_MAIL", 11, com.navdy.hud.app.framework.glance.GlanceConstants.colorAppleMail, R.drawable.icon_glance_email, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        SMS = new com.navdy.hud.app.framework.glance.GlanceApp("SMS", 12, com.navdy.hud.app.framework.glance.GlanceConstants.colorSms, R.drawable.icon_message_blue, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        GENERIC = new com.navdy.hud.app.framework.glance.GlanceApp("GENERIC", 13, com.navdy.hud.app.framework.glance.GlanceConstants.colorGeneric, R.drawable.icon_glance_generic, R.drawable.icon_glance_large_generic, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        GOOGLE_INBOX = new com.navdy.hud.app.framework.glance.GlanceApp("GOOGLE_INBOX", 14, com.navdy.hud.app.framework.glance.GlanceConstants.colorGoogleInbox, R.drawable.icon_glance_google_inbox, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        GENERIC_MAIL = new com.navdy.hud.app.framework.glance.GlanceApp("GENERIC_MAIL", 15, com.navdy.hud.app.framework.glance.GlanceConstants.colorAppleMail, R.drawable.icon_glance_email, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        GENERIC_CALENDAR = new com.navdy.hud.app.framework.glance.GlanceApp("GENERIC_CALENDAR", 16, com.navdy.hud.app.framework.glance.GlanceConstants.colorAppleCalendar, R.drawable.icon_glance_calendar, -1, false, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        GENERIC_MESSAGE = new com.navdy.hud.app.framework.glance.GlanceApp("GENERIC_MESSAGE", 17, com.navdy.hud.app.framework.glance.GlanceConstants.colorSms, R.drawable.icon_message_blue, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        GENERIC_SOCIAL = new com.navdy.hud.app.framework.glance.GlanceApp("GENERIC_SOCIAL", 18, com.navdy.hud.app.framework.glance.GlanceConstants.colorTwitter, R.drawable.icon_social_blue, -1, true, com.navdy.hud.app.framework.notifications.NotificationType.GLANCE);
//        com.navdy.hud.app.framework.glance.GlanceApp[] a = new com.navdy.hud.app.framework.glance.GlanceApp[19];
//        a[0] = FUEL;
//        a[1] = GOOGLE_CALENDAR;
//        a[2] = GOOGLE_MAIL;
//        a[3] = GOOGLE_HANGOUT;
//        a[4] = SLACK;
//        a[5] = WHATS_APP;
//        a[6] = FACEBOOK_MESSENGER;
//        a[7] = FACEBOOK;
//        a[8] = TWITTER;
//        a[9] = IMESSAGE;
//        a[10] = APPLE_CALENDAR;
//        a[11] = APPLE_MAIL;
//        a[12] = SMS;
//        a[13] = GENERIC;
//        a[14] = GOOGLE_INBOX;
//        a[15] = GENERIC_MAIL;
//        a[16] = GENERIC_CALENDAR;
//        a[17] = GENERIC_MESSAGE;
//        a[18] = GENERIC_SOCIAL;
//        $VALUES = a;
//    }
//    
//    private GlanceApp(String s, int i, int i0, int i1, int i2, boolean b, com.navdy.hud.app.framework.notifications.NotificationType a) {
//        super(s, i);
//        this.color = i0;
//        this.sideIcon = i1;
//        this.mainIcon = i2;
//        this.defaultPhotoBasedOnId = b;
//        this.notificationType = a;
//    }
//    
//    public static com.navdy.hud.app.framework.glance.GlanceApp valueOf(String s) {
//        return (com.navdy.hud.app.framework.glance.GlanceApp)Enum.valueOf(com.navdy.hud.app.framework.glance.GlanceApp.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.glance.GlanceApp[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getColor() {
//        return this.color;
//    }
//    
//    public int getMainIcon() {
//        return this.mainIcon;
//    }
//    
//    public int getSideIcon() {
//        return this.sideIcon;
//    }
//    
//    public boolean isDefaultIconBasedOnId() {
//        return this.defaultPhotoBasedOnId;
//    }
//}
//