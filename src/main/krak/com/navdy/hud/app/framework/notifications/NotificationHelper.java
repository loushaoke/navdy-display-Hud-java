package com.navdy.hud.app.framework.notifications;

public class NotificationHelper {
    static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = com.navdy.hud.app.framework.notifications.NotificationManager.sLogger;
    }
    
    public NotificationHelper() {
    }
    
    public static boolean isNotificationRemovable(String s) {
        int i = 0;
        switch(s.hashCode()) {
            case -54194414: {
                i = (s.equals("navdy#traffic#reroute#notif")) ? 1 : -1;
                break;
            }
            case -1409926357: {
                i = (s.equals("navdy#phone#call#notif")) ? 0 : -1;
                break;
            }
            default: {
                i = -1;
            }
        }
        boolean b = false;
        switch(i) {
            default: {
                b = true;
            }
            case 0: case 1: {
                return b;
            }
        }
    }
}
