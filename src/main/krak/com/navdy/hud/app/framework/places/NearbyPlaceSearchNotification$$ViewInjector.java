package com.navdy.hud.app.framework.places;
import com.navdy.hud.app.R;

public class NearbyPlaceSearchNotification$$ViewInjector {
    public NearbyPlaceSearchNotification$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a0, Object a1) {
        a0.title = (android.widget.TextView)a.findRequiredView(a1, R.id.title, "field 'title'");
        a0.subTitle = (android.widget.TextView)a.findRequiredView(a1, R.id.subTitle, "field 'subTitle'");
        a0.iconColorView = (com.navdy.hud.app.ui.component.image.IconColorImageView)a.findRequiredView(a1, R.id.iconColorView, "field 'iconColorView'");
        a0.resultsCount = (android.widget.TextView)a.findRequiredView(a1, R.id.resultsCount, "field 'resultsCount'");
        a0.resultsLabel = (android.widget.TextView)a.findRequiredView(a1, R.id.resultsLabel, "field 'resultsLabel'");
        a0.statusBadge = (android.widget.ImageView)a.findRequiredView(a1, R.id.iconSpinner, "field 'statusBadge'");
        a0.iconSide = (com.navdy.hud.app.ui.component.image.IconColorImageView)a.findRequiredView(a1, R.id.iconSide, "field 'iconSide'");
        a0.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)a.findRequiredView(a1, R.id.choiceLayout, "field 'choiceLayout'");
    }
    
    public static void reset(com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification a) {
        a.title = null;
        a.subTitle = null;
        a.iconColorView = null;
        a.resultsCount = null;
        a.resultsLabel = null;
        a.statusBadge = null;
        a.iconSide = null;
        a.choiceLayout = null;
    }
}
