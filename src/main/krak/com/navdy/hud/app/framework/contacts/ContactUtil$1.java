package com.navdy.hud.app.framework.contacts;

final class ContactUtil$1 implements Runnable {
    final String val$cName;
    final com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback val$callback;
    final java.io.File val$imagePath;
    final com.navdy.hud.app.ui.component.image.InitialsImageView val$imageView;
    final String val$number;
    final com.navdy.hud.app.framework.contacts.PhoneImageDownloader val$phoneImageDownloader;
    final com.squareup.picasso.Transformation val$transformation;
    final boolean val$triggerPhotoDownload;
    
    ContactUtil$1(com.navdy.hud.app.framework.contacts.ContactUtil$IContactCallback a, java.io.File a0, String s, String s0, boolean b, com.navdy.hud.app.ui.component.image.InitialsImageView a1, com.squareup.picasso.Transformation a2, com.navdy.hud.app.framework.contacts.PhoneImageDownloader a3) {
        super();
        this.val$callback = a;
        this.val$imagePath = a0;
        this.val$cName = s;
        this.val$number = s0;
        this.val$triggerPhotoDownload = b;
        this.val$imageView = a1;
        this.val$transformation = a2;
        this.val$phoneImageDownloader = a3;
    }
    
    public void run() {
        if (this.val$callback.isContextValid()) {
            boolean b = this.val$imagePath.exists();
            com.navdy.hud.app.framework.contacts.ContactUtil.access$100().post((Runnable)new com.navdy.hud.app.framework.contacts.ContactUtil$1$1(this, b));
        }
    }
}
