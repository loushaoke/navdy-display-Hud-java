package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;

public class VoiceSearchTipsView extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.activity.Main$INotificationExtensionView {
    final public static int ANIMATION_DURATION = 600;
    final public static int ANIMATION_INTERVAL = 3000;
    private static int TIPS_COUNT;
    private static android.content.res.TypedArray VOICE_SEARCH_TIPS_ICONS;
    private static String[] VOICE_SEARCH_TIPS_TEXTS;
    private android.animation.AnimatorSet animatorSet;
    @InjectView(R.id.audio_source_icon)
    public android.widget.ImageView audioSourceIcon;
    @InjectView(R.id.audio_source_text)
    public android.widget.TextView audioSourceText;
    private int currentTipIndex;
    private android.animation.Animator fadeIn;
    private android.animation.Animator fadeOut;
    private android.animation.Animator fadeOutVoiceSearchInformationView;
    private android.os.Handler handler;
    @InjectView(R.id.tip_holder_1)
    android.view.ViewGroup holder1;
    @InjectView(R.id.tip_holder_2)
    android.view.ViewGroup holder2;
    private boolean isListeningOverBluetooth;
    private String langauge;
    private String languageName;
    @InjectView(R.id.locale_full_text)
    public android.widget.TextView localeFullNameText;
    @InjectView(R.id.locale_tag_text)
    public android.widget.TextView localeTagText;
    private Runnable runnable;
    private boolean runningForFirstTime;
    @InjectView(R.id.tip_icon_1)
    android.widget.ImageView tipsIcon1;
    @InjectView(R.id.tip_icon_2)
    android.widget.ImageView tipsIcon2;
    @InjectView(R.id.tip_text_1)
    android.widget.TextView tipsText1;
    @InjectView(R.id.tip_text_2)
    android.widget.TextView tipsText2;
    @InjectView(R.id.voice_search_information)
    public android.view.View voiceSearchInformationView;
    
    static {
        TIPS_COUNT = 0;
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        VOICE_SEARCH_TIPS_TEXTS = a.getStringArray(R.array.voice_search_tips);
        TIPS_COUNT = VOICE_SEARCH_TIPS_TEXTS.length;
        VOICE_SEARCH_TIPS_ICONS = a.obtainTypedArray(R.array.voice_search_tips_icons);
    }
    
    public VoiceSearchTipsView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public VoiceSearchTipsView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, -1);
    }
    
    public VoiceSearchTipsView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.isListeningOverBluetooth = false;
        this.currentTipIndex = 0;
        this.runningForFirstTime = true;
    }
    
    static Runnable access$000(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a) {
        return a.runnable;
    }
    
    static android.os.Handler access$100(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a) {
        return a.handler;
    }
    
    static int access$200(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a) {
        return a.currentTipIndex;
    }
    
    static int access$202(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a, int i) {
        a.currentTipIndex = i;
        return i;
    }
    
    static int access$300() {
        return TIPS_COUNT;
    }
    
    static String[] access$400() {
        return VOICE_SEARCH_TIPS_TEXTS;
    }
    
    static int access$500(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a, int i) {
        return a.getIconIdForIndex(i);
    }
    
    static boolean access$600(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a) {
        return a.runningForFirstTime;
    }
    
    static boolean access$602(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a, boolean b) {
        a.runningForFirstTime = b;
        return b;
    }
    
    static android.animation.AnimatorSet access$700(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a) {
        return a.animatorSet;
    }
    
    static android.animation.Animator access$800(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a) {
        return a.fadeOutVoiceSearchInformationView;
    }
    
    private int getIconIdForIndex(int i) {
        return VOICE_SEARCH_TIPS_ICONS.getResourceId(i, -1);
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.fadeIn = android.animation.AnimatorInflater.loadAnimator(this.getContext(), 17498112);
        this.fadeOut = android.animation.AnimatorInflater.loadAnimator(this.getContext(), 17498113);
        this.fadeIn.setTarget(this.holder2);
        this.fadeOut.setTarget(this.holder1);
        this.fadeOutVoiceSearchInformationView = android.animation.AnimatorInflater.loadAnimator(this.getContext(), 17498113);
        this.fadeOutVoiceSearchInformationView.setTarget(this.voiceSearchInformationView);
        this.fadeOutVoiceSearchInformationView.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.framework.voice.VoiceSearchTipsView$1(this));
        this.animatorSet = new android.animation.AnimatorSet();
        this.animatorSet.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.framework.voice.VoiceSearchTipsView$2(this));
        this.animatorSet.setDuration(600L);
        android.animation.AnimatorSet a = this.animatorSet;
        android.animation.Animator[] a0 = new android.animation.Animator[2];
        a0[0] = this.fadeIn;
        a0[1] = this.fadeOut;
        a.playTogether(a0);
        this.handler = new android.os.Handler();
        this.runnable = (Runnable)new com.navdy.hud.app.framework.voice.VoiceSearchTipsView$3(this);
    }
    
    public void onStart() {
        this.currentTipIndex = 0;
        if (this.isListeningOverBluetooth) {
            this.audioSourceIcon.setImageResource(R.drawable.icon_badge_bt_tips);
            this.audioSourceText.setText(R.string.bluetooth_microphone);
        } else {
            this.audioSourceIcon.setImageResource(R.drawable.icon_badge_phone_source);
            this.audioSourceText.setText(R.string.phone_microphone);
        }
        this.localeTagText.setText((CharSequence)this.langauge);
        this.localeFullNameText.setText((CharSequence)this.languageName);
        this.handler.postDelayed((Runnable)new com.navdy.hud.app.framework.voice.VoiceSearchTipsView$4(this), 3000L);
    }
    
    public void onStop() {
        this.handler.removeCallbacks(this.runnable);
    }
    
    public void setListeningOverBluetooth(boolean b) {
        this.isListeningOverBluetooth = b;
    }
    
    public void setVoiceSearchLocale(java.util.Locale a) {
        this.langauge = a.getLanguage();
        this.languageName = a.getDisplayLanguage();
    }
}
