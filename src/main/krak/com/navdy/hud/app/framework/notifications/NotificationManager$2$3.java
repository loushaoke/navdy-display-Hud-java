package com.navdy.hud.app.framework.notifications;

class NotificationManager$2$3 implements Runnable {
    final com.navdy.hud.app.framework.notifications.NotificationManager$2 this$1;
    final Runnable val$endAction;
    final android.view.View val$expandedView;
    final android.widget.FrameLayout val$layout;
    
    NotificationManager$2$3(com.navdy.hud.app.framework.notifications.NotificationManager$2 a, android.view.View a0, android.widget.FrameLayout a1, Runnable a2) {
        super();
        this.this$1 = a;
        this.val$expandedView = a0;
        this.val$layout = a1;
        this.val$endAction = a2;
    }
    
    public void run() {
        com.navdy.hud.app.framework.notifications.NotificationAnimator.animateExpandedViews(this.val$expandedView, (android.view.View)null, (android.view.ViewGroup)this.val$layout, this.this$1.this$0.animationTranslation, 250, 0, this.val$endAction);
    }
}
