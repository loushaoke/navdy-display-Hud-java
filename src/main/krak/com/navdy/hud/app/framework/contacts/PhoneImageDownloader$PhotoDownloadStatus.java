package com.navdy.hud.app.framework.contacts;

public class PhoneImageDownloader$PhotoDownloadStatus {
    public boolean alreadyDownloaded;
    public com.navdy.service.library.events.photo.PhotoType photoType;
    public String sourceIdentifier;
    public boolean success;
    
    PhoneImageDownloader$PhotoDownloadStatus(String s, com.navdy.service.library.events.photo.PhotoType a, boolean b, boolean b0) {
        this.sourceIdentifier = s;
        this.photoType = a;
        this.success = b;
        this.alreadyDownloaded = b0;
    }
}
