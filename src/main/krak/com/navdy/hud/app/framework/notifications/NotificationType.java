package com.navdy.hud.app.framework.notifications;


public enum NotificationType {
    GLYMPSE(0),
    SMS(1),
    PHONE_CALL(2),
    ROUTE_CALC(3),
    PLACE_TYPE_SEARCH(4),
    VOICE_SEARCH(5),
    FASTER_ROUTE(6),
    ETA_DELAY(7),
    TRAFFIC_EVENT(8),
    TRAFFIC_JAM(9),
    LOW_FUEL(10),
    GLANCE(11),
    MUSIC(12),
    VOICE_ASSIST(13),
    BRIGHTNESS(14);

    private int value;
    NotificationType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class NotificationType extends Enum {
//    final private static com.navdy.hud.app.framework.notifications.NotificationType[] $VALUES;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType BRIGHTNESS;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType ETA_DELAY;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType FASTER_ROUTE;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType GLANCE;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType GLYMPSE;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType LOW_FUEL;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType MUSIC;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType PHONE_CALL;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType PLACE_TYPE_SEARCH;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType ROUTE_CALC;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType SMS;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType TRAFFIC_EVENT;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType TRAFFIC_JAM;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType VOICE_ASSIST;
//    final public static com.navdy.hud.app.framework.notifications.NotificationType VOICE_SEARCH;
//    int priority;
//    
//    static {
//        GLYMPSE = new com.navdy.hud.app.framework.notifications.NotificationType("GLYMPSE", 0, 100);
//        SMS = new com.navdy.hud.app.framework.notifications.NotificationType("SMS", 1, 100);
//        PHONE_CALL = new com.navdy.hud.app.framework.notifications.NotificationType("PHONE_CALL", 2, 99);
//        ROUTE_CALC = new com.navdy.hud.app.framework.notifications.NotificationType("ROUTE_CALC", 3, 98);
//        PLACE_TYPE_SEARCH = new com.navdy.hud.app.framework.notifications.NotificationType("PLACE_TYPE_SEARCH", 4, 98);
//        VOICE_SEARCH = new com.navdy.hud.app.framework.notifications.NotificationType("VOICE_SEARCH", 5, 98);
//        FASTER_ROUTE = new com.navdy.hud.app.framework.notifications.NotificationType("FASTER_ROUTE", 6, 97);
//        ETA_DELAY = new com.navdy.hud.app.framework.notifications.NotificationType("ETA_DELAY", 7, 96);
//        TRAFFIC_EVENT = new com.navdy.hud.app.framework.notifications.NotificationType("TRAFFIC_EVENT", 8, 95);
//        TRAFFIC_JAM = new com.navdy.hud.app.framework.notifications.NotificationType("TRAFFIC_JAM", 9, 94);
//        LOW_FUEL = new com.navdy.hud.app.framework.notifications.NotificationType("LOW_FUEL", 10, 97);
//        GLANCE = new com.navdy.hud.app.framework.notifications.NotificationType("GLANCE", 11, 50);
//        MUSIC = new com.navdy.hud.app.framework.notifications.NotificationType("MUSIC", 12, 1);
//        VOICE_ASSIST = new com.navdy.hud.app.framework.notifications.NotificationType("VOICE_ASSIST", 13, 1);
//        BRIGHTNESS = new com.navdy.hud.app.framework.notifications.NotificationType("BRIGHTNESS", 14, 1);
//        com.navdy.hud.app.framework.notifications.NotificationType[] a = new com.navdy.hud.app.framework.notifications.NotificationType[15];
//        a[0] = GLYMPSE;
//        a[1] = SMS;
//        a[2] = PHONE_CALL;
//        a[3] = ROUTE_CALC;
//        a[4] = PLACE_TYPE_SEARCH;
//        a[5] = VOICE_SEARCH;
//        a[6] = FASTER_ROUTE;
//        a[7] = ETA_DELAY;
//        a[8] = TRAFFIC_EVENT;
//        a[9] = TRAFFIC_JAM;
//        a[10] = LOW_FUEL;
//        a[11] = GLANCE;
//        a[12] = MUSIC;
//        a[13] = VOICE_ASSIST;
//        a[14] = BRIGHTNESS;
//        $VALUES = a;
//    }
//    
//    private NotificationType(String s, int i, int i0) {
//        super(s, i);
//        this.priority = i0;
//    }
//    
//    public static com.navdy.hud.app.framework.notifications.NotificationType valueOf(String s) {
//        return (com.navdy.hud.app.framework.notifications.NotificationType)Enum.valueOf(com.navdy.hud.app.framework.notifications.NotificationType.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.notifications.NotificationType[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getPriority() {
//        return this.priority;
//    }
//}
//