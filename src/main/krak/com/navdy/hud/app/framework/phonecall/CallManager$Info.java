package com.navdy.hud.app.framework.phonecall;

class CallManager$Info {
    boolean answered;
    boolean callMuted;
    long callStartMs;
    String callUUID;
    String contact;
    long duration;
    boolean incomingCall;
    String normalizedNumber;
    String number;
    boolean putOnStack;
    boolean userCancelledCall;
    boolean userRejectedCall;
    
    CallManager$Info(String s, String s0, String s1, boolean b, boolean b0, long j, String s2, long j0, boolean b1, boolean b2, boolean b3, boolean b4) {
        this.number = s;
        this.normalizedNumber = s0;
        this.contact = s1;
        this.incomingCall = b;
        this.answered = b0;
        this.callStartMs = j;
        this.callUUID = s2;
        this.duration = j0;
        this.userRejectedCall = b1;
        this.userCancelledCall = b2;
        this.putOnStack = b3;
        this.callMuted = b4;
    }
}
