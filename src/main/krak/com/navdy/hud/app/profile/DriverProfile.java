package com.navdy.hud.app.profile;

public class DriverProfile {
    final private static String CANNED_MESSAGES = "CannedMessages";
    final private static String CONTACTS_IMAGE_CACHE_DIR = "contacts";
    final public static String DRIVER_PROFILE_IMAGE = "DriverImage";
    final private static String DRIVER_PROFILE_PREFERENCES = "DriverProfilePreferences";
    final private static String INPUT_PREFERENCES = "InputPreferences";
    final private static String LOCALE_SEPARATOR = "_";
    final private static String LOCAL_PREFERENCES = "LocalPreferences";
    final private static String MUSIC_IMAGE_CACHE_DIR = "music";
    final private static String NAVIGATION_PREFERENCES = "NavigationPreferences";
    final private static String NOTIFICATION_PREFERENCES = "NotificationPreferences";
    final private static String PLACES_IMAGE_CACHE_DIR = "places";
    final private static String PREFERENCES_DIRECTORY = "Preferences";
    final private static String SPEAKER_PREFERENCES = "SpeakerPreferences";
    final private static com.navdy.service.library.log.Logger sLogger;
    com.navdy.service.library.events.glances.CannedMessagesUpdate mCannedMessages;
    private java.io.File mContactsImageDirectory;
    private android.graphics.Bitmap mDriverImage;
    com.navdy.service.library.events.preferences.DriverProfilePreferences mDriverProfilePreferences;
    com.navdy.service.library.events.preferences.InputPreferences mInputPreferences;
    private com.navdy.service.library.events.preferences.LocalPreferences mLocalPreferences;
    private java.util.Locale mLocale;
    private com.navdy.service.library.events.MessageStore mMessageStore;
    private java.io.File mMusicImageDirectory;
    com.navdy.service.library.events.preferences.NavigationPreferences mNavigationPreferences;
    com.navdy.service.library.events.preferences.NotificationPreferences mNotificationPreferences;
    private com.navdy.hud.app.profile.NotificationSettings mNotificationSettings;
    private java.io.File mPlacesImageDirectory;
    private java.io.File mPreferencesDirectory;
    private java.io.File mProfileDirectory;
    private String mProfileName;
    private com.navdy.service.library.events.preferences.DisplaySpeakerPreferences mSpeakerPreferences;
    private boolean mTrafficEnabled;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.profile.DriverProfile.class);
    }
    
    protected DriverProfile(java.io.File a) {
        this.mProfileName = a.getName();
        this.mProfileDirectory = a;
        this.mPreferencesDirectory = new java.io.File(a, "Preferences");
        this.mMessageStore = new com.navdy.service.library.events.MessageStore(this.mPreferencesDirectory);
        this.mPlacesImageDirectory = new java.io.File(a, "places");
        com.navdy.service.library.util.IOUtils.createDirectory(this.mPlacesImageDirectory);
        this.mContactsImageDirectory = new java.io.File(a, "contacts");
        com.navdy.service.library.util.IOUtils.createDirectory(this.mContactsImageDirectory);
        this.mMusicImageDirectory = new java.io.File(a, "music");
        com.navdy.service.library.util.IOUtils.createDirectory(this.mMusicImageDirectory);
        this.mDriverProfilePreferences = (com.navdy.service.library.events.preferences.DriverProfilePreferences)this.readPreference("DriverProfilePreferences", com.navdy.service.library.events.preferences.DriverProfilePreferences.class, (com.navdy.service.library.events.NavdyEventUtil$Initializer)new com.navdy.hud.app.profile.DriverProfile$1(this));
        this.mNavigationPreferences = (com.navdy.service.library.events.preferences.NavigationPreferences)this.readPreference("NavigationPreferences", com.navdy.service.library.events.preferences.NavigationPreferences.class);
        this.setTraffic();
        this.mInputPreferences = (com.navdy.service.library.events.preferences.InputPreferences)this.readPreference("InputPreferences", com.navdy.service.library.events.preferences.InputPreferences.class);
        this.mSpeakerPreferences = (com.navdy.service.library.events.preferences.DisplaySpeakerPreferences)this.readPreference("SpeakerPreferences", com.navdy.service.library.events.preferences.DisplaySpeakerPreferences.class);
        this.mNotificationPreferences = (com.navdy.service.library.events.preferences.NotificationPreferences)this.readPreference("NotificationPreferences", com.navdy.service.library.events.preferences.NotificationPreferences.class);
        this.mNotificationSettings = new com.navdy.hud.app.profile.NotificationSettings(this.mNotificationPreferences);
        this.mLocalPreferences = (com.navdy.service.library.events.preferences.LocalPreferences)this.readPreference("LocalPreferences", com.navdy.service.library.events.preferences.LocalPreferences.class);
        this.mCannedMessages = (com.navdy.service.library.events.glances.CannedMessagesUpdate)this.readPreference("CannedMessages", com.navdy.service.library.events.glances.CannedMessagesUpdate.class);
        label5: {
            java.io.FileInputStream a0 = null;
            label4: {
                label1: {
                    java.io.FileInputStream a1 = null;
                    Throwable a2 = null;
                    try {
                        a1 = null;
                        label0: {
                            label3: {
                                label2: {
                                    try {
                                        a1 = null;
                                        java.io.File a3 = new java.io.File(this.mPreferencesDirectory, "DriverImage");
                                        a1 = null;
                                        a1 = null;
                                        a0 = new java.io.FileInputStream(a3);
                                        break label2;
                                    } catch(java.io.FileNotFoundException ignoredException) {
                                    }
                                    a0 = null;
                                    break label3;
                                }
                                try {
                                    try {
                                        this.mDriverImage = android.graphics.BitmapFactory.decodeStream((java.io.InputStream)a0);
                                        break label1;
                                    } catch(java.io.FileNotFoundException ignoredException0) {
                                    }
                                } catch(Throwable a4) {
                                    a2 = a4;
                                    break label0;
                                }
                            }
                            a1 = a0;
                            this.mDriverImage = null;
                            break label4;
                        }
                        a1 = a0;
                    } catch(Throwable a5) {
                        a2 = a5;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                    throw a2;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                break label5;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
        }
        this.setLocale();
    }
    
    static java.io.File access$000(com.navdy.hud.app.profile.DriverProfile a) {
        return a.mProfileDirectory;
    }
    
    static java.io.File access$100(com.navdy.hud.app.profile.DriverProfile a) {
        return a.mPreferencesDirectory;
    }
    
    static com.navdy.hud.app.profile.DriverProfile createProfileForId(String s, String s0) {
        if (!s.contains((CharSequence)java.io.File.separator) && !s.startsWith(".")) {
            String s1 = new StringBuilder().append(s0).append(java.io.File.separator).append(s).toString();
            java.io.File a = new java.io.File(s1);
            if (!a.mkdir() && !a.isDirectory()) {
                throw new java.io.IOException("could not create profile");
            }
            java.io.File a0 = new java.io.File(new StringBuilder().append(s1).append(java.io.File.separator).append("Preferences").toString());
            if (!a0.mkdir() && !a0.isDirectory()) {
                throw new java.io.IOException("could not create preferences directory");
            }
            return new com.navdy.hud.app.profile.DriverProfile(a);
        }
        throw new IllegalArgumentException("Profile names can't refer to directories");
    }
    
    private com.squareup.wire.Message readPreference(String s, Class a) {
        return this.mMessageStore.readMessage(s, a);
    }
    
    private com.squareup.wire.Message readPreference(String s, Class a, com.navdy.service.library.events.NavdyEventUtil$Initializer a0) {
        return this.mMessageStore.readMessage(s, a, a0);
    }
    
    private void removeDriverImage() {
        this.mDriverImage = null;
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.profile.DriverProfile$2(this), 1);
    }
    
    private com.squareup.wire.Message removeNulls(com.squareup.wire.Message a) {
        return com.navdy.service.library.events.NavdyEventUtil.applyDefaults(a);
    }
    
    private void setLocale() {
        label0: try {
            Throwable a = null;
            this.mLocale = null;
            String s = this.mDriverProfilePreferences.locale;
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                sLogger.v("no locale string");
                break label0;
            } else {
                java.util.Locale a0 = com.navdy.hud.app.profile.HudLocale.getLocaleForID(s);
                sLogger.v(new StringBuilder().append("locale string:").append(s).append(" locale:").append(a0).append(" profile:").append(this.getProfileName()).append(" email:").append(this.getDriverEmail()).toString());
                try {
                    if (android.text.TextUtils.isEmpty((CharSequence)a0.getISO3Language())) {
                        sLogger.v("locale no language");
                        break label0;
                    } else {
                        this.mLocale = a0;
                        break label0;
                    }
                } catch(Throwable a1) {
                    a = a1;
                }
            }
            sLogger.v("locale not valid", a);
        } catch(Throwable a2) {
            sLogger.e("setLocale", a2);
        }
    }
    
    private void setTraffic() {
        this.mTrafficEnabled = true;
    }
    
    private void writeMessageToFile(com.squareup.wire.Message a, String s) {
        this.mMessageStore.writeMessage(a, s);
    }
    
    void copy(com.navdy.hud.app.profile.DriverProfile a) {
        this.mDriverProfilePreferences = new com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder(a.mDriverProfilePreferences).build();
        this.writeMessageToFile((com.squareup.wire.Message)this.mDriverProfilePreferences, "DriverProfilePreferences");
        this.mNavigationPreferences = new com.navdy.service.library.events.preferences.NavigationPreferences$Builder(a.mNavigationPreferences).build();
        this.writeMessageToFile((com.squareup.wire.Message)this.mNavigationPreferences, "NavigationPreferences");
        this.mInputPreferences = new com.navdy.service.library.events.preferences.InputPreferences$Builder(a.mInputPreferences).build();
        this.writeMessageToFile((com.squareup.wire.Message)this.mInputPreferences, "InputPreferences");
        this.mSpeakerPreferences = new com.navdy.service.library.events.preferences.DisplaySpeakerPreferences$Builder(a.mSpeakerPreferences).build();
        this.writeMessageToFile((com.squareup.wire.Message)this.mSpeakerPreferences, "SpeakerPreferences");
        this.mNotificationPreferences = new com.navdy.service.library.events.preferences.NotificationPreferences$Builder(a.mNotificationPreferences).build();
        this.writeMessageToFile((com.squareup.wire.Message)this.mNotificationPreferences, "NotificationPreferences");
        this.mLocalPreferences = new com.navdy.service.library.events.preferences.LocalPreferences$Builder(a.mLocalPreferences).build();
        this.writeMessageToFile((com.squareup.wire.Message)this.mLocalPreferences, "LocalPreferences");
        sLogger.v(new StringBuilder().append("copy done:").append(this.mPreferencesDirectory).toString());
    }
    
    public com.navdy.service.library.events.glances.CannedMessagesUpdate getCannedMessages() {
        return this.mCannedMessages;
    }
    
    public String getCarMake() {
        return this.mDriverProfilePreferences.car_make;
    }
    
    public String getCarModel() {
        return this.mDriverProfilePreferences.car_model;
    }
    
    public String getCarYear() {
        return this.mDriverProfilePreferences.car_year;
    }
    
    public java.io.File getContactsImageDir() {
        return this.mContactsImageDirectory;
    }
    
    public String getDeviceName() {
        return this.mDriverProfilePreferences.device_name;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat getDisplayFormat() {
        return this.mDriverProfilePreferences.display_format;
    }
    
    public String getDriverEmail() {
        return this.mDriverProfilePreferences.driver_email;
    }
    
    public android.graphics.Bitmap getDriverImage() {
        return this.mDriverImage;
    }
    
    public java.io.File getDriverImageFile() {
        return new java.io.File(this.mPreferencesDirectory, "DriverImage");
    }
    
    public String getDriverName() {
        return this.mDriverProfilePreferences.driver_name;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$FeatureMode getFeatureMode() {
        return this.mDriverProfilePreferences.feature_mode;
    }
    
    public String getFirstName() {
        String s = this.mDriverProfilePreferences.driver_name;
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            s = s.trim();
            int i = s.indexOf(" ");
            if (i != -1) {
                s = s.substring(0, i);
            }
        }
        return s;
    }
    
    public com.navdy.service.library.events.preferences.InputPreferences getInputPreferences() {
        return this.mInputPreferences;
    }
    
    public com.navdy.service.library.events.preferences.LocalPreferences getLocalPreferences() {
        return this.mLocalPreferences;
    }
    
    public java.util.Locale getLocale() {
        return (this.mLocale == null) ? java.util.Locale.getDefault() : this.mLocale;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction getLongPressAction() {
        return com.navdy.hud.app.ui.component.UISettings.isLongPressActionPlaceSearch() ? com.navdy.service.library.events.preferences.DriverProfilePreferences$DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH : this.mDriverProfilePreferences.dial_long_press_action;
    }
    
    public java.io.File getMusicImageDir() {
        return this.mMusicImageDirectory;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences getNavigationPreferences() {
        return this.mNavigationPreferences;
    }
    
    public com.navdy.service.library.events.preferences.NotificationPreferences getNotificationPreferences() {
        return this.mNotificationPreferences;
    }
    
    public com.navdy.hud.app.profile.NotificationSettings getNotificationSettings() {
        return this.mNotificationSettings;
    }
    
    public long getObdBlacklistModificationTime() {
        return (this.mDriverProfilePreferences.obdBlacklistLastModified == null) ? 0L : this.mDriverProfilePreferences.obdBlacklistLastModified.longValue();
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting getObdScanSetting() {
        return this.mDriverProfilePreferences.obdScanSetting;
    }
    
    public java.io.File getPlacesImageDir() {
        return this.mPlacesImageDirectory;
    }
    
    public java.io.File getPreferencesDirectory() {
        return this.mPreferencesDirectory;
    }
    
    public String getProfileName() {
        return this.mProfileName;
    }
    
    public com.navdy.service.library.events.preferences.DisplaySpeakerPreferences getSpeakerPreferences() {
        return this.mSpeakerPreferences;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem getUnitSystem() {
        return this.mDriverProfilePreferences.unit_system;
    }
    
    public boolean isAutoOnEnabled() {
        return ((Boolean)com.squareup.wire.Wire.get(this.mDriverProfilePreferences.auto_on_enabled, com.navdy.service.library.events.preferences.DriverProfilePreferences.DEFAULT_AUTO_ON_ENABLED)).booleanValue();
    }
    
    public boolean isDefaultProfile() {
        return com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isDefaultProfile(this);
    }
    
    public boolean isLimitBandwidthModeOn() {
        return Boolean.TRUE.equals(this.mDriverProfilePreferences.limit_bandwidth);
    }
    
    public boolean isProfilePublic() {
        return Boolean.TRUE.equals(this.mDriverProfilePreferences.profile_is_public);
    }
    
    public boolean isTrafficEnabled() {
        return this.mTrafficEnabled;
    }
    
    void setCannedMessages(com.navdy.service.library.events.glances.CannedMessagesUpdate a) {
        this.mCannedMessages = (com.navdy.service.library.events.glances.CannedMessagesUpdate)this.removeNulls((com.squareup.wire.Message)a);
        sLogger.i(new StringBuilder().append("[").append(this.mProfileName).append("] updating canned messages preferences to ver[").append(a.serial_number).append("]").toString());
        this.writeMessageToFile((com.squareup.wire.Message)this.mCannedMessages, "CannedMessages");
    }
    
    void setDriverProfilePreferences(com.navdy.service.library.events.preferences.DriverProfilePreferences a) {
        this.mDriverProfilePreferences = (com.navdy.service.library.events.preferences.DriverProfilePreferences)this.removeNulls((com.squareup.wire.Message)a);
        this.setLocale();
        sLogger.i(new StringBuilder().append("[").append(this.mProfileName).append("] updating driver profile preferences to ver[").append(a.serial_number).append("] ").append(this.mDriverProfilePreferences).toString());
        this.writeMessageToFile((com.squareup.wire.Message)this.mDriverProfilePreferences, "DriverProfilePreferences");
    }
    
    void setInputPreferences(com.navdy.service.library.events.preferences.InputPreferences a) {
        this.mInputPreferences = (com.navdy.service.library.events.preferences.InputPreferences)this.removeNulls((com.squareup.wire.Message)a);
        sLogger.i(new StringBuilder().append("[").append(this.mProfileName).append("] updating input preferences to ver[").append(a.serial_number).append("] ").append(this.mInputPreferences).toString());
        this.writeMessageToFile((com.squareup.wire.Message)this.mInputPreferences, "InputPreferences");
    }
    
    public void setLocalPreferences(com.navdy.service.library.events.preferences.LocalPreferences a) {
        this.mLocalPreferences = (com.navdy.service.library.events.preferences.LocalPreferences)this.removeNulls((com.squareup.wire.Message)a);
        sLogger.i(new StringBuilder().append("[").append(this.mProfileName).append("] updating local preferences to ").append(this.mLocalPreferences).toString());
        this.writeMessageToFile((com.squareup.wire.Message)this.mLocalPreferences, "LocalPreferences");
    }
    
    void setNavigationPreferences(com.navdy.service.library.events.preferences.NavigationPreferences a) {
        this.mNavigationPreferences = (com.navdy.service.library.events.preferences.NavigationPreferences)this.removeNulls((com.squareup.wire.Message)a);
        sLogger.i(new StringBuilder().append("[").append(this.mProfileName).append("] updating nav preferences to ver[").append(a.serial_number).append("] ").append(this.mNavigationPreferences).toString());
        this.writeMessageToFile((com.squareup.wire.Message)this.mNavigationPreferences, "NavigationPreferences");
        this.setTraffic();
    }
    
    void setNotificationPreferences(com.navdy.service.library.events.preferences.NotificationPreferences a) {
        this.mNotificationPreferences = (com.navdy.service.library.events.preferences.NotificationPreferences)this.removeNulls((com.squareup.wire.Message)a);
        sLogger.i(new StringBuilder().append("[").append(this.mProfileName).append("] updating notification preferences to ver[").append(a.serial_number).append("] ").append(this.mNotificationPreferences).toString());
        this.writeMessageToFile((com.squareup.wire.Message)this.mNotificationPreferences, "NotificationPreferences");
        this.mNotificationSettings.update(this.mNotificationPreferences);
    }
    
    void setSpeakerPreferences(com.navdy.service.library.events.preferences.DisplaySpeakerPreferences a) {
        this.mSpeakerPreferences = (com.navdy.service.library.events.preferences.DisplaySpeakerPreferences)this.removeNulls((com.squareup.wire.Message)a);
        sLogger.i(new StringBuilder().append("[").append(this.mProfileName).append("] updating speaker preferences to ver[").append(a.serial_number).append("] ").append(this.mSpeakerPreferences).toString());
        this.writeMessageToFile((com.squareup.wire.Message)this.mSpeakerPreferences, "SpeakerPreferences");
    }
    
    public void setTrafficEnabled(boolean b) {
        this.mTrafficEnabled = b;
    }
    
    public String toString() {
        return new StringBuilder().append("DriverProfile{mProfileName='").append(this.mProfileName).append((char)39).append((char)125).toString();
    }
}
