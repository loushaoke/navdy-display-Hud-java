package com.navdy.hud.app.profile;

class DriverProfile$1 extends com.navdy.service.library.events.NavdyEventUtil$Initializer {
    final com.navdy.hud.app.profile.DriverProfile this$0;
    
    DriverProfile$1(com.navdy.hud.app.profile.DriverProfile a) {
        super();
        this.this$0 = a;
    }
    
    public com.navdy.service.library.events.preferences.DriverProfilePreferences build(com.squareup.wire.Message.Builder a) {
        return ((com.navdy.service.library.events.preferences.DriverProfilePreferences$Builder)a).device_name(new StringBuilder().append("Phone ").append(com.navdy.hud.app.profile.DriverProfile.access$000(this.this$0).getName()).toString()).build();
    }
    
    publ    ic com.squareup.wire.Message build(com.squareup.wire.Message.Builder a) {
        return this.build(a);
    }
}
