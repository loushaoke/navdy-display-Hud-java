package com.navdy.hud.app.profile;

class DriverProfileManager$5 implements Runnable {
    final com.navdy.hud.app.profile.DriverProfileManager this$0;
    final com.navdy.service.library.events.preferences.DriverProfilePreferences val$preferences;
    
    DriverProfileManager$5(com.navdy.hud.app.profile.DriverProfileManager a, com.navdy.service.library.events.preferences.DriverProfilePreferences a0) {
        super();
        this.this$0 = a;
        this.val$preferences = a0;
    }
    
    public void run() {
        com.navdy.service.library.events.preferences.DriverProfilePreferences a = com.navdy.hud.app.profile.DriverProfileManager.access$400(this.this$0, this.val$preferences);
        java.util.Locale a0 = com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0).getLocale();
        com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0).setDriverProfilePreferences(a);
        com.navdy.hud.app.profile.DriverProfileManager.access$600(this.this$0).setDriverProfilePreferences(a);
        java.util.Locale a1 = com.navdy.hud.app.profile.DriverProfileManager.access$500(this.this$0).getLocale();
        boolean b = !a0.equals(a1);
        com.navdy.hud.app.profile.DriverProfileManager.access$000().i(new StringBuilder().append("[HUD-locale] changed[").append(b).append("] current[").append(a0).append("] new[").append(a1).append("]").toString());
        com.navdy.hud.app.profile.DriverProfileManager.access$100(this.this$0, b);
        com.navdy.hud.app.profile.DriverProfileManager.access$200(this.this$0).post(new com.navdy.hud.app.event.DriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated$State.UPDATED));
    }
}
