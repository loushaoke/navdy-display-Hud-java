package com.navdy.hud.app.manager;

final public class RemoteDeviceManager$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding calendarManager;
    private dagger.internal.Binding callManager;
    private dagger.internal.Binding connectionHandler;
    private dagger.internal.Binding driveRecorder;
    private dagger.internal.Binding featureUtil;
    private dagger.internal.Binding gestureServiceConnector;
    private dagger.internal.Binding httpManager;
    private dagger.internal.Binding inputManager;
    private dagger.internal.Binding musicManager;
    private dagger.internal.Binding preferences;
    private dagger.internal.Binding telemetryDataManager;
    private dagger.internal.Binding timeHelper;
    private dagger.internal.Binding tripManager;
    private dagger.internal.Binding uiStateManager;
    private dagger.internal.Binding voiceSearchHandler;
    
    public RemoteDeviceManager$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.manager.RemoteDeviceManager", false, com.navdy.hud.app.manager.RemoteDeviceManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.connectionHandler = a.requestBinding("com.navdy.hud.app.service.ConnectionHandler", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.callManager = a.requestBinding("com.navdy.hud.app.framework.phonecall.CallManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.inputManager = a.requestBinding("com.navdy.hud.app.manager.InputManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.preferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.timeHelper = a.requestBinding("com.navdy.hud.app.common.TimeHelper", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.calendarManager = a.requestBinding("com.navdy.hud.app.framework.calendar.CalendarManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.gestureServiceConnector = a.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.tripManager = a.requestBinding("com.navdy.hud.app.framework.trips.TripManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.driveRecorder = a.requestBinding("com.navdy.hud.app.debug.DriveRecorder", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.musicManager = a.requestBinding("com.navdy.hud.app.manager.MusicManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.voiceSearchHandler = a.requestBinding("com.navdy.hud.app.framework.voice.VoiceSearchHandler", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.featureUtil = a.requestBinding("com.navdy.hud.app.util.FeatureUtil", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.httpManager = a.requestBinding("com.navdy.service.library.network.http.IHttpManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
        this.telemetryDataManager = a.requestBinding("com.navdy.hud.app.analytics.TelemetryDataManager", com.navdy.hud.app.manager.RemoteDeviceManager.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.connectionHandler);
        a0.add(this.callManager);
        a0.add(this.inputManager);
        a0.add(this.preferences);
        a0.add(this.timeHelper);
        a0.add(this.calendarManager);
        a0.add(this.gestureServiceConnector);
        a0.add(this.uiStateManager);
        a0.add(this.tripManager);
        a0.add(this.driveRecorder);
        a0.add(this.musicManager);
        a0.add(this.voiceSearchHandler);
        a0.add(this.featureUtil);
        a0.add(this.httpManager);
        a0.add(this.telemetryDataManager);
    }
    
    public void injectMembers(com.navdy.hud.app.manager.RemoteDeviceManager a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.connectionHandler = (com.navdy.hud.app.service.ConnectionHandler)this.connectionHandler.get();
        a.callManager = (com.navdy.hud.app.framework.phonecall.CallManager)this.callManager.get();
        a.inputManager = (com.navdy.hud.app.manager.InputManager)this.inputManager.get();
        a.preferences = (android.content.SharedPreferences)this.preferences.get();
        a.timeHelper = (com.navdy.hud.app.common.TimeHelper)this.timeHelper.get();
        a.calendarManager = (com.navdy.hud.app.framework.calendar.CalendarManager)this.calendarManager.get();
        a.gestureServiceConnector = (com.navdy.hud.app.gesture.GestureServiceConnector)this.gestureServiceConnector.get();
        a.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get();
        a.tripManager = (com.navdy.hud.app.framework.trips.TripManager)this.tripManager.get();
        a.driveRecorder = (com.navdy.hud.app.debug.DriveRecorder)this.driveRecorder.get();
        a.musicManager = (com.navdy.hud.app.manager.MusicManager)this.musicManager.get();
        a.voiceSearchHandler = (com.navdy.hud.app.framework.voice.VoiceSearchHandler)this.voiceSearchHandler.get();
        a.featureUtil = (com.navdy.hud.app.util.FeatureUtil)this.featureUtil.get();
        a.httpManager = (com.navdy.service.library.network.http.IHttpManager)this.httpManager.get();
        a.telemetryDataManager = (com.navdy.hud.app.analytics.TelemetryDataManager)this.telemetryDataManager.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.manager.RemoteDeviceManager)a);
    }
}
