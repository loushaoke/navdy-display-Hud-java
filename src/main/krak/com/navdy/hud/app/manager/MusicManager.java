package com.navdy.hud.app.manager;
import com.navdy.hud.app.R;

public class MusicManager {
    final public static com.navdy.hud.app.manager.MusicManager$MediaControl[] CONTROLS;
    final public static int DEFAULT_ARTWORK_SIZE = 200;
    final public static com.navdy.service.library.events.audio.MusicTrackInfo EMPTY_TRACK;
    final public static long PAUSED_DUE_TO_HFP_EXPIRY_TIME;
    final public static String PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE = "PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE";
    final public static String PREFERENCE_MUSIC_CACHE_PROFILE = "PREFERENCE_MUSIC_CACHE_PROFILE";
    final public static String PREFERENCE_MUSIC_CACHE_SERIAL = "PREFERENCE_MUSIC_CACHE_SERIAL";
    final private static int PROGRESS_BAR_UPDATE_ON_ANDROID = 2000;
    final public static int THRESHOLD_TIME_BETWEEN_PAUSE_AND_POSSIBLE_CAUSE_FOR_HFP = 10000;
    final private static com.navdy.service.library.log.Logger sLogger;
    private boolean acceptingResumes;
    private com.navdy.hud.app.util.MusicArtworkCache artworkCache;
    private com.squareup.otto.Bus bus;
    private boolean clientSupportsArtworkCaching;
    private java.util.Set currentControls;
    private okio.ByteString currentPhoto;
    private int currentPosition;
    private com.navdy.service.library.events.audio.MusicTrackInfo currentTrack;
    private android.os.Handler handler;
    private volatile long interestingEventTime;
    private java.util.concurrent.atomic.AtomicBoolean isLongPress;
    private boolean isMusicLibraryCachingEnabled;
    private volatile long lastPausedTime;
    private long lastTrackUpdateTime;
    private com.navdy.service.library.events.audio.MusicCapabilitiesResponse musicCapabilities;
    private java.util.Map musicCapabilityTypeMap;
    private com.navdy.hud.app.storage.cache.MessageCache musicCollectionResponseMessageCache;
    private String musicMenuPath;
    private com.navdy.hud.app.framework.music.MusicNotification musicNotification;
    private java.util.Set musicUpdateListeners;
    final private Runnable openNotificationRunnable;
    private com.navdy.hud.app.service.pandora.PandoraManager pandoraManager;
    private boolean pausedByUs;
    private volatile long pausedDueToHfpDetectedTime;
    private com.navdy.service.library.events.audio.MusicDataSource previousDataSource;
    private Runnable progressBarUpdater;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private boolean updateListeners;
    private volatile boolean wasPossiblyPausedDueToHFP;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.MusicManager.class);
        CONTROLS = com.navdy.hud.app.manager.MusicManager$MediaControl.values();
        PAUSED_DUE_TO_HFP_EXPIRY_TIME = java.util.concurrent.TimeUnit.MINUTES.toMillis(10L);
        EMPTY_TRACK = new com.navdy.service.library.events.audio.MusicTrackInfo$Builder().playbackState(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE).name(com.navdy.hud.app.HudApplication.getAppContext().getString(R.string.music_init_title)).author("").album("").isPreviousAllowed(Boolean.valueOf(true)).isNextAllowed(Boolean.valueOf(true)).build();
    }
    
    public MusicManager(com.navdy.hud.app.storage.cache.MessageCache a, com.squareup.otto.Bus a0, android.content.res.Resources a1, com.navdy.hud.app.ui.framework.UIStateManager a2, com.navdy.hud.app.service.pandora.PandoraManager a3, com.navdy.hud.app.util.MusicArtworkCache a4) {
        this.updateListeners = true;
        this.handler = new android.os.Handler();
        this.pausedByUs = false;
        this.acceptingResumes = false;
        this.isMusicLibraryCachingEnabled = false;
        this.previousDataSource = com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE;
        this.wasPossiblyPausedDueToHFP = false;
        this.pausedDueToHfpDetectedTime = 0L;
        this.musicUpdateListeners = (java.util.Set)new java.util.HashSet();
        this.musicMenuPath = null;
        this.progressBarUpdater = (Runnable)new com.navdy.hud.app.manager.MusicManager$2(this);
        this.openNotificationRunnable = (Runnable)new com.navdy.hud.app.manager.MusicManager$3(this);
        this.isLongPress = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.musicCollectionResponseMessageCache = a;
        this.bus = a0;
        this.uiStateManager = a2;
        this.pandoraManager = a3;
        this.currentTrack = EMPTY_TRACK;
        this.currentControls = (java.util.Set)new java.util.HashSet();
        this.currentControls.add(com.navdy.hud.app.manager.MusicManager$MediaControl.PLAY);
        this.musicNotification = new com.navdy.hud.app.framework.music.MusicNotification(this, a0);
        this.artworkCache = a4;
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static okio.ByteString access$100(com.navdy.hud.app.manager.MusicManager a) {
        return a.currentPhoto;
    }
    
    static android.os.Handler access$1000(com.navdy.hud.app.manager.MusicManager a) {
        return a.handler;
    }
    
    static okio.ByteString access$102(com.navdy.hud.app.manager.MusicManager a, okio.ByteString a0) {
        a.currentPhoto = a0;
        return a0;
    }
    
    static com.navdy.hud.app.ui.framework.UIStateManager access$1100(com.navdy.hud.app.manager.MusicManager a) {
        return a.uiStateManager;
    }
    
    static java.util.Set access$1200(com.navdy.hud.app.manager.MusicManager a) {
        return a.musicUpdateListeners;
    }
    
    static void access$200(com.navdy.hud.app.manager.MusicManager a) {
        a.callAlbumArtCallbacks();
    }
    
    static com.squareup.otto.Bus access$300(com.navdy.hud.app.manager.MusicManager a) {
        return a.bus;
    }
    
    static com.navdy.hud.app.util.MusicArtworkCache access$400(com.navdy.hud.app.manager.MusicManager a) {
        return a.artworkCache;
    }
    
    static com.navdy.service.library.events.audio.MusicTrackInfo access$500(com.navdy.hud.app.manager.MusicManager a) {
        return a.currentTrack;
    }
    
    static long access$600(com.navdy.hud.app.manager.MusicManager a) {
        return a.lastTrackUpdateTime;
    }
    
    static int access$700(com.navdy.hud.app.manager.MusicManager a) {
        return a.currentPosition;
    }
    
    static int access$702(com.navdy.hud.app.manager.MusicManager a, int i) {
        a.currentPosition = i;
        return i;
    }
    
    static com.navdy.hud.app.framework.music.MusicNotification access$800(com.navdy.hud.app.manager.MusicManager a) {
        return a.musicNotification;
    }
    
    static Runnable access$900(com.navdy.hud.app.manager.MusicManager a) {
        return a.progressBarUpdater;
    }
    
    private void cacheAlbumArt(com.navdy.service.library.events.audio.MusicArtworkResponse a) {
        String s = a.author;
        label1: {
            label0: {
                if (s != null) {
                    break label0;
                }
                if (a.album != null) {
                    break label0;
                }
                if (a.name != null) {
                    break label0;
                }
                sLogger.d("Not a now playing update, returning");
                break label1;
            }
            this.artworkCache.putArtwork(a.author, a.album, a.name, a.photo.toByteArray());
        }
    }
    
    private void cacheAlbumArt(byte[] a, com.navdy.service.library.events.audio.MusicTrackInfo a0) {
        if (a0 != null) {
            this.artworkCache.putArtwork(a0.author, a0.album, a0.name, a);
        }
    }
    
    private void callAlbumArtCallbacks() {
        if (this.updateListeners) {
            sLogger.d("callAlbumArtCallbacks");
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.manager.MusicManager$5(this), 1);
        } else {
            sLogger.w("callAlbumArtCallbacks but updateListeners was false");
        }
    }
    
    private void callTrackUpdateCallbacks(boolean b) {
        if (this.updateListeners) {
            sLogger.d("callTrackUpdateCallbacks");
            Object a = this.musicUpdateListeners.iterator();
            while(((java.util.Iterator)a).hasNext()) {
                ((com.navdy.hud.app.manager.MusicManager$MusicUpdateListener)((java.util.Iterator)a).next()).onTrackUpdated(this.currentTrack, this.currentControls, b);
            }
        } else {
            sLogger.w("callTrackUpdateCallbacks but updateListeners was false");
        }
    }
    
    private boolean canSeekBackward() {
        boolean b = false;
        boolean b0 = Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!Boolean.TRUE.equals(this.currentTrack.isOnlineStream)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private boolean canSeekForward() {
        boolean b = false;
        boolean b0 = Boolean.TRUE.equals(this.currentTrack.isNextAllowed);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!Boolean.TRUE.equals(this.currentTrack.isOnlineStream)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void checkCache(String s, long j) {
        android.content.SharedPreferences a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
        String s0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
        String s1 = a.getString("PREFERENCE_MUSIC_CACHE_PROFILE", (String)null);
        long j0 = a.getLong("PREFERENCE_MUSIC_CACHE_SERIAL", 0L);
        String s2 = a.getString("PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE", (String)null);
        sLogger.d(new StringBuilder().append("Current Cache details ").append(s1).append(", Source : ").append(s2).append(", Serial : ").append(j0).toString());
        sLogger.d(new StringBuilder().append("Current Profile details ").append(s0).append(", Source : ").append(s).append(", Serial : ").append(j).toString());
        boolean b = android.text.TextUtils.equals((CharSequence)s0, (CharSequence)s1);
        label0: {
            label1: {
                if (!b) {
                    break label1;
                }
                if (j0 != j) {
                    break label1;
                }
                if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)s2)) {
                    break label0;
                }
            }
            sLogger.d("Clearing the Music data cache");
            this.musicCollectionResponseMessageCache.clear();
            a.edit().putString("PREFERENCE_MUSIC_CACHE_PROFILE", s0).putLong("PREFERENCE_MUSIC_CACHE_SERIAL", j).putString("PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE", s).apply();
        }
    }
    
    private com.navdy.service.library.events.audio.MusicDataSource dataSourceToUse() {
        com.navdy.service.library.events.audio.MusicDataSource a = null;
        com.navdy.service.library.events.audio.MusicTrackInfo a0 = this.currentTrack;
        label1: {
            label0: {
                if (a0 == null) {
                    break label0;
                }
                if (a0.dataSource == null) {
                    break label0;
                }
                if (com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE.equals(a0.dataSource)) {
                    break label0;
                }
                a = a0.dataSource;
                break label1;
            }
            a = this.previousDataSource;
        }
        return a;
    }
    
    private void postKeyDownUp(com.navdy.service.library.events.input.MediaRemoteKey a) {
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.input.MediaRemoteKeyEvent(a, com.navdy.service.library.events.input.KeyEvent.KEY_DOWN)));
        this.handler.postDelayed((Runnable)new com.navdy.hud.app.manager.MusicManager$6(this, a), 100L);
    }
    
    private void recordAnalytics(android.view.KeyEvent a, com.navdy.hud.app.manager.MusicManager$MediaControl a0, boolean b) {
        label0: if (a != null) {
            switch(a.getAction()) {
                case 1: {
                    if (!b) {
                        break label0;
                    }
                    if (this.isLongPress.get()) {
                        break label0;
                    }
                    switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[a0.ordinal()]) {
                        case 2: {
                            com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Previous");
                            break label0;
                        }
                        case 1: {
                            com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Next");
                            break label0;
                        }
                        default: {
                            break label0;
                        }
                    }
                }
                case 0: {
                    if (!a.isLongPress()) {
                        break label0;
                    }
                    if (this.isLongPress.get()) {
                        break label0;
                    }
                    switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[a0.ordinal()]) {
                        case 2: {
                            com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Rewind");
                            break label0;
                        }
                        case 1: {
                            com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Fast-forward");
                            break label0;
                        }
                        default: {
                            break label0;
                        }
                    }
                }
            }
        } else {
            switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[a0.ordinal()]) {
                case 4: {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Pause");
                    break;
                }
                case 3: {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordMusicAction("Play");
                    break;
                }
            }
        }
    }
    
    private void requestArtwork(com.navdy.service.library.events.audio.MusicTrackInfo a) {
        sLogger.d(new StringBuilder().append("requestArtwork: ").append(a).toString());
        boolean b = android.text.TextUtils.isEmpty((CharSequence)a.author);
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)a.album)) {
                    break label0;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)a.name)) {
                    break label0;
                }
                this.currentPhoto = null;
                this.callAlbumArtCallbacks();
                sLogger.d("Empty track, returning");
                break label1;
            }
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.manager.MusicManager$1(this, a), 22);
        }
    }
    
    private void requestPhotoUpdates(boolean b) {
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!this.clientSupportsArtworkCaching) {
                    break label0;
                }
                sLogger.d("client supports caching, no need to request photo updates");
                break label1;
            }
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.photo.PhotoUpdatesRequest$Builder().start(Boolean.valueOf(b)).photoType(com.navdy.service.library.events.photo.PhotoType.PHOTO_ALBUM_ART).build()));
        }
    }
    
    private boolean runAndroidAction(com.navdy.service.library.events.audio.MusicEvent$Action a) {
        com.navdy.service.library.events.audio.MusicDataSource a0 = this.dataSourceToUse();
        boolean b = com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_PLAY.equals(a);
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(a0)) {
                    break label0;
                }
                this.pandoraManager.startAndPlay();
                break label1;
            }
            sLogger.d(new StringBuilder().append("Media action: ").append(a).toString());
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.MusicEvent$Builder().action(a).dataSource(a0).build()));
        }
        return true;
    }
    
    private boolean sendMediaKeyEvent(com.navdy.hud.app.manager.MusicManager$MediaControl a, boolean b) {
        com.navdy.service.library.events.input.MediaRemoteKey a0 = null;
        switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[a.ordinal()]) {
            case 3: case 4: {
                a0 = com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY;
                break;
            }
            case 2: {
                a0 = com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_PREV;
                break;
            }
            case 1: {
                a0 = com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_NEXT;
                break;
            }
            default: {
                a0 = null;
            }
        }
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.input.MediaRemoteKeyEvent$Builder().key(a0).action(b ? com.navdy.service.library.events.input.KeyEvent.KEY_DOWN : com.navdy.service.library.events.input.KeyEvent.KEY_UP).build()));
        return true;
    }
    
    public static boolean tryingToPlay(com.navdy.service.library.events.audio.MusicPlaybackState a) {
        boolean b = false;
        boolean b0 = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE.equals(a);
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_CONNECTING.equals(a)) {
                        break label1;
                    }
                    if (com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_ERROR.equals(a)) {
                        break label1;
                    }
                    if (com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED.equals(a)) {
                        break label1;
                    }
                    if (!com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_STOPPED.equals(a)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void updateControls(com.navdy.service.library.events.audio.MusicTrackInfo a) {
        this.currentControls = (java.util.Set)new java.util.HashSet();
        if (Boolean.TRUE.equals(a.isPreviousAllowed)) {
            this.currentControls.add(com.navdy.hud.app.manager.MusicManager$MediaControl.PREVIOUS);
        }
        if (com.navdy.hud.app.manager.MusicManager.tryingToPlay(a.playbackState)) {
            this.currentControls.add(com.navdy.hud.app.manager.MusicManager$MediaControl.PAUSE);
        } else {
            this.currentControls.add(com.navdy.hud.app.manager.MusicManager$MediaControl.PLAY);
        }
        if (Boolean.TRUE.equals(a.isNextAllowed)) {
            this.currentControls.add(com.navdy.hud.app.manager.MusicManager$MediaControl.NEXT);
        }
    }
    
    public void acceptResumes() {
        this.acceptingResumes = true;
        sLogger.d("Now accepting music resume events");
    }
    
    public void addMusicUpdateListener(com.navdy.hud.app.manager.MusicManager$MusicUpdateListener a) {
        sLogger.d("addMusicUpdateListener");
        if (this.musicUpdateListeners.contains(a)) {
            sLogger.w("Tried to add a listener that's already listening");
        } else {
            if (this.musicUpdateListeners.size() == 0) {
                sLogger.d("Enabling album art updates (PhotoUpdate)");
                this.requestPhotoUpdates(true);
            }
            this.musicUpdateListeners.add(a);
        }
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.manager.MusicManager$4(this, a), 1);
    }
    
    public void executeMediaControl(com.navdy.hud.app.manager.MusicManager$MediaControl a, boolean b) {
        com.navdy.service.library.events.DeviceInfo$Platform a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        label0: if (a0 != null) {
            this.recordAnalytics((android.view.KeyEvent)null, a, b);
            switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform[a0.ordinal()]) {
                case 2: {
                    this.sendMediaKeyEvent(a, true);
                    this.sendMediaKeyEvent(a, false);
                    break;
                }
                case 1: {
                    switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[a.ordinal()]) {
                        case 4: {
                            this.runAndroidAction(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_PAUSE);
                            break label0;
                        }
                        case 3: {
                            this.runAndroidAction(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_PLAY);
                            break label0;
                        }
                        default: {
                            break label0;
                        }
                    }
                }
            }
        }
    }
    
    public java.util.List getCollectionTypesForSource(com.navdy.service.library.events.audio.MusicCollectionSource a) {
        return (java.util.List)this.musicCapabilityTypeMap.get(a);
    }
    
    public java.util.Set getCurrentControls() {
        return this.currentControls;
    }
    
    public int getCurrentPosition() {
        return this.currentPosition;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo getCurrentTrack() {
        return this.currentTrack;
    }
    
    public com.navdy.service.library.events.audio.MusicCapabilitiesResponse getMusicCapabilities() {
        return this.musicCapabilities;
    }
    
    public String getMusicMenuPath() {
        return this.musicMenuPath;
    }
    
    public void handleKeyEvent(android.view.KeyEvent a, com.navdy.hud.app.manager.MusicManager$MediaControl a0, boolean b) {
        com.navdy.service.library.events.DeviceInfo$Platform a1 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        label0: if (a1 != null && com.navdy.hud.app.manager.InputManager.isCenterKey(a.getKeyCode())) {
            this.recordAnalytics(a, a0, b);
            switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform[a1.ordinal()]) {
                case 2: {
                    switch(a.getAction()) {
                        case 1: {
                            if (!b) {
                                break label0;
                            }
                            this.sendMediaKeyEvent(a0, false);
                            break label0;
                        }
                        case 0: {
                            if (b) {
                                break label0;
                            }
                            com.navdy.hud.app.manager.MusicManager$MediaControl a2 = com.navdy.hud.app.manager.MusicManager$MediaControl.PLAY;
                            label1: {
                                label2: {
                                    if (a0 == a2) {
                                        break label2;
                                    }
                                    if (a0 != com.navdy.hud.app.manager.MusicManager$MediaControl.PAUSE) {
                                        break label1;
                                    }
                                }
                                this.sendMediaKeyEvent(a0, true);
                                this.sendMediaKeyEvent(a0, false);
                                break label0;
                            }
                            this.sendMediaKeyEvent(a0, true);
                            break label0;
                        }
                        default: {
                            break label0;
                        }
                    }
                }
                case 1: {
                    switch(a.getAction()) {
                        case 1: {
                            if (!b) {
                                break label0;
                            }
                            if (this.isLongPress.compareAndSet(true, false)) {
                                switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[a0.ordinal()]) {
                                    case 2: {
                                        this.runAndroidAction(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_REWIND_STOP);
                                        break label0;
                                    }
                                    case 1: {
                                        this.runAndroidAction(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_FAST_FORWARD_STOP);
                                        break label0;
                                    }
                                    default: {
                                        break label0;
                                    }
                                }
                            } else {
                                switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[a0.ordinal()]) {
                                    case 2: {
                                        if (Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed)) {
                                            this.runAndroidAction(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_PREVIOUS);
                                            break label0;
                                        } else {
                                            sLogger.w("Previous song action is not allowed");
                                            break label0;
                                        }
                                    }
                                    case 1: {
                                        if (Boolean.TRUE.equals(this.currentTrack.isNextAllowed)) {
                                            this.runAndroidAction(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_NEXT);
                                            break label0;
                                        } else {
                                            sLogger.w("Next song action is not allowed");
                                            break label0;
                                        }
                                    }
                                    default: {
                                        break label0;
                                    }
                                }
                            }
                        }
                        case 0: {
                            if (!a.isLongPress()) {
                                break label0;
                            }
                            if (!this.isLongPress.compareAndSet(false, true)) {
                                break label0;
                            }
                            switch(com.navdy.hud.app.manager.MusicManager$7.$SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[a0.ordinal()]) {
                                case 2: {
                                    if (this.canSeekForward()) {
                                        this.runAndroidAction(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_REWIND_START);
                                        break label0;
                                    } else {
                                        sLogger.w("Rewind is not allowed");
                                        break label0;
                                    }
                                }
                                case 1: {
                                    if (this.canSeekBackward()) {
                                        this.runAndroidAction(com.navdy.service.library.events.audio.MusicEvent$Action.MUSIC_ACTION_FAST_FORWARD_START);
                                        break label0;
                                    } else {
                                        sLogger.w("Fast-forward is not allowed");
                                        break label0;
                                    }
                                }
                                default: {
                                    break label0;
                                }
                            }
                        }
                        default: {
                            break label0;
                        }
                    }
                }
            }
        }
    }
    
    public boolean hasMusicCapabilities() {
        boolean b = false;
        com.navdy.service.library.events.audio.MusicCapabilitiesResponse a = this.musicCapabilities;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.musicCapabilities.capabilities == null) {
                        break label1;
                    }
                    if (this.musicCapabilities.capabilities.size() != 0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void initialize() {
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.MusicTrackInfoRequest()));
        sLogger.d("Request for music track info sent to the client.");
    }
    
    public boolean isMusicLibraryCachingEnabled() {
        return this.isMusicLibraryCachingEnabled;
    }
    
    public boolean isPandoraDataSource() {
        return com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource);
    }
    
    public boolean isShuffling() {
        boolean b = false;
        com.navdy.service.library.events.audio.MusicShuffleMode a = this.currentTrack.shuffleMode;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.currentTrack.shuffleMode == com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN) {
                        break label1;
                    }
                    if (this.currentTrack.shuffleMode != com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public void onConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (a.state == com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED) {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.musicNotification.getId());
            this.currentTrack = EMPTY_TRACK;
            this.currentPhoto = null;
            this.currentControls = null;
            this.updateListeners = true;
            this.callAlbumArtCallbacks();
            this.callTrackUpdateCallbacks(false);
            this.musicCapabilities = null;
            this.musicCapabilityTypeMap = null;
            this.musicMenuPath = null;
            com.navdy.hud.app.ui.component.mainmenu.MusicMenu2.clearMenuData();
        }
    }
    
    public void onDeviceInfo(com.navdy.service.library.events.DeviceInfo a) {
        sLogger.d(new StringBuilder().append("onDeviceInfo - ").append(a).toString());
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.MusicCapabilitiesRequest$Builder().build()));
        com.navdy.service.library.events.Capabilities a0 = a.capabilities;
        com.navdy.service.library.events.DeviceInfo$Platform a1 = a.platform;
        com.navdy.service.library.events.DeviceInfo$Platform a2 = com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS;
        label0: {
            label1: {
                if (a1 == a2) {
                    break label1;
                }
                if (a0 == null) {
                    break label0;
                }
                if (!((Boolean)com.squareup.wire.Wire.get(a0.musicArtworkCache, Boolean.valueOf(false))).booleanValue()) {
                    break label0;
                }
            }
            this.clientSupportsArtworkCaching = true;
        }
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        sLogger.d(new StringBuilder().append("onDriverProfileChanged - ").append(a).toString());
        if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isDefaultProfile() && this.musicUpdateListeners.size() > 0) {
            this.requestPhotoUpdates(true);
        }
    }
    
    public void onLocalSpeechRequest(com.navdy.hud.app.event.LocalSpeechRequest a) {
        long j = android.os.SystemClock.elapsedRealtime();
        if (!this.wasPossiblyPausedDueToHFP) {
            sLogger.d("Recording the speech request as an interesting event");
            this.interestingEventTime = j;
        }
    }
    
    public void onMediaKeyEventFromClient(com.navdy.service.library.events.input.MediaRemoteKeyEvent a) {
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a));
    }
    
    public void onMusicArtworkResponse(com.navdy.service.library.events.audio.MusicArtworkResponse a) {
        sLogger.d(new StringBuilder().append("onMusicArtworkResponse ").append(a).toString());
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.photo != null) {
                        break label0;
                    }
                }
                this.currentPhoto = null;
                break label2;
            }
            this.cacheAlbumArt(a);
            if (android.text.TextUtils.equals((CharSequence)com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack), (CharSequence)com.navdy.service.library.util.MusicDataUtils.songIdentifierFromArtworkResponse(a))) {
                this.currentPhoto = a.photo;
            }
        }
        this.callAlbumArtCallbacks();
    }
    
    public void onMusicCapabilitiesResponse(com.navdy.service.library.events.audio.MusicCapabilitiesResponse a) {
        long j = 0L;
        sLogger.d(new StringBuilder().append("onMusicCapabilitiesResponse ").append(a).toString());
        this.musicCapabilities = a;
        this.isMusicLibraryCachingEnabled = false;
        boolean b = this.hasMusicCapabilities();
        String s = null;
        if (b) {
            this.musicCapabilityTypeMap = (java.util.Map)new java.util.HashMap();
            java.util.Iterator a0 = this.musicCapabilities.capabilities.iterator();
            s = null;
            j = -1L;
            Object a1 = a0;
            while(((java.util.Iterator)a1).hasNext()) {
                com.navdy.service.library.events.audio.MusicCapability a2 = (com.navdy.service.library.events.audio.MusicCapability)((java.util.Iterator)a1).next();
                this.musicCapabilityTypeMap.put(a2.collectionSource, a2.collectionTypes);
                if (!this.isMusicLibraryCachingEnabled && a2.serial_number != null) {
                    this.isMusicLibraryCachingEnabled = true;
                    s = a2.collectionSource.name();
                    j = a2.serial_number.longValue();
                    sLogger.d(new StringBuilder().append("Enabling caching with Source ").append(s).append(", Serial :").append(j).toString());
                }
            }
        } else {
            j = -1L;
        }
        if (this.isMusicLibraryCachingEnabled) {
            this.checkCache(s, j);
        } else {
            sLogger.d("Cache is not enabled");
        }
    }
    
    public void onMusicCollectionSourceUpdate(com.navdy.service.library.events.audio.MusicCollectionSourceUpdate a) {
        sLogger.d(new StringBuilder().append("onMusicCollectionSourceUpdate ").append(a).toString());
        if (a.collectionSource != null && a.serial_number != null) {
            this.checkCache(a.collectionSource.name(), a.serial_number.longValue());
        }
    }
    
    public void onPhoneEvent(com.navdy.service.library.events.callcontrol.PhoneEvent a) {
        label1: if (a != null && a.status != null) {
            sLogger.d(new StringBuilder().append("PhoneEvent : new status ").append(a.status).toString());
            com.navdy.service.library.events.callcontrol.PhoneStatus a0 = a.status;
            com.navdy.service.library.events.callcontrol.PhoneStatus a1 = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING;
            label2: {
                if (a0 == a1) {
                    break label2;
                }
                if (a.status == com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING) {
                    break label2;
                }
                if (a.status != com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK) {
                    break label1;
                }
            }
            long j = android.os.SystemClock.elapsedRealtime();
            if (!this.wasPossiblyPausedDueToHFP) {
                com.navdy.service.library.events.audio.MusicTrackInfo a2 = this.currentTrack;
                label0: {
                    if (a2 == null) {
                        break label0;
                    }
                    if (!this.currentTrack.playbackState.equals(com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED)) {
                        break label0;
                    }
                    if (this.lastPausedTime - j >= 10000L) {
                        break label0;
                    }
                    sLogger.d("Current state is paused recently, possibly due to call");
                    this.wasPossiblyPausedDueToHFP = true;
                    this.pausedDueToHfpDetectedTime = j;
                    break label1;
                }
                sLogger.d("Recording the phone event as interesting event");
                this.interestingEventTime = j;
            }
        }
    }
    
    public void onPhotoUpdate(com.navdy.service.library.events.photo.PhotoUpdate a) {
        sLogger.d(new StringBuilder().append("onPhotoUpdate ").append(a).toString());
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.photo != null) {
                        break label0;
                    }
                }
                this.currentPhoto = null;
                break label2;
            }
            this.currentPhoto = a.photo;
            String s = com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack);
            if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)a.identifier)) {
                this.cacheAlbumArt(this.currentPhoto.toByteArray(), this.currentTrack);
            } else {
                sLogger.w(new StringBuilder().append("Received PhotoUpdate with wrong identifier: ").append(s).append(" != ").append(a.identifier).toString());
            }
        }
        this.callAlbumArtCallbacks();
    }
    
    public void onResumeMusicRequest(com.navdy.service.library.events.audio.ResumeMusicRequest a) {
        this.softResume();
    }
    
    public void onTrackInfo(com.navdy.service.library.events.audio.MusicTrackInfo a) {
        sLogger.d(new StringBuilder().append("updateState ").append(a).toString());
        boolean b = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING.equals(a.playbackState);
        boolean b0 = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING.equals(this.currentTrack.playbackState);
        if (a.collectionSource == com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN) {
            this.setMusicMenuPath((String)null);
        }
        label44: {
            boolean b1 = false;
            boolean b2 = false;
            boolean b3 = false;
            boolean b4 = false;
            boolean b5 = false;
            boolean b6 = false;
            boolean b7 = false;
            boolean b8 = false;
            boolean b9 = false;
            boolean b10 = false;
            label43: {
                if (b) {
                    break label43;
                }
                if (!b0) {
                    break label43;
                }
                if (com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource)) {
                    break label43;
                }
                if (!com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(a.dataSource)) {
                    break label43;
                }
                sLogger.d("Ignoring update from non playing Pandora");
                break label44;
            }
            boolean b11 = !android.text.TextUtils.equals((CharSequence)com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(a), (CharSequence)com.navdy.service.library.util.MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack));
            if (b11 && this.clientSupportsArtworkCaching) {
                this.requestArtwork(a);
            }
            label42: {
                label40: {
                    label41: {
                        if (!b) {
                            break label41;
                        }
                        if (b11) {
                            break label40;
                        }
                    }
                    b1 = false;
                    break label42;
                }
                b1 = true;
            }
            label39: {
                label37: {
                    label38: {
                        if (!b) {
                            break label38;
                        }
                        if (!b0) {
                            break label37;
                        }
                    }
                    b2 = false;
                    break label39;
                }
                b2 = true;
            }
            boolean b12 = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED.equals(a.playbackState);
            boolean b13 = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState);
            label36: {
                label34: {
                    label35: {
                        if (!b12) {
                            break label35;
                        }
                        if (!b13) {
                            break label34;
                        }
                    }
                    b3 = false;
                    break label36;
                }
                b3 = true;
            }
            label32: {
                label33: {
                    if (b2) {
                        break label33;
                    }
                    if (!b3) {
                        break label32;
                    }
                }
                if (com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS.equals(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                    this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a));
                }
            }
            if (b2) {
                sLogger.d("Music started playing");
            }
            if (b2 && this.pausedByUs) {
                sLogger.d("User started playback after we paused - setting pauseByUs = false");
                this.pausedByUs = false;
            }
            if (b3) {
                sLogger.d("New state is paused");
                long j = android.os.SystemClock.elapsedRealtime();
                if (!this.wasPossiblyPausedDueToHFP) {
                    if (j - this.interestingEventTime >= 10000L) {
                        this.lastPausedTime = j;
                    } else {
                        sLogger.d("Interesting event happened just before the song was paused, possibly playing through HFP");
                        this.wasPossiblyPausedDueToHFP = true;
                        this.pausedDueToHfpDetectedTime = j;
                    }
                }
            }
            this.lastTrackUpdateTime = android.os.SystemClock.elapsedRealtime();
            label31: {
                label29: {
                    label30: {
                        if (!b2) {
                            break label30;
                        }
                        if (!this.wasPossiblyPausedDueToHFP) {
                            break label30;
                        }
                        if (this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime < PAUSED_DUE_TO_HFP_EXPIRY_TIME) {
                            break label29;
                        }
                    }
                    b4 = false;
                    break label31;
                }
                b4 = true;
            }
            if (b4) {
                sLogger.d("Should suppress Notification");
            }
            label27: {
                label28: {
                    if (b4) {
                        break label28;
                    }
                    if (this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime <= PAUSED_DUE_TO_HFP_EXPIRY_TIME) {
                        break label27;
                    }
                }
                this.wasPossiblyPausedDueToHFP = false;
                this.pausedDueToHfpDetectedTime = 0L;
            }
            if (a.dataSource != null && !com.navdy.service.library.events.audio.MusicDataSource.MUSIC_SOURCE_NONE.equals(a.dataSource)) {
                this.previousDataSource = a.dataSource;
            }
            this.updateControls(a);
            Integer a0 = a.duration;
            label26: {
                label24: {
                    label25: {
                        if (a0 == null) {
                            break label25;
                        }
                        if (a.duration.intValue() <= 0) {
                            break label25;
                        }
                        if (a.currentPosition != null) {
                            break label24;
                        }
                    }
                    b5 = false;
                    break label26;
                }
                b5 = true;
            }
            if (b5) {
                this.currentPosition = a.currentPosition.intValue();
                this.musicNotification.updateProgressBar(this.currentPosition);
            }
            if (com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android.equals(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                this.handler.removeCallbacks(this.progressBarUpdater);
                if (com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING.equals(a.playbackState) && b5) {
                    this.handler.postDelayed(this.progressBarUpdater, 2000L);
                }
            }
            boolean b14 = com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState);
            label23: {
                label21: {
                    label22: {
                        if (b14) {
                            break label22;
                        }
                        if (!com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_NONE.equals(this.currentTrack.playbackState)) {
                            break label21;
                        }
                    }
                    b6 = true;
                    break label23;
                }
                b6 = false;
            }
            label20: {
                label18: {
                    label19: {
                        if (!b3) {
                            break label19;
                        }
                        if (!b11) {
                            break label18;
                        }
                    }
                    b7 = false;
                    break label20;
                }
                b7 = true;
            }
            label17: {
                label15: {
                    label16: {
                        if (!b5) {
                            break label16;
                        }
                        if (a.currentPosition.equals(a.duration)) {
                            break label15;
                        }
                    }
                    b8 = false;
                    break label17;
                }
                b8 = true;
            }
            label14: {
                label11: {
                    label13: {
                        if (!b12) {
                            break label13;
                        }
                        if (b8) {
                            break label13;
                        }
                        if (b7) {
                            break label11;
                        }
                        label12: {
                            if (!b13) {
                                break label12;
                            }
                            if (b11) {
                                break label11;
                            }
                        }
                        if (b6) {
                            break label11;
                        }
                    }
                    b9 = false;
                    break label14;
                }
                b9 = true;
            }
            boolean b15 = this.currentTrack.shuffleMode != a.shuffleMode;
            label10: {
                label8: {
                    label9: {
                        if (b) {
                            break label9;
                        }
                        if (b9) {
                            break label9;
                        }
                        if (!b15) {
                            break label8;
                        }
                    }
                    b10 = true;
                    break label10;
                }
                b10 = false;
            }
            this.updateListeners = b10;
            if (this.updateListeners) {
                boolean b16 = false;
                boolean b17 = false;
                this.currentTrack = a;
                label7: {
                    label5: {
                        label6: {
                            if (b1) {
                                break label6;
                            }
                            if (!b2) {
                                break label5;
                            }
                        }
                        b16 = true;
                        break label7;
                    }
                    b16 = false;
                }
                label4: {
                    label2: {
                        label3: {
                            if (b4) {
                                break label3;
                            }
                            if (!com.navdy.hud.app.framework.glance.GlanceHelper.isMusicNotificationEnabled()) {
                                break label3;
                            }
                            if (b16) {
                                break label2;
                            }
                        }
                        b17 = false;
                        break label4;
                    }
                    b17 = true;
                }
                label0: {
                    label1: {
                        if (b16) {
                            break label1;
                        }
                        if (b9) {
                            break label1;
                        }
                        if (!b15) {
                            break label0;
                        }
                    }
                    this.callTrackUpdateCallbacks(b17);
                    if (!this.clientSupportsArtworkCaching) {
                        this.callAlbumArtCallbacks();
                    }
                }
                if (b17) {
                    sLogger.d(new StringBuilder().append("Showing notification New Song ? : ").append(b1).append(", New state playing : ").append(b2).toString());
                    this.handler.removeCallbacks(this.openNotificationRunnable);
                    this.handler.post(this.openNotificationRunnable);
                }
            } else {
                sLogger.d("Ignoring updates");
            }
        }
    }
    
    public void removeMusicUpdateListener(com.navdy.hud.app.manager.MusicManager$MusicUpdateListener a) {
        sLogger.d("removeMusicUpdateListener");
        if (this.musicUpdateListeners.contains(a)) {
            this.musicUpdateListeners.remove(a);
            if (this.musicUpdateListeners.size() == 0) {
                sLogger.d("Disabling album art updates");
                this.requestPhotoUpdates(false);
            }
        } else {
            sLogger.w("Tried to remove a non-existent listener");
        }
        sLogger.d("album art listeners:");
        Object a0 = this.musicUpdateListeners.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            Object a1 = ((java.util.Iterator)a0).next();
            sLogger.v(new StringBuilder().append("- ").append(a1).toString());
        }
    }
    
    public void setMusicMenuPath(String s) {
        sLogger.d(new StringBuilder().append("setMusicMenuPath: ").append(s).toString());
        this.musicMenuPath = s;
    }
    
    public void showMusicNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification((com.navdy.hud.app.framework.notifications.INotification)this.musicNotification);
    }
    
    public void softPause() {
        if (this.currentTrack.playbackState == com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING) {
            sLogger.d("Pausing music");
            this.pausedByUs = true;
            this.acceptingResumes = false;
            this.postKeyDownUp(com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
        }
    }
    
    public void softResume() {
        if (this.pausedByUs && this.acceptingResumes) {
            this.postKeyDownUp(com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
            this.pausedByUs = false;
            sLogger.d("Unpausing music (resuming)");
        }
    }
}
