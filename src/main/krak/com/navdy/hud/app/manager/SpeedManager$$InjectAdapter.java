package com.navdy.hud.app.manager;

final public class SpeedManager$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding driverProfileManager;
    
    public SpeedManager$$InjectAdapter() {
        super("com.navdy.hud.app.manager.SpeedManager", "members/com.navdy.hud.app.manager.SpeedManager", false, com.navdy.hud.app.manager.SpeedManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.driverProfileManager = a.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.manager.SpeedManager.class, (this).getClass().getClassLoader());
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.manager.SpeedManager.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.manager.SpeedManager get() {
        com.navdy.hud.app.manager.SpeedManager a = new com.navdy.hud.app.manager.SpeedManager();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.driverProfileManager);
        a0.add(this.bus);
    }
    
    public void injectMembers(com.navdy.hud.app.manager.SpeedManager a) {
        a.driverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager)this.driverProfileManager.get();
        a.bus = (com.squareup.otto.Bus)this.bus.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.manager.SpeedManager)a);
    }
}
