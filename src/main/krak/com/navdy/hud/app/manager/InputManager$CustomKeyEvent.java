package com.navdy.hud.app.manager;


public enum InputManager$CustomKeyEvent {
    LEFT(0),
    RIGHT(1),
    SELECT(2),
    LONG_PRESS(3),
    POWER_BUTTON_CLICK(4),
    POWER_BUTTON_DOUBLE_CLICK(5),
    POWER_BUTTON_LONG_PRESS(6);

    private int value;
    InputManager$CustomKeyEvent(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class InputManager$CustomKeyEvent extends Enum {
//    final private static com.navdy.hud.app.manager.InputManager$CustomKeyEvent[] $VALUES;
//    final public static com.navdy.hud.app.manager.InputManager$CustomKeyEvent LEFT;
//    final public static com.navdy.hud.app.manager.InputManager$CustomKeyEvent LONG_PRESS;
//    final public static com.navdy.hud.app.manager.InputManager$CustomKeyEvent POWER_BUTTON_CLICK;
//    final public static com.navdy.hud.app.manager.InputManager$CustomKeyEvent POWER_BUTTON_DOUBLE_CLICK;
//    final public static com.navdy.hud.app.manager.InputManager$CustomKeyEvent POWER_BUTTON_LONG_PRESS;
//    final public static com.navdy.hud.app.manager.InputManager$CustomKeyEvent RIGHT;
//    final public static com.navdy.hud.app.manager.InputManager$CustomKeyEvent SELECT;
//    
//    static {
//        LEFT = new com.navdy.hud.app.manager.InputManager$CustomKeyEvent("LEFT", 0);
//        RIGHT = new com.navdy.hud.app.manager.InputManager$CustomKeyEvent("RIGHT", 1);
//        SELECT = new com.navdy.hud.app.manager.InputManager$CustomKeyEvent("SELECT", 2);
//        LONG_PRESS = new com.navdy.hud.app.manager.InputManager$CustomKeyEvent("LONG_PRESS", 3);
//        POWER_BUTTON_CLICK = new com.navdy.hud.app.manager.InputManager$CustomKeyEvent("POWER_BUTTON_CLICK", 4);
//        POWER_BUTTON_DOUBLE_CLICK = new com.navdy.hud.app.manager.InputManager$CustomKeyEvent("POWER_BUTTON_DOUBLE_CLICK", 5);
//        POWER_BUTTON_LONG_PRESS = new com.navdy.hud.app.manager.InputManager$CustomKeyEvent("POWER_BUTTON_LONG_PRESS", 6);
//        com.navdy.hud.app.manager.InputManager$CustomKeyEvent[] a = new com.navdy.hud.app.manager.InputManager$CustomKeyEvent[7];
//        a[0] = LEFT;
//        a[1] = RIGHT;
//        a[2] = SELECT;
//        a[3] = LONG_PRESS;
//        a[4] = POWER_BUTTON_CLICK;
//        a[5] = POWER_BUTTON_DOUBLE_CLICK;
//        a[6] = POWER_BUTTON_LONG_PRESS;
//        $VALUES = a;
//    }
//    
//    private InputManager$CustomKeyEvent(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.manager.InputManager$CustomKeyEvent valueOf(String s) {
//        return (com.navdy.hud.app.manager.InputManager$CustomKeyEvent)Enum.valueOf(com.navdy.hud.app.manager.InputManager$CustomKeyEvent.class, s);
//    }
//    
//    public static com.navdy.hud.app.manager.InputManager$CustomKeyEvent[] values() {
//        return $VALUES.clone();
//    }
//}
//