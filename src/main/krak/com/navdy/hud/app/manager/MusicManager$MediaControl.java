package com.navdy.hud.app.manager;


public enum MusicManager$MediaControl {
    PLAY(0),
    PAUSE(1),
    NEXT(2),
    PREVIOUS(3),
    MUSIC_MENU(4),
    MUSIC_MENU_DEEP(5);

    private int value;
    MusicManager$MediaControl(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MusicManager$MediaControl extends Enum {
//    final private static com.navdy.hud.app.manager.MusicManager$MediaControl[] $VALUES;
//    final public static com.navdy.hud.app.manager.MusicManager$MediaControl MUSIC_MENU;
//    final public static com.navdy.hud.app.manager.MusicManager$MediaControl MUSIC_MENU_DEEP;
//    final public static com.navdy.hud.app.manager.MusicManager$MediaControl NEXT;
//    final public static com.navdy.hud.app.manager.MusicManager$MediaControl PAUSE;
//    final public static com.navdy.hud.app.manager.MusicManager$MediaControl PLAY;
//    final public static com.navdy.hud.app.manager.MusicManager$MediaControl PREVIOUS;
//    
//    static {
//        PLAY = new com.navdy.hud.app.manager.MusicManager$MediaControl("PLAY", 0);
//        PAUSE = new com.navdy.hud.app.manager.MusicManager$MediaControl("PAUSE", 1);
//        NEXT = new com.navdy.hud.app.manager.MusicManager$MediaControl("NEXT", 2);
//        PREVIOUS = new com.navdy.hud.app.manager.MusicManager$MediaControl("PREVIOUS", 3);
//        MUSIC_MENU = new com.navdy.hud.app.manager.MusicManager$MediaControl("MUSIC_MENU", 4);
//        MUSIC_MENU_DEEP = new com.navdy.hud.app.manager.MusicManager$MediaControl("MUSIC_MENU_DEEP", 5);
//        com.navdy.hud.app.manager.MusicManager$MediaControl[] a = new com.navdy.hud.app.manager.MusicManager$MediaControl[6];
//        a[0] = PLAY;
//        a[1] = PAUSE;
//        a[2] = NEXT;
//        a[3] = PREVIOUS;
//        a[4] = MUSIC_MENU;
//        a[5] = MUSIC_MENU_DEEP;
//        $VALUES = a;
//    }
//    
//    private MusicManager$MediaControl(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.manager.MusicManager$MediaControl valueOf(String s) {
//        return (com.navdy.hud.app.manager.MusicManager$MediaControl)Enum.valueOf(com.navdy.hud.app.manager.MusicManager$MediaControl.class, s);
//    }
//    
//    public static com.navdy.hud.app.manager.MusicManager$MediaControl[] values() {
//        return $VALUES.clone();
//    }
//}
//