package com.navdy.hud.app.manager;

abstract public interface InputManager$IInputHandler {
    abstract public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler();
    
    
    abstract public boolean onGesture(com.navdy.service.library.events.input.GestureEvent arg);
    
    
    abstract public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent arg);
}
