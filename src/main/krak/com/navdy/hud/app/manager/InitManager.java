package com.navdy.hud.app.manager;

public class InitManager {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private Runnable checkConnectionServiceReady;
    private com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    private android.os.Handler handler;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.InitManager.class);
    }
    
    public InitManager(com.squareup.otto.Bus a, com.navdy.hud.app.service.ConnectionHandler a0) {
        this.checkConnectionServiceReady = (Runnable)new com.navdy.hud.app.manager.InitManager$1(this);
        this.bus = a;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.bus.register(this);
        this.connectionHandler = a0;
    }
    
    static com.navdy.hud.app.service.ConnectionHandler access$000(com.navdy.hud.app.manager.InitManager a) {
        return a.connectionHandler;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.squareup.otto.Bus access$200(com.navdy.hud.app.manager.InitManager a) {
        return a.bus;
    }
    
    static android.os.Handler access$300(com.navdy.hud.app.manager.InitManager a) {
        return a.handler;
    }
    
    public void onBluetoothChanged(com.navdy.hud.app.event.InitEvents$BluetoothStateChanged a) {
        this.handler.post(this.checkConnectionServiceReady);
    }
    
    public void onConnectionService(com.navdy.hud.app.event.InitEvents$ConnectionServiceStarted a) {
        this.handler.post(this.checkConnectionServiceReady);
    }
    
    public void start() {
        this.handler.post((Runnable)new com.navdy.hud.app.manager.InitManager$PhaseEmitter(this, com.navdy.hud.app.event.InitEvents$Phase.PRE_USER_INTERACTION));
        this.handler.post(this.checkConnectionServiceReady);
    }
}
