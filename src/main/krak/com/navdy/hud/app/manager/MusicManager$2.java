package com.navdy.hud.app.manager;

class MusicManager$2 implements Runnable {
    final com.navdy.hud.app.manager.MusicManager this$0;
    
    MusicManager$2(com.navdy.hud.app.manager.MusicManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.service.library.events.audio.MusicPlaybackState.PLAYBACK_PLAYING.equals(com.navdy.hud.app.manager.MusicManager.access$500(this.this$0).playbackState) && com.navdy.hud.app.manager.MusicManager.access$500(this.this$0).currentPosition != null) {
            int i = (int)(android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.manager.MusicManager.access$600(this.this$0));
            com.navdy.hud.app.manager.MusicManager.access$702(this.this$0, com.navdy.hud.app.manager.MusicManager.access$500(this.this$0).currentPosition.intValue() + i);
            com.navdy.hud.app.manager.MusicManager.access$800(this.this$0).updateProgressBar(com.navdy.hud.app.manager.MusicManager.access$700(this.this$0));
            com.navdy.hud.app.manager.MusicManager.access$1000(this.this$0).postDelayed(com.navdy.hud.app.manager.MusicManager.access$900(this.this$0), 2000L);
        }
    }
}
