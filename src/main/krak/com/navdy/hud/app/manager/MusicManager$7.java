package com.navdy.hud.app.manager;

class MusicManager$7 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl;
    final static int[] $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform;
    
    static {
        $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform = new int[com.navdy.service.library.events.DeviceInfo$Platform.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform;
        com.navdy.service.library.events.DeviceInfo$Platform a0 = com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform[com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl = new int[com.navdy.hud.app.manager.MusicManager$MediaControl.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl;
        com.navdy.hud.app.manager.MusicManager$MediaControl a2 = com.navdy.hud.app.manager.MusicManager$MediaControl.NEXT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[com.navdy.hud.app.manager.MusicManager$MediaControl.PREVIOUS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[com.navdy.hud.app.manager.MusicManager$MediaControl.PLAY.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$MusicManager$MediaControl[com.navdy.hud.app.manager.MusicManager$MediaControl.PAUSE.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException4) {
        }
    }
}
