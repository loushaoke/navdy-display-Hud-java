package com.navdy.hud.app.service;

class FileTransferHandler$2 implements Runnable {
    final com.navdy.hud.app.service.FileTransferHandler this$0;
    final com.navdy.service.library.events.file.FileTransferRequest val$request;
    
    FileTransferHandler$2(com.navdy.hud.app.service.FileTransferHandler a, com.navdy.service.library.events.file.FileTransferRequest a0) {
        super();
        this.this$0 = a;
        this.val$request = a0;
    }
    
    public void run() {
        com.navdy.hud.app.service.FileTransferHandler.access$100(this.this$0).v("onFileTransferRequest");
        if (this.this$0.remoteDevice.getLinkBandwidthLevel() > 0) {
            com.navdy.service.library.events.file.FileTransferResponse a = this.this$0.fileTransferManager.handleFileTransferRequest(this.val$request);
            this.this$0.remoteDevice.postEvent((com.squareup.wire.Message)a);
        } else {
            com.navdy.hud.app.service.FileTransferHandler.sLogger.d("Link bandwidth is low");
            if (this.val$request != null) {
                com.navdy.hud.app.service.FileTransferHandler.sLogger.d("FileTransferRequest (PUSH) : rejecting file transfer request to avoid link traffic");
                com.navdy.service.library.events.file.FileTransferResponse a0 = new com.navdy.service.library.events.file.FileTransferResponse$Builder().success(Boolean.valueOf(false)).destinationFileName(this.val$request.destinationFileName).fileType(this.val$request.fileType).error(com.navdy.service.library.events.file.FileTransferError.FILE_TRANSFER_HOST_BUSY).build();
                this.this$0.remoteDevice.postEvent((com.squareup.wire.Message)a0);
            }
        }
    }
}
