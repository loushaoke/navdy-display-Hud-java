package com.navdy.hud.app.service;

class ShutdownMonitor$NotificationReceiver$ShutdownRunnable implements Runnable {
    final private com.navdy.hud.app.event.Shutdown event;
    final com.navdy.hud.app.service.ShutdownMonitor$NotificationReceiver this$1;
    
    ShutdownMonitor$NotificationReceiver$ShutdownRunnable(com.navdy.hud.app.service.ShutdownMonitor$NotificationReceiver a, com.navdy.hud.app.event.Shutdown a0) {
        super();
        this.this$1 = a;
        this.event = a0;
    }
    
    public void run() {
        com.navdy.hud.app.event.Shutdown$Reason a = this.event.reason;
        com.navdy.hud.app.HudApplication.getApplication().setShutdownReason(a);
        boolean b = !this.this$1.this$0.powerManager.quietModeEnabled();
        double d = com.navdy.hud.app.service.ShutdownMonitor.access$800(this.this$1.this$0).getBatteryVoltage();
        boolean b0 = com.navdy.hud.app.service.ShutdownMonitor.access$800(this.this$1.this$0).getObdDeviceConfigurationManager().isAutoOnEnabled();
        boolean b1 = d >= 13.100000381469727;
        switch(com.navdy.hud.app.service.ShutdownMonitor$4.$SwitchMap$com$navdy$hud$app$event$Shutdown$Reason[a.ordinal()]) {
            case 1: case 2: case 3: case 4: case 5: {
                b = true;
            }
            default: {
                com.navdy.hud.app.service.ShutdownMonitor.access$100().i(new StringBuilder().append("Shutting down, auto-on:").append(b0).append(" voltage:").append(d).append(" charging:").append(b1).toString());
                label2: {
                    label0: {
                        label1: {
                            if (!b0) {
                                break label1;
                            }
                            if (!b1) {
                                break label0;
                            }
                        }
                        b = true;
                        break label2;
                    }
                    if (com.navdy.hud.app.service.ShutdownMonitor.access$800(this.this$1.this$0).isSleeping()) {
                        com.navdy.hud.app.service.ShutdownMonitor.access$800(this.this$1.this$0).wakeup();
                        com.navdy.hud.app.util.GenericUtil.sleep(1000);
                    }
                    com.navdy.hud.app.service.ShutdownMonitor.access$800(this.this$1.this$0).sleep(b);
                }
                this.this$1.this$0.powerManager.androidShutdown(a, b);
                return;
            }
        }
    }
}
