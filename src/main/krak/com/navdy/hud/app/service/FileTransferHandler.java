package com.navdy.hud.app.service;

public class FileTransferHandler {
    final public static String ACTION_OTA_DOWNLOAD = "com.navdy.hud.app.service.OTA_DOWNLOAD";
    final public static String OTA_DOWLOADING_PROPERTY = "navdy.ota.downloading";
    final static com.navdy.service.library.log.Logger sLogger;
    private boolean closed;
    private android.content.Context context;
    com.navdy.service.library.file.IFileTransferAuthority fileTransferAuthority;
    com.navdy.service.library.file.FileTransferSessionManager fileTransferManager;
    final private com.navdy.service.library.log.Logger logger;
    com.navdy.service.library.device.RemoteDevice remoteDevice;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.FileTransferHandler.class);
    }
    
    public FileTransferHandler(android.content.Context a) {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.fileTransferAuthority = (com.navdy.service.library.file.IFileTransferAuthority)com.navdy.hud.app.storage.PathManager.getInstance();
        this.fileTransferManager = new com.navdy.service.library.file.FileTransferSessionManager(a, this.fileTransferAuthority);
        this.context = a;
    }
    
    static com.navdy.service.library.events.file.FileTransferData access$000(com.navdy.hud.app.service.FileTransferHandler a, int i) {
        return a.sendNextChunk(i);
    }
    
    static com.navdy.service.library.log.Logger access$100(com.navdy.hud.app.service.FileTransferHandler a) {
        return a.logger;
    }
    
    static void access$200(com.navdy.hud.app.service.FileTransferHandler a, boolean b) {
        a.setDownloadingProperty(b);
    }
    
    static void access$300(com.navdy.hud.app.service.FileTransferHandler a, String s) {
        a.onFileTransferDone(s);
    }
    
    private void onFileTransferDone(String s) {
        synchronized(this) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.service.FileTransferHandler$5(this, s), 5);
        }
        /*monexit(this)*/;
    }
    
    private com.navdy.service.library.events.file.FileTransferData sendNextChunk(int i) {
        com.navdy.service.library.events.file.FileTransferData a = this.fileTransferManager.getNextChunk(i);
        if (a != null) {
            this.remoteDevice.postEvent((com.squareup.wire.Message)a);
        }
        return a;
    }
    
    private void setDownloadingProperty(boolean b) {
        int i = b ? (int)(android.os.SystemClock.elapsedRealtime() / 1000L) : 0;
        Object[] a = new Object[1];
        a[0] = Integer.valueOf(i);
        com.navdy.hud.app.util.os.SystemProperties.set("navdy.ota.downloading", String.format("%d", a));
    }
    
    public void close() {
        if (!this.closed) {
            this.closed = true;
            if (this.fileTransferManager != null) {
                this.fileTransferManager.stop();
            }
        }
    }
    
    public void onDeviceConnected(com.navdy.service.library.device.RemoteDevice a) {
        this.remoteDevice = a;
    }
    
    public void onDeviceDisconnected() {
        this.remoteDevice = null;
    }
    
    public void onFileTransferData(com.navdy.service.library.events.file.FileTransferData a) {
        if (!this.closed) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.service.FileTransferHandler$3(this, a), 5);
        }
    }
    
    public void onFileTransferRequest(com.navdy.service.library.events.file.FileTransferRequest a) {
        label2: if (!this.closed) {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.fileType == null) {
                        break label1;
                    }
                    if (a.fileType == com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS) {
                        break label0;
                    }
                }
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.service.FileTransferHandler$2(this, a), 5);
                break label2;
            }
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.service.FileTransferHandler$1(this, a), 5);
        }
    }
    
    public void onFileTransferStatus(com.navdy.service.library.events.file.FileTransferStatus a) {
        if (!this.closed) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.service.FileTransferHandler$4(this, a), 5);
        }
    }
}
