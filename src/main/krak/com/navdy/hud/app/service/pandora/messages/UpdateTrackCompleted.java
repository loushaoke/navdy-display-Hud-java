package com.navdy.hud.app.service.pandora.messages;

public class UpdateTrackCompleted extends com.navdy.hud.app.service.pandora.messages.BaseIncomingOneIntMessage {
    public UpdateTrackCompleted(int i) {
        super(i);
    }
    
    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] a) {
        return new com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted(com.navdy.hud.app.service.pandora.messages.UpdateTrackCompleted.parseIntValue(a));
    }
    
    public String toString() {
        return new StringBuilder().append("Track ended - token: ").append(this.value).toString();
    }
}
