package com.navdy.hud.app.service;

class RemoteDeviceProxy$1 implements com.navdy.service.library.device.RemoteDevice$EventDispatcher {
    final com.navdy.hud.app.service.RemoteDeviceProxy this$0;
    final com.navdy.service.library.events.NavdyEvent val$event;
    
    RemoteDeviceProxy$1(com.navdy.hud.app.service.RemoteDeviceProxy a, com.navdy.service.library.events.NavdyEvent a0) {
        super();
        this.this$0 = a;
        this.val$event = a0;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.RemoteDevice a, com.navdy.service.library.device.RemoteDevice$Listener a0) {
        a0.onNavdyEventReceived(a, this.val$event);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.RemoteDevice)a, (com.navdy.service.library.device.RemoteDevice$Listener)a0);
    }
}
