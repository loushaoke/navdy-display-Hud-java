package com.navdy.hud.app.service;

public class ObdCANBusDataUploadService extends com.navdy.hud.app.service.S3FileUploadService {
    final private static int MAX_FILES_TO_UPLOAD = 5;
    final public static String NAVDY_GESTURE_VIDEOS_BUCKET = "navdy-obd-data";
    private static java.util.concurrent.atomic.AtomicBoolean isUploading;
    private static boolean mIsInitialized;
    private static com.navdy.hud.app.service.S3FileUploadService$Request sCurrentRequest;
    private static com.navdy.service.library.log.Logger sLogger;
    private static String sObdDatFilesFolder;
    final private static com.navdy.hud.app.service.S3FileUploadService$UploadQueue sObdDataFilesUploadQueue;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.ObdCANBusDataUploadService.class);
        sObdDataFilesUploadQueue = new com.navdy.hud.app.service.S3FileUploadService$UploadQueue();
        isUploading = new java.util.concurrent.atomic.AtomicBoolean(false);
        mIsInitialized = false;
    }
    
    public ObdCANBusDataUploadService() {
        super(com.navdy.hud.app.service.ObdCANBusDataUploadService.class.getName());
    }
    
    public static void addObdDataFileToQueue(java.io.File a) {
        com.navdy.hud.app.service.ObdCANBusDataUploadService.initializeIfNecessary(a);
        synchronized(sObdDataFilesUploadQueue) {
            com.navdy.hud.app.service.S3FileUploadService$UploadQueue a1 = sObdDataFilesUploadQueue;
            a1.add(new com.navdy.hud.app.service.S3FileUploadService$Request(a, ""));
            sLogger.d(new StringBuilder().append("Queue size : ").append(sObdDataFilesUploadQueue.size()).toString());
            if (!isUploading.get()) {
                while(sObdDataFilesUploadQueue.size() > 5) {
                    sObdDataFilesUploadQueue.pop();
                }
            }
            /*monexit(a0)*/;
        }
    }
    
    private static void initializeIfNecessary() {
        com.navdy.hud.app.service.ObdCANBusDataUploadService.initializeIfNecessary((java.io.File)null);
    }
    
    private static void initializeIfNecessary(java.io.File a) {
        Throwable a0 = null;
        label0: synchronized(sObdDataFilesUploadQueue) {
        boolean b = mIsInitialized;
            label1: if (!b) {
                java.io.File[] a2 = null;
                java.util.ArrayList a3 = null;
                int i = 0;
                int i0 = 0;
                //try {
                    sLogger.d("Not initialized , initializing now");
                    if (mIsInitialized) {
                        break label1;
                    }
                    sObdDatFilesFolder = "/sdcard/.canBusLogs/upload";
                    a2 = new java.io.File(sObdDatFilesFolder).listFiles();
                    a3 = new java.util.ArrayList();
                    i = a2.length;
                    i0 = 0;
                //} catch(Throwable a4) {
                //    a0 = a4;
                //    break label0;
                //}
                while(i0 < i) {
                    java.io.File a5 = a2[i0];
                    try {
                        if (a5.isFile() && a5.getName().endsWith(".zip")) {
                            sLogger.d(new StringBuilder().append("ObdCanBus Data bundle :").append(a5).toString());
                            a3.add(a5);
                        }
                        i0 = i0 + 1;
                    } catch(Throwable a6) {
                        a0 = a6;
                        break label0;
                    }
                }
                //try {
                    com.navdy.hud.app.service.ObdCANBusDataUploadService.populateFilesQueue(a3, sObdDataFilesUploadQueue, 5);
                    mIsInitialized = true;
                //} catch(Throwable a7) {
                //    a0 = a7;
                //    break label0;
                //}
            }
            //try {
                /*monexit(a1)*/;
            //} catch(IllegalMonitorStateException | NullPointerException a8) {
            //    a0 = a8;
            //    break label0;
            //}
            return;
        }
    }
    
    public static void populateFilesQueue(java.util.ArrayList a, com.navdy.hud.app.service.S3FileUploadService$UploadQueue a0, int i) {
        if (a != null) {
            Object a1 = a.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                java.io.File a2 = (java.io.File)((java.util.Iterator)a1).next();
                if (a2.isFile()) {
                    a0.add(new com.navdy.hud.app.service.S3FileUploadService$Request(a2, (String)null));
                    if (a0.size() == i) {
                        a0.pop();
                    }
                }
            }
        }
    }
    
    public static void scheduleWithDelay(long j) {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.service.ObdCANBusDataUploadService.class);
        a.setAction("SYNC");
        android.app.PendingIntent a0 = android.app.PendingIntent.getService(com.navdy.hud.app.HudApplication.getAppContext(), 123, a, 268435456);
        ((android.app.AlarmManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("alarm")).setExact(3, android.os.SystemClock.elapsedRealtime() + j, a0);
    }
    
    public static void syncNow() {
        sLogger.d("synNow");
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.service.ObdCANBusDataUploadService.class);
        a.setAction("SYNC");
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    public boolean canCompleteRequest(com.navdy.hud.app.service.S3FileUploadService$Request a) {
        return true;
    }
    
    protected String getAWSBucket() {
        return "navdy-obd-data";
    }
    
    protected com.navdy.hud.app.service.S3FileUploadService$Request getCurrentRequest() {
        return sCurrentRequest;
    }
    
    protected java.util.concurrent.atomic.AtomicBoolean getIsUploading() {
        return isUploading;
    }
    
    protected String getKeyPrefix(java.io.File a) {
        return "CANBus";
    }
    
    protected com.navdy.service.library.log.Logger getLogger() {
        return sLogger;
    }
    
    protected com.navdy.hud.app.service.S3FileUploadService$UploadQueue getUploadQueue() {
        return sObdDataFilesUploadQueue;
    }
    
    protected void initialize() {
        com.navdy.hud.app.service.ObdCANBusDataUploadService.initializeIfNecessary();
    }
    
    public void onCreate() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        super.onCreate();
    }
    
    public void reSchedule() {
        sLogger.d("reschedule");
        com.navdy.hud.app.service.ObdCANBusDataUploadService.scheduleWithDelay(10000L);
    }
    
    protected void setCurrentRequest(com.navdy.hud.app.service.S3FileUploadService$Request a) {
        sCurrentRequest = a;
    }
    
    public void sync() {
        com.navdy.hud.app.service.ObdCANBusDataUploadService.syncNow();
    }
    
    protected void uploadFinished(boolean b, String s, String s0) {
        if (b) {
            com.navdy.hud.app.obd.ObdManager.getInstance().getObdCanBusRecordingPolicy().onFileUploaded(new java.io.File(s));
        } else {
            sLogger.e(new StringBuilder().append("File upload failed ").append(s).toString());
        }
    }
}
