package com.navdy.hud.app.service.pandora.messages;

public class EventTrackPlay extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingEmptyMessage {
    final public static com.navdy.hud.app.service.pandora.messages.EventTrackPlay INSTANCE;
    
    static {
        INSTANCE = new com.navdy.hud.app.service.pandora.messages.EventTrackPlay();
    }
    
    private EventTrackPlay() {
    }
    
    public byte[] buildPayload() {
        return super.buildPayload();
    }
    
    protected byte getMessageType() {
        return (byte)48;
    }
    
    public String toString() {
        return "Play music action";
    }
}
