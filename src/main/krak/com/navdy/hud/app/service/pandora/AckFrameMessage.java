package com.navdy.hud.app.service.pandora;

public class AckFrameMessage extends com.navdy.hud.app.service.pandora.FrameMessage {
    final private static java.nio.ByteBuffer ACK_FRAME_BASE;
    final public static com.navdy.hud.app.service.pandora.AckFrameMessage ACK_WITH_SEQUENCE_0;
    final private static byte[] ACK_WITH_SEQUENCE_0_FRAME;
    final public static com.navdy.hud.app.service.pandora.AckFrameMessage ACK_WITH_SEQUENCE_1;
    final private static byte[] ACK_WITH_SEQUENCE_1_FRAME;
    
    static {
        ACK_WITH_SEQUENCE_0 = new com.navdy.hud.app.service.pandora.AckFrameMessage(true);
        ACK_WITH_SEQUENCE_1 = new com.navdy.hud.app.service.pandora.AckFrameMessage(false);
        ACK_FRAME_BASE = java.nio.ByteBuffer.allocate(6).put((byte)1).put((byte)0).putInt(0);
        ACK_WITH_SEQUENCE_0_FRAME = com.navdy.hud.app.service.pandora.AckFrameMessage.buildFrameFromCRCPart(ACK_FRAME_BASE);
        ACK_WITH_SEQUENCE_1_FRAME = com.navdy.hud.app.service.pandora.AckFrameMessage.buildFrameFromCRCPart(ACK_FRAME_BASE.put(1, (byte)1));
    }
    
    private AckFrameMessage(boolean b) {
        super(b, ACK_PAYLOAD);
    }
    
    protected static com.navdy.hud.app.service.pandora.AckFrameMessage parseAckFrame(byte[] a) {
        com.navdy.hud.app.service.pandora.AckFrameMessage a0 = null;
        if (java.util.Arrays.equals(a, ACK_WITH_SEQUENCE_0_FRAME)) {
            a0 = ACK_WITH_SEQUENCE_0;
        } else {
            if (!java.util.Arrays.equals(a, ACK_WITH_SEQUENCE_1_FRAME)) {
                throw new IllegalArgumentException("Not a valid ACK message frame");
            }
            a0 = ACK_WITH_SEQUENCE_1;
        }
        return a0;
    }
    
    public byte[] buildFrame() {
        return (this.isSequence0) ? ACK_WITH_SEQUENCE_0_FRAME : ACK_WITH_SEQUENCE_1_FRAME;
    }
}
