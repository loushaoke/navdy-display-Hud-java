package com.navdy.hud.app.service.pandora.messages;

abstract class BaseOutgoingConstantMessage extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingMessage {
    private byte[] preBuiltPayload;
    
    BaseOutgoingConstantMessage() {
        this.preBuiltPayload = null;
    }
    
    public byte[] buildPayload() {
        if (this.preBuiltPayload == null) {
            this.preBuiltPayload = super.buildPayload();
        }
        return this.preBuiltPayload;
    }
}
