package com.navdy.hud.app.service;

public class ShutdownMonitor implements Runnable {
    final private static long ACCELERATED_TIMEOUT;
    final private static long ACTIVE_USE_TIMEOUT;
    final private static long BOOT_MODE_TIMEOUT;
    final private static float MOVEMENT_THRESHOLD = 60f;
    final private static long OBD_ACTIVE_TIMEOUT;
    final private static long OBD_DISCONNECT_DELAY;
    final private static long OBD_DISCONNECT_TIMEOUT;
    final private static long PENDING_SHUTDOWN_TIMEOUT;
    final private static long POWER_DISCONECT_SMOOTHING_TIMEOUT;
    private static long REPORT_INTERVAL = 0L;
    final private static long SAMPLING_INTERVAL;
    final private static String SHUTDOWND_REQUEST_CLICK = "powerclick";
    final private static String SHUTDOWND_REQUEST_DOUBLE_CLICK = "powerdoubleclick";
    final private static String SHUTDOWND_REQUEST_LONG_PRESS = "powerlongpress";
    final private static String SHUTDOWND_REQUEST_POWER_STATE = "powerstate";
    private static String SHUTDOWN_OVERRIDE_SETTING;
    final private static String SOCKET_NAME = "shutdownd";
    private static String TEMPORARY_SHUTDOWN_OVERRIDE_SETTING;
    private static com.navdy.hud.app.service.ShutdownMonitor sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    @Inject
    com.squareup.otto.Bus bus;
    private String lastWakeReason;
    private long mAccelerateTime;
    private long mDialLastActivityTimeMsecs;
    private com.navdy.hud.app.device.dial.DialManager mDialManager;
    private boolean mDriving;
    private android.os.Handler mHandler;
    private boolean mInactivityShutdownDisabled;
    @Inject
    com.navdy.hud.app.manager.InputManager mInputManager;
    private boolean mIsDialConnected;
    private long mLastInputEventTimeMsecs;
    private long mLastMovementReport;
    private long mLastMovementTimeMsecs;
    private android.location.LocationListener mLocationListener;
    private boolean mMainPowerOn;
    private com.navdy.hud.app.service.ShutdownMonitor$MonitorState mMonitorState;
    private android.util.ArrayMap mMovementBases;
    private long mObdDisconnectTimeMsecs;
    private com.navdy.hud.app.obd.ObdManager mObdManager;
    private long mRemoteDeviceConnectTimeMsecs;
    private boolean mScreenDimmingDisabled;
    private android.net.LocalSocket mSocket;
    private java.io.InputStream mSocketInputStream;
    private java.io.OutputStream mSocketOutputStream;
    private long mStateChangeTime;
    private boolean mUsbPowerOn;
    private double maxVoltage;
    final private com.navdy.hud.app.service.ShutdownMonitor$NotificationReceiver notifyReceiver;
    private Runnable powerLossRunnable;
    @Inject
    com.navdy.hud.app.device.PowerManager powerManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.ShutdownMonitor.class);
        sInstance = null;
        SAMPLING_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(15L);
        BOOT_MODE_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(7L);
        OBD_ACTIVE_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(20L);
        OBD_DISCONNECT_TIMEOUT = java.util.concurrent.TimeUnit.SECONDS.toMillis(5L);
        ACCELERATED_TIMEOUT = java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
        PENDING_SHUTDOWN_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(5L);
        ACTIVE_USE_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(3L);
        OBD_DISCONNECT_DELAY = java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
        POWER_DISCONECT_SMOOTHING_TIMEOUT = java.util.concurrent.TimeUnit.SECONDS.toMillis(3L);
        SHUTDOWN_OVERRIDE_SETTING = "persist.sys.noautoshutdown";
        TEMPORARY_SHUTDOWN_OVERRIDE_SETTING = "sys.powerctl.noautoshutdown";
        REPORT_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(1L);
    }
    
    private ShutdownMonitor() {
        this.mRemoteDeviceConnectTimeMsecs = 0L;
        this.mIsDialConnected = false;
        this.mDialLastActivityTimeMsecs = 0L;
        this.mLastInputEventTimeMsecs = 0L;
        this.mObdDisconnectTimeMsecs = 0L;
        this.mDriving = false;
        this.maxVoltage = 0.0;
        this.powerLossRunnable = null;
        this.mScreenDimmingDisabled = false;
        this.mInactivityShutdownDisabled = false;
        this.mMonitorState = com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_BOOT;
        this.mStateChangeTime = 0L;
        this.mAccelerateTime = 0L;
        this.mMovementBases = new android.util.ArrayMap();
        this.mLastMovementTimeMsecs = 0L;
        this.mLastMovementReport = 0L;
        this.mLocationListener = (android.location.LocationListener)new com.navdy.hud.app.service.ShutdownMonitor$2(this);
        this.notifyReceiver = new com.navdy.hud.app.service.ShutdownMonitor$NotificationReceiver(this, (com.navdy.hud.app.service.ShutdownMonitor$1)null);
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            long j = this.getCurrentTimeMsecs();
            this.mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
            this.mDialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
            this.mObdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
            this.mLastInputEventTimeMsecs = j;
            this.mDialLastActivityTimeMsecs = j;
            this.mObdDisconnectTimeMsecs = 0L;
            this.mLastMovementTimeMsecs = 0L;
            this.mRemoteDeviceConnectTimeMsecs = 0L;
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.service.ShutdownMonitor$1(this), 1);
        } else {
            sLogger.w("not a Navdy device, ShutdownMonitor will not run");
        }
    }
    
    static java.io.InputStream access$000(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.getSocketInputStream();
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static void access$1000(com.navdy.hud.app.service.ShutdownMonitor a) {
        a.establishSocketConnection();
    }
    
    static long access$1100() {
        return SAMPLING_INTERVAL;
    }
    
    static void access$1200(com.navdy.hud.app.service.ShutdownMonitor a) {
        a.enterActiveUseState();
    }
    
    static void access$1400(com.navdy.hud.app.service.ShutdownMonitor a) {
        a.enterBootState();
    }
    
    static long access$1502(com.navdy.hud.app.service.ShutdownMonitor a, long j) {
        a.mObdDisconnectTimeMsecs = j;
        return j;
    }
    
    static long access$1602(com.navdy.hud.app.service.ShutdownMonitor a, long j) {
        a.mAccelerateTime = j;
        return j;
    }
    
    static long access$1700(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.getCurrentTimeMsecs();
    }
    
    static long access$1800() {
        return OBD_DISCONNECT_TIMEOUT;
    }
    
    static long access$1902(com.navdy.hud.app.service.ShutdownMonitor a, long j) {
        a.mRemoteDeviceConnectTimeMsecs = j;
        return j;
    }
    
    static void access$200(com.navdy.hud.app.service.ShutdownMonitor a) {
        a.closeSocketConnection();
    }
    
    static void access$2000(com.navdy.hud.app.service.ShutdownMonitor a) {
        a.onWakeEvent();
    }
    
    static boolean access$2100(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.mDriving;
    }
    
    static boolean access$2102(com.navdy.hud.app.service.ShutdownMonitor a, boolean b) {
        a.mDriving = b;
        return b;
    }
    
    static void access$2200(com.navdy.hud.app.service.ShutdownMonitor a) {
        a.onMovement();
    }
    
    static android.util.ArrayMap access$2300(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.mMovementBases;
    }
    
    static long access$2400(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.mLastMovementReport;
    }
    
    static long access$2402(com.navdy.hud.app.service.ShutdownMonitor a, long j) {
        a.mLastMovementReport = j;
        return j;
    }
    
    static long access$2500() {
        return REPORT_INTERVAL;
    }
    
    static void access$2600(com.navdy.hud.app.service.ShutdownMonitor a) {
        a.updateStats();
    }
    
    static long access$2700(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.evaluateState();
    }
    
    static boolean access$300(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.mMainPowerOn;
    }
    
    static boolean access$302(com.navdy.hud.app.service.ShutdownMonitor a, boolean b) {
        a.mMainPowerOn = b;
        return b;
    }
    
    static boolean access$400(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.mUsbPowerOn;
    }
    
    static boolean access$402(com.navdy.hud.app.service.ShutdownMonitor a, boolean b) {
        a.mUsbPowerOn = b;
        return b;
    }
    
    static Runnable access$500(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.powerLossRunnable;
    }
    
    static Runnable access$502(com.navdy.hud.app.service.ShutdownMonitor a, Runnable a0) {
        a.powerLossRunnable = a0;
        return a0;
    }
    
    static long access$600() {
        return POWER_DISCONECT_SMOOTHING_TIMEOUT;
    }
    
    static android.os.Handler access$700(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.mHandler;
    }
    
    static com.navdy.hud.app.obd.ObdManager access$800(com.navdy.hud.app.service.ShutdownMonitor a) {
        return a.mObdManager;
    }
    
    private boolean canDoShutdown() {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.util.os.SystemProperties.getBoolean(SHUTDOWN_OVERRIDE_SETTING, false);
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (com.navdy.hud.app.util.os.SystemProperties.getBoolean(TEMPORARY_SHUTDOWN_OVERRIDE_SETTING, false)) {
                        break label1;
                    }
                    if (!this.mUsbPowerOn) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return !b;
    }
    
    private long checkObdConnection() {
        return (this.mObdDisconnectTimeMsecs != 0L) ? this.getCurrentTimeMsecs() - this.mObdDisconnectTimeMsecs : 0L;
    }
    
    private void closeSocketConnection() {
        synchronized(this) {
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mSocketInputStream);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mSocketOutputStream);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mSocket);
            this.mSocket = null;
            this.mSocketInputStream = null;
            this.mSocketOutputStream = null;
        }
        /*monexit(this)*/;
    }
    
    private void enterActiveUseState() {
        this.setMonitorState(com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_ACTIVE_USE);
    }
    
    private void enterBootState() {
        this.setMonitorState(com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_BOOT);
    }
    
    private void enterObdActiveState() {
        this.setMonitorState(com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_OBD_ACTIVE);
    }
    
    private void enterPendingShutdownState() {
        if (this.canDoShutdown()) {
            com.navdy.hud.app.device.light.LED.writeToSysfs("0", "/sys/dlpc/led_enable");
            this.setMonitorState(com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_PENDING_SHUTDOWN);
        } else {
            sLogger.v("pending shutdown state disabled by property setting");
        }
    }
    
    private void enterQuiteModeState() {
        this.setMonitorState(com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_QUIET_MODE);
    }
    
    private void enterShutdownPromptState(com.navdy.hud.app.event.Shutdown$Reason a) {
        this.bus.post(new com.navdy.hud.app.event.Shutdown(a));
        this.setMonitorState(com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_SHUTDOWN_PROMPT);
    }
    
    private void enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown$Reason a) {
        boolean b = this.canDoShutdown();
        boolean b0 = this.mInactivityShutdownDisabled;
        label1: {
            label0: {
                if (b0) {
                    break label0;
                }
                if (!b) {
                    break label0;
                }
                this.enterShutdownPromptState(a);
                break label1;
            }
            com.navdy.service.library.log.Logger a0 = sLogger;
            Object[] a1 = new Object[3];
            a1[0] = Boolean.valueOf(b);
            a1[1] = Boolean.valueOf(this.mInactivityShutdownDisabled);
            a1[2] = Boolean.valueOf(this.mUsbPowerOn);
            a0.v(String.format("shutdown prompt state disabled, shutdownAllowed = %b, mInactivityShutdownDisabled = %b, mUsbPowerOn = %b", a1));
        }
    }
    
    private void establishSocketConnection() {
        synchronized(this) {
            this.mSocket = new android.net.LocalSocket();
            android.net.LocalSocketAddress a = new android.net.LocalSocketAddress("shutdownd", android.net.LocalSocketAddress$Namespace.RESERVED);
            try {
                this.mSocket.connect(a);
                this.mSocketOutputStream = this.mSocket.getOutputStream();
                this.mSocketInputStream = this.mSocket.getInputStream();
            } catch(Exception a0) {
                sLogger.e("exception while attempting to connect to shutdownd socket", (Throwable)a0);
                this.closeSocketConnection();
            }
        }
        /*monexit(this)*/;
    }
    
    private long evaluateState() {
        long j = this.getCurrentTimeMsecs();
        long j0 = SAMPLING_INTERVAL;
        double d = this.mObdManager.getBatteryVoltage();
        if (d > this.maxVoltage) {
            this.maxVoltage = d;
        }
        switch(com.navdy.hud.app.service.ShutdownMonitor$4.$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState[this.mMonitorState.ordinal()]) {
            case 5: {
                if (this.mObdManager.isConnected()) {
                    this.enterObdActiveState();
                    break;
                } else if (this.mAccelerateTime == 0L) {
                    if (this.mScreenDimmingDisabled) {
                        break;
                    }
                    if (j - this.lastWakeEventTime() <= ACTIVE_USE_TIMEOUT) {
                        break;
                    }
                    if (this.mDriving) {
                        break;
                    }
                    sLogger.i(new StringBuilder().append("Active use timeout - last wake event was ").append(this.lastWakeReason).toString());
                    this.enterPendingShutdownState();
                    break;
                } else {
                    long j1 = this.lastWakeEventTime();
                    long j2 = j - j1;
                    long j3 = ACCELERATED_TIMEOUT;
                    int i = (j2 < j3) ? -1 : (j2 == j3) ? 0 : 1;
                    label2: {
                        label3: {
                            if (i < 0) {
                                break label3;
                            }
                            if (!this.mDriving) {
                                break label2;
                            }
                        }
                        j0 = ACCELERATED_TIMEOUT + j1 - j;
                        break;
                    }
                    this.enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown$Reason.ACCELERATE_SHUTDOWN);
                    break;
                }
            }
            case 4: {
                long j4 = this.checkObdConnection();
                if (j4 < OBD_DISCONNECT_TIMEOUT) {
                    if (j - this.lastWakeEventTime() < OBD_ACTIVE_TIMEOUT) {
                        break;
                    }
                    if (this.mDriving) {
                        break;
                    }
                    this.enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown$Reason.ENGINE_OFF);
                    break;
                } else {
                    com.navdy.service.library.log.Logger a = sLogger;
                    Object[] a0 = new Object[3];
                    a0[0] = Boolean.valueOf(this.mDriving);
                    a0[1] = Double.valueOf(d);
                    a0[2] = Double.valueOf(this.maxVoltage);
                    a.i(String.format("OBD disconnected, driving = %b, battery voltage = %5.1f, max voltage = %5.1f", a0));
                    boolean b = this.mDriving;
                    label1: {
                        if (b) {
                            break label1;
                        }
                        if (d == -1.0) {
                            break label1;
                        }
                        if (!(d <= 12.899999618530273)) {
                            break label1;
                        }
                        this.enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown$Reason.ENGINE_OFF);
                        break;
                    }
                    if (j4 < OBD_DISCONNECT_DELAY) {
                        j0 = Math.min(java.util.concurrent.TimeUnit.SECONDS.toMillis(5L), OBD_DISCONNECT_DELAY - j4);
                        break;
                    } else if (this.mDriving) {
                        this.enterActiveUseState();
                        break;
                    } else {
                        this.enterPendingShutdownState();
                        break;
                    }
                }
            }
            case 3: {
                if (this.mObdManager.isConnected()) {
                    this.enterObdActiveState();
                    break;
                } else {
                    if (j - this.mStateChangeTime <= BOOT_MODE_TIMEOUT) {
                        break;
                    }
                    this.enterActiveUseState();
                    break;
                }
            }
            case 1: {
                long j5 = this.lastWakeEventTime();
                if (this.mObdManager.isConnected()) {
                    this.enterObdActiveState();
                    break;
                } else if (j5 <= this.mStateChangeTime) {
                    long j6 = this.mAccelerateTime;
                    int i0 = (j6 < 0L) ? -1 : (j6 == 0L) ? 0 : 1;
                    label0: {
                        if (i0 != 0) {
                            break label0;
                        }
                        if (j - j5 < PENDING_SHUTDOWN_TIMEOUT) {
                            break;
                        }
                    }
                    this.enterShutdownPromptStateIfNotDisabled(com.navdy.hud.app.event.Shutdown$Reason.INACTIVITY);
                    break;
                } else {
                    sLogger.i(new StringBuilder().append("Wake reason:").append(this.lastWakeReason).toString());
                    this.enterActiveUseState();
                    break;
                }
            }
            default: {
                break;
            }
            case 2: {
            }
        }
        return j0;
    }
    
    private long getCurrentTimeMsecs() {
        return android.os.SystemClock.elapsedRealtime();
    }
    
    public static com.navdy.hud.app.service.ShutdownMonitor getInstance() {
        label0: synchronized(com.navdy.hud.app.service.ShutdownMonitor.class) {
        com.navdy.hud.app.service.ShutdownMonitor a = sInstance;
            //Throwable a0 = null;
            if (a != null) {
                break label0;
            }
            //try {
                sInstance = new com.navdy.hud.app.service.ShutdownMonitor();
                sInstance.init();
                break label0;
            //} catch(Throwable a1) {
            //    a0 = a1;
            //}
            /*monexit(com.navdy.hud.app.service.ShutdownMonitor.class)*/;
            //throw a0;
        }
        com.navdy.hud.app.service.ShutdownMonitor a2 = sInstance;
        /*monexit(com.navdy.hud.app.service.ShutdownMonitor.class)*/;
        return a2;
    }
    
    private java.io.InputStream getSocketInputStream() {
        java.io.InputStream a = null;
        synchronized(this) {
            if (this.mSocketInputStream == null) {
                this.establishSocketConnection();
            }
            a = this.mSocketInputStream;
        }
        /*monexit(this)*/;
        return a;
    }
    
    private java.io.OutputStream getSocketOutputStream() {
        java.io.OutputStream a = null;
        synchronized(this) {
            if (this.mSocketOutputStream == null) {
                this.establishSocketConnection();
            }
            a = this.mSocketOutputStream;
        }
        /*monexit(this)*/;
        return a;
    }
    
    private void init() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        android.os.Looper a = android.os.Looper.getMainLooper();
        android.location.LocationManager a0 = (android.location.LocationManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("location");
        try {
            a0.requestLocationUpdates("gps", 0L, 0.0f, this.mLocationListener, a);
            a0.requestLocationUpdates("network", 0L, 0.0f, this.mLocationListener, a);
        } catch(IllegalArgumentException ignoredException) {
            sLogger.e("failed to register with GPS or Network provider");
        }
        this.bus.register(this.notifyReceiver);
        if (this.powerManager.inQuietMode()) {
            this.enterQuiteModeState();
        } else {
            this.enterBootState();
        }
    }
    
    private long lastWakeEventTime() {
        long j = 0L;
        this.updateStats();
        long j0 = this.getCurrentTimeMsecs();
        long j1 = this.mStateChangeTime;
        this.lastWakeReason = "stateChange";
        if (this.mDialLastActivityTimeMsecs > j1) {
            j1 = this.mDialLastActivityTimeMsecs;
            this.lastWakeReason = "dial";
        }
        if (this.mLastInputEventTimeMsecs > j1) {
            j1 = this.mLastInputEventTimeMsecs;
            this.lastWakeReason = "input";
        }
        if (this.mRemoteDeviceConnectTimeMsecs > j1) {
            j1 = this.mRemoteDeviceConnectTimeMsecs;
            this.lastWakeReason = "phone";
        }
        if (this.mLastMovementTimeMsecs > j1) {
            j1 = this.mLastMovementTimeMsecs;
            this.lastWakeReason = "movement";
        }
        com.navdy.hud.app.debug.DriveRecorder a = this.mObdManager.getDriveRecorder();
        if (a == null) {
            j0 = j1;
        } else if (a.isDemoPlaying()) {
            this.lastWakeReason = "recording";
        } else {
            j0 = j1;
        }
        String s = com.navdy.hud.app.util.os.SystemProperties.get("navdy.ota.downloading", "0");
        try {
            j = (long)Integer.parseInt(s) * 1000L;
        } catch(NumberFormatException ignoredException) {
            com.navdy.service.library.log.Logger a0 = sLogger;
            Object[] a1 = new Object[2];
            a1[0] = "navdy.ota.downloading";
            a1[1] = s;
            a0.e(String.format("%s property has invalid value: %s", a1));
            j = 0L;
        }
        if (j <= j0) {
            j = j0;
        } else {
            this.lastWakeReason = "ota";
        }
        return j;
    }
    
    private void leavePendingShutdownState() {
        com.navdy.hud.app.device.light.LED.writeToSysfs("1", "/sys/dlpc/led_enable");
    }
    
    private void leaveShutdownPromptState() {
        this.mAccelerateTime = 0L;
    }
    
    private void onMovement() {
        this.mLastMovementTimeMsecs = this.getCurrentTimeMsecs();
        this.mAccelerateTime = 0L;
        this.onWakeEvent();
    }
    
    private void onWakeEvent() {
        com.navdy.hud.app.service.ShutdownMonitor$MonitorState a = this.mMonitorState;
        com.navdy.hud.app.service.ShutdownMonitor$MonitorState a0 = com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_SHUTDOWN_PROMPT;
        label0: {
            label1: {
                if (a == a0) {
                    break label1;
                }
                if (this.mMonitorState != com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_PENDING_SHUTDOWN) {
                    break label0;
                }
            }
            this.enterActiveUseState();
        }
    }
    
    private void setMonitorState(com.navdy.hud.app.service.ShutdownMonitor$MonitorState a) {
        switch(com.navdy.hud.app.service.ShutdownMonitor$4.$SwitchMap$com$navdy$hud$app$service$ShutdownMonitor$MonitorState[this.mMonitorState.ordinal()]) {
            case 2: {
                this.leaveShutdownPromptState();
                break;
            }
            case 1: {
                this.leavePendingShutdownState();
                break;
            }
        }
        com.navdy.service.library.log.Logger a0 = sLogger;
        Object[] a1 = new Object[1];
        a1[0] = a;
        a0.v(String.format("setting monitor state to %s", a1));
        this.mMonitorState = a;
        this.mStateChangeTime = this.getCurrentTimeMsecs();
    }
    
    private void updateStats() {
        long j = this.getCurrentTimeMsecs();
        boolean b = this.mDialManager.isDialConnected();
        if (b && !this.mIsDialConnected) {
            this.mDialLastActivityTimeMsecs = j;
        }
        this.mIsDialConnected = b;
    }
    
    public void disableInactivityShutdown(boolean b) {
        this.mInactivityShutdownDisabled = b;
    }
    
    public void disableScreenDim(boolean b) {
        this.mScreenDimmingDisabled = b;
        this.recordInputEvent();
        com.navdy.service.library.log.Logger a = sLogger;
        String s = b ? "disabled" : "enabled";
        Object[] a0 = new Object[1];
        a0[0] = s;
        a.v(String.format("screen dimming %s", a0));
    }
    
    public void recordInputEvent() {
        this.mLastInputEventTimeMsecs = this.getCurrentTimeMsecs();
        if (this.mMonitorState == com.navdy.hud.app.service.ShutdownMonitor$MonitorState.STATE_PENDING_SHUTDOWN) {
            this.enterActiveUseState();
        }
    }
    
    public void run() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.service.ShutdownMonitor$3(this), 10);
    }
}
