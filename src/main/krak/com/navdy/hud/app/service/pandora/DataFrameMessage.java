package com.navdy.hud.app.service.pandora;

public class DataFrameMessage extends com.navdy.hud.app.service.pandora.FrameMessage {
    public DataFrameMessage(boolean b, byte[] a) {
        super(b, a);
        if (a.length == 0) {
            throw new IllegalArgumentException("Empty payload in data frame is not allowed");
        }
    }
    
    protected static com.navdy.hud.app.service.pandora.DataFrameMessage parseDataFrame(byte[] a) {
        int i = a[0];
        if (i == 126) {
            int i0 = a[a.length - 1];
            if (i0 == 124) {
                int i1 = a[1];
                if (i1 == 0) {
                    boolean b = false;
                    java.nio.ByteBuffer a0 = com.navdy.hud.app.service.pandora.DataFrameMessage.unescapeBytes(a);
                    a0.position(2);
                    int i2 = a0.get();
                    if (i2 != 0) {
                        if (i2 != 1) {
                            StringBuilder a1 = new StringBuilder().append("Unknown message frame sequence: ");
                            Object[] a2 = new Object[1];
                            a2[0] = Byte.valueOf((byte)i2);
                            throw new IllegalArgumentException(a1.append(String.format("%02X", a2)).toString());
                        }
                        b = false;
                    } else {
                        b = true;
                    }
                    int i3 = a0.getInt();
                    if (i3 == 0) {
                        throw new IllegalArgumentException("Data frame has 0 as payload's length");
                    }
                    byte[] a3 = new byte[i3];
                    a0.get(a3);
                    byte[] a4 = new byte[2];
                    a0.get(a4);
                    if (a0.remaining() != 1) {
                        throw new IllegalArgumentException("Corrupted message frame");
                    }
                    byte[] a5 = new byte[i3 + 6];
                    a0.position(1);
                    a0.get(a5);
                    if (!com.navdy.hud.app.service.pandora.CRC16CCITT.checkCRC(a5, a4)) {
                        throw new IllegalArgumentException("CRC fail");
                    }
                    return new com.navdy.hud.app.service.pandora.DataFrameMessage(b, a3);
                }
            }
        }
        throw new IllegalArgumentException("Data frame start/stop/type bytes not in place");
    }
    
    public byte[] buildFrame() {
        int i = this.payload.length;
        return com.navdy.hud.app.service.pandora.DataFrameMessage.buildFrameFromCRCPart(java.nio.ByteBuffer.allocate(i + 6).put((byte)0).put((byte)!this.isSequence0).putInt(i).put(this.payload));
    }
}
