package com.navdy.hud.app.service;

public class ConnectionServiceProxy {
    final private static boolean VERBOSE = true;
    final private static com.navdy.service.library.log.Logger sLogger;
    protected com.squareup.otto.Bus bus;
    private com.navdy.service.library.events.debug.StartDriveRecordingEvent driveRecordingEvent;
    private com.navdy.hud.app.IEventListener$Stub eventListener;
    protected android.content.Context mContext;
    protected com.navdy.hud.app.IEventSource mEventSource;
    protected com.squareup.wire.Wire mWire;
    private android.content.ServiceConnection serviceConnection;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.ConnectionServiceProxy.class);
    }
    
    public ConnectionServiceProxy(android.content.Context a, com.squareup.otto.Bus a0) {
        this.serviceConnection = (android.content.ServiceConnection)new com.navdy.hud.app.service.ConnectionServiceProxy$2(this);
        this.eventListener = new com.navdy.hud.app.service.ConnectionServiceProxy$3(this);
        this.mContext = a;
        this.bus = a0;
        a0.register(this);
        Class[] a1 = new Class[1];
        a1[0] = com.navdy.service.library.events.Ext_NavdyEvent.class;
        this.mWire = new com.squareup.wire.Wire(a1);
        sLogger.i(new StringBuilder().append("Creating connection service proxy:").append(this).toString());
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static com.navdy.hud.app.IEventListener$Stub access$100(com.navdy.hud.app.service.ConnectionServiceProxy a) {
        return a.eventListener;
    }
    
    public void connect() {
        if (this.mEventSource == null) {
            sLogger.d("Starting connection service");
            android.content.Intent a = new android.content.Intent(this.mContext, com.navdy.hud.app.service.HudConnectionService.class);
            this.mContext.startService(a);
            sLogger.d("Binding to connection service");
            a.setAction(com.navdy.hud.app.IEventSource.class.getName());
            this.mContext.bindService(a, this.serviceConnection, 0);
        }
    }
    
    public boolean connected() {
        return this.mEventSource != null;
    }
    
    public void disconnect() {
        if (this.mEventSource != null) {
            try {
                this.mEventSource.removeEventListener((com.navdy.hud.app.IEventListener)this.eventListener);
            } catch(android.os.RemoteException a) {
                sLogger.e("Failed to remove event listener", (Throwable)a);
            }
            this.mContext.unbindService(this.serviceConnection);
            android.content.Intent a0 = new android.content.Intent(this.mContext, com.navdy.hud.app.service.HudConnectionService.class);
            this.mContext.stopService(a0);
            this.mEventSource = null;
        }
    }
    
    public com.squareup.otto.Bus getBus() {
        return this.bus;
    }
    
    public com.navdy.service.library.events.debug.StartDriveRecordingEvent getDriverRecordingEvent() {
        return this.driveRecordingEvent;
    }
    
    public void onEvent(com.navdy.service.library.events.NavdyEvent a) {
        com.squareup.wire.Message a0 = com.navdy.service.library.events.NavdyEventUtil.messageFromEvent(a);
        if (a0 != null) {
            if (sLogger.isLoggable(2)) {
                sLogger.d(new StringBuilder().append("Received message: ").append(a0).toString());
            }
            this.bus.post(a0);
            switch(com.navdy.hud.app.service.ConnectionServiceProxy$4.$SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[a.type.ordinal()]) {
                case 2: {
                    this.driveRecordingEvent = null;
                    break;
                }
                case 1: {
                    this.driveRecordingEvent = (com.navdy.service.library.events.debug.StartDriveRecordingEvent)a0;
                    break;
                }
            }
        }
    }
    
    public void postEvent(byte[] a) {
        com.squareup.otto.Bus a0 = this.bus;
        label0: {
            Throwable a1 = null;
            int i = 0;
            if (a0 == null) {
                break label0;
            }
            try {
                com.navdy.service.library.events.NavdyEvent a2 = (com.navdy.service.library.events.NavdyEvent)this.mWire.parseFrom(a, com.navdy.service.library.events.NavdyEvent.class);
                this.bus.post(a2);
                break label0;
            } catch(Throwable a3) {
                a1 = a3;
            }
            try {
                i = com.navdy.service.library.events.WireUtil.getEventTypeIndex(a);
            } catch(Throwable ignoredException) {
                i = -1;
            }
            sLogger.e(new StringBuilder().append("Ignoring invalid navdy event[").append(i).append("]").toString(), a1);
        }
    }
    
    public void postRemoteEvent(com.navdy.service.library.device.NavdyDeviceId a, com.navdy.service.library.events.NavdyEvent a0) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.service.ConnectionServiceProxy$1(this, a0, a), 11);
    }
}
