package com.navdy.hud.app.ui.component.systeminfo;

public class GpsSignalView$SatelliteData implements Comparable {
    int cNo;
    String provider;
    int satelliteId;
    
    public GpsSignalView$SatelliteData(int i, int i0, String s) {
        this.satelliteId = i;
        this.cNo = i0;
        this.provider = s;
    }
    
    public int compareTo(com.navdy.hud.app.ui.component.systeminfo.GpsSignalView$SatelliteData a) {
        return a.cNo - this.cNo;
    }
    
    public int compareTo(Object a) {
        return this.compareTo((com.navdy.hud.app.ui.component.systeminfo.GpsSignalView$SatelliteData)a);
    }
}
