package com.navdy.hud.app.ui.framework;

abstract public interface IScreenAnimationListener {
    abstract public void onStart(com.navdy.hud.app.screen.BaseScreen arg, com.navdy.hud.app.screen.BaseScreen arg0);
    
    
    abstract public void onStop(com.navdy.hud.app.screen.BaseScreen arg, com.navdy.hud.app.screen.BaseScreen arg0);
}
