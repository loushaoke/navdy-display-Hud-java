package com.navdy.hud.app.ui.component.mainmenu;

class ShareTripMenu implements com.navdy.hud.app.ui.component.destination.IDestinationPicker {
    final private static int SEND_WITHOUT_MESSAGE_POS = 0;
    final private static com.navdy.service.library.log.Logger logger;
    final private com.navdy.hud.app.framework.contacts.Contact contact;
    private String failedDestinationLabel;
    private double failedLatitude;
    private double failedLongitude;
    private String failedMessage;
    final private com.navdy.hud.app.framework.glympse.GlympseManager glympseManager;
    private boolean hasFailed;
    private boolean itemSelected;
    final private String notificationId;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ShareTripMenu.class);
    }
    
    ShareTripMenu(com.navdy.hud.app.framework.contacts.Contact a, String s) {
        this.itemSelected = false;
        this.glympseManager = com.navdy.hud.app.framework.glympse.GlympseManager.getInstance();
        this.contact = a;
        this.notificationId = s;
        this.hasFailed = false;
    }
    
    private void addGlympseOfflineGlance(String s, com.navdy.hud.app.framework.contacts.Contact a, String s0, double d, double d0) {
        com.navdy.hud.app.framework.glympse.GlympseNotification a0 = new com.navdy.hud.app.framework.glympse.GlympseNotification(a, com.navdy.hud.app.framework.glympse.GlympseNotification$Type.OFFLINE, s, s0, d, d0);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().addNotification((com.navdy.hud.app.framework.notifications.INotification)a0);
    }
    
    private void sendShare(com.navdy.hud.app.framework.contacts.Contact a) {
        this.sendShare(a, (String)null);
    }
    
    private void sendShare(com.navdy.hud.app.framework.contacts.Contact a, String s) {
        String s0 = null;
        com.here.android.mpa.common.GeoCoordinate a0 = null;
        String s1 = null;
        double d = 0.0;
        double d0 = 0.0;
        com.navdy.hud.app.maps.here.HereNavigationManager a1 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        if (this.notificationId != null) {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notificationId);
        }
        if (a1.isNavigationModeOn()) {
            s0 = a1.getDestinationLabel();
            com.here.android.mpa.mapping.MapMarker a2 = a1.getDestinationMarker();
            if (a2 == null) {
                com.navdy.service.library.events.navigation.NavigationRouteRequest a3 = a1.getCurrentNavigationRouteRequest();
                a0 = null;
                if (a3 == null) {
                    s1 = "Trip";
                } else {
                    com.navdy.service.library.events.location.Coordinate a4 = a3.destination;
                    a0 = null;
                    if (a4 == null) {
                        s1 = "Trip";
                    } else {
                        a0 = new com.here.android.mpa.common.GeoCoordinate(a3.destination.latitude.doubleValue(), a3.destination.longitude.doubleValue());
                        s1 = "Trip";
                    }
                }
            } else {
                a0 = a2.getCoordinate();
                s1 = "Trip";
            }
        } else {
            a0 = null;
            s0 = null;
            s1 = "Location";
        }
        if (a0 == null) {
            d = 0.0;
            d0 = 0.0;
        } else {
            d = a0.getLatitude();
            d0 = a0.getLongitude();
        }
        StringBuilder a5 = new StringBuilder();
        this.hasFailed = this.glympseManager.addMessage(a, s, s0, d, d0, a5) != com.navdy.hud.app.framework.glympse.GlympseManager$Error.NONE;
        if (this.hasFailed) {
            this.failedMessage = s;
            this.failedDestinationLabel = s0;
            this.failedLatitude = d;
            this.failedLongitude = d0;
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.recordGlympseSent(!this.hasFailed, s1, s, a5.toString());
        logger.v(new StringBuilder().append("Glympse: success= ").append(!this.hasFailed).append("share=").append(s1).toString());
    }
    
    public void onDestinationPickerClosed() {
        if (!this.itemSelected) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
        }
        if (this.hasFailed) {
            this.addGlympseOfflineGlance(this.failedMessage, this.contact, this.failedDestinationLabel, this.failedLatitude, this.failedLongitude);
        }
    }
    
    public boolean onItemClicked(int i, int i0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a) {
        String s = com.navdy.hud.app.framework.glympse.GlympseManager.getInstance().getMessages()[i0];
        logger.v(new StringBuilder().append("selected option: ").append(s).toString());
        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection(new StringBuilder().append("glympse_menu_").append(s).toString());
        this.itemSelected = true;
        if (i0 != 0) {
            this.sendShare(this.contact, s);
        } else {
            this.sendShare(this.contact);
        }
        return true;
    }
    
    public boolean onItemSelected(int i, int i0, com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a) {
        return true;
    }
}
