package com.navdy.hud.app.ui.component.tbt;

final public class TbtView$Companion {
    private TbtView$Companion() {
    }
    
    public TbtView$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static android.content.res.Resources access$getResources$p(com.navdy.hud.app.ui.component.tbt.TbtView$Companion a) {
        return a.getResources();
    }
    
    final private com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.tbt.TbtView.access$getLogger$cp();
    }
    
    final private android.content.res.Resources getResources() {
        return com.navdy.hud.app.ui.component.tbt.TbtView.access$getResources$cp();
    }
    
    final public int getPaddingFull() {
        return com.navdy.hud.app.ui.component.tbt.TbtView.access$getPaddingFull$cp();
    }
    
    final public int getPaddingMedium() {
        return com.navdy.hud.app.ui.component.tbt.TbtView.access$getPaddingMedium$cp();
    }
    
    final public float getShrinkLeftX() {
        return com.navdy.hud.app.ui.component.tbt.TbtView.access$getShrinkLeftX$cp();
    }
}
