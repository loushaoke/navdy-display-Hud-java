package com.navdy.hud.app.ui.component;
import com.navdy.hud.app.R;

public class ChoiceLayout2 extends android.support.constraint.ConstraintLayout {
    final private static float DEFAULT_SCALE = 0.5f;
    final private static String EMPTY = "";
    private static int defaultHaloDuration;
    private static int defaultHaloEndRadius;
    private static int defaultHaloMiddleRadius;
    private static int defaultHaloStartDelay;
    private static int defaultHaloStartRadius;
    private static int defaultIconHaloSize;
    private static int defaultIconSize;
    private static int defaultItemPadding;
    private static int defaultLabelSize;
    private static int defaultTopPadding;
    final private static com.navdy.service.library.log.Logger sLogger;
    private java.util.List choices;
    private boolean clickMode;
    private int currentSelection;
    private int haloDuration;
    private int haloEndRadius;
    private int haloMiddleRadius;
    private int haloStartDelay;
    private int haloStartRadius;
    private int iconHaloSize;
    private int iconSize;
    private int itemPadding;
    private int itemTopPadding;
    private int labelSize;
    private android.widget.TextView labelView;
    private com.navdy.hud.app.ui.component.ChoiceLayout2$IListener listener;
    private float scale;
    private com.navdy.hud.app.ui.component.ChoiceLayout2$Selection selection;
    private java.util.List viewContainers;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.ChoiceLayout2.class);
        defaultItemPadding = -1;
    }
    
    public ChoiceLayout2(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public ChoiceLayout2(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ChoiceLayout2(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.listener = null;
        this.currentSelection = -1;
        this.viewContainers = (java.util.List)new java.util.ArrayList();
        this.selection = new com.navdy.hud.app.ui.component.ChoiceLayout2$Selection();
        com.navdy.hud.app.ui.component.ChoiceLayout2.initDefaults(a);
        android.view.LayoutInflater.from(a).inflate(R.layout.choices2_lyt, (android.view.ViewGroup)this, true);
        this.labelView = (android.widget.TextView)this.findViewById(R.id.label);
        android.content.res.TypedArray a1 = a.obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.ChoiceLayout2, i, 0);
        if (a1 != null) {
            this.itemPadding = a1.getDimensionPixelSize(1, defaultItemPadding);
            this.itemTopPadding = a1.getDimensionPixelSize(0, defaultTopPadding);
            this.labelSize = a1.getDimensionPixelSize(2, defaultLabelSize);
            this.iconSize = a1.getDimensionPixelSize(3, defaultIconSize);
            this.iconHaloSize = a1.getDimensionPixelSize(4, defaultIconHaloSize);
            this.haloStartRadius = a1.getDimensionPixelSize(5, defaultHaloStartRadius);
            this.haloEndRadius = a1.getDimensionPixelSize(6, defaultHaloEndRadius);
            this.haloMiddleRadius = a1.getDimensionPixelSize(7, defaultHaloMiddleRadius);
            this.haloStartDelay = a1.getInteger(8, defaultHaloStartDelay);
            this.haloDuration = a1.getInteger(9, defaultHaloDuration);
            a1.recycle();
        }
    }
    
    static int access$600(com.navdy.hud.app.ui.component.ChoiceLayout2 a) {
        return a.currentSelection;
    }
    
    static void access$700(com.navdy.hud.app.ui.component.ChoiceLayout2 a, int i) {
        a.callClickListener(i);
    }
    
    private void buildChoices() {
        this.removeViewContainers();
        if (this.choices != null) {
            android.view.LayoutInflater a = android.view.LayoutInflater.from(this.getContext());
            int i = this.choices.size();
            int i0 = 0;
            while(i0 < i) {
                com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a0 = (com.navdy.hud.app.ui.component.ChoiceLayout2$Choice)this.choices.get(i0);
                com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer a1 = new com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer((com.navdy.hud.app.ui.component.ChoiceLayout2$1)null);
                a1.parent = a.inflate(R.layout.choices2_lyt_item, (android.view.ViewGroup)this, false);
                a1.parent.setId(android.view.View.generateViewId());
                a1.iconContainer = (android.view.ViewGroup)a1.parent.findViewById(R.id.iconContainer);
                a1.crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a1.parent.findViewById(R.id.crossFadeImageView);
                a1.crossFadeImageView.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL);
                a1.big = (com.navdy.hud.app.ui.component.image.IconColorImageView)a1.parent.findViewById(R.id.big);
                a1.small = (com.navdy.hud.app.ui.component.image.IconColorImageView)a1.parent.findViewById(R.id.small);
                a1.haloView = (com.navdy.hud.app.ui.component.HaloView)a1.parent.findViewById(R.id.halo);
                this.viewContainers.add(a1);
                android.support.constraint.ConstraintLayout$LayoutParams a2 = new android.support.constraint.ConstraintLayout$LayoutParams(this.iconHaloSize, this.iconHaloSize);
                if (i0 != 0) {
                    a2.leftMargin = this.itemPadding;
                }
                android.view.ViewGroup$MarginLayoutParams a3 = (android.view.ViewGroup$MarginLayoutParams)a1.iconContainer.getLayoutParams();
                a3.width = this.iconSize;
                a3.height = this.iconSize;
                a1.haloView.setStartRadius(this.haloStartRadius);
                a1.haloView.setMiddleRadius(this.haloMiddleRadius);
                a1.haloView.setEndRadius(this.haloEndRadius);
                a1.haloView.setDuration(this.haloDuration);
                a1.haloView.setStartDelay(this.haloStartDelay);
                a1.big.setIcon(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice.access$100(a0), com.navdy.hud.app.ui.component.ChoiceLayout2$Choice.access$200(a0), (android.graphics.Shader)null, this.scale);
                a1.small.setIcon(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice.access$300(a0), com.navdy.hud.app.ui.component.ChoiceLayout2$Choice.access$400(a0), (android.graphics.Shader)null, this.scale);
                a1.haloView.setVisibility(4);
                a1.haloView.setStrokeColor(com.navdy.hud.app.ui.component.ChoiceLayout2$Choice.access$500(a0));
                this.addView(a1.parent, (android.view.ViewGroup$LayoutParams)a2);
                i0 = i0 + 1;
            }
            this.labelView.setTextSize((float)this.labelSize);
            android.support.constraint.ConstraintSet a4 = new android.support.constraint.ConstraintSet();
            a4.clone((android.support.constraint.ConstraintLayout)this);
            int i1 = this.viewContainers.size();
            int i2 = this.getId();
            int i3 = this.labelView.getId();
            int i4 = 0;
            while(i4 < i1) {
                int i5 = ((com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer)this.viewContainers.get(i4)).parent.getId();
                if (i4 == 0) {
                    a4.setHorizontalChainStyle(i5, 2);
                    a4.connect(i5, 6, i2, 6, 0);
                }
                if (i4 != i1 - 1) {
                    if (i1 > 1) {
                        a4.connect(i5, 2, ((com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer)this.viewContainers.get(i4 + 1)).parent.getId(), 1, 0);
                        if (i4 != 0) {
                            a4.connect(i5, 1, ((com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer)this.viewContainers.get(i4 - 1)).parent.getId(), 2, 0);
                        }
                    }
                } else {
                    a4.connect(i5, 7, i2, 7, 0);
                    if (i1 > 1) {
                        a4.connect(i5, 1, ((com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer)this.viewContainers.get(i4 - 1)).parent.getId(), 2, 0);
                    }
                }
                a4.connect(i5, 3, i3, 4, this.itemTopPadding);
                i4 = i4 + 1;
            }
            a4.applyTo((android.support.constraint.ConstraintLayout)this);
            this.invalidate();
            this.requestLayout();
        }
    }
    
    private void callClickListener(int i) {
        if (this.listener != null) {
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_SELECT);
            this.selection.id = com.navdy.hud.app.ui.component.ChoiceLayout2$Choice.access$800((com.navdy.hud.app.ui.component.ChoiceLayout2$Choice)this.choices.get(i));
            this.selection.pos = i;
            this.listener.executeItem(this.selection);
        }
    }
    
    private void callSelectListener(int i) {
        if (this.listener != null) {
            com.navdy.hud.app.audio.SoundUtils.playSound(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE);
            this.selection.id = com.navdy.hud.app.ui.component.ChoiceLayout2$Choice.access$800((com.navdy.hud.app.ui.component.ChoiceLayout2$Choice)this.choices.get(i));
            this.selection.pos = i;
            this.listener.itemSelected(this.selection);
        }
    }
    
    private int getItemCount() {
        return (this.choices == null) ? 0 : this.choices.size();
    }
    
    private static void initDefaults(android.content.Context a) {
        if (defaultItemPadding == -1) {
            android.content.res.Resources a0 = a.getResources();
            defaultItemPadding = a0.getDimensionPixelSize(R.dimen.choices2_lyt_item_padding);
            defaultTopPadding = a0.getDimensionPixelSize(R.dimen.choices2_lyt_item_top_padding);
            defaultLabelSize = a0.getDimensionPixelSize(R.dimen.choices2_lyt_text_size);
            defaultIconSize = a0.getDimensionPixelSize(R.dimen.choices2_lyt_icon_size);
            defaultIconHaloSize = a0.getDimensionPixelSize(R.dimen.choices2_lyt_icon_halo_size);
            defaultHaloStartRadius = a0.getDimensionPixelSize(R.dimen.choices2_lyt_halo_start_radius);
            defaultHaloEndRadius = a0.getDimensionPixelSize(R.dimen.choices2_lyt_halo_end_radius);
            defaultHaloMiddleRadius = a0.getDimensionPixelSize(R.dimen.choices2_lyt_halo_middle_radius);
            defaultHaloStartDelay = a0.getInteger(R.integer.choices2_lyt_halo_start_delay);
            defaultHaloDuration = a0.getInteger(R.integer.choices2_lyt_halo_duration);
        }
    }
    
    private boolean isItemInBounds(int i) {
        boolean b = false;
        java.util.List a = this.choices;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.choices.size()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void removeViewContainers() {
        Object a = this.viewContainers.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            this.removeView(((com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer)((java.util.Iterator)a).next()).parent);
        }
        this.viewContainers.clear();
    }
    
    private void resetClickMode() {
        this.clickMode = false;
    }
    
    private void startFluctuator(int i, boolean b) {
        sLogger.v(new StringBuilder().append("startFluctuator:").append(i).append(" current=").append(this.currentSelection).toString());
        if (this.isItemInBounds(i)) {
            com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer a = (com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer)this.viewContainers.get(i);
            com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a0 = (com.navdy.hud.app.ui.component.ChoiceLayout2$Choice)this.choices.get(i);
            if (b) {
                a.crossFadeImageView.setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG);
            }
            a.haloView.setVisibility(0);
            a.haloView.start();
            this.labelView.setText((CharSequence)com.navdy.hud.app.ui.component.ChoiceLayout2$Choice.access$900(a0));
        }
    }
    
    private void stopFluctuator(int i, boolean b) {
        sLogger.v(new StringBuilder().append("stopFluctuator:").append(i).append(" current=").append(this.currentSelection).toString());
        if (this.isItemInBounds(i)) {
            com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a = (com.navdy.hud.app.ui.component.ChoiceLayout2$Choice)this.choices.get(i);
            com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer a0 = (com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer)this.viewContainers.get(i);
            if (b) {
                a0.crossFadeImageView.setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL);
            }
            a0.haloView.setVisibility(8);
            a0.haloView.stop();
        }
    }
    
    public void clear() {
        sLogger.v("clear");
        this.stopFluctuator(this.currentSelection, false);
        this.resetClickMode();
    }
    
    public void executeSelectedItem() {
        if (this.clickMode) {
            sLogger.v("select: click mode on");
        } else if (this.isItemInBounds(this.currentSelection)) {
            this.clickMode = true;
            this.stopFluctuator(this.currentSelection, false);
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClick((android.view.View)((com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer)this.viewContainers.get(this.currentSelection)).iconContainer, 50, (Runnable)new com.navdy.hud.app.ui.component.ChoiceLayout2$1(this));
        }
    }
    
    public java.util.List getChoices() {
        return this.choices;
    }
    
    public com.navdy.hud.app.ui.component.ChoiceLayout2$Choice getCurrentSelectedChoice() {
        com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a = this.getCurrentSelection();
        com.navdy.hud.app.ui.component.ChoiceLayout2$Choice a0 = (a == null) ? null : (com.navdy.hud.app.ui.component.ChoiceLayout2$Choice)this.choices.get(a.pos);
        return a0;
    }
    
    public com.navdy.hud.app.ui.component.ChoiceLayout2$Selection getCurrentSelection() {
        com.navdy.hud.app.ui.component.ChoiceLayout2$Selection a = null;
        if (this.isItemInBounds(this.currentSelection)) {
            this.selection.id = com.navdy.hud.app.ui.component.ChoiceLayout2$Choice.access$800((com.navdy.hud.app.ui.component.ChoiceLayout2$Choice)this.choices.get(this.currentSelection));
            this.selection.pos = this.currentSelection;
            a = this.selection;
        } else {
            a = null;
        }
        return a;
    }
    
    public boolean moveSelectionLeft() {
        boolean b = false;
        if (this.clickMode) {
            sLogger.v("left: click mode on");
            b = false;
        } else if (this.currentSelection == 0) {
            b = false;
        } else {
            this.currentSelection = this.currentSelection - 1;
            this.stopFluctuator(this.currentSelection + 1, true);
            this.startFluctuator(this.currentSelection, true);
            this.callSelectListener(this.currentSelection);
            b = true;
        }
        return b;
    }
    
    public boolean moveSelectionRight() {
        boolean b = false;
        if (this.clickMode) {
            sLogger.v("right: click mode on");
            b = false;
        } else if (this.currentSelection == this.choices.size() - 1) {
            b = false;
        } else {
            this.currentSelection = this.currentSelection + 1;
            this.stopFluctuator(this.currentSelection - 1, true);
            this.startFluctuator(this.currentSelection, true);
            this.callSelectListener(this.currentSelection);
            b = true;
        }
        return b;
    }
    
    public void setChoices(java.util.List a, int i, com.navdy.hud.app.ui.component.ChoiceLayout2$IListener a0) {
        this.setChoices(a, i, a0, 0.5f);
    }
    
    public void setChoices(java.util.List a, int i, com.navdy.hud.app.ui.component.ChoiceLayout2$IListener a0, float f) {
        this.resetClickMode();
        this.choices = a;
        this.listener = a0;
        this.scale = f;
        label4: {
            label2: {
                label3: {
                    if (a == null) {
                        break label3;
                    }
                    if (a.size() > 0) {
                        break label2;
                    }
                }
                sLogger.v("setChoices:clear");
                this.currentSelection = -1;
                this.removeViewContainers();
                this.labelView.setText((CharSequence)"");
                this.invalidate();
                this.requestLayout();
                break label4;
            }
            label1: {
                label0: {
                    if (i < 0) {
                        break label0;
                    }
                    if (i >= a.size()) {
                        break label0;
                    }
                    this.currentSelection = i;
                    break label1;
                }
                this.currentSelection = 0;
            }
            sLogger.v(new StringBuilder().append("setChoices:").append(a.size()).append(" sel:").append(this.currentSelection).toString());
            this.buildChoices();
            Object a1 = this.viewContainers.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                ((com.navdy.hud.app.ui.component.ChoiceLayout2$ViewContainer)((java.util.Iterator)a1).next()).crossFadeImageView.setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL);
            }
            this.startFluctuator(this.currentSelection, true);
        }
    }
}
