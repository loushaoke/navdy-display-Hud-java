package com.navdy.hud.app.ui.activity;

class MainActivity$2 extends android.content.BroadcastReceiver {
    final com.navdy.hud.app.ui.activity.MainActivity this$0;
    
    MainActivity$2(com.navdy.hud.app.ui.activity.MainActivity a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        String s = a0.getAction();
        android.bluetooth.BluetoothDevice a1 = (android.bluetooth.BluetoothDevice)a0.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        com.squareup.otto.Bus a2 = this.this$0.mainPresenter.bus;
        label0: if (s.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
            int i = a0.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -2147483648);
            int i0 = a0.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", -2147483648);
            com.navdy.hud.app.ui.activity.MainActivity.access$000(this.this$0).d(new StringBuilder().append("Bond state change - device:").append(a1).append(" state:").append(i).append(" prevState:").append(i0).toString());
            a2.post(new com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing$BondStateChange(a1, i0, i));
        } else if (s.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
            int i1 = a0.getIntExtra("android.bluetooth.adapter.extra.STATE", -2147483648);
            label1: {
                if (i1 == 12) {
                    break label1;
                }
                if (i1 != 10) {
                    break label0;
                }
            }
            a2.post(new com.navdy.hud.app.event.InitEvents$BluetoothStateChanged(i1 == 12));
        } else if (s.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
            this.abortBroadcast();
            if (com.navdy.hud.app.device.dial.DialManager.getInstance().isDialDevice(a1)) {
                com.navdy.hud.app.ui.activity.MainActivity.access$100(this.this$0).w(new StringBuilder().append("Pairing request from dial device, ignore [").append(a1.getName()).append("]").toString());
            } else {
                int i2 = a0.getIntExtra("android.bluetooth.device.extra.PAIRING_VARIANT", -2147483648);
                int i3 = a0.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY", -2147483648);
                com.navdy.hud.app.ui.activity.MainActivity.access$200(this.this$0).d(new StringBuilder().append("Showing pairing request from ").append(a1).append(" of type:").append(i2).append(" with pin:").append(i3).toString());
                android.os.Bundle a3 = new android.os.Bundle();
                a3.putParcelable("device", (android.os.Parcelable)a1);
                a3.putInt("variant", i2);
                a3.putInt("pin", i3);
                boolean b = this.this$0.pairingManager.isAutoPairing();
                android.os.Bundle a4 = this.getResultExtras(false);
                if (!b && a4 != null) {
                    b = a4.getBoolean("auto", false);
                }
                a3.putBoolean("auto", b);
                com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing.showBluetoothPairingToast(a3);
            }
        } else if (s.equals("android.bluetooth.device.action.PAIRING_CANCEL")) {
            this.abortBroadcast();
            a2.post(new com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing$PairingCancelled(a1));
        }
    }
}
