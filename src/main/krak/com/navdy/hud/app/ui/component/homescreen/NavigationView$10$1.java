package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$10$1 implements Runnable {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView$10 this$1;
    final int val$x;
    final int val$y;
    
    NavigationView$10$1(com.navdy.hud.app.ui.component.homescreen.NavigationView$10 a, int i, int i0) {
        super();
        this.this$1 = a;
        this.val$x = i;
        this.val$y = i0;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapController$State a = com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$1.this$0).getState();
        if (a != com.navdy.hud.app.maps.here.HereMapController$State.ROUTE_PICKER) {
            com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$1.this$0).v(new StringBuilder().append("fluctuator state from route search mode:").append(a).toString());
        } else {
            this.this$1.this$0.startDestinationFluctuatorView.setX((float)(this.val$x - com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterMoveX));
            this.this$1.this$0.startDestinationFluctuatorView.setY((float)this.val$y);
            this.this$1.this$0.startDestinationFluctuatorView.setVisibility(0);
            this.this$1.this$0.startDestinationFluctuatorView.start();
        }
    }
}
