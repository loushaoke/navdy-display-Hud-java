package com.navdy.hud.app.ui.component.vmenu;

public class VerticalFastScrollIndex {
    final private String[] entries;
    final public int length;
    private int[] offsetIndex;
    final private java.util.Map offsetMap;
    final private int positionOffset;
    
    private VerticalFastScrollIndex(String[] a, java.util.Map a0, int[] a1, int i) {
        this.entries = a;
        this.offsetMap = a0;
        this.offsetIndex = a1;
        this.positionOffset = i;
        this.length = a.length;
    }
    
    VerticalFastScrollIndex(String[] a, java.util.Map a0, int[] a1, int i, com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex$1 a2) {
        this(a, a0, a1, i);
    }
    
    public int getEntryCount() {
        return this.entries.length;
    }
    
    public int getIndexForPosition(int i) {
        int i0 = java.util.Arrays.binarySearch(this.offsetIndex, 0, this.offsetIndex.length, i);
        if (i0 < 0) {
            i0 = -i0 - 1;
            if (i0 > 0) {
                i0 = i0 - 1;
            }
        }
        return i0;
    }
    
    public int getOffset() {
        return this.positionOffset;
    }
    
    public int getPosition(String s) {
        return ((Integer)this.offsetMap.get(s)).intValue();
    }
    
    public String getTitle(int i) {
        return this.entries[i];
    }
}
