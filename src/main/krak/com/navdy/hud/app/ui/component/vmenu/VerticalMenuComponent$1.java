package com.navdy.hud.app.ui.component.vmenu;

class VerticalMenuComponent$1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent this$0;
    
    VerticalMenuComponent$1(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$102(this.this$0, false);
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.access$000().v("closeMenuShowListener");
        this.this$0.closeContainer.setVisibility(0);
        this.this$0.closeContainerScrim.setVisibility(0);
        this.this$0.closeContainerScrim.setAlpha(0.0f);
    }
}
