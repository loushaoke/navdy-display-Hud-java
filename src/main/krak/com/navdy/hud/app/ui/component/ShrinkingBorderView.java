package com.navdy.hud.app.ui.component;

public class ShrinkingBorderView extends android.view.View {
    final private static int FORCE_COLLAPSE_INTERVAL = 300;
    final private static int RESET_TIMEOUT_INITIAL_INTERVAL = 300;
    final private static int RESET_TIMEOUT_INTERVAL = 100;
    private android.animation.ValueAnimator animator;
    private android.animation.Animator$AnimatorListener animatorListener;
    private android.animation.Animator$AnimatorListener animatorResetListener;
    private android.animation.ValueAnimator$AnimatorUpdateListener animatorResetUpdateListener;
    private android.animation.ValueAnimator$AnimatorUpdateListener animatorUpdateListener;
    private Runnable forceCompleteCallback;
    private android.animation.Animator$AnimatorListener forceCompleteListener;
    private android.view.animation.Interpolator interpolator;
    private com.navdy.hud.app.ui.component.ShrinkingBorderView$IListener listener;
    private android.view.animation.Interpolator restoreInterpolator;
    private int timeoutVal;
    
    public ShrinkingBorderView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public ShrinkingBorderView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ShrinkingBorderView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.restoreInterpolator = (android.view.animation.Interpolator)new android.view.animation.LinearInterpolator();
        this.interpolator = (android.view.animation.Interpolator)new android.view.animation.AccelerateInterpolator();
        this.forceCompleteListener = (android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.ShrinkingBorderView$1(this);
        this.animatorListener = (android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.ShrinkingBorderView$2(this);
        this.animatorUpdateListener = (android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.ShrinkingBorderView$3(this);
        this.animatorResetListener = (android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.ShrinkingBorderView$4(this);
        this.animatorResetUpdateListener = (android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.ShrinkingBorderView$5(this);
    }
    
    static android.animation.ValueAnimator access$002(com.navdy.hud.app.ui.component.ShrinkingBorderView a, android.animation.ValueAnimator a0) {
        a.animator = a0;
        return a0;
    }
    
    static Runnable access$100(com.navdy.hud.app.ui.component.ShrinkingBorderView a) {
        return a.forceCompleteCallback;
    }
    
    static Runnable access$102(com.navdy.hud.app.ui.component.ShrinkingBorderView a, Runnable a0) {
        a.forceCompleteCallback = a0;
        return a0;
    }
    
    static com.navdy.hud.app.ui.component.ShrinkingBorderView$IListener access$200(com.navdy.hud.app.ui.component.ShrinkingBorderView a) {
        return a.listener;
    }
    
    static int access$300(com.navdy.hud.app.ui.component.ShrinkingBorderView a) {
        return a.timeoutVal;
    }
    
    static void access$400(com.navdy.hud.app.ui.component.ShrinkingBorderView a, int i, boolean b) {
        a.startTimeout(i, b);
    }
    
    private void clearAnimator() {
        if (this.animator != null) {
            this.animator.removeAllListeners();
            this.animator.cancel();
            this.animator = null;
            this.forceCompleteCallback = null;
        }
    }
    
    private void startTimeout(int i, boolean b) {
        if (i != 0 && this.animator == null) {
            this.timeoutVal = i;
            if (b) {
                int[] a = new int[2];
                a[0] = ((android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams()).height;
                a[1] = 0;
                this.animator = android.animation.ValueAnimator.ofInt(a);
                this.animator.addUpdateListener(this.animatorUpdateListener);
                this.animator.addListener(this.animatorListener);
                this.animator.setDuration((long)i);
                this.animator.setInterpolator((android.animation.TimeInterpolator)this.interpolator);
                this.animator.start();
            } else {
                ((android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams()).height = 0;
                this.requestLayout();
                this.resetTimeout(true);
            }
        }
    }
    
    public boolean isVisible() {
        return ((android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams()).height != 0;
    }
    
    public void resetTimeout() {
        this.resetTimeout(false);
    }
    
    public void resetTimeout(boolean b) {
        this.clearAnimator();
        int i = b ? 300 : 100;
        android.view.ViewGroup$MarginLayoutParams a = (android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams();
        int i0 = ((android.view.ViewGroup)this.getParent()).getHeight();
        int[] a0 = new int[2];
        a0[0] = a.height;
        a0[1] = i0;
        this.animator = android.animation.ValueAnimator.ofInt(a0);
        this.animator.addUpdateListener(this.animatorResetUpdateListener);
        this.animator.addListener(this.animatorResetListener);
        this.animator.setDuration((long)i);
        this.animator.setInterpolator((android.animation.TimeInterpolator)this.restoreInterpolator);
        if (b) {
            this.animator.setStartDelay(300L);
        }
        this.animator.start();
    }
    
    public void setListener(com.navdy.hud.app.ui.component.ShrinkingBorderView$IListener a) {
        this.listener = a;
    }
    
    public void startTimeout(int i) {
        this.startTimeout(i, false);
    }
    
    public void stopTimeout(boolean b, Runnable a) {
        if (b) {
            this.clearAnimator();
            if (a == null) {
                ((android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams()).height = 0;
                this.requestLayout();
            } else {
                this.forceCompleteCallback = a;
                int[] a0 = new int[2];
                a0[0] = ((android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams()).height;
                a0[1] = 0;
                this.animator = android.animation.ValueAnimator.ofInt(a0);
                this.animator.addUpdateListener(this.animatorResetUpdateListener);
                this.animator.addListener(this.forceCompleteListener);
                this.animator.setDuration(300L);
                this.animator.setInterpolator((android.animation.TimeInterpolator)this.interpolator);
                this.animator.start();
            }
        } else if (this.animator == null) {
            ((android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams()).height = 0;
            this.requestLayout();
        }
    }
}
