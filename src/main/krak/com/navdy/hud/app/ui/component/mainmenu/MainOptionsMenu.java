package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class MainOptionsMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model autoZoom;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static int dashColor;
    final private static String dashTitle;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model manualZoom;
    final private static int mapColor;
    final private static String mapTitle;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model pauseDemo;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model playDemo;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model rawGps;
    final private static android.content.res.Resources resources;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model restartDemo;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model scrollLeftGauge;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model scrollRightGauge;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model selectCenterGauge;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model sideGauges;
    private int backSelection;
    private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu dashGaugeConfiguratorMenu;
    private com.navdy.hud.app.debug.DriveRecorder driveRecorder;
    private com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    private com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode mode;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.class);
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        int i = resources.getColor(R.color.mm_back);
        dashColor = resources.getColor(R.color.mm_dash_options);
        mapColor = resources.getColor(R.color.mm_map_options);
        dashTitle = resources.getString(R.string.carousel_menu_smartdash_options);
        mapTitle = resources.getString(R.string.carousel_menu_map_options);
        String s = resources.getString(R.string.back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
        String s0 = resources.getString(R.string.manual_zoom);
        int i0 = resources.getColor(R.color.mm_options_manual_zoom);
        manualZoom = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_manual_zoom, R.drawable.icon_options_map_zoom_manual_2, i0, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i0, s0, (String)null);
        String s1 = resources.getString(R.string.show_raw_gps);
        int i1 = resources.getColor(R.color.mm_options_manual_zoom);
        boolean b = com.navdy.hud.app.device.gps.GpsUtils.SHOW_RAW_GPS.isEnabled();
        rawGps = com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion.buildModel(R.id.main_menu_options_raw_gps, i1, s1, resources.getString(b ? R.string.si_enabled : R.string.si_disabled), b, true);
        String s2 = resources.getString(R.string.auto_zoom);
        int i2 = resources.getColor(R.color.mm_options_auto_zoom);
        autoZoom = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_auto_zoom, R.drawable.icon_options_map_zoom_auto_2, i2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i2, s2, (String)null);
        String s3 = resources.getString(R.string.play_demo);
        int i3 = resources.getColor(R.color.mm_options_demo);
        playDemo = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_play_demo, R.drawable.icon_mm_demo_play_2, i3, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i3, s3, (String)null);
        String s4 = resources.getString(R.string.pause_demo);
        int i4 = resources.getColor(R.color.mm_options_demo);
        pauseDemo = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_pause_demo, R.drawable.icon_mm_demo_pause_2, i4, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i4, s4, (String)null);
        String s5 = resources.getString(R.string.restart_demo);
        int i5 = resources.getColor(R.color.mm_options_demo);
        restartDemo = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_restart_demo, R.drawable.icon_mm_demo_restart_2, i5, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i5, s5, (String)null);
        String s6 = resources.getString(R.string.scroll_left_gauge);
        int i6 = resources.getColor(R.color.mm_options_scroll_gauge);
        scrollLeftGauge = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_scroll_left, R.drawable.icon_options_dash_scroll_left_2, i6, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i6, s6, (String)null);
        String s7 = resources.getString(R.string.scroll_right_gauge);
        int i7 = resources.getColor(R.color.mm_options_scroll_gauge);
        scrollRightGauge = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_scroll_right, R.drawable.icon_options_dash_scroll_right_2, i7, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i7, s7, (String)null);
        String s8 = resources.getString(R.string.carousel_menu_smartdash_select_center_gauge);
        int i8 = resources.getColor(R.color.mm_options_scroll_gauge);
        selectCenterGauge = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_select_center_gauge, R.drawable.icon_center_gauge, i8, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i8, s8, (String)null);
        String s9 = resources.getString(R.string.carousel_menu_smartdash_side_gauges);
        int i9 = resources.getColor(R.color.mm_options_scroll_gauge);
        sideGauges = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_side_gauges, R.drawable.icon_side_gauges, i9, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i9, s9, (String)null);
    }
    
    MainOptionsMenu(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2) {
        this.bus = a;
        this.vscrollComponent = a0;
        this.presenter = a1;
        this.parent = a2;
        com.navdy.hud.app.manager.RemoteDeviceManager a3 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.driveRecorder = a3.getDriveRecorder();
        this.driverProfileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
        this.uiStateManager = a3.getUiStateManager();
        this.dashGaugeConfiguratorMenu = new com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu(a, a3.getSharedPreferences(), this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$000(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu a) {
        return a.presenter;
    }
    
    static com.navdy.hud.app.profile.DriverProfileManager access$100(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu a) {
        return a.driverProfileManager;
    }
    
    static com.navdy.hud.app.debug.DriveRecorder access$200(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu a) {
        return a.driveRecorder;
    }
    
    static com.navdy.hud.app.ui.framework.UIStateManager access$300(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu a) {
        return a.uiStateManager;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        return 1;
    }
    
    public java.util.List getItems() {
        java.util.ArrayList a = new java.util.ArrayList();
        ((java.util.List)a).add(back);
        switch(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$7.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainOptionsMenu$Mode[this.mode.ordinal()]) {
            case 2: {
                com.navdy.hud.app.ui.component.homescreen.SmartDashView a0 = this.uiStateManager.getSmartDashView();
                if (a0 != null) {
                    switch(a0.getCurrentScrollableSideOption()) {
                        case 1: {
                            ((java.util.List)a).add(scrollRightGauge);
                            break;
                        }
                        case 0: {
                            ((java.util.List)a).add(scrollLeftGauge);
                            break;
                        }
                    }
                }
                ((java.util.List)a).add(selectCenterGauge);
                ((java.util.List)a).add(sideGauges);
                break;
            }
            case 1: {
                if (!com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                    break;
                }
                com.navdy.service.library.events.preferences.LocalPreferences a1 = this.driverProfileManager.getLocalPreferences();
                Boolean a2 = a1.manualZoom;
                label1: {
                    label0: {
                        if (a2 == null) {
                            break label0;
                        }
                        if (!a1.manualZoom.booleanValue()) {
                            break label0;
                        }
                        ((java.util.List)a).add(autoZoom);
                        break label1;
                    }
                    ((java.util.List)a).add(manualZoom);
                }
                if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
                    ((java.util.List)a).add(rawGps);
                }
                boolean b = this.driveRecorder.isDemoAvailable();
                boolean b0 = this.driveRecorder.isDemoPlaying();
                if (!b) {
                    break;
                }
                if (b0) {
                    ((java.util.List)a).add(pauseDemo);
                    ((java.util.List)a).add(restartDemo);
                    break;
                } else {
                    ((java.util.List)a).add(playDemo);
                    break;
                }
            }
        }
        this.cachedList = (java.util.List)a;
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MAIN_OPTIONS;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        boolean b = false;
        sLogger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        int i = a.id;
        label1: {
            com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = null;
            boolean b0 = false;
            label0: {
                switch(i) {
                    case R.id.menu_back: {
                        sLogger.v("back");
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                        this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                        break;
                    }
                    case R.id.main_menu_options_side_gauges: {
                        this.dashGaugeConfiguratorMenu.setGaugeType(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType.SIDE);
                        this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.dashGaugeConfiguratorMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                        break;
                    }
                    case R.id.main_menu_options_select_center_gauge: {
                        this.dashGaugeConfiguratorMenu.setGaugeType(com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$GaugeType.CENTER);
                        this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.dashGaugeConfiguratorMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                        break;
                    }
                    case R.id.main_menu_options_scroll_left: case R.id.main_menu_options_scroll_right: {
                        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$6(this));
                        break;
                    }
                    case R.id.main_menu_options_restart_demo: {
                        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$5(this));
                        break;
                    }
                    case R.id.main_menu_options_raw_gps: {
                        sLogger.i("Toggling raw gps");
                        a0 = a.model;
                        b0 = a0.isOn;
                        break label0;
                    }
                    case R.id.main_menu_options_play_demo: {
                        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$3(this));
                        break;
                    }
                    case R.id.main_menu_options_pause_demo: {
                        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$4(this));
                        break;
                    }
                    case R.id.main_menu_options_manual_zoom: {
                        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$1(this));
                        break;
                    }
                    case R.id.main_menu_options_auto_zoom: {
                        this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$2(this));
                        break;
                    }
                }
                b = true;
                break label1;
            }
            boolean b1 = !b0;
            a0.isOn = b1;
            a0.subTitle = resources.getString(b1 ? R.string.si_enabled : R.string.si_disabled);
            com.navdy.hud.app.device.gps.GpsUtils.SHOW_RAW_GPS.setEnabled(b1);
            this.presenter.refreshDataforPos(a.pos);
            b = false;
        }
        return b;
    }
    
    public void setBackSelectionId(int i) {
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    void setMode(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$Mode a) {
        this.mode = a;
    }
    
    public void setSelectedIcon() {
        int i = 0;
        int i0 = 0;
        int i1 = com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu$7.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainOptionsMenu$Mode[this.mode.ordinal()];
        String s = null;
        switch(i1) {
            case 2: {
                i = dashColor;
                s = dashTitle;
                i0 = R.drawable.icon_main_menu_dash_options;
                break;
            }
            case 1: {
                i = mapColor;
                s = mapTitle;
                i0 = R.drawable.icon_main_menu_map_options;
                break;
            }
            default: {
                i0 = 0;
                i = 0;
            }
        }
        this.vscrollComponent.setSelectedIconColorImage(i0, i, (android.graphics.Shader)null, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)s);
    }
    
    public void showToolTip() {
    }
}
