package com.navdy.hud.app.ui.component.image;
import com.navdy.hud.app.R;

public class CrossFadeImageView extends android.widget.FrameLayout {
    android.view.View big;
    com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode mode;
    android.view.View small;
    float smallAlpha;
    boolean smallAlphaSet;
    
    public CrossFadeImageView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public CrossFadeImageView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public CrossFadeImageView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    public android.view.View getBig() {
        return this.big;
    }
    
    public android.animation.AnimatorSet getCrossFadeAnimator() {
        float f = 0.0f;
        float f0 = 0.0f;
        float f1 = 0.0f;
        float f2 = 0.0f;
        android.animation.AnimatorSet a = new android.animation.AnimatorSet();
        if (this.mode != com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG) {
            f = 0.0f;
            f0 = 1f;
            f1 = 1f;
            f2 = 0.0f;
        } else if (this.smallAlphaSet) {
            f2 = this.smallAlpha;
            f = 1f;
            f0 = 0.0f;
            f1 = 0.0f;
        } else {
            f = 1f;
            f0 = 0.0f;
            f1 = 0.0f;
            f2 = 1f;
        }
        android.view.View a0 = this.big;
        android.util.Property a1 = android.view.View.ALPHA;
        float[] a2 = new float[2];
        a2[0] = f;
        a2[1] = f0;
        android.animation.ObjectAnimator a3 = android.animation.ObjectAnimator.ofFloat(a0, a1, a2);
        a3.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.image.CrossFadeImageView$1(this));
        android.animation.AnimatorSet$Builder a4 = a.play((android.animation.Animator)a3);
        android.view.View a5 = this.small;
        android.util.Property a6 = android.view.View.ALPHA;
        float[] a7 = new float[2];
        a7[0] = f1;
        a7[1] = f2;
        android.animation.ObjectAnimator a8 = android.animation.ObjectAnimator.ofFloat(a5, a6, a7);
        a8.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.image.CrossFadeImageView$2(this));
        a4.with((android.animation.Animator)a8);
        a.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.image.CrossFadeImageView$3(this));
        return a;
    }
    
    public com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode getMode() {
        return this.mode;
    }
    
    public android.view.View getSmall() {
        return this.small;
    }
    
    public void inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode a) {
        if (this.big == null) {
            this.big = this.findViewById(R.id.big);
            this.small = this.findViewById(R.id.small);
            this.mode = a;
            if (a != com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG) {
                if (this.smallAlphaSet) {
                    this.small.setAlpha(this.smallAlpha);
                } else {
                    this.small.setAlpha(1f);
                }
                this.big.setAlpha(0.0f);
            } else {
                this.big.setAlpha(1f);
                this.small.setAlpha(0.0f);
            }
        }
    }
    
    public void setMode(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode a) {
        if (this.mode != a) {
            if (a != com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG) {
                if (this.smallAlphaSet) {
                    this.small.setAlpha(this.smallAlpha);
                } else {
                    this.small.setAlpha(1f);
                }
                this.big.setAlpha(0.0f);
            } else {
                this.big.setAlpha(1f);
                this.small.setAlpha(0.0f);
            }
            this.mode = a;
        }
    }
    
    public void setSmallAlpha(float f) {
        if (f != -1f) {
            this.smallAlphaSet = true;
            this.smallAlpha = f;
        } else {
            this.smallAlphaSet = false;
            this.smallAlpha = 0.0f;
        }
    }
}
