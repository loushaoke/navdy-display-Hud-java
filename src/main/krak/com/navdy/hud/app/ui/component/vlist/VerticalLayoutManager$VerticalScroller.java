package com.navdy.hud.app.ui.component.vlist;

class VerticalLayoutManager$VerticalScroller extends android.support.v7.widget.LinearSmoothScroller {
    private android.view.animation.Interpolator interpolator;
    private com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager layoutManager;
    private int position;
    private com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView recyclerView;
    private int snapPreference;
    private com.navdy.hud.app.ui.component.vlist.VerticalList vlist;
    
    public VerticalLayoutManager$VerticalScroller(android.content.Context a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager a1, com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView a2, int i, int i0) {
        super(a);
        this.vlist = a0;
        this.layoutManager = a1;
        this.recyclerView = a2;
        this.position = i;
        this.snapPreference = i0;
        this.interpolator = com.navdy.hud.app.ui.component.vlist.VerticalList.getInterpolator();
    }
    
    protected float calculateSpeedPerPixel(android.util.DisplayMetrics a) {
        return super.calculateSpeedPerPixel(a);
    }
    
    protected int calculateTimeForDeceleration(int i) {
        return this.vlist.animationDuration;
    }
    
    protected int calculateTimeForScrolling(int i) {
        return super.calculateTimeForScrolling(i);
    }
    
    public android.graphics.PointF computeScrollVectorForPosition(int i) {
        return this.layoutManager.computeScrollVectorForPosition(i);
    }
    
    protected int getVerticalSnapPreference() {
        return this.snapPreference;
    }
    
    protected void onSeekTargetStep(int i, int i0, android.support.v7.widget.RecyclerView$State a, android.support.v7.widget.RecyclerView$SmoothScroller$Action a0) {
        super.onSeekTargetStep(i, i0, a, a0);
    }
    
    protected void onStart() {
        super.onStart();
    }
    
    protected void onStop() {
        super.onStop();
    }
    
    protected void onTargetFound(android.view.View a, android.support.v7.widget.RecyclerView$State a0, android.support.v7.widget.RecyclerView$SmoothScroller$Action a1) {
        int i = this.calculateDxToMakeVisible(a, this.getHorizontalSnapPreference());
        int i0 = this.calculateDyToMakeVisible(a, this.getVerticalSnapPreference());
        int i1 = this.calculateTimeForDeceleration((int)Math.sqrt((double)(i * i + i0 * i0)));
        if (i1 > 0) {
            a1.update(-i, -i0, i1, this.interpolator);
        }
        if (com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager.access$000().isLoggable(2)) {
            com.navdy.hud.app.ui.component.vlist.VerticalLayoutManager.access$000().v(new StringBuilder().append("target found dy=").append(a1.getDy()).toString());
        }
        this.vlist.targetFound(a1.getDy());
    }
    
    protected void updateActionForInterimTarget(android.support.v7.widget.RecyclerView$SmoothScroller$Action a) {
        super.updateActionForInterimTarget(a);
    }
}
