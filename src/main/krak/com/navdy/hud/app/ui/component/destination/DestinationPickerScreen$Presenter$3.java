package com.navdy.hud.app.ui.component.destination;

class DestinationPickerScreen$Presenter$3 implements Runnable {
    final com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter this$0;
    final com.navdy.hud.app.ui.component.destination.DestinationParcelable val$dest;
    
    DestinationPickerScreen$Presenter$3(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a, com.navdy.hud.app.ui.component.destination.DestinationParcelable a0) {
        super();
        this.this$0 = a;
        this.val$dest = a0;
    }
    
    public void run() {
        com.navdy.hud.app.framework.destinations.Destination a = new com.navdy.hud.app.framework.destinations.Destination$Builder().destinationTitle(this.val$dest.destinationTitle).destinationSubtitle(this.val$dest.destinationSubTitle).fullAddress(this.val$dest.destinationAddress).displayPositionLatitude(this.val$dest.displayLatitude).displayPositionLongitude(this.val$dest.displayLongitude).navigationPositionLatitude(this.val$dest.navLatitude).navigationPositionLongitude(this.val$dest.navLongitude).destinationType(com.navdy.hud.app.framework.destinations.Destination$DestinationType.DEFAULT).identifier(this.val$dest.identifier).favoriteDestinationType(com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_NONE).destinationIcon(this.val$dest.icon).destinationIconBkColor(this.val$dest.iconSelectedColor).destinationPlaceId(this.val$dest.destinationPlaceId).placeType(this.val$dest.placeType).distanceStr(this.val$dest.distanceStr).contacts(this.val$dest.contacts).phoneNumbers(this.val$dest.phoneNumbers).build();
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("call requestNavigation");
        com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().requestNavigationWithNavLookup(a);
    }
}
