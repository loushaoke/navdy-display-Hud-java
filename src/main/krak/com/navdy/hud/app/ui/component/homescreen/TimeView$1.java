package com.navdy.hud.app.ui.component.homescreen;

class TimeView$1 extends android.content.BroadcastReceiver {
    final com.navdy.hud.app.ui.component.homescreen.TimeView this$0;
    
    TimeView$1(com.navdy.hud.app.ui.component.homescreen.TimeView a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        if ("android.intent.action.TIMEZONE_CHANGED".equals(a0.getAction())) {
            com.navdy.hud.app.ui.component.homescreen.TimeView.access$000(this.this$0).v("timezone change broadcast");
            com.navdy.hud.app.ui.component.homescreen.TimeView.access$100(this.this$0).updateLocale();
        }
    }
}
