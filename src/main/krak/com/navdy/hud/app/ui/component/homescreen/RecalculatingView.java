package com.navdy.hud.app.ui.component.homescreen;

public class RecalculatingView extends android.widget.FrameLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private com.navdy.hud.app.maps.MapEvents$RerouteEvent lastRerouteEvent;
    private com.navdy.service.library.log.Logger logger;
    private boolean paused;
    @InjectView(R.id.recalcAnimator)
    com.navdy.hud.app.ui.component.FluctuatorAnimatorView recalcAnimator;
    @InjectView(R.id.recalcImageView)
    com.navdy.hud.app.ui.component.image.ColorImageView recalcImageView;
    
    public RecalculatingView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public RecalculatingView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public RecalculatingView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    public void hideRecalculating() {
        this.setVisibility(8);
        this.recalcAnimator.stop();
        this.lastRerouteEvent = null;
    }
    
    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        this.homeScreenView = a;
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.recalcImageView.setColor(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.reCalcAnimColor);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:recalc");
        }
    }
    
    public void onRerouteEvent(com.navdy.hud.app.maps.MapEvents$RerouteEvent a) {
        this.logger.v(new StringBuilder().append("rerouteEvent:").append(a.routeEventType).toString());
        boolean b = this.homeScreenView.isNavigationActive();
        label0: {
            label2: {
                label3: {
                    if (!b) {
                        break label3;
                    }
                    if (!com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
                        break label2;
                    }
                }
                this.logger.w("rerouteEvent:navigation not active|hasArrived|routeCalcOn");
                break label0;
            }
            this.lastRerouteEvent = a;
            if (a.routeEventType != com.navdy.hud.app.maps.MapEvents$RouteEventType.STARTED) {
                this.homeScreenView.setRecalculating(false);
                boolean b0 = this.paused;
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.getVisibility() != 0) {
                        break label0;
                    }
                }
                this.hideRecalculating();
                this.homeScreenView.getTbtView().setVisibility(0);
            } else {
                this.homeScreenView.setRecalculating(true);
                if (!this.paused) {
                    this.showRecalculating();
                }
            }
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:recalc");
            com.navdy.hud.app.maps.MapEvents$RerouteEvent a = this.lastRerouteEvent;
            if (a != null) {
                this.lastRerouteEvent = null;
                this.logger.v("::onResume:recalc set last reroute event");
                this.onRerouteEvent(a);
            }
        }
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        switch(com.navdy.hud.app.ui.component.homescreen.RecalculatingView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.recalcShrinkLeftX);
                break;
            }
            case 1: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.recalcX);
                break;
            }
        }
    }
    
    public void showRecalculating() {
        this.homeScreenView.getTbtView().setVisibility(8);
        this.setVisibility(0);
        this.recalcAnimator.start();
    }
}
