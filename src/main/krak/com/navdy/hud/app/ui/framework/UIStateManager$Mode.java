package com.navdy.hud.app.ui.framework;


public enum UIStateManager$Mode {
    EXPAND(0),
    COLLAPSE(1);

    private int value;
    UIStateManager$Mode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class UIStateManager$Mode extends Enum {
//    final private static com.navdy.hud.app.ui.framework.UIStateManager$Mode[] $VALUES;
//    final public static com.navdy.hud.app.ui.framework.UIStateManager$Mode COLLAPSE;
//    final public static com.navdy.hud.app.ui.framework.UIStateManager$Mode EXPAND;
//    
//    static {
//        EXPAND = new com.navdy.hud.app.ui.framework.UIStateManager$Mode("EXPAND", 0);
//        COLLAPSE = new com.navdy.hud.app.ui.framework.UIStateManager$Mode("COLLAPSE", 1);
//        com.navdy.hud.app.ui.framework.UIStateManager$Mode[] a = new com.navdy.hud.app.ui.framework.UIStateManager$Mode[2];
//        a[0] = EXPAND;
//        a[1] = COLLAPSE;
//        $VALUES = a;
//    }
//    
//    private UIStateManager$Mode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.framework.UIStateManager$Mode valueOf(String s) {
//        return (com.navdy.hud.app.ui.framework.UIStateManager$Mode)Enum.valueOf(com.navdy.hud.app.ui.framework.UIStateManager$Mode.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.framework.UIStateManager$Mode[] values() {
//        return $VALUES.clone();
//    }
//}
//