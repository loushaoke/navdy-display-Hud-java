package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class NavigationView$$ViewInjector {
    public NavigationView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.NavigationView a0, Object a1) {
        a0.mapView = (com.here.android.mpa.mapping.MapView)a.findRequiredView(a1, R.id.mapView, "field 'mapView'");
        a0.startDestinationFluctuatorView = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView)a.findRequiredView(a1, R.id.startDestinationFluctuator, "field 'startDestinationFluctuatorView'");
        a0.mapMask = (android.widget.ImageView)a.findRequiredView(a1, R.id.map_mask, "field 'mapMask'");
        a0.mapIconIndicator = (android.widget.ImageView)a.findRequiredView(a1, R.id.mapIconIndicator, "field 'mapIconIndicator'");
        a0.noLocationView = (com.navdy.hud.app.ui.component.homescreen.NoLocationView)a.findRequiredView(a1, R.id.noLocationContainer, "field 'noLocationView'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        a.mapView = null;
        a.startDestinationFluctuatorView = null;
        a.mapMask = null;
        a.mapIconIndicator = null;
        a.noLocationView = null;
    }
}
