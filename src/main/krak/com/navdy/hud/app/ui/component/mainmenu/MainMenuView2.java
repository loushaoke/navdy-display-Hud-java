package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class MainMenuView2 extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback callback;
    @InjectView(R.id.confirmationLayout)
    public com.navdy.hud.app.ui.component.ConfirmationLayout confirmationLayout;
    @Inject
    public com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    @InjectView(R.id.rightBackground)
    android.view.View rightBackground;
    public android.view.View scrimCover;
    com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vmenuComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.MainMenuView2.class);
    }
    
    public MainMenuView2(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public MainMenuView2(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public MainMenuView2(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.callback = (com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent$Callback)new com.navdy.hud.app.ui.component.mainmenu.MainMenuView2$1(this);
        if (!this.isInEditMode()) {
            mortar.Mortar.inject(a, this);
        }
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler((android.view.View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        this.presenter.sendCloseEvent();
        this.vmenuComponent.verticalList.cancelLoadingAnimation(1);
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.vmenuComponent = new com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent((android.view.ViewGroup)this, this.callback, true);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
        this.scrimCover = android.view.LayoutInflater.from(this.getContext()).inflate(R.layout.screen_main_menu_2_scrim, (android.view.ViewGroup)null);
        this.scrimCover.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(-1, -1));
        this.vmenuComponent.rightContainer.addView(this.scrimCover);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return this.confirmationLayout.getVisibility() != 0 && this.vmenuComponent.handleGesture(a);
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return (this.confirmationLayout.getVisibility() != 0) ? this.vmenuComponent.handleKey(a) : this.confirmationLayout.handleKey(a);
    }
    
    void performSelectionAnimation(Runnable a, int i) {
        this.vmenuComponent.performSelectionAnimation(a, i);
    }
}
