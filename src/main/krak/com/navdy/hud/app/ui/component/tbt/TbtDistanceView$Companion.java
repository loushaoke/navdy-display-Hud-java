package com.navdy.hud.app.ui.component.tbt;

final public class TbtDistanceView$Companion {
    private TbtDistanceView$Companion() {
    }
    
    public TbtDistanceView$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static int access$getFullWidth$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getFullWidth();
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getLogger();
    }
    
    final public static int access$getMediumWidth$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getMediumWidth();
    }
    
    final public static int access$getNowHeightFull$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getNowHeightFull();
    }
    
    final public static int access$getNowHeightMedium$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getNowHeightMedium();
    }
    
    final public static int access$getNowWidthFull$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getNowWidthFull();
    }
    
    final public static int access$getNowWidthMedium$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getNowWidthMedium();
    }
    
    final public static int access$getProgressHeightFull$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getProgressHeightFull();
    }
    
    final public static int access$getProgressHeightMedium$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getProgressHeightMedium();
    }
    
    final public static android.content.res.Resources access$getResources$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getResources();
    }
    
    final public static float access$getSize18$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getSize18();
    }
    
    final public static float access$getSize22$p(com.navdy.hud.app.ui.component.tbt.TbtDistanceView$Companion a) {
        return a.getSize22();
    }
    
    final private int getFullWidth() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getFullWidth$cp();
    }
    
    final private com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getLogger$cp();
    }
    
    final private int getMediumWidth() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getMediumWidth$cp();
    }
    
    final private int getNowHeightFull() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getNowHeightFull$cp();
    }
    
    final private int getNowHeightMedium() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getNowHeightMedium$cp();
    }
    
    final private int getNowWidthFull() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getNowWidthFull$cp();
    }
    
    final private int getNowWidthMedium() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getNowWidthMedium$cp();
    }
    
    final private int getProgressHeightFull() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getProgressHeightFull$cp();
    }
    
    final private int getProgressHeightMedium() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getProgressHeightMedium$cp();
    }
    
    final private android.content.res.Resources getResources() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getResources$cp();
    }
    
    final private float getSize18() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getSize18$cp();
    }
    
    final private float getSize22() {
        return com.navdy.hud.app.ui.component.tbt.TbtDistanceView.access$getSize22$cp();
    }
}
