package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class MainMenuView2$$ViewInjector {
    public MainMenuView2$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a0, Object a1) {
        a0.rightBackground = a.findRequiredView(a1, R.id.rightBackground, "field 'rightBackground'");
        a0.confirmationLayout = (com.navdy.hud.app.ui.component.ConfirmationLayout)a.findRequiredView(a1, R.id.confirmationLayout, "field 'confirmationLayout'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a) {
        a.rightBackground = null;
        a.confirmationLayout = null;
    }
}
