package com.navdy.hud.app.ui.component.destination;
import com.navdy.hud.app.R;

@Layout(R.layout.screen_destination_picker)
public class DestinationPickerScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static int CANCEL_POSITION = 1;
    private static java.util.List CONFIRMATION_CHOICES;
    final private static float MIN_DISTANCE_EXPANSION = 3000f;
    final private static float MIN_DISTANCE_THRESHOLD = 5000f;
    final private static int NAVIGATE_POSITION = 0;
    final public static String PICKER_DESTINATIONS = "PICKER_DESTINATIONS";
    final public static String PICKER_DESTINATION_ICON = "PICKER_DESTINATION_ICON";
    final public static String PICKER_HIDE_IF_NAV_STOPS = "PICKER_HIDE";
    final public static String PICKER_INITIAL_SELECTION = "PICKER_INITIAL_SELECTION";
    final public static String PICKER_LEFT_ICON = "PICKER_LEFT_ICON";
    final public static String PICKER_LEFT_ICON_BKCOLOR = "PICKER_LEFT_ICON_BKCOLOR";
    final public static String PICKER_LEFT_TITLE = "PICKER_LEFT_TITLE";
    final public static String PICKER_MAP_END_LAT = "PICKER_MAP_END_LAT";
    final public static String PICKER_MAP_END_LNG = "PICKER_MAP_END_LNG";
    final public static String PICKER_MAP_START_LAT = "PICKER_MAP_START_LAT";
    final public static String PICKER_MAP_START_LNG = "PICKER_MAP_START_LNG";
    final public static String PICKER_SHOW_DESTINATION_MAP = "PICKER_SHOW_DESTINATION_MAP";
    final public static String PICKER_SHOW_ROUTE_MAP = "PICKER_SHOW_ROUTE_MAP";
    final public static String PICKER_TITLE = "PICKER_TITLE";
    final private static int SELECTION_ANIMATION_DELAY = 1000;
    private static com.navdy.hud.app.ui.component.destination.DestinationPickerScreen instance;
    final private static java.util.Map placeResMapping;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.class);
        CONFIRMATION_CHOICES = (java.util.List)new java.util.ArrayList();
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        CONFIRMATION_CHOICES.add(a.getString(R.string.destination_start_navigation_yes));
        CONFIRMATION_CHOICES.add(a.getString(R.string.destination_start_navigation_cancel));
        placeResMapping = (java.util.Map)new java.util.HashMap();
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_GAS, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder(R.drawable.icon_place_gas, R.string.gas, R.color.mm_search_gas, R.drawable.icon_pin_dot_destination_indigo, (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$1)null));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_PARKING, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder(R.drawable.icon_place_parking, R.string.parking, R.color.mm_search_parking, R.drawable.icon_pin_dot_destination_indigo, (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$1)null));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_RESTAURANT, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder(R.drawable.icon_place_restaurant, R.string.food, R.color.mm_search_food, R.drawable.icon_pin_dot_destination_orange, (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$1)null));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_STORE, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder(R.drawable.icon_place_store, R.string.grocery_store, R.color.mm_search_grocery_store, R.drawable.icon_pin_dot_destination_blue, (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$1)null));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_COFFEE, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder(R.drawable.icon_place_coffee, R.string.coffee, R.color.mm_search_coffee, R.drawable.icon_pin_dot_destination_orange, (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$1)null));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_ATM, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder(R.drawable.icon_place_a_t_m, R.string.atm, R.color.mm_search_atm, R.drawable.icon_pin_dot_destination_blue, (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$1)null));
        placeResMapping.put(com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_HOSPITAL, new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder(R.drawable.icon_place_hospital, R.string.hospital, R.color.mm_search_hospital, R.drawable.icon_pin_dot_destination_red, (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$1)null));
    }
    
    public DestinationPickerScreen() {
        instance = this;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.navdy.hud.app.ui.component.destination.DestinationPickerScreen access$200() {
        return instance;
    }
    
    static com.navdy.hud.app.ui.component.destination.DestinationPickerScreen access$202(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen a) {
        instance = a;
        return a;
    }
    
    static Object access$300(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen a) {
        return a.arguments2;
    }
    
    static java.util.List access$500() {
        return CONFIRMATION_CHOICES;
    }
    
    public static com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder getPlaceTypeHolder(com.navdy.service.library.events.places.PlaceType a) {
        return (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$PlaceTypeResourceHolder)placeResMapping.get(a);
    }
    
    public int getAnimationIn(flow.Flow$Direction a) {
        return -1;
    }
    
    public int getAnimationOut(flow.Flow$Direction a) {
        return -1;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER;
    }
}
