package com.navdy.hud.app.ui.framework;

public class AnimationQueue {
    private boolean animationRunning;
    private java.util.Queue animatorQueue;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener defaultAnimationListener;
    
    public AnimationQueue() {
        this.animatorQueue = (java.util.Queue)new java.util.concurrent.LinkedBlockingQueue();
        this.defaultAnimationListener = new com.navdy.hud.app.ui.framework.AnimationQueue$1(this);
    }
    
    static boolean access$002(com.navdy.hud.app.ui.framework.AnimationQueue a, boolean b) {
        a.animationRunning = b;
        return b;
    }
    
    static void access$100(com.navdy.hud.app.ui.framework.AnimationQueue a) {
        a.runQueuedAnimation();
    }
    
    private void runQueuedAnimation() {
        if (this.animatorQueue.size() != 0) {
            this.startAnimation((android.animation.AnimatorSet)this.animatorQueue.remove());
        }
    }
    
    public boolean isAnimationRunning() {
        return this.animationRunning;
    }
    
    public void startAnimation(android.animation.AnimatorSet a) {
        if (this.animationRunning) {
            this.animatorQueue.add(a);
        } else {
            this.animationRunning = true;
            a.addListener((android.animation.Animator$AnimatorListener)this.defaultAnimationListener);
            a.start();
        }
    }
}
