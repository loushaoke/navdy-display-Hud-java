package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$10 implements Runnable {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView this$0;
    
    NavigationView$10(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.here.android.mpa.common.GeoCoordinate a = com.navdy.hud.app.ui.component.homescreen.NavigationView.access$500(this.this$0).getCoordinate();
        com.here.android.mpa.mapping.Map$PixelResult a0 = com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).projectToPixel(a);
        if (a0 != null) {
            android.graphics.PointF a1 = a0.getResult();
            if (a1 != null) {
                int i = (int)a1.x;
                int i0 = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.startFluctuatorDimension / 2;
                int i1 = (int)a1.y;
                int i2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.startFluctuatorDimension / 2;
                com.navdy.hud.app.ui.component.homescreen.NavigationView.access$400(this.this$0).post((Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$10$1(this, i - i0, i1 - i2));
            }
        }
    }
}
