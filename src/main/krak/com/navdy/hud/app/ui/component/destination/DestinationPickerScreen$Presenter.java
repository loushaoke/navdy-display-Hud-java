package com.navdy.hud.app.ui.component.destination;
import com.navdy.hud.app.R;
import javax.inject.Inject;
import com.squareup.otto.Subscribe;

public class DestinationPickerScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    @Inject
    com.squareup.otto.Bus bus;
    private boolean closed;
    private java.util.List data;
    private java.util.List destinationGeoBBoxList;
    private java.util.List destinationGeoList;
    private int destinationIcon;
    com.navdy.hud.app.ui.component.destination.DestinationParcelable[] destinations;
    boolean doNotAddOriginalRoute;
    private com.here.android.mpa.common.GeoCoordinate end;
    private boolean handledSelection;
    private boolean hideScreenOnNavStop;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    int initialSelection;
    int itemSelection;
    private int leftBkColor;
    private int leftIcon;
    private String leftTitle;
    private com.navdy.hud.app.ui.component.homescreen.NavigationView navigationView;
    private com.navdy.hud.app.ui.component.destination.IDestinationPicker pickerCallback;
    private boolean registered;
    private boolean showDestinationMap;
    private boolean showRouteMap;
    private com.here.android.mpa.common.GeoCoordinate start;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode switchBackMode;
    private String title;
    
    public DestinationPickerScreen$Presenter() {
    }
    
    static java.util.List access$1000(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a) {
        return a.data;
    }
    
    static boolean access$1102(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a, boolean b) {
        a.handledSelection = b;
        return b;
    }
    
    static boolean access$1200(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a) {
        return a.showRouteMap;
    }
    
    static boolean access$1300(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a, int i) {
        return a.isNotADestination(i);
    }
    
    static void access$1400(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a, com.navdy.hud.app.ui.component.destination.DestinationPickerView a0, com.navdy.hud.app.ui.component.destination.DestinationParcelable a1) {
        a.launchDestination(a0, a1);
    }
    
    static com.navdy.hud.app.ui.component.destination.IDestinationPicker access$400(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a) {
        return a.pickerCallback;
    }
    
    static Object access$600(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a) {
        return a.getView();
    }
    
    static Object access$700(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a) {
        return a.getView();
    }
    
    static void access$800(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a, com.navdy.hud.app.ui.component.destination.DestinationPickerView a0, int i, int i0) {
        a.performAction(a0, i, i0);
    }
    
    static Object access$900(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter a) {
        return a.getView();
    }
    
    private void buildDestinationBBox() {
        java.util.ArrayList a = new java.util.ArrayList();
        com.here.android.mpa.common.GeoCoordinate a0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("destinationbbox-ce:").append(a0).toString());
        this.destinationGeoList = (java.util.List)new java.util.ArrayList();
        this.destinationGeoBBoxList = (java.util.List)new java.util.ArrayList();
        int i = 0;
        while(i < this.destinations.length) {
            double d = this.destinations[i].displayLatitude;
            int i0 = (d > 0.0) ? 1 : (d == 0.0) ? 0 : -1;
            label2: {
                label1: {
                    if (i0 != 0) {
                        break label1;
                    }
                    if (this.destinations[i].displayLongitude != 0.0) {
                        break label1;
                    }
                    this.destinationGeoList.add(null);
                    this.destinationGeoBBoxList.add(null);
                    break label2;
                }
                a.clear();
                if (a0 != null) {
                    a.add(a0);
                }
                com.here.android.mpa.common.GeoCoordinate a1 = new com.here.android.mpa.common.GeoCoordinate(this.destinations[i].displayLatitude, this.destinations[i].displayLongitude);
                a.add(a1);
                com.here.android.mpa.common.GeoBoundingBox a2 = com.here.android.mpa.common.GeoBoundingBox.getBoundingBoxContainingGeoCoordinates((java.util.List)a);
                label0: {
                    Throwable a3 = null;
                    if (a0 == null) {
                        break label0;
                    }
                    if (!((float)(long)a0.distanceTo(a1) <= 5000f)) {
                        break label0;
                    }
                    try {
                        a2.expand(3000f, 3000f);
                        break label0;
                    } catch(Throwable a4) {
                        a3 = a4;
                    }
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().e("expand bbox", a3);
                }
                this.destinationGeoList.add(a1);
                this.destinationGeoBBoxList.add(a2);
            }
            i = i + 1;
        }
    }
    
    private boolean isNotADestination(int i) {
        boolean b = false;
        com.navdy.hud.app.ui.component.destination.DestinationParcelable a = (com.navdy.hud.app.ui.component.destination.DestinationParcelable)((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.data.get(i)).state;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.type != com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType.DESTINATION) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void launchDestination(com.navdy.hud.app.ui.component.destination.DestinationPickerView a, com.navdy.hud.app.ui.component.destination.DestinationParcelable a0) {
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("launchDestination {").append(a0.destinationTitle).append("} display {").append(a0.displayLatitude).append(",").append(a0.displayLongitude).append("}").append(" nav {").append(a0.navLatitude).append(",").append(a0.navLongitude).append("}").toString());
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            this.close((Runnable)new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter$3(this, a0));
        } else {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().w("Here maps engine not initialized, exit");
            this.close((Runnable)new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter$2(this));
        }
    }
    
    private void performAction(com.navdy.hud.app.ui.component.destination.DestinationPickerView a, int i, int i0) {
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("performAction {").append(i).append(",").append(i0).append("}").toString());
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        boolean b = this.showRouteMap;
        label0: {
            label1: {
                if (b) {
                    break label1;
                }
                if (!this.showDestinationMap) {
                    break label0;
                }
            }
            a.setBackgroundColor(-16777216);
            if (this.navigationView != null) {
                this.navigationView.setMapMaskVisibility(0);
            }
        }
        a.vmenuComponent.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter$5(this, i0, i), 1000);
    }
    
    private void showConfirmation(com.navdy.hud.app.ui.component.destination.DestinationPickerView a, com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a0) {
        com.navdy.hud.app.ui.component.destination.DestinationParcelable a1 = (com.navdy.hud.app.ui.component.destination.DestinationParcelable)((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.data.get(a0.pos)).state;
        com.navdy.hud.app.ui.component.destination.DestinationConfirmationHelper.configure(a.confirmationLayout, a1);
        a.confirmationLayout.setChoices(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$500(), 0, (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter$4(this, a0));
        a.confirmationLayout.setVisibility(0);
    }
    
    private void updateView() {
        com.navdy.hud.app.ui.component.destination.DestinationPickerView a = (com.navdy.hud.app.ui.component.destination.DestinationPickerView)this.getView();
        label2: if (a != null) {
            android.content.res.Resources a0 = a.getResources();
            boolean b = this.showRouteMap;
            label8: {
                label7: {
                    if (b) {
                        break label7;
                    }
                    if (this.showDestinationMap) {
                        break label7;
                    }
                    a.vmenuComponent.setLeftContainerWidth(a0.getDimensionPixelSize(R.dimen.dest_picker_left_container_w));
                    a.vmenuComponent.setRightContainerWidth(a0.getDimensionPixelSize(R.dimen.dest_picker_right_container_w));
                    break label8;
                }
                a.vmenuComponent.leftContainer.removeAllViews();
                android.view.View a1 = android.view.LayoutInflater.from(a.getContext()).inflate(R.layout.screen_destination_picker_content, (android.view.ViewGroup)a, false);
                a.vmenuComponent.leftContainer.addView(a1);
                a.vmenuComponent.setLeftContainerWidth(a0.getDimensionPixelSize(R.dimen.dest_picker_map_left_container_w));
                a.vmenuComponent.setRightContainerWidth(a0.getDimensionPixelSize(R.dimen.dest_picker_map_right_container_w));
                a.vmenuComponent.rightContainer.setBackgroundColor(-16777216);
                a.vmenuComponent.closeContainer.setBackgroundColor(-16777216);
                a.setBackgroundColor(-16777216);
                a.rightBackground.setVisibility(0);
                if (this.showRouteMap) {
                    com.here.android.mpa.common.GeoBoundingBox a2 = null;
                    com.here.android.mpa.routing.Route a3 = null;
                    String s = this.destinations[0].routeId;
                    label6: {
                        com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a4 = null;
                        label4: {
                            label5: {
                                if (s == null) {
                                    break label5;
                                }
                                a4 = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(s);
                                if (a4 != null) {
                                    break label4;
                                }
                            }
                            a2 = null;
                            a3 = null;
                            break label6;
                        }
                        a3 = a4.route;
                        a2 = a4.route.getBoundingBox();
                    }
                    label3: {
                        Throwable a5 = null;
                        if (a2 != null) {
                            break label3;
                        }
                        if (this.start == null) {
                            break label3;
                        }
                        try {
                            a2 = new com.here.android.mpa.common.GeoBoundingBox(this.start, 5000f, 5000f);
                            break label3;
                        } catch(Throwable a6) {
                            a5 = a6;
                        }
                        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().e(a5);
                    }
                    this.navigationView.switchToRouteSearchMode(this.start, this.end, a2, a3, false, (java.util.List)null, -1);
                } else {
                    com.here.android.mpa.routing.Route a7 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentRoute();
                    this.navigationView.switchToRouteSearchMode(this.start, this.end, (com.here.android.mpa.common.GeoBoundingBox)this.destinationGeoBBoxList.get(0), a7, false, this.destinationGeoList, this.destinationIcon);
                }
            }
            if (this.leftTitle != null) {
                a.vmenuComponent.selectedText.setText((CharSequence)this.leftTitle);
            }
            if (this.leftIcon != 0) {
                if (this.leftBkColor == 0) {
                    a.vmenuComponent.setSelectedIconImage(this.leftIcon, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
                } else {
                    a.vmenuComponent.setSelectedIconColorImage(this.leftIcon, this.leftBkColor, (android.graphics.Shader)null, 1f);
                }
            }
            com.navdy.hud.app.ui.component.destination.DestinationParcelable[] a8 = this.destinations;
            label0: {
                label1: {
                    if (a8 == null) {
                        break label1;
                    }
                    if (this.destinations.length != 0) {
                        break label0;
                    }
                }
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().w("no destinations");
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_BACK).build());
                break label2;
            }
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("title:").append(this.title).toString());
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("destinations:").append(this.destinations.length).toString());
            java.util.ArrayList a9 = new java.util.ArrayList();
            ((java.util.List)a9).add(com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder.buildModel(this.title));
            int i = 0;
            while(i < this.destinations.length) {
                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a10 = (this.destinations[i].iconUnselected != 0) ? com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(this.destinations[i].id, this.destinations[i].icon, this.destinations[i].iconUnselected, this.destinations[i].iconSelectedColor, this.destinations[i].iconDeselectedColor, this.destinations[i].destinationTitle, this.destinations[i].destinationSubTitle, this.destinations[i].destinationSubTitle2) : com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(this.destinations[i].id, this.destinations[i].icon, this.destinations[i].iconSelectedColor, this.destinations[i].iconDeselectedColor, this.destinations[i].iconSelectedColor, this.destinations[i].destinationTitle, this.destinations[i].destinationSubTitle, this.destinations[i].destinationSubTitle2);
                a10.subTitleFormatted = this.destinations[i].destinationSubTitleFormatted;
                a10.subTitle2Formatted = this.destinations[i].destinationSubTitle2Formatted;
                a10.state = this.destinations[i];
                ((java.util.List)a9).add(a10);
                i = i + 1;
            }
            this.data = (java.util.List)a9;
            a.vmenuComponent.updateView(this.data, this.initialSelection, false);
        }
    }
    
    void clearSelection() {
        this.closed = false;
        this.handledSelection = false;
    }
    
    void clearState() {
        this.initialSelection = 0;
        this.title = null;
        this.destinations = null;
        this.showRouteMap = false;
        this.showDestinationMap = false;
        this.destinationIcon = -1;
        this.switchBackMode = null;
        this.destinationGeoList = null;
        this.destinationGeoBBoxList = null;
        this.itemSelection = -1;
        this.start = null;
        this.end = null;
        this.data = null;
        this.pickerCallback = null;
        this.doNotAddOriginalRoute = false;
    }
    
    void close() {
        this.close((Runnable)null);
    }
    
    void close(Runnable a) {
        if (this.closed) {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("already closed");
        } else {
            boolean b = this.showRouteMap;
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!this.showDestinationMap) {
                        break label0;
                    }
                }
                com.navdy.hud.app.ui.component.destination.DestinationPickerView a0 = (com.navdy.hud.app.ui.component.destination.DestinationPickerView)this.getView();
                if (a0 != null) {
                    a0.setBackgroundColor(-16777216);
                }
                if (this.navigationView != null) {
                    this.navigationView.setMapMaskVisibility(0);
                }
            }
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
            this.closed = true;
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("close");
            com.navdy.hud.app.ui.component.destination.DestinationPickerView a1 = (com.navdy.hud.app.ui.component.destination.DestinationPickerView)this.getView();
            if (a1 != null) {
                a1.vmenuComponent.animateOut((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$Presenter$1(this, a));
            }
        }
    }
    
    boolean isAlive() {
        return this.registered;
    }
    
    boolean isClosed() {
        return this.closed;
    }
    
    boolean isShowDestinationMap() {
        return this.showDestinationMap;
    }
    
    boolean isShowingRouteMap() {
        return this.showRouteMap;
    }
    
    boolean itemClicked(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        boolean b = false;
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("itemClicked:").append(a.id).append(",").append(a.pos).toString());
        label2: if (this.handledSelection) {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("already handled [").append(a.id).append("], ").append(a.pos).toString());
            b = true;
        } else {
            this.handledSelection = true;
            com.navdy.hud.app.ui.component.destination.DestinationPickerView a0 = (com.navdy.hud.app.ui.component.destination.DestinationPickerView)this.getView();
            if (a0 == null) {
                b = true;
            } else {
                boolean b0 = this.showDestinationMap;
                label0: {
                    label1: {
                        if (!b0) {
                            break label1;
                        }
                        if (this.isNotADestination(a.pos)) {
                            break label1;
                        }
                        com.navdy.hud.app.maps.here.HereNavigationManager a1 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                        if (!a1.isNavigationModeOn()) {
                            break label1;
                        }
                        if (!a1.hasArrived()) {
                            break label0;
                        }
                    }
                    this.performAction(a0, a.id, a.pos);
                    b = this.handledSelection;
                    break label2;
                }
                this.showConfirmation(a0, a);
                b = true;
            }
        }
        return b;
    }
    
    void itemSelected(int i) {
        com.navdy.hud.app.analytics.AnalyticsSupport.recordNearbySearchDestinationScroll(i);
        if (this.showRouteMap) {
            String s = this.destinations[i].routeId;
            com.navdy.hud.app.maps.here.HereRouteCache$RouteInfo a = com.navdy.hud.app.maps.here.HereRouteCache.getInstance().getRoute(s);
            if (a == null) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("itemSelected: route not found:").append(s).toString());
            } else {
                com.here.android.mpa.common.GeoBoundingBox a0 = a.route.getBoundingBox();
                this.navigationView.zoomToBoundBox(a0, a.route, true, true);
            }
        } else if (this.showDestinationMap) {
            this.navigationView.changeMarkerSelection(i, this.itemSelection);
            com.here.android.mpa.common.GeoBoundingBox a1 = (com.here.android.mpa.common.GeoBoundingBox)this.destinationGeoBBoxList.get(i);
            if (a1 != null) {
                this.navigationView.zoomToBoundBox(a1, (com.here.android.mpa.routing.Route)null, false, true);
            }
            this.itemSelection = i;
        }
    }
    
    void itemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        label1: if (this.data != null) {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("itemSelected:").append(a.id).append(" , ").append(a.pos).toString());
            com.navdy.hud.app.ui.component.destination.IDestinationPicker a0 = this.pickerCallback;
            label0: {
                if (a0 == null) {
                    break label0;
                }
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState a1 = new com.navdy.hud.app.ui.component.destination.DestinationPickerScreen$DestinationPickerState();
                if (!this.pickerCallback.onItemSelected(a.id, a.pos - 1, a1)) {
                    break label0;
                }
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("overridden onItemSelected : ").append(a.id).append(",").append(a.pos).toString());
                break label1;
            }
            if (this.isNotADestination(a.pos)) {
                this.navigationView.changeMarkerSelection(-1, this.itemSelection);
                this.itemSelection = -1;
            } else {
                this.itemSelected(a.pos - 1);
            }
        }
    }
    
    public void onLoad(android.os.Bundle a) {
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("onLoad");
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enablePhoneNotifications(true);
        this.clearState();
        this.reset();
        this.bus.register(this);
        this.registered = true;
        com.navdy.hud.app.ui.framework.UIStateManager a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        this.navigationView = a0.getNavigationView();
        this.homeScreenView = a0.getHomescreenView();
        label0: if (a != null) {
            this.leftTitle = a.getString("PICKER_LEFT_TITLE");
            this.leftIcon = a.getInt("PICKER_LEFT_ICON");
            this.leftBkColor = a.getInt("PICKER_LEFT_ICON_BKCOLOR");
            this.initialSelection = a.getInt("PICKER_INITIAL_SELECTION", 0);
            this.title = a.getString("PICKER_TITLE");
            this.destinations = (com.navdy.hud.app.ui.component.destination.DestinationParcelable[])a.getParcelableArray("PICKER_DESTINATIONS");
            this.hideScreenOnNavStop = a.getBoolean("PICKER_HIDE");
            this.showRouteMap = a.getBoolean("PICKER_SHOW_ROUTE_MAP");
            label3: if (this.showRouteMap) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("show route map");
                double d = a.getDouble("PICKER_MAP_START_LAT");
                double d0 = a.getDouble("PICKER_MAP_START_LNG");
                int i = (d > 0.0) ? 1 : (d == 0.0) ? 0 : -1;
                label5: {
                    label4: {
                        if (i == 0) {
                            break label4;
                        }
                        if (d0 == 0.0) {
                            break label4;
                        }
                        this.start = new com.here.android.mpa.common.GeoCoordinate(d, d0);
                        break label5;
                    }
                    this.start = null;
                }
                double d1 = a.getDouble("PICKER_MAP_END_LAT");
                double d2 = a.getDouble("PICKER_MAP_END_LNG");
                int i0 = (d1 > 0.0) ? 1 : (d1 == 0.0) ? 0 : -1;
                label2: {
                    if (i0 == 0) {
                        break label2;
                    }
                    if (d2 == 0.0) {
                        break label2;
                    }
                    this.end = new com.here.android.mpa.common.GeoCoordinate(d1, d2);
                    break label3;
                }
                this.end = null;
            } else {
                this.showDestinationMap = a.getBoolean("PICKER_SHOW_DESTINATION_MAP");
                if (this.showDestinationMap) {
                    this.destinationIcon = a.getInt("PICKER_DESTINATION_ICON", -1);
                    com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("show dest map");
                    this.buildDestinationBBox();
                }
                this.start = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                com.navdy.service.library.events.navigation.NavigationRouteRequest a1 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentNavigationRouteRequest();
                if (a1 == null) {
                    this.end = null;
                } else {
                    this.end = new com.here.android.mpa.common.GeoCoordinate(a1.destination.latitude.doubleValue(), a1.destination.longitude.doubleValue());
                }
            }
            boolean b = this.showRouteMap;
            label1: {
                if (b) {
                    break label1;
                }
                if (!this.showDestinationMap) {
                    break label0;
                }
            }
            this.switchBackMode = this.homeScreenView.getDisplayMode();
            if (this.switchBackMode != com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append(" onLoad switchbackmode: ").append(this.switchBackMode).toString());
                this.homeScreenView.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP);
            } else {
                com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v("switchbackmode: null");
            }
        }
        if (com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$200() == null) {
            this.pickerCallback = null;
        } else {
            this.pickerCallback = (com.navdy.hud.app.ui.component.destination.IDestinationPicker)com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$300(com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$200());
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$202((com.navdy.hud.app.ui.component.destination.DestinationPickerScreen)null);
        }
        this.updateView();
        super.onLoad(a);
    }
    
    @Subscribe
    public void onNavigationModeChanged(com.navdy.hud.app.maps.MapEvents$NavigationModeChange a) {
        if (this.hideScreenOnNavStop && (com.navdy.hud.app.ui.component.destination.DestinationPickerView)this.getView() != null) {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("onNavigationModeChanged event[").append(a.navigationMode).append("]").toString());
            if (a.navigationMode == com.navdy.hud.app.maps.NavigationMode.MAP) {
                this.close();
            }
        }
    }
    
    public void onUnload() {
        boolean b = !this.doNotAddOriginalRoute;
        if (!this.showRouteMap) {
            b = true;
        }
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("onUnload addbackRoute:").append(b).toString());
        com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$202((com.navdy.hud.app.ui.component.destination.DestinationPickerScreen)null);
        this.clearState();
        this.navigationView.switchBackfromRouteSearchMode(b);
        if (this.switchBackMode != null) {
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.access$100().v(new StringBuilder().append("onUnload switchbackmode: ").append(this.switchBackMode).toString());
            this.homeScreenView.setDisplayMode(this.switchBackMode);
        }
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        this.pickerCallback = null;
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
        }
        this.reset();
        super.onUnload();
    }
    
    void reset() {
        this.closed = false;
        this.handledSelection = false;
        this.data = null;
        this.hideScreenOnNavStop = false;
    }
    
    void startMapFluctuator() {
        if (this.registered && this.navigationView != null) {
            this.navigationView.cleanupFluctuator();
            this.navigationView.startFluctuator();
        }
    }
}
