package com.navdy.hud.app.ui.component;

final class ChoiceLayout$Choice$1 implements android.os.Parcelable$Creator {
    ChoiceLayout$Choice$1() {
    }
    
    public com.navdy.hud.app.ui.component.ChoiceLayout$Choice createFromParcel(android.os.Parcel a) {
        return new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.hud.app.ui.component.ChoiceLayout$Choice[] newArray(int i) {
        return new com.navdy.hud.app.ui.component.ChoiceLayout$Choice[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
