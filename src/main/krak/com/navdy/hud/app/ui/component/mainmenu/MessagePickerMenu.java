package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

final public class MessagePickerMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final public static com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion Companion;
    final private static int SELECTION_ANIMATION_DELAY = 1000;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static int baseMessageId = 100;
    final private static android.content.Context context;
    final private static int fluctuatorColor;
    final private static com.navdy.service.library.log.Logger logger;
    final private static int messageColor;
    final private static android.content.res.Resources resources;
    private int backSelection;
    private int backSelectionId;
    private java.util.List cachedList;
    final private com.navdy.hud.app.framework.contacts.Contact contact;
    final private java.util.List messages;
    final private String notificationId;
    final private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    final private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    final private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger("MessagePickerMenu");
        SELECTION_ANIMATION_DELAY = 1000;
        context = com.navdy.hud.app.HudApplication.getAppContext();
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        fluctuatorColor = android.support.v4.content.ContextCompat.getColor(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getContext$p(Companion), R.color.mm_back);
        messageColor = android.support.v4.content.ContextCompat.getColor(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getContext$p(Companion), R.color.share_location);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getFluctuatorColor$p(Companion), com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getFluctuatorColor$p(Companion), com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getResources$p(Companion).getString(R.string.back), (String)null);
        baseMessageId = 100;
    }
    
    public MessagePickerMenu(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a0, com.navdy.hud.app.ui.component.mainmenu.IMenu a1, java.util.List a2, com.navdy.hud.app.framework.contacts.Contact a3, String s) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "vscrollComponent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "presenter");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "parent");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a2, "messages");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a3, "contact");
        this.vscrollComponent = a;
        this.presenter = a0;
        this.parent = a1;
        this.messages = a2;
        this.contact = a3;
        this.notificationId = s;
    }
    
    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model access$getBack$cp() {
        return back;
    }
    
    final public static int access$getBaseMessageId$cp() {
        return baseMessageId;
    }
    
    final public static android.content.Context access$getContext$cp() {
        return context;
    }
    
    final public static int access$getFluctuatorColor$cp() {
        return fluctuatorColor;
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final public static int access$getMessageColor$cp() {
        return messageColor;
    }
    
    final public static java.util.List access$getMessages$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu a) {
        return a.messages;
    }
    
    final public static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$getPresenter$p(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu a) {
        return a.presenter;
    }
    
    final public static android.content.res.Resources access$getResources$cp() {
        return resources;
    }
    
    final public static int access$getSELECTION_ANIMATION_DELAY$cp() {
        return SELECTION_ANIMATION_DELAY;
    }
    
    final public static void access$sendMessage(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu a, String s) {
        a.sendMessage(s);
    }
    
    final private void sendMessage(String s) {
        com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getLogger$p(Companion).v(new StringBuilder().append("sendMessage:").append(this.contact.number).toString());
        com.navdy.hud.app.framework.glance.GlanceHandler a = com.navdy.hud.app.framework.glance.GlanceHandler.getInstance();
        if (this.notificationId != null) {
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(this.notificationId);
        }
        if (a.sendMessage(this.contact.number, s, this.contact.name)) {
            a.sendSmsSuccessNotification(java.util.UUID.randomUUID().toString(), this.contact.number, s, this.contact.name);
        } else {
            a.sendSmsFailedNotification(this.contact.number, s, this.contact.name);
        }
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        return 1;
    }
    
    public java.util.List getItems() {
        Object a = null;
        if (this.cachedList == null) {
            java.util.ArrayList a0 = new java.util.ArrayList();
            a0.add(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getBack$p(Companion));
            Object a1 = this.messages.iterator();
            int i = 0;
            while(((java.util.Iterator)a1).hasNext()) {
                String s = (String)((java.util.Iterator)a1).next();
                com.navdy.hud.app.ui.component.vlist.VerticalList$Model a2 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getBaseMessageId$p(Companion) + i, R.drawable.icon_message, com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getMessageColor$p(Companion), com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getMessageColor$p(Companion), s, (String)null);
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a2, "IconBkColorViewHolder.bu\u2026 messageColor, msg, null)");
                a0.add(a2);
                i = i + 1;
            }
            this.cachedList = (java.util.List)a0;
            a = a0;
        } else {
            java.util.List a3 = this.cachedList;
            if (a3 == null) {
                throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model>");
            }
            a = kotlin.jvm.internal.TypeIntrinsics.asMutableList(a3);
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)a0.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MESSAGE_PICKER;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "selection");
        com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getLogger$p(Companion).v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        if (a.id == R.id.menu_back) {
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
            this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
            this.backSelectionId = 0;
        } else {
            this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$selectItem$1(this, a), com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getSELECTION_ANIMATION_DELAY$p(Companion));
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
        this.backSelectionId = i;
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_message, com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getMessageColor$p(Companion), (android.graphics.Shader)null, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)com.navdy.hud.app.ui.component.mainmenu.MessagePickerMenu$Companion.access$getResources$p(Companion).getString(R.string.message));
    }
    
    public void showToolTip() {
    }
}
