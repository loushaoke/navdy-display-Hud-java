package com.navdy.hud.app.ui.activity;

public class Main extends com.navdy.hud.app.screen.BaseScreen {
    final public static String DEMO_VIDEO_PROPERTY = "persist.sys.demo.video";
    final private static String INSTALLING_OTA_TOAST_ID = "#ota#toast";
    final public static String MIME_TYPE_VIDEO = "video/mp4";
    final static String PREFERENCE_HOME_SCREEN_MODE = "PREFERENCE_LAST_HOME_SCREEN_MODE";
    private static android.util.SparseArray SCREEN_MAP;
    final public static int SNAPSHOT_TITLE_PICKER_DELAY_MILLIS = 2000;
    public static com.navdy.hud.app.ui.activity.Main$ProtocolStatus mProtocolStatus;
    private static com.navdy.hud.app.ui.activity.Main mainScreen;
    private static com.navdy.hud.app.ui.activity.Main$Presenter presenter;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.activity.Main.class);
        SCREEN_MAP = new android.util.SparseArray();
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH.ordinal(), com.navdy.hud.app.screen.FirstLaunchScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_HOME.ordinal(), com.navdy.hud.app.ui.component.homescreen.HomeScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME.ordinal(), com.navdy.hud.app.screen.WelcomeScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_MAIN_MENU.ordinal(), com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_OTA_CONFIRMATION.ordinal(), com.navdy.hud.app.screen.OSUpdateConfirmationScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_SHUTDOWN_CONFIRMATION.ordinal(), com.navdy.hud.app.screen.ShutDownScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_AUTO_BRIGHTNESS.ordinal(), com.navdy.hud.app.screen.AutoBrightnessScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_FACTORY_RESET.ordinal(), com.navdy.hud.app.screen.FactoryResetScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING.ordinal(), com.navdy.hud.app.screen.DialManagerScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_PROGRESS.ordinal(), com.navdy.hud.app.screen.DialUpdateProgressScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_TEMPERATURE_WARNING.ordinal(), com.navdy.hud.app.screen.TemperatureWarningScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE.ordinal(), com.navdy.hud.app.screen.ForceUpdateScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_UPDATE_CONFIRMATION.ordinal(), com.navdy.hud.app.screen.DialUpdateConfirmationScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_GESTURE_LEARNING.ordinal(), com.navdy.hud.app.screen.GestureLearningScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER.ordinal(), com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.class);
        SCREEN_MAP.put(com.navdy.service.library.events.ui.Screen.SCREEN_MUSIC_DETAILS.ordinal(), com.navdy.hud.app.framework.music.MusicDetailsScreen.class);
        mProtocolStatus = com.navdy.hud.app.ui.activity.Main$ProtocolStatus.PROTOCOL_VALID;
    }
    
    public Main() {
        mainScreen = this;
    }
    
    static android.util.SparseArray access$1100() {
        return SCREEN_MAP;
    }
    
    static com.navdy.service.library.log.Logger access$200() {
        return sLogger;
    }
    
    static void access$300() {
        com.navdy.hud.app.ui.activity.Main.showBrightnessNotification();
    }
    
    static com.navdy.hud.app.ui.activity.Main$Presenter access$702(com.navdy.hud.app.ui.activity.Main$Presenter a) {
        presenter = a;
        return a;
    }
    
    static com.navdy.hud.app.ui.activity.Main access$800() {
        return mainScreen;
    }
    
    public static void handleLibraryInitializationError(String s) {
        sLogger.e(new StringBuilder().append("Forcing update due to initialization failure: ").append(s).toString());
        mProtocolStatus = com.navdy.hud.app.ui.activity.Main$ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE;
        if (presenter == null) {
            sLogger.e("unable to launch FORCE_UPDATE screen.");
        } else {
            presenter.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_FORCE_UPDATE).build());
        }
    }
    
    public static void saveHomeScreenPreference(android.content.SharedPreferences a, int i) {
        boolean b = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        android.content.SharedPreferences a0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(com.navdy.hud.app.HudApplication.getAppContext());
        if (!b && a0 != null) {
            a0.edit().putInt("PREFERENCE_LAST_HOME_SCREEN_MODE", i).apply();
        }
        if (a != null) {
            a.edit().putInt("PREFERENCE_LAST_HOME_SCREEN_MODE", i).apply();
        }
    }
    
    private static void showBrightnessNotification() {
        if (com.navdy.hud.app.settings.HUDSettings.USE_ADAPTIVE_AUTOBRIGHTNESS) {
            sLogger.v("Use adaptive autobrightness");
            com.navdy.hud.app.framework.AdaptiveBrightnessNotification.showNotification();
        } else {
            sLogger.v("Use non-adaptive autobrightness");
            com.navdy.hud.app.framework.BrightnessNotification.showNotification();
        }
    }
    
    public void addNotificationExtensionView(android.view.View a) {
        if (presenter != null) {
            presenter.addNotificationExtensionView(a);
        }
    }
    
    public void clearInputFocus() {
        if (presenter != null) {
            presenter.clearInputFocus();
        }
    }
    
    public void ejectMainLowerView() {
        if (presenter != null) {
            presenter.ejectMainLowerView();
        }
    }
    
    public void enableNotificationColor(boolean b) {
        if (presenter != null) {
            presenter.enableNotificationColor(b);
        }
    }
    
    public void enableSystemTray(boolean b) {
        if (presenter != null) {
            presenter.enableSystemTray(b);
        }
    }
    
    public int getAnimationIn(flow.Flow$Direction a) {
        return -1;
    }
    
    public int getAnimationOut(flow.Flow$Direction a) {
        return -1;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.ui.activity.Main$Module(this);
    }
    
    public android.view.View getExpandedNotificationCoverView() {
        return (presenter != null) ? presenter.getExpandedNotificationCoverView() : null;
    }
    
    public android.widget.FrameLayout getExpandedNotificationView() {
        return (presenter != null) ? presenter.getExpandedNotificationView() : null;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler getInputHandler() {
        return (com.navdy.hud.app.manager.InputManager$IInputHandler)presenter;
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public android.view.ViewGroup getNotificationExtensionViewHolder() {
        android.view.ViewGroup a = (presenter == null) ? null : presenter.getNotificationExtensionView();
        return a;
    }
    
    public com.navdy.hud.app.ui.component.carousel.CarouselIndicator getNotificationIndicator() {
        return (presenter != null) ? presenter.getNotificationIndicator() : null;
    }
    
    public com.navdy.hud.app.ui.component.carousel.ProgressIndicator getNotificationScrollIndicator() {
        return (presenter != null) ? presenter.getNotificationScrollIndicator() : null;
    }
    
    public com.navdy.hud.app.view.NotificationView getNotificationView() {
        return (presenter != null) ? presenter.getNotificationView() : null;
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return null;
    }
    
    public void handleKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        if (presenter != null) {
            presenter.onKey(a);
        }
    }
    
    public void injectMainLowerView(android.view.View a) {
        if (presenter != null) {
            presenter.injectMainLowerView(a);
        }
    }
    
    public boolean isMainLowerViewVisible() {
        return presenter != null && presenter.isMainLowerViewVisible();
    }
    
    public boolean isMainUIShrunk() {
        return presenter != null && presenter.isMainUIShrunk();
    }
    
    public boolean isNotificationCollapsing() {
        return presenter != null && presenter.isNotificationCollapsing();
    }
    
    public boolean isNotificationExpanding() {
        return presenter != null && presenter.isNotificationExpanding();
    }
    
    public boolean isNotificationViewShowing() {
        return presenter != null && presenter.isNotificationViewShowing();
    }
    
    public boolean isScreenAnimating() {
        return presenter != null && presenter.isScreenAnimating();
    }
    
    public void removeNotificationExtensionView() {
        if (presenter != null) {
            presenter.removeNotificationExtensionView();
        }
    }
    
    public void setInputFocus() {
        if (presenter != null) {
            presenter.setInputFocus();
        }
    }
    
    public void setSystemTrayVisibility(int i) {
        if (presenter != null) {
            presenter.setSystemTrayVisibility(i);
        }
    }
}
