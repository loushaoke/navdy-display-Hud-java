package com.navdy.hud.app.ui.component.carousel;
import com.navdy.hud.app.R;

class CircleIndicator extends android.widget.RelativeLayout implements com.navdy.hud.app.ui.component.carousel.IProgressIndicator {
    private int circleFocusSize;
    private int circleMargin;
    private int circleSize;
    final private android.content.Context context;
    private int currentItem;
    private int currentItemColor;
    private int defaultColor;
    private int id;
    private int itemCount;
    private android.view.View moveAnimatorView;
    private com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation orientation;
    
    public CircleIndicator(android.content.Context a) {
        super(a);
        this.currentItem = -1;
        this.currentItemColor = -1;
        this.id = 100;
        this.context = a;
    }
    
    public CircleIndicator(android.content.Context a, android.util.AttributeSet a0) {
        super(a, a0);
        this.currentItem = -1;
        this.currentItemColor = -1;
        this.id = 100;
        this.context = a;
    }
    
    static int access$002(com.navdy.hud.app.ui.component.carousel.CircleIndicator a, int i) {
        a.currentItem = i;
        return i;
    }
    
    static com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation access$100(com.navdy.hud.app.ui.component.carousel.CircleIndicator a) {
        return a.orientation;
    }
    
    static void access$200(com.navdy.hud.app.ui.component.carousel.CircleIndicator a, android.view.View a0) {
        a.setViewPos(a0);
    }
    
    private void buildItems(int i) {
        if (i != 0) {
            int i0 = this.getChildCount();
            android.view.View a = null;
            if (i0 > 0) {
                a = this.getChildAt(i0 - 1);
                android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)a.getLayoutParams();
                if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                    a0.bottomMargin = 0;
                } else {
                    a0.rightMargin = 0;
                }
            }
            int i1 = i0;
            while(i1 < i + i0) {
                android.view.View a1 = new android.view.View(this.context);
                int i2 = this.id;
                this.id = i2 + 1;
                a1.setId(i2);
                a1.setBackgroundResource(R.drawable.circle_page_indicator);
                android.widget.RelativeLayout$LayoutParams a2 = new android.widget.RelativeLayout$LayoutParams(this.circleSize, this.circleSize);
                if (i1 == 0 && a == null) {
                    if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                        a2.addRule(10);
                        a2.topMargin = this.circleMargin;
                    } else {
                        a2.addRule(9);
                        a2.leftMargin = this.circleMargin;
                    }
                }
                if (i1 == this.itemCount - 1) {
                    if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                        a2.bottomMargin = this.circleMargin;
                    } else {
                        a2.rightMargin = this.circleMargin;
                    }
                }
                if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                    a2.addRule(14);
                    if (i1 > 0) {
                        a2.topMargin = this.circleMargin;
                    }
                    if (a != null) {
                        a2.addRule(3, a.getId());
                    }
                } else {
                    a2.addRule(15);
                    if (i1 > 0) {
                        a2.leftMargin = this.circleMargin;
                    }
                    if (a != null) {
                        a2.addRule(1, a.getId());
                    }
                }
                a1.setAlpha(0.5f);
                this.addView(a1, (android.view.ViewGroup$LayoutParams)a2);
                i1 = i1 + 1;
                a = a1;
            }
            if (this.moveAnimatorView == null) {
                this.moveAnimatorView = new android.view.View(this.context);
                this.moveAnimatorView.setBackgroundResource(R.drawable.circle_page_indicator_focus);
                this.moveAnimatorView.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.widget.RelativeLayout$LayoutParams(this.circleFocusSize, this.circleFocusSize));
                this.moveAnimatorView.setVisibility(8);
                this.addView(this.moveAnimatorView);
            }
        }
    }
    
    private int getTargetViewPos(android.view.View a) {
        return (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) ? a.getTop() + a.getHeight() / 2 - this.circleFocusSize / 2 : a.getLeft() + a.getWidth() / 2 - this.circleFocusSize / 2;
    }
    
    private void setViewPos(android.view.View a) {
        float f = (float)this.getTargetViewPos(a);
        int i = this.currentItemColor;
        if (i == -1) {
            i = this.defaultColor;
        }
        if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
            this.moveAnimatorView.setY(f);
        } else {
            this.moveAnimatorView.setX(f);
        }
        ((android.graphics.drawable.GradientDrawable)this.moveAnimatorView.getBackground()).setColor(i);
        this.moveAnimatorView.setVisibility(0);
        this.moveAnimatorView.invalidate();
    }
    
    public int getCurrentItem() {
        return this.currentItem;
    }
    
    public android.animation.AnimatorSet getItemMoveAnimator(int i, int i0) {
        android.animation.AnimatorSet a = null;
        if (this.currentItem != -1) {
            a = null;
            if (i >= 0) {
                int i1 = this.itemCount;
                a = null;
                if (i < i1) {
                    this.getChildAt(this.currentItem);
                    android.view.View a0 = this.getChildAt(i);
                    android.graphics.drawable.GradientDrawable a1 = (android.graphics.drawable.GradientDrawable)this.moveAnimatorView.getBackground();
                    if (i0 != -1) {
                        a1.setColor(i0);
                    } else {
                        a1.setColor(this.defaultColor);
                    }
                    a = new android.animation.AnimatorSet();
                    a.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.carousel.CircleIndicator$1(this, i));
                    android.view.View a2 = this.moveAnimatorView;
                    android.util.Property a3 = (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) ? android.view.View.Y : android.view.View.X;
                    float[] a4 = new float[1];
                    a4[0] = (float)this.getTargetViewPos(a0);
                    a.play((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a2, a3, a4));
                }
            }
        } else {
            a = null;
        }
        return a;
    }
    
    public android.graphics.RectF getItemPos(int i) {
        android.graphics.RectF a = null;
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.itemCount) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            android.view.View a0 = this.getChildAt(i);
            android.view.View a1 = (android.view.View)a0.getParent();
            a = new android.graphics.RectF((float)(a0.getLeft() + a1.getLeft()), (float)(a0.getTop() + a1.getTop()), 0.0f, 0.0f);
        }
        return a;
    }
    
    public void setCurrentItem(int i) {
        this.setCurrentItem(i, -1);
    }
    
    public void setCurrentItem(int i, int i0) {
        if (this.currentItem != i) {
            this.currentItemColor = i0;
            if (i >= 0 && i < this.itemCount) {
                android.view.View a = this.getChildAt(i);
                if (((this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) ? a.getTop() : a.getLeft()) != 0) {
                    this.setViewPos(a);
                } else {
                    a.getViewTreeObserver().addOnGlobalLayoutListener((android.view.ViewTreeObserver$OnGlobalLayoutListener)new com.navdy.hud.app.ui.component.carousel.CircleIndicator$2(this, a));
                }
            }
            this.currentItem = i;
        }
    }
    
    public void setItemCount(int i) {
        if (i < 0) {
            throw new IllegalArgumentException();
        }
        this.currentItem = -1;
        if (this.getChildCount() != 0) {
            if (this.itemCount != i) {
                int i0 = this.itemCount - i;
                if (i0 <= 0) {
                    this.itemCount = i;
                    this.removeView(this.moveAnimatorView);
                    this.buildItems(-i0);
                    this.addView(this.moveAnimatorView);
                } else {
                    this.itemCount = i;
                    this.removeView(this.moveAnimatorView);
                    int i1 = 0;
                    while(i1 < i0) {
                        this.removeView(this.getChildAt(this.getChildCount() - 1));
                        i1 = i1 + 1;
                    }
                    int i2 = this.getChildCount();
                    if (i2 > 0) {
                        android.view.ViewGroup$MarginLayoutParams a = (android.view.ViewGroup$MarginLayoutParams)this.getChildAt(i2 - 1).getLayoutParams();
                        if (this.orientation != com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation.HORIZONTAL) {
                            a.bottomMargin = this.circleMargin;
                        } else {
                            a.rightMargin = this.circleMargin;
                        }
                    }
                    this.addView(this.moveAnimatorView);
                }
            }
        } else {
            this.itemCount = i;
            this.buildItems(i);
        }
    }
    
    public void setOrientation(com.navdy.hud.app.ui.component.carousel.CarouselIndicator$Orientation a) {
        this.orientation = a;
    }
    
    public void setProperties(int i, int i0, int i1, int i2) {
        this.circleSize = i;
        this.circleFocusSize = i0;
        this.circleMargin = i1;
        this.defaultColor = i2;
    }
}
