package com.navdy.hud.app.ui.component.homescreen;

public class TbtShrunkDistanceView extends android.widget.RelativeLayout {
    final private static String TBT_SHRUNK_MODE = "persist.sys.tbtdistanceshrunk";
    final private static String TBT_SHRUNK_MODE_DISTANCE_NUMBER = "distance_number";
    final private static String TBT_SHRUNK_MODE_PROGRESS_BAR = "progress_bar";
    private com.navdy.service.library.log.Logger logger;
    final private com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode mode;
    @InjectView(R.id.now_shrunk_icon)
    android.view.View nowShrunkIcon;
    @InjectView(R.id.progress_bar_shrunk)
    android.widget.ProgressBar progressBarShrunk;
    @InjectView(R.id.tbtShrunkDistance)
    android.widget.TextView tbtShrunkDistance;
    
    public TbtShrunkDistanceView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public TbtShrunkDistanceView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public TbtShrunkDistanceView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        if (com.navdy.hud.app.util.os.SystemProperties.get("persist.sys.tbtdistanceshrunk", "progress_bar").equals("distance_number")) {
            this.mode = com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.DISTANCE_NUMBER;
        } else {
            this.mode = com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.PROGRESS_BAR;
        }
    }
    
    private void setRegularBackground() {
        this.setBackgroundResource(17170444);
    }
    
    private void setTransparentBackground() {
        this.setBackgroundResource(17170445);
    }
    
    public void animateProgressBar(double d) {
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.PROGRESS_BAR) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getProgressBarAnimator(this.progressBarShrunk, (int)(100.0 * d)).setDuration(250L).start();
        }
    }
    
    public void hideNowIcon() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowShrunkIcon).setDuration(250L).start();
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.DISTANCE_NUMBER) {
            this.tbtShrunkDistance.setVisibility(0);
        }
    }
    
    public void hideProgressBar() {
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.PROGRESS_BAR) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth((android.view.View)this.progressBarShrunk, 0);
            this.setTransparentBackground();
        }
    }
    
    public void initProgressBar() {
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.PROGRESS_BAR) {
            this.setRegularBackground();
            com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.setWidth((android.view.View)this.progressBarShrunk, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadShrunkProgressBarWidth);
            this.animateProgressBar(0.0);
        }
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.setY((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadShrunkDistanceY);
        if (com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$1.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$TbtShrunkDistanceView$Mode[this.mode.ordinal()] != 0) {
            this.progressBarShrunk.setVisibility(8);
        } else {
            this.tbtShrunkDistance.setVisibility(8);
        }
    }
    
    public void setDistanceText(String s) {
        this.tbtShrunkDistance.setText((CharSequence)s);
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        switch(com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadShrunkDistanceShrinkLeftX);
                break;
            }
            case 1: {
                this.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeRoadShrunkDistanceX);
                break;
            }
        }
    }
    
    public void showNowIcon() {
        com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowShrunkIcon).setDuration(250L).start();
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.TbtShrunkDistanceView$Mode.DISTANCE_NUMBER) {
            this.tbtShrunkDistance.setVisibility(4);
        }
    }
}
