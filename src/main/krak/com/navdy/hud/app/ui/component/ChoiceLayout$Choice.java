package com.navdy.hud.app.ui.component;

public class ChoiceLayout$Choice implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    public com.navdy.hud.app.ui.component.ChoiceLayout$Icon icon;
    public int id;
    public String label;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.hud.app.ui.component.ChoiceLayout$Choice$1();
    }
    
    public ChoiceLayout$Choice(android.os.Parcel a) {
        this.label = a.readString();
        if (android.text.TextUtils.isEmpty((CharSequence)this.label)) {
            this.label = null;
        }
        int i = a.readInt();
        int i0 = a.readInt();
        if (i != -1 && i0 != -1) {
            this.icon = new com.navdy.hud.app.ui.component.ChoiceLayout$Icon(i, i0);
        }
        this.id = a.readInt();
    }
    
    public ChoiceLayout$Choice(com.navdy.hud.app.ui.component.ChoiceLayout$Icon a, int i) {
        this.icon = a;
        this.id = i;
    }
    
    public ChoiceLayout$Choice(String s, int i) {
        this.label = s;
        this.id = i;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeString((this.label == null) ? "" : this.label);
        a.writeInt((this.icon == null) ? -1 : this.icon.resIdSelected);
        a.writeInt((this.icon == null) ? -1 : this.icon.resIdUnSelected);
        a.writeInt(this.id);
    }
}
