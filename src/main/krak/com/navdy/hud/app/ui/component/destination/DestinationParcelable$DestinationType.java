package com.navdy.hud.app.ui.component.destination;


public enum DestinationParcelable$DestinationType {
    NONE(0),
    DESTINATION(1);

    private int value;
    DestinationParcelable$DestinationType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class DestinationParcelable$DestinationType extends Enum {
//    final private static com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType DESTINATION;
//    final public static com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType NONE;
//    
//    static {
//        NONE = new com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType("NONE", 0);
//        DESTINATION = new com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType("DESTINATION", 1);
//        com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType[] a = new com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType[2];
//        a[0] = NONE;
//        a[1] = DESTINATION;
//        $VALUES = a;
//    }
//    
//    private DestinationParcelable$DestinationType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType)Enum.valueOf(com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.destination.DestinationParcelable$DestinationType[] values() {
//        return $VALUES.clone();
//    }
//}
//