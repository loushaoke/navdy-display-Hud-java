package com.navdy.hud.app.ui.component.vmenu;

abstract public interface VerticalMenuComponent$Callback {
    abstract public void close();
    
    
    abstract public boolean isClosed();
    
    
    abstract public boolean isItemClickable(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState arg);
    
    
    abstract public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model arg, android.view.View arg0, int arg1, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState arg2);
    
    
    abstract public void onFastScrollEnd();
    
    
    abstract public void onFastScrollStart();
    
    
    abstract public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState arg);
    
    
    abstract public void onLoad();
    
    
    abstract public void onScrollIdle();
    
    
    abstract public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState arg);
    
    
    abstract public void showToolTip();
}
