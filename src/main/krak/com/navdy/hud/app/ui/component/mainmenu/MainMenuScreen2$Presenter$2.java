package com.navdy.hud.app.ui.component.mainmenu;

class MainMenuScreen2$Presenter$2 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter this$0;
    final boolean val$hasScrollModel;
    
    MainMenuScreen2$Presenter$2(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a, boolean b) {
        super();
        this.this$0 = a;
        this.val$hasScrollModel = b;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.mainmenu.MainMenuView2 a = (com.navdy.hud.app.ui.component.mainmenu.MainMenuView2)com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$100(this.this$0);
        if (a != null) {
            com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.access$000().v(new StringBuilder().append("performEnterAnimation:").append(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).getType()).toString());
            a.vmenuComponent.unlock(false);
            a.vmenuComponent.verticalList.setBindCallbacks(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).isBindCallsEnabled());
            a.vmenuComponent.updateView(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).getItems(), com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).getInitialSelection(), true, this.val$hasScrollModel, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter.access$200(this.this$0).getScrollIndex());
        }
    }
}
