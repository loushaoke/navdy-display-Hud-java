package com.navdy.hud.app.ui.component.vlist;

class VerticalList$1 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.vlist.VerticalList this$0;
    
    VerticalList$1(com.navdy.hud.app.ui.component.vlist.VerticalList a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$ContainerCallback a0 = this.this$0.containerCallback;
        label0: {
            label1: {
                if (a0 == null) {
                    break label1;
                }
                if (this.this$0.containerCallback.isFastScrolling()) {
                    break label0;
                }
            }
            com.navdy.hud.app.ui.component.vlist.VerticalList.access$000(this.this$0).setCurrentItem(this.this$0.getCurrentPosition());
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList.access$102(this.this$0, (android.animation.AnimatorSet)null);
    }
}
