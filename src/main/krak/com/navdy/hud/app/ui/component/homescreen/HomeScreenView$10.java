package com.navdy.hud.app.ui.component.homescreen;

class HomeScreenView$10 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode;
    final static int[] $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode;
    
    static {
        $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode = new int[com.navdy.hud.app.view.MainView$CustomAnimationMode.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode;
        com.navdy.hud.app.view.MainView$CustomAnimationMode a0 = com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        com.navdy.hud.app.manager.InputManager$CustomKeyEvent a2 = com.navdy.hud.app.manager.InputManager$CustomKeyEvent.LEFT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[com.navdy.hud.app.manager.InputManager$CustomKeyEvent.RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        $SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode = new int[com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode;
        com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a4 = com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode[com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException4) {
        }
    }
}
