package com.navdy.hud.app.ui.component.vlist;


public enum VerticalList$FontSize {
    FONT_SIZE_26(0),
    FONT_SIZE_22(1),
    FONT_SIZE_22_2(2),
    FONT_SIZE_18(3),
    FONT_SIZE_18_2(4),
    FONT_SIZE_16(5),
    FONT_SIZE_16_2(6),
    FONT_SIZE_26_18(7),
    FONT_SIZE_22_18(8),
    FONT_SIZE_22_2_18(9),
    FONT_SIZE_18_16(10),
    FONT_SIZE_18_2_16(11),
    FONT_SIZE_16_16(12),
    FONT_SIZE_16_2_16(13),
    FONT_SIZE_26_16(14),
    FONT_SIZE_22_16(15),
    FONT_SIZE_22_2_16(16);

    private int value;
    VerticalList$FontSize(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class VerticalList$FontSize extends Enum {
//    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_16;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_16_16;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_16_2;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_16_2_16;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_18;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_18_16;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_18_2;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_18_2_16;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_22;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_22_16;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_22_18;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_22_2;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_22_2_16;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_22_2_18;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_26;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_26_16;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize FONT_SIZE_26_18;
//    
//    static {
//        FONT_SIZE_26 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_26", 0);
//        FONT_SIZE_22 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_22", 1);
//        FONT_SIZE_22_2 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_22_2", 2);
//        FONT_SIZE_18 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_18", 3);
//        FONT_SIZE_18_2 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_18_2", 4);
//        FONT_SIZE_16 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_16", 5);
//        FONT_SIZE_16_2 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_16_2", 6);
//        FONT_SIZE_26_18 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_26_18", 7);
//        FONT_SIZE_22_18 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_22_18", 8);
//        FONT_SIZE_22_2_18 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_22_2_18", 9);
//        FONT_SIZE_18_16 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_18_16", 10);
//        FONT_SIZE_18_2_16 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_18_2_16", 11);
//        FONT_SIZE_16_16 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_16_16", 12);
//        FONT_SIZE_16_2_16 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_16_2_16", 13);
//        FONT_SIZE_26_16 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_26_16", 14);
//        FONT_SIZE_22_16 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_22_16", 15);
//        FONT_SIZE_22_2_16 = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize("FONT_SIZE_22_2_16", 16);
//        com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize[] a = new com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize[17];
//        a[0] = FONT_SIZE_26;
//        a[1] = FONT_SIZE_22;
//        a[2] = FONT_SIZE_22_2;
//        a[3] = FONT_SIZE_18;
//        a[4] = FONT_SIZE_18_2;
//        a[5] = FONT_SIZE_16;
//        a[6] = FONT_SIZE_16_2;
//        a[7] = FONT_SIZE_26_18;
//        a[8] = FONT_SIZE_22_18;
//        a[9] = FONT_SIZE_22_2_18;
//        a[10] = FONT_SIZE_18_16;
//        a[11] = FONT_SIZE_18_2_16;
//        a[12] = FONT_SIZE_16_16;
//        a[13] = FONT_SIZE_16_2_16;
//        a[14] = FONT_SIZE_26_16;
//        a[15] = FONT_SIZE_22_16;
//        a[16] = FONT_SIZE_22_2_16;
//        $VALUES = a;
//    }
//    
//    private VerticalList$FontSize(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize)Enum.valueOf(com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize[] values() {
//        return $VALUES.clone();
//    }
//}
//