package com.navdy.hud.app.ui.component.dashboard;

class GaugeViewPager$2$1 implements Runnable {
    final com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$2 this$1;
    
    GaugeViewPager$2$1(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$2 a) {
        super();
        this.this$1 = a;
    }
    
    public void run() {
        this.this$1.this$0.setSelectedPosition((com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$200(this.this$1.this$0) != com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.NEXT) ? com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$400(this.this$1.this$0) : com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$300(this.this$1.this$0));
        com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation a = (com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$500(this.this$1.this$0).size() <= 0) ? null : (com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation)com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$500(this.this$1.this$0).remove();
        if (a == null) {
            com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$102(this.this$1.this$0, false);
        } else {
            com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$600(this.this$1.this$0, a, com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.access$500(this.this$1.this$0).size() > 0);
        }
    }
}
