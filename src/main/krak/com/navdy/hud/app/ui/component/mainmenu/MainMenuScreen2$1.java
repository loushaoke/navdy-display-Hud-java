package com.navdy.hud.app.ui.component.mainmenu;

class MainMenuScreen2$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu;
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel;
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode;
    final static int[] $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu = new int[com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu;
        com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu a0 = com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.MAIN;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$Menu[com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.ACTIVE_TRIP.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState = new int[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
        com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState a2 = com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_CONNECTED;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange$ConnectionState.CONNECTION_DISCONNECTED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel = new int[com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.values().length];
        int[] a3 = $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel;
        com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a4 = com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel[com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel[com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.REFRESH_CURRENT.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel[com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.ROOT.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException6) {
        }
        $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode = new int[com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.values().length];
        int[] a5 = $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode;
        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode a6 = com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.MAIN_MENU;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode[com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.REPLY_PICKER.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$mainmenu$MainMenuScreen2$MenuMode[com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.SNAPSHOT_TITLE_PICKER.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException9) {
        }
    }
}
