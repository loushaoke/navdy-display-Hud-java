package com.navdy.hud.app.ui.component;

class ChoiceLayout$9 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$ChoiceLayout$Operation;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$ChoiceLayout$Operation = new int[com.navdy.hud.app.ui.component.ChoiceLayout$Operation.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$ChoiceLayout$Operation;
        com.navdy.hud.app.ui.component.ChoiceLayout$Operation a0 = com.navdy.hud.app.ui.component.ChoiceLayout$Operation.MOVE_LEFT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$ChoiceLayout$Operation[com.navdy.hud.app.ui.component.ChoiceLayout$Operation.MOVE_RIGHT.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$ChoiceLayout$Operation[com.navdy.hud.app.ui.component.ChoiceLayout$Operation.EXECUTE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$ChoiceLayout$Operation[com.navdy.hud.app.ui.component.ChoiceLayout$Operation.KEY_DOWN.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$ChoiceLayout$Operation[com.navdy.hud.app.ui.component.ChoiceLayout$Operation.KEY_UP.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
    }
}
