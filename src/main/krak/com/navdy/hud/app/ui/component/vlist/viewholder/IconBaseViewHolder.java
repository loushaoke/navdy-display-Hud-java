package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

abstract public class IconBaseViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    final public static int FLUCTUATOR_OPACITY_ALPHA = 153;
    final public static int HALO_DELAY_START_DURATION = 100;
    final private static int[] location;
    final private static com.navdy.service.library.log.Logger sLogger;
    protected android.animation.AnimatorSet$Builder animatorSetBuilder;
    protected com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView;
    private Runnable fluctuatorRunnable;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener fluctuatorStartListener;
    protected com.navdy.hud.app.ui.component.HaloView haloView;
    private boolean hasIconFluctuatorColor;
    private boolean hasSubTitle;
    private boolean hasSubTitle2;
    protected android.view.ViewGroup iconContainer;
    protected boolean iconScaleAnimationDisabled;
    protected android.view.ViewGroup imageContainer;
    protected android.widget.TextView subTitle;
    protected android.widget.TextView subTitle2;
    protected boolean textAnimationDisabled;
    protected android.widget.TextView title;
    private float titleSelectedTopMargin;
    private float titleUnselectedScale;
    
    static {
        sLogger = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.sLogger;
        location = new int[2];
    }
    
    public IconBaseViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super(a, a0, a1);
        this.fluctuatorStartListener = new com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder$1(this);
        this.fluctuatorRunnable = (Runnable)new com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder$2(this);
        this.imageContainer = (android.view.ViewGroup)a.findViewById(R.id.imageContainer);
        this.iconContainer = (android.view.ViewGroup)a.findViewById(R.id.iconContainer);
        this.haloView = (com.navdy.hud.app.ui.component.HaloView)a.findViewById(R.id.halo);
        this.haloView.setVisibility(8);
        this.crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a.findViewById(R.id.vlist_image);
        this.title = (android.widget.TextView)a.findViewById(R.id.title);
        this.subTitle = (android.widget.TextView)a.findViewById(R.id.subTitle);
        this.subTitle2 = (android.widget.TextView)a.findViewById(R.id.subTitle2);
    }
    
    static Runnable access$000(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder a) {
        return a.fluctuatorRunnable;
    }
    
    static android.view.ViewGroup getLayout(android.view.ViewGroup a, int i, int i0) {
        android.view.LayoutInflater a0 = android.view.LayoutInflater.from(a.getContext());
        android.view.ViewGroup a1 = (android.view.ViewGroup)a0.inflate(i, a, false);
        android.view.ViewGroup a2 = (android.view.ViewGroup)a1.findViewById(R.id.iconContainer);
        com.navdy.hud.app.ui.component.image.CrossFadeImageView a3 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a0.inflate(i0, a2, false);
        a3.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL);
        a3.setId(R.id.vlist_image);
        a2.addView((android.view.View)a3);
        return a1;
    }
    
    private int getSubTitle2Color() {
        return (this.extras != null) ? this.getTextColor("SUBTITLE_2_COLOR") : 0;
    }
    
    private int getSubTitleColor() {
        return (this.extras != null) ? this.getTextColor("SUBTITLE_COLOR") : 0;
    }
    
    private int getTextColor(String s) {
        int i = 0;
        java.util.HashMap a = this.extras;
        label0: {
            label1: if (a != null) {
                String s0 = (String)this.extras.get(s);
                if (s0 == null) {
                    i = 0;
                    break label0;
                } else {
                    try {
                        i = Integer.parseInt(s0);
                    } catch(NumberFormatException ignoredException) {
                        break label1;
                    }
                    break label0;
                }
            } else {
                i = 0;
                break label0;
            }
            i = 0;
        }
        return i;
    }
    
    private void setIconFluctuatorColor(int i) {
        if (i == 0) {
            this.hasIconFluctuatorColor = false;
        } else {
            int i0 = android.graphics.Color.argb(153, android.graphics.Color.red(i), android.graphics.Color.green(i), android.graphics.Color.blue(i));
            this.hasIconFluctuatorColor = true;
            this.haloView.setStrokeColor(i0);
        }
    }
    
    private void setMultiLineStyles(android.widget.TextView a, boolean b, int i) {
        if (i != -1) {
            a.setTextAppearance(a.getContext(), i);
        }
        a.setSingleLine(b);
        if (b) {
            a.setEllipsize((android.text.TextUtils$TruncateAt)null);
        } else {
            a.setMaxLines(2);
            a.setEllipsize(android.text.TextUtils$TruncateAt.END);
        }
    }
    
    private void setSubTitle(String s, boolean b) {
        if (s != null) {
            int i = this.getSubTitleColor();
            if (i != 0) {
                this.subTitle.setTextColor(i);
            } else {
                this.subTitle.setTextColor(subTitleColor);
            }
            if (b) {
                this.subTitle.setText((CharSequence)android.text.Html.fromHtml(s));
            } else {
                this.subTitle.setText((CharSequence)s);
            }
            this.hasSubTitle = true;
        } else {
            this.subTitle.setText((CharSequence)"");
            this.hasSubTitle = false;
        }
    }
    
    private void setSubTitle2(String s, boolean b) {
        if (s != null) {
            int i = this.getSubTitle2Color();
            if (i != 0) {
                this.subTitle2.setTextColor(i);
            } else {
                this.subTitle2.setTextColor(subTitle2Color);
            }
            if (b) {
                this.subTitle2.setText((CharSequence)android.text.Html.fromHtml(s));
            } else {
                this.subTitle2.setText((CharSequence)s);
            }
            this.subTitle2.setVisibility(0);
            this.hasSubTitle2 = true;
        } else {
            this.subTitle2.setText((CharSequence)"");
            this.subTitle2.setVisibility(8);
            this.hasSubTitle2 = false;
        }
    }
    
    private void setTitle(String s) {
        this.title.setText((CharSequence)s);
    }
    
    private void stopFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.handler.removeCallbacks(this.fluctuatorRunnable);
            this.haloView.setVisibility(8);
            this.haloView.stop();
        }
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        if (a0.updateTitle) {
            this.setTitle(a.title);
        }
        if (a0.updateSubTitle) {
            if (a.subTitle == null) {
                this.setSubTitle((String)null, false);
            } else {
                this.setSubTitle(a.subTitle, a.subTitleFormatted);
            }
        }
        if (a0.updateSubTitle2) {
            if (a.subTitle2 == null) {
                this.setSubTitle2((String)null, false);
            } else {
                this.setSubTitle2(a.subTitle2, a.subTitle2Formatted);
            }
        }
        this.textAnimationDisabled = a.noTextAnimation;
        this.iconScaleAnimationDisabled = a.noImageScaleAnimation;
        this.setIconFluctuatorColor(a.iconFluctuatorColor);
    }
    
    public void clearAnimation() {
        this.stopFluctuator();
        this.stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1f);
    }
    
    public void copyAndPosition(android.widget.ImageView a, android.widget.TextView a0, android.widget.TextView a1, android.widget.TextView a2, boolean b) {
        android.view.View a3 = this.crossFadeImageView.getBig();
        if (b) {
            com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.copyImage((android.widget.ImageView)a3, a);
        }
        a.setX((float)selectedImageX);
        a.setY((float)selectedImageY);
        a0.setText(this.title.getText());
        a0.setX((float)selectedTextX);
        this.title.getLocationOnScreen(location);
        a0.setY((float)(location[1] - rootTopOffset));
        if (this.hasSubTitle) {
            a1.setText(this.subTitle.getText());
            a1.setX((float)selectedTextX);
            this.subTitle.getLocationOnScreen(location);
            a1.setY((float)(location[1] - rootTopOffset));
        } else {
            a1.setText((CharSequence)"");
        }
        if (this.hasSubTitle2) {
            a2.setText(this.subTitle2.getText());
            a2.setX((float)selectedTextX);
            this.subTitle2.getLocationOnScreen(location);
            a2.setY((float)(location[1] - rootTopOffset));
        } else {
            a2.setText((CharSequence)"");
        }
    }
    
    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        ((android.view.ViewGroup$MarginLayoutParams)this.title.getLayoutParams()).topMargin = (int)a.fontInfo.titleFontTopMargin;
        this.title.setTextSize(a.fontInfo.titleFontSize);
        this.setMultiLineStyles(this.title, a.fontInfo.titleSingleLine, -1);
        this.title.setLineSpacing(0.0f, 0.9f);
        this.titleSelectedTopMargin = (float)(int)a.fontInfo.titleFontTopMargin;
        ((android.view.ViewGroup$MarginLayoutParams)this.subTitle.getLayoutParams()).topMargin = (int)a.fontInfo.subTitleFontTopMargin;
        this.setMultiLineStyles(this.subTitle, !a.subTitle_2Lines, (a.subTitle_2Lines) ? R.style.vlist_subtitle_2_line : R.style.vlist_subtitle);
        this.subTitle.setTextSize(a.fontInfo.subTitleFontSize);
        ((android.view.ViewGroup$MarginLayoutParams)this.subTitle2.getLayoutParams()).topMargin = (int)a.fontInfo.subTitle2FontTopMargin;
        this.subTitle2.setTextSize(a.fontInfo.subTitle2FontSize);
        this.titleUnselectedScale = a.fontInfo.titleScale;
        this.crossFadeImageView.setSmallAlpha(-1f);
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, int i, int i0) {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(4);
            this.haloView.stop();
        }
        com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.performClick((android.view.View)this.iconContainer, i0, (Runnable)new com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder$3(this, a, i));
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
        this.animatorSetBuilder = null;
        boolean b0 = this.textAnimationDisabled;
        label2: {
            float f = 0.0f;
            float f0 = 0.0f;
            float f1 = 0.0f;
            float f2 = 0.0f;
            float f3 = 0.0f;
            float f4 = 0.0f;
            label1: {
                if (!b0) {
                    break label1;
                }
                com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a1 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.MOVE;
                label0: {
                    if (a0 == a1) {
                        break label0;
                    }
                    a = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED;
                    break label1;
                }
                this.itemAnimatorSet = new android.animation.AnimatorSet();
                android.animation.AnimatorSet a2 = this.itemAnimatorSet;
                float[] a3 = new float[2];
                a3[0] = 0.0f;
                a3[1] = 1f;
                this.animatorSetBuilder = a2.play((android.animation.Animator)android.animation.ValueAnimator.ofFloat(a3));
                break label2;
            }
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder$4.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State[a.ordinal()]) {
                case 2: {
                    f = this.titleUnselectedScale;
                    f0 = this.titleUnselectedScale;
                    f1 = this.titleUnselectedScale;
                    f2 = 0.0f;
                    f3 = 0.0f;
                    f4 = 0.0f;
                    break;
                }
                case 1: {
                    f2 = this.titleSelectedTopMargin;
                    f = 1f;
                    f3 = 1f;
                    f0 = 1f;
                    f4 = 1f;
                    f1 = 1f;
                    break;
                }
                default: {
                    f = 0.0f;
                    f2 = 0.0f;
                    f3 = 0.0f;
                    f0 = 0.0f;
                    f4 = 0.0f;
                    f1 = 0.0f;
                }
            }
            if (!this.hasSubTitle) {
                f2 = 0.0f;
                f3 = 0.0f;
                f4 = 0.0f;
            }
            this.title.setPivotX(0.0f);
            this.title.setPivotY(titleHeight / 2f);
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder$4.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[a0.ordinal()]) {
                case 3: {
                    this.itemAnimatorSet = new android.animation.AnimatorSet();
                    android.util.Property a4 = android.view.View.SCALE_X;
                    float[] a5 = new float[1];
                    a5[0] = f;
                    android.animation.PropertyValuesHolder a6 = android.animation.PropertyValuesHolder.ofFloat(a4, a5);
                    android.util.Property a7 = android.view.View.SCALE_Y;
                    float[] a8 = new float[1];
                    a8[0] = f;
                    android.animation.PropertyValuesHolder a9 = android.animation.PropertyValuesHolder.ofFloat(a7, a8);
                    android.animation.AnimatorSet a10 = this.itemAnimatorSet;
                    android.widget.TextView a11 = this.title;
                    android.animation.PropertyValuesHolder[] a12 = new android.animation.PropertyValuesHolder[2];
                    a12[0] = a6;
                    a12[1] = a9;
                    this.animatorSetBuilder = a10.play((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a11, a12));
                    this.animatorSetBuilder.with(com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils.animateMargin((android.view.View)this.title, (int)f2));
                    break;
                }
                case 1: case 2: {
                    this.title.setScaleX(f);
                    this.title.setScaleY(f);
                    ((android.view.ViewGroup$MarginLayoutParams)this.title.getLayoutParams()).topMargin = (int)f2;
                    this.title.requestLayout();
                    break;
                }
            }
            this.subTitle.setPivotX(0.0f);
            this.subTitle.setPivotY(subTitleHeight / 2f);
            com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a13 = (this.hasSubTitle) ? a0 : com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE;
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder$4.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[a13.ordinal()]) {
                case 3: {
                    android.util.Property a14 = android.view.View.SCALE_X;
                    float[] a15 = new float[1];
                    a15[0] = f0;
                    android.animation.PropertyValuesHolder a16 = android.animation.PropertyValuesHolder.ofFloat(a14, a15);
                    android.util.Property a17 = android.view.View.SCALE_Y;
                    float[] a18 = new float[1];
                    a18[0] = f0;
                    android.animation.PropertyValuesHolder a19 = android.animation.PropertyValuesHolder.ofFloat(a17, a18);
                    android.util.Property a20 = android.view.View.ALPHA;
                    float[] a21 = new float[1];
                    a21[0] = f3;
                    android.animation.PropertyValuesHolder a22 = android.animation.PropertyValuesHolder.ofFloat(a20, a21);
                    android.animation.AnimatorSet$Builder a23 = this.animatorSetBuilder;
                    android.widget.TextView a24 = this.subTitle;
                    android.animation.PropertyValuesHolder[] a25 = new android.animation.PropertyValuesHolder[3];
                    a25[0] = a16;
                    a25[1] = a19;
                    a25[2] = a22;
                    a23.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a24, a25));
                    break;
                }
                case 1: case 2: {
                    this.subTitle.setAlpha(f3);
                    this.subTitle.setScaleX(f0);
                    this.subTitle.setScaleY(f0);
                    break;
                }
            }
            this.subTitle2.setPivotX(0.0f);
            this.subTitle2.setPivotY(subTitleHeight / 2f);
            if (!this.hasSubTitle2) {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE;
            }
            switch(com.navdy.hud.app.ui.component.vlist.viewholder.IconBaseViewHolder$4.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[a0.ordinal()]) {
                case 3: {
                    android.util.Property a26 = android.view.View.SCALE_X;
                    float[] a27 = new float[1];
                    a27[0] = f1;
                    android.animation.PropertyValuesHolder a28 = android.animation.PropertyValuesHolder.ofFloat(a26, a27);
                    android.util.Property a29 = android.view.View.SCALE_Y;
                    float[] a30 = new float[1];
                    a30[0] = f1;
                    android.animation.PropertyValuesHolder a31 = android.animation.PropertyValuesHolder.ofFloat(a29, a30);
                    android.util.Property a32 = android.view.View.ALPHA;
                    float[] a33 = new float[1];
                    a33[0] = f3;
                    android.animation.PropertyValuesHolder a34 = android.animation.PropertyValuesHolder.ofFloat(a32, a33);
                    android.animation.AnimatorSet$Builder a35 = this.animatorSetBuilder;
                    android.widget.TextView a36 = this.subTitle2;
                    android.animation.PropertyValuesHolder[] a37 = new android.animation.PropertyValuesHolder[3];
                    a37[0] = a28;
                    a37[1] = a31;
                    a37[2] = a34;
                    a35.with((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a36, a37));
                    break;
                }
                case 1: case 2: {
                    this.subTitle2.setAlpha(f4);
                    this.subTitle2.setScaleX(f1);
                    this.subTitle2.setScaleY(f1);
                    break;
                }
            }
            if (a == com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED && b) {
                if (this.itemAnimatorSet == null) {
                    this.startFluctuator();
                } else {
                    this.itemAnimatorSet.addListener((android.animation.Animator$AnimatorListener)this.fluctuatorStartListener);
                }
            }
        }
    }
    
    public void startFluctuator() {
        if (this.hasIconFluctuatorColor) {
            this.haloView.setVisibility(0);
            this.haloView.start();
        }
    }
}
