package com.navdy.hud.app.ui.component.dashboard;
import com.navdy.hud.app.R;

public class GaugeViewPager extends android.widget.FrameLayout {
    final public static int FAST_SCROLL_ANIMATION_DURATION = 100;
    final public static int REGULAR_SCROLL_ANIMATION_DURATION = 200;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation currentOperation;
    private volatile boolean isAnimationRunning;
    private com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Adapter mAdapter;
    private com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$ChangeListener mChangeListener;
    private int mCurrentPosition;
    private int mNextPosition;
    private int mPreviousPosition;
    private android.widget.LinearLayout mSlider;
    private java.util.Queue operationQueue;
    final android.animation.ValueAnimator positionAnimator;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.class);
    }
    
    public GaugeViewPager(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public GaugeViewPager(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, -1);
    }
    
    public GaugeViewPager(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.mCurrentPosition = -1;
        this.mNextPosition = -1;
        this.mPreviousPosition = -1;
        this.operationQueue = (java.util.Queue)new java.util.LinkedList();
        this.isAnimationRunning = false;
        this.positionAnimator = new android.animation.ValueAnimator();
        this.positionAnimator.addUpdateListener((android.animation.ValueAnimator$AnimatorUpdateListener)new com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$1(this));
        this.positionAnimator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$2(this));
    }
    
    public GaugeViewPager(android.content.Context a, android.util.AttributeSet a0, int i, int i0) {
        super(a, a0, i, i0);
        this.mCurrentPosition = -1;
        this.mNextPosition = -1;
        this.mPreviousPosition = -1;
        this.operationQueue = (java.util.Queue)new java.util.LinkedList();
        this.isAnimationRunning = false;
        this.positionAnimator = new android.animation.ValueAnimator();
    }
    
    static android.widget.LinearLayout access$000(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager a) {
        return a.mSlider;
    }
    
    static boolean access$102(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager a, boolean b) {
        a.isAnimationRunning = b;
        return b;
    }
    
    static com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation access$200(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager a) {
        return a.currentOperation;
    }
    
    static int access$300(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager a) {
        return a.mNextPosition;
    }
    
    static int access$400(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager a) {
        return a.mPreviousPosition;
    }
    
    static java.util.Queue access$500(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager a) {
        return a.operationQueue;
    }
    
    static void access$600(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager a, com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation a0, boolean b) {
        a.performOperation(a0, b);
    }
    
    private int getMaxTopMarginValue() {
        return this.getResources().getDimensionPixelSize(R.dimen.gauge_transition_margin) + this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height);
    }
    
    private void performOperation(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation a, boolean b) {
        this.currentOperation = a;
        if (a != com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.NEXT) {
            if (a == com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.PREVIOUS) {
                this.updateWidgetView(this.mPreviousPosition);
                android.animation.ValueAnimator a0 = this.positionAnimator;
                int[] a1 = new int[2];
                a1[0] = -this.getMaxTopMarginValue();
                a1[1] = -this.getMaxTopMarginValue() * 2;
                a0.setIntValues(a1);
            }
        } else {
            this.updateWidgetView(this.mNextPosition);
            android.animation.ValueAnimator a2 = this.positionAnimator;
            int[] a3 = new int[2];
            a3[0] = -this.getMaxTopMarginValue();
            a3[1] = 0;
            a2.setIntValues(a3);
        }
        this.positionAnimator.setDuration(b ? 100L : 200L);
        this.positionAnimator.start();
    }
    
    private void populateView(int i, int i0) {
        android.view.ViewGroup a = (android.view.ViewGroup)this.mSlider.getChildAt(i);
        android.view.View a0 = a.getChildAt(0);
        android.view.View a1 = this.mAdapter.getView(i0, a0, a, i == 1);
        if (a1 != a0) {
            a.removeAllViews();
            a.addView(a1);
        }
    }
    
    private void updateWidgetView(int i) {
        com.navdy.hud.app.view.DashboardWidgetPresenter a = this.mAdapter.getPresenter(i);
        if (a != null) {
            sLogger.v("widget:: update before animation");
            a.setWidgetVisibleToUser(true);
        }
    }
    
    public int getSelectedPosition() {
        return this.mCurrentPosition;
    }
    
    public void moveNext() {
        if (this.isAnimationRunning) {
            if (this.operationQueue.peek() != com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.NEXT) {
                this.operationQueue.clear();
            }
            this.operationQueue.add(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.NEXT);
        } else {
            this.performOperation(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.NEXT, false);
        }
    }
    
    public void movePrevious() {
        if (this.isAnimationRunning) {
            if (this.operationQueue.peek() != com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.PREVIOUS) {
                this.operationQueue.clear();
            }
            this.operationQueue.add(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.PREVIOUS);
        } else {
            this.performOperation(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Operation.PREVIOUS, false);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.removeAllViews();
        this.mSlider = new android.widget.LinearLayout(this.getContext());
        this.mSlider.setOrientation(1);
        this.mSlider.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(-1, -2));
        this.addView((android.view.View)this.mSlider);
        android.view.LayoutInflater.from(this.getContext()).inflate(R.layout.gauge_view_scrims, (android.view.ViewGroup)this, true);
        int i = 0;
        while(i < 3) {
            android.widget.FrameLayout a = new android.widget.FrameLayout(this.getContext());
            label2: {
                label0: {
                    label1: {
                        if (i == 0) {
                            break label1;
                        }
                        if (i != 1) {
                            break label0;
                        }
                    }
                    a.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height) + this.getResources().getDimensionPixelSize(R.dimen.gauge_transition_margin)));
                    break label2;
                }
                a.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.view.ViewGroup$MarginLayoutParams(this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), this.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height)));
            }
            this.mSlider.addView((android.view.View)a);
            i = i + 1;
        }
        ((android.view.ViewGroup$MarginLayoutParams)this.mSlider.getLayoutParams()).topMargin = -this.getMaxTopMarginValue();
    }
    
    public void setAdapter(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$Adapter a) {
        this.mAdapter = a;
        this.setSelectedPosition(0);
    }
    
    public void setChangeListener(com.navdy.hud.app.ui.component.dashboard.GaugeViewPager$ChangeListener a) {
        this.mChangeListener = a;
    }
    
    public void setSelectedPosition(int i) {
        if (this.mAdapter == null) {
            throw new IllegalStateException("Set the Adapter before setting the position");
        }
        int i0 = this.mCurrentPosition;
        if (i >= 0 && i < this.mAdapter.getCount()) {
            int i1 = (this.mAdapter.getCount() + i - 1) % this.mAdapter.getCount();
            int i2 = (i + 1) % this.mAdapter.getCount();
            if (i1 == this.mAdapter.getExcludedPosition()) {
                i1 = (this.mAdapter.getCount() + i1 - 1) % this.mAdapter.getCount();
            }
            if (i2 == this.mAdapter.getExcludedPosition()) {
                i2 = (i2 + 1) % this.mAdapter.getCount();
            }
            this.mNextPosition = i1;
            this.mPreviousPosition = i2;
            this.mCurrentPosition = i;
            this.populateView(0, this.mNextPosition);
            this.populateView(1, this.mCurrentPosition);
            this.populateView(2, this.mPreviousPosition);
            ((android.view.ViewGroup$MarginLayoutParams)this.mSlider.getLayoutParams()).topMargin = -this.getMaxTopMarginValue();
            if (this.mChangeListener != null && this.mCurrentPosition != i0) {
                this.mChangeListener.onGaugeChanged(this.mCurrentPosition);
            }
        }
    }
    
    public void updateState() {
        this.setSelectedPosition(this.getSelectedPosition());
    }
}
