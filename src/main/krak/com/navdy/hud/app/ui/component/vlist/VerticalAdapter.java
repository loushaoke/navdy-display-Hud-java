package com.navdy.hud.app.ui.component.vlist;

public class VerticalAdapter extends android.support.v7.widget.RecyclerView$Adapter {
    final private static com.navdy.service.library.log.Logger sLogger;
    private java.util.List data;
    private android.os.Handler handler;
    private int highlightIndex;
    private java.util.HashMap holderToPosMap;
    private boolean initialState;
    private com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState modelState;
    private java.util.HashMap posToHolderMap;
    private java.util.HashSet viewHolderCache;
    private com.navdy.hud.app.ui.component.vlist.VerticalList vlist;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.VerticalAdapter.class);
    }
    
    public VerticalAdapter(java.util.List a, com.navdy.hud.app.ui.component.vlist.VerticalList a0) {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.modelState = new com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState();
        this.highlightIndex = -1;
        this.viewHolderCache = new java.util.HashSet();
        this.holderToPosMap = new java.util.HashMap();
        this.posToHolderMap = new java.util.HashMap();
        this.data = a;
        this.vlist = a0;
    }
    
    private void clearPrevPos(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a) {
        Integer a0 = (Integer)this.holderToPosMap.remove(a);
        if (a0 != null) {
            sLogger.v(new StringBuilder().append("clearPrevPos removed:").append(a0).toString());
            this.posToHolderMap.remove(a0);
        }
    }
    
    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder getHolderForPos(int i) {
        return (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)this.posToHolderMap.get(Integer.valueOf(i));
    }
    
    public int getItemCount() {
        return this.data.size();
    }
    
    public long getItemId(int i) {
        return (long)i;
    }
    
    public int getItemViewType(int i) {
        return ((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.data.get(i)).type.ordinal();
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModel(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.data.size()) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.data.get(i);
        }
        return a;
    }
    
    public void onBindViewHolder(android.support.v7.widget.RecyclerView$ViewHolder a, int i) {
        this.onBindViewHolder((com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a, i);
    }
    
    public void onBindViewHolder(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a, int i) {
        boolean b = false;
        if (sLogger.isLoggable(2)) {
            sLogger.v(new StringBuilder().append("onBindViewHolder: {").append(i).append("}").toString());
        }
        this.clearPrevPos(a);
        if (this.highlightIndex == -1) {
            b = false;
        } else {
            b = this.highlightIndex == i;
            this.highlightIndex = -1;
        }
        if (this.viewHolderCache.contains(Integer.valueOf(i))) {
            this.holderToPosMap.put(a, Integer.valueOf(i));
            this.posToHolderMap.put(Integer.valueOf(i), a);
        }
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.data.get(i);
        int i0 = a.getPos();
        label2: {
            boolean b0 = false;
            label3: {
                if (i != i0) {
                    break label3;
                }
                if (a0.needsRebind) {
                    break label3;
                }
                sLogger.i("onBindViewHolder: already bind unselect it");
                a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE, 0);
                break label2;
            }
            com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType a1 = a.getModelType();
            switch(com.navdy.hud.app.ui.component.vlist.VerticalAdapter$1.$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[a1.ordinal()]) {
                case 4: case 5: case 6: case 9: {
                    b0 = true;
                    break;
                }
                default: {
                    b0 = false;
                    break;
                }
                case 1: {
                    break label2;
                }
                case 2: case 3: case 7: case 8: {
                    b0 = false;
                }
            }
            if (!a0.fontSizeCheckDone) {
                com.navdy.hud.app.ui.component.vlist.VerticalList.setFontSize(a0, this.vlist.allowsTwoLineTitles());
            }
            boolean b1 = a0.needsRebind;
            boolean b2 = a0.dontStartFluctuator;
            a0.needsRebind = false;
            a0.dontStartFluctuator = false;
            if (!b1) {
                a.clearAnimation();
            }
            a.setExtras(a0.extras);
            a.setPos(i);
            this.modelState.reset();
            a.preBind(a0, this.modelState);
            if (this.vlist.bindCallbacks && b0) {
                int i1 = (this.vlist.firstEntryBlank) ? i - 1 : i;
                this.vlist.callback.onBindToView((com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.data.get(i), (android.view.View)a.layout, i1, this.modelState);
            }
            a.bind(a0, this.modelState);
            if (this.initialState) {
                label0: {
                    if (b) {
                        break label0;
                    }
                    label1: {
                        if (!b1) {
                            break label1;
                        }
                        if (this.vlist.getRawPosition() == i) {
                            break label0;
                        }
                    }
                    a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE, 0);
                    break label2;
                }
                a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.NONE, 0, !b2);
            } else if (this.vlist.getRawPosition() != i) {
                a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.UNSELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.INIT, 100);
            } else {
                this.initialState = true;
                a.setState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType.INIT, 100);
                sLogger.v(new StringBuilder().append("initial state: position:").append(i).toString());
            }
        }
    }
    
    public android.support.v7.widget.RecyclerView$ViewHolder onCreateViewHolder(android.view.ViewGroup a, int i) {
        return this.onCreateViewHolder(a, i);
    }
    
    public com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder onCreateViewHolder(android.view.ViewGroup a, int i) {
        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a0 = null;
        com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType a1 = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.values()[i];
        if (sLogger.isLoggable(2)) {
            sLogger.v(new StringBuilder().append("onCreateViewHolder: ").append(a1).toString());
        }
        switch(com.navdy.hud.app.ui.component.vlist.VerticalAdapter$1.$SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[a1.ordinal()]) {
            case 11: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder.Companion.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 10: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 9: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 8: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 7: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.LoadingViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 6: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconOneViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 5: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 4: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 3: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 2: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            case 1: {
                a0 = com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder.buildViewHolder(a, this.vlist, this.handler);
                break;
            }
            default: {
                throw new RuntimeException(new StringBuilder().append("implement the model:").append(a1).toString());
            }
        }
        return a0;
    }
    
    public boolean onFailedToRecycleView(android.support.v7.widget.RecyclerView$ViewHolder a) {
        return this.onFailedToRecycleView((com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a);
    }
    
    public boolean onFailedToRecycleView(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a) {
        a.clearAnimation();
        this.clearPrevPos(a);
        return ((android.support.v7.widget.RecyclerView$Adapter)this).onFailedToRecycleView((android.support.v7.widget.RecyclerView$ViewHolder)a);
    }
    
    public void onViewRecycled(android.support.v7.widget.RecyclerView$ViewHolder a) {
        this.onViewRecycled((com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder)a);
    }
    
    public void onViewRecycled(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder a) {
        a.clearAnimation();
        this.clearPrevPos(a);
    }
    
    public void setHighlightIndex(int i) {
        this.highlightIndex = i;
    }
    
    public void setInitialState(boolean b) {
        this.initialState = b;
    }
    
    public void setViewHolderCacheIndex(int[] a) {
        this.viewHolderCache.clear();
        if (a != null) {
            int i = 0;
            while(i < a.length) {
                this.viewHolderCache.add(Integer.valueOf((a[i] != 0) ? 1 : 0));
                i = i + 1;
            }
        }
    }
    
    public void updateModel(int i, com.navdy.hud.app.ui.component.vlist.VerticalList$Model a) {
        if (i >= 0 && i < this.data.size()) {
            this.data.set(i, a);
            sLogger.v(new StringBuilder().append("updated model:").append(i).toString());
        }
    }
}
