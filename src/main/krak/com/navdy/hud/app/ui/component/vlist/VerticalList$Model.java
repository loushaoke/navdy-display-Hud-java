package com.navdy.hud.app.ui.component.vlist;

public class VerticalList$Model {
    final public static String INITIALS = "INITIAL";
    final public static String SUBTITLE_2_COLOR = "SUBTITLE_2_COLOR";
    final public static String SUBTITLE_COLOR = "SUBTITLE_COLOR";
    public int currentIconSelection;
    boolean dontStartFluctuator;
    public java.util.HashMap extras;
    public com.navdy.hud.app.ui.component.vlist.VerticalList$FontInfo fontInfo;
    public com.navdy.hud.app.ui.component.vlist.VerticalList$FontSize fontSize;
    boolean fontSizeCheckDone;
    public int icon;
    public int iconDeselectedColor;
    public int[] iconDeselectedColors;
    public int iconFluctuatorColor;
    public int[] iconFluctuatorColors;
    public int[] iconIds;
    public int[] iconList;
    public int iconSelectedColor;
    public int[] iconSelectedColors;
    public com.navdy.hud.app.ui.component.image.IconColorImageView$IconShape iconShape;
    public int iconSize;
    public int iconSmall;
    public int id;
    public boolean isEnabled;
    public boolean isOn;
    boolean needsRebind;
    public boolean noImageScaleAnimation;
    public boolean noTextAnimation;
    public int scrollItemLayoutId;
    public Object state;
    public String subTitle;
    public String subTitle2;
    public boolean subTitle2Formatted;
    public boolean subTitleFormatted;
    public boolean subTitle_2Lines;
    public boolean supportsToolTip;
    public String title;
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType type;
    
    public VerticalList$Model() {
        this.icon = 0;
        this.iconSelectedColor = -1;
        this.iconDeselectedColor = -1;
        this.iconSmall = -1;
        this.iconFluctuatorColor = -1;
        this.scrollItemLayoutId = -1;
        this.iconSize = -1;
    }
    
    public VerticalList$Model(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a) {
        this.icon = 0;
        this.iconSelectedColor = -1;
        this.iconDeselectedColor = -1;
        this.iconSmall = -1;
        this.iconFluctuatorColor = -1;
        this.scrollItemLayoutId = -1;
        this.iconSize = -1;
        this.type = a.type;
        this.id = a.id;
        this.icon = a.icon;
        this.iconSelectedColor = a.iconSelectedColor;
        this.iconDeselectedColor = a.iconDeselectedColor;
        this.iconSmall = a.iconSmall;
        this.iconFluctuatorColor = a.iconFluctuatorColor;
        this.title = a.title;
        this.subTitle = a.subTitle;
        this.subTitle2 = a.subTitle2;
        this.subTitle_2Lines = a.subTitle_2Lines;
        this.subTitleFormatted = a.subTitleFormatted;
        this.subTitle2Formatted = a.subTitle2Formatted;
        this.scrollItemLayoutId = a.scrollItemLayoutId;
        this.iconList = a.iconList;
        this.iconIds = a.iconIds;
        this.iconSelectedColors = a.iconSelectedColors;
        this.iconDeselectedColors = a.iconDeselectedColors;
        this.iconFluctuatorColors = a.iconFluctuatorColors;
        this.currentIconSelection = a.currentIconSelection;
        this.supportsToolTip = a.supportsToolTip;
        this.extras = a.extras;
        this.state = a.state;
        this.iconShape = a.iconShape;
        this.needsRebind = a.needsRebind;
        this.dontStartFluctuator = a.dontStartFluctuator;
        this.fontSizeCheckDone = a.fontSizeCheckDone;
        this.fontInfo = a.fontInfo;
        this.fontSize = a.fontSize;
        this.noTextAnimation = a.noTextAnimation;
        this.noImageScaleAnimation = a.noImageScaleAnimation;
        this.iconSize = a.iconSize;
    }
    
    public void clear() {
        this.id = 0;
        this.icon = 0;
        this.iconSelectedColor = -1;
        this.iconDeselectedColor = -1;
        this.iconSmall = -1;
        this.iconFluctuatorColor = -1;
        this.title = null;
        this.subTitle = null;
        this.subTitle2 = null;
        this.subTitle_2Lines = false;
        this.subTitleFormatted = false;
        this.subTitle2Formatted = false;
        this.scrollItemLayoutId = -1;
        this.iconList = null;
        this.iconIds = null;
        this.currentIconSelection = 0;
        this.extras = null;
        this.state = null;
        this.iconShape = null;
        this.needsRebind = false;
        this.dontStartFluctuator = false;
        this.fontSizeCheckDone = false;
        this.fontInfo = null;
        this.fontSize = null;
        this.noTextAnimation = false;
        this.noImageScaleAnimation = false;
        this.iconSize = -1;
    }
}
