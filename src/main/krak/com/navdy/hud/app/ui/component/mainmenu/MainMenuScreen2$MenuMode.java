package com.navdy.hud.app.ui.component.mainmenu;


public enum MainMenuScreen2$MenuMode {
    MAIN_MENU(0),
    REPLY_PICKER(1),
    SNAPSHOT_TITLE_PICKER(2);

    private int value;
    MainMenuScreen2$MenuMode(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MainMenuScreen2$MenuMode extends Enum {
//    final private static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode MAIN_MENU;
//    final public static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode REPLY_PICKER;
//    final public static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode SNAPSHOT_TITLE_PICKER;
//    
//    static {
//        MAIN_MENU = new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode("MAIN_MENU", 0);
//        REPLY_PICKER = new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode("REPLY_PICKER", 1);
//        SNAPSHOT_TITLE_PICKER = new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode("SNAPSHOT_TITLE_PICKER", 2);
//        com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode[] a = new com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode[3];
//        a[0] = MAIN_MENU;
//        a[1] = REPLY_PICKER;
//        a[2] = SNAPSHOT_TITLE_PICKER;
//        $VALUES = a;
//    }
//    
//    private MainMenuScreen2$MenuMode(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode)Enum.valueOf(com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$MenuMode[] values() {
//        return $VALUES.clone();
//    }
//}
//