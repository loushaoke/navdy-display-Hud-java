package com.navdy.hud.app.ui.activity;

class Main$Presenter$4 implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
    final com.navdy.hud.app.ui.activity.Main$Presenter this$0;
    
    Main$Presenter$4(com.navdy.hud.app.ui.activity.Main$Presenter a) {
        super();
        this.this$0 = a;
    }
    
    public void onStart(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager$Mode a0) {
        com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("notification anim start id[").append(s).append("]  type[").append(a).append("] mode[").append(a0).append("]").toString());
        if (a0 == com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND) {
            this.this$0.setNotificationColorVisibility(4);
            this.this$0.setSystemTrayVisibility(4);
        }
        this.this$0.clearInputFocus();
        com.navdy.hud.app.ui.activity.Main$Presenter.access$502(this.this$0, true);
    }
    
    public void onStop(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager$Mode a0) {
        com.navdy.hud.app.ui.activity.Main.access$200().v(new StringBuilder().append("notification anim stop id[").append(s).append("]  type[").append(a).append("] mode[").append(a0).append("]").toString());
        com.navdy.hud.app.ui.activity.Main$Presenter.access$502(this.this$0, false);
        if (a0 == com.navdy.hud.app.ui.framework.UIStateManager$Mode.COLLAPSE) {
            com.navdy.hud.app.ui.activity.Main$Presenter.access$400(this.this$0, com.navdy.hud.app.ui.activity.Main$ScreenCategory.NOTIFICATION, (com.navdy.service.library.events.ui.Screen)null);
        }
        com.navdy.hud.app.ui.activity.Main$Presenter.access$600(this.this$0);
    }
}
