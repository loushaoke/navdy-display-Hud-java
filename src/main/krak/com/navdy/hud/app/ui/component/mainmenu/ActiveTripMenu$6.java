package com.navdy.hud.app.ui.component.mainmenu;

class ActiveTripMenu$6 {
    final static int[] $SwitchMap$com$navdy$hud$app$maps$MapEvents$ManeuverEvent$Type;
    final static int[] $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit;
    
    static {
        $SwitchMap$com$navdy$hud$app$maps$MapEvents$ManeuverEvent$Type = new int[com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$maps$MapEvents$ManeuverEvent$Type;
        com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type a0 = com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type.INTERMEDIATE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit = new int[com.navdy.service.library.events.navigation.DistanceUnit.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit;
        com.navdy.service.library.events.navigation.DistanceUnit a2 = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_METERS;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_KMS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_FEET.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException3) {
        }
    }
}
