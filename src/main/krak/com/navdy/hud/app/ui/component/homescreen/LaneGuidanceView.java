package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class LaneGuidanceView extends android.widget.LinearLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    private com.squareup.otto.Bus bus;
    private int iconH;
    private int iconW;
    private com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo lastEvent;
    private com.navdy.service.library.log.Logger logger;
    private int mapIconIndicatorBottomMarginWithLane;
    private boolean needsMeasurement;
    private boolean paused;
    private int separatorColor;
    private int separatorH;
    private int separatorMargin;
    private int separatorW;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public LaneGuidanceView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public LaneGuidanceView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public LaneGuidanceView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
    }
    
    private void addSeparator(android.content.Context a, boolean b, boolean b0) {
        android.view.View a0 = new android.view.View(a);
        android.widget.LinearLayout$LayoutParams a1 = new android.widget.LinearLayout$LayoutParams(this.separatorW, this.separatorH);
        a1.gravity = 80;
        if (b) {
            a1.leftMargin = this.separatorMargin;
        }
        if (b0) {
            a1.rightMargin = this.separatorMargin;
        }
        a0.setLayoutParams((android.view.ViewGroup$LayoutParams)a1);
        a0.setBackgroundColor(this.separatorColor);
        this.addView(a0, (android.view.ViewGroup$LayoutParams)a1);
    }
    
    private int getMapIconX(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        int i = 0;
        label2: if (a == null) {
            com.navdy.hud.app.ui.activity.Main a0 = this.uiStateManager.getRootScreen();
            boolean b = a0.isNotificationViewShowing();
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!a0.isNotificationExpanding()) {
                        break label0;
                    }
                }
                i = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
                break label2;
            }
            i = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorX;
        } else {
            switch(com.navdy.hud.app.ui.component.homescreen.LaneGuidanceView$1.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
                case 2: {
                    i = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorX;
                    break;
                }
                case 1: {
                    i = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
                    break;
                }
                default: {
                    i = 0;
                }
            }
        }
        return i;
    }
    
    private void hideMapIconIndicator() {
        this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator().setVisibility(8);
    }
    
    private void showMapIconIndicator() {
        android.widget.ImageView a = this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator();
        a.setX((float)this.getMapIconX((com.navdy.hud.app.view.MainView$CustomAnimationMode)null));
        ((android.view.ViewGroup$MarginLayoutParams)a.getLayoutParams()).bottomMargin = this.mapIconIndicatorBottomMarginWithLane;
        a.setVisibility(0);
    }
    
    public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        if (this.getVisibility() == 0 && this.getMeasuredWidth() != 0) {
            int i = this.getMeasuredWidth();
            int i0 = this.getMapIconX(a);
            a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this, (float)(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconWidth / 2 + i0 - i / 2)));
            a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator(), (float)i0));
        }
    }
    
    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        this.setY((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.laneGuidanceY);
        com.navdy.hud.app.manager.RemoteDeviceManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.bus = a.getBus();
        this.uiStateManager = a.getUiStateManager();
        android.content.res.Resources a0 = this.getContext().getResources();
        this.iconH = a0.getDimensionPixelSize(R.dimen.lane_guidance_icon_h);
        this.iconW = a0.getDimensionPixelSize(R.dimen.lane_guidance_icon_w);
        this.separatorH = a0.getDimensionPixelSize(R.dimen.lane_guidance_separator_h);
        this.separatorW = a0.getDimensionPixelSize(R.dimen.lane_guidance_separator_w);
        this.separatorMargin = a0.getDimensionPixelSize(R.dimen.lane_guidance_separator_margin);
        this.separatorColor = a0.getColor(17170443);
        this.mapIconIndicatorBottomMarginWithLane = a0.getDimensionPixelSize(R.dimen.map_icon_indicator_bottom_margin_with_lane);
    }
    
    public void onHideLaneInfo(com.navdy.hud.app.maps.MapEvents$HideTrafficLaneInfo a) {
        this.lastEvent = null;
        this.needsMeasurement = false;
        if (this.getVisibility() == 0) {
            this.logger.v("LaneGuidanceView:hideLanes");
            this.hideMapIconIndicator();
            this.setVisibility(8);
            this.removeAllViews();
        }
    }
    
    public void onLaneInfoShow(com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo a) {
        if (this.uiStateManager.getHomescreenView().getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
            this.logger.v("LaneGuidanceView:showLanes");
            this.lastEvent = a;
            if (!this.paused) {
                this.removeAllViews();
                android.content.Context a0 = this.getContext();
                int i = a.laneData.size();
                this.addSeparator(a0, false, true);
                int i0 = 0;
                while(i0 < i) {
                    android.widget.ImageView a1 = new android.widget.ImageView(a0);
                    a1.setLayoutParams((android.view.ViewGroup$LayoutParams)new android.widget.LinearLayout$LayoutParams(this.iconW, this.iconH));
                    android.graphics.drawable.Drawable[] a2 = ((com.navdy.hud.app.maps.MapEvents$LaneData)a.laneData.get(i0)).icons;
                    label1: {
                        label0: {
                            if (a2 == null) {
                                break label0;
                            }
                            if (a2.length <= 0) {
                                break label0;
                            }
                            a1.setImageDrawable((android.graphics.drawable.Drawable)new android.graphics.drawable.LayerDrawable(a2));
                            break label1;
                        }
                        a1.setImageResource(0);
                    }
                    this.addView((android.view.View)a1);
                    if (i0 < i - 1) {
                        this.addSeparator(a0, true, true);
                    }
                    i0 = i0 + 1;
                }
                this.addSeparator(a0, true, false);
                this.showMapIconIndicator();
                this.needsMeasurement = true;
                this.setVisibility(0);
            }
        } else {
            this.onHideLaneInfo((com.navdy.hud.app.maps.MapEvents$HideTrafficLaneInfo)null);
            this.lastEvent = a;
        }
    }
    
    protected void onMeasure(int i, int i0) {
        super.onMeasure(i, i0);
        if (this.needsMeasurement) {
            int i1 = this.getMeasuredWidth();
            if (i1 > 0) {
                int i2 = this.getMapIconX((com.navdy.hud.app.view.MainView$CustomAnimationMode)null);
                this.setX((float)(com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconWidth / 2 + i2 - i1 / 2));
                this.needsMeasurement = false;
            }
        }
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:lane");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:lane");
            if (this.uiStateManager.getHomescreenView().getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo a = this.lastEvent;
                if (a != null) {
                    this.lastEvent = null;
                    this.onLaneInfoShow(a);
                    this.logger.v("::onResume:shown last lane");
                }
            }
        }
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
    }
    
    public void showLastEvent() {
        if (this.lastEvent != null) {
            this.onLaneInfoShow(this.lastEvent);
        }
    }
}
