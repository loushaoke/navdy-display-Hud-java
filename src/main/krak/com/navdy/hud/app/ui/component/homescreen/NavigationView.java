package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class NavigationView extends android.widget.FrameLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler, com.navdy.hud.app.gesture.GestureDetector$GestureListener, com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    final private static int MAP_MASK_OFFSET = 1;
    final private static com.navdy.hud.app.maps.MapEvents$DialMapZoom ZOOM_IN;
    final private static com.navdy.hud.app.maps.MapEvents$DialMapZoom ZOOM_OUT;
    private com.squareup.otto.Bus bus;
    private Runnable cleanupMapOverviewRunnable;
    private com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    private com.here.android.mpa.mapping.MapMarker endMarker;
    private com.here.android.mpa.mapping.Map$OnTransformListener fluctuatorPosListener;
    private android.os.Handler handler;
    private com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager;
    private com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private boolean initComplete;
    private boolean isRenderingEnabled;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.maps.here.HereMapController mapController;
    @InjectView(R.id.mapIconIndicator)
    android.widget.ImageView mapIconIndicator;
    private java.util.List mapMarkerList;
    @InjectView(R.id.map_mask)
    android.widget.ImageView mapMask;
    @InjectView(R.id.mapView)
    com.here.android.mpa.mapping.MapView mapView;
    private boolean networkCheckRequired;
    @InjectView(R.id.noLocationContainer)
    com.navdy.hud.app.ui.component.homescreen.NoLocationView noLocationView;
    private java.util.ArrayList overviewMapRouteObjects;
    private boolean paused;
    private Runnable resetMapOverviewRunnable;
    private com.here.android.mpa.common.Image selectedDestinationImage;
    private boolean setInitialCenter;
    private Runnable showMapIconIndicatorRunnable;
    @InjectView(R.id.startDestinationFluctuator)
    com.navdy.hud.app.ui.component.FluctuatorAnimatorView startDestinationFluctuatorView;
    private com.here.android.mpa.mapping.MapMarker startMarker;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private com.here.android.mpa.common.Image unselectedDestinationImage;
    
    static {
        ZOOM_IN = new com.navdy.hud.app.maps.MapEvents$DialMapZoom(com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.IN);
        ZOOM_OUT = new com.navdy.hud.app.maps.MapEvents$DialMapZoom(com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type.OUT);
    }
    
    public NavigationView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public NavigationView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public NavigationView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.NavigationView.class);
        this.handler = new android.os.Handler();
        this.overviewMapRouteObjects = new java.util.ArrayList();
        this.mapMarkerList = (java.util.List)new java.util.ArrayList();
        this.fluctuatorPosListener = (com.here.android.mpa.mapping.Map$OnTransformListener)new com.navdy.hud.app.ui.component.homescreen.NavigationView$1(this);
        this.showMapIconIndicatorRunnable = (Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$2(this);
        this.resetMapOverviewRunnable = (Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$5(this);
        this.cleanupMapOverviewRunnable = (Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$6(this);
        if (!this.isInEditMode()) {
            this.uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            this.hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
            this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
        }
    }
    
    static com.navdy.hud.app.maps.here.HereMapController access$000(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        return a.mapController;
    }
    
    static void access$100(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        a.positionFluctuator();
    }
    
    static com.navdy.hud.app.profile.DriverProfileManager access$1000(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        return a.driverProfileManager;
    }
    
    static com.navdy.hud.app.ui.component.homescreen.HomeScreenView access$1100(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        return a.homeScreenView;
    }
    
    static com.squareup.otto.Bus access$1200(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        return a.bus;
    }
    
    static com.navdy.service.library.log.Logger access$200(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        return a.logger;
    }
    
    static void access$300(com.navdy.hud.app.ui.component.homescreen.NavigationView a, com.navdy.hud.app.maps.here.HereMapController$State a0) {
        a.setTransformCenter(a0);
    }
    
    static android.os.Handler access$400(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        return a.handler;
    }
    
    static com.here.android.mpa.mapping.MapMarker access$500(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        return a.startMarker;
    }
    
    static com.here.android.mpa.mapping.Map$OnTransformListener access$600(com.navdy.hud.app.ui.component.homescreen.NavigationView a, Runnable a0) {
        return a.animateBackfromOverviewMap(a0);
    }
    
    static com.here.android.mpa.mapping.MapMarker access$700(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        return a.endMarker;
    }
    
    static void access$800(com.navdy.hud.app.ui.component.homescreen.NavigationView a, boolean b, boolean b0, boolean b1, com.here.android.mpa.common.GeoPosition a0, double d, float f, boolean b2) {
        a.setMapToArModeInternal(b, b0, b1, a0, d, f, b2);
    }
    
    static Runnable access$900(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        return a.showMapIconIndicatorRunnable;
    }
    
    private com.here.android.mpa.mapping.Map$OnTransformListener animateBackfromOverviewMap(Runnable a) {
        return (com.here.android.mpa.mapping.Map$OnTransformListener)new com.navdy.hud.app.ui.component.homescreen.NavigationView$9(this, a);
    }
    
    private void handleEngineInit() {
        synchronized(this) {
            if (this.hereMapsManager.isRenderingAllowed()) {
                if (this.hereMapsManager.isInitialized()) {
                    this.logger.v("engine initialized");
                    this.onMapEngineInitializationCompleted();
                } else {
                    this.logger.e(new StringBuilder().append("onCreate() : Cannot initialize map engine: ").append(this.hereMapsManager.getError()).toString());
                    this.noLocationView.hideLocationUI();
                }
            } else {
                this.logger.v("onMapEngineInitializationCompleted: Maps Rendering not allowed, Quiet mode On!");
            }
        }
        /*monexit(this)*/;
    }
    
    private void initViews() {
        if (!this.isInEditMode()) {
            if (this.hereLocationFixManager != null) {
                this.hereLocationFixManager.hasLocationFix();
            }
            this.logger.v(new StringBuilder().append("first layout: location fix[").append(false).append("] initComplete[").append(this.initComplete).append("]").toString());
            this.noLocationView.showLocationUI();
        }
    }
    
    private void onMapEngineInitializationCompleted() {
        synchronized(this) {
            if (!this.initComplete) {
                this.mapController = this.hereMapsManager.getMapController();
                this.initComplete = true;
                this.logger.v("onMapEngineInitializationCompleted: Maps Rendering allowed");
                this.enableRendering();
                this.mapView.setCopyrightLogoPosition(com.here.android.mpa.common.CopyrightLogoPosition.TOP_RIGHT);
                this.mapView.onResume();
                this.hereMapsManager.setMapView(this.mapView, this);
                try {
                    com.here.android.mpa.common.Image a = new com.here.android.mpa.common.Image();
                    a.setImageResource(R.drawable.icon_driver_position);
                    com.here.android.mpa.common.GeoCoordinate a0 = this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
                    if (a0 == null) {
                        a0 = new com.here.android.mpa.common.GeoCoordinate(37.802086, -122.419015);
                    }
                    this.startMarker = new com.here.android.mpa.mapping.MapMarker(a0, a);
                } catch(Throwable a1) {
                    this.logger.e(a1);
                }
                this.logger.v("view rendered");
                if (this.noLocationView.isAcquiringLocation() && this.initComplete && this.setMapCenter()) {
                    this.noLocationView.hideLocationUI();
                }
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$MapUIReady());
            }
        }
        /*monexit(this)*/;
    }
    
    private void positionFluctuator() {
        try {
            if (this.startMarker != null) {
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$10(this), 3);
            }
        } catch(Throwable a) {
            this.logger.e(a);
        }
    }
    
    private boolean setMapCenter() {
        boolean b = false;
        if (this.setInitialCenter) {
            b = true;
        } else if (this.initComplete) {
            if (this.hereLocationFixManager == null) {
                this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
            }
            com.here.android.mpa.common.GeoCoordinate a = this.hereLocationFixManager.getLastGeoCoordinate();
            if (a == null) {
                b = false;
            } else {
                this.setInitialCenter = true;
                this.mapController.setCenter(a, com.here.android.mpa.mapping.Map$Animation.NONE, -1.0, -1f, -1f);
                this.logger.w(new StringBuilder().append("setMapCenter:setcenterInit done ").append(a).toString());
                if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil$Feature.FUEL_ROUTING)) {
                    com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance().markAvailable();
                }
                this.updateMapIndicator();
                if (com.navdy.hud.app.framework.network.NetworkStateManager.getInstance().isNetworkStateInitialized()) {
                    this.logger.v("checkPrevRoute:n/w is initialized");
                    this.startPreviousRoute(a);
                    this.logger.v("send map engine ready event-1");
                    this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_READY, (String)null, (String)null, (com.navdy.service.library.events.navigation.NavigationRouteResult)null, (String)null)));
                    this.bus.post(new com.navdy.hud.app.maps.MapEvents$MapEngineReady());
                    b = true;
                } else {
                    this.networkCheckRequired = true;
                    this.logger.v("checkPrevRoute:n/w is initialized, defer and wait");
                    b = true;
                }
            }
        } else {
            this.logger.w("setMapCenter:init not complete yet, deferring");
            b = false;
        }
        return b;
    }
    
    private void setMapToArModeInternal(boolean b, boolean b0, boolean b1, com.here.android.mpa.common.GeoPosition a, double d, float f, boolean b2) {
        com.here.android.mpa.common.GeoCoordinate a0 = null;
        float f0 = 0.0f;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.logger.v(new StringBuilder().append("setMapToArModeInternal animate:").append(b).append(" changeState:").append(b0).toString());
        com.navdy.hud.app.maps.here.HereMapCameraManager a1 = com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance();
        if (a == null) {
            a = a1.getLastGeoPosition();
        }
        if (b2) {
            this.cleanupMapOverview();
        }
        if (a == null) {
            a0 = this.mapController.getCenter();
            if (f == -1f) {
                f = 60f;
            }
            if (d != -1.0) {
                f0 = -1f;
            } else {
                d = 16.5;
                f0 = -1f;
            }
        } else {
            a0 = a.getCoordinate();
            f0 = (float)a.getHeading();
            if (f == -1f) {
                f = a1.getLastTilt();
            }
            if (b1) {
                d = a1.getLastZoom();
            } else if (d == -1.0) {
                d = 12.0;
            }
        }
        this.setTransformCenter(com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE);
        this.mapController.setCenterForState(this.mapController.getState(), a0, b ? com.here.android.mpa.mapping.Map$Animation.BOW : com.here.android.mpa.mapping.Map$Animation.NONE, d, f0, f);
        if (b0) {
            this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE);
        } else {
            this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController$State.TRANSITION);
        }
    }
    
    private void setTransformCenter() {
        if (this.mapController != null) {
            this.setTransformCenter(this.mapController.getState());
        } else {
            this.logger.v("setTransformCenter:no map");
        }
    }
    
    private void setTransformCenter(com.navdy.hud.app.maps.here.HereMapController$State a) {
        if (this.mapController != null) {
            switch(com.navdy.hud.app.ui.component.homescreen.NavigationView$12.$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[a.ordinal()]) {
                case 3: {
                    if (this.homeScreenView.isNavigationActive()) {
                        this.logger.v("setTransformCenter:ar:nav");
                        this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterSmallBottom);
                        break;
                    } else {
                        this.logger.v("setTransformCenter:ar:open");
                        this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterLargeBottom);
                        break;
                    }
                }
                case 2: {
                    this.logger.v("setTransformCenter:routeSearch");
                    this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterPicker);
                    break;
                }
                case 1: {
                    if (this.homeScreenView.isNavigationActive()) {
                        this.logger.v("setTransformCenter:overview:nav");
                        this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterSmallMiddle);
                        break;
                    } else {
                        this.logger.v("setTransformCenter:overview:open");
                        this.mapController.setTransformCenter(com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.transformCenterLargeMiddle);
                        break;
                    }
                }
            }
        } else {
            this.logger.v("setTransformCenter:no map");
        }
    }
    
    private void setViewMap(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        switch(com.navdy.hud.app.ui.component.homescreen.NavigationView$12.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.mapView.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewShrinkLeftX);
                break;
            }
            case 1: {
                this.mapView.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewX);
                break;
            }
        }
    }
    
    private void setViewMapIconIndicator(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        switch(com.navdy.hud.app.ui.component.homescreen.NavigationView$12.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.mapIconIndicator.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkLeft_X);
                break;
            }
            case 1: {
                this.mapIconIndicator.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorX);
                break;
            }
        }
    }
    
    private void setViewMapMask(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)this.mapMask.getLayoutParams();
        switch(com.navdy.hud.app.ui.component.homescreen.NavigationView$12.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.mapMask.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkLeftX);
                a0.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkWidth;
                break;
            }
            case 1: {
                this.mapMask.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskX);
                a0.width = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.fullWidth;
                break;
            }
        }
    }
    
    private void startPreviousRoute(com.here.android.mpa.common.GeoCoordinate a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$11(this, a), 3);
    }
    
    private void switchToOverviewMode(com.here.android.mpa.common.GeoCoordinate a, com.here.android.mpa.routing.Route a0, com.here.android.mpa.mapping.MapRoute a1, Runnable a2, boolean b, boolean b0, double d) {
        this.logger.v(new StringBuilder().append("switchToOverviewMode:").append(b0).toString());
        this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController$State.OVERVIEW);
        this.cleanupFluctuator();
        this.mapIconIndicator.setVisibility(4);
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$3(this, a0, a2, a, b0, d), 17);
    }
    
    private void updateMapIndicator() {
        boolean b = com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled();
        this.mapIconIndicator.setAlpha(b ? 0.4f : 1f);
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$7(this, b), 2);
        if (b) {
            this.hereLocationFixManager.addMarkers(this.mapController);
        } else {
            this.hereLocationFixManager.removeMarkers(this.mapController);
        }
    }
    
    public void addMapOverviewRoutes(com.here.android.mpa.mapping.MapRoute a) {
        this.logger.v(new StringBuilder().append("addMapOverviewRoutes:").append(a).toString());
        this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)a);
        this.overviewMapRouteObjects.add(a);
    }
    
    public void adjustMaplayoutHeight(int i) {
        int i0 = (i != com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeMapHeight) ? com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorOpenY : com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorActiveY;
        this.mapIconIndicator.setY((float)i0 - (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterIconHeight * 0.6f);
        ((android.view.ViewGroup$MarginLayoutParams)this.mapMask.getLayoutParams()).height = i;
        this.layoutMap();
        this.invalidate();
        this.requestLayout();
    }
    
    public void animateBackfromOverview(Runnable a) {
        this.logger.v("animateBackfromOverview");
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$4(this, a), 17);
    }
    
    public void animateToOverview(com.here.android.mpa.common.GeoCoordinate a, com.here.android.mpa.routing.Route a0, com.here.android.mpa.mapping.MapRoute a1, Runnable a2, boolean b) {
        this.switchToOverviewMode(a, a0, a1, a2, b, true, -1.0);
    }
    
    public void changeMarkerSelection(int i, int i0) {
        com.navdy.hud.app.maps.here.HereMapController$State a = this.mapController.getState();
        if (a == com.navdy.hud.app.maps.here.HereMapController$State.ROUTE_PICKER) {
            int i1 = this.mapMarkerList.size();
            if (i1 > 0) {
                this.logger.i(new StringBuilder().append("changeMarkerSelection {").append(i).append(",").append(i0).append("}").toString());
                if (i0 >= 0 && i0 < i1) {
                    com.here.android.mpa.mapping.MapMarker a0 = (com.here.android.mpa.mapping.MapMarker)this.mapMarkerList.get(i0);
                    if (a0 != null) {
                        com.here.android.mpa.mapping.MapMarker a1 = new com.here.android.mpa.mapping.MapMarker(a0.getCoordinate(), this.unselectedDestinationImage);
                        this.mapMarkerList.set(i0, a1);
                        this.mapController.removeMapObject((com.here.android.mpa.mapping.MapObject)a0);
                        this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)a1);
                    }
                }
                if (i >= 0 && i < i1) {
                    com.here.android.mpa.mapping.MapMarker a2 = (com.here.android.mpa.mapping.MapMarker)this.mapMarkerList.get(i);
                    if (a2 != null) {
                        com.here.android.mpa.mapping.MapMarker a3 = new com.here.android.mpa.mapping.MapMarker(a2.getCoordinate(), this.selectedDestinationImage);
                        this.mapMarkerList.set(i, a3);
                        this.mapController.removeMapObject((com.here.android.mpa.mapping.MapObject)a2);
                        this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)a3);
                    }
                }
            }
        } else {
            this.logger.i(new StringBuilder().append("changeMarkerSelection: incorrect state:").append(a).toString());
        }
    }
    
    public void cleanupFluctuator() {
        this.logger.v("cleanupFluctuator");
        this.mapController.removeTransformListener(this.fluctuatorPosListener);
        this.startDestinationFluctuatorView.stop();
        this.startDestinationFluctuatorView.setVisibility(8);
    }
    
    public void cleanupMapOverview() {
        if (android.os.Looper.getMainLooper() != android.os.Looper.myLooper()) {
            this.handler.post(this.cleanupMapOverviewRunnable);
        } else {
            this.cleanupMapOverviewRunnable.run();
        }
    }
    
    public void cleanupMapOverviewRoutes() {
        this.logger.v("cleanupMapOverviewRoutes");
        if (this.overviewMapRouteObjects.size() > 0) {
            java.util.ArrayList a = new java.util.ArrayList((java.util.Collection)this.overviewMapRouteObjects);
            Object a0 = ((java.util.List)a).iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                com.here.android.mpa.mapping.MapObject a1 = (com.here.android.mpa.mapping.MapObject)((java.util.Iterator)a0).next();
                this.logger.v(new StringBuilder().append("cleanupMapOverviewRoutes:").append(a1).toString());
            }
            this.mapController.removeMapObjects((java.util.List)a);
            this.overviewMapRouteObjects.clear();
        }
    }
    
    public void clearState() {
    }
    
    public void enableRendering() {
        synchronized(this) {
            if (this.isRenderingEnabled) {
                this.logger.v("enableRendering:Maps Rendering already enabled");
            } else if (this.initComplete) {
                this.hereMapsManager.initNavigation();
                this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE);
                this.hereMapsManager.installPositionListener();
                this.setTransformCenter(com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE);
                this.mapView.setMap(this.mapController.getMap());
                this.logger.v("enableRendering:Maps Rendering enabled");
                this.isRenderingEnabled = true;
            } else {
                this.logger.v("enableRendering:Maps Rendering init-engine not complete yet");
            }
        }
        /*monexit(this)*/;
    }
    
    public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        this.logger.v(new StringBuilder().append("getCustomAnimator: ").append(a).toString());
        switch(com.navdy.hud.app.ui.component.homescreen.NavigationView$12.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: case 3: {
                int i = 0;
                int i0 = 0;
                int i1 = 0;
                int i2 = 0;
                if (a != com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT) {
                    i = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkModeX;
                    i0 = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewShrinkModeX;
                    i1 = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkWidth;
                    i2 = 0;
                } else {
                    i = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
                    i0 = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewShrinkLeftX;
                    i2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkLeftX;
                    i1 = com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskShrinkWidth;
                }
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.mapIconIndicator, (float)i));
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.mapView, (float)i0));
                android.animation.ObjectAnimator a1 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.mapMask, (float)i2);
                android.animation.ValueAnimator a2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator((android.view.View)this.mapMask, i1);
                a0.with((android.animation.Animator)a1);
                a0.with((android.animation.Animator)a2);
                if (a != com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT) {
                    break;
                }
                if (this.isAcquiringLocation()) {
                    this.noLocationView.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT, a0);
                    break;
                } else {
                    this.noLocationView.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT);
                    break;
                }
            }
            case 1: {
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.mapIconIndicator, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.iconIndicatorX));
                a0.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.mapView, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapViewX));
                android.animation.ObjectAnimator a3 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.mapMask, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mapMaskX);
                android.animation.ValueAnimator a4 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getWidthAnimator((android.view.View)this.mapMask, this.mapMask.getMeasuredWidth() + 1, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.fullWidth + 1);
                a0.with((android.animation.Animator)a3);
                a0.with((android.animation.Animator)a4);
                if (this.isAcquiringLocation()) {
                    this.noLocationView.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND, a0);
                    break;
                } else {
                    this.noLocationView.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND);
                    break;
                }
            }
        }
    }
    
    public com.here.android.mpa.mapping.Map$OnTransformListener getFluctuatorTransformListener() {
        return this.fluctuatorPosListener;
    }
    
    public com.navdy.hud.app.maps.here.HereMapController getMapController() {
        return this.mapController;
    }
    
    public android.widget.ImageView getMapIconIndicatorView() {
        return this.mapIconIndicator;
    }
    
    public int getMapViewX() {
        return (int)this.mapView.getX();
    }
    
    public void getTopAnimator(android.animation.AnimatorSet$Builder a, boolean b) {
    }
    
    public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        this.homeScreenView = a;
        this.bus.register(this);
    }
    
    public boolean isAcquiringLocation() {
        return this.noLocationView.isAcquiringLocation();
    }
    
    public boolean isOverviewMapMode() {
        boolean b = false;
        if (this.mapController != null) {
            switch(com.navdy.hud.app.ui.component.homescreen.NavigationView$12.$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[this.mapController.getState().ordinal()]) {
                case 1: case 2: {
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void layoutMap() {
        this.setTransformCenter();
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.hereMapsManager.isInitializing()) {
            this.logger.v("engine initializing, wait");
        } else {
            this.handleEngineInit();
        }
    }
    
    public void onClick() {
    }
    
    public void onEngineInitialized(com.navdy.hud.app.maps.MapEvents$MapEngineInitialize a) {
        this.handleEngineInit();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        this.mapMask.setX(0.0f);
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.driverProfileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
        this.initViews();
    }
    
    public void onGeoPositionChange(com.here.android.mpa.common.GeoPosition a) {
        if (this.startMarker != null) {
            switch(com.navdy.hud.app.ui.component.homescreen.NavigationView$12.$SwitchMap$com$navdy$hud$app$maps$here$HereMapController$State[this.mapController.getState().ordinal()]) {
                case 2: {
                    this.startMarker.setCoordinate(a.getCoordinate());
                    break;
                }
                case 1: {
                    this.startMarker.setCoordinate(a.getCoordinate());
                    break;
                }
            }
        }
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public void onHudNetworkInit(com.navdy.hud.app.framework.network.NetworkStateManager$HudNetworkInitialized a) {
        this.logger.v(new StringBuilder().append("checkPrevRoute:hud n/w is initialized: ").append(this.networkCheckRequired).toString());
        if (this.networkCheckRequired) {
            this.networkCheckRequired = false;
            if (this.hereLocationFixManager == null) {
                this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
            }
            this.startPreviousRoute(this.hereLocationFixManager.getLastGeoCoordinate());
            this.logger.v("send map engine ready event-2");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_READY, (String)null, (String)null, (com.navdy.service.library.events.navigation.NavigationRouteResult)null, (String)null)));
        }
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        switch(com.navdy.hud.app.ui.component.homescreen.NavigationView$12.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 2: {
                this.bus.post(ZOOM_OUT);
                b = true;
                break;
            }
            case 1: {
                this.bus.post(ZOOM_IN);
                b = true;
                break;
            }
            default: {
                b = false;
            }
        }
        return b;
    }
    
    public void onLocationFixChangeEvent(com.navdy.hud.app.maps.MapEvents$LocationFix a) {
        this.logger.v(new StringBuilder().append("location fix event locationAvailable[").append(a.locationAvailable).append("] phone[").append(a.usingPhoneLocation).append("] gps [").append(a.usingLocalGpsLocation).append("]").toString());
        if (this.isAcquiringLocation() && this.initComplete && this.setMapCenter()) {
            this.noLocationView.hideLocationUI();
        }
    }
    
    public void onPause() {
        if (!this.paused) {
            this.logger.v("::onPause");
            this.paused = true;
            this.hereMapsManager.stopMapRendering();
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.logger.v("::onResume");
            this.paused = false;
            this.hereMapsManager.startMapRendering();
        }
    }
    
    public void onSettingsChange(com.navdy.hud.app.config.SettingsManager$SettingsChanged a) {
        if (a.setting == com.navdy.hud.app.device.gps.GpsUtils.SHOW_RAW_GPS) {
            this.updateMapIndicator();
        }
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onWakeup(com.navdy.hud.app.event.Wakeup a) {
        this.handleEngineInit();
    }
    
    public void resetMapOverview() {
        if (android.os.Looper.getMainLooper() != android.os.Looper.myLooper()) {
            this.handler.post(this.resetMapOverviewRunnable);
        } else {
            this.resetMapOverviewRunnable.run();
        }
    }
    
    public void resetTopViewsAnimator() {
    }
    
    public void setMapMaskVisibility(int i) {
        this.mapMask.setVisibility(i);
    }
    
    public void setMapToArMode(boolean b, boolean b0, boolean b1, com.here.android.mpa.common.GeoPosition a, double d, double d0, boolean b2) {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.component.homescreen.NavigationView$8(this, b, b0, b1, a, d, d0, b2), 17);
        } else {
            this.setMapToArModeInternal(b, b0, b1, a, d, (float)d0, b2);
        }
    }
    
    public void setMapViewX(int i) {
        this.mapView.setX((float)i);
    }
    
    public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        this.logger.v(new StringBuilder().append("setview: ").append(a).toString());
        this.setViewMapIconIndicator(a);
        this.setViewMap(a);
        this.setViewMapMask(a);
        this.noLocationView.setView(a);
    }
    
    public void showOverviewMap(com.here.android.mpa.routing.Route a, com.here.android.mpa.mapping.MapRoute a0, com.here.android.mpa.common.GeoCoordinate a1, boolean b) {
        this.logger.v("showOverviewMap");
        this.switchToOverviewMode(a1, a, a0, (Runnable)null, b, false, 12.0);
        this.mapIconIndicator.setVisibility(4);
    }
    
    public void showRouteMap(com.here.android.mpa.common.GeoPosition a, double d, float f) {
        this.logger.v("showRouteMap");
        this.setMapToArMode(false, true, false, a, d, (double)f, true);
        this.mapIconIndicator.setVisibility(0);
    }
    
    public void showStartMarker() {
        this.logger.v("showStartMarker");
        if (this.startMarker != null) {
            this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)this.startMarker);
        }
    }
    
    public void startFluctuator() {
        this.mapController.addTransformListener(this.getFluctuatorTransformListener());
    }
    
    public void switchBackfromRouteSearchMode(boolean b) {
        com.navdy.hud.app.maps.here.HereMapController$State a = this.mapController.getState();
        if (a == com.navdy.hud.app.maps.here.HereMapController$State.ROUTE_PICKER) {
            this.logger.v(new StringBuilder().append("map-route- switchBackfromRouteSearchMode:").append(b).toString());
            if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getSafetySpotListener().setSafetySpotsEnabledOnMap(true);
            }
            int i = this.mapMarkerList.size();
            if (i > 0) {
                this.logger.v(new StringBuilder().append("switchBackfromRouteSearchMode remove map markers:").append(i).toString());
                this.mapController.removeMapObjects(this.mapMarkerList);
                this.mapMarkerList = (java.util.List)new java.util.ArrayList();
            }
            this.setMapMaskVisibility(0);
            this.setMapViewX(0);
            this.homeScreenView.navigationViewsContainer.setVisibility(0);
            if (this.homeScreenView.isNavigationActive()) {
                if (this.homeScreenView.getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                    this.homeScreenView.activeEtaContainer.setVisibility(0);
                }
            } else {
                this.homeScreenView.openMapRoadInfoContainer.setVisibility(0);
                if (this.homeScreenView.getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                    this.homeScreenView.timeContainer.setVisibility(0);
                }
            }
            this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController$State.TRANSITION);
            if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().isManualZoom()) {
                this.setMapToArMode(false, true, true, (com.here.android.mpa.common.GeoPosition)null, -1.0, -1.0, false);
                this.mapIconIndicator.setVisibility(0);
            }
            if (this.startMarker != null) {
                this.mapController.removeMapObject((com.here.android.mpa.mapping.MapObject)this.startMarker);
            }
            if (this.endMarker != null) {
                this.mapController.removeMapObject((com.here.android.mpa.mapping.MapObject)this.endMarker);
            }
            this.logger.v("map-route- switchBackfromRouteSearchMode cleanup routes");
            this.cleanupMapOverviewRoutes();
            com.navdy.hud.app.maps.here.HereNavigationManager a0 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            com.here.android.mpa.routing.Route a1 = a0.getCurrentRoute();
            if (a1 != null && a0.isNavigationModeOn() && !a0.hasArrived()) {
                if (b) {
                    com.here.android.mpa.mapping.MapRoute a2 = new com.here.android.mpa.mapping.MapRoute(a1);
                    a2.setTrafficEnabled(true);
                    a0.addCurrentRoute(a2);
                    this.logger.v("map-route- switchBackfromRouteSearchMode: nav route added");
                } else {
                    this.logger.v("map-route- switchBackfromRouteSearchMode: nav route not added back");
                }
            }
            com.here.android.mpa.mapping.MapMarker a3 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getDestinationMarker();
            if (a3 != null) {
                this.logger.v("switchBackfromRouteSearchMode: destination marker added");
                this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)a3);
            }
            this.cleanupFluctuator();
            com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance().setZoom();
        } else {
            this.logger.v(new StringBuilder().append("switchBackfromRouteSearchMode: invalid state:").append(a).toString());
        }
    }
    
    public void switchScreen() {
        com.navdy.hud.app.screen.BaseScreen a = this.uiStateManager.getCurrentScreen();
        if (a == null) {
            this.logger.w("current screen is null");
        } else {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView a0 = this.uiStateManager.getHomescreenView();
            if (a0 != null) {
                com.navdy.hud.app.ui.activity.Main.saveHomeScreenPreference(a0.globalPreferences, com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP.ordinal());
                a0.setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP);
            }
            if (a.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_HOME) {
                this.logger.v("current screen is map no switch reqd");
            } else {
                this.logger.v(new StringBuilder().append("current screen is not map:").append(a.getScreen()).toString());
                com.navdy.hud.app.ui.activity.Main a1 = this.uiStateManager.getRootScreen();
                boolean b = a1.isNotificationViewShowing();
                label2: {
                    label0: {
                        label1: {
                            if (b) {
                                break label1;
                            }
                            if (!a1.isNotificationExpanding()) {
                                break label0;
                            }
                        }
                        this.logger.v("switching, but collpasing notif first");
                        this.homeScreenView.setShowCollapsedNotification(true);
                        break label2;
                    }
                    this.logger.v("switched to map");
                }
                this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_HYBRID_MAP).build());
            }
        }
    }
    
    public void switchToRouteSearchMode(com.here.android.mpa.common.GeoCoordinate a, com.here.android.mpa.common.GeoCoordinate a0, com.here.android.mpa.common.GeoBoundingBox a1, com.here.android.mpa.routing.Route a2, boolean b, java.util.List a3, int i) {
        this.logger.v(new StringBuilder().append("switchToRouteSearchMode: start[").append(a).append("] end [").append(a0).append("] x=").append(this.getMapViewX()).toString());
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getSafetySpotListener().setSafetySpotsEnabledOnMap(false);
        }
        this.setMapMaskVisibility(4);
        this.setMapViewX(-com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.transformCenterMoveX);
        this.homeScreenView.navigationViewsContainer.setVisibility(4);
        this.homeScreenView.openMapRoadInfoContainer.setVisibility(4);
        if (this.homeScreenView.timeContainer.getVisibility() != 0) {
            this.homeScreenView.activeEtaContainer.setVisibility(4);
        } else {
            this.homeScreenView.timeContainer.setVisibility(4);
        }
        this.mapController.setState(com.navdy.hud.app.maps.here.HereMapController$State.ROUTE_PICKER);
        this.mapIconIndicator.setVisibility(4);
        this.cleanupMapOverview();
        com.navdy.hud.app.maps.here.HereMapController$State a4 = com.navdy.hud.app.maps.here.HereMapController$State.ROUTE_PICKER;
        try {
            this.setTransformCenter(a4);
            com.here.android.mpa.common.GeoCoordinate a5 = (a != null) ? a : this.hereLocationFixManager.getLastGeoCoordinate();
            this.mapController.setCenterForState(com.navdy.hud.app.maps.here.HereMapController$State.ROUTE_PICKER, a5, com.here.android.mpa.mapping.Map$Animation.NONE, 15.5, 0.0f, 0.0f);
        } catch(Throwable a6) {
            this.logger.e(a6);
        }
        com.navdy.hud.app.maps.here.HereNavigationManager a7 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        if (a7.removeCurrentRoute()) {
            this.logger.v("map-route- switchToRouteSearchMode: nav route removed");
        } else {
            this.logger.v("map-route- switchToRouteSearchMode: nav route not removed, does not exist");
        }
        com.here.android.mpa.mapping.MapMarker a8 = a7.getDestinationMarker();
        if (a8 != null) {
            this.mapController.removeMapObject((com.here.android.mpa.mapping.MapObject)a8);
            this.logger.v("switchToRouteSearchMode: dest removed");
        }
        try {
            if (this.startMarker == null) {
                com.here.android.mpa.common.Image a9 = new com.here.android.mpa.common.Image();
                a9.setImageResource(R.drawable.icon_driver_position);
                this.startMarker = new com.here.android.mpa.mapping.MapMarker();
                this.startMarker.setIcon(a9);
            }
            if (a != null) {
                this.startMarker.setCoordinate(a);
                this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)this.startMarker);
            }
        } catch(Throwable a10) {
            this.logger.e(a10);
        }
        try {
            if (this.endMarker == null) {
                com.here.android.mpa.common.Image a11 = new com.here.android.mpa.common.Image();
                a11.setImageResource(R.drawable.icon_pin_dot_destination);
                this.endMarker = new com.here.android.mpa.mapping.MapMarker();
                this.endMarker.setIcon(a11);
            }
            if (a0 != null) {
                this.endMarker.setCoordinate(a0);
                this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)this.endMarker);
            }
        } catch(Throwable a12) {
            this.logger.e(a12);
        }
        if (this.mapMarkerList.size() > 0) {
            this.mapController.removeMapObjects(this.mapMarkerList);
            this.mapMarkerList = (java.util.List)new java.util.ArrayList();
        }
        if (a3 != null) {
            int i0 = a3.size();
            try {
                this.selectedDestinationImage = new com.here.android.mpa.common.Image();
                com.here.android.mpa.common.Image a13 = this.selectedDestinationImage;
                if (i == -1) {
                    i = R.drawable.icon_pin_dot_destination_blue;
                }
                a13.setImageResource(i);
            } catch(Throwable a14) {
                this.logger.e(a14);
            }
            com.here.android.mpa.common.Image a15 = this.unselectedDestinationImage;
            label0: {
                Throwable a16 = null;
                if (a15 != null) {
                    break label0;
                }
                try {
                    this.unselectedDestinationImage = new com.here.android.mpa.common.Image();
                    this.unselectedDestinationImage.setImageResource(R.drawable.icon_destination_pin_unselected);
                    break label0;
                } catch(Throwable a17) {
                    a16 = a17;
                }
                this.logger.e(a16);
            }
            Object a18 = a3;
            int i1 = 0;
            while(i1 < i0) {
                com.here.android.mpa.common.GeoCoordinate a19 = (com.here.android.mpa.common.GeoCoordinate)((java.util.List)a18).get(i1);
                if (a19 == null) {
                    this.mapMarkerList.add(null);
                } else {
                    com.here.android.mpa.mapping.MapMarker a20 = new com.here.android.mpa.mapping.MapMarker(a19, this.unselectedDestinationImage);
                    this.mapController.addMapObject((com.here.android.mpa.mapping.MapObject)a20);
                    this.mapMarkerList.add(a20);
                }
                i1 = i1 + 1;
            }
            this.logger.v(new StringBuilder().append("addded dest marker:").append(i0).toString());
        }
        this.zoomToBoundBox(a1, a2, b, true);
    }
    
    public void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode a) {
        this.adjustMaplayoutHeight((this.homeScreenView.isNavigationActive()) ? com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.activeMapHeight : com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.openMapHeight);
    }
    
    public void zoomToBoundBox(com.here.android.mpa.common.GeoBoundingBox a, com.here.android.mpa.routing.Route a0, boolean b, boolean b0) {
        com.navdy.hud.app.maps.here.HereMapController$State a1 = this.mapController.getState();
        com.navdy.hud.app.maps.here.HereMapController$State a2 = com.navdy.hud.app.maps.here.HereMapController$State.ROUTE_PICKER;
        label0: {
            Throwable a3 = null;
            if (a1 == a2) {
                try {
                    this.cleanupFluctuator();
                    if (b) {
                        this.startFluctuator();
                    }
                    if (a == null) {
                        this.logger.v("zoomed to bbox: null");
                        break label0;
                    } else {
                        if (b0) {
                            this.cleanupMapOverviewRoutes();
                        }
                        if (a0 != null) {
                            com.here.android.mpa.mapping.MapRoute a4 = new com.here.android.mpa.mapping.MapRoute(a0);
                            a4.setTrafficEnabled(true);
                            this.addMapOverviewRoutes(a4);
                        }
                        this.mapController.zoomTo(a, com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.routePickerViewRect, com.here.android.mpa.mapping.Map$Animation.BOW, -1f);
                        this.logger.v("zoomed to bbox");
                        break label0;
                    }
                } catch(Throwable a5) {
                    a3 = a5;
                }
            } else {
                this.logger.i(new StringBuilder().append("zoomToBoundBox: incorrect state:").append(a1).toString());
                break label0;
            }
            this.logger.e(a3);
        }
    }
}
