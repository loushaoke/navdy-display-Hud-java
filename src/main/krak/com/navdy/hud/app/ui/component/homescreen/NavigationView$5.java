package com.navdy.hud.app.ui.component.homescreen;

class NavigationView$5 implements Runnable {
    final com.navdy.hud.app.ui.component.homescreen.NavigationView this$0;
    
    NavigationView$5(com.navdy.hud.app.ui.component.homescreen.NavigationView a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.homescreen.NavigationView.access$200(this.this$0).v(new StringBuilder().append("resetMapOverview: ").append(com.navdy.hud.app.ui.component.homescreen.NavigationView.access$000(this.this$0).getState()).toString());
        this.this$0.cleanupMapOverview();
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.navdy.hud.app.maps.here.HereMapCameraManager a = com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance();
            if (a.isOverviewZoomLevel()) {
                this.this$0.showOverviewMap((com.here.android.mpa.routing.Route)null, (com.here.android.mpa.mapping.MapRoute)null, a.getLastGeoCoordinate(), false);
            }
        }
    }
}
