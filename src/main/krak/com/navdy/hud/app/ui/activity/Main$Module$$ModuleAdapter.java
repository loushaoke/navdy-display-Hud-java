package com.navdy.hud.app.ui.activity;

final public class Main$Module$$ModuleAdapter extends dagger.internal.ModuleAdapter {
    final private static Class[] INCLUDES;
    final private static String[] INJECTS;
    final private static Class[] STATIC_INJECTIONS;
    
    static {
        String[] a = new String[7];
        a[0] = "members/com.navdy.hud.app.ui.activity.MainActivity";
        a[1] = "members/com.navdy.hud.app.view.MainView";
        a[2] = "members/com.navdy.hud.app.view.NotificationView";
        a[3] = "members/com.navdy.hud.app.presenter.NotificationPresenter";
        a[4] = "members/com.navdy.hud.app.ui.framework.UIStateManager";
        a[5] = "members/com.navdy.hud.app.view.ContainerView";
        a[6] = "members/com.navdy.hud.app.debug.GestureEngine";
        INJECTS = a;
        STATIC_INJECTIONS = new Class[0];
        INCLUDES = new Class[0];
    }
    
    public Main$Module$$ModuleAdapter() {
        super(com.navdy.hud.app.ui.activity.Main$Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
