package com.navdy.hud.app.common;

public class MainThreadBus extends com.squareup.otto.Bus {
    private com.navdy.hud.app.debug.BusLeakDetector busLeakDetector;
    private boolean isEngBuild;
    final private com.navdy.service.library.log.Logger logger;
    final private android.os.Handler mHandler;
    public int threshold;
    
    public MainThreadBus() {
        this.mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.common.MainThreadBus.class);
        this.isEngBuild = !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
        if (this.isEngBuild) {
            this.busLeakDetector = com.navdy.hud.app.debug.BusLeakDetector.getInstance();
        }
    }
    
    static void access$001(com.navdy.hud.app.common.MainThreadBus a, Object a0) {
        ((com.squareup.otto.Bus)a).post(a0);
    }
    
    static com.navdy.service.library.log.Logger access$100(com.navdy.hud.app.common.MainThreadBus a) {
        return a.logger;
    }
    
    static boolean access$200(com.navdy.hud.app.common.MainThreadBus a) {
        return a.isEngBuild;
    }
    
    static com.navdy.hud.app.debug.BusLeakDetector access$300(com.navdy.hud.app.common.MainThreadBus a) {
        return a.busLeakDetector;
    }
    
    static void access$401(com.navdy.hud.app.common.MainThreadBus a, Object a0) {
        ((com.squareup.otto.Bus)a).register(a0);
    }
    
    static void access$501(com.navdy.hud.app.common.MainThreadBus a, Object a0) {
        ((com.squareup.otto.Bus)a).unregister(a0);
    }
    
    public void post(Object a) {
        if (a != null) {
            this.mHandler.post((Runnable)new com.navdy.hud.app.common.MainThreadBus$1(this, a));
        }
    }
    
    public void register(Object a) {
        if (a != null) {
            this.mHandler.post((Runnable)new com.navdy.hud.app.common.MainThreadBus$2(this, a));
        }
    }
    
    public void unregister(Object a) {
        if (a != null) {
            this.mHandler.post((Runnable)new com.navdy.hud.app.common.MainThreadBus$3(this, a));
        }
    }
}
