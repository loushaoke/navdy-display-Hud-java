package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideInputManagerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    final private com.navdy.hud.app.common.ProdModule module;
    private dagger.internal.Binding powerManager;
    private dagger.internal.Binding uiStateManager;
    
    public ProdModule$$ModuleAdapter$ProvideInputManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.manager.InputManager", true, "com.navdy.hud.app.common.ProdModule", "provideInputManager");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.manager.InputManager get() {
        return this.module.provideInputManager((com.squareup.otto.Bus)this.bus.get(), (com.navdy.hud.app.device.PowerManager)this.powerManager.get(), (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.bus);
        a.add(this.powerManager);
        a.add(this.uiStateManager);
    }
}
