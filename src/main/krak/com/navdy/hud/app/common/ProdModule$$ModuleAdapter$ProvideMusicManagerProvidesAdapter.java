package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    private dagger.internal.Binding artworkCache;
    private dagger.internal.Binding bus;
    final private com.navdy.hud.app.common.ProdModule module;
    private dagger.internal.Binding musicCollectionResponseMessageCache;
    private dagger.internal.Binding pandoraManager;
    private dagger.internal.Binding uiStateManager;
    
    public ProdModule$$ModuleAdapter$ProvideMusicManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.manager.MusicManager", true, "com.navdy.hud.app.common.ProdModule", "provideMusicManager");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.musicCollectionResponseMessageCache = a.requestBinding("com.navdy.hud.app.storage.cache.MessageCache<com.navdy.service.library.events.audio.MusicCollectionResponse>", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.pandoraManager = a.requestBinding("com.navdy.hud.app.service.pandora.PandoraManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.artworkCache = a.requestBinding("com.navdy.hud.app.util.MusicArtworkCache", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.manager.MusicManager get() {
        return this.module.provideMusicManager((com.navdy.hud.app.storage.cache.MessageCache)this.musicCollectionResponseMessageCache.get(), (com.squareup.otto.Bus)this.bus.get(), (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get(), (com.navdy.hud.app.service.pandora.PandoraManager)this.pandoraManager.get(), (com.navdy.hud.app.util.MusicArtworkCache)this.artworkCache.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.musicCollectionResponseMessageCache);
        a.add(this.bus);
        a.add(this.uiStateManager);
        a.add(this.pandoraManager);
        a.add(this.artworkCache);
    }
}
