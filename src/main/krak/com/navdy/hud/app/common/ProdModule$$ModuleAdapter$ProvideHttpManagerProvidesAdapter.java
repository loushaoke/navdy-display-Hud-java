package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.service.library.network.http.IHttpManager", true, "com.navdy.hud.app.common.ProdModule", "provideHttpManager");
        this.module = a;
        this.setLibrary(true);
    }
    
    public com.navdy.service.library.network.http.IHttpManager get() {
        return this.module.provideHttpManager();
    }
    
    public Object get() {
        return this.get();
    }
}
