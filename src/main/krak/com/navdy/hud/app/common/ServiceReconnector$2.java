package com.navdy.hud.app.common;

class ServiceReconnector$2 implements android.content.ServiceConnection {
    final com.navdy.hud.app.common.ServiceReconnector this$0;
    
    ServiceReconnector$2(com.navdy.hud.app.common.ServiceReconnector a) {
        super();
        this.this$0 = a;
    }
    
    public void onServiceConnected(android.content.ComponentName a, android.os.IBinder a0) {
        com.navdy.hud.app.common.ServiceReconnector.access$200(this.this$0).i(new StringBuilder().append("ServiceConnection established with ").append(a).toString());
        this.this$0.onConnected(a, a0);
    }
    
    public void onServiceDisconnected(android.content.ComponentName a) {
        this.this$0.onDisconnected(a);
        if (!this.this$0.shuttingDown) {
            com.navdy.hud.app.common.ServiceReconnector.access$200(this.this$0).i("Service disconnected - will try reconnecting");
            com.navdy.hud.app.common.ServiceReconnector.access$100(this.this$0).postDelayed(com.navdy.hud.app.common.ServiceReconnector.access$600(this.this$0), 15000L);
        }
    }
}
