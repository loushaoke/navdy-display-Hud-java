package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideConnectionHandlerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    private dagger.internal.Binding connectionServiceProxy;
    private dagger.internal.Binding driverProfileManager;
    final private com.navdy.hud.app.common.ProdModule module;
    private dagger.internal.Binding powerManager;
    private dagger.internal.Binding timeHelper;
    private dagger.internal.Binding uiStateManager;
    
    public ProdModule$$ModuleAdapter$ProvideConnectionHandlerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.service.ConnectionHandler", true, "com.navdy.hud.app.common.ProdModule", "provideConnectionHandler");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.connectionServiceProxy = a.requestBinding("com.navdy.hud.app.service.ConnectionServiceProxy", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.driverProfileManager = a.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.timeHelper = a.requestBinding("com.navdy.hud.app.common.TimeHelper", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.service.ConnectionHandler get() {
        return this.module.provideConnectionHandler((com.navdy.hud.app.service.ConnectionServiceProxy)this.connectionServiceProxy.get(), (com.navdy.hud.app.device.PowerManager)this.powerManager.get(), (com.navdy.hud.app.profile.DriverProfileManager)this.driverProfileManager.get(), (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get(), (com.navdy.hud.app.common.TimeHelper)this.timeHelper.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.connectionServiceProxy);
        a.add(this.powerManager);
        a.add(this.driverProfileManager);
        a.add(this.uiStateManager);
        a.add(this.timeHelper);
    }
}
