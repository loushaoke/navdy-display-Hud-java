package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideVoiceSearchHandlerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding featureUtil;
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvideVoiceSearchHandlerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.framework.voice.VoiceSearchHandler", true, "com.navdy.hud.app.common.ProdModule", "provideVoiceSearchHandler");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
        this.featureUtil = a.requestBinding("com.navdy.hud.app.util.FeatureUtil", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.framework.voice.VoiceSearchHandler get() {
        return this.module.provideVoiceSearchHandler((com.squareup.otto.Bus)this.bus.get(), (com.navdy.hud.app.util.FeatureUtil)this.featureUtil.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.bus);
        a.add(this.featureUtil);
    }
}
