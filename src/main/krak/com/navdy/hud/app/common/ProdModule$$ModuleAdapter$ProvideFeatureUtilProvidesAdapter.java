package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvideFeatureUtilProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    private dagger.internal.Binding sharedPreferences;
    
    public ProdModule$$ModuleAdapter$ProvideFeatureUtilProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.util.FeatureUtil", true, "com.navdy.hud.app.common.ProdModule", "provideFeatureUtil");
        this.module = a;
        this.setLibrary(true);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.common.ProdModule.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.util.FeatureUtil get() {
        return this.module.provideFeatureUtil((android.content.SharedPreferences)this.sharedPreferences.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.sharedPreferences);
    }
}
