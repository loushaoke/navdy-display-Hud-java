package com.navdy.hud.app.event;

public class Shutdown {
    final public static String EXTRA_SHUTDOWN_CAUSE = "SHUTDOWN_CAUSE";
    final public com.navdy.hud.app.event.Shutdown$Reason reason;
    final public com.navdy.hud.app.event.Shutdown$State state;
    
    public Shutdown(com.navdy.hud.app.event.Shutdown$Reason a) {
        this.reason = a;
        this.state = (a.requireConfirmation) ? com.navdy.hud.app.event.Shutdown$State.CONFIRMATION : com.navdy.hud.app.event.Shutdown$State.CONFIRMED;
    }
    
    public Shutdown(com.navdy.hud.app.event.Shutdown$Reason a, com.navdy.hud.app.event.Shutdown$State a0) {
        this.reason = a;
        this.state = a0;
    }
    
    public String toString() {
        StringBuilder a = new StringBuilder("Shutdown{");
        a.append("reason=").append(this.reason);
        a.append(", state=").append(this.state);
        a.append((char)125);
        return a.toString();
    }
}
