package com.navdy.hud.app.event;


public enum DriverProfileUpdated$State {
    UP_TO_DATE(0),
    UPDATED(1);

    private int value;
    DriverProfileUpdated$State(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class DriverProfileUpdated$State extends Enum {
//    final private static com.navdy.hud.app.event.DriverProfileUpdated$State[] $VALUES;
//    final public static com.navdy.hud.app.event.DriverProfileUpdated$State UPDATED;
//    final public static com.navdy.hud.app.event.DriverProfileUpdated$State UP_TO_DATE;
//    
//    static {
//        UP_TO_DATE = new com.navdy.hud.app.event.DriverProfileUpdated$State("UP_TO_DATE", 0);
//        UPDATED = new com.navdy.hud.app.event.DriverProfileUpdated$State("UPDATED", 1);
//        com.navdy.hud.app.event.DriverProfileUpdated$State[] a = new com.navdy.hud.app.event.DriverProfileUpdated$State[2];
//        a[0] = UP_TO_DATE;
//        a[1] = UPDATED;
//        $VALUES = a;
//    }
//    
//    private DriverProfileUpdated$State(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.event.DriverProfileUpdated$State valueOf(String s) {
//        return (com.navdy.hud.app.event.DriverProfileUpdated$State)Enum.valueOf(com.navdy.hud.app.event.DriverProfileUpdated$State.class, s);
//    }
//    
//    public static com.navdy.hud.app.event.DriverProfileUpdated$State[] values() {
//        return $VALUES.clone();
//    }
//}
//