package com.navdy.hud.app.device.dial;


    public enum DialConstants$PairingRule {
        FIRST(0);

        private int value;
        DialConstants$PairingRule(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class DialConstants$PairingRule extends Enum {
//    final private static com.navdy.hud.app.device.dial.DialConstants$PairingRule[] $VALUES;
//    final public static com.navdy.hud.app.device.dial.DialConstants$PairingRule FIRST;
//    
//    static {
//        FIRST = new com.navdy.hud.app.device.dial.DialConstants$PairingRule("FIRST", 0);
//        com.navdy.hud.app.device.dial.DialConstants$PairingRule[] a = new com.navdy.hud.app.device.dial.DialConstants$PairingRule[1];
//        a[0] = FIRST;
//        $VALUES = a;
//    }
//    
//    private DialConstants$PairingRule(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.device.dial.DialConstants$PairingRule valueOf(String s) {
//        return (com.navdy.hud.app.device.dial.DialConstants$PairingRule)Enum.valueOf(com.navdy.hud.app.device.dial.DialConstants$PairingRule.class, s);
//    }
//    
//    public static com.navdy.hud.app.device.dial.DialConstants$PairingRule[] values() {
//        return $VALUES.clone();
//    }
//}
//