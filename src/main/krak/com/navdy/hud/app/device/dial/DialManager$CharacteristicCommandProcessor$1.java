package com.navdy.hud.app.device.dial;

class DialManager$CharacteristicCommandProcessor$1 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor this$0;
    final com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command val$command;
    
    DialManager$CharacteristicCommandProcessor$1(com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor a, com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command a0) {
        super();
        this.this$0 = a;
        this.val$command = a0;
    }
    
    public void run() {
        try {
            if (com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor.access$2500(this.this$0) != null) {
                this.val$command.process(com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor.access$2500(this.this$0));
            }
        } catch(Throwable a) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.d("Error while executing GATT command", a);
        }
    }
}
