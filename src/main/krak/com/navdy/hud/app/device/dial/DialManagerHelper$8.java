package com.navdy.hud.app.device.dial;

final class DialManagerHelper$8 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection val$callBack;
    
    DialManagerHelper$8(com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection a) {
        super();
        this.val$callBack = a;
    }
    
    public void run() {
        this.val$callBack.onAttemptDisconnection(false);
    }
}
