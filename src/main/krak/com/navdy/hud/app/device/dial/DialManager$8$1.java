package com.navdy.hud.app.device.dial;

class DialManager$8$1 implements com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten {
    final com.navdy.hud.app.device.dial.DialManager$8 this$1;
    final String val$dialName;
    
    DialManager$8$1(com.navdy.hud.app.device.dial.DialManager$8 a, String s) {
        super();
        this.this$1 = a;
        this.val$dialName = s;
    }
    
    public void onForgotten() {
        com.navdy.hud.app.device.dial.DialManager.sLogger.v("connectionErrorRunnable: dial forgotten");
        com.navdy.hud.app.device.dial.DialManager.access$1300().dialName = this.val$dialName;
        com.navdy.hud.app.device.dial.DialManager.access$200(this.this$1.this$0).post(com.navdy.hud.app.device.dial.DialManager.access$1300());
        com.navdy.hud.app.device.dial.DialManager.access$400(this.this$1.this$0).post((Runnable)new com.navdy.hud.app.device.dial.DialManager$8$1$1(this));
    }
}
