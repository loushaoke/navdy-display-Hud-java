package com.navdy.hud.app.device.dial;

abstract class DialManager$CharacteristicCommandProcessor$Command {
    android.bluetooth.BluetoothGattCharacteristic characteristic;
    
    public DialManager$CharacteristicCommandProcessor$Command(android.bluetooth.BluetoothGattCharacteristic a) {
        this.characteristic = a;
    }
    
    abstract public void process(android.bluetooth.BluetoothGatt arg);
}
