package com.navdy.hud.app.device.light;

class LED$1 implements Runnable {
    final com.navdy.hud.app.device.light.LED this$0;
    final com.navdy.hud.app.device.light.LightSettings val$settings;
    
    LED$1(com.navdy.hud.app.device.light.LED a, com.navdy.hud.app.device.light.LightSettings a0) {
        super();
        this.this$0 = a;
        this.val$settings = a0;
    }
    
    public void run() {
        if (this.val$settings instanceof com.navdy.hud.app.device.light.LED$Settings) {
            com.navdy.hud.app.device.light.LED$Settings a = (com.navdy.hud.app.device.light.LED$Settings)this.val$settings;
            if (a.getActivityBlink()) {
                com.navdy.hud.app.device.light.LED.access$100(this.this$0);
            } else if (a.isTurnedOn()) {
                com.navdy.hud.app.device.light.LED.access$200(this.this$0, a.getColor());
                if (a.isBlinking()) {
                    if (a.isBlinkInfinite()) {
                        com.navdy.hud.app.device.light.LED.access$300(this.this$0, a.getBlinkFrequency());
                        com.navdy.hud.app.device.light.LED.access$400(this.this$0);
                    } else {
                        com.navdy.hud.app.device.light.LED.access$500(this.this$0, a.getBlinkPulseCount(), a.getColor(), a.getBlinkFrequency());
                    }
                } else {
                    com.navdy.hud.app.device.light.LED.access$600(this.this$0);
                }
            } else {
                this.this$0.turnOff();
            }
        }
    }
}
