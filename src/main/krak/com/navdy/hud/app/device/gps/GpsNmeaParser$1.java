package com.navdy.hud.app.device.gps;

class GpsNmeaParser$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage;
    
    static {
        $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage = new int[com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage;
        com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage a0 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.GGA;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.GLL.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.GNS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.RMC.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.VTG.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.GSV.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
    }
}
