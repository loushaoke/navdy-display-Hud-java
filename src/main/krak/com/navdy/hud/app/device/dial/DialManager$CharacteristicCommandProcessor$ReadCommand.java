package com.navdy.hud.app.device.dial;

class DialManager$CharacteristicCommandProcessor$ReadCommand extends com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command {
    public DialManager$CharacteristicCommandProcessor$ReadCommand(android.bluetooth.BluetoothGattCharacteristic a) {
        super(a);
    }
    
    public void process(android.bluetooth.BluetoothGatt a) {
        if (a.readCharacteristic(this.characteristic)) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.d(new StringBuilder().append("Processing GATT read succeeded ").append(this.characteristic.getUuid().toString()).toString());
        } else {
            com.navdy.hud.app.device.dial.DialManager.sLogger.d(new StringBuilder().append("Processing GATT read failed ").append(this.characteristic.getUuid().toString()).toString());
        }
    }
}
