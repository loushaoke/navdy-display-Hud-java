package com.navdy.hud.app.device.dial;

class DialManager$18 implements com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    final com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten val$callback;
    final int val$delay;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialManager$18(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0, int i, com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten a1) {
        super();
        this.this$0 = a;
        this.val$device = a0;
        this.val$delay = i;
        this.val$callback = a1;
    }
    
    public void onAttemptConnection(boolean b) {
    }
    
    public void onAttemptDisconnection(boolean b) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("attempt dis-connection [").append(this.val$device.getName()).append(" ] success[").append(b).append("] ").toString());
        if (this.val$delay > 0) {
            com.navdy.hud.app.util.GenericUtil.sleep(this.val$delay);
        }
        com.navdy.hud.app.device.dial.DialManager.access$4500(this.this$0, this.val$device);
        if (this.val$callback != null) {
            this.val$callback.onForgotten();
        }
    }
}
