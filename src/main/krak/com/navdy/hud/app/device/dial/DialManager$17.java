package com.navdy.hud.app.device.dial;

class DialManager$17 implements com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialManager$17(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        super();
        this.this$0 = a;
        this.val$device = a0;
    }
    
    public void onForgotten() {
        synchronized(com.navdy.hud.app.device.dial.DialManager.access$4400(this.this$0)) {
            Object a0 = com.navdy.hud.app.device.dial.DialManager.access$4400(this.this$0).iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                {
                    if (!android.text.TextUtils.equals((CharSequence)((android.bluetooth.BluetoothDevice)((java.util.Iterator)a0).next()).getName(), (CharSequence)this.val$device.getName())) {
                        continue;
                    }
                    ((java.util.Iterator)a0).remove();
                    break;
                }
            }
            /*monexit(a)*/;
        }
        com.navdy.hud.app.device.dial.DialManager.access$902(this.this$0, 0);
        com.navdy.hud.app.device.dial.DialManager.access$100().dialName = this.val$device.getName();
        com.navdy.hud.app.device.dial.DialManager.access$200(this.this$0).post(com.navdy.hud.app.device.dial.DialManager.access$100());
    }
}
