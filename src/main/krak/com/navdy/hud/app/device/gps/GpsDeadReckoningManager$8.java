package com.navdy.hud.app.device.gps;

class GpsDeadReckoningManager$8 implements com.navdy.hud.app.device.gps.GpsDeadReckoningManager$CommandWriter {
    final com.navdy.hud.app.device.gps.GpsDeadReckoningManager this$0;
    final String val$description;
    final byte[] val$message;
    
    GpsDeadReckoningManager$8(com.navdy.hud.app.device.gps.GpsDeadReckoningManager a, String s, byte[] a0) {
        super();
        this.this$0 = a;
        this.val$description = s;
        this.val$message = a0;
    }
    
    public void run() {
        if (com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$1900(this.this$0) == null) {
            throw new java.io.IOException(new StringBuilder().append("Disconnected socket - failed to ").append(this.val$description).toString());
        }
        if (this.val$description != null) {
            com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$1000().v(new StringBuilder().append(this.val$description).append(" [").append(com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$2000(this.val$message, 0, this.val$message.length)).append("]").toString());
        }
        com.navdy.hud.app.device.gps.GpsDeadReckoningManager.access$1900(this.this$0).write(this.val$message);
    }
}
