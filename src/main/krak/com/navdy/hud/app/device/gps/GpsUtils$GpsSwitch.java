package com.navdy.hud.app.device.gps;

public class GpsUtils$GpsSwitch {
    public boolean usingPhone;
    public boolean usingUblox;
    
    public GpsUtils$GpsSwitch(boolean b, boolean b0) {
        this.usingPhone = b;
        this.usingUblox = b0;
    }
    
    public String toString() {
        return new StringBuilder().append("GpsSwitch [UsingPhone :").append(this.usingPhone).append(", UsingUblox : ").append(this.usingUblox).append("]").toString();
    }
}
