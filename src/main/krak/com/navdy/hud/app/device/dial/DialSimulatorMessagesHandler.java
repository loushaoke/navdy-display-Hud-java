package com.navdy.hud.app.device.dial;

public class DialSimulatorMessagesHandler {
    final private static int LONG_PRESS_KEY_UP_FLAGS_SIMULATION = 640;
    final private static java.util.Map SIMULATION_TO_KEY_EVENT;
    private int longPressTimeThreshold;
    
    static {
        SIMULATION_TO_KEY_EVENT = (java.util.Map)new com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler$1();
    }
    
    public DialSimulatorMessagesHandler(android.content.Context a) {
        android.view.ViewConfiguration.get(a);
        this.longPressTimeThreshold = android.view.ViewConfiguration.getLongPressTimeout();
    }
    
    static int access$000(com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler a) {
        return a.longPressTimeThreshold;
    }
    
    static java.util.Map access$100() {
        return SIMULATION_TO_KEY_EVENT;
    }
    
    public void onDialSimulationEvent(com.navdy.service.library.events.input.DialSimulationEvent a) {
        if (a != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler$SimulatedKeyEventRunnable(this, a.dialAction), 1);
        }
    }
}
