package com.navdy.hud.app.device.gps;

public class GpsManager {
    final private static int ACCEPTABLE_THRESHOLD = 2;
    final private static String DISCARD_TIME = "DISCARD_TIME";
    final private static String GPS_SYSTEM_PROPERTY = "persist.sys.hud_gps";
    final public static long INACCURATE_GPS_REPORT_INTERVAL;
    final private static int INITIAL_INTERVAL = 15000;
    final private static int KEEP_PHONE_GPS_ON = -1;
    final private static int LOCATION_ACCURACY_THROW_AWAY_THRESHOLD = 30;
    final private static int LOCATION_TIME_THROW_AWAY_THRESHOLD = 2000;
    final public static double MAXIMUM_STOPPED_SPEED = 0.22353333333333333;
    final private static double METERS_PER_MILE = 1609.44;
    final public static long MINIMUM_DRIVE_TIME = 3000L;
    final public static double MINIMUM_DRIVING_SPEED = 2.2353333333333336;
    final static int MSG_NMEA = 1;
    final static int MSG_SATELLITE_STATUS = 2;
    final private static int SECONDS_PER_HOUR = 3600;
    final private static String SET_LOCATION_DISCARD_TIME_THRESHOLD = "SET_LOCATION_DISCARD_TIME_THRESHOLD";
    final private static String SET_UBLOX_ACCURACY = "SET_UBLOX_ACCURACY";
    final private static com.navdy.service.library.events.location.TransmitLocation START_SENDING_LOCATION;
    final private static String START_UBLOX = "START_REPORTING_UBLOX_LOCATION";
    final private static com.navdy.service.library.events.location.TransmitLocation STOP_SENDING_LOCATION;
    final private static String STOP_UBLOX = "STOP_REPORTING_UBLOX_LOCATION";
    final private static String TAG_GPS = "[Gps-i] ";
    final private static String TAG_GPS_LOC = "[Gps-loc] ";
    final private static String TAG_GPS_LOC_NAVDY = "[Gps-loc-navdy] ";
    final private static String TAG_GPS_NMEA = "[Gps-nmea] ";
    final private static String TAG_GPS_STATUS = "[Gps-stat] ";
    final private static String TAG_PHONE_LOC = "[Phone-loc] ";
    final private static String UBLOX_ACCURACY = "UBLOX_ACCURACY";
    final private static float UBLOX_MIN_ACCEPTABLE_THRESHOLD = 5f;
    final private static long WARM_RESET_INTERVAL;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.device.gps.GpsManager singleton;
    private com.navdy.hud.app.device.gps.GpsManager$LocationSource activeLocationSource;
    private com.navdy.hud.app.service.HudConnectionService connectionService;
    private boolean driving;
    private android.content.BroadcastReceiver eventReceiver;
    private volatile boolean extrapolationOn;
    private volatile long extrapolationStartTime;
    private boolean firstSwitchToUbloxFromPhone;
    private float gpsAccuracy;
    private long gpsLastEventTime;
    private long gpsManagerStartTime;
    private android.os.Handler handler;
    private long inaccurateGpsCount;
    private boolean isDebugTTSEnabled;
    private boolean keepPhoneGpsOn;
    private long lastInaccurateGpsTime;
    private com.navdy.service.library.events.location.Coordinate lastPhoneCoordinate;
    private long lastPhoneCoordinateTime;
    private android.location.Location lastUbloxCoordinate;
    private long lastUbloxCoordinateTime;
    private Runnable locationFixRunnable;
    private android.location.LocationManager locationManager;
    private android.location.LocationListener navdyGpsLocationListener;
    private android.os.Handler$Callback nmeaCallback;
    private android.os.Handler nmeaHandler;
    private android.os.HandlerThread nmeaHandlerThread;
    private com.navdy.hud.app.device.gps.GpsNmeaParser nmeaParser;
    private Runnable noLocationUpdatesRunnable;
    private Runnable noPhoneLocationFixRunnable;
    private java.util.ArrayList queue;
    private Runnable queueCallback;
    private volatile boolean startUbloxCalled;
    private boolean switchBetweenPhoneUbloxAllowed;
    private long transitionStartTime;
    private boolean transitioning;
    private android.location.LocationListener ubloxGpsLocationListener;
    private android.location.GpsStatus$NmeaListener ubloxGpsNmeaListener;
    private android.location.GpsStatus$Listener ubloxGpsStatusListener;
    private boolean warmResetCancelled;
    private Runnable warmResetRunnable;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.gps.GpsManager.class);
        INACCURATE_GPS_REPORT_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
        WARM_RESET_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(160L);
        START_SENDING_LOCATION = new com.navdy.service.library.events.location.TransmitLocation(Boolean.valueOf(true));
        STOP_SENDING_LOCATION = new com.navdy.service.library.events.location.TransmitLocation(Boolean.valueOf(false));
        singleton = new com.navdy.hud.app.device.gps.GpsManager();
    }
    
    private GpsManager() {
        this.queue = new java.util.ArrayList();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.ubloxGpsNmeaListener = (android.location.GpsStatus$NmeaListener)new com.navdy.hud.app.device.gps.GpsManager$1(this);
        this.ubloxGpsLocationListener = (android.location.LocationListener)new com.navdy.hud.app.device.gps.GpsManager$2(this);
        this.navdyGpsLocationListener = (android.location.LocationListener)new com.navdy.hud.app.device.gps.GpsManager$3(this);
        this.ubloxGpsStatusListener = (android.location.GpsStatus$Listener)new com.navdy.hud.app.device.gps.GpsManager$4(this);
        this.noPhoneLocationFixRunnable = (Runnable)new com.navdy.hud.app.device.gps.GpsManager$5(this);
        this.noLocationUpdatesRunnable = (Runnable)new com.navdy.hud.app.device.gps.GpsManager$6(this);
        this.warmResetRunnable = (Runnable)new com.navdy.hud.app.device.gps.GpsManager$7(this);
        this.locationFixRunnable = (Runnable)new com.navdy.hud.app.device.gps.GpsManager$8(this);
        this.nmeaCallback = (android.os.Handler$Callback)new com.navdy.hud.app.device.gps.GpsManager$9(this);
        this.queueCallback = (Runnable)new com.navdy.hud.app.device.gps.GpsManager$10(this);
        this.eventReceiver = new com.navdy.hud.app.device.gps.GpsManager$11(this);
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        try {
            int i = com.navdy.hud.app.util.os.SystemProperties.getInt("persist.sys.hud_gps", 0);
            if (i == -1) {
                this.keepPhoneGpsOn = true;
                this.switchBetweenPhoneUbloxAllowed = true;
                sLogger.v("switch between phone and ublox allowed");
            }
            sLogger.v(new StringBuilder().append("keepPhoneGpsOn[").append(this.keepPhoneGpsOn).append("] val[").append(i).append("]").toString());
            this.isDebugTTSEnabled = new java.io.File("/sdcard/debug_tts").exists();
            this.setUbloxAccuracyThreshold();
            this.setUbloxTimeDiscardThreshold();
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
        this.locationManager = (android.location.LocationManager)a.getSystemService("location");
        boolean b = this.locationManager.isProviderEnabled("gps");
        android.location.LocationProvider a1 = null;
        if (b) {
            a1 = this.locationManager.getProvider("gps");
            if (a1 != null) {
                this.nmeaHandlerThread = new android.os.HandlerThread("navdynmea");
                this.nmeaHandlerThread.start();
                this.nmeaHandler = new android.os.Handler(this.nmeaHandlerThread.getLooper(), this.nmeaCallback);
                this.nmeaParser = new com.navdy.hud.app.device.gps.GpsNmeaParser(sLogger, this.nmeaHandler);
                this.locationManager.addGpsStatusListener(this.ubloxGpsStatusListener);
                this.locationManager.addNmeaListener(this.ubloxGpsNmeaListener);
            }
        }
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            sLogger.i("[Gps-i] setting up ublox gps");
            if (a1 == null) {
                sLogger.e("[Gps-i] gps provider not found");
            } else {
                this.locationManager.requestLocationUpdates("gps", 0L, 0.0f, this.ubloxGpsLocationListener);
                try {
                    this.locationManager.requestLocationUpdates("NAVDY_GPS_PROVIDER", 0L, 0.0f, this.navdyGpsLocationListener);
                    sLogger.v("requestLocationUpdates successful for NAVDY_GPS_PROVIDER");
                } catch(Throwable a2) {
                    sLogger.e("requestLocationUpdates", a2);
                }
                sLogger.i("[Gps-i] ublox gps listeners installed");
            }
        } else {
            sLogger.i("[Gps-i] not a Hud device,not setting up ublox gps");
        }
        this.locationManager.addTestProvider("network", false, true, false, false, true, true, true, 0, 3);
        this.locationManager.setTestProviderEnabled("network", true);
        this.locationManager.setTestProviderStatus("network", 2, (android.os.Bundle)null, System.currentTimeMillis());
        sLogger.i("[Gps-i] added mock network provider");
        this.gpsManagerStartTime = android.os.SystemClock.elapsedRealtime();
        this.gpsLastEventTime = this.gpsManagerStartTime;
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsAttemptAcquireLocation();
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            this.setUbloxResetRunnable();
        }
        android.content.IntentFilter a3 = new android.content.IntentFilter();
        a3.addAction("GPS_DR_STARTED");
        a3.addAction("GPS_DR_STOPPED");
        a3.addAction("EXTRAPOLATION");
        a.registerReceiver(this.eventReceiver, a3);
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static void access$100(com.navdy.hud.app.device.gps.GpsManager a, String s) {
        a.processNmea(s);
    }
    
    static com.navdy.service.library.events.location.Coordinate access$1000(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.lastPhoneCoordinate;
    }
    
    static android.location.Location access$1100(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.lastUbloxCoordinate;
    }
    
    static android.location.Location access$1102(com.navdy.hud.app.device.gps.GpsManager a, android.location.Location a0) {
        a.lastUbloxCoordinate = a0;
        return a0;
    }
    
    static void access$1200(com.navdy.hud.app.device.gps.GpsManager a, String s, String s0, boolean b, boolean b0) {
        a.markLocationFix(s, s0, b, b0);
    }
    
    static long access$1300(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.lastUbloxCoordinateTime;
    }
    
    static long access$1302(com.navdy.hud.app.device.gps.GpsManager a, long j) {
        a.lastUbloxCoordinateTime = j;
        return j;
    }
    
    static boolean access$1400(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.warmResetCancelled;
    }
    
    static void access$1500(com.navdy.hud.app.device.gps.GpsManager a) {
        a.cancelUbloxResetRunnable();
    }
    
    static void access$1600(com.navdy.hud.app.device.gps.GpsManager a, android.location.Location a0) {
        a.updateDrivingState(a0);
    }
    
    static com.navdy.hud.app.service.HudConnectionService access$1700(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.connectionService;
    }
    
    static boolean access$1800(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.firstSwitchToUbloxFromPhone;
    }
    
    static boolean access$1900(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.switchBetweenPhoneUbloxAllowed;
    }
    
    static boolean access$200(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.startUbloxCalled;
    }
    
    static void access$2000(com.navdy.hud.app.device.gps.GpsManager a, android.location.Location a0) {
        a.checkGpsToPhoneAccuracy(a0);
    }
    
    static void access$2100(com.navdy.hud.app.device.gps.GpsManager a) {
        a.startSendingLocation();
    }
    
    static long access$2200() {
        return WARM_RESET_INTERVAL;
    }
    
    static void access$2300(com.navdy.hud.app.device.gps.GpsManager a, String s, android.os.Bundle a0) {
        a.sendGpsEventBroadcast(s, a0);
    }
    
    static boolean access$2400(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.extrapolationOn;
    }
    
    static boolean access$2402(com.navdy.hud.app.device.gps.GpsManager a, boolean b) {
        a.extrapolationOn = b;
        return b;
    }
    
    static long access$2500(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.extrapolationStartTime;
    }
    
    static long access$2502(com.navdy.hud.app.device.gps.GpsManager a, long j) {
        a.extrapolationStartTime = j;
        return j;
    }
    
    static com.navdy.hud.app.device.gps.GpsNmeaParser access$2600(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.nmeaParser;
    }
    
    static long access$2700(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.gpsManagerStartTime;
    }
    
    static java.util.ArrayList access$2800(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.queue;
    }
    
    static com.navdy.hud.app.device.gps.GpsManager$LocationSource access$300(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.activeLocationSource;
    }
    
    static com.navdy.hud.app.device.gps.GpsManager$LocationSource access$302(com.navdy.hud.app.device.gps.GpsManager a, com.navdy.hud.app.device.gps.GpsManager$LocationSource a0) {
        a.activeLocationSource = a0;
        return a0;
    }
    
    static Runnable access$400(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.noPhoneLocationFixRunnable;
    }
    
    static android.os.Handler access$500(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.handler;
    }
    
    static Runnable access$600(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.locationFixRunnable;
    }
    
    static void access$700(com.navdy.hud.app.device.gps.GpsManager a) {
        a.stopSendingLocation();
    }
    
    static long access$800(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.gpsLastEventTime;
    }
    
    static long access$802(com.navdy.hud.app.device.gps.GpsManager a, long j) {
        a.gpsLastEventTime = j;
        return j;
    }
    
    static long access$900(com.navdy.hud.app.device.gps.GpsManager a) {
        return a.lastPhoneCoordinateTime;
    }
    
    private static android.location.Location androidLocationFromCoordinate(com.navdy.service.library.events.location.Coordinate a) {
        android.location.Location a0 = new android.location.Location("network");
        a0.setLatitude(a.latitude.doubleValue());
        a0.setLongitude(a.longitude.doubleValue());
        a0.setAccuracy(a.accuracy.floatValue());
        a0.setAltitude(a.altitude.doubleValue());
        a0.setBearing(a.bearing.floatValue());
        a0.setSpeed(a.speed.floatValue());
        a0.setTime(System.currentTimeMillis());
        a0.setElapsedRealtimeNanos(android.os.SystemClock.elapsedRealtimeNanos());
        return a0;
    }
    
    private void cancelUbloxResetRunnable() {
        this.handler.removeCallbacks(this.warmResetRunnable);
        this.warmResetCancelled = true;
    }
    
    private void checkGpsToPhoneAccuracy(android.location.Location a) {
        float f = a.getAccuracy();
        long j = android.os.SystemClock.elapsedRealtime();
        long j0 = this.lastPhoneCoordinateTime;
        if (sLogger.isLoggable(3)) {
            com.navdy.service.library.log.Logger a0 = sLogger;
            StringBuilder a1 = new StringBuilder().append("[Gps-loc-navdy] checkGpsToPhoneAccuracy: ublox[").append(a.getAccuracy()).append("] phone[");
            Object a2 = (this.lastPhoneCoordinate == null) ? "n/a" : this.lastPhoneCoordinate.accuracy;
            StringBuilder a3 = a1.append(a2).append("] lastPhoneTime [");
            Object a4 = (this.lastPhoneCoordinate == null) ? "n/a" : Long.valueOf(j - j0);
            a0.v(a3.append(a4).append("]").toString());
        }
        if (f != 0.0f) {
            if (this.activeLocationSource != com.navdy.hud.app.device.gps.GpsManager$LocationSource.UBLOX) {
                if (f + 2f < this.lastPhoneCoordinate.accuracy.floatValue()) {
                    sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] < phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("], start uBloxReporting, switch complete").toString());
                    this.firstSwitchToUbloxFromPhone = true;
                    this.markLocationFix("Switched to Ublox Gps", new StringBuilder().append("Phone =").append(this.lastPhoneCoordinate.accuracy).append(" ublox =").append(f).toString(), false, false);
                    this.startUbloxReporting();
                } else if (sLogger.isLoggable(3)) {
                    sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] > phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("] no action").toString());
                }
            } else if (this.lastPhoneCoordinate != null && android.os.SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime <= 3000L) {
                if (this.lastPhoneCoordinate.accuracy.floatValue() + 2f < f) {
                    if (sLogger.isLoggable(3)) {
                        sLogger.v(new StringBuilder().append("[Gps-loc-navdy]  ublox accuracy [").append(f).append("] > phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("]").toString());
                    }
                    if (this.startUbloxCalled) {
                        if (f <= 5f) {
                            sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] > phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("], but not above threshold").toString());
                            this.stopSendingLocation();
                        } else {
                            sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] > phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("], stop uBloxReporting").toString());
                            this.handler.removeCallbacks(this.locationFixRunnable);
                            this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                            this.stopUbloxReporting();
                            sLogger.v(new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(this.activeLocationSource).append("] to [").append(com.navdy.hud.app.device.gps.GpsManager$LocationSource.PHONE).append("]").toString());
                            this.activeLocationSource = com.navdy.hud.app.device.gps.GpsManager$LocationSource.PHONE;
                            this.markLocationFix("Switched to Phone Gps", new StringBuilder().append("Phone =").append(this.lastPhoneCoordinate.accuracy).append(" ublox =").append(f).toString(), true, false);
                        }
                    }
                } else if (sLogger.isLoggable(3)) {
                    sLogger.v(new StringBuilder().append("ublox accuracy [").append(f).append("] < phone accuracy[").append(this.lastPhoneCoordinate.accuracy).append("] no action").toString());
                }
            }
        }
    }
    
    private void feedLocationToProvider(android.location.Location a, com.navdy.service.library.events.location.Coordinate a0) {
        try {
            if (sLogger.isLoggable(3)) {
                sLogger.d(new StringBuilder().append("[Phone-loc] ").append(a0).toString());
            }
            if (a == null) {
                a = com.navdy.hud.app.device.gps.GpsManager.androidLocationFromCoordinate(a0);
            }
            this.updateDrivingState(a);
            this.locationManager.setTestProviderLocation(a.getProvider(), a);
        } catch(Throwable a1) {
            sLogger.e("feedLocation", a1);
        }
    }
    
    public static com.navdy.hud.app.device.gps.GpsManager getInstance() {
        return singleton;
    }
    
    private void markLocationFix(String s, String s0, boolean b, boolean b0) {
        android.os.Bundle a = new android.os.Bundle();
        a.putString("title", s);
        a.putString("info", s0);
        a.putBoolean("phone", b);
        a.putBoolean("ublox", b0);
        this.sendGpsEventBroadcast("GPS_Switch", a);
    }
    
    private void processNmea(String s) {
        android.os.Message a = this.nmeaHandler.obtainMessage(1);
        a.obj = s;
        this.nmeaHandler.sendMessage(a);
    }
    
    private void sendGpsEventBroadcast(String s, android.os.Bundle a) {
        try {
            android.content.Intent a0 = new android.content.Intent(s);
            if (a != null) {
                a0.putExtras(a);
            }
            if (android.os.SystemClock.elapsedRealtime() - this.gpsManagerStartTime > 15000L) {
                android.content.Context a1 = com.navdy.hud.app.HudApplication.getAppContext();
                android.os.UserHandle a2 = android.os.Process.myUserHandle();
                if (this.queue.size() > 0) {
                    this.queueCallback.run();
                }
                a1.sendBroadcastAsUser(a0, a2);
            } else {
                this.queue.add(a0);
                this.handler.removeCallbacks(this.queueCallback);
                this.handler.postDelayed(this.queueCallback, 15000L);
            }
        } catch(Throwable a3) {
            sLogger.e(a3);
        }
    }
    
    private void sendSpeechRequest(com.navdy.service.library.events.audio.SpeechRequest a) {
        com.navdy.hud.app.service.HudConnectionService a0 = this.connectionService;
        label0: {
            Throwable a1 = null;
            if (a0 == null) {
                break label0;
            }
            if (!this.connectionService.isConnected()) {
                break label0;
            }
            try {
                sLogger.i("[Gps-loc] send speech request");
                this.connectionService.sendMessage((com.squareup.wire.Message)a);
                break label0;
            } catch(Throwable a2) {
                a1 = a2;
            }
            sLogger.e(a1);
        }
    }
    
    private void setDriving(boolean b) {
        if (this.driving != b) {
            this.driving = b;
            this.sendGpsEventBroadcast(b ? "driving_started" : "driving_stopped", (android.os.Bundle)null);
        }
    }
    
    private void setUbloxAccuracyThreshold() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            android.content.Intent a1 = new android.content.Intent("SET_UBLOX_ACCURACY");
            a1.putExtra("UBLOX_ACCURACY", 30);
            a.sendBroadcastAsUser(a1, a0);
            sLogger.v("setUbloxAccuracyThreshold:30");
        } catch(Throwable a2) {
            sLogger.e("setUbloxAccuracyThreshold", a2);
        }
    }
    
    private void setUbloxResetRunnable() {
        sLogger.v("setUbloxResetRunnable");
        this.handler.postDelayed(this.warmResetRunnable, WARM_RESET_INTERVAL);
    }
    
    private void setUbloxTimeDiscardThreshold() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            android.content.Intent a1 = new android.content.Intent("SET_LOCATION_DISCARD_TIME_THRESHOLD");
            a1.putExtra("DISCARD_TIME", 2000);
            a.sendBroadcastAsUser(a1, a0);
            sLogger.v("setUbloxTimeDiscardThreshold:2000");
        } catch(Throwable a2) {
            sLogger.e("setUbloxTimeDiscardThreshold", a2);
        }
    }
    
    private void startSendingLocation() {
        com.navdy.hud.app.service.HudConnectionService a = this.connectionService;
        label0: {
            Throwable a0 = null;
            if (a == null) {
                break label0;
            }
            if (!this.connectionService.isConnected()) {
                break label0;
            }
            try {
                sLogger.i("[Gps-loc] phone start sending location");
                this.connectionService.sendMessage((com.squareup.wire.Message)START_SENDING_LOCATION);
                break label0;
            } catch(Throwable a1) {
                a0 = a1;
            }
            sLogger.e(a0);
        }
    }
    
    private void stopSendingLocation() {
        boolean b = this.keepPhoneGpsOn;
        label1: {
            Throwable a = null;
            label0: if (b) {
                sLogger.v("stopSendingLocation: keeping phone gps on");
                break label1;
            } else if (this.isDebugTTSEnabled) {
                sLogger.v("stopSendingLocation: not stopping, debug_tts enabled");
                break label1;
            } else {
                if (this.connectionService == null) {
                    break label1;
                }
                if (!this.connectionService.isConnected()) {
                    break label1;
                }
                {
                    try {
                        sLogger.i("[Gps-loc] phone stop sending location");
                        this.connectionService.sendMessage((com.squareup.wire.Message)STOP_SENDING_LOCATION);
                    } catch(Throwable a0) {
                        a = a0;
                        break label0;
                    }
                    break label1;
                }
            }
            sLogger.e(a);
        }
    }
    
    private void updateDrivingState(android.location.Location a) {
        label2: if (this.driving) {
            label3: {
                if (a == null) {
                    break label3;
                }
                if ((double)a.getSpeed() < 0.22353333333333333) {
                    break label3;
                }
                this.transitioning = false;
                break label2;
            }
            if (this.transitioning) {
                if (android.os.SystemClock.elapsedRealtime() - this.transitionStartTime > 3000L) {
                    this.setDriving(false);
                    this.transitioning = false;
                }
            } else {
                this.transitioning = true;
                this.transitionStartTime = android.os.SystemClock.elapsedRealtime();
            }
        } else {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if ((double)a.getSpeed() > 2.2353333333333336) {
                        break label0;
                    }
                }
                this.transitioning = false;
                break label2;
            }
            if (this.transitioning) {
                if (android.os.SystemClock.elapsedRealtime() - this.transitionStartTime > 3000L) {
                    this.setDriving(true);
                    this.transitioning = false;
                }
            } else {
                this.transitioning = true;
                this.transitionStartTime = android.os.SystemClock.elapsedRealtime();
            }
        }
        this.handler.removeCallbacks(this.noLocationUpdatesRunnable);
        if (this.driving) {
            this.handler.postDelayed(this.noLocationUpdatesRunnable, 3000L);
        }
    }
    
    public void feedLocation(com.navdy.service.library.events.location.Coordinate a) {
        boolean b = false;
        long j = this.lastPhoneCoordinateTime;
        int i = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
        label7: {
            label5: {
                label6: {
                    if (i > 0) {
                        break label6;
                    }
                    if (this.lastUbloxCoordinateTime <= 0L) {
                        break label5;
                    }
                }
                b = true;
                break label7;
            }
            b = false;
        }
        long j0 = System.currentTimeMillis();
        float f = a.accuracy.floatValue();
        int i0 = (f > 30f) ? 1 : (f == 30f) ? 0 : -1;
        label1: {
            label4: {
                if (i0 < 0) {
                    break label4;
                }
                if (!b) {
                    break label4;
                }
                this.inaccurateGpsCount = this.inaccurateGpsCount + 1L;
                this.gpsAccuracy = this.gpsAccuracy + a.accuracy.floatValue();
                long j1 = this.lastInaccurateGpsTime;
                int i1 = (j1 < 0L) ? -1 : (j1 == 0L) ? 0 : 1;
                label3: {
                    if (i1 == 0) {
                        break label3;
                    }
                    if (j0 - this.lastInaccurateGpsTime < INACCURATE_GPS_REPORT_INTERVAL) {
                        break label1;
                    }
                }
                float f0 = this.gpsAccuracy / (float)this.inaccurateGpsCount;
                sLogger.e(new StringBuilder().append("BAD gps accuracy (discarding) avg:").append(f0).append(", count=").append(this.inaccurateGpsCount).append(", last coord: ").append(a).toString());
                this.inaccurateGpsCount = 0L;
                this.gpsAccuracy = 0.0f;
                this.lastInaccurateGpsTime = j0;
                break label1;
            }
            long j2 = j0 - a.timestamp.longValue();
            int i2 = (j2 < 2000L) ? -1 : (j2 == 2000L) ? 0 : 1;
            label2: {
                if (i2 < 0) {
                    break label2;
                }
                if (!b) {
                    break label2;
                }
                sLogger.e(new StringBuilder().append("OLD location from phone(discard) time:").append(j2).append(", timestamp:").append(a.timestamp).append(" now:").append(j0).append(" lat:").append(a.latitude).append(" lng:").append(a.longitude).toString());
                break label1;
            }
            com.navdy.service.library.events.location.Coordinate a0 = this.lastPhoneCoordinate;
            label0: {
                if (a0 == null) {
                    break label0;
                }
                if (this.connectionService.getDevicePlatform() != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.latitude != a.latitude) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.longitude != a.longitude) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.accuracy != a.accuracy) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.altitude != a.altitude) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.bearing != a.bearing) {
                    break label0;
                }
                if (this.lastPhoneCoordinate.speed != a.speed) {
                    break label0;
                }
                sLogger.e(new StringBuilder().append("same location(discard) diff=").append(a.timestamp.longValue() - this.lastPhoneCoordinate.timestamp.longValue()).toString());
                break label1;
            }
            this.lastPhoneCoordinate = a;
            this.lastPhoneCoordinateTime = android.os.SystemClock.elapsedRealtime();
            this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
            com.navdy.hud.app.debug.RouteRecorder a1 = com.navdy.hud.app.debug.RouteRecorder.getInstance();
            boolean b0 = a1.isRecording();
            android.location.Location a2 = null;
            if (b0) {
                a2 = com.navdy.hud.app.device.gps.GpsManager.androidLocationFromCoordinate(a);
                a1.injectLocation(a2, true);
            }
            if (this.activeLocationSource != null) {
                if (this.activeLocationSource != com.navdy.hud.app.device.gps.GpsManager$LocationSource.PHONE) {
                    if (android.os.SystemClock.elapsedRealtime() - this.lastUbloxCoordinateTime > 3000L) {
                        sLogger.v(new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(this.activeLocationSource).append("] to [").append(com.navdy.hud.app.device.gps.GpsManager$LocationSource.PHONE).append("]").toString());
                        this.activeLocationSource = com.navdy.hud.app.device.gps.GpsManager$LocationSource.PHONE;
                        this.stopUbloxReporting();
                        this.feedLocationToProvider(a2, a);
                        this.markLocationFix("Switched to Phone Gps", new StringBuilder().append("Phone =").append(this.lastPhoneCoordinate.accuracy).append(" ublox =lost").toString(), true, false);
                    }
                } else {
                    this.feedLocationToProvider(a2, a);
                }
            } else {
                sLogger.v(new StringBuilder().append("[Gps-loc] [LocationProvider] switched from [").append(this.activeLocationSource).append("] to [").append(com.navdy.hud.app.device.gps.GpsManager$LocationSource.PHONE).append("]").toString());
                this.stopUbloxReporting();
                this.activeLocationSource = com.navdy.hud.app.device.gps.GpsManager$LocationSource.PHONE;
                this.feedLocationToProvider(a2, a);
                this.markLocationFix("Switched to Phone Gps", new StringBuilder().append("Phone =").append(this.lastPhoneCoordinate.accuracy).append(" ublox=n/a").toString(), true, false);
            }
        }
    }
    
    public void setConnectionService(com.navdy.hud.app.service.HudConnectionService a) {
        this.connectionService = a;
    }
    
    public void shutdown() {
        if (this.locationManager != null) {
            this.locationManager.removeUpdates(this.ubloxGpsLocationListener);
            this.locationManager.removeUpdates(this.navdyGpsLocationListener);
            this.locationManager.removeGpsStatusListener(this.ubloxGpsStatusListener);
            this.locationManager.removeNmeaListener(this.ubloxGpsNmeaListener);
        }
    }
    
    public void startUbloxReporting() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            a.sendBroadcastAsUser(new android.content.Intent("START_REPORTING_UBLOX_LOCATION"), a0);
            this.startUbloxCalled = true;
            sLogger.v("started ublox reporting");
        } catch(Throwable a1) {
            sLogger.e("startUbloxReporting", a1);
        }
    }
    
    public void stopUbloxReporting() {
        try {
            this.startUbloxCalled = false;
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.os.UserHandle a0 = android.os.Process.myUserHandle();
            a.sendBroadcastAsUser(new android.content.Intent("STOP_REPORTING_UBLOX_LOCATION"), a0);
            sLogger.v("stopped ublox reporting");
        } catch(Throwable a1) {
            sLogger.e("stopUbloxReporting", a1);
        }
    }
}
