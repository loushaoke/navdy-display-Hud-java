package com.navdy.hud.app.device;

class PowerManager$2 implements Runnable {
    final com.navdy.hud.app.device.PowerManager this$0;
    
    PowerManager$2(com.navdy.hud.app.device.PowerManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.device.PowerManager.access$400().i("Quiet mode has timed out - forcing full shutdown");
        com.navdy.hud.app.device.PowerManager.access$500(this.this$0).post(new com.navdy.hud.app.event.Shutdown(com.navdy.hud.app.event.Shutdown$Reason.TIMEOUT));
    }
}
