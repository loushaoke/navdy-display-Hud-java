package com.navdy.hud.app.device.dial;

class DialManager$16 implements com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnectionStatus {
    final com.navdy.hud.app.device.dial.DialManager this$0;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialManager$16(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        super();
        this.this$0 = a;
        this.val$device = a0;
    }
    
    public void onStatus(boolean b) {
        if (b) {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("checkDialConnections: dial ").append(this.val$device.getName()).append(" connected").toString());
            if (com.navdy.hud.app.device.dial.DialManager.access$300(this.this$0) == null) {
                com.navdy.hud.app.device.dial.DialManager.access$302(this.this$0, this.val$device);
                com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("marking dial [").append(this.val$device.getName()).append("] as connected").toString());
                com.navdy.hud.app.device.dial.DialManager.access$1500(this.this$0);
            }
        } else {
            com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("checkDialConnections: dial ").append(this.val$device.getName()).append(" not connected").toString());
        }
    }
}
