package com.navdy.hud.app.device.dial;

final class DialSimulatorMessagesHandler$1 extends java.util.HashMap {
    DialSimulatorMessagesHandler$1() {
        this.put(com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction.DIAL_LEFT_TURN, Integer.valueOf(21));
        this.put(com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction.DIAL_RIGHT_TURN, Integer.valueOf(22));
        this.put(com.navdy.service.library.events.input.DialSimulationEvent$DialInputAction.DIAL_CLICK, Integer.valueOf(66));
    }
}
