package com.navdy.hud.app.device.gps;

class GpsManager$3 implements android.location.LocationListener {
    private float accuracyMax;
    private float accuracyMin;
    private int accuracySampleCount;
    private long accuracySampleStartTime;
    private float accuracySum;
    final com.navdy.hud.app.device.gps.GpsManager this$0;
    
    GpsManager$3(com.navdy.hud.app.device.gps.GpsManager a) {
        super();
        this.this$0 = a;
        this.accuracySampleStartTime = android.os.SystemClock.elapsedRealtime();
        this.accuracySum = 0.0f;
        this.accuracyMin = 0.0f;
        this.accuracyMax = 0.0f;
        this.accuracySampleCount = 0;
    }
    
    private void collectAccuracyStats(float f) {
        if (f != 0.0f) {
            this.accuracySum = this.accuracySum + f;
            this.accuracySampleCount = this.accuracySampleCount + 1;
            float f0 = this.accuracyMin;
            int i = (f0 > 0.0f) ? 1 : (f0 == 0.0f) ? 0 : -1;
            label0: {
                label1: {
                    if (i == 0) {
                        break label1;
                    }
                    if (!(f < this.accuracyMin)) {
                        break label0;
                    }
                }
                this.accuracyMin = f;
            }
            if (this.accuracyMax < f) {
                this.accuracyMax = f;
            }
        }
        if (com.navdy.hud.app.device.gps.GpsManager.access$1300(this.this$0) - this.accuracySampleStartTime > 300000L) {
            if (this.accuracySampleCount > 0) {
                com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.recordGpsAccuracy(Integer.toString(Math.round(this.accuracyMin)), Integer.toString(Math.round(this.accuracyMax)), Integer.toString(Math.round(this.accuracySum / (float)this.accuracySampleCount)));
                this.accuracySum = 0.0f;
                this.accuracyMax = 0.0f;
                this.accuracyMin = 0.0f;
                this.accuracySampleCount = 0;
            }
            this.accuracySampleStartTime = com.navdy.hud.app.device.gps.GpsManager.access$1300(this.this$0);
        }
    }
    
    public void onLocationChanged(android.location.Location a) {
        label0: try {
            if (!com.navdy.hud.app.device.gps.GpsManager.access$1400(this.this$0)) {
                com.navdy.hud.app.device.gps.GpsManager.access$1500(this.this$0);
            }
            if (!com.navdy.hud.app.debug.RouteRecorder.getInstance().isPlaying()) {
                com.navdy.hud.app.device.gps.GpsManager.access$1600(this.this$0, a);
                com.navdy.hud.app.device.gps.GpsManager.access$1102(this.this$0, a);
                com.navdy.hud.app.device.gps.GpsManager.access$1302(this.this$0, android.os.SystemClock.elapsedRealtime());
                this.collectAccuracyStats(a.getAccuracy());
                com.navdy.hud.app.service.HudConnectionService a0 = com.navdy.hud.app.device.gps.GpsManager.access$1700(this.this$0);
                label3: {
                    label4: {
                        if (a0 == null) {
                            break label4;
                        }
                        if (com.navdy.hud.app.device.gps.GpsManager.access$1700(this.this$0).isConnected()) {
                            break label3;
                        }
                    }
                    if (com.navdy.hud.app.device.gps.GpsManager.access$300(this.this$0) == com.navdy.hud.app.device.gps.GpsManager$LocationSource.UBLOX) {
                        break label0;
                    }
                    com.navdy.hud.app.device.gps.GpsManager.access$000().v("not connected with phone, start uBloxReporting");
                    this.this$0.startUbloxReporting();
                    break label0;
                }
                com.navdy.hud.app.device.gps.GpsManager$LocationSource a1 = com.navdy.hud.app.device.gps.GpsManager.access$300(this.this$0);
                com.navdy.hud.app.device.gps.GpsManager$LocationSource a2 = com.navdy.hud.app.device.gps.GpsManager$LocationSource.UBLOX;
                label2: {
                    if (a1 == a2) {
                        break label2;
                    }
                    long j = android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.device.gps.GpsManager.access$900(this.this$0);
                    if (j <= 3000L) {
                        break label2;
                    }
                    com.navdy.hud.app.device.gps.GpsManager.access$000().v(new StringBuilder().append("phone threshold exceeded= ").append(j).append(", start uBloxReporting").toString());
                    this.this$0.startUbloxReporting();
                    break label0;
                }
                boolean b = com.navdy.hud.app.device.gps.GpsManager.access$1800(this.this$0);
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (!com.navdy.hud.app.device.gps.GpsManager.access$1900(this.this$0)) {
                        break label0;
                    }
                }
                com.navdy.hud.app.device.gps.GpsManager.access$2000(this.this$0, a);
            }
        } catch(Throwable a3) {
            com.navdy.hud.app.device.gps.GpsManager.access$000().e(a3);
        }
    }
    
    public void onProviderDisabled(String s) {
    }
    
    public void onProviderEnabled(String s) {
    }
    
    public void onStatusChanged(String s, int i, android.os.Bundle a) {
    }
}
