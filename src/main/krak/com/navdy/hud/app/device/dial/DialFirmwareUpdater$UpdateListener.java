package com.navdy.hud.app.device.dial;

abstract public interface DialFirmwareUpdater$UpdateListener {
    abstract public void onUpdateState(boolean arg);
}
