package com.navdy.hud.app.device.gps;

public class GpsNmeaParser {
    final private static String COLON = ":";
    final private static String COMMA = ",";
    final private static char COMMA_CHAR = (char)44;
    final private static String DB = "db";
    final private static String EQUAL = "=";
    final public static int FIX_TYPE_2D_3D = 1;
    final public static int FIX_TYPE_DIFFERENTIAL = 2;
    final public static int FIX_TYPE_DR = 6;
    final public static int FIX_TYPE_FRTK = 5;
    final public static int FIX_TYPE_NONE = 0;
    final public static int FIX_TYPE_PPS = 3;
    final public static int FIX_TYPE_RTK = 4;
    final private static String GGA = "GGA";
    final private static String GLL = "GLL";
    final private static String GNS = "GNS";
    final private static String GSV = "GSV";
    final private static String RMC = "RMC";
    final private static int SATELLITE_REPORT_INTERVAL = 5000;
    final private static String SPACE = " ";
    final private static String SVID = "SV-";
    final private static String VTG = "VTG";
    private int fixType;
    private android.os.Handler handler;
    private long lastSatelliteReportTime;
    private int messageCount;
    private java.util.ArrayList messages;
    private String[] nmeaResult;
    private com.navdy.service.library.log.Logger sLogger;
    private java.util.HashMap satellitesSeen;
    private java.util.HashMap satellitesUsed;
    
    GpsNmeaParser(com.navdy.service.library.log.Logger a, android.os.Handler a0) {
        this.satellitesSeen = new java.util.HashMap();
        this.satellitesUsed = new java.util.HashMap();
        this.messages = new java.util.ArrayList();
        this.nmeaResult = new String[200];
        this.sLogger = a;
        this.handler = a0;
    }
    
    private String getGnssProviderName(String s) {
        int i = 0;
        String s0 = null;
        switch(s.hashCode()) {
            case 2281: {
                i = (s.equals("GP")) ? 0 : -1;
                break;
            }
            case 2279: {
                i = (s.equals("GN")) ? 4 : -1;
                break;
            }
            case 2277: {
                i = (s.equals("GL")) ? 1 : -1;
                break;
            }
            case 2267: {
                i = (s.equals("GB")) ? 3 : -1;
                break;
            }
            case 2266: {
                i = (s.equals("GA")) ? 2 : -1;
                break;
            }
            default: {
                i = -1;
            }
        }
        switch(i) {
            case 4: {
                s0 = "MultiGNSS";
                break;
            }
            case 3: {
                s0 = "BeiDou";
                break;
            }
            case 2: {
                s0 = "Galileo";
                break;
            }
            case 1: {
                s0 = "GLONASS";
                break;
            }
            case 0: {
                s0 = "GPS";
                break;
            }
            default: {
                s0 = "UNK";
            }
        }
        return s0;
    }
    
    static com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage getMessageType(char a, char a0, char a1) {
        com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage a2 = null;
        int i = a0;
        int i0 = a1;
        int i1 = a0;
        int i2 = a1;
        int i3 = a0;
        int i4 = a1;
        switch(a) {
            case 86: {
                label7: {
                    label8: {
                        if (i3 != 84) {
                            break label8;
                        }
                        if (i4 == 71) {
                            break label7;
                        }
                    }
                    a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.NOT_SUPPORTED;
                    break;
                }
                a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.VTG;
                break;
            }
            case 82: {
                label5: {
                    label6: {
                        if (i1 != 77) {
                            break label6;
                        }
                        if (i2 == 67) {
                            break label5;
                        }
                    }
                    a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.NOT_SUPPORTED;
                    break;
                }
                a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.RMC;
                break;
            }
            case 71: {
                label4: {
                    if (i != 83) {
                        break label4;
                    }
                    if (i0 != 86) {
                        break label4;
                    }
                    a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.GSV;
                    break;
                }
                label3: {
                    if (i != 71) {
                        break label3;
                    }
                    if (i0 != 65) {
                        break label3;
                    }
                    a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.GGA;
                    break;
                }
                label2: {
                    if (i != 76) {
                        break label2;
                    }
                    if (i0 != 76) {
                        break label2;
                    }
                    a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.GLL;
                    break;
                }
                label0: {
                    label1: {
                        if (i != 78) {
                            break label1;
                        }
                        if (i0 == 83) {
                            break label0;
                        }
                    }
                    a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.NOT_SUPPORTED;
                    break;
                }
                a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.GNS;
                break;
            }
            default: {
                a2 = com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.NOT_SUPPORTED;
            }
        }
        return a2;
    }
    
    static int parseData(String s, String[] a) {
        char[] a0 = s.toCharArray();
        int i = 0;
        int i0 = 0;
        int i1 = 0;
        while(i1 < a0.length) {
            int i2 = a0[i1];
            if (i2 == 44) {
                a[i] = new String(a0, i0, i1 - i0);
                i0 = i1 + 1;
                i = i + 1;
            }
            i1 = i1 + 1;
        }
        a[i] = new String(a0, i0, i1 - i0 - 2);
        return i + 1;
    }
    
    private void processGGA(String s) {
        com.navdy.hud.app.device.gps.GpsNmeaParser.parseData(s, this.nmeaResult);
        String s0 = this.nmeaResult[6];
        try {
            this.fixType = Integer.parseInt(s0);
        } catch(Throwable ignoredException) {
            this.fixType = 0;
        }
    }
    
    private void processGLL(String s) {
    }
    
    private void processGNS(String s) {
        com.navdy.hud.app.device.gps.GpsNmeaParser.parseData(s, this.nmeaResult);
        String s0 = this.nmeaResult[0].substring(1, 3);
        String s1 = this.nmeaResult[2];
        String s2 = this.nmeaResult[4];
        String s3 = this.nmeaResult[7];
        boolean b = android.text.TextUtils.isEmpty((CharSequence)s1);
        label2: {
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s3)) {
                        break label0;
                    }
                }
                this.satellitesUsed.put(s0, Integer.valueOf(0));
                break label2;
            }
            this.satellitesUsed.put(s0, Integer.valueOf(Integer.parseInt(s3)));
        }
    }
    
    private void processGSV(String s) {
        label5: if (com.navdy.hud.app.device.gps.GpsNmeaParser.parseData(s, this.nmeaResult) >= 8) {
            String s0 = this.nmeaResult[0].substring(1, 3);
            int i = Integer.parseInt(this.nmeaResult[1]);
            int i0 = Integer.parseInt(this.nmeaResult[2]);
            label3: {
                label4: {
                    if (i0 != 1) {
                        break label4;
                    }
                    this.messages.clear();
                    this.messageCount = i;
                    break label3;
                }
                if (this.messages.size() != 0) {
                    break label3;
                }
                break label5;
            }
            this.messages.add(s);
            if (i0 == this.messageCount) {
                java.util.HashMap a = (java.util.HashMap)this.satellitesSeen.get(s0);
                if (a != null) {
                    a.clear();
                } else {
                    a = new java.util.HashMap();
                    this.satellitesSeen.put(s0, a);
                }
                int i1 = this.messages.size();
                int i2 = 0;
                while(i2 < i1) {
                    int i3 = com.navdy.hud.app.device.gps.GpsNmeaParser.parseData((String)this.messages.get(i2), this.nmeaResult);
                    int i4 = 4;
                    while(i4 + 4 < i3) {
                        String s1 = this.nmeaResult[i4];
                        String s2 = this.nmeaResult[i4 + 1];
                        String s3 = this.nmeaResult[i4 + 2];
                        String s4 = this.nmeaResult[i4 + 3];
                        label0: if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                            int i5 = 0;
                            int i6 = 0;
                            boolean b = android.text.TextUtils.isEmpty((CharSequence)s2);
                            label1: {
                                label2: {
                                    if (b) {
                                        break label2;
                                    }
                                    if (android.text.TextUtils.isEmpty((CharSequence)s3)) {
                                        break label2;
                                    }
                                    if (!android.text.TextUtils.isEmpty((CharSequence)s4)) {
                                        break label1;
                                    }
                                }
                                a.remove(s1);
                                break label0;
                            }
                            try {
                                i5 = Integer.parseInt(s1);
                            } catch(NumberFormatException ignoredException) {
                                break label0;
                            }
                            try {
                                i6 = Integer.parseInt(s4);
                            } catch(NumberFormatException ignoredException0) {
                                break label0;
                            }
                            a.put(Integer.valueOf(i5), Integer.valueOf(i6));
                        }
                        i4 = i4 + 4;
                    }
                    i2 = i2 + 1;
                }
                this.messageCount = 0;
                this.messages.clear();
                long j = android.os.SystemClock.elapsedRealtime();
                if (j - this.lastSatelliteReportTime >= 5000L) {
                    this.sendGpsSatelliteStatus();
                    this.lastSatelliteReportTime = j;
                }
            }
        }
    }
    
    private void processRMC(String s) {
    }
    
    private void processVTG(String s) {
    }
    
    private void sendGpsSatelliteStatus() {
        android.os.Bundle a = new android.os.Bundle();
        java.util.Iterator a0 = this.satellitesSeen.entrySet().iterator();
        int i = 0;
        int i0 = 0;
        Object a1 = a0;
        while(((java.util.Iterator)a1).hasNext()) {
            Object a2 = ((java.util.Iterator)a1).next();
            String s = (String)((java.util.Map.Entry)a2).getKey();
            Object a3 = ((java.util.HashMap)((java.util.Map.Entry)a2).getValue()).entrySet().iterator();
            while(((java.util.Iterator)a3).hasNext()) {
                Object a4 = ((java.util.Iterator)a3).next();
                int i1 = ((Integer)((java.util.Map.Entry)a4).getKey()).intValue();
                int i2 = ((Integer)((java.util.Map.Entry)a4).getValue()).intValue();
                i = i + 1;
                a.putString(new StringBuilder().append("SAT_PROVIDER_").append(i).toString(), s);
                a.putInt(new StringBuilder().append("SAT_ID_").append(i).toString(), i1);
                a.putInt(new StringBuilder().append("SAT_DB_").append(i).toString(), i2);
                if (i2 > i0) {
                    i0 = i2;
                }
            }
        }
        a.putInt("SAT_SEEN", i);
        a.putInt("SAT_MAX_DB", i0);
        Object a5 = this.satellitesUsed.entrySet().iterator();
        int i3 = 0;
        while(((java.util.Iterator)a5).hasNext()) {
            i3 = i3 + ((Integer)((java.util.Map.Entry)((java.util.Iterator)a5).next()).getValue()).intValue();
        }
        a.putInt("SAT_USED", i3);
        a.putInt("SAT_FIX_TYPE", this.fixType);
        android.os.Message a6 = this.handler.obtainMessage(2);
        a6.obj = a;
        this.handler.sendMessage(a6);
    }
    
    public void parseNmeaMessage(String s) {
        if (s != null) {
            char[] a = s.toCharArray();
            if (a.length >= 7) {
                int i = a[0];
                if (i == 36) {
                    com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage a0 = com.navdy.hud.app.device.gps.GpsNmeaParser.getMessageType((char)(boolean)a[3], (char)(boolean)a[4], (char)(boolean)a[5]);
                    if (a0 != com.navdy.hud.app.device.gps.GpsNmeaParser$NmeaMessage.NOT_SUPPORTED) {
                        int i0 = 0;
                        while(i0 < this.nmeaResult.length) {
                            this.nmeaResult[i0] = null;
                            i0 = i0 + 1;
                        }
                        switch(com.navdy.hud.app.device.gps.GpsNmeaParser$1.$SwitchMap$com$navdy$hud$app$device$gps$GpsNmeaParser$NmeaMessage[a0.ordinal()]) {
                            case 6: {
                                this.processGSV(s);
                                break;
                            }
                            case 3: {
                                this.processGNS(s);
                                break;
                            }
                            case 1: {
                                this.processGGA(s);
                                break;
                            }
                            default: {
                                break;
                            }
                            case 2: case 4: case 5: {
                            }
                        }
                    }
                }
            }
        }
    }
}
