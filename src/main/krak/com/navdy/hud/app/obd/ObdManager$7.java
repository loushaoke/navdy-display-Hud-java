package com.navdy.hud.app.obd;

class ObdManager$7 extends com.navdy.obd.ICanBusMonitoringListener$Stub {
    final com.navdy.hud.app.obd.ObdManager this$0;
    
    ObdManager$7(com.navdy.hud.app.obd.ObdManager a) {
        super();
        this.this$0 = a;
    }
    
    public int getGpsSpeed() {
        return Math.round(com.navdy.hud.app.obd.ObdManager.access$1900(this.this$0).getGpsSpeedInMetersPerSecond());
    }
    
    public double getLatitude() {
        double d = 0.0;
        boolean b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
        label2: {
            com.here.android.mpa.common.GeoCoordinate a = null;
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    if (a != null) {
                        break label0;
                    }
                }
                d = -1.0;
                break label2;
            }
            d = a.getLatitude();
        }
        return d;
    }
    
    public double getLongitude() {
        double d = 0.0;
        boolean b = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized();
        label2: {
            com.here.android.mpa.common.GeoCoordinate a = null;
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    if (a != null) {
                        break label0;
                    }
                }
                d = -1.0;
                break label2;
            }
            d = a.getLongitude();
        }
        return d;
    }
    
    public String getMake() {
        String s = null;
        com.navdy.hud.app.profile.DriverProfileManager a = this.this$0.driverProfileManager;
        label2: {
            com.navdy.hud.app.profile.DriverProfile a0 = null;
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    a0 = this.this$0.driverProfileManager.getCurrentProfile();
                    if (a0 != null) {
                        break label0;
                    }
                }
                s = "";
                break label2;
            }
            s = a0.getCarMake();
        }
        return s;
    }
    
    public String getModel() {
        String s = null;
        com.navdy.hud.app.profile.DriverProfileManager a = this.this$0.driverProfileManager;
        label2: {
            com.navdy.hud.app.profile.DriverProfile a0 = null;
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    a0 = this.this$0.driverProfileManager.getCurrentProfile();
                    if (a0 != null) {
                        break label0;
                    }
                }
                s = "";
                break label2;
            }
            s = a0.getCarModel();
        }
        return s;
    }
    
    public String getYear() {
        String s = null;
        com.navdy.hud.app.profile.DriverProfileManager a = this.this$0.driverProfileManager;
        label2: {
            com.navdy.hud.app.profile.DriverProfile a0 = null;
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    a0 = this.this$0.driverProfileManager.getCurrentProfile();
                    if (a0 != null) {
                        break label0;
                    }
                }
                s = "";
                break label2;
            }
            s = a0.getCarYear();
        }
        return s;
    }
    
    public boolean isMonitoringLimitReached() {
        return com.navdy.hud.app.obd.ObdManager.access$2300(this.this$0).isCanBusMonitoringLimitReached();
    }
    
    public void onCanBusMonitorSuccess(int i) {
        com.navdy.hud.app.obd.ObdManager.access$700().d(new StringBuilder().append("onCanBusMonitorSuccess , Average amount of data : ").append(i).toString());
        com.navdy.hud.app.obd.ObdManager.access$2300(this.this$0).onCanBusMonitorSuccess(i);
    }
    
    public void onCanBusMonitoringError(String s) {
        com.navdy.hud.app.obd.ObdManager.access$700().d(new StringBuilder().append("onCanBusMonitoringError, Error : ").append(s).toString());
        com.navdy.hud.app.obd.ObdManager.access$2300(this.this$0).onCanBusMonitoringFailed();
    }
    
    public void onNewDataAvailable(String s) {
        com.navdy.hud.app.obd.ObdManager.access$700().d(new StringBuilder().append("onNewDataAvailable : ").append(s).toString());
        com.navdy.hud.app.obd.ObdManager.access$2300(this.this$0).onNewDataAvailable(s);
    }
}
