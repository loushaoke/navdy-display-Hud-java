package com.navdy.hud.app.obd;

class ObdManager$1 extends com.navdy.hud.app.obd.ObdManager$PidCheck {
    final com.navdy.hud.app.obd.ObdManager this$0;
    
    ObdManager$1(com.navdy.hud.app.obd.ObdManager a, int i, android.os.Handler a0) {
        this.this$0 = a;
        super(a, i, a0);
    }
    
    public long getWaitForValidDataTimeout() {
        return 45000L;
    }
    
    public void invalidatePid(int i) {
        com.navdy.hud.app.obd.ObdManager.access$000(this.this$0).remove(Integer.valueOf(47));
        if (com.navdy.hud.app.obd.ObdManager.access$100(this.this$0) != null) {
            com.navdy.hud.app.obd.ObdManager.access$100(this.this$0).remove(47);
            this.this$0.bus.post(new com.navdy.hud.app.obd.ObdManager$ObdSupportedPidsChangedEvent());
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.recordIncorrectFuelData();
    }
    
    public boolean isPidValueValid(double d) {
        return d > 0.0;
    }
}
