package com.navdy.hud.app.obd;
import com.navdy.hud.app.R;


public enum ObdManager$Firmware {
    OLD_320(0),
    NEW_421(1);

    private int value;
    ObdManager$Firmware(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class ObdManager$Firmware extends Enum {
//    final private static com.navdy.hud.app.obd.ObdManager$Firmware[] $VALUES;
//    final public static com.navdy.hud.app.obd.ObdManager$Firmware NEW_421;
//    final public static com.navdy.hud.app.obd.ObdManager$Firmware OLD_320;
//    public int resource;
//    
//    static {
//        OLD_320 = new com.navdy.hud.app.obd.ObdManager$Firmware("OLD_320", 0, R.raw.update3_2_0);
//        NEW_421 = new com.navdy.hud.app.obd.ObdManager$Firmware("NEW_421", 1, R.raw.update4_2_1);
//        com.navdy.hud.app.obd.ObdManager$Firmware[] a = new com.navdy.hud.app.obd.ObdManager$Firmware[2];
//        a[0] = OLD_320;
//        a[1] = NEW_421;
//        $VALUES = a;
//    }
//    
//    private ObdManager$Firmware(String s, int i, int i0) {
//        super(s, i);
//        this.resource = i0;
//    }
//    
//    public static com.navdy.hud.app.obd.ObdManager$Firmware valueOf(String s) {
//        return (com.navdy.hud.app.obd.ObdManager$Firmware)Enum.valueOf(com.navdy.hud.app.obd.ObdManager$Firmware.class, s);
//    }
//    
//    public static com.navdy.hud.app.obd.ObdManager$Firmware[] values() {
//        return $VALUES.clone();
//    }
//}
//