package com.navdy.hud.app.obd;


public enum ObdManager$ConnectionType {
    POWER_ONLY(0),
    OBD(1);

    private int value;
    ObdManager$ConnectionType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class ObdManager$ConnectionType extends Enum {
//    final private static com.navdy.hud.app.obd.ObdManager$ConnectionType[] $VALUES;
//    final public static com.navdy.hud.app.obd.ObdManager$ConnectionType OBD;
//    final public static com.navdy.hud.app.obd.ObdManager$ConnectionType POWER_ONLY;
//    
//    static {
//        POWER_ONLY = new com.navdy.hud.app.obd.ObdManager$ConnectionType("POWER_ONLY", 0);
//        OBD = new com.navdy.hud.app.obd.ObdManager$ConnectionType("OBD", 1);
//        com.navdy.hud.app.obd.ObdManager$ConnectionType[] a = new com.navdy.hud.app.obd.ObdManager$ConnectionType[2];
//        a[0] = POWER_ONLY;
//        a[1] = OBD;
//        $VALUES = a;
//    }
//    
//    private ObdManager$ConnectionType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.obd.ObdManager$ConnectionType valueOf(String s) {
//        return (com.navdy.hud.app.obd.ObdManager$ConnectionType)Enum.valueOf(com.navdy.hud.app.obd.ObdManager$ConnectionType.class, s);
//    }
//    
//    public static com.navdy.hud.app.obd.ObdManager$ConnectionType[] values() {
//        return $VALUES.clone();
//    }
//}
//