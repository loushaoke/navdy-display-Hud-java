package com.navdy.hud.mfi;


    public enum IAPCommunicationsManager$StartCallStateUpdates {
        RemoteID(0),
    DisplayName(1),
    Status(2),
    Direction(3),
    CallUUID(4),
    Skip(5),
    AddressBookID(6),
    Label(7),
    Service(8),
    IsConferenced(9),
    ConferenceGroup(10),
    DisconnectReason(11);

        private int value;
        IAPCommunicationsManager$StartCallStateUpdates(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class IAPCommunicationsManager$StartCallStateUpdates extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates AddressBookID;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates CallUUID;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates ConferenceGroup;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates Direction;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates DisconnectReason;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates DisplayName;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates IsConferenced;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates Label;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates RemoteID;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates Service;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates Skip;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates Status;
//    
//    static {
//        RemoteID = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("RemoteID", 0);
//        DisplayName = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("DisplayName", 1);
//        Status = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("Status", 2);
//        Direction = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("Direction", 3);
//        CallUUID = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("CallUUID", 4);
//        Skip = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("Skip", 5);
//        AddressBookID = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("AddressBookID", 6);
//        Label = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("Label", 7);
//        Service = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("Service", 8);
//        IsConferenced = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("IsConferenced", 9);
//        ConferenceGroup = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("ConferenceGroup", 10);
//        DisconnectReason = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates("DisconnectReason", 11);
//        com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates[12];
//        a[0] = RemoteID;
//        a[1] = DisplayName;
//        a[2] = Status;
//        a[3] = Direction;
//        a[4] = CallUUID;
//        a[5] = Skip;
//        a[6] = AddressBookID;
//        a[7] = Label;
//        a[8] = Service;
//        a[9] = IsConferenced;
//        a[10] = ConferenceGroup;
//        a[11] = DisconnectReason;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$StartCallStateUpdates(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$StartCallStateUpdates[] values() {
//        return $VALUES.clone();
//    }
//}
//