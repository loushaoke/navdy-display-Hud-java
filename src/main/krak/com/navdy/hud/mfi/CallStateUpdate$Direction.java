package com.navdy.hud.mfi;


public enum CallStateUpdate$Direction {
    Unknown(0),
    Incoming(1),
    Outgoing(2);

    private int value;
    CallStateUpdate$Direction(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class CallStateUpdate$Direction extends Enum {
//    final private static com.navdy.hud.mfi.CallStateUpdate$Direction[] $VALUES;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Direction Incoming;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Direction Outgoing;
//    final public static com.navdy.hud.mfi.CallStateUpdate$Direction Unknown;
//    
//    static {
//        Unknown = new com.navdy.hud.mfi.CallStateUpdate$Direction("Unknown", 0);
//        Incoming = new com.navdy.hud.mfi.CallStateUpdate$Direction("Incoming", 1);
//        Outgoing = new com.navdy.hud.mfi.CallStateUpdate$Direction("Outgoing", 2);
//        com.navdy.hud.mfi.CallStateUpdate$Direction[] a = new com.navdy.hud.mfi.CallStateUpdate$Direction[3];
//        a[0] = Unknown;
//        a[1] = Incoming;
//        a[2] = Outgoing;
//        $VALUES = a;
//    }
//    
//    private CallStateUpdate$Direction(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.CallStateUpdate$Direction valueOf(String s) {
//        return (com.navdy.hud.mfi.CallStateUpdate$Direction)Enum.valueOf(com.navdy.hud.mfi.CallStateUpdate$Direction.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.CallStateUpdate$Direction[] values() {
//        return $VALUES.clone();
//    }
//}
//