package com.navdy.hud.mfi;

abstract public interface PacketQueue {
    abstract public void queue(Object arg);
}
