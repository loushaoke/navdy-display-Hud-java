package com.navdy.hud.mfi;


public enum IAPMusicManager$PlaybackAttributes {
    PlaybackStatus(0),
    PlaybackElapsedTimeInMilliseconds(1),
    PlaybackShuffleMode(2),
    PlaybackRepeatMode(3),
    PlaybackAppName(4),
    PlaybackAppBundleId(5);

    private int value;
    IAPMusicManager$PlaybackAttributes(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class IAPMusicManager$PlaybackAttributes extends Enum {
//    final private static com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes PlaybackAppBundleId;
//    final public static com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes PlaybackAppName;
//    final public static com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes PlaybackElapsedTimeInMilliseconds;
//    final public static com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes PlaybackRepeatMode;
//    final public static com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes PlaybackShuffleMode;
//    final public static com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes PlaybackStatus;
//    int paramIndex;
//    
//    static {
//        PlaybackStatus = new com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes("PlaybackStatus", 0, 0);
//        PlaybackElapsedTimeInMilliseconds = new com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes("PlaybackElapsedTimeInMilliseconds", 1, 1);
//        PlaybackShuffleMode = new com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes("PlaybackShuffleMode", 2, 5);
//        PlaybackRepeatMode = new com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes("PlaybackRepeatMode", 3, 6);
//        PlaybackAppName = new com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes("PlaybackAppName", 4, 7);
//        PlaybackAppBundleId = new com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes("PlaybackAppBundleId", 5, 16);
//        com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes[] a = new com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes[6];
//        a[0] = PlaybackStatus;
//        a[1] = PlaybackElapsedTimeInMilliseconds;
//        a[2] = PlaybackShuffleMode;
//        a[3] = PlaybackRepeatMode;
//        a[4] = PlaybackAppName;
//        a[5] = PlaybackAppBundleId;
//        $VALUES = a;
//    }
//    
//    private IAPMusicManager$PlaybackAttributes(String s, int i, int i0) {
//        super(s, i);
//        this.paramIndex = i0;
//    }
//    
//    public static com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes)Enum.valueOf(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getParam() {
//        return this.paramIndex;
//    }
//}
//