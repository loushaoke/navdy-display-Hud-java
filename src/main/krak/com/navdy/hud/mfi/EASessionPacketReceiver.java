package com.navdy.hud.mfi;

abstract public interface EASessionPacketReceiver {
    abstract public void queue(com.navdy.hud.mfi.EASessionPacket arg);
}
