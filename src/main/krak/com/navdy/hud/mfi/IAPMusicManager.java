package com.navdy.hud.mfi;

public class IAPMusicManager extends com.navdy.hud.mfi.IAPControlMessageProcessor {
    final private static String TAG;
    public static com.navdy.hud.mfi.iAPProcessor$iAPMessage[] mMessages;
    private com.navdy.hud.mfi.NowPlayingUpdate aggregatedNowPlayingUpdate;
    private com.navdy.hud.mfi.IAPNowPlayingUpdateListener mNowPlayingUpdateListener;
    
    static {
        TAG = com.navdy.hud.mfi.IAPMusicManager.class.getSimpleName();
        com.navdy.hud.mfi.iAPProcessor$iAPMessage[] a = new com.navdy.hud.mfi.iAPProcessor$iAPMessage[1];
        a[0] = com.navdy.hud.mfi.iAPProcessor$iAPMessage.NowPlayingUpdate;
        mMessages = a;
    }
    
    public IAPMusicManager(com.navdy.hud.mfi.iAPProcessor a) {
        super(mMessages, a);
        this.aggregatedNowPlayingUpdate = new com.navdy.hud.mfi.NowPlayingUpdate();
    }
    
    private void mergeNowPlayingUpdate(com.navdy.hud.mfi.NowPlayingUpdate a) {
        if (a.mediaItemTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemTitle = a.mediaItemTitle;
        }
        if (a.mediaItemAlbumTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTitle = a.mediaItemAlbumTitle;
        }
        if (a.mediaItemArtist != null) {
            this.aggregatedNowPlayingUpdate.mediaItemArtist = a.mediaItemArtist;
        }
        if (a.mediaItemGenre != null) {
            this.aggregatedNowPlayingUpdate.mediaItemGenre = a.mediaItemGenre;
        }
        if (a.mPlaybackStatus != null) {
            this.aggregatedNowPlayingUpdate.mPlaybackStatus = a.mPlaybackStatus;
        }
        if (a.playbackShuffle != null) {
            this.aggregatedNowPlayingUpdate.playbackShuffle = a.playbackShuffle;
        }
        if (a.playbackRepeat != null) {
            this.aggregatedNowPlayingUpdate.playbackRepeat = a.playbackRepeat;
        }
        if (a.mediaItemPersistentIdentifier != null) {
            this.aggregatedNowPlayingUpdate.mediaItemPersistentIdentifier = a.mediaItemPersistentIdentifier;
        }
        if (a.mediaItemPlaybackDurationInMilliseconds > 0L) {
            this.aggregatedNowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds = a.mediaItemPlaybackDurationInMilliseconds;
        }
        if (a.mediaItemAlbumTrackNumber >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackNumber = a.mediaItemAlbumTrackNumber;
        }
        if (a.mediaItemAlbumTrackCount >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackCount = a.mediaItemAlbumTrackCount;
        }
        if (a.mPlaybackElapsedTimeMilliseconds >= 0L) {
            this.aggregatedNowPlayingUpdate.mPlaybackElapsedTimeMilliseconds = a.mPlaybackElapsedTimeMilliseconds;
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)a.mAppName)) {
            this.aggregatedNowPlayingUpdate.mAppName = a.mAppName;
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)a.mAppBundleId)) {
            this.aggregatedNowPlayingUpdate.mAppBundleId = a.mAppBundleId;
        }
        if (a.mediaItemArtworkFileTransferIdentifier >= 128 && a.mediaItemArtworkFileTransferIdentifier <= 255) {
            this.aggregatedNowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = a.mediaItemArtworkFileTransferIdentifier;
        }
    }
    
    private static com.navdy.hud.mfi.NowPlayingUpdate parseNowPlayingUpdate(com.navdy.hud.mfi.iAPProcessor$IAP2Params a) {
        com.navdy.hud.mfi.NowPlayingUpdate a0 = new com.navdy.hud.mfi.NowPlayingUpdate();
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters.MediaItemAttributes)) {
            com.navdy.hud.mfi.iAPProcessor$IAP2Params a1 = new com.navdy.hud.mfi.iAPProcessor$IAP2Params(a.getBlob((Enum)com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters.MediaItemAttributes), 0);
            if (a1.hasParam(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemPersistentIdentifier.getParam())) {
                a0.mediaItemPersistentIdentifier = a1.getUInt64(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemPersistentIdentifier.getParam());
            }
            if (a1.hasParam(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemTitle.getParam())) {
                a0.mediaItemTitle = a1.getUTF8(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemTitle.getParam());
            }
            if (a1.hasParam(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemAlbumTrackCount.getParam())) {
                a0.mediaItemAlbumTrackCount = a1.getUInt16(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemAlbumTrackCount.getParam());
            }
            if (a1.hasParam(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemAlbumTrackNumber.getParam())) {
                a0.mediaItemAlbumTrackNumber = a1.getUInt16(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemAlbumTrackNumber.getParam());
            }
            if (a1.hasParam(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemArtist.getParam())) {
                a0.mediaItemArtist = a1.getUTF8(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemArtist.getParam());
            }
            if (a1.hasParam(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemAlbumTitle.getParam())) {
                a0.mediaItemAlbumTitle = a1.getUTF8(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemAlbumTitle.getParam());
            }
            if (a1.hasParam(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam())) {
                a0.mediaItemArtworkFileTransferIdentifier = a1.getUInt8(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam());
            }
            if (a1.hasParam(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemGenre.getParam())) {
                a0.mediaItemGenre = a1.getUTF8(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemGenre.getParam());
            }
            if (a1.hasParam(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam())) {
                a0.mediaItemPlaybackDurationInMilliseconds = a1.getUInt32(com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam());
            }
        }
        if (a.hasParam((Enum)com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters.PlaybackAttributes)) {
            com.navdy.hud.mfi.iAPProcessor$IAP2Params a2 = new com.navdy.hud.mfi.iAPProcessor$IAP2Params(a.getBlob((Enum)com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters.PlaybackAttributes), 0);
            if (a2.hasParam(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackStatus.getParam())) {
                a0.mPlaybackStatus = (com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus)a2.getEnum(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackStatus.class, com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackStatus.getParam());
            }
            if (a2.hasParam(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam())) {
                a0.mPlaybackElapsedTimeMilliseconds = a2.getUInt32(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam());
            }
            if (a2.hasParam(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackAppName.getParam())) {
                a0.mAppName = a2.getUTF8(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackAppName.getParam());
            }
            if (a2.hasParam(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackAppBundleId.getParam())) {
                a0.mAppBundleId = a2.getUTF8(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackAppBundleId.getParam());
            }
            if (a2.hasParam(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackShuffleMode.getParam())) {
                a0.playbackShuffle = (com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle)a2.getEnum(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle.class, com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackShuffleMode.getParam());
            }
            if (a2.hasParam(com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackRepeatMode.getParam())) {
                a0.playbackRepeat = (com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat)a2.getEnum(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat.class, com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.PlaybackRepeatMode.getParam());
            }
        }
        return a0;
    }
    
    public void bProcessControlMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage a, int i, byte[] a0) {
        com.navdy.hud.mfi.iAPProcessor$IAP2Params a1 = com.navdy.hud.mfi.iAPProcessor.parse(a0);
        if (com.navdy.hud.mfi.IAPMusicManager$1.$SwitchMap$com$navdy$hud$mfi$iAPProcessor$iAPMessage[a.ordinal()] != 0) {
            com.navdy.hud.mfi.NowPlayingUpdate a2 = com.navdy.hud.mfi.IAPMusicManager.parseNowPlayingUpdate(a1);
            android.util.Log.d(TAG, new StringBuilder().append("Parsed ").append(a2).toString());
            this.mergeNowPlayingUpdate(a2);
            if (this.mNowPlayingUpdateListener != null) {
                this.mNowPlayingUpdateListener.onNowPlayingUpdate(this.aggregatedNowPlayingUpdate);
            }
        }
    }
    
    public void onKeyDown(int i) {
        this.miAPProcessor.onKeyDown(i);
    }
    
    public void onKeyDown(com.navdy.hud.mfi.IAPMusicManager$MediaKey a) {
        this.miAPProcessor.onKeyDown((Enum)a);
    }
    
    public void onKeyUp(int i) {
        this.miAPProcessor.onKeyUp(i);
    }
    
    public void onKeyUp(com.navdy.hud.mfi.IAPMusicManager$MediaKey a) {
        this.miAPProcessor.onKeyUp((Enum)a);
    }
    
    public void setNowPlayingUpdateListener(com.navdy.hud.mfi.IAPNowPlayingUpdateListener a) {
        this.mNowPlayingUpdateListener = a;
    }
    
    public void startNowPlayingUpdates() {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StartNowPlayingUpdates);
        com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator a0 = new com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator();
        com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes[] a1 = com.navdy.hud.mfi.IAPMusicManager$MediaItemAttributes.values();
        int i = a1.length;
        int i0 = 0;
        while(i0 < i) {
            a0.addNone(a1[i0].getParam());
            i0 = i0 + 1;
        }
        a.addBlob((Enum)com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters.MediaItemAttributes, a0.toBytes());
        com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator a2 = new com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator();
        com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes[] a3 = com.navdy.hud.mfi.IAPMusicManager$PlaybackAttributes.values();
        int i1 = a3.length;
        int i2 = 0;
        while(i2 < i1) {
            a2.addNone(a3[i2].getParam());
            i2 = i2 + 1;
        }
        a.addBlob((Enum)com.navdy.hud.mfi.IAPMusicManager$NowPlayingUpdatesParameters.PlaybackAttributes, a2.toBytes());
        this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
    
    public void stopNowPlayingUpdates() {
        com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage a = new com.navdy.hud.mfi.iAPProcessor$IAP2SessionMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage.StopNowPlayingUpdates);
        this.miAPProcessor.sendControlMessage((com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator)a);
    }
}
