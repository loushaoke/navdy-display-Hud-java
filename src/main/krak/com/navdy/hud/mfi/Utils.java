package com.navdy.hud.mfi;

public class Utils {
    final public static String MFI_TAG = "MFi";
    final protected static String TAG = "navdy-mfi-Utils";
    final protected static char[] hexArray;
    
    static {
        hexArray = "0123456789ABCDEF".toCharArray();
    }
    
    public Utils() {
    }
    
    public static String bytesToHex(byte[] a) {
        return com.navdy.hud.mfi.Utils.bytesToHex(a, false);
    }
    
    public static String bytesToHex(byte[] a, boolean b) {
        int i = b ? 3 : 2;
        char[] a0 = new char[a.length * i];
        int i0 = 0;
        while(i0 < a.length) {
            int i1 = a[i0];
            int i2 = i1 & 255;
            int i3 = i0 * i;
            int i4 = (char)(boolean)hexArray[i2 >>> 4];
            a0[i3] = (char)i4;
            int i5 = i0 * i + 1;
            int i6 = (char)(boolean)hexArray[i2 & 15];
            a0[i5] = (char)i6;
            if (b) {
                a0[i0 * i + 2] = (char)32;
            }
            i0 = i0 + 1;
        }
        return new String(a0);
    }
    
    public static String bytesToMacAddress(byte[] a) {
        java.util.Locale a0 = java.util.Locale.US;
        Object[] a1 = new Object[6];
        int i = a[0];
        a1[0] = Byte.valueOf((byte)i);
        int i0 = a[1];
        a1[1] = Byte.valueOf((byte)i0);
        int i1 = a[2];
        a1[2] = Byte.valueOf((byte)i1);
        int i2 = a[3];
        a1[3] = Byte.valueOf((byte)i2);
        int i3 = a[4];
        a1[4] = Byte.valueOf((byte)i3);
        int i4 = a[5];
        a1[5] = Byte.valueOf((byte)i4);
        return String.format(a0, "%02X:%02X:%02X:%02X:%02X:%02X", a1);
    }
    
    public static android.util.SparseArray getConstantsMap(Class a) {
        return com.navdy.hud.mfi.Utils.getConstantsMap(a, (String)null);
    }
    
    public static android.util.SparseArray getConstantsMap(Class a, String s) {
        android.util.SparseArray a0 = new android.util.SparseArray();
        java.lang.reflect.Field[] a1 = a.getDeclaredFields();
        int i = a1.length;
        int i0 = 0;
        while(i0 < i) {
            java.lang.reflect.Field a2 = a1[i0];
            label0: try {
                if ((a2.getType()).equals(Integer.TYPE) && java.lang.reflect.Modifier.isStatic(a2.getModifiers())) {
                    label1: {
                        if (s == null) {
                            break label1;
                        }
                        if (!a2.getName().startsWith(s)) {
                            break label0;
                        }
                    }
                    a0.put(a2.getInt(null), a2.getName());
                }
            } catch(IllegalAccessException a3) {
                android.util.Log.e("navdy-mfi-Utils", "", (Throwable)a3);
            }
            i0 = i0 + 1;
        }
        return a0;
    }
    
    public static byte[] intsToBytes(int[] a) {
        byte[] a0 = new byte[a.length];
        int i = 0;
        while(i < a.length) {
            int i0 = (byte)(a[i] != 0);
            int i1 = (byte)i0;
            a0[i] = (byte)i1;
            i = i + 1;
        }
        return a0;
    }
    
    public static void logTransfer(String s, String s0, Object[] a) {
        if (android.util.Log.isLoggable(s, 3)) {
            boolean b = android.util.Log.isLoggable(s, 2);
            int i = 0;
            while(i < a.length) {
                if (a[i] instanceof byte[]) {
                    String s1 = null;
                    byte[] a0 = (byte[])a[i];
                    if (b) {
                        s1 = com.navdy.hud.mfi.Utils.bytesToHex(a0, true);
                    } else {
                        Object[] a1 = new Object[1];
                        a1[0] = Integer.valueOf(a0.length);
                        s1 = String.format("%d bytes", a1);
                    }
                    a[i] = s1;
                }
                i = i + 1;
            }
            android.util.Log.d(s, String.format(s0, a));
        }
    }
    
    public static int nibbleToByte(byte a) {
        int i = a & 255;
        return (i > 57) ? i - 65 + 10 : i + -48;
    }
    
    public static byte[] packUInt16(int i) {
        byte[] a = new byte[2];
        int i0 = (byte)(i >> 8 & 255);
        a[0] = (byte)i0;
        int i1 = (byte)(i & 255);
        a[1] = (byte)i1;
        return a;
    }
    
    public static byte[] packUInt8(int i) {
        byte[] a = new byte[1];
        int i0 = (byte)(i & 255);
        a[0] = (byte)i0;
        return a;
    }
    
    public static byte[] parseMACAddress(String s) {
        byte[] a = s.toUpperCase().getBytes();
        byte[] a0 = new byte[6];
        int i = 0;
        while(i < 6) {
            int i0 = a[i * 3];
            int i1 = com.navdy.hud.mfi.Utils.nibbleToByte((byte)i0) << 4;
            int i2 = a[i * 3 + 1];
            int i3 = (byte)(i1 | com.navdy.hud.mfi.Utils.nibbleToByte((byte)i2));
            int i4 = (byte)i3;
            a0[i] = (byte)i4;
            i = i + 1;
        }
        return a0;
    }
    
    public static String toASCII(byte[] a) {
        String s = "";
        int i = 0;
        while(i < a.length) {
            int i0 = 0;
            int i1 = a[i];
            int i2 = i1 & 255;
            StringBuilder a0 = new StringBuilder().append(s);
            label2: {
                label0: {
                    label1: {
                        if (i2 < 32) {
                            break label1;
                        }
                        if (i2 < 128) {
                            break label0;
                        }
                    }
                    i0 = 46;
                    break label2;
                }
                i0 = (char)i2;
            }
            s = a0.append((char)i0).toString();
            i = i + 1;
        }
        return s;
    }
    
    public static long unpackInt64(byte[] a, int i) {
        return java.nio.ByteBuffer.wrap(a, i, a.length - i).getLong();
    }
    
    public static int unpackUInt16(byte[] a, int i) {
        int i0 = a[i];
        int i1 = (i0 & 255) << 8;
        int i2 = a[i + 1];
        return i1 | i2 & 255;
    }
    
    public static long unpackUInt32(byte[] a, int i) {
        int i0 = a[i];
        long j = (long)(i0 & 255) << 24;
        int i1 = a[i + 1];
        long j0 = j | (long)(i1 & 255) << 16;
        int i2 = a[i + 2];
        long j1 = j0 | (long)(i2 & 255) << 8;
        int i3 = a[i + 3];
        return j1 | (long)(i3 & 255);
    }
    
    public static java.math.BigInteger unpackUInt64(byte[] a, int i) {
        byte[] a0 = new byte[8];
        int i0 = a[i];
        int i1 = (byte)(i0 & 255);
        a0[0] = (byte)i1;
        int i2 = a[i + 1];
        int i3 = (byte)(i2 & 255);
        a0[1] = (byte)i3;
        int i4 = a[i + 2];
        int i5 = (byte)(i4 & 255);
        a0[2] = (byte)i5;
        int i6 = a[i + 3];
        int i7 = (byte)(i6 & 255);
        a0[3] = (byte)i7;
        int i8 = a[i + 4];
        int i9 = (byte)(i8 & 255);
        a0[4] = (byte)i9;
        int i10 = a[i + 5];
        int i11 = (byte)(i10 & 255);
        a0[5] = (byte)i11;
        int i12 = a[i + 6];
        int i13 = (byte)(i12 & 255);
        a0[6] = (byte)i13;
        int i14 = a[i + 7];
        int i15 = (byte)(i14 & 255);
        a0[7] = (byte)i15;
        return new java.math.BigInteger(a0);
    }
    
    public static int unpackUInt8(byte[] a, int i) {
        int i0 = a[i];
        return i0 & 255;
    }
}
