package com.navdy.hud.mfi;


public enum NowPlayingUpdate$PlaybackRepeat {
    Off(0),
    One(1),
    All(2);

    private int value;
    NowPlayingUpdate$PlaybackRepeat(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class NowPlayingUpdate$PlaybackRepeat extends Enum {
//    final private static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat[] $VALUES;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat All;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat Off;
//    final public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat One;
//    
//    static {
//        Off = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat("Off", 0);
//        One = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat("One", 1);
//        All = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat("All", 2);
//        com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat[] a = new com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat[3];
//        a[0] = Off;
//        a[1] = One;
//        a[2] = All;
//        $VALUES = a;
//    }
//    
//    private NowPlayingUpdate$PlaybackRepeat(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat valueOf(String s) {
//        return (com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat)Enum.valueOf(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat[] values() {
//        return $VALUES.clone();
//    }
//}
//