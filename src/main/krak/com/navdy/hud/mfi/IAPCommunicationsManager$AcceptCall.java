package com.navdy.hud.mfi;


    public enum IAPCommunicationsManager$AcceptCall {
        AcceptAction(0),
    CallUUID(1);

        private int value;
        IAPCommunicationsManager$AcceptCall(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class IAPCommunicationsManager$AcceptCall extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall AcceptAction;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall CallUUID;
//    
//    static {
//        AcceptAction = new com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall("AcceptAction", 0);
//        CallUUID = new com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall("CallUUID", 1);
//        com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall[2];
//        a[0] = AcceptAction;
//        a[1] = CallUUID;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$AcceptCall(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptCall[] values() {
//        return $VALUES.clone();
//    }
//}
//