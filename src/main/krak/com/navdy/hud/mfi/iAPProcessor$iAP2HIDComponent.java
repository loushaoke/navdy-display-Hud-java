package com.navdy.hud.mfi;

final class iAPProcessor$iAP2HIDComponent extends Enum {
    final private static com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent[] $VALUES;
    final public static com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent HIDComponentFunction;
    final public static com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent HIDComponentIdentifier;
    final public static com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent HIDComponentName;
    
    static {
        HIDComponentIdentifier = new com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent("HIDComponentIdentifier", 0);
        HIDComponentName = new com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent("HIDComponentName", 1);
        HIDComponentFunction = new com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent("HIDComponentFunction", 2);
        com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent[] a = new com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent[3];
        a[0] = HIDComponentIdentifier;
        a[1] = HIDComponentName;
        a[2] = HIDComponentFunction;
        $VALUES = a;
    }
    
    private iAPProcessor$iAP2HIDComponent(String s, int i) {
        super(s, i);
    }
    
    public static com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent valueOf(String s) {
        return (com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent.class, s);
    }
    
    public static com.navdy.hud.mfi.iAPProcessor$iAP2HIDComponent[] values() {
        return $VALUES.clone();
    }
}
