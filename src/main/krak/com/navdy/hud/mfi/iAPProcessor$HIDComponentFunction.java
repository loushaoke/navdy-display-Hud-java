package com.navdy.hud.mfi;


    public enum iAPProcessor$HIDComponentFunction {
        Keyboard(0),
    MediaPlaybackRemote(1),
    AssistiveTouchPointer(2),
    StandardGamepadFormFitting(3),
    ExtendedGamepadFormFitting(4),
    ExtendedGamepadNonFormFitting(5),
    AssistiveSwitchControl(6),
    Headphone(7);

        private int value;
        iAPProcessor$HIDComponentFunction(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$HIDComponentFunction extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction AssistiveSwitchControl;
//    final public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction AssistiveTouchPointer;
//    final public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction ExtendedGamepadFormFitting;
//    final public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction ExtendedGamepadNonFormFitting;
//    final public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction Headphone;
//    final public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction Keyboard;
//    final public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction MediaPlaybackRemote;
//    final public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction StandardGamepadFormFitting;
//    
//    static {
//        Keyboard = new com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction("Keyboard", 0);
//        MediaPlaybackRemote = new com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction("MediaPlaybackRemote", 1);
//        AssistiveTouchPointer = new com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction("AssistiveTouchPointer", 2);
//        StandardGamepadFormFitting = new com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction("StandardGamepadFormFitting", 3);
//        ExtendedGamepadFormFitting = new com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction("ExtendedGamepadFormFitting", 4);
//        ExtendedGamepadNonFormFitting = new com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction("ExtendedGamepadNonFormFitting", 5);
//        AssistiveSwitchControl = new com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction("AssistiveSwitchControl", 6);
//        Headphone = new com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction("Headphone", 7);
//        com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction[] a = new com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction[8];
//        a[0] = Keyboard;
//        a[1] = MediaPlaybackRemote;
//        a[2] = AssistiveTouchPointer;
//        a[3] = StandardGamepadFormFitting;
//        a[4] = ExtendedGamepadFormFitting;
//        a[5] = ExtendedGamepadNonFormFitting;
//        a[6] = AssistiveSwitchControl;
//        a[7] = Headphone;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$HIDComponentFunction(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$HIDComponentFunction[] values() {
//        return $VALUES.clone();
//    }
//}
//