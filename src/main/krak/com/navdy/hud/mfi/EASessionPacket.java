package com.navdy.hud.mfi;

public class EASessionPacket extends com.navdy.hud.mfi.Packet {
    int session;
    
    public EASessionPacket(int i, byte[] a) {
        super(a);
        this.session = i;
    }
}
