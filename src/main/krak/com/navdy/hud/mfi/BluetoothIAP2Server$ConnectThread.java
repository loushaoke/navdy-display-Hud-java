package com.navdy.hud.mfi;

class BluetoothIAP2Server$ConnectThread extends Thread {
    final private android.bluetooth.BluetoothDevice mmDevice;
    final private android.bluetooth.BluetoothSocket mmSocket;
    final com.navdy.hud.mfi.BluetoothIAP2Server this$0;
    
    public BluetoothIAP2Server$ConnectThread(com.navdy.hud.mfi.BluetoothIAP2Server a, android.bluetooth.BluetoothDevice a0) {
        super();
        android.bluetooth.BluetoothSocket a1 = null;
        this.this$0 = a;
        this.mmDevice = a0;
        java.util.UUID a2 = com.navdy.hud.mfi.BluetoothIAP2Server.DEVICE_IAP2;
        try {
            a1 = a0.createRfcommSocketToServiceRecord(a2);
        } catch(java.io.IOException a3) {
            android.util.Log.e("BluetoothIAP2Server", "Socket create() failed", (Throwable)a3);
            a1 = null;
        }
        this.mmSocket = a1;
    }
    
    public void cancel() {
        try {
            this.mmSocket.close();
        } catch(java.io.IOException a) {
            android.util.Log.e("BluetoothIAP2Server", "close() of connect socket failed", (Throwable)a);
        }
    }
    
    public void run() {
        android.util.Log.i("BluetoothIAP2Server", "BEGIN mConnectThread");
        this.setName("ConnectThread");
        com.navdy.hud.mfi.BluetoothIAP2Server.access$000(this.this$0).cancelDiscovery();
        label1: {
            label0: {
                try {
                    this.mmSocket.connect();
                    break label0;
                } catch(java.io.IOException ignoredException) {
                }
                try {
                    this.mmSocket.close();
                } catch(java.io.IOException a) {
                    android.util.Log.e("BluetoothIAP2Server", "unable to close() socket during connection failure", (Throwable)a);
                }
                com.navdy.hud.mfi.BluetoothIAP2Server.access$200(this.this$0);
                break label1;
            }
            synchronized(this.this$0) {
                com.navdy.hud.mfi.BluetoothIAP2Server.access$302(this.this$0, (com.navdy.hud.mfi.BluetoothIAP2Server$ConnectThread)null);
                /*monexit(a0)*/;
            }
            this.this$0.connected(this.mmSocket, this.mmDevice);
        }
    }
}
