package com.navdy.hud.mfi;


    public enum iAPProcessor$AccessoryHIDReport {
        HIDComponentIdentifier(0),
    HIDReport(1);

        private int value;
        iAPProcessor$AccessoryHIDReport(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class iAPProcessor$AccessoryHIDReport extends Enum {
//    final private static com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport[] $VALUES;
//    final public static com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport HIDComponentIdentifier;
//    final public static com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport HIDReport;
//    
//    static {
//        HIDComponentIdentifier = new com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport("HIDComponentIdentifier", 0);
//        HIDReport = new com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport("HIDReport", 1);
//        com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport[] a = new com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport[2];
//        a[0] = HIDComponentIdentifier;
//        a[1] = HIDReport;
//        $VALUES = a;
//    }
//    
//    private iAPProcessor$AccessoryHIDReport(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport valueOf(String s) {
//        return (com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport)Enum.valueOf(com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.iAPProcessor$AccessoryHIDReport[] values() {
//        return $VALUES.clone();
//    }
//}
//