package com.navdy.hud.mfi;

class EASession$BlockingInputStream extends java.io.InputStream {
    private java.io.InputStream source;
    final com.navdy.hud.mfi.EASession this$0;
    
    public EASession$BlockingInputStream(com.navdy.hud.mfi.EASession a, java.io.InputStream a0) {
        super();
        this.this$0 = a;
        this.source = a0;
    }
    
    private void waitForAvailable() {
        if (this.source.available() == 0) {
            try {
                com.navdy.hud.mfi.EASession.access$000(this.this$0).wait();
            } catch(InterruptedException ignoredException) {
            }
        }
    }
    
    public int read() {
        int i = 0;
        synchronized(com.navdy.hud.mfi.EASession.access$000(this.this$0)) {
            this.waitForAvailable();
            i = this.source.read();
            /*monexit(a)*/;
        }
        return i;
    }
    
    public int read(byte[] a) {
        int i = 0;
        synchronized(com.navdy.hud.mfi.EASession.access$000(this.this$0)) {
            this.waitForAvailable();
            i = this.source.read(a);
            /*monexit(a0)*/;
        }
        return i;
    }
    
    public int read(byte[] a, int i, int i0) {
        int i1 = 0;
        synchronized(com.navdy.hud.mfi.EASession.access$000(this.this$0)) {
            this.waitForAvailable();
            i1 = this.source.read(a, i, i0);
            /*monexit(a0)*/;
        }
        return i1;
    }
}
