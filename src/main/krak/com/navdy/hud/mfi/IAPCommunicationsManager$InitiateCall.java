package com.navdy.hud.mfi;


    public enum IAPCommunicationsManager$InitiateCall {
        Type(0),
    DestinationID(1),
    Service(2),
    AddressBookId(3);

        private int value;
        IAPCommunicationsManager$InitiateCall(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class IAPCommunicationsManager$InitiateCall extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall AddressBookId;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall DestinationID;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall Service;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall Type;
//    
//    static {
//        Type = new com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall("Type", 0);
//        DestinationID = new com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall("DestinationID", 1);
//        Service = new com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall("Service", 2);
//        AddressBookId = new com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall("AddressBookId", 3);
//        com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall[4];
//        a[0] = Type;
//        a[1] = DestinationID;
//        a[2] = Service;
//        a[3] = AddressBookId;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$InitiateCall(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$InitiateCall[] values() {
//        return $VALUES.clone();
//    }
//}
//