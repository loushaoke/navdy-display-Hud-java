package com.navdy.hud.mfi;

class BluetoothIAP2Server$AcceptThread extends Thread {
    final private android.bluetooth.BluetoothServerSocket mmServerSocket;
    final com.navdy.hud.mfi.BluetoothIAP2Server this$0;
    
    public BluetoothIAP2Server$AcceptThread(com.navdy.hud.mfi.BluetoothIAP2Server a) {
        super();
        android.bluetooth.BluetoothServerSocket a0 = null;
        this.this$0 = a;
        try {
            a0 = com.navdy.hud.mfi.BluetoothIAP2Server.access$000(a).listenUsingRfcommWithServiceRecord("Wireless iAP", com.navdy.hud.mfi.BluetoothIAP2Server.ACCESSORY_IAP2);
        } catch(java.io.IOException a1) {
            android.util.Log.e("BluetoothIAP2Server", "Socket listen() failed", (Throwable)a1);
            a0 = null;
        }
        this.mmServerSocket = a0;
    }
    
    public void cancel() {
        android.util.Log.d("BluetoothIAP2Server", new StringBuilder().append("Socket cancel ").append(this).toString());
        android.bluetooth.BluetoothServerSocket a = this.mmServerSocket;
        label0: {
            java.io.IOException a0 = null;
            if (a == null) {
                break label0;
            }
            try {
                this.mmServerSocket.close();
                break label0;
            } catch(java.io.IOException a1) {
                a0 = a1;
            }
            android.util.Log.e("BluetoothIAP2Server", "Socket close() of bluetoothServer failed", (Throwable)a0);
        }
    }
    
    public void run() {
        com.navdy.hud.mfi.BluetoothIAP2Server a = null;
        Throwable a0 = null;
        android.util.Log.d("BluetoothIAP2Server", new StringBuilder().append("Socket BEGIN mAcceptThread").append(this).toString());
        this.setName("AcceptThread");
        android.bluetooth.BluetoothServerSocket a1 = this.mmServerSocket;
        label0: {
            if (a1 != null) {
                while(com.navdy.hud.mfi.BluetoothIAP2Server.access$100(this.this$0) != 3) {
                    java.io.IOException a2 = null;
                    label1: {
                        android.bluetooth.BluetoothSocket a3 = null;
                        try {
                            a3 = this.mmServerSocket.accept();
                        } catch(java.io.IOException a4) {
                            a2 = a4;
                            break label1;
                        }
                        android.util.Log.d("BluetoothIAP2Server", "Socket accepted");
                        if (a3 == null) {
                            continue;
                        }
                        synchronized(this.this$0) {
                            switch(com.navdy.hud.mfi.BluetoothIAP2Server.access$100(this.this$0)) {
                                case 1: case 2: {
                                    this.this$0.connected(a3, a3.getRemoteDevice());
                                    break;
                                }
                                case 0: case 3: {
                                    try {
                                        a3.close();
                                        break;
                                    } catch(java.io.IOException a5) {
                                        android.util.Log.e("BluetoothIAP2Server", "Could not close unwanted socket", (Throwable)a5);
                                        break;
                                    }
                                }
                            }
                            /*monexit(a)*/;
                            continue;
                        }
                    }
                    android.util.Log.e("BluetoothIAP2Server", "Socket accept() failed", (Throwable)a2);
                    break;
                }
                android.util.Log.i("BluetoothIAP2Server", "END mAcceptThread");
            } else {
                android.util.Log.e("BluetoothIAP2Server", "No socket (maybe BT not enabled?) - END mAcceptThread");
            }
            return;
        }
        while(true) {
            try {
                /*monexit(a)*/;
            } catch(IllegalMonitorStateException | NullPointerException a7) {
                Throwable a8 = a7;
                a0 = a8;
                continue;
            }
            throw a0;
        }
    }
}
