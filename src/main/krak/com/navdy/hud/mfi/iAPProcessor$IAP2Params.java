package com.navdy.hud.mfi;

class iAPProcessor$IAP2Params {
    final public static int DATA_OFFSET = 4;
    final public static int ID_OFFSET = 2;
    byte[] data;
    android.util.SparseArray params;
    
    iAPProcessor$IAP2Params(byte[] a, int i) {
        this.params = new android.util.SparseArray();
        this.data = a;
        while(i < a.length) {
            com.navdy.hud.mfi.iAPProcessor$IAP2Param a0 = new com.navdy.hud.mfi.iAPProcessor$IAP2Param();
            a0.offset = i;
            a0.length = com.navdy.hud.mfi.Utils.unpackUInt16(a, i);
            a0.id = com.navdy.hud.mfi.Utils.unpackUInt16(a, i + 2);
            this.params.put(a0.id, a0);
            i = i + a0.length;
        }
    }
    
    byte[] getBlob(int i) {
        com.navdy.hud.mfi.iAPProcessor$IAP2Param a = (com.navdy.hud.mfi.iAPProcessor$IAP2Param)this.params.get(i);
        return java.util.Arrays.copyOfRange(this.data, a.offset + 4, a.offset + a.length);
    }
    
    byte[] getBlob(Enum a) {
        return this.getBlob(a.ordinal());
    }
    
    boolean getBoolean(int i) {
        return this.getUInt8(i) != 0;
    }
    
    boolean getBoolean(Enum a) {
        return this.getBoolean(a.ordinal());
    }
    
    int getEnum(int i) {
        return this.getUInt8(i);
    }
    
    int getEnum(Enum a) {
        return this.getEnum(a.ordinal());
    }
    
    Object getEnum(Class a, int i) {
        Object a0 = null;
        int i0 = this.getEnum(i);
        Object[] a1 = a.getEnumConstants();
        label2: {
            label0: {
                label1: {
                    if (i0 < 0) {
                        break label1;
                    }
                    if (i0 < a1.length) {
                        break label0;
                    }
                }
                a0 = null;
                break label2;
            }
            a0 = a1[i0];
        }
        return a0;
    }
    
    Object getEnum(Class a, Enum a0) {
        return this.getEnum(a, a0.ordinal());
    }
    
    int getUInt16(int i) {
        com.navdy.hud.mfi.iAPProcessor$IAP2Param a = (com.navdy.hud.mfi.iAPProcessor$IAP2Param)this.params.get(i);
        return com.navdy.hud.mfi.Utils.unpackUInt16(this.data, a.offset + 4);
    }
    
    int getUInt16(Enum a) {
        return this.getUInt16(a.ordinal());
    }
    
    long getUInt32(int i) {
        com.navdy.hud.mfi.iAPProcessor$IAP2Param a = (com.navdy.hud.mfi.iAPProcessor$IAP2Param)this.params.get(i);
        return com.navdy.hud.mfi.Utils.unpackUInt32(this.data, a.offset + 4);
    }
    
    long getUInt32(Enum a) {
        return this.getUInt32(a.ordinal());
    }
    
    java.math.BigInteger getUInt64(int i) {
        com.navdy.hud.mfi.iAPProcessor$IAP2Param a = (com.navdy.hud.mfi.iAPProcessor$IAP2Param)this.params.get(i);
        return com.navdy.hud.mfi.Utils.unpackUInt64(this.data, a.offset + 4);
    }
    
    int getUInt8(int i) {
        com.navdy.hud.mfi.iAPProcessor$IAP2Param a = (com.navdy.hud.mfi.iAPProcessor$IAP2Param)this.params.get(i);
        return com.navdy.hud.mfi.Utils.unpackUInt8(this.data, a.offset + 4);
    }
    
    int getUInt8(Enum a) {
        return this.getUInt8(a.ordinal());
    }
    
    String getUTF8(int i) {
        String s = null;
        byte[] a = this.getBlob(i);
        try {
            s = new String(a, 0, a.length - 1, "UTF-8");
        } catch(java.io.UnsupportedEncodingException ignoredException) {
            s = null;
        }
        return s;
    }
    
    String getUTF8(Enum a) {
        return this.getUTF8(a.ordinal());
    }
    
    boolean hasParam(int i) {
        return this.params.get(i) != null;
    }
    
    boolean hasParam(Enum a) {
        return this.hasParam(a.ordinal());
    }
}
