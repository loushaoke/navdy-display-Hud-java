package com.navdy.hud.mfi;

class iAPProcessor$IAP2ParamsCreator {
    final private static int META_DATA_LENGTH = 4;
    final public static int SIZE_OF_LENGTH = 2;
    final public static int SIZE_OF_PARAM_ID = 2;
    java.io.DataOutputStream dos;
    java.io.ByteArrayOutputStream params;
    
    public iAPProcessor$IAP2ParamsCreator() {
        this.params = new java.io.ByteArrayOutputStream();
        this.dos = new java.io.DataOutputStream((java.io.OutputStream)this.params);
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addBlob(int i, byte[] a) {
        try {
            int i0 = a.length;
            this.dos.writeShort(i0 + 4);
            this.dos.writeShort(i);
            this.dos.write(a);
        } catch(java.io.IOException a0) {
            android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.access$400(), "Exception while creating message ", (Throwable)a0);
        }
        return this;
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addBlob(Enum a, byte[] a0) {
        return this.addBlob(a.ordinal(), a0);
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addBoolean(int i, boolean b) {
        return this.addUInt8(i, b ? 1 : 0);
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addBoolean(Enum a, boolean b) {
        return this.addUInt8(a.ordinal(), b ? 1 : 0);
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addEnum(int i, int i0) {
        return this.addUInt8(i, i0);
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addEnum(Enum a, int i) {
        return this.addEnum(a.ordinal(), i);
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addEnum(Enum a, Enum a0) {
        return this.addEnum(a.ordinal(), a0.ordinal());
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addNone(int i) {
        try {
            this.dos.writeShort(4);
            this.dos.writeShort(i);
        } catch(java.io.IOException a) {
            android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.access$400(), "Exception while creating message ", (Throwable)a);
        }
        return this;
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addNone(Enum a) {
        return this.addNone(a.ordinal());
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addString(int i, String s) {
        try {
            byte[] a = s.getBytes("UTF-8");
            int i0 = a.length;
            this.dos.writeShort(i0 + 4 + 1);
            this.dos.writeShort(i);
            this.dos.write(a);
            java.io.DataOutputStream a0 = this.dos;
            byte[] a1 = new byte[1];
            a1[0] = (byte)0;
            a0.write(a1);
        } catch(java.io.IOException a2) {
            android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.access$400(), "Exception while creating message ", (Throwable)a2);
        }
        return this;
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addString(Enum a, String s) {
        return this.addString(a.ordinal(), s);
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addUInt16(int i, int i0) {
        try {
            this.dos.writeShort(6);
            this.dos.writeShort(i);
            this.dos.writeShort(i0);
        } catch(java.io.IOException a) {
            android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.access$400(), "Exception while creating message ", (Throwable)a);
        }
        return this;
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addUInt16(Enum a, int i) {
        return this.addUInt16(a.ordinal(), i);
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addUInt8(int i, int i0) {
        try {
            this.dos.writeShort(5);
            this.dos.writeShort(i);
            this.dos.writeByte(i0);
        } catch(java.io.IOException a) {
            android.util.Log.e(com.navdy.hud.mfi.iAPProcessor.access$400(), "Exception while creating message ", (Throwable)a);
        }
        return this;
    }
    
    com.navdy.hud.mfi.iAPProcessor$IAP2ParamsCreator addUInt8(Enum a, int i) {
        return this.addUInt8(a.ordinal(), i);
    }
    
    byte[] toBytes() {
        return this.params.toByteArray();
    }
}
