package com.navdy.hud.device.connection;

class iAP2MusicHelper$1 implements Runnable {
    final com.navdy.hud.device.connection.iAP2MusicHelper this$0;
    final byte[] val$data;
    final int val$fileTransferIdentifier;
    final com.navdy.service.library.events.audio.MusicTrackInfo val$musicTrackInfo;
    
    iAP2MusicHelper$1(com.navdy.hud.device.connection.iAP2MusicHelper a, int i, byte[] a0, com.navdy.service.library.events.audio.MusicTrackInfo a1) {
        super();
        this.this$0 = a;
        this.val$fileTransferIdentifier = i;
        this.val$data = a0;
        this.val$musicTrackInfo = a1;
    }
    
    public void run() {
        com.navdy.hud.device.connection.iAP2MusicHelper.access$000().d(new StringBuilder().append("onFileReceived task ").append(this.val$fileTransferIdentifier).toString());
        byte[] a = this.val$data;
        okio.ByteString a0 = null;
        label0: {
            Exception a1 = null;
            if (a == null) {
                break label0;
            }
            int i = this.val$data.length;
            a0 = null;
            if (i <= 0) {
                break label0;
            }
            try {
                android.graphics.Bitmap a2 = com.navdy.service.library.util.ScalingUtilities.decodeByteArray(this.val$data, 200, 200, com.navdy.service.library.util.ScalingUtilities$ScalingLogic.FIT);
                if (a2 == null) {
                    com.navdy.hud.device.connection.iAP2MusicHelper.access$000().e("Couldn't decode byte array to bitmap");
                    a0 = null;
                    break label0;
                } else {
                    a0 = okio.ByteString.of(com.navdy.service.library.util.ScalingUtilities.encodeByteArray(com.navdy.service.library.util.ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(a2, 200, 200, com.navdy.service.library.util.ScalingUtilities$ScalingLogic.FIT)));
                    break label0;
                }
            } catch(Exception a3) {
                a1 = a3;
            }
            com.navdy.hud.device.connection.iAP2MusicHelper.access$000().e("Error updating the art work received ", (Throwable)a1);
            a0 = null;
        }
        com.navdy.hud.device.connection.iAP2MusicHelper.access$102(this.this$0, new com.navdy.service.library.events.audio.MusicArtworkResponse$Builder().name(this.val$musicTrackInfo.name).album(this.val$musicTrackInfo.album).author(this.val$musicTrackInfo.author).photo(a0).build());
        com.navdy.hud.device.connection.iAP2MusicHelper.access$000().d(new StringBuilder().append("sending artworkResponse ").append(com.navdy.hud.device.connection.iAP2MusicHelper.access$100(this.this$0)).toString());
        com.navdy.hud.device.connection.iAP2MusicHelper.access$200(this.this$0).sendMessageAsNavdyEvent((com.squareup.wire.Message)com.navdy.hud.device.connection.iAP2MusicHelper.access$100(this.this$0));
    }
}
