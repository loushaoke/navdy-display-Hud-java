package com.navdy.hud.device.connection;

final class IAPMessageUtility$3 extends java.util.HashMap {
    IAPMessageUtility$3() {
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat.Off, com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_OFF);
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat.One, com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_ONE);
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackRepeat.All, com.navdy.service.library.events.audio.MusicRepeatMode.MUSIC_REPEAT_MODE_ALL);
    }
}
