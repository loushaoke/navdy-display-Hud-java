package com.navdy.hud.device.connection;

class iAP2Link$ProxyStream extends java.io.OutputStream {
    private java.io.OutputStream outputStream;
    final com.navdy.hud.device.connection.iAP2Link this$0;
    
    private iAP2Link$ProxyStream(com.navdy.hud.device.connection.iAP2Link a) {
        super();
        this.this$0 = a;
    }
    
    iAP2Link$ProxyStream(com.navdy.hud.device.connection.iAP2Link a, com.navdy.hud.device.connection.iAP2Link$1 a0) {
        this(a);
    }
    
    public void setOutputStream(java.io.OutputStream a) {
        this.outputStream = a;
    }
    
    public void write(int i) {
        throw new java.io.IOException("Shouldn't be calling this method!");
    }
    
    public void write(byte[] a) {
        if (this.outputStream != null) {
            this.outputStream.write(a);
        }
    }
}
