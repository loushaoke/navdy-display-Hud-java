package com.navdy.hud.device.connection;

class iAP2Link$4 implements com.navdy.hud.device.connection.iAP2Link$NavdyEventProcessor {
    final com.navdy.hud.device.connection.iAP2Link this$0;
    
    iAP2Link$4(com.navdy.hud.device.connection.iAP2Link a) {
        super();
        this.this$0 = a;
    }
    
    public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent a) {
        com.navdy.service.library.events.input.LaunchAppEvent a0 = (com.navdy.service.library.events.input.LaunchAppEvent)a.getExtension(com.navdy.service.library.events.Ext_NavdyEvent.launchAppEvent);
        if (android.text.TextUtils.isEmpty((CharSequence)a0.appBundleID)) {
            com.navdy.hud.device.connection.iAP2Link.access$600(this.this$0).launchApp(false);
        } else {
            com.navdy.hud.device.connection.iAP2Link.access$600(this.this$0).launchApp(a0.appBundleID, false);
        }
        return true;
    }
}
