package com.navdy.obd;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.amazonaws.services.s3.internal.Constants;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class ScanSchedule implements Parcelable {
    public static final Creator<ScanSchedule> CREATOR = new Creator<ScanSchedule>() {
        public ScanSchedule createFromParcel(Parcel in) {
            return new ScanSchedule(in, null);
        }

        public ScanSchedule[] newArray(int size) {
            return new ScanSchedule[size];
        }
    };
    Map<Integer, Scan> schedule;

    public static class Scan {
        public int pid;
        public int scanInterval;

        public Scan(int pid, int scanInterval) {
            this.pid = pid;
            this.scanInterval = scanInterval;
        }
    }

    /* synthetic */ ScanSchedule(Parcel x0, AnonymousClass1 x1) {
        this(x0);
    }

    public ScanSchedule() {
        this.schedule = new HashMap();
    }

    public ScanSchedule(ScanSchedule source) {
        this.schedule = new HashMap(source.schedule);
    }

    private ScanSchedule(Parcel in) {
        readFromParcel(in);
    }

    public void merge(ScanSchedule otherSchedule) {
        for (Entry<Integer, Scan> entry : otherSchedule.schedule.entrySet()) {
            Scan scan = (Scan) entry.getValue();
            addPid(scan.pid, scan.scanInterval);
        }
    }

    public boolean isEmpty() {
        return this.schedule.isEmpty();
    }

    public void addPid(int pid, int scanInterval) {
        Scan oldScan = (Scan) this.schedule.get(Integer.valueOf(pid));
        if (oldScan == null) {
            this.schedule.put(Integer.valueOf(pid), new Scan(pid, scanInterval));
        } else if (scanInterval < oldScan.scanInterval) {
            oldScan.scanInterval = scanInterval;
        }
    }

    public void addPids(List<Pid> pids, int scanInterval) {
        for (Pid pid : pids) {
            addPid(pid.getId(), scanInterval);
        }
    }

    public Scan remove(int pid) {
        return (Scan) this.schedule.remove(Integer.valueOf(pid));
    }

    public int size() {
        return this.schedule.size();
    }

    public List<Scan> getScanList() {
        return new ArrayList(this.schedule.values());
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(this.schedule.size());
        for (Entry<Integer, Scan> entry : this.schedule.entrySet()) {
            Scan scan = (Scan) entry.getValue();
            out.writeInt(scan.pid);
            out.writeInt(scan.scanInterval);
        }
    }

    public void readFromParcel(Parcel in) {
        this.schedule = new HashMap();
        int length = in.readInt();
        for (int i = 0; i < length; i++) {
            addPid(in.readInt(), in.readInt());
        }
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("ScanSchedule{ schedule=");
        boolean first = true;
        if (this.schedule != null) {
            for (Entry<Integer, Scan> entry : this.schedule.entrySet()) {
                Scan scan = (Scan) entry.getValue();
                if (first) {
                    first = false;
                } else {
                    builder.append(HereManeuverDisplayBuilder.COMMA);
                }
                builder.append("[").append(scan.pid).append(", ").append(scan.scanInterval).append("ms]");
            }
        } else {
            builder.append(Constants.NULL_VERSION_ID);
        }
        builder.append(" }");
        return builder.toString();
    }
}
