package com.navdy.hud.app.obd;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.navdy.service.library.log.Logger;

public class CarDetails {
    public static final Logger sLogger = new Logger(CarDetails.class);
    String aaia;
    String engine;
    String engineType;
    String make;
    String model;
    String vin;
    String year;

    public static boolean matches(CarDetails details, String vinNumber) {
        return ObdDeviceConfigurationManager.isValidVin(vinNumber) && details != null && TextUtils.equals(details.vin, vinNumber);
    }

    public static CarDetails fromJson(String data) {
        try {
            return (CarDetails) new Gson().fromJson(data.toString(), CarDetails.class);
        } catch (JsonSyntaxException se) {
            sLogger.e("Exception while parsing the car details response from CarMD ", se);
            return null;
        }
    }
}
