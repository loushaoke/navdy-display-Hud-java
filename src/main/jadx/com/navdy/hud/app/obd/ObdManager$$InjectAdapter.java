package com.navdy.hud.app.obd;

import android.content.SharedPreferences;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class ObdManager$$InjectAdapter extends Binding<ObdManager> implements MembersInjector<ObdManager> {
    private Binding<Bus> bus;
    private Binding<DriverProfileManager> driverProfileManager;
    private Binding<PowerManager> powerManager;
    private Binding<SharedPreferences> sharedPreferences;
    private Binding<TelemetryDataManager> telemetryDataManager;
    private Binding<TripManager> tripManager;

    public ObdManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.obd.ObdManager", false, ObdManager.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", ObdManager.class, getClass().getClassLoader());
        this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", ObdManager.class, getClass().getClassLoader());
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", ObdManager.class, getClass().getClassLoader());
        this.tripManager = linker.requestBinding("com.navdy.hud.app.framework.trips.TripManager", ObdManager.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", ObdManager.class, getClass().getClassLoader());
        this.telemetryDataManager = linker.requestBinding("com.navdy.hud.app.analytics.TelemetryDataManager", ObdManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.driverProfileManager);
        injectMembersBindings.add(this.powerManager);
        injectMembersBindings.add(this.tripManager);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.telemetryDataManager);
    }

    public void injectMembers(ObdManager object) {
        object.bus = (Bus) this.bus.get();
        object.driverProfileManager = (DriverProfileManager) this.driverProfileManager.get();
        object.powerManager = (PowerManager) this.powerManager.get();
        object.tripManager = (TripManager) this.tripManager.get();
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
        object.telemetryDataManager = (TelemetryDataManager) this.telemetryDataManager.get();
    }
}
