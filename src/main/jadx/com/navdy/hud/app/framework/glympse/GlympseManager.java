package com.navdy.hud.app.framework.glympse;

import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.glympse.android.api.GEventListener;
import com.glympse.android.api.GGlympse;
import com.glympse.android.api.GPlace;
import com.glympse.android.api.GTicket;
import com.glympse.android.api.GTrack;
import com.glympse.android.api.GTrackBuilder;
import com.glympse.android.api.GUser;
import com.glympse.android.api.GlympseFactory;
import com.glympse.android.core.CoreFactory;
import com.glympse.android.core.GArray;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.glympse.GlympseNotification.Type;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavController;
import com.navdy.hud.app.maps.here.HereNavController.State;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.CredentialUtil;
import com.squareup.otto.Subscribe;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public final class GlympseManager {
    private static final String GLYMPSE_API_KEY = CredentialUtil.getCredentials(HudApplication.getAppContext(), "GLYMPSE_API_KEY");
    private static final String GLYMPSE_BASE_URL = "api.glympse.com";
    public static final int GLYMPSE_DURATION = ((int) TimeUnit.HOURS.toMillis(1));
    public static final String GLYMPSE_TYPE_SHARE_LOCATION = "Location";
    public static final String GLYMPSE_TYPE_SHARE_TRIP = "Trip";
    private static final String NAVDY_CONTACT_UUID = "navdy_contact_uuid";
    private static final long NAVDY_GLYMPSE_PARTNER_ID = 1;
    private static final int UPDATE_ETA_INTERVAL = ((int) TimeUnit.MINUTES.toMillis(1));
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final Logger logger = new Logger(GlympseManager.class);
    private static final String[] messages;
    private String currentGlympseUserId;
    private GGlympse glympse;
    private boolean glympseInitFailed;
    private boolean isTrackingETA;
    private final NotificationManager notificationManager;
    private final Map<String, Contact> ticketsAwaitingReadReceipt;
    private Runnable updateETA;

    public enum Error {
        NONE,
        INTERNAL_ERROR,
        NO_INTERNET
    }

    private static class InternalSingleton {
        private static final GlympseManager singleton = new GlympseManager();

        private InternalSingleton() {
        }
    }

    /* synthetic */ GlympseManager(AnonymousClass1 x0) {
        this();
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        messages = new String[]{resources.getString(R.string.send_without_message), resources.getString(R.string.on_my_way), resources.getString(R.string.i_am_driving), resources.getString(R.string.running_late), resources.getString(R.string.i_am_here)};
    }

    public static GlympseManager getInstance() {
        return InternalSingleton.singleton;
    }

    private GlympseManager() {
        this.updateETA = new Runnable() {
            public void run() {
                if (GlympseManager.getInstance().isSynced()) {
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            if (HereMapsManager.getInstance().isInitialized()) {
                                HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
                                HereNavController navController = hereNavigationManager.getNavController();
                                if (navController.getState() == State.NAVIGATING) {
                                    final Date etaDate = navController.getEta(true, TrafficPenaltyMode.OPTIMAL);
                                    if (HereMapUtil.isValidEtaDate(etaDate)) {
                                        long l1 = SystemClock.elapsedRealtime();
                                        GTrackBuilder trackBuilder = GlympseFactory.createTrackBuilder();
                                        trackBuilder.setSource(0);
                                        trackBuilder.setDistance((int) navController.getDestinationDistance());
                                        Route route = hereNavigationManager.getCurrentRoute();
                                        StringBuilder builder = new StringBuilder();
                                        if (route != null) {
                                            List<Maneuver> maneuverList = route.getManeuvers();
                                            if (maneuverList != null) {
                                                for (Maneuver maneuver : maneuverList) {
                                                    GeoCoordinate coordinate = maneuver.getCoordinate();
                                                    if (coordinate != null) {
                                                        double lat = coordinate.getLatitude();
                                                        double lng = coordinate.getLongitude();
                                                        trackBuilder.addLocation(CoreFactory.createLocation(lat, lng));
                                                        builder.append("{" + lat + HereManeuverDisplayBuilder.COMMA + lng + "} ");
                                                    }
                                                }
                                            }
                                        }
                                        GTrack track = trackBuilder.getTrack();
                                        GlympseManager.logger.v("update ETA track = " + builder.toString());
                                        GlympseManager.logger.v("update ETA time to generate track:" + (SystemClock.elapsedRealtime() - l1));
                                        final GTrack gTrack = track;
                                        GlympseManager.handler.post(new Runnable() {
                                            public void run() {
                                                long now = System.currentTimeMillis();
                                                long durationInMillis = etaDate.getTime() - now;
                                                if (durationInMillis < 0) {
                                                    durationInMillis = 0;
                                                }
                                                long minutes = durationInMillis / 60000;
                                                GArray<GTicket> activeTickets = GlympseManager.this.glympse.getHistoryManager().getTickets();
                                                int ticketListLength = activeTickets.length();
                                                for (int i = 0; i < ticketListLength; i++) {
                                                    GTicket ticket = (GTicket) activeTickets.at(i);
                                                    if (!ticket.isActive()) {
                                                        GlympseManager.logger.v("update ETA ticket not active:" + ticket.getId());
                                                    } else if (ticket.getDestination() == null) {
                                                        GlympseManager.logger.v("update ETA ticket not for trip:" + ticket.getId());
                                                    } else {
                                                        GlympseManager.logger.v("update ETA for ticket with id[" + ticket.getId() + "] durationMillis[" + durationInMillis + "] ETA[" + etaDate + "] currentTime=" + now + " minutes[" + minutes + "] expireTime[" + new Date(ticket.getExpireTime()) + "]");
                                                        ticket.updateEta(durationInMillis);
                                                        ticket.updateRoute(gTrack);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }, 1);
                    GlympseManager.handler.postDelayed(this, (long) GlympseManager.UPDATE_ETA_INTERVAL);
                    return;
                }
                GlympseManager.logger.v("update ETA: glympse not synched");
            }
        };
        this.notificationManager = NotificationManager.getInstance();
        this.ticketsAwaitingReadReceipt = new HashMap();
        try {
            logger.v("creating glympse");
            this.glympse = GlympseFactory.createGlympse(HudApplication.getAppContext(), GLYMPSE_BASE_URL, GLYMPSE_API_KEY);
            setUpEventListener();
            this.glympse.setEtaMode(1);
            this.glympse.start();
            logger.v("calling glympse start duration=" + (((long) MapSettings.getGlympseDuration()) / TimeUnit.MINUTES.toMillis(1)) + " minutes");
        } catch (Throwable t) {
            logger.e(t);
            this.glympseInitFailed = true;
        }
        RemoteDeviceManager.getInstance().getBus().register(this);
    }

    public Error addMessage(Contact contact, String message, @Nullable String destinationLabel, double latitude, double longitude, StringBuilder id) {
        if (this.glympseInitFailed) {
            logger.e("glympse init failed, cannot add message, no-op");
            return Error.INTERNAL_ERROR;
        } else if (NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
            GPlace place = null;
            boolean hasRouteData = (TextUtils.isEmpty(destinationLabel) || (latitude == 0.0d && longitude == 0.0d)) ? false : true;
            if (hasRouteData) {
                place = GlympseFactory.createPlace(latitude, longitude, CoreFactory.createString(destinationLabel));
                startTrackingETA();
            } else {
                stopTrackingETA();
            }
            GTicket ticket = GlympseFactory.createTicket(MapSettings.getGlympseDuration(), message, place);
            String e164Number = PhoneUtil.convertToE164Format(contact.number);
            ticket.addInvite(GlympseFactory.createInvite(3, contact.name, e164Number));
            String uuid = UUID.randomUUID().toString();
            id.setLength(0);
            id.append(uuid);
            this.ticketsAwaitingReadReceipt.put(uuid, contact);
            ticket.appendData(1, NAVDY_CONTACT_UUID, CoreFactory.createPrimitive(uuid));
            this.glympse.sendTicket(ticket);
            logger.v("addMessage, added ticket name[" + contact.name + "] e164Number[" + e164Number + "] number[" + contact.number + "] uuid[" + uuid + "]");
            return Error.NONE;
        } else {
            logger.v("addMessage, no connection, no-op");
            return Error.NO_INTERNET;
        }
    }

    public boolean isSynced() {
        return !this.glympseInitFailed && this.glympse.getHistoryManager().isSynced();
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged event) {
        if (isSynced()) {
            setUserProfile();
        }
    }

    private void setUserProfile() {
        DriverProfile driverProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        if (TextUtils.equals(this.currentGlympseUserId, driverProfile.getProfileName())) {
            logger.v("called setUserProfile with same profile as current, no-op");
            return;
        }
        GUser user = this.glympse.getUserManager().getSelf();
        if (driverProfile.isDefaultProfile()) {
            logger.v("driverProfile is default, not setting it for Glympse");
            return;
        }
        deleteActiveTickets();
        this.ticketsAwaitingReadReceipt.clear();
        logger.v("setting new user profile for Glympse: " + driverProfile.getProfileName());
        if (!TextUtils.isEmpty(driverProfile.getFirstName())) {
            user.setNickname(driverProfile.getFirstName());
        }
        if (driverProfile.getDriverImage() != null) {
            logger.v("setting photo");
            user.setAvatar(CoreFactory.createDrawable(driverProfile.getDriverImage()));
        } else {
            logger.v("not setting photo");
            user.setAvatar(null);
        }
        this.currentGlympseUserId = driverProfile.getProfileName();
    }

    private void startTrackingETA() {
        if (!this.isTrackingETA) {
            this.isTrackingETA = true;
            handler.post(this.updateETA);
        }
    }

    private void stopTrackingETA() {
        handler.removeCallbacks(this.updateETA);
        this.isTrackingETA = false;
    }

    private void deleteActiveTickets() {
        logger.v("deleteActiveTickets");
        stopTrackingETA();
        GArray<GTicket> activeTickets = this.glympse.getHistoryManager().getTickets();
        int ticketListLength = activeTickets.length();
        for (int i = 0; i < ticketListLength; i++) {
            GTicket ticket = (GTicket) activeTickets.at(i);
            logger.v("deleteActiveTickets deleting ticket with id " + ticket.getId());
            ticket.deleteTicket();
        }
    }

    public void expireActiveTickets() {
        if (isSynced()) {
            logger.v("expireActiveTickets");
            stopTrackingETA();
            GArray<GTicket> activeTickets = this.glympse.getHistoryManager().getTickets();
            int ticketListLength = activeTickets.length();
            int i = 0;
            while (i < ticketListLength) {
                GTicket ticket = (GTicket) activeTickets.at(i);
                if (ticket.isActive()) {
                    logger.v("expireActiveTickets ticket[ " + ticket.getId() + "] expiryTime[" + new Date(ticket.getExpireTime()) + "]");
                    ticket.expire();
                    i++;
                } else {
                    logger.v("expireActiveTickets ticket[" + ticket.getId() + "] not active");
                    return;
                }
            }
        }
    }

    private boolean hasGlympseEvent(int receivedEvents, int glympseEvent) {
        return (receivedEvents & glympseEvent) != 0;
    }

    private void setUpEventListener() {
        this.glympse.addListener(new GEventListener() {
            public void eventsOccurred(GGlympse gGlympse, int listener, int events, Object obj) {
                if (GlympseManager.this.hasGlympseEvent(events, 128)) {
                    GlympseManager.logger.v("synced with server, setting user profile");
                    GlympseManager.this.setUserProfile();
                }
                if (GlympseManager.this.hasGlympseEvent(events, 131072)) {
                    GlympseManager.this.addTicketListener((GTicket) obj);
                }
            }
        });
    }

    private void addTicketListener(final GTicket ticket) {
        ticket.addListener(new GEventListener() {
            public void eventsOccurred(GGlympse gGlympse, int listener, int events, Object obj) {
                String ticketId;
                String contactUuid;
                GTicket updatedTicket = (GTicket) obj;
                if (GlympseManager.this.hasGlympseEvent(events, 8192)) {
                    ticketId = updatedTicket.getId();
                    contactUuid = updatedTicket.getProperty(1, GlympseManager.NAVDY_CONTACT_UUID).getString();
                    GlympseManager.logger.v("ticket added, ticket id is " + ticketId + "; contact uuid is " + contactUuid);
                    if (!TextUtils.isEmpty(contactUuid) && GlympseManager.this.ticketsAwaitingReadReceipt.containsKey(contactUuid)) {
                        GlympseManager.this.addGlympseSentGlance((Contact) GlympseManager.this.ticketsAwaitingReadReceipt.get(contactUuid));
                    }
                }
                if (GlympseManager.this.hasGlympseEvent(events, 16384)) {
                    ticketId = updatedTicket.getId();
                    contactUuid = updatedTicket.getProperty(1, GlympseManager.NAVDY_CONTACT_UUID).getString();
                    GlympseManager.logger.v("ticket updated, ticket id is " + ticketId + "; contact uuid is " + contactUuid);
                    if (GlympseManager.this.ticketsAwaitingReadReceipt.containsKey(contactUuid)) {
                        Contact contact = (Contact) GlympseManager.this.ticketsAwaitingReadReceipt.remove(contactUuid);
                        GlympseManager.this.addGlympseReadGlance(contact);
                        ticket.removeListener(this);
                        if (contactUuid == null) {
                            contactUuid = "";
                        }
                        AnalyticsSupport.recordGlympseViewed(contactUuid);
                        GlympseManager.logger.v("Glympse viewed ticket id is " + ticketId + " contact[" + contact.name + "] number[" + contact.number + "] e164[" + PhoneUtil.convertToE164Format(contact.number) + "]");
                    }
                }
            }
        });
    }

    private void addGlympseSentGlance(Contact contact) {
        this.notificationManager.addNotification(new GlympseNotification(contact, Type.SENT));
    }

    private void addGlympseReadGlance(Contact contact) {
        this.notificationManager.addNotification(new GlympseNotification(contact, Type.READ));
    }

    public String[] getMessages() {
        return messages;
    }
}
