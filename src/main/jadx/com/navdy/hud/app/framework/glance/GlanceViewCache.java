package com.navdy.hud.app.framework.glance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.navdy.hud.app.R;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class GlanceViewCache {
    private static final boolean VERBOSE = false;
    private static ArrayList<View> bigCalendarViewCache = new ArrayList(ViewType.BIG_CALENDAR.cacheSize);
    private static ArrayList<View> bigGlanceMessageSingleViewCache = new ArrayList(ViewType.BIG_GLANCE_MESSAGE_SINGLE.cacheSize);
    private static ArrayList<View> bigGlanceMessageViewCache = new ArrayList(ViewType.BIG_GLANCE_MESSAGE.cacheSize);
    private static ArrayList<View> bigMultiTextViewCache = new ArrayList(ViewType.BIG_MULTI_TEXT.cacheSize);
    private static ArrayList<View> bigTextViewCache = new ArrayList(ViewType.BIG_TEXT.cacheSize);
    private static HashMap<ViewType, ArrayList<View>> cachedViewMap = new HashMap();
    private static final Logger sLogger = new Logger(GlanceViewCache.class);
    private static ArrayList<View> smallGlanceMessageViewCache = new ArrayList(ViewType.SMALL_GLANCE_MESSAGE.cacheSize);
    private static ArrayList<View> smallImageViewCache = new ArrayList(ViewType.SMALL_IMAGE.cacheSize);
    private static ArrayList<View> smallSignViewCache = new ArrayList(ViewType.SMALL_SIGN.cacheSize);
    private static HashSet<View> viewSet = new HashSet();

    public enum ViewType {
        SMALL_GLANCE_MESSAGE(R.layout.glance_small_message, 2),
        BIG_GLANCE_MESSAGE(R.layout.glance_large_message, 2),
        BIG_GLANCE_MESSAGE_SINGLE(R.layout.glance_large_message_single, 2),
        SMALL_IMAGE(R.layout.glance_small_image, 2),
        BIG_TEXT(R.layout.glance_large_text, 2),
        BIG_MULTI_TEXT(R.layout.glance_large_multi_text, 2),
        SMALL_SIGN(R.layout.glance_small_sign, 2),
        BIG_CALENDAR(R.layout.glance_large_calendar, 2);
        
        final int cacheSize;
        final int layoutId;

        private ViewType(int layoutId, int cacheSize) {
            this.layoutId = layoutId;
            this.cacheSize = cacheSize;
        }
    }

    static {
        cachedViewMap.put(ViewType.SMALL_GLANCE_MESSAGE, smallGlanceMessageViewCache);
        cachedViewMap.put(ViewType.BIG_GLANCE_MESSAGE, bigGlanceMessageViewCache);
        cachedViewMap.put(ViewType.BIG_GLANCE_MESSAGE_SINGLE, bigGlanceMessageSingleViewCache);
        cachedViewMap.put(ViewType.SMALL_IMAGE, smallImageViewCache);
        cachedViewMap.put(ViewType.BIG_TEXT, bigTextViewCache);
        cachedViewMap.put(ViewType.BIG_MULTI_TEXT, bigMultiTextViewCache);
        cachedViewMap.put(ViewType.SMALL_SIGN, smallSignViewCache);
        cachedViewMap.put(ViewType.BIG_CALENDAR, bigCalendarViewCache);
    }

    public static View getView(ViewType viewType, Context context) {
        View cachedView = null;
        ArrayList<View> list = (ArrayList) cachedViewMap.get(viewType);
        if (list.size() > 0) {
            cachedView = (View) list.remove(0);
            viewSet.remove(cachedView);
            if (cachedView.getParent() != null) {
                sLogger.e(":-/ view already has parent:" + viewType);
            }
        }
        if (cachedView == null) {
            cachedView = LayoutInflater.from(context).inflate(viewType.layoutId, null);
            if (sLogger.isLoggable(2)) {
                sLogger.v(" creating view for " + viewType);
            }
        } else if (sLogger.isLoggable(2)) {
            sLogger.v(" reusing cache for " + viewType);
        }
        return cachedView;
    }

    public static void putView(ViewType viewType, View view) {
        if (view != null) {
            if (viewSet.contains(view)) {
                sLogger.e(":-/ view already in cache:" + viewType);
                return;
            }
            if (view.getParent() != null) {
                sLogger.e(":-/ view already has parent:" + viewType);
            }
            view.setTranslationX(0.0f);
            view.setTranslationY(0.0f);
            view.setTag(null);
            ArrayList<View> list = (ArrayList) cachedViewMap.get(viewType);
            if (list.size() < viewType.cacheSize) {
                list.add(view);
                viewSet.add(view);
            }
            if (sLogger.isLoggable(2)) {
                sLogger.v(" putView cache for " + viewType);
            }
        }
    }

    public static void clearCache() {
        smallGlanceMessageViewCache.clear();
        bigGlanceMessageViewCache.clear();
        bigGlanceMessageSingleViewCache.clear();
        smallImageViewCache.clear();
        bigTextViewCache.clear();
        bigMultiTextViewCache.clear();
        smallSignViewCache.clear();
        bigCalendarViewCache.clear();
        viewSet.clear();
    }

    public static boolean supportScroll(ViewType viewType) {
        switch (viewType) {
            case BIG_GLANCE_MESSAGE:
                return true;
            default:
                return false;
        }
    }
}
