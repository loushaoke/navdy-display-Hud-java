package com.navdy.hud.app.framework.music;

import com.navdy.hud.app.framework.music.MusicDetailsScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class MusicDetailsView$$InjectAdapter extends Binding<MusicDetailsView> implements MembersInjector<MusicDetailsView> {
    private Binding<Presenter> presenter;

    public MusicDetailsView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.framework.music.MusicDetailsView", false, MusicDetailsView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.framework.music.MusicDetailsScreen$Presenter", MusicDetailsView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(MusicDetailsView object) {
        object.presenter = (Presenter) this.presenter.get();
    }
}
