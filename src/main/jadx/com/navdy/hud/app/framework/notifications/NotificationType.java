package com.navdy.hud.app.framework.notifications;

public enum NotificationType {
    GLYMPSE(100),
    SMS(100),
    PHONE_CALL(99),
    ROUTE_CALC(98),
    PLACE_TYPE_SEARCH(98),
    VOICE_SEARCH(98),
    FASTER_ROUTE(97),
    ETA_DELAY(96),
    TRAFFIC_EVENT(95),
    TRAFFIC_JAM(94),
    LOW_FUEL(97),
    GLANCE(50),
    MUSIC(1),
    VOICE_ASSIST(1),
    BRIGHTNESS(1);
    
    int priority;

    private NotificationType(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return this.priority;
    }
}
