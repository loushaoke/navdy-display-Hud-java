package com.navdy.hud.app.framework.places;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.search.Address;
import com.here.android.mpa.search.CategoryFilter;
import com.here.android.mpa.search.NavigationPosition;
import com.here.android.mpa.search.Place;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.here.HerePlacesManager;
import com.navdy.hud.app.maps.here.HerePlacesManager.Error;
import com.navdy.hud.app.maps.here.HerePlacesManager.OnCategoriesSearchListener;
import com.navdy.hud.app.maps.util.DestinationUtil;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout2.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout2.Selection;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable;
import com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.DestinationPickerState;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder;
import com.navdy.hud.app.ui.component.destination.IDestinationPicker;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.destination.Destination.SuggestionType;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.places.PlaceTypeSearchRequest;
import com.navdy.service.library.events.places.PlaceTypeSearchRequest.Builder;
import com.navdy.service.library.events.places.PlaceTypeSearchResponse;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class NearbyPlaceSearchNotification implements INotification, IListener {
    private static final float ICON_SCALE = 1.38f;
    private static final int N_OFFLINE_RESULTS = 7;
    private static final long TIMEOUT = 10000;
    private static final Map<PlaceTypeSearchState, List<Choice>> choicesMapping = new HashMap(4);
    private static final Logger logger = new Logger(NearbyPlaceSearchNotification.class);
    private static final String searchAgain;
    private static final int searchColor;
    private static final int secondaryColor;
    private final Bus bus = RemoteDeviceManager.getInstance().getBus();
    @InjectView(R.id.choiceLayout)
    ChoiceLayout2 choiceLayout;
    private INotificationController controller;
    private PlaceTypeSearchState currentState;
    private final Handler handler = new Handler(Looper.getMainLooper());
    @InjectView(R.id.iconColorView)
    IconColorImageView iconColorView;
    @InjectView(R.id.iconSide)
    IconColorImageView iconSide;
    private ObjectAnimator loadingAnimator;
    private final NotificationManager notificationManager = NotificationManager.getInstance();
    private final PlaceType placeType;
    private final String requestId = UUID.randomUUID().toString();
    @InjectView(R.id.resultsCount)
    TextView resultsCount;
    @InjectView(R.id.resultsLabel)
    TextView resultsLabel;
    private List<Destination> returnedDestinations;
    private final SpeedManager speedManager = SpeedManager.getInstance();
    @InjectView(R.id.iconSpinner)
    ImageView statusBadge;
    @InjectView(R.id.subTitle)
    TextView subTitle;
    private final Runnable timeoutForFailure = new Runnable() {
        public void run() {
            if (NearbyPlaceSearchNotification.this.controller != null) {
                NearbyPlaceSearchNotification.this.stopLoadingAnimation();
                NearbyPlaceSearchNotification.this.goToFailedState();
            }
        }
    };
    @InjectView(R.id.title)
    TextView title;

    private enum PlaceTypeSearchState {
        SEARCHING,
        ERROR,
        NO_RESULTS,
        SEARCH_COMPLETE
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        String dismiss = resources.getString(R.string.dismiss);
        int dismissColor = resources.getColor(R.color.glance_dismiss);
        int retryColor = resources.getColor(R.color.glance_ok_blue);
        Choice dismissChoice = new Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, dismiss, dismissColor);
        Choice retryChoice = new Choice(R.id.retry, R.drawable.icon_glances_retry, retryColor, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), retryColor);
        List<Choice> searchingChoices = new ArrayList();
        List<Choice> list = searchingChoices;
        list.add(new Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, dismissColor, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.cancel), dismissColor));
        List<Choice> errorChoices = new ArrayList();
        errorChoices.add(retryChoice);
        errorChoices.add(dismissChoice);
        List<Choice> noResultsChoices = new ArrayList();
        noResultsChoices.add(retryChoice);
        noResultsChoices.add(dismissChoice);
        int readColor = resources.getColor(R.color.glance_ok_blue);
        List<Choice> searchCompleteChoices = new ArrayList();
        list = searchCompleteChoices;
        list.add(new Choice(R.id.dismiss, R.drawable.icon_glances_read, readColor, R.drawable.icon_glances_read, -16777216, resources.getString(R.string.view), readColor));
        searchCompleteChoices.add(dismissChoice);
        secondaryColor = resources.getColor(R.color.place_type_search_secondary_color);
        choicesMapping.put(PlaceTypeSearchState.SEARCHING, searchingChoices);
        choicesMapping.put(PlaceTypeSearchState.ERROR, errorChoices);
        choicesMapping.put(PlaceTypeSearchState.NO_RESULTS, noResultsChoices);
        choicesMapping.put(PlaceTypeSearchState.SEARCH_COMPLETE, searchCompleteChoices);
        searchColor = resources.getColor(R.color.mm_search);
        searchAgain = resources.getString(R.string.search_again);
    }

    public NearbyPlaceSearchNotification(PlaceType placeType) {
        this.placeType = placeType;
    }

    public NotificationType getType() {
        return NotificationType.PLACE_TYPE_SEARCH;
    }

    public String getId() {
        return NotificationId.PLACE_TYPE_SEARCH_NOTIFICATION_ID;
    }

    public View getView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.notification_place_type_search, null);
        ButterKnife.inject( this, view);
        PlaceTypeResourceHolder placeTypeResourceHolder = DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        this.title.setText(context.getString(placeTypeResourceHolder.titleRes));
        this.iconColorView.setIcon(placeTypeResourceHolder.iconRes, context.getResources().getColor(placeTypeResourceHolder.colorRes), null, ICON_SCALE);
        this.choiceLayout.setVisibility(0);
        this.choiceLayout.setChoices((List) choicesMapping.get(PlaceTypeSearchState.SEARCHING), 0, this);
        return view;
    }

    public View getExpandedView(Context context, Object data) {
        return null;
    }

    public int getExpandedViewIndicatorColor() {
        return 0;
    }

    public void onStart(INotificationController controller) {
        this.controller = controller;
        this.bus.register(this);
        startLoadingAnimation();
        sendRequest();
    }

    public void onUpdate() {
    }

    public void onStop() {
        this.controller = null;
        this.bus.unregister(this);
    }

    public int getTimeout() {
        return 0;
    }

    public boolean isAlive() {
        return false;
    }

    public boolean isPurgeable() {
        return false;
    }

    public boolean canAddToStackIfCurrentExists() {
        return true;
    }

    public int getColor() {
        return 0;
    }

    public void onNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationEvent(Mode mode) {
    }

    public void onExpandedNotificationSwitched() {
    }

    public AnimatorSet getViewSwitchAnimation(boolean viewIn) {
        return null;
    }

    public boolean expandNotification() {
        return false;
    }

    public boolean supportScroll() {
        return false;
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        if (this.controller == null || this.currentState != PlaceTypeSearchState.SEARCH_COMPLETE) {
            return false;
        }
        switch (event.gesture) {
            case GESTURE_SWIPE_LEFT:
                logger.v("show route picker:gesture");
                launchPicker();
                return true;
            default:
                return false;
        }
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.controller == null) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                return true;
            default:
                return false;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    private void recordSelection(int pos, int id) {
        switch (this.currentState) {
            case SEARCHING:
                if (id == R.id.dismiss) {
                    AnalyticsSupport.recordNearbySearchDismiss();
                    return;
                }
                return;
            case NO_RESULTS:
                AnalyticsSupport.recordNearbySearchNoResult(id == R.id.retry);
                return;
            case SEARCH_COMPLETE:
                if (id != R.id.dismiss) {
                    return;
                }
                if (pos == 0) {
                    AnalyticsSupport.recordNearbySearchResultsView();
                    return;
                } else {
                    AnalyticsSupport.recordNearbySearchResultsDismiss();
                    return;
                }
            default:
                return;
        }
    }

    public void executeItem(Selection selection) {
        recordSelection(selection.pos, selection.id);
        switch (selection.id) {
            case R.id.dismiss:
                if (this.currentState == PlaceTypeSearchState.SEARCH_COMPLETE) {
                    if (selection.pos == 0) {
                        launchPicker();
                        return;
                    } else {
                        dismissNotification();
                        return;
                    }
                }
                dismissNotification();
                return;
            case R.id.retry:
                startLoadingAnimation();
                sendRequest();
                return;
            default:
                return;
        }
    }

    public void itemSelected(Selection selection) {
    }

    @Subscribe
    public void onPlaceTypeSearchResponse(PlaceTypeSearchResponse response) {
        logger.v("PlaceTypeSearchResponse:" + response.request_status);
        this.handler.removeCallbacks(this.timeoutForFailure);
        stopLoadingAnimation();
        if (!TextUtils.equals(response.request_id, this.requestId)) {
            logger.w("received wrong request_id on the PlaceTypeSearchResponse, no-op");
        } else if (this.currentState == PlaceTypeSearchState.ERROR) {
            logger.w("received response after place type search has already failed, no-op");
        } else {
            switch (response.request_status) {
                case REQUEST_SERVICE_ERROR:
                    goToFailedState();
                    return;
                case REQUEST_NOT_AVAILABLE:
                    goToZeroResultsState();
                    return;
                case REQUEST_SUCCESS:
                    goToSuccessfulState(response.destinations);
                    return;
                default:
                    return;
            }
        }
    }

    private void launchPicker() {
        logger.v("launch picker screen");
        Context context = HudApplication.getAppContext();
        Resources resources = context.getResources();
        Bundle args = new Bundle();
        args.putBoolean(DestinationPickerScreen.PICKER_SHOW_DESTINATION_MAP, true);
        int icon = -1;
        PlaceTypeResourceHolder holder = DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        if (holder != null) {
            icon = holder.destinationIcon;
        }
        args.putInt(DestinationPickerScreen.PICKER_DESTINATION_ICON, icon);
        args.putString(DestinationPickerScreen.PICKER_LEFT_TITLE, resources.getString(R.string.quick_search));
        args.putInt(DestinationPickerScreen.PICKER_LEFT_ICON, R.drawable.icon_mm_search_2);
        args.putInt(DestinationPickerScreen.PICKER_LEFT_ICON_BKCOLOR, ContextCompat.getColor(context, R.color.mm_search));
        args.putInt(DestinationPickerScreen.PICKER_INITIAL_SELECTION, 1);
        int selectedColor = ContextCompat.getColor(context, holder.colorRes);
        int unselectedColor = ContextCompat.getColor(context, R.color.icon_bk_color_unselected);
        DestinationParcelable searchAgainItem = new DestinationParcelable(R.id.search_again, context.getString(DestinationPickerScreen.getPlaceTypeHolder(this.placeType).titleRes), searchAgain, false, null, true, null, 0.0d, 0.0d, 0.0d, 0.0d, R.drawable.icon_mm_search_2, 0, searchColor, unselectedColor, DestinationType.NONE, null);
        List<DestinationParcelable> destinations = DestinationUtil.convert(context, this.returnedDestinations, selectedColor, unselectedColor, false);
        destinations.add(0, searchAgainItem);
        DestinationParcelable[] destinationParcelables = new DestinationParcelable[destinations.size()];
        destinations.toArray(destinationParcelables);
        args.putParcelableArray(DestinationPickerScreen.PICKER_DESTINATIONS, destinationParcelables);
        NotificationManager.getInstance().removeNotification(NotificationId.PLACE_TYPE_SEARCH_NOTIFICATION_ID, true, Screen.SCREEN_DESTINATION_PICKER, args, new IDestinationPicker() {
            private boolean itemSelected = false;
            private boolean retrySelected = false;

            public boolean onItemClicked(int id, int pos, DestinationPickerState state) {
                AnalyticsSupport.recordNearbySearchSelection(NearbyPlaceSearchNotification.this.placeType, pos);
                this.itemSelected = true;
                if (id != R.id.search_again) {
                    return false;
                }
                NearbyPlaceSearchNotification.logger.v("search again");
                this.retrySelected = true;
                return true;
            }

            public boolean onItemSelected(int id, int pos, DestinationPickerState state) {
                return false;
            }

            public void onDestinationPickerClosed() {
                if (!(this.itemSelected || this.retrySelected)) {
                    AnalyticsSupport.recordNearbySearchResultsClose();
                }
                if (this.retrySelected) {
                    NotificationManager notificationManager = NotificationManager.getInstance();
                    NearbyPlaceSearchNotification notif = (NearbyPlaceSearchNotification) notificationManager.getNotification(NotificationId.PLACE_TYPE_SEARCH_NOTIFICATION_ID);
                    if (notif == null) {
                        notif = new NearbyPlaceSearchNotification(NearbyPlaceSearchNotification.this.placeType);
                    }
                    NearbyPlaceSearchNotification.logger.v("launching notif search again:" + NearbyPlaceSearchNotification.this.placeType);
                    notificationManager.addNotification(notif);
                }
            }
        });
    }

    private void sendRequest() {
        this.currentState = PlaceTypeSearchState.SEARCHING;
        PlaceTypeSearchRequest placeTypeSearchRequest = new Builder().request_id(this.requestId).place_type(this.placeType).build();
        if (RemoteDeviceManager.getInstance().isAppConnected() && NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
            logger.v("performing online quick search");
            this.bus.post(new RemoteEvent(placeTypeSearchRequest));
        } else {
            logger.v("performing offline quick search");
            performOfflineSearch();
        }
        this.handler.postDelayed(this.timeoutForFailure, TIMEOUT);
    }

    private void performOfflineSearch() {
        HerePlacesManager.handleCategoriesRequest(getHereCategoryFilter(this.placeType), 7, new OnCategoriesSearchListener() {
            public void onCompleted(List<Place> places) {
                if (NearbyPlaceSearchNotification.this.controller != null) {
                    if (NearbyPlaceSearchNotification.this.currentState == PlaceTypeSearchState.ERROR) {
                        NearbyPlaceSearchNotification.logger.w("received response after place type search has already failed, no-op");
                        return;
                    }
                    NearbyPlaceSearchNotification.logger.v("performing offline search, returned places: ");
                    for (Place place : places) {
                        NearbyPlaceSearchNotification.logger.v(place.getName());
                    }
                    final List<Destination> destinations = new ArrayList();
                    for (Place place2 : places) {
                        LatLong navigationCoords;
                        String subtitle;
                        GeoCoordinate coords = place2.getLocation().getCoordinate();
                        LatLong displayCoords = new LatLong(Double.valueOf(coords.getLatitude()), Double.valueOf(coords.getLongitude()));
                        List<NavigationPosition> accessPoints = place2.getLocation().getAccessPoints();
                        if (accessPoints.size() > 0) {
                            GeoCoordinate accessPointCoords = ((NavigationPosition) accessPoints.get(0)).getCoordinate();
                            navigationCoords = new LatLong(Double.valueOf(accessPointCoords.getLatitude()), Double.valueOf(accessPointCoords.getLongitude()));
                        } else {
                            navigationCoords = displayCoords;
                        }
                        Address placeAddress = place2.getLocation().getAddress();
                        if (placeAddress.getHouseNumber() == null || placeAddress.getStreet() == null) {
                            subtitle = placeAddress.toString();
                        } else {
                            subtitle = placeAddress.getHouseNumber() + " " + placeAddress.getStreet();
                        }
                        destinations.add(new Destination.Builder().navigation_position(navigationCoords).display_position(displayCoords).full_address(place2.getLocation().getAddress().toString()).destination_title(place2.getName()).destination_subtitle(subtitle).favorite_type(FavoriteType.FAVORITE_NONE).identifier(UUID.randomUUID().toString()).suggestion_type(SuggestionType.SUGGESTION_NONE).is_recommendation(Boolean.valueOf(false)).last_navigated_to(Long.valueOf(0)).place_type(NearbyPlaceSearchNotification.this.placeType).build());
                    }
                    NearbyPlaceSearchNotification.this.handler.post(new Runnable() {
                        public void run() {
                            if (NearbyPlaceSearchNotification.this.controller != null) {
                                NearbyPlaceSearchNotification.this.handler.removeCallbacks(NearbyPlaceSearchNotification.this.timeoutForFailure);
                                NearbyPlaceSearchNotification.this.stopLoadingAnimation();
                                if (destinations.size() > 0) {
                                    NearbyPlaceSearchNotification.this.goToSuccessfulState(destinations);
                                } else {
                                    NearbyPlaceSearchNotification.this.goToZeroResultsState();
                                }
                            }
                        }
                    });
                }
            }

            public void onError(Error error) {
                if (NearbyPlaceSearchNotification.this.controller != null) {
                    NearbyPlaceSearchNotification.logger.w("error while performing offline search: " + error.name());
                    NearbyPlaceSearchNotification.this.handler.post(new Runnable() {
                        public void run() {
                            if (NearbyPlaceSearchNotification.this.controller != null) {
                                NearbyPlaceSearchNotification.this.handler.removeCallbacks(NearbyPlaceSearchNotification.this.timeoutForFailure);
                                NearbyPlaceSearchNotification.this.stopLoadingAnimation();
                                NearbyPlaceSearchNotification.this.goToFailedState();
                            }
                        }
                    });
                }
            }
        }, true);
    }

    private CategoryFilter getHereCategoryFilter(PlaceType placeType) {
        CategoryFilter categoryFilter = new CategoryFilter();
        switch (placeType) {
            case PLACE_TYPE_GAS:
                categoryFilter.add(HerePlacesManager.HERE_PLACE_TYPE_GAS);
                break;
            case PLACE_TYPE_PARKING:
                categoryFilter.add(HerePlacesManager.HERE_PLACE_TYPE_PARKING);
                break;
            case PLACE_TYPE_RESTAURANT:
                for (String restaurantPlaceType : HerePlacesManager.HERE_PLACE_TYPE_RESTAURANT) {
                    categoryFilter.add(restaurantPlaceType);
                }
                break;
            case PLACE_TYPE_STORE:
                categoryFilter.add(HerePlacesManager.HERE_PLACE_TYPE_STORE);
                break;
            case PLACE_TYPE_COFFEE:
                categoryFilter.add(HerePlacesManager.HERE_PLACE_TYPE_COFFEE);
                break;
            case PLACE_TYPE_ATM:
                categoryFilter.add(HerePlacesManager.HERE_PLACE_TYPE_ATM);
                break;
            case PLACE_TYPE_HOSPITAL:
                categoryFilter.add(HerePlacesManager.HERE_PLACE_TYPE_HOSPITAL);
                break;
        }
        return categoryFilter;
    }

    private void goToSuccessfulState(List<Destination> destinations) {
        Resources resources = HudApplication.getAppContext().getResources();
        this.iconColorView.setIcon(0, secondaryColor, null, ICON_SCALE);
        this.resultsCount.setVisibility(0);
        this.resultsCount.setText(String.valueOf(destinations.size()));
        this.resultsLabel.setVisibility(0);
        PlaceTypeResourceHolder holder = DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        int badgeRes = holder.iconRes;
        int badgeColor = resources.getColor(holder.colorRes);
        if (badgeRes != 0) {
            this.statusBadge.setVisibility(8);
            this.iconSide.setIcon(badgeRes, badgeColor, null, 0.5f);
            this.iconSide.setVisibility(0);
        }
        this.currentState = PlaceTypeSearchState.SEARCH_COMPLETE;
        this.choiceLayout.setChoices((List) choicesMapping.get(this.currentState), 0, this);
        this.returnedDestinations = destinations;
    }

    private void goToZeroResultsState() {
        Resources resources = HudApplication.getAppContext().getResources();
        this.iconColorView.setIcon(0, secondaryColor, null, ICON_SCALE);
        this.resultsCount.setVisibility(0);
        this.resultsCount.setText(resources.getString(R.string.zero));
        this.resultsLabel.setVisibility(0);
        PlaceTypeResourceHolder holder = DestinationPickerScreen.getPlaceTypeHolder(this.placeType);
        int badgeRes = holder.iconRes;
        int badgeColor = resources.getColor(holder.colorRes);
        if (badgeRes != 0) {
            this.statusBadge.setVisibility(8);
            this.iconSide.setIcon(badgeRes, badgeColor, null, 0.5f);
            this.iconSide.setVisibility(0);
        }
        this.currentState = PlaceTypeSearchState.NO_RESULTS;
        this.choiceLayout.setChoices((List) choicesMapping.get(this.currentState), 0, this);
    }

    private void goToFailedState() {
        this.iconColorView.setIcon(DestinationPickerScreen.getPlaceTypeHolder(this.placeType).iconRes, secondaryColor, null, ICON_SCALE);
        this.subTitle.setText(R.string.place_type_search_failed);
        this.statusBadge.setImageResource(R.drawable.icon_badge_alert);
        this.statusBadge.setVisibility(0);
        this.iconSide.setVisibility(8);
        this.currentState = PlaceTypeSearchState.ERROR;
        this.choiceLayout.setChoices((List) choicesMapping.get(this.currentState), 0, this);
    }

    private void dismissNotification() {
        this.notificationManager.removeNotification(NotificationId.PLACE_TYPE_SEARCH_NOTIFICATION_ID);
    }

    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            this.statusBadge.setImageResource(R.drawable.loader_circle);
            this.statusBadge.setVisibility(0);
            this.iconSide.setVisibility(8);
            this.loadingAnimator = ObjectAnimator.ofFloat(this.statusBadge, View.ROTATION, new float[]{360.0f});
            this.loadingAnimator.setDuration(500);
            this.loadingAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener(new DefaultAnimationListener() {
                public void onAnimationEnd(Animator animation) {
                    if (NearbyPlaceSearchNotification.this.loadingAnimator != null) {
                        NearbyPlaceSearchNotification.this.loadingAnimator.setStartDelay(33);
                        NearbyPlaceSearchNotification.this.loadingAnimator.start();
                        return;
                    }
                    NearbyPlaceSearchNotification.logger.v("abandon loading animation");
                }
            });
        }
        if (!this.loadingAnimator.isRunning()) {
            logger.v("started loading animation");
            this.loadingAnimator.start();
        }
    }

    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            logger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.statusBadge.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
}
