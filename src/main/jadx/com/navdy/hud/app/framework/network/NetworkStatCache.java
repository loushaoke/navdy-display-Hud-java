package com.navdy.hud.app.framework.network;

import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class NetworkStatCache {
    private DnsCache dnsCache;
    private HashMap<Integer, NetworkStatCacheInfo> networkStatMap = new HashMap();
    private HashMap<String, NetworkStatCacheInfo> networkStatMapBoot = new HashMap();

    public static class NetworkStatCacheInfo {
        public String destIP;
        public int rxBytes;
        public int txBytes;

        NetworkStatCacheInfo() {
        }

        NetworkStatCacheInfo(int txBytes, int rxBytes, String destIP) {
            this.txBytes = txBytes;
            this.rxBytes = rxBytes;
            this.destIP = destIP;
        }
    }

    NetworkStatCache(DnsCache dnsCache) {
        this.dnsCache = dnsCache;
    }

    public synchronized void addStat(String destIP, int txBytes, int rxBytes, int fd) {
        NetworkStatCacheInfo stat = (NetworkStatCacheInfo) this.networkStatMap.get(Integer.valueOf(fd));
        if (stat == null) {
            this.networkStatMap.put(Integer.valueOf(fd), new NetworkStatCacheInfo(txBytes, rxBytes, destIP));
        } else {
            stat.txBytes += txBytes;
            stat.rxBytes += rxBytes;
        }
        if (destIP != null) {
            NetworkStatCacheInfo bootstat = (NetworkStatCacheInfo) this.networkStatMapBoot.get(destIP);
            if (bootstat == null) {
                this.networkStatMapBoot.put(destIP, new NetworkStatCacheInfo(txBytes, rxBytes, destIP));
            } else {
                bootstat.txBytes += txBytes;
                bootstat.rxBytes += rxBytes;
            }
        }
    }

    public synchronized void clear() {
        this.networkStatMap.clear();
    }

    public synchronized void dump(Logger logger, boolean clearAll) {
        dumpSessionStat(logger, clearAll);
        dumpBootStat(logger);
    }

    private void dumpSessionStat(Logger logger, boolean clearAll) {
        if (this.networkStatMap.size() != 0) {
            logger.v("dump-session total connections:" + this.networkStatMap.size());
            for (Integer intValue : this.networkStatMap.keySet()) {
                int fd = intValue.intValue();
                NetworkStatCacheInfo info = (NetworkStatCacheInfo) this.networkStatMap.get(Integer.valueOf(fd));
                String host = this.dnsCache.getHostnamefromIP(info.destIP);
                if (host == null) {
                    host = info.destIP;
                }
                logger.v("dump-session fd[" + fd + "] dest[" + host + "] tx[" + info.txBytes + "] rx[" + info.rxBytes + "]");
            }
            if (clearAll) {
                clear();
            }
        }
    }

    private void dumpBootStat(Logger logger) {
        if (this.networkStatMapBoot.size() != 0) {
            logger.v("dump-boot total endpoints:" + this.networkStatMapBoot.size());
            for (NetworkStatCacheInfo bootStat : this.networkStatMapBoot.values()) {
                String host = this.dnsCache.getHostnamefromIP(bootStat.destIP);
                if (host == null) {
                    host = bootStat.destIP;
                }
                logger.v("dump-boot  dest[" + host + "] tx[" + bootStat.txBytes + "] rx[" + bootStat.rxBytes + "]");
            }
        }
    }

    public synchronized List<NetworkStatCacheInfo> getSessionStat() {
        return getStat(this.networkStatMap.values().iterator());
    }

    public synchronized List<NetworkStatCacheInfo> getBootStat() {
        return getStat(this.networkStatMapBoot.values().iterator());
    }

    public synchronized List<NetworkStatCacheInfo> getStat(Iterator<NetworkStatCacheInfo> iterator) {
        List<NetworkStatCacheInfo> list;
        list = new ArrayList();
        while (iterator.hasNext()) {
            NetworkStatCacheInfo info = (NetworkStatCacheInfo) iterator.next();
            String host = this.dnsCache.getHostnamefromIP(info.destIP);
            if (host == null) {
                host = info.destIP;
            }
            list.add(new NetworkStatCacheInfo(info.txBytes, info.rxBytes, host));
        }
        return list;
    }
}
