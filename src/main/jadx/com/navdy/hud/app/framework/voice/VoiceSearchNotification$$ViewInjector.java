package com.navdy.hud.app.framework.voice;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout2;

public class VoiceSearchNotification$$ViewInjector {
    public static void inject(Finder finder, VoiceSearchNotification target, Object source) {
        target.subStatus = (TextView) finder.findRequiredView(source, R.id.subStatus, "field 'subStatus'");
        target.imageInactive = (ImageView) finder.findRequiredView(source, R.id.image_voice_search_inactive, "field 'imageInactive'");
        target.imageActive = (ImageView) finder.findRequiredView(source, R.id.image_voice_search_active, "field 'imageActive'");
        target.imageStatus = (ImageView) finder.findRequiredView(source, R.id.img_status_badge, "field 'imageStatus'");
        target.imageProcessing = (ImageView) finder.findRequiredView(source, R.id.img_processing_badge, "field 'imageProcessing'");
        target.choiceLayout = (ChoiceLayout2) finder.findRequiredView(source, R.id.choiceLayout, "field 'choiceLayout'");
        target.resultsCountContainer = (ViewGroup) finder.findRequiredView(source, R.id.results_count_container, "field 'resultsCountContainer'");
        target.resultsCount = (TextView) finder.findRequiredView(source, R.id.results_count, "field 'resultsCount'");
        target.listeningFeedbackView = finder.findRequiredView(source, R.id.voice_search_listening_feedback, "field 'listeningFeedbackView'");
    }

    public static void reset(VoiceSearchNotification target) {
        target.subStatus = null;
        target.imageInactive = null;
        target.imageActive = null;
        target.imageStatus = null;
        target.imageProcessing = null;
        target.choiceLayout = null;
        target.resultsCountContainer = null;
        target.resultsCount = null;
        target.listeningFeedbackView = null;
    }
}
