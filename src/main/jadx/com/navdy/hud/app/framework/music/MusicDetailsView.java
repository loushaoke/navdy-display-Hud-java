package com.navdy.hud.app.framework.music;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.music.MusicDetailsScreen.Presenter;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import javax.inject.Inject;
import mortar.Mortar;

public class MusicDetailsView extends RelativeLayout implements IInputHandler {
    private static final int musicRightContainerLeftMargin;
    private static final int musicRightContainerSize;
    private static final Logger sLogger = new Logger(MusicDetailsView.class);
    ImageView albumArt;
    ViewGroup albumArtContainer;
    private Callback callback;
    TextView counter;
    @Inject
    public Presenter presenter;
    VerticalMenuComponent vmenuComponent;

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        musicRightContainerSize = resources.getDimensionPixelSize(R.dimen.music_details_rightC_w);
        musicRightContainerLeftMargin = resources.getDimensionPixelSize(R.dimen.music_details_rightC_left_margin);
    }

    public MusicDetailsView(Context context) {
        this(context, null);
    }

    public MusicDetailsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MusicDetailsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.callback = new Callback() {
            public void select(ItemSelectionState selection) {
                MusicDetailsView.this.presenter.selectItem(selection);
            }

            public void onLoad() {
                MusicDetailsView.sLogger.v("onLoad");
                MusicDetailsView.this.presenter.resetSelectedItem();
            }

            public boolean isItemClickable(ItemSelectionState selection) {
                return MusicDetailsView.this.presenter.isItemClickable(selection);
            }

            public void onBindToView(Model model, View view, int pos, ModelState state) {
            }

            public void onItemSelected(ItemSelectionState selection) {
            }

            public void onScrollIdle() {
            }

            public void onFastScrollStart() {
            }

            public void onFastScrollEnd() {
            }

            public void showToolTip() {
            }

            public void close() {
                MusicDetailsView.this.presenter.close();
            }

            public boolean isClosed() {
                return MusicDetailsView.this.presenter.isClosed();
            }
        };
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.vmenuComponent = new VerticalMenuComponent(this, this.callback, true);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
        MarginLayoutParams layoutParams = (MarginLayoutParams) this.vmenuComponent.rightContainer.getLayoutParams();
        layoutParams.width = musicRightContainerSize;
        layoutParams.leftMargin = musicRightContainerLeftMargin;
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        this.vmenuComponent.leftContainer.removeAllViews();
        this.albumArtContainer = (ViewGroup) layoutInflater.inflate(R.layout.music_details_album_art, this, false);
        this.albumArt = (ImageView) this.albumArtContainer.findViewById(R.id.albumArt);
        this.counter = (TextView) this.albumArtContainer.findViewById(R.id.counter);
        this.albumArt.setImageDrawable(new ColorDrawable(-12303292));
        this.vmenuComponent.leftContainer.addView(this.albumArtContainer);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        if (this.presenter != null) {
            this.presenter.dropView((View) this);
        }
    }

    public boolean onGesture(GestureEvent event) {
        return this.vmenuComponent.handleGesture(event);
    }

    public boolean onKey(CustomKeyEvent event) {
        return this.vmenuComponent.handleKey(event);
    }

    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler(this);
    }

    void performSelectionAnimation(Runnable endAction, int delay) {
        this.vmenuComponent.performSelectionAnimation(endAction, delay);
    }
}
