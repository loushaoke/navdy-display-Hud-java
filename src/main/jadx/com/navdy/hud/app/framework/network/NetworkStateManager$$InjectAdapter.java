package com.navdy.hud.app.framework.network;

import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class NetworkStateManager$$InjectAdapter extends Binding<NetworkStateManager> implements MembersInjector<NetworkStateManager> {
    private Binding<Bus> bus;

    public NetworkStateManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.framework.network.NetworkStateManager", false, NetworkStateManager.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", NetworkStateManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
    }

    public void injectMembers(NetworkStateManager object) {
        object.bus = (Bus) this.bus.get();
    }
}
