package com.navdy.hud.app.framework.calendar;

import com.squareup.otto.Bus;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class CalendarManager$$InjectAdapter extends Binding<CalendarManager> implements Provider<CalendarManager> {
    private Binding<Bus> bus;

    public CalendarManager$$InjectAdapter() {
        super("com.navdy.hud.app.framework.calendar.CalendarManager", "members/com.navdy.hud.app.framework.calendar.CalendarManager", false, CalendarManager.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", CalendarManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> set) {
        getBindings.add(this.bus);
    }

    public CalendarManager get() {
        return new CalendarManager((Bus) this.bus.get());
    }
}
