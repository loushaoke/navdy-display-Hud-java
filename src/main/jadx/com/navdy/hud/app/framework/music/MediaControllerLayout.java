package com.navdy.hud.app.framework.music;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.MusicManager.MediaControl;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.Icon;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.ui.component.UISettings;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class MediaControllerLayout extends ChoiceLayout {
    public static final HashMap<MediaControl, Integer> MEDIA_CONTROL_ICON_MAPPING = new HashMap();
    private Set<MediaControl> availableMediaControls;

    static {
        MEDIA_CONTROL_ICON_MAPPING.put(MediaControl.PLAY, Integer.valueOf(R.drawable.icon_play_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(MediaControl.PAUSE, Integer.valueOf(R.drawable.icon_pause_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(MediaControl.PREVIOUS, Integer.valueOf(R.drawable.icon_prev_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(MediaControl.NEXT, Integer.valueOf(R.drawable.icon_next_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(MediaControl.MUSIC_MENU, Integer.valueOf(R.drawable.icon_music_sm_2));
        MEDIA_CONTROL_ICON_MAPPING.put(MediaControl.MUSIC_MENU_DEEP, Integer.valueOf(R.drawable.icon_music_queue_sm));
    }

    public MediaControllerLayout(Context context) {
        super(context);
    }

    public MediaControllerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MediaControllerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void updateControls(Set<MediaControl> controls) {
        int i = 0;
        boolean changed = false;
        List<Choice> choices;
        if (controls == null || controls.size() == 0) {
            this.availableMediaControls = controls;
            setVisibility(4);
        } else if (this.availableMediaControls == null || this.availableMediaControls.size() != controls.size()) {
            this.availableMediaControls = controls;
            choices = getChoicesForControls();
            setChoices(Mode.ICON, choices, getDefaultSelection(choices), null);
            setHighlightVisibility(0);
            setVisibility(0);
        } else {
            MediaControl[] mediaControlArr = MusicManager.CONTROLS;
            int length = mediaControlArr.length;
            while (i < length) {
                MediaControl control = mediaControlArr[i];
                if (controls.contains(control) != this.availableMediaControls.contains(control)) {
                    changed = true;
                    this.availableMediaControls = controls;
                    break;
                }
                i++;
            }
            choices = getChoicesForControls();
            if (changed) {
                updateChoices(choices);
                return;
            }
            int selected = getSelectedItemIndex();
            if (UISettings.isMusicBrowsingEnabled() && selected == 0) {
                setSelectedItem(getDefaultSelection(choices));
            }
        }
    }

    private int getDefaultSelection(List<Choice> choices) {
        int selection = 0;
        for (Choice choice : choices) {
            if (choice.id == MediaControl.MUSIC_MENU.ordinal() || choice.id == MediaControl.PREVIOUS.ordinal()) {
                selection++;
            }
        }
        return selection;
    }

    private List<Choice> getChoicesForControls() {
        int iconRes;
        List<Choice> choices = new ArrayList();
        if (UISettings.isMusicBrowsingEnabled()) {
            if (RemoteDeviceManager.getInstance().getMusicManager().getMusicMenuPath() != null) {
                iconRes = ((Integer) MEDIA_CONTROL_ICON_MAPPING.get(MediaControl.MUSIC_MENU_DEEP)).intValue();
            } else {
                iconRes = ((Integer) MEDIA_CONTROL_ICON_MAPPING.get(MediaControl.MUSIC_MENU)).intValue();
            }
            choices.add(new Choice(new Icon(iconRes, iconRes), MediaControl.MUSIC_MENU.ordinal()));
        }
        if (this.availableMediaControls.contains(MediaControl.PREVIOUS)) {
            iconRes = ((Integer) MEDIA_CONTROL_ICON_MAPPING.get(MediaControl.PREVIOUS)).intValue();
            choices.add(new Choice(new Icon(iconRes, iconRes), MediaControl.PREVIOUS.ordinal()));
        }
        MediaControl control = this.availableMediaControls.contains(MediaControl.PLAY) ? MediaControl.PLAY : MediaControl.PAUSE;
        iconRes = ((Integer) MEDIA_CONTROL_ICON_MAPPING.get(control)).intValue();
        choices.add(new Choice(new Icon(iconRes, iconRes), control.ordinal()));
        if (this.availableMediaControls.contains(MediaControl.NEXT)) {
            iconRes = ((Integer) MEDIA_CONTROL_ICON_MAPPING.get(MediaControl.NEXT)).intValue();
            choices.add(new Choice(new Icon(iconRes, iconRes), MediaControl.NEXT.ordinal()));
        }
        return choices;
    }

    private void updateChoices(List<Choice> choices) {
        this.choices = choices;
        for (int i = 0; i < this.choices.size(); i++) {
            ((ImageView) this.choiceContainer.getChildAt(i)).setImageResource(((Choice) this.choices.get(i)).icon.resIdSelected);
        }
    }

    protected boolean isHighlightPersistent() {
        return true;
    }
}
