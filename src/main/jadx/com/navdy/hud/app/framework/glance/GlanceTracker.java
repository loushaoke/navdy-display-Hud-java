package com.navdy.hud.app.framework.glance;

import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.service.library.events.glances.GlanceEvent;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

class GlanceTracker {
    private static final long NOTIFICATION_SEEN_THRESHOLD = TimeUnit.MINUTES.toMillis(60);
    private static final Logger sLogger = new Logger(GlanceTracker.class);
    private HashMap<GlanceApp, List<GlanceInfo>> notificationSavedState;
    private HashMap<GlanceApp, List<GlanceInfo>> notificationSeen = new HashMap();

    private static class GlanceInfo {
        Map<String, String> data;
        GlanceEvent event;
        long time;

        GlanceInfo(long time, GlanceEvent event, Map<String, String> data) {
            this.time = time;
            this.event = event;
            this.data = data;
        }
    }

    GlanceTracker() {
    }

    synchronized long isNotificationSeen(GlanceEvent event, GlanceApp app, Map<String, String> data) {
        long ret = 0;
        synchronized (this) {
            switch (app) {
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR:
                case GOOGLE_MAIL:
                case GOOGLE_INBOX:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                    List<GlanceInfo> list = (List) this.notificationSeen.get(app);
                    if (list != null) {
                        Iterator<GlanceInfo> iterator = list.iterator();
                        while (iterator.hasNext()) {
                            GlanceInfo glanceInfo = (GlanceInfo) iterator.next();
                            if (SystemClock.elapsedRealtime() - glanceInfo.time > NOTIFICATION_SEEN_THRESHOLD) {
                                iterator.remove();
                                sLogger.v("expired notification removed [" + app + "]");
                            } else if (isNotificationSame(glanceInfo, app, data)) {
                                ret = glanceInfo.time;
                                glanceInfo.time = SystemClock.elapsedRealtime();
                                sLogger.v("notification has been seen [" + app + "] time [" + ret + "]");
                                break;
                            }
                        }
                        list.add(new GlanceInfo(SystemClock.elapsedRealtime(), event, data));
                        sLogger.v("new notification [" + app + "]");
                        break;
                    }
                    list = new ArrayList();
                    list.add(new GlanceInfo(SystemClock.elapsedRealtime(), event, data));
                    this.notificationSeen.put(app, list);
                    sLogger.v("first notification[" + app + "]");
                    break;
            }
        }
        return ret;
    }

    synchronized void clearState() {
        sLogger.v("clearState");
        this.notificationSavedState = null;
    }

    synchronized void saveState() {
        sLogger.v("saveState");
        this.notificationSavedState = this.notificationSeen;
        this.notificationSeen = new HashMap();
    }

    synchronized void restoreState() {
        sLogger.v("restoreState");
        this.notificationSeen = this.notificationSavedState;
        this.notificationSavedState = null;
        if (this.notificationSeen == null) {
            this.notificationSeen = new HashMap();
        }
    }

    private boolean isNotificationSame(GlanceInfo glanceInfo, GlanceApp app, Map<String, String> data) {
        if (data.size() != glanceInfo.data.size()) {
            return false;
        }
        for (Entry<String, String> entry : glanceInfo.data.entrySet()) {
            String val = (String) data.get(entry.getKey());
            if (val == null) {
                return false;
            }
            if (!TextUtils.equals(val, (CharSequence) entry.getValue())) {
                return false;
            }
        }
        return true;
    }
}
