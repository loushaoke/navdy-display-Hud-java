package com.navdy.hud.app.util;

import com.navdy.service.library.util.TaskQueue;

public class HudTaskQueue extends TaskQueue {
    public static final int DRIVE_LOGGING = 9;
    public static final int FILE_DOWNLOAD = 5;
    public static final int GENERAL_SERIAL = 10;
    public static final int GLANCE_SERIAL = 14;
    public static final int HERE_BACKGROUND = 2;
    public static final int HERE_BACKGROUND_SERIAL = 3;
    public static final int HERE_FUEL_MANAGER_SERIAL = 21;
    public static final int HERE_LANE_INFO_SERIAL = 15;
    public static final int HERE_MAP_ANIMATION_SERIAL = 17;
    public static final int HERE_NAVIGATION_MANAGER_SERIAL = 20;
    public static final int HERE_POSITION_SERIAL = 18;
    public static final int HERE_ROUTE_MANAGER_SERIAL = 19;
    public static final int HERE_TBT_SERIAL = 4;
    public static final int HERE_ZOOM_SERIAL = 16;
    public static final int IMAGE_CACHE_PROCESSING = 22;
    public static final int IMAGE_DOWNLOAD = 7;
    public static final int NETWORK_SERIAL = 23;
    public static final int OBD_CONFIGURATION_SERIAL = 13;
    public static final int OBD_STATUS = 6;
    public static final int REMOTE_SEND_SERIAL = 11;
    public static final int SPEED_WARNING_SERIAL = 12;
    public static final int TRIP_TRACKING = 8;
}
