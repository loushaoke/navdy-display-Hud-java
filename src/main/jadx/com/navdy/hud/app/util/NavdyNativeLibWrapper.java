package com.navdy.hud.app.util;

import com.navdy.service.library.log.Logger;

public class NavdyNativeLibWrapper {
    private static boolean loaded;
    private static final Logger sLogger = new Logger(NavdyNativeLibWrapper.class);

    public static native void crashSegv();

    public static native void crashSegvOnNewThread();

    public static native synchronized void startCpuHog();

    public static native synchronized void stopCpuHog();

    public static synchronized void loadlibrary() {
        synchronized (NavdyNativeLibWrapper.class) {
            if (!loaded) {
                loaded = true;
                try {
                    sLogger.v("loading navdy lib");
                    System.loadLibrary("Navdy");
                    sLogger.v("loaded navdy lib");
                } catch (Throwable t) {
                    sLogger.e(t);
                }
            }
        }
        return;
    }
}
