package com.navdy.hud.app.util;

import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.View;

public class CustomDimension {
    private TypedValue mAttribute;
    private float mValue;

    public static CustomDimension getDimension(View v, TypedArray a, int index, float defValue) {
        TypedValue attributeValue;
        if (v.isInEditMode()) {
            String value = a.getString(index);
            if (value == null) {
                return new CustomDimension(defValue);
            }
            if (!value.endsWith("%") && !value.endsWith("%p")) {
                return new CustomDimension(a.getDimension(index, defValue));
            }
            attributeValue = new TypedValue();
            a.getValue(index, attributeValue);
            return new CustomDimension(attributeValue);
        }
        attributeValue = new TypedValue();
        a.getValue(index, attributeValue);
        if (attributeValue.type == 0) {
            return new CustomDimension(defValue);
        }
        if (attributeValue.type == 5) {
            return new CustomDimension(a.getDimension(index, defValue));
        }
        return new CustomDimension(attributeValue);
    }

    public static boolean hasDimension(View v, TypedArray a, int index) {
        if (!v.isInEditMode()) {
            TypedValue attributeValue = new TypedValue();
            a.getValue(index, attributeValue);
            if (attributeValue.type == 0) {
                return false;
            }
            return true;
        } else if (a.getString(index) != null) {
            return true;
        } else {
            return false;
        }
    }

    public CustomDimension(float fixedValue) {
        this.mValue = fixedValue;
    }

    public CustomDimension(TypedValue attribute) {
        this.mAttribute = attribute;
    }

    public float getSize(View v, float base, float pBase) {
        if (this.mAttribute == null) {
            return this.mValue;
        }
        switch (this.mAttribute.type) {
            case 4:
                return this.mAttribute.getFloat() * base;
            case 6:
                return this.mAttribute.getFraction(base, pBase);
            default:
                throw new IllegalArgumentException("Attribute must have type fraction or float - it is " + this.mAttribute.type);
        }
    }
}
