package com.navdy.hud.app.util;

import android.text.format.DateUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.service.library.log.Logger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    private static final int HOUR_HAND_ANGLE_PER_HOUR = 30;
    private static final int MINUTE_HAND_ANGLE_PER_MINUTE = 6;
    private static SimpleDateFormat dateLabelFormat = new SimpleDateFormat("d MMMM", Locale.US);
    private static final Logger sLogger = new Logger(DateUtil.class);

    public static Date parseIrmcDateStr(String str) {
        if (str == null) {
            return null;
        }
        try {
            if (str.length() != 15) {
                return null;
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(0);
            calendar.set(1, Integer.parseInt(str.substring(0, 4)));
            calendar.set(2, Integer.parseInt(str.substring(4, 6)) - 1);
            calendar.set(5, Integer.parseInt(str.substring(6, 8)));
            calendar.set(11, Integer.parseInt(str.substring(9, 11)));
            calendar.set(12, Integer.parseInt(str.substring(11, 13)));
            calendar.set(13, Integer.parseInt(str.substring(13, 15)));
            return calendar.getTime();
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    public static float getClockAngleForHour(int hour, int minutes) {
        return ((float) (((((hour % 12) - 3) * 30) + 360) % 360)) + ((((float) minutes) / 60.0f) * 30.0f);
    }

    public static float getClockAngleForMinutes(int minutes) {
        return (float) ((((minutes - 15) * 6) + 360) % 360);
    }

    public static String getDateLabel(Date d) {
        try {
            if (DateUtils.isToday(d.getTime())) {
                return HudApplication.getAppContext().getResources().getString(R.string.today);
            }
            Date now = new Date();
            Calendar nowCal = Calendar.getInstance();
            nowCal.setTime(now);
            Calendar dCal = Calendar.getInstance();
            dCal.setTime(d);
            int year1 = nowCal.get(1);
            int year2 = dCal.get(1);
            int dayOfYear1 = nowCal.get(6);
            int dayOfYear2 = dCal.get(6);
            if (year1 == year2 && dayOfYear1 - dayOfYear2 == 1) {
                return HudApplication.getAppContext().getResources().getString(R.string.yesterday);
            }
            String format;
            synchronized (dateLabelFormat) {
                format = dateLabelFormat.format(d);
            }
            return format;
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }
}
