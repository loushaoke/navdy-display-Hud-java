package com.navdy.hud.app.settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Handler;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings.System;
import android.util.Log;
import android.view.WindowManager.LayoutParams;
import com.navdy.hud.app.debug.DebugReceiver;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;

public class BrightnessControl implements OnSharedPreferenceChangeListener {
    public static final String AUTOBRIGHTNESSD_PROPERTY = "hw.navdy.autobrightnessd";
    public static final String AUTO_BRIGHTNESS_PROPERTY = "persist.sys.autobrightness";
    public static final String DEFAULT_AUTO_BRIGHTNESS = "false";
    public static final String DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT = "0";
    public static final String DEFAULT_BRIGHTNESS = "128";
    public static final String DEFAULT_LED_BRIGHTNESS = "255";
    public static final String DISABLED = "disabled";
    public static final String ENABLED = "enabled";
    public static final int MAX_BRIGHTNESS_ADJUSTMENT = 64;
    public static final int MIN_BRIGHTNESS_ADJUSTMENT = -64;
    private static final int MIN_INITIAL_BRIGHTNESS = 128;
    public static final String OFF = "off";
    public static final String ON = "on";
    private static final String TAG = BrightnessControl.class.getName();
    private String autoBrightnessAdjustmentKey;
    private String autoBrightnessKey;
    private String brightnessKey;
    private Bus bus;
    private Context context;
    private float lastBrightness = 0.5f;
    private String ledBrightnessKey;
    private Handler mainHandler;
    private SharedPreferences preferences;

    public BrightnessControl(Context context, Bus bus, SharedPreferences preferences, String brightnessKey, String autoBrightnessKey, String autoBrightnessAdjustmentKey, String ledBrightnessKey) {
        this.context = context;
        this.bus = bus;
        bus.register(this);
        this.preferences = preferences;
        this.mainHandler = new Handler(context.getMainLooper());
        preferences.registerOnSharedPreferenceChangeListener(this);
        this.brightnessKey = brightnessKey;
        this.autoBrightnessKey = autoBrightnessKey;
        this.autoBrightnessAdjustmentKey = autoBrightnessAdjustmentKey;
        this.ledBrightnessKey = ledBrightnessKey;
        SystemProperties.set("hw.navdy.autobrightnessd", "0");
        boolean autoBrightnessEnabled = getAutoBrightnessProperty();
        preferences.edit().putString(autoBrightnessKey, autoBrightnessEnabled ? "true" : "false").apply();
        if (!autoBrightnessEnabled) {
            if (getBrightnessPreference() < 128) {
                preferences.edit().putString(brightnessKey, String.valueOf(128)).apply();
            }
            onSharedPreferenceChanged(preferences, this.brightnessKey);
        }
        onSharedPreferenceChanged(preferences, autoBrightnessKey);
        onSharedPreferenceChanged(preferences, ledBrightnessKey);
    }

    public String getValue() {
        return Integer.toString(System.getInt(this.context.getContentResolver(), "screen_brightness", -1));
    }

    public void setBrightnessValue(final int val) {
        if (val >= 0 && val <= 255) {
            this.lastBrightness = ((float) val) / 255.0f;
            try {
                if (System.getInt(this.context.getContentResolver(), "screen_brightness_mode") != 1) {
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            System.putInt(BrightnessControl.this.context.getContentResolver(), "screen_brightness", val);
                        }
                    }, 1);
                    updateWindowBrightness(this.lastBrightness);
                }
            } catch (SettingNotFoundException e) {
                Log.e(TAG, "", e);
            }
        }
    }

    public void setLEDValue(String value) {
        int val = Integer.parseInt(value);
        if (val >= 0 && val <= 255) {
            SystemProperties.set("hw.navdy.led_max_brightness", String.format("%f", new Object[]{Float.valueOf(((float) val) / 255.0f)}));
        }
    }

    public void toggleAutoBrightness(final boolean autoBrightness) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                System.putInt(BrightnessControl.this.context.getContentResolver(), "screen_brightness_mode", autoBrightness ? 1 : 0);
                BrightnessControl.this.mainHandler.post(new Runnable() {
                    public void run() {
                        float f;
                        BrightnessControl brightnessControl = BrightnessControl.this;
                        if (autoBrightness) {
                            f = -1.0f;
                        } else {
                            f = BrightnessControl.this.lastBrightness;
                        }
                        brightnessControl.updateWindowBrightness(f);
                    }
                });
            }
        }, 1);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(this.brightnessKey)) {
            setBrightnessValue(getBrightnessPreference());
        } else if (key.equals(this.autoBrightnessAdjustmentKey)) {
            setAutoBrightnessAdjustment(getAutoBrightnessAdjustmentPreference());
        } else if (key.equals(this.autoBrightnessKey)) {
            toggleAutoBrightness(Boolean.valueOf(sharedPreferences.getString(key, "false")).booleanValue());
        } else if (key.equals(this.ledBrightnessKey)) {
            setLEDValue(sharedPreferences.getString(key, "255"));
        }
    }

    private final boolean getAutoBrightnessProperty() {
        String autoBrightness = SystemProperties.get("persist.sys.autobrightness", "on");
        return autoBrightness.equals("on") || autoBrightness.equals("enabled");
    }

    private void setAutoBrightnessAdjustment(final int val) {
        if (val >= -64 && val <= 64) {
            try {
                if (System.getInt(this.context.getContentResolver(), "screen_brightness_mode") == 1) {
                    String SCREEN_AUTO_BRIGHTNESS_ADJ = "screen_auto_brightness_adj";
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            float normalizedBrightnessAdjustment = ((float) val) / 255.0f;
                            Log.d(DebugReceiver.EXTRA_TEST, "Normalized : " + normalizedBrightnessAdjustment);
                            System.putFloat(BrightnessControl.this.context.getContentResolver(), "screen_auto_brightness_adj", normalizedBrightnessAdjustment);
                        }
                    }, 1);
                }
            } catch (SettingNotFoundException e) {
                Log.e(TAG, "", e);
            }
        }
    }

    private void updateWindowBrightness(float brightness) {
        LayoutParams layout = ((Activity) this.context).getWindow().getAttributes();
        layout.screenBrightness = brightness;
        ((Activity) this.context).getWindow().setAttributes(layout);
    }

    private int getBrightnessPreference() {
        return Integer.parseInt(this.preferences.getString(this.brightnessKey, "128"));
    }

    private int getAutoBrightnessAdjustmentPreference() {
        return Integer.parseInt(this.preferences.getString(this.autoBrightnessAdjustmentKey, "0"));
    }
}
