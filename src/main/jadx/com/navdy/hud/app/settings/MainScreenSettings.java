package com.navdy.hud.app.settings;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class MainScreenSettings implements Parcelable {
    public static final Creator<MainScreenSettings> CREATOR = new Creator<MainScreenSettings>() {
        public MainScreenSettings createFromParcel(Parcel source) {
            return new MainScreenSettings(source);
        }

        public MainScreenSettings[] newArray(int size) {
            return new MainScreenSettings[size];
        }
    };
    protected ScreenConfiguration landscapeConfiguration;
    protected ScreenConfiguration portraitConfiguration;

    public static class ScreenConfiguration implements Parcelable {
        public static final Creator<ScreenConfiguration> CREATOR = new Creator<ScreenConfiguration>() {
            public ScreenConfiguration createFromParcel(Parcel source) {
                return new ScreenConfiguration(source);
            }

            public ScreenConfiguration[] newArray(int size) {
                return new ScreenConfiguration[size];
            }
        };
        protected Rect margins;
        protected float scaleX;
        protected float scaleY;

        public ScreenConfiguration() {
            this(new Rect(), 1.0f, 1.0f);
        }

        public ScreenConfiguration(Rect margins, float scaleX, float scaleY) {
            this.margins = margins;
            this.scaleX = scaleX;
            this.scaleY = scaleY;
        }

        public ScreenConfiguration(Parcel in) {
            this((Rect) in.readParcelable(ScreenConfiguration.class.getClassLoader()), in.readFloat(), in.readFloat());
        }

        public Rect getMargins() {
            return this.margins;
        }

        public void setMargins(Rect margins) {
            this.margins = margins;
        }

        public float getScaleX() {
            return this.scaleX;
        }

        public void setScaleX(float scaleX) {
            this.scaleX = scaleX;
        }

        public float getScaleY() {
            return this.scaleY;
        }

        public void setScaleY(float scaleY) {
            this.scaleY = scaleY;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(this.margins, 0);
            dest.writeFloat(this.scaleX);
            dest.writeFloat(this.scaleY);
        }
    }

    public MainScreenSettings() {
        this(new ScreenConfiguration(), new ScreenConfiguration());
    }

    public MainScreenSettings(ScreenConfiguration portraitConfiguration, ScreenConfiguration landscapeConfiguration) {
        this.portraitConfiguration = portraitConfiguration;
        this.landscapeConfiguration = landscapeConfiguration;
    }

    public MainScreenSettings(Parcel in) {
        this((ScreenConfiguration) in.readParcelable(MainScreenSettings.class.getClassLoader()), (ScreenConfiguration) in.readParcelable(MainScreenSettings.class.getClassLoader()));
    }

    public ScreenConfiguration getPortraitConfiguration() {
        return this.portraitConfiguration;
    }

    public void setPortraitConfiguration(ScreenConfiguration portraitConfiguration) {
        this.portraitConfiguration = portraitConfiguration;
    }

    public ScreenConfiguration getLandscapeConfiguration() {
        return this.landscapeConfiguration;
    }

    public void setLandscapeConfiguration(ScreenConfiguration landscapeConfiguration) {
        this.landscapeConfiguration = landscapeConfiguration;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.portraitConfiguration, 0);
        dest.writeParcelable(this.landscapeConfiguration, 0);
    }
}
