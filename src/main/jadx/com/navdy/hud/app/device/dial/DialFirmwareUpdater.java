package com.navdy.hud.app.device.dial;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Handler;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

public class DialFirmwareUpdater {
    private static final int FIRMWARE_CHUNK_SIZE = 16;
    public static final String FIRMWARE_INCREMENTAL_VERSION_PREFIX = "Firmware incremental version: ";
    public static final String FIRMWARE_LOCAL_FILE = "/system/etc/firmware/dial.ota";
    public static final String FIRMWARE_VERSION_PREFIX = "Firmware version: ";
    public static final int LOG_PROGRESS_PERIOD = 30;
    private static final int OTA_COMMAND_BOOT_DFU = 3;
    private static final int OTA_COMMAND_ERASE_BLOCK_0 = 0;
    private static final int OTA_COMMAND_ERASE_BLOCK_1 = 1;
    private static final int OTA_COMMAND_PREPARE = 4;
    private static final int OTA_COMMAND_RESET_DFU_PTR = 2;
    private static final int OTA_STATE_CANCELLED = 1;
    private static final int OTA_STATE_NONE = 0;
    private static final int OTA_STATE_READY_TO_REBOOT = 32;
    private static final int OTA_STATE_STARTED = 16;
    private static final int RESP_ERROR_OTA_ALREADY_STARTED = 131;
    private static final int RESP_ERROR_OTA_INVALID_COMMAND = 128;
    private static final int RESP_ERROR_OTA_TOO_LARGE = 129;
    private static final int RESP_ERROR_OTA_UNKNOWN_STATE = 130;
    private static final int RESP_OK = 0;
    private static final int UNDETERMINED_FIRMWARE_VERSION = -1;
    private static final long UPDATE_FINISH_DELAY = 5000;
    private static final long UPDATE_INTERPACKET_DELAY = 50;
    private static final Logger sLogger = new Logger(DialFirmwareUpdater.class);
    private int commandSent;
    private BluetoothGattCharacteristic deviceInfoFirmwareRevisionCharacteristic;
    private final DialManager dialManager;
    private String dialName = null;
    private byte[] firmwareData;
    private int firmwareOffset;
    private Handler handler;
    private BluetoothGattCharacteristic otaControlCharacteristic;
    private BluetoothGattCharacteristic otaDataCharacteristic;
    private BluetoothGattCharacteristic otaIncrementalVersionCharacteristic;
    private Runnable sendFirmwareChunkRunnable = new Runnable() {
        public void run() {
            DialFirmwareUpdater.this.sendFirmwareChunk();
        }
    };
    private UpdateListener updateListener;
    private UpdateProgressListener updateProgressListener;
    private AtomicBoolean updateRunning = new AtomicBoolean(false);
    private Versions versions = new Versions();

    public enum Error {
        NONE,
        INVALID_IMAGE_SIZE,
        INVALID_COMMAND,
        INVALID_RESPONSE,
        CANCELLED,
        TIMEDOUT
    }

    public interface UpdateListener {
        void onUpdateState(boolean z);
    }

    public interface UpdateProgressListener {
        void onFinished(Error error, String str);

        void onProgress(int i);
    }

    public class Versions {
        public Version dial = new Version();
        public Version local = new Version();

        public class Version {
            public int incrementalVersion;
            public String revisionString;

            Version() {
                reset();
            }

            void reset() {
                this.revisionString = null;
                this.incrementalVersion = -1;
            }
        }

        public boolean isUpdateAvailable() {
            if (this.local.incrementalVersion == -1 || this.dial.incrementalVersion == -1 || this.dial.incrementalVersion >= this.local.incrementalVersion || !isUpdateOK()) {
                return false;
            }
            return true;
        }

        private boolean isUpdateOK() {
            if (this.dial.incrementalVersion >= 29) {
                return true;
            }
            DialFirmwareUpdater.sLogger.i("New dial firmware update logic is disabled (Current version < 29)");
            return false;
        }
    }

    public Versions getVersions() {
        return this.versions;
    }

    public void setUpdateListener(UpdateListener updateListener) {
        this.updateListener = updateListener;
    }

    public DialFirmwareUpdater(DialManager dialManager, Handler handler) {
        this.dialManager = dialManager;
        this.handler = handler;
        try {
            this.firmwareData = IOUtils.readBinaryFile(FIRMWARE_LOCAL_FILE);
            this.versions.local.reset();
            String v = extractVersion(this.firmwareData, FIRMWARE_INCREMENTAL_VERSION_PREFIX);
            if (v != null) {
                this.versions.local.incrementalVersion = Integer.parseInt(v);
            }
            this.versions.local.revisionString = extractVersion(this.firmwareData, FIRMWARE_VERSION_PREFIX);
        } catch (FileNotFoundException e) {
            sLogger.w("no dial firmware OTA file found");
        } catch (Throwable e2) {
            sLogger.e("can't read dial firmware OTA file", e2);
        }
    }

    private static String extractVersion(byte[] firmwareData, String prefix) {
        int j = indexOf(firmwareData, prefix.getBytes());
        if (j < 0) {
            return null;
        }
        int i = j;
        while (i < firmwareData.length) {
            if (firmwareData[i] == (byte) 0 || firmwareData[i] == (byte) 13) {
                return new String(Arrays.copyOfRange(firmwareData, prefix.length() + j, i));
            }
            i++;
        }
        return null;
    }

    public static int indexOf(byte[] array, byte[] target) {
        for (int i = 0; i < (array.length - target.length) + 1; i++) {
            boolean found = true;
            for (int j = 0; j < target.length; j++) {
                if (array[i + j] != target[j]) {
                    found = false;
                    break;
                }
            }
            if (found) {
                return i;
            }
        }
        return -1;
    }

    public boolean characteristicsAvailable() {
        return (this.otaControlCharacteristic == null || this.otaDataCharacteristic == null || this.otaIncrementalVersionCharacteristic == null || this.deviceInfoFirmwareRevisionCharacteristic == null) ? false : true;
    }

    public void onServicesDiscovered(BluetoothGatt gatt) {
        this.otaControlCharacteristic = null;
        this.otaDataCharacteristic = null;
        this.otaIncrementalVersionCharacteristic = null;
        this.deviceInfoFirmwareRevisionCharacteristic = null;
        this.dialName = this.dialManager.getDialName();
        for (BluetoothGattService service : gatt.getServices()) {
            UUID uuid = service.getUuid();
            sLogger.d("service: " + uuid.toString());
            if (DialConstants.OTA_SERVICE_UUID.equals(uuid)) {
                for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                    if (DialConstants.OTA_CONTROL_CHARACTERISTIC_UUID.equals(characteristic.getUuid())) {
                        this.otaControlCharacteristic = characteristic;
                        this.otaControlCharacteristic.setWriteType(2);
                    } else if (DialConstants.OTA_DATA_CHARACTERISTIC_UUID.equals(characteristic.getUuid())) {
                        this.otaDataCharacteristic = characteristic;
                        this.otaDataCharacteristic.setWriteType(1);
                    } else if (DialConstants.OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID.equals(characteristic.getUuid())) {
                        this.otaIncrementalVersionCharacteristic = characteristic;
                    }
                }
            } else if (DialConstants.DEVICE_INFO_SERVICE_UUID.equals(uuid)) {
                for (BluetoothGattCharacteristic characteristic2 : service.getCharacteristics()) {
                    if (DialConstants.FIRMWARE_REVISION_CHARACTERISTIC_UUID.equals(characteristic2.getUuid())) {
                        this.deviceInfoFirmwareRevisionCharacteristic = characteristic2;
                    }
                }
            }
        }
        if (characteristicsAvailable()) {
            sLogger.d("required OTA characteristics found");
            startVersionDetection();
            return;
        }
        sLogger.e("required OTA characteristics not found");
    }

    public void onCharacteristicRead(BluetoothGattCharacteristic characteristic, int status) {
        if (characteristicsAvailable()) {
            if (characteristic == this.otaIncrementalVersionCharacteristic) {
                if (status == 0) {
                    String ver = new String(characteristic.getValue());
                    this.versions.dial.incrementalVersion = Integer.parseInt(ver);
                    sLogger.i(String.format("updateAvailable: %s (%d -> %d)", new Object[]{Boolean.valueOf(this.versions.isUpdateAvailable()), Integer.valueOf(this.versions.dial.incrementalVersion), Integer.valueOf(this.versions.local.incrementalVersion)}));
                    if (this.updateListener != null) {
                        this.updateListener.onUpdateState(this.versions.isUpdateAvailable());
                    }
                } else {
                    sLogger.e("can't read dial firmware incremental version");
                }
            }
            if (characteristic != this.deviceInfoFirmwareRevisionCharacteristic) {
                return;
            }
            if (status == 0) {
                this.versions.dial.revisionString = new String(characteristic.getValue());
                sLogger.i("dial.revisionString: " + this.versions.dial.revisionString);
                this.dialManager.queueRead(this.otaIncrementalVersionCharacteristic);
                return;
            }
            sLogger.e("can't read dial firmware revisionString string");
        }
    }

    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        if (!characteristicsAvailable()) {
            return;
        }
        if (characteristic == this.otaControlCharacteristic) {
            sLogger.d("dial OTA response for command " + this.commandSent + ", status: " + status);
            if (status == 0) {
                switch (this.commandSent) {
                    case 0:
                        sendCommand(1);
                        return;
                    case 1:
                        sendCommand(2);
                        return;
                    case 2:
                        startFirmwareTransfer();
                        return;
                    case 4:
                        sendCommand(0);
                        return;
                    default:
                        return;
                }
            } else if (this.commandSent != 3) {
                finishUpdate(Error.INVALID_COMMAND, String.valueOf(this.commandSent));
            }
        } else if (characteristic != this.otaDataCharacteristic) {
        } else {
            if (status != 0) {
                sLogger.d("dial OTA error response for data chunk, status: " + status);
                finishUpdate(Error.INVALID_RESPONSE, String.valueOf(status));
            } else if (!this.updateRunning.get()) {
            } else {
                if (this.firmwareOffset < this.firmwareData.length) {
                    this.handler.postDelayed(this.sendFirmwareChunkRunnable, UPDATE_INTERPACKET_DELAY);
                    return;
                }
                sLogger.v("f/w upgrade complete, send OTA_COMMAND_BOOT_DFU, mark updating false");
                this.updateRunning.set(false);
                sendCommand(3);
                final BluetoothDevice device = gatt.getDevice();
                this.handler.postDelayed(new Runnable() {
                    public void run() {
                        DialManager.getInstance().forgetDial(device);
                        DialFirmwareUpdater.this.finishUpdate(Error.NONE, null);
                    }
                }, 5000);
            }
        }
    }

    private void sendCommand(int command) {
        sLogger.d("sendCommand: " + command);
        this.commandSent = command;
        this.dialManager.queueWrite(this.otaControlCharacteristic, new byte[]{(byte) command});
    }

    public boolean startVersionDetection() {
        this.versions.dial.reset();
        if (!characteristicsAvailable()) {
            return false;
        }
        this.dialManager.queueRead(this.deviceInfoFirmwareRevisionCharacteristic);
        return true;
    }

    public boolean startUpdate(UpdateProgressListener progressListener) {
        sLogger.i("starting dial firmware update");
        if (this.firmwareData.length % 16 != 0) {
            String err = "dial firmware OTA file size is not divisible by 16, aborting update";
            sLogger.e(err);
            progressListener.onFinished(Error.INVALID_IMAGE_SIZE, err);
            return false;
        } else if (!this.updateRunning.compareAndSet(false, true)) {
            return false;
        } else {
            this.updateProgressListener = progressListener;
            sendCommand(4);
            return true;
        }
    }

    public void cancelUpdate() {
        if (this.updateRunning.compareAndSet(true, false)) {
            finishUpdate(Error.CANCELLED, null);
        }
    }

    public String getDialName() {
        return this.dialName;
    }

    private void startFirmwareTransfer() {
        this.firmwareOffset = 0;
        sendFirmwareChunk();
    }

    private void sendFirmwareChunk() {
        if ((this.firmwareOffset / 16) % 30 == 0) {
            sLogger.d("firmwareOffset: " + this.firmwareOffset);
            if (this.updateProgressListener != null) {
                this.updateProgressListener.onProgress((this.firmwareOffset * 100) / this.firmwareData.length);
            }
        }
        this.dialManager.queueWrite(this.otaDataCharacteristic, Arrays.copyOfRange(this.firmwareData, this.firmwareOffset, this.firmwareOffset + 16));
        this.firmwareOffset += 16;
    }

    private void finishUpdate(Error error, String detail) {
        sLogger.i("finished dial update error: " + error + " , " + detail);
        if (this.updateProgressListener != null) {
            this.updateProgressListener.onFinished(error, detail);
        }
    }
}
