package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.util.ReportIssueService;
import kotlin.Metadata;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: ReportIssueMenu.kt */
final class ReportIssueMenu$takeSnapshot$1 implements Runnable {
    final /* synthetic */ int $id;
    final /* synthetic */ ReportIssueMenu this$0;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
    /* compiled from: ReportIssueMenu.kt */
    /* renamed from: com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu$takeSnapshot$1$1 */
    static final class AnonymousClass1 implements Runnable {
        final /* synthetic */ ReportIssueMenu$takeSnapshot$1 this$0;

        AnonymousClass1(ReportIssueMenu$takeSnapshot$1 reportIssueMenu$takeSnapshot$1) {
            this.this$0 = reportIssueMenu$takeSnapshot$1;
        }

        public final void run() {
            ReportIssueService.submitJiraTicketAsync((String) ReportIssueMenu.Companion.getIdToTitleMap().get(Integer.valueOf(this.this$0.$id)));
        }
    }

    ReportIssueMenu$takeSnapshot$1(ReportIssueMenu reportIssueMenu, int i) {
        this.this$0 = reportIssueMenu;
        this.$id = i;
    }

    public final void run() {
        this.this$0.presenter.close(new AnonymousClass1(this));
    }
}
