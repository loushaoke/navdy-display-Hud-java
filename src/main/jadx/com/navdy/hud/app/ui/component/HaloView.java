package com.navdy.hud.app.ui.component;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;

public class HaloView extends View {
    private int animationDelay;
    private int animationDuration;
    private DefaultAnimationListener animationListener;
    private AnimatorSet animatorSet;
    private AnimatorSet currentAnimatorSet;
    protected float currentStrokeWidth;
    protected float endRadius;
    protected float endStrokeWidth;
    private AnimatorSet firstAnimatorSet;
    private boolean firstIterationDone;
    private ValueAnimator firstRadiusAnimator;
    private Handler handler;
    private Interpolator interpolator;
    protected float middleRadius;
    protected float middleStrokeWidth;
    private Paint paint;
    private ValueAnimator radiusAnimator;
    private AnimatorSet reverseAnimatorSet;
    private ValueAnimator reverseRadiusAnimator;
    protected float startRadius;
    private Runnable startRunnable;
    private boolean started;
    private int strokeColor;
    private AnimatorUpdateListener updateListener;

    public HaloView(Context context) {
        this(context, null);
    }

    public HaloView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HaloView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.interpolator = new AccelerateDecelerateInterpolator();
        this.handler = new Handler(Looper.getMainLooper());
        this.animationListener = new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                if (HaloView.this.started) {
                    HaloView.this.handler.removeCallbacks(HaloView.this.startRunnable);
                    HaloView.this.handler.postDelayed(HaloView.this.startRunnable, (long) HaloView.this.animationDelay);
                }
            }
        };
        this.startRunnable = new Runnable() {
            public void run() {
                HaloView.this.toggleAnimator();
                HaloView.this.currentAnimatorSet.start();
            }
        };
        this.updateListener = new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                HaloView.this.onAnimationUpdateInternal(animation);
            }
        };
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HaloView, defStyleAttr, 0);
        if (a != null) {
            this.strokeColor = a.getColor(0, -1);
            this.startRadius = a.getDimension(1, 0.0f);
            this.endRadius = a.getDimension(2, 0.0f);
            this.middleRadius = a.getDimension(3, 0.0f);
            this.animationDuration = a.getInteger(4, 0);
            this.animationDelay = a.getInteger(5, 0);
            a.recycle();
        }
        this.endStrokeWidth = this.endRadius - this.startRadius;
        if (this.endStrokeWidth < 0.0f) {
            this.endStrokeWidth = 0.0f;
        }
        this.middleStrokeWidth = this.endRadius - this.middleRadius;
        if (this.middleStrokeWidth < 0.0f) {
            this.middleStrokeWidth = 0.0f;
        }
        this.paint = new Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
        this.firstRadiusAnimator = ValueAnimator.ofFloat(new float[]{this.startRadius, this.endRadius});
        this.radiusAnimator = ValueAnimator.ofFloat(new float[]{this.middleRadius, this.endRadius});
        this.reverseRadiusAnimator = ValueAnimator.ofFloat(new float[]{this.endRadius, this.middleRadius});
        this.firstRadiusAnimator.addUpdateListener(this.updateListener);
        this.radiusAnimator.addUpdateListener(this.updateListener);
        this.reverseRadiusAnimator.addUpdateListener(this.updateListener);
        this.firstAnimatorSet = new AnimatorSet();
        this.firstAnimatorSet.setDuration((long) this.animationDuration);
        this.firstAnimatorSet.setInterpolator(this.interpolator);
        this.animatorSet = new AnimatorSet();
        this.animatorSet.setDuration((long) this.animationDuration);
        this.animatorSet.setInterpolator(this.interpolator);
        this.reverseAnimatorSet = new AnimatorSet();
        this.reverseAnimatorSet.setDuration((long) this.animationDuration);
        this.reverseAnimatorSet.setInterpolator(this.interpolator);
        this.animatorSet.play(this.radiusAnimator);
        this.reverseAnimatorSet.play(this.reverseRadiusAnimator);
        this.currentStrokeWidth = this.endStrokeWidth;
        invalidate();
    }

    public void onAnimationUpdateInternal(ValueAnimator animation) {
        this.currentStrokeWidth = ((Float) animation.getAnimatedValue()).floatValue() - this.startRadius;
        invalidate();
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        onDrawInternal(canvas);
    }

    public void onDrawInternal(Canvas canvas) {
        this.paint.setStyle(Style.FILL);
        this.paint.setStrokeWidth(0.0f);
        this.paint.setColor(this.strokeColor);
        canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), this.startRadius + this.currentStrokeWidth, this.paint);
    }

    public void start() {
        stop();
        this.started = true;
        this.firstAnimatorSet.addListener(this.animationListener);
        this.animatorSet.addListener(this.animationListener);
        this.reverseAnimatorSet.addListener(this.animationListener);
        if (this.firstIterationDone) {
            this.currentAnimatorSet = this.reverseAnimatorSet;
            this.reverseAnimatorSet.start();
            return;
        }
        this.firstIterationDone = true;
        this.currentAnimatorSet = this.animatorSet;
        this.firstAnimatorSet.start();
    }

    public void stop() {
        this.started = false;
        this.handler.removeCallbacks(this.startRunnable);
        if (this.firstAnimatorSet.isRunning()) {
            this.firstAnimatorSet.removeAllListeners();
            this.firstAnimatorSet.cancel();
        }
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
        if (this.reverseAnimatorSet.isRunning()) {
            this.reverseAnimatorSet.removeAllListeners();
            this.reverseAnimatorSet.cancel();
        }
        this.currentStrokeWidth = this.endStrokeWidth;
        requestLayout();
    }

    private void toggleAnimator() {
        if (this.currentAnimatorSet == this.animatorSet) {
            this.currentAnimatorSet = this.reverseAnimatorSet;
        } else {
            this.currentAnimatorSet = this.animatorSet;
        }
    }

    public void setStrokeColor(int color) {
        this.strokeColor = color;
        invalidate();
    }

    public void setStrokeWidth(int n) {
        this.currentStrokeWidth = (float) n;
    }

    public void setStartRadius(int n) {
        this.startRadius = (float) n;
    }

    public void setMiddleRadius(int n) {
        this.middleRadius = (float) n;
    }

    public void setEndRadius(int n) {
        this.endRadius = (float) n;
    }

    public void setStartDelay(int n) {
        this.animationDelay = n;
    }

    public void setDuration(int n) {
        this.animationDuration = n;
    }
}
