package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.service.library.log.Logger;

public class TitleViewHolder extends VerticalViewHolder {
    private static final Logger sLogger = VerticalViewHolder.sLogger;
    private TextView title;

    public static Model buildModel(String title) {
        Model model = new Model();
        model.type = ModelType.TITLE;
        model.id = R.id.vlist_title;
        model.title = title;
        return model;
    }

    public static TitleViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        return new TitleViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.vlist_title, parent, false), vlist, handler);
    }

    public TitleViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
        this.title = (TextView) layout.findViewById(R.id.title);
    }

    public ModelType getModelType() {
        return ModelType.TITLE;
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
    }

    public void preBind(Model model, ModelState modelState) {
        this.title.setTextSize(model.fontInfo.titleFontSize);
    }

    public void bind(Model model, ModelState modelState) {
        this.title.setText(model.title);
    }

    public void clearAnimation() {
    }

    public void select(Model model, int pos, int duration) {
    }

    public void copyAndPosition(ImageView imageC, TextView titleC, TextView subTitleC, TextView subTitle2C, boolean setImage) {
    }
}
