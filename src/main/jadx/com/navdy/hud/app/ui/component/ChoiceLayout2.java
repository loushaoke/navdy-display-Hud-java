package com.navdy.hud.app.ui.component;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintLayout.LayoutParams;
import android.support.constraint.ConstraintSet;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.audio.SoundUtils;
import com.navdy.hud.app.audio.SoundUtils.Sound;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;

public class ChoiceLayout2 extends ConstraintLayout {
    private static final float DEFAULT_SCALE = 0.5f;
    private static final String EMPTY = "";
    private static int defaultHaloDuration;
    private static int defaultHaloEndRadius;
    private static int defaultHaloMiddleRadius;
    private static int defaultHaloStartDelay;
    private static int defaultHaloStartRadius;
    private static int defaultIconHaloSize;
    private static int defaultIconSize;
    private static int defaultItemPadding = -1;
    private static int defaultLabelSize;
    private static int defaultTopPadding;
    private static final Logger sLogger = new Logger(ChoiceLayout2.class);
    private List<Choice> choices;
    private boolean clickMode;
    private int currentSelection;
    private int haloDuration;
    private int haloEndRadius;
    private int haloMiddleRadius;
    private int haloStartDelay;
    private int haloStartRadius;
    private int iconHaloSize;
    private int iconSize;
    private int itemPadding;
    private int itemTopPadding;
    private int labelSize;
    private TextView labelView;
    private IListener listener;
    private float scale;
    private Selection selection;
    private List<ViewContainer> viewContainers;

    public interface IListener {
        void executeItem(Selection selection);

        void itemSelected(Selection selection);
    }

    public static class Choice implements Parcelable {
        public static final Creator<Choice> CREATOR = new Creator<Choice>() {
            public Choice createFromParcel(Parcel in) {
                return new Choice(in);
            }

            public Choice[] newArray(int size) {
                return new Choice[size];
            }
        };
        private final int fluctuatorColor;
        private final int id;
        private final String label;
        private final int resIdSelected;
        private final int resIdUnselected;
        private final int selectedColor;
        private final int unselectedColor;

        public Choice(int id, int resIdSelected, int selectedColor, int resIdUnselected, int unselectedColor, String label, int fluctuatorColor) {
            this.id = id;
            this.resIdSelected = resIdSelected;
            this.selectedColor = selectedColor;
            this.resIdUnselected = resIdUnselected;
            this.unselectedColor = unselectedColor;
            this.label = label;
            this.fluctuatorColor = fluctuatorColor;
        }

        public Choice(Parcel in) {
            this.id = in.readInt();
            this.resIdSelected = in.readInt();
            this.selectedColor = in.readInt();
            this.resIdUnselected = in.readInt();
            this.unselectedColor = in.readInt();
            String str = in.readString();
            if (TextUtils.isEmpty(str)) {
                this.label = null;
            } else {
                this.label = str;
            }
            this.fluctuatorColor = in.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeInt(this.resIdSelected);
            dest.writeInt(this.selectedColor);
            dest.writeInt(this.resIdUnselected);
            dest.writeInt(this.unselectedColor);
            dest.writeString(this.label != null ? this.label : "");
            dest.writeInt(this.fluctuatorColor);
        }

        public int getId() {
            return this.id;
        }
    }

    public static class Selection {
        public int id;
        public int pos;
    }

    private static class ViewContainer {
        IconColorImageView big;
        CrossFadeImageView crossFadeImageView;
        HaloView haloView;
        ViewGroup iconContainer;
        View parent;
        IconColorImageView small;

        private ViewContainer() {
        }

        /* synthetic */ ViewContainer(AnonymousClass1 x0) {
            this();
        }
    }

    private static void initDefaults(Context context) {
        if (defaultItemPadding == -1) {
            Resources resources = context.getResources();
            defaultItemPadding = resources.getDimensionPixelSize(R.dimen.choices2_lyt_item_padding);
            defaultTopPadding = resources.getDimensionPixelSize(R.dimen.choices2_lyt_item_top_padding);
            defaultLabelSize = resources.getDimensionPixelSize(R.dimen.choices2_lyt_text_size);
            defaultIconSize = resources.getDimensionPixelSize(R.dimen.choices2_lyt_icon_size);
            defaultIconHaloSize = resources.getDimensionPixelSize(R.dimen.choices2_lyt_icon_halo_size);
            defaultHaloStartRadius = resources.getDimensionPixelSize(R.dimen.choices2_lyt_halo_start_radius);
            defaultHaloEndRadius = resources.getDimensionPixelSize(R.dimen.choices2_lyt_halo_end_radius);
            defaultHaloMiddleRadius = resources.getDimensionPixelSize(R.dimen.choices2_lyt_halo_middle_radius);
            defaultHaloStartDelay = resources.getInteger(R.integer.choices2_lyt_halo_start_delay);
            defaultHaloDuration = resources.getInteger(R.integer.choices2_lyt_halo_duration);
        }
    }

    public ChoiceLayout2(Context context) {
        this(context, null);
    }

    public ChoiceLayout2(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChoiceLayout2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.listener = null;
        this.currentSelection = -1;
        this.viewContainers = new ArrayList();
        this.selection = new Selection();
        initDefaults(context);
        LayoutInflater.from(context).inflate(R.layout.choices2_lyt, this, true);
        this.labelView = (TextView) findViewById(R.id.label);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ChoiceLayout2, defStyleAttr, 0);
        if (a != null) {
            this.itemPadding = a.getDimensionPixelSize(1, defaultItemPadding);
            this.itemTopPadding = a.getDimensionPixelSize(0, defaultTopPadding);
            this.labelSize = a.getDimensionPixelSize(2, defaultLabelSize);
            this.iconSize = a.getDimensionPixelSize(3, defaultIconSize);
            this.iconHaloSize = a.getDimensionPixelSize(4, defaultIconHaloSize);
            this.haloStartRadius = a.getDimensionPixelSize(5, defaultHaloStartRadius);
            this.haloEndRadius = a.getDimensionPixelSize(6, defaultHaloEndRadius);
            this.haloMiddleRadius = a.getDimensionPixelSize(7, defaultHaloMiddleRadius);
            this.haloStartDelay = a.getInteger(8, defaultHaloStartDelay);
            this.haloDuration = a.getInteger(9, defaultHaloDuration);
            a.recycle();
        }
    }

    public void setChoices(List<Choice> choices, int initialSelection, IListener listener) {
        setChoices(choices, initialSelection, listener, 0.5f);
    }

    public void setChoices(List<Choice> choices, int initialSelection, IListener listener, float scale) {
        resetClickMode();
        this.choices = choices;
        this.listener = listener;
        this.scale = scale;
        if (choices == null || choices.size() <= 0) {
            sLogger.v("setChoices:clear");
            this.currentSelection = -1;
            removeViewContainers();
            this.labelView.setText("");
            invalidate();
            requestLayout();
            return;
        }
        if (initialSelection < 0 || initialSelection >= choices.size()) {
            this.currentSelection = 0;
        } else {
            this.currentSelection = initialSelection;
        }
        sLogger.v("setChoices:" + choices.size() + " sel:" + this.currentSelection);
        buildChoices();
        for (ViewContainer viewContainer : this.viewContainers) {
            viewContainer.crossFadeImageView.setMode(Mode.SMALL);
        }
        startFluctuator(this.currentSelection, true);
    }

    public List<Choice> getChoices() {
        return this.choices;
    }

    private void buildChoices() {
        removeViewContainers();
        if (this.choices != null) {
            int i;
            LayoutInflater inflater = LayoutInflater.from(getContext());
            int len = this.choices.size();
            for (i = 0; i < len; i++) {
                Choice choice = (Choice) this.choices.get(i);
                ViewContainer viewContainer = new ViewContainer();
                viewContainer.parent = inflater.inflate(R.layout.choices2_lyt_item, this, false);
                viewContainer.parent.setId(View.generateViewId());
                viewContainer.iconContainer = (ViewGroup) viewContainer.parent.findViewById(R.id.iconContainer);
                viewContainer.crossFadeImageView = (CrossFadeImageView) viewContainer.parent.findViewById(R.id.crossFadeImageView);
                viewContainer.crossFadeImageView.inject(Mode.SMALL);
                viewContainer.big = (IconColorImageView) viewContainer.parent.findViewById(R.id.big);
                viewContainer.small = (IconColorImageView) viewContainer.parent.findViewById(R.id.small);
                viewContainer.haloView = (HaloView) viewContainer.parent.findViewById(R.id.halo);
                this.viewContainers.add(viewContainer);
                LayoutParams layoutParams = new LayoutParams(this.iconHaloSize, this.iconHaloSize);
                if (i != 0) {
                    layoutParams.leftMargin = this.itemPadding;
                }
                MarginLayoutParams marginLayoutParams = (MarginLayoutParams) viewContainer.iconContainer.getLayoutParams();
                marginLayoutParams.width = this.iconSize;
                marginLayoutParams.height = this.iconSize;
                viewContainer.haloView.setStartRadius(this.haloStartRadius);
                viewContainer.haloView.setMiddleRadius(this.haloMiddleRadius);
                viewContainer.haloView.setEndRadius(this.haloEndRadius);
                viewContainer.haloView.setDuration(this.haloDuration);
                viewContainer.haloView.setStartDelay(this.haloStartDelay);
                viewContainer.big.setIcon(choice.resIdSelected, choice.selectedColor, null, this.scale);
                viewContainer.small.setIcon(choice.resIdUnselected, choice.unselectedColor, null, this.scale);
                viewContainer.haloView.setVisibility(4);
                viewContainer.haloView.setStrokeColor(choice.fluctuatorColor);
                addView(viewContainer.parent, layoutParams);
            }
            this.labelView.setTextSize((float) this.labelSize);
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone((ConstraintLayout) this);
            len = this.viewContainers.size();
            int parentId = getId();
            int labelId = this.labelView.getId();
            for (i = 0; i < len; i++) {
                int viewId = ((ViewContainer) this.viewContainers.get(i)).parent.getId();
                if (i == 0) {
                    constraintSet.setHorizontalChainStyle(viewId, 2);
                    constraintSet.connect(viewId, 6, parentId, 6, 0);
                }
                if (i == len - 1) {
                    constraintSet.connect(viewId, 7, parentId, 7, 0);
                    if (len > 1) {
                        constraintSet.connect(viewId, 1, ((ViewContainer) this.viewContainers.get(i - 1)).parent.getId(), 2, 0);
                    }
                } else if (len > 1) {
                    constraintSet.connect(viewId, 2, ((ViewContainer) this.viewContainers.get(i + 1)).parent.getId(), 1, 0);
                    if (i != 0) {
                        constraintSet.connect(viewId, 1, ((ViewContainer) this.viewContainers.get(i - 1)).parent.getId(), 2, 0);
                    }
                }
                constraintSet.connect(viewId, 3, labelId, 4, this.itemTopPadding);
            }
            constraintSet.applyTo(this);
            invalidate();
            requestLayout();
        }
    }

    public boolean moveSelectionLeft() {
        if (this.clickMode) {
            sLogger.v("left: click mode on");
            return false;
        } else if (this.currentSelection == 0) {
            return false;
        } else {
            this.currentSelection--;
            stopFluctuator(this.currentSelection + 1, true);
            startFluctuator(this.currentSelection, true);
            callSelectListener(this.currentSelection);
            return true;
        }
    }

    public boolean moveSelectionRight() {
        if (this.clickMode) {
            sLogger.v("right: click mode on");
            return false;
        } else if (this.currentSelection == this.choices.size() - 1) {
            return false;
        } else {
            this.currentSelection++;
            stopFluctuator(this.currentSelection - 1, true);
            startFluctuator(this.currentSelection, true);
            callSelectListener(this.currentSelection);
            return true;
        }
    }

    public void executeSelectedItem() {
        if (this.clickMode) {
            sLogger.v("select: click mode on");
        } else if (isItemInBounds(this.currentSelection)) {
            this.clickMode = true;
            stopFluctuator(this.currentSelection, false);
            VerticalAnimationUtils.performClick(((ViewContainer) this.viewContainers.get(this.currentSelection)).iconContainer, 50, new Runnable() {
                public void run() {
                    ChoiceLayout2.this.callClickListener(ChoiceLayout2.this.currentSelection);
                }
            });
        }
    }

    private int getItemCount() {
        if (this.choices != null) {
            return this.choices.size();
        }
        return 0;
    }

    private void callClickListener(int pos) {
        if (this.listener != null) {
            SoundUtils.playSound(Sound.MENU_SELECT);
            this.selection.id = ((Choice) this.choices.get(pos)).id;
            this.selection.pos = pos;
            this.listener.executeItem(this.selection);
        }
    }

    private void callSelectListener(int pos) {
        if (this.listener != null) {
            SoundUtils.playSound(Sound.MENU_MOVE);
            this.selection.id = ((Choice) this.choices.get(pos)).id;
            this.selection.pos = pos;
            this.listener.itemSelected(this.selection);
        }
    }

    public Selection getCurrentSelection() {
        if (!isItemInBounds(this.currentSelection)) {
            return null;
        }
        this.selection.id = ((Choice) this.choices.get(this.currentSelection)).id;
        this.selection.pos = this.currentSelection;
        return this.selection;
    }

    public Choice getCurrentSelectedChoice() {
        Selection selection = getCurrentSelection();
        if (selection != null) {
            return (Choice) this.choices.get(selection.pos);
        }
        return null;
    }

    private void removeViewContainers() {
        for (ViewContainer viewContainer : this.viewContainers) {
            removeView(viewContainer.parent);
        }
        this.viewContainers.clear();
    }

    private boolean isItemInBounds(int item) {
        if (this.choices == null || item < 0 || item >= this.choices.size()) {
            return false;
        }
        return true;
    }

    private void startFluctuator(int item, boolean changeColor) {
        sLogger.v("startFluctuator:" + item + " current=" + this.currentSelection);
        if (isItemInBounds(item)) {
            ViewContainer viewContainer = (ViewContainer) this.viewContainers.get(item);
            Choice choice = (Choice) this.choices.get(item);
            if (changeColor) {
                viewContainer.crossFadeImageView.setMode(Mode.BIG);
            }
            viewContainer.haloView.setVisibility(0);
            viewContainer.haloView.start();
            this.labelView.setText(choice.label);
        }
    }

    private void stopFluctuator(int item, boolean changeColor) {
        sLogger.v("stopFluctuator:" + item + " current=" + this.currentSelection);
        if (isItemInBounds(item)) {
            Choice choice = (Choice) this.choices.get(item);
            ViewContainer viewContainer = (ViewContainer) this.viewContainers.get(item);
            if (changeColor) {
                viewContainer.crossFadeImageView.setMode(Mode.SMALL);
            }
            viewContainer.haloView.setVisibility(8);
            viewContainer.haloView.stop();
        }
    }

    private void resetClickMode() {
        this.clickMode = false;
    }

    public void clear() {
        sLogger.v("clear");
        stopFluctuator(this.currentSelection, false);
        resetClickMode();
    }
}
