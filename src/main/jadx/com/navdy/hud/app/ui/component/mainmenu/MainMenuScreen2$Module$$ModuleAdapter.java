package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Module;
import dagger.internal.ModuleAdapter;

public final class MainMenuScreen2$Module$$ModuleAdapter extends ModuleAdapter<Module> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.hud.app.ui.component.mainmenu.MainMenuView2"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    public MainMenuScreen2$Module$$ModuleAdapter() {
        super(Module.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, false);
    }
}
