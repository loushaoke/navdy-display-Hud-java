package com.navdy.hud.app.ui.component.carousel;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;

public interface AnimationStrategy {

    public enum Direction {
        LEFT,
        RIGHT
    }

    AnimatorSet buildLayoutAnimation(AnimatorListener animatorListener, CarouselLayout carouselLayout, int i, int i2);

    AnimatorSet createHiddenViewAnimation(CarouselLayout carouselLayout, Direction direction);

    AnimatorSet createMiddleLeftViewAnimation(CarouselLayout carouselLayout, Direction direction);

    AnimatorSet createMiddleRightViewAnimation(CarouselLayout carouselLayout, Direction direction);

    AnimatorSet createNewMiddleRightViewAnimation(CarouselLayout carouselLayout, Direction direction);

    AnimatorSet createSideViewToMiddleAnimation(CarouselLayout carouselLayout, Direction direction);

    Animator createViewOutAnimation(CarouselLayout carouselLayout, Direction direction);
}
