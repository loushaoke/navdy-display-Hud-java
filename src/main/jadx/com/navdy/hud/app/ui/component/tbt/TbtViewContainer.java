package com.navdy.hud.app.ui.component.tbt;

import android.animation.AnimatorSet.Builder;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import net.hockeyapp.android.LoginActivity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 .2\u00020\u00012\u00020\u0002:\u0001.B\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B\u0017\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bB\u001f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0006\u0010\u001f\u001a\u00020 J\u001a\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u00192\n\u0010#\u001a\u00060$R\u00020%J\b\u0010&\u001a\u0004\u0018\u00010\u0012J\u000e\u0010'\u001a\u00020 2\u0006\u0010\u000e\u001a\u00020\u000fJ\b\u0010(\u001a\u00020 H\u0014J\b\u0010)\u001a\u00020 H\u0016J\b\u0010*\u001a\u00020 H\u0016J\u000e\u0010+\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u0019J\u0010\u0010,\u001a\u00020 2\u0006\u0010-\u001a\u00020\u0012H\u0007R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer;", "Landroid/widget/FrameLayout;", "Lcom/navdy/hud/app/ui/component/homescreen/IHomeScreenLifecycle;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "activeView", "Lcom/navdy/hud/app/ui/component/tbt/TbtView;", "homeScreenView", "Lcom/navdy/hud/app/ui/component/homescreen/HomeScreenView;", "inactiveView", "lastEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "lastManeuverDisplay", "lastManeuverId", "", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "lastRoadText", "lastTurnIconId", "lastTurnText", "paused", "", "clearState", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "getLastManeuverDisplay", "init", "onFinishInflate", "onPause", "onResume", "setView", "updateDisplay", "event", "Companion", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtViewContainer.kt */
public final class TbtViewContainer extends FrameLayout implements IHomeScreenLifecycle {
    public static final Companion Companion = new Companion();
    private static final long MANEUVER_PROGRESS_ANIMATION_DURATION = 250;
    private static final long MANEUVER_SWAP_DELAY = MANEUVER_SWAP_DELAY;
    private static final long MANEUVER_SWAP_DURATION = 500;
    @NotNull
    private static final Bus bus;
    private static final float itemHeight = Companion.getResources().getDimension(R.dimen.tbt_view_ht);
    private static final Logger logger = new Logger("TbtViewContainer");
    private static final AccelerateDecelerateInterpolator maneuverSwapInterpolator = new AccelerateDecelerateInterpolator();
    @NotNull
    private static final RemoteDeviceManager remoteDeviceManager;
    @NotNull
    private static final Resources resources;
    @NotNull
    private static final UIStateManager uiStateManager;
    private HashMap _$_findViewCache;
    private TbtView activeView;
    private HomeScreenView homeScreenView;
    private TbtView inactiveView;
    private ManeuverDisplay lastEvent;
    private ManeuverDisplay lastManeuverDisplay;
    private String lastManeuverId;
    private ManeuverState lastManeuverState;
    private CustomAnimationMode lastMode;
    private String lastRoadText;
    private int lastTurnIconId = -1;
    private String lastTurnText;
    private boolean paused;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u001f\u001a\u00020 \u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0011\u0010#\u001a\u00020$\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010&\u00a8\u0006'"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtViewContainer$Companion;", "", "()V", "MANEUVER_PROGRESS_ANIMATION_DURATION", "", "getMANEUVER_PROGRESS_ANIMATION_DURATION", "()J", "MANEUVER_SWAP_DELAY", "getMANEUVER_SWAP_DELAY", "MANEUVER_SWAP_DURATION", "getMANEUVER_SWAP_DURATION", "bus", "Lcom/squareup/otto/Bus;", "getBus", "()Lcom/squareup/otto/Bus;", "itemHeight", "", "getItemHeight", "()F", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "maneuverSwapInterpolator", "Landroid/view/animation/AccelerateDecelerateInterpolator;", "getManeuverSwapInterpolator", "()Landroid/view/animation/AccelerateDecelerateInterpolator;", "remoteDeviceManager", "Lcom/navdy/hud/app/manager/RemoteDeviceManager;", "getRemoteDeviceManager", "()Lcom/navdy/hud/app/manager/RemoteDeviceManager;", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "getUiStateManager", "()Lcom/navdy/hud/app/ui/framework/UIStateManager;", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtViewContainer.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final Logger getLogger() {
            return TbtViewContainer.logger;
        }

        @NotNull
        public final Resources getResources() {
            return TbtViewContainer.resources;
        }

        public final float getItemHeight() {
            return TbtViewContainer.itemHeight;
        }

        public final long getMANEUVER_PROGRESS_ANIMATION_DURATION() {
            return TbtViewContainer.MANEUVER_PROGRESS_ANIMATION_DURATION;
        }

        public final long getMANEUVER_SWAP_DURATION() {
            return TbtViewContainer.MANEUVER_SWAP_DURATION;
        }

        public final long getMANEUVER_SWAP_DELAY() {
            return TbtViewContainer.MANEUVER_SWAP_DELAY;
        }

        private final AccelerateDecelerateInterpolator getManeuverSwapInterpolator() {
            return TbtViewContainer.maneuverSwapInterpolator;
        }

        @NotNull
        public final RemoteDeviceManager getRemoteDeviceManager() {
            return TbtViewContainer.remoteDeviceManager;
        }

        @NotNull
        public final Bus getBus() {
            return TbtViewContainer.bus;
        }

        @NotNull
        public final UIStateManager getUiStateManager() {
            return TbtViewContainer.uiStateManager;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        view = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), view);
        return view;
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "HudApplication.getAppContext().resources");
        resources = resources;
        RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        Intrinsics.checkExpressionValueIsNotNull(instance, "RemoteDeviceManager.getInstance()");
        remoteDeviceManager = instance;
        Bus bus = Companion.getRemoteDeviceManager().getBus();
        Intrinsics.checkExpressionValueIsNotNull(bus, "remoteDeviceManager.bus");
        bus = bus;
        UIStateManager uiStateManager = Companion.getRemoteDeviceManager().getUiStateManager();
        Intrinsics.checkExpressionValueIsNotNull(uiStateManager, "remoteDeviceManager.uiStateManager");
        uiStateManager = uiStateManager;
    }

    public TbtViewContainer(@NotNull Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public TbtViewContainer(@NotNull Context context, @NotNull AttributeSet attrs) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtViewContainer(@NotNull Context context, @NotNull AttributeSet attrs, int defStyleAttr) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        View findViewById = findViewById(R.id.view1);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView");
        }
        this.activeView = (TbtView) findViewById;
        findViewById = findViewById(R.id.view2);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView");
        }
        this.inactiveView = (TbtView) findViewById;
        TbtView tbtView = this.activeView;
        if (tbtView != null) {
            tbtView.setTranslationY(-Companion.getItemHeight());
        }
    }

    public final void init(@NotNull HomeScreenView homeScreenView) {
        Intrinsics.checkParameterIsNotNull(homeScreenView, "homeScreenView");
        this.homeScreenView = homeScreenView;
        Companion.getBus().register(this);
    }

    public final void clearState() {
        this.lastTurnIconId = -1;
        this.lastTurnText = (String) null;
        this.lastRoadText = (String) null;
        TbtView tbtView = this.activeView;
        if (tbtView != null) {
            tbtView.clear();
        }
        tbtView = this.inactiveView;
        if (tbtView != null) {
            tbtView.clear();
        }
    }

    public final void setView(@NotNull CustomAnimationMode mode) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        this.lastMode = mode;
        TbtView tbtView = this.activeView;
        if (tbtView != null) {
            tbtView.setView(mode);
        }
        tbtView = this.inactiveView;
        if (tbtView != null) {
            tbtView.setView(mode);
        }
    }

    public final void getCustomAnimator(@NotNull CustomAnimationMode mode, @NotNull Builder mainBuilder) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
        this.lastMode = mode;
        TbtView tbtView = this.activeView;
        if (tbtView != null) {
            tbtView.getCustomAnimator(mode, mainBuilder);
        }
        tbtView = this.inactiveView;
        if (tbtView != null) {
            tbtView.getCustomAnimator(mode, mainBuilder);
        }
    }

    @Subscribe
    public final void updateDisplay(@NotNull ManeuverDisplay event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        this.lastManeuverDisplay = event;
        HomeScreenView homeScreenView = this.homeScreenView;
        if (!(homeScreenView != null ? homeScreenView.isNavigationActive() : false)) {
            return;
        }
        if (event.empty) {
            clearState();
        } else if (event.pendingTurn == null) {
            Companion.getLogger().v("null pending turn");
            this.lastEvent = (ManeuverDisplay) null;
            this.lastManeuverId = (String) null;
        } else {
            this.lastEvent = event;
            if (this.paused) {
                this.lastManeuverId = event.maneuverId;
                return;
            }
            TbtView tbtView;
            if (TextUtils.equals(this.lastManeuverId, event.maneuverId)) {
                tbtView = this.activeView;
                if (tbtView != null) {
                    tbtView.updateDisplay(event);
                }
            } else {
                ViewPropertyAnimator animate;
                TbtView temp = this.activeView;
                this.activeView = this.inactiveView;
                this.inactiveView = temp;
                tbtView = this.activeView;
                if (tbtView != null) {
                    tbtView.setTranslationY(-Companion.getItemHeight());
                }
                tbtView = this.activeView;
                if (tbtView != null) {
                    tbtView.updateDisplay(event);
                }
                tbtView = this.activeView;
                if (tbtView != null) {
                    animate = tbtView.animate();
                    if (animate != null) {
                        animate = animate.translationY(0.0f);
                        if (animate != null) {
                            animate = animate.setDuration(Companion.getMANEUVER_SWAP_DURATION());
                            if (animate != null) {
                                animate = animate.setStartDelay(Companion.getMANEUVER_SWAP_DELAY());
                                if (animate != null) {
                                    animate.start();
                                }
                            }
                        }
                    }
                }
                tbtView = this.inactiveView;
                if (tbtView != null) {
                    animate = tbtView.animate();
                    if (animate != null) {
                        animate = animate.translationY(Companion.getItemHeight());
                        if (animate != null) {
                            animate = animate.setDuration(Companion.getMANEUVER_SWAP_DURATION());
                            if (animate != null) {
                                ViewPropertyAnimator startDelay = animate.setStartDelay(Companion.getMANEUVER_SWAP_DELAY());
                                if (startDelay != null) {
                                    animate = startDelay.setListener(new TbtViewContainer$updateDisplay$1(this));
                                    if (animate != null) {
                                        animate.start();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.lastManeuverId = event.maneuverId;
            this.lastManeuverState = event.maneuverState;
        }
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            Companion.getLogger().v("::onPause:tbt");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            Companion.getLogger().v("::onResume:tbt");
            ManeuverDisplay event = this.lastEvent;
            if (event != null) {
                this.lastEvent = (ManeuverDisplay) null;
                Companion.getLogger().v("::onResume:tbt updated last event");
                updateDisplay(event);
            }
        }
    }

    @Nullable
    public final ManeuverDisplay getLastManeuverDisplay() {
        return this.lastManeuverDisplay;
    }
}
