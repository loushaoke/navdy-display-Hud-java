package com.navdy.hud.app.ui.component.tbt;

import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import net.hockeyapp.android.LoginActivity;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 ,2\u00020\u0001:\u0001,B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u001a\u001a\u00020\u001bH\u0002J\u0006\u0010\u001c\u001a\u00020\u001bJ\u001a\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\f2\n\u0010\u001f\u001a\u00060 R\u00020!J\b\u0010\"\u001a\u00020\u001bH\u0014J$\u0010#\u001a\u00020\u001b2\b\u0010$\u001a\u0004\u0018\u00010\u00132\b\u0010%\u001a\u0004\u0018\u00010\u00112\u0006\u0010&\u001a\u00020'H\u0002J\u000e\u0010(\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\fJ\u000e\u0010)\u001a\u00020\u001b2\u0006\u0010*\u001a\u00020+R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "distanceView", "Landroid/widget/TextView;", "initialProgressBarDistance", "lastDistance", "", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "nowView", "Landroid/widget/ImageView;", "progressAnimator", "Landroid/animation/ValueAnimator;", "progressView", "Landroid/widget/ProgressBar;", "cancelProgressAnimator", "", "clear", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "onFinishInflate", "setDistance", "maneuverState", "text", "distanceInMeters", "", "setMode", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "Companion", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtDistanceView.kt */
public final class TbtDistanceView extends ConstraintLayout {
    public static final Companion Companion = new Companion();
    private static final int fullWidth = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_full_w);
    private static final Logger logger = new Logger("TbtDistanceView");
    private static final int mediumWidth = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_medium_w);
    private static final int nowHeightFull = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_h_full);
    private static final int nowHeightMedium = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_h_medium);
    private static final int nowWidthFull = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_w_full);
    private static final int nowWidthMedium = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_w_medium);
    private static final int progressHeightFull = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_progress_h_full);
    private static final int progressHeightMedium = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_progress_h_medium);
    private static final Resources resources;
    private static final float size18 = Companion.getResources().getDimension(R.dimen.tbt_instruction_18);
    private static final float size22 = Companion.getResources().getDimension(R.dimen.tbt_instruction_22);
    private HashMap _$_findViewCache;
    private CustomAnimationMode currentMode;
    private TextView distanceView;
    private int initialProgressBarDistance;
    private String lastDistance;
    private ManeuverState lastManeuverState;
    private ImageView nowView;
    private ValueAnimator progressAnimator;
    private ProgressBar progressView;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006R\u0014\u0010\u000f\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0006R\u0014\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0006R\u0014\u0010\u0013\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0006R\u0014\u0010\u0015\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0006R\u0014\u0010\u0017\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0006R\u0014\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0014\u0010!\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010 \u00a8\u0006#"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;", "", "()V", "fullWidth", "", "getFullWidth", "()I", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "mediumWidth", "getMediumWidth", "nowHeightFull", "getNowHeightFull", "nowHeightMedium", "getNowHeightMedium", "nowWidthFull", "getNowWidthFull", "nowWidthMedium", "getNowWidthMedium", "progressHeightFull", "getProgressHeightFull", "progressHeightMedium", "getProgressHeightMedium", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "size18", "", "getSize18", "()F", "size22", "getSize22", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtDistanceView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final Logger getLogger() {
            return TbtDistanceView.logger;
        }

        private final Resources getResources() {
            return TbtDistanceView.resources;
        }

        private final int getFullWidth() {
            return TbtDistanceView.fullWidth;
        }

        private final int getMediumWidth() {
            return TbtDistanceView.mediumWidth;
        }

        private final float getSize22() {
            return TbtDistanceView.size22;
        }

        private final float getSize18() {
            return TbtDistanceView.size18;
        }

        private final int getProgressHeightFull() {
            return TbtDistanceView.progressHeightFull;
        }

        private final int getProgressHeightMedium() {
            return TbtDistanceView.progressHeightMedium;
        }

        private final int getNowWidthFull() {
            return TbtDistanceView.nowWidthFull;
        }

        private final int getNowHeightFull() {
            return TbtDistanceView.nowHeightFull;
        }

        private final int getNowWidthMedium() {
            return TbtDistanceView.nowWidthMedium;
        }

        private final int getNowHeightMedium() {
            return TbtDistanceView.nowHeightMedium;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        view = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), view);
        return view;
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "HudApplication.getAppContext().resources");
        resources = resources;
    }

    public TbtDistanceView(@NotNull Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public TbtDistanceView(@NotNull Context context, @NotNull AttributeSet attrs) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtDistanceView(@NotNull Context context, @NotNull AttributeSet attrs, int defStyleAttr) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        View findViewById = findViewById(R.id.distance);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.distanceView = (TextView) findViewById;
        findViewById = findViewById(R.id.progress);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.ProgressBar");
        }
        this.progressView = (ProgressBar) findViewById;
        findViewById = findViewById(R.id.now);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
        }
        this.nowView = (ImageView) findViewById;
    }

    public final void setMode(@NotNull CustomAnimationMode mode) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        if (!Intrinsics.areEqual(this.currentMode,  mode)) {
            int lytWidth = 0;
            float fontSize = 0.0f;
            int progressSize = 0;
            int nowW = 0;
            int nowH = 0;
            switch (mode) {
                case EXPAND:
                    lytWidth = Companion.getFullWidth();
                    fontSize = Companion.getSize22();
                    progressSize = Companion.getProgressHeightFull();
                    nowW = Companion.getNowWidthFull();
                    nowH = Companion.getNowHeightFull();
                    break;
                case SHRINK_LEFT:
                    lytWidth = Companion.getMediumWidth();
                    fontSize = Companion.getSize18();
                    progressSize = Companion.getProgressHeightMedium();
                    nowW = Companion.getNowWidthMedium();
                    nowH = Companion.getNowHeightMedium();
                    break;
            }
            MarginLayoutParams lytParams = getLayoutParams();
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams.width = lytWidth;
            TextView textView = this.distanceView;
            if (textView != null) {
                textView.setTextSize(fontSize);
            }
            ProgressBar progressBar = this.progressView;
            lytParams = progressBar != null ? progressBar.getLayoutParams() : null;
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams.height = progressSize;
            ImageView imageView = this.nowView;
            lytParams = imageView != null ? imageView.getLayoutParams() : null;
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams = lytParams;
            lytParams.width = nowW;
            lytParams.height = nowH;
            invalidate();
            requestLayout();
            this.currentMode = mode;
            Companion.getLogger().v("view width = " + lytParams.width);
        }
    }

    public final void getCustomAnimator(@NotNull CustomAnimationMode mode, @NotNull Builder mainBuilder) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
    }

    public final void clear() {
        this.lastDistance = (String) null;
        TextView textView = this.distanceView;
        if (textView != null) {
            textView.setText("");
        }
        cancelProgressAnimator();
        ProgressBar progressBar = this.progressView;
        if (progressBar != null) {
            progressBar.setVisibility(8);
        }
    }

    public final void updateDisplay(@NotNull ManeuverDisplay event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        setDistance(event.maneuverState, event.distanceToPendingRoadText, event.distanceInMeters);
        this.lastManeuverState = event.maneuverState;
    }

    private final void setDistance(ManeuverState maneuverState, String text, long distanceInMeters) {
        if (!TextUtils.equals(text, this.lastDistance) || !Intrinsics.areEqual(this.lastManeuverState,  maneuverState)) {
            ProgressBar progressBar;
            TextView textView;
            ImageView imageView;
            if (maneuverState == null) {
                cancelProgressAnimator();
                progressBar = this.progressView;
                if (progressBar != null) {
                    progressBar.setVisibility(8);
                }
                textView = this.distanceView;
                if (textView != null) {
                    textView.setText("");
                }
                imageView = this.nowView;
                if (imageView != null) {
                    imageView.setAlpha(0.0f);
                }
                imageView = this.nowView;
                if (imageView != null) {
                    imageView.setScaleX(0.9f);
                }
                imageView = this.nowView;
                if (imageView != null) {
                    imageView.setScaleY(0.9f);
                }
                imageView = this.nowView;
                if (imageView != null) {
                    imageView.setVisibility(8);
                }
                setVisibility(8);
                return;
            }
            boolean showNowIcon;
            boolean hideNowIcon;
            AnimatorSet fadeIn;
            TextView textView2;
            AnimatorSet distanceAnimator;
            ObjectAnimator fadeIn2;
            AnimatorSet duration;
            boolean showProgress;
            boolean hideProgress;
            this.lastDistance = text;
            textView = this.distanceView;
            if (textView != null) {
                if (text != null) {
                    text = text;
                } else {
                    CharSequence text2 = "";
                }
                textView.setText(text);
            }
            if ((Intrinsics.areEqual(this.lastManeuverState, ManeuverState.NOW) ^ 1) != 0) {
                if (Intrinsics.areEqual( maneuverState, ManeuverState.NOW)) {
                    showNowIcon = true;
                    if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.NOW)) {
                        if ((Intrinsics.areEqual( maneuverState, ManeuverState.NOW) ^ 1) != 0) {
                            hideNowIcon = true;
                            if (!showNowIcon) {
                                imageView = this.nowView;
                                if (imageView != null) {
                                    imageView.setAlpha(0.0f);
                                }
                                imageView = this.nowView;
                                if (imageView != null) {
                                    imageView.setVisibility(0);
                                }
                                fadeIn = HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowView);
                                cancelProgressAnimator();
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(8);
                                }
                                textView2 = this.distanceView;
                                if (textView2 != null) {
                                    textView2.setAlpha(0.0f);
                                }
                                distanceAnimator = new AnimatorSet();
                                distanceAnimator.play(fadeIn);
                                distanceAnimator.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                            } else if (hideNowIcon) {
                                imageView = this.nowView;
                                if ((Intrinsics.areEqual(imageView == null ? Integer.valueOf(imageView.getVisibility()) : null, Integer.valueOf(0)) ^ 1) != 0) {
                                    textView2 = this.distanceView;
                                    if (textView2 != null) {
                                        textView2.setAlpha(1.0f);
                                    }
                                }
                            } else {
                                imageView = this.nowView;
                                if (imageView != null) {
                                    imageView.setVisibility(8);
                                }
                                imageView = this.nowView;
                                if (imageView != null) {
                                    imageView.setAlpha(0.0f);
                                }
                                fadeIn2 = HomeScreenUtils.getAlphaAnimator(this.distanceView, 1);
                                distanceAnimator = new AnimatorSet();
                                distanceAnimator.play(fadeIn2);
                                duration = distanceAnimator.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION());
                                if (duration != null) {
                                    duration.start();
                                }
                            }
                            if ((Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON) ^ 1) != 0) {
                                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                    showProgress = true;
                                    if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON)) {
                                        if ((Intrinsics.areEqual( maneuverState, ManeuverState.SOON) ^ 1) != 0) {
                                            hideProgress = true;
                                            if (!hideProgress) {
                                                progressBar = this.progressView;
                                                if (progressBar != null) {
                                                    progressBar.setVisibility(8);
                                                }
                                                cancelProgressAnimator();
                                                this.initialProgressBarDistance = 0;
                                            } else if (showProgress) {
                                                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                                    HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                                }
                                            } else {
                                                cancelProgressAnimator();
                                                this.initialProgressBarDistance = (int) distanceInMeters;
                                                progressBar = this.progressView;
                                                if (progressBar != null) {
                                                    progressBar.setProgress(0);
                                                }
                                                progressBar = this.progressView;
                                                if (progressBar != null) {
                                                    progressBar.setVisibility(0);
                                                }
                                                progressBar = this.progressView;
                                                if (progressBar != null) {
                                                    progressBar.requestLayout();
                                                }
                                            }
                                            setVisibility(0);
                                        }
                                    }
                                    hideProgress = false;
                                    if (!hideProgress) {
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setVisibility(8);
                                        }
                                        cancelProgressAnimator();
                                        this.initialProgressBarDistance = 0;
                                    } else if (showProgress) {
                                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                            HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                        }
                                    } else {
                                        cancelProgressAnimator();
                                        this.initialProgressBarDistance = (int) distanceInMeters;
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setProgress(0);
                                        }
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setVisibility(0);
                                        }
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.requestLayout();
                                        }
                                    }
                                    setVisibility(0);
                                }
                            }
                            showProgress = false;
                            if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON)) {
                                if ((Intrinsics.areEqual( maneuverState, ManeuverState.SOON) ^ 1) != 0) {
                                    hideProgress = true;
                                    if (!hideProgress) {
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setVisibility(8);
                                        }
                                        cancelProgressAnimator();
                                        this.initialProgressBarDistance = 0;
                                    } else if (showProgress) {
                                        cancelProgressAnimator();
                                        this.initialProgressBarDistance = (int) distanceInMeters;
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setProgress(0);
                                        }
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setVisibility(0);
                                        }
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.requestLayout();
                                        }
                                    } else {
                                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                            HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                        }
                                    }
                                    setVisibility(0);
                                }
                            }
                            hideProgress = false;
                            if (!hideProgress) {
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(8);
                                }
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = 0;
                            } else if (showProgress) {
                                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                    HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                }
                            } else {
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = (int) distanceInMeters;
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setProgress(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.requestLayout();
                                }
                            }
                            setVisibility(0);
                        }
                    }
                    hideNowIcon = false;
                    if (!showNowIcon) {
                        imageView = this.nowView;
                        if (imageView != null) {
                            imageView.setAlpha(0.0f);
                        }
                        imageView = this.nowView;
                        if (imageView != null) {
                            imageView.setVisibility(0);
                        }
                        fadeIn = HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowView);
                        cancelProgressAnimator();
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(8);
                        }
                        textView2 = this.distanceView;
                        if (textView2 != null) {
                            textView2.setAlpha(0.0f);
                        }
                        distanceAnimator = new AnimatorSet();
                        distanceAnimator.play(fadeIn);
                        distanceAnimator.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                    } else if (hideNowIcon) {
                        imageView = this.nowView;
                        if (imageView == null) {
                        }
                        if ((Intrinsics.areEqual(imageView == null ? Integer.valueOf(imageView.getVisibility()) : null, Integer.valueOf(0)) ^ 1) != 0) {
                            textView2 = this.distanceView;
                            if (textView2 != null) {
                                textView2.setAlpha(1.0f);
                            }
                        }
                    } else {
                        imageView = this.nowView;
                        if (imageView != null) {
                            imageView.setVisibility(8);
                        }
                        imageView = this.nowView;
                        if (imageView != null) {
                            imageView.setAlpha(0.0f);
                        }
                        fadeIn2 = HomeScreenUtils.getAlphaAnimator(this.distanceView, 1);
                        distanceAnimator = new AnimatorSet();
                        distanceAnimator.play(fadeIn2);
                        duration = distanceAnimator.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION());
                        if (duration != null) {
                            duration.start();
                        }
                    }
                    if ((Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON) ^ 1) != 0) {
                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                            showProgress = true;
                            if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON)) {
                                if ((Intrinsics.areEqual( maneuverState, ManeuverState.SOON) ^ 1) != 0) {
                                    hideProgress = true;
                                    if (!hideProgress) {
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setVisibility(8);
                                        }
                                        cancelProgressAnimator();
                                        this.initialProgressBarDistance = 0;
                                    } else if (showProgress) {
                                        cancelProgressAnimator();
                                        this.initialProgressBarDistance = (int) distanceInMeters;
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setProgress(0);
                                        }
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setVisibility(0);
                                        }
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.requestLayout();
                                        }
                                    } else {
                                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                            HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                        }
                                    }
                                    setVisibility(0);
                                }
                            }
                            hideProgress = false;
                            if (!hideProgress) {
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(8);
                                }
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = 0;
                            } else if (showProgress) {
                                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                    HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                }
                            } else {
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = (int) distanceInMeters;
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setProgress(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.requestLayout();
                                }
                            }
                            setVisibility(0);
                        }
                    }
                    showProgress = false;
                    if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON)) {
                        if ((Intrinsics.areEqual( maneuverState, ManeuverState.SOON) ^ 1) != 0) {
                            hideProgress = true;
                            if (!hideProgress) {
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(8);
                                }
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = 0;
                            } else if (showProgress) {
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = (int) distanceInMeters;
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setProgress(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.requestLayout();
                                }
                            } else {
                                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                    HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                }
                            }
                            setVisibility(0);
                        }
                    }
                    hideProgress = false;
                    if (!hideProgress) {
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(8);
                        }
                        cancelProgressAnimator();
                        this.initialProgressBarDistance = 0;
                    } else if (showProgress) {
                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                            HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                        }
                    } else {
                        cancelProgressAnimator();
                        this.initialProgressBarDistance = (int) distanceInMeters;
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setProgress(0);
                        }
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(0);
                        }
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.requestLayout();
                        }
                    }
                    setVisibility(0);
                }
            }
            showNowIcon = false;
            if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.NOW)) {
                if ((Intrinsics.areEqual( maneuverState, ManeuverState.NOW) ^ 1) != 0) {
                    hideNowIcon = true;
                    if (!showNowIcon) {
                        imageView = this.nowView;
                        if (imageView != null) {
                            imageView.setAlpha(0.0f);
                        }
                        imageView = this.nowView;
                        if (imageView != null) {
                            imageView.setVisibility(0);
                        }
                        fadeIn = HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowView);
                        cancelProgressAnimator();
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(8);
                        }
                        textView2 = this.distanceView;
                        if (textView2 != null) {
                            textView2.setAlpha(0.0f);
                        }
                        distanceAnimator = new AnimatorSet();
                        distanceAnimator.play(fadeIn);
                        distanceAnimator.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                    } else if (hideNowIcon) {
                        imageView = this.nowView;
                        if (imageView != null) {
                            imageView.setVisibility(8);
                        }
                        imageView = this.nowView;
                        if (imageView != null) {
                            imageView.setAlpha(0.0f);
                        }
                        fadeIn2 = HomeScreenUtils.getAlphaAnimator(this.distanceView, 1);
                        distanceAnimator = new AnimatorSet();
                        distanceAnimator.play(fadeIn2);
                        duration = distanceAnimator.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION());
                        if (duration != null) {
                            duration.start();
                        }
                    } else {
                        imageView = this.nowView;
                        if (imageView == null) {
                        }
                        if ((Intrinsics.areEqual(imageView == null ? Integer.valueOf(imageView.getVisibility()) : null, Integer.valueOf(0)) ^ 1) != 0) {
                            textView2 = this.distanceView;
                            if (textView2 != null) {
                                textView2.setAlpha(1.0f);
                            }
                        }
                    }
                    if ((Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON) ^ 1) != 0) {
                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                            showProgress = true;
                            if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON)) {
                                if ((Intrinsics.areEqual( maneuverState, ManeuverState.SOON) ^ 1) != 0) {
                                    hideProgress = true;
                                    if (!hideProgress) {
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setVisibility(8);
                                        }
                                        cancelProgressAnimator();
                                        this.initialProgressBarDistance = 0;
                                    } else if (showProgress) {
                                        cancelProgressAnimator();
                                        this.initialProgressBarDistance = (int) distanceInMeters;
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setProgress(0);
                                        }
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.setVisibility(0);
                                        }
                                        progressBar = this.progressView;
                                        if (progressBar != null) {
                                            progressBar.requestLayout();
                                        }
                                    } else {
                                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                            HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                        }
                                    }
                                    setVisibility(0);
                                }
                            }
                            hideProgress = false;
                            if (!hideProgress) {
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(8);
                                }
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = 0;
                            } else if (showProgress) {
                                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                    HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                }
                            } else {
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = (int) distanceInMeters;
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setProgress(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.requestLayout();
                                }
                            }
                            setVisibility(0);
                        }
                    }
                    showProgress = false;
                    if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON)) {
                        if ((Intrinsics.areEqual( maneuverState, ManeuverState.SOON) ^ 1) != 0) {
                            hideProgress = true;
                            if (!hideProgress) {
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(8);
                                }
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = 0;
                            } else if (showProgress) {
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = (int) distanceInMeters;
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setProgress(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.requestLayout();
                                }
                            } else {
                                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                    HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                }
                            }
                            setVisibility(0);
                        }
                    }
                    hideProgress = false;
                    if (!hideProgress) {
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(8);
                        }
                        cancelProgressAnimator();
                        this.initialProgressBarDistance = 0;
                    } else if (showProgress) {
                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                            HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                        }
                    } else {
                        cancelProgressAnimator();
                        this.initialProgressBarDistance = (int) distanceInMeters;
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setProgress(0);
                        }
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(0);
                        }
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.requestLayout();
                        }
                    }
                    setVisibility(0);
                }
            }
            hideNowIcon = false;
            if (!showNowIcon) {
                imageView = this.nowView;
                if (imageView != null) {
                    imageView.setAlpha(0.0f);
                }
                imageView = this.nowView;
                if (imageView != null) {
                    imageView.setVisibility(0);
                }
                fadeIn = HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowView);
                cancelProgressAnimator();
                progressBar = this.progressView;
                if (progressBar != null) {
                    progressBar.setVisibility(8);
                }
                textView2 = this.distanceView;
                if (textView2 != null) {
                    textView2.setAlpha(0.0f);
                }
                distanceAnimator = new AnimatorSet();
                distanceAnimator.play(fadeIn);
                distanceAnimator.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
            } else if (hideNowIcon) {
                imageView = this.nowView;
                if (imageView == null) {
                }
                if ((Intrinsics.areEqual(imageView == null ? Integer.valueOf(imageView.getVisibility()) : null, Integer.valueOf(0)) ^ 1) != 0) {
                    textView2 = this.distanceView;
                    if (textView2 != null) {
                        textView2.setAlpha(1.0f);
                    }
                }
            } else {
                imageView = this.nowView;
                if (imageView != null) {
                    imageView.setVisibility(8);
                }
                imageView = this.nowView;
                if (imageView != null) {
                    imageView.setAlpha(0.0f);
                }
                fadeIn2 = HomeScreenUtils.getAlphaAnimator(this.distanceView, 1);
                distanceAnimator = new AnimatorSet();
                distanceAnimator.play(fadeIn2);
                duration = distanceAnimator.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION());
                if (duration != null) {
                    duration.start();
                }
            }
            if ((Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON) ^ 1) != 0) {
                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                    showProgress = true;
                    if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON)) {
                        if ((Intrinsics.areEqual( maneuverState, ManeuverState.SOON) ^ 1) != 0) {
                            hideProgress = true;
                            if (!hideProgress) {
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(8);
                                }
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = 0;
                            } else if (showProgress) {
                                cancelProgressAnimator();
                                this.initialProgressBarDistance = (int) distanceInMeters;
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setProgress(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.setVisibility(0);
                                }
                                progressBar = this.progressView;
                                if (progressBar != null) {
                                    progressBar.requestLayout();
                                }
                            } else {
                                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                                    HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                                }
                            }
                            setVisibility(0);
                        }
                    }
                    hideProgress = false;
                    if (!hideProgress) {
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(8);
                        }
                        cancelProgressAnimator();
                        this.initialProgressBarDistance = 0;
                    } else if (showProgress) {
                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                            HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                        }
                    } else {
                        cancelProgressAnimator();
                        this.initialProgressBarDistance = (int) distanceInMeters;
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setProgress(0);
                        }
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(0);
                        }
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.requestLayout();
                        }
                    }
                    setVisibility(0);
                }
            }
            showProgress = false;
            if (Intrinsics.areEqual(this.lastManeuverState, ManeuverState.SOON)) {
                if ((Intrinsics.areEqual( maneuverState, ManeuverState.SOON) ^ 1) != 0) {
                    hideProgress = true;
                    if (!hideProgress) {
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(8);
                        }
                        cancelProgressAnimator();
                        this.initialProgressBarDistance = 0;
                    } else if (showProgress) {
                        cancelProgressAnimator();
                        this.initialProgressBarDistance = (int) distanceInMeters;
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setProgress(0);
                        }
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.setVisibility(0);
                        }
                        progressBar = this.progressView;
                        if (progressBar != null) {
                            progressBar.requestLayout();
                        }
                    } else {
                        if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                            HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                        }
                    }
                    setVisibility(0);
                }
            }
            hideProgress = false;
            if (!hideProgress) {
                progressBar = this.progressView;
                if (progressBar != null) {
                    progressBar.setVisibility(8);
                }
                cancelProgressAnimator();
                this.initialProgressBarDistance = 0;
            } else if (showProgress) {
                if (Intrinsics.areEqual( maneuverState, ManeuverState.SOON)) {
                    HomeScreenUtils.getProgressBarAnimator(this.progressView, (int) (((double) 100) * (((double) 1) - (((double) distanceInMeters) / ((double) this.initialProgressBarDistance))))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
                }
            } else {
                cancelProgressAnimator();
                this.initialProgressBarDistance = (int) distanceInMeters;
                progressBar = this.progressView;
                if (progressBar != null) {
                    progressBar.setProgress(0);
                }
                progressBar = this.progressView;
                if (progressBar != null) {
                    progressBar.setVisibility(0);
                }
                progressBar = this.progressView;
                if (progressBar != null) {
                    progressBar.requestLayout();
                }
            }
            setVisibility(0);
        }
    }

    private final void cancelProgressAnimator() {
        ValueAnimator valueAnimator = this.progressAnimator;
        if (valueAnimator != null ? valueAnimator.isRunning() : false) {
            valueAnimator = this.progressAnimator;
            if (valueAnimator != null) {
                valueAnimator.removeAllListeners();
            }
            valueAnimator = this.progressAnimator;
            if (valueAnimator != null) {
                valueAnimator.cancel();
            }
            this.progressAnimator = (ValueAnimator) null;
        }
    }
}
