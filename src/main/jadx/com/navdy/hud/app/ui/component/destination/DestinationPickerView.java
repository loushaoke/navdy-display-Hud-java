package com.navdy.hud.app.ui.component.destination;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent.Callback;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import javax.inject.Inject;
import mortar.Mortar;

public class DestinationPickerView extends RelativeLayout implements IInputHandler {
    private static final Logger sLogger = new Logger(DestinationPickerView.class);
    private Callback callback;
    @InjectView(R.id.confirmationLayout)
    ConfirmationLayout confirmationLayout;
    @Inject
    public Presenter presenter;
    @InjectView(R.id.rightBackground)
    View rightBackground;
    VerticalMenuComponent vmenuComponent;

    public DestinationPickerView(Context context) {
        this(context, null);
    }

    public DestinationPickerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DestinationPickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.callback = new Callback() {
            public void select(ItemSelectionState selection) {
                selection.pos++;
                DestinationPickerView.this.presenter.itemClicked(selection);
            }

            public void onLoad() {
                DestinationPickerView.sLogger.v("onLoad");
                if (DestinationPickerView.this.presenter != null && (DestinationPickerView.this.presenter.isShowingRouteMap() || DestinationPickerView.this.presenter.isShowDestinationMap())) {
                    DestinationPickerView.this.setBackgroundColor(0);
                    DestinationPickerView.this.getHandler().postDelayed(new Runnable() {
                        public void run() {
                            if (!DestinationPickerView.this.presenter.isAlive()) {
                                return;
                            }
                            if (DestinationPickerView.this.presenter.isShowingRouteMap()) {
                                DestinationPickerView.this.presenter.startMapFluctuator();
                            } else if (DestinationPickerView.this.presenter.isShowDestinationMap()) {
                                DestinationPickerView.this.presenter.itemSelected(DestinationPickerView.this.presenter.initialSelection);
                                DestinationPickerView.this.presenter.itemSelection = DestinationPickerView.this.presenter.initialSelection;
                            }
                        }
                    }, 1000);
                }
                DestinationPickerView.this.vmenuComponent.animateIn(null);
            }

            public boolean isItemClickable(ItemSelectionState selection) {
                return true;
            }

            public void onBindToView(Model model, View view, int pos, ModelState state) {
            }

            public void onItemSelected(ItemSelectionState selection) {
                selection.pos++;
                DestinationPickerView.this.presenter.itemSelected(selection);
            }

            public void onScrollIdle() {
            }

            public void onFastScrollStart() {
            }

            public void onFastScrollEnd() {
            }

            public void showToolTip() {
            }

            public void close() {
                AnalyticsSupport.recordRouteSelectionCancelled();
                DestinationPickerView.this.presenter.close();
            }

            public boolean isClosed() {
                return DestinationPickerView.this.presenter.isClosed();
            }
        };
        if (isInEditMode()) {
            HudApplication.setContext(context);
        } else {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.vmenuComponent = new VerticalMenuComponent(this, this.callback, false);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        if (this.presenter != null) {
            this.presenter.dropView((View) this);
        }
    }

    public boolean onGesture(GestureEvent event) {
        if (this.confirmationLayout.getVisibility() == 0) {
            return false;
        }
        return this.vmenuComponent.handleGesture(event);
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.confirmationLayout.getVisibility() == 0) {
            return this.confirmationLayout.handleKey(event);
        }
        return this.vmenuComponent.handleKey(event);
    }

    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler(this);
    }
}
