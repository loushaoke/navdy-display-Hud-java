package com.navdy.hud.app.ui.activity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import com.google.gson.Gson;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.ancs.AncsServiceConnector;
import com.navdy.hud.app.common.ProdModule;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.debug.GestureEngine;
import com.navdy.hud.app.debug.SettingsActivity;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialConstants.BatteryChangeEvent;
import com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus;
import com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status;
import com.navdy.hud.app.device.dial.DialConstants.NotificationReason;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.device.gps.GpsUtils.GpsSatelliteData;
import com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.event.DisplayScaleChange;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.event.InitEvents.InitPhase;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.AdaptiveBrightnessNotification;
import com.navdy.hud.app.framework.BrightnessNotification;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.music.MusicDetailsScreen;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationManager.NotificationChange;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.phonecall.CallNotification;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.framework.voice.VoiceAssistNotification;
import com.navdy.hud.app.framework.voice.VoiceSearchNotification;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture;
import com.navdy.hud.app.manager.InitManager;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.LocationFix;
import com.navdy.hud.app.maps.here.HereLocationFixManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ConnectionType;
import com.navdy.hud.app.presenter.NotificationPresenter;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.screen.AutoBrightnessScreen;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.screen.DialManagerScreen;
import com.navdy.hud.app.screen.DialUpdateConfirmationScreen;
import com.navdy.hud.app.screen.DialUpdateProgressScreen;
import com.navdy.hud.app.screen.FactoryResetScreen;
import com.navdy.hud.app.screen.FirstLaunchScreen;
import com.navdy.hud.app.screen.ForceUpdateScreen;
import com.navdy.hud.app.screen.GestureLearningScreen;
import com.navdy.hud.app.screen.OSUpdateConfirmationScreen;
import com.navdy.hud.app.screen.ShutDownScreen;
import com.navdy.hud.app.screen.TemperatureWarningScreen;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.hud.app.settings.MainScreenSettings;
import com.navdy.hud.app.settings.MainScreenSettings.ScreenConfiguration;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.SystemTrayView;
import com.navdy.hud.app.ui.component.SystemTrayView.Device;
import com.navdy.hud.app.ui.component.SystemTrayView.State;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.ui.framework.IScreenAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.FeatureUtil.Feature;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.util.OTAUpdateService.InstallingUpdate;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.util.ScreenOrientation;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.view.ContainerView;
import com.navdy.hud.app.view.MainView;
import com.navdy.hud.app.view.NotificationView;
import com.navdy.hud.app.view.ToastView;
import com.navdy.service.library.Version;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.events.settings.NetworkStateChange;
import com.navdy.service.library.events.settings.ReadSettingsRequest;
import com.navdy.service.library.events.settings.ReadSettingsResponse;
import com.navdy.service.library.events.settings.UpdateSettings;
import com.navdy.service.library.events.ui.DismissScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import flow.Backstack;
import flow.Flow;
import flow.Flow.Callback;
import flow.Flow.Direction;
import flow.Flow.Listener;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javax.inject.Inject;
import javax.inject.Singleton;
import mortar.Blueprint;
import mortar.Mortar;

public class Main extends BaseScreen {
    public static final String DEMO_VIDEO_PROPERTY = "persist.sys.demo.video";
    private static final String INSTALLING_OTA_TOAST_ID = "#ota#toast";
    public static final String MIME_TYPE_VIDEO = "video/mp4";
    static final String PREFERENCE_HOME_SCREEN_MODE = "PREFERENCE_LAST_HOME_SCREEN_MODE";
    private static SparseArray<Class> SCREEN_MAP = null;
    public static final int SNAPSHOT_TITLE_PICKER_DELAY_MILLIS = 2000;
    private static final boolean TRIPLE_CLICK_BRIGHTNESS = SystemProperties.getBoolean("persist.hud.tclick.brightness", false);
    public static ProtocolStatus mProtocolStatus = ProtocolStatus.PROTOCOL_VALID;
    private static Main mainScreen;
    private static Presenter presenter;
    private static final Logger sLogger = new Logger(Main.class);

    public interface INotificationExtensionView {
        void onStart();

        void onStop();
    }

    @dagger.Module(addsTo = ProdModule.class, injects = {MainActivity.class, MainView.class, NotificationView.class, NotificationPresenter.class, UIStateManager.class, ContainerView.class, GestureEngine.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<MainView> implements Listener, IInputHandler {
        public static final String MAIN_SETTINGS = "main.settings";
        private boolean animationRunning;
        @Inject
        Bus bus;
        @Inject
        CallManager callManager;
        @Inject
        ConnectionHandler connectionHandler;
        private boolean createdScreens;
        private Screen currentScreen;
        private int defaultNotifColor;
        @Inject
        DialSimulatorMessagesHandler dialSimulatorMessagesHandler;
        @Inject
        DriverProfileManager driverProfileManager;
        Flow flow;
        @Inject
        SharedPreferences globalPreferences;
        private Handler handler = new Handler();
        private HUDSettings hudSettings;
        private InitManager initManager;
        private MultipleClickGestureDetector innerEventsDetector = new MultipleClickGestureDetector(4, new IMultipleClickKeyGesture() {
            public IInputHandler nextHandler() {
                return null;
            }

            public boolean onGesture(GestureEvent gestureEvent) {
                return false;
            }

            public boolean onKey(CustomKeyEvent customKeyEvent) {
                return Presenter.this.executeKeyEvent(customKeyEvent);
            }

            public void onMultipleClick(int n) {
                Presenter.this.executeMultipleKeyEvent(n);
            }
        });
        @Inject
        InputManager inputManager;
        @Inject
        MusicManager musicManager;
        private boolean notifAnimationRunning;
        private INotificationAnimationListener notificationAnimationListener = new INotificationAnimationListener() {
            public void onStart(String s, NotificationType notificationType, Mode mode) {
                Main.sLogger.v("notification anim start id[" + s + "]  type[" + notificationType + "] mode[" + mode + "]");
                if (mode == Mode.EXPAND) {
                    Presenter.this.setNotificationColorVisibility(4);
                    Presenter.this.setSystemTrayVisibility(4);
                }
                Presenter.this.clearInputFocus();
                Presenter.this.notifAnimationRunning = true;
            }

            public void onStop(String s, NotificationType notificationType, Mode mode) {
                Main.sLogger.v("notification anim stop id[" + s + "]  type[" + notificationType + "] mode[" + mode + "]");
                Presenter.this.notifAnimationRunning = false;
                if (mode == Mode.COLLAPSE) {
                    Presenter.this.updateSystemTrayVisibility(ScreenCategory.NOTIFICATION, null);
                }
                Presenter.this.runQueuedOperation();
            }
        };
        private boolean notificationColorEnabled = true;
        private NotificationManager notificationManager;
        private Queue<ScreenInfo> operationQueue = new LinkedList();
        @Inject
        PandoraManager pandoraManager;
        @Inject
        PowerManager powerManager;
        @Inject
        SharedPreferences preferences;
        private IScreenAnimationListener screenAnimationListener = new IScreenAnimationListener() {
            public void onStart(BaseScreen baseScreen, BaseScreen baseScreen2) {
                Main.sLogger.v("screen anim start in[" + baseScreen + "]  out[" + baseScreen2 + "]");
                Presenter.this.clearInputFocus();
                UIStateManager uiStateManager = Presenter.this.uiStateManager;
                if (!UIStateManager.isFullscreenMode(baseScreen.getScreen())) {
                    Presenter.this.setNotificationColorVisibility(4);
                    Presenter.this.setSystemTrayVisibility(4);
                }
            }

            public void onStop(BaseScreen baseScreen, BaseScreen baseScreen2) {
                Main.sLogger.v("screen anim stop in[" + baseScreen + "]  out[" + baseScreen2 + "]");
                Presenter.this.updateSystemTrayVisibility(ScreenCategory.SCREEN, baseScreen.getScreen());
            }
        };
        private DeferredServices services;
        private MainScreenSettings settings;
        @Inject
        SettingsManager settingsManager;
        private boolean showingPortrait;
        private boolean systemTrayEnabled = true;
        @Inject
        TripManager tripManager;
        @Inject
        UIStateManager uiStateManager;
        private VoiceAssistNotification voiceAssistNotification;
        private VoiceSearchNotification voiceSearchNotification;

        public static class DeferredServices {
            @Inject
            public AncsServiceConnector ancsService;
            @Inject
            public GestureServiceConnector gestureService;

            DeferredServices(Context context) {
                Mortar.inject(context, this);
            }
        }

        private void clearViews() {
            MainView mainView = (MainView) getView();
            if (mainView != null && mainView.isNotificationViewShowing() && !this.notifAnimationRunning) {
                Main.sLogger.v("NotificationManager:dismissNotification");
                mainView.dismissNotification();
            }
        }

        private boolean executeKeyEvent(CustomKeyEvent customKeyEvent) {
            switch (customKeyEvent) {
                case SELECT:
                    this.bus.post(new Builder().screen(Screen.SCREEN_MAIN_MENU).build());
                    return true;
                case LONG_PRESS:
                    RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
                    if (!instance.isRemoteDeviceConnected() || !instance.isAppConnected()) {
                        this.connectionHandler.sendConnectionNotification();
                    } else if (DriverProfileHelper.getInstance().getCurrentProfile().getLongPressAction() == DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH) {
                        Main.sLogger.v("longpress, Place Search ");
                        launchVoiceSearch();
                    } else {
                        startVoiceAssistance(false);
                    }
                    return true;
                case POWER_BUTTON_LONG_PRESS:
                    Main.sLogger.v("power button click: show shutdown");
                    this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_SHUTDOWN_CONFIRMATION, Reason.POWER_BUTTON.asBundle(), false));
                    return true;
                case POWER_BUTTON_DOUBLE_CLICK:
                    Main.sLogger.v("power button double click: show dial pairing");
                    this.bus.post(new Builder().screen(Screen.SCREEN_DIAL_PAIRING).build());
                    return true;
                case POWER_BUTTON_CLICK:
                    Main.sLogger.v("power button click: no-op");
                    return false;
                default:
                    return false;
            }
        }

        private void executeMultipleKeyEvent(int n) {
            if (n == 2) {
                Main.sLogger.v("Double click");
                takeNotificationAction();
            } else if (n == 3) {
                Main.sLogger.v("Triple click");
                if (Main.TRIPLE_CLICK_BRIGHTNESS) {
                    Main.showBrightnessNotification();
                    return;
                }
                HomeScreenView homescreenView = this.uiStateManager.getHomescreenView();
                if (homescreenView.getDisplayMode() == DisplayMode.MAP) {
                    homescreenView.setDisplayMode(DisplayMode.SMART_DASH);
                    Main.saveHomeScreenPreference(this.globalPreferences, DisplayMode.SMART_DASH.ordinal());
                    return;
                }
                homescreenView.setDisplayMode(DisplayMode.MAP);
                Main.saveHomeScreenPreference(this.globalPreferences, DisplayMode.MAP.ordinal());
            } else if (n >= 4) {
                Main.sLogger.d("4 clicks, dumping a snapshot");
                ReportIssueService.dumpSnapshotAsync();
                if (!DeviceUtil.isUserBuild()) {
                    this.handler.postDelayed(new Runnable() {
                        public void run() {
                            ReportIssueService.showSnapshotMenu();
                        }
                    }, 2000);
                }
            }
        }

        private void hideNotification() {
            MainView mainView = (MainView) getView();
            if (mainView != null && this.notificationManager.getCurrentNotification() != null) {
                Main.sLogger.v("NotificationManager:calling shownotification");
                mainView.dismissNotification();
            }
        }

        private boolean isPortrait() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                this.showingPortrait = ScreenOrientation.isPortrait((Activity) mainView.getContext());
            }
            return this.showingPortrait;
        }

        private void launchVoiceAssistance(boolean b) {
            ToastManager instance = ToastManager.getInstance();
            if (instance.isToastDisplayed() && TextUtils.equals(instance.getCurrentToastId(), CallNotification.CALL_NOTIFICATION_TOAST_ID)) {
                Main.sLogger.v("startVoiceAssistance:cannot launch, phone toast");
                return;
            }
            INotification notification;
            instance.disableToasts(true);
            Main.sLogger.v("startVoiceAssistance: disable toast, current notif:" + this.notificationManager.getCurrentNotification());
            NotificationManager notificationManager = this.notificationManager;
            if (b) {
                notification = this.voiceSearchNotification;
            } else {
                notification = this.voiceAssistNotification;
            }
            notificationManager.addNotification(notification);
        }

        private void launchVoiceSearch() {
            NotificationManager instance = NotificationManager.getInstance();
            VoiceSearchNotification voiceSearchNotification = (VoiceSearchNotification) instance.getNotification(NotificationId.VOICE_SEARCH_NOTIFICATION_ID);
            if (voiceSearchNotification == null) {
                voiceSearchNotification = new VoiceSearchNotification();
            }
            instance.addNotification(voiceSearchNotification);
        }

        private static Blueprint lookupScreen(Screen screen_HOME) {
            try {
                switch (screen_HOME) {
                    case SCREEN_HYBRID_MAP:
                    case SCREEN_DASHBOARD:
                        screen_HOME = Screen.SCREEN_HOME;
                        break;
                }
                Class clz = (Class) Main.SCREEN_MAP.get(screen_HOME.ordinal());
                if (clz != null) {
                    return (Blueprint) clz.newInstance();
                }
            } catch (InstantiationException ex) {
                Main.sLogger.e("Failed to instantiate screen", ex);
            } catch (IllegalAccessException ex2) {
                Main.sLogger.e("Failed to instantiate screen", ex2);
            }
            return null;
        }

        private void persistSettings() {
            Main.sLogger.d("Starting to persist settings");
            String json = new Gson().toJson(this.settings);
            this.preferences.edit().putString(MAIN_SETTINGS, json).apply();
            Main.sLogger.d("Persisted settings:" + json);
        }

        private void runQueuedOperation() {
            int size = this.operationQueue.size();
            if (size > 0) {
                final ScreenInfo screenInfo = (ScreenInfo) this.operationQueue.remove();
                Main.sLogger.v("runQueuedOperation size:" + size + " posting:" + screenInfo.screen);
                this.handler.post(new Runnable() {
                    public void run() {
                        Presenter.this.updateScreen(screenInfo.screen, screenInfo.args, screenInfo.args2, true, screenInfo.dismissScreen);
                    }
                });
                return;
            }
            setInputFocus();
            this.animationRunning = false;
            Main.sLogger.v("animationRunning:false");
        }

        private void setDisplayFormat(DisplayFormat displayFormat) {
            Main.sLogger.i("Switching display format to " + displayFormat);
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                mainView.setDisplayFormat(displayFormat);
            }
        }

        private void setupFlow() {
            Backstack single = Backstack.single(new FirstLaunchScreen());
            this.currentScreen = Screen.SCREEN_FIRST_LAUNCH;
            this.uiStateManager.setCurrentViewMode(this.currentScreen);
            Flow flow = new Flow(single, this);
            this.flow = flow;
            flow.resetTo(this.flow.getBackstack().current().getScreen());
        }

        private void signalBootComplete() {
            SystemProperties.set("dev.app_started", ToastPresenter.EXTRA_MAIN_TITLE);
            SystemProperties.set("dev.late_service", ToastPresenter.EXTRA_MAIN_TITLE);
        }

        private void startLateServices() {
            Main.sLogger.v("starting update service");
            Context appContext = HudApplication.getAppContext();
            Intent intent = new Intent(appContext, OTAUpdateService.class);
            intent.putExtra("COMMAND", OTAUpdateService.COMMAND_VERIFY_UPDATE);
            appContext.startService(intent);
            ReportIssueService.scheduleSync();
            Main.sLogger.v("initializing additional services");
            this.services = new DeferredServices(appContext);
            if (RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(Feature.GESTURE_ENGINE)) {
                Main.sLogger.v("gesture is supported");
                InputPreferences inputPreferences = DriverProfileHelper.getInstance().getCurrentProfile().getInputPreferences();
                if (inputPreferences == null || !inputPreferences.use_gestures.booleanValue()) {
                    Main.sLogger.v("gesture is not enabled");
                } else {
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            Main.sLogger.v("gesture is enabled, starting");
                            Presenter.this.services.gestureService.start();
                        }
                    }, 1);
                }
            }
        }

        private void takeNotificationAction() {
            RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
            if (!instance.isRemoteDeviceConnected() || !instance.isAppConnected()) {
                this.connectionHandler.sendConnectionNotification();
            } else if (this.notificationManager.isNotificationPresent(NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                Main.sLogger.v("show phone call");
                this.notificationManager.expandNotification();
            } else {
                Main.sLogger.v("show music");
                this.musicManager.showMusicNotification();
            }
        }

        private void updateDisplayFormat() {
            setDisplayFormat(this.driverProfileManager.getCurrentProfile().getDisplayFormat());
        }

        private void updateScreen(Screen screen, Bundle bundle, Object o, boolean b, boolean b2) {
            Main.sLogger.v("updateScreen screen[" + screen + "] animRunning[" + this.animationRunning + "] ignoreAnim [" + b + "] dismiss[" + b2 + "]");
            if (b || !this.animationRunning) {
                this.animationRunning = true;
                Main.sLogger.v("animationRunning:true");
                int n = 0;
                switch (screen) {
                    case SCREEN_MUSIC:
                        if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                            this.musicManager.showMusicNotification();
                        } else {
                            this.connectionHandler.sendConnectionNotification();
                        }
                        n = 1;
                        break;
                    case SCREEN_VOICE_CONTROL:
                        if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                            boolean z;
                            if (bundle != null) {
                                if (bundle.getBoolean(NotificationManager.EXTRA_VOICE_SEARCH_NOTIFICATION, false)) {
                                    z = true;
                                    startVoiceAssistance(z);
                                }
                            }
                            z = false;
                            startVoiceAssistance(z);
                        } else {
                            this.connectionHandler.sendConnectionNotification();
                        }
                        n = 1;
                        break;
                    case SCREEN_BRIGHTNESS:
                        Main.showBrightnessNotification();
                        n = 1;
                        break;
                }
                if (n != 0) {
                    Main.sLogger.v("notif screen");
                    runQueuedOperation();
                    return;
                }
                Screen currentViewMode = screen;
                if (screen == Screen.SCREEN_BACK) {
                    currentViewMode = this.uiStateManager.getCurrentViewMode();
                }
                if (!UIStateManager.isSidePanelMode(currentViewMode)) {
                    clearViews();
                    Blueprint lookupScreen = lookupScreen(currentViewMode);
                    if (lookupScreen instanceof BaseScreen) {
                        ((BaseScreen) lookupScreen).setArguments(bundle, o);
                    }
                    screen = currentViewMode;
                    if (lookupScreen instanceof HomeScreen) {
                        switch (currentViewMode) {
                            case SCREEN_HYBRID_MAP:
                                screen = Screen.SCREEN_HOME;
                                Main.saveHomeScreenPreference(this.globalPreferences, DisplayMode.MAP.ordinal());
                                ((HomeScreen) lookupScreen).setDisplayMode(DisplayMode.MAP);
                                break;
                            case SCREEN_DASHBOARD:
                                Main.saveHomeScreenPreference(this.globalPreferences, DisplayMode.SMART_DASH.ordinal());
                                ((HomeScreen) lookupScreen).setDisplayMode(DisplayMode.SMART_DASH);
                                screen = Screen.SCREEN_HOME;
                                break;
                            case SCREEN_HOME:
                                DisplayMode displayMode = ((HomeScreen) lookupScreen).getDisplayMode();
                                int n2 = displayMode.ordinal();
                                boolean defaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
                                SharedPreferences sharedPreferences = DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
                                if (defaultProfile || sharedPreferences == null) {
                                    sharedPreferences = this.globalPreferences;
                                }
                                if (sharedPreferences != null) {
                                    n2 = sharedPreferences.getInt(Main.PREFERENCE_HOME_SCREEN_MODE, displayMode.ordinal());
                                }
                                screen = currentViewMode;
                                if (n2 != displayMode.ordinal()) {
                                    if (n2 == DisplayMode.MAP.ordinal()) {
                                        ((HomeScreen) lookupScreen).setDisplayMode(DisplayMode.MAP);
                                    } else if (n2 == DisplayMode.SMART_DASH.ordinal()) {
                                        ((HomeScreen) lookupScreen).setDisplayMode(DisplayMode.SMART_DASH);
                                    }
                                    Main.saveHomeScreenPreference(this.globalPreferences, n2);
                                    screen = currentViewMode;
                                    break;
                                }
                                break;
                            default:
                                screen = currentViewMode;
                                break;
                        }
                    }
                    if (UIStateManager.isFullscreenMode(screen)) {
                        this.uiStateManager.setCurrentViewMode(screen);
                    }
                    if (lookupScreen != null) {
                        this.currentScreen = screen;
                        this.flow.replaceTo(lookupScreen);
                        return;
                    }
                    return;
                } else if (UIStateManager.isOverlayMode(this.currentScreen)) {
                    this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_BACK, null, true));
                    this.bus.post(new ShowScreenWithArgs(currentViewMode, bundle, o, true));
                    return;
                } else {
                    switch (currentViewMode) {
                        case SCREEN_NOTIFICATION:
                            if (b2) {
                                MainView mainView = (MainView) getView();
                                if (mainView != null && mainView.isNotificationViewShowing() && !this.notifAnimationRunning) {
                                    Main.sLogger.v("NotificationManager:calling dismissNotification");
                                    mainView.dismissNotification();
                                    return;
                                }
                                return;
                            }
                            showNotification();
                            return;
                        default:
                            return;
                    }
                }
            }
            this.operationQueue.add(new ScreenInfo(screen, bundle, o, b2));
        }

        private void updateSystemTrayVisibility(ScreenCategory screenCategory, Screen screen) {
            switch (screenCategory) {
                case NOTIFICATION:
                    BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
                    if (currentScreen != null) {
                        screen = currentScreen.getScreen();
                    }
                    UIStateManager uiStateManager = this.uiStateManager;
                    if (UIStateManager.isFullscreenMode(screen)) {
                        setNotificationColorVisibility(0);
                        setSystemTrayVisibility(0);
                        return;
                    }
                    return;
                case SCREEN:
                    if (!this.notifAnimationRunning) {
                        runQueuedOperation();
                    }
                    if (!this.notifAnimationRunning && !isNotificationViewShowing()) {
                        UIStateManager uiStateManager2 = this.uiStateManager;
                        if (UIStateManager.isFullscreenMode(screen)) {
                            setNotificationColorVisibility(0);
                            setSystemTrayVisibility(0);
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        private boolean verifyProtocolVersion(DeviceInfo deviceInfo) {
            while (true) {
                try {
                    break;
                } catch (NumberFormatException e) {
                    Main.sLogger.e("invalid remote device protocol version: " + deviceInfo.protocolVersion);
                }
            }
            int int1 = Integer.parseInt(deviceInfo.protocolVersion.split("\\.", 2)[0]);
            if (Version.PROTOCOL_VERSION.majorVersion == int1 || (Version.PROTOCOL_VERSION.majorVersion == 1 && int1 == 0)) {
                Main.mProtocolStatus = ProtocolStatus.PROTOCOL_VALID;
                return true;
            }
            if (Version.PROTOCOL_VERSION.majorVersion < int1) {
                Main.mProtocolStatus = ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE;
            } else {
                Main.mProtocolStatus = ProtocolStatus.PROTOCOL_REMOTE_NEEDS_UPDATE;
            }
            Main.sLogger.e(String.format("protocol version mismatch remote = %s, local = %s", new Object[]{deviceInfo.protocolVersion, Version.PROTOCOL_VERSION.toString()}));
            return false;
        }

        public void addNotificationExtensionView(View view) {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                mainView.addNotificationExtensionView(view);
            }
        }

        public void clearInputFocus() {
            if (!(this.inputManager.getFocus() instanceof ToastView)) {
                this.inputManager.setFocus(null);
                Main.sLogger.v("[focus] cleared");
            }
        }

        public void createScreens() {
            if (!this.createdScreens) {
                MainView mainView = (MainView) getView();
                if (mainView != null) {
                    Main.sLogger.v("createScreens");
                    ContainerView containerView = mainView.getContainerView();
                    this.createdScreens = true;
                    containerView.createScreens();
                }
            }
        }

        public void ejectMainLowerView() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                mainView.ejectMainLowerView();
            }
        }

        void enableNotificationColor(boolean notificationColorEnabled) {
            this.notificationColorEnabled = notificationColorEnabled;
            MainView mainView = (MainView) getView();
            if (mainView == null) {
                return;
            }
            if (this.notificationColorEnabled) {
                BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
                if (currentScreen != null) {
                    UIStateManager uiStateManager = this.uiStateManager;
                    if (UIStateManager.isFullscreenMode(currentScreen.getScreen())) {
                        setNotificationColorVisibility(0);
                        Main.sLogger.v("notif visible");
                        return;
                    }
                }
                mainView.setNotificationColorVisibility(4);
                Main.sLogger.v("notif invisible");
                return;
            }
            mainView.setNotificationColorVisibility(4);
            Main.sLogger.v("notif invisible");
        }

        void enableSystemTray(boolean systemTrayEnabled) {
            this.systemTrayEnabled = systemTrayEnabled;
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                SystemTrayView systemTray = mainView.getSystemTray();
                if (this.systemTrayEnabled) {
                    setSystemTrayVisibility(0);
                } else {
                    systemTray.setVisibility(4);
                }
            }
        }

        public ConnectionHandler getConnectionHandler() {
            return this.connectionHandler;
        }

        View getExpandedNotificationCoverView() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                return mainView.getExpandedNotificationCoverView();
            }
            return null;
        }

        FrameLayout getExpandedNotificationView() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                return mainView.getExpandedNotificationView();
            }
            return null;
        }

        ViewGroup getNotificationExtensionView() {
            FrameLayout notificationExtensionView;
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                notificationExtensionView = mainView.getNotificationExtensionView();
            } else {
                notificationExtensionView = null;
            }
            return notificationExtensionView;
        }

        CarouselIndicator getNotificationIndicator() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                return mainView.getNotificationIndicator();
            }
            return null;
        }

        ProgressIndicator getNotificationScrollIndicator() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                return mainView.getNotificationScrollIndicator();
            }
            return null;
        }

        NotificationView getNotificationView() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                return mainView.getNotificationView();
            }
            return null;
        }

        public ScreenConfiguration getScreenConfiguration() {
            if (isPortrait()) {
                return this.settings.getPortraitConfiguration();
            }
            return this.settings.getLandscapeConfiguration();
        }

        SystemTrayView getSystemTray() {
            MainView mainView = (MainView) getView();
            if (mainView == null) {
                return null;
            }
            return mainView.getSystemTray();
        }

        public void go(Backstack backstack, Direction direction, Callback callback) {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                ContainerView containerView = mainView.getContainerView();
                BaseScreen baseScreen = (BaseScreen) backstack.current().getScreen();
                int animationOut = -1;
                Backstack backstack2 = this.flow.getBackstack();
                if (backstack2.size() > 0) {
                    animationOut = ((BaseScreen) backstack2.current().getScreen()).getAnimationOut(Direction.FORWARD);
                }
                containerView.showScreen(baseScreen, Direction.FORWARD, baseScreen.getAnimationIn(Direction.FORWARD), animationOut);
            }
            callback.onComplete();
        }

        public void injectMainLowerView(View view) {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                mainView.injectMainLowerView(view);
            }
        }

        public boolean isMainLowerViewVisible() {
            MainView mainView = (MainView) getView();
            return mainView != null && mainView.isMainLowerViewVisible();
        }

        boolean isMainUIShrunk() {
            MainView mainView = (MainView) getView();
            return mainView != null && mainView.isMainUIShrunk();
        }

        boolean isNotificationCollapsing() {
            MainView mainView = (MainView) getView();
            return mainView != null && mainView.isNotificationCollapsing();
        }

        boolean isNotificationExpanding() {
            MainView mainView = (MainView) getView();
            return mainView != null && mainView.isNotificationExpanding();
        }

        boolean isNotificationViewShowing() {
            MainView mainView = (MainView) getView();
            return mainView != null && mainView.isNotificationViewShowing();
        }

        boolean isScreenAnimating() {
            MainView mainView = (MainView) getView();
            return mainView != null && mainView.isScreenAnimating();
        }

        public IInputHandler nextHandler() {
            return null;
        }

        @Subscribe
        public void onDeviceConnectionStateChange(ConnectionStateChange connectionStateChange) {
            Main.sLogger.v("status:" + connectionStateChange.remoteDeviceId + " : " + connectionStateChange.state.name());
            SystemTrayView systemTray = getSystemTray();
            switch (connectionStateChange.state) {
                case CONNECTION_VERIFIED:
                    this.musicManager.initialize();
                    if (systemTray != null) {
                        systemTray.setState(Device.Phone, State.CONNECTED);
                        return;
                    }
                    return;
                case CONNECTION_DISCONNECTED:
                    if (systemTray != null) {
                        systemTray.setState(Device.Phone, State.DISCONNECTED);
                    }
                    if (Main.mProtocolStatus == ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE && this.currentScreen == Screen.SCREEN_FORCE_UPDATE) {
                        this.bus.post(new Builder().screen(Screen.SCREEN_HOME).build());
                    }
                    Main.mProtocolStatus = ProtocolStatus.PROTOCOL_VALID;
                    return;
                default:
                    return;
            }
        }

        @Subscribe
        public void onDeviceInfo(DeviceInfo deviceInfo) {
            if (verifyProtocolVersion(deviceInfo)) {
                ConnectionStateChange.Builder builder = new ConnectionStateChange.Builder();
                builder.state(ConnectionState.CONNECTION_VERIFIED);
                builder.remoteDeviceId(deviceInfo.deviceId);
                this.bus.post(builder.build());
                return;
            }
            this.bus.post(new Builder().screen(Screen.SCREEN_FORCE_UPDATE).build());
        }

        @Subscribe
        public void onDialBatteryEvent(BatteryChangeEvent batteryChangeEvent) {
            SystemTrayView systemTray = getSystemTray();
            if (systemTray != null) {
                NotificationReason batteryNotificationReason = DialManager.getInstance().getBatteryNotificationReason();
                State state = null;
                if (batteryNotificationReason != null) {
                    switch (batteryNotificationReason) {
                        case EXTREMELY_LOW_BATTERY:
                            state = State.EXTREMELY_LOW_BATTERY;
                            break;
                        case LOW_BATTERY:
                        case VERY_LOW_BATTERY:
                            state = State.LOW_BATTERY;
                            break;
                        case OK_BATTERY:
                            state = State.OK_BATTERY;
                            break;
                    }
                }
                state = State.OK_BATTERY;
                systemTray.setState(Device.Dial, state);
            }
        }

        @Subscribe
        public void onDialConnectionEvent(DialConnectionStatus dialConnectionStatus) {
            SystemTrayView systemTray = getSystemTray();
            if (systemTray != null) {
                State state;
                if (dialConnectionStatus.status == Status.CONNECTED) {
                    state = State.CONNECTED;
                } else {
                    state = State.DISCONNECTED;
                }
                systemTray.setState(Device.Dial, state);
            }
        }

        @Subscribe
        public void onDismissScreen(DismissScreen dismissScreen) {
            if (dismissScreen.screen == Screen.SCREEN_NOTIFICATION) {
                updateScreen(Screen.SCREEN_NOTIFICATION, null, null, false, true);
            }
        }

        @Subscribe
        public void onDisplayScaleChange(DisplayScaleChange displayScaleChange) {
            setDisplayFormat(displayScaleChange.format);
        }

        @Subscribe
        public void onDriverProfileChanged(DriverProfileChanged driverProfileChanged) {
            updateDisplayFormat();
        }

        @Subscribe
        public void onDriverProfileUpdate(DriverProfileUpdated driverProfileUpdated) {
            if (driverProfileUpdated.state == DriverProfileUpdated.State.UPDATED) {
                updateDisplayFormat();
            }
        }

        protected void onExitScope() {
            super.onExitScope();
            Main.sLogger.i("Shutting down main service");
            this.connectionHandler.disconnect();
        }

        public boolean onGesture(GestureEvent gestureEvent) {
            if (gestureEvent == null || gestureEvent.gesture == null) {
                return false;
            }
            switch (gestureEvent.gesture) {
                case GESTURE_SWIPE_LEFT:
                    NotificationManager instance = NotificationManager.getInstance();
                    if (instance.getNotificationCount() > 0) {
                        AnalyticsSupport.setGlanceNavigationSource("swipe");
                    }
                    instance.expandNotification();
                    break;
            }
            return true;
        }

        @Subscribe
        public void onGpsSatelliteData(GpsSatelliteData gpsSatelliteData) {
            if (GpsUtils.isDebugRawGpsPosEnabled()) {
                int int1 = gpsSatelliteData.data.getInt(GpsConstants.GPS_FIX_TYPE, 0);
                SystemTrayView systemTray = getSystemTray();
                if (systemTray != null) {
                    String debugGpsMarker = null;
                    switch (int1) {
                        case 1:
                            debugGpsMarker = "2D";
                            break;
                        case 2:
                            debugGpsMarker = "Diff";
                            return;
                        case 3:
                        case 4:
                        case 5:
                            break;
                        case 6:
                            debugGpsMarker = "DR";
                            return;
                        default:
                            return;
                    }
                    systemTray.setDebugGpsMarker(debugGpsMarker);
                }
            }
        }

        @Subscribe
        public void onGpsSwitchEvent(GpsSwitch gpsSwitch) {
            if (GpsUtils.isDebugRawGpsPosEnabled()) {
                Main.sLogger.v("gps_switch_event:" + gpsSwitch.usingPhone);
                SystemTrayView systemTray = getSystemTray();
                if (systemTray != null) {
                    String debugGpsMarker;
                    if (gpsSwitch.usingPhone) {
                        debugGpsMarker = GpsConstants.GPS_PHONE_MARKER;
                    } else {
                        debugGpsMarker = GpsConstants.GPS_UBLOX_MARKER;
                    }
                    systemTray.setDebugGpsMarker(debugGpsMarker);
                }
            }
        }

        @Subscribe
        public void onInitEvent(InitPhase initPhase) {
            switch (initPhase.phase) {
                case PRE_USER_INTERACTION:
                    boolean b;
                    if (this.powerManager.inQuietMode()) {
                        this.powerManager.enterSleepMode();
                    } else {
                        HUDLightUtils.resetFrontLED(LightManager.getInstance());
                    }
                    String value = SystemProperties.get(Main.DEMO_VIDEO_PROPERTY);
                    if (TextUtils.isEmpty(value)) {
                        b = false;
                    } else {
                        b = true;
                    }
                    if (this.preferences.getBoolean(SettingsActivity.START_VIDEO_ON_BOOT_KEY, false) || b) {
                        File externalStoragePublicDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
                        signalBootComplete();
                        if (!b) {
                            value = "navdy-demo.mov";
                        }
                        File file = new File(externalStoragePublicDirectory, value);
                        Main.sLogger.d("Trying to play video: " + file);
                        if (file.exists()) {
                            playMedia(HudApplication.getAppContext(), Uri.fromFile(file));
                            return;
                        } else {
                            Main.sLogger.d("Missing demo video file...");
                            return;
                        }
                    }
                    return;
                case POST_START:
                    Main.sLogger.v("initializing obd manager");
                    ObdManager.getInstance();
                    if (!this.powerManager.inQuietMode()) {
                        startLateServices();
                    }
                    signalBootComplete();
                    return;
                default:
                    return;
            }
        }

        @Subscribe
        public void onInstallingUpdate(InstallingUpdate installingUpdate) {
            showInstallingUpdateToast();
        }

        public boolean onKey(CustomKeyEvent customKeyEvent) {
            return this.innerEventsDetector.onKey(customKeyEvent);
        }

        public void onLoad(Bundle savedInstanceState) {
            Main.presenter = this;
            this.notificationManager = NotificationManager.getInstance();
            this.voiceAssistNotification = new VoiceAssistNotification();
            this.voiceSearchNotification = new VoiceSearchNotification();
            this.defaultNotifColor = 0;
            this.uiStateManager.setRootScreen(Main.mainScreen);
            this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
            this.uiStateManager.addNotificationAnimationListener(this.notificationAnimationListener);
            super.onLoad(savedInstanceState);
            MainScreenSettings savedSettings = null;
            if (savedInstanceState != null) {
                savedSettings = (MainScreenSettings) savedInstanceState.getParcelable(MAIN_SETTINGS);
            }
            if (savedSettings == null) {
                String json = this.preferences.getString(MAIN_SETTINGS, null);
                try {
                    savedSettings = (MainScreenSettings) new Gson().fromJson(json, MainScreenSettings.class);
                } catch (Throwable th) {
                    Main.sLogger.e("Failed to parse json " + json);
                }
            }
            if (savedSettings == null) {
                savedSettings = new MainScreenSettings();
            }
            this.settings = savedSettings;
            this.settingsManager.addSetting(GpsUtils.SHOW_RAW_GPS);
            this.hudSettings = new HUDSettings(this.preferences);
            updateDisplayFormat();
            this.bus.register(this);
            this.connectionHandler.connect();
            setupFlow();
            updateView();
            this.bus.register(this.dialSimulatorMessagesHandler);
            this.bus.register(this.musicManager);
            this.bus.register(this.pandoraManager);
            this.bus.register(this.tripManager);
            this.initManager = new InitManager(this.bus, this.connectionHandler);
            this.initManager.start();
        }

        @Subscribe
        public void onLocationFixChangeEvent(LocationFix locationFix) {
            SystemTrayView systemTray = getSystemTray();
            if (systemTray == null) {
                return;
            }
            if (locationFix.locationAvailable) {
                systemTray.setState(Device.Gps, State.LOCATION_AVAILABLE);
                if (GpsUtils.isDebugRawGpsPosEnabled()) {
                    onGpsSwitchEvent(new GpsSwitch(locationFix.usingPhoneLocation, locationFix.usingLocalGpsLocation));
                    return;
                }
                return;
            }
            systemTray.setState(Device.Gps, State.LOCATION_LOST);
        }

        @Subscribe
        public void onNetworkStateChange(NetworkStateChange networkStateChange) {
            SystemTrayView systemTray = getSystemTray();
            if (systemTray == null) {
                return;
            }
            if (Boolean.TRUE.equals(networkStateChange.networkAvailable)) {
                systemTray.setState(Device.Phone, State.PHONE_NETWORK_AVAILABLE);
            } else {
                systemTray.setState(Device.Phone, State.NO_PHONE_NETWORK);
            }
        }

        @Subscribe
        public void onNotificationChange(NotificationChange notificationChange) {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                setNotificationColorVisibility(mainView.getNotificationColorVisibility());
            }
        }

        @Subscribe
        public void onPhoneBatteryStatusEvent(PhoneBatteryStatus phoneBatteryStatus) {
            Main.sLogger.v("PhoneBatteryStatus status[" + phoneBatteryStatus.status + "] level[" + phoneBatteryStatus.status + "] charging[" + phoneBatteryStatus.charging + "]");
            SystemTrayView systemTray = getSystemTray();
            if (systemTray == null) {
                return;
            }
            if (Boolean.TRUE.equals(phoneBatteryStatus.charging)) {
                systemTray.setState(Device.Phone, State.OK_BATTERY);
                return;
            }
            switch (phoneBatteryStatus.status) {
                case BATTERY_OK:
                    systemTray.setState(Device.Phone, State.OK_BATTERY);
                    return;
                case BATTERY_EXTREMELY_LOW:
                    systemTray.setState(Device.Phone, State.EXTREMELY_LOW_BATTERY);
                    return;
                case BATTERY_LOW:
                    systemTray.setState(Device.Phone, State.LOW_BATTERY);
                    return;
                default:
                    return;
            }
        }

        @Subscribe
        public void onReadSettings(ReadSettingsRequest readSettingsRequest) {
            RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
            if (remoteDevice != null) {
                ScreenConfiguration screenConfiguration = getScreenConfiguration();
                Rect margins = screenConfiguration.getMargins();
                remoteDevice.postEvent(new ReadSettingsResponse(new com.navdy.service.library.events.settings.ScreenConfiguration(Integer.valueOf(margins.left), Integer.valueOf(margins.top), Integer.valueOf(margins.right), Integer.valueOf(margins.bottom), Float.valueOf(screenConfiguration.getScaleX()), Float.valueOf(screenConfiguration.getScaleY())), this.hudSettings.readSettings(readSettingsRequest.settings)));
            }
        }

        public void onSave(Bundle bundle) {
            bundle.putParcelable(MAIN_SETTINGS, this.settings);
            persistSettings();
            if (this.services != null) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        Presenter.this.services.gestureService.stop();
                    }
                }, 1);
            }
            super.onSave(bundle);
        }

        @Subscribe
        public void onShowScreen(ShowScreenWithArgs showScreenWithArgs) {
            if (this.uiStateManager.getRootScreen() == null) {
                Main.sLogger.w("Main screen not up, cannot show screen-args:" + showScreenWithArgs);
            } else {
                updateScreen(showScreenWithArgs.screen, showScreenWithArgs.args, showScreenWithArgs.args2, showScreenWithArgs.ignoreAnimation, false);
            }
        }

        @Subscribe
        public void onShowScreen(ShowScreen showScreen) {
            if (this.uiStateManager.getRootScreen() == null) {
                Main.sLogger.w("Main screen not up, cannot show screen:" + showScreen);
            } else if (showScreen.arguments == null || showScreen.arguments.size() == 0) {
                updateScreen(showScreen.screen, null, null, false, false);
            } else {
                List<KeyValue> arguments = showScreen.arguments;
                Bundle bundle = new Bundle();
                for (KeyValue keyValue : arguments) {
                    if (!(keyValue.key == null || keyValue.value == null)) {
                        bundle.putString(keyValue.key, keyValue.value);
                    }
                }
                updateScreen(showScreen.screen, bundle, null, false, false);
            }
        }

        @Subscribe
        public void onShutdownEvent(Shutdown shutdown) {
            switch (shutdown.state) {
                case CONFIRMATION:
                    this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_SHUTDOWN_CONFIRMATION, shutdown.reason.asBundle(), false));
                    return;
                case SHUTTING_DOWN:
                    Context appContext = HudApplication.getAppContext();
                    Main.sLogger.i("Stopping update service");
                    appContext.stopService(new Intent(appContext, OTAUpdateService.class));
                    return;
                default:
                    return;
            }
        }

        @Subscribe
        public void onUpdateSettings(UpdateSettings updateSettings) {
            com.navdy.service.library.events.settings.ScreenConfiguration screenConfiguration = updateSettings.screenConfiguration;
            if (screenConfiguration != null) {
                ScreenConfiguration screenConfiguration2 = getScreenConfiguration();
                screenConfiguration2.setMargins(new Rect(screenConfiguration.marginLeft.intValue(), screenConfiguration.marginTop.intValue(), screenConfiguration.marginRight.intValue(), screenConfiguration.marginBottom.intValue()));
                screenConfiguration2.setScaleX(screenConfiguration.scaleX.floatValue());
                screenConfiguration2.setScaleY(screenConfiguration.scaleY.floatValue());
            }
            this.hudSettings.updateSettings(updateSettings.settings);
            persistSettings();
            updateView();
        }

        @Subscribe
        public void onWakeUp(Wakeup wakeup) {
            startLateServices();
        }

        private void showInstallingUpdateToast() {
            ToastManager toastManager = ToastManager.getInstance();
            toastManager.clearAllPendingToast();
            toastManager.disableToasts(false);
            Resources resources = HudApplication.getAppContext().getResources();
            Bundle bundle = new Bundle();
            toastManager.dismissCurrentToast(Main.INSTALLING_OTA_TOAST_ID);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE, resources.getString(R.string.warning));
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_warning);
            bundle.putInt(ToastPresenter.EXTRA_SIDE_IMAGE, R.drawable.icon_sm_spinner);
            int powerWarning = ObdManager.getInstance().getConnectionType() == ConnectionType.POWER_ONLY ? R.string.do_not_remove_power_or_turn_off_vehicle : R.string.do_not_remove_power;
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_1, resources.getString(R.string.preparing_to_install_update));
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, R.style.installing_update_title_1);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(powerWarning));
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.installing_update_title_2);
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, (int) resources.getDimension(R.dimen.toast_installing_update_width));
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_LEFT_PADDING, (int) resources.getDimension(R.dimen.toast_installing_update_padding));
            toastManager.addToast(new ToastParams(Main.INSTALLING_OTA_TOAST_ID, bundle, new IToastCallback() {
                ObjectAnimator animator = this.animator;

                public void onStart(ToastView view) {
                    ConfirmationLayout lyt = view.getView();
                    lyt.title1.setMaxLines(2);
                    lyt.title1.setSingleLine(false);
                    lyt.title2.setMaxLines(2);
                    lyt.title2.setSingleLine(false);
                    if (this.animator == null) {
                        this.animator = ObjectAnimator.ofFloat(lyt.sideImage, View.ROTATION, new float[]{360.0f});
                        this.animator.setDuration(500);
                        this.animator.setInterpolator(new AccelerateDecelerateInterpolator());
                        this.animator.addListener(new DefaultAnimationListener() {
                            public void onAnimationEnd(Animator animation) {
                                if (AnonymousClass8.this.animator != null) {
                                    AnonymousClass8.this.animator.setStartDelay(33);
                                    AnonymousClass8.this.animator.start();
                                }
                            }
                        });
                    }
                    if (!this.animator.isRunning()) {
                        this.animator.start();
                    }
                }

                public void onStop() {
                    if (this.animator != null) {
                        this.animator.removeAllListeners();
                        this.animator.cancel();
                    }
                }

                public boolean onKey(CustomKeyEvent event) {
                    return false;
                }

                public void executeChoiceItem(int pos, int id) {
                }
            }, true, true));
        }

        private void showNotification() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                INotification currentNotification = this.notificationManager.getCurrentNotification();
                if (currentNotification == null) {
                    Main.sLogger.v("no current notification to be displayed");
                    runQueuedOperation();
                    return;
                }
                Main.sLogger.v("NotificationManager:calling shownotification");
                HomeScreenView homescreenView = this.uiStateManager.getHomescreenView();
                if (homescreenView.hasModeView() && homescreenView.isModeVisible()) {
                    Main.sLogger.v("mode-view: reset mode view show notif");
                    homescreenView.resetModeView();
                }
                mainView.showNotification(currentNotification.getId(), currentNotification.getType());
            }
        }

        public void playMedia(Context context, Uri uri) {
            Main.sLogger.d("playMedia:" + uri);
            try {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(uri, Main.MIME_TYPE_VIDEO);
                intent.addFlags(268435456);
                context.startActivity(intent);
            } catch (Exception ex) {
                Main.sLogger.e("Unable to start video loop", ex);
            }
        }

        public void removeNotificationExtensionView() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                mainView.removeNotificationExtensionView();
            }
        }

        public void setInputFocus() {
            if (this.inputManager.getFocus() instanceof ToastView) {
                Main.sLogger.v("[focus] not setting focus[toast]");
                return;
            }
            MainView mainView = (MainView) getView();
            if (mainView == null) {
                Main.sLogger.v("[focus] mainview is null");
            } else if (isNotificationViewShowing()) {
                this.inputManager.setFocus(mainView.getNotificationView());
                Main.sLogger.v("[focus] notification");
            } else {
                ContainerView containerView = mainView.getContainerView();
                if (containerView == null) {
                    this.inputManager.setFocus(mainView);
                    Main.sLogger.v("[focus] mainView");
                    return;
                }
                View currentView = containerView.getCurrentView();
                if (currentView instanceof IInputHandler) {
                    this.inputManager.setFocus((IInputHandler) currentView);
                    Main.sLogger.v("[focus] screen:" + ((IInputHandler) currentView).getClass().getSimpleName());
                    return;
                }
                this.inputManager.setFocus(mainView);
                Main.sLogger.v("[focus] mainView");
            }
        }

        public void setMargins(int n, int n2, int n3, int n4) {
            getScreenConfiguration().setMargins(new Rect(0, n2, 0, 0));
            persistSettings();
            updateView();
        }

        void setNotificationColorVisibility(int n) {
            if (this.notificationColorEnabled) {
                MainView mainView = (MainView) getView();
                if (mainView != null) {
                    String s;
                    if (n != 0) {
                        mainView.setNotificationColor(this.defaultNotifColor);
                        mainView.setNotificationColorVisibility(n);
                    } else if (this.notificationManager.getCurrentNotification() == null || !(isNotificationExpanding() || isNotificationViewShowing())) {
                        int notificationColor = this.notificationManager.getNotificationColor();
                        if (notificationColor == -1) {
                            notificationColor = this.defaultNotifColor;
                        }
                        mainView.setNotificationColor(notificationColor);
                        mainView.setNotificationColorVisibility(n);
                    } else {
                        Main.sLogger.v("not showing notifcolor, pending notif");
                        return;
                    }
                    Logger access$200 = Main.sLogger;
                    StringBuilder append = new StringBuilder().append("notifColor: ");
                    if (n == 0) {
                        s = "visible";
                    } else {
                        s = "invisible";
                    }
                    access$200.v(append.append(s).toString());
                    return;
                }
                return;
            }
            Main.sLogger.v("notifColor is disabled");
        }

        public void setSystemTrayVisibility(int visibility) {
            if (this.systemTrayEnabled) {
                MainView mainView = (MainView) getView();
                if (mainView == null) {
                    return;
                }
                if (visibility == 0 && this.notificationManager.getCurrentNotification() != null && (isNotificationExpanding() || isNotificationViewShowing())) {
                    Main.sLogger.v("not showing systemtray, pending notif");
                    return;
                }
                String s;
                mainView.getSystemTray().setVisibility(visibility);
                Logger access$200 = Main.sLogger;
                StringBuilder append = new StringBuilder().append("systemtray is ");
                if (visibility == 0) {
                    s = "visible";
                } else {
                    s = "invisible";
                }
                access$200.v(append.append(s).toString());
                return;
            }
            Main.sLogger.v("systemtray is disabled");
        }

        public void startVoiceAssistance(boolean b) {
            Main.sLogger.v("startVoiceAssistance , Voice search : " + b);
            if (this.notificationManager.isNotificationViewShowing()) {
                INotification notification;
                Main.sLogger.v("startVoiceAssistance:add pending");
                NotificationManager notificationManager = this.notificationManager;
                if (b) {
                    notification = this.voiceSearchNotification;
                } else {
                    notification = this.voiceAssistNotification;
                }
                notificationManager.addPendingNotification(notification);
                if (this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible()) {
                    this.notificationManager.collapseExpandedNotification(true, true);
                    return;
                } else {
                    this.notificationManager.collapseNotification();
                    return;
                }
            }
            launchVoiceAssistance(b);
        }

        public void updateView() {
            MainView mainView = (MainView) getView();
            if (mainView != null) {
                mainView.setVerticalOffset(getScreenConfiguration().getMargins().top);
                setNotificationColorVisibility(0);
                setSystemTrayVisibility(0);
                SystemTrayView systemTray = getSystemTray();
                if (systemTray != null) {
                    State state;
                    State state2;
                    Device dial = Device.Dial;
                    if (DialManager.getInstance().isDialConnected()) {
                        state = State.CONNECTED;
                    } else {
                        state = State.DISCONNECTED;
                    }
                    systemTray.setState(dial, state);
                    Device phone = Device.Phone;
                    if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                        state2 = State.CONNECTED;
                    } else {
                        state2 = State.DISCONNECTED;
                    }
                    systemTray.setState(phone, state2);
                    HereMapsManager instance = HereMapsManager.getInstance();
                    if (instance.isInitialized()) {
                        HereLocationFixManager locationFixManager = instance.getLocationFixManager();
                        if (locationFixManager != null && !locationFixManager.hasLocationFix()) {
                            systemTray.setState(Device.Gps, State.LOCATION_LOST);
                        }
                    }
                }
            }
        }
    }

    public enum ProtocolStatus {
        PROTOCOL_LOCAL_NEEDS_UPDATE,
        PROTOCOL_REMOTE_NEEDS_UPDATE,
        PROTOCOL_VALID
    }

    private enum ScreenCategory {
        NOTIFICATION,
        SCREEN
    }

    private static class ScreenInfo {
        Bundle args;
        Object args2;
        boolean dismissScreen;
        Screen screen;

        ScreenInfo(Screen screen, Bundle args, Object args2, boolean dismissScreen) {
            this.screen = screen;
            this.args = args;
            this.args2 = args2;
            this.dismissScreen = dismissScreen;
        }
    }

    static {
        SparseArray sparseArray = new SparseArray();
        SCREEN_MAP = sparseArray;
        sparseArray.put(Screen.SCREEN_FIRST_LAUNCH.ordinal(), FirstLaunchScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_HOME.ordinal(), HomeScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_WELCOME.ordinal(), WelcomeScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_MAIN_MENU.ordinal(), MainMenuScreen2.class);
        SCREEN_MAP.put(Screen.SCREEN_OTA_CONFIRMATION.ordinal(), OSUpdateConfirmationScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_SHUTDOWN_CONFIRMATION.ordinal(), ShutDownScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_AUTO_BRIGHTNESS.ordinal(), AutoBrightnessScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_FACTORY_RESET.ordinal(), FactoryResetScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_DIAL_PAIRING.ordinal(), DialManagerScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_DIAL_UPDATE_PROGRESS.ordinal(), DialUpdateProgressScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_TEMPERATURE_WARNING.ordinal(), TemperatureWarningScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_FORCE_UPDATE.ordinal(), ForceUpdateScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_DIAL_UPDATE_CONFIRMATION.ordinal(), DialUpdateConfirmationScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_GESTURE_LEARNING.ordinal(), GestureLearningScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_DESTINATION_PICKER.ordinal(), DestinationPickerScreen.class);
        SCREEN_MAP.put(Screen.SCREEN_MUSIC_DETAILS.ordinal(), MusicDetailsScreen.class);
    }

    public Main() {
        mainScreen = this;
    }

    public static void handleLibraryInitializationError(String s) {
        sLogger.e("Forcing update due to initialization failure: " + s);
        mProtocolStatus = ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE;
        if (presenter != null) {
            presenter.bus.post(new Builder().screen(Screen.SCREEN_FORCE_UPDATE).build());
        } else {
            sLogger.e("unable to launch FORCE_UPDATE screen.");
        }
    }

    public static void saveHomeScreenPreference(SharedPreferences sharedPreferences, int n) {
        boolean defaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        SharedPreferences localPreferencesForCurrentDriverProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
        if (!(defaultProfile || localPreferencesForCurrentDriverProfile == null)) {
            localPreferencesForCurrentDriverProfile.edit().putInt(PREFERENCE_HOME_SCREEN_MODE, n).apply();
        }
        if (sharedPreferences != null) {
            sharedPreferences.edit().putInt(PREFERENCE_HOME_SCREEN_MODE, n).apply();
        }
    }

    private static void showBrightnessNotification() {
        if (HUDSettings.USE_ADAPTIVE_AUTOBRIGHTNESS) {
            sLogger.v("Use adaptive autobrightness");
            AdaptiveBrightnessNotification.showNotification();
            return;
        }
        sLogger.v("Use non-adaptive autobrightness");
        BrightnessNotification.showNotification();
    }

    public void addNotificationExtensionView(View view) {
        if (presenter != null) {
            presenter.addNotificationExtensionView(view);
        }
    }

    public void clearInputFocus() {
        if (presenter != null) {
            presenter.clearInputFocus();
        }
    }

    public void ejectMainLowerView() {
        if (presenter != null) {
            presenter.ejectMainLowerView();
        }
    }

    public void enableNotificationColor(boolean b) {
        if (presenter != null) {
            presenter.enableNotificationColor(b);
        }
    }

    public void enableSystemTray(boolean b) {
        if (presenter != null) {
            presenter.enableSystemTray(b);
        }
    }

    public int getAnimationIn(Direction direction) {
        return -1;
    }

    public int getAnimationOut(Direction direction) {
        return -1;
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public View getExpandedNotificationCoverView() {
        if (presenter == null) {
            return null;
        }
        return presenter.getExpandedNotificationCoverView();
    }

    public FrameLayout getExpandedNotificationView() {
        if (presenter == null) {
            return null;
        }
        return presenter.getExpandedNotificationView();
    }

    public IInputHandler getInputHandler() {
        return presenter;
    }

    public String getMortarScopeName() {
        return getClass().getName();
    }

    public ViewGroup getNotificationExtensionViewHolder() {
        if (presenter != null) {
            return presenter.getNotificationExtensionView();
        }
        return null;
    }

    public CarouselIndicator getNotificationIndicator() {
        if (presenter == null) {
            return null;
        }
        return presenter.getNotificationIndicator();
    }

    public ProgressIndicator getNotificationScrollIndicator() {
        if (presenter == null) {
            return null;
        }
        return presenter.getNotificationScrollIndicator();
    }

    public NotificationView getNotificationView() {
        if (presenter == null) {
            return null;
        }
        return presenter.getNotificationView();
    }

    public Screen getScreen() {
        return null;
    }

    public void handleKey(CustomKeyEvent customKeyEvent) {
        if (presenter != null) {
            presenter.onKey(customKeyEvent);
        }
    }

    public void injectMainLowerView(View view) {
        if (presenter != null) {
            presenter.injectMainLowerView(view);
        }
    }

    public boolean isMainLowerViewVisible() {
        return presenter != null && presenter.isMainLowerViewVisible();
    }

    public boolean isMainUIShrunk() {
        return presenter != null && presenter.isMainUIShrunk();
    }

    public boolean isNotificationCollapsing() {
        return presenter != null && presenter.isNotificationCollapsing();
    }

    public boolean isNotificationExpanding() {
        return presenter != null && presenter.isNotificationExpanding();
    }

    public boolean isNotificationViewShowing() {
        return presenter != null && presenter.isNotificationViewShowing();
    }

    public boolean isScreenAnimating() {
        return presenter != null && presenter.isScreenAnimating();
    }

    public void removeNotificationExtensionView() {
        if (presenter != null) {
            presenter.removeNotificationExtensionView();
        }
    }

    public void setInputFocus() {
        if (presenter != null) {
            presenter.setInputFocus();
        }
    }

    public void setSystemTrayVisibility(int systemTrayVisibility) {
        if (presenter != null) {
            presenter.setSystemTrayVisibility(systemTrayVisibility);
        }
    }
}
