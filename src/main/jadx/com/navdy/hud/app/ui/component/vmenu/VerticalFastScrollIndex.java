package com.navdy.hud.app.ui.component.vmenu;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class VerticalFastScrollIndex {
    private final String[] entries;
    public final int length;
    private int[] offsetIndex;
    private final Map<String, Integer> offsetMap;
    private final int positionOffset;

    public static class Builder {
        private LinkedHashMap<String, Integer> map = new LinkedHashMap();
        private int positionOffset;

        public Builder setEntry(char ch, int startIndex) {
            this.map.put(String.valueOf(ch), Integer.valueOf(startIndex));
            return this;
        }

        public Builder positionOffset(int n) {
            this.positionOffset = n;
            return this;
        }

        public VerticalFastScrollIndex build() {
            int len = this.map.size();
            String[] entries = new String[len];
            int[] offsetIndex = new int[len];
            int counter = 0;
            for (String key : this.map.keySet()) {
                entries[counter] = key;
                offsetIndex[counter] = ((Integer) this.map.get(key)).intValue();
                counter++;
            }
            return new VerticalFastScrollIndex(entries, this.map, offsetIndex, this.positionOffset);
        }
    }

    private VerticalFastScrollIndex(String[] entries, Map<String, Integer> offsetMap, int[] offsetIndex, int positionOffset) {
        this.entries = entries;
        this.offsetMap = offsetMap;
        this.offsetIndex = offsetIndex;
        this.positionOffset = positionOffset;
        this.length = entries.length;
    }

    public String getTitle(int index) {
        return this.entries[index];
    }

    public int getIndexForPosition(int position) {
        int index = Arrays.binarySearch(this.offsetIndex, 0, this.offsetIndex.length, position);
        if (index >= 0) {
            return index;
        }
        index = (-index) - 1;
        if (index > 0) {
            return index - 1;
        }
        return index;
    }

    public int getPosition(String title) {
        return ((Integer) this.offsetMap.get(title)).intValue();
    }

    public int getEntryCount() {
        return this.entries.length;
    }

    public int getOffset() {
        return this.positionOffset;
    }
}
