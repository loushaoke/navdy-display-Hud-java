package com.navdy.hud.app.ui.component.homescreen;

import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class TbtView$$ViewInjector {
    public static void inject(Finder finder, TbtView target, Object source) {
        target.activeMapDistance = (TextView) finder.findRequiredView(source, R.id.activeMapDistance, "field 'activeMapDistance'");
        target.activeMapIcon = (ImageView) finder.findRequiredView(source, R.id.activeMapIcon, "field 'activeMapIcon'");
        target.nextMapIconContainer = finder.findRequiredView(source, R.id.nextMapIconContainer, "field 'nextMapIconContainer'");
        target.nextMapIcon = (ImageView) finder.findRequiredView(source, R.id.nextMapIcon, "field 'nextMapIcon'");
        target.activeMapInstruction = (TextView) finder.findRequiredView(source, R.id.activeMapInstruction, "field 'activeMapInstruction'");
        target.nextMapInstructionContainer = finder.findRequiredView(source, R.id.nextMapInstructionContainer, "field 'nextMapInstructionContainer'");
        target.nextMapInstruction = (TextView) finder.findRequiredView(source, R.id.nextMapInstruction, "field 'nextMapInstruction'");
        target.progressBar = (ProgressBar) finder.findRequiredView(source, R.id.progress_bar, "field 'progressBar'");
        target.nowIcon = finder.findRequiredView(source, R.id.now_icon, "field 'nowIcon'");
    }

    public static void reset(TbtView target) {
        target.activeMapDistance = null;
        target.activeMapIcon = null;
        target.nextMapIconContainer = null;
        target.nextMapIcon = null;
        target.activeMapInstruction = null;
        target.nextMapInstructionContainer = null;
        target.nextMapInstruction = null;
        target.progressBar = null;
        target.nowIcon = null;
    }
}
