package com.navdy.hud.app.ui.component.vmenu;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.HaloView;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView;
import com.navdy.hud.app.view.ToolTipView;

public class VerticalMenuComponent$$ViewInjector {
    public static void inject(Finder finder, VerticalMenuComponent target, Object source) {
        target.leftContainer = (ViewGroup) finder.findRequiredView(source, R.id.leftContainer, "field 'leftContainer'");
        target.selectedImage = (ViewGroup) finder.findRequiredView(source, R.id.selectedImage, "field 'selectedImage'");
        target.selectedIconColorImage = (IconColorImageView) finder.findRequiredView(source, R.id.selectedIconColorImage, "field 'selectedIconColorImage'");
        target.selectedIconImage = (InitialsImageView) finder.findRequiredView(source, R.id.selectedIconImage, "field 'selectedIconImage'");
        target.selectedText = (TextView) finder.findRequiredView(source, R.id.selectedText, "field 'selectedText'");
        target.rightContainer = (ViewGroup) finder.findRequiredView(source, R.id.rightContainer, "field 'rightContainer'");
        target.indicator = (CarouselIndicator) finder.findRequiredView(source, R.id.indicator, "field 'indicator'");
        target.recyclerView = (VerticalRecyclerView) finder.findRequiredView(source, R.id.recyclerView, "field 'recyclerView'");
        target.toolTip = (ToolTipView) finder.findRequiredView(source, R.id.tooltip, "field 'toolTip'");
        target.closeContainerScrim = finder.findRequiredView(source, R.id.closeContainerScrim, "field 'closeContainerScrim'");
        target.closeIconContainer = (ViewGroup) finder.findRequiredView(source, R.id.closeIconContainer, "field 'closeIconContainer'");
        target.closeContainer = (ViewGroup) finder.findRequiredView(source, R.id.closeContainer, "field 'closeContainer'");
        target.closeHalo = (HaloView) finder.findRequiredView(source, R.id.closeHalo, "field 'closeHalo'");
        target.fastScrollContainer = (ViewGroup) finder.findRequiredView(source, R.id.fastScrollView, "field 'fastScrollContainer'");
        target.fastScrollText = (TextView) finder.findRequiredView(source, R.id.fastScrollText, "field 'fastScrollText'");
        target.selectedCustomView = (FrameLayout) finder.findRequiredView(source, R.id.selectedCustomView, "field 'selectedCustomView'");
    }

    public static void reset(VerticalMenuComponent target) {
        target.leftContainer = null;
        target.selectedImage = null;
        target.selectedIconColorImage = null;
        target.selectedIconImage = null;
        target.selectedText = null;
        target.rightContainer = null;
        target.indicator = null;
        target.recyclerView = null;
        target.toolTip = null;
        target.closeContainerScrim = null;
        target.closeIconContainer = null;
        target.closeContainer = null;
        target.closeHalo = null;
        target.fastScrollContainer = null;
        target.fastScrollText = null;
        target.selectedCustomView = null;
    }
}
