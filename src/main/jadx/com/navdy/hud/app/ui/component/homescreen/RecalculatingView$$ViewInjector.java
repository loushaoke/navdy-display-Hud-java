package com.navdy.hud.app.ui.component.homescreen;

import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.navdy.hud.app.ui.component.image.ColorImageView;

public class RecalculatingView$$ViewInjector {
    public static void inject(Finder finder, RecalculatingView target, Object source) {
        target.recalcImageView = (ColorImageView) finder.findRequiredView(source, R.id.recalcImageView, "field 'recalcImageView'");
        target.recalcAnimator = (FluctuatorAnimatorView) finder.findRequiredView(source, R.id.recalcAnimator, "field 'recalcAnimator'");
    }

    public static void reset(RecalculatingView target) {
        target.recalcImageView = null;
        target.recalcAnimator = null;
    }
}
