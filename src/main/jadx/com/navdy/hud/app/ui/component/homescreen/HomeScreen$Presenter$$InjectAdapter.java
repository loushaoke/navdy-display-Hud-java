package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.ui.component.homescreen.HomeScreen.Presenter;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class HomeScreen$Presenter$$InjectAdapter extends Binding<Presenter> implements Provider<Presenter>, MembersInjector<Presenter> {
    private Binding<Bus> bus;
    private Binding<BasePresenter> supertype;

    public HomeScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter", "members/com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter", true, Presenter.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", Presenter.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.supertype);
    }

    public Presenter get() {
        Presenter result = new Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(Presenter object) {
        object.bus = (Bus) this.bus.get();
        this.supertype.injectMembers(object);
    }
}
