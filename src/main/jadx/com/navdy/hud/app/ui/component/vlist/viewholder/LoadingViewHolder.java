package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.service.library.log.Logger;

public class LoadingViewHolder extends VerticalViewHolder {
    private static final int LOADING_ANIMATION_DELAY = 33;
    private static final int LOADING_ANIMATION_DURATION = 500;
    private static final int loadingImageSize;
    private static final int loadingImageSizeUnselected;
    private static final Logger sLogger = VerticalViewHolder.sLogger;
    private ObjectAnimator loadingAnimator;
    private ImageView loadingImage;

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        loadingImageSize = resources.getDimensionPixelSize(R.dimen.vlist_loading_image);
        loadingImageSizeUnselected = resources.getDimensionPixelSize(R.dimen.vlist_loading_image_unselected);
    }

    public static LoadingViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        return new LoadingViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.vlist_loading_item, parent, false), vlist, handler);
    }

    public static Model buildModel() {
        Model model = new Model();
        model.type = ModelType.LOADING;
        model.id = R.id.vlist_loading;
        return model;
    }

    public LoadingViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
        this.loadingImage = (ImageView) layout.findViewById(R.id.loading);
    }

    public ModelType getModelType() {
        return ModelType.LOADING;
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
        int size;
        if (state == State.SELECTED) {
            size = loadingImageSize;
        } else {
            size = loadingImageSizeUnselected;
        }
        switch (animation) {
            case NONE:
            case INIT:
                startLoadingAnimation();
                MarginLayoutParams lytParams = (MarginLayoutParams) this.loadingImage.getLayoutParams();
                lytParams.width = size;
                lytParams.height = size;
                return;
            case MOVE:
                startLoadingAnimation();
                Animator animator = VerticalAnimationUtils.animateDimension(this.loadingImage, size);
                animator.setDuration((long) duration);
                animator.setInterpolator(this.interpolator);
                animator.start();
                return;
            default:
                return;
        }
    }

    public void bind(Model model, ModelState modelState) {
    }

    public void clearAnimation() {
        stopLoadingAnimation();
    }

    public void select(Model model, int pos, int duration) {
    }

    public void copyAndPosition(ImageView imageC, TextView titleC, TextView subTitleC, TextView subTitle2C, boolean setImage) {
    }

    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            this.loadingAnimator = ObjectAnimator.ofFloat(this.loadingImage, View.ROTATION, new float[]{360.0f});
            this.loadingAnimator.setDuration(500);
            this.loadingAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener(new DefaultAnimationListener() {
                public void onAnimationEnd(Animator animation) {
                    if (LoadingViewHolder.this.loadingAnimator != null) {
                        LoadingViewHolder.this.loadingAnimator.setStartDelay(33);
                        LoadingViewHolder.this.loadingAnimator.start();
                        return;
                    }
                    LoadingViewHolder.sLogger.v("abandon loading animation");
                }
            });
        }
        sLogger.v("started loading animation");
        this.loadingAnimator.start();
    }

    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            sLogger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.loadingImage.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
}
