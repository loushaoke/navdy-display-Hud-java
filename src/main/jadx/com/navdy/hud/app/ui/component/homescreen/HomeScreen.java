package com.navdy.hud.app.ui.component.homescreen;

import android.os.Bundle;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import flow.Flow.Direction;
import flow.Layout;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_home)
public class HomeScreen extends BaseScreen {
    private static Presenter presenter;

    public enum DisplayMode {
        MAP,
        SMART_DASH
    }

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {HomeScreenView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<HomeScreenView> {
        private static final Logger logger = new Logger(Presenter.class);
        @Inject
        Bus bus;

        public void onLoad(Bundle savedInstanceState) {
            HomeScreen.presenter = this;
        }

        protected void onUnload() {
            HomeScreen.presenter = null;
        }

        HomeScreenView getHomeScreenView() {
            return (HomeScreenView) getView();
        }

        void setDisplayMode(DisplayMode mode) {
            HomeScreenView view = getHomeScreenView();
            if (view != null) {
                view.setDisplayMode(mode);
            }
        }

        DisplayMode getDisplayMode() {
            HomeScreenView view = getHomeScreenView();
            if (view != null) {
                return view.getDisplayMode();
            }
            return null;
        }
    }

    public String getMortarScopeName() {
        return getClass().getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_HOME;
    }

    public int getAnimationIn(Direction direction) {
        return 17432576;
    }

    public int getAnimationOut(Direction direction) {
        return 17432577;
    }

    public DisplayMode getDisplayMode() {
        if (presenter != null) {
            return presenter.getDisplayMode();
        }
        return null;
    }

    public void setDisplayMode(DisplayMode mode) {
        if (presenter != null) {
            presenter.setDisplayMode(mode);
        }
    }

    public HomeScreenView getHomeScreenView() {
        if (presenter != null) {
            return presenter.getHomeScreenView();
        }
        return null;
    }
}
