package com.navdy.hud.app.ui.component.mainmenu;

import android.bluetooth.BluetoothAdapter;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.BuildConfig;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsUtils.GpsSatelliteData;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.event.DriverProfileUpdated.State;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged;
import com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents.LocationFix;
import com.navdy.hud.app.maps.here.HereLocationFixManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereOfflineMapsVersion;
import com.navdy.hud.app.maps.util.DistanceConverter;
import com.navdy.hud.app.maps.util.DistanceConverter.Distance;
import com.navdy.hud.app.obd.ObdDeviceConfigurationManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ConnectionType;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent;
import com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.ui.component.UISettings;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.systeminfo.GpsSignalView;
import com.navdy.hud.app.ui.component.systeminfo.GpsSignalView.SatelliteData;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.obd.PidSet;
import com.navdy.obd.Pids;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.text.Typography;

public class SystemInfoMenu implements IMenu {
    private static final int UPDATE_FREQUENCY = 2000;
    private static final Model back;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    private static String fuelUnitText = resources.getString(R.string.litre);
    private static final Model infoModel = ScrollableViewHolder.buildModel(R.layout.system_info_lyt);
    private static final String infoTitle = resources.getString(R.string.carousel_menu_system_info_title);
    private static final String noData = resources.getString(R.string.si_no_data);
    private static final String noFixStr = resources.getString(R.string.gps_no_fix);
    private static final String off = resources.getString(R.string.si_off);
    private static final String on = resources.getString(R.string.si_on);
    private static String pressureUnitText = resources.getString(R.string.pressure_unit);
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(SystemInfoMenu.class);
    @InjectView(R.id.si_auto_on)
    protected TextView autoOn;
    private int backSelection;
    private Bus bus;
    private List<Model> cachedList;
    @InjectView(R.id.si_model)
    protected TextView carModel;
    @InjectView(R.id.si_check_engine_light)
    protected TextView checkEngineLight;
    @InjectView(R.id.si_device_mileage)
    protected TextView deviceMileage;
    @InjectView(R.id.si_dial_bt)
    protected TextView dialBt;
    @InjectView(R.id.si_dial_fw)
    protected TextView dialFw;
    @InjectView(R.id.si_display_bt)
    protected TextView displayBt;
    @InjectView(R.id.si_display_fw)
    protected TextView displayFw;
    @InjectView(R.id.si_eng_oil_pressure)
    protected TextView engineOilPressure;
    @InjectView(R.id.si_engine_oil_pressure_lyt)
    protected ViewGroup engineOilPressureLayout;
    @InjectView(R.id.si_eng_temp)
    protected TextView engineTemp;
    @InjectView(R.id.si_engine_trouble_codes)
    protected TextView engineTroubleCodes;
    @InjectView(R.id.si_fix)
    protected TextView fix;
    @InjectView(R.id.si_fuel)
    protected TextView fuel;
    @InjectView(R.id.si_gestures)
    protected TextView gestures;
    @InjectView(R.id.si_gps)
    protected TextView gps;
    @InjectView(R.id.si_gps_speed)
    protected TextView gpsSpeed;
    private Handler handler;
    private HereLocationFixManager hereLocationFixManager;
    @InjectView(R.id.si_maps_data_verified)
    protected TextView hereMapVerified;
    @InjectView(R.id.si_map_version)
    protected TextView hereMapsVersion;
    @InjectView(R.id.si_offline_maps)
    protected TextView hereOfflineMaps;
    @InjectView(R.id.si_here_sdk_version)
    protected TextView hereSdk;
    @InjectView(R.id.si_updated)
    protected TextView hereUpdated;
    @InjectView(R.id.si_maps_voices_verified)
    protected TextView hereVoiceVerified;
    @InjectView(R.id.si_maf)
    protected TextView maf;
    @InjectView(R.id.si_make)
    protected TextView make;
    @InjectView(R.id.si_obd_data)
    protected TextView obdData;
    @InjectView(R.id.obd_data_lyt)
    protected ViewGroup obdDataLayout;
    @InjectView(R.id.si_obd_fw)
    protected TextView obdFw;
    private ObdManager obdManager;
    @InjectView(R.id.si_odometer)
    protected TextView odo;
    @InjectView(R.id.si_odometer_lyt)
    protected ViewGroup odometerLayout;
    private IMenu parent;
    private Presenter presenter;
    protected boolean registered;
    @InjectView(R.id.si_route_calc)
    protected TextView routeCalc;
    @InjectView(R.id.si_route_pref)
    protected TextView routePref;
    @InjectView(R.id.si_rpm)
    protected TextView rpm;
    @InjectView(R.id.gps_signal)
    GpsSignalView satelliteView;
    @InjectView(R.id.si_satellites)
    protected TextView satellites;
    @InjectView(R.id.si_special_pids_lyt)
    protected ViewGroup specialPidsLayout;
    @InjectView(R.id.si_speed)
    protected TextView speed;
    private SpeedManager speedManager;
    @InjectView(R.id.si_temperature)
    protected TextView temperature;
    @InjectView(R.id.si_eng_trip_fuel)
    protected TextView tripFuel;
    @InjectView(R.id.si_trip_fuel_layout)
    protected ViewGroup tripFuelLayout;
    protected String ubloxFixType;
    @InjectView(R.id.si_ui_scaling)
    protected TextView uiScaling;
    private Runnable updateRunnable = new Runnable() {
        public void run() {
            if (SystemInfoMenu.this.isMenuLoaded()) {
                SystemInfoMenu.this.updateTemperature();
                SystemInfoMenu.this.updateVoltage();
                SystemInfoMenu.this.setFix();
                SystemInfoMenu.this.handler.removeCallbacks(this);
                SystemInfoMenu.this.handler.postDelayed(this, 2000);
            }
        }
    };
    @InjectView(R.id.si_voltage)
    protected TextView voltage;
    private VerticalMenuComponent vscrollComponent;
    @InjectView(R.id.si_year)
    protected TextView year;

    static {
        int backColor = resources.getColor(R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, backColor, MainMenu.bkColorUnselected, backColor, resources.getString(R.string.back), null);
    }

    SystemInfoMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.handler = new Handler(Looper.getMainLooper());
        this.obdManager = ObdManager.getInstance();
        this.speedManager = SpeedManager.getInstance();
    }

    public List<Model> getItems() {
        List<Model> list = new ArrayList();
        list.add(back);
        list.add(infoModel);
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        return 1;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconImage(R.drawable.icon_settings_navdy_data, null, Style.DEFAULT);
        this.vscrollComponent.selectedText.setText(infoTitle);
    }

    public boolean selectItem(ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.menu_back:
                sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
        }
        return true;
    }

    public Menu getType() {
        return Menu.SYSTEM_INFO;
    }

    public boolean isItemClickable(int id, int pos) {
        if (pos == 0) {
            return true;
        }
        return false;
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
        if (pos == 1 && ((ViewGroup) view).getChildCount() > 0) {
            ViewGroup viewGroup = (ViewGroup) ((ViewGroup) view).getChildAt(0);
            ButterKnife.inject( this, view);
            this.satelliteView.setVisibility(UISettings.advancedGpsStatsEnabled() ? 0 : 8);
            update();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                ObdManager.getInstance().enableInstantaneousMode(true);
                sLogger.v("registered, enabled instantaneous mode");
            }
            this.handler.removeCallbacks(this.updateRunnable);
            this.handler.postDelayed(this.updateRunnable, 2000);
        }
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
        if (this.registered) {
            if (RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView().getDisplayMode() != DisplayMode.SMART_DASH) {
                sLogger.v("onUnload: disable instantaneous mode");
                ObdManager.getInstance().enableInstantaneousMode(false);
            } else {
                sLogger.v("onUnload: instantaneous mode still enabled");
            }
            this.bus.unregister(this);
            this.registered = false;
            sLogger.v("unregistered");
        }
        this.displayFw = null;
        this.dialFw = null;
        this.obdFw = null;
        this.dialBt = null;
        this.displayBt = null;
        this.gps = null;
        this.satellites = null;
        this.fix = null;
        this.temperature = null;
        this.deviceMileage = null;
        this.hereSdk = null;
        this.hereUpdated = null;
        this.hereMapsVersion = null;
        this.hereOfflineMaps = null;
        this.hereMapVerified = null;
        this.hereVoiceVerified = null;
        this.make = null;
        this.carModel = null;
        this.year = null;
        this.fuel = null;
        this.rpm = null;
        this.speed = null;
        this.odo = null;
        this.maf = null;
        this.engineTemp = null;
        this.voltage = null;
        this.gestures = null;
        this.autoOn = null;
        this.obdData = null;
        this.uiScaling = null;
        this.routeCalc = null;
        this.routePref = null;
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    private void update() {
        updateVersions();
        updateStats();
        updateHere();
        updateVehicleInfo();
        updateGenericPref();
        updateNavPref();
    }

    private String removeBrackets(String str) {
        return str.replace(HereManeuverDisplayBuilder.OPEN_BRACKET, "").replace(HereManeuverDisplayBuilder.CLOSE_BRACKET, "");
    }

    private void updateVersions() {
        String str;
        String versionName = BuildConfig.VERSION_NAME;
        if (DeviceUtil.isUserBuild()) {
            versionName = OTAUpdateService.shortVersion(versionName);
        }
        this.displayFw.setText(versionName);
        DialManager dialManager = DialManager.getInstance();
        if (dialManager.isDialConnected()) {
            try {
                DialFirmwareUpdater dialFirmwareUpdater = dialManager.getDialFirmwareUpdater();
                str = null;
                if (dialFirmwareUpdater != null) {
                    int incremental = dialFirmwareUpdater.getVersions().dial.incrementalVersion;
                    if (incremental != -1) {
                        str = String.valueOf(incremental);
                    }
                    if (!TextUtils.isEmpty(str)) {
                        this.dialFw.setText(str);
                    }
                }
            } catch (Throwable t) {
                sLogger.e(t);
            }
            str = dialManager.getDialName();
            if (!TextUtils.isEmpty(str)) {
                this.dialBt.setText(removeBrackets(str));
            }
        }
        str = BluetoothAdapter.getDefaultAdapter().getName();
        if (!TextUtils.isEmpty(str)) {
            this.displayBt.setText(removeBrackets(str));
        }
        str = this.obdManager.getObdChipFirmwareVersion();
        if (!TextUtils.isEmpty(str)) {
            this.obdFw.setText(str);
        }
    }

    private void updateStats() {
        updateTemperature();
        updateDeviceMileage();
        updateGPSSpeed();
        HereMapsManager hereMapsManager = HereMapsManager.getInstance();
        if (hereMapsManager.isInitialized()) {
            this.hereLocationFixManager = hereMapsManager.getLocationFixManager();
        }
        setFix();
    }

    private void updateDeviceMileage() {
        if (this.presenter != null) {
            long d = this.presenter.tripManager.getTotalDistanceTravelledWithNavdy();
            sLogger.v("navdy distance travelled:" + d);
            if (d > 0) {
                Distance distance = new Distance();
                DistanceConverter.convertToDistance(SpeedManager.getInstance().getSpeedUnit(), (float) d, distance);
                this.deviceMileage.setText(HereManeuverDisplayBuilder.getFormattedDistance(distance.value, distance.unit, false));
            }
        }
    }

    private void updateTemperature() {
        int temp = AnalyticsSupport.getCurrentTemperature();
        if (temp != -1) {
            this.temperature.setText(String.valueOf(temp) + " " + Typography.degree + resources.getString(R.string.si_celsius));
        }
    }

    private void updateVehicleInfo() {
        DriverProfile profile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        String str = profile.getCarMake();
        if (!TextUtils.isEmpty(str)) {
            this.make.setText(str);
        }
        str = profile.getCarModel();
        if (!TextUtils.isEmpty(str)) {
            this.carModel.setText(str);
        }
        str = profile.getCarYear();
        if (!TextUtils.isEmpty(str)) {
            this.year.setText(str);
        }
        updateDynamicVehicleInfo();
    }

    private void updateHere() {
        HereMapsManager hereMapsManager = HereMapsManager.getInstance();
        if (!hereMapsManager.isInitializing()) {
            if (hereMapsManager.isInitialized()) {
                this.hereSdk.setText(hereMapsManager.getVersion());
                if (hereMapsManager.isVoiceSkinsLoaded()) {
                    this.hereVoiceVerified.setText(resources.getString(R.string.si_yes));
                } else {
                    this.hereVoiceVerified.setText(resources.getString(R.string.si_no));
                }
                if (hereMapsManager.isMapDataVerified()) {
                    sLogger.v("map package installed count:" + hereMapsManager.getMapPackageCount());
                    this.hereMapVerified.setText(resources.getString(R.string.si_yes));
                } else {
                    this.hereMapVerified.setText(resources.getString(R.string.si_no));
                }
                HereOfflineMapsVersion offlineMapsVersion = HereMapsManager.getInstance().getOfflineMapsVersion();
                if (offlineMapsVersion != null) {
                    String mapPackages = null;
                    List<String> offlinePackages = offlineMapsVersion.getPackages();
                    if (offlinePackages != null) {
                        int len = offlinePackages.size();
                        StringBuilder builder = new StringBuilder();
                        for (int i = 0; i < len; i++) {
                            if (i != 0) {
                                builder.append(HereManeuverDisplayBuilder.COMMA);
                                builder.append(" ");
                            }
                            builder.append((String) offlinePackages.get(i));
                        }
                        mapPackages = builder.toString();
                    }
                    String dateUpdated = offlineMapsVersion.getDate() != null ? dateFormat.format(offlineMapsVersion.getDate()) : null;
                    String mapVersion = offlineMapsVersion.getVersion();
                    if (!TextUtils.isEmpty(dateUpdated)) {
                        this.hereUpdated.setText(dateUpdated);
                    }
                    if (!TextUtils.isEmpty(mapPackages)) {
                        this.hereOfflineMaps.setText(mapPackages);
                    }
                    if (!TextUtils.isEmpty(mapVersion)) {
                        this.hereMapsVersion.setText(mapVersion);
                        return;
                    }
                    return;
                }
                return;
            }
            this.hereSdk.setText(hereMapsManager.getError().name());
        }
    }

    private void updateDynamicVehicleInfo() {
        int i;
        boolean obdConnected = this.obdManager.isConnected();
        ConnectionType connectionType = this.obdManager.getConnectionType();
        ViewGroup viewGroup = this.obdDataLayout;
        if (connectionType == ConnectionType.POWER_ONLY) {
            i = 8;
        } else {
            i = 0;
        }
        viewGroup.setVisibility(i);
        if (obdConnected) {
            boolean shouldShowSpecialPidsSection;
            updateFuel();
            updateRPM();
            updateSpeed();
            updateMAF();
            updateEngineTemperature();
            updateVoltage();
            updateOdometer();
            PidSet supportedPids = this.obdManager.getSupportedPids();
            if (supportedPids.contains((int) Pids.ENGINE_OIL_PRESSURE) || supportedPids.contains((int) Pids.ENGINE_TRIP_FUEL)) {
                shouldShowSpecialPidsSection = true;
            } else {
                shouldShowSpecialPidsSection = false;
            }
            if (shouldShowSpecialPidsSection) {
                this.specialPidsLayout.setVisibility(0);
                updateEngineFuelPressure();
                updateEngineTripFuel();
            } else {
                this.specialPidsLayout.setVisibility(8);
            }
            this.checkEngineLight.setText(this.obdManager.isCheckEngineLightOn() ? on : off);
            List<String> troubleCodes = this.obdManager.getTroubleCodes();
            if (troubleCodes == null || troubleCodes.size() <= 0) {
                this.engineTroubleCodes.setText(noData);
                return;
            }
            StringBuilder builder = new StringBuilder();
            for (String troubleCode : troubleCodes) {
                builder.append(troubleCode + HereManeuverDisplayBuilder.COMMA);
            }
            builder.deleteCharAt(builder.length() - 1);
            this.engineTroubleCodes.setText(builder.toString());
            return;
        }
        this.specialPidsLayout.setVisibility(8);
        this.odometerLayout.setVisibility(8);
        this.checkEngineLight.setText(noData);
        this.engineTroubleCodes.setText(noData);
    }

    private void updateEngineFuelPressure() {
        int val = (int) this.obdManager.getPidValue(Pids.ENGINE_OIL_PRESSURE);
        if (val > 0) {
            this.engineOilPressure.setText(val + " " + pressureUnitText);
        } else {
            this.engineOilPressure.setText(noData);
        }
    }

    private void updateEngineTripFuel() {
        int val = (int) this.obdManager.getPidValue(Pids.ENGINE_TRIP_FUEL);
        if (val > 0) {
            this.tripFuel.setText(val + " " + fuelUnitText);
        } else {
            this.tripFuel.setText(noData);
        }
    }

    private void updateFuel() {
        if (this.obdManager.getFuelLevel() != -1) {
            this.fuel.setText(resources.getString(R.string.si_fuel_level, new Object[]{Integer.valueOf(val)}));
            return;
        }
        this.fuel.setText(noData);
    }

    private void updateRPM() {
        int val = this.obdManager.getEngineRpm();
        if (val != -1) {
            this.rpm.setText(String.valueOf(val));
        } else {
            this.rpm.setText(noData);
        }
    }

    private void updateSpeed() {
        int val = this.speedManager.getCurrentSpeed();
        if (val != -1) {
            this.speed.setText(String.valueOf(val));
        } else {
            this.speed.setText(noData);
        }
    }

    private void updateGPSSpeed() {
        int val = this.speedManager.getGpsSpeed();
        if (val != -1) {
            this.gpsSpeed.setText(String.valueOf(val));
        } else {
            this.gpsSpeed.setText(noData);
        }
    }

    private void updateMAF() {
        int val = (int) this.obdManager.getInstantFuelConsumption();
        if (val > 0) {
            this.maf.setText(String.valueOf(val));
        } else {
            this.maf.setText(noData);
        }
    }

    private void updateVoltage() {
        if (this.obdManager.getBatteryVoltage() > 0.0d) {
            this.voltage.setText(String.format("%.1f", new Object[]{Double.valueOf(dbl)}) + resources.getString(R.string.si_volt));
            return;
        }
        this.voltage.setText(noData);
    }

    private void updateEngineTemperature() {
        DriverProfile profile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        double celsius = this.obdManager.getPidValue(5);
        if (celsius > 0.0d) {
            int unit;
            int rounded;
            switch (profile.getUnitSystem()) {
                case UNIT_SYSTEM_METRIC:
                    unit = R.string.si_celsius;
                    rounded = (int) Math.round(celsius);
                    break;
                default:
                    unit = R.string.si_fahrenheit;
                    rounded = (int) Math.round((1.8d * celsius) + 32.0d);
                    break;
            }
            this.engineTemp.setText(String.format("%d", new Object[]{Integer.valueOf(rounded)}) + " " + Typography.degree + resources.getString(unit));
            return;
        }
        this.engineTemp.setText(noData);
    }

    private void updateOdometer() {
        if (this.obdManager.getSupportedPids().contains((int) Pids.TOTAL_VEHICLE_DISTANCE)) {
            this.odometerLayout.setVisibility(0);
            int val = (int) this.obdManager.getPidValue(Pids.TOTAL_VEHICLE_DISTANCE);
            if (val > 0) {
                Distance distance = new Distance();
                DistanceConverter.convertToDistance(this.speedManager.getSpeedUnit(), (float) val, distance);
                this.odo.setText(HereManeuverDisplayBuilder.getFormattedDistance(distance.value, distance.unit, false));
                return;
            }
            this.odo.setText(noData);
            return;
        }
        this.odometerLayout.setVisibility(8);
    }

    private void updateGenericPref() {
        DriverProfile profile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        InputPreferences inputPref = profile.getInputPreferences();
        if (inputPref != null) {
            if (Boolean.TRUE.equals(inputPref.use_gestures)) {
                this.gestures.setText(resources.getString(R.string.si_enabled));
            } else {
                this.gestures.setText(resources.getString(R.string.si_disabled));
            }
        }
        ObdDeviceConfigurationManager configurationManager = this.obdManager.getObdDeviceConfigurationManager();
        if (configurationManager != null) {
            if (configurationManager.isAutoOnEnabled()) {
                this.autoOn.setText(resources.getString(R.string.si_enabled));
            } else {
                this.autoOn.setText(resources.getString(R.string.si_disabled));
            }
        }
        ObdScanSetting scanSetting = profile.getObdScanSetting();
        if (scanSetting != null) {
            switch (scanSetting) {
                case SCAN_ON:
                case SCAN_DEFAULT_ON:
                    this.obdData.setText(resources.getString(R.string.si_on));
                    break;
                default:
                    this.obdData.setText(resources.getString(R.string.si_off));
                    break;
            }
        }
        DisplayFormat displayFormat = profile.getDisplayFormat();
        if (displayFormat != null) {
            switch (displayFormat) {
                case DISPLAY_FORMAT_COMPACT:
                    this.uiScaling.setText(resources.getString(R.string.si_compact));
                    return;
                case DISPLAY_FORMAT_NORMAL:
                    this.uiScaling.setText(resources.getString(R.string.si_normal));
                    return;
                default:
                    return;
            }
        }
    }

    private void updateNavPref() {
        NavigationPreferences navPref = DriverProfileHelper.getInstance().getCurrentProfile().getNavigationPreferences();
        if (navPref != null) {
            switch (navPref.routingType) {
                case ROUTING_FASTEST:
                    this.routeCalc.setText(resources.getString(R.string.si_fastest));
                    break;
                case ROUTING_SHORTEST:
                    this.routeCalc.setText(resources.getString(R.string.si_shortest));
                    break;
            }
            StringBuilder stringBuilder = new StringBuilder();
            if (Boolean.TRUE.equals(navPref.allowHighways)) {
                stringBuilder.append(resources.getString(R.string.si_highways));
            }
            if (Boolean.TRUE.equals(navPref.allowTollRoads)) {
                formatRoutePref(stringBuilder, resources.getString(R.string.si_tollroads));
            }
            if (Boolean.TRUE.equals(navPref.allowFerries)) {
                formatRoutePref(stringBuilder, resources.getString(R.string.si_ferries));
            }
            if (Boolean.TRUE.equals(navPref.allowTunnels)) {
                formatRoutePref(stringBuilder, resources.getString(R.string.si_tunnels));
            }
            if (Boolean.TRUE.equals(navPref.allowUnpavedRoads)) {
                formatRoutePref(stringBuilder, resources.getString(R.string.si_unpaved_roads));
            }
            if (Boolean.TRUE.equals(navPref.allowAutoTrains)) {
                formatRoutePref(stringBuilder, resources.getString(R.string.si_auto_trains));
            }
            stringBuilder.append(GlanceConstants.NEWLINE);
            this.routePref.setText(stringBuilder.toString());
        }
    }

    private void formatRoutePref(StringBuilder stringBuilder, String str) {
        boolean uppercase = false;
        if (stringBuilder.length() > 0) {
            stringBuilder.append(HereManeuverDisplayBuilder.COMMA);
            stringBuilder.append(" ");
        } else {
            uppercase = true;
        }
        if (uppercase) {
            str = Character.toUpperCase(str.charAt(0)) + str.substring(1);
        }
        stringBuilder.append(str);
    }

    @Subscribe
    public void onGpsSatelliteData(GpsSatelliteData satelliteData) {
        try {
            if (isMenuLoaded()) {
                int seen = satelliteData.data.getInt(GpsConstants.GPS_SATELLITE_SEEN, 0);
                int used = satelliteData.data.getInt(GpsConstants.GPS_SATELLITE_USED, 0);
                if (seen > 0 || used > 0) {
                    this.satellites.setText(resources.getString(R.string.si_seen, new Object[]{Integer.valueOf(used), Integer.valueOf(seen)}));
                } else {
                    this.satellites.setText(noData);
                }
                this.ubloxFixType = getFixTypeDescription(satelliteData.data.getInt(GpsConstants.GPS_FIX_TYPE, 0));
                int max = satelliteData.data.getInt(GpsConstants.GPS_SATELLITE_MAX_DB, 0);
                if (max > 0) {
                    this.gps.setText(resources.getString(R.string.si_gps_signal, new Object[]{Integer.valueOf(max)}));
                } else {
                    this.gps.setText(noData);
                }
                if (UISettings.advancedGpsStatsEnabled()) {
                    SatelliteData[] data = new SatelliteData[seen];
                    for (int i = 0; i < seen; i++) {
                        data[i] = new SatelliteData(satelliteData.data.getInt(GpsConstants.GPS_SATELLITE_ID + (i + 1)), satelliteData.data.getInt(GpsConstants.GPS_SATELLITE_DB + (i + 1)), satelliteData.data.getString(GpsConstants.GPS_SATELLITE_PROVIDER + (i + 1)));
                    }
                    Arrays.sort(data);
                    this.satelliteView.setSatelliteData(data);
                }
                sLogger.v("satellite data { seen=" + seen + " used=" + used + " fixType=" + this.ubloxFixType + " maxDB=" + max);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private static String getFixTypeDescription(int fixType) {
        switch (fixType) {
            case 0:
                return "No Fix";
            case 1:
                return "Standard 2D/3D";
            case 2:
                return "Differential";
            case 4:
                return "RTK Fixed";
            case 5:
                return "RTK Float";
            case 6:
                return "DR Fix";
            default:
                return "Unknown";
        }
    }

    @Subscribe
    public void onNavigationPrefChangeEvent(NavigationPreferences preferences) {
        if (isMenuLoaded()) {
            updateNavPref();
        }
    }

    @Subscribe
    public void onDriverProfilePrefChangeEvent(DriverProfileUpdated event) {
        if (isMenuLoaded() && event.state == State.UPDATED) {
            updateGenericPref();
        }
    }

    @Subscribe
    public void onSupportedPidsChanged(ObdSupportedPidsChangedEvent supportedPidsChangedEvent) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateGPSSpeed();
            updateDynamicVehicleInfo();
        }
    }

    @Subscribe
    public void onPidChangeEvent(ObdPidChangeEvent event) {
        if (isMenuLoaded()) {
            switch (event.pid.getId()) {
                case 5:
                    updateEngineTemperature();
                    return;
                case 12:
                    updateRPM();
                    return;
                case 13:
                    updateSpeed();
                    return;
                case 47:
                    updateFuel();
                    return;
                case 256:
                    updateMAF();
                    return;
                case Pids.ENGINE_OIL_PRESSURE /*258*/:
                    updateEngineFuelPressure();
                    return;
                case Pids.ENGINE_TRIP_FUEL /*259*/:
                    updateEngineTripFuel();
                    return;
                case Pids.TOTAL_VEHICLE_DISTANCE /*260*/:
                    updateOdometer();
                    return;
                default:
                    return;
            }
        }
    }

    @Subscribe
    public void ObdStateChangeEvent(ObdConnectionStatusEvent event) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateDynamicVehicleInfo();
        }
    }

    @Subscribe
    public void onSpeedUnitChanged(SpeedUnitChanged event) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateGPSSpeed();
        }
    }

    @Subscribe
    public void GPSSpeedChangeEvent(GPSSpeedEvent event) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateGPSSpeed();
        }
    }

    @Subscribe
    public void onSpeedDataExpired(SpeedDataExpired speedDataExpired) {
        if (isMenuLoaded()) {
            updateSpeed();
            updateGPSSpeed();
        }
    }

    private boolean isMenuLoaded() {
        return this.dialFw != null;
    }

    private void setFix() {
        if (this.hereLocationFixManager != null) {
            Resources res = HudApplication.getAppContext().getResources();
            LocationFix event = this.hereLocationFixManager.getLocationFixEvent();
            sLogger.v("fix [" + event.locationAvailable + "] phone[" + event.usingPhoneLocation + "] u-blox[" + event.usingLocalGpsLocation + "] type[" + this.ubloxFixType + "]");
            if (!event.locationAvailable) {
                this.fix.setText(noFixStr);
            } else if (event.usingPhoneLocation) {
                this.fix.setText(res.getText(R.string.gps_phone));
            } else if (event.usingLocalGpsLocation) {
                this.fix.setText(this.ubloxFixType != null ? this.ubloxFixType : res.getText(R.string.gps_ublox_navdy));
            } else {
                this.fix.setText(noFixStr);
            }
        }
    }
}
