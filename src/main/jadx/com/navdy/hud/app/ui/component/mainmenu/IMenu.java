package com.navdy.hud.app.ui.component.mainmenu;

import android.view.View;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.List;

public interface IMenu {

    public enum Menu {
        MAIN,
        SETTINGS,
        PLACES,
        RECENT_PLACES,
        CONTACTS,
        RECENT_CONTACTS,
        CONTACT_OPTIONS,
        MAIN_OPTIONS,
        MUSIC,
        SEARCH,
        REPORT_ISSUE,
        ACTIVE_TRIP,
        SYSTEM_INFO,
        TEST_FAST_SCROLL_MENU,
        NUMBER_PICKER,
        MESSAGE_PICKER
    }

    public enum MenuLevel {
        BACK_TO_PARENT,
        SUB_LEVEL,
        CLOSE,
        REFRESH_CURRENT,
        ROOT
    }

    IMenu getChildMenu(IMenu iMenu, String str, String str2);

    int getInitialSelection();

    List<Model> getItems();

    Model getModelfromPos(int i);

    VerticalFastScrollIndex getScrollIndex();

    Menu getType();

    boolean isBindCallsEnabled();

    boolean isFirstItemEmpty();

    boolean isItemClickable(int i, int i2);

    void onBindToView(Model model, View view, int i, ModelState modelState);

    void onFastScrollEnd();

    void onFastScrollStart();

    void onItemSelected(ItemSelectionState itemSelectionState);

    void onScrollIdle();

    void onUnload(MenuLevel menuLevel);

    boolean selectItem(ItemSelectionState itemSelectionState);

    void setBackSelectionId(int i);

    void setBackSelectionPos(int i);

    void setSelectedIcon();

    void showToolTip();
}
