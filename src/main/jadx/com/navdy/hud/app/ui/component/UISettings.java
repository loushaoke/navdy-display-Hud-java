package com.navdy.hud.app.ui.component;

import com.navdy.hud.app.util.os.SystemProperties;

public class UISettings {
    private static final String ADVANCED_GPS_STATS = "persist.sys.gps.stats";
    private static final String DIAL_LONG_PRESS_ACTION_PLACE_SEARCH = "persist.sys.dlpress_search";
    private static final boolean dialLongPressPlaceSearchAction = SystemProperties.getBoolean(DIAL_LONG_PRESS_ACTION_PLACE_SEARCH, false);

    public static boolean isMusicBrowsingEnabled() {
        return true;
    }

    public static boolean isVerticalListNoCloseTimeout() {
        return true;
    }

    public static boolean isLongPressActionPlaceSearch() {
        return dialLongPressPlaceSearchAction;
    }

    public static boolean supportsIosSms() {
        return true;
    }

    public static boolean advancedGpsStatsEnabled() {
        return SystemProperties.getBoolean(ADVANCED_GPS_STATS, false);
    }
}
