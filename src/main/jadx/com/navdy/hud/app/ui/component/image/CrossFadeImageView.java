package com.navdy.hud.app.ui.component.image;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;

public class CrossFadeImageView extends FrameLayout {
    View big;
    Mode mode;
    View small;
    float smallAlpha;
    boolean smallAlphaSet;

    public enum Mode {
        BIG,
        SMALL
    }

    public CrossFadeImageView(Context context) {
        this(context, null);
    }

    public CrossFadeImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CrossFadeImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void inject(Mode mode) {
        if (this.big == null) {
            this.big = findViewById(R.id.big);
            this.small = findViewById(R.id.small);
            this.mode = mode;
            if (mode == Mode.BIG) {
                this.big.setAlpha(1.0f);
                this.small.setAlpha(0.0f);
                return;
            }
            if (this.smallAlphaSet) {
                this.small.setAlpha(this.smallAlpha);
            } else {
                this.small.setAlpha(1.0f);
            }
            this.big.setAlpha(0.0f);
        }
    }

    public View getBig() {
        return this.big;
    }

    public View getSmall() {
        return this.small;
    }

    public AnimatorSet getCrossFadeAnimator() {
        float bigFrom;
        float bigTo;
        float smallFrom;
        float smallTo;
        AnimatorSet set = new AnimatorSet();
        if (this.mode == Mode.BIG) {
            bigFrom = 1.0f;
            bigTo = 0.0f;
            smallFrom = 0.0f;
            if (this.smallAlphaSet) {
                smallTo = this.smallAlpha;
            } else {
                smallTo = 1.0f;
            }
        } else {
            bigFrom = 0.0f;
            bigTo = 1.0f;
            smallFrom = 1.0f;
            smallTo = 0.0f;
        }
        ObjectAnimator a = ObjectAnimator.ofFloat(this.big, View.ALPHA, new float[]{bigFrom, bigTo});
        a.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                CrossFadeImageView.this.big.setAlpha(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        Builder builder = set.play(a);
        a = ObjectAnimator.ofFloat(this.small, View.ALPHA, new float[]{smallFrom, smallTo});
        a.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                CrossFadeImageView.this.small.setAlpha(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        builder.with(a);
        set.addListener(new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                if (CrossFadeImageView.this.mode == Mode.BIG) {
                    CrossFadeImageView.this.mode = Mode.SMALL;
                    return;
                }
                CrossFadeImageView.this.mode = Mode.BIG;
            }
        });
        return set;
    }

    public void setMode(Mode newMode) {
        if (this.mode != newMode) {
            if (newMode == Mode.BIG) {
                this.big.setAlpha(1.0f);
                this.small.setAlpha(0.0f);
            } else {
                if (this.smallAlphaSet) {
                    this.small.setAlpha(this.smallAlpha);
                } else {
                    this.small.setAlpha(1.0f);
                }
                this.big.setAlpha(0.0f);
            }
            this.mode = newMode;
        }
    }

    public Mode getMode() {
        return this.mode;
    }

    public void setSmallAlpha(float alpha) {
        if (alpha == -1.0f) {
            this.smallAlphaSet = false;
            this.smallAlpha = 0.0f;
            return;
        }
        this.smallAlphaSet = true;
        this.smallAlpha = alpha;
    }
}
