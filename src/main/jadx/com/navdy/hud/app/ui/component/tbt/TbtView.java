package com.navdy.hud.app.ui.component.tbt;

import android.animation.AnimatorSet.Builder;
import android.content.Context;
import android.content.res.Resources;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import net.hockeyapp.android.LoginActivity;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 %2\u00020\u0001:\u0001%B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0017\u001a\u00020\u0018J\u001a\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\f2\n\u0010\u001b\u001a\u00060\u001cR\u00020\u001dJ\b\u0010\u001e\u001a\u00020\u0018H\u0014J\u0012\u0010\u001f\u001a\u00020\u00182\b\u0010\u001a\u001a\u0004\u0018\u00010\fH\u0002J\u000e\u0010 \u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\fJ\u0010\u0010!\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\fH\u0002J\u000e\u0010\"\u001a\u00020\u00182\u0006\u0010#\u001a\u00020$R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "directionView", "Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;", "distanceView", "Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;", "groupContainer", "Landroid/support/constraint/ConstraintLayout;", "tbtNextManeuverView", "Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;", "textInstructionView", "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;", "clear", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "onFinishInflate", "setConstraints", "setView", "setViewAttrs", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "Companion", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: TbtView.kt */
public final class TbtView extends FrameLayout {
    public static final Companion Companion = new Companion();
    private static final Logger logger = new Logger("TbtView");
    private static final int paddingFull = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_padding_full);
    private static final int paddingMedium = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_padding_medium);
    private static final Resources resources;
    private static final float shrinkLeftX = Companion.getResources().getDimension(R.dimen.tbt_shrinkleft_x);
    private HashMap _$_findViewCache;
    private CustomAnimationMode currentMode;
    private TbtDirectionView directionView;
    private TbtDistanceView distanceView;
    private ConstraintLayout groupContainer;
    private TbtNextManeuverView tbtNextManeuverView;
    private TbtTextInstructionView textInstructionView;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;", "", "()V", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "paddingFull", "", "getPaddingFull", "()I", "paddingMedium", "getPaddingMedium", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "shrinkLeftX", "", "getShrinkLeftX", "()F", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtView.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final Logger getLogger() {
            return TbtView.logger;
        }

        private final Resources getResources() {
            return TbtView.resources;
        }

        public final int getPaddingFull() {
            return TbtView.paddingFull;
        }

        public final int getPaddingMedium() {
            return TbtView.paddingMedium;
        }

        public final float getShrinkLeftX() {
            return TbtView.shrinkLeftX;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        view = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), view);
        return view;
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources, "HudApplication.getAppContext().resources");
        resources = resources;
    }

    public TbtView(@NotNull Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public TbtView(@NotNull Context context, @NotNull AttributeSet attrs) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public TbtView(@NotNull Context context, @NotNull AttributeSet attrs, int defStyleAttr) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        View findViewById = findViewById(R.id.groupContainer);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.support.constraint.ConstraintLayout");
        }
        this.groupContainer = (ConstraintLayout) findViewById;
        findViewById = findViewById(R.id.tbtDistanceView);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDistanceView");
        }
        this.distanceView = (TbtDistanceView) findViewById;
        findViewById = findViewById(R.id.tbtDirectionView);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDirectionView");
        }
        this.directionView = (TbtDirectionView) findViewById;
        findViewById = findViewById(R.id.tbtTextInstructionView);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView");
        }
        this.textInstructionView = (TbtTextInstructionView) findViewById;
        findViewById = findViewById(R.id.tbtNextManeuverView);
        if (findViewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView");
        }
        this.tbtNextManeuverView = (TbtNextManeuverView) findViewById;
    }

    public final void clear() {
        TbtDistanceView tbtDistanceView = this.distanceView;
        if (tbtDistanceView != null) {
            tbtDistanceView.clear();
        }
        TbtDirectionView tbtDirectionView = this.directionView;
        if (tbtDirectionView != null) {
            tbtDirectionView.clear();
        }
        TbtTextInstructionView tbtTextInstructionView = this.textInstructionView;
        if (tbtTextInstructionView != null) {
            tbtTextInstructionView.clear();
        }
        TbtNextManeuverView tbtNextManeuverView = this.tbtNextManeuverView;
        if (tbtNextManeuverView != null) {
            tbtNextManeuverView.clear();
        }
    }

    public final void setView(@NotNull CustomAnimationMode mode) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        setViewAttrs(mode);
        switch (mode) {
            case EXPAND:
                setX(0.0f);
                return;
            case SHRINK_LEFT:
                setX(Companion.getShrinkLeftX());
                return;
            default:
                return;
        }
    }

    public final void getCustomAnimator(@NotNull CustomAnimationMode mode, @NotNull Builder mainBuilder) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
        setViewAttrs(mode);
        switch (mode) {
            case EXPAND:
                mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this, 0.0f));
                return;
            case SHRINK_LEFT:
                mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this, Companion.getShrinkLeftX()));
                return;
            default:
                return;
        }
    }

    private final void setViewAttrs(CustomAnimationMode mode) {
        TbtDistanceView tbtDistanceView = this.distanceView;
        if (tbtDistanceView != null) {
            tbtDistanceView.setMode(mode);
        }
        TbtDirectionView tbtDirectionView = this.directionView;
        if (tbtDirectionView != null) {
            tbtDirectionView.setMode(mode);
        }
        TbtTextInstructionView tbtTextInstructionView = this.textInstructionView;
        if (tbtTextInstructionView != null) {
            tbtTextInstructionView.setMode(mode);
        }
        TbtNextManeuverView tbtNextManeuverView = this.tbtNextManeuverView;
        if (tbtNextManeuverView != null) {
            tbtNextManeuverView.setMode(mode);
        }
        setConstraints(mode);
        this.currentMode = mode;
    }

    public final void updateDisplay(@NotNull ManeuverDisplay event) {
        Object valueOf;
        Intrinsics.checkParameterIsNotNull(event, "event");
        TbtDistanceView tbtDistanceView = this.distanceView;
        if (tbtDistanceView != null) {
            tbtDistanceView.updateDisplay(event);
        }
        TbtDirectionView tbtDirectionView = this.directionView;
        if (tbtDirectionView != null) {
            tbtDirectionView.updateDisplay(event);
        }
        TbtNextManeuverView tbtNextManeuverView = this.tbtNextManeuverView;
        if (tbtNextManeuverView != null) {
            valueOf = Integer.valueOf(tbtNextManeuverView.getVisibility());
        } else {
            valueOf = null;
        }
        boolean visibleBefore = Intrinsics.areEqual(valueOf, Integer.valueOf(0));
        tbtNextManeuverView = this.tbtNextManeuverView;
        if (tbtNextManeuverView != null) {
            tbtNextManeuverView.updateDisplay(event);
        }
        tbtNextManeuverView = this.tbtNextManeuverView;
        if (tbtNextManeuverView != null) {
            valueOf = Integer.valueOf(tbtNextManeuverView.getVisibility());
        } else {
            valueOf = null;
        }
        boolean visibleAfter = Intrinsics.areEqual(valueOf, Integer.valueOf(0));
        TbtTextInstructionView tbtTextInstructionView;
        if (visibleBefore != visibleAfter) {
            tbtTextInstructionView = this.textInstructionView;
            if (tbtTextInstructionView != null) {
                tbtTextInstructionView.updateDisplay(event, true, visibleAfter);
            }
            setConstraints(this.currentMode);
            return;
        }
        tbtTextInstructionView = this.textInstructionView;
        if (tbtTextInstructionView != null) {
            TbtTextInstructionView.updateDisplay$default(tbtTextInstructionView, event, false, false, 6, null);
        }
    }

    private final void setConstraints(CustomAnimationMode mode) {
        if (mode != null) {
            Object valueOf;
            int padding;
            TbtNextManeuverView tbtNextManeuverView = this.tbtNextManeuverView;
            if (tbtNextManeuverView != null) {
                valueOf = Integer.valueOf(tbtNextManeuverView.getVisibility());
            } else {
                valueOf = null;
            }
            boolean addNextManeuver = Intrinsics.areEqual(valueOf, Integer.valueOf(0));
            switch (mode) {
                case EXPAND:
                    if (!addNextManeuver) {
                        padding = Companion.getPaddingFull();
                        break;
                    } else {
                        padding = Companion.getPaddingMedium();
                        break;
                    }
                default:
                    padding = Companion.getPaddingMedium();
                    break;
            }
            MarginLayoutParams lytParams = ((TbtDirectionView) findViewById(R.id.tbtDirectionView)).getLayoutParams();
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams.leftMargin = padding;
            LayoutParams layoutParams = ((TbtTextInstructionView) findViewById(R.id.tbtTextInstructionView)).getLayoutParams();
            if (layoutParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((MarginLayoutParams) layoutParams).leftMargin = padding;
            if (addNextManeuver) {
                tbtNextManeuverView = this.tbtNextManeuverView;
                lytParams = tbtNextManeuverView != null ? tbtNextManeuverView.getLayoutParams() : null;
                if (lytParams == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                lytParams.leftMargin = Companion.getPaddingMedium() * 2;
            }
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(this.groupContainer);
            TbtDirectionView tbtDirectionView = this.directionView;
            int id = tbtDirectionView != null ? tbtDirectionView.getId() : 0;
            TbtDistanceView tbtDistanceView = this.distanceView;
            constraintSet.connect(id, 1, tbtDistanceView != null ? tbtDistanceView.getId() : 0, 2, 0);
            TbtTextInstructionView tbtTextInstructionView = this.textInstructionView;
            if (tbtTextInstructionView != null) {
                id = tbtTextInstructionView.getId();
            } else {
                id = 0;
            }
            TbtDirectionView tbtDirectionView2 = this.directionView;
            constraintSet.connect(id, 1, tbtDirectionView2 != null ? tbtDirectionView2.getId() : 0, 2, 0);
            if (addNextManeuver) {
                int id2;
                tbtNextManeuverView = this.tbtNextManeuverView;
                if (tbtNextManeuverView != null) {
                    id = tbtNextManeuverView.getId();
                } else {
                    id = 0;
                }
                TbtTextInstructionView tbtTextInstructionView2 = this.textInstructionView;
                if (tbtTextInstructionView2 != null) {
                    id2 = tbtTextInstructionView2.getId();
                } else {
                    id2 = 0;
                }
                constraintSet.connect(id, 1, id2, 2, 0);
            }
            constraintSet.applyTo(this.groupContainer);
            invalidate();
            requestLayout();
        }
    }
}
