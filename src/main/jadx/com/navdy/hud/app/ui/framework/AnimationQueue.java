package com.navdy.hud.app.ui.framework;

import android.animation.Animator;
import android.animation.AnimatorSet;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class AnimationQueue {
    private boolean animationRunning;
    private Queue<AnimatorSet> animatorQueue = new LinkedBlockingQueue();
    private DefaultAnimationListener defaultAnimationListener = new DefaultAnimationListener() {
        public void onAnimationEnd(Animator animation) {
            AnimationQueue.this.animationRunning = false;
            AnimationQueue.this.runQueuedAnimation();
        }
    };

    public boolean isAnimationRunning() {
        return this.animationRunning;
    }

    public void startAnimation(AnimatorSet animatorSet) {
        if (this.animationRunning) {
            this.animatorQueue.add(animatorSet);
            return;
        }
        this.animationRunning = true;
        animatorSet.addListener(this.defaultAnimationListener);
        animatorSet.start();
    }

    private void runQueuedAnimation() {
        if (this.animatorQueue.size() != 0) {
            startAnimation((AnimatorSet) this.animatorQueue.remove());
        }
    }
}
