package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;

public class RecentContactsMenu implements IMenu {
    private static final Model back;
    private static final int backColor = resources.getColor(R.color.mm_back);
    private static final int contactColor = resources.getColor(R.color.mm_contacts);
    private static final String contactStr = resources.getString(R.string.carousel_menu_recent_contacts_title);
    private static final int noContactColor = resources.getColor(R.color.icon_user_bg_4);
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(RecentContactsMenu.class);
    private int backSelection;
    private Bus bus;
    private List<Model> cachedList;
    private IMenu parent;
    private Presenter presenter;
    private List<Model> returnToCacheList;
    private VerticalMenuComponent vscrollComponent;

    static {
        String title = resources.getString(R.string.back);
        int fluctuatorColor = backColor;
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    public RecentContactsMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }

    public List<Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        List<Model> list = new ArrayList();
        this.returnToCacheList = new ArrayList();
        list.add(back);
        int counter = 0;
        List<RecentCall> recentCalls = RecentCallManager.getInstance().getRecentCalls();
        if (recentCalls != null) {
            sLogger.v("recent contacts:" + recentCalls.size());
            for (RecentCall recentCall : recentCalls) {
                int counter2 = counter + 1;
                Model model = buildModel(recentCall, counter);
                model.state = recentCall;
                list.add(model);
                this.returnToCacheList.add(model);
                counter = counter2;
            }
        } else {
            sLogger.v("no recent contacts");
        }
        this.cachedList = list;
        return list;
    }

    public int getInitialSelection() {
        if (this.cachedList == null || this.cachedList.size() <= 1) {
            return 0;
        }
        return 1;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_place_recent_2, contactColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(contactStr);
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
        this.parent.onBindToView(model, view, pos, state);
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
        switch (level) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    sLogger.v("rcm:unload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.menu_back:
                sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                break;
            default:
                RecentCall recentCall = ((Model) this.cachedList.get(selection.pos)).state;
                Contact recentContact = new Contact(recentCall.name, recentCall.number, recentCall.numberType, recentCall.defaultImageIndex, recentCall.numericNumber);
                List<Contact> list = new ArrayList(1);
                list.add(recentContact);
                this.presenter.loadMenu(new ContactOptionsMenu(list, this.vscrollComponent, this.presenter, this, this.bus), MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
        }
        return true;
    }

    public Menu getType() {
        return Menu.RECENT_CONTACTS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    private Model buildModel(RecentCall call, int id) {
        return ((ContactsMenu) this.parent).buildModel(id, call.name, call.formattedNumber, call.numberTypeStr, call.defaultImageIndex, call.numberType, call.initials);
    }
}
