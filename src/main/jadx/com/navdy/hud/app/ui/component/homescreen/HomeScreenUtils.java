package com.navdy.hud.app.ui.component.homescreen;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ProgressBar;
import com.navdy.hud.app.HudApplication;

public class HomeScreenUtils {
    private static final float DENSITY = HudApplication.getAppContext().getResources().getDisplayMetrics().density;

    public static ObjectAnimator getXPositionAnimator(View view, float xPos) {
        return ObjectAnimator.ofFloat(view, "x", new float[]{xPos});
    }

    public static ObjectAnimator getYPositionAnimator(View view, float yPos) {
        return ObjectAnimator.ofFloat(view, "y", new float[]{yPos});
    }

    public static ObjectAnimator getTranslationXPositionAnimator(View view, int xPosBy) {
        return ObjectAnimator.ofFloat(view, View.TRANSLATION_X, new float[]{(float) xPosBy});
    }

    public static ObjectAnimator getTranslationYPositionAnimator(View view, int yPosBy) {
        return ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, new float[]{(float) yPosBy});
    }

    public static ValueAnimator getWidthAnimator(final View view, int targetWidth) {
        ValueAnimator widthAnimator = ValueAnimator.ofInt(new int[]{((MarginLayoutParams) view.getLayoutParams()).width, targetWidth});
        widthAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                ((MarginLayoutParams) view.getLayoutParams()).width = ((Integer) animation.getAnimatedValue()).intValue();
                view.invalidate();
                view.requestLayout();
            }
        });
        return widthAnimator;
    }

    public static ValueAnimator getProgressBarAnimator(final ProgressBar progressBar, int progress) {
        ValueAnimator progressAnimator = ValueAnimator.ofInt(new int[]{progressBar.getProgress(), progress});
        progressAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                progressBar.setProgress(((Integer) valueAnimator.getAnimatedValue()).intValue());
            }
        });
        return progressAnimator;
    }

    public static ValueAnimator getWidthAnimator(final View view, int startWidth, int targetWidth) {
        ValueAnimator widthAnimator = ValueAnimator.ofInt(new int[]{startWidth, targetWidth});
        widthAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                HomeScreenUtils.setWidth(view, ((Integer) animation.getAnimatedValue()).intValue());
            }
        });
        return widthAnimator;
    }

    public static void setWidth(View view, int width) {
        ((MarginLayoutParams) view.getLayoutParams()).width = width;
        view.invalidate();
        view.requestLayout();
    }

    public static AnimatorSet getMarginAnimator(final View view, int marginLeft, int marginRight) {
        MarginLayoutParams margins = (MarginLayoutParams) view.getLayoutParams();
        ValueAnimator.ofInt(new int[]{margins.leftMargin, marginLeft}).addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                ((MarginLayoutParams) view.getLayoutParams()).leftMargin = ((Integer) animation.getAnimatedValue()).intValue();
                view.invalidate();
                view.requestLayout();
            }
        });
        ValueAnimator.ofInt(new int[]{margins.rightMargin, marginRight}).addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                ((MarginLayoutParams) view.getLayoutParams()).rightMargin = ((Integer) animation.getAnimatedValue()).intValue();
                view.invalidate();
                view.requestLayout();
            }
        });
        AnimatorSet set = new AnimatorSet();
        set.playTogether(new Animator[]{v1, v2});
        return set;
    }

    public static ObjectAnimator getAlphaAnimator(View view, int alpha) {
        return ObjectAnimator.ofFloat(view, "alpha", new float[]{(float) alpha});
    }

    public static AnimatorSet getFadeInAndScaleUpAnimator(View view) {
        ObjectAnimator fadeInAnim = ObjectAnimator.ofFloat(view, "alpha", new float[]{0.0f, 1.0f});
        ObjectAnimator scaleUpXAnim = ObjectAnimator.ofFloat(view, "scaleX", new float[]{1.0f});
        ObjectAnimator scaleUpYAnim = ObjectAnimator.ofFloat(view, "scaleY", new float[]{1.0f});
        AnimatorSet fadeInAndScaleUp = new AnimatorSet();
        fadeInAndScaleUp.play(fadeInAnim).with(scaleUpXAnim).with(scaleUpYAnim);
        return fadeInAndScaleUp;
    }

    public static AnimatorSet getFadeOutAndScaleDownAnimator(View view) {
        ObjectAnimator fadeOutAnim = ObjectAnimator.ofFloat(view, "alpha", new float[]{0.0f});
        ObjectAnimator scaleDownXAnim = ObjectAnimator.ofFloat(view, "scaleX", new float[]{0.9f});
        ObjectAnimator scaleDownYAnim = ObjectAnimator.ofFloat(view, "scaleY", new float[]{0.9f});
        AnimatorSet fadeOutWithScaleDown = new AnimatorSet();
        fadeOutWithScaleDown.play(fadeOutAnim).with(scaleDownXAnim).with(scaleDownYAnim);
        return fadeOutWithScaleDown;
    }
}
