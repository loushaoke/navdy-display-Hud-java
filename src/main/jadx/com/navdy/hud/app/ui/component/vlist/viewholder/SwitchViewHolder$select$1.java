package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.drawable.ClipDrawable;
import kotlin.Metadata;
import kotlin.TypeCastException;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder$select$1", "Landroid/animation/ValueAnimator$AnimatorUpdateListener;", "(Lcom/navdy/hud/app/ui/component/vlist/viewholder/SwitchViewHolder;Landroid/graphics/drawable/ClipDrawable;)V", "onAnimationUpdate", "", "animation", "Landroid/animation/ValueAnimator;", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: SwitchViewHolder.kt */
public final class SwitchViewHolder$select$1 implements AnimatorUpdateListener {
    final /* synthetic */ ClipDrawable $clipDrawable;
    final /* synthetic */ SwitchViewHolder this$0;

    SwitchViewHolder$select$1(SwitchViewHolder $outer, ClipDrawable $captured_local_variable$1) {
        this.this$0 = $outer;
        this.$clipDrawable = $captured_local_variable$1;
    }

    public void onAnimationUpdate(@Nullable ValueAnimator animation) {
        Object animatedValue = animation != null ? animation.getAnimatedValue() : null;
        if (animatedValue == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
        this.$clipDrawable.setLevel((int) ((Float) animatedValue).floatValue());
        this.this$0.getSwitchBackground().invalidate();
    }
}
