package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.os.Handler;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView.Mode;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.AnimationType;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder.State;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import com.navdy.service.library.log.Logger;

public class IconsTwoViewHolder extends IconBaseViewHolder {
    private static final Logger sLogger = new Logger(IconsTwoViewHolder.class);
    private int imageIconSize = -1;

    public static Model buildModel(int id, int icon, int iconSmall, int iconFluctuatorColor, int iconDeselectedColor, String title, String subTitle) {
        return buildModel(id, icon, iconSmall, iconFluctuatorColor, iconDeselectedColor, title, subTitle, null);
    }

    public static Model buildModel(int id, int icon, int iconSmall, int iconFluctuatorColor, int iconDeselectedColor, String title, String subTitle, String subTitle2) {
        Model model = VerticalModelCache.getFromCache(ModelType.TWO_ICONS);
        if (model == null) {
            model = new Model();
        }
        model.type = ModelType.TWO_ICONS;
        model.id = id;
        model.icon = icon;
        model.iconSmall = iconSmall;
        model.iconFluctuatorColor = iconFluctuatorColor;
        model.iconDeselectedColor = iconDeselectedColor;
        model.title = title;
        model.subTitle = subTitle;
        model.subTitle2 = subTitle2;
        return model;
    }

    public static IconsTwoViewHolder buildViewHolder(ViewGroup parent, VerticalList vlist, Handler handler) {
        return new IconsTwoViewHolder(IconBaseViewHolder.getLayout(parent, R.layout.vlist_item, R.layout.crossfade_image_lyt), vlist, handler);
    }

    private IconsTwoViewHolder(ViewGroup layout, VerticalList vlist, Handler handler) {
        super(layout, vlist, handler);
    }

    public ModelType getModelType() {
        return ModelType.TWO_ICONS;
    }

    public void setItemState(State state, AnimationType animation, int duration, boolean startFluctuator) {
        super.setItemState(state, animation, duration, startFluctuator);
        int iconSize = 0;
        Mode mode = null;
        switch (state) {
            case SELECTED:
                iconSize = selectedIconSize;
                mode = Mode.BIG;
                break;
            case UNSELECTED:
                if (!this.iconScaleAnimationDisabled) {
                    iconSize = unselectedIconSize;
                    mode = Mode.SMALL;
                    break;
                }
                iconSize = selectedIconSize;
                mode = Mode.SMALL;
                break;
        }
        if (this.imageIconSize != -1) {
            iconSize = this.imageIconSize;
        }
        switch (animation) {
            case NONE:
            case INIT:
                MarginLayoutParams layoutParams = (MarginLayoutParams) this.iconContainer.getLayoutParams();
                layoutParams.width = iconSize;
                layoutParams.height = iconSize;
                this.crossFadeImageView.setMode(mode);
                return;
            case MOVE:
                this.animatorSetBuilder.with(VerticalAnimationUtils.animateDimension(this.iconContainer, iconSize));
                if (this.crossFadeImageView.getMode() != mode) {
                    this.animatorSetBuilder.with(this.crossFadeImageView.getCrossFadeAnimator());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void bind(Model model, ModelState modelState) {
        super.bind(model, modelState);
        setIcon(model.icon, model.iconSmall, modelState.updateImage, modelState.updateSmallImage, model.iconDeselectedColor, model.iconSize);
    }

    private void setIcon(int icon, int iconSmall, boolean updateImage, boolean updateSmallImage, int deselectedColor, int iconSize) {
        this.imageIconSize = iconSize;
        String initials = null;
        if (this.extras != null) {
            initials = (String) this.extras.get(Model.INITIALS);
        }
        if (updateImage) {
            Style bigStyle;
            if (initials != null) {
                bigStyle = Style.MEDIUM;
            } else {
                bigStyle = Style.DEFAULT;
            }
            ((InitialsImageView) this.crossFadeImageView.getBig()).setImage(icon, initials, bigStyle);
        }
        if (updateSmallImage) {
            InitialsImageView small = (InitialsImageView) this.crossFadeImageView.getSmall();
            small.setAlpha(1.0f);
            if (iconSmall == 0) {
                small.setBkColor(deselectedColor);
            } else {
                small.setBkColor(0);
            }
            small.setImage(iconSmall, initials, Style.TINY);
        }
    }
}
