package com.navdy.hud.app.ui.component.destination;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.service.library.events.places.PlaceType;
import java.util.List;

public class DestinationParcelable implements Parcelable {
    public static final Creator<DestinationParcelable> CREATOR = new Creator<DestinationParcelable>() {
        public DestinationParcelable createFromParcel(Parcel source) {
            return new DestinationParcelable(source);
        }

        public DestinationParcelable[] newArray(int size) {
            return new DestinationParcelable[size];
        }
    };
    private static final String EMPTY = "";
    List<Contact> contacts;
    String destinationAddress;
    String destinationPlaceId;
    String destinationSubTitle;
    String destinationSubTitle2;
    boolean destinationSubTitle2Formatted;
    boolean destinationSubTitleFormatted;
    String destinationTitle;
    double displayLatitude;
    double displayLongitude;
    public String distanceStr;
    int icon;
    int iconDeselectedColor;
    int iconSelectedColor;
    int iconUnselected;
    int id;
    String identifier;
    double navLatitude;
    double navLongitude;
    List<Contact> phoneNumbers;
    PlaceType placeType;
    String routeId;
    DestinationType type;

    public enum DestinationType {
        NONE,
        DESTINATION
    }

    public DestinationParcelable(int id, String destinationTitle, String destinationSubTitle, boolean destinationSubTitleFormatted, String destinationSubTitle2, boolean destinationSubTitleFormatted2, String destinationAddress, double navLatitude, double navLongitude, double displayLatitude, double displayLongitude, int icon, int iconUnselected, int iconSelectedColor, int iconDeselectedColor, DestinationType type, PlaceType placeType) {
        this.id = id;
        this.destinationTitle = destinationTitle;
        this.destinationSubTitle = destinationSubTitle;
        this.destinationSubTitleFormatted = destinationSubTitleFormatted;
        this.destinationSubTitle2 = destinationSubTitle2;
        this.destinationSubTitle2Formatted = destinationSubTitleFormatted2;
        this.destinationAddress = destinationAddress;
        this.navLatitude = navLatitude;
        this.navLongitude = navLongitude;
        this.displayLatitude = displayLatitude;
        this.displayLongitude = displayLongitude;
        this.icon = icon;
        this.iconUnselected = iconUnselected;
        this.iconSelectedColor = iconSelectedColor;
        this.iconDeselectedColor = iconDeselectedColor;
        this.type = type;
        this.placeType = placeType;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public void setPlaceId(String placeId) {
        this.destinationPlaceId = placeId;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public void setPhoneNumbers(List<Contact> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        byte b;
        byte b2 = (byte) 1;
        dest.writeString(this.destinationTitle == null ? "" : this.destinationTitle);
        dest.writeString(this.destinationSubTitle == null ? "" : this.destinationSubTitle);
        if (this.destinationSubTitleFormatted) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        dest.writeByte(b);
        dest.writeString(this.destinationSubTitle2 == null ? "" : this.destinationSubTitle2);
        if (!this.destinationSubTitle2Formatted) {
            b2 = (byte) 0;
        }
        dest.writeByte(b2);
        dest.writeString(this.destinationAddress == null ? "" : this.destinationAddress);
        dest.writeDouble(this.navLatitude);
        dest.writeDouble(this.navLongitude);
        dest.writeDouble(this.displayLatitude);
        dest.writeDouble(this.displayLongitude);
        dest.writeInt(this.icon);
        dest.writeInt(this.iconUnselected);
        dest.writeInt(this.iconSelectedColor);
        dest.writeInt(this.iconDeselectedColor);
        dest.writeString(this.routeId == null ? "" : this.routeId);
        dest.writeString(this.destinationPlaceId == null ? "" : this.routeId);
        dest.writeInt(this.type.ordinal());
        dest.writeInt(this.id);
        dest.writeInt(this.placeType.ordinal());
        dest.writeList(this.contacts);
        dest.writeList(this.phoneNumbers);
        dest.writeString(this.identifier);
    }

    protected DestinationParcelable(Parcel in) {
        boolean z = true;
        this.destinationTitle = in.readString();
        this.destinationSubTitle = in.readString();
        this.destinationSubTitleFormatted = in.readByte() == (byte) 1;
        this.destinationSubTitle2 = in.readString();
        if (in.readByte() != (byte) 1) {
            z = false;
        }
        this.destinationSubTitle2Formatted = z;
        this.destinationAddress = in.readString();
        this.navLatitude = in.readDouble();
        this.navLongitude = in.readDouble();
        this.displayLatitude = in.readDouble();
        this.displayLongitude = in.readDouble();
        this.icon = in.readInt();
        this.iconUnselected = in.readInt();
        this.iconSelectedColor = in.readInt();
        this.iconDeselectedColor = in.readInt();
        this.routeId = in.readString();
        this.destinationPlaceId = in.readString();
        this.type = DestinationType.values()[in.readInt()];
        this.id = in.readInt();
        this.placeType = PlaceType.values()[in.readInt()];
        this.contacts = in.readArrayList(null);
        this.phoneNumbers = in.readArrayList(null);
        this.identifier = in.readString();
    }
}
