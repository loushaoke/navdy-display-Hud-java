package com.navdy.hud.app.ui.component.homescreen;

import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedDataExpired;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged;
import com.navdy.hud.app.maps.MapEvents.GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents.SpeedWarning;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class SpeedView extends LinearLayout {
    private Bus bus;
    private int lastSpeed;
    private SpeedUnit lastSpeedUnit;
    private Logger logger;
    private SpeedManager speedManager;
    @InjectView(R.id.speedUnitView)
    TextView speedUnitView;
    @InjectView(R.id.speedView)
    TextView speedView;

    public SpeedView(Context context) {
        this(context, null);
    }

    public SpeedView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SpeedView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.lastSpeed = -1;
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        if (!isInEditMode()) {
            this.speedManager = SpeedManager.getInstance();
            this.bus = RemoteDeviceManager.getInstance().getBus();
            this.bus.register(this);
        }
    }

    @Subscribe
    public void onSpeedWarningEvent(SpeedWarning speedWarning) {
        if (speedWarning.exceed) {
            this.speedView.setTextColor(-16711681);
        } else {
            this.speedView.setTextColor(-1);
        }
    }

    @Subscribe
    public void ObdStateChangeEvent(ObdConnectionStatusEvent event) {
        this.lastSpeed = -1;
        this.lastSpeedUnit = null;
        setTrackingSpeed();
    }

    @Subscribe
    public void ObdPidChangeEvent(ObdPidChangeEvent event) {
        switch (event.pid.getId()) {
            case 13:
                setTrackingSpeed();
                return;
            default:
                return;
        }
    }

    @Subscribe
    public void onSpeedUnitChanged(SpeedUnitChanged event) {
        setTrackingSpeed();
    }

    @Subscribe
    public void GPSSpeedChangeEvent(GPSSpeedEvent event) {
        setTrackingSpeed();
    }

    @Subscribe
    public void onSpeedDataExpired(SpeedDataExpired speedDataExpired) {
        setTrackingSpeed();
    }

    private void setTrackingSpeed() {
        int speed = this.speedManager.getCurrentSpeed();
        if (speed < 0) {
            speed = 0;
        }
        SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
        if (speed != this.lastSpeed) {
            this.lastSpeed = speed;
            this.speedView.setText(String.valueOf(speed));
        }
        if (speedUnit != this.lastSpeedUnit) {
            this.lastSpeedUnit = speedUnit;
            String str = "";
            switch (speedUnit) {
                case MILES_PER_HOUR:
                    str = HomeScreenConstants.SPEED_MPH;
                    break;
                case KILOMETERS_PER_HOUR:
                    str = HomeScreenConstants.SPEED_KM;
                    break;
                case METERS_PER_SECOND:
                    str = HomeScreenConstants.SPEED_METERS;
                    break;
            }
            this.speedUnitView.setText(str);
        }
    }

    public void clearState() {
        this.lastSpeed = -1;
        this.lastSpeedUnit = null;
        setTrackingSpeed();
    }

    public void getTopAnimator(Builder builder, boolean out) {
        if (out) {
            builder.with(HomeScreenUtils.getTranslationXPositionAnimator(this.speedUnitView, HomeScreenResourceValues.topViewSpeedOut));
            builder.with(ObjectAnimator.ofFloat(this.speedUnitView, View.ALPHA, new float[]{1.0f, 0.0f}));
            return;
        }
        builder.with(HomeScreenUtils.getTranslationXPositionAnimator(this.speedUnitView, 0));
        builder.with(ObjectAnimator.ofFloat(this.speedUnitView, View.ALPHA, new float[]{0.0f, 1.0f}));
        this.speedView.setVisibility(0);
    }

    public void resetTopViewsAnimator() {
        this.speedUnitView.setAlpha(1.0f);
        this.speedUnitView.setTranslationX(0.0f);
        this.speedView.setVisibility(0);
    }
}
