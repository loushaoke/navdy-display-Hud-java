package com.navdy.hud.app.ui.component;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.View;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\u00152\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001c\u001a\u00020\tH\u0016R\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000e\"\u0004\b\u0013\u0010\u0010\u00a8\u0006\u001d"}, d2 = {"Lcom/navdy/hud/app/ui/component/SwitchHaloView;", "Lcom/navdy/hud/app/ui/component/HaloView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "maxScalingFactorX", "", "getMaxScalingFactorX", "()F", "setMaxScalingFactorX", "(F)V", "maxScalingFactorY", "getMaxScalingFactorY", "setMaxScalingFactorY", "onAnimationUpdateInternal", "", "animation", "Landroid/animation/ValueAnimator;", "onDrawInternal", "canvas", "Landroid/graphics/Canvas;", "setStrokeColor", "color", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: SwitchHaloView.kt */
public final class SwitchHaloView extends HaloView {
    private HashMap _$_findViewCache;
    private float maxScalingFactorX;
    private float maxScalingFactorY;

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        view = findViewById(i);
        this._$_findViewCache.put(Integer.valueOf(i), view);
        return view;
    }

    public final float getMaxScalingFactorX() {
        return this.maxScalingFactorX;
    }

    public final void setMaxScalingFactorX(float <set-?>) {
        this.maxScalingFactorX = <set-?>;
    }

    public final float getMaxScalingFactorY() {
        return this.maxScalingFactorY;
    }

    public final void setMaxScalingFactorY(float <set-?>) {
        this.maxScalingFactorY = <set-?>;
    }

    public SwitchHaloView(@NotNull Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public SwitchHaloView(@NotNull Context context, @NotNull AttributeSet attrs) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs);
    }

    public SwitchHaloView(@NotNull Context context, @NotNull AttributeSet attrs, int defStyleAttr) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
        super(context, attrs, defStyleAttr);
    }

    public void onAnimationUpdateInternal(@NotNull ValueAnimator animation) {
        Intrinsics.checkParameterIsNotNull(animation, "animation");
        super.onAnimationUpdateInternal(animation);
        Object animatedValue = animation.getAnimatedValue();
        if (animatedValue == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
        float scaleFactor = ((((Float) animatedValue).floatValue() - this.startRadius) / (this.endRadius - this.startRadius)) * ((float) 2);
        this.maxScalingFactorX = (this.endRadius - this.startRadius) / ((float) getWidth());
        this.maxScalingFactorY = (this.endRadius - this.startRadius) / ((float) getHeight());
        setScaleX(((float) 1) + (this.maxScalingFactorX * scaleFactor));
        setScaleY(((float) 1) + (this.maxScalingFactorY * scaleFactor));
        setPivotX((float) (getWidth() / 2));
        setPivotY((float) (getHeight() / 2));
        invalidate();
    }

    public void onDrawInternal(@Nullable Canvas canvas) {
    }

    public void setStrokeColor(int color) {
        super.setStrokeColor(color);
        GradientDrawable gradientDrawable = getBackground();
        if (gradientDrawable == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
        }
        gradientDrawable.setColor(color);
    }
}
