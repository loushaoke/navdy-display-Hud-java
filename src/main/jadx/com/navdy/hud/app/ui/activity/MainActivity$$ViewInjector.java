package com.navdy.hud.app.ui.activity;

import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.view.ContainerView;

public class MainActivity$$ViewInjector {
    public static void inject(Finder finder, MainActivity target, Object source) {
        target.container = (ContainerView) finder.findRequiredView(source, R.id.container, "field 'container'");
    }

    public static void reset(MainActivity target) {
        target.container = null;
    }
}
