package com.navdy.hud.app.ui.component.homescreen;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.TextView;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents.LocationFix;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent;
import com.navdy.hud.app.obd.ObdManager.ObdSupportedPidsChangedEvent;
import com.navdy.hud.app.ui.component.dashboard.GaugeViewPager;
import com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.Adapter;
import com.navdy.hud.app.ui.component.dashboard.GaugeViewPager.ChangeListener;
import com.navdy.hud.app.ui.component.dashboard.IDashboardOptionsAdapter;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.IWidgetFilter;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.UserPreferenceChanged;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.util.HeadingDataUtil;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.hud.app.view.GaugeView;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.hud.app.view.SpeedometerGaugePresenter2;
import com.navdy.hud.app.view.TachometerGaugePresenter;
import com.navdy.obd.PidSet;
import com.navdy.service.library.events.preferences.DashboardPreferences;
import com.navdy.service.library.events.preferences.MiddleGauge;
import com.navdy.service.library.events.preferences.ScrollableSide;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.concurrent.TimeUnit;

public class SmartDashView extends BaseSmartDashView implements IDashboardOptionsAdapter {
    public static final int HEADING_EXPIRE_INTERVAL = 3000;
    public static final long MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE = TimeUnit.MINUTES.toMillis(15);
    public static final float NAVIGATION_MODE_SCALE_FACTOR = 0.8f;
    private static final boolean SHOW_ETA = false;
    private static final Logger sLogger = new Logger(SmartDashView.class);
    private int activeModeGaugeMargin;
    private String activeRightGaugeId;
    private String activeleftGaugeId;
    @InjectView(R.id.eta_time_amPm)
    TextView etaAmPm;
    @InjectView(R.id.eta_time)
    TextView etaText;
    private int fullModeGaugeTopMargin;
    private SharedPreferences globalPreferences;
    private HeadingDataUtil headingDataUtil = new HeadingDataUtil();
    public boolean isShowingDriveScoreGauge;
    private String lastETA;
    private String lastETA_AmPm;
    private double lastHeading = 0.0d;
    private long lastHeadingSampleTime = 0;
    private long lastUserPreferenceRecordedTime = 0;
    @InjectView(R.id.eta_layout)
    ViewGroup mEtaLayout;
    GaugeViewPagerAdapter mLeftAdapter;
    @InjectView(R.id.left_gauge)
    GaugeViewPager mLeftGaugeViewPager;
    @InjectView(R.id.middle_gauge)
    GaugeView mMiddleGaugeView;
    GaugeViewPagerAdapter mRightAdapter;
    @InjectView(R.id.right_gauge)
    GaugeViewPager mRightGaugeViewPager;
    private SmartDashWidgetManager mSmartDashWidgetManager;
    private int middleGauge = 1;
    private int middleGaugeShrinkEarlyTbtOffset;
    private int middleGaugeShrinkMargin;
    private int middleGaugeShrinkModeMargin;
    private boolean paused;
    private int scrollableSide = 1;
    SpeedometerGaugePresenter2 speedometerGaugePresenter2;
    TachometerGaugePresenter tachometerPresenter;

    /* renamed from: com.navdy.hud.app.ui.component.homescreen.SmartDashView$8 */
    static /* synthetic */ class AnonymousClass8 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge = new int[MiddleGauge.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide = new int[ScrollableSide.values().length];

        static {
            $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode = new int[CustomAnimationMode.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[CustomAnimationMode.EXPAND.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[CustomAnimationMode.SHRINK_LEFT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[CustomAnimationMode.SHRINK_MODE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent = new int[CustomKeyEvent.values().length];
            try {
                $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[CustomKeyEvent.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[CustomKeyEvent.RIGHT.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[ScrollableSide.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[ScrollableSide.RIGHT.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[MiddleGauge.SPEEDOMETER.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[MiddleGauge.TACHOMETER.ordinal()] = 2;
            } catch (NoSuchFieldError e9) {
            }
        }
    }

    class GaugeViewPagerAdapter implements Adapter {
        private int mExcludedPosition = -1;
        private boolean mLeft;
        private SmartDashWidgetManager mSmartDashWidgetManager;
        private SmartDashWidgetCache mWidgetCache;

        public GaugeViewPagerAdapter(SmartDashWidgetManager smartDashWidgetManager, boolean left) {
            this.mSmartDashWidgetManager = smartDashWidgetManager;
            this.mLeft = left;
            this.mWidgetCache = this.mSmartDashWidgetManager.buildSmartDashWidgetCache(this.mLeft ? 0 : 1);
            this.mSmartDashWidgetManager.registerForChanges(this);
        }

        @Subscribe
        public void onWidgetsReload(Reload reload) {
            if (reload == Reload.RELOAD_CACHE) {
                this.mWidgetCache = this.mSmartDashWidgetManager.buildSmartDashWidgetCache(this.mLeft ? 0 : 1);
            }
        }

        public int getCount() {
            return this.mWidgetCache.getWidgetsCount();
        }

        public int getExcludedPosition() {
            return this.mExcludedPosition;
        }

        public void setExcludedPosition(int excludedPosition) {
            if (this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(SmartDashWidgetManager.EMPTY_WIDGET_ID) == excludedPosition) {
                this.mExcludedPosition = -1;
            } else {
                this.mExcludedPosition = excludedPosition;
            }
        }

        public View getView(int position, View recycledView, ViewGroup parent, boolean isActive) {
            DashboardWidgetView gaugeView;
            boolean z = false;
            if (recycledView == null) {
                gaugeView = new DashboardWidgetView(parent.getContext());
                gaugeView.setLayoutParams(new LayoutParams(parent.getResources().getDimensionPixelSize(R.dimen.gauge_frame_width), parent.getResources().getDimensionPixelSize(R.dimen.gauge_frame_height)));
            } else {
                gaugeView = (DashboardWidgetView) recycledView;
            }
            DashboardWidgetPresenter oldPresenter = (DashboardWidgetPresenter) gaugeView.getTag();
            DashboardWidgetPresenter presenter = this.mWidgetCache.getWidgetPresenter(position);
            if (!(oldPresenter == null || oldPresenter == presenter || oldPresenter.getWidgetView() != gaugeView)) {
                oldPresenter.setView(null, null);
            }
            if (presenter != null) {
                Bundle args = new Bundle();
                args.putInt(DashboardWidgetPresenter.EXTRA_GRAVITY, this.mLeft ? 0 : 2);
                args.putBoolean(DashboardWidgetPresenter.EXTRA_IS_ACTIVE, isActive);
                if (isActive) {
                    if (this.mLeft) {
                        SmartDashView.this.activeleftGaugeId = presenter.getWidgetIdentifier();
                    } else {
                        SmartDashView.this.activeRightGaugeId = presenter.getWidgetIdentifier();
                    }
                    SmartDashView smartDashView = SmartDashView.this;
                    if (SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID.equals(SmartDashView.this.activeleftGaugeId) || SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID.equals(SmartDashView.this.activeRightGaugeId)) {
                        z = true;
                    }
                    smartDashView.isShowingDriveScoreGauge = z;
                    presenter.setWidgetVisibleToUser(true);
                } else {
                    presenter.setWidgetVisibleToUser(false);
                }
                presenter.setView(gaugeView, args);
            }
            gaugeView.setTag(presenter);
            return gaugeView;
        }

        public DashboardWidgetPresenter getPresenter(int position) {
            return this.mWidgetCache.getWidgetPresenter(position);
        }
    }

    public SmartDashView(Context context) {
        super(context);
    }

    public SmartDashView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SmartDashView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.fullModeGaugeTopMargin = getResources().getDimensionPixelSize(R.dimen.gauge_frame_top_margin_full_mode);
        this.activeModeGaugeMargin = getResources().getDimensionPixelSize(R.dimen.gauge_frame_top_margin_active_mode);
    }

    public void init(final HomeScreenView homeScreenView) {
        this.globalPreferences = homeScreenView.globalPreferences;
        this.mSmartDashWidgetManager = new SmartDashWidgetManager(this.globalPreferences, getContext());
        SharedPreferences sharedPreferences = homeScreenView.getDriverPreferences();
        this.mSmartDashWidgetManager.setFilter(new IWidgetFilter() {
            public boolean filter(String identifier) {
                boolean z = true;
                SharedPreferences sharedPreferences = homeScreenView.getDriverPreferences();
                String leftGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID);
                String rightGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, SmartDashWidgetManager.COMPASS_WIDGET_ID);
                if (!TextUtils.isEmpty(identifier) && (identifier.equals(leftGaugeId) || identifier.equals(rightGaugeId))) {
                    return false;
                }
                boolean z2 = true;
                switch (identifier.hashCode()) {
                    case -1158963060:
                        if (identifier.equals(SmartDashWidgetManager.MPG_AVG_WIDGET_ID)) {
                            z2 = true;
                            break;
                        }
                        break;
                    case -131933527:
                        if (identifier.equals(SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                            z2 = true;
                            break;
                        }
                        break;
                    case 1168400042:
                        if (identifier.equals(SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                            z2 = true;
                            break;
                        }
                        break;
                    case 1872543020:
                        if (identifier.equals(SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID)) {
                            z2 = false;
                            break;
                        }
                        break;
                }
                PidSet pidSet;
                switch (z2) {
                    case false:
                        if (MapSettings.isTrafficDashWidgetsEnabled()) {
                            z = false;
                        }
                        return z;
                    case true:
                        pidSet = ObdManager.getInstance().getSupportedPids();
                        if (pidSet == null || !pidSet.contains(47)) {
                            return true;
                        }
                        return false;
                    case true:
                        pidSet = ObdManager.getInstance().getSupportedPids();
                        if (pidSet == null || !pidSet.contains(5)) {
                            return true;
                        }
                        return false;
                    case true:
                        pidSet = ObdManager.getInstance().getSupportedPids();
                        if (pidSet == null || !pidSet.contains(256)) {
                            return true;
                        }
                        return false;
                    default:
                        return false;
                }
            }
        });
        this.mSmartDashWidgetManager.registerForChanges(this);
        this.tachometerPresenter = new TachometerGaugePresenter(getContext());
        this.speedometerGaugePresenter2 = new SpeedometerGaugePresenter2(getContext());
        this.bus.register(new Object() {
            @Subscribe
            public void onGpsLocationChanged(Location location) {
                SmartDashView.this.headingDataUtil.setHeading((double) location.getBearing());
                double newHeadingValue = SmartDashView.this.headingDataUtil.getHeading();
                if (SmartDashView.sLogger.isLoggable(2)) {
                    SmartDashView.sLogger.v("SmartDash New: Heading :" + newHeadingValue + ", " + GpsUtils.getHeadingDirection(newHeadingValue) + ", Provider :" + location.getProvider());
                }
                SmartDashView.this.mSmartDashWidgetManager.updateWidget(SmartDashWidgetManager.COMPASS_WIDGET_ID, Double.valueOf(newHeadingValue));
            }

            @Subscribe
            public void onLocationFixChangeEvent(LocationFix event) {
                if (!event.locationAvailable) {
                    SmartDashView.this.headingDataUtil.reset();
                    SmartDashView.this.mSmartDashWidgetManager.updateWidget(SmartDashWidgetManager.COMPASS_WIDGET_ID, Float.valueOf(0.0f));
                }
            }

            @Subscribe
            public void onDriverProfileChanged(DriverProfileChanged profileChanged) {
                homeScreenView.updateDriverPrefs();
                if (SmartDashView.this.mSmartDashWidgetManager != null) {
                    SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
                }
            }

            @Subscribe
            public void updateETA(ManeuverDisplay event) {
                if (!homeScreenView.isNavigationActive()) {
                    return;
                }
                if (event.etaDate == null) {
                    SmartDashView.sLogger.w("etaDate is not set");
                    return;
                }
                if (!TextUtils.equals(event.eta, SmartDashView.this.lastETA)) {
                    SmartDashView.this.lastETA = event.eta;
                    SmartDashView.this.etaText.setText(SmartDashView.this.lastETA);
                }
                if (!TextUtils.equals(event.etaAmPm, SmartDashView.this.lastETA_AmPm)) {
                    SmartDashView.this.lastETA_AmPm = event.etaAmPm;
                    SmartDashView.this.etaAmPm.setText(SmartDashView.this.lastETA_AmPm);
                }
            }

            @Subscribe
            public void ObdStateChangeEvent(ObdConnectionStatusEvent event) {
                ObdManager obdManager = ObdManager.getInstance();
                if (SmartDashView.this.tachometerPresenter != null) {
                    int rpm = obdManager.getEngineRpm();
                    if (rpm < 0) {
                        rpm = 0;
                    }
                    SmartDashView.this.tachometerPresenter.setRPM(rpm);
                }
                if (SmartDashView.this.mSmartDashWidgetManager != null) {
                    SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
                }
            }

            @Subscribe
            public void onSupportedPidEventsChange(ObdSupportedPidsChangedEvent event) {
                SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
            }

            @Subscribe
            public void onUserGaugePreferencesChanged(UserPreferenceChanged userPreferenceChanged) {
                SmartDashView.this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
            }

            @Subscribe
            public void onDashboardPreferences(DashboardPreferences dashboardPreferences) {
                if (dashboardPreferences != null) {
                    if (dashboardPreferences.middleGauge != null) {
                        switch (AnonymousClass8.$SwitchMap$com$navdy$service$library$events$preferences$MiddleGauge[dashboardPreferences.middleGauge.ordinal()]) {
                            case 1:
                                SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, Integer.valueOf(1));
                                break;
                            case 2:
                                SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, Integer.valueOf(0));
                                break;
                        }
                    }
                    if (dashboardPreferences.scrollableSide != null) {
                        switch (AnonymousClass8.$SwitchMap$com$navdy$service$library$events$preferences$ScrollableSide[dashboardPreferences.scrollableSide.ordinal()]) {
                            case 1:
                                SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, Integer.valueOf(0));
                                break;
                            case 2:
                                SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, Integer.valueOf(1));
                                break;
                        }
                    }
                    if (SmartDashWidgetManager.isValidGaugeId(dashboardPreferences.leftGaugeId)) {
                        SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, dashboardPreferences.leftGaugeId);
                    }
                    if (SmartDashWidgetManager.isValidGaugeId(dashboardPreferences.rightGaugeId)) {
                        SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, dashboardPreferences.rightGaugeId);
                    }
                    SmartDashView.this.showViewsAccordingToUserPreferences();
                }
            }
        });
        Resources resources = getResources();
        this.middleGaugeShrinkMargin = resources.getDimensionPixelSize(R.dimen.middle_gauge_shrink_margin);
        this.middleGaugeShrinkModeMargin = resources.getDimensionPixelSize(R.dimen.middle_gauge_shrink_mode_margin);
        this.middleGaugeShrinkEarlyTbtOffset = getResources().getDimensionPixelSize(R.dimen.middle_gauge_shrink_early_tbt_offset);
        super.init(homeScreenView);
    }

    @Subscribe
    public void onReload(Reload reload) {
        if (reload == Reload.RELOADED) {
            showViewsAccordingToUserPreferences();
        }
    }

    private void showViewsAccordingToUserPreferences() {
        sLogger.d("showViewsAccordingToUserPreferences");
        boolean isDefaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        SharedPreferences sharedPreferences = isDefaultProfile ? this.globalPreferences : this.homeScreenView.getDriverPreferences();
        this.scrollableSide = sharedPreferences.getInt(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, 1);
        sLogger.v("scrollableSide:" + this.scrollableSide);
        showMiddleGaugeAccordingToUserPreferences();
        String leftGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID);
        int leftGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(leftGaugeId);
        sLogger.d("Left Gauge ID : " + leftGaugeId + ", Index : " + leftGaugeIndex);
        if (leftGaugeIndex < 0) {
            leftGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(SmartDashWidgetManager.EMPTY_WIDGET_ID);
        }
        this.mLeftGaugeViewPager.setSelectedPosition(leftGaugeIndex);
        String rightGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, SmartDashWidgetManager.COMPASS_WIDGET_ID);
        int rightGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(rightGaugeId);
        sLogger.d("Right Gauge ID : " + rightGaugeId + ", Index : " + rightGaugeIndex);
        if (rightGaugeIndex < 0) {
            rightGaugeIndex = this.mSmartDashWidgetManager.getIndexForWidgetIdentifier(SmartDashWidgetManager.EMPTY_WIDGET_ID);
        }
        this.mRightGaugeViewPager.setSelectedPosition(rightGaugeIndex);
        if (!isDefaultProfile) {
            this.globalPreferences.edit().putInt(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, this.middleGauge).putInt(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, this.scrollableSide).putString(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, leftGaugeId).putString(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, rightGaugeId).apply();
        }
        this.mLeftAdapter.setExcludedPosition(rightGaugeIndex);
        this.mRightAdapter.setExcludedPosition(leftGaugeIndex);
        recordUsersWidgetPreference();
    }

    private void showTachometer() {
        this.speedometerGaugePresenter2.onPause();
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(false);
        this.speedometerGaugePresenter2.setView(null);
        if (!this.paused) {
            this.tachometerPresenter.onResume();
        }
        this.tachometerPresenter.setWidgetVisibleToUser(true);
        this.tachometerPresenter.setView(this.mMiddleGaugeView);
        int engineRpm = ObdManager.getInstance().getEngineRpm();
        if (!(engineRpm == -1 || this.tachometerPresenter == null)) {
            this.tachometerPresenter.setRPM(engineRpm);
        }
        int speed = SpeedManager.getInstance().getCurrentSpeed();
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(speed);
        }
    }

    private void showSpeedometerGauge() {
        this.tachometerPresenter.onPause();
        this.tachometerPresenter.setWidgetVisibleToUser(false);
        this.tachometerPresenter.setView(null);
        if (!this.paused) {
            this.speedometerGaugePresenter2.onResume();
        }
        this.speedometerGaugePresenter2.setWidgetVisibleToUser(true);
        this.speedometerGaugePresenter2.setView(this.mMiddleGaugeView);
        this.speedometerGaugePresenter2.setSpeed(SpeedManager.getInstance().getCurrentSpeed());
    }

    protected void initializeView() {
        super.initializeView();
        this.mSmartDashWidgetManager.reLoadAvailableWidgets(false);
        if (this.mLeftAdapter == null) {
            this.mLeftAdapter = new GaugeViewPagerAdapter(this.mSmartDashWidgetManager, true);
            this.mLeftGaugeViewPager.setAdapter(this.mLeftAdapter);
            this.mLeftGaugeViewPager.setChangeListener(new ChangeListener() {
                public void onGaugeChanged(int position) {
                    String identifier = SmartDashView.this.mSmartDashWidgetManager.getWidgetIdentifierForIndex(position);
                    SmartDashView.sLogger.d("onGaugeChanged, (Left) , Widget ID : " + identifier + " , Position : " + position);
                    SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, identifier);
                    SmartDashView.this.mRightAdapter.setExcludedPosition(position);
                    SmartDashView.this.mRightGaugeViewPager.updateState();
                }
            });
        }
        if (this.mRightAdapter == null) {
            this.mRightAdapter = new GaugeViewPagerAdapter(this.mSmartDashWidgetManager, false);
            this.mRightGaugeViewPager.setAdapter(this.mRightAdapter);
            this.mRightGaugeViewPager.setChangeListener(new ChangeListener() {
                public void onGaugeChanged(int position) {
                    String identifier = SmartDashView.this.mSmartDashWidgetManager.getWidgetIdentifierForIndex(position);
                    SmartDashView.sLogger.d("onGaugeChanged, (Right) , Widget ID : " + identifier + " , Position : " + position);
                    SmartDashView.this.savePreference(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, identifier);
                    SmartDashView.this.mLeftAdapter.setExcludedPosition(position);
                    SmartDashView.this.mLeftGaugeViewPager.updateState();
                }
            });
        }
        showViewsAccordingToUserPreferences();
    }

    private void showMiddleGaugeAccordingToUserPreferences() {
        this.middleGauge = (DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile() ? this.globalPreferences : this.homeScreenView.getDriverPreferences()).getInt(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, 1);
        sLogger.d("Middle Gauge : " + this.middleGauge);
        ObdManager.getInstance();
        switch (this.middleGauge) {
            case 0:
                showTachometer();
                return;
            case 1:
                showSpeedometerGauge();
                return;
            default:
                return;
        }
    }

    public boolean shouldShowTopViews() {
        return false;
    }

    public int getMiddleGaugeOption() {
        return this.middleGauge == 0 ? 1 : 0;
    }

    public void onTachoMeterSelected() {
        if (this.middleGauge != 0) {
            this.middleGauge = 0;
            savePreference(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, Integer.valueOf(0));
            showMiddleGaugeAccordingToUserPreferences();
        }
    }

    public void onSpeedoMeterSelected() {
        if (this.middleGauge != 1) {
            this.middleGauge = 1;
            savePreference(IDashboardOptionsAdapter.PREFERENCE_MIDDLE_GAUGE, Integer.valueOf(1));
            showMiddleGaugeAccordingToUserPreferences();
        }
    }

    public int getCurrentScrollableSideOption() {
        return this.scrollableSide == 0 ? 1 : 0;
    }

    public void onScrollableSideOptionSelected() {
        switch (this.scrollableSide) {
            case 0:
                this.scrollableSide = 1;
                savePreference(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, Integer.valueOf(1));
                return;
            case 1:
                this.scrollableSide = 0;
                savePreference(IDashboardOptionsAdapter.PREFERENCE_SCROLLABLE_SIDE, Integer.valueOf(0));
                return;
            default:
                return;
        }
    }

    private void savePreference(String key, Object value) {
        boolean isDefaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        SharedPreferences sharedPreferences = this.homeScreenView.getDriverPreferences();
        if (!isDefaultProfile) {
            if (value instanceof String) {
                sharedPreferences.edit().putString(key, (String) value).apply();
            } else if (value instanceof Integer) {
                sharedPreferences.edit().putInt(key, ((Integer) value).intValue()).apply();
            }
        }
        if (value instanceof String) {
            this.globalPreferences.edit().putString(key, (String) value).apply();
        } else if (value instanceof Integer) {
            this.globalPreferences.edit().putInt(key, ((Integer) value).intValue()).apply();
        }
    }

    public boolean onKey(CustomKeyEvent event) {
        GaugeViewPager gaugeViewPager = this.scrollableSide == 1 ? this.mRightGaugeViewPager : this.mLeftGaugeViewPager;
        switch (event) {
            case LEFT:
                gaugeViewPager.moveNext();
                break;
            case RIGHT:
                gaugeViewPager.movePrevious();
                break;
        }
        return false;
    }

    public void ObdPidChangeEvent(ObdPidChangeEvent event) {
        super.ObdPidChangeEvent(event);
        switch (event.pid.getId()) {
            case 5:
                this.mSmartDashWidgetManager.updateWidget(SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID, Double.valueOf(event.pid.getValue()));
                return;
            case 12:
                if (this.tachometerPresenter != null) {
                    this.tachometerPresenter.setRPM((int) event.pid.getValue());
                    return;
                }
                return;
            case 47:
                this.mSmartDashWidgetManager.updateWidget(SmartDashWidgetManager.FUEL_GAUGE_ID, Double.valueOf(event.pid.getValue()));
                return;
            case 256:
                this.mSmartDashWidgetManager.updateWidget(SmartDashWidgetManager.MPG_AVG_WIDGET_ID, Double.valueOf(event.pid.getValue()));
                return;
            default:
                return;
        }
    }

    public void updateLayoutForMode(NavigationMode navigationMode, boolean forced) {
        boolean navigationActive = this.homeScreenView.isNavigationActive();
        sLogger.v("updateLayoutForMode:" + navigationMode + " forced=" + forced + " nav:" + navigationActive);
        if (navigationActive) {
            sLogger.v("updateLayoutForMode: active");
            ((MarginLayoutParams) this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((MarginLayoutParams) this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.activeModeGaugeMargin;
            ((MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).topMargin = getResources().getDimensionPixelSize(R.dimen.tachometer_tbt_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(0.8f);
            this.mMiddleGaugeView.setScaleY(0.8f);
            this.mEtaLayout.setVisibility(0);
            invalidate();
            requestLayout();
        } else {
            sLogger.v("updateLayoutForMode: not active");
            ((MarginLayoutParams) this.mLeftGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((MarginLayoutParams) this.mRightGaugeViewPager.getLayoutParams()).topMargin = this.fullModeGaugeTopMargin;
            ((MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).topMargin = getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_margin_top);
            this.mMiddleGaugeView.setScaleX(1.0f);
            this.mMiddleGaugeView.setScaleY(1.0f);
            this.mEtaLayout.setVisibility(8);
            invalidate();
            requestLayout();
        }
        showEta(navigationActive);
        invalidate();
        requestLayout();
    }

    public void adjustDashHeight(int height) {
        super.adjustDashHeight(height);
    }

    protected void updateSpeed(int speed) {
        super.updateSpeed(speed);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeed(speed);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeed(speed);
        }
    }

    protected void updateSpeedLimit(int speedLimit) {
        super.updateSpeedLimit(speedLimit);
        if (this.tachometerPresenter != null) {
            this.tachometerPresenter.setSpeedLimit(speedLimit);
        }
        if (this.speedometerGaugePresenter2 != null) {
            this.speedometerGaugePresenter2.setSpeedLimit(speedLimit);
        }
        this.mSmartDashWidgetManager.updateWidget(SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID, Integer.valueOf(speedLimit));
    }

    protected void animateToFullMode() {
        super.animateToFullMode();
    }

    protected void animateToFullModeInternal(Builder builder) {
        super.animateToFullModeInternal(builder);
    }

    public void setView(CustomAnimationMode mode) {
        sLogger.v("setview: " + mode);
        super.setView(mode);
        switch (mode) {
            case EXPAND:
                this.mLeftGaugeViewPager.setVisibility(0);
                this.mRightGaugeViewPager.setVisibility(0);
                if (this.homeScreenView.isNavigationActive()) {
                    showEta(true);
                }
                ((MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).leftMargin = 0;
                return;
            case SHRINK_LEFT:
                this.mLeftGaugeViewPager.setVisibility(8);
                this.mRightGaugeViewPager.setVisibility(8);
                showEta(false);
                ((MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams()).leftMargin = getResources().getDimensionPixelSize(R.dimen.middle_gauge_shrink_margin);
                return;
            default:
                return;
        }
    }

    public void getCustomAnimator(CustomAnimationMode mode, Builder mainBuilder) {
        sLogger.v("getCustomAnimator" + mode);
        super.getCustomAnimator(mode, mainBuilder);
        AnimatorSet set = new AnimatorSet();
        final MarginLayoutParams marginParams;
        int srcMargin;
        ValueAnimator valueAnimator;
        switch (mode) {
            case EXPAND:
                marginParams = (MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams();
                srcMargin = marginParams.leftMargin;
                valueAnimator = new ValueAnimator();
                valueAnimator.setIntValues(new int[]{srcMargin, 0});
                valueAnimator.addUpdateListener(new AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        marginParams.leftMargin = ((Integer) animation.getAnimatedValue()).intValue();
                        SmartDashView.this.mMiddleGaugeView.setLayoutParams(marginParams);
                    }
                });
                valueAnimator.addListener(new DefaultAnimationListener() {
                    public void onAnimationEnd(Animator animation) {
                        SmartDashView.this.mLeftGaugeViewPager.setVisibility(0);
                        SmartDashView.this.mRightGaugeViewPager.setVisibility(0);
                        if (SmartDashView.this.homeScreenView.isNavigationActive()) {
                            SmartDashView.this.showEta(true);
                        }
                    }
                });
                set.playTogether(new Animator[]{valueAnimator});
                break;
            case SHRINK_LEFT:
            case SHRINK_MODE:
                int targetMargin;
                this.mLeftGaugeViewPager.setVisibility(8);
                this.mRightGaugeViewPager.setVisibility(8);
                showEta(false);
                marginParams = (MarginLayoutParams) this.mMiddleGaugeView.getLayoutParams();
                srcMargin = marginParams.leftMargin;
                if (mode == CustomAnimationMode.SHRINK_LEFT) {
                    targetMargin = this.middleGaugeShrinkMargin;
                } else {
                    targetMargin = this.middleGaugeShrinkModeMargin;
                }
                valueAnimator = new ValueAnimator();
                valueAnimator.setIntValues(new int[]{srcMargin, targetMargin});
                valueAnimator.addUpdateListener(new AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        marginParams.leftMargin = ((Integer) animation.getAnimatedValue()).intValue();
                        SmartDashView.this.mMiddleGaugeView.setLayoutParams(marginParams);
                    }
                });
                set.playTogether(new Animator[]{valueAnimator});
                break;
        }
        mainBuilder.with(set);
    }

    private void showEta(boolean show) {
        if (this.mEtaLayout.getVisibility() == 0) {
            this.mEtaLayout.setVisibility(8);
        }
    }

    public void onPause() {
        if (!this.paused) {
            sLogger.v("::onPause");
            this.paused = true;
            if (this.mSmartDashWidgetManager != null) {
                sLogger.v("widgets disabled");
                this.mSmartDashWidgetManager.onPause();
                if (this.tachometerPresenter != null) {
                    this.tachometerPresenter.onPause();
                }
                if (this.speedometerGaugePresenter2 != null) {
                    this.speedometerGaugePresenter2.onPause();
                }
            }
        }
    }

    public void onResume() {
        if (this.paused) {
            sLogger.v("::onResume");
            this.paused = false;
            if (this.mSmartDashWidgetManager != null) {
                sLogger.v("widgets enabled");
                this.mSmartDashWidgetManager.onResume();
                if (this.tachometerPresenter != null && this.tachometerPresenter.isWidgetActive()) {
                    this.tachometerPresenter.onResume();
                }
                if (this.speedometerGaugePresenter2 != null && this.speedometerGaugePresenter2.isWidgetActive()) {
                    this.speedometerGaugePresenter2.onResume();
                }
            }
            recordUsersWidgetPreference();
        }
    }

    public void recordUsersWidgetPreference() {
        sLogger.d("Recording the users Widget Preference");
        long currentTime = SystemClock.elapsedRealtime();
        if (!DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            if (this.lastUserPreferenceRecordedTime <= 0 || currentTime - this.lastUserPreferenceRecordedTime >= MINIMUM_INTERVAL_BETWEEN_RECORDING_USER_PREFERENCE) {
                SharedPreferences sharedPreferences = this.homeScreenView.getDriverPreferences();
                String leftGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_LEFT_GAUGE_ID, SmartDashWidgetManager.ANALOG_CLOCK_WIDGET_ID);
                String rightGaugeId = sharedPreferences.getString(IDashboardOptionsAdapter.PREFERENCE_RIGHT_GAUGE_ID, SmartDashWidgetManager.COMPASS_WIDGET_ID);
                AnalyticsSupport.recordDashPreference(leftGaugeId, true);
                AnalyticsSupport.recordDashPreference(rightGaugeId, false);
                this.lastUserPreferenceRecordedTime = currentTime;
            }
        }
    }

    public String getActiveleftGaugeId() {
        return this.activeleftGaugeId;
    }

    public String getActiveRightGaugeId() {
        return this.activeRightGaugeId;
    }
}
