package com.navdy.hud.app.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.widget.RelativeLayout;
import com.navdy.hud.app.R;
import java.util.ArrayList;
import java.util.List;

public class FullScreenNotificationLayout extends RelativeLayout {
    private static final long ANIM_DURATION = 800;
    private boolean autoPopup;
    private List<View> bodyViews = new ArrayList();
    private float directionalTranslation;
    private View footer;
    private int footerId;
    private View title;
    private int titleId;

    private enum AnimationDirection {
        UP,
        DOWN
    }

    public FullScreenNotificationLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        float translationY = getResources().getDimension(R.dimen.text_fade_anim_translationy);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FullScreenNotificationLayout);
        if (ta != null) {
            try {
                this.titleId = ta.getResourceId(0, 0);
                this.footerId = ta.getResourceId(1, 0);
                if (AnimationDirection.values()[ta.getInt(2, 0)] != AnimationDirection.UP) {
                    translationY = -translationY;
                }
                this.directionalTranslation = translationY;
                this.autoPopup = ta.getBoolean(3, true);
            } finally {
                ta.recycle();
            }
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        initialize();
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.autoPopup) {
            animateIn();
        }
    }

    private void initialize() {
        this.title = findViewById(this.titleId);
        this.footer = findViewById(this.footerId);
        this.title.setAlpha(0.0f);
        this.footer.setAlpha(0.0f);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = getChildAt(i);
            if (!(view.getId() == this.titleId || view.getId() == this.footerId)) {
                view.setAlpha(0.0f);
                view.setTranslationY(this.directionalTranslation);
                this.bodyViews.add(view);
            }
        }
    }

    public void animateIn() {
        animateIn(null);
    }

    public void animateIn(final Runnable endAction) {
        boolean secondStageTriggered = false;
        for (View v : this.bodyViews) {
            if (secondStageTriggered) {
                getInBodyAnimation(v);
            } else {
                secondStageTriggered = true;
                if (this.title != null || this.footer != null) {
                    getInBodyAnimation(v).withEndAction(new Runnable() {
                        public void run() {
                            FullScreenNotificationLayout.this.animateInSecondStage(endAction);
                        }
                    });
                } else if (endAction != null) {
                    getInBodyAnimation(v).withEndAction(endAction);
                } else {
                    getInBodyAnimation(v);
                }
            }
        }
        if (!secondStageTriggered) {
            animateInSecondStage(endAction);
        }
    }

    private void animateInSecondStage(Runnable endAction) {
        if (this.title != null) {
            if (endAction != null) {
                getInTitleFooterAnimation(this.title).withEndAction(endAction);
            } else {
                getInTitleFooterAnimation(this.title);
            }
        }
        if (this.footer == null) {
            return;
        }
        if (this.title != null) {
            getInTitleFooterAnimation(this.footer);
        } else if (endAction != null) {
            getInTitleFooterAnimation(this.footer).withEndAction(endAction);
        } else {
            getInTitleFooterAnimation(this.footer);
        }
    }

    public void animateOut(final Runnable endAction) {
        if (this.title == null && this.footer == null) {
            animateOutSecondStage(endAction);
            return;
        }
        if (this.title != null) {
            getOutTitleFooterAnimation(this.title).withEndAction(new Runnable() {
                public void run() {
                    FullScreenNotificationLayout.this.animateOutSecondStage(endAction);
                }
            });
        }
        if (this.footer == null) {
            return;
        }
        if (this.title == null) {
            getOutTitleFooterAnimation(this.footer).withEndAction(new Runnable() {
                public void run() {
                    FullScreenNotificationLayout.this.animateOutSecondStage(endAction);
                }
            });
        } else {
            getOutTitleFooterAnimation(this.footer);
        }
    }

    private void animateOutSecondStage(Runnable endAction) {
        boolean secondStageTriggered = false;
        for (View bodyView : this.bodyViews) {
            if (secondStageTriggered) {
                getOutBodyAnimation(bodyView);
            } else {
                secondStageTriggered = true;
                if (endAction != null) {
                    getOutBodyAnimation(bodyView).withEndAction(endAction);
                } else {
                    getOutBodyAnimation(bodyView);
                }
            }
        }
    }

    private ViewPropertyAnimator getInBodyAnimation(View v) {
        return v.animate().alpha(1.0f).translationY(0.0f).setDuration(ANIM_DURATION);
    }

    private ViewPropertyAnimator getOutBodyAnimation(View v) {
        return v.animate().alpha(0.0f).translationY(this.directionalTranslation).setDuration(ANIM_DURATION);
    }

    private ViewPropertyAnimator getInTitleFooterAnimation(View v) {
        return v.animate().alpha(1.0f).setDuration(ANIM_DURATION);
    }

    private ViewPropertyAnimator getOutTitleFooterAnimation(View v) {
        return v.animate().alpha(0.0f).setDuration(ANIM_DURATION);
    }
}
