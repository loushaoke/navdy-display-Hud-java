package com.navdy.hud.app.ui.component;

import android.widget.ImageView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class SystemTrayView$$ViewInjector {
    public static void inject(Finder finder, SystemTrayView target, Object source) {
        target.dialImageView = (ImageView) finder.findRequiredView(source, R.id.dial, "field 'dialImageView'");
        target.phoneImageView = (ImageView) finder.findRequiredView(source, R.id.phone, "field 'phoneImageView'");
        target.phoneNetworkImageView = (ImageView) finder.findRequiredView(source, R.id.phoneNetwork, "field 'phoneNetworkImageView'");
        target.locationImageView = (ImageView) finder.findRequiredView(source, R.id.gps, "field 'locationImageView'");
    }

    public static void reset(SystemTrayView target) {
        target.dialImageView = null;
        target.phoneImageView = null;
        target.phoneNetworkImageView = null;
        target.locationImageView = null;
    }
}
