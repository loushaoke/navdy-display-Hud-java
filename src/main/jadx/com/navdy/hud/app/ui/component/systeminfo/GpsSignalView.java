package com.navdy.hud.app.ui.component.systeminfo;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.service.library.log.Logger;

public class GpsSignalView extends LinearLayout {
    private Logger logger = new Logger(GpsSignalView.class);

    public static class SatelliteData implements Comparable<SatelliteData> {
        int cNo;
        String provider;
        int satelliteId;

        public SatelliteData(int satelliteId, int cNo, String provider) {
            this.satelliteId = satelliteId;
            this.cNo = cNo;
            this.provider = provider;
        }

        public int compareTo(SatelliteData another) {
            return another.cNo - this.cNo;
        }
    }

    public GpsSignalView(Context context) {
        super(context);
    }

    public GpsSignalView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GpsSignalView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOrientation(0);
    }

    public void setSatelliteData(SatelliteData[] satelliteData) {
        removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        Resources resources = HudApplication.getAppContext().getResources();
        int separator = resources.getDimensionPixelSize(R.dimen.satellite_width_separator);
        int width = resources.getDimensionPixelSize(R.dimen.satellite_width);
        for (int i = 0; i < satelliteData.length; i++) {
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.screen_home_system_info_gps_satellite, null);
            layout.setLayoutParams(new LayoutParams(width, -1));
            ((TextView) layout.findViewById(R.id.satellite_cno)).setText(String.valueOf(satelliteData[i].cNo));
            ((TextView) layout.findViewById(R.id.satellite_type)).setText(satelliteData[i].provider);
            ((TextView) layout.findViewById(R.id.satellite_id)).setText(String.valueOf(satelliteData[i].satelliteId));
            ((RelativeLayout.LayoutParams) layout.findViewById(R.id.satellite_strength).getLayoutParams()).height = satelliteData[i].cNo * 2;
            addView(layout);
            if (i < satelliteData.length - 1) {
                View v = new View(getContext());
                v.setLayoutParams(new LayoutParams(separator, separator));
                addView(v);
            }
        }
        this.logger.v("set satellite =" + satelliteData.length);
    }
}
