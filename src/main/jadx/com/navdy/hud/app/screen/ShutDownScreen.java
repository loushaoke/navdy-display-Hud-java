package com.navdy.hud.app.screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.event.Shutdown.State;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ConnectionType;
import com.navdy.hud.app.service.ShutdownMonitor;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.view.ShutDownConfirmationView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import flow.Layout;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.shutdown_confirmation_lyt)
public class ShutDownScreen extends BaseScreen {
    private static final Logger sLogger = new Logger(ShutDownScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {ShutDownConfirmationView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<ShutDownConfirmationView> {
        @Inject
        GestureServiceConnector gestureServiceConnector;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;
        ObdManager obdManager;
        Reason shutDownCause;
        @Inject
        UIStateManager uiStateManager;

        public void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            this.obdManager = ObdManager.getInstance();
            this.mBus.register(this);
            if (savedInstanceState != null) {
                try {
                    String reason = savedInstanceState.getString(Shutdown.EXTRA_SHUTDOWN_CAUSE);
                    if (reason != null) {
                        this.shutDownCause = Reason.valueOf(reason);
                    }
                } catch (Exception e) {
                    ShutDownScreen.sLogger.e("failed to parse shutdown reason " + e.getMessage());
                    if (this.shutDownCause == null) {
                        ShutDownScreen.sLogger.e("defaulting to inactivity reason");
                        this.shutDownCause = Reason.INACTIVITY;
                    }
                } catch (Throwable th) {
                    if (this.shutDownCause == null) {
                        ShutDownScreen.sLogger.e("defaulting to inactivity reason");
                        this.shutDownCause = Reason.INACTIVITY;
                    }
                }
            }
            if (this.shutDownCause == null) {
                ShutDownScreen.sLogger.e("defaulting to inactivity reason");
                this.shutDownCause = Reason.INACTIVITY;
            }
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            NotificationManager.getInstance().enableNotifications(false);
            ShutdownMonitor.getInstance().disableInactivityShutdown(true);
        }

        protected void onUnload() {
            ShutDownScreen.sLogger.v("onUnload");
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            NotificationManager.getInstance().enableNotifications(true);
            ShutdownMonitor.getInstance().disableInactivityShutdown(false);
            this.mBus.unregister(this);
            super.onUnload();
        }

        public void installAndShutDown() {
            String command = OTAUpdateService.COMMAND_INSTALL_UPDATE_SHUTDOWN;
            if (this.obdManager.getConnectionType() == ConnectionType.OBD && this.obdManager.getBatteryVoltage() < 13.100000381469727d) {
                command = OTAUpdateService.COMMAND_INSTALL_UPDATE_REBOOT_QUIET;
            }
            Intent intent = new Intent(HudApplication.getAppContext(), OTAUpdateService.class);
            intent.putExtra("COMMAND", command);
            HudApplication.getAppContext().startService(intent);
        }

        public void shutDown() {
            Shutdown event = new Shutdown(this.shutDownCause, State.CONFIRMED);
            ShutDownScreen.sLogger.i("Posting " + event);
            this.mBus.post(event);
        }

        public String getUpdateVersion() {
            return OTAUpdateService.getSWUpdateVersion();
        }

        public String getCurrentVersion() {
            return OTAUpdateService.getCurrentVersion();
        }

        @Subscribe
        public void onDriving(DrivingStateChange event) {
            if (event.driving && getShutDownCause() == Reason.INACTIVITY) {
                finish();
            }
        }

        public void finish() {
            this.mBus.post(new Builder().screen(Screen.SCREEN_BACK).build());
            Shutdown event = new Shutdown(this.shutDownCause, State.CANCELED);
            ShutDownScreen.sLogger.i("Posting " + event);
            this.mBus.post(event);
        }

        public boolean isSoftwareUpdatePending() {
            return OTAUpdateService.isUpdateAvailable();
        }

        public boolean isDialFirmwareUpdatePending() {
            DialManager dialManager = DialManager.getInstance();
            return dialManager.isDialConnected() && dialManager.getDialFirmwareUpdater().getVersions().isUpdateAvailable();
        }

        public void installDialUpdateAndShutDown() {
            Bundle args = new Bundle();
            args.putInt(DialUpdateProgressScreen.EXTRA_PROGRESS_CAUSE, 2);
            this.mBus.post(new ShowScreenWithArgs(Screen.SCREEN_DIAL_UPDATE_PROGRESS, args, false));
        }

        public Reason getShutDownCause() {
            return this.shutDownCause;
        }
    }

    public String getMortarScopeName() {
        return ShutDownScreen.class.getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }

    public Screen getScreen() {
        return Screen.SCREEN_SHUTDOWN_CONFIRMATION;
    }
}
