package com.navdy.hud.app.screen;

import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.manager.PairingManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.screen.WelcomeScreen.Presenter;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class WelcomeScreen$Presenter$$InjectAdapter extends Binding<Presenter> implements Provider<Presenter>, MembersInjector<Presenter> {
    private Binding<Bus> bus;
    private Binding<ConnectionHandler> connectionHandler;
    private Binding<GestureServiceConnector> gestureServiceConnector;
    private Binding<PairingManager> pairingManager;
    private Binding<DriverProfileManager> profileManager;
    private Binding<BasePresenter> supertype;
    private Binding<UIStateManager> uiStateManager;

    public WelcomeScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.WelcomeScreen$Presenter", "members/com.navdy.hud.app.screen.WelcomeScreen$Presenter", true, Presenter.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", Presenter.class, getClass().getClassLoader());
        this.profileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", Presenter.class, getClass().getClassLoader());
        this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", Presenter.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", Presenter.class, getClass().getClassLoader());
        this.pairingManager = linker.requestBinding("com.navdy.hud.app.manager.PairingManager", Presenter.class, getClass().getClassLoader());
        this.gestureServiceConnector = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", Presenter.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.profileManager);
        injectMembersBindings.add(this.connectionHandler);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.pairingManager);
        injectMembersBindings.add(this.gestureServiceConnector);
        injectMembersBindings.add(this.supertype);
    }

    public Presenter get() {
        Presenter result = new Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(Presenter object) {
        object.bus = (Bus) this.bus.get();
        object.profileManager = (DriverProfileManager) this.profileManager.get();
        object.connectionHandler = (ConnectionHandler) this.connectionHandler.get();
        object.uiStateManager = (UIStateManager) this.uiStateManager.get();
        object.pairingManager = (PairingManager) this.pairingManager.get();
        object.gestureServiceConnector = (GestureServiceConnector) this.gestureServiceConnector.get();
        this.supertype.injectMembers(object);
    }
}
