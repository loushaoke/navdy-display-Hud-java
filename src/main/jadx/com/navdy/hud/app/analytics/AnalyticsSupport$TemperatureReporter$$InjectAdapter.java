package com.navdy.hud.app.analytics;

import com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter;
import com.navdy.hud.app.device.PowerManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class AnalyticsSupport$TemperatureReporter$$InjectAdapter extends Binding<TemperatureReporter> implements Provider<TemperatureReporter>, MembersInjector<TemperatureReporter> {
    private Binding<PowerManager> powerManager;

    public AnalyticsSupport$TemperatureReporter$$InjectAdapter() {
        super("com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter", "members/com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter", false, TemperatureReporter.class);
    }

    public void attach(Linker linker) {
        this.powerManager = linker.requestBinding("com.navdy.hud.app.device.PowerManager", TemperatureReporter.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.powerManager);
    }

    public TemperatureReporter get() {
        TemperatureReporter result = new TemperatureReporter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(TemperatureReporter object) {
        object.powerManager = (PowerManager) this.powerManager.get();
    }
}
