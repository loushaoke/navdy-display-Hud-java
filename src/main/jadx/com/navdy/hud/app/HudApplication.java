package com.navdy.hud.app;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.os.SystemClock;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import com.here.android.mpa.common.ViewRect;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.analytics.AnalyticsSupport.AnalyticsIntentsReceiver;
import com.navdy.hud.app.audio.SoundUtils;
import com.navdy.hud.app.bluetooth.pbap.PBAPClientManager;
import com.navdy.hud.app.common.ProdModule;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.ProjectorBrightness;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsDeadReckoningManager;
import com.navdy.hud.app.device.gps.GpsManager;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.event.Shutdown.State;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.FavoriteContactsManager;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glance.GlanceHandler;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.glympse.GlympseManager;
import com.navdy.hud.app.framework.message.MessageManager;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.maps.GpsEventsReceiver;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.profile.HudLocale;
import com.navdy.hud.app.receiver.LogLevelReceiver;
import com.navdy.hud.app.service.FileTransferHandler;
import com.navdy.hud.app.service.ShutdownMonitor;
import com.navdy.hud.app.service.StickyService;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.storage.db.HudDatabase;
import com.navdy.hud.app.ui.activity.MainActivity;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues;
import com.navdy.hud.app.ui.component.homescreen.SmartDashViewResourceValues;
import com.navdy.hud.app.util.CrashReporter;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.NavdyNativeLibWrapper;
import com.navdy.hud.app.util.OTAUpdateService.OTADownloadIntentsReceiver;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.util.os.CpuProfiler;
import com.navdy.hud.app.util.os.PropsFileUpdater;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.log.LogAppender;
import com.navdy.service.library.log.LogcatAppender;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.SystemUtils;
import com.squareup.otto.Bus;
import dagger.ObjectGraph;
import java.util.Locale;
import javax.inject.Inject;
import mortar.Mortar;
import mortar.MortarScope;

public class HudApplication extends MultiDexApplication {
    public static final String NOT_A_CRASH = "NAVDY_NOT_A_CRASH";
    private static final String TAG = "HudApplication";
    private static Context sAppContext;
    private static HudApplication sApplication;
    private static final Logger sLogger = new Logger(HudApplication.class);
    @Inject
    public Bus bus;
    private Handler handler = new Handler(Looper.getMainLooper());
    private boolean initialized;
    private Reason lastSeenReason = Reason.UNKNOWN;
    private MortarScope rootScope;
    private UpdateReminderManager updateReminderManager;

    public static void setContext(Context context) {
        sAppContext = context;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(HudLocale.onAttach(base));
    }

    public void onCreate() {
        Log.e("", "::onCreate locale:" + getResources().getConfiguration().locale);
        sApplication = this;
        sAppContext = sApplication;
        super.onCreate();
        String processName = SystemUtils.getProcessName(sAppContext, Process.myPid());
        String packageName = getPackageName();
        initLogger(false);
        sLogger.d(processName + " starting  on " + Build.BRAND + HereManeuverDisplayBuilder.COMMA + Build.HARDWARE + HereManeuverDisplayBuilder.COMMA + Build.MODEL);
        if (processName.equalsIgnoreCase(packageName + ":connectionService")) {
            sLogger.v("startup:" + processName + " :connection service");
            initConnectionService(processName);
        } else if (processName.equalsIgnoreCase(packageName)) {
            sLogger.v("startup:" + processName + " :hud app locale=" + Locale.getDefault().toString());
            sLogger.v(processName + ":initializing Mortar");
            long l1 = SystemClock.elapsedRealtime();
            this.rootScope = Mortar.createRootScope(false, ObjectGraph.create(new ProdModule(this)));
            this.rootScope.getObjectGraph().inject(this);
            sLogger.v(processName + ":mortar init took:" + (SystemClock.elapsedRealtime() - l1));
            initTaskManager(processName);
            sLogger.i("init network state manager");
            NetworkStateManager.getInstance();
            sLogger.i("*** starting hud sticky-service ***");
            Intent intent = new Intent();
            intent.setClass(this, StickyService.class);
            startService(intent);
            sLogger.i("*** started hud sticky-service ***");
            IntentFilter filter = new IntentFilter();
            filter.addAction(GpsConstants.GPS_EVENT_SWITCH);
            filter.addAction(GpsConstants.GPS_EVENT_WARM_RESET_UBLOX);
            filter.addAction(GpsConstants.GPS_SATELLITE_STATUS);
            filter.addAction(GpsConstants.GPS_COLLECT_LOGS);
            filter.addAction(GpsConstants.GPS_EVENT_DRIVING_STARTED);
            filter.addAction(GpsConstants.GPS_EVENT_DRIVING_STOPPED);
            filter.addAction(GpsConstants.GPS_EVENT_ENABLE_ESF_RAW);
            registerReceiver(new GpsEventsReceiver(), filter);
            sLogger.v("registered GpsEventsReceiver");
            IntentFilter analyticsFilter = new IntentFilter();
            analyticsFilter.addAction(AnalyticsSupport.ANALYTICS_INTENT);
            registerReceiver(new AnalyticsIntentsReceiver(), analyticsFilter);
            sLogger.v("registered AnalyticsIntentsReceiver");
            Intent myIntent = new Intent(this, MainActivity.class);
            myIntent.addFlags(268435456);
            startActivity(myIntent);
        } else {
            sLogger.v("startup:" + processName + " :no-op");
        }
        sLogger.v("startup:" + processName + " :initialization done");
    }

    public MortarScope getRootScope() {
        return this.rootScope;
    }

    public Object getSystemService(String name) {
        if (Mortar.isScopeSystemService(name)) {
            return this.rootScope;
        }
        return super.getSystemService(name);
    }

    public static Context getAppContext() {
        return sAppContext;
    }

    public static HudApplication getApplication() {
        return sApplication;
    }

    private static void initLogger(boolean initializingApp) {
        Logger.init(new LogAppender[]{new LogcatAppender()});
    }

    private void initTaskManager(String processName) {
        sLogger.v(processName + ":initializing taskMgr");
        TaskManager taskManager = TaskManager.getInstance();
        try {
            taskManager.addTaskQueue(1, 3);
            taskManager.addTaskQueue(5, 1);
            taskManager.addTaskQueue(8, 1);
            taskManager.addTaskQueue(6, 1);
            taskManager.addTaskQueue(7, 1);
            taskManager.addTaskQueue(9, 1);
            taskManager.addTaskQueue(10, 1);
            taskManager.addTaskQueue(11, 1);
            taskManager.addTaskQueue(12, 1);
            taskManager.addTaskQueue(13, 1);
            taskManager.addTaskQueue(14, 1);
            taskManager.addTaskQueue(22, 1);
            taskManager.addTaskQueue(15, 1);
            taskManager.addTaskQueue(16, 1);
            taskManager.addTaskQueue(17, 1);
            taskManager.addTaskQueue(18, 1);
            taskManager.addTaskQueue(2, 5);
            taskManager.addTaskQueue(3, 1);
            taskManager.addTaskQueue(4, 1);
            taskManager.addTaskQueue(19, 1);
            taskManager.addTaskQueue(20, 1);
            taskManager.addTaskQueue(21, 1);
            taskManager.addTaskQueue(23, 1);
            taskManager.init();
        } catch (IllegalStateException e) {
            if (!e.getMessage().equals("already initialized")) {
                throw e;
            }
        }
    }

    private void initConnectionService(String processName) {
        sLogger.v(processName + ":initializing taskMgr");
        TaskManager taskManager = TaskManager.getInstance();
        taskManager.addTaskQueue(1, 3);
        taskManager.addTaskQueue(5, 1);
        taskManager.addTaskQueue(9, 1);
        taskManager.init();
        sLogger.v(processName + ":initializing gps manager");
        GpsManager.getInstance();
        registerReceiver(new LogLevelReceiver(), new IntentFilter(Logger.ACTION_RELOAD));
    }

    public void initHudApp() {
        if (!this.initialized) {
            this.initialized = true;
            String processName = SystemUtils.getProcessName(sAppContext, Process.myPid());
            NavdyNativeLibWrapper.loadlibrary();
            if (CrashReporter.isEnabled()) {
                sLogger.v(processName + ":initializing crash reporter");
                CrashReporter.getInstance().installCrashHandler(sAppContext);
            } else {
                sLogger.v(processName + ":crash reporter not installed");
            }
            if (DeviceUtil.isNavdyDevice()) {
                PropsFileUpdater.run();
                ProjectorBrightness.init();
            }
            sLogger.v(processName + ":updating logging setup");
            initLogger(true);
            IntentFilter otaFilter = new IntentFilter();
            otaFilter.addAction(FileTransferHandler.ACTION_OTA_DOWNLOAD);
            SharedPreferences prefs = RemoteDeviceManager.getInstance().getSharedPreferences();
            if (prefs == null) {
                sLogger.e("unable to get SharedPreferences");
            } else {
                registerReceiver(new OTADownloadIntentsReceiver(prefs), otaFilter);
            }
            PicassoUtil.initPicasso(sAppContext);
            RemoteDeviceManager.getInstance().getSharedPreferences().edit();
            long l1;
            try {
                sLogger.v("dbase:opening...");
                l1 = SystemClock.elapsedRealtime();
                HudDatabase.getInstance().getWritableDatabase();
                sLogger.v("dbase: opened[" + (SystemClock.elapsedRealtime() - l1) + "]");
            } catch (Throwable th) {
                sLogger.e("dbase: error opening", t);
            }
            AnalyticsSupport.analyticsApplicationInit(this);
            final DriveRecorder dataRecorder = (DriveRecorder) this.rootScope.getObjectGraph().get(DriveRecorder.class);
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    dataRecorder.load();
                }
            }, 1);
            sLogger.v(processName + ":initializing here maps engine");
            HereMapsManager.getInstance();
            sLogger.v(processName + ":called initialized");
            String s = HomeScreenResourceValues.speedMph;
            ViewRect rect = HomeScreenConstants.routeOverviewRect;
            int n = SmartDashViewResourceValues.middleGaugeShrinkLeftX;
            s = TTSUtils.TTS_DIAL_BATTERY_EXTREMELY_LOW;
            n = GlanceConstants.colorFacebook;
            GlanceHelper.initMessageAttributes();
            if (CrashReporter.isEnabled()) {
                this.handler.post(new Runnable() {
                    public void run() {
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                CrashReporter.getInstance().checkForKernelCrashes(PathManager.getInstance().getKernelCrashFiles());
                            }
                        }, 1);
                    }
                });
                this.handler.postDelayed(new Runnable() {
                    public void run() {
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                CrashReporter crashReporter = CrashReporter.getInstance();
                                crashReporter.checkForAnr();
                                crashReporter.checkForTombstones();
                                crashReporter.checkForOTAFailure();
                            }
                        }, 1);
                    }
                }, 45000);
            }
            DialManager.getInstance();
            RecentCallManager.getInstance();
            PBAPClientManager.getInstance();
            ContactImageHelper.getInstance();
            FavoriteContactsManager.getInstance();
            DestinationsManager.getInstance();
            MessageManager.getInstance();
            GlanceHandler.getInstance();
            ReportIssueService.initialize();
            ShutdownMonitor.getInstance();
            if (!DeviceUtil.isNavdyDevice()) {
                SoundUtils.init();
            }
            RemoteDeviceManager.getInstance().getFeatureUtil();
            if (DeviceUtil.isNavdyDevice()) {
                sLogger.v("start dead reckoning mgr");
                GpsDeadReckoningManager.getInstance();
            }
            this.updateReminderManager = new UpdateReminderManager(this);
            sLogger.i("init network b/w controller");
            NetworkBandwidthController.getInstance();
            CpuProfiler.getInstance().start();
            this.handler.post(new Runnable() {
                public void run() {
                    long initialTime = SystemClock.elapsedRealtime();
                    GlympseManager.getInstance();
                    HudApplication.sLogger.v("time taken by Glympse init: " + (SystemClock.elapsedRealtime() - initialTime) + " ms");
                }
            });
            sLogger.v(processName + ":background init done");
        }
    }

    public static boolean isDeveloperBuild() {
        return false;
    }

    public Bus getBus() {
        return this.bus;
    }

    public void setShutdownReason(Reason reason) {
        this.lastSeenReason = reason;
    }

    public void shutdown() {
        sLogger.i("shutting down HUD");
        CrashReporter.getInstance().stopCrashReporting(true);
        Intent intent = new Intent();
        intent.setClass(this, StickyService.class);
        stopService(intent);
        if (this.bus != null) {
            sLogger.i("shutting down HUD - reason: " + this.lastSeenReason);
            this.bus.post(new Shutdown(this.lastSeenReason, State.SHUTTING_DOWN));
        }
        HereMapsManager.getInstance().getLocationFixManager().shutdown();
    }
}
