package com.navdy.hud.app.bluetooth.vcard;

public interface VCardPhoneNumberTranslationCallback {
    String onValueReceived(String str, int i, String str2, boolean z);
}
