package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardVersionException extends VCardException {
    public VCardVersionException(String message) {
        super(message);
    }
}
