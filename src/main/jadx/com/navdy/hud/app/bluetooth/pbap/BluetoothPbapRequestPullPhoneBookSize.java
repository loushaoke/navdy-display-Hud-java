package com.navdy.hud.app.bluetooth.pbap;

import android.util.Log;
import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters;

class BluetoothPbapRequestPullPhoneBookSize extends BluetoothPbapRequest {
    private static final String TAG = "BTPbapReqPullPBookSize";
    private static final String TYPE = "x-bt/phonebook";
    private int mSize;

    public BluetoothPbapRequestPullPhoneBookSize(String pbName) {
        this.mHeaderSet.setHeader(1, pbName);
        this.mHeaderSet.setHeader(66, TYPE);
        ObexAppParameters oap = new ObexAppParameters();
        oap.add((byte) 4, (short) 0);
        oap.addToHeaderSet(this.mHeaderSet);
    }

    protected void readResponseHeaders(HeaderSet headerset) {
        Log.v(TAG, "readResponseHeaders");
        this.mSize = ObexAppParameters.fromHeaderSet(headerset).getShort((byte) 8);
    }

    public int getSize() {
        return this.mSize;
    }
}
