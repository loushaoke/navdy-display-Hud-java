package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.GestureLearningScreen.Presenter;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class GestureLearningView$$InjectAdapter extends Binding<GestureLearningView> implements MembersInjector<GestureLearningView> {
    private Binding<Bus> mBus;
    private Binding<Presenter> mPresenter;

    public GestureLearningView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.GestureLearningView", false, GestureLearningView.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.GestureLearningScreen$Presenter", GestureLearningView.class, getClass().getClassLoader());
        this.mBus = linker.requestBinding("com.squareup.otto.Bus", GestureLearningView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
        injectMembersBindings.add(this.mBus);
    }

    public void injectMembers(GestureLearningView object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
        object.mBus = (Bus) this.mBus.get();
    }
}
