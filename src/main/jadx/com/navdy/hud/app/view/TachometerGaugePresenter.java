package com.navdy.hud.app.view;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.util.TypedValue;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.view.drawable.TachometerGaugeDrawable;

public class TachometerGaugePresenter extends GaugeViewPresenter implements SerialValueAnimatorAdapter {
    public static final String ID = "TACHOMETER_GAUGE_IDENTIFIER";
    private static final int MAX_MARKER_ANIMATION_DELAY = 500;
    private static final int MAX_MARKER_ANIMATION_DURATION = 1000;
    private static final int MAX_RPM_VALUE = 8000;
    private static final int RPM_DIFFERENCE = 100;
    private static final int RPM_WARNING_THRESHOLD = 7000;
    public static final int VALUE_ANIMATION_DURATION = 200;
    int animatedRpm;
    private Runnable animationRunnable;
    private boolean fullMode;
    private Handler handler = new Handler();
    private final String kmhString;
    private int lastRPM = Integer.MIN_VALUE;
    TachometerGaugeDrawable mTachometerGaugeDrawable;
    private int maxMarkerAlpha;
    private int maxRpmValue;
    private final String mphString;
    int rpm;
    private SerialValueAnimator serialValueAnimator = new SerialValueAnimator(this, 200);
    private boolean showingMaxMarkerAnimation;
    int speed;
    private int speedFastWarningColor;
    int speedLimit;
    private SpeedManager speedManager = SpeedManager.getInstance();
    private int speedSafeColor;
    private int speedVeryFastWarningColor;
    private ValueAnimator valueAnimator;
    private String widgetName;

    public TachometerGaugePresenter(Context context) {
        this.mTachometerGaugeDrawable = new TachometerGaugeDrawable(context, 0);
        this.mTachometerGaugeDrawable.setMaxGaugeValue(8000.0f);
        this.mTachometerGaugeDrawable.setRPMWarningLevel(RPM_WARNING_THRESHOLD);
        this.speedSafeColor = context.getResources().getColor(17170443);
        this.speedFastWarningColor = context.getResources().getColor(R.color.cyan);
        this.speedVeryFastWarningColor = context.getResources().getColor(R.color.speed_very_fast_warning_color);
        this.widgetName = context.getResources().getString(R.string.gauge_tacho_meter);
        Resources resources = context.getResources();
        this.mphString = resources.getString(R.string.mph);
        this.kmhString = resources.getString(R.string.kilometers_per_hour);
        this.animationRunnable = new Runnable() {
            public void run() {
                TachometerGaugePresenter.this.valueAnimator = new ValueAnimator();
                TachometerGaugePresenter.this.valueAnimator.setIntValues(new int[]{255, 0});
                TachometerGaugePresenter.this.valueAnimator.setDuration(1000);
                TachometerGaugePresenter.this.valueAnimator.addUpdateListener(new AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator animation) {
                        TachometerGaugePresenter.this.maxMarkerAlpha = ((Integer) animation.getAnimatedValue()).intValue();
                        TachometerGaugePresenter.this.reDraw();
                    }
                });
                TachometerGaugePresenter.this.valueAnimator.addListener(new AnimatorListener() {
                    public void onAnimationStart(Animator animation) {
                    }

                    public void onAnimationEnd(Animator animation) {
                        TachometerGaugePresenter.this.maxMarkerAlpha = 0;
                        TachometerGaugePresenter.this.showingMaxMarkerAnimation = false;
                        TachometerGaugePresenter.this.maxRpmValue = 0;
                        TachometerGaugePresenter.this.reDraw();
                    }

                    public void onAnimationCancel(Animator animation) {
                    }

                    public void onAnimationRepeat(Animator animation) {
                    }
                });
                TachometerGaugePresenter.this.valueAnimator.start();
            }
        };
    }

    public void setRPM(int rpm) {
        if (this.isDashActive && this.lastRPM != rpm && Math.abs(this.lastRPM - rpm) >= 100) {
            this.lastRPM = rpm;
            this.serialValueAnimator.setValue((float) rpm);
        }
    }

    public void setSpeed(int speed) {
        if (speed != -1 && this.speed != speed) {
            this.speed = speed;
            if (this.isDashActive) {
                reDraw();
            }
        }
    }

    public void setSpeedLimit(int speedLimit) {
        if (this.speedLimit != speedLimit) {
            this.speedLimit = speedLimit;
            if (this.isDashActive) {
                reDraw();
            }
        }
    }

    public Drawable getDrawable() {
        return this.mTachometerGaugeDrawable;
    }

    protected void updateGauge() {
        if (this.mGaugeView != null) {
            String unitText;
            int speedLimitThreshold;
            this.mTachometerGaugeDrawable.setGaugeValue((float) this.rpm);
            this.mTachometerGaugeDrawable.setMaxMarkerValue(this.maxRpmValue);
            this.mTachometerGaugeDrawable.setMaxMarkerAlpha(this.maxMarkerAlpha);
            this.mGaugeView.setValueText(Integer.toString(this.speed));
            switch (this.speedManager.getSpeedUnit()) {
                case KILOMETERS_PER_HOUR:
                    unitText = this.kmhString;
                    speedLimitThreshold = 13;
                    break;
                default:
                    unitText = this.mphString;
                    speedLimitThreshold = 8;
                    break;
            }
            if (this.speedLimit <= 0) {
                this.mGaugeView.setUnitText(unitText);
                this.mGaugeView.setValueText(Integer.toString(this.speed));
                this.mGaugeView.getUnitTextView().setTextColor(this.speedSafeColor);
            } else if (this.speed >= this.speedLimit + speedLimitThreshold) {
                this.mGaugeView.setUnitText(this.mGaugeView.getResources().getString(R.string.speed_limit_with_unit, new Object[]{Integer.valueOf(this.speedLimit), unitText}));
                this.mGaugeView.getUnitTextView().setTextColor(this.speedVeryFastWarningColor);
            } else if (this.speed >= this.speedLimit) {
                this.mGaugeView.setUnitText(this.mGaugeView.getResources().getString(R.string.speed_limit_with_unit, new Object[]{Integer.valueOf(this.speedLimit), unitText}));
                this.mGaugeView.getUnitTextView().setTextColor(this.speedFastWarningColor);
            } else {
                this.mGaugeView.getUnitTextView().setTextColor(this.speedSafeColor);
                this.mGaugeView.setUnitText(unitText);
            }
        }
    }

    public String getWidgetIdentifier() {
        return ID;
    }

    public String getWidgetName() {
        return null;
    }

    public void setView(DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.tachometer_gauge);
        }
        super.setView(dashboardWidgetView);
        this.mGaugeView = (GaugeView) dashboardWidgetView;
        if (this.mGaugeView != null && VERSION.SDK_INT >= 21) {
            TypedValue outValue = new TypedValue();
            this.mGaugeView.getResources().getValue(R.dimen.speedometer_text_letter_spacing, outValue, true);
            this.mGaugeView.getValueTextView().setLetterSpacing(outValue.getFloat());
        }
    }

    public float getValue() {
        return (float) this.rpm;
    }

    public void setValue(float newValue) {
        this.rpm = (int) newValue;
        if (this.rpm > this.maxRpmValue) {
            this.maxRpmValue = Math.min(MAX_RPM_VALUE, this.rpm);
            this.handler.removeCallbacks(this.animationRunnable);
            this.showingMaxMarkerAnimation = false;
            this.maxMarkerAlpha = 0;
            if (this.valueAnimator != null && this.valueAnimator.isRunning()) {
                this.valueAnimator.cancel();
            }
        }
        if (this.rpm < this.maxRpmValue && !this.showingMaxMarkerAnimation) {
            this.maxMarkerAlpha = 255;
            this.showingMaxMarkerAnimation = true;
            this.handler.removeCallbacks(this.animationRunnable);
            this.handler.postDelayed(this.animationRunnable, 500);
        }
        reDraw();
    }

    public void animationComplete(float newValue) {
    }
}
