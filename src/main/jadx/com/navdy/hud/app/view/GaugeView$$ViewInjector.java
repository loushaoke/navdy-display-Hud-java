package com.navdy.hud.app.view;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class GaugeView$$ViewInjector {
    public static void inject(Finder finder, GaugeView target, Object source) {
        DashboardWidgetView$$ViewInjector.inject(finder, target, source);
        target.mTvValue = (TextView) finder.findRequiredView(source, R.id.txt_value, "field 'mTvValue'");
        target.mTvUnit = (TextView) finder.findOptionalView(source, R.id.txt_unit);
    }

    public static void reset(GaugeView target) {
        DashboardWidgetView$$ViewInjector.reset(target);
        target.mTvValue = null;
        target.mTvUnit = null;
    }
}
