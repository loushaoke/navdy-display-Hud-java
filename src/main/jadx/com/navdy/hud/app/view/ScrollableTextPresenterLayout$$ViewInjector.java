package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;

public class ScrollableTextPresenterLayout$$ViewInjector {
    public static void inject(Finder finder, ScrollableTextPresenterLayout target, Object source) {
        target.mMainTitleText = (TextView) finder.findRequiredView(source, R.id.mainTitle, "field 'mMainTitleText'");
        target.mMainImageView = (ImageView) finder.findRequiredView(source, R.id.mainImage, "field 'mMainImageView'");
        target.mObservableScrollView = (ObservableScrollView) finder.findRequiredView(source, R.id.scrollView, "field 'mObservableScrollView'");
        target.mTitleText = (TextView) finder.findRequiredView(source, R.id.title, "field 'mTitleText'");
        target.mMessageText = (TextView) finder.findRequiredView(source, R.id.message, "field 'mMessageText'");
        target.topScrub = finder.findRequiredView(source, R.id.topScrub, "field 'topScrub'");
        target.bottomScrub = finder.findRequiredView(source, R.id.bottomScrub, "field 'bottomScrub'");
        target.mNotificationIndicator = (CarouselIndicator) finder.findRequiredView(source, R.id.notifIndicator, "field 'mNotificationIndicator'");
        target.mNotificationScrollIndicator = (ProgressIndicator) finder.findRequiredView(source, R.id.notifScrollIndicator, "field 'mNotificationScrollIndicator'");
        target.mChoiceLayout = (ChoiceLayout) finder.findRequiredView(source, R.id.choiceLayout, "field 'mChoiceLayout'");
    }

    public static void reset(ScrollableTextPresenterLayout target) {
        target.mMainTitleText = null;
        target.mMainImageView = null;
        target.mObservableScrollView = null;
        target.mTitleText = null;
        target.mMessageText = null;
        target.topScrub = null;
        target.bottomScrub = null;
        target.mNotificationIndicator = null;
        target.mNotificationScrollIndicator = null;
        target.mChoiceLayout = null;
    }
}
