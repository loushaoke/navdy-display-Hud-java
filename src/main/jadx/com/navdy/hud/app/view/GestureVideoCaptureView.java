package com.navdy.hud.app.view;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.gesture.GestureServiceConnector.RecordingSaved;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.screen.GestureLearningScreen.Presenter;
import com.navdy.hud.app.service.GestureVideosSyncService;
import com.navdy.hud.app.service.S3FileUploadService.UploadFinished;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.inject.Inject;
import mortar.Mortar;

public class GestureVideoCaptureView extends RelativeLayout implements IListener, IInputHandler {
    public static final int TAG_EXIT = 0;
    public static final int TAG_NEXT = 1;
    private static final long UPLOAD_TIMEOUT = 300000;
    public static final Logger sLogger = new Logger(GestureVideoCaptureView.class);
    @Inject
    Bus bus;
    private List<Choice> emptyChoices;
    private List<Choice> exitChoices;
    @Inject
    GestureServiceConnector gestureService;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    @Inject
    Presenter mPresenter;
    private Handler mainHandler;
    @InjectView(R.id.image)
    ImageView mainImageView;
    @InjectView(R.id.title1)
    TextView mainText;
    private List<Choice> nextAndExitChoices;
    private State prevState;
    private RecordingSavingContext recordingToSave;
    private Set<String> recordingsToUpload;
    private boolean registered;
    @InjectView(R.id.title3)
    TextView secondaryText;
    private ExecutorService serialExecutor;
    private String sessionName;
    @InjectView(R.id.sideImage)
    ImageView sideImageView;
    private State state;
    private Runnable uploadTimeoutRunnable;

    private static class ChoiceInfo {
        public final int tag;
        public final int text;

        public ChoiceInfo(int text, int tag) {
            this.text = text;
            this.tag = tag;
        }
    }

    private class RecordingSavingContext {
        public final String sessionName;
        public final State state;

        public RecordingSavingContext(String sessionName, State state) {
            this.sessionName = sessionName;
            this.state = state;
        }
    }

    enum State {
        NOT_PAIRED(R.string.please_pair_phone, R.string.please_pair_phone_detail, null),
        SWIPE_LEFT(R.string.swipe_left, 0, "swipe-left"),
        SWIPE_RIGHT(R.string.swipe_right, 0, "swipe-right"),
        DOUBLE_TAP_1(R.string.double_tap_1, R.string.double_tap_detail_1, "tap"),
        DOUBLE_TAP_2(R.string.double_tap_2, R.string.double_tap_detail_2, "tap2"),
        SAVING(0, R.string.saving_recording, null),
        UPLOADING(R.string.uploading, R.string.uploading_detail, null),
        ALL_DONE(R.string.uploading_success, 0, null),
        UPLOADING_FAILURE(R.string.upload_failure, R.string.upload_failure_detail, null);
        
        int detailText;
        String filename;
        int mainText;

        private State(int mainText, int detailText, String filename) {
            this.mainText = mainText;
            this.detailText = detailText;
            this.filename = filename;
        }
    }

    private class UploadTimeoutRunnable implements Runnable {
        private final String sessionName;

        UploadTimeoutRunnable(String sessionName) {
            this.sessionName = sessionName;
        }

        public void run() {
            GestureVideoCaptureView.sLogger.d("Upload has timed out.");
            if (this.sessionName.equals(GestureVideoCaptureView.this.sessionName)) {
                GestureVideoCaptureView.this.setState(State.UPLOADING_FAILURE);
            }
        }
    }

    public GestureVideoCaptureView(Context context) {
        this(context, null);
    }

    public GestureVideoCaptureView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public GestureVideoCaptureView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, -1);
    }

    public GestureVideoCaptureView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.registered = false;
        this.recordingsToUpload = new HashSet();
        this.emptyChoices = Collections.EMPTY_LIST;
        this.mainHandler = new Handler(Looper.getMainLooper());
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
        this.serialExecutor = Executors.newSingleThreadExecutor();
        this.nextAndExitChoices = new ArrayList();
        this.nextAndExitChoices.add(new Choice(context.getString(R.string.next_recording_step), 1));
        this.nextAndExitChoices.add(new Choice(context.getString(R.string.exit_recording), 0));
        this.exitChoices = new ArrayList();
        this.exitChoices.add(new Choice(context.getString(R.string.exit_recording), 0));
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.sideImageView.setVisibility(0);
        this.mainImageView.setVisibility(0);
        this.mainImageView.setImageResource(R.drawable.icon_settings_learning_to_gesture_gray);
        findViewById(R.id.title2).setVisibility(8);
        findViewById(R.id.title4).setVisibility(8);
        this.secondaryText.setSingleLine(false);
        this.secondaryText.setTextSize(1, 17.0f);
        this.mChoiceLayout.setHighlightPersistent(true);
    }

    private void createSession() {
        this.gestureService.setCalibrationEnabled(false);
        DriverProfile driverProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        if (driverProfile.isDefaultProfile()) {
            setState(State.NOT_PAIRED);
            return;
        }
        long time = System.currentTimeMillis();
        this.sessionName = driverProfile.getDriverEmail() + "-" + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date(time));
        setState(State.SWIPE_LEFT);
    }

    private void resetSession() {
        this.gestureService.setCalibrationEnabled(true);
        this.recordingToSave = null;
        this.recordingsToUpload.clear();
        if (this.uploadTimeoutRunnable != null) {
            this.mainHandler.removeCallbacks(this.uploadTimeoutRunnable);
            this.uploadTimeoutRunnable = null;
        }
    }

    private void setState(State newState) {
        if (this.state != null) {
            switch (this.state) {
                case SWIPE_LEFT:
                case SWIPE_RIGHT:
                case DOUBLE_TAP_1:
                case DOUBLE_TAP_2:
                    stopRecording();
                    break;
            }
        }
        this.prevState = this.state;
        this.state = newState;
        if (this.state == State.SAVING) {
            this.secondaryText.setText(this.state.detailText);
        } else {
            this.mainText.setText(this.state.mainText);
            if (this.state.detailText != 0) {
                this.secondaryText.setText(this.state.detailText);
            } else {
                this.secondaryText.setText("");
            }
        }
        this.mChoiceLayout.setChoices(Mode.LABEL, choicesForState(this.state), 0, this);
        switch (this.state) {
            case SWIPE_LEFT:
            case SWIPE_RIGHT:
            case DOUBLE_TAP_1:
            case DOUBLE_TAP_2:
                startRecording();
                return;
            case UPLOADING:
                if (this.recordingsToUpload.isEmpty()) {
                    this.mainHandler.post(new Runnable() {
                        public void run() {
                            GestureVideoCaptureView.this.setState(State.ALL_DONE);
                        }
                    });
                    return;
                }
                this.uploadTimeoutRunnable = new UploadTimeoutRunnable(this.sessionName);
                this.mainHandler.postDelayed(this.uploadTimeoutRunnable, UPLOAD_TIMEOUT);
                return;
            default:
                return;
        }
    }

    private List<Choice> choicesForState(State state) {
        switch (state) {
            case SWIPE_LEFT:
            case SWIPE_RIGHT:
            case DOUBLE_TAP_1:
            case DOUBLE_TAP_2:
                return this.nextAndExitChoices;
            case UPLOADING:
            case ALL_DONE:
            case UPLOADING_FAILURE:
            case NOT_PAIRED:
                return this.exitChoices;
            default:
                return this.emptyChoices;
        }
    }

    private State nextState() {
        switch (this.state) {
            case SWIPE_LEFT:
            case SWIPE_RIGHT:
            case DOUBLE_TAP_1:
            case DOUBLE_TAP_2:
                return State.SAVING;
            case SAVING:
                switch (this.prevState) {
                    case SWIPE_LEFT:
                        return State.SWIPE_RIGHT;
                    case SWIPE_RIGHT:
                        return State.DOUBLE_TAP_1;
                    case DOUBLE_TAP_1:
                        return State.DOUBLE_TAP_2;
                    case DOUBLE_TAP_2:
                        return State.UPLOADING;
                    default:
                        return null;
                }
            default:
                return null;
        }
    }

    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == 0) {
            this.gestureService.setRecordMode(true);
            createSession();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                return;
            }
            return;
        }
        this.gestureService.stopRecordingVideo();
        resetSession();
        this.gestureService.setRecordMode(false);
        if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    public void executeItem(int pos, int id) {
        switch (id) {
            case 0:
                this.mPresenter.hideCaptureView();
                return;
            case 1:
                setState(nextState());
                return;
            default:
                return;
        }
    }

    @Subscribe
    public void onRecordingSaved(RecordingSaved recordingSaved) {
        sLogger.d("Recording saved");
        RecordingSavingContext context = this.recordingToSave;
        if (this.recordingToSave == null) {
            sLogger.d("Ignore recording " + recordingSaved.path + " due to missing context.");
        } else if (this.recordingToSave.sessionName.equals(this.sessionName)) {
            this.recordingToSave = null;
            setState(nextState());
            final String sessionPath = PathManager.getInstance().getGestureVideosSyncFolder() + File.separator + context.sessionName;
            sLogger.d("Session Path : " + sessionPath);
            sLogger.d("Video archive name to be saved : " + context.state.filename);
            final String videoArchivePath = sessionPath + File.separator + context.state.filename + ".zip";
            this.recordingsToUpload.add(videoArchivePath);
            final String userTag = this.sessionName;
            final RecordingSaved recordingSaved2 = recordingSaved;
            this.serialExecutor.submit(new Runnable() {
                public void run() {
                    File sessionDirectory = new File(sessionPath);
                    if (!sessionDirectory.exists()) {
                        sessionDirectory.mkdirs();
                    }
                    IOUtils.compressFilesToZip(GestureVideoCaptureView.this.getContext(), new File(recordingSaved2.path).listFiles(), videoArchivePath);
                    GestureVideoCaptureView.sLogger.d("Compressed file : " + videoArchivePath);
                    GestureVideosSyncService.addGestureVideoToUploadQueue(new File(videoArchivePath), userTag);
                    GestureVideosSyncService.syncNow();
                }
            });
        } else {
            sLogger.d("Recording session " + this.recordingToSave.sessionName + " doesn't match current session " + this.sessionName);
            sLogger.d("Ignore recording " + recordingSaved.path);
        }
    }

    @Subscribe
    public void onUploadFinished(UploadFinished result) {
        if (result.userTag != null && result.userTag.equals(this.sessionName)) {
            if (result.succeeded) {
                this.recordingsToUpload.remove(result.filePath);
                sLogger.d("Remaining files to be uploaded: " + this.recordingsToUpload.size());
                if (this.recordingsToUpload.isEmpty() && this.state == State.UPLOADING) {
                    if (this.uploadTimeoutRunnable != null) {
                        this.mainHandler.removeCallbacks(this.uploadTimeoutRunnable);
                        this.uploadTimeoutRunnable = null;
                    }
                    setState(State.ALL_DONE);
                    return;
                }
                return;
            }
            sLogger.d("Failed to upload: " + result.filePath);
            setState(State.UPLOADING_FAILURE);
        }
    }

    private void stopRecording() {
        sLogger.d("Stop recording for " + this.state.filename);
        this.gestureService.stopRecordingVideo();
    }

    private void startRecording() {
        sLogger.d("Start recording");
        this.recordingToSave = new RecordingSavingContext(this.sessionName, this.state);
        this.gestureService.startRecordingVideo();
    }

    public void itemSelected(int pos, int id) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                break;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                break;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                break;
        }
        return false;
    }

    public IInputHandler nextHandler() {
        return null;
    }
}
