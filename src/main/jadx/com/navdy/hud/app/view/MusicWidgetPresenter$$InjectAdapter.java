package com.navdy.hud.app.view;

import com.navdy.hud.app.manager.MusicManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class MusicWidgetPresenter$$InjectAdapter extends Binding<MusicWidgetPresenter> implements MembersInjector<MusicWidgetPresenter> {
    private Binding<MusicManager> musicManager;
    private Binding<DashboardWidgetPresenter> supertype;

    public MusicWidgetPresenter$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.MusicWidgetPresenter", false, MusicWidgetPresenter.class);
    }

    public void attach(Linker linker) {
        this.musicManager = linker.requestBinding("com.navdy.hud.app.manager.MusicManager", MusicWidgetPresenter.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.view.DashboardWidgetPresenter", MusicWidgetPresenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.musicManager);
        injectMembersBindings.add(this.supertype);
    }

    public void injectMembers(MusicWidgetPresenter object) {
        object.musicManager = (MusicManager) this.musicManager.get();
        this.supertype.injectMembers(object);
    }
}
