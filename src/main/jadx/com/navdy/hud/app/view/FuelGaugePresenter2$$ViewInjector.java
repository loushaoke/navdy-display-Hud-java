package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class FuelGaugePresenter2$$ViewInjector {
    public static void inject(Finder finder, FuelGaugePresenter2 target, Object source) {
        target.lowFuelIndicatorLeft = (ImageView) finder.findRequiredView(source, R.id.low_fuel_indicator_left, "field 'lowFuelIndicatorLeft'");
        target.lowFuelIndicatorRight = (ImageView) finder.findRequiredView(source, R.id.low_fuel_indicator_right, "field 'lowFuelIndicatorRight'");
        target.fuelTankIndicatorLeft = (ImageView) finder.findRequiredView(source, R.id.fuel_tank_side_indicator_left, "field 'fuelTankIndicatorLeft'");
        target.fuelTankIndicatorRight = (ImageView) finder.findRequiredView(source, R.id.fuel_tank_side_indicator_right, "field 'fuelTankIndicatorRight'");
        target.fuelTypeIndicator = (ImageView) finder.findRequiredView(source, R.id.fuel_type_icon, "field 'fuelTypeIndicator'");
        target.rangeText = (TextView) finder.findRequiredView(source, R.id.txt_value, "field 'rangeText'");
        target.rangeUnitText = (TextView) finder.findRequiredView(source, R.id.txt_unit, "field 'rangeUnitText'");
    }

    public static void reset(FuelGaugePresenter2 target) {
        target.lowFuelIndicatorLeft = null;
        target.lowFuelIndicatorRight = null;
        target.fuelTankIndicatorLeft = null;
        target.fuelTankIndicatorRight = null;
        target.fuelTypeIndicator = null;
        target.rangeText = null;
        target.rangeUnitText = null;
    }
}
