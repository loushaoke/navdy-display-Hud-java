package com.navdy.hud.app.view;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.log.Logger;

public abstract class DashboardWidgetPresenter {
    public static final String EXTRA_GRAVITY = "EXTRA_GRAVITY";
    public static final String EXTRA_IS_ACTIVE = "EXTRA_IS_ACTIVE";
    public static final int GRAVITY_CENTER = 1;
    public static final int GRAVITY_LEFT = 0;
    public static final int GRAVITY_RIGHT = 2;
    protected boolean isDashActive;
    protected boolean isRegistered = false;
    protected boolean isWidgetVisibleToUser;
    protected Logger logger = new Logger(getClass());
    protected View mCustomView;
    protected DashboardWidgetView mWidgetView;

    public abstract Drawable getDrawable();

    public abstract String getWidgetIdentifier();

    public abstract String getWidgetName();

    protected abstract void updateGauge();

    public void setView(DashboardWidgetView dashboardWidgetView) {
        this.mWidgetView = dashboardWidgetView;
        if (this.mWidgetView != null) {
            this.mCustomView = dashboardWidgetView.getCustomView();
            if (this.mCustomView != null) {
                this.mCustomView.setBackground(getDrawable());
            }
            if (isRegisteringToBusRequired() && !this.isRegistered) {
                try {
                    registerToBus();
                    this.isRegistered = true;
                } catch (RuntimeException re) {
                    this.logger.e("Runtime exception registering to the bus ", re);
                }
            }
        } else if (this.isRegistered) {
            try {
                unregisterToBus();
                this.isRegistered = false;
            } catch (RuntimeException re2) {
                this.logger.e("Runtime exception unregistering from the bus ", re2);
            }
        }
        reDraw();
    }

    protected boolean isRegisteringToBusRequired() {
        return false;
    }

    protected void registerToBus() {
        HudApplication.getApplication().getBus().register(this);
    }

    protected void unregisterToBus() {
        HudApplication.getApplication().getBus().unregister(this);
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        setView(dashboardWidgetView);
    }

    public void reDraw() {
        if (canDraw()) {
            updateGauge();
            if (this.mCustomView != null) {
                this.mCustomView.invalidate();
            }
        }
    }

    public boolean isWidgetActive() {
        return this.mWidgetView != null;
    }

    public DashboardWidgetView getWidgetView() {
        return this.mWidgetView;
    }

    public void onPause() {
        this.isDashActive = false;
        if (canDraw()) {
            this.logger.v("widget::onPause:");
        }
    }

    public void onResume() {
        this.isDashActive = true;
        if (canDraw()) {
            this.logger.v("widget::onResume:");
            reDraw();
        }
    }

    public void setWidgetVisibleToUser(boolean b) {
        this.isWidgetVisibleToUser = b;
        if (canDraw()) {
            this.logger.v("widget::visible:reDraw");
            reDraw();
        }
    }

    public boolean canDraw() {
        if (this.isDashActive && this.isWidgetVisibleToUser && this.mWidgetView != null) {
            return true;
        }
        return false;
    }
}
