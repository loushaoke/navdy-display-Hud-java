package com.navdy.hud.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.GestureLearningScreen.Presenter;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.service.library.events.input.GestureEvent;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class LearnGestureScreenLayout extends FrameLayout implements IInputHandler, IListener {
    public static final int TAG_BACK = 0;
    public static final int TAG_TIPS = 1;
    @InjectView(R.id.sensor_blocked_message)
    ViewGroup mCameraBlockedMessage;
    ChoiceLayout mCameraSensorBlockedChoiceLayout;
    private Mode mCurrentMode;
    @InjectView(R.id.gesture_learning_view)
    GestureLearningView mGestureLearningView;
    @Inject
    Presenter mPresenter;
    @InjectView(R.id.scrollable_text_presenter)
    ScrollableTextPresenterLayout mScrollableTextPresenter;
    @InjectView(R.id.capture_instructions_lyt)
    GestureVideoCaptureView mVideoCaptureInstructionsLayout;

    enum Mode {
        GESTURE,
        SENSOR_BLOCKED,
        TIPS,
        CAPTURE
    }

    public LearnGestureScreenLayout(Context context) {
        this(context, null);
    }

    public LearnGestureScreenLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LearnGestureScreenLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCurrentMode = Mode.GESTURE;
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        ImageView sideImageView = (ImageView) this.mCameraBlockedMessage.findViewById(R.id.sideImage);
        sideImageView.setVisibility(0);
        sideImageView.setImageResource(R.drawable.icon_alert_sensor_blocked);
        ImageView mainImageView = (ImageView) this.mCameraBlockedMessage.findViewById(R.id.image);
        mainImageView.setVisibility(0);
        mainImageView.setImageResource(R.drawable.icon_settings_learning_to_gesture_gray);
        ((TextView) this.mCameraBlockedMessage.findViewById(R.id.title1)).setText(R.string.sensor_is_blocked);
        TextView secondaryText = (TextView) this.mCameraBlockedMessage.findViewById(R.id.title3);
        ((MaxWidthLinearLayout) this.mCameraBlockedMessage.findViewById(R.id.infoContainer)).setMaxWidth(getResources().getDimensionPixelSize(R.dimen.gesture_sensor_blocked_max_width));
        secondaryText.setText(R.string.sensor_is_blocked_message);
        this.mCameraBlockedMessage.findViewById(R.id.title2).setVisibility(8);
        this.mCameraBlockedMessage.findViewById(R.id.title4).setVisibility(8);
        secondaryText.setSingleLine(false);
        secondaryText.setTextSize(1, 17.0f);
        List<Choice> list = new ArrayList();
        list.add(new Choice(getContext().getString(R.string.back), 0));
        list.add(new Choice(getContext().getString(R.string.tips), 1));
        this.mCameraSensorBlockedChoiceLayout = (ChoiceLayout) this.mCameraBlockedMessage.findViewById(R.id.choiceLayout);
        this.mCameraSensorBlockedChoiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout.Mode.LABEL, list, 0, this);
        this.mCameraSensorBlockedChoiceLayout.setHighlightPersistent(true);
        String[] array = getResources().getStringArray(R.array.gesture_tips);
        CharSequence[] charSequences = new CharSequence[array.length];
        for (int i = 0; i < array.length; i++) {
            charSequences[i] = array[i];
        }
        this.mScrollableTextPresenter.setTextContents(charSequences);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
        }
        this.mGestureLearningView.setVisibility(0);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mPresenter != null) {
            this.mPresenter.dropView((View) this);
        }
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.mCurrentMode != Mode.SENSOR_BLOCKED) {
            return false;
        }
        switch (event) {
            case LEFT:
                this.mCameraSensorBlockedChoiceLayout.moveSelectionLeft();
                return true;
            case RIGHT:
                this.mCameraSensorBlockedChoiceLayout.moveSelectionRight();
                return true;
            case SELECT:
                this.mCameraSensorBlockedChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return true;
        }
    }

    public IInputHandler nextHandler() {
        switch (this.mCurrentMode) {
            case GESTURE:
                return this.mGestureLearningView;
            case TIPS:
                return this.mScrollableTextPresenter;
            case CAPTURE:
                return this.mVideoCaptureInstructionsLayout;
            default:
                return null;
        }
    }

    public void showTips() {
        this.mGestureLearningView.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(0);
        this.mCurrentMode = Mode.TIPS;
    }

    public void hideTips() {
        this.mGestureLearningView.setVisibility(0);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = Mode.GESTURE;
    }

    public void showSensorBlocked() {
        this.mGestureLearningView.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(0);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = Mode.SENSOR_BLOCKED;
    }

    public void hideSensorBlocked() {
        this.mGestureLearningView.setVisibility(0);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = Mode.GESTURE;
    }

    public void showCaptureGestureVideosView() {
        this.mGestureLearningView.setVisibility(8);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(0);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = Mode.CAPTURE;
    }

    public void hideCaptureGestureVideosView() {
        this.mGestureLearningView.setVisibility(0);
        this.mCameraBlockedMessage.setVisibility(8);
        this.mVideoCaptureInstructionsLayout.setVisibility(8);
        this.mScrollableTextPresenter.setVisibility(8);
        this.mCurrentMode = Mode.GESTURE;
    }

    public void executeItem(int pos, int id) {
        switch (id) {
            case 0:
                this.mPresenter.hideCameraSensorBlocked();
                return;
            case 1:
                this.mPresenter.showTips();
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }
}
