package com.navdy.hud.app.view;

import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ConfirmationLayout;

public class ToastView$$ViewInjector {
    public static void inject(Finder finder, ToastView target, Object source) {
        target.mainView = (ConfirmationLayout) finder.findRequiredView(source, R.id.mainView, "field 'mainView'");
    }

    public static void reset(ToastView target) {
        target.mainView = null;
    }
}
