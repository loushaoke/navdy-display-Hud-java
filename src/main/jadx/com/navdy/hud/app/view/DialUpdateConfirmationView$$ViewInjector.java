package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout;

public class DialUpdateConfirmationView$$ViewInjector {
    public static void inject(Finder finder, DialUpdateConfirmationView target, Object source) {
        target.mScreenTitleText = (TextView) finder.findRequiredView(source, R.id.mainTitle, "field 'mScreenTitleText'");
        target.mMainTitleText = (TextView) finder.findRequiredView(source, R.id.title2, "field 'mMainTitleText'");
        target.mInfoText = (TextView) finder.findRequiredView(source, R.id.title3, "field 'mInfoText'");
        target.mSummaryText = (TextView) finder.findRequiredView(source, R.id.title4, "field 'mSummaryText'");
        target.mChoiceLayout = (ChoiceLayout) finder.findRequiredView(source, R.id.choiceLayout, "field 'mChoiceLayout'");
        target.mIcon = (ImageView) finder.findRequiredView(source, R.id.image, "field 'mIcon'");
        target.mRightSwipe = (ImageView) finder.findRequiredView(source, R.id.rightSwipe, "field 'mRightSwipe'");
        target.mLefttSwipe = (ImageView) finder.findRequiredView(source, R.id.leftSwipe, "field 'mLefttSwipe'");
        target.mainSection = (RelativeLayout) finder.findRequiredView(source, R.id.mainSection, "field 'mainSection'");
    }

    public static void reset(DialUpdateConfirmationView target) {
        target.mScreenTitleText = null;
        target.mMainTitleText = null;
        target.mInfoText = null;
        target.mSummaryText = null;
        target.mChoiceLayout = null;
        target.mIcon = null;
        target.mRightSwipe = null;
        target.mLefttSwipe = null;
        target.mainSection = null;
    }
}
