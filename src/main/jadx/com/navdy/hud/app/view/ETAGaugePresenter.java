package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.content.ContextCompat;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.util.DistanceConverter;
import com.navdy.hud.app.maps.util.DistanceConverter.Distance;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.view.drawable.ETAProgressDrawable;
import com.squareup.otto.Subscribe;
import java.util.concurrent.TimeUnit;

public class ETAGaugePresenter extends DashboardWidgetPresenter {
    static final /* synthetic */ boolean $assertionsDisabled;
    private static final long TRIP_INFO_UPDATE_INTERVAL = TimeUnit.MINUTES.toMillis(1);
    @InjectView(R.id.txt_eta_am_pm)
    TextView etaAmPmText;
    private ETAProgressDrawable etaProgressDrawable;
    @InjectView(R.id.txt_eta)
    TextView etaText;
    private String etaTripGaugeName;
    @InjectView(R.id.eta_view)
    ViewGroup etaView;
    private int farMargin;
    private String feetLabel;
    private int fontSize18;
    private int fontSize30;
    private int gravity;
    private int greyColor;
    private boolean isNavigationActive;
    private String kiloMetersLabel;
    private String[] labelShort;
    private String[] labels;
    private Handler mHandler;
    private Runnable mTripInfoUpdateRunnable;
    private TripManager mTripManager = RemoteDeviceManager.getInstance().getTripManager();
    private ManeuverDisplay maneuverDisplay;
    private String metersLabel;
    private String milesLabel;
    private int nearMargin;
    @InjectView(R.id.txt_remaining_distance)
    TextView remainingDistanceText;
    @InjectView(R.id.txt_remaining_distance_unit)
    TextView remainingDistanceUnitText;
    @InjectView(R.id.tripDistance)
    TextView tripDistanceView;
    private int tripGaugeIconMargin;
    private int tripGaugeLongMargin;
    private int tripGaugeShortMargin;
    @InjectView(R.id.tripIcon)
    ImageView tripIconView;
    @InjectView(R.id.tripOpenMap)
    ConstraintLayout tripOpenMapView;
    @InjectView(R.id.tripTime)
    TextView tripTimeView;
    @InjectView(R.id.txt_tta1)
    TextView ttaText1;
    @InjectView(R.id.txt_tta2)
    TextView ttaText2;
    @InjectView(R.id.txt_tta3)
    TextView ttaText3;
    @InjectView(R.id.txt_tta4)
    TextView ttaText4;

    static {
        boolean z;
        if (ETAGaugePresenter.class.desiredAssertionStatus()) {
            z = false;
        } else {
            z = true;
        }
        $assertionsDisabled = z;
    }

    public ETAGaugePresenter(Context context) {
        Resources resources = context.getResources();
        this.labels = resources.getStringArray(R.array.time_labels);
        this.labelShort = resources.getStringArray(R.array.time_labels_short);
        this.metersLabel = context.getResources().getString(R.string.unit_meters);
        this.kiloMetersLabel = context.getResources().getString(R.string.unit_kilometers);
        this.feetLabel = context.getResources().getString(R.string.unit_feet);
        this.milesLabel = context.getResources().getString(R.string.unit_miles);
        this.farMargin = resources.getDimensionPixelSize(R.dimen.far_margin);
        this.nearMargin = resources.getDimensionPixelSize(R.dimen.near_margin);
        this.etaTripGaugeName = context.getResources().getString(R.string.widget_trip_eta);
        this.tripGaugeShortMargin = resources.getDimensionPixelSize(R.dimen.trip_gauge_short_margin);
        this.tripGaugeLongMargin = resources.getDimensionPixelSize(R.dimen.trip_gauge_long_margin);
        this.tripGaugeIconMargin = resources.getDimensionPixelSize(R.dimen.trip_gauge_icon_margin);
        this.greyColor = ContextCompat.getColor(context, R.color.grey_ababab);
        this.fontSize18 = resources.getDimensionPixelSize(R.dimen.active_trip_18);
        this.fontSize30 = resources.getDimensionPixelSize(R.dimen.size_30);
        this.mHandler = new Handler();
        this.mTripInfoUpdateRunnable = new Runnable() {
            public void run() {
                ETAGaugePresenter.this.mHandler.postDelayed(ETAGaugePresenter.this.mTripInfoUpdateRunnable, ETAGaugePresenter.TRIP_INFO_UPDATE_INTERVAL);
                ETAGaugePresenter.this.reDraw();
            }
        };
        this.etaProgressDrawable = new ETAProgressDrawable(context);
        this.etaProgressDrawable.setMinValue(0.0f);
        this.etaProgressDrawable.setMaxGaugeValue(100.0f);
    }

    private boolean parseSeconds(long timeInSeconds, int[] splitTime, byte[] startEnd) {
        splitTime[3] = (int) TimeUnit.SECONDS.toDays(timeInSeconds);
        splitTime[2] = (int) (TimeUnit.SECONDS.toHours(timeInSeconds) - ((long) (splitTime[3] * 24)));
        splitTime[1] = (int) (TimeUnit.SECONDS.toMinutes(timeInSeconds) - (TimeUnit.SECONDS.toHours(timeInSeconds) * 60));
        splitTime[0] = (int) (timeInSeconds - (TimeUnit.SECONDS.toMinutes(timeInSeconds) * 60));
        startEnd[0] = (byte) 0;
        startEnd[1] = (byte) 0;
        for (byte i = (byte) 3; i > (byte) 0; i = (byte) (i - 1)) {
            if (splitTime[i] > 0) {
                startEnd[1] = i;
                if (startEnd[0] == (byte) 0) {
                    startEnd[0] = i;
                }
            } else if (startEnd[0] != (byte) 0) {
                break;
            }
        }
        if (!$assertionsDisabled && startEnd[0] != startEnd[1] && startEnd[0] != startEnd[1] + 1) {
            throw new AssertionError();
        } else if (startEnd[0] > startEnd[1]) {
            return true;
        } else {
            return false;
        }
    }

    private void setTTAText(long timeInMilliSeconds) {
        int[] splitTime = new int[4];
        byte[] startEnd = new byte[2];
        boolean singleCharacter = parseSeconds(Math.max(60, timeInMilliSeconds / 1000), splitTime, startEnd);
        byte start = startEnd[0];
        byte end = startEnd[1];
        if (singleCharacter) {
            this.ttaText1.setText(Integer.toString(splitTime[start]));
            this.ttaText2.setText(Character.toString(this.labels[start].charAt(0)));
            this.ttaText3.setVisibility(0);
            this.ttaText4.setVisibility(0);
            this.ttaText3.setText(Integer.toString(splitTime[end]));
            this.ttaText4.setText(Character.toString(this.labels[end].charAt(0)));
            return;
        }
        this.ttaText1.setText(Integer.toString(splitTime[start]));
        this.ttaText2.setText(this.labels[start]);
        this.ttaText3.setVisibility(8);
        this.ttaText4.setVisibility(8);
    }

    private void setTripTimeText(long timeInMilliSeconds) {
        long timeInSeconds = timeInMilliSeconds / 1000;
        if (timeInSeconds == 0) {
            this.tripDistanceView.setVisibility(8);
            this.tripTimeView.setText(getTime(String.valueOf(0), this.labelShort[0]));
            return;
        }
        SpannableStringBuilder stringBuilder;
        byte[] startEnd = new byte[2];
        boolean singleCharacter = parseSeconds(Math.max(60, timeInSeconds), new int[4], startEnd);
        byte start = startEnd[0];
        byte end = startEnd[1];
        if (singleCharacter) {
            stringBuilder = getTime(Integer.toString(splitTime[start]), String.valueOf(this.labelShort[start].charAt(0)), Integer.toString(splitTime[end]), String.valueOf(this.labelShort[end].charAt(0)));
        } else {
            stringBuilder = getTime(Integer.toString(splitTime[start]), String.valueOf(this.labels[start]));
        }
        this.tripTimeView.setText(stringBuilder);
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        if (dashboardWidgetView != null) {
            this.gravity = arguments != null ? arguments.getInt(DashboardWidgetPresenter.EXTRA_GRAVITY, 0) : 0;
            dashboardWidgetView.setContentView((int) R.layout.eta_gauge_layout);
            ButterKnife.inject( this, (View) dashboardWidgetView);
            MarginLayoutParams marginParams = (MarginLayoutParams) this.etaView.getLayoutParams();
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(this.tripOpenMapView);
            int parentId = this.tripOpenMapView.getId();
            int iconViewId = this.tripIconView.getId();
            switch (this.gravity) {
                case 0:
                    marginParams.leftMargin = this.nearMargin;
                    marginParams.rightMargin = this.farMargin;
                    constraintSet.connect(iconViewId, 6, parentId, 6, 0);
                    constraintSet.connect(this.tripTimeView.getId(), 1, iconViewId, 2, this.tripGaugeIconMargin);
                    break;
                case 2:
                    marginParams.leftMargin = this.farMargin;
                    marginParams.rightMargin = this.nearMargin;
                    constraintSet.connect(iconViewId, 7, parentId, 7, 0);
                    constraintSet.connect(this.tripTimeView.getId(), 2, iconViewId, 1, this.tripGaugeIconMargin);
                    break;
            }
            constraintSet.applyTo(this.tripOpenMapView);
            this.etaView.setLayoutParams(marginParams);
            this.isNavigationActive = RemoteDeviceManager.getInstance().getUiStateManager().isNavigationActive();
            HereMapsManager.getInstance().postManeuverDisplay();
            super.setView(dashboardWidgetView, arguments);
            reDraw();
            return;
        }
        super.setView(dashboardWidgetView, arguments);
        reDraw();
    }

    protected boolean isRegisteringToBusRequired() {
        return true;
    }

    public Drawable getDrawable() {
        return this.etaProgressDrawable;
    }

    protected void updateGauge() {
        if (!this.isNavigationActive || this.maneuverDisplay == null || this.maneuverDisplay.etaDate == null || this.maneuverDisplay.totalDistanceUnit == null || this.maneuverDisplay.totalDistanceRemainingUnit == null) {
            this.tripOpenMapView.setVisibility(0);
            long tripTime = this.mTripManager.getCurrentTripStartTime() > 0 ? System.currentTimeMillis() - this.mTripManager.getCurrentTripStartTime() : 0;
            long distanceTravelledInMeters = (long) this.mTripManager.getCurrentDistanceTraveled();
            SpeedManager speedManager = SpeedManager.getInstance();
            Distance distance = new Distance();
            DistanceConverter.convertToDistance(speedManager.getSpeedUnit(), (float) distanceTravelledInMeters, distance);
            String unitLabel = "";
            switch (distance.unit) {
                case DISTANCE_METERS:
                    unitLabel = this.metersLabel;
                    break;
                case DISTANCE_KMS:
                    unitLabel = this.kiloMetersLabel;
                    break;
                case DISTANCE_MILES:
                    unitLabel = this.milesLabel;
                    break;
                case DISTANCE_FEET:
                    unitLabel = this.feetLabel;
                    break;
            }
            this.tripDistanceView.setText(getDistance(Float.toString(distance.value), unitLabel));
            this.tripDistanceView.setVisibility(0);
            setTripTimeText(tripTime);
            this.mHandler.removeCallbacks(this.mTripInfoUpdateRunnable);
            this.mHandler.postDelayed(this.mTripInfoUpdateRunnable, TRIP_INFO_UPDATE_INTERVAL);
            this.etaView.setVisibility(8);
            return;
        }
        this.mHandler.removeCallbacks(this.mTripInfoUpdateRunnable);
        this.tripOpenMapView.setVisibility(8);
        this.etaView.setVisibility(0);
        long currentTime = System.currentTimeMillis();
        long etaTime = this.maneuverDisplay.etaDate.getTime();
        setTTAText(etaTime > currentTime ? etaTime - currentTime : 0);
        this.etaText.setText(this.maneuverDisplay.eta);
        this.etaAmPmText.setText(this.maneuverDisplay.etaAmPm);
        float totalDistanceInMeters = DistanceConverter.convertToMeters(this.maneuverDisplay.totalDistance, this.maneuverDisplay.totalDistanceUnit);
        float coveredDistanceInMeters = totalDistanceInMeters - DistanceConverter.convertToMeters(this.maneuverDisplay.totalDistanceRemaining, this.maneuverDisplay.totalDistanceRemainingUnit);
        int percentage = (totalDistanceInMeters <= 0.0f || totalDistanceInMeters <= coveredDistanceInMeters) ? 0 : (int) ((coveredDistanceInMeters / totalDistanceInMeters) * 100.0f);
        this.etaProgressDrawable.setGaugeValue((float) percentage);
        this.remainingDistanceText.setText(Float.toString(this.maneuverDisplay.totalDistanceRemaining));
        switch (this.maneuverDisplay.totalDistanceRemainingUnit) {
            case DISTANCE_METERS:
                this.remainingDistanceUnitText.setText(this.metersLabel);
                return;
            case DISTANCE_KMS:
                this.remainingDistanceUnitText.setText(this.kiloMetersLabel);
                return;
            case DISTANCE_MILES:
                this.remainingDistanceUnitText.setText(this.milesLabel);
                return;
            case DISTANCE_FEET:
                this.remainingDistanceUnitText.setText(this.feetLabel);
                return;
            default:
                return;
        }
    }

    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.ETA_GAUGE_ID;
    }

    public String getWidgetName() {
        return this.etaTripGaugeName;
    }

    @Subscribe
    public void onNavigationModeChange(NavigationMode navigationMode) {
        this.isNavigationActive = RemoteDeviceManager.getInstance().getUiStateManager().isNavigationActive();
        this.maneuverDisplay = null;
        reDraw();
    }

    @Subscribe
    public void onManeuverDisplay(ManeuverDisplay maneuverDisplay) {
        if (!maneuverDisplay.isEmpty()) {
            UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            this.maneuverDisplay = maneuverDisplay;
            if (this.maneuverDisplay != null) {
                this.isNavigationActive = this.maneuverDisplay.isNavigating();
            } else {
                this.isNavigationActive = uiStateManager.isNavigationActive();
            }
            reDraw();
        }
    }

    private SpannableStringBuilder getTime(String... data) {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        if (this.gravity == 0) {
            addPadding(stringBuilder);
        }
        int i = 0;
        while (i < data.length) {
            if (i != 0) {
                stringBuilder.append(" ");
            }
            String val = data[i];
            String unit = data[i + 1];
            i += 2;
            int start = stringBuilder.length();
            stringBuilder.append(val);
            int len = stringBuilder.length();
            stringBuilder.setSpan(new AbsoluteSizeSpan(this.fontSize30), start, len, 34);
            stringBuilder.setSpan(new ForegroundColorSpan(-1), start, len, 34);
            stringBuilder.append(" ");
            start = stringBuilder.length();
            stringBuilder.append(unit);
            len = stringBuilder.length();
            stringBuilder.setSpan(new AbsoluteSizeSpan(this.fontSize18), start, len, 34);
            stringBuilder.setSpan(new ForegroundColorSpan(this.greyColor), start, len, 34);
        }
        if (this.gravity == 2) {
            addPadding(stringBuilder);
        }
        return stringBuilder;
    }

    private SpannableStringBuilder getDistance(String val, String unit) {
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        if (this.gravity == 0) {
            addPadding(stringBuilder);
        }
        stringBuilder.append(val);
        int len = stringBuilder.length();
        stringBuilder.setSpan(new AbsoluteSizeSpan(this.fontSize18), 0, len, 34);
        stringBuilder.setSpan(new ForegroundColorSpan(-1), 0, len, 34);
        stringBuilder.append(" ");
        int start = stringBuilder.length();
        stringBuilder.append(unit);
        len = stringBuilder.length();
        stringBuilder.setSpan(new AbsoluteSizeSpan(this.fontSize18), start, len, 34);
        stringBuilder.setSpan(new ForegroundColorSpan(this.greyColor), start, len, 34);
        if (this.gravity == 2) {
            addPadding(stringBuilder);
        }
        return stringBuilder;
    }

    private void addPadding(SpannableStringBuilder stringBuilder) {
        stringBuilder.append(" ");
        stringBuilder.append(" ");
        stringBuilder.append(" ");
    }
}
