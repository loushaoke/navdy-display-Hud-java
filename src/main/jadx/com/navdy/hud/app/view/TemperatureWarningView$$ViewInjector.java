package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout;

public class TemperatureWarningView$$ViewInjector {
    public static void inject(Finder finder, TemperatureWarningView target, Object source) {
        target.mScreenTitleText = (TextView) finder.findRequiredView(source, R.id.mainTitle, "field 'mScreenTitleText'");
        target.mTitle1 = (TextView) finder.findRequiredView(source, R.id.title1, "field 'mTitle1'");
        target.mMainTitleText = (TextView) finder.findRequiredView(source, R.id.title2, "field 'mMainTitleText'");
        target.mWarningMessage = (TextView) finder.findRequiredView(source, R.id.title3, "field 'mWarningMessage'");
        target.mChoiceLayout = (ChoiceLayout) finder.findRequiredView(source, R.id.choiceLayout, "field 'mChoiceLayout'");
        target.mRightSwipe = (ImageView) finder.findRequiredView(source, R.id.rightSwipe, "field 'mRightSwipe'");
        target.mIcon = (ImageView) finder.findRequiredView(source, R.id.image, "field 'mIcon'");
    }

    public static void reset(TemperatureWarningView target) {
        target.mScreenTitleText = null;
        target.mTitle1 = null;
        target.mMainTitleText = null;
        target.mWarningMessage = null;
        target.mChoiceLayout = null;
        target.mRightSwipe = null;
        target.mIcon = null;
    }
}
