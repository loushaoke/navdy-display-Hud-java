package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.DialUpdateProgressScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class DialUpdateProgressView$$InjectAdapter extends Binding<DialUpdateProgressView> implements MembersInjector<DialUpdateProgressView> {
    private Binding<Presenter> mPresenter;

    public DialUpdateProgressView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.DialUpdateProgressView", false, DialUpdateProgressView.class);
    }

    public void attach(Linker linker) {
        this.mPresenter = linker.requestBinding("com.navdy.hud.app.screen.DialUpdateProgressScreen$Presenter", DialUpdateProgressView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mPresenter);
    }

    public void injectMembers(DialUpdateProgressView object) {
        object.mPresenter = (Presenter) this.mPresenter.get();
    }
}
