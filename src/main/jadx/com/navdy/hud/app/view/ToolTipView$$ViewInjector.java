package com.navdy.hud.app.view;

import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class ToolTipView$$ViewInjector {
    public static void inject(Finder finder, ToolTipView target, Object source) {
        target.toolTipContainer = (ViewGroup) finder.findRequiredView(source, R.id.tooltip_container, "field 'toolTipContainer'");
        target.toolTipTextView = (TextView) finder.findRequiredView(source, R.id.tooltip_text, "field 'toolTipTextView'");
        target.toolTipScrim = finder.findRequiredView(source, R.id.tooltip_scrim, "field 'toolTipScrim'");
        target.toolTipTriangle = finder.findRequiredView(source, R.id.tooltip_triangle, "field 'toolTipTriangle'");
    }

    public static void reset(ToolTipView target) {
        target.toolTipContainer = null;
        target.toolTipTextView = null;
        target.toolTipScrim = null;
        target.toolTipTriangle = null;
    }
}
