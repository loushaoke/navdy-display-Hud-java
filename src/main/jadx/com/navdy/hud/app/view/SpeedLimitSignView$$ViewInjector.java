package com.navdy.hud.app.view;

import android.view.ViewGroup;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class SpeedLimitSignView$$ViewInjector {
    public static void inject(Finder finder, SpeedLimitSignView target, Object source) {
        target.usSpeedLimitSignView = (ViewGroup) finder.findRequiredView(source, R.id.speed_limit_sign_us, "field 'usSpeedLimitSignView'");
        target.euSpeedLimitSignView = (ViewGroup) finder.findRequiredView(source, R.id.speed_limit_sign_eu, "field 'euSpeedLimitSignView'");
    }

    public static void reset(SpeedLimitSignView target) {
        target.usSpeedLimitSignView = null;
        target.euSpeedLimitSignView = null;
    }
}
