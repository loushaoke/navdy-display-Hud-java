package com.navdy.hud.app.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.R;
import com.navdy.hud.app.device.gps.CalibratedGForceData;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.squareup.otto.Subscribe;

public class GForcePresenter extends DashboardWidgetPresenter {
    GForceDrawable drawable;
    private String gMeterGaugeName;
    private boolean registered;
    private float xAccel;
    private float yAccel;
    private float zAccel;

    public GForcePresenter(Context context) {
        this.drawable = new GForceDrawable(context);
        this.gMeterGaugeName = context.getResources().getString(R.string.widget_g_meter);
    }

    public void setView(DashboardWidgetView dashboardWidgetView) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.smart_dash_widget_circular_content_layout);
        }
        super.setView(dashboardWidgetView);
    }

    protected boolean isRegisteringToBusRequired() {
        return true;
    }

    @Subscribe
    public void onCalibratedGForceData(CalibratedGForceData calibratedGForceData) {
        this.xAccel = calibratedGForceData.getXAccel();
        this.yAccel = calibratedGForceData.getYAccel();
        this.zAccel = calibratedGForceData.getZAccel();
        reDraw();
    }

    public Drawable getDrawable() {
        return this.drawable;
    }

    protected void updateGauge() {
        this.drawable.setAcceleration(this.xAccel, this.yAccel, this.zAccel);
    }

    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.GFORCE_WIDGET_ID;
    }

    public String getWidgetName() {
        return this.gMeterGaugeName;
    }
}
