package com.navdy.hud.app.view.drawable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class GaugeDrawable extends CustomDrawable {
    private Drawable mBackgroundDrawable;
    protected int[] mColorTable;
    protected int mDefaultColor;
    protected float mMaxValue;
    protected float mMinValue;
    protected float mValue;

    public GaugeDrawable(Context context, int backgroundResource) {
        if (backgroundResource != 0) {
            this.mBackgroundDrawable = context.getResources().getDrawable(backgroundResource);
        }
        this.mPaint.setAntiAlias(true);
    }

    public GaugeDrawable(Context context, int backgroundResource, int stateColorsResId) {
        this(context, backgroundResource);
        this.mColorTable = context.getResources().getIntArray(stateColorsResId);
        if (this.mColorTable != null && this.mColorTable.length > 0) {
            this.mDefaultColor = this.mColorTable[0];
        }
    }

    public void setGaugeValue(float value) {
        this.mValue = value;
        if (value > this.mMaxValue) {
            this.mValue = this.mMaxValue;
        } else if (value < this.mMinValue) {
            this.mValue = this.mMinValue;
        }
    }

    public void setMaxGaugeValue(float maxValue) {
        this.mMaxValue = maxValue;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.mBackgroundDrawable != null) {
            this.mBackgroundDrawable.draw(canvas);
        }
    }

    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        if (this.mBackgroundDrawable != null) {
            this.mBackgroundDrawable.setBounds(left, top, right, bottom);
        }
    }

    public void setState(int index) {
        if (this.mColorTable == null || index >= this.mColorTable.length || index < 0) {
            throw new IllegalArgumentException();
        }
        this.mDefaultColor = this.mColorTable[index];
    }

    public int getHeight() {
        Rect rect = getBounds();
        if (rect != null) {
            return rect.height();
        }
        return 0;
    }

    public int getWidth() {
        Rect rect = getBounds();
        if (rect != null) {
            return rect.width();
        }
        return 0;
    }

    public void setMinValue(float mMinValue) {
        this.mMinValue = mMinValue;
    }
}
