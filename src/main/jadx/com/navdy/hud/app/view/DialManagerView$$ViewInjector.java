package com.navdy.hud.app.view;

import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.VideoView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ConfirmationLayout;

public class DialManagerView$$ViewInjector {
    public static void inject(Finder finder, DialManagerView target, Object source) {
        target.videoContainer = (ViewGroup) finder.findRequiredView(source, R.id.videoContainer, "field 'videoContainer'");
        target.videoView = (VideoView) finder.findRequiredView(source, R.id.videoView, "field 'videoView'");
        target.rePairTextView = (TextView) finder.findRequiredView(source, R.id.repair_text, "field 'rePairTextView'");
        target.connectedView = (ConfirmationLayout) finder.findRequiredView(source, R.id.connectedView, "field 'connectedView'");
    }

    public static void reset(DialManagerView target) {
        target.videoContainer = null;
        target.videoView = null;
        target.rePairTextView = null;
        target.connectedView = null;
    }
}
