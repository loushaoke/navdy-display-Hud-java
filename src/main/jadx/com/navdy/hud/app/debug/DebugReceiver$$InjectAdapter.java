package com.navdy.hud.app.debug;

import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class DebugReceiver$$InjectAdapter extends Binding<DebugReceiver> implements Provider<DebugReceiver>, MembersInjector<DebugReceiver> {
    private Binding<Bus> mBus;

    public DebugReceiver$$InjectAdapter() {
        super("com.navdy.hud.app.debug.DebugReceiver", "members/com.navdy.hud.app.debug.DebugReceiver", false, DebugReceiver.class);
    }

    public void attach(Linker linker) {
        this.mBus = linker.requestBinding("com.squareup.otto.Bus", DebugReceiver.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mBus);
    }

    public DebugReceiver get() {
        DebugReceiver result = new DebugReceiver();
        injectMembers(result);
        return result;
    }

    public void injectMembers(DebugReceiver object) {
        object.mBus = (Bus) this.mBus.get();
    }
}
