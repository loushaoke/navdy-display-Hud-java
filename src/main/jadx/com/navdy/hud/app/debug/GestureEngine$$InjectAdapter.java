package com.navdy.hud.app.debug;

import com.navdy.hud.app.gesture.GestureServiceConnector;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class GestureEngine$$InjectAdapter extends Binding<GestureEngine> implements MembersInjector<GestureEngine> {
    private Binding<GestureServiceConnector> gestureServiceConnector;

    public GestureEngine$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.debug.GestureEngine", false, GestureEngine.class);
    }

    public void attach(Linker linker) {
        this.gestureServiceConnector = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", GestureEngine.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.gestureServiceConnector);
    }

    public void injectMembers(GestureEngine object) {
        object.gestureServiceConnector = (GestureServiceConnector) this.gestureServiceConnector.get();
    }
}
