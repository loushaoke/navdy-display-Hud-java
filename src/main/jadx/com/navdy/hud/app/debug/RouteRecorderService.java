package com.navdy.hud.app.debug;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.navdy.hud.app.debug.IRouteRecorder.Stub;

public class RouteRecorderService extends Service {
    private RouteRecorder mRouteRecorder = RouteRecorder.getInstance();

    public IBinder onBind(Intent intent) {
        return new Stub() {
            public void prepare(String fileName, boolean secondary) throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.prepare(fileName, secondary);
            }

            public String startRecording(String label) throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.startRecording(label, true);
            }

            public void stopRecording() throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.stopRecording(true);
            }

            public boolean isRecording() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.isRecording();
            }

            public void startPlayback(String fileName, boolean secondary, boolean loop) throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.startPlayback(fileName, secondary, loop, true);
            }

            public void pausePlayback() throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.pausePlayback();
            }

            public void resumePlayback() throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.resumePlayback();
            }

            public void stopPlayback() throws RemoteException {
                RouteRecorderService.this.mRouteRecorder.stopPlayback();
            }

            public boolean restartPlayback() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.restartPlayback();
            }

            public boolean isPlaying() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.isPlaying();
            }

            public boolean isPaused() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.isPaused();
            }

            public boolean isStopped() throws RemoteException {
                return RouteRecorderService.this.mRouteRecorder.isStopped();
            }
        };
    }
}
