package com.navdy.hud.app.debug;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import kotlin.Metadata;
import kotlin.jvm.JvmOverloads;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

@Metadata(bv = {1, 0, 1}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u0010\u000f\u001a\u00020\u000eJ\u001a\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00032\b\b\u0002\u0010\u0012\u001a\u00020\u0013H\u0007R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/navdy/hud/app/debug/AsyncBufferedFileWriter;", "", "filePath", "", "executor", "Lcom/navdy/hud/app/debug/SerialExecutor;", "bufferSize", "", "(Ljava/lang/String;Lcom/navdy/hud/app/debug/SerialExecutor;I)V", "bufferedWriter", "Ljava/io/BufferedWriter;", "getBufferedWriter", "()Ljava/io/BufferedWriter;", "flush", "", "flushAndClose", "write", "data", "forceToDisk", "", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: AsyncBufferedFileWriter.kt */
public final class AsyncBufferedFileWriter {
    @NotNull
    private final BufferedWriter bufferedWriter;
    private final SerialExecutor executor;

    @JvmOverloads
    public final void write(@NotNull String data) {
        write$default(this, data, false, 2, null);
    }

    public AsyncBufferedFileWriter(@NotNull String filePath, @NotNull SerialExecutor executor, int bufferSize) {
        Intrinsics.checkParameterIsNotNull(filePath, "filePath");
        Intrinsics.checkParameterIsNotNull(executor, "executor");
        this.executor = executor;
        this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(filePath)), "utf-8"), bufferSize);
    }

    @NotNull
    public final BufferedWriter getBufferedWriter() {
        return this.bufferedWriter;
    }

    @JvmOverloads
    public static /* bridge */ /* synthetic */ void write$default(AsyncBufferedFileWriter asyncBufferedFileWriter, String str, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        asyncBufferedFileWriter.write(str, z);
    }

    @JvmOverloads
    public final void write(@NotNull String data, boolean forceToDisk) {
        Intrinsics.checkParameterIsNotNull(data, "data");
        this.executor.execute(new AsyncBufferedFileWriter$write$1(this, data, forceToDisk));
    }

    public final void flush() {
        this.executor.execute(new AsyncBufferedFileWriter$flush$1(this));
    }

    public final void flushAndClose() {
        this.executor.execute(new AsyncBufferedFileWriter$flushAndClose$1(this));
    }
}
