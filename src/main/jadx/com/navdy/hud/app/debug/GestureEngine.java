package com.navdy.hud.app.debug;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import com.navdy.hud.app.R;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.service.library.task.TaskManager;
import javax.inject.Inject;
import mortar.Mortar;

public class GestureEngine extends LinearLayout {
    @Inject
    GestureServiceConnector gestureServiceConnector;

    public GestureEngine(Context context) {
        this(context, null);
    }

    public GestureEngine(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        ((CheckBox) findViewById(R.id.enable_gesture)).setChecked(this.gestureServiceConnector.isRunning());
    }

    @OnCheckedChanged({R.id.enable_gesture})
    public void onToggleGesture(final boolean checked) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (checked) {
                    GestureEngine.this.gestureServiceConnector.start();
                } else {
                    GestureEngine.this.gestureServiceConnector.stop();
                }
            }
        }, 1);
    }

    @OnCheckedChanged({R.id.enable_discrete_mode})
    public void onToggleDiscreteMode(boolean checked) {
        this.gestureServiceConnector.setDiscreteMode(checked);
    }

    @OnCheckedChanged({R.id.enable_preview})
    public void onTogglePreview(boolean checked) {
        this.gestureServiceConnector.enablePreview(checked);
    }
}
