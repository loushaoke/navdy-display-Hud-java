package com.navdy.hud.app.manager;

import android.content.SharedPreferences;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.framework.calendar.CalendarManager;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.framework.voice.VoiceSearchHandler;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.FeatureUtil;
import com.navdy.service.library.network.http.IHttpManager;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class RemoteDeviceManager$$InjectAdapter extends Binding<RemoteDeviceManager> implements MembersInjector<RemoteDeviceManager> {
    private Binding<Bus> bus;
    private Binding<CalendarManager> calendarManager;
    private Binding<CallManager> callManager;
    private Binding<ConnectionHandler> connectionHandler;
    private Binding<DriveRecorder> driveRecorder;
    private Binding<FeatureUtil> featureUtil;
    private Binding<GestureServiceConnector> gestureServiceConnector;
    private Binding<IHttpManager> httpManager;
    private Binding<InputManager> inputManager;
    private Binding<MusicManager> musicManager;
    private Binding<SharedPreferences> preferences;
    private Binding<TelemetryDataManager> telemetryDataManager;
    private Binding<TimeHelper> timeHelper;
    private Binding<TripManager> tripManager;
    private Binding<UIStateManager> uiStateManager;
    private Binding<VoiceSearchHandler> voiceSearchHandler;

    public RemoteDeviceManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.manager.RemoteDeviceManager", false, RemoteDeviceManager.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", RemoteDeviceManager.class, getClass().getClassLoader());
        this.connectionHandler = linker.requestBinding("com.navdy.hud.app.service.ConnectionHandler", RemoteDeviceManager.class, getClass().getClassLoader());
        this.callManager = linker.requestBinding("com.navdy.hud.app.framework.phonecall.CallManager", RemoteDeviceManager.class, getClass().getClassLoader());
        this.inputManager = linker.requestBinding("com.navdy.hud.app.manager.InputManager", RemoteDeviceManager.class, getClass().getClassLoader());
        this.preferences = linker.requestBinding("android.content.SharedPreferences", RemoteDeviceManager.class, getClass().getClassLoader());
        this.timeHelper = linker.requestBinding("com.navdy.hud.app.common.TimeHelper", RemoteDeviceManager.class, getClass().getClassLoader());
        this.calendarManager = linker.requestBinding("com.navdy.hud.app.framework.calendar.CalendarManager", RemoteDeviceManager.class, getClass().getClassLoader());
        this.gestureServiceConnector = linker.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", RemoteDeviceManager.class, getClass().getClassLoader());
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", RemoteDeviceManager.class, getClass().getClassLoader());
        this.tripManager = linker.requestBinding("com.navdy.hud.app.framework.trips.TripManager", RemoteDeviceManager.class, getClass().getClassLoader());
        this.driveRecorder = linker.requestBinding("com.navdy.hud.app.debug.DriveRecorder", RemoteDeviceManager.class, getClass().getClassLoader());
        this.musicManager = linker.requestBinding("com.navdy.hud.app.manager.MusicManager", RemoteDeviceManager.class, getClass().getClassLoader());
        this.voiceSearchHandler = linker.requestBinding("com.navdy.hud.app.framework.voice.VoiceSearchHandler", RemoteDeviceManager.class, getClass().getClassLoader());
        this.featureUtil = linker.requestBinding("com.navdy.hud.app.util.FeatureUtil", RemoteDeviceManager.class, getClass().getClassLoader());
        this.httpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", RemoteDeviceManager.class, getClass().getClassLoader());
        this.telemetryDataManager = linker.requestBinding("com.navdy.hud.app.analytics.TelemetryDataManager", RemoteDeviceManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.connectionHandler);
        injectMembersBindings.add(this.callManager);
        injectMembersBindings.add(this.inputManager);
        injectMembersBindings.add(this.preferences);
        injectMembersBindings.add(this.timeHelper);
        injectMembersBindings.add(this.calendarManager);
        injectMembersBindings.add(this.gestureServiceConnector);
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.tripManager);
        injectMembersBindings.add(this.driveRecorder);
        injectMembersBindings.add(this.musicManager);
        injectMembersBindings.add(this.voiceSearchHandler);
        injectMembersBindings.add(this.featureUtil);
        injectMembersBindings.add(this.httpManager);
        injectMembersBindings.add(this.telemetryDataManager);
    }

    public void injectMembers(RemoteDeviceManager object) {
        object.bus = (Bus) this.bus.get();
        object.connectionHandler = (ConnectionHandler) this.connectionHandler.get();
        object.callManager = (CallManager) this.callManager.get();
        object.inputManager = (InputManager) this.inputManager.get();
        object.preferences = (SharedPreferences) this.preferences.get();
        object.timeHelper = (TimeHelper) this.timeHelper.get();
        object.calendarManager = (CalendarManager) this.calendarManager.get();
        object.gestureServiceConnector = (GestureServiceConnector) this.gestureServiceConnector.get();
        object.uiStateManager = (UIStateManager) this.uiStateManager.get();
        object.tripManager = (TripManager) this.tripManager.get();
        object.driveRecorder = (DriveRecorder) this.driveRecorder.get();
        object.musicManager = (MusicManager) this.musicManager.get();
        object.voiceSearchHandler = (VoiceSearchHandler) this.voiceSearchHandler.get();
        object.featureUtil = (FeatureUtil) this.featureUtil.get();
        object.httpManager = (IHttpManager) this.httpManager.get();
        object.telemetryDataManager = (TelemetryDataManager) this.telemetryDataManager.get();
    }
}
