package com.navdy.hud.app.presenter;

import com.navdy.hud.app.ui.framework.BasePresenter;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class NotificationPresenter$$InjectAdapter extends Binding<NotificationPresenter> implements Provider<NotificationPresenter>, MembersInjector<NotificationPresenter> {
    private Binding<Bus> bus;
    private Binding<BasePresenter> supertype;

    public NotificationPresenter$$InjectAdapter() {
        super("com.navdy.hud.app.presenter.NotificationPresenter", "members/com.navdy.hud.app.presenter.NotificationPresenter", true, NotificationPresenter.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", NotificationPresenter.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", NotificationPresenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.supertype);
    }

    public NotificationPresenter get() {
        NotificationPresenter result = new NotificationPresenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(NotificationPresenter object) {
        object.bus = (Bus) this.bus.get();
        this.supertype.injectMembers(object);
    }
}
