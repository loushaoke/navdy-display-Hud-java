package com.navdy.hud.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.navdy.hud.app.HudApplication;

public class ShutdownReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        HudApplication.getApplication().shutdown();
    }
}
