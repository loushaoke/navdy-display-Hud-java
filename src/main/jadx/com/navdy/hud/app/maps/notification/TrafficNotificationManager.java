package com.navdy.hud.app.maps.notification;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.here.android.mpa.common.Image;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.DisplayJunction;
import com.navdy.hud.app.maps.MapEvents.HideSignPostJunction;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficDismissEvent;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficJamDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficRerouteDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficRerouteEvent;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapUtil.IImageLoadCallback;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.util.ImageUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class TrafficNotificationManager {
    private static final TrafficNotificationManager sInstance = new TrafficNotificationManager();
    public static final Logger sLogger = new Logger(TrafficNotificationManager.class);
    private Bus bus = RemoteDeviceManager.getInstance().getBus();
    private Handler handler = new Handler(Looper.getMainLooper());
    private final int junctionViewH;
    private final int junctionViewInflateH;
    private final int junctionViewInflateW;
    private final int junctionViewW;
    private DisplayJunction lastJunctionEvent;

    public static TrafficNotificationManager getInstance() {
        return sInstance;
    }

    private TrafficNotificationManager() {
        Resources resources = HudApplication.getAppContext().getResources();
        this.junctionViewW = resources.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_w);
        this.junctionViewH = resources.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_h);
        this.junctionViewInflateW = resources.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_inflate_w);
        this.junctionViewInflateH = resources.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_inflate_h);
        this.bus.register(this);
    }

    @Subscribe
    public void onLiveTrafficEvent(LiveTrafficEvent event) {
        if (GlanceHelper.isTrafficNotificationEnabled()) {
            NotificationManager notificationManager = NotificationManager.getInstance();
            boolean isHigherPriorityActive = notificationManager.isNotificationPresent(NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID) || notificationManager.isNotificationPresent(NotificationId.TRAFFIC_DELAY_NOTIFICATION_ID) || notificationManager.isNotificationPresent(NotificationId.TRAFFIC_JAM_NOTIFICATION_ID);
            if (isHigherPriorityActive) {
                sLogger.v("higher priority notification active ignore:" + event.type.name());
                return;
            }
            TrafficEventNotification notification = (TrafficEventNotification) notificationManager.getNotification(NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID);
            if (notification == null) {
                notification = new TrafficEventNotification(this.bus);
            }
            notification.setTrafficEvent(event);
            notificationManager.addNotification(notification);
            return;
        }
        sLogger.v("traffic notification disabled:" + event);
    }

    @Subscribe
    public void onLiveTrafficDismiss(LiveTrafficDismissEvent event) {
        NotificationManager.getInstance().removeNotification(NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID);
    }

    @Subscribe
    public void onTrafficDelayEvent(TrafficDelayEvent event) {
    }

    @Subscribe
    public void onTrafficDelayDismiss(TrafficDelayDismissEvent event) {
        NotificationManager.getInstance().removeNotification(NotificationId.TRAFFIC_DELAY_NOTIFICATION_ID);
    }

    @Subscribe
    public void onTrafficReroutePrompt(TrafficRerouteEvent event) {
        if (GlanceHelper.isTrafficNotificationEnabled()) {
            NotificationManager notificationManager = NotificationManager.getInstance();
            notificationManager.removeNotification(NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID);
            notificationManager.removeNotification(NotificationId.TRAFFIC_DELAY_NOTIFICATION_ID);
            notificationManager.removeNotification(NotificationId.TRAFFIC_JAM_NOTIFICATION_ID);
            RouteTimeUpdateNotification notification = (RouteTimeUpdateNotification) notificationManager.getNotification(NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID);
            if (notification == null) {
                notification = new RouteTimeUpdateNotification(NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID, NotificationType.FASTER_ROUTE, this.bus);
            }
            notification.setFasterRouteEvent(event);
            notificationManager.addNotification(notification);
            return;
        }
        sLogger.v("traffic notification disabled:" + event);
    }

    @Subscribe
    public void onTrafficReroutePromptDismiss(TrafficRerouteDismissEvent event) {
        removeFasterRouteNotifiation();
    }

    @Subscribe
    public void onTrafficJamProgress(TrafficJamProgressEvent event) {
        if (GlanceHelper.isTrafficNotificationEnabled()) {
            NotificationManager notificationManager = NotificationManager.getInstance();
            if (notificationManager.isNotificationPresent(NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID)) {
                sLogger.v("reroute notification active ignore jam event:" + event.remainingTime);
                return;
            }
            notificationManager.removeNotification(NotificationId.TRAFFIC_EVENT_NOTIFICATION_ID);
            notificationManager.removeNotification(NotificationId.TRAFFIC_DELAY_NOTIFICATION_ID);
            TrafficJamNotification notification = (TrafficJamNotification) notificationManager.getNotification(NotificationId.TRAFFIC_JAM_NOTIFICATION_ID);
            if (notification == null) {
                notification = new TrafficJamNotification(this.bus, event);
            }
            notification.setTrafficEvent(event);
            notificationManager.addNotification(notification);
            return;
        }
        sLogger.v("traffic notification disabled:" + event);
    }

    @Subscribe
    public void onTrafficJamProgressDismiss(TrafficJamDismissEvent event) {
        NotificationManager.getInstance().removeNotification(NotificationId.TRAFFIC_JAM_NOTIFICATION_ID);
    }

    public void removeFasterRouteNotifiation() {
        NotificationManager.getInstance().removeNotification(NotificationId.TRAFFIC_REROUTE_NOTIFICATION_ID);
    }

    @Subscribe
    public void onDisplayJunction(final DisplayJunction event) {
        try {
            sLogger.v("onDisplayJunction");
            if (event.junction == null) {
                sLogger.v("no junction image");
                return;
            }
            removeJunctionView();
            this.lastJunctionEvent = event;
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    HereMapUtil.loadImage(event.junction, TrafficNotificationManager.this.junctionViewInflateW, TrafficNotificationManager.this.junctionViewInflateH, new IImageLoadCallback() {
                        public void result(Image image, Bitmap bitmap) {
                            if (bitmap == null) {
                                try {
                                    TrafficNotificationManager.sLogger.w("junction view bitmap not loaded");
                                    return;
                                } catch (Throwable t) {
                                    TrafficNotificationManager.sLogger.e("junction view mode", t);
                                    return;
                                }
                            }
                            TrafficNotificationManager.sLogger.w("junction view bitmap loaded w=" + bitmap.getWidth() + " h=" + bitmap.getHeight() + " orig w=" + event.junction.getWidth() + " h=" + event.junction.getHeight());
                            if (TrafficNotificationManager.this.lastJunctionEvent != null) {
                                long l1 = SystemClock.elapsedRealtime();
                                Bitmap hue = ImageUtil.hueShift(bitmap, 90.0f);
                                if (hue == null) {
                                    TrafficNotificationManager.sLogger.w("hue shift not performed");
                                    bitmap.recycle();
                                    return;
                                }
                                long l2 = SystemClock.elapsedRealtime();
                                Bitmap saturation = ImageUtil.applySaturation(bitmap, 0.0f);
                                if (saturation == null) {
                                    TrafficNotificationManager.sLogger.w("saturation not performed");
                                    bitmap.recycle();
                                    hue.recycle();
                                    return;
                                }
                                long l3 = SystemClock.elapsedRealtime();
                                bitmap.recycle();
                                final Bitmap combined = ImageUtil.blend(hue, saturation);
                                saturation.recycle();
                                hue.recycle();
                                if (combined == null) {
                                    TrafficNotificationManager.sLogger.w("combine not performed");
                                    return;
                                }
                                long l4 = SystemClock.elapsedRealtime();
                                TrafficNotificationManager.sLogger.i("junction view bitmap took[" + (l4 - l1) + "] hue [" + (l2 - l1) + "] sat[" + (l3 - l2) + "] blend[" + (l4 - l3) + "]");
                                TrafficNotificationManager.this.handler.post(new Runnable() {
                                    public void run() {
                                        try {
                                            HomeScreenView homeScreenView = RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                                            View view = LayoutInflater.from(homeScreenView.getContext()).inflate(R.layout.junction_view_lyt, null);
                                            ImageView imageView = (ImageView) view.findViewById(R.id.image);
                                            LayoutParams params = new LayoutParams(TrafficNotificationManager.this.junctionViewW, TrafficNotificationManager.this.junctionViewH);
                                            params.gravity = 83;
                                            view.setLayoutParams(params);
                                            imageView.setImageBitmap(combined);
                                            homeScreenView.injectRightSection(view);
                                            TrafficNotificationManager.sLogger.i("junction view mode injected");
                                        } catch (Throwable t) {
                                            TrafficNotificationManager.sLogger.e("junction view mode", t);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }, 10);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    @Subscribe
    public void onHideJunctionSignPost(HideSignPostJunction event) {
        sLogger.v("onHideJunctionSignPost");
        this.lastJunctionEvent = null;
        removeJunctionView();
    }

    private void removeJunctionView() {
        this.handler.post(new Runnable() {
            public void run() {
                try {
                    RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView().ejectRightSection();
                    TrafficNotificationManager.sLogger.i("junction view mode Ejected");
                } catch (Throwable t) {
                    TrafficNotificationManager.sLogger.e("junction view mode", t);
                }
            }
        });
    }
}
