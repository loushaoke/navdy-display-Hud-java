package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutingError;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.GeocodeRequest;
import com.here.android.mpa.search.Location;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.analytics.NavigationQualityTracker;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent;
import com.navdy.hud.app.maps.MapEvents.RouteCalculationState;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.hud.app.maps.here.HerePlacesManager.GeoCodeCallback;
import com.navdy.hud.app.maps.here.HereRouteCache.RouteInfo;
import com.navdy.hud.app.maps.here.HereRouteCalculator.RouteCalculatorListener;
import com.navdy.hud.app.maps.notification.RouteCalculationNotification;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteCancelRequest;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationRouteStatus;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.service.library.events.navigation.NavigationSessionRequest.Builder;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.RouteManeuver;
import com.navdy.service.library.events.navigation.RouteManeuverRequest;
import com.navdy.service.library.events.navigation.RouteManeuverResponse;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

public class HereRouteManager {
    private static final String EMPTY_STR = "";
    public static final long MAX_ALLOWED_NAV_DISPLAY_POSITION_DIFF = 160934;
    private static final double MAX_DISTANCE = 1.5E7d;
    private static final int MAX_ONLINE_ROUTE_CALCULATION_TIME = ((int) TimeUnit.SECONDS.toMillis(20));
    private static final int MAX_ROUTES = SystemProperties.getInt(MAX_ROUTES_PROP, 3);
    private static final String MAX_ROUTES_PROP = "persist.sys.routes.max";
    private static final boolean VERBOSE = false;
    private static volatile String activeGeoCalcId;
    private static NavigationRouteRequest activeGeoRouteRequest;
    private static volatile String activeRouteCalcId;
    private static HereRouteCalculator activeRouteCalculator;
    private static boolean activeRouteCancelledByUser;
    private static GeocodeRequest activeRouteGeocodeRequest;
    private static int activeRouteProgress;
    private static NavigationRouteRequest activeRouteRequest;
    private static final Bus bus = RemoteDeviceManager.getInstance().getBus();
    private static final Context context = HudApplication.getAppContext();
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static final HereMapsManager hereMapsManager = HereMapsManager.getInstance();
    private static final Object lockObj = new Object();
    private static final MapsEventHandler mapsEventHandler = MapsEventHandler.getInstance();
    private static final HashSet<String> pendingRouteRequestIds = new HashSet();
    private static final Queue<NavigationRouteRequest> pendingRouteRequestQueue = new LinkedList();
    private static RouteCalculationEvent routeCalculationEvent;
    private static final Logger sLogger = new Logger(HereRouteManager.class);

    public static void handleRouteRequest(final NavigationRouteRequest routeRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                try {
                    HereRouteManager.printRouteRequest(routeRequest);
                    if (HereRouteManager.hereMapsManager.isInitialized()) {
                        HereLocationFixManager hereLocationFixManager = HereRouteManager.hereMapsManager.getLocationFixManager();
                        if (hereLocationFixManager == null) {
                            HereRouteManager.sLogger.i("engine not ready, location fix manager n/a");
                            HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_NOT_READY, routeRequest, HereRouteManager.context.getString(R.string.map_engine_not_ready));
                            return;
                        } else if (NetworkStateManager.getInstance().isNetworkStateInitialized()) {
                            GeoCoordinate geoPosition = HereRouteManager.hereMapsManager.getRouteStartPoint();
                            if (geoPosition == null) {
                                geoPosition = hereLocationFixManager.getLastGeoCoordinate();
                            } else {
                                HereRouteManager.sLogger.v("using debug start point:" + geoPosition);
                            }
                            if (geoPosition == null) {
                                HereRouteManager.sLogger.i("start location n/a");
                                HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_NO_LOCATION_SERVICE, routeRequest, HereRouteManager.context.getString(R.string.no_current_position));
                                return;
                            } else if (routeRequest.requestId == null) {
                                HereRouteManager.sLogger.i("no request id specified");
                                HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_INVALID_REQUEST, routeRequest, "no route id");
                                return;
                            } else {
                                HereRouteManager.sLogger.i("reset traffic reroute listener");
                                HereNavigationManager.getInstance().resetTrafficRerouteListener();
                                synchronized (HereRouteManager.lockObj) {
                                    String activeRouteId = null;
                                    if (HereRouteManager.activeRouteRequest != null || HereRouteManager.isUIShowingRouteCalculation()) {
                                        if (HereMapUtil.isSavedRoute(routeRequest)) {
                                            HereRouteManager.sLogger.i("saved route cannot override an existing route calculation");
                                            HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, routeRequest, null);
                                            return;
                                        } else if (HereRouteManager.activeRouteRequest != null) {
                                            activeRouteId = HereRouteManager.activeRouteRequest.requestId;
                                        }
                                    }
                                    if (HereRouteManager.pendingRouteRequestIds.contains(routeRequest.requestId) || TextUtils.equals(routeRequest.requestId, activeRouteId)) {
                                        HereRouteManager.sLogger.v("route request[" + routeRequest.requestId + "] already received");
                                        return;
                                    }
                                }
                            }
                        } else {
                            HereRouteManager.sLogger.i("network state is not initialized yet");
                            HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_NOT_READY, routeRequest, HereRouteManager.context.getString(R.string.network_not_initialized));
                            return;
                        }
                    }
                    HereRouteManager.sLogger.i("engine not ready");
                    HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_NOT_READY, routeRequest, HereRouteManager.context.getString(R.string.map_engine_not_ready));
                } catch (Throwable t) {
                    HereRouteManager.sLogger.e("", t);
                    HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_UNKNOWN_ERROR, routeRequest, HereRouteManager.context.getString(R.string.unknown_error));
                }
            }
        }, 19);
    }

    private static void handleRouteRequest(NavigationRouteRequest request, GeoCoordinate startPoint) {
        GeoCoordinate geoCheck;
        boolean b;
        long l1 = SystemClock.elapsedRealtime();
        List<GeoCoordinate> waypoints = new ArrayList();
        if (request.waypoints != null) {
            for (Coordinate waypoint : request.waypoints) {
                waypoints.add(new GeoCoordinate(waypoint.latitude.doubleValue(), waypoint.longitude.doubleValue()));
            }
        }
        if (request.destinationDisplay == null || request.destinationDisplay.latitude.doubleValue() == 0.0d || request.destinationDisplay.longitude.doubleValue() == 0.0d || request.destination.latitude == request.destinationDisplay.latitude || request.destination.longitude == request.destinationDisplay.longitude) {
            sLogger.v("display/nav distance check pass: no display pos");
            geoCheck = new GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
        } else {
            long d = (long) new GeoCoordinate(request.destinationDisplay.latitude.doubleValue(), request.destinationDisplay.longitude.doubleValue()).distanceTo(new GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue()));
            if (d >= MAX_ALLOWED_NAV_DISPLAY_POSITION_DIFF) {
                geoCheck = new GeoCoordinate(request.destinationDisplay.latitude.doubleValue(), request.destinationDisplay.longitude.doubleValue());
                sLogger.e("display/nav distance check failed:" + d);
                AnalyticsSupport.recordBadRoutePosition(request.destinationDisplay.latitude + HereManeuverDisplayBuilder.COMMA + request.destinationDisplay.longitude, request.destination.latitude + HereManeuverDisplayBuilder.COMMA + request.destination.longitude, request.streetAddress);
            } else {
                sLogger.v("display/nav distance check pass:" + d);
                geoCheck = new GeoCoordinate(request.destination.latitude.doubleValue(), request.destination.longitude.doubleValue());
            }
        }
        GeoCoordinate endPoint = geoCheck;
        sLogger.v("handleRouteRequest start=" + startPoint + ", end=" + endPoint);
        boolean geoCode = false;
        if (!Boolean.TRUE.equals(request.geoCodeStreetAddress)) {
            double distance = startPoint.distanceTo(endPoint);
            if (distance > MAX_DISTANCE) {
                sLogger.e("distance between start and endpoint:" + distance + " max:" + MAX_DISTANCE);
                returnErrorResponse(RequestStatus.REQUEST_INVALID_REQUEST, request, context.getString(R.string.long_drive_error));
                return;
            }
        } else if (TextUtils.isEmpty(request.streetAddress)) {
            returnErrorResponse(RequestStatus.REQUEST_INVALID_REQUEST, request, context.getString(R.string.unknown_address));
            return;
        } else {
            geoCode = true;
        }
        if (hereMapsManager.isEngineOnline()) {
            b = true;
        } else {
            b = false;
        }
        final boolean factoringInTraffic = b;
        sLogger.v("maps engine online:" + factoringInTraffic);
        if (geoCode) {
            sLogger.v("Street Address:" + request.streetAddress);
            synchronized (lockObj) {
                if (isRouteCalcInProgress()) {
                    if (request.cancelCurrent == null || !request.cancelCurrent.booleanValue()) {
                        returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, request, activeRouteCalcId);
                        return;
                    } else if (activeRouteCalculator != null) {
                        sLogger.v("geo:route cancelling current request :" + activeRouteCalcId);
                        activeRouteCalculator.cancel();
                        activeRouteCalculator = null;
                        returnErrorResponse(RequestStatus.REQUEST_CANCELLED, activeRouteRequest, activeRouteCalcId);
                    }
                }
                if (isRouteCalcGeocodeRequestInProgress()) {
                    if (request.cancelCurrent == null || !request.cancelCurrent.booleanValue()) {
                        returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, request, activeGeoCalcId);
                        return;
                    }
                    cancelCurrentGeocodeRequest();
                }
                activeGeoCalcId = request.requestId;
                activeGeoRouteRequest = request;
                final NavigationRouteRequest navigationRouteRequest = request;
                final GeoCoordinate geoCoordinate = startPoint;
                final List<GeoCoordinate> list = waypoints;
                activeRouteGeocodeRequest = HerePlacesManager.geoCodeStreetAddress(startPoint, request.streetAddress, 100000, new GeoCodeCallback() {
                    public void result(ErrorCode errorCode, List<Location> locationList) {
                        try {
                            if (errorCode == ErrorCode.CANCELLED) {
                                HereRouteManager.sLogger.v("geocode request has been cancelled [" + navigationRouteRequest.requestId + "]");
                                return;
                            }
                            boolean matchesCurrentRequestId;
                            synchronized (HereRouteManager.lockObj) {
                                matchesCurrentRequestId = TextUtils.equals(navigationRouteRequest.requestId, HereRouteManager.activeGeoCalcId);
                            }
                            if (matchesCurrentRequestId) {
                                synchronized (HereRouteManager.lockObj) {
                                    HereRouteManager.activeRouteGeocodeRequest = null;
                                    HereRouteManager.activeGeoCalcId = null;
                                    HereRouteManager.activeGeoRouteRequest = null;
                                }
                                if (errorCode != ErrorCode.NONE || locationList == null || locationList.size() <= 0) {
                                    HereRouteManager.sLogger.v("could not geocode, Street Address placesSearch error:" + errorCode.name());
                                    HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_SERVICE_ERROR, navigationRouteRequest, HereRouteManager.context.getString(R.string.geocoding_failed));
                                    return;
                                }
                                HereRouteManager.sLogger.v("Street Address results size=" + locationList.size());
                                GeoCoordinate newEndPoint = ((Location) locationList.get(0)).getCoordinate();
                                HereRouteManager.sLogger.v("Street Address placesSearch success: new-end-geo:" + newEndPoint);
                                HereRouteManager.routeSearch(navigationRouteRequest, geoCoordinate, list, newEndPoint, factoringInTraffic);
                                return;
                            }
                            HereRouteManager.sLogger.v("geocode request is not valid active:" + HereRouteManager.activeGeoCalcId + " request:" + navigationRouteRequest.requestId);
                            MapsEventHandler.getInstance().sendEventToClient(new NavigationRouteResponse(RequestStatus.REQUEST_CANCELLED, null, navigationRouteRequest.destination, null, null, Boolean.valueOf(false), navigationRouteRequest.requestId));
                        } catch (Throwable t) {
                            HereRouteManager.sLogger.v("Street Address placesSearch exception", t);
                            HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_UNKNOWN_ERROR, navigationRouteRequest, HereRouteManager.context.getString(R.string.geocoding_failed));
                        }
                    }
                });
                if (activeRouteGeocodeRequest == null) {
                    activeRouteGeocodeRequest = null;
                    activeGeoCalcId = null;
                    sLogger.v("geocode request failed");
                } else {
                    sLogger.v("geocode request triggered");
                }
            }
        } else {
            sLogger.v("geocoding not set");
            routeSearch(request, startPoint, waypoints, endPoint, factoringInTraffic);
        }
        sLogger.v("handleRouteRequest- time-1 =" + (SystemClock.elapsedRealtime() - l1));
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void routeSearch(NavigationRouteRequest request, GeoCoordinate startPoint, List<GeoCoordinate> waypoints, GeoCoordinate endPoint, boolean factoringInTraffic) {
        synchronized (lockObj) {
            if (isRouteCalcGeocodeRequestInProgress()) {
                if (request.cancelCurrent == null || !request.cancelCurrent.booleanValue()) {
                    returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, request, activeGeoCalcId);
                    return;
                }
                cancelCurrentGeocodeRequest();
            }
            if (isRouteCalcInProgress()) {
                if (request.cancelCurrent == null || !request.cancelCurrent.booleanValue()) {
                    returnErrorResponse(RequestStatus.REQUEST_ALREADY_IN_PROGRESS, request, activeRouteCalcId);
                } else {
                    pendingRouteRequestQueue.add(request);
                    pendingRouteRequestIds.add(request.requestId);
                    sLogger.v("add to pending Queue");
                    if (activeRouteCancelledByUser) {
                        sLogger.v("cancellation in progress");
                        return;
                    }
                    sLogger.v("cancelling current request :" + activeRouteCalcId);
                    activeRouteCancelledByUser = true;
                    activeRouteCalculator.cancel();
                }
            }
        }
    }

    private static void routeSearchTraffic(NavigationRouteRequest request, GeoCoordinate startPoint, List<GeoCoordinate> waypoints, GeoCoordinate endPoint, boolean inProgressRouteCheck) {
        final HereRouteCalculator traffic = new HereRouteCalculator(sLogger, false);
        if (inProgressRouteCheck) {
            synchronized (lockObj) {
                activeRouteCalcId = request.requestId;
                activeRouteRequest = request;
                activeRouteProgress = 0;
                activeRouteCalculator = traffic;
                returnStatusResponse(activeRouteCalcId, activeRouteProgress, activeRouteRequest.requestId);
            }
        }
        final Runnable runnable = new Runnable() {
            public void run() {
                HereRouteManager.sLogger.w("cancel traffic based route,max time reached:" + HereRouteManager.MAX_ONLINE_ROUTE_CALCULATION_TIME);
                traffic.cancel();
            }
        };
        sLogger.v("traffic route calc started");
        handler.postDelayed(runnable, (long) MAX_ONLINE_ROUTE_CALCULATION_TIME);
        RouteOptions routeOptions = hereMapsManager.getRouteOptions();
        synchronized (lockObj) {
            routeCalculationEvent = new RouteCalculationEvent();
            routeCalculationEvent.state = RouteCalculationState.STARTED;
            routeCalculationEvent.start = startPoint;
            routeCalculationEvent.waypoints = waypoints;
            routeCalculationEvent.end = endPoint;
            routeCalculationEvent.request = request;
            routeCalculationEvent.routeOptions = routeOptions;
            bus.post(routeCalculationEvent);
        }
        final NavigationRouteRequest navigationRouteRequest = request;
        final GeoCoordinate geoCoordinate = startPoint;
        final List<GeoCoordinate> list = waypoints;
        final GeoCoordinate geoCoordinate2 = endPoint;
        AnonymousClass4 anonymousClass4 = new RouteCalculatorListener() {
            public void postSuccess(ArrayList<NavigationRouteResult> outgoingResults) {
                HereRouteManager.sLogger.v("got result for traffic");
                HereRouteManager.returnSuccessResponse(navigationRouteRequest, outgoingResults, true);
            }

            public void error(RoutingError error, Throwable t) {
                HereRouteManager.handler.removeCallbacks(runnable);
                if (error != null) {
                    HereRouteManager.sLogger.v("got error for traffic [" + error.name() + "]");
                } else {
                    HereRouteManager.sLogger.v("got error for traffic", t);
                }
                synchronized (HereRouteManager.lockObj) {
                    if (HereRouteManager.activeRouteCancelledByUser) {
                        HereRouteManager.sLogger.v("user cancelled routing");
                        HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_CANCELLED, navigationRouteRequest, null);
                        return;
                    }
                    HereRouteManager.sLogger.v("launching no traffic");
                    HereRouteManager.routeSearchNoTraffic(navigationRouteRequest, geoCoordinate, list, geoCoordinate2, false);
                }
            }

            public void progress(int progress) {
            }

            public void preSuccess() {
                synchronized (HereRouteManager.lockObj) {
                    HereRouteManager.activeRouteCalculator = null;
                }
                HereRouteManager.handler.removeCallbacks(runnable);
                HereRouteManager.sLogger.v("route successfully calculated, post calc in progress");
            }
        };
        traffic.calculateRoute(request, startPoint, waypoints, endPoint, true, anonymousClass4, MAX_ROUTES, routeOptions, true);
    }

    private static void routeSearchNoTraffic(final NavigationRouteRequest request, GeoCoordinate startPoint, List<GeoCoordinate> waypoints, GeoCoordinate endPoint, boolean newRequest) {
        HereRouteCalculator noTraffic = new HereRouteCalculator(sLogger, false);
        RouteOptions routeOptions = hereMapsManager.getRouteOptions();
        if (newRequest) {
            activeRouteCalcId = request.requestId;
            activeRouteRequest = request;
            activeRouteProgress = 0;
            activeRouteCalculator = noTraffic;
            returnStatusResponse(activeRouteCalcId, activeRouteProgress, activeRouteRequest.requestId);
            synchronized (lockObj) {
                routeCalculationEvent = new RouteCalculationEvent();
                routeCalculationEvent.state = RouteCalculationState.STARTED;
                routeCalculationEvent.start = startPoint;
                routeCalculationEvent.waypoints = waypoints;
                routeCalculationEvent.end = endPoint;
                routeCalculationEvent.request = request;
                routeCalculationEvent.routeOptions = routeOptions;
                bus.post(routeCalculationEvent);
            }
        } else {
            synchronized (lockObj) {
                if (activeRouteCalculator != null) {
                    sLogger.v("replaced active route calc with no traffic");
                    activeRouteCalculator = noTraffic;
                }
            }
        }
        sLogger.v("no-traffic route calc started");
        noTraffic.calculateRoute(request, startPoint, waypoints, endPoint, false, new RouteCalculatorListener() {
            public void postSuccess(ArrayList<NavigationRouteResult> outgoingResults) {
                HereRouteManager.returnSuccessResponse(request, outgoingResults, false);
            }

            public void error(RoutingError error, Throwable t) {
                String errorStr;
                if (error != null) {
                    errorStr = error.name();
                } else {
                    errorStr = HereRouteManager.context.getString(R.string.unknown_error);
                }
                synchronized (HereRouteManager.lockObj) {
                    if (HereRouteManager.activeRouteCancelledByUser) {
                        HereRouteManager.sLogger.v("user cancelled routing");
                        HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_CANCELLED, request, null);
                    } else {
                        HereRouteManager.returnErrorResponse(RequestStatus.REQUEST_SERVICE_ERROR, request, errorStr);
                    }
                }
            }

            public void progress(int progress) {
            }

            public void preSuccess() {
                synchronized (HereRouteManager.lockObj) {
                    HereRouteManager.activeRouteCalculator = null;
                }
            }
        }, MAX_ROUTES, routeOptions, true);
    }

    private static void cancelCurrentGeocodeRequest() {
        synchronized (lockObj) {
            if (activeRouteGeocodeRequest != null) {
                try {
                    activeRouteGeocodeRequest.cancel();
                    sLogger.v("geo: geocode request cancelled:" + activeGeoCalcId);
                    if (activeGeoRouteRequest != null) {
                        returnErrorResponse(RequestStatus.REQUEST_CANCELLED, activeGeoRouteRequest, null);
                    }
                } catch (Throwable t) {
                    sLogger.e(t);
                }
                activeRouteGeocodeRequest = null;
                activeGeoCalcId = null;
                activeGeoRouteRequest = null;
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void returnErrorResponse(RequestStatus status, NavigationRouteRequest request, String errorText) {
        try {
            sLogger.e(status + ": " + errorText);
            NavigationRouteResponse response = new NavigationRouteResponse(status, errorText, request.destination, null, null, Boolean.valueOf(false), request.requestId);
            MapsEventHandler.getInstance().sendEventToClient(response);
            synchronized (lockObj) {
                if (status != RequestStatus.REQUEST_ALREADY_IN_PROGRESS) {
                    if (routeCalculationEvent != null) {
                        routeCalculationEvent.response = response;
                        routeCalculationEvent.state = RouteCalculationState.STOPPED;
                        bus.post(routeCalculationEvent);
                    }
                }
            }
        } catch (Throwable t) {
            try {
                sLogger.e(t);
                if (1 != null) {
                    clearActiveRouteCalc();
                }
            } catch (Throwable th) {
                if (1 != null) {
                    clearActiveRouteCalc();
                }
            }
        }
    }

    private static void returnStatusResponse(String handle, int progress, String requestId) {
        try {
            sLogger.v("handle[" + handle + "] progress [" + progress + "]");
            MapsEventHandler.getInstance().sendEventToClient(new NavigationRouteStatus(handle, Integer.valueOf(progress), requestId));
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private static void returnSuccessResponse(NavigationRouteRequest request, ArrayList<NavigationRouteResult> outgoingResults, boolean factoringInTraffic) {
        try {
            NavigationRouteResponse navigationRouteResponse = new NavigationRouteResponse(RequestStatus.REQUEST_SUCCESS, "", request.destination, request.label, outgoingResults, Boolean.valueOf(factoringInTraffic), request.requestId);
            if (!factoringInTraffic) {
                TTSUtils.debugShowNoTrafficToast();
            }
            MapsEventHandler.getInstance().sendEventToClient(navigationRouteResponse);
            synchronized (lockObj) {
                if (routeCalculationEvent != null) {
                    routeCalculationEvent.response = navigationRouteResponse;
                    routeCalculationEvent.state = RouteCalculationState.STOPPED;
                    bus.post(routeCalculationEvent);
                }
            }
            sLogger.v("sent route response");
            if (request.autoNavigate != null && request.autoNavigate.booleanValue()) {
                boolean z;
                sLogger.i("start auto-navigation to route " + request.label);
                Builder simulationSpeed = new Builder().newState(NavigationSessionState.NAV_SESSION_STARTED).label(request.label).routeId(((NavigationRouteResult) outgoingResults.get(0)).routeId).simulationSpeed(Integer.valueOf(0));
                if (request.originDisplay == null) {
                    z = false;
                } else {
                    z = request.originDisplay.booleanValue();
                }
                NavigationSessionRequest navigationSessionRequest = simulationSpeed.originDisplay(Boolean.valueOf(z)).build();
                bus.post(navigationSessionRequest);
                bus.post(new RemoteEvent(navigationSessionRequest));
            }
            NavigationQualityTracker.getInstance().trackCalculationWithTraffic(factoringInTraffic);
            clearActiveRouteCalc();
        } catch (Throwable t) {
            try {
                sLogger.e(t);
            } finally {
                clearActiveRouteCalc();
            }
        }
    }

    public static void handleRouteManeuverRequest(final RouteManeuverRequest routeManeuverRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    if (routeManeuverRequest.routeId != null) {
                        long l1 = SystemClock.elapsedRealtime();
                        RouteInfo routeInfo = HereRouteCache.getInstance().getRoute(routeManeuverRequest.routeId);
                        if (routeInfo != null) {
                            String streetAddress = null;
                            if (routeInfo.routeRequest != null) {
                                streetAddress = routeInfo.routeRequest.streetAddress;
                            }
                            List<Maneuver> list = routeInfo.route.getManeuvers();
                            List<RouteManeuver> arrayList = new ArrayList(20);
                            int len = list.size();
                            Maneuver previous = null;
                            Maneuver current = null;
                            for (int i = 0; i < len; i++) {
                                ManeuverDisplay display;
                                if (current != null) {
                                    previous = current;
                                }
                                current = (Maneuver) list.get(i);
                                Maneuver after = null;
                                if (i < len - 1) {
                                    after = (Maneuver) list.get(i + 1);
                                }
                                int maneuverTime = 0;
                                if (i == 0) {
                                    display = HereManeuverDisplayBuilder.getStartManeuverDisplay(current, after);
                                } else {
                                    display = HereManeuverDisplayBuilder.getManeuverDisplay(current, after == null, (long) current.getDistanceToNextManeuver(), streetAddress, previous, routeInfo.routeRequest, after, true, false, null);
                                    if (after != null) {
                                        maneuverTime = (int) ((after.getStartTime().getTime() - current.getStartTime().getTime()) / 1000);
                                    }
                                }
                                arrayList.add(new RouteManeuver(display.currentRoad, display.navigationTurn, display.pendingRoad, Float.valueOf(display.distance), display.distanceUnit, Integer.valueOf(maneuverTime)));
                            }
                            HereRouteManager.sLogger.v("Time to build maneuver list:" + (SystemClock.elapsedRealtime() - l1));
                            HereRouteManager.mapsEventHandler.sendEventToClient(new RouteManeuverResponse(RequestStatus.REQUEST_SUCCESS, null, arrayList));
                            return;
                        }
                    }
                    HereRouteManager.mapsEventHandler.sendEventToClient(new RouteManeuverResponse(RequestStatus.REQUEST_INVALID_REQUEST, null, null));
                } catch (Throwable t) {
                    HereRouteManager.mapsEventHandler.sendEventToClient(new RouteManeuverResponse(RequestStatus.REQUEST_SERVICE_ERROR, t.toString(), null));
                    HereRouteManager.sLogger.e(t);
                }
            }
        }, 2);
    }

    private static boolean isRouteCalcInProgress() {
        boolean z;
        synchronized (lockObj) {
            if (activeRouteCalculator != null) {
                sLogger.i("route calculation already in progress:" + activeRouteCalcId);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    private static boolean isRouteCalcGeocodeRequestInProgress() {
        boolean z;
        synchronized (lockObj) {
            if (activeRouteGeocodeRequest != null) {
                sLogger.i("route calculation geocode already in progress:" + activeRouteCalcId);
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static void clearActiveRouteCalc(String id) {
        synchronized (lockObj) {
            if (TextUtils.equals(id, activeRouteCalcId)) {
                clearActiveRouteCalc();
            }
        }
    }

    public static void clearActiveRouteCalc() {
        synchronized (lockObj) {
            sLogger.v("clearActiveRouteCalc");
            activeRouteCalculator = null;
            activeRouteCalcId = null;
            activeRouteRequest = null;
            activeRouteProgress = 0;
            activeRouteCancelledByUser = false;
            routeCalculationEvent = null;
            int len = pendingRouteRequestQueue.size();
            if (len > 0) {
                sLogger.v("pending route queue size:" + len);
                final NavigationRouteRequest pendingRequest = (NavigationRouteRequest) pendingRouteRequestQueue.remove();
                pendingRouteRequestIds.remove(pendingRequest.requestId);
                handler.post(new Runnable() {
                    public void run() {
                        HereRouteManager.sLogger.v("launched pending request");
                        HereRouteManager.handleRouteRequest(pendingRequest);
                    }
                });
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean handleRouteCancelRequest(NavigationRouteCancelRequest routeCancelRequest, boolean originDisplay) {
        RouteCalculationEvent event;
        try {
            sLogger.v("routeCancelRequest:" + routeCancelRequest.handle);
            if (routeCancelRequest.handle == null) {
                sLogger.v("routeCancelRequest:null handle");
                return false;
            }
            synchronized (lockObj) {
                if (activeRouteGeocodeRequest != null && TextUtils.equals(routeCancelRequest.handle, activeGeoCalcId)) {
                    cancelCurrentGeocodeRequest();
                    sLogger.v("routeCancelRequest: sent navrouteresponse-geocode cancel [" + routeCancelRequest.handle + "]");
                    bus.post(new RemoteEvent(new NavigationRouteResponse.Builder().requestId(routeCancelRequest.handle).status(RequestStatus.REQUEST_CANCELLED).destination(new Coordinate.Builder().latitude(Double.valueOf(0.0d)).longitude(Double.valueOf(0.0d)).build()).build()));
                    if (null == null) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        event = new RouteCalculationEvent();
                        event.pendingNavigationRequestId = routeCancelRequest.handle;
                        event.abortOriginDisplay = originDisplay;
                        event.state = RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event);
                    }
                } else if (activeRouteCalculator == null) {
                    sLogger.v("no active route calculation:" + routeCancelRequest.handle);
                    if (null == null) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        event = new RouteCalculationEvent();
                        event.pendingNavigationRequestId = routeCancelRequest.handle;
                        event.abortOriginDisplay = originDisplay;
                        event.state = RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event);
                    }
                } else if (!TextUtils.equals(routeCancelRequest.handle, activeRouteCalcId)) {
                    sLogger.v("invalid active route id:" + routeCancelRequest.handle);
                    if (null == null) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        event = new RouteCalculationEvent();
                        event.pendingNavigationRequestId = routeCancelRequest.handle;
                        event.abortOriginDisplay = originDisplay;
                        event.state = RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event);
                    }
                } else if (activeRouteCancelledByUser) {
                    sLogger.v("request already cancelled:" + routeCancelRequest.handle);
                    if (null == null) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        event = new RouteCalculationEvent();
                        event.pendingNavigationRequestId = routeCancelRequest.handle;
                        event.abortOriginDisplay = originDisplay;
                        event.state = RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event);
                    }
                } else {
                    if (originDisplay) {
                        sLogger.v("send cancel to client");
                        bus.post(new RemoteEvent(routeCancelRequest));
                    }
                    activeRouteCancelledByUser = true;
                    activeRouteCalculator.cancel();
                    sLogger.v("called cancel");
                    sLogger.v("routeCancelRequest: sent navrouteresponse cancel [" + routeCancelRequest.handle + "]");
                    bus.post(new RemoteEvent(new NavigationRouteResponse.Builder().requestId(routeCancelRequest.handle).status(RequestStatus.REQUEST_CANCELLED).destination(new Coordinate.Builder().latitude(Double.valueOf(0.0d)).longitude(Double.valueOf(0.0d)).build()).build()));
                    if (!true) {
                        sLogger.v("may be pending route exists: " + routeCancelRequest.handle);
                        event = new RouteCalculationEvent();
                        event.pendingNavigationRequestId = routeCancelRequest.handle;
                        event.abortOriginDisplay = originDisplay;
                        event.state = RouteCalculationState.ABORT_NAVIGATION;
                        bus.post(event);
                    }
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
            return false;
        }
    }

    public static boolean cancelActiveRouteRequest() {
        if (!TextUtils.isEmpty(activeRouteCalcId)) {
            return handleRouteCancelRequest(new NavigationRouteCancelRequest(activeRouteCalcId), true);
        }
        sLogger.v("no active route id");
        return false;
    }

    public static void printRouteRequest(NavigationRouteRequest request) {
        String routeAttributesStr = null;
        try {
            List<RouteAttribute> routeAttributes = request.routeAttributes;
            if (routeAttributes != null && routeAttributes.size() > 0) {
                StringBuilder builder = new StringBuilder();
                for (RouteAttribute attribute : routeAttributes) {
                    builder.append(attribute.name());
                    builder.append(HereManeuverDisplayBuilder.COMMA);
                }
                routeAttributesStr = builder.toString();
            }
            sLogger.v("NavigationRouteRequest label[" + request.label + "] local[" + request.originDisplay + "] streetAddress[" + request.streetAddress + "] destination_id[" + request.destination_identifier + "] autonav[" + request.autoNavigate + "] geoCode[" + request.geoCodeStreetAddress + "] dest_coordinate[" + request.destination + "] favType[" + request.destinationType + "] requestId[" + request.requestId + "]" + "] display_coordinate[" + request.destinationDisplay + "] route attrs[" + routeAttributesStr + "]");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public static boolean isUIShowingRouteCalculation() {
        if (NotificationManager.getInstance().getNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID) != null) {
            return true;
        }
        return false;
    }

    public static String getActiveRouteCalcId() {
        return activeRouteCalcId;
    }

    public static void startNavigationLookup(Destination lookupDestination) {
        if (lookupDestination == null) {
            sLogger.e("startNavigationLookup null destination");
            return;
        }
        synchronized (lockObj) {
            routeCalculationEvent = new RouteCalculationEvent();
            routeCalculationEvent.state = RouteCalculationState.FINDING_NAV_COORDINATES;
            routeCalculationEvent.lookupDestination = lookupDestination;
            routeCalculationEvent.routeOptions = HereMapsManager.getInstance().getRouteOptions();
            sLogger.v("startNavigationLookup sent nav_coordinate_lookup ");
            bus.post(routeCalculationEvent);
        }
    }

    public static String buildStartRouteTTS() {
        return buildRouteTTS(R.string.route_start_msg, null);
    }

    public static String buildChangeRouteTTS(String routeId) {
        return buildRouteTTS(R.string.route_change_msg, routeId);
    }

    public static String buildRouteTTS(int id, String routeId) {
        if (!HereMapsManager.getInstance().isInitialized()) {
            return "";
        }
        HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
        Resources resources = HudApplication.getAppContext().getResources();
        String routeType = "";
        String eta = "";
        String via = "";
        HereRouteCache instance = HereRouteCache.getInstance();
        if (routeId == null) {
            routeId = hereNavigationManager.getCurrentRouteId();
        }
        RouteInfo info = instance.getRoute(routeId);
        if (info != null) {
            via = info.routeResult.via;
            if (Boolean.TRUE.equals(hereNavigationManager.getGeneratePhoneticTTS())) {
                via = "\u001b\\tn=address\\" + via + "\u001b\\tn=normal\\";
            }
            Date ttaDate = HereMapUtil.getRouteTtaDate(info.route);
            StringBuilder builder = new StringBuilder();
            eta = RemoteDeviceManager.getInstance().getTimeHelper().formatTime12Hour(ttaDate, builder, false) + " " + builder.toString();
        }
        switch (id) {
            case R.string.route_change_msg:
                return resources.getString(R.string.route_change_msg, new Object[]{via, eta});
            case R.string.route_start_msg:
                RouteOptions routeOptions = hereNavigationManager.getRouteOptions();
                if (routeOptions != null) {
                    switch (routeOptions.getRouteType()) {
                        case SHORTEST:
                            routeType = RouteCalculationNotification.shortestRoute;
                            break;
                        case FASTEST:
                            routeType = RouteCalculationNotification.fastestRoute;
                            break;
                    }
                }
                return resources.getString(R.string.route_start_msg, new Object[]{routeType, via, eta});
            default:
                return "";
        }
    }
}
