package com.navdy.hud.app.maps.here;

import android.text.TextUtils;
import com.here.android.mpa.guidance.NavigationManager.RerouteListener;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.analytics.NavigationQualityTracker;
import com.navdy.hud.app.maps.MapEvents.RerouteEvent;
import com.navdy.hud.app.maps.MapEvents.RouteEventType;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;

public class HereRerouteListener extends RerouteListener {
    public static final RerouteEvent REROUTE_FAILED = new RerouteEvent(RouteEventType.FAILED);
    public static final RerouteEvent REROUTE_FINISHED = new RerouteEvent(RouteEventType.FINISHED);
    public static final RerouteEvent REROUTE_STARTED = new RerouteEvent(RouteEventType.STARTED);
    private Bus bus;
    private HereNavigationManager hereNavigationManager;
    private Logger logger = new Logger(HereRerouteListener.class);
    private final NavigationQualityTracker navigationQualityTracker;
    private String routeCalcId;
    private volatile boolean routeCalculationOn;
    private String tag;

    HereRerouteListener(Logger logger, String tag, HereNavigationManager hereNavigationManager, Bus bus) {
        this.tag = tag;
        this.hereNavigationManager = hereNavigationManager;
        this.bus = bus;
        this.navigationQualityTracker = NavigationQualityTracker.getInstance();
    }

    public void onRerouteBegin() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                HereRerouteListener.this.routeCalcId = HereRerouteListener.this.hereNavigationManager.getCurrentRouteId();
                HereRerouteListener.this.logger.i(HereRerouteListener.this.tag + " reroute-begin: id=" + HereRerouteListener.this.routeCalcId);
                HereRerouteListener.this.routeCalculationOn = true;
                if (!(!HereRerouteListener.this.hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn || HereRerouteListener.this.hereNavigationManager.hasArrived() || MapSettings.isTtsRecalculationDisabled())) {
                    HereRerouteListener.this.bus.post(HereRerouteListener.this.hereNavigationManager.TTS_REROUTING);
                }
                HereRerouteListener.this.bus.post(HereRerouteListener.REROUTE_STARTED);
                HereRerouteListener.this.bus.post(HereManeuverDisplayBuilder.EMPTY_MANEUVER_DISPLAY);
            }
        }, 20);
    }

    public void onRerouteEnd(final Route route) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                String currentRouteId = HereRerouteListener.this.hereNavigationManager.getCurrentRouteId();
                String id = HereRerouteListener.this.routeCalcId;
                HereRerouteListener.this.logger.i(HereRerouteListener.this.tag + " reroute-end: " + route + " id=" + HereRerouteListener.this.routeCalcId + " current=" + currentRouteId);
                HereRerouteListener.this.routeCalcId = null;
                HereRerouteListener.this.routeCalculationOn = false;
                HereRerouteListener.this.bus.post(HereRerouteListener.REROUTE_FINISHED);
                if (route == null) {
                    return;
                }
                if (!HereRerouteListener.this.hereNavigationManager.isNavigationModeOn()) {
                    HereRerouteListener.this.logger.i(HereRerouteListener.this.tag + "reroute-end: navigation already ended, not adding route");
                } else if (TextUtils.equals(id, currentRouteId)) {
                    HereRerouteListener.this.hereNavigationManager.setReroute(route, RerouteReason.NAV_SESSION_OFF_REROUTE, null, null, false, HereRerouteListener.this.hereNavigationManager.isTrafficConsidered(currentRouteId));
                    HereRerouteListener.this.navigationQualityTracker.trackTripRecalculated();
                } else {
                    HereRerouteListener.this.logger.i("route change before recalc completed: cannot use");
                }
            }
        }, 20);
    }

    public void onRerouteFailed() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                HereRerouteListener.this.routeCalculationOn = false;
                HereRerouteListener.this.routeCalcId = null;
                HereRerouteListener.this.logger.w(HereRerouteListener.this.tag + " reroute-failed");
                if (!(!HereRerouteListener.this.hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn || HereRerouteListener.this.hereNavigationManager.hasArrived() || MapSettings.isTtsRecalculationDisabled())) {
                    HereRerouteListener.this.bus.post(HereRerouteListener.this.hereNavigationManager.TTS_REROUTING_FAILED);
                }
                HereRerouteListener.this.bus.post(HereRerouteListener.REROUTE_FAILED);
            }
        }, 20);
    }

    public boolean isRecalculating() {
        return this.routeCalculationOn;
    }
}
