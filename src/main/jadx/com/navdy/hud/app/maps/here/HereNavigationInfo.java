package com.navdy.hud.app.maps.here;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.navdy.hud.app.maps.MapEvents.DestinationDirection;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ManeuverState;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange.RerouteReason;
import java.util.List;

public class HereNavigationInfo {
    ManeuverDisplay arrivedManeuverDisplay;
    RerouteReason currentRerouteReason;
    GeoCoordinate destination;
    DestinationDirection destinationDirection;
    String destinationDirectionStr;
    int destinationIconId;
    public String destinationIdentifier;
    String destinationLabel;
    NavdyDeviceId deviceId;
    boolean firstManeuverShown;
    long firstManeuverShownTime;
    boolean hasArrived;
    boolean ignoreArrived;
    boolean lastManeuver;
    long lastManeuverPostTime;
    NavigationRouteRequest lastRequest;
    String lastRouteId;
    long lastTrafficRerouteTime;
    Maneuver maneuverAfterCurrent;
    int maneuverAfterCurrentIconid = -1;
    ManeuverState maneuverState;
    MapMarker mapDestinationMarker;
    MapRoute mapRoute;
    NavigationRouteRequest navigationRouteRequest;
    Route route;
    String routeId;
    RouteOptions routeOptions;
    int simulationSpeed;
    GeoCoordinate startLocation;
    String streetAddress;
    int trafficRerouteCount;
    boolean trafficRerouteOnce;
    List<GeoCoordinate> waypoints;

    void clear() {
        if (this.navigationRouteRequest != null) {
            this.lastRequest = this.navigationRouteRequest;
            this.lastRouteId = this.routeId;
        } else {
            this.lastRequest = null;
            this.lastRouteId = null;
        }
        this.navigationRouteRequest = null;
        this.routeId = null;
        this.route = null;
        this.simulationSpeed = -1;
        this.streetAddress = null;
        this.destinationLabel = null;
        this.destinationIdentifier = null;
        this.lastManeuverPostTime = 0;
        this.deviceId = null;
        this.destinationDirection = null;
        this.destinationDirectionStr = null;
        this.destinationIconId = -1;
        this.maneuverAfterCurrent = null;
        this.maneuverAfterCurrentIconid = -1;
        this.hasArrived = false;
        this.ignoreArrived = false;
        this.lastManeuver = false;
        this.arrivedManeuverDisplay = null;
        this.destination = null;
        this.trafficRerouteCount = 0;
        this.lastTrafficRerouteTime = 0;
        this.currentRerouteReason = null;
        this.trafficRerouteOnce = false;
        this.firstManeuverShown = false;
        this.firstManeuverShownTime = 0;
        this.routeOptions = null;
        this.startLocation = null;
    }

    void copy(HereNavigationInfo info) {
        clear();
        if (info != null) {
            this.navigationRouteRequest = info.navigationRouteRequest;
            if (this.navigationRouteRequest != null) {
                this.destination = new GeoCoordinate(this.navigationRouteRequest.destination.latitude.doubleValue(), this.navigationRouteRequest.destination.longitude.doubleValue());
            }
            this.routeId = info.routeId;
            this.routeOptions = info.routeOptions;
            this.route = info.route;
            this.simulationSpeed = info.simulationSpeed;
            this.streetAddress = info.streetAddress;
            this.destinationLabel = info.destinationLabel;
            this.destinationIdentifier = info.destinationIdentifier;
            this.deviceId = info.deviceId;
            this.destinationDirection = info.destinationDirection;
            this.destinationDirectionStr = info.destinationDirectionStr;
            this.destinationIconId = info.destinationIconId;
            this.maneuverAfterCurrent = info.maneuverAfterCurrent;
            this.maneuverAfterCurrentIconid = info.maneuverAfterCurrentIconid;
            this.lastRequest = null;
            this.lastRouteId = null;
            this.currentRerouteReason = null;
            this.trafficRerouteOnce = false;
            this.startLocation = info.startLocation;
        }
    }

    NavigationRouteRequest getLastNavigationRequest() {
        NavigationRouteRequest ret = this.lastRequest;
        this.lastRequest = null;
        return ret;
    }

    String getLastRouteId() {
        String ret = this.lastRouteId;
        this.lastRouteId = null;
        return ret;
    }

    int getTrafficReRouteCount() {
        return this.trafficRerouteCount;
    }

    long getLastTrafficRerouteTime() {
        return this.lastTrafficRerouteTime;
    }

    void setLastTrafficRerouteTime(long l) {
        this.lastTrafficRerouteTime = l;
    }

    void incrTrafficRerouteCount() {
        this.trafficRerouteCount++;
    }
}
