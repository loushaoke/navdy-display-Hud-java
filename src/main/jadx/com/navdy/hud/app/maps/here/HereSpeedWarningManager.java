package com.navdy.hud.app.maps.here;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.RoadElement;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.phonecall.CallUtils;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.service.library.events.audio.SpeechRequest.Category;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.concurrent.TimeUnit;

public class HereSpeedWarningManager {
    private static final int BUFFER_SPEED = 7;
    private static final long DISCARD_INTERVAL = TimeUnit.SECONDS.toMillis(10);
    private static final int PERIODIC_CHECK_INTERVAL = 5000;
    private static final Logger sLogger = new Logger(HereSpeedWarningManager.class);
    private Bus bus;
    private Runnable checkSpeedRunnable = new Runnable() {
        public void run() {
            try {
                if (SystemClock.elapsedRealtime() - HereSpeedWarningManager.this.lastUpdate >= 1000) {
                    HereSpeedWarningManager.this.checkSpeed();
                }
            } catch (Throwable t) {
                HereSpeedWarningManager.sLogger.e(t);
            }
        }
    };
    private Context context = HudApplication.getAppContext();
    private Handler handler;
    private HereNavigationManager hereNavigationManager;
    private long lastSpeedWarningTime;
    private long lastUpdate;
    private MapsEventHandler mapsEventHandler;
    private Runnable periodicCheckRunnable = new Runnable() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            try {
                if (HereSpeedWarningManager.this.registered) {
                    if (SystemClock.elapsedRealtime() - HereSpeedWarningManager.this.lastUpdate >= 5000) {
                        HereSpeedWarningManager.this.checkSpeed();
                        if (HereSpeedWarningManager.this.registered) {
                            HereSpeedWarningManager.this.handler.postDelayed(HereSpeedWarningManager.this.periodicRunnable, 5000);
                        }
                    } else if (HereSpeedWarningManager.this.registered) {
                        HereSpeedWarningManager.this.handler.postDelayed(HereSpeedWarningManager.this.periodicRunnable, 5000);
                    }
                } else if (HereSpeedWarningManager.this.registered) {
                    HereSpeedWarningManager.this.handler.postDelayed(HereSpeedWarningManager.this.periodicRunnable, 5000);
                }
            } catch (Throwable th) {
                if (HereSpeedWarningManager.this.registered) {
                    HereSpeedWarningManager.this.handler.postDelayed(HereSpeedWarningManager.this.periodicRunnable, 5000);
                }
            }
        }
    };
    private Runnable periodicRunnable = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(HereSpeedWarningManager.this.periodicCheckRunnable, 12);
        }
    };
    private boolean registered;
    private SpeedManager speedManager = SpeedManager.getInstance();
    private volatile boolean speedWarningOn;
    private String tag;
    private int warningSpeed;

    HereSpeedWarningManager(String tag, Bus bus, MapsEventHandler mapsEventHandler, HereNavigationManager hereNavigationManager) {
        this.tag = tag;
        this.bus = bus;
        this.mapsEventHandler = mapsEventHandler;
        this.hereNavigationManager = hereNavigationManager;
        this.handler = new Handler(Looper.getMainLooper());
    }

    public void start() {
        if (!this.registered) {
            this.registered = true;
            this.bus.register(this);
            this.handler.removeCallbacks(this.periodicRunnable);
            this.handler.postDelayed(this.periodicRunnable, 5000);
            sLogger.v("started");
        }
    }

    public void stop() {
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
            this.handler.removeCallbacks(this.periodicRunnable);
            sLogger.v("stopped");
        }
    }

    private void onSpeedExceeded(String roadName, int speedLimit, int currentSpeed) {
        this.bus.post(HereNavigationManager.SPEED_EXCEEDED);
        if (!this.speedWarningOn) {
            this.warningSpeed = -1;
        }
        boolean alreadyOn = this.speedWarningOn;
        this.speedWarningOn = true;
        if (Boolean.TRUE.equals(this.mapsEventHandler.getNavigationPreferences().spokenSpeedLimitWarnings) && !CallUtils.isPhoneCallInProgress()) {
            int speedToConsider;
            SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
            if (this.warningSpeed == -1) {
                speedToConsider = speedLimit + 7;
            } else {
                speedToConsider = this.warningSpeed + (this.warningSpeed / 10);
            }
            if (currentSpeed >= speedToConsider) {
                sLogger.w(this.tag + " speed exceeded-notify current[" + currentSpeed + "] threshold[" + speedToConsider + "] allowed[" + speedLimit + "] " + speedUnit.name());
                this.warningSpeed = currentSpeed;
                String str;
                switch (speedUnit) {
                    case MILES_PER_HOUR:
                        str = this.hereNavigationManager.TTS_MILES;
                        break;
                    case KILOMETERS_PER_HOUR:
                        str = this.hereNavigationManager.TTS_KMS;
                        break;
                    case METERS_PER_SECOND:
                        str = this.hereNavigationManager.TTS_METERS;
                        break;
                    default:
                        str = "";
                        break;
                }
                long now = SystemClock.elapsedRealtime();
                long diff = now - this.lastSpeedWarningTime;
                if (diff <= DISCARD_INTERVAL) {
                    sLogger.v("don't post speed tts:" + diff);
                    return;
                }
                String speedWarning = this.context.getString(R.string.tts_speed_warning, new Object[]{Integer.valueOf(speedLimit)});
                this.lastSpeedWarningTime = now;
                TTSUtils.sendSpeechRequest(speedWarning, Category.SPEECH_SPEED_WARNING, null);
            } else if (!alreadyOn) {
                sLogger.w(this.tag + " speed exceeded current[" + currentSpeed + "] threshold[" + speedToConsider + "] allowed[" + speedLimit + "] " + speedUnit.name());
            }
        } else if (!alreadyOn) {
            sLogger.w(this.tag + " speed exceeded:" + roadName + " limit=" + speedLimit + " current=" + currentSpeed);
        }
    }

    private void onSpeedExceededEnd(String roadName, int speedLimit, int currentSpeed) {
        this.speedWarningOn = false;
        this.warningSpeed = -1;
        this.bus.post(HereNavigationManager.SPEED_NORMAL);
        sLogger.i(this.tag + " speed normal:" + roadName + " limit=" + speedLimit + " current=" + currentSpeed);
    }

    @Subscribe
    public void onPositionUpdated(GeoPosition geoPosition) {
        TaskManager.getInstance().execute(this.checkSpeedRunnable, 12);
    }

    private void checkSpeed() {
        float currentSpeed = (float) this.speedManager.getCurrentSpeed();
        if (currentSpeed != -1.0f) {
            RoadElement roadElement = HereMapUtil.getCurrentRoadElement();
            if (roadElement != null) {
                this.lastUpdate = SystemClock.elapsedRealtime();
                int speedAllowed = SpeedManager.convert((double) roadElement.getSpeedLimit(), SpeedUnit.METERS_PER_SECOND, this.speedManager.getSpeedUnit());
                if (speedAllowed <= 0) {
                    if (this.speedWarningOn) {
                        sLogger.v("clear speed warning, no speedlimit info avail");
                        onSpeedExceededEnd(roadElement.getRoadName(), speedAllowed, (int) currentSpeed);
                    }
                } else if (currentSpeed > ((float) speedAllowed)) {
                    if (!this.speedWarningOn || this.warningSpeed == -1 || (this.warningSpeed != -1 && currentSpeed > ((float) this.warningSpeed))) {
                        onSpeedExceeded(roadElement.getRoadName(), speedAllowed, (int) currentSpeed);
                    }
                } else if (this.speedWarningOn) {
                    onSpeedExceededEnd(roadElement.getRoadName(), speedAllowed, (int) currentSpeed);
                }
            }
        } else if (this.speedWarningOn) {
            sLogger.i("lost speed info while warning on");
            onSpeedExceededEnd(null, 0, 0);
        }
    }
}
