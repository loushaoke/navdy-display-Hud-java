package com.navdy.hud.app.maps.here;

import android.os.SystemClock;
import android.text.TextUtils;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteElement;
import com.here.android.mpa.routing.RouteResult;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class HereRouteViaGenerator {
    private static final String COMMA = ", ";
    private static final String EMPTY = "";
    private static final Logger sLogger = new Logger(HereRouteViaGenerator.class);
    private static final StringBuilder viaBuilder = new StringBuilder();

    private static class DistanceContainer implements Comparable<DistanceContainer> {
        int index;
        long order;
        double val;
        String via;

        DistanceContainer(double val, int index, long order, String via) {
            this.val = val;
            this.index = index;
            this.order = order;
            this.via = via;
        }

        public int compareTo(DistanceContainer another) {
            int diff = this.index - another.index;
            if (diff != 0) {
                return diff;
            }
            diff = ((int) another.val) - ((int) this.val);
            if (diff != 0) {
                return diff;
            }
            return (int) (this.order - another.order);
        }
    }

    public static String getViaString(Route route) {
        return getViaString(route, 1);
    }

    public static String getViaString(Route route, int elements) {
        String viaStr;
        AtomicInteger orderId = new AtomicInteger(0);
        synchronized (viaBuilder) {
            viaBuilder.setLength(0);
            if (route != null) {
                List<DistanceContainer> list = buildDistanceListDesc(null, route.getManeuvers(), orderId, null);
                if (list != null && list.size() > 0) {
                    int len = list.size();
                    for (int i = 0; i < len; i++) {
                        if (viaBuilder.length() > 0) {
                            viaBuilder.append(COMMA);
                        }
                        viaBuilder.append(((DistanceContainer) list.get(i)).via);
                        elements--;
                        if (elements == 0) {
                            break;
                        }
                    }
                }
            }
            viaStr = viaBuilder.toString();
            viaBuilder.setLength(0);
            sLogger.v("getViaString[" + viaStr + "]");
        }
        return viaStr;
    }

    public static String[] generateVia(List<RouteResult> results, String requestId) {
        DistanceContainer container;
        sLogger.v("generating via for " + results.size() + " routes");
        AtomicInteger orderId = new AtomicInteger(0);
        long l1 = SystemClock.elapsedRealtime();
        int len = results.size();
        int viaIndex = 0;
        String[] viaList = new String[len];
        double[] viaDistance = new double[len];
        List<DistanceContainer> list = buildDistanceListDesc(results, null, orderId, requestId);
        if (sLogger.isLoggable(2)) {
            for (DistanceContainer container2 : list) {
                sLogger.v("[" + container2.index + "] road[" + container2.via + "] distance[" + container2.val + "]");
            }
        }
        int i;
        try {
            HashSet<String> seenVias = new HashSet();
            while (list.size() > 0 && viaIndex != viaList.length) {
                if (isRouteCancelled(requestId)) {
                    break;
                }
                int tmpIndex = 0;
                int currentIndex = -1;
                while (true) {
                    container2 = (DistanceContainer) list.get(tmpIndex);
                    if (currentIndex != -1) {
                        if (currentIndex != container2.index) {
                            sLogger.i("no more roadelements,go back");
                            DistanceContainer c = (DistanceContainer) list.get(tmpIndex - 1);
                            viaList[viaIndex] = c.via;
                            viaDistance[viaIndex] = c.val;
                            break;
                        }
                    }
                    currentIndex = container2.index;
                    if (!seenVias.contains(container2.via)) {
                        viaList[container2.index] = container2.via;
                        viaDistance[container2.index] = container2.val;
                        seenVias.add(container2.via);
                        break;
                    }
                    sLogger.v("already seen[" + container2.via + "]");
                    if (tmpIndex + 1 == list.size()) {
                        sLogger.i("no more roadelements,use current");
                        break;
                    }
                    tmpIndex++;
                }
                Iterator<DistanceContainer> iterator = list.iterator();
                while (iterator.hasNext()) {
                    if (((DistanceContainer) iterator.next()).index == viaIndex) {
                        iterator.remove();
                    }
                }
                viaIndex++;
            }
            sLogger.v("via algo took[" + (SystemClock.elapsedRealtime() - l1) + "] len[" + viaList.length + "]");
            for (i = 0; i < viaList.length; i++) {
                sLogger.v("via[" + (i + 1) + "] [" + viaList[i] + "][" + viaDistance[i] + "]");
            }
        } catch (Throwable th) {
            sLogger.v("via algo took[" + (SystemClock.elapsedRealtime() - l1) + "] len[" + viaList.length + "]");
            for (i = 0; i < viaList.length; i++) {
                sLogger.v("via[" + (i + 1) + "] [" + viaList[i] + "][" + viaDistance[i] + "]");
            }
        }
        return viaList;
    }

    private static List<DistanceContainer> buildDistanceListDesc(List<RouteResult> results, List<Maneuver> maneuvers, AtomicInteger orderId, String requestId) {
        HashMap<String, DistanceContainer> map = new HashMap();
        ArrayList<DistanceContainer> list = new ArrayList();
        int index = 0;
        if (results != null) {
            loop0:
            for (RouteResult routeResult : results) {
                int counter = 1;
                for (RouteElement routeElement : routeResult.getRoute().getRouteElements().getElements()) {
                    processRoadElement(routeElement.getRoadElement(), map, index, orderId);
                    if (counter % 5 == 0 && requestId != null && isRouteCancelled(requestId)) {
                        break loop0;
                    }
                    counter++;
                }
                list.addAll(map.values());
                map.clear();
                index++;
            }
            Collections.sort(list);
        } else {
            for (Maneuver maneuver : maneuvers) {
                for (RoadElement roadElement : maneuver.getRoadElements()) {
                    processRoadElement(roadElement, map, 0, orderId);
                }
            }
            list.addAll(map.values());
            Collections.sort(list);
        }
        return list;
    }

    private static void processRoadElement(RoadElement roadElement, HashMap<String, DistanceContainer> map, int index, AtomicInteger orderId) {
        String name = getDisplayString(roadElement);
        if (!TextUtils.isEmpty(name)) {
            DistanceContainer distance = (DistanceContainer) map.get(name);
            if (distance == null) {
                map.put(name, new DistanceContainer(roadElement.getGeometryLength(), index, (long) orderId.getAndIncrement(), name));
                return;
            }
            distance.val += roadElement.getGeometryLength();
        }
    }

    private static String getDisplayString(RoadElement roadElement) {
        if (roadElement == null) {
            return null;
        }
        String str = roadElement.getRouteName();
        if (TextUtils.isEmpty(str)) {
            return roadElement.getRoadName();
        }
        return str;
    }

    private static boolean isRouteCancelled(String requestId) {
        if (requestId == null) {
            return false;
        }
        String activeRouteCalcId = HereRouteManager.getActiveRouteCalcId();
        if (TextUtils.equals(requestId, activeRouteCalcId)) {
            return false;
        }
        sLogger.v("route request [" + requestId + "] is not active anymore, current [" + activeRouteCalcId + "]");
        return true;
    }
}
