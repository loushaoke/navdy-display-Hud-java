package com.navdy.hud.app.maps;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.service.library.log.Logger;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class MapSchemeController implements SensorEventListener {
    private static final int AVERAGING_PERIOD = 300;
    private static final long DELAY_MILLIS = 10000;
    private static final float HI_THRESHOLD = 600.0f;
    private static final float LO_THRESHOLD = 200.0f;
    public static final String MAP_SCHEME_CARNAV_DAY = "carnav.day";
    public static final String MAP_SCHEME_CARNAV_NIGHT = "carnav.day";
    public static final int MESSAGE_PERIODIC = 0;
    public static final Logger sLogger = new Logger(MapSchemeController.class);
    private static final MapSchemeController singleton = new MapSchemeController();
    private Context context = HudApplication.getAppContext();
    private int currentIdx = 0;
    private float lastSensorValue = 400.0f;
    private Sensor light;
    private Handler myHandler;
    private boolean ringBufferFull = false;
    private SensorManager sensorManager;
    private float[] sensorValues = new float[30];
    @Inject
    SharedPreferences sharedPreferences;

    enum MapScheme {
        DAY,
        NIGHT
    }

    public static MapSchemeController getInstance() {
        return singleton;
    }

    MapScheme getActiveScheme() {
        if (this.sharedPreferences.getString(HUDSettings.MAP_SCHEME, "").equals("carnav.day")) {
            return MapScheme.DAY;
        }
        return MapScheme.NIGHT;
    }

    public MapSchemeController() {
        Mortar.inject(this.context, this);
        this.sensorManager = (SensorManager) this.context.getSystemService("sensor");
        List<Sensor> deviceSensors = this.sensorManager.getSensorList(-1);
        for (int i = 0; i < deviceSensors.size(); i++) {
            Sensor sensor = (Sensor) deviceSensors.get(i);
            sLogger.d("sensor: " + sensor.toString());
            if (sensor.getName().equals("tsl2572")) {
                this.light = sensor;
            }
        }
        sLogger.d("light sensor selected: " + this.light);
        this.sensorManager.registerListener(this, this.light, 3);
        this.myHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    MapSchemeController.this.collectLightSensorSample();
                    MapSchemeController.this.evaluateCollectedSamples();
                    sendEmptyMessageDelayed(0, MapSchemeController.DELAY_MILLIS);
                }
            }
        };
        this.myHandler.sendEmptyMessageDelayed(0, DELAY_MILLIS);
    }

    private void collectLightSensorSample() {
        this.sensorValues[this.currentIdx] = this.lastSensorValue;
        if (this.currentIdx == this.sensorValues.length - 1) {
            this.ringBufferFull = true;
        }
        this.currentIdx = (this.currentIdx + 1) % this.sensorValues.length;
    }

    private void evaluateCollectedSamples() {
        float s = 0.0f;
        int l = this.ringBufferFull ? this.sensorValues.length : this.currentIdx;
        for (int i = 0; i < l; i++) {
            s += this.sensorValues[i];
        }
        s /= (float) l;
        sLogger.d("evaluateCollectedSamples: avg lux: " + s);
        if (getActiveScheme() == MapScheme.DAY && s < LO_THRESHOLD) {
            switchMapScheme(MapScheme.NIGHT);
        } else if (getActiveScheme() == MapScheme.NIGHT && s > HI_THRESHOLD) {
            switchMapScheme(MapScheme.DAY);
        }
    }

    private void switchMapScheme(MapScheme scheme) {
        String schemeName = this.sharedPreferences.getString(HUDSettings.MAP_SCHEME, "");
        if (schemeName.equals("carnav.day") && scheme == MapScheme.NIGHT) {
            schemeName = "carnav.day";
        } else if (schemeName.equals("carnav.day") && scheme == MapScheme.DAY) {
            schemeName = "carnav.day";
        }
        sLogger.i("changing map scheme to " + schemeName);
        this.sharedPreferences.edit().putString(HUDSettings.MAP_SCHEME, schemeName).apply();
    }

    public final void onSensorChanged(SensorEvent event) {
        float lux = event.values[0];
        if (sLogger.isLoggable(2)) {
            sLogger.d("onSensorChanged: lux: " + lux);
        }
        this.lastSensorValue = lux;
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        sLogger.d("onAccuracyChanged: accuracy: " + accuracy);
    }
}
