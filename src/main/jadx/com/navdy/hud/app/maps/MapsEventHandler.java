package com.navdy.hud.app.maps;

import android.content.SharedPreferences;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HerePlacesManager;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.navigation.GetNavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationRouteCancelRequest;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.service.library.events.navigation.NavigationSessionResponse;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.service.library.events.navigation.RouteManeuverRequest;
import com.navdy.service.library.events.navigation.RouteManeuverResponse;
import com.navdy.service.library.events.places.AutoCompleteRequest;
import com.navdy.service.library.events.places.PlacesSearchRequest;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import javax.inject.Inject;
import mortar.Mortar;

public final class MapsEventHandler {
    private static final boolean VERBOSE = false;
    private static final Logger sLogger = new Logger(MapsEventHandler.class);
    private static final MapsEventHandler sSingleton = new MapsEventHandler();
    @Inject
    Bus bus;
    @Inject
    ConnectionHandler connectionHandler;
    @Inject
    DriverProfileManager mDriverProfileManager;
    @Inject
    protected SharedPreferences sharedPreferences;

    public static final MapsEventHandler getInstance() {
        return sSingleton;
    }

    private MapsEventHandler() {
        Mortar.inject(HudApplication.getAppContext(), this);
        this.bus.register(this);
    }

    @Subscribe
    public void onConnectionStatusChange(ConnectionStateChange event) {
        handleConnectionStateChange(event);
    }

    @Subscribe
    public void onPlaceSearchRequest(PlacesSearchRequest event) {
        HerePlacesManager.handlePlacesSearchRequest(event);
    }

    @Subscribe
    public void onAutoCompleteRequest(AutoCompleteRequest event) {
        HerePlacesManager.handleAutoCompleteRequest(event);
    }

    @Subscribe
    public void onRouteSearchRequest(NavigationRouteRequest event) {
        HereRouteManager.handleRouteRequest(event);
    }

    @Subscribe
    public void onRouteCancelRequest(NavigationRouteCancelRequest event) {
        HereRouteManager.handleRouteCancelRequest(event, false);
    }

    @Subscribe
    public void onNavigationRequest(NavigationSessionRequest event) {
        handleNavigationRequest(event);
    }

    @Subscribe
    public void onGetNavigationSessionState(GetNavigationSessionState event) {
        if (HereMapsManager.getInstance().isInitialized()) {
            HereNavigationManager.getInstance().postNavigationSessionStatusEvent(true);
            return;
        }
        sLogger.i("onGetNavigationSessionState:engine not ready");
        this.bus.post(new RemoteEvent(new NavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY, null, null, null, null)));
    }

    @Subscribe
    public void onGetRouteManeuverRequest(RouteManeuverRequest routeManeuverRequest) {
        try {
            HereRouteManager.handleRouteManeuverRequest(routeManeuverRequest);
        } catch (Throwable t) {
            sendEventToClient(new RouteManeuverResponse(RequestStatus.REQUEST_SERVICE_ERROR, t.toString(), null));
            sLogger.e(t);
        }
    }

    public void sendEventToClient(Message message) {
        try {
            RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
            if (remoteDevice != null) {
                remoteDevice.postEvent(message);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public Bus getBus() {
        return this.bus;
    }

    public SharedPreferences getSharedPreferences() {
        return this.sharedPreferences;
    }

    public NavigationPreferences getNavigationPreferences() {
        return this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
    }

    private void handleConnectionStateChange(ConnectionStateChange event) {
        if (HereMapsManager.getInstance().isInitialized()) {
            boolean connected = false;
            if (event.state == ConnectionState.CONNECTION_VERIFIED) {
                connected = true;
                this.bus.post(new RemoteEvent(new NavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ENGINE_READY, null, null, null, null)));
            }
            HereNavigationManager.getInstance().handleConnectionState(connected);
            return;
        }
        sLogger.v("handleConnectionStateChange:engine not initiazed");
    }

    private void handleNavigationRequest(NavigationSessionRequest request) {
        if (HereMapsManager.getInstance().isInitialized()) {
            HereNavigationManager.getInstance().handleNavigationSessionRequest(request);
        } else {
            sendEventToClient(new NavigationSessionResponse(RequestStatus.REQUEST_NOT_READY, HudApplication.getAppContext().getString(R.string.map_engine_not_ready), request.newState, request.routeId));
        }
    }
}
