package com.navdy.hud.app.maps;

import android.content.res.Resources;
import android.os.Bundle;
import com.here.android.mpa.common.OnEngineInitListener.Error;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.service.library.log.Logger;

public class MapsNotification {
    private static final String MAP_ENGINE_NOT_INIT_ID = "maps-no-init";
    private static final Logger sLogger = new Logger(MapsNotification.class);

    public static void showMapsEngineNotInitializedToast() {
        String str;
        sLogger.d("maps engine not init toast");
        Resources resources = HudApplication.getAppContext().getResources();
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 2000);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_mm_map);
        Error error = HereMapsManager.getInstance().getError();
        if (error == null) {
            str = "";
        } else {
            str = error.name();
        }
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.map_engine_not_ready));
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, str);
        ToastManager.getInstance().addToast(new ToastParams(MAP_ENGINE_NOT_INIT_ID, bundle, null, true, false));
    }
}
