package com.navdy.hud.app.maps.here;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.guidance.TrafficUpdater;
import com.here.android.mpa.guidance.TrafficUpdater.Error;
import com.here.android.mpa.guidance.TrafficUpdater.GetEventsListener;
import com.here.android.mpa.guidance.TrafficUpdater.Listener;
import com.here.android.mpa.guidance.TrafficUpdater.RequestInfo;
import com.here.android.mpa.guidance.TrafficUpdater.RequestState;
import com.here.android.mpa.mapping.TrafficEvent;
import com.here.android.mpa.mapping.TrafficEvent.Severity;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident;
import com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Category;
import com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident.Type;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.SystemUtils;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HereTrafficUpdater2 {
    private static final DisplayTrafficIncident FAILED_EVENT = new DisplayTrafficIncident(Type.FAILED, Category.UNDEFINED);
    private static final DisplayTrafficIncident INACTIVE_EVENT = new DisplayTrafficIncident(Type.INACTIVE, Category.UNDEFINED);
    private static final DisplayTrafficIncident NORMAL_EVENT = new DisplayTrafficIncident(Type.NORMAL, Category.UNDEFINED);
    private static final int STALE_REQUEST_TIME = ((int) TimeUnit.MINUTES.toMillis(2));
    private static final String TAG = "HereTrafficUpdater2";
    private static final int TRAFFIC_DATA_CHECK_TIME_INTERVAL = ((int) TimeUnit.SECONDS.toMillis(30));
    private static final int TRAFFIC_DATA_REFRESH_INTERVAL = ((int) TimeUnit.MINUTES.toMillis(5));
    private static final int TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH = ((int) TimeUnit.MINUTES.toMillis(15));
    private static final Logger sLogger = new Logger(TAG);
    private Bus bus;
    private Runnable checkRunnable = new Runnable() {
        public void run() {
            boolean check = false;
            try {
                HereTrafficUpdater2.sLogger.v("checkRunnable");
                if (HereTrafficUpdater2.this.currentRequestInfo != null && HereTrafficUpdater2.this.lastRequestTime > 0) {
                    long diff = SystemClock.elapsedRealtime() - HereTrafficUpdater2.this.lastRequestTime;
                    HereTrafficUpdater2.sLogger.v("checkRunnable:" + diff);
                    if (diff >= ((long) HereTrafficUpdater2.STALE_REQUEST_TIME)) {
                        HereTrafficUpdater2.sLogger.v("checkRunnable: request stale");
                        HereTrafficUpdater2.this.reset(5000);
                    } else {
                        check = true;
                        HereTrafficUpdater2.sLogger.v("checkRunnable: request not stale");
                    }
                }
                if (check) {
                    HereTrafficUpdater2.this.handler.postDelayed(this, (long) HereTrafficUpdater2.TRAFFIC_DATA_CHECK_TIME_INTERVAL);
                }
            } catch (Throwable th) {
                if (check) {
                    HereTrafficUpdater2.this.handler.postDelayed(this, (long) HereTrafficUpdater2.TRAFFIC_DATA_CHECK_TIME_INTERVAL);
                }
            }
        }
    };
    private RequestInfo currentRequestInfo;
    private Handler handler = new Handler(Looper.getMainLooper());
    private long lastRequestTime;
    private GetEventsListener onGetEventsListener = new GetEventsListener() {
        public void onComplete(final List<TrafficEvent> list, Error error) {
            boolean reset = false;
            try {
                HereTrafficUpdater2.sLogger.i("onGetEventsListener::" + error);
                final Route r = HereTrafficUpdater2.this.route;
                if (r == null) {
                    HereTrafficUpdater2.sLogger.i("onGetEventsListener: route is null");
                    return;
                }
                reset = true;
                HereTrafficUpdater2.this.currentRequestInfo = null;
                HereTrafficUpdater2.this.lastRequestTime = 0;
                if (error != Error.NONE) {
                    HereTrafficUpdater2.sLogger.i("onGetEventsListener: error " + error);
                    HereTrafficUpdater2.this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                } else if (list == null || list.size() == 0) {
                    HereTrafficUpdater2.sLogger.i("onGetEventsListener: no events");
                    HereTrafficUpdater2.this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                } else {
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            if (HereTrafficUpdater2.this.route == null) {
                                HereTrafficUpdater2.sLogger.w("no current route, cannot process traffic events");
                                return;
                            }
                            HereTrafficUpdater2.this.processEvents(r, list, HereNavigationManager.getInstance().getNavController().getDestinationDistance());
                        }
                    }, 2);
                }
                if (1 != null) {
                    HereTrafficUpdater2.this.reset(HereTrafficUpdater2.this.getRefereshInterval());
                }
            } finally {
                if (reset) {
                    HereTrafficUpdater2.this.reset(HereTrafficUpdater2.this.getRefereshInterval());
                }
            }
        }
    };
    private Listener onRequestListener = new Listener() {
        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onStatusChanged(RequestState requestState) {
            boolean reset = false;
            int resetTime = HereTrafficUpdater2.this.getRefereshInterval();
            try {
                HereTrafficUpdater2.sLogger.i("onRequestListener::" + requestState);
                if (requestState != RequestState.DONE) {
                    HereTrafficUpdater2.sLogger.i("onRequestListener:: traffic data download failed");
                    resetTime = HereTrafficUpdater2.this.getRefereshInterval() / 2;
                    HereTrafficUpdater2.this.bus.post(HereTrafficUpdater2.FAILED_EVENT);
                    reset = true;
                } else if (HereTrafficUpdater2.this.route == null) {
                    HereTrafficUpdater2.sLogger.i("onRequestListener:traffic downloaded for open map");
                    if (!MapSettings.isTrafficDashWidgetsEnabled()) {
                        HereTrafficUpdater2.this.currentRequestInfo = null;
                        HereTrafficUpdater2.this.lastRequestTime = 0;
                    }
                    if (true) {
                        HereTrafficUpdater2.this.reset(resetTime);
                        return;
                    }
                    return;
                } else {
                    HereTrafficUpdater2.sLogger.i("onRequestListener:traffic downloaded for route");
                    if (MapSettings.isTrafficDashWidgetsEnabled()) {
                        HereTrafficUpdater2.sLogger.i("onRequestListener:call getEvents");
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                long l1 = SystemClock.elapsedRealtime();
                                try {
                                    HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
                                    if (hereNavigationManager.isNavigationModeOn()) {
                                        Maneuver next = hereNavigationManager.getNextManeuver();
                                        Maneuver prev = hereNavigationManager.getPrevManeuver();
                                        List aheadRouteElements = HereMapUtil.getAheadRouteElements(HereTrafficUpdater2.this.route, HereMapUtil.getCurrentRoadElement(), prev, next);
                                        if (aheadRouteElements == null || aheadRouteElements.size() == 0) {
                                            HereTrafficUpdater2.sLogger.i("onRequestListener: no ahead road elements");
                                            HereTrafficUpdater2.this.currentRequestInfo = null;
                                            HereTrafficUpdater2.this.lastRequestTime = 0;
                                            HereTrafficUpdater2.this.reset(HereTrafficUpdater2.this.getRefereshInterval());
                                            HereTrafficUpdater2.sLogger.v("ahead route build time [" + (SystemClock.elapsedRealtime() - l1) + "]");
                                            return;
                                        }
                                        HereTrafficUpdater2.sLogger.i("onRequestListener: calling getevent size:" + aheadRouteElements.size());
                                        HereTrafficUpdater2.this.trafficUpdater.getEvents(aheadRouteElements, HereTrafficUpdater2.this.onGetEventsListener);
                                        HereTrafficUpdater2.sLogger.v("ahead route build time [" + (SystemClock.elapsedRealtime() - l1) + "]");
                                        return;
                                    }
                                    HereTrafficUpdater2.sLogger.v("nav is off");
                                } catch (Throwable t) {
                                    HereTrafficUpdater2.this.currentRequestInfo = null;
                                    HereTrafficUpdater2.this.lastRequestTime = 0;
                                    HereTrafficUpdater2.this.reset(HereTrafficUpdater2.this.getRefereshInterval() / 2);
                                    HereTrafficUpdater2.sLogger.e("onRequestListener", t);
                                }
                            }
                        }, 2);
                    } else {
                        if (!MapSettings.isTrafficDashWidgetsEnabled()) {
                            HereTrafficUpdater2.this.currentRequestInfo = null;
                            HereTrafficUpdater2.this.lastRequestTime = 0;
                        }
                        if (true) {
                            HereTrafficUpdater2.this.reset(resetTime);
                            return;
                        }
                        return;
                    }
                }
                if (!MapSettings.isTrafficDashWidgetsEnabled()) {
                    HereTrafficUpdater2.this.currentRequestInfo = null;
                    HereTrafficUpdater2.this.lastRequestTime = 0;
                }
                if (reset) {
                    HereTrafficUpdater2.this.reset(resetTime);
                }
            } catch (Throwable th) {
                if (!MapSettings.isTrafficDashWidgetsEnabled()) {
                    HereTrafficUpdater2.this.currentRequestInfo = null;
                    HereTrafficUpdater2.this.lastRequestTime = 0;
                }
                if (null != null) {
                    HereTrafficUpdater2.this.reset(resetTime);
                }
            }
        }
    };
    private Runnable refreshRunnable = new Runnable() {
        public void run() {
            HereTrafficUpdater2.this.startRequest();
        }
    };
    private Route route;
    private TrafficUpdater trafficUpdater;

    private static class HereTrafficEvent {
        public long distance;
        public TrafficEvent trafficEvent;

        public HereTrafficEvent(TrafficEvent trafficEvent, long distance) {
            this.trafficEvent = trafficEvent;
            this.distance = distance;
        }
    }

    public HereTrafficUpdater2(Bus bus) {
        sLogger.i("ctor");
        this.bus = bus;
        this.trafficUpdater = TrafficUpdater.getInstance();
    }

    public void start() {
        sLogger.i("start");
        startRequest();
    }

    public void setRoute(Route route) {
        sLogger.i("setRoute:" + route + " id=" + System.identityHashCode(route));
        cancelRequest();
        this.route = route;
        startRequest();
    }

    private void startRequest() {
        try {
            if (!SystemUtils.isConnectedToNetwork(HudApplication.getAppContext())) {
                sLogger.i("startRequest: not connected to n/w, retry");
                reset(getRefereshInterval() / 2);
                this.bus.post(FAILED_EVENT);
                this.lastRequestTime = 0;
            } else if (HereNavigationManager.getInstance().hasArrived()) {
                sLogger.i("startRequest: arrived, not making request");
                this.bus.post(INACTIVE_EVENT);
                this.lastRequestTime = 0;
            } else {
                if (this.route != null) {
                    sLogger.i("startRequest: getting route traffic");
                    this.currentRequestInfo = this.trafficUpdater.request(this.route, this.onRequestListener);
                } else {
                    GeoCoordinate coordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                    if (coordinate == null) {
                        sLogger.i("startRequest: getting area traffic, no current coordinate");
                        this.lastRequestTime = 0;
                        this.bus.post(FAILED_EVENT);
                        reset(getRefereshInterval() / 2);
                        return;
                    }
                    sLogger.i("startRequest: getting area traffic");
                    this.currentRequestInfo = this.trafficUpdater.request(coordinate, this.onRequestListener);
                }
                Error error = this.currentRequestInfo.getError();
                sLogger.i("startRequest: returned:" + error);
                switch (error) {
                    case NONE:
                        this.handler.removeCallbacks(this.checkRunnable);
                        this.handler.postDelayed(this.checkRunnable, (long) TRAFFIC_DATA_CHECK_TIME_INTERVAL);
                        this.lastRequestTime = SystemClock.elapsedRealtime();
                        return;
                    default:
                        this.lastRequestTime = 0;
                        this.bus.post(FAILED_EVENT);
                        reset(getRefereshInterval() / 2);
                        return;
                }
            }
        } catch (Throwable t) {
            sLogger.e(t);
            reset(getRefereshInterval() / 2);
        }
    }

    private void cancelRequest() {
        try {
            if (this.currentRequestInfo != null) {
                if (this.currentRequestInfo.getError() == Error.NONE) {
                    this.trafficUpdater.cancelRequest(this.currentRequestInfo.getRequestId());
                    this.lastRequestTime = 0;
                    sLogger.v("cancelRequest: called cancel");
                }
                this.currentRequestInfo = null;
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private void reset(int delay) {
        sLogger.v("reset");
        cancelRequest();
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, (long) delay);
        this.handler.removeCallbacks(this.checkRunnable);
    }

    private void processEvents(Route route, List<TrafficEvent> events, long distanceRemaining) {
        long l1 = SystemClock.elapsedRealtime();
        sLogger.v("processEvents size:" + events.size());
        GenericUtil.checkNotOnMainThread();
        GeoCoordinate destination = route.getDestination();
        int blocking = 0;
        int veryHighCount = 0;
        int highCount = 0;
        List<HereTrafficEvent> blockingList = new ArrayList();
        List<HereTrafficEvent> veryHighList = new ArrayList();
        List<HereTrafficEvent> highList = new ArrayList();
        RoadElement currentRoadElement = HereMapUtil.getCurrentRoadElement();
        if (currentRoadElement == null) {
            sLogger.w("no current road element");
            return;
        }
        GeoCoordinate coordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        if (coordinate == null) {
            sLogger.w("no current coordinate");
            return;
        }
        List<Maneuver> maneuvers = route.getManeuvers();
        RoadElement lastRoadElement = null;
        Maneuver lastManeuver = HereMapUtil.getLastManeuver(maneuvers);
        if (lastManeuver != null) {
            List<RoadElement> lastManeuverRoadElements = lastManeuver.getRoadElements();
            if (lastManeuverRoadElements != null && lastManeuverRoadElements.size() > 0) {
                lastRoadElement = (RoadElement) lastManeuverRoadElements.get(lastManeuverRoadElements.size() - 1);
            }
        }
        for (TrafficEvent event : events) {
            if (event.isOnRoute(route)) {
                List<RoadElement> affectedRoadElements = event.getAffectedRoadElements();
                if (affectedRoadElements != null && affectedRoadElements.size() != 0) {
                    boolean calcDistance = false;
                    Severity severity = event.getSeverity();
                    switch (severity) {
                        case BLOCKING:
                        case VERY_HIGH:
                        case HIGH:
                            calcDistance = true;
                            break;
                    }
                    long distanceToEvent = -1;
                    if (calcDistance) {
                        distanceToEvent = HereMapUtil.getDistanceToRoadElement(currentRoadElement, (RoadElement) affectedRoadElements.get(0), maneuvers);
                        long eventToDestinationDistance = HereMapUtil.getDistanceToRoadElement((RoadElement) affectedRoadElements.get(0), lastRoadElement, maneuvers);
                        if (eventToDestinationDistance == -1) {
                            sLogger.v("could not get distance");
                        } else {
                            long distanceToEventHaversine = (long) event.getDistanceTo(coordinate);
                            long total = distanceToEvent + eventToDestinationDistance;
                            sLogger.v("[EVENT] distanceToEvent:" + distanceToEvent + " eventToDestinationDistance:" + eventToDestinationDistance + " distanceToDest:" + distanceRemaining + " total:" + total + " distanceToEventHaversine:" + distanceToEventHaversine);
                        }
                    } else {
                        sLogger.v("[EVENT] n/a severity = " + severity);
                    }
                    switch (severity) {
                        case BLOCKING:
                            blockingList.add(new HereTrafficEvent(event, distanceToEvent));
                            blocking++;
                            break;
                        case VERY_HIGH:
                            veryHighList.add(new HereTrafficEvent(event, distanceToEvent));
                            veryHighCount++;
                            break;
                        case HIGH:
                            highList.add(new HereTrafficEvent(event, distanceToEvent));
                            highCount++;
                            break;
                        default:
                            break;
                    }
                }
                sLogger.v("no road elements for event");
            } else {
                sLogger.v("[NOT_ON_ROUTE] event is not on route");
            }
        }
        sLogger.v("blocking:" + blocking + " very high:" + veryHighCount + " high:" + highCount);
        if (blocking > 0) {
            this.bus.post(buildEvent(Type.BLOCKING, blockingList));
        } else if (veryHighCount > 0) {
            this.bus.post(buildEvent(Type.VERY_HIGH, veryHighList));
        } else if (highCount > 0) {
            this.bus.post(buildEvent(Type.HIGH, highList));
        } else {
            this.bus.post(NORMAL_EVENT);
        }
        sLogger.v("processEvents took [" + (SystemClock.elapsedRealtime() - l1) + "]");
    }

    private DisplayTrafficIncident buildEvent(Type type, List<HereTrafficEvent> list) {
        Object obj;
        String title;
        String description;
        Category category;
        HereTrafficEvent accidentEvent = null;
        HereTrafficEvent closureEvent = null;
        HereTrafficEvent roadworkEvent = null;
        HereTrafficEvent congestionEvent = null;
        HereTrafficEvent flowEvent = null;
        HereTrafficEvent otherEvent = null;
        HereTrafficEvent undefinedEvent = null;
        for (HereTrafficEvent event : list) {
            String shortText = event.trafficEvent.getShortText();
            obj = -1;
            switch (shortText.hashCode()) {
                case -1360575985:
                    if (shortText.equals(DisplayTrafficIncident.ACCIDENT)) {
                        obj = null;
                        break;
                    }
                    break;
                case 2160942:
                    if (shortText.equals(DisplayTrafficIncident.FLOW)) {
                        obj = 4;
                        break;
                    }
                    break;
                case 75532016:
                    if (shortText.equals("OTHER")) {
                        obj = 5;
                        break;
                    }
                    break;
                case 1584535067:
                    if (shortText.equals(DisplayTrafficIncident.CLOSURE)) {
                        obj = 1;
                        break;
                    }
                    break;
                case 1748463920:
                    if (shortText.equals(DisplayTrafficIncident.UNDEFINED)) {
                        obj = 6;
                        break;
                    }
                    break;
                case 1943412802:
                    if (shortText.equals(DisplayTrafficIncident.ROADWORKS)) {
                        obj = 2;
                        break;
                    }
                    break;
                case 2101625895:
                    if (shortText.equals(DisplayTrafficIncident.CONGESTION)) {
                        obj = 3;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case null:
                    if (accidentEvent != null) {
                        if (accidentEvent.distance <= event.distance) {
                            break;
                        }
                        accidentEvent = event;
                        break;
                    }
                    accidentEvent = event;
                    break;
                case 1:
                    if (closureEvent != null) {
                        if (closureEvent.distance <= event.distance) {
                            break;
                        }
                        closureEvent = event;
                        break;
                    }
                    closureEvent = event;
                    break;
                case 2:
                    if (roadworkEvent != null) {
                        if (roadworkEvent.distance <= event.distance) {
                            break;
                        }
                        roadworkEvent = event;
                        break;
                    }
                    roadworkEvent = event;
                    break;
                case 3:
                    if (congestionEvent != null) {
                        if (congestionEvent.distance <= event.distance) {
                            break;
                        }
                        congestionEvent = event;
                        break;
                    }
                    congestionEvent = event;
                    break;
                case 4:
                    if (flowEvent != null) {
                        if (flowEvent.distance <= event.distance) {
                            break;
                        }
                        flowEvent = event;
                        break;
                    }
                    flowEvent = event;
                    break;
                case 5:
                    if (otherEvent != null) {
                        if (otherEvent.distance <= event.distance) {
                            break;
                        }
                        otherEvent = event;
                        break;
                    }
                    otherEvent = event;
                    break;
                default:
                    if (undefinedEvent != null) {
                        if (undefinedEvent.distance <= event.distance) {
                            break;
                        }
                        undefinedEvent = event;
                        break;
                    }
                    undefinedEvent = event;
                    break;
            }
        }
        HereTrafficEvent userEvent = null;
        if (accidentEvent != null) {
            userEvent = accidentEvent;
        } else if (closureEvent != null) {
            userEvent = closureEvent;
        } else if (roadworkEvent != null) {
            userEvent = roadworkEvent;
        } else if (congestionEvent != null) {
            userEvent = congestionEvent;
        } else if (flowEvent != null) {
            userEvent = flowEvent;
        } else if (otherEvent != null) {
            userEvent = otherEvent;
        } else if (undefinedEvent != null) {
            userEvent = undefinedEvent;
        }
        String affectedStreet = userEvent.trafficEvent.getFirstAffectedStreet();
        String shortText2 = userEvent.trafficEvent.getShortText();
        obj = -1;
        switch (shortText2.hashCode()) {
            case -1360575985:
                if (shortText2.equals(DisplayTrafficIncident.ACCIDENT)) {
                    obj = null;
                    break;
                }
                break;
            case 2160942:
                if (shortText2.equals(DisplayTrafficIncident.FLOW)) {
                    obj = 4;
                    break;
                }
                break;
            case 75532016:
                if (shortText2.equals("OTHER")) {
                    obj = 5;
                    break;
                }
                break;
            case 1584535067:
                if (shortText2.equals(DisplayTrafficIncident.CLOSURE)) {
                    obj = 1;
                    break;
                }
                break;
            case 1748463920:
                if (shortText2.equals(DisplayTrafficIncident.UNDEFINED)) {
                    obj = 6;
                    break;
                }
                break;
            case 1943412802:
                if (shortText2.equals(DisplayTrafficIncident.ROADWORKS)) {
                    obj = 2;
                    break;
                }
                break;
            case 2101625895:
                if (shortText2.equals(DisplayTrafficIncident.CONGESTION)) {
                    obj = 3;
                    break;
                }
                break;
        }
        switch (obj) {
            case null:
                title = "Accident";
                description = userEvent.trafficEvent.getEventText();
                category = Category.ACCIDENT;
                break;
            case 1:
                title = "Closure";
                description = userEvent.trafficEvent.getEventText();
                category = Category.CLOSURE;
                break;
            case 2:
                title = "RoadWork";
                description = userEvent.trafficEvent.getEventText();
                category = Category.ROADWORKS;
                break;
            case 3:
                title = "Congestion";
                description = userEvent.trafficEvent.getEventText();
                category = Category.CONGESTION;
                break;
            case 4:
                title = "Congestion";
                description = "";
                category = Category.FLOW;
                break;
            case 5:
                title = "Other";
                description = userEvent.trafficEvent.getEventText();
                category = Category.OTHER;
                break;
            default:
                title = "Undefined:" + userEvent.trafficEvent.getShortText();
                description = userEvent.trafficEvent.getEventText();
                category = Category.UNDEFINED;
                break;
        }
        return new DisplayTrafficIncident(type, category, title, description, affectedStreet, userEvent.distance, userEvent.trafficEvent.getActivationDate(), userEvent.trafficEvent.getActivationDate(), userEvent.trafficEvent.getIconOnRoute());
    }

    boolean isRunning() {
        return this.route != null;
    }

    int getRefereshInterval() {
        if (NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
            return TRAFFIC_DATA_REFRESH_INTERVAL_LIMITED_BANDWIDTH;
        }
        return TRAFFIC_DATA_REFRESH_INTERVAL;
    }
}
