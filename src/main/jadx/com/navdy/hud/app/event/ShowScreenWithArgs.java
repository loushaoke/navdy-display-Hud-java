package com.navdy.hud.app.event;

import android.os.Bundle;
import com.navdy.service.library.events.ui.Screen;

public class ShowScreenWithArgs {
    public Bundle args;
    public Object args2;
    public boolean ignoreAnimation;
    public Screen screen;

    public ShowScreenWithArgs(Screen screen, Bundle args, boolean ignoreAnimation) {
        this(screen, args, null, ignoreAnimation);
    }

    public ShowScreenWithArgs(Screen screen, Bundle args, Object args2, boolean ignoreAnimation) {
        this.screen = screen;
        this.args = args;
        this.args2 = args2;
        this.ignoreAnimation = ignoreAnimation;
    }
}
