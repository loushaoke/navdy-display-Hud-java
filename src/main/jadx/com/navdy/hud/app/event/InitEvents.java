package com.navdy.hud.app.event;

public class InitEvents {

    public static class BluetoothStateChanged {
        public final boolean enabled;

        public BluetoothStateChanged(boolean enabled) {
            this.enabled = enabled;
        }
    }

    public static class ConnectionServiceStarted {
    }

    public static class InitPhase {
        public final Phase phase;

        public InitPhase(Phase phase) {
            this.phase = phase;
        }
    }

    public enum Phase {
        EARLY,
        PRE_USER_INTERACTION,
        CONNECTION_SERVICE_STARTED,
        POST_START,
        LATE,
        SWITCHING_LOCALE,
        LOCALE_UP_TO_DATE
    }
}
