package com.navdy.hud.app.event;

import android.os.Bundle;

public class Shutdown {
    public static final String EXTRA_SHUTDOWN_CAUSE = "SHUTDOWN_CAUSE";
    public final Reason reason;
    public final State state;

    public enum Reason {
        UNKNOWN("Unknown", false),
        CRITICAL_VOLTAGE("Critical_Voltage", false),
        LOW_VOLTAGE("Low_Voltage", false),
        HIGH_TEMPERATURE("High_Temperature", false),
        TIMEOUT("Timeout", false),
        OTA("OTA", false),
        DIAL_OTA("Dial_OTA", false),
        FORCED_UPDATE("Forced_Update", false),
        FACTORY_RESET("Factory_Reset", false),
        POWER_LOSS("Power_Loss", false),
        MENU("Menu", true),
        POWER_BUTTON("Power_Button", true),
        ENGINE_OFF("Engine_Off", true),
        ACCELERATE_SHUTDOWN("Accelerate_Shutdown", true),
        INACTIVITY("Inactivity", true);
        
        public final String attr;
        public final boolean requireConfirmation;

        private Reason(String attr, boolean requireConfirmation) {
            this.attr = attr;
            this.requireConfirmation = requireConfirmation;
        }

        public Bundle asBundle() {
            Bundle b = new Bundle();
            b.putString(Shutdown.EXTRA_SHUTDOWN_CAUSE, toString());
            return b;
        }
    }

    public enum State {
        CONFIRMATION,
        CANCELED,
        CONFIRMED,
        SHUTTING_DOWN
    }

    public Shutdown(Reason reason) {
        this.reason = reason;
        this.state = reason.requireConfirmation ? State.CONFIRMATION : State.CONFIRMED;
    }

    public Shutdown(Reason reason, State state) {
        this.reason = reason;
        this.state = state;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Shutdown{");
        sb.append("reason=").append(this.reason);
        sb.append(", state=").append(this.state);
        sb.append('}');
        return sb.toString();
    }
}
