package com.navdy.hud.app.storage.db.helper;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.text.TextUtils;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.FavoriteContactsManager;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority;
import com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable;
import com.navdy.hud.app.storage.db.HudDatabase;
import com.navdy.hud.app.storage.db.table.FavoriteContactsTable;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FavoriteContactsPersistenceHelper {
    private static final String BULK_INSERT_SQL = "INSERT INTO fav_contacts(device_id,name,number,number_type,def_image,number_numeric) VALUES (?,?,?,?,?,?)";
    private static final int DEFAULT_IMAGE_INDEX_ORDINAL = 3;
    private static final List<Contact> EMPTY_LIST = new LinkedList();
    private static final String[] FAV_CONTACT_ARGS = new String[1];
    private static final String FAV_CONTACT_ORDER_BY = "rowid";
    private static final String[] FAV_CONTACT_PROJECTION = new String[]{"name", "number", "number_type", "def_image", "number_numeric"};
    private static final String FAV_CONTACT_WHERE = "device_id=?";
    private static final int NAME_ORDINAL = 0;
    private static final int NUMBER_NUMERIC_ORDINAL = 4;
    private static final int NUMBER_ORDINAL = 1;
    private static final int NUMBER_TYPE_ORDINAL = 2;
    private static final Object lockObj = new Object();
    private static final Logger sLogger = new Logger(FavoriteContactsPersistenceHelper.class);

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static List<Contact> getFavoriteContacts(String driverId) {
        GenericUtil.checkNotOnMainThread();
        if (DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
            return EMPTY_LIST;
        }
        synchronized (lockObj) {
            SQLiteDatabase database = HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new DatabaseNotAvailable();
            }
            List<Contact> favContacts;
            FAV_CONTACT_ARGS[0] = driverId;
            Cursor cursor = database.query(FavoriteContactsTable.TABLE_NAME, FAV_CONTACT_PROJECTION, FAV_CONTACT_WHERE, FAV_CONTACT_ARGS, null, null, "rowid");
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
                        favContacts = new ArrayList();
                        while (true) {
                            String name = cursor.getString(0);
                            String number = cursor.getString(1);
                            int numberType = cursor.getInt(2);
                            favContacts.add(new Contact(name, number, NumberType.buildFromValue(numberType), cursor.getInt(3), cursor.getLong(4)));
                            if (!cursor.moveToNext()) {
                                break;
                            }
                        }
                    }
                } finally {
                    IOUtils.closeStream(cursor);
                }
            }
            favContacts = EMPTY_LIST;
            IOUtils.closeStream(cursor);
            return favContacts;
        }
    }

    public static void storeFavoriteContacts(String driverId, List<Contact> favContacts, boolean refreshPhotos) {
        GenericUtil.checkNotOnMainThread();
        sLogger.v("fav-contacts passed [" + favContacts.size() + "]");
        FavoriteContactsManager favoriteContactsManager = FavoriteContactsManager.getInstance();
        deleteFavoriteContacts(driverId);
        if (favContacts.size() == 0) {
            favoriteContactsManager.setFavoriteContacts(null);
            return;
        }
        PhoneImageDownloader.getInstance().clearAllPhotoCheckEntries();
        synchronized (lockObj) {
            SQLiteDatabase database = HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new DatabaseNotAvailable();
            }
            SQLiteStatement sqLiteStatement = database.compileStatement(BULK_INSERT_SQL);
            int rows = 0;
            try {
                ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
                database.beginTransaction();
                for (Contact favContact : favContacts) {
                    sqLiteStatement.clearBindings();
                    sqLiteStatement.bindString(1, driverId);
                    if (TextUtils.isEmpty(favContact.name)) {
                        sqLiteStatement.bindNull(2);
                    } else {
                        sqLiteStatement.bindString(2, favContact.name);
                    }
                    sqLiteStatement.bindString(3, favContact.number);
                    sqLiteStatement.bindLong(4, (long) favContact.numberType.getValue());
                    favContact.defaultImageIndex = contactImageHelper.getContactImageIndex(favContact.number);
                    sqLiteStatement.bindLong(5, (long) favContact.defaultImageIndex);
                    sqLiteStatement.bindLong(6, favContact.numericNumber);
                    sqLiteStatement.execute();
                    rows++;
                    if (refreshPhotos) {
                        PhoneImageDownloader.getInstance().submitDownload(favContact.number, Priority.NORMAL, PhotoType.PHOTO_CONTACT, favContact.name);
                    }
                }
                database.setTransactionSuccessful();
                FavoriteContactsManager.getInstance().setFavoriteContacts(favContacts);
                sLogger.v("fav-contacts rows added:" + rows);
            } finally {
                database.endTransaction();
            }
        }
    }

    private static void deleteFavoriteContacts(String driverId) {
        try {
            synchronized (lockObj) {
                SQLiteDatabase database = HudDatabase.getInstance().getWritableDatabase();
                if (database == null) {
                    throw new DatabaseNotAvailable();
                }
                FAV_CONTACT_ARGS[0] = driverId;
                sLogger.v("fav-contact rows deleted:" + database.delete(FavoriteContactsTable.TABLE_NAME, FAV_CONTACT_WHERE, FAV_CONTACT_ARGS));
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
