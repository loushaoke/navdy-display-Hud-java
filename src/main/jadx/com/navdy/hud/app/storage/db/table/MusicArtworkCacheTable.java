package com.navdy.hud.app.storage.db.table;

import android.database.sqlite.SQLiteDatabase;
import com.navdy.hud.app.storage.db.DatabaseUtil;
import com.navdy.service.library.log.Logger;

public class MusicArtworkCacheTable {
    public static final String ALBUM = "album";
    public static final String AUTHOR = "author";
    public static final String FILE_NAME = "file_name";
    public static final String NAME = "name";
    public static final String TABLE_NAME = "music_artwork_cache";
    public static final String TYPE = "type";
    public static final String TYPE_AUDIOBOOK = "audiobook";
    public static final String TYPE_MUSIC = "music";
    public static final String TYPE_PLAYLIST = "playlist";
    public static final String TYPE_PODCAST = "podcast";
    private static final String[] VALID_TYPES = new String[]{TYPE_MUSIC, TYPE_PLAYLIST, TYPE_PODCAST, TYPE_AUDIOBOOK};
    private static final Logger sLogger = new Logger(MusicArtworkCacheTable.class);

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" + "type" + " TEXT NOT NULL CHECK(" + "type" + " IN " + validTypes() + "), " + AUTHOR + " TEXT, " + ALBUM + " TEXT, " + "name" + " TEXT, " + FILE_NAME + " TEXT NOT NULL, UNIQUE (" + "type" + ", " + AUTHOR + ", " + ALBUM + ", " + "name" + ") ON CONFLICT REPLACE);");
        DatabaseUtil.createIndex(db, TABLE_NAME, "type", sLogger);
        DatabaseUtil.createIndex(db, TABLE_NAME, ALBUM, sLogger);
        DatabaseUtil.createIndex(db, TABLE_NAME, AUTHOR, sLogger);
        DatabaseUtil.createIndex(db, TABLE_NAME, "name", sLogger);
    }

    private static String validTypes() {
        StringBuilder sb = new StringBuilder();
        sb.append("('");
        sb.append(VALID_TYPES[0]);
        for (int i = 1; i < VALID_TYPES.length; i++) {
            sb.append("', '");
            sb.append(VALID_TYPES[i]);
        }
        sb.append("')");
        return sb.toString();
    }
}
