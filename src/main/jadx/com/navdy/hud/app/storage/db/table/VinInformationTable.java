package com.navdy.hud.app.storage.db.table;

import android.database.sqlite.SQLiteDatabase;
import com.navdy.hud.app.storage.db.DatabaseUtil;
import com.navdy.service.library.log.Logger;

public class VinInformationTable {
    public static final String TABLE_NAME = "vininfo";
    public static final String UNKNOWN_VIN = "UNKNOWN_VIN";
    public static final String VIN_INFO = "info";
    public static final String VIN_NUMBER = "vin";
    private static final Logger sLogger = new Logger(VinInformationTable.class);

    public static void createTable(SQLiteDatabase db) {
        createTable_1(db);
    }

    public static void createTable_1(SQLiteDatabase db) {
        String tableName = TABLE_NAME;
        db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName + " (" + "vin" + " TEXT NOT NULL," + "info" + " TEXT" + ");");
        sLogger.v("createdTable:" + tableName);
        DatabaseUtil.createIndex(db, TABLE_NAME, "vin", sLogger);
    }
}
