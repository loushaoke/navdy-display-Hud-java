package com.navdy.hud.app.storage.db.helper;

import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.storage.db.DatabaseUtil.DatabaseNotAvailable;
import com.navdy.hud.app.storage.db.HudDatabase;
import com.navdy.hud.app.storage.db.table.VinInformationTable;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;

public class VinInformationHelper {
    private static final String[] DATASET_ARGS = new String[1];
    private static final String[] DATASET_PROJECTION = new String[]{"info"};
    private static final String DATASET_WHERE = "vin=?";
    private static final int VIN_INFO_ORDINAL = 0;
    public static final String VIN_SHARED_PREF = "vin";
    private static final String VIN_SHARED_PREF_NAME = "activeVin";
    private static final Object lockObj = new Object();
    private static final Logger sLogger = new Logger(VinInformationHelper.class);

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static String getVinInfo(String vin) {
        GenericUtil.checkNotOnMainThread();
        synchronized (lockObj) {
            SQLiteDatabase database = HudDatabase.getInstance().getWritableDatabase();
            if (database == null) {
                throw new DatabaseNotAvailable();
            }
            DATASET_ARGS[0] = vin;
            Cursor cursor = database.query(VinInformationTable.TABLE_NAME, DATASET_PROJECTION, DATASET_WHERE, DATASET_ARGS, null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        String string = cursor.getString(0);
                    }
                } finally {
                    IOUtils.closeStream(cursor);
                }
            }
            IOUtils.closeStream(cursor);
            return null;
        }
    }

    public static void storeVinInfo(String vin, String info) {
        GenericUtil.checkNotOnMainThread();
        deleteVinInfo(vin);
        synchronized (lockObj) {
            try {
                SQLiteDatabase database = HudDatabase.getInstance().getWritableDatabase();
                if (database == null) {
                    throw new DatabaseNotAvailable();
                }
                ContentValues values = new ContentValues();
                values.put("vin", vin);
                values.put("info", info);
                database.insert(VinInformationTable.TABLE_NAME, null, values);
                sLogger.v("added vin info [ " + vin + " , " + info + "]");
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static void deleteVinInfo(String vin) {
        GenericUtil.checkNotOnMainThread();
        synchronized (lockObj) {
            try {
                SQLiteDatabase database = HudDatabase.getInstance().getWritableDatabase();
                if (database == null) {
                    throw new DatabaseNotAvailable();
                }
                DATASET_ARGS[0] = vin;
                sLogger.v("vin info deleted:" + vin + " result=" + database.delete(VinInformationTable.TABLE_NAME, DATASET_WHERE, DATASET_ARGS));
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public static SharedPreferences getVinPreference() {
        return HudApplication.getAppContext().getSharedPreferences(VIN_SHARED_PREF_NAME, 4);
    }
}
