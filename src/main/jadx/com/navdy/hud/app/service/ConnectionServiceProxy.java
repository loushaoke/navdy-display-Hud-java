package com.navdy.hud.app.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.TransactionTooLargeException;
import com.navdy.hud.app.IEventListener.Stub;
import com.navdy.hud.app.IEventSource;
import com.navdy.hud.app.event.InitEvents.ConnectionServiceStarted;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;

public class ConnectionServiceProxy {
    private static final boolean VERBOSE = true;
    private static final Logger sLogger = new Logger(ConnectionServiceProxy.class);
    protected Bus bus;
    private StartDriveRecordingEvent driveRecordingEvent;
    private Stub eventListener = new Stub() {
        public void onEvent(byte[] bytes) throws RemoteException {
            ConnectionServiceProxy.this.postEvent(bytes);
        }
    };
    protected Context mContext;
    protected IEventSource mEventSource;
    protected Wire mWire;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            ConnectionServiceProxy.sLogger.i("Connected to IEventSource service");
            ConnectionServiceProxy.this.mEventSource = IEventSource.Stub.asInterface(service);
            try {
                ConnectionServiceProxy.this.mEventSource.addEventListener(ConnectionServiceProxy.this.eventListener);
            } catch (RemoteException e) {
                ConnectionServiceProxy.sLogger.e("Failed to connect event listener", e);
            }
            ConnectionServiceProxy.sLogger.d("Starting connection service state machine");
            ConnectionServiceProxy.this.mContext.startService(new Intent(ConnectionServiceProxy.this.mContext, HudConnectionService.class));
            ConnectionServiceProxy.this.bus.post(new ConnectionServiceStarted());
        }

        public void onServiceDisconnected(ComponentName name) {
            ConnectionServiceProxy.sLogger.i("Disconnected from IEventSource service");
            ConnectionServiceProxy.this.bus.post(new ConnectionStateChange("", ConnectionState.CONNECTION_DISCONNECTED));
            ConnectionServiceProxy.this.mEventSource = null;
        }
    };

    public ConnectionServiceProxy(Context context, Bus bus) {
        this.mContext = context;
        this.bus = bus;
        bus.register(this);
        this.mWire = new Wire(Ext_NavdyEvent.class);
        sLogger.i("Creating connection service proxy:" + this);
    }

    public void connect() {
        if (this.mEventSource == null) {
            sLogger.d("Starting connection service");
            Intent intent = new Intent(this.mContext, HudConnectionService.class);
            this.mContext.startService(intent);
            sLogger.d("Binding to connection service");
            intent.setAction(IEventSource.class.getName());
            this.mContext.bindService(intent, this.serviceConnection, 0);
        }
    }

    public void disconnect() {
        if (this.mEventSource != null) {
            try {
                this.mEventSource.removeEventListener(this.eventListener);
            } catch (RemoteException e) {
                sLogger.e("Failed to remove event listener", e);
            }
            this.mContext.unbindService(this.serviceConnection);
            this.mContext.stopService(new Intent(this.mContext, HudConnectionService.class));
            this.mEventSource = null;
        }
    }

    public boolean connected() {
        return this.mEventSource != null;
    }

    public Bus getBus() {
        return this.bus;
    }

    @Subscribe
    public void onEvent(NavdyEvent event) {
        Message message = NavdyEventUtil.messageFromEvent(event);
        if (message != null) {
            if (sLogger.isLoggable(2)) {
                sLogger.d("Received message: " + message);
            }
            this.bus.post(message);
            switch (event.type) {
                case StartDriveRecordingEvent:
                    this.driveRecordingEvent = (StartDriveRecordingEvent) message;
                    return;
                case StopDriveRecordingEvent:
                    this.driveRecordingEvent = null;
                    return;
                default:
                    return;
            }
        }
    }

    public void postEvent(byte[] bytes) {
        int eventTypeIndex;
        if (this.bus != null) {
            try {
                this.bus.post((NavdyEvent) this.mWire.parseFrom(bytes, NavdyEvent.class));
                return;
            } catch (Throwable th) {
            }
        } else {
            return;
        }
        sLogger.e("Ignoring invalid navdy event[" + eventTypeIndex + "]", e);
    }

    public void postRemoteEvent(final NavdyDeviceId deviceId, final NavdyEvent event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (ConnectionServiceProxy.this.mEventSource != null) {
                    try {
                        long l1 = SystemClock.elapsedRealtime();
                        byte[] eventData = event.toByteArray();
                        long l2 = SystemClock.elapsedRealtime();
                        int len = eventData.length;
                        if (ConnectionServiceProxy.sLogger.isLoggable(2)) {
                            ConnectionServiceProxy.sLogger.v("NAVDY-PACKET [H2P-Outgoing-Event] " + event);
                        }
                        long time = l2 - l1;
                        if (time >= 100) {
                            ConnectionServiceProxy.sLogger.v("NAVDY-PACKET [H2P-Outgoing-Event]" + event.type.name() + " took: " + time + " len:" + len);
                        }
                        ConnectionServiceProxy.this.mEventSource.postRemoteEvent(deviceId.toString(), eventData);
                        return;
                    } catch (DeadObjectException ex) {
                        ConnectionServiceProxy.sLogger.e("Exception posting remote event, server died", ex);
                        return;
                    } catch (TransactionTooLargeException e) {
                        ConnectionServiceProxy.sLogger.e("Exception posting remote event, large payload type[" + event.type + "] size[" + -1 + "]", e);
                        return;
                    } catch (Throwable t) {
                        ConnectionServiceProxy.sLogger.e("Exception posting remote event", t);
                        return;
                    }
                }
                ConnectionServiceProxy.sLogger.e("Failed to send event - service connection broken" + event);
            }
        }, 11);
    }

    public StartDriveRecordingEvent getDriverRecordingEvent() {
        return this.driveRecordingEvent;
    }
}
