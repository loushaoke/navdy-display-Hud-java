package com.navdy.hud.app.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.SystemClock;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.service.S3FileUploadService.UploadFinished;
import com.navdy.hud.app.service.S3FileUploadService.UploadQueue;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Inject;
import mortar.Mortar;

public class GestureVideosSyncService extends S3FileUploadService {
    private static final int MAX_GESTURE_VIDEOS_TO_UPLOAD = 100;
    public static final String NAVDY_GESTURE_VIDEOS_BUCKET = "navdy-gesture-videos";
    private static AtomicBoolean isUploading = new AtomicBoolean(false);
    private static boolean mIsInitialized = false;
    private static Request sCurrentRequest;
    private static String sGestureVideosFolder;
    private static final UploadQueue sGestureVideosUploadQueue = new UploadQueue();
    private static Logger sLogger = new Logger(GestureVideosSyncService.class);
    @Inject
    Bus bus;

    public void onCreate() {
        Mortar.inject(HudApplication.getAppContext(), this);
        super.onCreate();
    }

    public GestureVideosSyncService() {
        super("GESTURE_VIDEOS_SYNC");
    }

    protected void initialize() {
        initializeIfNecessary();
    }

    protected AtomicBoolean getIsUploading() {
        return isUploading;
    }

    protected Request getCurrentRequest() {
        return sCurrentRequest;
    }

    protected void setCurrentRequest(Request currentRequest) {
        sCurrentRequest = currentRequest;
    }

    protected UploadQueue getUploadQueue() {
        return sGestureVideosUploadQueue;
    }

    protected String getKeyPrefix(File file) {
        return "archives" + File.separator + file.getParentFile().getName();
    }

    protected String getAWSBucket() {
        return NAVDY_GESTURE_VIDEOS_BUCKET;
    }

    public static void addGestureVideoToUploadQueue(File file, String userTag) {
        initializeIfNecessary(file);
        synchronized (sGestureVideosUploadQueue) {
            sLogger.d("Add gesture video to upload " + file.getName());
            sGestureVideosUploadQueue.add(new Request(file, userTag));
            sLogger.d("Queue size : " + sGestureVideosUploadQueue.size());
            if (!isUploading.get() && sGestureVideosUploadQueue.size() > 100) {
                sLogger.d("Number of videos to upload exceeded ");
                sGestureVideosUploadQueue.pop();
            }
        }
    }

    private static void initializeIfNecessary() {
        initializeIfNecessary(null);
    }

    private static void initializeIfNecessary(File fileToIgnore) {
        synchronized (sGestureVideosUploadQueue) {
            if (!mIsInitialized) {
                sLogger.d("Not initialized , initializing now");
                if (!mIsInitialized) {
                    sGestureVideosFolder = PathManager.getInstance().getGestureVideosSyncFolder();
                    File[] directories = new File(sGestureVideosFolder).listFiles();
                    ArrayList<File> filesToPopulate = new ArrayList();
                    for (File sessionDirectory : directories) {
                        sLogger.d("Session directory :" + sessionDirectory);
                        if (sessionDirectory.isDirectory()) {
                            File[] gestureVideoFiles = sessionDirectory.listFiles();
                            if (gestureVideoFiles.length == 0) {
                                sLogger.d("Remove empty directory:" + sessionDirectory);
                                IOUtils.deleteDirectory(HudApplication.getAppContext(), sessionDirectory);
                            }
                            for (File gestureVideoFile : gestureVideoFiles) {
                                sLogger.d("Gesture video :" + gestureVideoFile);
                                if (gestureVideoFile.isFile()) {
                                    if (fileToIgnore != null) {
                                        try {
                                            if (gestureVideoFile.getCanonicalPath().equals(fileToIgnore.getCanonicalPath())) {
                                                continue;
                                            }
                                        } catch (IOException e) {
                                        }
                                    }
                                    filesToPopulate.add(gestureVideoFile);
                                }
                            }
                            continue;
                        }
                    }
                    sLogger.d("Number of Gesture videos :" + filesToPopulate.size());
                    S3FileUploadService.populateFilesQueue(filesToPopulate, sGestureVideosUploadQueue, 100);
                    mIsInitialized = true;
                }
            }
        }
    }

    protected void uploadFinished(boolean succeeded, String path, String userTag) {
        this.bus.post(new UploadFinished(succeeded, path, userTag));
    }

    public void sync() {
        syncNow();
    }

    public static void syncNow() {
        sLogger.d("synNow");
        Intent intent = new Intent(HudApplication.getAppContext(), GestureVideosSyncService.class);
        intent.setAction(S3FileUploadService.ACTION_SYNC);
        HudApplication.getAppContext().startService(intent);
    }

    public void reSchedule() {
        sLogger.d("reschedule");
        scheduleWithDelay(10000);
    }

    public boolean canCompleteRequest(Request request) {
        return false;
    }

    protected Logger getLogger() {
        return sLogger;
    }

    public static void scheduleWithDelay(long delay) {
        Intent intent = new Intent(HudApplication.getAppContext(), GestureVideosSyncService.class);
        intent.setAction(S3FileUploadService.ACTION_SYNC);
        ((AlarmManager) HudApplication.getAppContext().getSystemService("alarm")).setExact(3, SystemClock.elapsedRealtime() + delay, PendingIntent.getService(HudApplication.getAppContext(), 123, intent, 268435456));
    }
}
