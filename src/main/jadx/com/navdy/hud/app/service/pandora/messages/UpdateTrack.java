package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;

public class UpdateTrack extends BaseIncomingOneIntMessage {
    public UpdateTrack(int value) {
        super(value);
    }

    protected static BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        return new UpdateTrack(BaseIncomingOneIntMessage.parseIntValue(payload));
    }

    public String toString() {
        return "New track with token: " + this.value;
    }
}
