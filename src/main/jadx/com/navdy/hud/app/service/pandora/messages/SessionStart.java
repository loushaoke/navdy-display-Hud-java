package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class SessionStart extends BaseOutgoingConstantMessage {
    private static final int ACCESSORY_ID_FIELD_LENGTH = 8;
    public static final SessionStart INSTANCE = new SessionStart();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        return super.buildPayload();
    }

    private SessionStart() {
    }

    public ByteArrayOutputStream putThis(ByteArrayOutputStream os) throws IOException, StringOverflowException, UnexpectedEndOfStringException {
        BaseOutgoingMessage.putByte(os, (byte) 0);
        BaseOutgoingMessage.putShort(os, (short) 3);
        BaseOutgoingMessage.putFixedLengthASCIIString(os, 8, BaseMessage.ACCESSORY_ID);
        BaseOutgoingMessage.putShort(os, (short) 100);
        BaseOutgoingMessage.putByte(os, (byte) 1);
        BaseOutgoingMessage.putByte(os, (byte) 2);
        BaseOutgoingMessage.putShort(os, (short) 0);
        return os;
    }

    public String toString() {
        return "Session Start";
    }
}
