package com.navdy.hud.app.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.SystemClock;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.obd.ObdCanBusRecordingPolicy;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.service.S3FileUploadService.UploadQueue;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import mortar.Mortar;

public class ObdCANBusDataUploadService extends S3FileUploadService {
    private static final int MAX_FILES_TO_UPLOAD = 5;
    public static final String NAVDY_GESTURE_VIDEOS_BUCKET = "navdy-obd-data";
    private static AtomicBoolean isUploading = new AtomicBoolean(false);
    private static boolean mIsInitialized = false;
    private static Request sCurrentRequest;
    private static Logger sLogger = new Logger(ObdCANBusDataUploadService.class);
    private static String sObdDatFilesFolder;
    private static final UploadQueue sObdDataFilesUploadQueue = new UploadQueue();

    public ObdCANBusDataUploadService() {
        super(ObdCANBusDataUploadService.class.getName());
    }

    public void onCreate() {
        Mortar.inject(HudApplication.getAppContext(), this);
        super.onCreate();
    }

    protected void initialize() {
        initializeIfNecessary();
    }

    protected AtomicBoolean getIsUploading() {
        return isUploading;
    }

    protected Request getCurrentRequest() {
        return sCurrentRequest;
    }

    protected void setCurrentRequest(Request currentRequest) {
        sCurrentRequest = currentRequest;
    }

    protected UploadQueue getUploadQueue() {
        return sObdDataFilesUploadQueue;
    }

    protected String getKeyPrefix(File file) {
        return "CANBus";
    }

    protected String getAWSBucket() {
        return NAVDY_GESTURE_VIDEOS_BUCKET;
    }

    public static void populateFilesQueue(ArrayList<File> files, UploadQueue filesQueue, int maxSize) {
        if (files != null) {
            Iterator it = files.iterator();
            while (it.hasNext()) {
                File file = (File) it.next();
                if (file.isFile()) {
                    filesQueue.add(new Request(file, null));
                    if (filesQueue.size() == maxSize) {
                        filesQueue.pop();
                    }
                }
            }
        }
    }

    public static void addObdDataFileToQueue(File file) {
        initializeIfNecessary(file);
        synchronized (sObdDataFilesUploadQueue) {
            sObdDataFilesUploadQueue.add(new Request(file, ""));
            sLogger.d("Queue size : " + sObdDataFilesUploadQueue.size());
            if (!isUploading.get()) {
                while (sObdDataFilesUploadQueue.size() > 5) {
                    sObdDataFilesUploadQueue.pop();
                }
            }
        }
    }

    private static void initializeIfNecessary() {
        initializeIfNecessary(null);
    }

    private static void initializeIfNecessary(File fileToIgnore) {
        synchronized (sObdDataFilesUploadQueue) {
            if (!mIsInitialized) {
                sLogger.d("Not initialized , initializing now");
                if (!mIsInitialized) {
                    sObdDatFilesFolder = ObdCanBusRecordingPolicy.FILES_TO_UPLOAD_DIRECTORY;
                    File[] files = new File(sObdDatFilesFolder).listFiles();
                    ArrayList<File> filesToPopulate = new ArrayList();
                    for (File obdCanBusDataBundle : files) {
                        if (obdCanBusDataBundle.isFile() && obdCanBusDataBundle.getName().endsWith(".zip")) {
                            sLogger.d("ObdCanBus Data bundle :" + obdCanBusDataBundle);
                            filesToPopulate.add(obdCanBusDataBundle);
                        }
                    }
                    populateFilesQueue(filesToPopulate, sObdDataFilesUploadQueue, 5);
                    mIsInitialized = true;
                }
            }
        }
    }

    protected void uploadFinished(boolean succeeded, String path, String userTag) {
        if (succeeded) {
            ObdManager.getInstance().getObdCanBusRecordingPolicy().onFileUploaded(new File(path));
        } else {
            sLogger.e("File upload failed " + path);
        }
    }

    public void sync() {
        syncNow();
    }

    public static void syncNow() {
        sLogger.d("synNow");
        Intent intent = new Intent(HudApplication.getAppContext(), ObdCANBusDataUploadService.class);
        intent.setAction(S3FileUploadService.ACTION_SYNC);
        HudApplication.getAppContext().startService(intent);
    }

    public void reSchedule() {
        sLogger.d("reschedule");
        scheduleWithDelay(10000);
    }

    public boolean canCompleteRequest(Request request) {
        return true;
    }

    protected Logger getLogger() {
        return sLogger;
    }

    public static void scheduleWithDelay(long delay) {
        Intent intent = new Intent(HudApplication.getAppContext(), ObdCANBusDataUploadService.class);
        intent.setAction(S3FileUploadService.ACTION_SYNC);
        ((AlarmManager) HudApplication.getAppContext().getSystemService("alarm")).setExact(3, SystemClock.elapsedRealtime() + delay, PendingIntent.getService(HudApplication.getAppContext(), 123, intent, 268435456));
    }
}
