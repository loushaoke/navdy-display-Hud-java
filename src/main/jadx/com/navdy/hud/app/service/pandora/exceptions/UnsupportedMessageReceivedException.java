package com.navdy.hud.app.service.pandora.exceptions;

public class UnsupportedMessageReceivedException extends Exception {
    public UnsupportedMessageReceivedException(byte msgType) {
        super("Message received from Pandora is not supported: " + String.format("%02X", new Object[]{Byte.valueOf(msgType)}));
    }
}
