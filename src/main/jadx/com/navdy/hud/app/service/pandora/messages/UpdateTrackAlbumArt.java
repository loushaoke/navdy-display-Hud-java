package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import java.nio.ByteBuffer;

public class UpdateTrackAlbumArt extends BaseIncomingMessage {
    private static int MESSAGE_LENGTH = 9;
    public int imageLength;
    public int trackToken;

    protected static BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        if (payload.length != MESSAGE_LENGTH) {
            throw new CorruptedPayloadException();
        }
        ByteBuffer buffer = ByteBuffer.wrap(payload);
        buffer.get();
        UpdateTrackAlbumArt result = new UpdateTrackAlbumArt();
        result.trackToken = buffer.getInt();
        result.imageLength = buffer.getInt();
        return result;
    }

    public String toString() {
        return "Artwork image loaded on client for track: " + this.trackToken;
    }
}
