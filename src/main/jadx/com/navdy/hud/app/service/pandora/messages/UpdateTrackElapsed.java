package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import java.nio.ByteBuffer;

public class UpdateTrackElapsed extends BaseIncomingMessage {
    public short elapsed;
    public int trackToken;

    protected static BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        ByteBuffer buffer = ByteBuffer.wrap(payload);
        buffer.get();
        UpdateTrackElapsed result = new UpdateTrackElapsed();
        result.trackToken = buffer.getInt();
        result.elapsed = buffer.getShort();
        return result;
    }

    public String toString() {
        return "Got track's elapsed time update - seconds: " + this.elapsed;
    }
}
