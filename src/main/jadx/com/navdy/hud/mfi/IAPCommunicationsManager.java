package com.navdy.hud.mfi;

import android.text.TextUtils;
import android.util.Log;
import com.navdy.hud.mfi.CallStateUpdate.Direction;
import com.navdy.hud.mfi.CallStateUpdate.DisconnectReason;
import com.navdy.hud.mfi.CallStateUpdate.Status;

public class IAPCommunicationsManager extends IAPControlMessageProcessor {
    private static String TAG = IAPCommunicationsManager.class.getSimpleName();
    public static iAPMessage[] mMessages = new iAPMessage[]{iAPMessage.CallStateUpdate, iAPMessage.CommunicationUpdate};
    private Status callStatus;
    private String callUUid;
    private IAPCommunicationsUpdateListener mUpdatesListener;

    enum AcceptAction {
        HoldAndAccept,
        EndAndAccept
    }

    enum AcceptCall {
        AcceptAction,
        CallUUID
    }

    enum EndCall {
        EndAction,
        CallUUID
    }

    enum EndCallAction {
        EndDecline,
        EndAll
    }

    enum InitiateCall {
        Type,
        DestinationID,
        Service,
        AddressBookId
    }

    enum InitiateCallType {
        Destination,
        VoiceMail,
        Redial
    }

    enum MuteStatusUpdate {
        MuteStatus
    }

    public enum Service {
        Unknown,
        Telephony,
        FaceTimeAudio,
        FaceTimeVideo
    }

    enum StartCallStateUpdates {
        RemoteID,
        DisplayName,
        Status,
        Direction,
        CallUUID,
        Skip,
        AddressBookID,
        Label,
        Service,
        IsConferenced,
        ConferenceGroup,
        DisconnectReason
    }

    enum StartCommunicationUpdates {
        MuteStatus(9);
        
        int id;

        private StartCommunicationUpdates(int id) {
            this.id = id;
        }
    }

    public IAPCommunicationsManager(iAPProcessor processor) {
        super(mMessages, processor);
    }

    public void setUpdatesListener(IAPCommunicationsUpdateListener listener) {
        this.mUpdatesListener = listener;
    }

    public void bProcessControlMessage(iAPMessage message, int session, byte[] data) {
        IAP2Params params = iAPProcessor.parse(data);
        if (message.id == iAPMessage.CallStateUpdate.id) {
            CallStateUpdate update = parseCallStateUpdate(params);
            this.callStatus = update.status;
            this.callUUid = update.callUUID;
            try {
                this.mUpdatesListener.onCallStateUpdate(update);
            } catch (Throwable t) {
                Log.d(TAG, "Bad call state update listener " + t);
            }
        } else if (message.id == iAPMessage.CommunicationUpdate.id) {
            try {
                this.mUpdatesListener.onCommunicationUpdate(parseCommunicationUpdate(params));
            } catch (Throwable t2) {
                Log.d(TAG, "Bad call state update listener " + t2);
            }
        }
    }

    public void startUpdates() {
        IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.StartCallStateUpdates);
        message.addNone(StartCallStateUpdates.RemoteID).addNone(StartCallStateUpdates.DisplayName).addNone(StartCallStateUpdates.Status).addNone(StartCallStateUpdates.Direction).addNone(StartCallStateUpdates.CallUUID).addNone(StartCallStateUpdates.AddressBookID).addNone(StartCallStateUpdates.Label).addNone(StartCallStateUpdates.Service).addNone(StartCallStateUpdates.IsConferenced).addNone(StartCallStateUpdates.ConferenceGroup).addNone(StartCallStateUpdates.DisconnectReason);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void stopUpdates() {
        this.miAPProcessor.sendControlMessage(new IAP2SessionMessage(iAPMessage.StopCallStateUpdates));
    }

    public void startCommunicationUpdates() {
    }

    public void stopCommunicationUpdates() {
    }

    public static CallStateUpdate parseCallStateUpdate(IAP2Params updateParams) {
        CallStateUpdate update = new CallStateUpdate();
        if (updateParams.hasParam(StartCallStateUpdates.RemoteID)) {
            update.remoteID = updateParams.getUTF8(StartCallStateUpdates.RemoteID);
        }
        if (updateParams.hasParam(StartCallStateUpdates.DisplayName)) {
            update.displayName = updateParams.getUTF8(StartCallStateUpdates.DisplayName);
        }
        if (updateParams.hasParam(StartCallStateUpdates.Status)) {
            update.status = (Status) updateParams.getEnum(Status.class, StartCallStateUpdates.Status);
        }
        if (updateParams.hasParam(StartCallStateUpdates.Direction)) {
            update.direction = (Direction) updateParams.getEnum(Direction.class, StartCallStateUpdates.Direction);
        }
        if (updateParams.hasParam(StartCallStateUpdates.CallUUID)) {
            update.callUUID = updateParams.getUTF8(StartCallStateUpdates.CallUUID);
        }
        if (updateParams.hasParam(StartCallStateUpdates.AddressBookID)) {
            update.addressBookID = updateParams.getUTF8(StartCallStateUpdates.AddressBookID);
        }
        if (updateParams.hasParam(StartCallStateUpdates.Label)) {
            update.label = updateParams.getUTF8(StartCallStateUpdates.Label);
        }
        if (updateParams.hasParam(StartCallStateUpdates.Service)) {
            update.service = (Service) updateParams.getEnum(Service.class, StartCallStateUpdates.Service);
        }
        if (updateParams.hasParam(StartCallStateUpdates.IsConferenced)) {
            update.isConferenced = updateParams.getBoolean(StartCallStateUpdates.IsConferenced);
        }
        if (updateParams.hasParam(StartCallStateUpdates.ConferenceGroup)) {
            update.conferenceGroup = updateParams.getUInt8(StartCallStateUpdates.ConferenceGroup);
        }
        if (updateParams.hasParam(StartCallStateUpdates.DisconnectReason)) {
            update.disconnectReason = (DisconnectReason) updateParams.getEnum(DisconnectReason.class, StartCallStateUpdates.DisconnectReason);
        }
        return update;
    }

    public static CommunicationUpdate parseCommunicationUpdate(IAP2Params updateParams) {
        CommunicationUpdate communicationUpdate = new CommunicationUpdate();
        if (updateParams.hasParam(StartCommunicationUpdates.MuteStatus.id)) {
            communicationUpdate.muteStatus = updateParams.getBoolean(StartCommunicationUpdates.MuteStatus.id);
        }
        return communicationUpdate;
    }

    public void initiateDestinationCall(String number) {
        IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.InitiateCall);
        message.addEnum(InitiateCall.Type, InitiateCallType.Destination);
        message.addString(InitiateCall.DestinationID, number);
        message.addEnum(InitiateCall.Service, Service.Telephony);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void redial() {
        IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.InitiateCall);
        message.addEnum(InitiateCall.Type, InitiateCallType.Redial);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void callVoiceMail() {
        IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.InitiateCall);
        message.addEnum(InitiateCall.Type, InitiateCallType.VoiceMail);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void acceptCall(String callUUid) {
        if (!TextUtils.isEmpty(callUUid)) {
            IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.AcceptCall);
            message.addEnum(AcceptCall.AcceptAction, AcceptAction.HoldAndAccept);
            message.addString(AcceptCall.CallUUID, callUUid);
            this.miAPProcessor.sendControlMessage(message);
        }
    }

    public void acceptCall() {
        if (!TextUtils.isEmpty(this.callUUid)) {
            IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.AcceptCall);
            message.addEnum(AcceptCall.AcceptAction, AcceptAction.HoldAndAccept);
            message.addString(AcceptCall.CallUUID, this.callUUid);
            this.miAPProcessor.sendControlMessage(message);
        }
    }

    public void endCall() {
        if (!TextUtils.isEmpty(this.callUUid)) {
            IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.EndCall);
            message.addEnum(EndCall.EndAction, EndCallAction.EndDecline);
            message.addString(EndCall.CallUUID, this.callUUid);
            this.miAPProcessor.sendControlMessage(message);
        }
    }

    public void endCall(String callUUid) {
        if (!TextUtils.isEmpty(callUUid)) {
            IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.EndCall);
            message.addEnum(EndCall.EndAction, EndCallAction.EndDecline);
            message.addString(EndCall.CallUUID, callUUid);
            this.miAPProcessor.sendControlMessage(message);
        }
    }

    public void mute() {
        IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.MuteStatusUpdate);
        message.addBoolean(MuteStatusUpdate.MuteStatus, true);
        this.miAPProcessor.sendControlMessage(message);
    }

    public void unMute() {
        IAP2SessionMessage message = new IAP2SessionMessage(iAPMessage.MuteStatusUpdate);
        message.addBoolean(MuteStatusUpdate.MuteStatus, false);
        this.miAPProcessor.sendControlMessage(message);
    }
}
