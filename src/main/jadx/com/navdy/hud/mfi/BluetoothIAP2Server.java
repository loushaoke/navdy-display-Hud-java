package com.navdy.hud.mfi;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.navdy.hud.mfi.LinkLayer.PhysicalLayer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.UUID;

public class BluetoothIAP2Server implements PhysicalLayer {
    public static final UUID ACCESSORY_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    public static final UUID DEVICE_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
    private static final String SDP_NAME = "Wireless iAP";
    public static final int STATE_CONNECTED = 3;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_LISTEN = 1;
    public static final int STATE_NONE = 0;
    private static final String TAG = "BluetoothIAP2Server";
    private LinkLayer linkLayer;
    private AcceptThread mAcceptThread;
    private final BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private final Handler mHandler;
    private int mState = 0;

    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;
            try {
                tmp = BluetoothIAP2Server.this.mAdapter.listenUsingRfcommWithServiceRecord(BluetoothIAP2Server.SDP_NAME, BluetoothIAP2Server.ACCESSORY_IAP2);
            } catch (IOException e) {
                Log.e(BluetoothIAP2Server.TAG, "Socket listen() failed", e);
            }
            this.mmServerSocket = tmp;
        }

        public void run() {
            Log.d(BluetoothIAP2Server.TAG, "Socket BEGIN mAcceptThread" + this);
            setName("AcceptThread");
            if (this.mmServerSocket == null) {
                Log.e(BluetoothIAP2Server.TAG, "No socket (maybe BT not enabled?) - END mAcceptThread");
                return;
            }
            while (BluetoothIAP2Server.this.mState != 3) {
                try {
                    BluetoothSocket socket = this.mmServerSocket.accept();
                    Log.d(BluetoothIAP2Server.TAG, "Socket accepted");
                    if (socket != null) {
                        synchronized (BluetoothIAP2Server.this) {
                            switch (BluetoothIAP2Server.this.mState) {
                                case 0:
                                case 3:
                                    try {
                                        socket.close();
                                        break;
                                    } catch (IOException e) {
                                        Log.e(BluetoothIAP2Server.TAG, "Could not close unwanted socket", e);
                                        break;
                                    }
                                case 1:
                                case 2:
                                    BluetoothIAP2Server.this.connected(socket, socket.getRemoteDevice());
                                    break;
                            }
                        }
                    }
                } catch (IOException e2) {
                    Log.e(BluetoothIAP2Server.TAG, "Socket accept() failed", e2);
                }
            }
            Log.i(BluetoothIAP2Server.TAG, "END mAcceptThread");
            return;
        }

        public void cancel() {
            Log.d(BluetoothIAP2Server.TAG, "Socket cancel " + this);
            try {
                if (this.mmServerSocket != null) {
                    this.mmServerSocket.close();
                }
            } catch (IOException e) {
                Log.e(BluetoothIAP2Server.TAG, "Socket close() of bluetoothServer failed", e);
            }
        }
    }

    private class ConnectThread extends Thread {
        private final BluetoothDevice mmDevice;
        private final BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device) {
            this.mmDevice = device;
            BluetoothSocket tmp = null;
            try {
                tmp = device.createRfcommSocketToServiceRecord(BluetoothIAP2Server.DEVICE_IAP2);
            } catch (IOException e) {
                Log.e(BluetoothIAP2Server.TAG, "Socket create() failed", e);
            }
            this.mmSocket = tmp;
        }

        public void run() {
            Log.i(BluetoothIAP2Server.TAG, "BEGIN mConnectThread");
            setName("ConnectThread");
            BluetoothIAP2Server.this.mAdapter.cancelDiscovery();
            try {
                this.mmSocket.connect();
                synchronized (BluetoothIAP2Server.this) {
                    BluetoothIAP2Server.this.mConnectThread = null;
                }
                BluetoothIAP2Server.this.connected(this.mmSocket, this.mmDevice);
            } catch (IOException e) {
                try {
                    this.mmSocket.close();
                } catch (IOException e2) {
                    Log.e(BluetoothIAP2Server.TAG, "unable to close() socket during connection failure", e2);
                }
                BluetoothIAP2Server.this.connectionFailed();
            }
        }

        public void cancel() {
            try {
                this.mmSocket.close();
            } catch (IOException e) {
                Log.e(BluetoothIAP2Server.TAG, "close() of connect socket failed", e);
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private final BluetoothSocket mmSocket;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(BluetoothIAP2Server.TAG, "create ConnectedThread");
            this.mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(BluetoothIAP2Server.TAG, "temp sockets not created", e);
            }
            this.mmInStream = tmpIn;
            this.mmOutStream = tmpOut;
        }

        public void run() {
            BluetoothDevice device = this.mmSocket.getRemoteDevice();
            Log.i(BluetoothIAP2Server.TAG, "BEGIN mConnectedThread - " + device.getAddress() + " name:" + device.getName());
            BluetoothIAP2Server.this.linkLayer.connectionStarted(device.getAddress(), device.getName());
            byte[] buffer = new byte[1024];
            while (true) {
                try {
                    BluetoothIAP2Server.this.linkLayer.queue(new LinkPacket(Arrays.copyOf(buffer, this.mmInStream.read(buffer))));
                } catch (IOException e) {
                    Log.e(BluetoothIAP2Server.TAG, "disconnected", e);
                    BluetoothIAP2Server.this.connectionLost();
                    return;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                this.mmOutStream.write(buffer);
            } catch (IOException e) {
                Log.e(BluetoothIAP2Server.TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                this.mmSocket.close();
            } catch (IOException e) {
                Log.e(BluetoothIAP2Server.TAG, "close() of connect socket failed", e);
            }
        }
    }

    public BluetoothIAP2Server(Handler handler) {
        this.mHandler = handler;
    }

    public String getName() {
        return this.mAdapter.getName();
    }

    public byte[] getLocalAddress() {
        return Utils.parseMACAddress(this.mAdapter.getAddress());
    }

    public void connect(LinkLayer linkLayer) {
        this.linkLayer = linkLayer;
    }

    public synchronized void start() {
        Log.d(TAG, "start");
        setState(1);
        if (this.mAcceptThread == null) {
            this.mAcceptThread = new AcceptThread();
            this.mAcceptThread.start();
        }
    }

    public synchronized void stop() {
        Log.d(TAG, "stop");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }
        if (this.mAcceptThread != null) {
            this.mAcceptThread.cancel();
            this.mAcceptThread = null;
        }
        setState(0);
    }

    public synchronized void connect(BluetoothDevice device) {
        Log.d(TAG, "connect to: " + device);
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }
        this.mConnectThread = new ConnectThread(device);
        this.mConnectThread.start();
        setState(2);
    }

    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        Log.d(TAG, "connected");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mConnectedThread != null) {
            this.mConnectedThread.cancel();
            this.mConnectedThread = null;
        }
        if (this.mAcceptThread != null) {
            this.mAcceptThread.cancel();
            this.mAcceptThread = null;
        }
        this.mConnectedThread = new ConnectedThread(socket);
        this.mConnectedThread.start();
        Message msg = this.mHandler.obtainMessage(4);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        this.mHandler.sendMessage(msg);
        setState(3);
    }

    private synchronized void setState(int state) {
        Log.d(TAG, "setState() " + this.mState + " -> " + state);
        this.mState = state;
        this.mHandler.obtainMessage(1, state, -1).sendToTarget();
    }

    public void write(byte[] out) {
        synchronized (this) {
            if (this.mState != 3) {
                return;
            }
            ConnectedThread r = this.mConnectedThread;
            r.write(out);
        }
    }

    private void connectionFailed() {
        Message msg = this.mHandler.obtainMessage(5);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Unable to connect device");
        msg.setData(bundle);
        this.mHandler.sendMessage(msg);
        start();
    }

    private void connectionLost() {
        this.linkLayer.connectionEnded();
        Message msg = this.mHandler.obtainMessage(5);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.TOAST, "Device connection was lost");
        msg.setData(bundle);
        this.mHandler.sendMessage(msg);
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                BluetoothIAP2Server.this.start();
            }
        }, 500);
    }

    public synchronized void reconfirmDiscoverableState(Activity intentContext) {
        if (intentContext != null) {
            try {
                Intent discoverableIntent = new Intent("android.bluetooth.adapter.action.REQUEST_DISCOVERABLE");
                discoverableIntent.putExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", 300);
                intentContext.startActivity(discoverableIntent);
                Log.d(TAG, "Now Discoverable");
            } catch (Exception e) {
                Log.e(TAG, "Failed to make discoverable", e);
            }
        }
        return;
    }

    public void queue(LinkPacket pkt) {
        write(pkt.data);
    }
}
