package com.navdy.hud.mfi;

public interface IAPFileTransferListener {
    void onFileReceived(int i, byte[] bArr);

    void onFileTransferCancel(int i);

    void onFileTransferSetup(int i, long j);
}
