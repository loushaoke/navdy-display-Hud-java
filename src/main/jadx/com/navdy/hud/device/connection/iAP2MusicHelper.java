package com.navdy.hud.device.connection;

import android.graphics.Bitmap;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.LruCache;
import com.amazonaws.services.s3.internal.Constants;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.mfi.IAPFileTransferListener;
import com.navdy.hud.mfi.IAPFileTransferManager;
import com.navdy.hud.mfi.IAPMusicManager;
import com.navdy.hud.mfi.IAPNowPlayingUpdateListener;
import com.navdy.hud.mfi.NowPlayingUpdate;
import com.navdy.hud.mfi.iAPProcessor;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEvent.MessageType;
import com.navdy.service.library.events.audio.MusicArtworkRequest;
import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.service.library.events.audio.MusicArtworkResponse.Builder;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.input.MediaRemoteKeyEvent;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.photo.PhotoUpdatesRequest;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities.ScalingLogic;
import okio.ByteString;

class iAP2MusicHelper implements IAPNowPlayingUpdateListener, IAPFileTransferListener {
    private static final int LIMITED_BANDWIDTH_FILE_TRANSFER_LIMIT = 10000;
    private static final Logger sLogger = new Logger(iAP2MusicHelper.class);
    private boolean fileTransferCanceled = false;
    private final Object helperStateLock = new Object();
    private IAPMusicManager iAPMusicManager;
    private IAPFileTransferManager iapFileTransferManager;
    private long keyDownTime = 0;
    private MusicArtworkResponse lastMusicArtworkResponse;
    private iAP2Link link;
    private int nowPlayingFileTransferIdentifier = -1;
    private LruCache<Integer, MusicTrackInfo> transferIdTrackInfoMap = new LruCache(5);
    private MusicArtworkRequest waitingArtworkRequest = null;
    private FileHolder waitingFile;
    private int waitingFileTransferIdentifier = -1;

    private class FileHolder {
        byte[] data = null;

        int size() {
            return this.data != null ? this.data.length : 0;
        }

        FileHolder(byte[] data) {
            this.data = data;
        }
    }

    iAP2MusicHelper(iAP2Link link, iAPProcessor iAPProcessor) {
        this.link = link;
        this.iapFileTransferManager = new IAPFileTransferManager(iAPProcessor);
        iAPProcessor.connect(this.iapFileTransferManager);
        this.iAPMusicManager = new IAPMusicManager(iAPProcessor);
        this.iAPMusicManager.setNowPlayingUpdateListener(this);
        addEventProcessors();
    }

    private MusicTrackInfo getLastMusicTrackInfo() {
        MusicTrackInfo musicTrackInfo;
        synchronized (this.helperStateLock) {
            musicTrackInfo = (MusicTrackInfo) this.transferIdTrackInfoMap.get(Integer.valueOf(this.nowPlayingFileTransferIdentifier));
        }
        return musicTrackInfo;
    }

    public synchronized void onNowPlayingUpdate(NowPlayingUpdate nowPlayingUpdate) {
        MusicTrackInfo musicTrackInfo = IAPMessageUtility.getMusicTrackInfoForNowPlayingUpdate(nowPlayingUpdate);
        synchronized (this.helperStateLock) {
            this.nowPlayingFileTransferIdentifier = nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier;
            sLogger.d("onNowPlayingUpdate nowPlayingFileTransferIdentifier: " + this.nowPlayingFileTransferIdentifier + ", musicTrackInfo: " + musicTrackInfo);
            if (!(musicTrackInfo == null || (TextUtils.isEmpty(musicTrackInfo.name) && TextUtils.isEmpty(musicTrackInfo.album) && TextUtils.isEmpty(musicTrackInfo.author)))) {
                this.transferIdTrackInfoMap.put(Integer.valueOf(this.nowPlayingFileTransferIdentifier), musicTrackInfo);
            }
        }
        this.link.sendMessageAsNavdyEvent(musicTrackInfo);
    }

    public void onFileTransferSetup(int fileTransferIdentifier, long size) {
        synchronized (this.helperStateLock) {
            sLogger.d("onFileTransferSetup, fileTransferIdentifier: " + fileTransferIdentifier + ", size: " + size + ", waitingArtworkRequest: " + this.waitingArtworkRequest + ", nowPlayingFileTransferIdentifier: " + this.nowPlayingFileTransferIdentifier);
            MusicTrackInfo musicTrackInfo = (MusicTrackInfo) this.transferIdTrackInfoMap.get(Integer.valueOf(fileTransferIdentifier));
            this.fileTransferCanceled = false;
            if (this.waitingArtworkRequest != null && sameTrack(this.waitingArtworkRequest, musicTrackInfo) && this.nowPlayingFileTransferIdentifier == fileTransferIdentifier) {
                sLogger.d("onFileTransferSetup continuing file transfer");
                this.iapFileTransferManager.onFileTransferSetupResponse(this.nowPlayingFileTransferIdentifier, true);
            } else {
                sLogger.d("onFileTransferSetup waiting for artwork request, waitingFileTransferIdentifier: " + fileTransferIdentifier);
                this.waitingFileTransferIdentifier = fileTransferIdentifier;
                this.waitingFile = null;
            }
        }
    }

    public synchronized void onFileReceived(int fileTransferIdentifier, byte[] data) {
        synchronized (this.helperStateLock) {
            sLogger.d("onFileReceived: " + fileTransferIdentifier + " (" + (data != null ? data.length : 0) + HereManeuverDisplayBuilder.CLOSE_BRACKET);
            if (this.transferIdTrackInfoMap.get(Integer.valueOf(fileTransferIdentifier)) == null) {
                sLogger.e("onFileReceived: no music info for this file (yet?)");
                this.waitingFile = new FileHolder(data);
            } else {
                MusicTrackInfo musicTrackInfo = (MusicTrackInfo) this.transferIdTrackInfoMap.remove(Integer.valueOf(fileTransferIdentifier));
                sendArtworkResponse(fileTransferIdentifier, data, musicTrackInfo);
            }
        }
    }

    private void sendArtworkResponse(final int fileTransferIdentifier, final byte[] data, final MusicTrackInfo musicTrackInfo) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                iAP2MusicHelper.sLogger.d("onFileReceived task " + fileTransferIdentifier);
                ByteString photo = null;
                if (data != null && data.length > 0) {
                    try {
                        Bitmap bitmap = ScalingUtilities.decodeByteArray(data, 200, 200, ScalingLogic.FIT);
                        if (bitmap != null) {
                            photo = ByteString.of(ScalingUtilities.encodeByteArray(ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(bitmap, 200, 200, ScalingLogic.FIT)));
                        } else {
                            iAP2MusicHelper.sLogger.e("Couldn't decode byte array to bitmap");
                        }
                    } catch (Exception e) {
                        iAP2MusicHelper.sLogger.e("Error updating the art work received ", e);
                    }
                }
                iAP2MusicHelper.this.lastMusicArtworkResponse = new Builder().name(musicTrackInfo.name).album(musicTrackInfo.album).author(musicTrackInfo.author).photo(photo).build();
                iAP2MusicHelper.sLogger.d("sending artworkResponse " + iAP2MusicHelper.this.lastMusicArtworkResponse);
                iAP2MusicHelper.this.link.sendMessageAsNavdyEvent(iAP2MusicHelper.this.lastMusicArtworkResponse);
            }
        }, 1);
    }

    public synchronized void onFileTransferCancel(int fileTransferIdentifier) {
        synchronized (this.helperStateLock) {
            sLogger.d("onFileTransferCancel: " + fileTransferIdentifier);
            MusicTrackInfo musicTrackInfo = (MusicTrackInfo) this.transferIdTrackInfoMap.remove(Integer.valueOf(fileTransferIdentifier));
            if (this.waitingArtworkRequest != null && sameTrack(this.waitingArtworkRequest, musicTrackInfo) && this.nowPlayingFileTransferIdentifier == fileTransferIdentifier) {
                sLogger.d("Sending response with null data");
                sendArtworkResponse(fileTransferIdentifier, null, musicTrackInfo);
            } else {
                sLogger.d("File transferred canceled, waiting for the meta data to send empty response");
                this.fileTransferCanceled = true;
                this.waitingFileTransferIdentifier = fileTransferIdentifier;
                this.waitingFile = null;
            }
        }
    }

    private void addEventProcessors() {
        this.link.addEventProcessor(MessageType.MediaRemoteKeyEvent, new NavdyEventProcessor() {
            public boolean processNavdyEvent(NavdyEvent event) {
                iAP2MusicHelper.this.onMediaRemoteKeyEvent((MediaRemoteKeyEvent) event.getExtension(Ext_NavdyEvent.mediaRemoteKeyEvent));
                return true;
            }
        });
        this.link.addEventProcessor(MessageType.NowPlayingUpdateRequest, new NavdyEventProcessor() {
            public boolean processNavdyEvent(NavdyEvent event) {
                return true;
            }
        });
        this.link.addEventProcessor(MessageType.PhotoUpdatesRequest, new NavdyEventProcessor() {
            public boolean processNavdyEvent(NavdyEvent event) {
                iAP2MusicHelper.sLogger.d("got PhotoUpdatesRequest");
                iAP2MusicHelper.this.onPhotoUpdatesRequest((PhotoUpdatesRequest) event.getExtension(Ext_NavdyEvent.photoUpdateRequest));
                return true;
            }
        });
        this.link.addEventProcessor(MessageType.MusicArtworkRequest, new NavdyEventProcessor() {
            public boolean processNavdyEvent(NavdyEvent event) {
                return iAP2MusicHelper.this.onMusicArtworkRequest((MusicArtworkRequest) event.getExtension(Ext_NavdyEvent.musicArtworkRequest));
            }
        });
    }

    private synchronized boolean onMusicArtworkRequest(MusicArtworkRequest musicArtworkRequest) {
        boolean z = true;
        synchronized (this) {
            MusicTrackInfo musicTrackInfo = getLastMusicTrackInfo();
            sLogger.d("onMusicArtworkRequest: " + musicArtworkRequest + ", musicTrackInfo: " + musicTrackInfo);
            if (musicArtworkRequest == null || (TextUtils.isEmpty(musicArtworkRequest.name) && TextUtils.isEmpty(musicArtworkRequest.album) && TextUtils.isEmpty(musicArtworkRequest.author))) {
                z = false;
            } else {
                synchronized (this.helperStateLock) {
                    if (musicTrackInfo != null) {
                        if (sameTrack(musicArtworkRequest, musicTrackInfo) && this.nowPlayingFileTransferIdentifier == this.waitingFileTransferIdentifier) {
                            sLogger.d("onMusicArtworkRequest, waiting file: " + (this.waitingFile != null ? Integer.valueOf(this.waitingFile.size()) : Constants.NULL_VERSION_ID));
                            if (this.waitingFile != null) {
                                sLogger.d("onMusicArtworkRequest using waiting file " + this.waitingFileTransferIdentifier);
                                sendArtworkResponse(this.waitingFileTransferIdentifier, this.waitingFile.data, musicTrackInfo);
                            } else if (this.fileTransferCanceled) {
                                sLogger.d("onMusicArtworkRequest, The file transfer has been canceled, send an empty artwork response");
                                sendArtworkResponse(this.waitingFileTransferIdentifier, null, musicTrackInfo);
                            } else {
                                sLogger.d("onMusicArtworkRequest continuing file transfer");
                                this.iapFileTransferManager.onFileTransferSetupResponse(this.nowPlayingFileTransferIdentifier, true);
                            }
                        }
                    }
                    this.waitingArtworkRequest = musicArtworkRequest;
                    sLogger.d("onMusicArtworkRequest waiting for file transfer, waitingArtworkRequest: " + this.waitingArtworkRequest);
                }
            }
        }
        return z;
    }

    private void onPhotoUpdatesRequest(PhotoUpdatesRequest request) {
        boolean postPhotoUpdates = (request.photoType != PhotoType.PHOTO_ALBUM_ART || request.start == null) ? false : request.start.booleanValue();
        sLogger.d("onPhotoUpdatesRequest: " + postPhotoUpdates + ", " + request);
        if (postPhotoUpdates) {
            this.link.sendMessageAsNavdyEvent(this.lastMusicArtworkResponse);
        }
    }

    private void onMediaRemoteKeyEvent(MediaRemoteKeyEvent request) {
        sLogger.d("(MFi) Media Key : " + request.key + " , " + request.action);
        switch (request.action) {
            case KEY_DOWN:
                this.keyDownTime = SystemClock.elapsedRealtime();
                this.iAPMusicManager.onKeyDown(request.key.ordinal());
                return;
            case KEY_UP:
                if (SystemClock.elapsedRealtime() - this.keyDownTime <= 500) {
                    this.iapFileTransferManager.cancel();
                }
                this.keyDownTime = 0;
                this.iAPMusicManager.onKeyUp(request.key.ordinal());
                return;
            default:
                return;
        }
    }

    private boolean sameTrack(MusicArtworkRequest artworkRequest, MusicTrackInfo musicTrackInfo) {
        return artworkRequest != null && musicTrackInfo != null && TextUtils.equals(artworkRequest.name, musicTrackInfo.name) && TextUtils.equals(artworkRequest.album, musicTrackInfo.album) && TextUtils.equals(artworkRequest.author, musicTrackInfo.author);
    }

    void setBandwidthLevel(int level) {
        sLogger.d("Bandwidth level changing : " + (level <= 0 ? "LOW" : "NORMAL"));
        if (level <= 0) {
            this.iapFileTransferManager.setFileTransferLimit(10000);
        } else {
            this.iapFileTransferManager.setFileTransferLimit(1048576);
        }
    }

    void close() {
        this.keyDownTime = 0;
    }

    void onReady() {
        this.iapFileTransferManager.setFileTransferListener(this);
        this.iAPMusicManager.startNowPlayingUpdates();
    }
}
