package com.navdy.hud.device.connection;

import android.text.TextUtils;
import com.navdy.hud.mfi.CallStateUpdate;
import com.navdy.hud.mfi.IAPCommunicationsManager;
import com.navdy.hud.mfi.NowPlayingUpdate;
import com.navdy.hud.mfi.NowPlayingUpdate.PlaybackRepeat;
import com.navdy.hud.mfi.NowPlayingUpdate.PlaybackShuffle;
import com.navdy.hud.mfi.NowPlayingUpdate.PlaybackStatus;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicRepeatMode;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.callcontrol.CallAction;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneEvent.Builder;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.callcontrol.TelephonyRequest;
import java.util.HashMap;
import java.util.Map;

public class IAPMessageUtility {
    private static final Map<PlaybackStatus, MusicPlaybackState> PLAYBACK_STATES = new HashMap<PlaybackStatus, MusicPlaybackState>() {
        {
            put(PlaybackStatus.Stopped, MusicPlaybackState.PLAYBACK_STOPPED);
            put(PlaybackStatus.Playing, MusicPlaybackState.PLAYBACK_PLAYING);
            put(PlaybackStatus.Paused, MusicPlaybackState.PLAYBACK_PAUSED);
            put(PlaybackStatus.SeekForward, MusicPlaybackState.PLAYBACK_FAST_FORWARDING);
            put(PlaybackStatus.SeekBackward, MusicPlaybackState.PLAYBACK_REWINDING);
        }
    };
    private static final Map<PlaybackRepeat, MusicRepeatMode> REPEAT_MODES = new HashMap<PlaybackRepeat, MusicRepeatMode>() {
        {
            put(PlaybackRepeat.Off, MusicRepeatMode.MUSIC_REPEAT_MODE_OFF);
            put(PlaybackRepeat.One, MusicRepeatMode.MUSIC_REPEAT_MODE_ONE);
            put(PlaybackRepeat.All, MusicRepeatMode.MUSIC_REPEAT_MODE_ALL);
        }
    };
    private static final Map<PlaybackShuffle, MusicShuffleMode> SHUFFLE_MODES = new HashMap<PlaybackShuffle, MusicShuffleMode>() {
        {
            put(PlaybackShuffle.Off, MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF);
            put(PlaybackShuffle.Songs, MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS);
            put(PlaybackShuffle.Albums, MusicShuffleMode.MUSIC_SHUFFLE_MODE_ALBUMS);
        }
    };

    public static PhoneEvent getPhoneEventForCallStateUpdate(CallStateUpdate callstateUpdate, CallAction lastCallActionRequested) {
        if (callstateUpdate == null) {
            return null;
        }
        Builder builder = new Builder();
        switch (callstateUpdate.status) {
            case Disconnected:
                if (lastCallActionRequested != null && lastCallActionRequested == CallAction.CALL_DIAL) {
                    builder.status(PhoneStatus.PHONE_DISCONNECTING);
                    break;
                }
                builder.status(PhoneStatus.PHONE_IDLE);
                break;
            case Sending:
                builder.status(PhoneStatus.PHONE_DIALING);
                break;
            case Ringing:
                builder.status(PhoneStatus.PHONE_RINGING);
                break;
            case Connecting:
                builder.status(PhoneStatus.PHONE_OFFHOOK);
                break;
            case Active:
                builder.status(PhoneStatus.PHONE_OFFHOOK);
                break;
            case Held:
                builder.status(PhoneStatus.PHONE_HELD);
                break;
            case Disconnecting:
                return null;
        }
        if (!TextUtils.isEmpty(callstateUpdate.displayName)) {
            builder.contact_name(callstateUpdate.displayName);
        }
        if (!TextUtils.isEmpty(callstateUpdate.remoteID)) {
            builder.number(callstateUpdate.remoteID);
        }
        if (!TextUtils.isEmpty(callstateUpdate.label)) {
            builder.label(callstateUpdate.label);
        }
        if (!TextUtils.isEmpty(callstateUpdate.callUUID)) {
            builder.callUUID(callstateUpdate.callUUID);
        }
        return builder.build();
    }

    public static boolean performActionForTelephonyRequest(TelephonyRequest request, IAPCommunicationsManager manager) {
        if (request == null) {
            return false;
        }
        switch (request.action) {
            case CALL_ACCEPT:
                if (!TextUtils.isEmpty(request.callUUID)) {
                    manager.acceptCall(request.callUUID);
                    break;
                }
                manager.acceptCall();
                break;
            case CALL_END:
            case CALL_REJECT:
                if (!TextUtils.isEmpty(request.callUUID)) {
                    manager.endCall(request.callUUID);
                    break;
                }
                manager.endCall();
                break;
            case CALL_DIAL:
                if (!TextUtils.isEmpty(request.number)) {
                    manager.initiateDestinationCall(request.number);
                    break;
                }
                break;
            case CALL_MUTE:
                manager.mute();
                break;
            case CALL_UNMUTE:
                manager.unMute();
                break;
            default:
                return false;
        }
        return true;
    }

    public static MusicTrackInfo getMusicTrackInfoForNowPlayingUpdate(NowPlayingUpdate nowPlayingUpdate) {
        if (nowPlayingUpdate == null) {
            return null;
        }
        MusicTrackInfo.Builder resultBuilder = new MusicTrackInfo.Builder().playbackState((MusicPlaybackState) PLAYBACK_STATES.get(nowPlayingUpdate.mPlaybackStatus)).name(nowPlayingUpdate.mediaItemTitle).album(nowPlayingUpdate.mediaItemAlbumTitle).author(nowPlayingUpdate.mediaItemArtist).duration(Integer.valueOf((int) nowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds)).currentPosition(Integer.valueOf((int) nowPlayingUpdate.mPlaybackElapsedTimeMilliseconds)).shuffleMode((MusicShuffleMode) SHUFFLE_MODES.get(nowPlayingUpdate.playbackShuffle)).repeatMode((MusicRepeatMode) REPEAT_MODES.get(nowPlayingUpdate.playbackRepeat));
        if (nowPlayingUpdate.mediaItemPersistentIdentifier != null) {
            resultBuilder.trackId(nowPlayingUpdate.mediaItemPersistentIdentifier.toString());
        }
        resultBuilder.isPreviousAllowed(Boolean.valueOf(true)).isNextAllowed(Boolean.valueOf(true));
        return resultBuilder.build();
    }
}
