package com.navdy.service.library.util;

import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.service.library.events.audio.MusicTrackInfo;

public final class MusicDataUtils {
    public static final String ALTERNATE_SEPARATOR = "_";
    private static final String SEPARATOR = " // ";

    public static String photoIdentifierFromTrackInfo(MusicTrackInfo trackInfo) {
        if (trackInfo == null) {
            return null;
        }
        return trackInfo.index == null ? songIdentifierFromTrackInfo(trackInfo) : trackInfo.index.toString();
    }

    public static String songIdentifierFromTrackInfo(MusicTrackInfo trackInfo) {
        return songIdentifierFromTrackInfo(trackInfo, SEPARATOR);
    }

    public static String songIdentifierFromTrackInfo(MusicTrackInfo trackInfo, String separator) {
        if (trackInfo == null) {
            return null;
        }
        return (String.valueOf(trackInfo.name) + separator + String.valueOf(trackInfo.album) + separator + String.valueOf(trackInfo.author)).replaceAll("[^A-Za-z0-9_]", ALTERNATE_SEPARATOR);
    }

    public static String songIdentifierFromArtworkResponse(MusicArtworkResponse artworkResponse) {
        if (artworkResponse == null) {
            return null;
        }
        return (String.valueOf(artworkResponse.name) + SEPARATOR + String.valueOf(artworkResponse.album) + SEPARATOR + String.valueOf(artworkResponse.author)).replaceAll("[^A-Za-z0-9_]", ALTERNATE_SEPARATOR);
    }
}
