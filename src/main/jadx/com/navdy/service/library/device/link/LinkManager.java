package com.navdy.service.library.device.link;

import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import java.util.HashMap;

public class LinkManager {
    private static LinkFactory sDefaultFactory = new LinkFactory() {
        public Link build(ConnectionInfo connectionInfo) {
            switch (AnonymousClass2.$SwitchMap$com$navdy$service$library$device$connection$ConnectionType[connectionInfo.getType().ordinal()]) {
                case 1:
                case 2:
                    return new ProtobufLink(connectionInfo);
                default:
                    return null;
            }
        }
    };
    private static HashMap<ConnectionType, LinkFactory> sFactoryMap = new HashMap();

    public interface LinkFactory {
        Link build(ConnectionInfo connectionInfo);
    }

    /* renamed from: com.navdy.service.library.device.link.LinkManager$2 */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$device$connection$ConnectionType = new int[ConnectionType.values().length];

        static {
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[ConnectionType.BT_PROTOBUF.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[ConnectionType.TCP_PROTOBUF.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    static {
        registerFactory(ConnectionType.BT_PROTOBUF, sDefaultFactory);
        registerFactory(ConnectionType.TCP_PROTOBUF, sDefaultFactory);
    }

    public static void registerFactory(ConnectionType type, LinkFactory factory) {
        if (factory != null) {
            sFactoryMap.put(type, factory);
        }
    }

    public static Link build(ConnectionInfo connectionInfo) {
        LinkFactory factory = (LinkFactory) sFactoryMap.get(connectionInfo.getType());
        if (factory != null) {
            return factory.build(connectionInfo);
        }
        return null;
    }
}
