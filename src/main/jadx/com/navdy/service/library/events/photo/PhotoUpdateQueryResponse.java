package com.navdy.service.library.events.photo;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class PhotoUpdateQueryResponse extends Message {
    public static final String DEFAULT_IDENTIFIER = "";
    public static final Boolean DEFAULT_UPDATEREQUIRED = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean updateRequired;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhotoUpdateQueryResponse> {
        public String identifier;
        public Boolean updateRequired;

        public Builder(PhotoUpdateQueryResponse message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
                this.updateRequired = message.updateRequired;
            }
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder updateRequired(Boolean updateRequired) {
            this.updateRequired = updateRequired;
            return this;
        }

        public PhotoUpdateQueryResponse build() {
            checkRequiredFields();
            return new PhotoUpdateQueryResponse();
        }
    }

    public PhotoUpdateQueryResponse(String identifier, Boolean updateRequired) {
        this.identifier = identifier;
        this.updateRequired = updateRequired;
    }

    private PhotoUpdateQueryResponse(Builder builder) {
        this(builder.identifier, builder.updateRequired);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhotoUpdateQueryResponse)) {
            return false;
        }
        PhotoUpdateQueryResponse o = (PhotoUpdateQueryResponse) other;
        if (equals( this.identifier,  o.identifier) && equals( this.updateRequired,  o.updateRequired)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.identifier != null) {
            result = this.identifier.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.updateRequired != null) {
            i = this.updateRequired.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
