package com.navdy.service.library.events.photo;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class PhotoRequest extends Message {
    public static final String DEFAULT_IDENTIFIER = "";
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_PHOTOCHECKSUM = "";
    public static final PhotoType DEFAULT_PHOTOTYPE = PhotoType.PHOTO_CONTACT;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String photoChecksum;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final PhotoType photoType;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhotoRequest> {
        public String identifier;
        public String name;
        public String photoChecksum;
        public PhotoType photoType;

        public Builder(PhotoRequest message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
                this.photoChecksum = message.photoChecksum;
                this.photoType = message.photoType;
                this.name = message.name;
            }
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder photoChecksum(String photoChecksum) {
            this.photoChecksum = photoChecksum;
            return this;
        }

        public Builder photoType(PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public PhotoRequest build() {
            checkRequiredFields();
            return new PhotoRequest();
        }
    }

    public PhotoRequest(String identifier, String photoChecksum, PhotoType photoType, String name) {
        this.identifier = identifier;
        this.photoChecksum = photoChecksum;
        this.photoType = photoType;
        this.name = name;
    }

    private PhotoRequest(Builder builder) {
        this(builder.identifier, builder.photoChecksum, builder.photoType, builder.name);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhotoRequest)) {
            return false;
        }
        PhotoRequest o = (PhotoRequest) other;
        if (equals( this.identifier,  o.identifier) && equals( this.photoChecksum,  o.photoChecksum) && equals( this.photoType,  o.photoType) && equals( this.name,  o.name)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.identifier != null ? this.identifier.hashCode() : 0) * 37;
        if (this.photoChecksum != null) {
            hashCode = this.photoChecksum.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.photoType != null) {
            hashCode = this.photoType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.name != null) {
            i = this.name.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
