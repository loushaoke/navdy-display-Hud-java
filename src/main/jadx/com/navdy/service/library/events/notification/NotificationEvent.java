package com.navdy.service.library.events.notification;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;
import okio.ByteString;

public final class NotificationEvent extends Message {
    public static final List<NotificationAction> DEFAULT_ACTIONS = Collections.emptyList();
    public static final String DEFAULT_APPID = "";
    public static final String DEFAULT_APPNAME = "";
    public static final Boolean DEFAULT_CANNOTREPLYBACK = Boolean.valueOf(false);
    public static final NotificationCategory DEFAULT_CATEGORY = NotificationCategory.CATEGORY_OTHER;
    public static final String DEFAULT_ICONRESOURCENAME = "";
    public static final Integer DEFAULT_ID = Integer.valueOf(0);
    public static final String DEFAULT_IMAGERESOURCENAME = "";
    public static final String DEFAULT_MESSAGE = "";
    public static final ByteString DEFAULT_PHOTO = ByteString.EMPTY;
    public static final String DEFAULT_SOURCEIDENTIFIER = "";
    public static final String DEFAULT_SUBTITLE = "";
    public static final String DEFAULT_TITLE = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = NotificationAction.class, tag = 7)
    public final List<NotificationAction> actions;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String appId;
    @ProtoField(tag = 13, type = Datatype.STRING)
    public final String appName;
    @ProtoField(tag = 12, type = Datatype.BOOL)
    public final Boolean cannotReplyBack;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final NotificationCategory category;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String iconResourceName;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT32)
    public final Integer id;
    @ProtoField(tag = 10, type = Datatype.STRING)
    public final String imageResourceName;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String message;
    @ProtoField(tag = 8, type = Datatype.BYTES)
    public final ByteString photo;
    @ProtoField(tag = 11, type = Datatype.STRING)
    public final String sourceIdentifier;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String subtitle;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String title;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationEvent> {
        public List<NotificationAction> actions;
        public String appId;
        public String appName;
        public Boolean cannotReplyBack;
        public NotificationCategory category;
        public String iconResourceName;
        public Integer id;
        public String imageResourceName;
        public String message;
        public ByteString photo;
        public String sourceIdentifier;
        public String subtitle;
        public String title;

        public Builder(NotificationEvent message) {
            super(message);
            if (message != null) {
                this.id = message.id;
                this.category = message.category;
                this.title = message.title;
                this.subtitle = message.subtitle;
                this.message = message.message;
                this.appId = message.appId;
                this.actions = Message.copyOf(message.actions);
                this.photo = message.photo;
                this.iconResourceName = message.iconResourceName;
                this.imageResourceName = message.imageResourceName;
                this.sourceIdentifier = message.sourceIdentifier;
                this.cannotReplyBack = message.cannotReplyBack;
                this.appName = message.appName;
            }
        }

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder category(NotificationCategory category) {
            this.category = category;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder subtitle(String subtitle) {
            this.subtitle = subtitle;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder appId(String appId) {
            this.appId = appId;
            return this;
        }

        public Builder actions(List<NotificationAction> actions) {
            this.actions = com.squareup.wire.Message.Builder.checkForNulls(actions);
            return this;
        }

        public Builder photo(ByteString photo) {
            this.photo = photo;
            return this;
        }

        public Builder iconResourceName(String iconResourceName) {
            this.iconResourceName = iconResourceName;
            return this;
        }

        public Builder imageResourceName(String imageResourceName) {
            this.imageResourceName = imageResourceName;
            return this;
        }

        public Builder sourceIdentifier(String sourceIdentifier) {
            this.sourceIdentifier = sourceIdentifier;
            return this;
        }

        public Builder cannotReplyBack(Boolean cannotReplyBack) {
            this.cannotReplyBack = cannotReplyBack;
            return this;
        }

        public Builder appName(String appName) {
            this.appName = appName;
            return this;
        }

        public NotificationEvent build() {
            checkRequiredFields();
            return new NotificationEvent();
        }
    }

    public NotificationEvent(Integer id, NotificationCategory category, String title, String subtitle, String message, String appId, List<NotificationAction> actions, ByteString photo, String iconResourceName, String imageResourceName, String sourceIdentifier, Boolean cannotReplyBack, String appName) {
        this.id = id;
        this.category = category;
        this.title = title;
        this.subtitle = subtitle;
        this.message = message;
        this.appId = appId;
        this.actions = Message.immutableCopyOf(actions);
        this.photo = photo;
        this.iconResourceName = iconResourceName;
        this.imageResourceName = imageResourceName;
        this.sourceIdentifier = sourceIdentifier;
        this.cannotReplyBack = cannotReplyBack;
        this.appName = appName;
    }

    private NotificationEvent(Builder builder) {
        this(builder.id, builder.category, builder.title, builder.subtitle, builder.message, builder.appId, builder.actions, builder.photo, builder.iconResourceName, builder.imageResourceName, builder.sourceIdentifier, builder.cannotReplyBack, builder.appName);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NotificationEvent)) {
            return false;
        }
        NotificationEvent o = (NotificationEvent) other;
        if (equals( this.id,  o.id) && equals( this.category,  o.category) && equals( this.title,  o.title) && equals( this.subtitle,  o.subtitle) && equals( this.message,  o.message) && equals( this.appId,  o.appId) && equals(this.actions, o.actions) && equals( this.photo,  o.photo) && equals( this.iconResourceName,  o.iconResourceName) && equals( this.imageResourceName,  o.imageResourceName) && equals( this.sourceIdentifier,  o.sourceIdentifier) && equals( this.cannotReplyBack,  o.cannotReplyBack) && equals( this.appName,  o.appName)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.id != null ? this.id.hashCode() : 0) * 37;
        if (this.category != null) {
            hashCode = this.category.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.title != null) {
            hashCode = this.title.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.subtitle != null) {
            hashCode = this.subtitle.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.message != null) {
            hashCode = this.message.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.appId != null) {
            hashCode = this.appId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (((hashCode2 + hashCode) * 37) + (this.actions != null ? this.actions.hashCode() : 1)) * 37;
        if (this.photo != null) {
            hashCode = this.photo.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.iconResourceName != null) {
            hashCode = this.iconResourceName.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.imageResourceName != null) {
            hashCode = this.imageResourceName.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.sourceIdentifier != null) {
            hashCode = this.sourceIdentifier.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.cannotReplyBack != null) {
            hashCode = this.cannotReplyBack.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.appName != null) {
            i = this.appName.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
