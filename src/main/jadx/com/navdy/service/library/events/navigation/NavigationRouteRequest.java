package com.navdy.service.library.events.navigation;

import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.location.Coordinate;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class NavigationRouteRequest extends Message {
    public static final Boolean DEFAULT_AUTONAVIGATE = Boolean.valueOf(false);
    public static final Boolean DEFAULT_CANCELCURRENT = Boolean.valueOf(false);
    public static final FavoriteType DEFAULT_DESTINATIONTYPE = FavoriteType.FAVORITE_NONE;
    public static final String DEFAULT_DESTINATION_IDENTIFIER = "";
    public static final Boolean DEFAULT_GEOCODESTREETADDRESS = Boolean.valueOf(false);
    public static final String DEFAULT_LABEL = "";
    public static final Boolean DEFAULT_ORIGINDISPLAY = Boolean.valueOf(false);
    public static final String DEFAULT_REQUESTID = "";
    public static final List<RouteAttribute> DEFAULT_ROUTEATTRIBUTES = Collections.emptyList();
    public static final String DEFAULT_STREETADDRESS = "";
    public static final List<Coordinate> DEFAULT_WAYPOINTS = Collections.emptyList();
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 6, type = Datatype.BOOL)
    public final Boolean autoNavigate;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean cancelCurrent;
    @ProtoField(label = Label.REQUIRED, tag = 1)
    public final Coordinate destination;
    @ProtoField(tag = 12)
    public final Coordinate destinationDisplay;
    @ProtoField(tag = 8, type = Datatype.ENUM)
    public final FavoriteType destinationType;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String destination_identifier;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean geoCodeStreetAddress;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean originDisplay;
    @ProtoField(tag = 14)
    public final Destination requestDestination;
    @ProtoField(tag = 11, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(enumType = RouteAttribute.class, label = Label.REPEATED, tag = 13, type = Datatype.ENUM)
    public final List<RouteAttribute> routeAttributes;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String streetAddress;
    @ProtoField(label = Label.REPEATED, messageType = Coordinate.class, tag = 3)
    public final List<Coordinate> waypoints;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationRouteRequest> {
        public Boolean autoNavigate;
        public Boolean cancelCurrent;
        public Coordinate destination;
        public Coordinate destinationDisplay;
        public FavoriteType destinationType;
        public String destination_identifier;
        public Boolean geoCodeStreetAddress;
        public String label;
        public Boolean originDisplay;
        public Destination requestDestination;
        public String requestId;
        public List<RouteAttribute> routeAttributes;
        public String streetAddress;
        public List<Coordinate> waypoints;

        public Builder(NavigationRouteRequest message) {
            super(message);
            if (message != null) {
                this.destination = message.destination;
                this.label = message.label;
                this.waypoints = Message.copyOf(message.waypoints);
                this.streetAddress = message.streetAddress;
                this.geoCodeStreetAddress = message.geoCodeStreetAddress;
                this.autoNavigate = message.autoNavigate;
                this.destination_identifier = message.destination_identifier;
                this.destinationType = message.destinationType;
                this.originDisplay = message.originDisplay;
                this.cancelCurrent = message.cancelCurrent;
                this.requestId = message.requestId;
                this.destinationDisplay = message.destinationDisplay;
                this.routeAttributes = Message.copyOf(message.routeAttributes);
                this.requestDestination = message.requestDestination;
            }
        }

        public Builder destination(Coordinate destination) {
            this.destination = destination;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder waypoints(List<Coordinate> waypoints) {
            this.waypoints = com.squareup.wire.Message.Builder.checkForNulls(waypoints);
            return this;
        }

        public Builder streetAddress(String streetAddress) {
            this.streetAddress = streetAddress;
            return this;
        }

        public Builder geoCodeStreetAddress(Boolean geoCodeStreetAddress) {
            this.geoCodeStreetAddress = geoCodeStreetAddress;
            return this;
        }

        public Builder autoNavigate(Boolean autoNavigate) {
            this.autoNavigate = autoNavigate;
            return this;
        }

        public Builder destination_identifier(String destination_identifier) {
            this.destination_identifier = destination_identifier;
            return this;
        }

        public Builder destinationType(FavoriteType destinationType) {
            this.destinationType = destinationType;
            return this;
        }

        public Builder originDisplay(Boolean originDisplay) {
            this.originDisplay = originDisplay;
            return this;
        }

        public Builder cancelCurrent(Boolean cancelCurrent) {
            this.cancelCurrent = cancelCurrent;
            return this;
        }

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder destinationDisplay(Coordinate destinationDisplay) {
            this.destinationDisplay = destinationDisplay;
            return this;
        }

        public Builder routeAttributes(List<RouteAttribute> routeAttributes) {
            this.routeAttributes = com.squareup.wire.Message.Builder.checkForNulls(routeAttributes);
            return this;
        }

        public Builder requestDestination(Destination requestDestination) {
            this.requestDestination = requestDestination;
            return this;
        }

        public NavigationRouteRequest build() {
            checkRequiredFields();
            return new NavigationRouteRequest();
        }
    }

    public enum RouteAttribute implements ProtoEnum {
        ROUTE_ATTRIBUTE_GAS(0),
        ROUTE_ATTRIBUTE_SAVED_ROUTE(1);
        
        private final int value;

        private RouteAttribute(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public NavigationRouteRequest(Coordinate destination, String label, List<Coordinate> waypoints, String streetAddress, Boolean geoCodeStreetAddress, Boolean autoNavigate, String destination_identifier, FavoriteType destinationType, Boolean originDisplay, Boolean cancelCurrent, String requestId, Coordinate destinationDisplay, List<RouteAttribute> routeAttributes, Destination requestDestination) {
        this.destination = destination;
        this.label = label;
        this.waypoints = Message.immutableCopyOf(waypoints);
        this.streetAddress = streetAddress;
        this.geoCodeStreetAddress = geoCodeStreetAddress;
        this.autoNavigate = autoNavigate;
        this.destination_identifier = destination_identifier;
        this.destinationType = destinationType;
        this.originDisplay = originDisplay;
        this.cancelCurrent = cancelCurrent;
        this.requestId = requestId;
        this.destinationDisplay = destinationDisplay;
        this.routeAttributes = Message.immutableCopyOf(routeAttributes);
        this.requestDestination = requestDestination;
    }

    private NavigationRouteRequest(Builder builder) {
        this(builder.destination, builder.label, builder.waypoints, builder.streetAddress, builder.geoCodeStreetAddress, builder.autoNavigate, builder.destination_identifier, builder.destinationType, builder.originDisplay, builder.cancelCurrent, builder.requestId, builder.destinationDisplay, builder.routeAttributes, builder.requestDestination);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationRouteRequest)) {
            return false;
        }
        NavigationRouteRequest o = (NavigationRouteRequest) other;
        if (equals( this.destination,  o.destination) && equals( this.label,  o.label) && equals(this.waypoints, o.waypoints) && equals( this.streetAddress,  o.streetAddress) && equals( this.geoCodeStreetAddress,  o.geoCodeStreetAddress) && equals( this.autoNavigate,  o.autoNavigate) && equals( this.destination_identifier,  o.destination_identifier) && equals( this.destinationType,  o.destinationType) && equals( this.originDisplay,  o.originDisplay) && equals( this.cancelCurrent,  o.cancelCurrent) && equals( this.requestId,  o.requestId) && equals( this.destinationDisplay,  o.destinationDisplay) && equals(this.routeAttributes, o.routeAttributes) && equals( this.requestDestination,  o.requestDestination)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 1;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.destination != null ? this.destination.hashCode() : 0) * 37;
        if (this.label != null) {
            hashCode = this.label.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.waypoints != null) {
            hashCode = this.waypoints.hashCode();
        } else {
            hashCode = 1;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.streetAddress != null) {
            hashCode = this.streetAddress.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.geoCodeStreetAddress != null) {
            hashCode = this.geoCodeStreetAddress.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.autoNavigate != null) {
            hashCode = this.autoNavigate.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destination_identifier != null) {
            hashCode = this.destination_identifier.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destinationType != null) {
            hashCode = this.destinationType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.originDisplay != null) {
            hashCode = this.originDisplay.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.cancelCurrent != null) {
            hashCode = this.cancelCurrent.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.requestId != null) {
            hashCode = this.requestId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destinationDisplay != null) {
            hashCode = this.destinationDisplay.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.routeAttributes != null) {
            i = this.routeAttributes.hashCode();
        }
        hashCode = (hashCode + i) * 37;
        if (this.requestDestination != null) {
            i2 = this.requestDestination.hashCode();
        }
        result = hashCode + i2;
        this.hashCode = result;
        return result;
    }
}
