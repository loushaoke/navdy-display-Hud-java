package com.navdy.service.library.events.navigation;

import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.location.Coordinate;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class NavigationRouteResponse extends Message {
    public static final Boolean DEFAULT_CONSIDEREDTRAFFIC = Boolean.valueOf(false);
    public static final String DEFAULT_LABEL = "";
    public static final String DEFAULT_REQUESTID = "";
    public static final List<NavigationRouteResult> DEFAULT_RESULTS = Collections.emptyList();
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 6, type = Datatype.BOOL)
    public final Boolean consideredTraffic;
    @ProtoField(label = Label.REQUIRED, tag = 3)
    public final Coordinate destination;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(label = Label.REPEATED, messageType = NavigationRouteResult.class, tag = 5)
    public final List<NavigationRouteResult> results;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationRouteResponse> {
        public Boolean consideredTraffic;
        public Coordinate destination;
        public String label;
        public String requestId;
        public List<NavigationRouteResult> results;
        public RequestStatus status;
        public String statusDetail;

        public Builder(NavigationRouteResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.destination = message.destination;
                this.label = message.label;
                this.results = Message.copyOf(message.results);
                this.consideredTraffic = message.consideredTraffic;
                this.requestId = message.requestId;
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder destination(Coordinate destination) {
            this.destination = destination;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder results(List<NavigationRouteResult> results) {
            this.results = com.squareup.wire.Message.Builder.checkForNulls(results);
            return this;
        }

        public Builder consideredTraffic(Boolean consideredTraffic) {
            this.consideredTraffic = consideredTraffic;
            return this;
        }

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public NavigationRouteResponse build() {
            checkRequiredFields();
            return new NavigationRouteResponse();
        }
    }

    public NavigationRouteResponse(RequestStatus status, String statusDetail, Coordinate destination, String label, List<NavigationRouteResult> results, Boolean consideredTraffic, String requestId) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.destination = destination;
        this.label = label;
        this.results = Message.immutableCopyOf(results);
        this.consideredTraffic = consideredTraffic;
        this.requestId = requestId;
    }

    private NavigationRouteResponse(Builder builder) {
        this(builder.status, builder.statusDetail, builder.destination, builder.label, builder.results, builder.consideredTraffic, builder.requestId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationRouteResponse)) {
            return false;
        }
        NavigationRouteResponse o = (NavigationRouteResponse) other;
        if (equals( this.status,  o.status) && equals( this.statusDetail,  o.statusDetail) && equals( this.destination,  o.destination) && equals( this.label,  o.label) && equals(this.results, o.results) && equals( this.consideredTraffic,  o.consideredTraffic) && equals( this.requestId,  o.requestId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            hashCode = this.statusDetail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destination != null) {
            hashCode = this.destination.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.label != null) {
            hashCode = this.label.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (((hashCode2 + hashCode) * 37) + (this.results != null ? this.results.hashCode() : 1)) * 37;
        if (this.consideredTraffic != null) {
            hashCode = this.consideredTraffic.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.requestId != null) {
            i = this.requestId.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
