package com.navdy.service.library.events.file;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class PreviewFileResponse extends Message {
    public static final String DEFAULT_FILENAME = "";
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUS_DETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String filename;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String status_detail;

    public static final class Builder extends com.squareup.wire.Message.Builder<PreviewFileResponse> {
        public String filename;
        public RequestStatus status;
        public String status_detail;

        public Builder(PreviewFileResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.status_detail = message.status_detail;
                this.filename = message.filename;
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder status_detail(String status_detail) {
            this.status_detail = status_detail;
            return this;
        }

        public Builder filename(String filename) {
            this.filename = filename;
            return this;
        }

        public PreviewFileResponse build() {
            checkRequiredFields();
            return new PreviewFileResponse();
        }
    }

    public PreviewFileResponse(RequestStatus status, String status_detail, String filename) {
        this.status = status;
        this.status_detail = status_detail;
        this.filename = filename;
    }

    private PreviewFileResponse(Builder builder) {
        this(builder.status, builder.status_detail, builder.filename);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PreviewFileResponse)) {
            return false;
        }
        PreviewFileResponse o = (PreviewFileResponse) other;
        if (equals( this.status,  o.status) && equals( this.status_detail,  o.status_detail) && equals( this.filename,  o.filename)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.status_detail != null) {
            hashCode = this.status_detail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.filename != null) {
            i = this.filename.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
