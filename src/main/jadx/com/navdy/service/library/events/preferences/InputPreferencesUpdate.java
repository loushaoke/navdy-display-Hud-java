package com.navdy.service.library.events.preferences;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class InputPreferencesUpdate extends Message {
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4)
    public final InputPreferences preferences;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<InputPreferencesUpdate> {
        public InputPreferences preferences;
        public Long serial_number;
        public RequestStatus status;
        public String statusDetail;

        public Builder(InputPreferencesUpdate message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.serial_number = message.serial_number;
                this.preferences = message.preferences;
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public Builder preferences(InputPreferences preferences) {
            this.preferences = preferences;
            return this;
        }

        public InputPreferencesUpdate build() {
            checkRequiredFields();
            return new InputPreferencesUpdate();
        }
    }

    public InputPreferencesUpdate(RequestStatus status, String statusDetail, Long serial_number, InputPreferences preferences) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.serial_number = serial_number;
        this.preferences = preferences;
    }

    private InputPreferencesUpdate(Builder builder) {
        this(builder.status, builder.statusDetail, builder.serial_number, builder.preferences);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof InputPreferencesUpdate)) {
            return false;
        }
        InputPreferencesUpdate o = (InputPreferencesUpdate) other;
        if (equals( this.status,  o.status) && equals( this.statusDetail,  o.statusDetail) && equals( this.serial_number,  o.serial_number) && equals( this.preferences,  o.preferences)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            hashCode = this.statusDetail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.serial_number != null) {
            hashCode = this.serial_number.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.preferences != null) {
            i = this.preferences.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
