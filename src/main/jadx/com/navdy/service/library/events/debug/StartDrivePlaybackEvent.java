package com.navdy.service.library.events.debug;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class StartDrivePlaybackEvent extends Message {
    public static final String DEFAULT_LABEL = "";
    public static final Boolean DEFAULT_PLAYSECONDARYLOCATION = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean playSecondaryLocation;

    public static final class Builder extends com.squareup.wire.Message.Builder<StartDrivePlaybackEvent> {
        public String label;
        public Boolean playSecondaryLocation;

        public Builder(StartDrivePlaybackEvent message) {
            super(message);
            if (message != null) {
                this.label = message.label;
                this.playSecondaryLocation = message.playSecondaryLocation;
            }
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder playSecondaryLocation(Boolean playSecondaryLocation) {
            this.playSecondaryLocation = playSecondaryLocation;
            return this;
        }

        public StartDrivePlaybackEvent build() {
            checkRequiredFields();
            return new StartDrivePlaybackEvent();
        }
    }

    public StartDrivePlaybackEvent(String label, Boolean playSecondaryLocation) {
        this.label = label;
        this.playSecondaryLocation = playSecondaryLocation;
    }

    private StartDrivePlaybackEvent(Builder builder) {
        this(builder.label, builder.playSecondaryLocation);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof StartDrivePlaybackEvent)) {
            return false;
        }
        StartDrivePlaybackEvent o = (StartDrivePlaybackEvent) other;
        if (equals( this.label,  o.label) && equals( this.playSecondaryLocation,  o.playSecondaryLocation)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.label != null) {
            result = this.label.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.playSecondaryLocation != null) {
            i = this.playSecondaryLocation.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
