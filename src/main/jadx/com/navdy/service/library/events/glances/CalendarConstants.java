package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum CalendarConstants implements ProtoEnum {
    CALENDAR_TITLE(0),
    CALENDAR_TIME(1),
    CALENDAR_TIME_STR(2),
    CALENDAR_LOCATION(3);
    
    private final int value;

    private CalendarConstants(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
