package com.navdy.service.library.events.location;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class LatLong extends Message {
    public static final Double DEFAULT_LATITUDE = Double.valueOf(0.0d);
    public static final Double DEFAULT_LONGITUDE = Double.valueOf(0.0d);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.DOUBLE)
    public final Double latitude;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.DOUBLE)
    public final Double longitude;

    public static final class Builder extends com.squareup.wire.Message.Builder<LatLong> {
        public Double latitude;
        public Double longitude;

        public Builder(LatLong message) {
            super(message);
            if (message != null) {
                this.latitude = message.latitude;
                this.longitude = message.longitude;
            }
        }

        public Builder latitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public LatLong build() {
            checkRequiredFields();
            return new LatLong();
        }
    }

    public LatLong(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private LatLong(Builder builder) {
        this(builder.latitude, builder.longitude);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof LatLong)) {
            return false;
        }
        LatLong o = (LatLong) other;
        if (equals( this.latitude,  o.latitude) && equals( this.longitude,  o.longitude)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.latitude != null) {
            result = this.latitude.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.longitude != null) {
            i = this.longitude.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
