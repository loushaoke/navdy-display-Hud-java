package com.navdy.service.library.events.file;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class FileListResponse extends Message {
    public static final List<String> DEFAULT_FILES = Collections.emptyList();
    public static final FileType DEFAULT_FILE_TYPE = FileType.FILE_TYPE_OTA;
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUS_DETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.ENUM)
    public final FileType file_type;
    @ProtoField(label = Label.REPEATED, tag = 4, type = Datatype.STRING)
    public final List<String> files;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String status_detail;

    public static final class Builder extends com.squareup.wire.Message.Builder<FileListResponse> {
        public FileType file_type;
        public List<String> files;
        public RequestStatus status;
        public String status_detail;

        public Builder(FileListResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.status_detail = message.status_detail;
                this.file_type = message.file_type;
                this.files = Message.copyOf(message.files);
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder status_detail(String status_detail) {
            this.status_detail = status_detail;
            return this;
        }

        public Builder file_type(FileType file_type) {
            this.file_type = file_type;
            return this;
        }

        public Builder files(List<String> files) {
            this.files = com.squareup.wire.Message.Builder.checkForNulls(files);
            return this;
        }

        public FileListResponse build() {
            checkRequiredFields();
            return new FileListResponse();
        }
    }

    public FileListResponse(RequestStatus status, String status_detail, FileType file_type, List<String> files) {
        this.status = status;
        this.status_detail = status_detail;
        this.file_type = file_type;
        this.files = Message.immutableCopyOf(files);
    }

    private FileListResponse(Builder builder) {
        this(builder.status, builder.status_detail, builder.file_type, builder.files);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof FileListResponse)) {
            return false;
        }
        FileListResponse o = (FileListResponse) other;
        if (equals( this.status,  o.status) && equals( this.status_detail,  o.status_detail) && equals( this.file_type,  o.file_type) && equals(this.files, o.files)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.status_detail != null) {
            hashCode = this.status_detail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.file_type != null) {
            i = this.file_type.hashCode();
        }
        result = ((hashCode + i) * 37) + (this.files != null ? this.files.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
