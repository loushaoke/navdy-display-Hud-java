package com.navdy.service.library.events.contacts;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class ContactResponse extends Message {
    public static final List<Contact> DEFAULT_CONTACTS = Collections.emptyList();
    public static final String DEFAULT_IDENTIFIER = "";
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = Contact.class, tag = 5)
    public final List<Contact> contacts;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<ContactResponse> {
        public List<Contact> contacts;
        public String identifier;
        public RequestStatus status;
        public String statusDetail;

        public Builder(ContactResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.identifier = message.identifier;
                this.contacts = Message.copyOf(message.contacts);
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder contacts(List<Contact> contacts) {
            this.contacts = com.squareup.wire.Message.Builder.checkForNulls(contacts);
            return this;
        }

        public ContactResponse build() {
            checkRequiredFields();
            return new ContactResponse();
        }
    }

    public ContactResponse(RequestStatus status, String statusDetail, String identifier, List<Contact> contacts) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.identifier = identifier;
        this.contacts = Message.immutableCopyOf(contacts);
    }

    private ContactResponse(Builder builder) {
        this(builder.status, builder.statusDetail, builder.identifier, builder.contacts);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ContactResponse)) {
            return false;
        }
        ContactResponse o = (ContactResponse) other;
        if (equals( this.status,  o.status) && equals( this.statusDetail,  o.statusDetail) && equals( this.identifier,  o.identifier) && equals(this.contacts, o.contacts)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            hashCode = this.statusDetail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.identifier != null) {
            i = this.identifier.hashCode();
        }
        result = ((hashCode + i) * 37) + (this.contacts != null ? this.contacts.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
