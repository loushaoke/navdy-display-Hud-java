package com.navdy.service.library.events.dial;

import com.squareup.wire.Message;

public final class DialStatusRequest extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<DialStatusRequest> {
        public Builder(DialStatusRequest message) {
            super(message);
        }

        public DialStatusRequest build() {
            return new DialStatusRequest();
        }
    }

    private DialStatusRequest(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof DialStatusRequest;
    }

    public int hashCode() {
        return 0;
    }
}
