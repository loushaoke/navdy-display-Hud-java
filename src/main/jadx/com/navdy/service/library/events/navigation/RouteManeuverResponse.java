package com.navdy.service.library.events.navigation;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class RouteManeuverResponse extends Message {
    public static final List<RouteManeuver> DEFAULT_MANEUVERS = Collections.emptyList();
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = RouteManeuver.class, tag = 3)
    public final List<RouteManeuver> maneuvers;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<RouteManeuverResponse> {
        public List<RouteManeuver> maneuvers;
        public RequestStatus status;
        public String statusDetail;

        public Builder(RouteManeuverResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.maneuvers = Message.copyOf(message.maneuvers);
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder maneuvers(List<RouteManeuver> maneuvers) {
            this.maneuvers = com.squareup.wire.Message.Builder.checkForNulls(maneuvers);
            return this;
        }

        public RouteManeuverResponse build() {
            checkRequiredFields();
            return new RouteManeuverResponse();
        }
    }

    public RouteManeuverResponse(RequestStatus status, String statusDetail, List<RouteManeuver> maneuvers) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.maneuvers = Message.immutableCopyOf(maneuvers);
    }

    private RouteManeuverResponse(Builder builder) {
        this(builder.status, builder.statusDetail, builder.maneuvers);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof RouteManeuverResponse)) {
            return false;
        }
        RouteManeuverResponse o = (RouteManeuverResponse) other;
        if (equals( this.status,  o.status) && equals( this.statusDetail,  o.statusDetail) && equals(this.maneuvers, o.maneuvers)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            i = this.statusDetail.hashCode();
        }
        result = ((hashCode + i) * 37) + (this.maneuvers != null ? this.maneuvers.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
