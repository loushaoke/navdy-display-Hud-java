package com.navdy.service.library.events.connection;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class LinkPropertiesChanged extends Message {
    public static final Integer DEFAULT_BANDWIDTHLEVEL = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.INT32)
    public final Integer bandwidthLevel;

    public static final class Builder extends com.squareup.wire.Message.Builder<LinkPropertiesChanged> {
        public Integer bandwidthLevel;

        public Builder(LinkPropertiesChanged message) {
            super(message);
            if (message != null) {
                this.bandwidthLevel = message.bandwidthLevel;
            }
        }

        public Builder bandwidthLevel(Integer bandwidthLevel) {
            this.bandwidthLevel = bandwidthLevel;
            return this;
        }

        public LinkPropertiesChanged build() {
            return new LinkPropertiesChanged();
        }
    }

    public LinkPropertiesChanged(Integer bandwidthLevel) {
        this.bandwidthLevel = bandwidthLevel;
    }

    private LinkPropertiesChanged(Builder builder) {
        this(builder.bandwidthLevel);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof LinkPropertiesChanged) {
            return equals( this.bandwidthLevel,  ((LinkPropertiesChanged) other).bandwidthLevel);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.bandwidthLevel != null ? this.bandwidthLevel.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
