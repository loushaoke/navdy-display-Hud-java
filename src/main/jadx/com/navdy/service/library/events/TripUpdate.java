package com.navdy.service.library.events;

import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.LatLong;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class TripUpdate extends Message {
    public static final String DEFAULT_ARRIVED_AT_DESTINATION_ID = "";
    public static final Float DEFAULT_BEARING = Float.valueOf(0.0f);
    public static final String DEFAULT_CHOSEN_DESTINATION_ID = "";
    public static final Integer DEFAULT_DISTANCE_TO_DESTINATION = Integer.valueOf(0);
    public static final Integer DEFAULT_DISTANCE_TRAVELED = Integer.valueOf(0);
    public static final Double DEFAULT_ELEVATION = Double.valueOf(0.0d);
    public static final Float DEFAULT_ELEVATION_ACCURACY = Float.valueOf(0.0f);
    public static final Integer DEFAULT_ESTIMATED_TIME_REMAINING = Integer.valueOf(0);
    public static final Double DEFAULT_EXCESSIVE_SPEEDING_RATIO = Double.valueOf(0.0d);
    public static final Float DEFAULT_GPS_SPEED = Float.valueOf(0.0f);
    public static final Integer DEFAULT_HARD_ACCELERATION_COUNT = Integer.valueOf(0);
    public static final Integer DEFAULT_HARD_BREAKING_COUNT = Integer.valueOf(0);
    public static final Integer DEFAULT_HIGH_G_COUNT = Integer.valueOf(0);
    public static final Float DEFAULT_HORIZONTAL_ACCURACY = Float.valueOf(0.0f);
    public static final Integer DEFAULT_METERS_TRAVELED_SINCE_BOOT = Integer.valueOf(0);
    public static final Integer DEFAULT_OBD_SPEED = Integer.valueOf(0);
    public static final String DEFAULT_ROAD_ELEMENT = "";
    public static final Integer DEFAULT_SEQUENCE_NUMBER = Integer.valueOf(0);
    public static final Double DEFAULT_SPEEDING_RATIO = Double.valueOf(0.0d);
    public static final Long DEFAULT_TIMESTAMP = Long.valueOf(0);
    public static final Long DEFAULT_TRIP_NUMBER = Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 12, type = Datatype.STRING)
    public final String arrived_at_destination_id;
    @ProtoField(tag = 7, type = Datatype.FLOAT)
    public final Float bearing;
    @ProtoField(tag = 11, type = Datatype.STRING)
    public final String chosen_destination_id;
    @ProtoField(label = Label.REQUIRED, tag = 5)
    public final LatLong current_position;
    @ProtoField(tag = 14, type = Datatype.INT32)
    public final Integer distance_to_destination;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.INT32)
    public final Integer distance_traveled;
    @ProtoField(tag = 6, type = Datatype.DOUBLE)
    public final Double elevation;
    @ProtoField(tag = 22, type = Datatype.FLOAT)
    public final Float elevation_accuracy;
    @ProtoField(tag = 13, type = Datatype.INT32)
    public final Integer estimated_time_remaining;
    @ProtoField(tag = 19, type = Datatype.DOUBLE)
    public final Double excessive_speeding_ratio;
    @ProtoField(tag = 8, type = Datatype.FLOAT)
    public final Float gps_speed;
    @ProtoField(tag = 17, type = Datatype.INT32)
    public final Integer hard_acceleration_count;
    @ProtoField(tag = 16, type = Datatype.INT32)
    public final Integer hard_breaking_count;
    @ProtoField(tag = 15, type = Datatype.INT32)
    public final Integer high_g_count;
    @ProtoField(tag = 21, type = Datatype.FLOAT)
    public final Float horizontal_accuracy;
    @ProtoField(tag = 23)
    public final Coordinate last_raw_coordinate;
    @ProtoField(tag = 20, type = Datatype.INT32)
    public final Integer meters_traveled_since_boot;
    @ProtoField(tag = 9, type = Datatype.INT32)
    public final Integer obd_speed;
    @ProtoField(tag = 10, type = Datatype.STRING)
    public final String road_element;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer sequence_number;
    @ProtoField(tag = 18, type = Datatype.DOUBLE)
    public final Double speeding_ratio;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT64)
    public final Long timestamp;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long trip_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<TripUpdate> {
        public String arrived_at_destination_id;
        public Float bearing;
        public String chosen_destination_id;
        public LatLong current_position;
        public Integer distance_to_destination;
        public Integer distance_traveled;
        public Double elevation;
        public Float elevation_accuracy;
        public Integer estimated_time_remaining;
        public Double excessive_speeding_ratio;
        public Float gps_speed;
        public Integer hard_acceleration_count;
        public Integer hard_breaking_count;
        public Integer high_g_count;
        public Float horizontal_accuracy;
        public Coordinate last_raw_coordinate;
        public Integer meters_traveled_since_boot;
        public Integer obd_speed;
        public String road_element;
        public Integer sequence_number;
        public Double speeding_ratio;
        public Long timestamp;
        public Long trip_number;

        public Builder(TripUpdate message) {
            super(message);
            if (message != null) {
                this.trip_number = message.trip_number;
                this.sequence_number = message.sequence_number;
                this.timestamp = message.timestamp;
                this.distance_traveled = message.distance_traveled;
                this.current_position = message.current_position;
                this.elevation = message.elevation;
                this.bearing = message.bearing;
                this.gps_speed = message.gps_speed;
                this.obd_speed = message.obd_speed;
                this.road_element = message.road_element;
                this.chosen_destination_id = message.chosen_destination_id;
                this.arrived_at_destination_id = message.arrived_at_destination_id;
                this.estimated_time_remaining = message.estimated_time_remaining;
                this.distance_to_destination = message.distance_to_destination;
                this.high_g_count = message.high_g_count;
                this.hard_breaking_count = message.hard_breaking_count;
                this.hard_acceleration_count = message.hard_acceleration_count;
                this.speeding_ratio = message.speeding_ratio;
                this.excessive_speeding_ratio = message.excessive_speeding_ratio;
                this.meters_traveled_since_boot = message.meters_traveled_since_boot;
                this.horizontal_accuracy = message.horizontal_accuracy;
                this.elevation_accuracy = message.elevation_accuracy;
                this.last_raw_coordinate = message.last_raw_coordinate;
            }
        }

        public Builder trip_number(Long trip_number) {
            this.trip_number = trip_number;
            return this;
        }

        public Builder sequence_number(Integer sequence_number) {
            this.sequence_number = sequence_number;
            return this;
        }

        public Builder timestamp(Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder distance_traveled(Integer distance_traveled) {
            this.distance_traveled = distance_traveled;
            return this;
        }

        public Builder current_position(LatLong current_position) {
            this.current_position = current_position;
            return this;
        }

        public Builder elevation(Double elevation) {
            this.elevation = elevation;
            return this;
        }

        public Builder bearing(Float bearing) {
            this.bearing = bearing;
            return this;
        }

        public Builder gps_speed(Float gps_speed) {
            this.gps_speed = gps_speed;
            return this;
        }

        public Builder obd_speed(Integer obd_speed) {
            this.obd_speed = obd_speed;
            return this;
        }

        public Builder road_element(String road_element) {
            this.road_element = road_element;
            return this;
        }

        public Builder chosen_destination_id(String chosen_destination_id) {
            this.chosen_destination_id = chosen_destination_id;
            return this;
        }

        public Builder arrived_at_destination_id(String arrived_at_destination_id) {
            this.arrived_at_destination_id = arrived_at_destination_id;
            return this;
        }

        public Builder estimated_time_remaining(Integer estimated_time_remaining) {
            this.estimated_time_remaining = estimated_time_remaining;
            return this;
        }

        public Builder distance_to_destination(Integer distance_to_destination) {
            this.distance_to_destination = distance_to_destination;
            return this;
        }

        public Builder high_g_count(Integer high_g_count) {
            this.high_g_count = high_g_count;
            return this;
        }

        public Builder hard_breaking_count(Integer hard_breaking_count) {
            this.hard_breaking_count = hard_breaking_count;
            return this;
        }

        public Builder hard_acceleration_count(Integer hard_acceleration_count) {
            this.hard_acceleration_count = hard_acceleration_count;
            return this;
        }

        public Builder speeding_ratio(Double speeding_ratio) {
            this.speeding_ratio = speeding_ratio;
            return this;
        }

        public Builder excessive_speeding_ratio(Double excessive_speeding_ratio) {
            this.excessive_speeding_ratio = excessive_speeding_ratio;
            return this;
        }

        public Builder meters_traveled_since_boot(Integer meters_traveled_since_boot) {
            this.meters_traveled_since_boot = meters_traveled_since_boot;
            return this;
        }

        public Builder horizontal_accuracy(Float horizontal_accuracy) {
            this.horizontal_accuracy = horizontal_accuracy;
            return this;
        }

        public Builder elevation_accuracy(Float elevation_accuracy) {
            this.elevation_accuracy = elevation_accuracy;
            return this;
        }

        public Builder last_raw_coordinate(Coordinate last_raw_coordinate) {
            this.last_raw_coordinate = last_raw_coordinate;
            return this;
        }

        public TripUpdate build() {
            checkRequiredFields();
            return new TripUpdate();
        }
    }

    public TripUpdate(Long trip_number, Integer sequence_number, Long timestamp, Integer distance_traveled, LatLong current_position, Double elevation, Float bearing, Float gps_speed, Integer obd_speed, String road_element, String chosen_destination_id, String arrived_at_destination_id, Integer estimated_time_remaining, Integer distance_to_destination, Integer high_g_count, Integer hard_breaking_count, Integer hard_acceleration_count, Double speeding_ratio, Double excessive_speeding_ratio, Integer meters_traveled_since_boot, Float horizontal_accuracy, Float elevation_accuracy, Coordinate last_raw_coordinate) {
        this.trip_number = trip_number;
        this.sequence_number = sequence_number;
        this.timestamp = timestamp;
        this.distance_traveled = distance_traveled;
        this.current_position = current_position;
        this.elevation = elevation;
        this.bearing = bearing;
        this.gps_speed = gps_speed;
        this.obd_speed = obd_speed;
        this.road_element = road_element;
        this.chosen_destination_id = chosen_destination_id;
        this.arrived_at_destination_id = arrived_at_destination_id;
        this.estimated_time_remaining = estimated_time_remaining;
        this.distance_to_destination = distance_to_destination;
        this.high_g_count = high_g_count;
        this.hard_breaking_count = hard_breaking_count;
        this.hard_acceleration_count = hard_acceleration_count;
        this.speeding_ratio = speeding_ratio;
        this.excessive_speeding_ratio = excessive_speeding_ratio;
        this.meters_traveled_since_boot = meters_traveled_since_boot;
        this.horizontal_accuracy = horizontal_accuracy;
        this.elevation_accuracy = elevation_accuracy;
        this.last_raw_coordinate = last_raw_coordinate;
    }

    private TripUpdate(Builder builder) {
        this(builder.trip_number, builder.sequence_number, builder.timestamp, builder.distance_traveled, builder.current_position, builder.elevation, builder.bearing, builder.gps_speed, builder.obd_speed, builder.road_element, builder.chosen_destination_id, builder.arrived_at_destination_id, builder.estimated_time_remaining, builder.distance_to_destination, builder.high_g_count, builder.hard_breaking_count, builder.hard_acceleration_count, builder.speeding_ratio, builder.excessive_speeding_ratio, builder.meters_traveled_since_boot, builder.horizontal_accuracy, builder.elevation_accuracy, builder.last_raw_coordinate);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof TripUpdate)) {
            return false;
        }
        TripUpdate o = (TripUpdate) other;
        if (equals( this.trip_number,  o.trip_number) && equals( this.sequence_number,  o.sequence_number) && equals( this.timestamp,  o.timestamp) && equals( this.distance_traveled,  o.distance_traveled) && equals( this.current_position,  o.current_position) && equals( this.elevation,  o.elevation) && equals( this.bearing,  o.bearing) && equals( this.gps_speed,  o.gps_speed) && equals( this.obd_speed,  o.obd_speed) && equals( this.road_element,  o.road_element) && equals( this.chosen_destination_id,  o.chosen_destination_id) && equals( this.arrived_at_destination_id,  o.arrived_at_destination_id) && equals( this.estimated_time_remaining,  o.estimated_time_remaining) && equals( this.distance_to_destination,  o.distance_to_destination) && equals( this.high_g_count,  o.high_g_count) && equals( this.hard_breaking_count,  o.hard_breaking_count) && equals( this.hard_acceleration_count,  o.hard_acceleration_count) && equals( this.speeding_ratio,  o.speeding_ratio) && equals( this.excessive_speeding_ratio,  o.excessive_speeding_ratio) && equals( this.meters_traveled_since_boot,  o.meters_traveled_since_boot) && equals( this.horizontal_accuracy,  o.horizontal_accuracy) && equals( this.elevation_accuracy,  o.elevation_accuracy) && equals( this.last_raw_coordinate,  o.last_raw_coordinate)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.trip_number != null ? this.trip_number.hashCode() : 0) * 37;
        if (this.sequence_number != null) {
            hashCode = this.sequence_number.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.timestamp != null) {
            hashCode = this.timestamp.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.distance_traveled != null) {
            hashCode = this.distance_traveled.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.current_position != null) {
            hashCode = this.current_position.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.elevation != null) {
            hashCode = this.elevation.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.bearing != null) {
            hashCode = this.bearing.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.gps_speed != null) {
            hashCode = this.gps_speed.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.obd_speed != null) {
            hashCode = this.obd_speed.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.road_element != null) {
            hashCode = this.road_element.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.chosen_destination_id != null) {
            hashCode = this.chosen_destination_id.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.arrived_at_destination_id != null) {
            hashCode = this.arrived_at_destination_id.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.estimated_time_remaining != null) {
            hashCode = this.estimated_time_remaining.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.distance_to_destination != null) {
            hashCode = this.distance_to_destination.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.high_g_count != null) {
            hashCode = this.high_g_count.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.hard_breaking_count != null) {
            hashCode = this.hard_breaking_count.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.hard_acceleration_count != null) {
            hashCode = this.hard_acceleration_count.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.speeding_ratio != null) {
            hashCode = this.speeding_ratio.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.excessive_speeding_ratio != null) {
            hashCode = this.excessive_speeding_ratio.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.meters_traveled_since_boot != null) {
            hashCode = this.meters_traveled_since_boot.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.horizontal_accuracy != null) {
            hashCode = this.horizontal_accuracy.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.elevation_accuracy != null) {
            hashCode = this.elevation_accuracy.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.last_raw_coordinate != null) {
            i = this.last_raw_coordinate.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
