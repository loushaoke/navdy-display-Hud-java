package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class MusicArtworkRequest extends Message {
    public static final String DEFAULT_ALBUM = "";
    public static final String DEFAULT_AUTHOR = "";
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final String DEFAULT_NAME = "";
    public static final Integer DEFAULT_SIZE = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String album;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String author;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 6, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 5, type = Datatype.UINT32)
    public final Integer size;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicArtworkRequest> {
        public String album;
        public String author;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public String name;
        public Integer size;

        public Builder(MusicArtworkRequest message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.name = message.name;
                this.album = message.album;
                this.author = message.author;
                this.size = message.size;
                this.collectionType = message.collectionType;
                this.collectionId = message.collectionId;
            }
        }

        public Builder collectionSource(MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder album(String album) {
            this.album = album;
            return this;
        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder size(Integer size) {
            this.size = size;
            return this;
        }

        public Builder collectionType(MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }

        public Builder collectionId(String collectionId) {
            this.collectionId = collectionId;
            return this;
        }

        public MusicArtworkRequest build() {
            return new MusicArtworkRequest();
        }
    }

    public MusicArtworkRequest(MusicCollectionSource collectionSource, String name, String album, String author, Integer size, MusicCollectionType collectionType, String collectionId) {
        this.collectionSource = collectionSource;
        this.name = name;
        this.album = album;
        this.author = author;
        this.size = size;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
    }

    private MusicArtworkRequest(Builder builder) {
        this(builder.collectionSource, builder.name, builder.album, builder.author, builder.size, builder.collectionType, builder.collectionId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicArtworkRequest)) {
            return false;
        }
        MusicArtworkRequest o = (MusicArtworkRequest) other;
        if (equals( this.collectionSource,  o.collectionSource) && equals( this.name,  o.name) && equals( this.album,  o.album) && equals( this.author,  o.author) && equals( this.size,  o.size) && equals( this.collectionType,  o.collectionType) && equals( this.collectionId,  o.collectionId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.name != null) {
            hashCode = this.name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.album != null) {
            hashCode = this.album.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.author != null) {
            hashCode = this.author.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.size != null) {
            hashCode = this.size.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.collectionType != null) {
            hashCode = this.collectionType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.collectionId != null) {
            i = this.collectionId.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
