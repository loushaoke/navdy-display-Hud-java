package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum GenericConstants implements ProtoEnum {
    GENERIC_TITLE(0),
    GENERIC_MESSAGE(1),
    GENERIC_MAIN_ICON(2),
    GENERIC_SIDE_ICON(3);
    
    private final int value;

    private GenericConstants(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
