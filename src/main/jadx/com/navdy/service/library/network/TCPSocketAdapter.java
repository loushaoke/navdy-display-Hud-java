package com.navdy.service.library.network;

import com.navdy.service.library.device.NavdyDeviceId;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class TCPSocketAdapter implements SocketAdapter {
    private final String host;
    private final int port;
    private Socket socket;

    public TCPSocketAdapter(String host, int port) {
        if (host == null) {
            throw new IllegalArgumentException("Host must not be null");
        }
        this.host = host;
        this.port = port;
    }

    public TCPSocketAdapter(Socket socket) {
        this.host = null;
        this.port = -1;
        this.socket = socket;
    }

    public void connect() throws IOException {
        if (this.host == null) {
            throw new IllegalStateException("Can't connect with accepted socket");
        }
        this.socket = new Socket(this.host, this.port);
    }

    public void close() throws IOException {
        if (this.socket != null) {
            this.socket.close();
        }
        this.socket = null;
    }

    public InputStream getInputStream() throws IOException {
        return this.socket.getInputStream();
    }

    public OutputStream getOutputStream() throws IOException {
        return this.socket.getOutputStream();
    }

    public boolean isConnected() {
        if (this.socket != null) {
            return this.socket.isConnected();
        }
        return false;
    }

    public NavdyDeviceId getRemoteDevice() {
        if (isConnected()) {
            return NavdyDeviceId.UNKNOWN_ID;
        }
        return null;
    }

    public String getRemoteAddress() {
        if (isConnected()) {
            return this.socket.getInetAddress().getHostAddress();
        }
        return null;
    }
}
