package com.navdy.service.library.log;

public class FilteringAppender implements LogAppender {
    private LogAppender baseAppender;
    private Filter filter;

    public FilteringAppender(LogAppender baseAppender, Filter filter) {
        this.baseAppender = baseAppender;
        this.filter = filter;
    }

    public void v(String tag, String msg) {
        if (this.filter.matches(2, tag, msg)) {
            this.baseAppender.v(tag, msg);
        }
    }

    public void v(String tag, String msg, Throwable tr) {
        if (this.filter.matches(2, tag, msg)) {
            this.baseAppender.v(tag, msg, tr);
        }
    }

    public void d(String tag, String msg) {
        if (this.filter.matches(3, tag, msg)) {
            this.baseAppender.d(tag, msg);
        }
    }

    public void d(String tag, String msg, Throwable tr) {
        if (this.filter.matches(3, tag, msg)) {
            this.baseAppender.d(tag, msg, tr);
        }
    }

    public void i(String tag, String msg) {
        if (this.filter.matches(4, tag, msg)) {
            this.baseAppender.i(tag, msg);
        }
    }

    public void i(String tag, String msg, Throwable tr) {
        if (this.filter.matches(4, tag, msg)) {
            this.baseAppender.i(tag, msg, tr);
        }
    }

    public void w(String tag, String msg) {
        if (this.filter.matches(5, tag, msg)) {
            this.baseAppender.w(tag, msg);
        }
    }

    public void w(String tag, String msg, Throwable tr) {
        if (this.filter.matches(5, tag, msg)) {
            this.baseAppender.w(tag, msg, tr);
        }
    }

    public void e(String tag, String msg) {
        if (this.filter.matches(6, tag, msg)) {
            this.baseAppender.e(tag, msg);
        }
    }

    public void e(String tag, String msg, Throwable tr) {
        if (this.filter.matches(6, tag, msg)) {
            this.baseAppender.e(tag, msg, tr);
        }
    }

    public void flush() {
        this.baseAppender.flush();
    }

    public void close() {
        this.baseAppender.close();
    }
}
