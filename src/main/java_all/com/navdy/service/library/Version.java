package com.navdy.service.library;

public class Version
{
    public static final Version PROTOCOL_VERSION;
    public final int majorVersion;
    public final int minorVersion;
    
    static {
        PROTOCOL_VERSION = new Version(1, 0);
    }
    
    public Version(final int majorVersion, final int minorVersion) {
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
    }
    
    @Override
    public String toString() {
        return this.majorVersion + "." + this.minorVersion;
    }
}
