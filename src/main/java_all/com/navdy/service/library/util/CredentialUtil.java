package com.navdy.service.library.util;

import android.content.pm.PackageManager;
import android.content.Context;
import com.navdy.service.library.log.Logger;

public class CredentialUtil
{
    public static final Logger logger;
    
    static {
        logger = new Logger(CredentialUtil.class);
    }
    
    public static String getCredentials(final Context context, final String s) {
        final String s2 = "";
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(s);
        }
        catch (PackageManager$NameNotFoundException ex) {
            CredentialUtil.logger.e("Failed to load meta-data, NameNotFound: " + ex.getMessage());
            return s2;
        }
        catch (NullPointerException ex2) {
            CredentialUtil.logger.e("Failed to load meta-data, NullPointer: " + ex2.getMessage());
            return s2;
        }
    }
}
