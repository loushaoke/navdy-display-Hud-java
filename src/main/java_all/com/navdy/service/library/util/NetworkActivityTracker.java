package com.navdy.service.library.util;

import java.util.concurrent.atomic.AtomicLong;
import com.navdy.service.library.log.Logger;

public class NetworkActivityTracker extends Thread
{
    private static final int LOGGING_INTERVAL = 60000;
    private static final NetworkActivityTracker sInstance;
    private static final Logger sLogger;
    private AtomicLong bytesReceived;
    private AtomicLong bytesSent;
    private double highestReceivedBandwidth;
    private double highestSentBandwidth;
    private long starttime;
    
    static {
        sLogger = new Logger(NetworkActivityTracker.class);
        sInstance = new NetworkActivityTracker();
    }
    
    private NetworkActivityTracker() {
        this.bytesSent = new AtomicLong(0L);
        this.bytesReceived = new AtomicLong(0L);
        this.starttime = System.currentTimeMillis();
        this.highestSentBandwidth = 0.0;
        this.highestReceivedBandwidth = 0.0;
    }
    
    public static NetworkActivityTracker getInstance() {
        return NetworkActivityTracker.sInstance;
    }
    
    public void addBytesReceived(final int n) {
        this.bytesReceived.addAndGet(n);
    }
    
    public void addBytesSent(final int n) {
        this.bytesSent.addAndGet(n);
    }
    
    @Override
    public void run() {
        long value = this.bytesSent.get();
        long value2 = this.bytesReceived.get();
        long starttime = this.starttime;
        int n = 0;
        while (true) {
            try {
                int n2;
                do {
                    Thread.sleep(60000L);
                    n2 = n;
                    final long value3 = this.bytesSent.get();
                    final long value4 = this.bytesReceived.get();
                    final long currentTimeMillis = System.currentTimeMillis();
                    final long n3 = value3 - value;
                    final long n4 = value4 - value2;
                    final long n5 = currentTimeMillis - starttime;
                    final long starttime2 = this.starttime;
                    final double n6 = n3 * 1000.0 / n5;
                    final double n7 = n4 * 1000.0 / n5;
                    value = value3;
                    value2 = value4;
                    starttime = currentTimeMillis;
                    this.highestSentBandwidth = Math.max(this.highestSentBandwidth, n6);
                    this.highestReceivedBandwidth = Math.max(this.highestReceivedBandwidth, n7);
                    if (n3 > 0L || n4 > 0L) {
                        NetworkActivityTracker.sLogger.i(String.format("Total time: %dms interval: %dms sent: %d bytes received: %d bytes totalSent: %d bytes totalReceived: %d bytes sentBandwidth: %.2f bytes/sec receivedBandwidth: %.2f bytes/sec maxSentBandwidth: %.2f bytes/sec maxReceivedBandwidth: %.2f bytes/sec", currentTimeMillis - starttime2, n5, n3, n4, value3, value4, n6, n7, this.highestSentBandwidth, this.highestReceivedBandwidth));
                    }
                } while ((n = n2) == 0);
            }
            catch (InterruptedException ex) {
                final int n2 = 1;
                continue;
            }
            break;
        }
    }
}
