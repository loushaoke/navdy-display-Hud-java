package com.navdy.service.library.util;

import java.util.Iterator;
import java.util.Map;
import android.os.Debug;
import java.io.PrintWriter;
import java.io.Writer;
import com.navdy.service.library.log.FastPrintWriter;
import java.io.StringWriter;
import android.os.Build;
import android.os.Build;
import java.util.Calendar;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import android.util.Log;
import android.content.Context;
import java.io.File;
import android.os.Environment;
import com.navdy.service.library.log.Logger;

public final class LogUtils
{
    private static final int BUFFER_SIZE = 32768;
    private static final String CRASH_DUMP_FILE = "crash.log";
    private static final String CRASH_DUMP_STAGING = "staging";
    private static final byte[] CRLF;
    private static RotatingLogFile[] LOG_FILES;
    private static RotatingLogFile[] LOG_FILES_FOR_SNAPSHOT;
    private static final String LOG_MARKER = "--------- beginning of main";
    private static final String SYSTEM_LOG_PATH = ".logs";
    private static final String TAG;
    private static Logger sLogger;
    
    static {
        LogUtils.sLogger = new Logger(LogUtils.class);
        TAG = LogUtils.class.getName();
        LogUtils.LOG_FILES = new RotatingLogFile[] { new RotatingLogFile("a.log", 4), new RotatingLogFile("obd.log", 2), new RotatingLogFile("mfi.log", 2), new RotatingLogFile("lighting.log", 2), new RotatingLogFile("dial.log", 4), new RotatingLogFile("obdRaw.log", 2) };
        LogUtils.LOG_FILES_FOR_SNAPSHOT = new RotatingLogFile[] { new RotatingLogFile("a.log", 2), new RotatingLogFile("obd.log", 2), new RotatingLogFile("mfi.log", 2), new RotatingLogFile("lighting.log", 2), new RotatingLogFile("dial.log", 2), new RotatingLogFile("obdRaw.log", 2) };
        CRLF = "\r\n".getBytes();
    }
    
    public static void copyComprehensiveSystemLogs(final String s) throws Throwable {
        copySystemLogs(s, LogUtils.LOG_FILES);
    }
    
    public static void copySnapshotSystemLogs(final String s) throws Throwable {
        copySystemLogs(s, LogUtils.LOG_FILES_FOR_SNAPSHOT);
    }
    
    public static void copySystemLogs(final String s, final RotatingLogFile[] array) throws Throwable {
        final String string = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + ".logs";
        for (final RotatingLogFile rotatingLogFile : array) {
            final String name = rotatingLogFile.name;
            for (int maxFiles = rotatingLogFile.maxFiles, j = 0; j < maxFiles; ++j) {
                String string2;
                final String s2 = string2 = name;
                if (j > 0) {
                    string2 = s2 + "." + j;
                }
                IOUtils.copyFile(string + File.separator + string2, s + File.separator + string2);
            }
        }
    }
    
    public static void createCrashDump(final Context p0, final String p1, final Thread p2, final Throwable p3, final boolean p4) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: dup            
        //     3: astore          5
        //     5: monitorenter   
        //     6: aconst_null    
        //     7: astore          6
        //     9: aload           6
        //    11: astore          7
        //    13: aload_0        
        //    14: invokestatic    android/os/Process.myPid:()I
        //    17: invokestatic    com/navdy/service/library/util/SystemUtils.getProcessName:(Landroid/content/Context;I)Ljava/lang/String;
        //    20: astore          8
        //    22: aload           6
        //    24: astore          7
        //    26: getstatic       com/navdy/service/library/util/LogUtils.TAG:Ljava/lang/String;
        //    29: ldc             "creating CrashDump"
        //    31: invokestatic    android/util/Log.v:(Ljava/lang/String;Ljava/lang/String;)I
        //    34: pop            
        //    35: aload           6
        //    37: astore          7
        //    39: new             Ljava/io/File;
        //    42: astore          9
        //    44: aload           6
        //    46: astore          7
        //    48: new             Ljava/lang/StringBuilder;
        //    51: astore          10
        //    53: aload           6
        //    55: astore          7
        //    57: aload           10
        //    59: invokespecial   java/lang/StringBuilder.<init>:()V
        //    62: aload           6
        //    64: astore          7
        //    66: aload           9
        //    68: aload           10
        //    70: aload_1        
        //    71: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    74: getstatic       java/io/File.separator:Ljava/lang/String;
        //    77: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    80: ldc             "staging"
        //    82: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    85: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    88: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    91: aload           6
        //    93: astore          7
        //    95: aload           9
        //    97: invokevirtual   java/io/File.exists:()Z
        //   100: ifeq            113
        //   103: aload           6
        //   105: astore          7
        //   107: aload_0        
        //   108: aload           9
        //   110: invokestatic    com/navdy/service/library/util/IOUtils.deleteDirectory:(Landroid/content/Context;Ljava/io/File;)V
        //   113: aload           6
        //   115: astore          7
        //   117: aload           9
        //   119: invokestatic    com/navdy/service/library/util/IOUtils.createDirectory:(Ljava/io/File;)Z
        //   122: pop            
        //   123: aload           6
        //   125: astore          7
        //   127: aload           9
        //   129: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   132: astore          9
        //   134: aload           6
        //   136: astore          7
        //   138: new             Ljava/io/FileOutputStream;
        //   141: astore          10
        //   143: aload           6
        //   145: astore          7
        //   147: new             Ljava/lang/StringBuilder;
        //   150: astore          11
        //   152: aload           6
        //   154: astore          7
        //   156: aload           11
        //   158: invokespecial   java/lang/StringBuilder.<init>:()V
        //   161: aload           6
        //   163: astore          7
        //   165: aload           10
        //   167: aload           11
        //   169: aload           9
        //   171: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   174: getstatic       java/io/File.separator:Ljava/lang/String;
        //   177: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   180: ldc             "crash.log"
        //   182: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   185: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   188: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   191: aload           10
        //   193: aload_3        
        //   194: aload           8
        //   196: aload_2        
        //   197: iload           4
        //   199: invokestatic    com/navdy/service/library/util/LogUtils.writeCrashInfo:(Ljava/io/FileOutputStream;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Thread;Z)V
        //   202: aload           10
        //   204: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   207: aload           10
        //   209: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   212: aconst_null    
        //   213: astore_3       
        //   214: aload_3        
        //   215: astore          7
        //   217: aload           9
        //   219: invokestatic    com/navdy/service/library/util/LogUtils.copyComprehensiveSystemLogs:(Ljava/lang/String;)V
        //   222: ldc             ""
        //   224: astore_2       
        //   225: iload           4
        //   227: ifeq            233
        //   230: ldc             "_native"
        //   232: astore_2       
        //   233: aload_3        
        //   234: astore          7
        //   236: new             Ljava/lang/StringBuilder;
        //   239: astore          10
        //   241: aload_3        
        //   242: astore          7
        //   244: aload           10
        //   246: invokespecial   java/lang/StringBuilder.<init>:()V
        //   249: aload_3        
        //   250: astore          7
        //   252: aload_0        
        //   253: aload           10
        //   255: aload_1        
        //   256: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   259: getstatic       java/io/File.separator:Ljava/lang/String;
        //   262: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   265: aload           8
        //   267: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   270: aload_2        
        //   271: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   274: ldc             "_crash_"
        //   276: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   279: invokestatic    java/lang/System.currentTimeMillis:()J
        //   282: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   285: ldc             ".zip"
        //   287: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   290: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   293: aload           9
        //   295: invokestatic    com/navdy/service/library/util/LogUtils.zipStaging:(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
        //   298: aload_3        
        //   299: astore          7
        //   301: new             Ljava/io/File;
        //   304: astore_1       
        //   305: aload_3        
        //   306: astore          7
        //   308: aload_1        
        //   309: aload           9
        //   311: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   314: aload_3        
        //   315: astore          7
        //   317: aload_0        
        //   318: aload_1        
        //   319: invokestatic    com/navdy/service/library/util/IOUtils.deleteDirectory:(Landroid/content/Context;Ljava/io/File;)V
        //   322: aconst_null    
        //   323: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   326: aload           5
        //   328: monitorexit    
        //   329: return         
        //   330: astore_2       
        //   331: aload_3        
        //   332: astore          7
        //   334: getstatic       com/navdy/service/library/util/LogUtils.TAG:Ljava/lang/String;
        //   337: ldc             "copySystemLogs"
        //   339: aload_2        
        //   340: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   343: pop            
        //   344: goto            222
        //   347: aconst_null    
        //   348: astore_0       
        //   349: astore_1       
        //   350: aload_0        
        //   351: astore          7
        //   353: getstatic       com/navdy/service/library/util/LogUtils.TAG:Ljava/lang/String;
        //   356: ldc             "createCrashDump"
        //   358: aload_1        
        //   359: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   362: pop            
        //   363: aload_0        
        //   364: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   367: goto            326
        //   370: astore_0       
        //   371: aload           5
        //   373: monitorexit    
        //   374: aload_0        
        //   375: athrow         
        //   376: astore_0       
        //   377: aload           7
        //   379: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   382: aload_0        
        //   383: athrow         
        //   384: astore_0       
        //   385: aload           10
        //   387: astore          7
        //   389: goto            377
        //   392: astore_1       
        //   393: aload           10
        //   395: astore_0       
        //   396: goto            350
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  13     22     347    350    Ljava/lang/Throwable;
        //  13     22     376    377    Any
        //  26     35     347    350    Ljava/lang/Throwable;
        //  26     35     376    377    Any
        //  39     44     347    350    Ljava/lang/Throwable;
        //  39     44     376    377    Any
        //  48     53     347    350    Ljava/lang/Throwable;
        //  48     53     376    377    Any
        //  57     62     347    350    Ljava/lang/Throwable;
        //  57     62     376    377    Any
        //  66     91     347    350    Ljava/lang/Throwable;
        //  66     91     376    377    Any
        //  95     103    347    350    Ljava/lang/Throwable;
        //  95     103    376    377    Any
        //  107    113    347    350    Ljava/lang/Throwable;
        //  107    113    376    377    Any
        //  117    123    347    350    Ljava/lang/Throwable;
        //  117    123    376    377    Any
        //  127    134    347    350    Ljava/lang/Throwable;
        //  127    134    376    377    Any
        //  138    143    347    350    Ljava/lang/Throwable;
        //  138    143    376    377    Any
        //  147    152    347    350    Ljava/lang/Throwable;
        //  147    152    376    377    Any
        //  156    161    347    350    Ljava/lang/Throwable;
        //  156    161    376    377    Any
        //  165    191    347    350    Ljava/lang/Throwable;
        //  165    191    376    377    Any
        //  191    212    392    399    Ljava/lang/Throwable;
        //  191    212    384    392    Any
        //  217    222    330    347    Ljava/lang/Throwable;
        //  217    222    376    377    Any
        //  236    241    347    350    Ljava/lang/Throwable;
        //  236    241    376    377    Any
        //  244    249    347    350    Ljava/lang/Throwable;
        //  244    249    376    377    Any
        //  252    298    347    350    Ljava/lang/Throwable;
        //  252    298    376    377    Any
        //  301    305    347    350    Ljava/lang/Throwable;
        //  301    305    376    377    Any
        //  308    314    347    350    Ljava/lang/Throwable;
        //  308    314    376    377    Any
        //  317    322    347    350    Ljava/lang/Throwable;
        //  317    322    376    377    Any
        //  322    326    370    376    Any
        //  334    344    347    350    Ljava/lang/Throwable;
        //  334    344    376    377    Any
        //  353    363    376    377    Any
        //  363    367    370    376    Any
        //  377    384    370    376    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0326:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void createKernelCrashDump(final Context context, String string, final String[] array) {
        synchronized (LogUtils.class) {
            try {
                final File file = new File(string + File.separator + "staging");
                if (file.exists()) {
                    IOUtils.deleteDirectory(context, file);
                }
                IOUtils.createDirectory(file);
                int n = 0;
                final String absolutePath = file.getAbsolutePath();
                int n2;
                for (int length = array.length, i = 0; i < length; ++i, n = n2) {
                    final String s = array[i];
                    final File file2 = new File(s);
                    final StringBuilder sb = new StringBuilder();
                    n2 = n;
                    if (IOUtils.copyFile(s, sb.append(absolutePath).append(File.separator).append(file2.getName()).toString())) {
                        LogUtils.sLogger.v("copied:" + s);
                        n2 = n + 1;
                    }
                }
                if (n > 0) {
                    string = string + File.separator + "kernel_crash_" + System.currentTimeMillis() + ".zip";
                    zipStaging(context, string, absolutePath);
                    LogUtils.sLogger.v("kernel dump created:" + string);
                }
                else {
                    LogUtils.sLogger.v("no kernel crash files");
                }
                IOUtils.deleteDirectory(context, new File(absolutePath));
            }
            catch (Throwable t) {
                Log.e(LogUtils.TAG, "createKernelCrashDump", t);
            }
        }
    }
    
    public static void dumpLog(final Context p0, final String p1) throws Throwable {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: dup            
        //     3: astore_2       
        //     4: monitorenter   
        //     5: new             Ljava/io/File;
        //     8: astore_3       
        //     9: aload_3        
        //    10: aload_1        
        //    11: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    14: aload_3        
        //    15: invokevirtual   java/io/File.exists:()Z
        //    18: ifeq            27
        //    21: aload_0        
        //    22: aload_1        
        //    23: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //    26: pop            
        //    27: aconst_null    
        //    28: astore          4
        //    30: aconst_null    
        //    31: astore          5
        //    33: aconst_null    
        //    34: astore          6
        //    36: aconst_null    
        //    37: astore          7
        //    39: aconst_null    
        //    40: astore          8
        //    42: aconst_null    
        //    43: astore_0       
        //    44: ldc             32768
        //    46: newarray        B
        //    48: astore          9
        //    50: new             Ljava/io/FileOutputStream;
        //    53: astore          10
        //    55: aload           10
        //    57: aload_1        
        //    58: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //    61: new             Ljava/io/BufferedOutputStream;
        //    64: astore_3       
        //    65: aload_3        
        //    66: aload           10
        //    68: ldc             32768
        //    70: invokespecial   java/io/BufferedOutputStream.<init>:(Ljava/io/OutputStream;I)V
        //    73: aload           4
        //    75: astore_1       
        //    76: aload           8
        //    78: astore_0       
        //    79: invokestatic    java/lang/Runtime.getRuntime:()Ljava/lang/Runtime;
        //    82: iconst_4       
        //    83: anewarray       Ljava/lang/String;
        //    86: dup            
        //    87: iconst_0       
        //    88: ldc             "logcat"
        //    90: aastore        
        //    91: dup            
        //    92: iconst_1       
        //    93: ldc             "-d"
        //    95: aastore        
        //    96: dup            
        //    97: iconst_2       
        //    98: ldc             "-v"
        //   100: aastore        
        //   101: dup            
        //   102: iconst_3       
        //   103: ldc             "time"
        //   105: aastore        
        //   106: invokevirtual   java/lang/Runtime.exec:([Ljava/lang/String;)Ljava/lang/Process;
        //   109: astore          4
        //   111: aload           4
        //   113: astore_1       
        //   114: aload           8
        //   116: astore_0       
        //   117: aload           4
        //   119: invokevirtual   java/lang/Process.getInputStream:()Ljava/io/InputStream;
        //   122: astore          5
        //   124: aload           4
        //   126: astore_1       
        //   127: aload           5
        //   129: astore_0       
        //   130: new             Ljava/io/BufferedInputStream;
        //   133: astore          6
        //   135: aload           4
        //   137: astore_1       
        //   138: aload           5
        //   140: astore_0       
        //   141: aload           6
        //   143: aload           5
        //   145: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //   148: aload           6
        //   150: aload           9
        //   152: invokevirtual   java/io/BufferedInputStream.read:([B)I
        //   155: istore          11
        //   157: iload           11
        //   159: ifle            229
        //   162: aload_3        
        //   163: aload           9
        //   165: iconst_0       
        //   166: iload           11
        //   168: invokevirtual   java/io/BufferedOutputStream.write:([BII)V
        //   171: goto            148
        //   174: astore          8
        //   176: aload           6
        //   178: astore          7
        //   180: aload           5
        //   182: astore_0       
        //   183: aload           4
        //   185: astore_1       
        //   186: aload           7
        //   188: astore          4
        //   190: aload           10
        //   192: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   195: aload_3        
        //   196: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   199: aload           4
        //   201: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   204: aload           10
        //   206: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   209: aload_0        
        //   210: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   213: aload_1        
        //   214: ifnull          221
        //   217: aload_1        
        //   218: invokevirtual   java/lang/Process.destroy:()V
        //   221: aload           8
        //   223: athrow         
        //   224: astore_0       
        //   225: aload_2        
        //   226: monitorexit    
        //   227: aload_0        
        //   228: athrow         
        //   229: aload_3        
        //   230: invokevirtual   java/io/BufferedOutputStream.flush:()V
        //   233: aload           10
        //   235: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
        //   238: aload_3        
        //   239: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   242: aload           6
        //   244: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   247: aload           10
        //   249: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   252: aload           5
        //   254: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   257: aload           4
        //   259: ifnull          267
        //   262: aload           4
        //   264: invokevirtual   java/lang/Process.destroy:()V
        //   267: aload_2        
        //   268: monitorexit    
        //   269: return         
        //   270: astore_0       
        //   271: getstatic       com/navdy/service/library/util/LogUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   274: aload_0        
        //   275: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   278: goto            267
        //   281: astore_0       
        //   282: getstatic       com/navdy/service/library/util/LogUtils.sLogger:Lcom/navdy/service/library/log/Logger;
        //   285: aload_0        
        //   286: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   289: goto            221
        //   292: astore          8
        //   294: aload           7
        //   296: astore          4
        //   298: aload           6
        //   300: astore_3       
        //   301: aload           5
        //   303: astore_1       
        //   304: goto            190
        //   307: astore          8
        //   309: aload           7
        //   311: astore          4
        //   313: goto            190
        //    Exceptions:
        //  throws java.lang.Throwable
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  5      27     224    229    Any
        //  44     61     224    229    Any
        //  61     73     292    307    Any
        //  79     111    307    316    Any
        //  117    124    307    316    Any
        //  130    135    307    316    Any
        //  141    148    307    316    Any
        //  148    157    174    190    Any
        //  162    171    174    190    Any
        //  190    213    224    229    Any
        //  217    221    281    292    Ljava/lang/Throwable;
        //  217    221    224    229    Any
        //  221    224    224    229    Any
        //  229    233    174    190    Any
        //  233    257    224    229    Any
        //  262    267    270    281    Ljava/lang/Throwable;
        //  262    267    224    229    Any
        //  271    278    224    229    Any
        //  282    289    224    229    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 170, Size: 170
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static String getSystemLog() {
        final String string = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + ".logs" + File.separator + "a.log";
        String convertFileToString = null;
        try {
            convertFileToString = IOUtils.convertFileToString(string);
            return convertFileToString;
        }
        catch (IOException ex) {
            Log.i(LogUtils.TAG, "Failed to grab system log");
            return convertFileToString;
        }
    }
    
    public static String systemLogStr(final int n, final String s) {
        String s3;
        final String s2 = s3 = getSystemLog();
        if (s2 != null) {
            final int length = s2.length();
            int min;
            final int n2 = min = length;
            if (s != null) {
                final int length2 = s.length();
                final int lastIndex = s2.lastIndexOf(s);
                min = n2;
                if (lastIndex != -1) {
                    min = Math.min(lastIndex + length2 + 10, length);
                }
            }
            s3 = s2.substring(Math.max(min - n, 0), min);
        }
        return s3;
    }
    
    private static void writeCrashInfo(final FileOutputStream fileOutputStream, final Throwable t, final String s, Thread thread, final boolean b) throws Throwable {
        final Runtime runtime = Runtime.getRuntime();
        final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        bufferedOutputStream.write(("process: " + s).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("time: " + Calendar.getInstance().getTime().toString()).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("serial: " + Build.SERIAL).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("device: " + Build.DEVICE).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("model: " + Build.MODEL).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("hw: " + Build.HARDWARE).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("id: " + Build.ID).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("version: " + Build$VERSION.SDK_INT).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("bootloader: " + Build.BOOTLOADER).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("board: " + Build.BOARD).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("brand: " + Build.BRAND).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("manufacturer: " + Build.MANUFACTURER).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("cpu_abi: " + Build.CPU_ABI).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("cpu_abi2: " + Build.CPU_ABI2).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("processors: " + runtime.availableProcessors()).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(LogUtils.CRLF);
        if (b) {
            bufferedOutputStream.write("------ native crash -----".getBytes());
        }
        else {
            bufferedOutputStream.write("------ crash -----".getBytes());
        }
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("thread name crashed:" + thread.getName()).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(LogUtils.CRLF);
        final StringWriter stringWriter = new StringWriter();
        final FastPrintWriter fastPrintWriter = new FastPrintWriter(stringWriter, false, 256);
        t.printStackTrace(fastPrintWriter);
        fastPrintWriter.flush();
        bufferedOutputStream.write(stringWriter.toString().getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write("------ state -----".getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("total:" + runtime.totalMemory()).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("free:" + runtime.freeMemory()).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("max:" + runtime.maxMemory()).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("native heap size: " + Debug.getNativeHeapSize()).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("native heap allocated: " + Debug.getNativeHeapAllocatedSize()).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(("native heap free: " + Debug.getNativeHeapFreeSize()).getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(LogUtils.CRLF);
        final Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
        bufferedOutputStream.write(("------ thread stackstraces count = " + allStackTraces.size() + " -----").getBytes());
        bufferedOutputStream.write(LogUtils.CRLF);
        bufferedOutputStream.write(LogUtils.CRLF);
        for (final Map.Entry<Thread, StackTraceElement[]> entry : allStackTraces.entrySet()) {
            thread = entry.getKey();
            bufferedOutputStream.write(("\"" + thread.getName() + "\"").getBytes());
            bufferedOutputStream.write(LogUtils.CRLF);
            bufferedOutputStream.write(("    Thread.State: " + thread.getState()).getBytes());
            bufferedOutputStream.write(LogUtils.CRLF);
            final StackTraceElement[] array = entry.getValue();
            for (int length = array.length, i = 0; i < length; ++i) {
                bufferedOutputStream.write(("       at " + array[i]).getBytes());
                bufferedOutputStream.write(LogUtils.CRLF);
            }
            bufferedOutputStream.write(LogUtils.CRLF);
            bufferedOutputStream.write(LogUtils.CRLF);
        }
        bufferedOutputStream.flush();
    }
    
    public static void zipStaging(final Context p0, final String p1, final String p2) throws Throwable {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: new             Ljava/lang/StringBuilder;
        //     6: dup            
        //     7: invokespecial   java/lang/StringBuilder.<init>:()V
        //    10: ldc_w           "zip started:"
        //    13: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    16: aload_1        
        //    17: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    20: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    23: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //    26: pop            
        //    27: aconst_null    
        //    28: astore_3       
        //    29: aconst_null    
        //    30: astore          4
        //    32: aconst_null    
        //    33: astore          5
        //    35: aconst_null    
        //    36: astore          6
        //    38: aconst_null    
        //    39: astore          7
        //    41: aconst_null    
        //    42: astore          8
        //    44: aconst_null    
        //    45: astore          9
        //    47: aconst_null    
        //    48: astore          10
        //    50: aconst_null    
        //    51: astore          11
        //    53: aconst_null    
        //    54: astore          12
        //    56: aconst_null    
        //    57: astore          13
        //    59: aload           6
        //    61: astore          14
        //    63: aload_3        
        //    64: astore          15
        //    66: aload           11
        //    68: astore          16
        //    70: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //    73: lstore          17
        //    75: aload           6
        //    77: astore          14
        //    79: aload_3        
        //    80: astore          15
        //    82: aload           11
        //    84: astore          16
        //    86: sipush          16384
        //    89: newarray        B
        //    91: astore          19
        //    93: aload           6
        //    95: astore          14
        //    97: aload_3        
        //    98: astore          15
        //   100: aload           11
        //   102: astore          16
        //   104: new             Ljava/io/FileOutputStream;
        //   107: astore          20
        //   109: aload           6
        //   111: astore          14
        //   113: aload_3        
        //   114: astore          15
        //   116: aload           11
        //   118: astore          16
        //   120: aload           20
        //   122: aload_1        
        //   123: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   126: new             Ljava/util/zip/ZipOutputStream;
        //   129: astore          15
        //   131: aload           15
        //   133: aload           20
        //   135: invokespecial   java/util/zip/ZipOutputStream.<init>:(Ljava/io/OutputStream;)V
        //   138: aload           10
        //   140: astore          5
        //   142: aload           9
        //   144: astore          13
        //   146: new             Ljava/io/File;
        //   149: astore          14
        //   151: aload           10
        //   153: astore          5
        //   155: aload           9
        //   157: astore          13
        //   159: aload           14
        //   161: aload_2        
        //   162: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   165: aload           10
        //   167: astore          5
        //   169: aload           9
        //   171: astore          13
        //   173: aload           14
        //   175: invokevirtual   java/io/File.listFiles:()[Ljava/io/File;
        //   178: astore          14
        //   180: aload           8
        //   182: astore_2       
        //   183: aload           14
        //   185: ifnull          432
        //   188: iconst_0       
        //   189: istore          21
        //   191: iload           21
        //   193: aload           14
        //   195: arraylength    
        //   196: if_icmpge       430
        //   199: new             Ljava/io/FileInputStream;
        //   202: astore_2       
        //   203: aload_2        
        //   204: aload           14
        //   206: iload           21
        //   208: aaload         
        //   209: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   212: aload_2        
        //   213: astore          5
        //   215: aload_2        
        //   216: astore          13
        //   218: new             Ljava/util/zip/ZipEntry;
        //   221: astore          16
        //   223: aload_2        
        //   224: astore          5
        //   226: aload_2        
        //   227: astore          13
        //   229: aload           16
        //   231: aload           14
        //   233: iload           21
        //   235: aaload         
        //   236: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //   239: invokespecial   java/util/zip/ZipEntry.<init>:(Ljava/lang/String;)V
        //   242: aload_2        
        //   243: astore          5
        //   245: aload_2        
        //   246: astore          13
        //   248: aload           15
        //   250: aload           16
        //   252: invokevirtual   java/util/zip/ZipOutputStream.putNextEntry:(Ljava/util/zip/ZipEntry;)V
        //   255: aload_2        
        //   256: astore          5
        //   258: aload_2        
        //   259: astore          13
        //   261: aload_2        
        //   262: aload           19
        //   264: invokevirtual   java/io/FileInputStream.read:([B)I
        //   267: istore          22
        //   269: iload           22
        //   271: ifle            403
        //   274: aload_2        
        //   275: astore          5
        //   277: aload_2        
        //   278: astore          13
        //   280: aload           15
        //   282: aload           19
        //   284: iconst_0       
        //   285: iload           22
        //   287: invokevirtual   java/util/zip/ZipOutputStream.write:([BII)V
        //   290: goto            255
        //   293: astore_2       
        //   294: aload           15
        //   296: astore          13
        //   298: aload           20
        //   300: astore          15
        //   302: aload_2        
        //   303: astore          20
        //   305: aload           15
        //   307: astore_2       
        //   308: aload           5
        //   310: astore          14
        //   312: aload_2        
        //   313: astore          15
        //   315: aload           13
        //   317: astore          16
        //   319: aload_2        
        //   320: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   323: aconst_null    
        //   324: astore_2       
        //   325: aload           5
        //   327: astore          14
        //   329: aload_2        
        //   330: astore          15
        //   332: aload           13
        //   334: astore          16
        //   336: aload_0        
        //   337: aload_1        
        //   338: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //   341: pop            
        //   342: aload           5
        //   344: astore          14
        //   346: aload_2        
        //   347: astore          15
        //   349: aload           13
        //   351: astore          16
        //   353: getstatic       com/navdy/service/library/util/LogUtils.TAG:Ljava/lang/String;
        //   356: ldc_w           "zip error"
        //   359: aload           20
        //   361: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   364: pop            
        //   365: aload           5
        //   367: astore          14
        //   369: aload_2        
        //   370: astore          15
        //   372: aload           13
        //   374: astore          16
        //   376: aload           20
        //   378: athrow         
        //   379: astore_1       
        //   380: aload           16
        //   382: astore_0       
        //   383: aload           14
        //   385: astore          13
        //   387: aload_0        
        //   388: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   391: aload           13
        //   393: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   396: aload           15
        //   398: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   401: aload_1        
        //   402: athrow         
        //   403: aload_2        
        //   404: astore          5
        //   406: aload_2        
        //   407: astore          13
        //   409: aload           15
        //   411: invokevirtual   java/util/zip/ZipOutputStream.closeEntry:()V
        //   414: aload_2        
        //   415: astore          5
        //   417: aload_2        
        //   418: astore          13
        //   420: aload_2        
        //   421: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   424: iinc            21, 1
        //   427: goto            191
        //   430: aconst_null    
        //   431: astore_2       
        //   432: aload_2        
        //   433: astore          5
        //   435: aload_2        
        //   436: astore          13
        //   438: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //   441: lstore          23
        //   443: aload_2        
        //   444: astore          5
        //   446: aload_2        
        //   447: astore          13
        //   449: getstatic       com/navdy/service/library/util/LogUtils.TAG:Ljava/lang/String;
        //   452: astore          14
        //   454: aload_2        
        //   455: astore          5
        //   457: aload_2        
        //   458: astore          13
        //   460: new             Ljava/lang/StringBuilder;
        //   463: astore          16
        //   465: aload_2        
        //   466: astore          5
        //   468: aload_2        
        //   469: astore          13
        //   471: aload           16
        //   473: invokespecial   java/lang/StringBuilder.<init>:()V
        //   476: aload_2        
        //   477: astore          5
        //   479: aload_2        
        //   480: astore          13
        //   482: aload           14
        //   484: aload           16
        //   486: ldc_w           "zip finished:"
        //   489: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   492: aload_1        
        //   493: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   496: ldc_w           " time:"
        //   499: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   502: lload           23
        //   504: lload           17
        //   506: lsub           
        //   507: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   510: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   513: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //   516: pop            
        //   517: aload           15
        //   519: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   522: aload_2        
        //   523: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   526: aload           20
        //   528: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   531: return         
        //   532: astore_1       
        //   533: aload           7
        //   535: astore          13
        //   537: aload           20
        //   539: astore          15
        //   541: aload           12
        //   543: astore_0       
        //   544: goto            387
        //   547: astore_1       
        //   548: aload           15
        //   550: astore_0       
        //   551: aload           20
        //   553: astore          15
        //   555: goto            387
        //   558: astore_1       
        //   559: aload           15
        //   561: astore_0       
        //   562: aconst_null    
        //   563: astore          13
        //   565: aload           20
        //   567: astore          15
        //   569: goto            387
        //   572: astore          20
        //   574: aload           4
        //   576: astore_2       
        //   577: goto            308
        //   580: astore          15
        //   582: aload           20
        //   584: astore_2       
        //   585: aload           15
        //   587: astore          20
        //   589: goto            308
        //   592: astore          13
        //   594: aconst_null    
        //   595: astore          5
        //   597: aload           20
        //   599: astore_2       
        //   600: aload           13
        //   602: astore          20
        //   604: aload           15
        //   606: astore          13
        //   608: goto            308
        //    Exceptions:
        //  throws java.lang.Throwable
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  70     75     572    580    Ljava/lang/Throwable;
        //  70     75     379    387    Any
        //  86     93     572    580    Ljava/lang/Throwable;
        //  86     93     379    387    Any
        //  104    109    572    580    Ljava/lang/Throwable;
        //  104    109    379    387    Any
        //  120    126    572    580    Ljava/lang/Throwable;
        //  120    126    379    387    Any
        //  126    138    580    592    Ljava/lang/Throwable;
        //  126    138    532    547    Any
        //  146    151    293    308    Ljava/lang/Throwable;
        //  146    151    547    558    Any
        //  159    165    293    308    Ljava/lang/Throwable;
        //  159    165    547    558    Any
        //  173    180    293    308    Ljava/lang/Throwable;
        //  173    180    547    558    Any
        //  191    212    592    611    Ljava/lang/Throwable;
        //  191    212    558    572    Any
        //  218    223    293    308    Ljava/lang/Throwable;
        //  218    223    547    558    Any
        //  229    242    293    308    Ljava/lang/Throwable;
        //  229    242    547    558    Any
        //  248    255    293    308    Ljava/lang/Throwable;
        //  248    255    547    558    Any
        //  261    269    293    308    Ljava/lang/Throwable;
        //  261    269    547    558    Any
        //  280    290    293    308    Ljava/lang/Throwable;
        //  280    290    547    558    Any
        //  319    323    379    387    Any
        //  336    342    379    387    Any
        //  353    365    379    387    Any
        //  376    379    379    387    Any
        //  409    414    293    308    Ljava/lang/Throwable;
        //  409    414    547    558    Any
        //  420    424    293    308    Ljava/lang/Throwable;
        //  420    424    547    558    Any
        //  438    443    293    308    Ljava/lang/Throwable;
        //  438    443    547    558    Any
        //  449    454    293    308    Ljava/lang/Throwable;
        //  449    454    547    558    Any
        //  460    465    293    308    Ljava/lang/Throwable;
        //  460    465    547    558    Any
        //  471    476    293    308    Ljava/lang/Throwable;
        //  471    476    547    558    Any
        //  482    517    293    308    Ljava/lang/Throwable;
        //  482    517    547    558    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 321, Size: 321
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static class RotatingLogFile
    {
        int maxFiles;
        String name;
        
        RotatingLogFile(final String name, final int maxFiles) {
            this.name = name;
            this.maxFiles = maxFiles;
        }
    }
}
