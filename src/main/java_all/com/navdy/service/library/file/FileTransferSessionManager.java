package com.navdy.service.library.file;

import com.squareup.wire.Wire;
import android.text.TextUtils;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferStatus;
import okio.ByteString;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileType;
import java.util.Iterator;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashMap;
import android.content.Context;
import com.navdy.service.library.log.Logger;

public class FileTransferSessionManager implements IFileTransferManager
{
    private static final int MAX_SESSIONS = 5;
    private static final int SESSION_TIMEOUT_INTERVAL = 60000;
    private static final Logger sLogger;
    private Context mContext;
    private IFileTransferAuthority mFileTransferAuthority;
    private HashMap<Integer, FileTransferSession> mFileTransferSessionsIdIndexed;
    private HashMap<String, FileTransferSession> mFileTransferSessionsPathIndexed;
    private AtomicInteger mTransferId;
    
    static {
        sLogger = new Logger(FileTransferSessionManager.class);
    }
    
    public FileTransferSessionManager(final Context mContext, final IFileTransferAuthority mFileTransferAuthority) {
        this.mTransferId = new AtomicInteger(0);
        this.mContext = mContext;
        this.mFileTransferAuthority = mFileTransferAuthority;
        this.mFileTransferSessionsPathIndexed = new HashMap<String, FileTransferSession>();
        this.mFileTransferSessionsIdIndexed = new HashMap<Integer, FileTransferSession>();
    }
    
    public static final String absolutePath(final Context context, String filesDir, final String s) {
        final StringBuilder sb = new StringBuilder();
        if (filesDir == null) {
            filesDir = (String)context.getFilesDir();
        }
        return sb.append(filesDir).append(File.separator).append(s).toString();
    }
    
    private void endFileTransferSession(final FileTransferSession fileTransferSession, final boolean b) {
        if (fileTransferSession == null) {
            return;
        }
        synchronized (fileTransferSession) {
            final String mFileName = fileTransferSession.mFileName;
            final String mDestinationFolder = fileTransferSession.mDestinationFolder;
            // monitorexit(fileTransferSession)
            this.mFileTransferSessionsPathIndexed.remove(absolutePath(this.mContext, mDestinationFolder, mFileName));
            this.mFileTransferSessionsIdIndexed.remove(fileTransferSession.mTransferId);
            fileTransferSession.endSession(this.mContext, b);
        }
    }
    
    private int endSessions(final boolean b) {
        final long currentTimeMillis = System.currentTimeMillis();
        int n = 0;
        final Iterator<Integer> iterator = this.mFileTransferSessionsIdIndexed.keySet().iterator();
        while (iterator.hasNext()) {
            final FileTransferSession fileTransferSession = this.mFileTransferSessionsIdIndexed.get(iterator.next());
            if (!b || (fileTransferSession != null && currentTimeMillis - fileTransferSession.getLastActivity() > 60000L)) {
                iterator.remove();
                this.endFileTransferSession(fileTransferSession, false);
                ++n;
            }
        }
        return n;
    }
    
    public static boolean isPullRequest(final FileType fileType) {
        boolean b = false;
        switch (fileType) {
            default:
                b = false;
                break;
            case FILE_TYPE_LOGS:
                b = true;
                break;
        }
        return b;
    }
    
    public static FileTransferData prepareFileTransferData(final int n, final TransferDataSource transferDataSource, final int n2, final int n3, long n4, final long n5) throws Throwable {
        try {
            n4 = transferDataSource.length() - n5;
            int n6 = n3;
            boolean b = false;
            if (n4 < n3) {
                n6 = (int)n4;
                b = true;
            }
            final byte[] array = new byte[n6];
            transferDataSource.seek(n5);
            transferDataSource.read(array);
            final FileTransferData.Builder chunkIndex = new FileTransferData.Builder().transferId(n).chunkIndex(n2);
            if (b) {
                chunkIndex.fileCheckSum(transferDataSource.checkSum());
            }
            return chunkIndex.dataBytes(ByteString.of(array)).lastChunk(b).build();
        }
        catch (Throwable t) {
            FileTransferSessionManager.sLogger.e("Exception while preparing the chunk data " + t);
            throw t;
        }
    }
    
    public String absolutePathForTransferId(final int n) {
        final FileTransferSession fileTransferSession = this.mFileTransferSessionsIdIndexed.get(n);
        if (fileTransferSession == null) {
            return null;
        }
        synchronized (fileTransferSession) {
            final String mFileName = fileTransferSession.mFileName;
            final String mDestinationFolder = fileTransferSession.mDestinationFolder;
            // monitorexit(fileTransferSession)
            return absolutePath(this.mContext, mDestinationFolder, mFileName);
        }
        return null;
    }
    
    public void endFileTransferSession(final int n, final boolean b) {
        final FileTransferSession fileTransferSession = this.mFileTransferSessionsIdIndexed.get(n);
        if (fileTransferSession != null) {
            this.endFileTransferSession(fileTransferSession, b);
        }
    }
    
    @Override
    public FileType getFileType(final int n) {
        final FileTransferSession fileTransferSession = this.mFileTransferSessionsIdIndexed.get(n);
        FileType fileType;
        if (fileTransferSession != null) {
            fileType = fileTransferSession.getFileType();
        }
        else {
            fileType = null;
        }
        return fileType;
    }
    
    @Override
    public FileTransferData getNextChunk(final int n) throws Throwable {
        final FileTransferSession fileTransferSession = this.mFileTransferSessionsIdIndexed.get(n);
        FileTransferData fileTransferData;
        if (fileTransferSession != null) {
            final FileTransferData nextChunk = fileTransferSession.getNextChunk();
            if (nextChunk != null) {
                fileTransferData = nextChunk;
                if (!nextChunk.lastChunk) {
                    return fileTransferData;
                }
                fileTransferData = nextChunk;
                if (fileTransferSession.isFlowControlEnabled()) {
                    return fileTransferData;
                }
            }
            this.endFileTransferSession(fileTransferSession, true);
            fileTransferData = nextChunk;
        }
        else {
            fileTransferData = null;
        }
        return fileTransferData;
    }
    
    @Override
    public FileTransferStatus handleFileTransferData(final FileTransferData fileTransferData) {
        final FileTransferSession fileTransferSession = this.mFileTransferSessionsIdIndexed.get(fileTransferData.transferId);
        FileTransferStatus build;
        if (fileTransferSession != null) {
            final FileTransferStatus appendChunk = fileTransferSession.appendChunk(this.mContext, fileTransferData);
            if (!appendChunk.transferComplete) {
                build = appendChunk;
                if (appendChunk.error == null) {
                    return build;
                }
                build = appendChunk;
                if (appendChunk.error == FileTransferError.FILE_TRANSFER_NO_ERROR) {
                    return build;
                }
            }
            this.endFileTransferSession(fileTransferSession, false);
            build = appendChunk;
        }
        else {
            build = new FileTransferStatus.Builder().success(false).transferComplete(false).error(FileTransferError.FILE_TRANSFER_NOT_INITIATED).build();
        }
        return build;
    }
    
    @Override
    public FileTransferResponse handleFileTransferRequest(final FileTransferRequest fileTransferRequest) {
        FileTransferResponse fileTransferResponse;
        if (fileTransferRequest == null) {
            fileTransferResponse = null;
        }
        else {
            Label_0204: {
                try {
                    if (!this.mFileTransferAuthority.isFileTypeAllowed(fileTransferRequest.fileType)) {
                        fileTransferResponse = new FileTransferResponse.Builder().success(false).destinationFileName(fileTransferRequest.destinationFileName).fileType(fileTransferRequest.fileType).error(FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).build();
                    }
                    else {
                        if (fileTransferRequest.fileType != FileType.FILE_TYPE_PERF_TEST) {
                            break Label_0204;
                        }
                        final FileTransferSession fileTransferSession = new FileTransferSession(this.mTransferId.incrementAndGet());
                        final FileTransferResponse fileTransferResponse2 = fileTransferResponse = fileTransferSession.initTestData(this.mContext, fileTransferRequest.fileType, "<TESTDATA>", fileTransferRequest.fileSize);
                        if (fileTransferResponse2.success) {
                            this.mFileTransferSessionsIdIndexed.put(fileTransferSession.mTransferId, fileTransferSession);
                            fileTransferResponse = fileTransferResponse2;
                        }
                    }
                }
                catch (Throwable t) {
                    FileTransferSessionManager.sLogger.e("Exception in handle file request ", t);
                    fileTransferResponse = new FileTransferResponse.Builder().success(false).destinationFileName(fileTransferRequest.destinationFileName).fileType(fileTransferRequest.fileType).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                }
                return fileTransferResponse;
            }
            if (!isPullRequest(fileTransferRequest.fileType)) {
                final String directoryForFileType = this.mFileTransferAuthority.getDirectoryForFileType(fileTransferRequest.fileType);
                final String absolutePath = absolutePath(this.mContext, directoryForFileType, fileTransferRequest.destinationFileName);
                final FileTransferSession fileTransferSession2 = this.mFileTransferSessionsPathIndexed.get(absolutePath);
                if (fileTransferSession2 == null && this.mFileTransferSessionsPathIndexed.size() == 5 && this.endSessions(true) == 0) {
                    fileTransferResponse = new FileTransferResponse.Builder().success(false).destinationFileName(fileTransferRequest.destinationFileName).fileType(fileTransferRequest.fileType).error(FileTransferError.FILE_TRANSFER_HOST_BUSY).build();
                }
                else {
                    this.endFileTransferSession(fileTransferSession2, false);
                    final FileTransferSession fileTransferSession3 = new FileTransferSession(this.mTransferId.incrementAndGet());
                    final FileTransferResponse fileTransferResponse3 = fileTransferResponse = fileTransferSession3.initFileTransfer(this.mContext, fileTransferRequest.fileType, directoryForFileType, fileTransferRequest.destinationFileName, fileTransferRequest.fileSize, fileTransferRequest.offset, fileTransferRequest.fileDataChecksum, Boolean.TRUE.equals(fileTransferRequest.override));
                    if (fileTransferResponse3.success) {
                        this.mFileTransferSessionsIdIndexed.put(fileTransferSession3.mTransferId, fileTransferSession3);
                        this.mFileTransferSessionsPathIndexed.put(absolutePath, fileTransferSession3);
                        fileTransferResponse = fileTransferResponse3;
                    }
                }
            }
            else {
                final FileTransferSession fileTransferSession4 = new FileTransferSession(this.mTransferId.incrementAndGet());
                final String directoryForFileType2 = this.mFileTransferAuthority.getDirectoryForFileType(fileTransferRequest.fileType);
                final String fileToSend = this.mFileTransferAuthority.getFileToSend(fileTransferRequest.fileType);
                if (!TextUtils.isEmpty((CharSequence)fileToSend)) {
                    final File file = new File(fileToSend);
                    if (!file.exists()) {
                        fileTransferResponse = new FileTransferResponse.Builder().success(false).destinationFileName(fileTransferRequest.destinationFileName).fileType(fileTransferRequest.fileType).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                    }
                    else {
                        final FileTransferResponse fileTransferResponse4 = fileTransferResponse = fileTransferSession4.initPull(this.mContext, fileTransferRequest.fileType, directoryForFileType2, file.getName(), Wire.<Boolean>get(fileTransferRequest.supportsAcks, FileTransferRequest.DEFAULT_SUPPORTSACKS));
                        if (fileTransferResponse4.success) {
                            this.mFileTransferSessionsIdIndexed.put(fileTransferSession4.mTransferId, fileTransferSession4);
                            fileTransferResponse = fileTransferResponse4;
                        }
                    }
                }
                else {
                    fileTransferResponse = new FileTransferResponse.Builder().success(false).destinationFileName(fileTransferRequest.destinationFileName).fileType(fileTransferRequest.fileType).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                }
            }
        }
        return fileTransferResponse;
    }
    
    @Override
    public boolean handleFileTransferStatus(final FileTransferStatus fileTransferStatus) {
        boolean handleFileTransferStatus = false;
        if (fileTransferStatus != null) {
            final FileTransferSession fileTransferSession = this.mFileTransferSessionsIdIndexed.get(fileTransferStatus.transferId);
            if (fileTransferSession != null) {
                handleFileTransferStatus = fileTransferSession.handleFileTransferStatus(fileTransferStatus);
            }
        }
        return handleFileTransferStatus;
    }
    
    @Override
    public void stop() {
        this.endSessions(false);
    }
}
