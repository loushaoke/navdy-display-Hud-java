package com.navdy.service.library.network;

import java.io.IOException;
import com.navdy.service.library.device.connection.ServiceAddress;

public class TCPSocketFactory implements SocketFactory
{
    private final String host;
    private final int port;
    
    public TCPSocketFactory(final ServiceAddress serviceAddress) {
        this(serviceAddress.getAddress(), Integer.parseInt(serviceAddress.getService()));
    }
    
    public TCPSocketFactory(final String host, final int port) {
        this.host = host;
        this.port = port;
    }
    
    @Override
    public SocketAdapter build() throws IOException {
        return new TCPSocketAdapter(this.host, this.port);
    }
}
