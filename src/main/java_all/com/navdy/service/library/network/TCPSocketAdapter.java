package com.navdy.service.library.network;

import com.navdy.service.library.device.NavdyDeviceId;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.net.Socket;

public class TCPSocketAdapter implements SocketAdapter
{
    private final String host;
    private final int port;
    private Socket socket;
    
    public TCPSocketAdapter(final String host, final int port) {
        if (host == null) {
            throw new IllegalArgumentException("Host must not be null");
        }
        this.host = host;
        this.port = port;
    }
    
    public TCPSocketAdapter(final Socket socket) {
        this.host = null;
        this.port = -1;
        this.socket = socket;
    }
    
    @Override
    public void close() throws IOException {
        if (this.socket != null) {
            this.socket.close();
        }
        this.socket = null;
    }
    
    @Override
    public void connect() throws IOException {
        if (this.host == null) {
            throw new IllegalStateException("Can't connect with accepted socket");
        }
        this.socket = new Socket(this.host, this.port);
    }
    
    @Override
    public InputStream getInputStream() throws IOException {
        return this.socket.getInputStream();
    }
    
    @Override
    public OutputStream getOutputStream() throws IOException {
        return this.socket.getOutputStream();
    }
    
    public String getRemoteAddress() {
        String hostAddress;
        if (this.isConnected()) {
            hostAddress = this.socket.getInetAddress().getHostAddress();
        }
        else {
            hostAddress = null;
        }
        return hostAddress;
    }
    
    @Override
    public NavdyDeviceId getRemoteDevice() {
        NavdyDeviceId unknown_ID;
        if (this.isConnected()) {
            unknown_ID = NavdyDeviceId.UNKNOWN_ID;
        }
        else {
            unknown_ID = null;
        }
        return unknown_ID;
    }
    
    @Override
    public boolean isConnected() {
        return this.socket != null && this.socket.isConnected();
    }
}
