package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class VoiceAssistRequest extends Message
{
    public static final Boolean DEFAULT_END;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean end;
    
    static {
        DEFAULT_END = false;
    }
    
    private VoiceAssistRequest(final Builder builder) {
        this(builder.end);
        this.setBuilder((Message.Builder)builder);
    }
    
    public VoiceAssistRequest(final Boolean end) {
        this.end = end;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof VoiceAssistRequest && this.equals(this.end, ((VoiceAssistRequest)o).end));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.end != null) {
                hashCode = this.end.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<VoiceAssistRequest>
    {
        public Boolean end;
        
        public Builder() {
        }
        
        public Builder(final VoiceAssistRequest voiceAssistRequest) {
            super(voiceAssistRequest);
            if (voiceAssistRequest != null) {
                this.end = voiceAssistRequest.end;
            }
        }
        
        public VoiceAssistRequest build() {
            return new VoiceAssistRequest(this, null);
        }
        
        public Builder end(final Boolean end) {
            this.end = end;
            return this;
        }
    }
}
