package com.navdy.service.library.events.places;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import java.util.List;
import com.squareup.wire.Message;

public final class AutoCompleteResponse extends Message
{
    public static final String DEFAULT_PARTIALSEARCH = "";
    public static final List<String> DEFAULT_RESULTS;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String partialSearch;
    @ProtoField(label = Label.REPEATED, tag = 4, type = Datatype.STRING)
    public final List<String> results;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_RESULTS = Collections.<String>emptyList();
    }
    
    private AutoCompleteResponse(final Builder builder) {
        this(builder.partialSearch, builder.status, builder.statusDetail, builder.results);
        this.setBuilder((Message.Builder)builder);
    }
    
    public AutoCompleteResponse(final String partialSearch, final RequestStatus status, final String statusDetail, final List<String> list) {
        this.partialSearch = partialSearch;
        this.status = status;
        this.statusDetail = statusDetail;
        this.results = Message.<String>immutableCopyOf(list);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof AutoCompleteResponse)) {
                b = false;
            }
            else {
                final AutoCompleteResponse autoCompleteResponse = (AutoCompleteResponse)o;
                if (!this.equals(this.partialSearch, autoCompleteResponse.partialSearch) || !this.equals(this.status, autoCompleteResponse.status) || !this.equals(this.statusDetail, autoCompleteResponse.statusDetail) || !this.equals(this.results, autoCompleteResponse.results)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.partialSearch != null) {
                hashCode3 = this.partialSearch.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.status != null) {
                hashCode4 = this.status.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.statusDetail != null) {
                hashCode = this.statusDetail.hashCode();
            }
            int hashCode5;
            if (this.results != null) {
                hashCode5 = this.results.hashCode();
            }
            else {
                hashCode5 = 1;
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode) * 37 + hashCode5;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<AutoCompleteResponse>
    {
        public String partialSearch;
        public List<String> results;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final AutoCompleteResponse autoCompleteResponse) {
            super(autoCompleteResponse);
            if (autoCompleteResponse != null) {
                this.partialSearch = autoCompleteResponse.partialSearch;
                this.status = autoCompleteResponse.status;
                this.statusDetail = autoCompleteResponse.statusDetail;
                this.results = (List<String>)Message.<Object>copyOf((List<Object>)autoCompleteResponse.results);
            }
        }
        
        public AutoCompleteResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new AutoCompleteResponse(this, null);
        }
        
        public Builder partialSearch(final String partialSearch) {
            this.partialSearch = partialSearch;
            return this;
        }
        
        public Builder results(final List<String> list) {
            this.results = Message.Builder.<String>checkForNulls(list);
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
