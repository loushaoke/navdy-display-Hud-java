package com.navdy.service.library.events.preferences;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class DisplaySpeakerPreferences extends Message
{
    public static final Boolean DEFAULT_FEEDBACK_SOUND;
    public static final Boolean DEFAULT_MASTER_SOUND;
    public static final Boolean DEFAULT_NOTIFICATION_SOUND;
    public static final Long DEFAULT_SERIAL_NUMBER;
    public static final Integer DEFAULT_VOLUME_LEVEL;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean feedback_sound;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean master_sound;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean notification_sound;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer volume_level;
    
    static {
        DEFAULT_SERIAL_NUMBER = 0L;
        DEFAULT_MASTER_SOUND = true;
        DEFAULT_VOLUME_LEVEL = 50;
        DEFAULT_FEEDBACK_SOUND = true;
        DEFAULT_NOTIFICATION_SOUND = true;
    }
    
    private DisplaySpeakerPreferences(final Builder builder) {
        this(builder.serial_number, builder.master_sound, builder.volume_level, builder.feedback_sound, builder.notification_sound);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DisplaySpeakerPreferences(final Long serial_number, final Boolean master_sound, final Integer volume_level, final Boolean feedback_sound, final Boolean notification_sound) {
        this.serial_number = serial_number;
        this.master_sound = master_sound;
        this.volume_level = volume_level;
        this.feedback_sound = feedback_sound;
        this.notification_sound = notification_sound;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof DisplaySpeakerPreferences)) {
                b = false;
            }
            else {
                final DisplaySpeakerPreferences displaySpeakerPreferences = (DisplaySpeakerPreferences)o;
                if (!this.equals(this.serial_number, displaySpeakerPreferences.serial_number) || !this.equals(this.master_sound, displaySpeakerPreferences.master_sound) || !this.equals(this.volume_level, displaySpeakerPreferences.volume_level) || !this.equals(this.feedback_sound, displaySpeakerPreferences.feedback_sound) || !this.equals(this.notification_sound, displaySpeakerPreferences.notification_sound)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.serial_number != null) {
                hashCode3 = this.serial_number.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.master_sound != null) {
                hashCode4 = this.master_sound.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.volume_level != null) {
                hashCode5 = this.volume_level.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.feedback_sound != null) {
                hashCode6 = this.feedback_sound.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.notification_sound != null) {
                hashCode = this.notification_sound.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<DisplaySpeakerPreferences>
    {
        public Boolean feedback_sound;
        public Boolean master_sound;
        public Boolean notification_sound;
        public Long serial_number;
        public Integer volume_level;
        
        public Builder() {
        }
        
        public Builder(final DisplaySpeakerPreferences displaySpeakerPreferences) {
            super(displaySpeakerPreferences);
            if (displaySpeakerPreferences != null) {
                this.serial_number = displaySpeakerPreferences.serial_number;
                this.master_sound = displaySpeakerPreferences.master_sound;
                this.volume_level = displaySpeakerPreferences.volume_level;
                this.feedback_sound = displaySpeakerPreferences.feedback_sound;
                this.notification_sound = displaySpeakerPreferences.notification_sound;
            }
        }
        
        public DisplaySpeakerPreferences build() {
            ((Message.Builder)this).checkRequiredFields();
            return new DisplaySpeakerPreferences(this, null);
        }
        
        public Builder feedback_sound(final Boolean feedback_sound) {
            this.feedback_sound = feedback_sound;
            return this;
        }
        
        public Builder master_sound(final Boolean master_sound) {
            this.master_sound = master_sound;
            return this;
        }
        
        public Builder notification_sound(final Boolean notification_sound) {
            this.notification_sound = notification_sound;
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
        
        public Builder volume_level(final Integer volume_level) {
            this.volume_level = volume_level;
            return this;
        }
    }
}
