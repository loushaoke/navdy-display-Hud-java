package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;
import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class GlanceEvent extends Message
{
    public static final List<GlanceActions> DEFAULT_ACTIONS;
    public static final List<KeyValue> DEFAULT_GLANCEDATA;
    public static final GlanceType DEFAULT_GLANCETYPE;
    public static final String DEFAULT_ID = "";
    public static final String DEFAULT_LANGUAGE = "";
    public static final Long DEFAULT_POSTTIME;
    public static final String DEFAULT_PROVIDER = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(enumType = GlanceActions.class, label = Label.REPEATED, tag = 6, type = Datatype.ENUM)
    public final List<GlanceActions> actions;
    @ProtoField(label = Label.REPEATED, messageType = KeyValue.class, tag = 5)
    public final List<KeyValue> glanceData;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final GlanceType glanceType;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String language;
    @ProtoField(tag = 4, type = Datatype.INT64)
    public final Long postTime;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String provider;
    
    static {
        DEFAULT_GLANCETYPE = GlanceType.GLANCE_TYPE_CALENDAR;
        DEFAULT_POSTTIME = 0L;
        DEFAULT_GLANCEDATA = Collections.<KeyValue>emptyList();
        DEFAULT_ACTIONS = Collections.<GlanceActions>emptyList();
    }
    
    private GlanceEvent(final Builder builder) {
        this(builder.glanceType, builder.provider, builder.id, builder.postTime, builder.glanceData, builder.actions, builder.language);
        this.setBuilder((Message.Builder)builder);
    }
    
    public GlanceEvent(final GlanceType glanceType, final String provider, final String id, final Long postTime, final List<KeyValue> list, final List<GlanceActions> list2, final String language) {
        this.glanceType = glanceType;
        this.provider = provider;
        this.id = id;
        this.postTime = postTime;
        this.glanceData = Message.<KeyValue>immutableCopyOf(list);
        this.actions = Message.<GlanceActions>immutableCopyOf(list2);
        this.language = language;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof GlanceEvent)) {
                b = false;
            }
            else {
                final GlanceEvent glanceEvent = (GlanceEvent)o;
                if (!this.equals(this.glanceType, glanceEvent.glanceType) || !this.equals(this.provider, glanceEvent.provider) || !this.equals(this.id, glanceEvent.id) || !this.equals(this.postTime, glanceEvent.postTime) || !this.equals(this.glanceData, glanceEvent.glanceData) || !this.equals(this.actions, glanceEvent.actions) || !this.equals(this.language, glanceEvent.language)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 1;
        int hashCode2 = 0;
        int hashCode3;
        if ((hashCode3 = this.hashCode) == 0) {
            int hashCode4;
            if (this.glanceType != null) {
                hashCode4 = this.glanceType.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.provider != null) {
                hashCode5 = this.provider.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.id != null) {
                hashCode6 = this.id.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.postTime != null) {
                hashCode7 = this.postTime.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.glanceData != null) {
                hashCode8 = this.glanceData.hashCode();
            }
            else {
                hashCode8 = 1;
            }
            if (this.actions != null) {
                hashCode = this.actions.hashCode();
            }
            if (this.language != null) {
                hashCode2 = this.language.hashCode();
            }
            hashCode3 = (((((hashCode4 * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode) * 37 + hashCode2;
            this.hashCode = hashCode3;
        }
        return hashCode3;
    }
    
    public static final class Builder extends Message.Builder<GlanceEvent>
    {
        public List<GlanceActions> actions;
        public List<KeyValue> glanceData;
        public GlanceType glanceType;
        public String id;
        public String language;
        public Long postTime;
        public String provider;
        
        public Builder() {
        }
        
        public Builder(final GlanceEvent glanceEvent) {
            super(glanceEvent);
            if (glanceEvent != null) {
                this.glanceType = glanceEvent.glanceType;
                this.provider = glanceEvent.provider;
                this.id = glanceEvent.id;
                this.postTime = glanceEvent.postTime;
                this.glanceData = (List<KeyValue>)Message.<Object>copyOf((List<Object>)glanceEvent.glanceData);
                this.actions = (List<GlanceActions>)Message.<Object>copyOf((List<Object>)glanceEvent.actions);
                this.language = glanceEvent.language;
            }
        }
        
        public Builder actions(final List<GlanceActions> list) {
            this.actions = Message.Builder.<GlanceActions>checkForNulls(list);
            return this;
        }
        
        public GlanceEvent build() {
            return new GlanceEvent(this, null);
        }
        
        public Builder glanceData(final List<KeyValue> list) {
            this.glanceData = Message.Builder.<KeyValue>checkForNulls(list);
            return this;
        }
        
        public Builder glanceType(final GlanceType glanceType) {
            this.glanceType = glanceType;
            return this;
        }
        
        public Builder id(final String id) {
            this.id = id;
            return this;
        }
        
        public Builder language(final String language) {
            this.language = language;
            return this;
        }
        
        public Builder postTime(final Long postTime) {
            this.postTime = postTime;
            return this;
        }
        
        public Builder provider(final String provider) {
            this.provider = provider;
            return this;
        }
    }
    
    public enum GlanceActions implements ProtoEnum
    {
        REPLY(0);
        
        private final int value;
        
        private GlanceActions(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public enum GlanceType implements ProtoEnum
    {
        GLANCE_TYPE_CALENDAR(0), 
        GLANCE_TYPE_EMAIL(1), 
        GLANCE_TYPE_FUEL(5), 
        GLANCE_TYPE_GENERIC(4), 
        GLANCE_TYPE_MESSAGE(2), 
        GLANCE_TYPE_SOCIAL(3);
        
        private final int value;
        
        private GlanceType(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
