package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;
import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class MusicCapability extends Message
{
    public static final MusicAuthorizationStatus DEFAULT_AUTHORIZATIONSTATUS;
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    public static final List<MusicCollectionType> DEFAULT_COLLECTIONTYPES;
    public static final Long DEFAULT_SERIAL_NUMBER;
    public static final List<MusicRepeatMode> DEFAULT_SUPPORTEDREPEATMODES;
    public static final List<MusicShuffleMode> DEFAULT_SUPPORTEDSHUFFLEMODES;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final MusicAuthorizationStatus authorizationStatus;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(enumType = MusicCollectionType.class, label = Label.REPEATED, tag = 2, type = Datatype.ENUM)
    public final List<MusicCollectionType> collectionTypes;
    @ProtoField(tag = 6, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(enumType = MusicRepeatMode.class, label = Label.REPEATED, tag = 5, type = Datatype.ENUM)
    public final List<MusicRepeatMode> supportedRepeatModes;
    @ProtoField(enumType = MusicShuffleMode.class, label = Label.REPEATED, tag = 4, type = Datatype.ENUM)
    public final List<MusicShuffleMode> supportedShuffleModes;
    
    static {
        DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPES = Collections.<MusicCollectionType>emptyList();
        DEFAULT_AUTHORIZATIONSTATUS = MusicAuthorizationStatus.MUSIC_AUTHORIZATION_NOT_DETERMINED;
        DEFAULT_SUPPORTEDSHUFFLEMODES = Collections.<MusicShuffleMode>emptyList();
        DEFAULT_SUPPORTEDREPEATMODES = Collections.<MusicRepeatMode>emptyList();
        DEFAULT_SERIAL_NUMBER = 0L;
    }
    
    private MusicCapability(final Builder builder) {
        this(builder.collectionSource, builder.collectionTypes, builder.authorizationStatus, builder.supportedShuffleModes, builder.supportedRepeatModes, builder.serial_number);
        this.setBuilder((Message.Builder)builder);
    }
    
    public MusicCapability(final MusicCollectionSource collectionSource, final List<MusicCollectionType> list, final MusicAuthorizationStatus authorizationStatus, final List<MusicShuffleMode> list2, final List<MusicRepeatMode> list3, final Long serial_number) {
        this.collectionSource = collectionSource;
        this.collectionTypes = Message.<MusicCollectionType>immutableCopyOf(list);
        this.authorizationStatus = authorizationStatus;
        this.supportedShuffleModes = Message.<MusicShuffleMode>immutableCopyOf(list2);
        this.supportedRepeatModes = Message.<MusicRepeatMode>immutableCopyOf(list3);
        this.serial_number = serial_number;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicCapability)) {
                b = false;
            }
            else {
                final MusicCapability musicCapability = (MusicCapability)o;
                if (!this.equals(this.collectionSource, musicCapability.collectionSource) || !this.equals(this.collectionTypes, musicCapability.collectionTypes) || !this.equals(this.authorizationStatus, musicCapability.authorizationStatus) || !this.equals(this.supportedShuffleModes, musicCapability.supportedShuffleModes) || !this.equals(this.supportedRepeatModes, musicCapability.supportedRepeatModes) || !this.equals(this.serial_number, musicCapability.serial_number)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 1;
        int hashCode2 = 0;
        int hashCode3;
        if ((hashCode3 = this.hashCode) == 0) {
            int hashCode4;
            if (this.collectionSource != null) {
                hashCode4 = this.collectionSource.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.collectionTypes != null) {
                hashCode5 = this.collectionTypes.hashCode();
            }
            else {
                hashCode5 = 1;
            }
            int hashCode6;
            if (this.authorizationStatus != null) {
                hashCode6 = this.authorizationStatus.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.supportedShuffleModes != null) {
                hashCode7 = this.supportedShuffleModes.hashCode();
            }
            else {
                hashCode7 = 1;
            }
            if (this.supportedRepeatModes != null) {
                hashCode = this.supportedRepeatModes.hashCode();
            }
            if (this.serial_number != null) {
                hashCode2 = this.serial_number.hashCode();
            }
            hashCode3 = ((((hashCode4 * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode) * 37 + hashCode2;
            this.hashCode = hashCode3;
        }
        return hashCode3;
    }
    
    public static final class Builder extends Message.Builder<MusicCapability>
    {
        public MusicAuthorizationStatus authorizationStatus;
        public MusicCollectionSource collectionSource;
        public List<MusicCollectionType> collectionTypes;
        public Long serial_number;
        public List<MusicRepeatMode> supportedRepeatModes;
        public List<MusicShuffleMode> supportedShuffleModes;
        
        public Builder() {
        }
        
        public Builder(final MusicCapability musicCapability) {
            super(musicCapability);
            if (musicCapability != null) {
                this.collectionSource = musicCapability.collectionSource;
                this.collectionTypes = (List<MusicCollectionType>)Message.<Object>copyOf((List<Object>)musicCapability.collectionTypes);
                this.authorizationStatus = musicCapability.authorizationStatus;
                this.supportedShuffleModes = (List<MusicShuffleMode>)Message.<Object>copyOf((List<Object>)musicCapability.supportedShuffleModes);
                this.supportedRepeatModes = (List<MusicRepeatMode>)Message.<Object>copyOf((List<Object>)musicCapability.supportedRepeatModes);
                this.serial_number = musicCapability.serial_number;
            }
        }
        
        public Builder authorizationStatus(final MusicAuthorizationStatus authorizationStatus) {
            this.authorizationStatus = authorizationStatus;
            return this;
        }
        
        public MusicCapability build() {
            return new MusicCapability(this, null);
        }
        
        public Builder collectionSource(final MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }
        
        public Builder collectionTypes(final List<MusicCollectionType> list) {
            this.collectionTypes = Message.Builder.<MusicCollectionType>checkForNulls(list);
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
        
        public Builder supportedRepeatModes(final List<MusicRepeatMode> list) {
            this.supportedRepeatModes = Message.Builder.<MusicRepeatMode>checkForNulls(list);
            return this;
        }
        
        public Builder supportedShuffleModes(final List<MusicShuffleMode> list) {
            this.supportedShuffleModes = Message.Builder.<MusicShuffleMode>checkForNulls(list);
            return this;
        }
    }
    
    public enum MusicAuthorizationStatus implements ProtoEnum
    {
        MUSIC_AUTHORIZATION_AUTHORIZED(2), 
        MUSIC_AUTHORIZATION_DENIED(3), 
        MUSIC_AUTHORIZATION_NOT_DETERMINED(1);
        
        private final int value;
        
        private MusicAuthorizationStatus(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
