package com.navdy.service.library.events;

import com.squareup.wire.Wire;
import java.lang.reflect.Modifier;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.lang.reflect.Field;
import java.util.ArrayList;
import com.squareup.wire.Message;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Extension;
import java.util.HashMap;

public class NavdyEventUtil
{
    private static final int EXTENSION_BASE = 101;
    private static HashMap<Class, Extension> classToExtensionMap;
    private static int minTag;
    private static final Logger sLogger;
    private static Extension[] tagToExtensionMap;
    
    static {
        sLogger = new Logger(NavdyEventUtil.class);
        NavdyEventUtil.classToExtensionMap = new HashMap<Class, Extension>();
        NavdyEventUtil.tagToExtensionMap = cacheExtensions();
    }
    
    public static <T extends Message> T applyDefaults(final T t) {
        return NavdyEventUtil.<T>merge(t, (T)NavdyEventUtil.<T>getDefault(t.getClass()));
    }
    
    private static Extension[] cacheExtensions() {
        final ArrayList<Extension> list = new ArrayList<Extension>();
        NavdyEventUtil.minTag = Integer.MAX_VALUE;
        int n = -1;
        final Field[] declaredFields = Ext_NavdyEvent.class.getDeclaredFields();
        final int length = declaredFields.length;
        int i = 0;
    Label_0141_Outer:
        while (i < length) {
            final Field field = declaredFields[i];
            int n2 = n;
            while (true) {
                if (field.getType().equals(Extension.class)) {
                    int n3 = n;
                    try {
                        final Extension extension = (Extension)field.get(null);
                        n3 = n;
                        NavdyEventUtil.classToExtensionMap.put(extension.getMessageType(), extension);
                        n3 = n;
                        final int tag = extension.getTag();
                        n3 = n;
                        if (tag < NavdyEventUtil.minTag) {
                            n3 = n;
                            NavdyEventUtil.minTag = tag;
                        }
                        if (tag > (n2 = n)) {
                            n2 = tag;
                        }
                        n3 = n2;
                        list.add(extension);
                        ++i;
                        n = n2;
                        continue Label_0141_Outer;
                    }
                    catch (IllegalAccessException ex) {
                        NavdyEventUtil.sLogger.d("Skipping field " + field.getName());
                        n2 = n3;
                        continue;
                    }
                    break;
                }
                continue;
            }
        }
        final Extension[] array = new Extension[n - NavdyEventUtil.minTag + 1];
        for (final Extension extension2 : list) {
            array[extension2.getTag() - NavdyEventUtil.minTag] = extension2;
        }
        return array;
    }
    
    public static NavdyEvent eventFromMessage(final Message message) {
        final Extension extension = findExtension(message);
        if (extension == null) {
            NavdyEventUtil.sLogger.e("Not a valid NavdyEvent protobuf message: " + message);
            throw new IllegalArgumentException("Not a valid NavdyEvent protobuf message: " + message);
        }
        return new NavdyEvent.Builder().type(Message.<NavdyEvent.MessageType>enumFromInt(NavdyEvent.MessageType.class, extension.getTag() - 100)).<Message>setExtension(extension, message).build();
    }
    
    private static Extension findExtension(final NavdyEvent navdyEvent) {
        Extension extension2;
        final Extension extension = extension2 = null;
        if (navdyEvent.type != null) {
            final int n = navdyEvent.type.ordinal() + 101 - NavdyEventUtil.minTag;
            extension2 = extension;
            if (n >= 0) {
                if (n >= NavdyEventUtil.tagToExtensionMap.length) {
                    extension2 = extension;
                }
                else {
                    extension2 = NavdyEventUtil.tagToExtensionMap[n];
                }
            }
        }
        return extension2;
    }
    
    private static Extension findExtension(final Message message) {
        return NavdyEventUtil.classToExtensionMap.get(message.getClass());
    }
    
    private static <T extends Message> Class getBuilder(final Class<T> clazz) throws ClassNotFoundException {
        Class<?> forName;
        if (clazz == null) {
            forName = null;
        }
        else {
            forName = Class.forName(clazz.getName() + "$Builder");
        }
        return forName;
    }
    
    public static <T extends Message> T getDefault(final Class<T> clazz) {
        return NavdyEventUtil.<T>getDefault(clazz, null);
    }
    
    public static <T extends Message> T getDefault(Class<T> o, final Initializer<T> initializer) {
        if (o == null) {
            o = null;
        }
        else {
            Class<T> clazz = null;
            Object o2 = null;
            Class<T> clazz2 = null;
            Object o3 = null;
            Object builder = null;
            final Message.Builder<T> builder2 = null;
            Object o4;
            while (true) {
                try {
                    o4 = (o3 = (clazz2 = (Class<T>)(o2 = (clazz = (Class<T>)(builder = NavdyEventUtil.<Message>getBuilder((Class<Message>)o))))));
                    builder = ((Class<T>)o4).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                    if (o4 == null || builder == null) {
                        o = null;
                        return (T)o;
                    }
                }
                catch (ClassNotFoundException ex) {
                    NavdyEventUtil.sLogger.e(ex.getMessage(), ex);
                    o4 = builder;
                    builder = builder2;
                    continue;
                }
                catch (InvocationTargetException ex2) {
                    NavdyEventUtil.sLogger.e(ex2.getMessage(), ex2);
                    o4 = clazz;
                    builder = builder2;
                    continue;
                }
                catch (NoSuchMethodException ex3) {
                    NavdyEventUtil.sLogger.e(ex3.getMessage(), ex3);
                    o4 = o2;
                    builder = builder2;
                    continue;
                }
                catch (InstantiationException ex4) {
                    NavdyEventUtil.sLogger.e(ex4.getMessage(), ex4);
                    o4 = clazz2;
                    builder = builder2;
                    continue;
                }
                catch (IllegalAccessException ex5) {
                    NavdyEventUtil.sLogger.e(ex5.getMessage(), ex5);
                    o4 = o3;
                    builder = builder2;
                    continue;
                }
                break;
            }
            final Field[] declaredFields = ((Class)o4).getDeclaredFields();
            final int length = declaredFields.length;
            int i = 0;
        Label_0281_Outer:
            while (i < length) {
                final Field field = declaredFields[i];
                final Message message = null;
                while (true) {
                    try {
                        Object o5;
                        if (Message.class.isAssignableFrom(field.getType())) {
                            o5 = NavdyEventUtil.<Message>getDefault(field.getType());
                        }
                        else {
                            o5 = message;
                            if (!Modifier.isFinal(field.getModifiers())) {
                                o5 = message;
                                if (!field.getName().startsWith("$")) {
                                    o5 = ((Class)o).getField("DEFAULT_" + field.getName().toUpperCase()).get(null);
                                }
                            }
                        }
                        if (o5 != null) {
                            field.set(builder, o5);
                        }
                        ++i;
                        continue Label_0281_Outer;
                    }
                    catch (IllegalAccessException ex6) {
                        NavdyEventUtil.sLogger.e(ex6.getMessage(), ex6);
                        continue;
                    }
                    catch (NoSuchFieldException ex7) {
                        NavdyEventUtil.sLogger.e(ex7.getMessage(), ex7);
                        continue;
                    }
                    break;
                }
                break;
            }
            if (initializer != null) {
                o = initializer.build((Message.Builder<T>)builder);
            }
            else {
                o = ((Message.Builder<T>)builder).build();
            }
        }
        return (T)o;
    }
    
    public static <T extends Message> T merge(T o, final T t) {
        Message build;
        if (o == null || t == null) {
            build = null;
        }
        else {
            final Class<?> class1 = o.getClass();
            final Object o2 = null;
            final Object o3 = null;
            final Object o4 = null;
            final Object o5 = null;
            final Object o6 = null;
            final Class<Message.Builder<?>> clazz = null;
            Object o7 = null;
            Object o8 = null;
            Object o9 = null;
            Object o10 = null;
            Object builder = null;
            Object o11 = o6;
            Object o12 = o2;
            Object o13 = o3;
            Object o14 = o4;
            Object o15 = o5;
            Object o16;
            while (true) {
                try {
                    o16 = (builder = NavdyEventUtil.<Message>getBuilder(class1));
                    o11 = o6;
                    o7 = o16;
                    o12 = o2;
                    o8 = o16;
                    o13 = o3;
                    o9 = o16;
                    o14 = o4;
                    o10 = o16;
                    o15 = o5;
                    o = ((Class<Message.Builder<?>>)o16).getConstructor(class1).newInstance(o);
                    builder = o16;
                    o11 = o;
                    o7 = o16;
                    o12 = o;
                    o8 = o16;
                    o13 = o;
                    o9 = o16;
                    o14 = o;
                    o10 = o16;
                    o15 = o;
                    builder = ((Class<Message.Builder<?>>)o16).getConstructor(class1).newInstance(t);
                    if (o16 == null || o == null || builder == null) {
                        build = null;
                        return (T)build;
                    }
                }
                catch (InstantiationException ex) {
                    NavdyEventUtil.sLogger.e(ex.getMessage(), ex);
                    o16 = builder;
                    builder = clazz;
                    o = o11;
                    continue;
                }
                catch (IllegalAccessException ex2) {
                    NavdyEventUtil.sLogger.e(ex2.getMessage(), ex2);
                    o16 = o7;
                    builder = clazz;
                    o = o12;
                    continue;
                }
                catch (InvocationTargetException ex3) {
                    NavdyEventUtil.sLogger.e(ex3.getMessage(), ex3);
                    o16 = o8;
                    builder = clazz;
                    o = o13;
                    continue;
                }
                catch (NoSuchMethodException ex4) {
                    NavdyEventUtil.sLogger.e(ex4.getMessage(), ex4);
                    o16 = o9;
                    builder = clazz;
                    o = o14;
                    continue;
                }
                catch (ClassNotFoundException ex5) {
                    NavdyEventUtil.sLogger.e(ex5.getMessage(), ex5);
                    o16 = o10;
                    builder = clazz;
                    o = o15;
                    continue;
                }
                break;
            }
            final Field[] declaredFields = ((Class)o16).getDeclaredFields();
            final int length = declaredFields.length;
            int i = 0;
        Label_0442_Outer:
            while (i < length) {
                final Field field = declaredFields[i];
                while (true) {
                    try {
                        final Object value = field.get(o);
                        final Object value2 = field.get(builder);
                        Object o17 = null;
                        if (Message.class.isAssignableFrom(field.getType())) {
                            o17 = NavdyEventUtil.<Object>merge(value, value2);
                        }
                        else if (!Modifier.isFinal(field.getModifiers())) {
                            o17 = Wire.<T>get(value, value2);
                        }
                        if (o17 != null) {
                            field.set(o, o17);
                        }
                        ++i;
                        continue Label_0442_Outer;
                    }
                    catch (IllegalAccessException ex6) {
                        NavdyEventUtil.sLogger.e(ex6.getMessage(), ex6);
                        continue;
                    }
                    break;
                }
                break;
            }
            build = ((Message.Builder<T>)o).build();
        }
        return (T)build;
    }
    
    public static Message messageFromEvent(final NavdyEvent navdyEvent) {
        final Extension extension = findExtension(navdyEvent);
        Message message;
        if (extension == null) {
            NavdyEventUtil.sLogger.e("Unable to find extension for event: " + navdyEvent);
            message = null;
        }
        else {
            message = navdyEvent.<Message>getExtension((Extension<NavdyEvent, Message>)extension);
        }
        return message;
    }
    
    public abstract static class Initializer<T extends Message>
    {
        public abstract T build(final Message.Builder<T> p0);
    }
}
