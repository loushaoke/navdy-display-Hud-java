package com.navdy.service.library.events.settings;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class DateTimeConfiguration extends Message
{
    public static final Clock DEFAULT_FORMAT;
    public static final Long DEFAULT_TIMESTAMP;
    public static final String DEFAULT_TIMEZONE = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final Clock format;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long timestamp;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.STRING)
    public final String timezone;
    
    static {
        DEFAULT_TIMESTAMP = 0L;
        DEFAULT_FORMAT = Clock.CLOCK_24_HOUR;
    }
    
    private DateTimeConfiguration(final Builder builder) {
        this(builder.timestamp, builder.timezone, builder.format);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DateTimeConfiguration(final Long timestamp, final String timezone, final Clock format) {
        this.timestamp = timestamp;
        this.timezone = timezone;
        this.format = format;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof DateTimeConfiguration)) {
                b = false;
            }
            else {
                final DateTimeConfiguration dateTimeConfiguration = (DateTimeConfiguration)o;
                if (!this.equals(this.timestamp, dateTimeConfiguration.timestamp) || !this.equals(this.timezone, dateTimeConfiguration.timezone) || !this.equals(this.format, dateTimeConfiguration.format)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.timestamp != null) {
                hashCode3 = this.timestamp.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.timezone != null) {
                hashCode4 = this.timezone.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.format != null) {
                hashCode = this.format.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<DateTimeConfiguration>
    {
        public Clock format;
        public Long timestamp;
        public String timezone;
        
        public Builder() {
        }
        
        public Builder(final DateTimeConfiguration dateTimeConfiguration) {
            super(dateTimeConfiguration);
            if (dateTimeConfiguration != null) {
                this.timestamp = dateTimeConfiguration.timestamp;
                this.timezone = dateTimeConfiguration.timezone;
                this.format = dateTimeConfiguration.format;
            }
        }
        
        public DateTimeConfiguration build() {
            ((Message.Builder)this).checkRequiredFields();
            return new DateTimeConfiguration(this, null);
        }
        
        public Builder format(final Clock format) {
            this.format = format;
            return this;
        }
        
        public Builder timestamp(final Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }
        
        public Builder timezone(final String timezone) {
            this.timezone = timezone;
            return this;
        }
    }
    
    public enum Clock implements ProtoEnum
    {
        CLOCK_12_HOUR(2), 
        CLOCK_24_HOUR(1);
        
        private final int value;
        
        private Clock(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
