package com.navdy.service.library.events.debug;

import com.squareup.wire.Message;

public final class StopDriveRecordingEvent extends Message
{
    private static final long serialVersionUID = 0L;
    
    public StopDriveRecordingEvent() {
    }
    
    private StopDriveRecordingEvent(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof StopDriveRecordingEvent;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<StopDriveRecordingEvent>
    {
        public Builder() {
        }
        
        public Builder(final StopDriveRecordingEvent stopDriveRecordingEvent) {
            super(stopDriveRecordingEvent);
        }
        
        public StopDriveRecordingEvent build() {
            return new StopDriveRecordingEvent(this, null);
        }
    }
}
