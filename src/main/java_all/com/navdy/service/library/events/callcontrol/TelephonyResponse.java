package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class TelephonyResponse extends Message
{
    public static final CallAction DEFAULT_ACTION;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final CallAction action;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_ACTION = CallAction.CALL_ACCEPT;
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    }
    
    public TelephonyResponse(final CallAction action, final RequestStatus status, final String statusDetail) {
        this.action = action;
        this.status = status;
        this.statusDetail = statusDetail;
    }
    
    private TelephonyResponse(final Builder builder) {
        this(builder.action, builder.status, builder.statusDetail);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof TelephonyResponse)) {
                b = false;
            }
            else {
                final TelephonyResponse telephonyResponse = (TelephonyResponse)o;
                if (!this.equals(this.action, telephonyResponse.action) || !this.equals(this.status, telephonyResponse.status) || !this.equals(this.statusDetail, telephonyResponse.statusDetail)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.action != null) {
                hashCode3 = this.action.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.status != null) {
                hashCode4 = this.status.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.statusDetail != null) {
                hashCode = this.statusDetail.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<TelephonyResponse>
    {
        public CallAction action;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final TelephonyResponse telephonyResponse) {
            super(telephonyResponse);
            if (telephonyResponse != null) {
                this.action = telephonyResponse.action;
                this.status = telephonyResponse.status;
                this.statusDetail = telephonyResponse.statusDetail;
            }
        }
        
        public Builder action(final CallAction action) {
            this.action = action;
            return this;
        }
        
        public TelephonyResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new TelephonyResponse(this, null);
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
