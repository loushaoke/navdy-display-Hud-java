package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;

public enum MusicShuffleMode implements ProtoEnum
{
    MUSIC_SHUFFLE_MODE_ALBUMS(4), 
    MUSIC_SHUFFLE_MODE_OFF(2), 
    MUSIC_SHUFFLE_MODE_SONGS(3), 
    MUSIC_SHUFFLE_MODE_UNKNOWN(1);
    
    private final int value;
    
    private MusicShuffleMode(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
