package com.navdy.service.library.events.ui;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class DismissScreen extends Message
{
    public static final Screen DEFAULT_SCREEN;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Screen screen;
    
    static {
        DEFAULT_SCREEN = Screen.SCREEN_DASHBOARD;
    }
    
    private DismissScreen(final Builder builder) {
        this(builder.screen);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DismissScreen(final Screen screen) {
        this.screen = screen;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof DismissScreen && this.equals(this.screen, ((DismissScreen)o).screen));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.screen != null) {
                hashCode = this.screen.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<DismissScreen>
    {
        public Screen screen;
        
        public Builder() {
        }
        
        public Builder(final DismissScreen dismissScreen) {
            super(dismissScreen);
            if (dismissScreen != null) {
                this.screen = dismissScreen.screen;
            }
        }
        
        public DismissScreen build() {
            ((Message.Builder)this).checkRequiredFields();
            return new DismissScreen(this, null);
        }
        
        public Builder screen(final Screen screen) {
            this.screen = screen;
            return this;
        }
    }
}
