package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class MusicEvent extends Message
{
    public static final Action DEFAULT_ACTION;
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE;
    public static final MusicDataSource DEFAULT_DATASOURCE;
    public static final Integer DEFAULT_INDEX;
    public static final MusicRepeatMode DEFAULT_REPEATMODE;
    public static final MusicShuffleMode DEFAULT_SHUFFLEMODE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Action action;
    @ProtoField(tag = 13, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 14, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 15, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final MusicDataSource dataSource;
    @ProtoField(tag = 16, type = Datatype.INT32)
    public final Integer index;
    @ProtoField(tag = 18, type = Datatype.ENUM)
    public final MusicRepeatMode repeatMode;
    @ProtoField(tag = 17, type = Datatype.ENUM)
    public final MusicShuffleMode shuffleMode;
    
    static {
        DEFAULT_ACTION = Action.MUSIC_ACTION_PLAY;
        DEFAULT_DATASOURCE = MusicDataSource.MUSIC_SOURCE_NONE;
        DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
        DEFAULT_INDEX = 0;
        DEFAULT_SHUFFLEMODE = MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN;
        DEFAULT_REPEATMODE = MusicRepeatMode.MUSIC_REPEAT_MODE_UNKNOWN;
    }
    
    public MusicEvent(final Action action, final MusicDataSource dataSource, final MusicCollectionSource collectionSource, final MusicCollectionType collectionType, final String collectionId, final Integer index, final MusicShuffleMode shuffleMode, final MusicRepeatMode repeatMode) {
        this.action = action;
        this.dataSource = dataSource;
        this.collectionSource = collectionSource;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
        this.index = index;
        this.shuffleMode = shuffleMode;
        this.repeatMode = repeatMode;
    }
    
    private MusicEvent(final Builder builder) {
        this(builder.action, builder.dataSource, builder.collectionSource, builder.collectionType, builder.collectionId, builder.index, builder.shuffleMode, builder.repeatMode);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicEvent)) {
                b = false;
            }
            else {
                final MusicEvent musicEvent = (MusicEvent)o;
                if (!this.equals(this.action, musicEvent.action) || !this.equals(this.dataSource, musicEvent.dataSource) || !this.equals(this.collectionSource, musicEvent.collectionSource) || !this.equals(this.collectionType, musicEvent.collectionType) || !this.equals(this.collectionId, musicEvent.collectionId) || !this.equals(this.index, musicEvent.index) || !this.equals(this.shuffleMode, musicEvent.shuffleMode) || !this.equals(this.repeatMode, musicEvent.repeatMode)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.action != null) {
                hashCode3 = this.action.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.dataSource != null) {
                hashCode4 = this.dataSource.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.collectionSource != null) {
                hashCode5 = this.collectionSource.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.collectionType != null) {
                hashCode6 = this.collectionType.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.collectionId != null) {
                hashCode7 = this.collectionId.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.index != null) {
                hashCode8 = this.index.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.shuffleMode != null) {
                hashCode9 = this.shuffleMode.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            if (this.repeatMode != null) {
                hashCode = this.repeatMode.hashCode();
            }
            hashCode2 = ((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public enum Action implements ProtoEnum
    {
        MUSIC_ACTION_FAST_FORWARD_START(6), 
        MUSIC_ACTION_FAST_FORWARD_STOP(8), 
        MUSIC_ACTION_MODE_CHANGE(9), 
        MUSIC_ACTION_NEXT(3), 
        MUSIC_ACTION_PAUSE(2), 
        MUSIC_ACTION_PLAY(1), 
        MUSIC_ACTION_PREVIOUS(4), 
        MUSIC_ACTION_REWIND_START(5), 
        MUSIC_ACTION_REWIND_STOP(7);
        
        private final int value;
        
        private Action(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public static final class Builder extends Message.Builder<MusicEvent>
    {
        public Action action;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public MusicDataSource dataSource;
        public Integer index;
        public MusicRepeatMode repeatMode;
        public MusicShuffleMode shuffleMode;
        
        public Builder() {
        }
        
        public Builder(final MusicEvent musicEvent) {
            super(musicEvent);
            if (musicEvent != null) {
                this.action = musicEvent.action;
                this.dataSource = musicEvent.dataSource;
                this.collectionSource = musicEvent.collectionSource;
                this.collectionType = musicEvent.collectionType;
                this.collectionId = musicEvent.collectionId;
                this.index = musicEvent.index;
                this.shuffleMode = musicEvent.shuffleMode;
                this.repeatMode = musicEvent.repeatMode;
            }
        }
        
        public Builder action(final Action action) {
            this.action = action;
            return this;
        }
        
        public MusicEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new MusicEvent(this, null);
        }
        
        public Builder collectionId(final String collectionId) {
            this.collectionId = collectionId;
            return this;
        }
        
        public Builder collectionSource(final MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }
        
        public Builder collectionType(final MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }
        
        public Builder dataSource(final MusicDataSource dataSource) {
            this.dataSource = dataSource;
            return this;
        }
        
        public Builder index(final Integer index) {
            this.index = index;
            return this;
        }
        
        public Builder repeatMode(final MusicRepeatMode repeatMode) {
            this.repeatMode = repeatMode;
            return this;
        }
        
        public Builder shuffleMode(final MusicShuffleMode shuffleMode) {
            this.shuffleMode = shuffleMode;
            return this;
        }
    }
}
