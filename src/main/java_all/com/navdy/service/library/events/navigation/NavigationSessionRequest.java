package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NavigationSessionRequest extends Message
{
    public static final String DEFAULT_LABEL = "";
    public static final NavigationSessionState DEFAULT_NEWSTATE;
    public static final Boolean DEFAULT_ORIGINDISPLAY;
    public static final String DEFAULT_ROUTEID = "";
    public static final Integer DEFAULT_SIMULATIONSPEED;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String label;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final NavigationSessionState newState;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean originDisplay;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String routeId;
    @ProtoField(tag = 4, type = Datatype.UINT32)
    public final Integer simulationSpeed;
    
    static {
        DEFAULT_NEWSTATE = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
        DEFAULT_SIMULATIONSPEED = 0;
        DEFAULT_ORIGINDISPLAY = false;
    }
    
    private NavigationSessionRequest(final Builder builder) {
        this(builder.newState, builder.label, builder.routeId, builder.simulationSpeed, builder.originDisplay);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NavigationSessionRequest(final NavigationSessionState newState, final String label, final String routeId, final Integer simulationSpeed, final Boolean originDisplay) {
        this.newState = newState;
        this.label = label;
        this.routeId = routeId;
        this.simulationSpeed = simulationSpeed;
        this.originDisplay = originDisplay;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationSessionRequest)) {
                b = false;
            }
            else {
                final NavigationSessionRequest navigationSessionRequest = (NavigationSessionRequest)o;
                if (!this.equals(this.newState, navigationSessionRequest.newState) || !this.equals(this.label, navigationSessionRequest.label) || !this.equals(this.routeId, navigationSessionRequest.routeId) || !this.equals(this.simulationSpeed, navigationSessionRequest.simulationSpeed) || !this.equals(this.originDisplay, navigationSessionRequest.originDisplay)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.newState != null) {
                hashCode3 = this.newState.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.label != null) {
                hashCode4 = this.label.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.routeId != null) {
                hashCode5 = this.routeId.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.simulationSpeed != null) {
                hashCode6 = this.simulationSpeed.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.originDisplay != null) {
                hashCode = this.originDisplay.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationSessionRequest>
    {
        public String label;
        public NavigationSessionState newState;
        public Boolean originDisplay;
        public String routeId;
        public Integer simulationSpeed;
        
        public Builder() {
        }
        
        public Builder(final NavigationSessionRequest navigationSessionRequest) {
            super(navigationSessionRequest);
            if (navigationSessionRequest != null) {
                this.newState = navigationSessionRequest.newState;
                this.label = navigationSessionRequest.label;
                this.routeId = navigationSessionRequest.routeId;
                this.simulationSpeed = navigationSessionRequest.simulationSpeed;
                this.originDisplay = navigationSessionRequest.originDisplay;
            }
        }
        
        public NavigationSessionRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationSessionRequest(this, null);
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder newState(final NavigationSessionState newState) {
            this.newState = newState;
            return this;
        }
        
        public Builder originDisplay(final Boolean originDisplay) {
            this.originDisplay = originDisplay;
            return this;
        }
        
        public Builder routeId(final String routeId) {
            this.routeId = routeId;
            return this;
        }
        
        public Builder simulationSpeed(final Integer simulationSpeed) {
            this.simulationSpeed = simulationSpeed;
            return this;
        }
    }
}
