package com.navdy.service.library.events.places;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class AutoCompleteRequest extends Message
{
    public static final Integer DEFAULT_MAXRESULTS;
    public static final String DEFAULT_PARTIALSEARCH = "";
    public static final Integer DEFAULT_SEARCHAREA;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer maxResults;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String partialSearch;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer searchArea;
    
    static {
        DEFAULT_SEARCHAREA = 0;
        DEFAULT_MAXRESULTS = 0;
    }
    
    private AutoCompleteRequest(final Builder builder) {
        this(builder.partialSearch, builder.searchArea, builder.maxResults);
        this.setBuilder((Message.Builder)builder);
    }
    
    public AutoCompleteRequest(final String partialSearch, final Integer searchArea, final Integer maxResults) {
        this.partialSearch = partialSearch;
        this.searchArea = searchArea;
        this.maxResults = maxResults;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof AutoCompleteRequest)) {
                b = false;
            }
            else {
                final AutoCompleteRequest autoCompleteRequest = (AutoCompleteRequest)o;
                if (!this.equals(this.partialSearch, autoCompleteRequest.partialSearch) || !this.equals(this.searchArea, autoCompleteRequest.searchArea) || !this.equals(this.maxResults, autoCompleteRequest.maxResults)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.partialSearch != null) {
                hashCode3 = this.partialSearch.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.searchArea != null) {
                hashCode4 = this.searchArea.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.maxResults != null) {
                hashCode = this.maxResults.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<AutoCompleteRequest>
    {
        public Integer maxResults;
        public String partialSearch;
        public Integer searchArea;
        
        public Builder() {
        }
        
        public Builder(final AutoCompleteRequest autoCompleteRequest) {
            super(autoCompleteRequest);
            if (autoCompleteRequest != null) {
                this.partialSearch = autoCompleteRequest.partialSearch;
                this.searchArea = autoCompleteRequest.searchArea;
                this.maxResults = autoCompleteRequest.maxResults;
            }
        }
        
        public AutoCompleteRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new AutoCompleteRequest(this, null);
        }
        
        public Builder maxResults(final Integer maxResults) {
            this.maxResults = maxResults;
            return this;
        }
        
        public Builder partialSearch(final String partialSearch) {
            this.partialSearch = partialSearch;
            return this;
        }
        
        public Builder searchArea(final Integer searchArea) {
            this.searchArea = searchArea;
            return this;
        }
    }
}
