package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum MessageConstants implements ProtoEnum
{
    MESSAGE_BODY(2), 
    MESSAGE_DOMAIN(3), 
    MESSAGE_FROM(0), 
    MESSAGE_FROM_NUMBER(1), 
    MESSAGE_IS_SMS(4);
    
    private final int value;
    
    private MessageConstants(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
