package com.navdy.service.library.events.dial;

import com.squareup.wire.ProtoEnum;

public enum DialError implements ProtoEnum
{
    DIAL_ALREADY_PAIRED(2), 
    DIAL_ERROR(1), 
    DIAL_NOT_FOUND(5), 
    DIAL_NOT_PAIRED(6), 
    DIAL_OPERATION_IN_PROGRESS(3), 
    DIAL_PAIRED(4);
    
    private final int value;
    
    private DialError(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
