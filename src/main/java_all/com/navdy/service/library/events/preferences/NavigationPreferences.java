package com.navdy.service.library.events.preferences;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NavigationPreferences extends Message
{
    public static final Boolean DEFAULT_ALLOWAUTOTRAINS;
    public static final Boolean DEFAULT_ALLOWFERRIES;
    public static final Boolean DEFAULT_ALLOWHIGHWAYS;
    public static final Boolean DEFAULT_ALLOWHOVLANES;
    public static final Boolean DEFAULT_ALLOWTOLLROADS;
    public static final Boolean DEFAULT_ALLOWTUNNELS;
    public static final Boolean DEFAULT_ALLOWUNPAVEDROADS;
    public static final Boolean DEFAULT_PHONETICTURNBYTURN;
    public static final RerouteForTraffic DEFAULT_REROUTEFORTRAFFIC;
    public static final RoutingType DEFAULT_ROUTINGTYPE;
    public static final Long DEFAULT_SERIAL_NUMBER;
    public static final Boolean DEFAULT_SHOWTRAFFICINOPENMAP;
    public static final Boolean DEFAULT_SHOWTRAFFICWHILENAVIGATING;
    public static final Boolean DEFAULT_SPOKENCAMERAWARNINGS;
    public static final Boolean DEFAULT_SPOKENSPEEDLIMITWARNINGS;
    public static final Boolean DEFAULT_SPOKENTURNBYTURN;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 13, type = Datatype.BOOL)
    public final Boolean allowAutoTrains;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean allowFerries;
    @ProtoField(tag = 14, type = Datatype.BOOL)
    public final Boolean allowHOVLanes;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean allowHighways;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean allowTollRoads;
    @ProtoField(tag = 11, type = Datatype.BOOL)
    public final Boolean allowTunnels;
    @ProtoField(tag = 12, type = Datatype.BOOL)
    public final Boolean allowUnpavedRoads;
    @ProtoField(tag = 16, type = Datatype.BOOL)
    public final Boolean phoneticTurnByTurn;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final RerouteForTraffic rerouteForTraffic;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final RoutingType routingType;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(tag = 6, type = Datatype.BOOL)
    public final Boolean showTrafficInOpenMap;
    @ProtoField(tag = 7, type = Datatype.BOOL)
    public final Boolean showTrafficWhileNavigating;
    @ProtoField(tag = 15, type = Datatype.BOOL)
    public final Boolean spokenCameraWarnings;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean spokenSpeedLimitWarnings;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean spokenTurnByTurn;
    
    static {
        DEFAULT_SERIAL_NUMBER = 0L;
        DEFAULT_REROUTEFORTRAFFIC = RerouteForTraffic.REROUTE_CONFIRM;
        DEFAULT_ROUTINGTYPE = RoutingType.ROUTING_FASTEST;
        DEFAULT_SPOKENTURNBYTURN = true;
        DEFAULT_SPOKENSPEEDLIMITWARNINGS = true;
        DEFAULT_SHOWTRAFFICINOPENMAP = true;
        DEFAULT_SHOWTRAFFICWHILENAVIGATING = true;
        DEFAULT_ALLOWHIGHWAYS = true;
        DEFAULT_ALLOWTOLLROADS = true;
        DEFAULT_ALLOWFERRIES = true;
        DEFAULT_ALLOWTUNNELS = true;
        DEFAULT_ALLOWUNPAVEDROADS = true;
        DEFAULT_ALLOWAUTOTRAINS = true;
        DEFAULT_ALLOWHOVLANES = true;
        DEFAULT_SPOKENCAMERAWARNINGS = true;
        DEFAULT_PHONETICTURNBYTURN = false;
    }
    
    private NavigationPreferences(final Builder builder) {
        this(builder.serial_number, builder.rerouteForTraffic, builder.routingType, builder.spokenTurnByTurn, builder.spokenSpeedLimitWarnings, builder.showTrafficInOpenMap, builder.showTrafficWhileNavigating, builder.allowHighways, builder.allowTollRoads, builder.allowFerries, builder.allowTunnels, builder.allowUnpavedRoads, builder.allowAutoTrains, builder.allowHOVLanes, builder.spokenCameraWarnings, builder.phoneticTurnByTurn);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NavigationPreferences(final Long serial_number, final RerouteForTraffic rerouteForTraffic, final RoutingType routingType, final Boolean spokenTurnByTurn, final Boolean spokenSpeedLimitWarnings, final Boolean showTrafficInOpenMap, final Boolean showTrafficWhileNavigating, final Boolean allowHighways, final Boolean allowTollRoads, final Boolean allowFerries, final Boolean allowTunnels, final Boolean allowUnpavedRoads, final Boolean allowAutoTrains, final Boolean allowHOVLanes, final Boolean spokenCameraWarnings, final Boolean phoneticTurnByTurn) {
        this.serial_number = serial_number;
        this.rerouteForTraffic = rerouteForTraffic;
        this.routingType = routingType;
        this.spokenTurnByTurn = spokenTurnByTurn;
        this.spokenSpeedLimitWarnings = spokenSpeedLimitWarnings;
        this.showTrafficInOpenMap = showTrafficInOpenMap;
        this.showTrafficWhileNavigating = showTrafficWhileNavigating;
        this.allowHighways = allowHighways;
        this.allowTollRoads = allowTollRoads;
        this.allowFerries = allowFerries;
        this.allowTunnels = allowTunnels;
        this.allowUnpavedRoads = allowUnpavedRoads;
        this.allowAutoTrains = allowAutoTrains;
        this.allowHOVLanes = allowHOVLanes;
        this.spokenCameraWarnings = spokenCameraWarnings;
        this.phoneticTurnByTurn = phoneticTurnByTurn;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationPreferences)) {
                b = false;
            }
            else {
                final NavigationPreferences navigationPreferences = (NavigationPreferences)o;
                if (!this.equals(this.serial_number, navigationPreferences.serial_number) || !this.equals(this.rerouteForTraffic, navigationPreferences.rerouteForTraffic) || !this.equals(this.routingType, navigationPreferences.routingType) || !this.equals(this.spokenTurnByTurn, navigationPreferences.spokenTurnByTurn) || !this.equals(this.spokenSpeedLimitWarnings, navigationPreferences.spokenSpeedLimitWarnings) || !this.equals(this.showTrafficInOpenMap, navigationPreferences.showTrafficInOpenMap) || !this.equals(this.showTrafficWhileNavigating, navigationPreferences.showTrafficWhileNavigating) || !this.equals(this.allowHighways, navigationPreferences.allowHighways) || !this.equals(this.allowTollRoads, navigationPreferences.allowTollRoads) || !this.equals(this.allowFerries, navigationPreferences.allowFerries) || !this.equals(this.allowTunnels, navigationPreferences.allowTunnels) || !this.equals(this.allowUnpavedRoads, navigationPreferences.allowUnpavedRoads) || !this.equals(this.allowAutoTrains, navigationPreferences.allowAutoTrains) || !this.equals(this.allowHOVLanes, navigationPreferences.allowHOVLanes) || !this.equals(this.spokenCameraWarnings, navigationPreferences.spokenCameraWarnings) || !this.equals(this.phoneticTurnByTurn, navigationPreferences.phoneticTurnByTurn)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.serial_number != null) {
                hashCode3 = this.serial_number.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.rerouteForTraffic != null) {
                hashCode4 = this.rerouteForTraffic.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.routingType != null) {
                hashCode5 = this.routingType.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.spokenTurnByTurn != null) {
                hashCode6 = this.spokenTurnByTurn.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.spokenSpeedLimitWarnings != null) {
                hashCode7 = this.spokenSpeedLimitWarnings.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.showTrafficInOpenMap != null) {
                hashCode8 = this.showTrafficInOpenMap.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.showTrafficWhileNavigating != null) {
                hashCode9 = this.showTrafficWhileNavigating.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            int hashCode10;
            if (this.allowHighways != null) {
                hashCode10 = this.allowHighways.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            int hashCode11;
            if (this.allowTollRoads != null) {
                hashCode11 = this.allowTollRoads.hashCode();
            }
            else {
                hashCode11 = 0;
            }
            int hashCode12;
            if (this.allowFerries != null) {
                hashCode12 = this.allowFerries.hashCode();
            }
            else {
                hashCode12 = 0;
            }
            int hashCode13;
            if (this.allowTunnels != null) {
                hashCode13 = this.allowTunnels.hashCode();
            }
            else {
                hashCode13 = 0;
            }
            int hashCode14;
            if (this.allowUnpavedRoads != null) {
                hashCode14 = this.allowUnpavedRoads.hashCode();
            }
            else {
                hashCode14 = 0;
            }
            int hashCode15;
            if (this.allowAutoTrains != null) {
                hashCode15 = this.allowAutoTrains.hashCode();
            }
            else {
                hashCode15 = 0;
            }
            int hashCode16;
            if (this.allowHOVLanes != null) {
                hashCode16 = this.allowHOVLanes.hashCode();
            }
            else {
                hashCode16 = 0;
            }
            int hashCode17;
            if (this.spokenCameraWarnings != null) {
                hashCode17 = this.spokenCameraWarnings.hashCode();
            }
            else {
                hashCode17 = 0;
            }
            if (this.phoneticTurnByTurn != null) {
                hashCode = this.phoneticTurnByTurn.hashCode();
            }
            hashCode2 = ((((((((((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode11) * 37 + hashCode12) * 37 + hashCode13) * 37 + hashCode14) * 37 + hashCode15) * 37 + hashCode16) * 37 + hashCode17) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationPreferences>
    {
        public Boolean allowAutoTrains;
        public Boolean allowFerries;
        public Boolean allowHOVLanes;
        public Boolean allowHighways;
        public Boolean allowTollRoads;
        public Boolean allowTunnels;
        public Boolean allowUnpavedRoads;
        public Boolean phoneticTurnByTurn;
        public RerouteForTraffic rerouteForTraffic;
        public RoutingType routingType;
        public Long serial_number;
        public Boolean showTrafficInOpenMap;
        public Boolean showTrafficWhileNavigating;
        public Boolean spokenCameraWarnings;
        public Boolean spokenSpeedLimitWarnings;
        public Boolean spokenTurnByTurn;
        
        public Builder() {
        }
        
        public Builder(final NavigationPreferences navigationPreferences) {
            super(navigationPreferences);
            if (navigationPreferences != null) {
                this.serial_number = navigationPreferences.serial_number;
                this.rerouteForTraffic = navigationPreferences.rerouteForTraffic;
                this.routingType = navigationPreferences.routingType;
                this.spokenTurnByTurn = navigationPreferences.spokenTurnByTurn;
                this.spokenSpeedLimitWarnings = navigationPreferences.spokenSpeedLimitWarnings;
                this.showTrafficInOpenMap = navigationPreferences.showTrafficInOpenMap;
                this.showTrafficWhileNavigating = navigationPreferences.showTrafficWhileNavigating;
                this.allowHighways = navigationPreferences.allowHighways;
                this.allowTollRoads = navigationPreferences.allowTollRoads;
                this.allowFerries = navigationPreferences.allowFerries;
                this.allowTunnels = navigationPreferences.allowTunnels;
                this.allowUnpavedRoads = navigationPreferences.allowUnpavedRoads;
                this.allowAutoTrains = navigationPreferences.allowAutoTrains;
                this.allowHOVLanes = navigationPreferences.allowHOVLanes;
                this.spokenCameraWarnings = navigationPreferences.spokenCameraWarnings;
                this.phoneticTurnByTurn = navigationPreferences.phoneticTurnByTurn;
            }
        }
        
        public Builder allowAutoTrains(final Boolean allowAutoTrains) {
            this.allowAutoTrains = allowAutoTrains;
            return this;
        }
        
        public Builder allowFerries(final Boolean allowFerries) {
            this.allowFerries = allowFerries;
            return this;
        }
        
        public Builder allowHOVLanes(final Boolean allowHOVLanes) {
            this.allowHOVLanes = allowHOVLanes;
            return this;
        }
        
        public Builder allowHighways(final Boolean allowHighways) {
            this.allowHighways = allowHighways;
            return this;
        }
        
        public Builder allowTollRoads(final Boolean allowTollRoads) {
            this.allowTollRoads = allowTollRoads;
            return this;
        }
        
        public Builder allowTunnels(final Boolean allowTunnels) {
            this.allowTunnels = allowTunnels;
            return this;
        }
        
        public Builder allowUnpavedRoads(final Boolean allowUnpavedRoads) {
            this.allowUnpavedRoads = allowUnpavedRoads;
            return this;
        }
        
        public NavigationPreferences build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationPreferences(this, null);
        }
        
        public Builder phoneticTurnByTurn(final Boolean phoneticTurnByTurn) {
            this.phoneticTurnByTurn = phoneticTurnByTurn;
            return this;
        }
        
        public Builder rerouteForTraffic(final RerouteForTraffic rerouteForTraffic) {
            this.rerouteForTraffic = rerouteForTraffic;
            return this;
        }
        
        public Builder routingType(final RoutingType routingType) {
            this.routingType = routingType;
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
        
        public Builder showTrafficInOpenMap(final Boolean showTrafficInOpenMap) {
            this.showTrafficInOpenMap = showTrafficInOpenMap;
            return this;
        }
        
        public Builder showTrafficWhileNavigating(final Boolean showTrafficWhileNavigating) {
            this.showTrafficWhileNavigating = showTrafficWhileNavigating;
            return this;
        }
        
        public Builder spokenCameraWarnings(final Boolean spokenCameraWarnings) {
            this.spokenCameraWarnings = spokenCameraWarnings;
            return this;
        }
        
        public Builder spokenSpeedLimitWarnings(final Boolean spokenSpeedLimitWarnings) {
            this.spokenSpeedLimitWarnings = spokenSpeedLimitWarnings;
            return this;
        }
        
        public Builder spokenTurnByTurn(final Boolean spokenTurnByTurn) {
            this.spokenTurnByTurn = spokenTurnByTurn;
            return this;
        }
    }
    
    public enum RerouteForTraffic implements ProtoEnum
    {
        REROUTE_AUTOMATIC(1), 
        REROUTE_CONFIRM(0), 
        REROUTE_NEVER(2);
        
        private final int value;
        
        private RerouteForTraffic(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public enum RoutingType implements ProtoEnum
    {
        ROUTING_FASTEST(0), 
        ROUTING_SHORTEST(1);
        
        private final int value;
        
        private RoutingType(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
