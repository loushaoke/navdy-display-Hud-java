package com.navdy.service.library.events.navigation;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.location.Coordinate;
import java.util.List;
import com.squareup.wire.Message;

public final class NavigationRouteResult extends Message
{
    public static final String DEFAULT_ADDRESS = "";
    public static final Integer DEFAULT_DURATION;
    public static final Integer DEFAULT_DURATION_TRAFFIC;
    public static final List<Coordinate> DEFAULT_GEOPOINTS_OBSOLETE;
    public static final String DEFAULT_LABEL = "";
    public static final Integer DEFAULT_LENGTH;
    public static final String DEFAULT_ROUTEID = "";
    public static final List<Float> DEFAULT_ROUTELATLONGS;
    public static final String DEFAULT_VIA = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String address;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.UINT32)
    public final Integer duration;
    @ProtoField(tag = 6, type = Datatype.UINT32)
    public final Integer duration_traffic;
    @ProtoField(label = Label.REPEATED, messageType = Coordinate.class, tag = 3)
    public final List<Coordinate> geoPoints_OBSOLETE;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String label;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.UINT32)
    public final Integer length;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String routeId;
    @ProtoField(label = Label.PACKED, tag = 7, type = Datatype.FLOAT)
    public final List<Float> routeLatLongs;
    @ProtoField(tag = 8, type = Datatype.STRING)
    public final String via;
    
    static {
        DEFAULT_GEOPOINTS_OBSOLETE = Collections.<Coordinate>emptyList();
        DEFAULT_LENGTH = 0;
        DEFAULT_DURATION = 0;
        DEFAULT_DURATION_TRAFFIC = 0;
        DEFAULT_ROUTELATLONGS = Collections.<Float>emptyList();
    }
    
    private NavigationRouteResult(final Builder builder) {
        this(builder.routeId, builder.label, builder.geoPoints_OBSOLETE, builder.length, builder.duration, builder.duration_traffic, builder.routeLatLongs, builder.via, builder.address);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NavigationRouteResult(final String routeId, final String label, final List<Coordinate> list, final Integer length, final Integer duration, final Integer duration_traffic, final List<Float> list2, final String via, final String address) {
        this.routeId = routeId;
        this.label = label;
        this.geoPoints_OBSOLETE = Message.<Coordinate>immutableCopyOf(list);
        this.length = length;
        this.duration = duration;
        this.duration_traffic = duration_traffic;
        this.routeLatLongs = Message.<Float>immutableCopyOf(list2);
        this.via = via;
        this.address = address;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationRouteResult)) {
                b = false;
            }
            else {
                final NavigationRouteResult navigationRouteResult = (NavigationRouteResult)o;
                if (!this.equals(this.routeId, navigationRouteResult.routeId) || !this.equals(this.label, navigationRouteResult.label) || !this.equals(this.geoPoints_OBSOLETE, navigationRouteResult.geoPoints_OBSOLETE) || !this.equals(this.length, navigationRouteResult.length) || !this.equals(this.duration, navigationRouteResult.duration) || !this.equals(this.duration_traffic, navigationRouteResult.duration_traffic) || !this.equals(this.routeLatLongs, navigationRouteResult.routeLatLongs) || !this.equals(this.via, navigationRouteResult.via) || !this.equals(this.address, navigationRouteResult.address)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 1;
        int hashCode2 = 0;
        int hashCode3;
        if ((hashCode3 = this.hashCode) == 0) {
            int hashCode4;
            if (this.routeId != null) {
                hashCode4 = this.routeId.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.label != null) {
                hashCode5 = this.label.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.geoPoints_OBSOLETE != null) {
                hashCode6 = this.geoPoints_OBSOLETE.hashCode();
            }
            else {
                hashCode6 = 1;
            }
            int hashCode7;
            if (this.length != null) {
                hashCode7 = this.length.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.duration != null) {
                hashCode8 = this.duration.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.duration_traffic != null) {
                hashCode9 = this.duration_traffic.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            if (this.routeLatLongs != null) {
                hashCode = this.routeLatLongs.hashCode();
            }
            int hashCode10;
            if (this.via != null) {
                hashCode10 = this.via.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            if (this.address != null) {
                hashCode2 = this.address.hashCode();
            }
            hashCode3 = (((((((hashCode4 * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode) * 37 + hashCode10) * 37 + hashCode2;
            this.hashCode = hashCode3;
        }
        return hashCode3;
    }
    
    public static final class Builder extends Message.Builder<NavigationRouteResult>
    {
        public String address;
        public Integer duration;
        public Integer duration_traffic;
        public List<Coordinate> geoPoints_OBSOLETE;
        public String label;
        public Integer length;
        public String routeId;
        public List<Float> routeLatLongs;
        public String via;
        
        public Builder() {
        }
        
        public Builder(final NavigationRouteResult navigationRouteResult) {
            super(navigationRouteResult);
            if (navigationRouteResult != null) {
                this.routeId = navigationRouteResult.routeId;
                this.label = navigationRouteResult.label;
                this.geoPoints_OBSOLETE = (List<Coordinate>)Message.<Object>copyOf((List<Object>)navigationRouteResult.geoPoints_OBSOLETE);
                this.length = navigationRouteResult.length;
                this.duration = navigationRouteResult.duration;
                this.duration_traffic = navigationRouteResult.duration_traffic;
                this.routeLatLongs = (List<Float>)Message.<Object>copyOf((List<Object>)navigationRouteResult.routeLatLongs);
                this.via = navigationRouteResult.via;
                this.address = navigationRouteResult.address;
            }
        }
        
        public Builder address(final String address) {
            this.address = address;
            return this;
        }
        
        public NavigationRouteResult build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationRouteResult(this, null);
        }
        
        public Builder duration(final Integer duration) {
            this.duration = duration;
            return this;
        }
        
        public Builder duration_traffic(final Integer duration_traffic) {
            this.duration_traffic = duration_traffic;
            return this;
        }
        
        public Builder geoPoints_OBSOLETE(final List<Coordinate> list) {
            this.geoPoints_OBSOLETE = Message.Builder.<Coordinate>checkForNulls(list);
            return this;
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder length(final Integer length) {
            this.length = length;
            return this;
        }
        
        public Builder routeId(final String routeId) {
            this.routeId = routeId;
            return this;
        }
        
        public Builder routeLatLongs(final List<Float> list) {
            this.routeLatLongs = Message.Builder.<Float>checkForNulls(list);
            return this;
        }
        
        public Builder via(final String via) {
            this.via = via;
            return this;
        }
    }
}
