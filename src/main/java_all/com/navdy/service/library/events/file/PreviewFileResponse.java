package com.navdy.service.library.events.file;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class PreviewFileResponse extends Message
{
    public static final String DEFAULT_FILENAME = "";
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUS_DETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String filename;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String status_detail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    }
    
    public PreviewFileResponse(final RequestStatus status, final String status_detail, final String filename) {
        this.status = status;
        this.status_detail = status_detail;
        this.filename = filename;
    }
    
    private PreviewFileResponse(final Builder builder) {
        this(builder.status, builder.status_detail, builder.filename);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PreviewFileResponse)) {
                b = false;
            }
            else {
                final PreviewFileResponse previewFileResponse = (PreviewFileResponse)o;
                if (!this.equals(this.status, previewFileResponse.status) || !this.equals(this.status_detail, previewFileResponse.status_detail) || !this.equals(this.filename, previewFileResponse.filename)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.status_detail != null) {
                hashCode4 = this.status_detail.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.filename != null) {
                hashCode = this.filename.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PreviewFileResponse>
    {
        public String filename;
        public RequestStatus status;
        public String status_detail;
        
        public Builder() {
        }
        
        public Builder(final PreviewFileResponse previewFileResponse) {
            super(previewFileResponse);
            if (previewFileResponse != null) {
                this.status = previewFileResponse.status;
                this.status_detail = previewFileResponse.status_detail;
                this.filename = previewFileResponse.filename;
            }
        }
        
        public PreviewFileResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PreviewFileResponse(this, null);
        }
        
        public Builder filename(final String filename) {
            this.filename = filename;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder status_detail(final String status_detail) {
            this.status_detail = status_detail;
            return this;
        }
    }
}
