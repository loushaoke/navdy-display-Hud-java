package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class RouteManeuverRequest extends Message
{
    public static final String DEFAULT_ROUTEID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String routeId;
    
    private RouteManeuverRequest(final Builder builder) {
        this(builder.routeId);
        this.setBuilder((Message.Builder)builder);
    }
    
    public RouteManeuverRequest(final String routeId) {
        this.routeId = routeId;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof RouteManeuverRequest && this.equals(this.routeId, ((RouteManeuverRequest)o).routeId));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.routeId != null) {
                hashCode = this.routeId.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<RouteManeuverRequest>
    {
        public String routeId;
        
        public Builder() {
        }
        
        public Builder(final RouteManeuverRequest routeManeuverRequest) {
            super(routeManeuverRequest);
            if (routeManeuverRequest != null) {
                this.routeId = routeManeuverRequest.routeId;
            }
        }
        
        public RouteManeuverRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new RouteManeuverRequest(this, null);
        }
        
        public Builder routeId(final String routeId) {
            this.routeId = routeId;
            return this;
        }
    }
}
