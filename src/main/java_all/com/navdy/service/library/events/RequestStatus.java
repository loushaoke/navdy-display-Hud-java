package com.navdy.service.library.events;

import com.squareup.wire.ProtoEnum;

public enum RequestStatus implements ProtoEnum
{
    REQUEST_ALREADY_IN_PROGRESS(10), 
    REQUEST_CANCELLED(11), 
    REQUEST_INVALID_REQUEST(5), 
    REQUEST_INVALID_STATE(6), 
    REQUEST_NOT_AVAILABLE(9), 
    REQUEST_NOT_READY(2), 
    REQUEST_NO_LOCATION_SERVICE(3), 
    REQUEST_SERVICE_ERROR(4), 
    REQUEST_SUCCESS(1), 
    REQUEST_UNKNOWN_ERROR(7), 
    REQUEST_VERSION_IS_CURRENT(8);
    
    private final int value;
    
    private RequestStatus(final int value) {
        this.value = value;
    }
    
    @Override
    public int getValue() {
        return this.value;
    }
}
