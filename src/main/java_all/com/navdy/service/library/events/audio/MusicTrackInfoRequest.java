package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;

public final class MusicTrackInfoRequest extends Message
{
    private static final long serialVersionUID = 0L;
    
    public MusicTrackInfoRequest() {
    }
    
    private MusicTrackInfoRequest(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof MusicTrackInfoRequest;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<MusicTrackInfoRequest>
    {
        public Builder() {
        }
        
        public Builder(final MusicTrackInfoRequest musicTrackInfoRequest) {
            super(musicTrackInfoRequest);
        }
        
        public MusicTrackInfoRequest build() {
            return new MusicTrackInfoRequest(this, null);
        }
    }
}
