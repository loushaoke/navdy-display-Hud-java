package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import okio.ByteString;
import com.squareup.wire.Message;

public final class MusicArtworkResponse extends Message
{
    public static final String DEFAULT_ALBUM = "";
    public static final String DEFAULT_AUTHOR = "";
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE;
    public static final String DEFAULT_NAME = "";
    public static final ByteString DEFAULT_PHOTO;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String album;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String author;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 6, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 5, type = Datatype.BYTES)
    public final ByteString photo;
    
    static {
        DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_PHOTO = ByteString.EMPTY;
        DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    }
    
    private MusicArtworkResponse(final Builder builder) {
        this(builder.collectionSource, builder.name, builder.album, builder.author, builder.photo, builder.collectionType, builder.collectionId);
        this.setBuilder((Message.Builder)builder);
    }
    
    public MusicArtworkResponse(final MusicCollectionSource collectionSource, final String name, final String album, final String author, final ByteString photo, final MusicCollectionType collectionType, final String collectionId) {
        this.collectionSource = collectionSource;
        this.name = name;
        this.album = album;
        this.author = author;
        this.photo = photo;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicArtworkResponse)) {
                b = false;
            }
            else {
                final MusicArtworkResponse musicArtworkResponse = (MusicArtworkResponse)o;
                if (!this.equals(this.collectionSource, musicArtworkResponse.collectionSource) || !this.equals(this.name, musicArtworkResponse.name) || !this.equals(this.album, musicArtworkResponse.album) || !this.equals(this.author, musicArtworkResponse.author) || !this.equals(this.photo, musicArtworkResponse.photo) || !this.equals(this.collectionType, musicArtworkResponse.collectionType) || !this.equals(this.collectionId, musicArtworkResponse.collectionId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.collectionSource != null) {
                hashCode3 = this.collectionSource.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.name != null) {
                hashCode4 = this.name.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.album != null) {
                hashCode5 = this.album.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.author != null) {
                hashCode6 = this.author.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.photo != null) {
                hashCode7 = this.photo.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.collectionType != null) {
                hashCode8 = this.collectionType.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            if (this.collectionId != null) {
                hashCode = this.collectionId.hashCode();
            }
            hashCode2 = (((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MusicArtworkResponse>
    {
        public String album;
        public String author;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public String name;
        public ByteString photo;
        
        public Builder() {
        }
        
        public Builder(final MusicArtworkResponse musicArtworkResponse) {
            super(musicArtworkResponse);
            if (musicArtworkResponse != null) {
                this.collectionSource = musicArtworkResponse.collectionSource;
                this.name = musicArtworkResponse.name;
                this.album = musicArtworkResponse.album;
                this.author = musicArtworkResponse.author;
                this.photo = musicArtworkResponse.photo;
                this.collectionType = musicArtworkResponse.collectionType;
                this.collectionId = musicArtworkResponse.collectionId;
            }
        }
        
        public Builder album(final String album) {
            this.album = album;
            return this;
        }
        
        public Builder author(final String author) {
            this.author = author;
            return this;
        }
        
        public MusicArtworkResponse build() {
            return new MusicArtworkResponse(this, null);
        }
        
        public Builder collectionId(final String collectionId) {
            this.collectionId = collectionId;
            return this;
        }
        
        public Builder collectionSource(final MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }
        
        public Builder collectionType(final MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder photo(final ByteString photo) {
            this.photo = photo;
            return this;
        }
    }
}
