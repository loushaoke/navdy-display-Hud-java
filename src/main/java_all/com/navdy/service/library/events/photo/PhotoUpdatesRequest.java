package com.navdy.service.library.events.photo;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PhotoUpdatesRequest extends Message
{
    public static final PhotoType DEFAULT_PHOTOTYPE;
    public static final Boolean DEFAULT_START;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final PhotoType photoType;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.BOOL)
    public final Boolean start;
    
    static {
        DEFAULT_START = false;
        DEFAULT_PHOTOTYPE = PhotoType.PHOTO_ALBUM_ART;
    }
    
    private PhotoUpdatesRequest(final Builder builder) {
        this(builder.start, builder.photoType);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PhotoUpdatesRequest(final Boolean start, final PhotoType photoType) {
        this.start = start;
        this.photoType = photoType;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhotoUpdatesRequest)) {
                b = false;
            }
            else {
                final PhotoUpdatesRequest photoUpdatesRequest = (PhotoUpdatesRequest)o;
                if (!this.equals(this.start, photoUpdatesRequest.start) || !this.equals(this.photoType, photoUpdatesRequest.photoType)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.start != null) {
                hashCode3 = this.start.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.photoType != null) {
                hashCode = this.photoType.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PhotoUpdatesRequest>
    {
        public PhotoType photoType;
        public Boolean start;
        
        public Builder() {
        }
        
        public Builder(final PhotoUpdatesRequest photoUpdatesRequest) {
            super(photoUpdatesRequest);
            if (photoUpdatesRequest != null) {
                this.start = photoUpdatesRequest.start;
                this.photoType = photoUpdatesRequest.photoType;
            }
        }
        
        public PhotoUpdatesRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PhotoUpdatesRequest(this, null);
        }
        
        public Builder photoType(final PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }
        
        public Builder start(final Boolean start) {
            this.start = start;
            return this;
        }
    }
}
