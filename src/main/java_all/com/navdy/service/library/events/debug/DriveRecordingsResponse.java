package com.navdy.service.library.events.debug;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import java.util.List;
import com.squareup.wire.Message;

public final class DriveRecordingsResponse extends Message
{
    public static final List<String> DEFAULT_RECORDINGS;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, tag = 1, type = Datatype.STRING)
    public final List<String> recordings;
    
    static {
        DEFAULT_RECORDINGS = Collections.<String>emptyList();
    }
    
    private DriveRecordingsResponse(final Builder builder) {
        this(builder.recordings);
        this.setBuilder((Message.Builder)builder);
    }
    
    public DriveRecordingsResponse(final List<String> list) {
        this.recordings = Message.<String>immutableCopyOf(list);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof DriveRecordingsResponse && this.equals(this.recordings, ((DriveRecordingsResponse)o).recordings));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.recordings != null) {
                hashCode = this.recordings.hashCode();
            }
            else {
                hashCode = 1;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<DriveRecordingsResponse>
    {
        public List<String> recordings;
        
        public Builder() {
        }
        
        public Builder(final DriveRecordingsResponse driveRecordingsResponse) {
            super(driveRecordingsResponse);
            if (driveRecordingsResponse != null) {
                this.recordings = (List<String>)Message.<Object>copyOf((List<Object>)driveRecordingsResponse.recordings);
            }
        }
        
        public DriveRecordingsResponse build() {
            return new DriveRecordingsResponse(this, null);
        }
        
        public Builder recordings(final List<String> list) {
            this.recordings = Message.Builder.<String>checkForNulls(list);
            return this;
        }
    }
}
