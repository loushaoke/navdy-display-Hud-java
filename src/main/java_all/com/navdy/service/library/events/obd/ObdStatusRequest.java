package com.navdy.service.library.events.obd;

import com.squareup.wire.Message;

public final class ObdStatusRequest extends Message
{
    private static final long serialVersionUID = 0L;
    
    public ObdStatusRequest() {
    }
    
    private ObdStatusRequest(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ObdStatusRequest;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<ObdStatusRequest>
    {
        public Builder() {
        }
        
        public Builder(final ObdStatusRequest obdStatusRequest) {
            super(obdStatusRequest);
        }
        
        public ObdStatusRequest build() {
            return new ObdStatusRequest(this, null);
        }
    }
}
