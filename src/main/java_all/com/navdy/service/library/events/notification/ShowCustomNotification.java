package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class ShowCustomNotification extends Message
{
    public static final String DEFAULT_ID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String id;
    
    private ShowCustomNotification(final Builder builder) {
        this(builder.id);
        this.setBuilder((Message.Builder)builder);
    }
    
    public ShowCustomNotification(final String id) {
        this.id = id;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof ShowCustomNotification && this.equals(this.id, ((ShowCustomNotification)o).id));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.id != null) {
                hashCode = this.id.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<ShowCustomNotification>
    {
        public String id;
        
        public Builder() {
        }
        
        public Builder(final ShowCustomNotification showCustomNotification) {
            super(showCustomNotification);
            if (showCustomNotification != null) {
                this.id = showCustomNotification.id;
            }
        }
        
        public ShowCustomNotification build() {
            ((Message.Builder)this).checkRequiredFields();
            return new ShowCustomNotification(this, null);
        }
        
        public Builder id(final String id) {
            this.id = id;
            return this;
        }
    }
}
