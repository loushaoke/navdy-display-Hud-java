package com.navdy.service.library.events;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class Frame extends Message
{
    public static final Integer DEFAULT_SIZE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.FIXED32)
    public final Integer size;
    
    static {
        DEFAULT_SIZE = 0;
    }
    
    private Frame(final Builder builder) {
        this(builder.size);
        this.setBuilder((Message.Builder)builder);
    }
    
    public Frame(final Integer size) {
        this.size = size;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof Frame && this.equals(this.size, ((Frame)o).size));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.size != null) {
                hashCode = this.size.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<Frame>
    {
        public Integer size;
        
        public Builder() {
        }
        
        public Builder(final Frame frame) {
            super(frame);
            if (frame != null) {
                this.size = frame.size;
            }
        }
        
        public Frame build() {
            ((Message.Builder)this).checkRequiredFields();
            return new Frame(this, null);
        }
        
        public Builder size(final Integer size) {
            this.size = size;
            return this;
        }
    }
}
