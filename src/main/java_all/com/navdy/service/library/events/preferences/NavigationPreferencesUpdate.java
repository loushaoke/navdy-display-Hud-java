package com.navdy.service.library.events.preferences;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class NavigationPreferencesUpdate extends Message
{
    public static final Long DEFAULT_SERIAL_NUMBER;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 4)
    public final NavigationPreferences preferences;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_SERIAL_NUMBER = 0L;
    }
    
    public NavigationPreferencesUpdate(final RequestStatus status, final String statusDetail, final Long serial_number, final NavigationPreferences preferences) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.serial_number = serial_number;
        this.preferences = preferences;
    }
    
    private NavigationPreferencesUpdate(final Builder builder) {
        this(builder.status, builder.statusDetail, builder.serial_number, builder.preferences);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationPreferencesUpdate)) {
                b = false;
            }
            else {
                final NavigationPreferencesUpdate navigationPreferencesUpdate = (NavigationPreferencesUpdate)o;
                if (!this.equals(this.status, navigationPreferencesUpdate.status) || !this.equals(this.statusDetail, navigationPreferencesUpdate.statusDetail) || !this.equals(this.serial_number, navigationPreferencesUpdate.serial_number) || !this.equals(this.preferences, navigationPreferencesUpdate.preferences)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.statusDetail != null) {
                hashCode4 = this.statusDetail.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.serial_number != null) {
                hashCode5 = this.serial_number.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.preferences != null) {
                hashCode = this.preferences.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationPreferencesUpdate>
    {
        public NavigationPreferences preferences;
        public Long serial_number;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final NavigationPreferencesUpdate navigationPreferencesUpdate) {
            super(navigationPreferencesUpdate);
            if (navigationPreferencesUpdate != null) {
                this.status = navigationPreferencesUpdate.status;
                this.statusDetail = navigationPreferencesUpdate.statusDetail;
                this.serial_number = navigationPreferencesUpdate.serial_number;
                this.preferences = navigationPreferencesUpdate.preferences;
            }
        }
        
        public NavigationPreferencesUpdate build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationPreferencesUpdate(this, null);
        }
        
        public Builder preferences(final NavigationPreferences preferences) {
            this.preferences = preferences;
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
