package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class CancelSpeechRequest extends Message
{
    public static final String DEFAULT_ID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String id;
    
    private CancelSpeechRequest(final Builder builder) {
        this(builder.id);
        this.setBuilder((Message.Builder)builder);
    }
    
    public CancelSpeechRequest(final String id) {
        this.id = id;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof CancelSpeechRequest && this.equals(this.id, ((CancelSpeechRequest)o).id));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.id != null) {
                hashCode = this.id.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<CancelSpeechRequest>
    {
        public String id;
        
        public Builder() {
        }
        
        public Builder(final CancelSpeechRequest cancelSpeechRequest) {
            super(cancelSpeechRequest);
            if (cancelSpeechRequest != null) {
                this.id = cancelSpeechRequest.id;
            }
        }
        
        public CancelSpeechRequest build() {
            return new CancelSpeechRequest(this, null);
        }
        
        public Builder id(final String id) {
            this.id = id;
            return this;
        }
    }
}
