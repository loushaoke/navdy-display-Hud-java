package com.navdy.service.library.events.input;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class GestureEvent extends Message
{
    public static final Gesture DEFAULT_GESTURE;
    public static final Integer DEFAULT_X;
    public static final Integer DEFAULT_Y;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Gesture gesture;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer x;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer y;
    
    static {
        DEFAULT_GESTURE = Gesture.GESTURE_SWIPE_LEFT;
        DEFAULT_X = 0;
        DEFAULT_Y = 0;
    }
    
    public GestureEvent(final Gesture gesture, final Integer x, final Integer y) {
        this.gesture = gesture;
        this.x = x;
        this.y = y;
    }
    
    private GestureEvent(final Builder builder) {
        this(builder.gesture, builder.x, builder.y);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof GestureEvent)) {
                b = false;
            }
            else {
                final GestureEvent gestureEvent = (GestureEvent)o;
                if (!this.equals(this.gesture, gestureEvent.gesture) || !this.equals(this.x, gestureEvent.x) || !this.equals(this.y, gestureEvent.y)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.gesture != null) {
                hashCode3 = this.gesture.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.x != null) {
                hashCode4 = this.x.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.y != null) {
                hashCode = this.y.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<GestureEvent>
    {
        public Gesture gesture;
        public Integer x;
        public Integer y;
        
        public Builder() {
        }
        
        public Builder(final GestureEvent gestureEvent) {
            super(gestureEvent);
            if (gestureEvent != null) {
                this.gesture = gestureEvent.gesture;
                this.x = gestureEvent.x;
                this.y = gestureEvent.y;
            }
        }
        
        public GestureEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new GestureEvent(this, null);
        }
        
        public Builder gesture(final Gesture gesture) {
            this.gesture = gesture;
            return this;
        }
        
        public Builder x(final Integer x) {
            this.x = x;
            return this;
        }
        
        public Builder y(final Integer y) {
            this.y = y;
            return this;
        }
    }
}
