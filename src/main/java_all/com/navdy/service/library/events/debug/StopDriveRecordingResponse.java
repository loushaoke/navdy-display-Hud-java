package com.navdy.service.library.events.debug;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class StopDriveRecordingResponse extends Message
{
    public static final RequestStatus DEFAULT_STATUS;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    }
    
    public StopDriveRecordingResponse(final RequestStatus status) {
        this.status = status;
    }
    
    private StopDriveRecordingResponse(final Builder builder) {
        this(builder.status);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof StopDriveRecordingResponse && this.equals(this.status, ((StopDriveRecordingResponse)o).status));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.status != null) {
                hashCode = this.status.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<StopDriveRecordingResponse>
    {
        public RequestStatus status;
        
        public Builder() {
        }
        
        public Builder(final StopDriveRecordingResponse stopDriveRecordingResponse) {
            super(stopDriveRecordingResponse);
            if (stopDriveRecordingResponse != null) {
                this.status = stopDriveRecordingResponse.status;
            }
        }
        
        public StopDriveRecordingResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new StopDriveRecordingResponse(this, null);
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
    }
}
