package com.navdy.service.library.device.link;

import java.util.concurrent.LinkedBlockingDeque;
import com.navdy.service.library.network.SocketAdapter;
import java.io.Closeable;

public interface Link extends Closeable
{
    public static final int BANDWIDTH_LEVEL_LOW = 0;
    public static final int BANDWIDTH_LEVEL_NORMAL = 1;
    
    void close();
    
    int getBandWidthLevel();
    
    void setBandwidthLevel(final int p0);
    
    boolean start(final SocketAdapter p0, final LinkedBlockingDeque<EventRequest> p1, final LinkListener p2);
}
