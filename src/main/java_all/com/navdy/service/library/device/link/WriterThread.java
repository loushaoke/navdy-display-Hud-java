package com.navdy.service.library.device.link;

import java.io.IOException;
import com.navdy.service.library.device.RemoteDevice;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.io.OutputStream;
import com.navdy.service.library.events.NavdyEventWriter;
import java.util.concurrent.LinkedBlockingDeque;

public class WriterThread extends IOThread
{
    private final EventProcessor mEventProcessor;
    private final LinkedBlockingDeque<EventRequest> mEventQueue;
    private final NavdyEventWriter mmEventWriter;
    private OutputStream mmOutStream;
    
    public WriterThread(final LinkedBlockingDeque<EventRequest> mEventQueue, final OutputStream mmOutStream, final EventProcessor mEventProcessor) {
        this.setName("WriterThread");
        this.logger.d("create WriterThread");
        this.mmOutStream = mmOutStream;
        this.mmEventWriter = new NavdyEventWriter(this.mmOutStream);
        this.mEventQueue = mEventQueue;
        this.mEventProcessor = mEventProcessor;
    }
    
    @Override
    public void cancel() {
        super.cancel();
        IOUtils.closeStream(this.mmOutStream);
        this.mmOutStream = null;
    }
    
    @Override
    public void run() {
        this.logger.i("starting WriterThread loop.");
        byte[] eventData = null;
    Label_0178_Outer:
        while (true) {
            EventRequest eventRequest = null;
            Label_0038: {
                if (this.closing) {
                    break Label_0038;
                }
                eventRequest = null;
                while (true) {
                    try {
                        eventRequest = this.mEventQueue.take();
                        if (this.closing) {
                            this.logger.i("Writer thread ending");
                            return;
                        }
                    }
                    catch (InterruptedException ex2) {
                        this.logger.i("WriterThread interrupted");
                        continue;
                    }
                    break;
                }
            }
            if (eventRequest == null) {
                this.logger.e("WriterThread: unable to retrieve event from queue");
            }
            else {
                eventData = eventRequest.eventData;
                if (eventData.length > 524288) {
                    break;
                }
                byte[] processEvent = eventData;
                if (this.mEventProcessor != null) {
                    processEvent = this.mEventProcessor.processEvent(eventData);
                }
                if (processEvent == null) {
                    continue;
                }
                final boolean b = false;
                while (true) {
                    try {
                        this.mmEventWriter.write(eventRequest.eventData);
                        final boolean b2 = true;
                        if (b2) {
                            continue Label_0178_Outer;
                        }
                        this.logger.i("send failed");
                        eventRequest.callCompletionHandlers(RemoteDevice.PostEventStatus.SEND_FAILED);
                    }
                    catch (IOException ex) {
                        boolean b2 = b;
                        if (!this.closing) {
                            this.logger.e("Error writing event:" + ex.getMessage());
                            b2 = b;
                        }
                        continue;
                    }
                    break;
                }
            }
        }
        throw new RuntimeException("writer Max packet size exceeded [" + eventData.length + "] bytes[" + IOUtils.bytesToHexString(eventData, 0, 50) + "]");
    }
    
    public interface EventProcessor
    {
        byte[] processEvent(final byte[] p0);
    }
}
