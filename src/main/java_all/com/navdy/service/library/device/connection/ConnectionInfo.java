package com.navdy.service.library.device.connection;

import java.util.regex.Matcher;
import android.text.TextUtils;
import android.net.nsd.NsdServiceInfo;
import com.google.gson.annotations.SerializedName;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.log.Logger;
import java.util.regex.Pattern;
import com.navdy.service.library.util.RuntimeTypeAdapterFactory;

public abstract class ConnectionInfo
{
    private static final int DEVICE_ID_GROUP = 1;
    public static final RuntimeTypeAdapterFactory<ConnectionInfo> connectionInfoAdapter;
    private static final Pattern deviceIdFromServiceName;
    public static final Logger sLogger;
    private final ConnectionType connectionType;
    @SerializedName("deviceId")
    protected final NavdyDeviceId mDeviceId;
    
    static {
        sLogger = new Logger(ConnectionInfo.class);
        deviceIdFromServiceName = Pattern.compile("Navdy\\s(.*)");
        connectionInfoAdapter = RuntimeTypeAdapterFactory.<ConnectionInfo>of(ConnectionInfo.class).registerSubtype(BTConnectionInfo.class, "BT").registerSubtype(TCPConnectionInfo.class, "TCP").registerSubtypeAlias(BTConnectionInfo.class, "EA");
    }
    
    public ConnectionInfo(final NsdServiceInfo nsdServiceInfo) {
        if (nsdServiceInfo == null) {
            throw new IllegalArgumentException("serviceInfo can't be null");
        }
        final String deviceIdStringFromServiceInfo = this.parseDeviceIdStringFromServiceInfo(nsdServiceInfo);
        if (deviceIdStringFromServiceInfo == null) {
            throw new IllegalArgumentException("Service info doesn't contain valid device id");
        }
        this.connectionType = ConnectionType.TCP_PROTOBUF;
        this.mDeviceId = new NavdyDeviceId(deviceIdStringFromServiceInfo);
    }
    
    public ConnectionInfo(final NavdyDeviceId mDeviceId, final ConnectionType connectionType) {
        if (mDeviceId == null) {
            throw new IllegalArgumentException("Device id required.");
        }
        this.mDeviceId = mDeviceId;
        this.connectionType = connectionType;
    }
    
    protected ConnectionInfo(final ConnectionType connectionType) {
        this.connectionType = connectionType;
        this.mDeviceId = NavdyDeviceId.UNKNOWN_ID;
    }
    
    public static ConnectionInfo fromConnection(final Connection connection) {
        ConnectionInfo connectionInfo;
        if (connection == null) {
            connectionInfo = null;
        }
        else {
            connectionInfo = connection.getConnectionInfo();
        }
        return connectionInfo;
    }
    
    public static ConnectionInfo fromServiceInfo(final NsdServiceInfo nsdServiceInfo) {
        final ConnectionInfo connectionInfo = null;
        String s2;
        final String s = s2 = nsdServiceInfo.getServiceType();
        if (s.startsWith(".")) {
            s2 = s.substring(1);
        }
        String string = s2;
        if (!s2.endsWith(".")) {
            string = s2 + ".";
        }
        final ConnectionType fromServiceType = ConnectionType.fromServiceType(string);
        ConnectionInfo connectionInfo2 = null;
        if (fromServiceType == null) {
            connectionInfo2 = connectionInfo;
        }
        else {
            switch (fromServiceType) {
                default:
                    ConnectionInfo.sLogger.e("No connectioninfo from mDNS info for type: " + fromServiceType);
                    connectionInfo2 = connectionInfo;
                    break;
                case TCP_PROTOBUF:
                    connectionInfo2 = new TCPConnectionInfo(nsdServiceInfo);
                    break;
            }
        }
        return connectionInfo2;
    }
    
    public static boolean isValidNavdyServiceInfo(final NsdServiceInfo nsdServiceInfo) {
        boolean b = false;
        if (!ConnectionType.getServiceTypes().contains(nsdServiceInfo.getServiceType())) {
            ConnectionInfo.sLogger.d("Unknown service type: " + nsdServiceInfo.getServiceType());
        }
        else if (ConnectionInfo.deviceIdFromServiceName.matcher(nsdServiceInfo.getServiceName()).matches()) {
            b = true;
        }
        else {
            ConnectionInfo.sLogger.d("Not a Navdy: " + nsdServiceInfo.getServiceName());
        }
        return b;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && this.getClass() == o.getClass() && this.mDeviceId.equals(((ConnectionInfo)o).mDeviceId));
    }
    
    public abstract ServiceAddress getAddress();
    
    public NavdyDeviceId getDeviceId() {
        return this.mDeviceId;
    }
    
    public ConnectionType getType() {
        return this.connectionType;
    }
    
    @Override
    public int hashCode() {
        return this.mDeviceId.hashCode();
    }
    
    protected String parseDeviceIdStringFromServiceInfo(final NsdServiceInfo nsdServiceInfo) {
        final String s = null;
        final String replace = nsdServiceInfo.getServiceName().replace("\\032", " ");
        String group;
        if (TextUtils.isEmpty((CharSequence)replace)) {
            group = s;
        }
        else {
            final Matcher matcher = ConnectionInfo.deviceIdFromServiceName.matcher(replace);
            group = s;
            if (matcher.matches()) {
                group = s;
                if (matcher.groupCount() >= 1) {
                    group = matcher.group(1);
                }
            }
        }
        return group;
    }
    
    @Override
    public String toString() {
        return this.getType() + " id: " + this.mDeviceId + " " + this.getAddress();
    }
}
