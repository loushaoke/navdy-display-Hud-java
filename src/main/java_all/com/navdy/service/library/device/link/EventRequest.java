package com.navdy.service.library.device.link;

import android.os.Handler;
import com.navdy.service.library.device.RemoteDevice;

public class EventRequest
{
    public final RemoteDevice.PostEventHandler eventCompleteHandler;
    public final byte[] eventData;
    private final Handler handler;
    
    public EventRequest(final Handler handler, final byte[] array, final RemoteDevice.PostEventHandler eventCompleteHandler) {
        this.eventData = array.clone();
        this.eventCompleteHandler = eventCompleteHandler;
        this.handler = handler;
    }
    
    public void callCompletionHandlers(final RemoteDevice.PostEventStatus postEventStatus) {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                if (EventRequest.this.eventCompleteHandler != null) {
                    EventRequest.this.eventCompleteHandler.onComplete(postEventStatus);
                }
            }
        });
    }
}
