package com.navdy.service.library.device;

import java.util.Collection;
import android.bluetooth.BluetoothDevice;
import java.util.Iterator;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.device.connection.ServiceAddress;
import com.navdy.service.library.device.connection.ConnectionService;
import com.navdy.service.library.device.connection.BTConnectionInfo;
import java.util.ListIterator;
import java.lang.reflect.Type;
import com.google.gson.JsonParseException;
import java.util.Arrays;
import java.util.List;
import com.google.gson.reflect.TypeToken;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import android.content.SharedPreferences;
import java.util.HashSet;
import com.navdy.service.library.device.connection.ConnectionInfo;
import java.util.Set;
import com.google.gson.Gson;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.device.discovery.RemoteDeviceScanner;
import com.navdy.service.library.util.Listenable;

public class RemoteDeviceRegistry extends Listenable<DeviceListUpdatedListener> implements RemoteDeviceScanner.Listener
{
    public static final int MAX_PAIRED_DEVICES = 5;
    public static final String PREFS_FILE_DEVICE_REGISTRY = "DeviceRegistry";
    public static final String PREFS_KEY_DEFAULT_CONNECTION_INFO = "DefaultConnectionInfo";
    public static final String PREFS_KEY_PAIRED_CONNECTION_INFOS = "PairedConnectionInfos";
    public static final Logger sLogger;
    private static RemoteDeviceRegistry sRemoteDeviceRegistry;
    protected Context mContext;
    protected Gson mGson;
    protected Set<ConnectionInfo> mKnownConnectionInfo;
    protected HashSet<RemoteDeviceScanner> mRemoteDeviceScanners;
    protected SharedPreferences mSharedPrefs;
    private ArrayList<ConnectionInfo> pairedConnections;
    
    static {
        sLogger = new Logger(RemoteDeviceRegistry.class);
    }
    
    private RemoteDeviceRegistry(final Context mContext) {
        this.mContext = mContext;
        this.mGson = new GsonBuilder().registerTypeAdapterFactory(ConnectionInfo.connectionInfoAdapter).create();
        this.mKnownConnectionInfo = new HashSet<ConnectionInfo>();
        this.mRemoteDeviceScanners = new HashSet<RemoteDeviceScanner>();
        this.loadPairedConnections();
    }
    
    public static RemoteDeviceRegistry getInstance(final Context context) {
        if (RemoteDeviceRegistry.sRemoteDeviceRegistry == null) {
            RemoteDeviceRegistry.sRemoteDeviceRegistry = new RemoteDeviceRegistry(context.getApplicationContext());
        }
        return RemoteDeviceRegistry.sRemoteDeviceRegistry;
    }
    
    private void loadPairedConnections() {
        this.mSharedPrefs = this.mContext.getSharedPreferences("DeviceRegistry", 4);
        final ArrayList<ConnectionInfo> list = new ArrayList<ConnectionInfo>();
        while (true) {
            try {
                final String string = this.mSharedPrefs.getString("PairedConnectionInfos", (String)null);
                final Type type = new TypeToken<List<ConnectionInfo>>() {}.getType();
                ArrayList<ConnectionInfo> pairedConnections = list;
                if (string != null) {
                    pairedConnections = this.mGson.<ArrayList<ConnectionInfo>>fromJson(string, type);
                }
                RemoteDeviceRegistry.sLogger.v("Read pairing list of:" + Arrays.toString(pairedConnections.toArray()));
                this.pairedConnections = pairedConnections;
            }
            catch (JsonParseException ex) {
                RemoteDeviceRegistry.sLogger.e("Unable to read connection infos: ", ex);
                final ArrayList<ConnectionInfo> pairedConnections = list;
                continue;
            }
            break;
        }
    }
    
    public void addDiscoveredConnectionInfo(final ConnectionInfo connectionInfo) {
        if (this.mKnownConnectionInfo.contains(connectionInfo)) {
            RemoteDeviceRegistry.sLogger.d("Already contains: " + connectionInfo);
        }
        else {
            this.mKnownConnectionInfo.add(connectionInfo);
            this.sendDeviceListUpdate();
        }
    }
    
    public void addPairedConnection(final ConnectionInfo connectionInfo) {
        this.pairedConnections.remove(connectionInfo);
        if (this.pairedConnections.size() >= 5) {
            this.pairedConnections.remove(4);
        }
        this.pairedConnections.add(0, connectionInfo);
        this.savePairedConnections();
    }
    
    public void addRemoteDeviceScanner(final RemoteDeviceScanner remoteDeviceScanner) {
        remoteDeviceScanner.addListener((RemoteDeviceScanner.Listener)this);
        this.mRemoteDeviceScanners.add(remoteDeviceScanner);
    }
    
    public ConnectionInfo findDevice(final NavdyDeviceId navdyDeviceId) {
        final ListIterator<ConnectionInfo> listIterator = this.pairedConnections.listIterator();
        while (listIterator.hasNext()) {
            final ConnectionInfo connectionInfo = listIterator.next();
            if (connectionInfo.getDeviceId().equals(navdyDeviceId)) {
                return connectionInfo;
            }
        }
        return null;
    }
    
    public ConnectionInfo getDefaultConnectionInfo() {
        final ConnectionInfo connectionInfo = null;
        ConnectionInfo connectionInfo2 = null;
        try {
            final String string = this.mSharedPrefs.getString("DefaultConnectionInfo", (String)null);
            if (string == null) {
                connectionInfo2 = connectionInfo;
            }
            else {
                connectionInfo2 = this.mGson.<ConnectionInfo>fromJson(string, ConnectionInfo.class);
            }
            return connectionInfo2;
        }
        catch (JsonParseException ex) {
            RemoteDeviceRegistry.sLogger.e("Unable to read connection info: ", ex);
            return connectionInfo2;
        }
    }
    
    public Set<ConnectionInfo> getKnownConnectionInfo() {
        return this.mKnownConnectionInfo;
    }
    
    public ConnectionInfo getLastPairedDevice() {
        try {
            ConnectionInfo connectionInfo;
            if (this.pairedConnections.size() > 0) {
                connectionInfo = this.pairedConnections.get(0);
            }
            else {
                connectionInfo = null;
            }
            return connectionInfo;
        }
        catch (Throwable t) {
            return null;
        }
    }
    
    public List<ConnectionInfo> getPairedConnections() {
        return this.pairedConnections;
    }
    
    public boolean hasPaired() {
        return this.pairedConnections.size() > 0;
    }
    
    @Override
    public void onDiscovered(final RemoteDeviceScanner remoteDeviceScanner, final List<ConnectionInfo> list) {
        for (final ConnectionInfo connectionInfo : list) {
            if (connectionInfo == null) {
                RemoteDeviceRegistry.sLogger.e("missing connection info.");
            }
            else {
                this.addDiscoveredConnectionInfo(connectionInfo);
                if (connectionInfo instanceof BTConnectionInfo || connectionInfo.getDeviceId().getBluetoothAddress() == null) {
                    continue;
                }
                this.addDiscoveredConnectionInfo(new BTConnectionInfo(connectionInfo.getDeviceId(), new ServiceAddress(connectionInfo.getDeviceId().getBluetoothAddress(), ConnectionService.NAVDY_PROTO_SERVICE_UUID.toString()), ConnectionType.BT_PROTOBUF));
            }
        }
    }
    
    @Override
    public void onLost(final RemoteDeviceScanner remoteDeviceScanner, final List<ConnectionInfo> list) {
    }
    
    @Override
    public void onScanStarted(final RemoteDeviceScanner remoteDeviceScanner) {
        RemoteDeviceRegistry.sLogger.e("Scan started: " + remoteDeviceScanner.toString());
    }
    
    @Override
    public void onScanStopped(final RemoteDeviceScanner remoteDeviceScanner) {
        RemoteDeviceRegistry.sLogger.e("Scan stopped: " + remoteDeviceScanner.toString());
    }
    
    public void refresh() {
        RemoteDeviceRegistry.sLogger.i("refreshing paired connections info");
        this.loadPairedConnections();
    }
    
    public void removePairedConnection(final BluetoothDevice bluetoothDevice) {
        final ListIterator<ConnectionInfo> listIterator = this.pairedConnections.listIterator();
        while (listIterator.hasNext()) {
            final ConnectionInfo connectionInfo = listIterator.next();
            if (connectionInfo instanceof BTConnectionInfo && connectionInfo.getAddress().getAddress().equals(bluetoothDevice.getAddress())) {
                listIterator.remove();
            }
        }
        this.savePairedConnections();
    }
    
    public void removePairedConnection(final ConnectionInfo connectionInfo) {
        this.pairedConnections.remove(connectionInfo);
        this.savePairedConnections();
    }
    
    public void removeRemoteDeviceScanner(final RemoteDeviceScanner remoteDeviceScanner) {
        remoteDeviceScanner.removeListener((RemoteDeviceScanner.Listener)this);
        this.mRemoteDeviceScanners.remove(remoteDeviceScanner);
    }
    
    public void savePairedConnections() {
        try {
            this.mSharedPrefs.edit().putString("PairedConnectionInfos", this.mGson.toJson(this.pairedConnections, new TypeToken<List<ConnectionInfo>>() {}.getType())).commit();
        }
        catch (Exception ex) {
            RemoteDeviceRegistry.sLogger.e("Exception saving paired connections", ex);
        }
    }
    
    protected void sendDeviceListUpdate() {
        this.dispatchToListeners((EventDispatcher)new DeviceListEventDispatcher() {
            public void dispatchEvent(final RemoteDeviceRegistry remoteDeviceRegistry, final DeviceListUpdatedListener deviceListUpdatedListener) {
                deviceListUpdatedListener.onDeviceListChanged(new HashSet<ConnectionInfo>(RemoteDeviceRegistry.this.mKnownConnectionInfo));
            }
        });
    }
    
    public void setDefaultConnectionInfo(final ConnectionInfo connectionInfo) {
        try {
            this.mSharedPrefs.edit().putString("DefaultConnectionInfo", this.mGson.toJson(connectionInfo, ConnectionInfo.class)).apply();
        }
        catch (Exception ex) {
            RemoteDeviceRegistry.sLogger.e("Exception " + ex);
        }
    }
    
    public void startScanning() {
        this.mKnownConnectionInfo.clear();
        final Iterator<RemoteDeviceScanner> iterator = this.mRemoteDeviceScanners.iterator();
        while (iterator.hasNext()) {
            iterator.next().startScan();
        }
    }
    
    public void stopScanning() {
        final Iterator<RemoteDeviceScanner> iterator = this.mRemoteDeviceScanners.iterator();
        while (iterator.hasNext()) {
            iterator.next().stopScan();
        }
    }
    
    protected interface DeviceListEventDispatcher extends EventDispatcher<RemoteDeviceRegistry, DeviceListUpdatedListener>
    {
    }
    
    public interface DeviceListUpdatedListener extends Listenable.Listener
    {
        void onDeviceListChanged(final Set<ConnectionInfo> p0);
    }
}
