package com.navdy.service.library.device.connection;

import java.io.IOException;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.Listenable;

public abstract class ConnectionListener extends Listenable
{
    protected final Logger logger;
    AcceptThread mAcceptThread;
    protected final Context mContext;
    
    public ConnectionListener(final Context mContext, final String s) {
        this.mContext = mContext;
        this.logger = new Logger(s);
    }
    
    public void dispatchConnected(final Connection connection) {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final ConnectionListener connectionListener, final ConnectionListener.Listener listener) {
                listener.onConnected(connectionListener, connection);
            }
        });
    }
    
    public void dispatchConnectionFailed() {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final ConnectionListener connectionListener, final ConnectionListener.Listener listener) {
                listener.onConnectionFailed(connectionListener);
            }
        });
    }
    
    public void dispatchStartFailure() {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final ConnectionListener connectionListener, final ConnectionListener.Listener listener) {
                listener.onStartFailure(connectionListener);
            }
        });
    }
    
    public void dispatchStarted() {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final ConnectionListener connectionListener, final ConnectionListener.Listener listener) {
                listener.onStarted(connectionListener);
            }
        });
    }
    
    public void dispatchStopped() {
        this.dispatchToListeners((Listenable.EventDispatcher)new EventDispatcher() {
            public void dispatchEvent(final ConnectionListener connectionListener, final ConnectionListener.Listener listener) {
                listener.onStopped(connectionListener);
            }
        });
    }
    
    protected abstract AcceptThread getNewAcceptThread() throws IOException;
    
    public abstract ConnectionType getType();
    
    public boolean start() {
        boolean b = false;
        synchronized (this) {
            Label_0057: {
                if (this.mAcceptThread == null) {
                    break Label_0057;
                }
                Block_5: {
                    if (!this.mAcceptThread.isAlive()) {
                        this.logger.e("clearing existing accept thread");
                        this.mAcceptThread.cancel();
                        this.mAcceptThread = null;
                        break Block_5;
                    }
                    this.logger.e("Already running");
                    return b;
                }
                try {
                    this.mAcceptThread = this.getNewAcceptThread();
                    if (this.mAcceptThread != null) {
                        this.mAcceptThread.start();
                    }
                    b = true;
                }
                catch (Throwable t) {
                    this.logger.e("Unable to start accept thread: ", t);
                    this.dispatchStartFailure();
                }
            }
            return b;
        }
    }
    
    public boolean stop() {
        synchronized (this) {
            boolean b;
            if (this.mAcceptThread == null) {
                this.logger.e("Already stopped.");
                b = false;
            }
            else {
                this.mAcceptThread.cancel();
                this.mAcceptThread = null;
                b = true;
            }
            return b;
        }
    }
    
    protected abstract class AcceptThread extends Thread
    {
        abstract void cancel();
    }
    
    protected interface EventDispatcher extends Listenable.EventDispatcher<ConnectionListener, ConnectionListener.Listener>
    {
    }
    
    public interface Listener extends Listenable.Listener
    {
        void onConnected(final ConnectionListener p0, final Connection p1);
        
        void onConnectionFailed(final ConnectionListener p0);
        
        void onStartFailure(final ConnectionListener p0);
        
        void onStarted(final ConnectionListener p0);
        
        void onStopped(final ConnectionListener p0);
    }
}
