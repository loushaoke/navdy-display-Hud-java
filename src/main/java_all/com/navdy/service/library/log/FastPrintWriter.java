package com.navdy.service.library.log;

import java.io.UnsupportedEncodingException;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.Charset;
import java.nio.charset.CoderResult;
import java.nio.CharBuffer;
import android.os.Build;
import java.io.IOException;
import android.util.Printer;
import java.io.OutputStream;
import java.nio.charset.CharsetEncoder;
import java.nio.ByteBuffer;
import java.io.Writer;
import java.io.PrintWriter;

public class FastPrintWriter extends PrintWriter
{
    private static Writer sDummyWriter;
    private final boolean mAutoFlush;
    private final int mBufferLen;
    private final ByteBuffer mBytes;
    private CharsetEncoder mCharset;
    private boolean mIoError;
    private final OutputStream mOutputStream;
    private int mPos;
    private final Printer mPrinter;
    private final String mSeparator;
    private final char[] mText;
    private final Writer mWriter;
    
    static {
        FastPrintWriter.sDummyWriter = new Writer() {
            @Override
            public void close() throws IOException {
                throw new UnsupportedOperationException("Shouldn't be here");
            }
            
            @Override
            public void flush() throws IOException {
                this.close();
            }
            
            @Override
            public void write(final char[] array, final int n, final int n2) throws IOException {
                this.close();
            }
        };
    }
    
    public FastPrintWriter(final Printer printer) {
        this(printer, 512);
    }
    
    public FastPrintWriter(final Printer mPrinter, final int mBufferLen) {
        super(FastPrintWriter.sDummyWriter, true);
        if (mPrinter == null) {
            throw new NullPointerException("pr is null");
        }
        this.mBufferLen = mBufferLen;
        this.mText = new char[mBufferLen];
        this.mBytes = null;
        this.mOutputStream = null;
        this.mWriter = null;
        this.mPrinter = mPrinter;
        this.mAutoFlush = true;
        if (Build$VERSION.SDK_INT >= 19) {
            this.mSeparator = System.lineSeparator();
        }
        else {
            this.mSeparator = "\n";
        }
        this.initDefaultEncoder();
    }
    
    public FastPrintWriter(final OutputStream outputStream) {
        this(outputStream, false, 8192);
    }
    
    public FastPrintWriter(final OutputStream outputStream, final boolean b) {
        this(outputStream, b, 8192);
    }
    
    public FastPrintWriter(final OutputStream mOutputStream, final boolean mAutoFlush, final int mBufferLen) {
        super(FastPrintWriter.sDummyWriter, mAutoFlush);
        if (mOutputStream == null) {
            throw new NullPointerException("out is null");
        }
        this.mBufferLen = mBufferLen;
        this.mText = new char[mBufferLen];
        this.mBytes = ByteBuffer.allocate(this.mBufferLen);
        this.mOutputStream = mOutputStream;
        this.mWriter = null;
        this.mPrinter = null;
        this.mAutoFlush = mAutoFlush;
        if (Build$VERSION.SDK_INT >= 19) {
            this.mSeparator = System.lineSeparator();
        }
        else {
            this.mSeparator = "\n";
        }
        this.initDefaultEncoder();
    }
    
    public FastPrintWriter(final Writer writer) {
        this(writer, false, 8192);
    }
    
    public FastPrintWriter(final Writer writer, final boolean b) {
        this(writer, b, 8192);
    }
    
    public FastPrintWriter(final Writer mWriter, final boolean mAutoFlush, final int mBufferLen) {
        super(FastPrintWriter.sDummyWriter, mAutoFlush);
        if (mWriter == null) {
            throw new NullPointerException("wr is null");
        }
        this.mBufferLen = mBufferLen;
        this.mText = new char[mBufferLen];
        this.mBytes = null;
        this.mOutputStream = null;
        this.mWriter = mWriter;
        this.mPrinter = null;
        this.mAutoFlush = mAutoFlush;
        if (Build$VERSION.SDK_INT >= 19) {
            this.mSeparator = System.lineSeparator();
        }
        else {
            this.mSeparator = "\n";
        }
        this.initDefaultEncoder();
    }
    
    private void appendLocked(final char c) throws IOException {
        int n;
        if ((n = this.mPos) >= this.mBufferLen - 1) {
            this.flushLocked();
            n = this.mPos;
        }
        this.mText[n] = c;
        this.mPos = n + 1;
    }
    
    private void appendLocked(final String s, int i, int n) throws IOException {
        final int mBufferLen = this.mBufferLen;
        if (n > mBufferLen) {
            int n3;
            for (int n2 = i + n; i < n2; i = n3) {
                n3 = i + mBufferLen;
                if (n3 < n2) {
                    n = mBufferLen;
                }
                else {
                    n = n2 - i;
                }
                this.appendLocked(s, i, n);
            }
        }
        else {
            int n4;
            if ((n4 = this.mPos) + n > mBufferLen) {
                this.flushLocked();
                n4 = this.mPos;
            }
            s.getChars(i, i + n, this.mText, n4);
            this.mPos = n4 + n;
        }
    }
    
    private void appendLocked(final char[] array, int i, int n) throws IOException {
        final int mBufferLen = this.mBufferLen;
        if (n > mBufferLen) {
            int n3;
            for (int n2 = i + n; i < n2; i = n3) {
                n3 = i + mBufferLen;
                if (n3 < n2) {
                    n = mBufferLen;
                }
                else {
                    n = n2 - i;
                }
                this.appendLocked(array, i, n);
            }
        }
        else {
            int n4;
            if ((n4 = this.mPos) + n > mBufferLen) {
                this.flushLocked();
                n4 = this.mPos;
            }
            System.arraycopy(array, i, this.mText, n4, n);
            this.mPos = n4 + n;
        }
    }
    
    private void flushBytesLocked() throws IOException {
        final int position = this.mBytes.position();
        if (position > 0) {
            this.mBytes.flip();
            this.mOutputStream.write(this.mBytes.array(), 0, position);
            this.mBytes.clear();
        }
    }
    
    private void flushLocked() throws IOException {
        if (this.mPos > 0) {
            Label_0099: {
                if (this.mOutputStream != null) {
                    CharBuffer wrap;
                    CoderResult coderResult;
                    for (wrap = CharBuffer.wrap(this.mText, 0, this.mPos), coderResult = this.mCharset.encode(wrap, this.mBytes, true); !coderResult.isError(); coderResult = this.mCharset.encode(wrap, this.mBytes, true)) {
                        if (!coderResult.isOverflow()) {
                            this.flushBytesLocked();
                            this.mOutputStream.flush();
                            break Label_0099;
                        }
                        this.flushBytesLocked();
                    }
                    throw new IOException(coderResult.toString());
                }
                if (this.mWriter != null) {
                    this.mWriter.write(this.mText, 0, this.mPos);
                    this.mWriter.flush();
                }
                else {
                    int n = 0;
                    int n2 = this.mSeparator.length();
                    if (n2 >= this.mPos) {
                        n2 = this.mPos;
                    }
                    while (n < n2 && this.mText[this.mPos - 1 - n] == this.mSeparator.charAt(this.mSeparator.length() - 1 - n)) {
                        ++n;
                    }
                    if (n >= this.mPos) {
                        this.mPrinter.println("");
                    }
                    else {
                        this.mPrinter.println(new String(this.mText, 0, this.mPos - n));
                    }
                }
            }
            this.mPos = 0;
        }
    }
    
    private final void initDefaultEncoder() {
        (this.mCharset = Charset.defaultCharset().newEncoder()).onMalformedInput(CodingErrorAction.REPLACE);
        this.mCharset.onUnmappableCharacter(CodingErrorAction.REPLACE);
    }
    
    private final void initEncoder(final String s) throws UnsupportedEncodingException {
        try {
            (this.mCharset = Charset.forName(s).newEncoder()).onMalformedInput(CodingErrorAction.REPLACE);
            this.mCharset.onUnmappableCharacter(CodingErrorAction.REPLACE);
        }
        catch (Exception ex) {
            throw new UnsupportedEncodingException(s);
        }
    }
    
    @Override
    public PrintWriter append(final CharSequence charSequence, final int n, final int n2) {
        CharSequence charSequence2 = charSequence;
        if (charSequence == null) {
            charSequence2 = "null";
        }
        final String string = charSequence2.subSequence(n, n2).toString();
        this.write(string, 0, string.length());
        return this;
    }
    
    @Override
    public boolean checkError() {
        this.flush();
        final Object lock = this.lock;
        synchronized (lock) {
            return this.mIoError;
        }
    }
    
    @Override
    protected void clearError() {
        final Object lock = this.lock;
        synchronized (lock) {
            this.mIoError = false;
        }
    }
    
    @Override
    public void close() {
        final Object lock = this.lock;
        synchronized (lock) {
            try {
                this.flushLocked();
                if (this.mOutputStream != null) {
                    this.mOutputStream.close();
                }
                else if (this.mWriter != null) {
                    this.mWriter.close();
                }
            }
            catch (IOException ex) {
                this.setError();
            }
        }
    }
    
    @Override
    public void flush() {
        final Object lock = this.lock;
        synchronized (lock) {
            try {
                this.flushLocked();
                if (this.mOutputStream != null) {
                    this.mOutputStream.flush();
                }
                else if (this.mWriter != null) {
                    this.mWriter.flush();
                }
            }
            catch (IOException ex) {
                this.setError();
            }
        }
    }
    
    @Override
    public void print(final char c) {
        // monitorenter(lock = this.lock)
        try {
            try {
                this.appendLocked(c);
            }
            finally {
            }
            // monitorexit(lock)
        }
        catch (IOException ex) {}
    }
    
    @Override
    public void print(final int n) {
        if (n == 0) {
            this.print("0");
        }
        else {
            super.print(n);
        }
    }
    
    @Override
    public void print(final long n) {
        if (n == 0L) {
            this.print("0");
        }
        else {
            super.print(n);
        }
    }
    
    @Override
    public void print(final String s) {
        String value = s;
        if (s == null) {
            value = String.valueOf(null);
        }
        final Object lock = this.lock;
        synchronized (lock) {
            try {
                this.appendLocked(value, 0, value.length());
            }
            catch (IOException ex) {
                this.setError();
            }
        }
    }
    
    @Override
    public void print(final char[] array) {
        // monitorenter(lock = this.lock)
        try {
            try {
                this.appendLocked(array, 0, array.length);
            }
            finally {
            }
            // monitorexit(lock)
        }
        catch (IOException ex) {}
    }
    
    @Override
    public void println() {
        final Object lock = this.lock;
        synchronized (lock) {
            try {
                this.appendLocked(this.mSeparator, 0, this.mSeparator.length());
                if (this.mAutoFlush) {
                    this.flushLocked();
                }
            }
            catch (IOException ex) {
                this.setError();
            }
        }
    }
    
    @Override
    public void println(final char c) {
        this.print(c);
        this.println();
    }
    
    @Override
    public void println(final int n) {
        if (n == 0) {
            this.println("0");
        }
        else {
            super.println(n);
        }
    }
    
    @Override
    public void println(final long n) {
        if (n == 0L) {
            this.println("0");
        }
        else {
            super.println(n);
        }
    }
    
    @Override
    public void println(final char[] array) {
        this.print(array);
        this.println();
    }
    
    @Override
    protected void setError() {
        final Object lock = this.lock;
        synchronized (lock) {
            this.mIoError = true;
        }
    }
    
    @Override
    public void write(final int n) {
        // monitorenter(lock = this.lock)
        final char c = (char)n;
        try {
            try {
                this.appendLocked(c);
            }
            finally {
            }
            // monitorexit(lock)
        }
        catch (IOException ex) {}
    }
    
    @Override
    public void write(final String s) {
        // monitorenter(lock = this.lock)
        try {
            try {
                this.appendLocked(s, 0, s.length());
            }
            finally {
            }
            // monitorexit(lock)
        }
        catch (IOException ex) {}
    }
    
    @Override
    public void write(final String s, final int n, final int n2) {
        // monitorenter(lock = this.lock)
        try {
            try {
                this.appendLocked(s, n, n2);
            }
            finally {
            }
            // monitorexit(lock)
        }
        catch (IOException ex) {}
    }
    
    @Override
    public void write(final char[] array, final int n, final int n2) {
        // monitorenter(lock = this.lock)
        try {
            try {
                this.appendLocked(array, n, n2);
            }
            finally {
            }
            // monitorexit(lock)
        }
        catch (IOException ex) {}
    }
}
