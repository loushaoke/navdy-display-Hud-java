package com.navdy.service.library.log;

import android.util.Log;

public class LogcatAppender implements LogAppender
{
    @Override
    public void close() {
    }
    
    @Override
    public void d(final String s, final String s2) {
        Log.d(s, s2);
    }
    
    @Override
    public void d(final String s, final String s2, final Throwable t) {
        Log.d(s, s2, t);
    }
    
    @Override
    public void e(final String s, final String s2) {
        Log.e(s, s2);
    }
    
    @Override
    public void e(final String s, final String s2, final Throwable t) {
        Log.e(s, s2, t);
    }
    
    @Override
    public void flush() {
    }
    
    @Override
    public void i(final String s, final String s2) {
        Log.i(s, s2);
    }
    
    @Override
    public void i(final String s, final String s2, final Throwable t) {
        Log.i(s, s2, t);
    }
    
    @Override
    public void v(final String s, final String s2) {
        Log.v(s, s2);
    }
    
    @Override
    public void v(final String s, final String s2, final Throwable t) {
        Log.v(s, s2, t);
    }
    
    @Override
    public void w(final String s, final String s2) {
        Log.w(s, s2);
    }
    
    @Override
    public void w(final String s, final String s2, final Throwable t) {
        Log.w(s, s2, t);
    }
}
