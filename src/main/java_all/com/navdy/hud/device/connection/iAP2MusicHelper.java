package com.navdy.hud.device.connection;

import com.navdy.service.library.events.input.KeyEvent;
import com.navdy.hud.mfi.NowPlayingUpdate;
import android.graphics.Bitmap;
import okio.ByteString;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.task.TaskManager;
import com.squareup.wire.Message;
import com.navdy.service.library.events.photo.PhotoType;
import java.io.Serializable;
import android.text.TextUtils;
import android.os.SystemClock;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.photo.PhotoUpdatesRequest;
import com.navdy.service.library.events.input.MediaRemoteKeyEvent;
import com.navdy.hud.mfi.IIAPFileTransferManager;
import com.navdy.hud.mfi.iAPProcessor;
import com.navdy.service.library.events.audio.MusicArtworkRequest;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import android.util.LruCache;
import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.hud.mfi.IAPFileTransferManager;
import com.navdy.hud.mfi.IAPMusicManager;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.mfi.IAPFileTransferListener;
import com.navdy.hud.mfi.IAPNowPlayingUpdateListener;

class iAP2MusicHelper implements IAPNowPlayingUpdateListener, IAPFileTransferListener
{
    private static final int LIMITED_BANDWIDTH_FILE_TRANSFER_LIMIT = 10000;
    private static final Logger sLogger;
    private boolean fileTransferCanceled;
    private final Object helperStateLock;
    private IAPMusicManager iAPMusicManager;
    private IAPFileTransferManager iapFileTransferManager;
    private long keyDownTime;
    private MusicArtworkResponse lastMusicArtworkResponse;
    private iAP2Link link;
    private int nowPlayingFileTransferIdentifier;
    private LruCache<Integer, MusicTrackInfo> transferIdTrackInfoMap;
    private MusicArtworkRequest waitingArtworkRequest;
    private FileHolder waitingFile;
    private int waitingFileTransferIdentifier;
    
    static {
        sLogger = new Logger(iAP2MusicHelper.class);
    }
    
    iAP2MusicHelper(final iAP2Link link, final iAPProcessor iapProcessor) {
        this.nowPlayingFileTransferIdentifier = -1;
        this.waitingFileTransferIdentifier = -1;
        this.fileTransferCanceled = false;
        this.waitingArtworkRequest = null;
        this.keyDownTime = 0L;
        this.transferIdTrackInfoMap = (LruCache<Integer, MusicTrackInfo>)new LruCache(5);
        this.helperStateLock = new Object();
        this.link = link;
        iapProcessor.connect(this.iapFileTransferManager = new IAPFileTransferManager(iapProcessor));
        (this.iAPMusicManager = new IAPMusicManager(iapProcessor)).setNowPlayingUpdateListener(this);
        this.addEventProcessors();
    }
    
    private void addEventProcessors() {
        this.link.addEventProcessor(NavdyEvent.MessageType.MediaRemoteKeyEvent, (iAP2Link.NavdyEventProcessor)new iAP2Link.NavdyEventProcessor() {
            @Override
            public boolean processNavdyEvent(final NavdyEvent navdyEvent) {
                iAP2MusicHelper.this.onMediaRemoteKeyEvent(navdyEvent.<MediaRemoteKeyEvent>getExtension(Ext_NavdyEvent.mediaRemoteKeyEvent));
                return true;
            }
        });
        this.link.addEventProcessor(NavdyEvent.MessageType.NowPlayingUpdateRequest, (iAP2Link.NavdyEventProcessor)new iAP2Link.NavdyEventProcessor() {
            @Override
            public boolean processNavdyEvent(final NavdyEvent navdyEvent) {
                return true;
            }
        });
        this.link.addEventProcessor(NavdyEvent.MessageType.PhotoUpdatesRequest, (iAP2Link.NavdyEventProcessor)new iAP2Link.NavdyEventProcessor() {
            @Override
            public boolean processNavdyEvent(final NavdyEvent navdyEvent) {
                iAP2MusicHelper.sLogger.d("got PhotoUpdatesRequest");
                iAP2MusicHelper.this.onPhotoUpdatesRequest(navdyEvent.<PhotoUpdatesRequest>getExtension(Ext_NavdyEvent.photoUpdateRequest));
                return true;
            }
        });
        this.link.addEventProcessor(NavdyEvent.MessageType.MusicArtworkRequest, (iAP2Link.NavdyEventProcessor)new iAP2Link.NavdyEventProcessor() {
            @Override
            public boolean processNavdyEvent(final NavdyEvent navdyEvent) {
                return iAP2MusicHelper.this.onMusicArtworkRequest(navdyEvent.<MusicArtworkRequest>getExtension(Ext_NavdyEvent.musicArtworkRequest));
            }
        });
    }
    
    private MusicTrackInfo getLastMusicTrackInfo() {
        final Object helperStateLock = this.helperStateLock;
        synchronized (helperStateLock) {
            return (MusicTrackInfo)this.transferIdTrackInfoMap.get(this.nowPlayingFileTransferIdentifier);
        }
    }
    
    private void onMediaRemoteKeyEvent(final MediaRemoteKeyEvent mediaRemoteKeyEvent) {
        iAP2MusicHelper.sLogger.d("(MFi) Media Key : " + mediaRemoteKeyEvent.key + " , " + mediaRemoteKeyEvent.action);
        switch (mediaRemoteKeyEvent.action) {
            case KEY_DOWN:
                this.keyDownTime = SystemClock.elapsedRealtime();
                this.iAPMusicManager.onKeyDown(mediaRemoteKeyEvent.key.ordinal());
                break;
            case KEY_UP:
                if (SystemClock.elapsedRealtime() - this.keyDownTime <= 500L) {
                    this.iapFileTransferManager.cancel();
                }
                this.keyDownTime = 0L;
                this.iAPMusicManager.onKeyUp(mediaRemoteKeyEvent.key.ordinal());
                break;
        }
    }
    
    private boolean onMusicArtworkRequest(final MusicArtworkRequest musicArtworkRequest) {
        while (true) {
            boolean b = true;
            Label_0320: {
                Label_0296: {
                    synchronized (this) {
                        final MusicTrackInfo lastMusicTrackInfo = this.getLastMusicTrackInfo();
                        iAP2MusicHelper.sLogger.d("onMusicArtworkRequest: " + musicArtworkRequest + ", musicTrackInfo: " + lastMusicTrackInfo);
                        if (musicArtworkRequest == null || (TextUtils.isEmpty((CharSequence)musicArtworkRequest.name) && TextUtils.isEmpty((CharSequence)musicArtworkRequest.album) && TextUtils.isEmpty((CharSequence)musicArtworkRequest.author))) {
                            return false;
                        }
                        // monitorenter(helperStateLock = this.helperStateLock)
                        if (lastMusicTrackInfo == null) {
                            break Label_0320;
                        }
                        try {
                            if (this.sameTrack(musicArtworkRequest, lastMusicTrackInfo) && this.nowPlayingFileTransferIdentifier == this.waitingFileTransferIdentifier) {
                                final Logger sLogger = iAP2MusicHelper.sLogger;
                                final StringBuilder append = new StringBuilder().append("onMusicArtworkRequest, waiting file: ");
                                Serializable value;
                                if (this.waitingFile != null) {
                                    value = this.waitingFile.size();
                                }
                                else {
                                    value = "null";
                                }
                                sLogger.d(append.append(value).toString());
                                if (this.waitingFile != null) {
                                    iAP2MusicHelper.sLogger.d("onMusicArtworkRequest using waiting file " + this.waitingFileTransferIdentifier);
                                    this.sendArtworkResponse(this.waitingFileTransferIdentifier, this.waitingFile.data, lastMusicTrackInfo);
                                }
                                else {
                                    if (!this.fileTransferCanceled) {
                                        break Label_0296;
                                    }
                                    iAP2MusicHelper.sLogger.d("onMusicArtworkRequest, The file transfer has been canceled, send an empty artwork response");
                                    this.sendArtworkResponse(this.waitingFileTransferIdentifier, null, lastMusicTrackInfo);
                                }
                                return b;
                            }
                            break Label_0320;
                        }
                        finally {
                        }
                        // monitorexit(helperStateLock)
                    }
                }
                iAP2MusicHelper.sLogger.d("onMusicArtworkRequest continuing file transfer");
                this.iapFileTransferManager.onFileTransferSetupResponse(this.nowPlayingFileTransferIdentifier, true);
                return b;
            }
            final MusicArtworkRequest waitingArtworkRequest;
            this.waitingArtworkRequest = waitingArtworkRequest;
            iAP2MusicHelper.sLogger.d("onMusicArtworkRequest waiting for file transfer, waitingArtworkRequest: " + this.waitingArtworkRequest);
            return b;
            b = false;
            return b;
        }
    }
    
    private void onPhotoUpdatesRequest(final PhotoUpdatesRequest photoUpdatesRequest) {
        final boolean b = photoUpdatesRequest.photoType == PhotoType.PHOTO_ALBUM_ART && photoUpdatesRequest.start != null && photoUpdatesRequest.start;
        iAP2MusicHelper.sLogger.d("onPhotoUpdatesRequest: " + b + ", " + photoUpdatesRequest);
        if (b) {
            this.link.sendMessageAsNavdyEvent(this.lastMusicArtworkResponse);
        }
    }
    
    private boolean sameTrack(final MusicArtworkRequest musicArtworkRequest, final MusicTrackInfo musicTrackInfo) {
        return musicArtworkRequest != null && musicTrackInfo != null && TextUtils.equals((CharSequence)musicArtworkRequest.name, (CharSequence)musicTrackInfo.name) && TextUtils.equals((CharSequence)musicArtworkRequest.album, (CharSequence)musicTrackInfo.album) && TextUtils.equals((CharSequence)musicArtworkRequest.author, (CharSequence)musicTrackInfo.author);
    }
    
    private void sendArtworkResponse(final int n, final byte[] array, final MusicTrackInfo musicTrackInfo) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                iAP2MusicHelper.sLogger.d("onFileReceived task " + n);
                ByteString of;
                final ByteString byteString = of = null;
            Label_0090:
                while (true) {
                    if (array == null) {
                        break Label_0090;
                    }
                    of = byteString;
                    if (array.length <= 0) {
                        break Label_0090;
                    }
                    while (true) {
                        try {
                            final Bitmap decodeByteArray = ScalingUtilities.decodeByteArray(array, 200, 200, ScalingUtilities.ScalingLogic.FIT);
                            if (decodeByteArray != null) {
                                of = ByteString.of(ScalingUtilities.encodeByteArray(ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(decodeByteArray, 200, 200, ScalingUtilities.ScalingLogic.FIT)));
                            }
                            else {
                                iAP2MusicHelper.sLogger.e("Couldn't decode byte array to bitmap");
                                of = byteString;
                            }
                            iAP2MusicHelper.this.lastMusicArtworkResponse = new MusicArtworkResponse.Builder().name(musicTrackInfo.name).album(musicTrackInfo.album).author(musicTrackInfo.author).photo(of).build();
                            iAP2MusicHelper.sLogger.d("sending artworkResponse " + iAP2MusicHelper.this.lastMusicArtworkResponse);
                            iAP2MusicHelper.this.link.sendMessageAsNavdyEvent(iAP2MusicHelper.this.lastMusicArtworkResponse);
                            return;
                        }
                        catch (Exception ex) {
                            iAP2MusicHelper.sLogger.e("Error updating the art work received ", ex);
                            of = byteString;
                            continue Label_0090;
                        }
                        continue Label_0090;
                    }
                    break;
                }
            }
        }, 1);
    }
    
    void close() {
        this.keyDownTime = 0L;
    }
    
    @Override
    public void onFileReceived(final int p0, final byte[] p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: dup            
        //     2: astore_3       
        //     3: monitorenter   
        //     4: aload_0        
        //     5: getfield        com/navdy/hud/device/connection/iAP2MusicHelper.helperStateLock:Ljava/lang/Object;
        //     8: astore          4
        //    10: aload           4
        //    12: dup            
        //    13: astore          5
        //    15: monitorenter   
        //    16: getstatic       com/navdy/hud/device/connection/iAP2MusicHelper.sLogger:Lcom/navdy/service/library/log/Logger;
        //    19: astore          6
        //    21: new             Ljava/lang/StringBuilder;
        //    24: astore          7
        //    26: aload           7
        //    28: invokespecial   java/lang/StringBuilder.<init>:()V
        //    31: aload           7
        //    33: ldc_w           "onFileReceived: "
        //    36: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    39: iload_1        
        //    40: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    43: ldc_w           " ("
        //    46: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    49: astore          7
        //    51: aload_2        
        //    52: ifnull          127
        //    55: aload_2        
        //    56: arraylength    
        //    57: istore          8
        //    59: aload           6
        //    61: aload           7
        //    63: iload           8
        //    65: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    68: ldc_w           ")"
        //    71: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    74: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    77: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    80: aload_0        
        //    81: getfield        com/navdy/hud/device/connection/iAP2MusicHelper.transferIdTrackInfoMap:Landroid/util/LruCache;
        //    84: iload_1        
        //    85: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //    88: invokevirtual   android/util/LruCache.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    91: ifnonnull       133
        //    94: getstatic       com/navdy/hud/device/connection/iAP2MusicHelper.sLogger:Lcom/navdy/service/library/log/Logger;
        //    97: ldc_w           "onFileReceived: no music info for this file (yet?)"
        //   100: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   103: new             Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;
        //   106: astore          6
        //   108: aload           6
        //   110: aload_0        
        //   111: aload_2        
        //   112: invokespecial   com/navdy/hud/device/connection/iAP2MusicHelper$FileHolder.<init>:(Lcom/navdy/hud/device/connection/iAP2MusicHelper;[B)V
        //   115: aload_0        
        //   116: aload           6
        //   118: putfield        com/navdy/hud/device/connection/iAP2MusicHelper.waitingFile:Lcom/navdy/hud/device/connection/iAP2MusicHelper$FileHolder;
        //   121: aload           5
        //   123: monitorexit    
        //   124: aload_3        
        //   125: monitorexit    
        //   126: return         
        //   127: iconst_0       
        //   128: istore          8
        //   130: goto            59
        //   133: aload_0        
        //   134: getfield        com/navdy/hud/device/connection/iAP2MusicHelper.transferIdTrackInfoMap:Landroid/util/LruCache;
        //   137: iload_1        
        //   138: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   141: invokevirtual   android/util/LruCache.remove:(Ljava/lang/Object;)Ljava/lang/Object;
        //   144: checkcast       Lcom/navdy/service/library/events/audio/MusicTrackInfo;
        //   147: astore          6
        //   149: aload           5
        //   151: monitorexit    
        //   152: aload_0        
        //   153: iload_1        
        //   154: aload_2        
        //   155: aload           6
        //   157: invokespecial   com/navdy/hud/device/connection/iAP2MusicHelper.sendArtworkResponse:(I[BLcom/navdy/service/library/events/audio/MusicTrackInfo;)V
        //   160: goto            124
        //   163: astore_2       
        //   164: aload_3        
        //   165: monitorexit    
        //   166: aload_2        
        //   167: athrow         
        //   168: astore_2       
        //   169: aload           5
        //   171: monitorexit    
        //   172: aload_2        
        //   173: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  4      16     163    168    Any
        //  16     51     168    174    Any
        //  55     59     168    174    Any
        //  59     124    168    174    Any
        //  133    152    168    174    Any
        //  152    160    163    168    Any
        //  169    172    168    174    Any
        //  172    174    163    168    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0059:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void onFileTransferCancel(final int waitingFileTransferIdentifier) {
        synchronized (this) {
            final Object helperStateLock = this.helperStateLock;
            synchronized (helperStateLock) {
                iAP2MusicHelper.sLogger.d("onFileTransferCancel: " + waitingFileTransferIdentifier);
                final MusicTrackInfo musicTrackInfo = (MusicTrackInfo)this.transferIdTrackInfoMap.remove(waitingFileTransferIdentifier);
                if (this.waitingArtworkRequest != null && this.sameTrack(this.waitingArtworkRequest, musicTrackInfo) && this.nowPlayingFileTransferIdentifier == waitingFileTransferIdentifier) {
                    iAP2MusicHelper.sLogger.d("Sending response with null data");
                    this.sendArtworkResponse(waitingFileTransferIdentifier, null, musicTrackInfo);
                }
                else {
                    iAP2MusicHelper.sLogger.d("File transferred canceled, waiting for the meta data to send empty response");
                    this.fileTransferCanceled = true;
                    this.waitingFileTransferIdentifier = waitingFileTransferIdentifier;
                    this.waitingFile = null;
                }
            }
        }
    }
    
    @Override
    public void onFileTransferSetup(final int waitingFileTransferIdentifier, final long n) {
        final Object helperStateLock = this.helperStateLock;
        synchronized (helperStateLock) {
            iAP2MusicHelper.sLogger.d("onFileTransferSetup, fileTransferIdentifier: " + waitingFileTransferIdentifier + ", size: " + n + ", waitingArtworkRequest: " + this.waitingArtworkRequest + ", nowPlayingFileTransferIdentifier: " + this.nowPlayingFileTransferIdentifier);
            final MusicTrackInfo musicTrackInfo = (MusicTrackInfo)this.transferIdTrackInfoMap.get(waitingFileTransferIdentifier);
            this.fileTransferCanceled = false;
            if (this.waitingArtworkRequest != null && this.sameTrack(this.waitingArtworkRequest, musicTrackInfo) && this.nowPlayingFileTransferIdentifier == waitingFileTransferIdentifier) {
                iAP2MusicHelper.sLogger.d("onFileTransferSetup continuing file transfer");
                this.iapFileTransferManager.onFileTransferSetupResponse(this.nowPlayingFileTransferIdentifier, true);
            }
            else {
                iAP2MusicHelper.sLogger.d("onFileTransferSetup waiting for artwork request, waitingFileTransferIdentifier: " + waitingFileTransferIdentifier);
                this.waitingFileTransferIdentifier = waitingFileTransferIdentifier;
                this.waitingFile = null;
            }
        }
    }
    
    @Override
    public void onNowPlayingUpdate(final NowPlayingUpdate nowPlayingUpdate) {
        synchronized (this) {
            final MusicTrackInfo musicTrackInfoForNowPlayingUpdate = IAPMessageUtility.getMusicTrackInfoForNowPlayingUpdate(nowPlayingUpdate);
            final Object helperStateLock = this.helperStateLock;
            synchronized (helperStateLock) {
                this.nowPlayingFileTransferIdentifier = nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier;
                iAP2MusicHelper.sLogger.d("onNowPlayingUpdate nowPlayingFileTransferIdentifier: " + this.nowPlayingFileTransferIdentifier + ", musicTrackInfo: " + musicTrackInfoForNowPlayingUpdate);
                if (musicTrackInfoForNowPlayingUpdate != null && (!TextUtils.isEmpty((CharSequence)musicTrackInfoForNowPlayingUpdate.name) || !TextUtils.isEmpty((CharSequence)musicTrackInfoForNowPlayingUpdate.album) || !TextUtils.isEmpty((CharSequence)musicTrackInfoForNowPlayingUpdate.author))) {
                    this.transferIdTrackInfoMap.put(this.nowPlayingFileTransferIdentifier, musicTrackInfoForNowPlayingUpdate);
                }
                // monitorexit(helperStateLock)
                this.link.sendMessageAsNavdyEvent(musicTrackInfoForNowPlayingUpdate);
            }
        }
    }
    
    void onReady() {
        this.iapFileTransferManager.setFileTransferListener(this);
        this.iAPMusicManager.startNowPlayingUpdates();
    }
    
    void setBandwidthLevel(final int n) {
        final Logger sLogger = iAP2MusicHelper.sLogger;
        final StringBuilder append = new StringBuilder().append("Bandwidth level changing : ");
        String s;
        if (n <= 0) {
            s = "LOW";
        }
        else {
            s = "NORMAL";
        }
        sLogger.d(append.append(s).toString());
        if (n <= 0) {
            this.iapFileTransferManager.setFileTransferLimit(10000);
        }
        else {
            this.iapFileTransferManager.setFileTransferLimit(1048576);
        }
    }
    
    private class FileHolder
    {
        byte[] data;
        
        FileHolder(final byte[] data) {
            this.data = null;
            this.data = data;
        }
        
        int size() {
            int length;
            if (this.data != null) {
                length = this.data.length;
            }
            else {
                length = 0;
            }
            return length;
        }
    }
}
