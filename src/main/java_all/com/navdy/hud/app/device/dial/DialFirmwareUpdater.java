package com.navdy.hud.app.device.dial;

import java.util.UUID;
import java.util.Iterator;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import java.util.Arrays;
import java.io.FileNotFoundException;
import com.navdy.service.library.util.IOUtils;
import java.util.concurrent.atomic.AtomicBoolean;
import android.os.Handler;
import android.bluetooth.BluetoothGattCharacteristic;
import com.navdy.service.library.log.Logger;

public class DialFirmwareUpdater
{
    private static final int FIRMWARE_CHUNK_SIZE = 16;
    public static final String FIRMWARE_INCREMENTAL_VERSION_PREFIX = "Firmware incremental version: ";
    public static final String FIRMWARE_LOCAL_FILE = "/system/etc/firmware/dial.ota";
    public static final String FIRMWARE_VERSION_PREFIX = "Firmware version: ";
    public static final int LOG_PROGRESS_PERIOD = 30;
    private static final int OTA_COMMAND_BOOT_DFU = 3;
    private static final int OTA_COMMAND_ERASE_BLOCK_0 = 0;
    private static final int OTA_COMMAND_ERASE_BLOCK_1 = 1;
    private static final int OTA_COMMAND_PREPARE = 4;
    private static final int OTA_COMMAND_RESET_DFU_PTR = 2;
    private static final int OTA_STATE_CANCELLED = 1;
    private static final int OTA_STATE_NONE = 0;
    private static final int OTA_STATE_READY_TO_REBOOT = 32;
    private static final int OTA_STATE_STARTED = 16;
    private static final int RESP_ERROR_OTA_ALREADY_STARTED = 131;
    private static final int RESP_ERROR_OTA_INVALID_COMMAND = 128;
    private static final int RESP_ERROR_OTA_TOO_LARGE = 129;
    private static final int RESP_ERROR_OTA_UNKNOWN_STATE = 130;
    private static final int RESP_OK = 0;
    private static final int UNDETERMINED_FIRMWARE_VERSION = -1;
    private static final long UPDATE_FINISH_DELAY = 5000L;
    private static final long UPDATE_INTERPACKET_DELAY = 50L;
    private static final Logger sLogger;
    private int commandSent;
    private BluetoothGattCharacteristic deviceInfoFirmwareRevisionCharacteristic;
    private final DialManager dialManager;
    private String dialName;
    private byte[] firmwareData;
    private int firmwareOffset;
    private Handler handler;
    private BluetoothGattCharacteristic otaControlCharacteristic;
    private BluetoothGattCharacteristic otaDataCharacteristic;
    private BluetoothGattCharacteristic otaIncrementalVersionCharacteristic;
    private Runnable sendFirmwareChunkRunnable;
    private UpdateListener updateListener;
    private UpdateProgressListener updateProgressListener;
    private AtomicBoolean updateRunning;
    private Versions versions;
    
    static {
        sLogger = new Logger(DialFirmwareUpdater.class);
    }
    
    public DialFirmwareUpdater(final DialManager dialManager, final Handler handler) {
        this.versions = new Versions();
        this.updateRunning = new AtomicBoolean(false);
        this.dialName = null;
        this.sendFirmwareChunkRunnable = new Runnable() {
            @Override
            public void run() {
                DialFirmwareUpdater.this.sendFirmwareChunk();
            }
        };
        this.dialManager = dialManager;
        this.handler = handler;
        try {
            this.firmwareData = IOUtils.readBinaryFile("/system/etc/firmware/dial.ota");
            this.versions.local.reset();
            final String version = extractVersion(this.firmwareData, "Firmware incremental version: ");
            if (version != null) {
                this.versions.local.incrementalVersion = Integer.parseInt(version);
            }
            this.versions.local.revisionString = extractVersion(this.firmwareData, "Firmware version: ");
        }
        catch (FileNotFoundException ex) {
            DialFirmwareUpdater.sLogger.w("no dial firmware OTA file found");
        }
        catch (Throwable t) {
            DialFirmwareUpdater.sLogger.e("can't read dial firmware OTA file", t);
        }
    }
    
    private static String extractVersion(final byte[] array, final String s) {
        final String s2 = null;
        final int index = indexOf(array, s.getBytes());
        String s3;
        if (index < 0) {
            s3 = s2;
        }
        else {
            int n = index;
            while (true) {
                s3 = s2;
                if (n >= array.length) {
                    return s3;
                }
                if (array[n] == 0 || array[n] == 13) {
                    break;
                }
                ++n;
            }
            s3 = new String(Arrays.copyOfRange(array, s.length() + index, n));
        }
        return s3;
    }
    
    private void finishUpdate(final Error error, final String s) {
        DialFirmwareUpdater.sLogger.i("finished dial update error: " + error + " , " + s);
        if (this.updateProgressListener != null) {
            this.updateProgressListener.onFinished(error, s);
        }
    }
    
    public static int indexOf(final byte[] array, final byte[] array2) {
        for (int i = 0; i < array.length - array2.length + 1; ++i) {
            final boolean b = true;
            int n = 0;
            boolean b2;
            while (true) {
                b2 = b;
                if (n >= array2.length) {
                    break;
                }
                if (array[i + n] != array2[n]) {
                    b2 = false;
                    break;
                }
                ++n;
            }
            if (b2) {
                return i;
            }
        }
        return -1;
    }
    
    private void sendCommand(final int commandSent) {
        DialFirmwareUpdater.sLogger.d("sendCommand: " + commandSent);
        this.commandSent = commandSent;
        this.dialManager.queueWrite(this.otaControlCharacteristic, new byte[] { (byte)commandSent });
    }
    
    private void sendFirmwareChunk() {
        if (this.firmwareOffset / 16 % 30 == 0) {
            DialFirmwareUpdater.sLogger.d("firmwareOffset: " + this.firmwareOffset);
            if (this.updateProgressListener != null) {
                this.updateProgressListener.onProgress(this.firmwareOffset * 100 / this.firmwareData.length);
            }
        }
        this.dialManager.queueWrite(this.otaDataCharacteristic, Arrays.copyOfRange(this.firmwareData, this.firmwareOffset, this.firmwareOffset + 16));
        this.firmwareOffset += 16;
    }
    
    private void startFirmwareTransfer() {
        this.firmwareOffset = 0;
        this.sendFirmwareChunk();
    }
    
    public void cancelUpdate() {
        if (this.updateRunning.compareAndSet(true, false)) {
            this.finishUpdate(Error.CANCELLED, null);
        }
    }
    
    public boolean characteristicsAvailable() {
        return this.otaControlCharacteristic != null && this.otaDataCharacteristic != null && this.otaIncrementalVersionCharacteristic != null && this.deviceInfoFirmwareRevisionCharacteristic != null;
    }
    
    public String getDialName() {
        return this.dialName;
    }
    
    public Versions getVersions() {
        return this.versions;
    }
    
    public void onCharacteristicRead(final BluetoothGattCharacteristic bluetoothGattCharacteristic, final int n) {
        if (this.characteristicsAvailable()) {
            if (bluetoothGattCharacteristic == this.otaIncrementalVersionCharacteristic) {
                if (n == 0) {
                    this.versions.dial.incrementalVersion = Integer.parseInt(new String(bluetoothGattCharacteristic.getValue()));
                    DialFirmwareUpdater.sLogger.i(String.format("updateAvailable: %s (%d -> %d)", this.versions.isUpdateAvailable(), this.versions.dial.incrementalVersion, this.versions.local.incrementalVersion));
                    if (this.updateListener != null) {
                        this.updateListener.onUpdateState(this.versions.isUpdateAvailable());
                    }
                }
                else {
                    DialFirmwareUpdater.sLogger.e("can't read dial firmware incremental version");
                }
            }
            if (bluetoothGattCharacteristic == this.deviceInfoFirmwareRevisionCharacteristic) {
                if (n == 0) {
                    this.versions.dial.revisionString = new String(bluetoothGattCharacteristic.getValue());
                    DialFirmwareUpdater.sLogger.i("dial.revisionString: " + this.versions.dial.revisionString);
                    this.dialManager.queueRead(this.otaIncrementalVersionCharacteristic);
                }
                else {
                    DialFirmwareUpdater.sLogger.e("can't read dial firmware revisionString string");
                }
            }
        }
    }
    
    public void onCharacteristicWrite(final BluetoothGatt bluetoothGatt, final BluetoothGattCharacteristic bluetoothGattCharacteristic, final int n) {
        if (this.characteristicsAvailable()) {
            if (bluetoothGattCharacteristic == this.otaControlCharacteristic) {
                DialFirmwareUpdater.sLogger.d("dial OTA response for command " + this.commandSent + ", status: " + n);
                if (n == 0) {
                    switch (this.commandSent) {
                        case 0:
                            this.sendCommand(1);
                            break;
                        case 4:
                            this.sendCommand(0);
                            break;
                        case 1:
                            this.sendCommand(2);
                            break;
                        case 2:
                            this.startFirmwareTransfer();
                            break;
                    }
                }
                else if (this.commandSent != 3) {
                    this.finishUpdate(Error.INVALID_COMMAND, String.valueOf(this.commandSent));
                }
            }
            else if (bluetoothGattCharacteristic == this.otaDataCharacteristic) {
                if (n == 0) {
                    if (this.updateRunning.get()) {
                        if (this.firmwareOffset < this.firmwareData.length) {
                            this.handler.postDelayed(this.sendFirmwareChunkRunnable, 50L);
                        }
                        else {
                            DialFirmwareUpdater.sLogger.v("f/w upgrade complete, send OTA_COMMAND_BOOT_DFU, mark updating false");
                            this.updateRunning.set(false);
                            this.sendCommand(3);
                            this.handler.postDelayed((Runnable)new Runnable() {
                                final /* synthetic */ BluetoothDevice val$device = bluetoothGatt.getDevice();
                                
                                @Override
                                public void run() {
                                    DialManager.getInstance().forgetDial(this.val$device);
                                    DialFirmwareUpdater.this.finishUpdate(Error.NONE, null);
                                }
                            }, 5000L);
                        }
                    }
                }
                else {
                    DialFirmwareUpdater.sLogger.d("dial OTA error response for data chunk, status: " + n);
                    this.finishUpdate(Error.INVALID_RESPONSE, String.valueOf(n));
                }
            }
        }
    }
    
    public void onServicesDiscovered(final BluetoothGatt bluetoothGatt) {
        this.otaControlCharacteristic = null;
        this.otaDataCharacteristic = null;
        this.otaIncrementalVersionCharacteristic = null;
        this.deviceInfoFirmwareRevisionCharacteristic = null;
        this.dialName = this.dialManager.getDialName();
        for (final BluetoothGattService bluetoothGattService : bluetoothGatt.getServices()) {
            final UUID uuid = bluetoothGattService.getUuid();
            DialFirmwareUpdater.sLogger.d("service: " + uuid.toString());
            if (DialConstants.OTA_SERVICE_UUID.equals(uuid)) {
                for (final BluetoothGattCharacteristic otaIncrementalVersionCharacteristic : bluetoothGattService.getCharacteristics()) {
                    if (DialConstants.OTA_CONTROL_CHARACTERISTIC_UUID.equals(otaIncrementalVersionCharacteristic.getUuid())) {
                        (this.otaControlCharacteristic = otaIncrementalVersionCharacteristic).setWriteType(2);
                    }
                    else if (DialConstants.OTA_DATA_CHARACTERISTIC_UUID.equals(otaIncrementalVersionCharacteristic.getUuid())) {
                        (this.otaDataCharacteristic = otaIncrementalVersionCharacteristic).setWriteType(1);
                    }
                    else {
                        if (!DialConstants.OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID.equals(otaIncrementalVersionCharacteristic.getUuid())) {
                            continue;
                        }
                        this.otaIncrementalVersionCharacteristic = otaIncrementalVersionCharacteristic;
                    }
                }
            }
            else {
                if (!DialConstants.DEVICE_INFO_SERVICE_UUID.equals(uuid)) {
                    continue;
                }
                for (final BluetoothGattCharacteristic deviceInfoFirmwareRevisionCharacteristic : bluetoothGattService.getCharacteristics()) {
                    if (DialConstants.FIRMWARE_REVISION_CHARACTERISTIC_UUID.equals(deviceInfoFirmwareRevisionCharacteristic.getUuid())) {
                        this.deviceInfoFirmwareRevisionCharacteristic = deviceInfoFirmwareRevisionCharacteristic;
                    }
                }
            }
        }
        if (this.characteristicsAvailable()) {
            DialFirmwareUpdater.sLogger.d("required OTA characteristics found");
            this.startVersionDetection();
        }
        else {
            DialFirmwareUpdater.sLogger.e("required OTA characteristics not found");
        }
    }
    
    public void setUpdateListener(final UpdateListener updateListener) {
        this.updateListener = updateListener;
    }
    
    public boolean startUpdate(final UpdateProgressListener updateProgressListener) {
        boolean b = false;
        DialFirmwareUpdater.sLogger.i("starting dial firmware update");
        if (this.firmwareData.length % 16 != 0) {
            DialFirmwareUpdater.sLogger.e("dial firmware OTA file size is not divisible by 16, aborting update");
            updateProgressListener.onFinished(Error.INVALID_IMAGE_SIZE, "dial firmware OTA file size is not divisible by 16, aborting update");
        }
        else if (this.updateRunning.compareAndSet(false, true)) {
            this.updateProgressListener = updateProgressListener;
            this.sendCommand(4);
            b = true;
        }
        return b;
    }
    
    public boolean startVersionDetection() {
        this.versions.dial.reset();
        boolean b;
        if (this.characteristicsAvailable()) {
            this.dialManager.queueRead(this.deviceInfoFirmwareRevisionCharacteristic);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public enum Error
    {
        CANCELLED, 
        INVALID_COMMAND, 
        INVALID_IMAGE_SIZE, 
        INVALID_RESPONSE, 
        NONE, 
        TIMEDOUT;
    }
    
    public interface UpdateListener
    {
        void onUpdateState(final boolean p0);
    }
    
    public interface UpdateProgressListener
    {
        void onFinished(final Error p0, final String p1);
        
        void onProgress(final int p0);
    }
    
    public class Versions
    {
        public Version dial;
        public Version local;
        
        public Versions() {
            this.dial = new Version();
            this.local = new Version();
        }
        
        private boolean isUpdateOK() {
            boolean b;
            if (this.dial.incrementalVersion < 29) {
                DialFirmwareUpdater.sLogger.i("New dial firmware update logic is disabled (Current version < 29)");
                b = false;
            }
            else {
                b = true;
            }
            return b;
        }
        
        public boolean isUpdateAvailable() {
            boolean b2;
            final boolean b = b2 = false;
            if (this.local.incrementalVersion != -1) {
                if (this.dial.incrementalVersion == -1) {
                    b2 = b;
                }
                else {
                    b2 = b;
                    if (this.dial.incrementalVersion < this.local.incrementalVersion) {
                        b2 = b;
                        if (this.isUpdateOK()) {
                            b2 = true;
                        }
                    }
                }
            }
            return b2;
        }
        
        public class Version
        {
            public int incrementalVersion;
            public String revisionString;
            
            Version() {
                this.reset();
            }
            
            void reset() {
                this.revisionString = null;
                this.incrementalVersion = -1;
            }
        }
    }
}
