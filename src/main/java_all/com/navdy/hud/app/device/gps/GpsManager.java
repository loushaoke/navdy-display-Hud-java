package com.navdy.hud.app.device.gps;

import com.navdy.hud.app.debug.RouteRecorder;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.audio.SpeechRequest;
import android.os.UserHandle;
import android.content.Context;
import android.os.Process;
import com.navdy.hud.app.HudApplication;
import android.os.Message;
import java.io.Serializable;
import android.os.SystemClock;
import android.os.Bundle;
import java.util.concurrent.TimeUnit;
import android.location.GpsStatus;
import android.location.GpsStatus;
import android.content.Intent;
import java.util.ArrayList;
import android.os.HandlerThread;
import android.os.Handler;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.Location;
import com.navdy.service.library.events.location.Coordinate;
import android.os.Handler;
import android.content.BroadcastReceiver;
import com.navdy.hud.app.service.HudConnectionService;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.events.location.TransmitLocation;

public class GpsManager
{
    private static final int ACCEPTABLE_THRESHOLD = 2;
    private static final String DISCARD_TIME = "DISCARD_TIME";
    private static final String GPS_SYSTEM_PROPERTY = "persist.sys.hud_gps";
    public static final long INACCURATE_GPS_REPORT_INTERVAL;
    private static final int INITIAL_INTERVAL = 15000;
    private static final int KEEP_PHONE_GPS_ON = -1;
    private static final int LOCATION_ACCURACY_THROW_AWAY_THRESHOLD = 30;
    private static final int LOCATION_TIME_THROW_AWAY_THRESHOLD = 2000;
    public static final double MAXIMUM_STOPPED_SPEED = 0.22353333333333333;
    private static final double METERS_PER_MILE = 1609.44;
    public static final long MINIMUM_DRIVE_TIME = 3000L;
    public static final double MINIMUM_DRIVING_SPEED = 2.2353333333333336;
    static final int MSG_NMEA = 1;
    static final int MSG_SATELLITE_STATUS = 2;
    private static final int SECONDS_PER_HOUR = 3600;
    private static final String SET_LOCATION_DISCARD_TIME_THRESHOLD = "SET_LOCATION_DISCARD_TIME_THRESHOLD";
    private static final String SET_UBLOX_ACCURACY = "SET_UBLOX_ACCURACY";
    private static final TransmitLocation START_SENDING_LOCATION;
    private static final String START_UBLOX = "START_REPORTING_UBLOX_LOCATION";
    private static final TransmitLocation STOP_SENDING_LOCATION;
    private static final String STOP_UBLOX = "STOP_REPORTING_UBLOX_LOCATION";
    private static final String TAG_GPS = "[Gps-i] ";
    private static final String TAG_GPS_LOC = "[Gps-loc] ";
    private static final String TAG_GPS_LOC_NAVDY = "[Gps-loc-navdy] ";
    private static final String TAG_GPS_NMEA = "[Gps-nmea] ";
    private static final String TAG_GPS_STATUS = "[Gps-stat] ";
    private static final String TAG_PHONE_LOC = "[Phone-loc] ";
    private static final String UBLOX_ACCURACY = "UBLOX_ACCURACY";
    private static final float UBLOX_MIN_ACCEPTABLE_THRESHOLD = 5.0f;
    private static final long WARM_RESET_INTERVAL;
    private static final Logger sLogger;
    private static final GpsManager singleton;
    private LocationSource activeLocationSource;
    private HudConnectionService connectionService;
    private boolean driving;
    private BroadcastReceiver eventReceiver;
    private volatile boolean extrapolationOn;
    private volatile long extrapolationStartTime;
    private boolean firstSwitchToUbloxFromPhone;
    private float gpsAccuracy;
    private long gpsLastEventTime;
    private long gpsManagerStartTime;
    private Handler handler;
    private long inaccurateGpsCount;
    private boolean isDebugTTSEnabled;
    private boolean keepPhoneGpsOn;
    private long lastInaccurateGpsTime;
    private Coordinate lastPhoneCoordinate;
    private long lastPhoneCoordinateTime;
    private Location lastUbloxCoordinate;
    private long lastUbloxCoordinateTime;
    private Runnable locationFixRunnable;
    private LocationManager locationManager;
    private LocationListener navdyGpsLocationListener;
    private Handler$Callback nmeaCallback;
    private Handler nmeaHandler;
    private HandlerThread nmeaHandlerThread;
    private GpsNmeaParser nmeaParser;
    private Runnable noLocationUpdatesRunnable;
    private Runnable noPhoneLocationFixRunnable;
    private ArrayList<Intent> queue;
    private Runnable queueCallback;
    private volatile boolean startUbloxCalled;
    private boolean switchBetweenPhoneUbloxAllowed;
    private long transitionStartTime;
    private boolean transitioning;
    private LocationListener ubloxGpsLocationListener;
    private GpsStatus$NmeaListener ubloxGpsNmeaListener;
    private GpsStatus$Listener ubloxGpsStatusListener;
    private boolean warmResetCancelled;
    private Runnable warmResetRunnable;
    
    static {
        sLogger = new Logger(GpsManager.class);
        INACCURATE_GPS_REPORT_INTERVAL = TimeUnit.SECONDS.toMillis(30L);
        WARM_RESET_INTERVAL = TimeUnit.SECONDS.toMillis(160L);
        START_SENDING_LOCATION = new TransmitLocation(Boolean.valueOf(true));
        STOP_SENDING_LOCATION = new TransmitLocation(Boolean.valueOf(false));
        singleton = new GpsManager();
    }
    
    private GpsManager() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   java/lang/Object.<init>:()V
        //     4: aload_0        
        //     5: new             Ljava/util/ArrayList;
        //     8: dup            
        //     9: invokespecial   java/util/ArrayList.<init>:()V
        //    12: putfield        com/navdy/hud/app/device/gps/GpsManager.queue:Ljava/util/ArrayList;
        //    15: aload_0        
        //    16: new             Landroid/os/Handler;
        //    19: dup            
        //    20: invokestatic    android/os/Looper.getMainLooper:()Landroid/os/Looper;
        //    23: invokespecial   android/os/Handler.<init>:(Landroid/os/Looper;)V
        //    26: putfield        com/navdy/hud/app/device/gps/GpsManager.handler:Landroid/os/Handler;
        //    29: aload_0        
        //    30: new             Lcom/navdy/hud/app/device/gps/GpsManager$1;
        //    33: dup            
        //    34: aload_0        
        //    35: invokespecial   com/navdy/hud/app/device/gps/GpsManager$1.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //    38: putfield        com/navdy/hud/app/device/gps/GpsManager.ubloxGpsNmeaListener:Landroid/location/GpsStatus$NmeaListener;
        //    41: aload_0        
        //    42: new             Lcom/navdy/hud/app/device/gps/GpsManager$2;
        //    45: dup            
        //    46: aload_0        
        //    47: invokespecial   com/navdy/hud/app/device/gps/GpsManager$2.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //    50: putfield        com/navdy/hud/app/device/gps/GpsManager.ubloxGpsLocationListener:Landroid/location/LocationListener;
        //    53: aload_0        
        //    54: new             Lcom/navdy/hud/app/device/gps/GpsManager$3;
        //    57: dup            
        //    58: aload_0        
        //    59: invokespecial   com/navdy/hud/app/device/gps/GpsManager$3.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //    62: putfield        com/navdy/hud/app/device/gps/GpsManager.navdyGpsLocationListener:Landroid/location/LocationListener;
        //    65: aload_0        
        //    66: new             Lcom/navdy/hud/app/device/gps/GpsManager$4;
        //    69: dup            
        //    70: aload_0        
        //    71: invokespecial   com/navdy/hud/app/device/gps/GpsManager$4.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //    74: putfield        com/navdy/hud/app/device/gps/GpsManager.ubloxGpsStatusListener:Landroid/location/GpsStatus$Listener;
        //    77: aload_0        
        //    78: new             Lcom/navdy/hud/app/device/gps/GpsManager$5;
        //    81: dup            
        //    82: aload_0        
        //    83: invokespecial   com/navdy/hud/app/device/gps/GpsManager$5.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //    86: putfield        com/navdy/hud/app/device/gps/GpsManager.noPhoneLocationFixRunnable:Ljava/lang/Runnable;
        //    89: aload_0        
        //    90: new             Lcom/navdy/hud/app/device/gps/GpsManager$6;
        //    93: dup            
        //    94: aload_0        
        //    95: invokespecial   com/navdy/hud/app/device/gps/GpsManager$6.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //    98: putfield        com/navdy/hud/app/device/gps/GpsManager.noLocationUpdatesRunnable:Ljava/lang/Runnable;
        //   101: aload_0        
        //   102: new             Lcom/navdy/hud/app/device/gps/GpsManager$7;
        //   105: dup            
        //   106: aload_0        
        //   107: invokespecial   com/navdy/hud/app/device/gps/GpsManager$7.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //   110: putfield        com/navdy/hud/app/device/gps/GpsManager.warmResetRunnable:Ljava/lang/Runnable;
        //   113: aload_0        
        //   114: new             Lcom/navdy/hud/app/device/gps/GpsManager$8;
        //   117: dup            
        //   118: aload_0        
        //   119: invokespecial   com/navdy/hud/app/device/gps/GpsManager$8.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //   122: putfield        com/navdy/hud/app/device/gps/GpsManager.locationFixRunnable:Ljava/lang/Runnable;
        //   125: aload_0        
        //   126: new             Lcom/navdy/hud/app/device/gps/GpsManager$9;
        //   129: dup            
        //   130: aload_0        
        //   131: invokespecial   com/navdy/hud/app/device/gps/GpsManager$9.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //   134: putfield        com/navdy/hud/app/device/gps/GpsManager.nmeaCallback:Landroid/os/Handler$Callback;
        //   137: aload_0        
        //   138: new             Lcom/navdy/hud/app/device/gps/GpsManager$10;
        //   141: dup            
        //   142: aload_0        
        //   143: invokespecial   com/navdy/hud/app/device/gps/GpsManager$10.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //   146: putfield        com/navdy/hud/app/device/gps/GpsManager.queueCallback:Ljava/lang/Runnable;
        //   149: aload_0        
        //   150: new             Lcom/navdy/hud/app/device/gps/GpsManager$11;
        //   153: dup            
        //   154: aload_0        
        //   155: invokespecial   com/navdy/hud/app/device/gps/GpsManager$11.<init>:(Lcom/navdy/hud/app/device/gps/GpsManager;)V
        //   158: putfield        com/navdy/hud/app/device/gps/GpsManager.eventReceiver:Landroid/content/BroadcastReceiver;
        //   161: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   164: astore_1       
        //   165: ldc             "persist.sys.hud_gps"
        //   167: iconst_0       
        //   168: invokestatic    com/navdy/hud/app/util/os/SystemProperties.getInt:(Ljava/lang/String;I)I
        //   171: istore_2       
        //   172: iload_2        
        //   173: iconst_m1      
        //   174: if_icmpne       196
        //   177: aload_0        
        //   178: iconst_1       
        //   179: putfield        com/navdy/hud/app/device/gps/GpsManager.keepPhoneGpsOn:Z
        //   182: aload_0        
        //   183: iconst_1       
        //   184: putfield        com/navdy/hud/app/device/gps/GpsManager.switchBetweenPhoneUbloxAllowed:Z
        //   187: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   190: ldc_w           "switch between phone and ublox allowed"
        //   193: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   196: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   199: astore_3       
        //   200: new             Ljava/lang/StringBuilder;
        //   203: astore          4
        //   205: aload           4
        //   207: invokespecial   java/lang/StringBuilder.<init>:()V
        //   210: aload_3        
        //   211: aload           4
        //   213: ldc_w           "keepPhoneGpsOn["
        //   216: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   219: aload_0        
        //   220: getfield        com/navdy/hud/app/device/gps/GpsManager.keepPhoneGpsOn:Z
        //   223: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   226: ldc_w           "] val["
        //   229: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   232: iload_2        
        //   233: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   236: ldc_w           "]"
        //   239: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   242: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   245: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   248: new             Ljava/io/File;
        //   251: astore          4
        //   253: aload           4
        //   255: ldc_w           "/sdcard/debug_tts"
        //   258: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   261: aload_0        
        //   262: aload           4
        //   264: invokevirtual   java/io/File.exists:()Z
        //   267: putfield        com/navdy/hud/app/device/gps/GpsManager.isDebugTTSEnabled:Z
        //   270: aload_0        
        //   271: invokespecial   com/navdy/hud/app/device/gps/GpsManager.setUbloxAccuracyThreshold:()V
        //   274: aload_0        
        //   275: invokespecial   com/navdy/hud/app/device/gps/GpsManager.setUbloxTimeDiscardThreshold:()V
        //   278: aconst_null    
        //   279: astore          4
        //   281: aload_0        
        //   282: aload_1        
        //   283: ldc_w           "location"
        //   286: invokevirtual   android/content/Context.getSystemService:(Ljava/lang/String;)Ljava/lang/Object;
        //   289: checkcast       Landroid/location/LocationManager;
        //   292: putfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   295: aload_0        
        //   296: getfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   299: ldc_w           "gps"
        //   302: invokevirtual   android/location/LocationManager.isProviderEnabled:(Ljava/lang/String;)Z
        //   305: ifeq            414
        //   308: aload_0        
        //   309: getfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   312: ldc_w           "gps"
        //   315: invokevirtual   android/location/LocationManager.getProvider:(Ljava/lang/String;)Landroid/location/LocationProvider;
        //   318: astore_3       
        //   319: aload_3        
        //   320: astore          4
        //   322: aload_3        
        //   323: ifnull          414
        //   326: aload_0        
        //   327: new             Landroid/os/HandlerThread;
        //   330: dup            
        //   331: ldc_w           "navdynmea"
        //   334: invokespecial   android/os/HandlerThread.<init>:(Ljava/lang/String;)V
        //   337: putfield        com/navdy/hud/app/device/gps/GpsManager.nmeaHandlerThread:Landroid/os/HandlerThread;
        //   340: aload_0        
        //   341: getfield        com/navdy/hud/app/device/gps/GpsManager.nmeaHandlerThread:Landroid/os/HandlerThread;
        //   344: invokevirtual   android/os/HandlerThread.start:()V
        //   347: aload_0        
        //   348: new             Landroid/os/Handler;
        //   351: dup            
        //   352: aload_0        
        //   353: getfield        com/navdy/hud/app/device/gps/GpsManager.nmeaHandlerThread:Landroid/os/HandlerThread;
        //   356: invokevirtual   android/os/HandlerThread.getLooper:()Landroid/os/Looper;
        //   359: aload_0        
        //   360: getfield        com/navdy/hud/app/device/gps/GpsManager.nmeaCallback:Landroid/os/Handler$Callback;
        //   363: invokespecial   android/os/Handler.<init>:(Landroid/os/Looper;Landroid/os/Handler$Callback;)V
        //   366: putfield        com/navdy/hud/app/device/gps/GpsManager.nmeaHandler:Landroid/os/Handler;
        //   369: aload_0        
        //   370: new             Lcom/navdy/hud/app/device/gps/GpsNmeaParser;
        //   373: dup            
        //   374: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   377: aload_0        
        //   378: getfield        com/navdy/hud/app/device/gps/GpsManager.nmeaHandler:Landroid/os/Handler;
        //   381: invokespecial   com/navdy/hud/app/device/gps/GpsNmeaParser.<init>:(Lcom/navdy/service/library/log/Logger;Landroid/os/Handler;)V
        //   384: putfield        com/navdy/hud/app/device/gps/GpsManager.nmeaParser:Lcom/navdy/hud/app/device/gps/GpsNmeaParser;
        //   387: aload_0        
        //   388: getfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   391: aload_0        
        //   392: getfield        com/navdy/hud/app/device/gps/GpsManager.ubloxGpsStatusListener:Landroid/location/GpsStatus$Listener;
        //   395: invokevirtual   android/location/LocationManager.addGpsStatusListener:(Landroid/location/GpsStatus$Listener;)Z
        //   398: pop            
        //   399: aload_0        
        //   400: getfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   403: aload_0        
        //   404: getfield        com/navdy/hud/app/device/gps/GpsManager.ubloxGpsNmeaListener:Landroid/location/GpsStatus$NmeaListener;
        //   407: invokevirtual   android/location/LocationManager.addNmeaListener:(Landroid/location/GpsStatus$NmeaListener;)Z
        //   410: pop            
        //   411: aload_3        
        //   412: astore          4
        //   414: invokestatic    com/navdy/hud/app/util/DeviceUtil.isNavdyDevice:()Z
        //   417: ifeq            652
        //   420: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   423: ldc_w           "[Gps-i] setting up ublox gps"
        //   426: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   429: aload           4
        //   431: ifnull          640
        //   434: aload_0        
        //   435: getfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   438: ldc_w           "gps"
        //   441: lconst_0       
        //   442: fconst_0       
        //   443: aload_0        
        //   444: getfield        com/navdy/hud/app/device/gps/GpsManager.ubloxGpsLocationListener:Landroid/location/LocationListener;
        //   447: invokevirtual   android/location/LocationManager.requestLocationUpdates:(Ljava/lang/String;JFLandroid/location/LocationListener;)V
        //   450: aload_0        
        //   451: getfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   454: ldc_w           "NAVDY_GPS_PROVIDER"
        //   457: lconst_0       
        //   458: fconst_0       
        //   459: aload_0        
        //   460: getfield        com/navdy/hud/app/device/gps/GpsManager.navdyGpsLocationListener:Landroid/location/LocationListener;
        //   463: invokevirtual   android/location/LocationManager.requestLocationUpdates:(Ljava/lang/String;JFLandroid/location/LocationListener;)V
        //   466: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   469: ldc_w           "requestLocationUpdates successful for NAVDY_GPS_PROVIDER"
        //   472: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   475: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   478: ldc_w           "[Gps-i] ublox gps listeners installed"
        //   481: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   484: aload_0        
        //   485: getfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   488: ldc_w           "network"
        //   491: iconst_0       
        //   492: iconst_1       
        //   493: iconst_0       
        //   494: iconst_0       
        //   495: iconst_1       
        //   496: iconst_1       
        //   497: iconst_1       
        //   498: iconst_0       
        //   499: iconst_3       
        //   500: invokevirtual   android/location/LocationManager.addTestProvider:(Ljava/lang/String;ZZZZZZZII)V
        //   503: aload_0        
        //   504: getfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   507: ldc_w           "network"
        //   510: iconst_1       
        //   511: invokevirtual   android/location/LocationManager.setTestProviderEnabled:(Ljava/lang/String;Z)V
        //   514: aload_0        
        //   515: getfield        com/navdy/hud/app/device/gps/GpsManager.locationManager:Landroid/location/LocationManager;
        //   518: ldc_w           "network"
        //   521: iconst_2       
        //   522: aconst_null    
        //   523: invokestatic    java/lang/System.currentTimeMillis:()J
        //   526: invokevirtual   android/location/LocationManager.setTestProviderStatus:(Ljava/lang/String;ILandroid/os/Bundle;J)V
        //   529: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   532: ldc_w           "[Gps-i] added mock network provider"
        //   535: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   538: aload_0        
        //   539: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //   542: putfield        com/navdy/hud/app/device/gps/GpsManager.gpsManagerStartTime:J
        //   545: aload_0        
        //   546: aload_0        
        //   547: getfield        com/navdy/hud/app/device/gps/GpsManager.gpsManagerStartTime:J
        //   550: putfield        com/navdy/hud/app/device/gps/GpsManager.gpsLastEventTime:J
        //   553: invokestatic    com/navdy/hud/app/service/ConnectionServiceAnalyticsSupport.recordGpsAttemptAcquireLocation:()V
        //   556: invokestatic    com/navdy/hud/app/util/DeviceUtil.isNavdyDevice:()Z
        //   559: ifeq            566
        //   562: aload_0        
        //   563: invokespecial   com/navdy/hud/app/device/gps/GpsManager.setUbloxResetRunnable:()V
        //   566: new             Landroid/content/IntentFilter;
        //   569: dup            
        //   570: invokespecial   android/content/IntentFilter.<init>:()V
        //   573: astore          4
        //   575: aload           4
        //   577: ldc_w           "GPS_DR_STARTED"
        //   580: invokevirtual   android/content/IntentFilter.addAction:(Ljava/lang/String;)V
        //   583: aload           4
        //   585: ldc_w           "GPS_DR_STOPPED"
        //   588: invokevirtual   android/content/IntentFilter.addAction:(Ljava/lang/String;)V
        //   591: aload           4
        //   593: ldc_w           "EXTRAPOLATION"
        //   596: invokevirtual   android/content/IntentFilter.addAction:(Ljava/lang/String;)V
        //   599: aload_1        
        //   600: aload_0        
        //   601: getfield        com/navdy/hud/app/device/gps/GpsManager.eventReceiver:Landroid/content/BroadcastReceiver;
        //   604: aload           4
        //   606: invokevirtual   android/content/Context.registerReceiver:(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
        //   609: pop            
        //   610: return         
        //   611: astore          4
        //   613: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   616: aload           4
        //   618: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   621: goto            278
        //   624: astore          4
        //   626: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   629: ldc_w           "requestLocationUpdates"
        //   632: aload           4
        //   634: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   637: goto            475
        //   640: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   643: ldc_w           "[Gps-i] gps provider not found"
        //   646: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   649: goto            484
        //   652: getstatic       com/navdy/hud/app/device/gps/GpsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   655: ldc_w           "[Gps-i] not a Hud device,not setting up ublox gps"
        //   658: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   661: goto            484
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  165    172    611    624    Ljava/lang/Throwable;
        //  177    196    611    624    Ljava/lang/Throwable;
        //  196    278    611    624    Ljava/lang/Throwable;
        //  450    475    624    640    Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0475:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:692)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:529)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static Location androidLocationFromCoordinate(final Coordinate coordinate) {
        final Location location = new Location("network");
        location.setLatitude((double)coordinate.latitude);
        location.setLongitude((double)coordinate.longitude);
        location.setAccuracy((float)coordinate.accuracy);
        location.setAltitude((double)coordinate.altitude);
        location.setBearing((float)coordinate.bearing);
        location.setSpeed((float)coordinate.speed);
        location.setTime(System.currentTimeMillis());
        location.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
        return location;
    }
    
    private void cancelUbloxResetRunnable() {
        this.handler.removeCallbacks(this.warmResetRunnable);
        this.warmResetCancelled = true;
    }
    
    private void checkGpsToPhoneAccuracy(final Location location) {
        final float accuracy = location.getAccuracy();
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final long lastPhoneCoordinateTime = this.lastPhoneCoordinateTime;
        if (GpsManager.sLogger.isLoggable(3)) {
            final Logger sLogger = GpsManager.sLogger;
            final StringBuilder append = new StringBuilder().append("[Gps-loc-navdy] checkGpsToPhoneAccuracy: ublox[").append(location.getAccuracy()).append("] phone[");
            Serializable accuracy2;
            if (this.lastPhoneCoordinate != null) {
                accuracy2 = this.lastPhoneCoordinate.accuracy;
            }
            else {
                accuracy2 = "n/a";
            }
            final StringBuilder append2 = append.append(accuracy2).append("] lastPhoneTime [");
            Serializable value;
            if (this.lastPhoneCoordinate != null) {
                value = elapsedRealtime - lastPhoneCoordinateTime;
            }
            else {
                value = "n/a";
            }
            sLogger.v(append2.append(value).append("]").toString());
        }
        if (accuracy != 0.0f) {
            if (this.activeLocationSource == LocationSource.UBLOX) {
                if (this.lastPhoneCoordinate != null && SystemClock.elapsedRealtime() - this.lastPhoneCoordinateTime <= 3000L) {
                    if (this.lastPhoneCoordinate.accuracy + 2.0f < accuracy) {
                        if (GpsManager.sLogger.isLoggable(3)) {
                            GpsManager.sLogger.v("[Gps-loc-navdy]  ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "]");
                        }
                        if (this.startUbloxCalled) {
                            if (accuracy <= 5.0f) {
                                GpsManager.sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], but not above threshold");
                                this.stopSendingLocation();
                            }
                            else {
                                GpsManager.sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], stop uBloxReporting");
                                this.handler.removeCallbacks(this.locationFixRunnable);
                                this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                                this.stopUbloxReporting();
                                GpsManager.sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                                this.activeLocationSource = LocationSource.PHONE;
                                this.markLocationFix("Switched to Phone Gps", "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, true, false);
                            }
                        }
                    }
                    else if (GpsManager.sLogger.isLoggable(3)) {
                        GpsManager.sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
                    }
                }
            }
            else if (accuracy + 2.0f < this.lastPhoneCoordinate.accuracy) {
                GpsManager.sLogger.v("ublox accuracy [" + accuracy + "] < phone accuracy[" + this.lastPhoneCoordinate.accuracy + "], start uBloxReporting, switch complete");
                this.firstSwitchToUbloxFromPhone = true;
                this.markLocationFix("Switched to Ublox Gps", "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =" + accuracy, false, false);
                this.startUbloxReporting();
            }
            else if (GpsManager.sLogger.isLoggable(3)) {
                GpsManager.sLogger.v("ublox accuracy [" + accuracy + "] > phone accuracy[" + this.lastPhoneCoordinate.accuracy + "] no action");
            }
        }
    }
    
    private void feedLocationToProvider(final Location location, final Coordinate coordinate) {
        try {
            if (GpsManager.sLogger.isLoggable(3)) {
                GpsManager.sLogger.d("[Phone-loc] " + coordinate);
            }
            Location androidLocationFromCoordinate;
            if ((androidLocationFromCoordinate = location) == null) {
                androidLocationFromCoordinate = androidLocationFromCoordinate(coordinate);
            }
            this.updateDrivingState(androidLocationFromCoordinate);
            this.locationManager.setTestProviderLocation(androidLocationFromCoordinate.getProvider(), androidLocationFromCoordinate);
        }
        catch (Throwable t) {
            GpsManager.sLogger.e("feedLocation", t);
        }
    }
    
    public static GpsManager getInstance() {
        return GpsManager.singleton;
    }
    
    private void markLocationFix(final String s, final String s2, final boolean b, final boolean b2) {
        final Bundle bundle = new Bundle();
        bundle.putString("title", s);
        bundle.putString("info", s2);
        bundle.putBoolean("phone", b);
        bundle.putBoolean("ublox", b2);
        this.sendGpsEventBroadcast("GPS_Switch", bundle);
    }
    
    private void processNmea(final String obj) {
        final Message obtainMessage = this.nmeaHandler.obtainMessage(1);
        obtainMessage.obj = obj;
        this.nmeaHandler.sendMessage(obtainMessage);
    }
    
    private void sendGpsEventBroadcast(final String s, final Bundle bundle) {
        try {
            final Intent intent = new Intent(s);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            if (SystemClock.elapsedRealtime() - this.gpsManagerStartTime <= 15000L) {
                this.queue.add(intent);
                this.handler.removeCallbacks(this.queueCallback);
                this.handler.postDelayed(this.queueCallback, 15000L);
            }
            else {
                final Context appContext = HudApplication.getAppContext();
                final UserHandle myUserHandle = Process.myUserHandle();
                if (this.queue.size() > 0) {
                    this.queueCallback.run();
                }
                appContext.sendBroadcastAsUser(intent, myUserHandle);
            }
        }
        catch (Throwable t) {
            GpsManager.sLogger.e(t);
        }
    }
    
    private void sendSpeechRequest(final SpeechRequest speechRequest) {
        if (this.connectionService == null || !this.connectionService.isConnected()) {
            return;
        }
        try {
            GpsManager.sLogger.i("[Gps-loc] send speech request");
            this.connectionService.sendMessage(speechRequest);
        }
        catch (Throwable t) {
            GpsManager.sLogger.e(t);
        }
    }
    
    private void setDriving(final boolean driving) {
        if (this.driving != driving) {
            this.driving = driving;
            String s;
            if (driving) {
                s = "driving_started";
            }
            else {
                s = "driving_stopped";
            }
            this.sendGpsEventBroadcast(s, null);
        }
    }
    
    private void setUbloxAccuracyThreshold() {
        try {
            final Context appContext = HudApplication.getAppContext();
            final UserHandle myUserHandle = Process.myUserHandle();
            final Intent intent = new Intent("SET_UBLOX_ACCURACY");
            intent.putExtra("UBLOX_ACCURACY", 30);
            appContext.sendBroadcastAsUser(intent, myUserHandle);
            GpsManager.sLogger.v("setUbloxAccuracyThreshold:30");
        }
        catch (Throwable t) {
            GpsManager.sLogger.e("setUbloxAccuracyThreshold", t);
        }
    }
    
    private void setUbloxResetRunnable() {
        GpsManager.sLogger.v("setUbloxResetRunnable");
        this.handler.postDelayed(this.warmResetRunnable, GpsManager.WARM_RESET_INTERVAL);
    }
    
    private void setUbloxTimeDiscardThreshold() {
        try {
            final Context appContext = HudApplication.getAppContext();
            final UserHandle myUserHandle = Process.myUserHandle();
            final Intent intent = new Intent("SET_LOCATION_DISCARD_TIME_THRESHOLD");
            intent.putExtra("DISCARD_TIME", 2000);
            appContext.sendBroadcastAsUser(intent, myUserHandle);
            GpsManager.sLogger.v("setUbloxTimeDiscardThreshold:2000");
        }
        catch (Throwable t) {
            GpsManager.sLogger.e("setUbloxTimeDiscardThreshold", t);
        }
    }
    
    private void startSendingLocation() {
        if (this.connectionService == null || !this.connectionService.isConnected()) {
            return;
        }
        try {
            GpsManager.sLogger.i("[Gps-loc] phone start sending location");
            this.connectionService.sendMessage(GpsManager.START_SENDING_LOCATION);
        }
        catch (Throwable t) {
            GpsManager.sLogger.e(t);
        }
    }
    
    private void stopSendingLocation() {
        if (this.keepPhoneGpsOn) {
            GpsManager.sLogger.v("stopSendingLocation: keeping phone gps on");
        }
        else if (this.isDebugTTSEnabled) {
            GpsManager.sLogger.v("stopSendingLocation: not stopping, debug_tts enabled");
        }
        else if (this.connectionService != null && this.connectionService.isConnected()) {
            try {
                GpsManager.sLogger.i("[Gps-loc] phone stop sending location");
                this.connectionService.sendMessage(GpsManager.STOP_SENDING_LOCATION);
            }
            catch (Throwable t) {
                GpsManager.sLogger.e(t);
            }
        }
    }
    
    private void updateDrivingState(final Location location) {
        if (this.driving) {
            if (location == null || location.getSpeed() < 0.22353333333333333) {
                if (this.transitioning) {
                    if (SystemClock.elapsedRealtime() - this.transitionStartTime > 3000L) {
                        this.setDriving(false);
                        this.transitioning = false;
                    }
                }
                else {
                    this.transitioning = true;
                    this.transitionStartTime = SystemClock.elapsedRealtime();
                }
            }
            else {
                this.transitioning = false;
            }
        }
        else if (location != null && location.getSpeed() > 2.2353333333333336) {
            if (this.transitioning) {
                if (SystemClock.elapsedRealtime() - this.transitionStartTime > 3000L) {
                    this.setDriving(true);
                    this.transitioning = false;
                }
            }
            else {
                this.transitioning = true;
                this.transitionStartTime = SystemClock.elapsedRealtime();
            }
        }
        else {
            this.transitioning = false;
        }
        this.handler.removeCallbacks(this.noLocationUpdatesRunnable);
        if (this.driving) {
            this.handler.postDelayed(this.noLocationUpdatesRunnable, 3000L);
        }
    }
    
    public void feedLocation(final Coordinate lastPhoneCoordinate) {
        boolean b;
        if (this.lastPhoneCoordinateTime > 0L || this.lastUbloxCoordinateTime > 0L) {
            b = true;
        }
        else {
            b = false;
        }
        final long currentTimeMillis = System.currentTimeMillis();
        if (lastPhoneCoordinate.accuracy >= 30.0f && b) {
            ++this.inaccurateGpsCount;
            this.gpsAccuracy += lastPhoneCoordinate.accuracy;
            if (this.lastInaccurateGpsTime == 0L || currentTimeMillis - this.lastInaccurateGpsTime >= GpsManager.INACCURATE_GPS_REPORT_INTERVAL) {
                GpsManager.sLogger.e("BAD gps accuracy (discarding) avg:" + this.gpsAccuracy / this.inaccurateGpsCount + ", count=" + this.inaccurateGpsCount + ", last coord: " + lastPhoneCoordinate);
                this.inaccurateGpsCount = 0L;
                this.gpsAccuracy = 0.0f;
                this.lastInaccurateGpsTime = currentTimeMillis;
            }
        }
        else {
            final long n = currentTimeMillis - lastPhoneCoordinate.timestamp;
            if (n >= 2000L && b) {
                GpsManager.sLogger.e("OLD location from phone(discard) time:" + n + ", timestamp:" + lastPhoneCoordinate.timestamp + " now:" + currentTimeMillis + " lat:" + lastPhoneCoordinate.latitude + " lng:" + lastPhoneCoordinate.longitude);
            }
            else if (this.lastPhoneCoordinate != null && this.connectionService.getDevicePlatform() == DeviceInfo.Platform.PLATFORM_iOS && this.lastPhoneCoordinate.latitude == lastPhoneCoordinate.latitude && this.lastPhoneCoordinate.longitude == lastPhoneCoordinate.longitude && this.lastPhoneCoordinate.accuracy == lastPhoneCoordinate.accuracy && this.lastPhoneCoordinate.altitude == lastPhoneCoordinate.altitude && this.lastPhoneCoordinate.bearing == lastPhoneCoordinate.bearing && this.lastPhoneCoordinate.speed == lastPhoneCoordinate.speed) {
                GpsManager.sLogger.e("same location(discard) diff=" + (lastPhoneCoordinate.timestamp - this.lastPhoneCoordinate.timestamp));
            }
            else {
                this.lastPhoneCoordinate = lastPhoneCoordinate;
                this.lastPhoneCoordinateTime = SystemClock.elapsedRealtime();
                this.handler.removeCallbacks(this.noPhoneLocationFixRunnable);
                Location androidLocationFromCoordinate = null;
                final RouteRecorder instance = RouteRecorder.getInstance();
                if (instance.isRecording()) {
                    androidLocationFromCoordinate = androidLocationFromCoordinate(lastPhoneCoordinate);
                    instance.injectLocation(androidLocationFromCoordinate, true);
                }
                if (this.activeLocationSource == null) {
                    GpsManager.sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                    this.stopUbloxReporting();
                    this.activeLocationSource = LocationSource.PHONE;
                    this.feedLocationToProvider(androidLocationFromCoordinate, lastPhoneCoordinate);
                    this.markLocationFix("Switched to Phone Gps", "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox=n/a", true, false);
                }
                else if (this.activeLocationSource == LocationSource.PHONE) {
                    this.feedLocationToProvider(androidLocationFromCoordinate, lastPhoneCoordinate);
                }
                else if (SystemClock.elapsedRealtime() - this.lastUbloxCoordinateTime > 3000L) {
                    GpsManager.sLogger.v("[Gps-loc] [LocationProvider] switched from [" + this.activeLocationSource + "] to [" + LocationSource.PHONE + "]");
                    this.activeLocationSource = LocationSource.PHONE;
                    this.stopUbloxReporting();
                    this.feedLocationToProvider(androidLocationFromCoordinate, lastPhoneCoordinate);
                    this.markLocationFix("Switched to Phone Gps", "Phone =" + this.lastPhoneCoordinate.accuracy + " ublox =lost", true, false);
                }
            }
        }
    }
    
    public void setConnectionService(final HudConnectionService connectionService) {
        this.connectionService = connectionService;
    }
    
    public void shutdown() {
        if (this.locationManager != null) {
            this.locationManager.removeUpdates(this.ubloxGpsLocationListener);
            this.locationManager.removeUpdates(this.navdyGpsLocationListener);
            this.locationManager.removeGpsStatusListener(this.ubloxGpsStatusListener);
            this.locationManager.removeNmeaListener(this.ubloxGpsNmeaListener);
        }
    }
    
    public void startUbloxReporting() {
        try {
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent("START_REPORTING_UBLOX_LOCATION"), Process.myUserHandle());
            this.startUbloxCalled = true;
            GpsManager.sLogger.v("started ublox reporting");
        }
        catch (Throwable t) {
            GpsManager.sLogger.e("startUbloxReporting", t);
        }
    }
    
    public void stopUbloxReporting() {
        try {
            this.startUbloxCalled = false;
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent("STOP_REPORTING_UBLOX_LOCATION"), Process.myUserHandle());
            GpsManager.sLogger.v("stopped ublox reporting");
        }
        catch (Throwable t) {
            GpsManager.sLogger.e("stopUbloxReporting", t);
        }
    }
    
    enum LocationSource
    {
        PHONE, 
        UBLOX;
    }
}
