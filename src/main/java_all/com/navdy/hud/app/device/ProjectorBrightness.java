package com.navdy.hud.app.device;

import java.io.IOException;
import com.navdy.service.library.util.IOUtils;
import java.util.ArrayList;
import com.navdy.service.library.log.Logger;
import java.util.List;

public class ProjectorBrightness
{
    private static final List<String> AUTO_BRIGHTNESS_MAPPING;
    private static final List<String> AUTO_BRIGHTNESS_MAPPING_FILES;
    private static final int BRIGHTNESS_SCALE_VALUES_COUNT = 256;
    private static final String DATA_FILE = "/sys/dlpc/RGB_Brightness";
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(ProjectorBrightness.class);
        AUTO_BRIGHTNESS_MAPPING_FILES = new ArrayList<String>() {
            {
                this.add("/device/etc/calibration/dlpc_brightness");
                this.add("/system/etc/calibration/dlpc_brightness");
            }
        };
        AUTO_BRIGHTNESS_MAPPING = readAutoBrightnessMapping();
    }
    
    public static int getValue() throws IOException {
        try {
            return getValue(IOUtils.convertFileToString("/sys/dlpc/RGB_Brightness").trim());
        }
        catch (NumberFormatException ex) {
            ProjectorBrightness.sLogger.e("Exception while reading auto-brightness value", ex);
            throw new IOException("Corrupted data in file with auto-brightness value");
        }
    }
    
    protected static int getValue(final String s) {
        int index;
        if ((index = ProjectorBrightness.AUTO_BRIGHTNESS_MAPPING.indexOf(s)) < 0) {
            ProjectorBrightness.sLogger.e("Cannot get brightness value for auto-brightness string '" + s + "'");
            index = 0;
        }
        return index;
    }
    
    public static void init() {
    }
    
    private static List<String> readAutoBrightnessMapping() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_0       
        //     4: ldc             "_v4"
        //     6: astore_1       
        //     7: getstatic       com/navdy/hud/app/util/SerialNumber.instance:Lcom/navdy/hud/app/util/SerialNumber;
        //    10: getfield        com/navdy/hud/app/util/SerialNumber.revisionCode:Ljava/lang/String;
        //    13: ldc             "3"
        //    15: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    18: ifeq            24
        //    21: ldc             "_v3"
        //    23: astore_1       
        //    24: getstatic       com/navdy/hud/app/device/ProjectorBrightness.AUTO_BRIGHTNESS_MAPPING_FILES:Ljava/util/List;
        //    27: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //    32: astore_2       
        //    33: aload_2        
        //    34: invokeinterface java/util/Iterator.hasNext:()Z
        //    39: ifeq            313
        //    42: aload_2        
        //    43: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    48: checkcast       Ljava/lang/String;
        //    51: astore_0       
        //    52: new             Ljava/lang/StringBuilder;
        //    55: dup            
        //    56: invokespecial   java/lang/StringBuilder.<init>:()V
        //    59: aload_0        
        //    60: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    63: aload_1        
        //    64: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    67: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    70: astore_3       
        //    71: new             Ljava/util/ArrayList;
        //    74: dup            
        //    75: sipush          256
        //    78: invokespecial   java/util/ArrayList.<init>:(I)V
        //    81: astore          4
        //    83: aconst_null    
        //    84: astore          5
        //    86: aconst_null    
        //    87: astore          6
        //    89: aconst_null    
        //    90: astore          7
        //    92: aconst_null    
        //    93: astore          8
        //    95: aconst_null    
        //    96: astore          9
        //    98: aconst_null    
        //    99: astore          10
        //   101: aconst_null    
        //   102: astore          11
        //   104: aload           6
        //   106: astore          12
        //   108: aload           9
        //   110: astore          13
        //   112: new             Ljava/io/FileInputStream;
        //   115: astore_0       
        //   116: aload           6
        //   118: astore          12
        //   120: aload           9
        //   122: astore          13
        //   124: aload_0        
        //   125: aload_3        
        //   126: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //   129: new             Ljava/io/BufferedReader;
        //   132: astore          12
        //   134: new             Ljava/io/InputStreamReader;
        //   137: astore          13
        //   139: aload           13
        //   141: aload_0        
        //   142: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //   145: aload           12
        //   147: aload           13
        //   149: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //   152: aload           12
        //   154: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //   157: astore          13
        //   159: aload           13
        //   161: ifnull          200
        //   164: aload           4
        //   166: invokeinterface java/util/List.size:()I
        //   171: sipush          256
        //   174: if_icmpge       200
        //   177: aload           4
        //   179: aload           13
        //   181: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //   184: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   189: pop            
        //   190: aload           12
        //   192: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //   195: astore          13
        //   197: goto            159
        //   200: aload           4
        //   202: invokeinterface java/util/List.size:()I
        //   207: sipush          256
        //   210: if_icmpge       267
        //   213: getstatic       com/navdy/hud/app/device/ProjectorBrightness.sLogger:Lcom/navdy/service/library/log/Logger;
        //   216: astore          13
        //   218: new             Ljava/lang/StringBuilder;
        //   221: astore          11
        //   223: aload           11
        //   225: invokespecial   java/lang/StringBuilder.<init>:()V
        //   228: aload           13
        //   230: aload           11
        //   232: ldc             "File "
        //   234: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   237: aload_3        
        //   238: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   241: ldc             " not complete - skipping it"
        //   243: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   246: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   249: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
        //   252: aload           12
        //   254: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   257: aload_0        
        //   258: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   261: aload           4
        //   263: astore_0       
        //   264: goto            33
        //   267: getstatic       com/navdy/hud/app/device/ProjectorBrightness.sLogger:Lcom/navdy/service/library/log/Logger;
        //   270: astore          13
        //   272: new             Ljava/lang/StringBuilder;
        //   275: astore          11
        //   277: aload           11
        //   279: invokespecial   java/lang/StringBuilder.<init>:()V
        //   282: aload           13
        //   284: aload           11
        //   286: ldc             "Successfully read mapping from file "
        //   288: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   291: aload_3        
        //   292: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   295: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   298: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   301: aload           12
        //   303: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   306: aload_0        
        //   307: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   310: aload           4
        //   312: astore_0       
        //   313: aload_0        
        //   314: astore          12
        //   316: aload_0        
        //   317: invokeinterface java/util/List.size:()I
        //   322: sipush          256
        //   325: if_icmpge       341
        //   328: getstatic       com/navdy/hud/app/device/ProjectorBrightness.sLogger:Lcom/navdy/service/library/log/Logger;
        //   331: ldc             "No auto-brightness mapping file found"
        //   333: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   336: invokestatic    java/util/Collections.emptyList:()Ljava/util/List;
        //   339: astore          12
        //   341: aload           12
        //   343: areturn        
        //   344: astore_0       
        //   345: aload           7
        //   347: astore_0       
        //   348: aload_0        
        //   349: astore          12
        //   351: aload           11
        //   353: astore          13
        //   355: getstatic       com/navdy/hud/app/device/ProjectorBrightness.sLogger:Lcom/navdy/service/library/log/Logger;
        //   358: astore          8
        //   360: aload_0        
        //   361: astore          12
        //   363: aload           11
        //   365: astore          13
        //   367: new             Ljava/lang/StringBuilder;
        //   370: astore          10
        //   372: aload_0        
        //   373: astore          12
        //   375: aload           11
        //   377: astore          13
        //   379: aload           10
        //   381: invokespecial   java/lang/StringBuilder.<init>:()V
        //   384: aload_0        
        //   385: astore          12
        //   387: aload           11
        //   389: astore          13
        //   391: aload           8
        //   393: aload           10
        //   395: aload_3        
        //   396: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   399: ldc             " not found - checking next file"
        //   401: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   404: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   407: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
        //   410: aload           11
        //   412: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   415: aload_0        
        //   416: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   419: aload           4
        //   421: astore_0       
        //   422: goto            33
        //   425: astore          11
        //   427: aload           5
        //   429: astore_0       
        //   430: aload_0        
        //   431: astore          12
        //   433: aload           8
        //   435: astore          13
        //   437: getstatic       com/navdy/hud/app/device/ProjectorBrightness.sLogger:Lcom/navdy/service/library/log/Logger;
        //   440: astore          10
        //   442: aload_0        
        //   443: astore          12
        //   445: aload           8
        //   447: astore          13
        //   449: new             Ljava/lang/StringBuilder;
        //   452: astore          5
        //   454: aload_0        
        //   455: astore          12
        //   457: aload           8
        //   459: astore          13
        //   461: aload           5
        //   463: invokespecial   java/lang/StringBuilder.<init>:()V
        //   466: aload_0        
        //   467: astore          12
        //   469: aload           8
        //   471: astore          13
        //   473: aload           10
        //   475: aload           5
        //   477: ldc             "Cannot read auto-brightness mapping file "
        //   479: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   482: aload_3        
        //   483: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   486: ldc             " : "
        //   488: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   491: aload           11
        //   493: invokevirtual   java/io/IOException.getMessage:()Ljava/lang/String;
        //   496: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   499: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   502: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   505: aload           8
        //   507: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   510: aload_0        
        //   511: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   514: aload           4
        //   516: astore_0       
        //   517: goto            33
        //   520: astore_0       
        //   521: aload           13
        //   523: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   526: aload           12
        //   528: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   531: aload_0        
        //   532: athrow         
        //   533: astore          11
        //   535: aload_0        
        //   536: astore          12
        //   538: aload           10
        //   540: astore          13
        //   542: aload           11
        //   544: astore_0       
        //   545: goto            521
        //   548: astore          11
        //   550: aload           12
        //   552: astore          13
        //   554: aload_0        
        //   555: astore          12
        //   557: aload           11
        //   559: astore_0       
        //   560: goto            521
        //   563: astore          11
        //   565: goto            430
        //   568: astore          11
        //   570: aload           12
        //   572: astore          8
        //   574: goto            430
        //   577: astore          12
        //   579: goto            348
        //   582: astore          13
        //   584: aload           12
        //   586: astore          11
        //   588: goto            348
        //    Signature:
        //  ()Ljava/util/List<Ljava/lang/String;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  112    116    344    348    Ljava/io/FileNotFoundException;
        //  112    116    425    430    Ljava/io/IOException;
        //  112    116    520    521    Any
        //  124    129    344    348    Ljava/io/FileNotFoundException;
        //  124    129    425    430    Ljava/io/IOException;
        //  124    129    520    521    Any
        //  129    152    577    582    Ljava/io/FileNotFoundException;
        //  129    152    563    568    Ljava/io/IOException;
        //  129    152    533    548    Any
        //  152    159    582    591    Ljava/io/FileNotFoundException;
        //  152    159    568    577    Ljava/io/IOException;
        //  152    159    548    563    Any
        //  164    197    582    591    Ljava/io/FileNotFoundException;
        //  164    197    568    577    Ljava/io/IOException;
        //  164    197    548    563    Any
        //  200    252    582    591    Ljava/io/FileNotFoundException;
        //  200    252    568    577    Ljava/io/IOException;
        //  200    252    548    563    Any
        //  267    301    582    591    Ljava/io/FileNotFoundException;
        //  267    301    568    577    Ljava/io/IOException;
        //  267    301    548    563    Any
        //  355    360    520    521    Any
        //  367    372    520    521    Any
        //  379    384    520    521    Any
        //  391    410    520    521    Any
        //  437    442    520    521    Any
        //  449    454    520    521    Any
        //  461    466    520    521    Any
        //  473    505    520    521    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 270, Size: 270
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
