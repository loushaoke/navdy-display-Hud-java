package com.navdy.hud.app.device.dial;

import java.util.LinkedList;
import com.navdy.hud.app.screen.BaseScreen;
import android.view.InputDevice;
import com.navdy.service.library.events.ui.Screen;
import android.view.KeyEvent;
import com.navdy.service.library.task.TaskManager;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.lang.reflect.Method;
import android.content.IntentFilter;
import android.os.ParcelUuid;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.util.GenericUtil;
import java.util.Collection;
import com.navdy.hud.app.bluetooth.utils.BluetoothUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import java.util.List;
import java.util.UUID;
import java.util.Iterator;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothGattDescriptor;
import com.navdy.hud.app.util.os.SystemProperties;
import android.text.TextUtils;
import com.navdy.hud.app.util.DeviceUtil;
import android.content.Intent;
import android.content.Context;
import android.bluetooth.le.ScanResult;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.HashMap;
import android.content.SharedPreferences;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanCallback;
import java.util.Set;
import android.os.HandlerThread;
import android.os.Handler;
import android.bluetooth.BluetoothGattCallback;
import com.squareup.otto.Bus;
import android.bluetooth.BluetoothDevice;
import java.util.ArrayList;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.BluetoothGatt;
import android.content.BroadcastReceiver;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import com.navdy.service.library.log.Logger;
import java.util.Map;

public class DialManager
{
    private static final String BALANCED = "balanced";
    private static final DialConstants.DialConnectionStatus DIAL_CONNECTED;
    private static final DialConstants.DialConnectionStatus DIAL_CONNECTING;
    private static final DialConstants.DialConnectionStatus DIAL_CONNECTION_FAILED;
    private static final DialConstants.DialConnectionStatus DIAL_NOT_CONNECTED;
    private static final DialConstants.DialConnectionStatus DIAL_NOT_FOUND;
    private static final String DIAL_POWER_SETTING = "persist.sys.bt.dial.pwr";
    private static final String HIGH_POWER = "high";
    private static final String LOW_POWER = "low";
    private static final Map<String, Integer> PowerMap;
    static final String TAG_DIAL = "[Dial]";
    static final Logger sLogger;
    private static final DialManager singleton;
    private BluetoothGattCharacteristic batteryCharateristic;
    private Runnable batteryReadRunnable;
    private BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver bluetoothBondingReceiver;
    private BroadcastReceiver bluetoothConnectivityReceiver;
    private BluetoothGatt bluetoothGatt;
    private BluetoothLeScanner bluetoothLEScanner;
    private BluetoothManager bluetoothManager;
    private BroadcastReceiver bluetoothOnOffReceiver;
    private Runnable bondHangRunnable;
    private int bondTries;
    private ArrayList<BluetoothDevice> bondedDials;
    private BluetoothDevice bondingDial;
    private Bus bus;
    private volatile BluetoothDevice connectedDial;
    private Runnable connectionErrorRunnable;
    private Runnable deviceInfoGattReadRunnable;
    private Thread dialEventsListenerThread;
    private DialFirmwareUpdater dialFirmwareUpdater;
    private BluetoothGattCharacteristic dialForgetKeysCharacteristic;
    private volatile boolean dialManagerInitialized;
    private BluetoothGattCharacteristic dialRebootCharacteristic;
    private String disconnectedDial;
    private int encryptionRetryCount;
    private String firmWareVersion;
    private BluetoothGattCharacteristic firmwareVersionCharacteristic;
    private BluetoothGattCallback gattCallback;
    private CharacteristicCommandProcessor gattCharacteristicProcessor;
    private Handler handler;
    private HandlerThread handlerThread;
    private String hardwareVersion;
    private BluetoothGattCharacteristic hardwareVersionCharacteristic;
    private BluetoothGattCharacteristic hidHostReadyCharacteristic;
    private Runnable hidHostReadyReadRunnable;
    private Set<BluetoothDevice> ignoredNonNavdyDials;
    private volatile boolean isGattOn;
    private volatile boolean isScanning;
    private int lastKnownBatteryLevel;
    private Integer lastKnownRawBatteryLevel;
    private Integer lastKnownSystemTemperature;
    private ScanCallback leScanCallback;
    private DialConstants.NotificationReason notificationReasonBattery;
    private BluetoothGattCharacteristic rawBatteryCharateristic;
    private Runnable scanHangRunnable;
    private Map<BluetoothDevice, ScanRecord> scannedNavdyDials;
    private Runnable sendLocalyticsSuccessRunnable;
    private SharedPreferences sharedPreferences;
    private Runnable stopScanRunnable;
    private volatile BluetoothDevice tempConnectedDial;
    private BluetoothGattCharacteristic temperatureCharateristic;
    
    static {
        sLogger = new Logger(DialManager.class);
        singleton = new DialManager();
        DIAL_CONNECTED = new DialConstants.DialConnectionStatus(DialConstants.DialConnectionStatus.Status.CONNECTED);
        DIAL_NOT_CONNECTED = new DialConstants.DialConnectionStatus(DialConstants.DialConnectionStatus.Status.DISCONNECTED);
        DIAL_CONNECTING = new DialConstants.DialConnectionStatus(DialConstants.DialConnectionStatus.Status.CONNECTING);
        DIAL_CONNECTION_FAILED = new DialConstants.DialConnectionStatus(DialConstants.DialConnectionStatus.Status.CONNECTION_FAILED);
        DIAL_NOT_FOUND = new DialConstants.DialConnectionStatus(DialConstants.DialConnectionStatus.Status.NO_DIAL_FOUND);
        (PowerMap = new HashMap<String, Integer>()).put("low", 2);
        DialManager.PowerMap.put("balanced", 0);
        DialManager.PowerMap.put("high", 1);
    }
    
    public DialManager() {
        this.lastKnownBatteryLevel = -1;
        this.bondedDials = new ArrayList<BluetoothDevice>();
        this.scannedNavdyDials = new LinkedHashMap<BluetoothDevice, ScanRecord>();
        this.ignoredNonNavdyDials = new HashSet<BluetoothDevice>();
        this.notificationReasonBattery = DialConstants.NotificationReason.OK_BATTERY;
        this.scanHangRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    DialManager.sLogger.v("scan hang runnable");
                    DialManager.this.stopScan(true);
                }
                catch (Throwable t) {
                    DialManager.sLogger.e("[Dial]", t);
                }
            }
        };
        this.bondHangRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    DialManager.sLogger.e("[Dial]bond hang detected");
                    String name = null;
                    if (DialManager.this.bondingDial != null) {
                        name = DialManager.this.bondingDial.getName();
                    }
                    DialManager.this.bondingDial = null;
                    DialManager.DIAL_NOT_CONNECTED.dialName = name;
                    DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
                }
                catch (Throwable t) {
                    DialManager.sLogger.e("[Dial]", t);
                }
            }
        };
        this.sendLocalyticsSuccessRunnable = new Runnable() {
            @Override
            public void run() {
                if (DialManager.this.connectedDial != null) {
                    DialManagerHelper.sendLocalyticsEvent(DialManager.this.handler, false, true, 0, false);
                }
            }
        };
        this.leScanCallback = new ScanCallback() {
            public void onScanFailed(final int n) {
                DialManager.sLogger.e("btle scan failed:" + n);
                DialManager.this.stopScan(true);
            }
            
            public void onScanResult(final int n, final ScanResult scanResult) {
                try {
                    final ScanRecord scanRecord = scanResult.getScanRecord();
                    DialManager.this.handleScannedDevice(scanResult.getDevice(), scanRecord.getTxPowerLevel(), scanRecord);
                }
                catch (Throwable t) {
                    DialManager.sLogger.e("[Dial]", t);
                }
            }
        };
        this.stopScanRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    DialManager.this.stopScan(false);
                }
                catch (Throwable t) {
                    DialManager.sLogger.e("[Dial]", t);
                }
            }
        };
        this.bluetoothBondingReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                while (true) {
                    String action = null;
                    BluetoothDevice bluetoothDevice = null;
                    Label_0104: {
                        try {
                            action = intent.getAction();
                            bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                            if (bluetoothDevice != null && DialManager.this.bondingDial != null) {
                                if (DialManager.this.isDialDevice(bluetoothDevice)) {
                                    break Label_0104;
                                }
                                DialManager.sLogger.i("[Dial]btBondRecvr: notification not for dial:" + bluetoothDevice.getName() + " addr:" + bluetoothDevice.getAddress());
                            }
                            return;
                        }
                        catch (Throwable t) {
                            DialManager.sLogger.e("[Dial]", t);
                            return;
                        }
                    }
                    DialManager.sLogger.v("[Dial]btBondRecvr [" + bluetoothDevice.getName() + "] action[" + action + "]");
                    if (!"android.bluetooth.device.action.BOND_STATE_CHANGED".equals(action)) {
                        return;
                    }
                    final int intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                    DialManager.sLogger.v("[Dial]btBondRecvr: prev state:" + intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", Integer.MIN_VALUE) + " new state:" + intExtra);
                    if (intExtra == 12) {
                        DialManager.sLogger.v("btBondRecvr removed hang runnable");
                        DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                        DialManager.sLogger.v("[Dial]btBondRecvr: got pairing event");
                        if (DialManager.this.bondingDial != null && DialManager.this.bondingDial.equals(bluetoothDevice)) {
                            DialManager.sLogger.v("[Dial]btBondRecvr: bonding active bondDial[" + DialManager.this.bondingDial + "] device[" + bluetoothDevice + "]");
                            DialManager.sLogger.v("btBondRecvr:" + bluetoothDevice + "state is:" + bluetoothDevice.getBondState());
                            if (DialManager.this.addtoBondList(bluetoothDevice)) {
                                DialManager.sLogger.v("btBondRecvr: added to bond list");
                            }
                            else {
                                DialManager.sLogger.v("btConnRecvr: already in bond list");
                            }
                            DialManager.sLogger.v("btConnRecvr: attempt connection reqd");
                            DialManager.this.bondingDial = null;
                            DialManagerHelper.connectToDial(DialManager.this.bluetoothAdapter, bluetoothDevice, (DialManagerHelper.IDialConnection)new DialManagerHelper.IDialConnection() {
                                @Override
                                public void onAttemptConnection(final boolean b) {
                                    DialManager.sLogger.v("btConnRecvr: attempt connection [" + bluetoothDevice.getName() + " ] success[" + b + "]");
                                }
                                
                                @Override
                                public void onAttemptDisconnection(final boolean b) {
                                }
                            });
                            return;
                        }
                        DialManager.sLogger.v("[Dial]no bonding active bondDial[" + DialManager.this.bondingDial + "] device[" + bluetoothDevice + "]");
                    }
                    else {
                        if (intExtra != 10) {
                            return;
                        }
                        DialManager.sLogger.e("[Dial]btBondRecvr: not paired tries[" + DialManager.this.bondTries + "]");
                        if (DialManager.this.bondTries == 3) {
                            DialManager.sLogger.v("btBondRecvr removed runnable");
                            DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                            String name = null;
                            if (DialManager.this.bondingDial != null) {
                                name = DialManager.this.bondingDial.getName();
                            }
                            DialManager.this.bondingDial = null;
                            DialManager.sLogger.e("[Dial]btBondRecvr: giving up");
                            DialManager.DIAL_NOT_CONNECTED.dialName = name;
                            DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
                            return;
                        }
                        DialManager.sLogger.e("[Dial]btBondRecvr: trying to bond again");
                        DialManager.this.bondTries++;
                        DialManager.this.handler.postDelayed((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                DialManager.sLogger.v("btBondRecvr removed runnable");
                                DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                                bluetoothDevice.createBond();
                                DialManager.this.handler.postDelayed(DialManager.this.bondHangRunnable, 30000L);
                                DialManager.sLogger.v("btBondRecvr posted runnable");
                            }
                        }, 4000L);
                    }
                }
            }
        };
        this.bluetoothOnOffReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                while (true) {
                    Label_0116: {
                        Label_0088: {
                            Label_0077: {
                                try {
                                    if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                                        switch (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE)) {
                                            case 10:
                                                DialManager.sLogger.v("[Dial]onoff: state off");
                                                break;
                                            case 13:
                                                break Label_0077;
                                            case 12:
                                                break Label_0088;
                                            case 11:
                                                break Label_0116;
                                        }
                                    }
                                    return;
                                }
                                catch (Throwable t) {
                                    DialManager.sLogger.e("[Dial]", t);
                                    return;
                                }
                            }
                            DialManager.sLogger.v("[Dial]onoff: state turning off");
                            return;
                        }
                        DialManager.sLogger.v("[Dial]onoff: state on");
                        if (!DialManager.this.dialManagerInitialized) {
                            DialManager.this.init();
                            return;
                        }
                        return;
                    }
                    DialManager.sLogger.v("[Dial]onoff: state turning on");
                }
            }
        };
        this.connectionErrorRunnable = new Runnable() {
            @Override
            public void run() {
                DialManager.sLogger.v("connectionErrorRunnable no input descriptor event:" + DialManager.this.tempConnectedDial);
                if (DialManager.this.tempConnectedDial != null) {
                    DialManager.this.disconectAndRemoveBond(DialManager.this.tempConnectedDial, new DialManagerHelper.IDialForgotten() {
                        final /* synthetic */ String val$dialName = DialManager.this.tempConnectedDial.getName();
                        
                        @Override
                        public void onForgotten() {
                            DialManager.sLogger.v("connectionErrorRunnable: dial forgotten");
                            DialManager.DIAL_CONNECTION_FAILED.dialName = this.val$dialName;
                            DialManager.this.bus.post(DialManager.DIAL_CONNECTION_FAILED);
                            DialManager.this.handler.post((Runnable)new Runnable() {
                                @Override
                                public void run() {
                                    DialManagerHelper.sendLocalyticsEvent(DialManager.this.handler, false, false, 0, false, "Connection error");
                                }
                            });
                        }
                    }, 2000);
                }
                else {
                    DialManager.sLogger.v("connectionErrorRunnable no temp connected dial");
                }
            }
        };
        this.bluetoothConnectivityReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (intent != null) {
                    String action = null;
                    BluetoothDevice bluetoothDevice = null;
                    Label_0095: {
                        try {
                            action = intent.getAction();
                            bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                            if (bluetoothDevice != null) {
                                if (DialManager.this.isDialDevice(bluetoothDevice)) {
                                    break Label_0095;
                                }
                                DialManager.sLogger.i("[Dial]btConnRecvr: notification not for dial:" + bluetoothDevice.getName() + " addr:" + bluetoothDevice.getAddress());
                            }
                        }
                        catch (Throwable t) {
                            DialManager.sLogger.e("[Dial]", t);
                        }
                        return;
                    }
                    DialManager.sLogger.v("[Dial]btConnRecvr [" + bluetoothDevice.getName() + "] action [" + action + "]");
                    if (!DeviceUtil.isNavdyDevice()) {
                        boolean b = false;
                        if ("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED".equals(action)) {
                            final int intExtra = intent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", -1);
                            final int intExtra2 = intent.getIntExtra("android.bluetooth.profile.extra.STATE", -1);
                            DialManager.sLogger.v("[Dial]btConnRecvr: dial connection state changed old[" + intExtra + "] new[" + intExtra2 + "]");
                            b = b;
                            if (intExtra2 == 2) {
                                b = true;
                            }
                        }
                        if (DialManager.this.connectedDial != null && !TextUtils.equals((CharSequence)DialManager.this.connectedDial.getName(), (CharSequence)bluetoothDevice.getName())) {
                            DialManager.sLogger.v("btConnRecvr: notification for not current connected dial");
                            DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                        }
                        else if ("android.bluetooth.device.action.ACL_CONNECTED".equals(action) || b) {
                            DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                            DialManager.this.connectedDial = bluetoothDevice;
                            if (DialManager.this.addtoBondList(bluetoothDevice)) {
                                DialManager.sLogger.v("attempt connection reqd");
                                DialManagerHelper.connectToDial(DialManager.this.bluetoothAdapter, bluetoothDevice, (DialManagerHelper.IDialConnection)new DialManagerHelper.IDialConnection() {
                                    @Override
                                    public void onAttemptConnection(final boolean b) {
                                        DialManager.sLogger.v("attempt connection [" + bluetoothDevice.getName() + " ] success[" + b + "]");
                                        if (b) {
                                            DialManager.this.handleDialConnection();
                                            DialManager.sLogger.i("[Dial]btConnRecvr: dial connected [" + bluetoothDevice.getName() + "] addr:" + bluetoothDevice.getAddress());
                                        }
                                    }
                                    
                                    @Override
                                    public void onAttemptDisconnection(final boolean b) {
                                    }
                                });
                            }
                            else {
                                DialManager.sLogger.v("attempt connection not read");
                                DialManager.this.handleDialConnection();
                            }
                        }
                        else if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(action)) {
                            DialManager.this.handler.removeCallbacks(DialManager.this.bondHangRunnable);
                            if (DialManager.this.connectedDial != null) {
                                DialManager.this.disconnectedDial = DialManager.this.connectedDial.getName();
                            }
                            DialManager.DIAL_NOT_CONNECTED.dialName = DialManager.this.disconnectedDial;
                            DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
                            if (DialManager.this.getBondedDialCount() > 0) {}
                            DialManager.this.clearConnectedDial();
                            DialManager.this.stopGATTClient();
                            DialManager.sLogger.e("[Dial]btConnRecvr: dial NOT connected [" + bluetoothDevice.getName() + "] addr:" + bluetoothDevice.getAddress());
                        }
                    }
                }
            }
        };
        this.batteryReadRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (DialManager.this.batteryCharateristic != null) {
                        DialManager.this.queueRead(DialManager.this.batteryCharateristic);
                    }
                    if (DialManager.this.rawBatteryCharateristic != null) {
                        DialManager.this.queueRead(DialManager.this.rawBatteryCharateristic);
                    }
                    if (DialManager.this.temperatureCharateristic != null) {
                        DialManager.this.queueRead(DialManager.this.temperatureCharateristic);
                    }
                    DialManager.this.handler.postDelayed((Runnable)this, 300000L);
                }
                catch (Throwable t) {
                    DialManager.sLogger.e("[Dial]", t);
                }
            }
        };
        this.deviceInfoGattReadRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (DialManager.this.firmwareVersionCharacteristic != null) {
                        DialManager.this.queueRead(DialManager.this.firmwareVersionCharacteristic);
                    }
                    if (DialManager.this.hardwareVersionCharacteristic != null) {
                        DialManager.this.queueRead(DialManager.this.hardwareVersionCharacteristic);
                    }
                }
                catch (Throwable t) {
                    DialManager.sLogger.e("[Dial]", t);
                }
            }
        };
        this.hidHostReadyReadRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (DialManager.this.hidHostReadyCharacteristic != null) {
                        DialManager.sLogger.v("queuing hid host ready characteristic");
                        DialManager.this.queueRead(DialManager.this.hidHostReadyCharacteristic);
                    }
                    else {
                        DialManager.sLogger.v("not queuing hid host ready characteristic: null");
                    }
                }
                catch (Throwable t) {
                    DialManager.sLogger.e("[Dial]", t);
                }
            }
        };
        this.gattCallback = new BluetoothGattCallback() {
            public void onCharacteristicChanged(final BluetoothGatt bluetoothGatt, final BluetoothGattCharacteristic bluetoothGattCharacteristic) {
                DialManager.sLogger.v("[Dial]onCharacteristicChanged");
                this.print(bluetoothGattCharacteristic);
            }
            
            public void onCharacteristicRead(final BluetoothGatt bluetoothGatt, final BluetoothGattCharacteristic bluetoothGattCharacteristic, final int n) {
                DialManager.this.dialFirmwareUpdater.onCharacteristicRead(bluetoothGattCharacteristic, n);
                if (n == 0) {
                    this.print(bluetoothGattCharacteristic);
                }
                else {
                    DialManager.sLogger.v("[Dial]onCharacteristicRead fail:" + n);
                }
                DialManager.sLogger.d("onCharacteristicRead finished");
                if (DialManager.this.gattCharacteristicProcessor != null) {
                    DialManager.this.gattCharacteristicProcessor.commandFinished();
                }
            }
            
            public void onCharacteristicWrite(final BluetoothGatt bluetoothGatt, final BluetoothGattCharacteristic bluetoothGattCharacteristic, final int n) {
                DialManager.this.dialFirmwareUpdater.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, n);
                if (bluetoothGattCharacteristic == DialManager.this.dialForgetKeysCharacteristic) {
                    DialManager.this.forgetDial(bluetoothGatt.getDevice());
                }
                if (bluetoothGattCharacteristic == DialManager.this.dialRebootCharacteristic) {
                    final boolean b = n == 0;
                    if (b) {
                        DialManager.this.sharedPreferences.edit().putLong("last_dial_reboot", System.currentTimeMillis()).apply();
                    }
                    DialManagerHelper.sendRebootLocalyticsEvent(DialManager.this.handler, b, bluetoothGatt.getDevice(), null);
                }
                if (DialManager.this.gattCharacteristicProcessor != null) {
                    DialManager.this.gattCharacteristicProcessor.commandFinished();
                }
            }
            
            public void onConnectionStateChange(final BluetoothGatt bluetoothGatt, final int n, final int n2) {
                while (true) {
                    Integer n3 = null;
                    Label_0208: {
                        try {
                            DialManager.sLogger.v("[Dial]gatt state change:" + n + " newState=" + n2);
                            if (n2 == 2) {
                                if (bluetoothGatt != null) {
                                    if (DeviceUtil.isNavdyDevice()) {
                                        final String value = SystemProperties.get("persist.sys.bt.dial.pwr");
                                        if (value != null) {
                                            n3 = (Integer)DialManager.PowerMap.get(value);
                                        }
                                        if (n3 != null) {
                                            DialManager.sLogger.v("[Dial] setting BT power to " + value);
                                            bluetoothGatt.requestConnectionPriority((int)n3);
                                        }
                                    }
                                    DialManager.this.gattCharacteristicProcessor = new CharacteristicCommandProcessor(bluetoothGatt, DialManager.this.handler);
                                    DialManager.sLogger.v("[Dial]gatt connected, discover");
                                    bluetoothGatt.discoverServices();
                                }
                                else {
                                    DialManager.sLogger.v("[Dial]gatt is null, cannot discover");
                                }
                                return;
                            }
                            break Label_0208;
                        }
                        catch (Throwable t) {
                            DialManager.sLogger.e("[Dial]", t);
                            return;
                        }
                        return;
                    }
                    if (n2 == 0) {
                        DialManager.sLogger.v("[Dial]gatt disconnected");
                        if (DialManager.this.gattCharacteristicProcessor != null) {
                            DialManager.this.gattCharacteristicProcessor.release();
                            DialManager.this.gattCharacteristicProcessor = null;
                        }
                        DialManager.this.stopGATTClient();
                    }
                }
            }
            
            public void onDescriptorRead(final BluetoothGatt bluetoothGatt, final BluetoothGattDescriptor bluetoothGattDescriptor, final int n) {
                DialManager.sLogger.v("[Dial]onDescriptorRead");
            }
            
            public void onDescriptorWrite(final BluetoothGatt bluetoothGatt, final BluetoothGattDescriptor bluetoothGattDescriptor, final int n) {
                final Logger sLogger = DialManager.sLogger;
                final StringBuilder append = new StringBuilder().append("[Dial]onDescriptorWrite : ");
                String s;
                if (n == 0) {
                    s = "success";
                }
                else {
                    s = "fail";
                }
                sLogger.v(append.append(s).toString());
            }
            
            public void onServicesDiscovered(final BluetoothGatt bluetoothGatt, final int n) {
            Label_0124_Outer:
                while (true) {
                    DialManager.this.hardwareVersionCharacteristic = null;
                    DialManager.this.firmwareVersionCharacteristic = null;
                    DialManager.this.hidHostReadyCharacteristic = null;
                    DialManager.this.dialForgetKeysCharacteristic = null;
                    DialManager.this.dialRebootCharacteristic = null;
                    DialManager.this.batteryCharateristic = null;
                    DialManager.this.rawBatteryCharateristic = null;
                    DialManager.this.temperatureCharateristic = null;
                    Label_0807: {
                        while (true) {
                            UUID uuid = null;
                            Label_0302: {
                                while (true) {
                                    Label_0264: {
                                        try {
                                            if (!DialManager.this.isGattOn) {
                                                DialManager.sLogger.w("gatt is not on but onServicesDiscovered called");
                                            }
                                            else {
                                                if (n != 0) {
                                                    break Label_0807;
                                                }
                                                DialManager.sLogger.i("[Dial]onServicesDiscovered");
                                                DialManager.this.dialFirmwareUpdater.onServicesDiscovered(bluetoothGatt);
                                                for (final BluetoothGattService bluetoothGattService : bluetoothGatt.getServices()) {
                                                    uuid = bluetoothGattService.getUuid();
                                                    if (!DialConstants.HID_SERVICE_UUID.equals(uuid)) {
                                                        break Label_0302;
                                                    }
                                                    DialManager.sLogger.d("Hid service found " + uuid);
                                                    DialManager.this.hidHostReadyCharacteristic = bluetoothGattService.getCharacteristic(DialConstants.HID_HOST_READY_UUID);
                                                    if (DialManager.this.hidHostReadyCharacteristic != null) {
                                                        break Label_0264;
                                                    }
                                                    DialManager.sLogger.e("Hid host ready Characteristic is null");
                                                    DialManager.this.handler.post(DialManager.this.hidHostReadyReadRunnable);
                                                }
                                            }
                                            return;
                                        }
                                        catch (Throwable t) {
                                            DialManager.sLogger.e("[Dial]", t);
                                            return;
                                        }
                                    }
                                    DialManager.sLogger.v("Found Hid host ready Characteristic");
                                    DialManager.this.sharedPreferences.edit().putBoolean("first_run_dial_video_shown", true).apply();
                                    continue;
                                }
                            }
                            if (DialConstants.BATTERY_SERVICE_UUID.equals(uuid)) {
                                final BluetoothGattService bluetoothGattService;
                                DialManager.sLogger.i("[Dial]Service: " + bluetoothGattService.getUuid().toString());
                                final List characteristics = bluetoothGattService.getCharacteristics();
                                if (characteristics == null) {
                                    DialManager.sLogger.i("[Dial]No characteristic");
                                    return;
                                }
                                for (final BluetoothGattCharacteristic bluetoothGattCharacteristic : characteristics) {
                                    if (DialConstants.BATTERY_LEVEL_UUID.equals(bluetoothGattCharacteristic.getUuid())) {
                                        DialManager.this.batteryCharateristic = bluetoothGattCharacteristic;
                                    }
                                    else if (DialConstants.RAW_BATTERY_LEVEL_UUID.equals(bluetoothGattCharacteristic.getUuid())) {
                                        DialManager.this.rawBatteryCharateristic = bluetoothGattCharacteristic;
                                    }
                                    else {
                                        if (!DialConstants.SYSTEM_TEMPERATURE_UUID.equals(bluetoothGattCharacteristic.getUuid())) {
                                            continue Label_0124_Outer;
                                        }
                                        DialManager.this.temperatureCharateristic = bluetoothGattCharacteristic;
                                    }
                                }
                                if (DialManager.this.batteryCharateristic != null) {
                                    DialManager.this.handler.postDelayed(DialManager.this.batteryReadRunnable, 5000L);
                                    DialManager.this.handler.postDelayed(DialManager.this.sendLocalyticsSuccessRunnable, 10000L);
                                    continue;
                                }
                                DialManager.sLogger.v("[Dial] battery characteristic not found");
                                continue;
                            }
                            else {
                                final BluetoothGattService bluetoothGattService;
                                if (DialConstants.DEVICE_INFO_SERVICE_UUID.equals(uuid)) {
                                    DialManager.sLogger.d("Device info service found " + uuid);
                                    DialManager.this.firmwareVersionCharacteristic = bluetoothGattService.getCharacteristic(DialConstants.DEVICE_INFO_FIRMWARE_VERSION_UUID);
                                    if (DialManager.this.firmwareVersionCharacteristic == null) {
                                        DialManager.sLogger.e("Firmware Version Characteristic is null");
                                    }
                                    DialManager.this.hardwareVersionCharacteristic = bluetoothGattService.getCharacteristic(DialConstants.DEVICE_INFO_HARDWARE_VERSION_UUID);
                                    if (DialManager.this.hardwareVersionCharacteristic == null) {
                                        DialManager.sLogger.e("Hardware Version Characteristic is null");
                                    }
                                    DialManager.this.handler.postDelayed(DialManager.this.deviceInfoGattReadRunnable, 5000L);
                                    continue;
                                }
                                if (!DialConstants.OTA_SERVICE_UUID.equals(uuid)) {
                                    DialManager.sLogger.v("[Dial]Ignoring service:" + uuid);
                                    continue;
                                }
                                DialManager.this.dialForgetKeysCharacteristic = bluetoothGattService.getCharacteristic(DialConstants.DIAL_FORGET_KEYS_UUID);
                                if (DialManager.this.dialForgetKeysCharacteristic == null) {
                                    DialManager.sLogger.d("Dial key forget is null");
                                }
                                DialManager.this.dialRebootCharacteristic = bluetoothGattService.getCharacteristic(DialConstants.DIAL_REBOOT_UUID);
                                if (DialManager.this.dialRebootCharacteristic == null) {
                                    DialManager.sLogger.d("Dial reboot is null");
                                    continue;
                                }
                                continue;
                            }
                            break;
                        }
                    }
                    DialManager.sLogger.e("[Dial]onServicesDiscovered error:" + n);
                }
            }
            
            void print(final BluetoothGattCharacteristic bluetoothGattCharacteristic) {
                if (DialConstants.HID_HOST_READY_UUID.equals(bluetoothGattCharacteristic.getUuid())) {
                    DialManager.sLogger.v("[Dial]onCharacteristicRead hid host ready, length " + bluetoothGattCharacteristic.getValue().length);
                }
                else if (DialConstants.BATTERY_LEVEL_UUID.equals(bluetoothGattCharacteristic.getUuid())) {
                    DialManager.sLogger.v("[Dial]onCharacteristicRead battery");
                    final Integer intValue = bluetoothGattCharacteristic.getIntValue(33, 0);
                    if (intValue != null) {
                        DialManager.sLogger.v("[Dial]battery level is [" + intValue + "] %");
                        DialManager.this.processBatteryLevel(intValue);
                    }
                    else {
                        DialManager.sLogger.v("[Dial]battery level no data");
                    }
                }
                else if (DialConstants.RAW_BATTERY_LEVEL_UUID.equals(bluetoothGattCharacteristic.getUuid())) {
                    DialManager.sLogger.v("[Dial]onCharacteristicRead rawBattery");
                    final Integer intValue2 = bluetoothGattCharacteristic.getIntValue(34, 0);
                    if (intValue2 != null) {
                        DialManager.sLogger.v("[Dial]raw battery level is [" + intValue2 + "]");
                        DialManager.this.processRawBatteryLevel(intValue2);
                    }
                    else {
                        DialManager.sLogger.v("[Dial]raw battery level no data");
                    }
                }
                else if (DialConstants.SYSTEM_TEMPERATURE_UUID.equals(bluetoothGattCharacteristic.getUuid())) {
                    DialManager.sLogger.v("[Dial]onCharacteristicRead temperature");
                    final Integer intValue3 = bluetoothGattCharacteristic.getIntValue(34, 0);
                    if (intValue3 != null) {
                        DialManager.sLogger.v("[Dial]system temperature is [" + intValue3 + "]");
                        DialManager.this.processSystemTemperature(intValue3);
                    }
                    else {
                        DialManager.sLogger.v("[Dial]system temperature no data");
                    }
                }
                else if (DialConstants.DEVICE_INFO_HARDWARE_VERSION_UUID.equals(bluetoothGattCharacteristic.getUuid())) {
                    DialManager.sLogger.v("[Dial]onCharacteristicRead Hardware version");
                    final String stringValue = bluetoothGattCharacteristic.getStringValue(0);
                    if (!TextUtils.isEmpty((CharSequence)stringValue)) {
                        DialManager.this.hardwareVersion = stringValue.trim();
                        DialManager.sLogger.d("Device Info hardware version received: " + DialManager.this.hardwareVersion);
                    }
                }
                else if (DialConstants.DEVICE_INFO_FIRMWARE_VERSION_UUID.equals(bluetoothGattCharacteristic.getUuid())) {
                    DialManager.sLogger.v("[Dial]onCharacteristicRead Firmware version");
                    final String stringValue2 = bluetoothGattCharacteristic.getStringValue(0);
                    if (!TextUtils.isEmpty((CharSequence)stringValue2)) {
                        DialManager.this.firmWareVersion = stringValue2.trim();
                        DialManager.sLogger.d("Device Info firmware version received: " + DialManager.this.firmWareVersion);
                    }
                }
                else {
                    DialManager.sLogger.v("[Dial]onCharacteristicRead:" + bluetoothGattCharacteristic.getUuid());
                }
            }
        };
        DialManager.sLogger.v("[Dial]initializing...");
        if (HudApplication.getApplication() != null) {
            this.bus = RemoteDeviceManager.getInstance().getBus();
            this.sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
            this.bluetoothManager = (BluetoothManager)HudApplication.getAppContext().getSystemService("bluetooth");
            if (this.bluetoothManager == null) {
                DialManager.sLogger.e("[Dial]Bluetooth manager not available");
            }
            else {
                this.bluetoothAdapter = this.bluetoothManager.getAdapter();
                if (this.bluetoothAdapter == null) {
                    DialManager.sLogger.e("[Dial]Bluetooth is not available");
                }
                else {
                    (this.handlerThread = new HandlerThread("DialHandlerThread")).start();
                    this.handler = new Handler(this.handlerThread.getLooper());
                    if (!this.bluetoothAdapter.isEnabled()) {
                        DialManager.sLogger.i("[Dial]Bluetooth is not enabled, should be booting up, wait for the event");
                    }
                    else {
                        DialManager.sLogger.i("[Dial]Bluetooth is enabled");
                        this.init();
                    }
                    this.startDialEventListener();
                    this.registerReceivers();
                }
            }
        }
    }
    
    private boolean addtoBondList(final BluetoothDevice bluetoothDevice) {
        boolean b = false;
        if (bluetoothDevice != null) {
            // monitorenter(bondedDials = this.bondedDials)
            boolean b2 = false;
            try {
                final Iterator<BluetoothDevice> iterator = this.bondedDials.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().equals(bluetoothDevice)) {
                        b2 = true;
                    }
                }
                if (!b2) {
                    this.bondedDials.add(bluetoothDevice);
                    DialManager.sLogger.v("bonded device added to list [" + bluetoothDevice.getName() + "]");
                    b = true;
                    return b;
                }
            }
            finally {
            }
            // monitorexit(bondedDials)
        }
        // monitorexit(bondedDials)
        return b;
    }
    
    private void bondWithDial(final BluetoothDevice bondingDial, final ScanRecord scanRecord) {
        this.bondTries = 0;
        this.bondingDial = bondingDial;
        DialManager.sLogger.v("[Dial]calling createBond [" + bondingDial.getName() + "] addr[" + bondingDial.getAddress() + "]" + " rssi[" + scanRecord.getTxPowerLevel() + "] state[" + BluetoothUtils.getBondState(bondingDial.getBondState()) + "]");
        final boolean removeFromBondList = this.removeFromBondList(bondingDial);
        if (removeFromBondList || bondingDial.getBondState() == 12) {
            if (removeFromBondList) {
                DialManager.sLogger.v("[Dial]present in bond list:" + bondingDial.getName() + " invalid state");
            }
            if (bondingDial.getBondState() == 12) {
                DialManager.sLogger.v("[Dial]already bonded:" + bondingDial.getName() + " invalid state");
            }
            this.bondingDial = null;
            this.disconectAndRemoveBond(bondingDial, new DialManagerHelper.IDialForgotten() {
                @Override
                public void onForgotten() {
                    final ArrayList access$4400 = DialManager.this.bondedDials;
                    synchronized (access$4400) {
                        final Iterator<BluetoothDevice> iterator = (Iterator<BluetoothDevice>)DialManager.this.bondedDials.iterator();
                        while (iterator.hasNext()) {
                            if (TextUtils.equals((CharSequence)iterator.next().getName(), (CharSequence)bondingDial.getName())) {
                                iterator.remove();
                                break;
                            }
                        }
                        // monitorexit(access$4400)
                        DialManager.this.bondTries = 0;
                        DialManager.DIAL_NOT_CONNECTED.dialName = bondingDial.getName();
                        DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
                    }
                }
            }, 2000);
        }
        else {
            ++this.bondTries;
            this.handler.removeCallbacks(this.bondHangRunnable);
            DialManager.DIAL_CONNECTING.dialName = bondingDial.getName();
            this.bus.post(DialManager.DIAL_CONNECTING);
            bondingDial.createBond();
            this.handler.postDelayed(this.bondHangRunnable, 30000L);
        }
    }
    
    private void buildDialList() {
        ArrayList<BluetoothDevice> list = null;
        Label_0261: {
            while (true) {
                while (true) {
                    Set bondedDevices = null;
                    Label_0250: {
                        try {
                            DialManager.sLogger.v("[Dial]trying to find paired dials");
                            bondedDevices = this.bluetoothAdapter.getBondedDevices();
                            final Logger sLogger = DialManager.sLogger;
                            final StringBuilder append = new StringBuilder().append("[Dial]Bonded devices:");
                            if (bondedDevices == null) {
                                final int size = 0;
                                sLogger.v(append.append(size).toString());
                                list = new ArrayList<BluetoothDevice>(8);
                                for (final BluetoothDevice bluetoothDevice : bondedDevices) {
                                    DialManager.sLogger.v("[Dial]Bonded Device Address[" + bluetoothDevice.getAddress() + "] name[" + bluetoothDevice.getName() + " type[" + BluetoothUtils.getDeviceType(bluetoothDevice.getType()) + "]");
                                    if (isDialDeviceName(bluetoothDevice.getName())) {
                                        DialManager.sLogger.v("[Dial]found paired dial Address[" + bluetoothDevice + "] name[" + bluetoothDevice.getName() + "]");
                                        list.add(bluetoothDevice);
                                    }
                                }
                                break Label_0261;
                            }
                            break Label_0250;
                        }
                        catch (Throwable t) {
                            DialManager.sLogger.e("[Dial]", t);
                        }
                        break;
                    }
                    final int size = bondedDevices.size();
                    continue;
                }
            }
            return;
        }
        if (list.size() == 0) {
            DialManager.sLogger.v("[Dial]no dial device found");
            return;
        }
        DialManager.sLogger.v("[Dial]found paired dials:" + list.size());
        final ArrayList<BluetoothDevice> bondedDials = this.bondedDials;
        synchronized (bondedDials) {
            this.bondedDials.clear();
            this.bondedDials.addAll(list);
        }
    }
    
    private void checkDialConnections() {
        try {
            final ArrayList<BluetoothDevice> bondedDials = this.bondedDials;
            synchronized (bondedDials) {
                for (final BluetoothDevice bluetoothDevice : this.bondedDials) {
                    DialManagerHelper.isDialConnected(this.bluetoothAdapter, bluetoothDevice, (DialManagerHelper.IDialConnectionStatus)new DialManagerHelper.IDialConnectionStatus() {
                        @Override
                        public void onStatus(final boolean b) {
                            if (!b) {
                                DialManager.sLogger.v("checkDialConnections: dial " + bluetoothDevice.getName() + " not connected");
                            }
                            else {
                                DialManager.sLogger.v("checkDialConnections: dial " + bluetoothDevice.getName() + " connected");
                                if (DialManager.this.connectedDial == null) {
                                    DialManager.this.connectedDial = bluetoothDevice;
                                    DialManager.sLogger.v("marking dial [" + bluetoothDevice.getName() + "] as connected");
                                    DialManager.this.handleDialConnection();
                                }
                            }
                        }
                    });
                }
            }
        }
        catch (Throwable t) {
            DialManager.sLogger.e("[Dial]", t);
        }
        return;
    }
    // monitorexit(list)
    
    private void clearConnectedDial() {
        this.firmWareVersion = null;
        this.hardwareVersion = null;
        this.lastKnownBatteryLevel = -1;
        this.lastKnownRawBatteryLevel = null;
        this.lastKnownSystemTemperature = null;
        this.connectedDial = null;
    }
    
    private void connectEvent(final BluetoothDevice bluetoothDevice) {
        DialManager.sLogger.v("[DialEvent-connect] " + bluetoothDevice);
        this.handler.removeCallbacks(this.bondHangRunnable);
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        if (this.connectedDial != null) {
            DialManager.sLogger.v("[DialEvent-connect] ignore already connected dial:" + this.connectedDial);
        }
        else if (this.isPresentInBondList(bluetoothDevice)) {
            DialManager.sLogger.v("[DialEvent-connect] not on dial pairing screen, mark as connected");
            this.connectedDial = bluetoothDevice;
            this.handleDialConnection();
        }
        else {
            DialManager.sLogger.v("launch connectionErrorRunnable");
            this.tempConnectedDial = bluetoothDevice;
            this.handler.postDelayed(this.connectionErrorRunnable, 5000L);
        }
    }
    
    private void disconectAndRemoveBond(final BluetoothDevice bluetoothDevice, final DialManagerHelper.IDialForgotten dialForgotten, final int n) {
        DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, bluetoothDevice, (DialManagerHelper.IDialConnection)new DialManagerHelper.IDialConnection() {
            @Override
            public void onAttemptConnection(final boolean b) {
            }
            
            @Override
            public void onAttemptDisconnection(final boolean b) {
                GenericUtil.checkNotOnMainThread();
                DialManager.sLogger.v("attempt dis-connection [" + bluetoothDevice.getName() + " ] success[" + b + "] ");
                if (n > 0) {
                    GenericUtil.sleep(n);
                }
                DialManager.this.removeBond(bluetoothDevice);
                if (dialForgotten != null) {
                    dialForgotten.onForgotten();
                }
            }
        });
    }
    
    private void disconnectEvent(final BluetoothDevice bluetoothDevice) {
        this.handler.removeCallbacks(this.bondHangRunnable);
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        this.tempConnectedDial = null;
        if (this.connectedDial == null) {
            DialManager.sLogger.v("[DialEvent-disconnect] no connected dial");
        }
        else if (!this.connectedDial.equals(bluetoothDevice)) {
            DialManager.sLogger.v("[DialEvent-disconnect] notif not for connected dial connected[" + this.connectedDial + "]");
        }
        else {
            this.disconnectedDial = this.connectedDial.getName();
            this.clearConnectedDial();
            this.stopGATTClient();
            DialManager.DIAL_NOT_CONNECTED.dialName = this.disconnectedDial;
            this.bus.post(DialManager.DIAL_NOT_CONNECTED);
            DialManager.sLogger.v("[DialEvent-disconnect] dial marked not connected");
        }
    }
    
    private void encryptionFailedEvent(final BluetoothDevice bluetoothDevice) {
        if (this.isPresentInBondList(bluetoothDevice)) {
            if (this.encryptionRetryCount < 2) {
                ++this.encryptionRetryCount;
                DialManager.sLogger.e("[DialEvent-encryptfail] Attempt " + this.encryptionRetryCount + " will retry.");
                this.stopGATTClient();
                GenericUtil.sleep(2000);
                DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, bluetoothDevice, (DialManagerHelper.IDialConnection)new DialManagerHelper.IDialConnection() {
                    @Override
                    public void onAttemptConnection(final boolean b) {
                    }
                    
                    @Override
                    public void onAttemptDisconnection(final boolean b) {
                        DialManagerHelper.sendLocalyticsEvent(DialManager.this.handler, false, false, 0, false, "Encryption failed, retrying");
                    }
                });
            }
            else {
                this.removeFromBondList(bluetoothDevice);
                DialManager.sLogger.e("[DialEvent-encryptfail] device found in bonded dial list, stop gatt");
                this.stopGATTClient();
                DialManager.sLogger.e("[DialEvent-encryptfail] sleeping");
                GenericUtil.sleep(2000);
                DialManager.sLogger.e("[DialEvent-encryptfail] sleep");
                DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, bluetoothDevice, (DialManagerHelper.IDialConnection)new DialManagerHelper.IDialConnection() {
                    @Override
                    public void onAttemptConnection(final boolean b) {
                    }
                    
                    @Override
                    public void onAttemptDisconnection(final boolean b) {
                        DialManager.sLogger.e("[DialEvent-encryptfail] disconnected");
                        DialManager.this.handler.postDelayed((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                DialManager.this.forgetDial(bluetoothDevice);
                                DialManager.sLogger.e("[DialEvent-encryptfail] bond removed :-/ connected[" + DialManager.this.connectedDial + "] event[" + bluetoothDevice + "]");
                                if (bluetoothDevice.equals(DialManager.this.connectedDial)) {
                                    DialManager.this.disconnectedDial = DialManager.this.connectedDial.getName();
                                    DialManager.this.clearConnectedDial();
                                    DialManager.DIAL_CONNECTION_FAILED.dialName = DialManager.this.disconnectedDial;
                                    DialManager.this.bus.post(DialManager.DIAL_CONNECTION_FAILED);
                                    DialManager.sLogger.e("[DialEvent-encryptfail] current dial is disconnected");
                                }
                                DialManagerHelper.sendLocalyticsEvent(DialManager.this.handler, false, false, 0, false, "Encryption failed, deleting bond");
                            }
                        }, 3000L);
                    }
                });
            }
        }
        else {
            this.encryptionRetryCount = 0;
            DialManager.sLogger.e("[DialEventsListener] device not in bonded dial list");
        }
    }
    
    public static DialConstants.NotificationReason getBatteryLevelCategory(final int n) {
        DialConstants.NotificationReason notificationReason;
        if (n > 50 && n <= 80) {
            notificationReason = DialConstants.NotificationReason.LOW_BATTERY;
        }
        else if (n > 10 && n <= 50) {
            notificationReason = DialConstants.NotificationReason.VERY_LOW_BATTERY;
        }
        else if (n <= 10) {
            notificationReason = DialConstants.NotificationReason.EXTREMELY_LOW_BATTERY;
        }
        else {
            notificationReason = DialConstants.NotificationReason.OK_BATTERY;
        }
        return notificationReason;
    }
    
    private BluetoothDevice getBluetoothDevice(final String s) {
        final ArrayList<BluetoothDevice> bondedDials = this.bondedDials;
        synchronized (bondedDials) {
            for (final BluetoothDevice bluetoothDevice : this.bondedDials) {
                if (TextUtils.equals((CharSequence)bluetoothDevice.getName(), (CharSequence)s)) {
                    // monitorexit(bondedDials)
                    return bluetoothDevice;
                }
            }
            // monitorexit(bondedDials)
            return null;
        }
    }
    
    public static int getDisplayBatteryLevel(final int n) {
        return (int)(n * 0.1f);
    }
    
    public static DialManager getInstance() {
        return DialManager.singleton;
    }
    
    private void handleDialConnection() {
        this.encryptionRetryCount = 0;
        final ToastManager instance = ToastManager.getInstance();
        DialNotification.dismissAllBatteryToasts();
        instance.dismissCurrentToast("dial-disconnect");
        instance.clearPendingToast("dial-disconnect");
        DialManager.DIAL_CONNECTED.dialName = this.getDialName();
        this.bus.post(DialManager.DIAL_CONNECTED);
        this.startGATTClient();
    }
    
    private void handleInputReportDescriptor(final BluetoothDevice connectedDial) {
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        DialManager.sLogger.v("[DialEvent-handleInputReportDescriptor] " + connectedDial);
        if (this.tempConnectedDial == null) {
            DialManager.sLogger.v("[DialEvent-handleInputReportDescriptor] no temp connected dial");
        }
        else if (this.connectedDial != null) {
            DialManager.sLogger.v("[DialEvent-handleInputReportDescriptor] dial already connected[" + this.connectedDial + "]");
            if (this.connectedDial.equals(connectedDial)) {
                DialManager.DIAL_CONNECTED.dialName = this.getDialName();
                this.bus.post(DialManager.DIAL_CONNECTED);
            }
        }
        else {
            DialManager.sLogger.v("[DialEvent-handleInputReportDescriptor] done");
            this.tempConnectedDial = null;
            this.connectedDial = connectedDial;
            this.handleDialConnection();
        }
    }
    
    private void handleScannedDevice(final BluetoothDevice bluetoothDevice, final int n, final ScanRecord scanRecord) {
        if (bluetoothDevice.getType() == 2) {
            if (!this.isScanning) {
                DialManager.sLogger.i("[Dial]not scanning anymore [" + bluetoothDevice.getName() + "]");
            }
            else {
                final Map<BluetoothDevice, ScanRecord> scannedNavdyDials = this.scannedNavdyDials;
                synchronized (scannedNavdyDials) {
                    if (this.scannedNavdyDials.containsKey(bluetoothDevice)) {
                        return;
                    }
                }
                // monitorexit(map)
                final BluetoothDevice bluetoothDevice2;
                final String name = bluetoothDevice2.getName();
                if (!TextUtils.isEmpty((CharSequence)name)) {
                    if (!this.isDialDevice(bluetoothDevice2)) {
                        if (!this.ignoredNonNavdyDials.contains(bluetoothDevice2)) {
                            DialManager.sLogger.v("[Dial]ignoring [" + name + "]");
                            this.ignoredNonNavdyDials.add(bluetoothDevice2);
                        }
                    }
                    else {
                        final List serviceUuids = scanRecord.getServiceUuids();
                        if (serviceUuids != null) {
                            final boolean b = false;
                            final Iterator<ParcelUuid> iterator = serviceUuids.iterator();
                            while (true) {
                                do {
                                    final boolean b2 = b;
                                    if (iterator.hasNext()) {
                                        continue;
                                    }
                                    if (!b2) {
                                        if (!this.ignoredNonNavdyDials.contains(bluetoothDevice2)) {
                                            DialManager.sLogger.v("[Dial]HID service not found ignoring [" + name + "]");
                                            this.ignoredNonNavdyDials.add(bluetoothDevice2);
                                        }
                                        return;
                                    }
                                    else {
                                        DialManager.sLogger.v("[Dial]Scanned name[" + bluetoothDevice2.getName() + "] mac[" + bluetoothDevice2.getAddress() + "] bonded[" + BluetoothUtils.getBondState(bluetoothDevice2.getBondState()) + "] rssi[" + n + "]" + " type[" + BluetoothUtils.getDeviceType(bluetoothDevice2.getType()) + "]");
                                        final Map<BluetoothDevice, ScanRecord> scannedNavdyDials2 = this.scannedNavdyDials;
                                        synchronized (scannedNavdyDials2) {
                                            this.scannedNavdyDials.put(bluetoothDevice2, scanRecord);
                                            // monitorexit(scannedNavdyDials2)
                                            if (DialConstants.PAIRING_RULE == DialConstants.PairingRule.FIRST) {
                                                DialManager.sLogger.v("[Dial]pairing rule is first, stopping scan...");
                                                this.handler.removeCallbacks(this.stopScanRunnable);
                                                this.handler.post(this.stopScanRunnable);
                                            }
                                            return;
                                        }
                                    }
                                } while (!iterator.next().getUuid().equals(DialConstants.HID_SERVICE_UUID));
                                final boolean b2 = true;
                                continue;
                            }
                        }
                        DialManager.sLogger.v("[Dial]no service uuid found [" + name + "]");
                    }
                }
            }
        }
    }
    
    private void init() {
        synchronized (this) {
            DialManager.sLogger.v("[Dial]Client Address:" + this.bluetoothAdapter.getAddress() + " name:" + this.bluetoothAdapter.getName());
            this.bluetoothLEScanner = this.bluetoothAdapter.getBluetoothLeScanner();
            DialManager.sLogger.v("[Dial]btle scanner:" + this.bluetoothLEScanner);
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    if (!DialManager.this.dialManagerInitialized) {
                        DialManager.this.buildDialList();
                        DialManager.this.checkDialConnections();
                        DialManager.this.dialManagerInitialized = true;
                        DialManager.this.bus.post(DialConstants.INIT_EVENT);
                        DialManager.sLogger.v("dial manager initialized");
                    }
                }
            });
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    if (DialManager.this.dialFirmwareUpdater == null) {
                        DialManager.this.dialFirmwareUpdater = new DialFirmwareUpdater(DialManager.this, DialManager.this.handler);
                        DialManager.this.dialFirmwareUpdater.setUpdateListener((DialFirmwareUpdater.UpdateListener)new DialFirmwareUpdater.UpdateListener() {
                            @Override
                            public void onUpdateState(final boolean b) {
                                DialManager.this.bus.post(new DialUpdateStatus(b));
                            }
                        });
                    }
                }
            });
        }
    }
    
    public static boolean isDialDeviceName(final String s) {
        final boolean b = false;
        boolean b2;
        if (s == null) {
            b2 = b;
        }
        else {
            int n = 0;
            while (true) {
                b2 = b;
                if (n >= DialConstants.NAVDY_DIAL_FILTER.length) {
                    return b2;
                }
                if (s.contains(DialConstants.NAVDY_DIAL_FILTER[n])) {
                    break;
                }
                ++n;
            }
            b2 = true;
        }
        return b2;
    }
    
    private boolean isPresentInBondList(final BluetoothDevice bluetoothDevice) {
        boolean b = false;
        if (bluetoothDevice != null) {
            final ArrayList<BluetoothDevice> bondedDials = this.bondedDials;
            Label_0068: {
                synchronized (bondedDials) {
                    final Iterator<BluetoothDevice> iterator = this.bondedDials.iterator();
                    while (iterator.hasNext()) {
                        if (iterator.next().equals(bluetoothDevice)) {
                            return true;
                        }
                    }
                    break Label_0068;
                    b = true;
                    return b;
                }
            }
        }
        // monitorexit(list)
        return b;
    }
    
    private void processBatteryLevel(final int lastKnownBatteryLevel) {
        final DialConstants.NotificationReason batteryLevelCategory = getBatteryLevelCategory(lastKnownBatteryLevel);
        this.lastKnownBatteryLevel = lastKnownBatteryLevel;
        DialManager.sLogger.v("battery status current[" + batteryLevelCategory + "] previous[" + this.notificationReasonBattery + "]");
        if (batteryLevelCategory == DialConstants.NotificationReason.OK_BATTERY) {
            if (this.notificationReasonBattery != null && this.notificationReasonBattery != DialConstants.NotificationReason.OK_BATTERY) {
                this.notificationReasonBattery = null;
            }
            DialNotification.dismissAllBatteryToasts();
        }
        else if (this.notificationReasonBattery == batteryLevelCategory) {
            DialManager.sLogger.v("battery no change");
        }
        else {
            switch (batteryLevelCategory) {
                case EXTREMELY_LOW_BATTERY:
                    DialManager.sLogger.v("battery notification change");
                    this.notificationReasonBattery = batteryLevelCategory;
                    break;
                case LOW_BATTERY:
                    if (this.notificationReasonBattery == DialConstants.NotificationReason.EXTREMELY_LOW_BATTERY || this.notificationReasonBattery == DialConstants.NotificationReason.VERY_LOW_BATTERY) {
                        DialManager.sLogger.v("battery threshold change-ignore");
                        break;
                    }
                    DialManager.sLogger.v("battery notification change");
                    this.notificationReasonBattery = batteryLevelCategory;
                    break;
                case VERY_LOW_BATTERY:
                    if (this.notificationReasonBattery == DialConstants.NotificationReason.EXTREMELY_LOW_BATTERY) {
                        DialManager.sLogger.v("battery threshold change-ignore");
                        break;
                    }
                    DialManager.sLogger.v("battery notification change");
                    this.notificationReasonBattery = batteryLevelCategory;
                    break;
            }
        }
    }
    
    private void processRawBatteryLevel(final Integer lastKnownRawBatteryLevel) {
        this.lastKnownRawBatteryLevel = lastKnownRawBatteryLevel;
    }
    
    private void processSystemTemperature(final Integer lastKnownSystemTemperature) {
        this.lastKnownSystemTemperature = lastKnownSystemTemperature;
    }
    
    private void registerReceivers() {
        final Context appContext = HudApplication.getAppContext();
        appContext.registerReceiver(this.bluetoothBondingReceiver, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.device.action.ACL_CONNECTED");
        intentFilter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        intentFilter.addAction("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED");
        appContext.registerReceiver(this.bluetoothConnectivityReceiver, intentFilter);
        appContext.registerReceiver(this.bluetoothOnOffReceiver, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }
    
    private void removeBond(final BluetoothDevice bluetoothDevice) {
        try {
            final Method method = bluetoothDevice.getClass().getMethod("removeBond", (Class<?>[])null);
            if (method != null) {
                DialManager.sLogger.e("[Dial]removingBond [" + bluetoothDevice.getName() + "]");
                method.invoke(bluetoothDevice, (Object[])null);
                DialManager.sLogger.e("[Dial]removedBond");
            }
            else {
                DialManager.sLogger.e("[Dial]cannot get to removeBond api");
            }
        }
        catch (Throwable t) {
            DialManager.sLogger.e("[Dial]", t);
        }
    }
    
    private boolean removeFromBondList(final BluetoothDevice bluetoothDevice) {
        boolean b = false;
        if (bluetoothDevice != null) {
            // monitorenter(bondedDials = this.bondedDials)
            boolean b2 = false;
            try {
                final Iterator<BluetoothDevice> iterator = this.bondedDials.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().equals(bluetoothDevice)) {
                        b2 = true;
                    }
                }
                if (b2) {
                    this.bondedDials.remove(bluetoothDevice);
                    b = true;
                    return b;
                }
            }
            finally {
            }
            // monitorexit(bondedDials)
        }
        // monitorexit(bondedDials)
        return b;
    }
    
    private void startDialEventListener() {
        synchronized (this) {
            if (this.dialEventsListenerThread == null) {
                (this.dialEventsListenerThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Object o = null;
                        while (true) {
                            try {
                                Object o2;
                                while (true) {
                                    DialManager.sLogger.v("DialEventsListener thread enter");
                                    final DatagramSocket datagramSocket = new DatagramSocket(23654, InetAddress.getByName("127.0.0.1"));
                                    while (true) {
                                        String s = null;
                                        int index = 0;
                                        Label_0176: {
                                            try {
                                                o = new byte[56];
                                                final DatagramPacket datagramPacket = new DatagramPacket((byte[])o, ((byte[])o).length);
                                                while (true) {
                                                    datagramSocket.receive(datagramPacket);
                                                    s = new String((byte[])o, 0, datagramPacket.getLength());
                                                    DialManager.sLogger.v("[DialEventsListener] [" + s + "]");
                                                    try {
                                                        index = s.indexOf(",");
                                                        if (index >= 0) {
                                                            break Label_0176;
                                                        }
                                                        DialManager.sLogger.v("[DialEventsListener] invalid data");
                                                    }
                                                    catch (Throwable t) {
                                                        DialManager.sLogger.e("[DialEventsListener]", t);
                                                    }
                                                }
                                            }
                                            catch (Throwable o) {
                                                o2 = o;
                                                o = datagramSocket;
                                            }
                                            break;
                                        }
                                        final String substring = s.substring(0, index);
                                        final String upperCase = s.substring(index + 1).toUpperCase();
                                        DialManager.this.bluetoothAdapter;
                                        if (!BluetoothAdapter.checkBluetoothAddress(upperCase)) {
                                            DialManager.sLogger.e("[DialEventsListener] not a valid bluetooth address");
                                            continue;
                                        }
                                        final BluetoothDevice remoteDevice = DialManager.this.bluetoothAdapter.getRemoteDevice(upperCase);
                                        DialManager.sLogger.v("[DialEventsListener] event for [" + remoteDevice + "]");
                                        if (!DialManager.this.isPresentInBondList(remoteDevice)) {
                                            DialManager.sLogger.v("[DialEventsListener] " + upperCase + " not present in bond list, ignore");
                                            continue;
                                        }
                                        switch (substring) {
                                            default:
                                                continue;
                                            case "CONNECT":
                                                DialManager.this.connectEvent(remoteDevice);
                                                continue;
                                            case "DISCONNECT":
                                                DialManager.this.disconnectEvent(remoteDevice);
                                                continue;
                                            case "ENCRYPTION_FAILED":
                                                DialManager.this.encryptionFailedEvent(remoteDevice);
                                                continue;
                                            case "INPUTDESCRIPTOR_WRITTEN":
                                                DialManager.this.handleInputReportDescriptor(remoteDevice);
                                                continue;
                                        }
                                        break;
                                    }
                                }
                                DialManager.sLogger.e((Throwable)o2);
                                if (o != null) {
                                    IOUtils.closeStream((Closeable)o);
                                }
                                DialManager.sLogger.v("DialEventsListener thread exit");
                            }
                            catch (Throwable t2) {
                                final Object o2 = t2;
                                continue;
                            }
                            break;
                        }
                    }
                })).setName("dialEventListener");
                this.dialEventsListenerThread.start();
                DialManager.sLogger.v("dialEventListener thread started");
            }
        }
    }
    
    private void startGATTClient() {
        synchronized (this) {
            try {
                if (this.bluetoothGatt == null) {
                    DialManager.sLogger.v("[Dial]startGATTClient, launched gatt");
                    this.isGattOn = true;
                    this.bluetoothGatt = this.connectedDial.connectGatt(HudApplication.getAppContext(), true, this.gattCallback);
                }
            }
            catch (Throwable t) {
                DialManager.sLogger.e("[Dial]", t);
                this.isGattOn = false;
            }
        }
    }
    
    private void stopGATTClient() {
        // monitorenter(this)
        try {
            Label_0026: {
                if (this.bluetoothGatt != null) {
                    break Label_0026;
                }
                try {
                    this.bluetoothGatt = null;
                    this.isGattOn = false;
                    return;
                    this.handler.removeCallbacks(this.batteryReadRunnable);
                    this.bluetoothGatt.close();
                    DialManager.sLogger.v("[Dial]stopGATTClient, gatt closed");
                    this.dialFirmwareUpdater.cancelUpdate();
                    this.bluetoothGatt = null;
                    this.isGattOn = false;
                }
                finally {
                }
                // monitorexit(this)
            }
        }
        catch (Throwable t) {
            DialManager.sLogger.e("[Dial]gatt closed", t);
        }
        finally {
            this.bluetoothGatt = null;
            this.isGattOn = false;
        }
    }
    
    public void forgetAllDials(final DialManagerHelper.IDialForgotten dialForgotten) {
        this.requestDialForgetKeys();
        this.handler.removeCallbacks(this.bondHangRunnable);
        final ArrayList<BluetoothDevice> bondedDials = this.bondedDials;
        synchronized (bondedDials) {
            DialManager.sLogger.v("forget all dials:" + this.bondedDials.size());
            final ArrayList<BluetoothDevice> list = new ArrayList<BluetoothDevice>(this.bondedDials);
            this.bondedDials.clear();
            this.clearConnectedDial();
            final AtomicInteger atomicInteger = new AtomicInteger(list.size());
            final Iterator<BluetoothDevice> iterator = list.iterator();
            while (iterator.hasNext()) {
                this.disconectAndRemoveBond(iterator.next(), new DialManagerHelper.IDialForgotten() {
                    @Override
                    public void onForgotten() {
                        final int decrementAndGet = atomicInteger.decrementAndGet();
                        DialManager.sLogger.v("forgetAllDials counter:" + decrementAndGet);
                        if (decrementAndGet == 0) {
                            DialManager.DIAL_NOT_CONNECTED.dialName = null;
                            DialManager.this.bus.post(DialManager.DIAL_NOT_CONNECTED);
                            if (dialForgotten != null) {
                                dialForgotten.onForgotten();
                            }
                        }
                    }
                }, 2000);
            }
        }
    }
    // monitorexit(list2)
    
    public void forgetAllDials(final boolean b, final DialManagerHelper.IDialForgotten dialForgotten) {
        final boolean b2 = true;
        final int bondedDialCount = this.getBondedDialCount();
        final String bondedDialList = this.getBondedDialList();
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                DialManager.this.forgetAllDials(dialForgotten);
            }
        }, 1);
        if (b) {
            final ToastManager instance = ToastManager.getInstance();
            instance.dismissCurrentToast();
            instance.clearAllPendingToast();
            instance.disableToasts(false);
            DialNotification.showForgottenToast(bondedDialCount > 1 && b2, bondedDialList);
        }
    }
    
    public void forgetDial(final BluetoothDevice bluetoothDevice) {
        this.removeBond(bluetoothDevice);
        this.removeFromBondList(bluetoothDevice);
    }
    
    public DialConstants.NotificationReason getBatteryNotificationReason() {
        return this.notificationReasonBattery;
    }
    
    public int getBondedDialCount() {
        final ArrayList<BluetoothDevice> bondedDials = this.bondedDials;
        synchronized (bondedDials) {
            return this.bondedDials.size();
        }
    }
    
    public String getBondedDialList() {
        final StringBuilder sb = new StringBuilder();
        final ArrayList<BluetoothDevice> bondedDials = this.bondedDials;
        synchronized (bondedDials) {
            for (int size = this.bondedDials.size(), i = 0; i < size; ++i) {
                sb.append(this.bondedDials.get(i).getName());
                if (i + 1 < size) {
                    sb.append(", ");
                }
            }
            // monitorexit(bondedDials)
            return sb.toString();
        }
    }
    
    BluetoothDevice getDialDevice() {
        return this.connectedDial;
    }
    
    public DialFirmwareUpdater getDialFirmwareUpdater() {
        return this.dialFirmwareUpdater;
    }
    
    public String getDialName() {
        String name;
        if (this.connectedDial != null) {
            name = this.connectedDial.getName();
        }
        else {
            name = null;
        }
        return name;
    }
    
    public String getFirmWareVersion() {
        return this.firmWareVersion;
    }
    
    public String getHardwareVersion() {
        return this.hardwareVersion;
    }
    
    public int getLastKnownBatteryLevel() {
        return this.lastKnownBatteryLevel;
    }
    
    public Integer getLastKnownRawBatteryLevel() {
        return this.lastKnownRawBatteryLevel;
    }
    
    public Integer getLastKnownSystemTemperature() {
        return this.lastKnownSystemTemperature;
    }
    
    public boolean isDialConnected() {
        return this.connectedDial != null;
    }
    
    public boolean isDialDevice(final BluetoothDevice bluetoothDevice) {
        final boolean b = false;
        boolean b2;
        if (bluetoothDevice == null) {
            b2 = b;
        }
        else {
            final String name = bluetoothDevice.getName();
            b2 = b;
            if (name != null) {
                b2 = b;
                if (isDialDeviceName(name)) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    public boolean isDialPaired() {
        return this.getBondedDialCount() > 0;
    }
    
    public boolean isInitialized() {
        return this.dialManagerInitialized;
    }
    
    public boolean isScanning() {
        return this.isScanning;
    }
    
    public void queueRead(final BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        if (this.gattCharacteristicProcessor == null) {
            DialManager.sLogger.w("gattCharacteristicProcessor not initialized");
        }
        else {
            this.gattCharacteristicProcessor.process((Command)new ReadCommand(bluetoothGattCharacteristic));
        }
    }
    
    public void queueWrite(final BluetoothGattCharacteristic bluetoothGattCharacteristic, final byte[] array) {
        if (this.gattCharacteristicProcessor == null) {
            DialManager.sLogger.w("gattCharacteristicProcessor not initialized");
        }
        else {
            this.gattCharacteristicProcessor.process((Command)new WriteCommand(bluetoothGattCharacteristic, array));
        }
    }
    
    public boolean reportInputEvent(final KeyEvent keyEvent) {
        final InputDevice device = keyEvent.getDevice();
        boolean b;
        if (device == null || device.isVirtual() || !isDialDeviceName(device.getName())) {
            b = false;
        }
        else {
            if (!this.isDialConnected()) {
                DialManager.sLogger.v("reportInputEvent:got dial connection via input:" + device.getName());
                this.connectedDial = this.getBluetoothDevice(device.getName());
                this.handler.removeCallbacks(this.connectionErrorRunnable);
                this.handler.removeCallbacks(this.bondHangRunnable);
                DialManager.sLogger.v("reportInputEvent remove connectionErrorRunnable");
                if (this.connectedDial != null) {
                    DialManager.sLogger.v("reportInputEvent: got device from bonded list");
                    this.handleDialConnection();
                }
                else {
                    DialManager.sLogger.i("reportInputEvent: did not get device from bonded list");
                    this.buildDialList();
                    this.connectedDial = this.getBluetoothDevice(device.getName());
                    if (this.connectedDial != null) {
                        DialManager.sLogger.v("reportInputEvent: got device from new bonded list");
                        this.handleDialConnection();
                    }
                    else {
                        DialManager.sLogger.v("reportInputEvent: did not get device from new bonded list");
                    }
                }
            }
            else {
                final BaseScreen currentScreen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
                if (currentScreen != null && currentScreen.getScreen() == Screen.SCREEN_DIAL_PAIRING) {
                    DialManager.sLogger.v("reportInputEvent: sent connected event");
                    DialManager.DIAL_CONNECTED.dialName = this.getDialName();
                    this.bus.post(DialManager.DIAL_CONNECTED);
                }
            }
            b = true;
        }
        return b;
    }
    
    public void requestDialForgetKeys() {
        try {
            if (this.dialForgetKeysCharacteristic != null) {
                DialManager.sLogger.v("Queueing dial forget keys request");
                this.queueWrite(this.dialForgetKeysCharacteristic, DialConstants.DIAL_FORGET_KEYS_MAGIC);
            }
            else {
                DialManager.sLogger.v("Not queuing dial forget keys request: null");
            }
        }
        catch (Throwable t) {
            DialManager.sLogger.e("[Dial]", t);
        }
    }
    
    public void requestDialReboot(final boolean b) {
        if (!b && System.currentTimeMillis() - this.sharedPreferences.getLong("last_dial_reboot", 0L) < DialConstants.MIN_TIME_BETWEEN_REBOOTS) {
            DialManager.sLogger.v("Not rebooting dial, last reboot too recent.");
        }
        else {
            Label_0081: {
                try {
                    if (this.dialRebootCharacteristic == null) {
                        break Label_0081;
                    }
                    DialManager.sLogger.v("Queueing dial reboot request");
                    this.queueWrite(this.dialRebootCharacteristic, DialConstants.DIAL_REBOOT_MAGIC);
                }
                catch (Throwable t) {
                    DialManager.sLogger.e("[Dial]", t);
                }
                return;
            }
            DialManager.sLogger.v("Not queuing dial reboot request: null");
        }
    }
    
    public void startScan() {
        final Map<BluetoothDevice, ScanRecord> scannedNavdyDials;
        try {
            if (this.isScanning) {
                DialManager.sLogger.v("scan already running");
            }
            else {
                this.handler.removeCallbacks(this.scanHangRunnable);
                this.bondingDial = null;
                // monitorenter(scannedNavdyDials = this.scannedNavdyDials)
                final DialManager dialManager = this;
                final Map<BluetoothDevice, ScanRecord> map = dialManager.scannedNavdyDials;
                map.clear();
                final DialManager dialManager2 = this;
                final Set<BluetoothDevice> set = dialManager2.ignoredNonNavdyDials;
                set.clear();
                final Map<BluetoothDevice, ScanRecord> map2 = scannedNavdyDials;
                // monitorexit(map2)
                final DialManager dialManager3 = this;
                final BluetoothLeScanner bluetoothLeScanner = dialManager3.bluetoothLEScanner;
                final DialManager dialManager4 = this;
                final ScanCallback scanCallback = dialManager4.leScanCallback;
                bluetoothLeScanner.startScan(scanCallback);
                final DialManager dialManager5 = this;
                final Handler handler = dialManager5.handler;
                final DialManager dialManager6 = this;
                final Runnable runnable = dialManager6.scanHangRunnable;
                final long n = 30000L;
                handler.postDelayed(runnable, n);
                final DialManager dialManager7 = this;
                final boolean b = true;
                dialManager7.isScanning = b;
            }
            return;
        }
        catch (Throwable t) {
            DialManager.sLogger.e(t);
            return;
        }
        try {
            final DialManager dialManager = this;
            final Map<BluetoothDevice, ScanRecord> map = dialManager.scannedNavdyDials;
            map.clear();
            final DialManager dialManager2 = this;
            final Set<BluetoothDevice> set = dialManager2.ignoredNonNavdyDials;
            set.clear();
            final Map<BluetoothDevice, ScanRecord> map2 = scannedNavdyDials;
            // monitorexit(map2)
            final DialManager dialManager3 = this;
            final BluetoothLeScanner bluetoothLeScanner = dialManager3.bluetoothLEScanner;
            final DialManager dialManager4 = this;
            final ScanCallback scanCallback = dialManager4.leScanCallback;
            bluetoothLeScanner.startScan(scanCallback);
            final DialManager dialManager5 = this;
            final Handler handler = dialManager5.handler;
            final DialManager dialManager6 = this;
            final Runnable runnable = dialManager6.scanHangRunnable;
            final long n = 30000L;
            handler.postDelayed(runnable, n);
            final DialManager dialManager7 = this;
            final boolean b = true;
            dialManager7.isScanning = b;
        }
        finally {
        }
        // monitorexit(scannedNavdyDials)
    }
    
    public void stopScan(final boolean p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/navdy/hud/app/device/dial/DialManager.isScanning:Z
        //     4: ifeq            230
        //     7: getstatic       com/navdy/hud/app/device/dial/DialManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //    10: ldc_w           "[Dial]stopping btle scan..."
        //    13: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    16: aload_0        
        //    17: getfield        com/navdy/hud/app/device/dial/DialManager.handler:Landroid/os/Handler;
        //    20: aload_0        
        //    21: getfield        com/navdy/hud/app/device/dial/DialManager.scanHangRunnable:Ljava/lang/Runnable;
        //    24: invokevirtual   android/os/Handler.removeCallbacks:(Ljava/lang/Runnable;)V
        //    27: aload_0        
        //    28: iconst_0       
        //    29: putfield        com/navdy/hud/app/device/dial/DialManager.isScanning:Z
        //    32: aload_0        
        //    33: getfield        com/navdy/hud/app/device/dial/DialManager.bluetoothLEScanner:Landroid/bluetooth/le/BluetoothLeScanner;
        //    36: aload_0        
        //    37: getfield        com/navdy/hud/app/device/dial/DialManager.leScanCallback:Landroid/bluetooth/le/ScanCallback;
        //    40: invokevirtual   android/bluetooth/le/BluetoothLeScanner.stopScan:(Landroid/bluetooth/le/ScanCallback;)V
        //    43: aload_0        
        //    44: getfield        com/navdy/hud/app/device/dial/DialManager.scannedNavdyDials:Ljava/util/Map;
        //    47: astore_2       
        //    48: aload_2        
        //    49: dup            
        //    50: astore_3       
        //    51: monitorenter   
        //    52: aload_0        
        //    53: getfield        com/navdy/hud/app/device/dial/DialManager.ignoredNonNavdyDials:Ljava/util/Set;
        //    56: invokeinterface java/util/Set.clear:()V
        //    61: getstatic       com/navdy/hud/app/device/dial/DialManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //    64: astore          4
        //    66: new             Ljava/lang/StringBuilder;
        //    69: astore          5
        //    71: aload           5
        //    73: invokespecial   java/lang/StringBuilder.<init>:()V
        //    76: aload           4
        //    78: aload           5
        //    80: ldc_w           "[Dial]stopscan dials found:"
        //    83: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    86: aload_0        
        //    87: getfield        com/navdy/hud/app/device/dial/DialManager.scannedNavdyDials:Ljava/util/Map;
        //    90: invokeinterface java/util/Map.size:()I
        //    95: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    98: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   101: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   104: aload_0        
        //   105: getfield        com/navdy/hud/app/device/dial/DialManager.scannedNavdyDials:Ljava/util/Map;
        //   108: invokeinterface java/util/Map.size:()I
        //   113: ifne            153
        //   116: getstatic       com/navdy/hud/app/device/dial/DialManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   119: ldc_w           "[Dial]No dials found"
        //   122: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   125: iload_1        
        //   126: ifeq            139
        //   129: aload_0        
        //   130: getfield        com/navdy/hud/app/device/dial/DialManager.bus:Lcom/squareup/otto/Bus;
        //   133: getstatic       com/navdy/hud/app/device/dial/DialManager.DIAL_NOT_FOUND:Lcom/navdy/hud/app/device/dial/DialConstants$DialConnectionStatus;
        //   136: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
        //   139: aload_3        
        //   140: monitorexit    
        //   141: return         
        //   142: astore_2       
        //   143: getstatic       com/navdy/hud/app/device/dial/DialManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   146: aload_2        
        //   147: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   150: goto            43
        //   153: aload_0        
        //   154: getfield        com/navdy/hud/app/device/dial/DialManager.scannedNavdyDials:Ljava/util/Map;
        //   157: invokeinterface java/util/Map.entrySet:()Ljava/util/Set;
        //   162: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //   167: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   172: checkcast       Ljava/util/Map$Entry;
        //   175: astore          5
        //   177: aload           5
        //   179: invokeinterface java/util/Map$Entry.getKey:()Ljava/lang/Object;
        //   184: checkcast       Landroid/bluetooth/BluetoothDevice;
        //   187: astore          4
        //   189: aload           5
        //   191: invokeinterface java/util/Map$Entry.getValue:()Ljava/lang/Object;
        //   196: checkcast       Landroid/bluetooth/le/ScanRecord;
        //   199: astore          5
        //   201: aload_0        
        //   202: getfield        com/navdy/hud/app/device/dial/DialManager.scannedNavdyDials:Ljava/util/Map;
        //   205: invokeinterface java/util/Map.clear:()V
        //   210: aload_3        
        //   211: monitorexit    
        //   212: aload_0        
        //   213: aload           4
        //   215: aload           5
        //   217: invokespecial   com/navdy/hud/app/device/dial/DialManager.bondWithDial:(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/le/ScanRecord;)V
        //   220: goto            141
        //   223: astore          4
        //   225: aload_3        
        //   226: monitorexit    
        //   227: aload           4
        //   229: athrow         
        //   230: getstatic       com/navdy/hud/app/device/dial/DialManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   233: ldc_w           "[Dial]not currently scanning"
        //   236: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
        //   239: goto            141
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  32     43     142    153    Ljava/lang/Throwable;
        //  52     125    223    230    Any
        //  129    139    223    230    Any
        //  139    141    223    230    Any
        //  153    212    223    230    Any
        //  225    227    223    230    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0139:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static class CharacteristicCommandProcessor
    {
        private BluetoothGatt bluetoothGatt;
        private LinkedList<Command> commandsQueue;
        private Handler handler;
        
        public CharacteristicCommandProcessor(final BluetoothGatt bluetoothGatt, final Handler handler) {
            this.bluetoothGatt = bluetoothGatt;
            this.handler = handler;
            this.commandsQueue = new LinkedList<Command>();
        }
        
        public void commandFinished() {
            synchronized (this) {
                if (this.commandsQueue.size() > 0) {
                    final Command command = this.commandsQueue.poll();
                    this.submitNext();
                }
            }
        }
        
        public void process(final Command command) {
            synchronized (this) {
                this.commandsQueue.add(command);
                if (this.commandsQueue.size() == 1) {
                    this.submitNext();
                }
            }
        }
        
        public void release() {
            this.commandsQueue.clear();
            this.handler = null;
            this.bluetoothGatt = null;
        }
        
        public void submitNext() {
            synchronized (this) {
                final Command command = this.commandsQueue.peek();
                if (command != null) {
                    this.handler.post((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (CharacteristicCommandProcessor.this.bluetoothGatt != null) {
                                    command.process(CharacteristicCommandProcessor.this.bluetoothGatt);
                                }
                            }
                            catch (Throwable t) {
                                DialManager.sLogger.d("Error while executing GATT command", t);
                            }
                        }
                    });
                }
            }
        }
        
        abstract static class Command
        {
            BluetoothGattCharacteristic characteristic;
            
            public Command(final BluetoothGattCharacteristic characteristic) {
                this.characteristic = characteristic;
            }
            
            public abstract void process(final BluetoothGatt p0);
        }
        
        static class ReadCommand extends Command
        {
            public ReadCommand(final BluetoothGattCharacteristic bluetoothGattCharacteristic) {
                super(bluetoothGattCharacteristic);
            }
            
            @Override
            public void process(final BluetoothGatt bluetoothGatt) {
                if (bluetoothGatt.readCharacteristic(this.characteristic)) {
                    DialManager.sLogger.d("Processing GATT read succeeded " + this.characteristic.getUuid().toString());
                }
                else {
                    DialManager.sLogger.d("Processing GATT read failed " + this.characteristic.getUuid().toString());
                }
            }
        }
        
        static class WriteCommand extends Command
        {
            byte[] data;
            
            public WriteCommand(final BluetoothGattCharacteristic bluetoothGattCharacteristic, final byte[] data) {
                super(bluetoothGattCharacteristic);
                this.data = data;
            }
            
            @Override
            public void process(final BluetoothGatt bluetoothGatt) {
                this.characteristic.setValue(this.data);
                if (bluetoothGatt.writeCharacteristic(this.characteristic)) {
                    if (!this.characteristic.getUuid().equals(DialConstants.OTA_DATA_CHARACTERISTIC_UUID)) {
                        DialManager.sLogger.d("Processing GATT write succeeded " + this.characteristic.getUuid().toString());
                    }
                }
                else {
                    DialManager.sLogger.d("Processing GATT write failed " + this.characteristic.getUuid().toString());
                }
            }
        }
    }
    
    public static class DialUpdateStatus
    {
        public boolean available;
        
        DialUpdateStatus(final boolean available) {
            this.available = available;
        }
    }
}
