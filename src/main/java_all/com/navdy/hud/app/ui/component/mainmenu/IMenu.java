package com.navdy.hud.app.ui.component.mainmenu;

import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import java.util.List;

public interface IMenu
{
    IMenu getChildMenu(final IMenu p0, final String p1, final String p2);
    
    int getInitialSelection();
    
    List<VerticalList.Model> getItems();
    
    VerticalList.Model getModelfromPos(final int p0);
    
    VerticalFastScrollIndex getScrollIndex();
    
    Menu getType();
    
    boolean isBindCallsEnabled();
    
    boolean isFirstItemEmpty();
    
    boolean isItemClickable(final int p0, final int p1);
    
    void onBindToView(final VerticalList.Model p0, final View p1, final int p2, final VerticalList.ModelState p3);
    
    void onFastScrollEnd();
    
    void onFastScrollStart();
    
    void onItemSelected(final VerticalList.ItemSelectionState p0);
    
    void onScrollIdle();
    
    void onUnload(final MenuLevel p0);
    
    boolean selectItem(final VerticalList.ItemSelectionState p0);
    
    void setBackSelectionId(final int p0);
    
    void setBackSelectionPos(final int p0);
    
    void setSelectedIcon();
    
    void showToolTip();
    
    public enum Menu
    {
        ACTIVE_TRIP, 
        CONTACTS, 
        CONTACT_OPTIONS, 
        MAIN, 
        MAIN_OPTIONS, 
        MESSAGE_PICKER, 
        MUSIC, 
        NUMBER_PICKER, 
        PLACES, 
        RECENT_CONTACTS, 
        RECENT_PLACES, 
        REPORT_ISSUE, 
        SEARCH, 
        SETTINGS, 
        SYSTEM_INFO, 
        TEST_FAST_SCROLL_MENU;
    }
    
    public enum MenuLevel
    {
        BACK_TO_PARENT, 
        CLOSE, 
        REFRESH_CURRENT, 
        ROOT, 
        SUB_LEVEL;
    }
}
