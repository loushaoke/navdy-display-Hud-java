package com.navdy.hud.app.ui.activity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import com.google.gson.Gson;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.ancs.AncsServiceConnector;
import com.navdy.hud.app.common.ProdModule;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.debug.GestureEngine;
import com.navdy.hud.app.debug.SettingsActivity;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialConstants;
import com.navdy.hud.app.device.dial.DialConstants.BatteryChangeEvent;
import com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus;
import com.navdy.hud.app.device.dial.DialConstants.DialConnectionStatus.Status;
import com.navdy.hud.app.device.dial.DialConstants.NotificationReason;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.device.gps.GpsUtils.GpsSatelliteData;
import com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.event.DisplayScaleChange;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.DriverProfileUpdated;
import com.navdy.hud.app.event.InitEvents.InitPhase;
import com.navdy.hud.app.event.InitEvents.Phase;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.event.Shutdown.State;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.AdaptiveBrightnessNotification;
import com.navdy.hud.app.framework.BrightnessNotification;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.music.MusicDetailsScreen;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationManager.NotificationChange;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.phonecall.CallNotification;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.framework.voice.VoiceAssistNotification;
import com.navdy.hud.app.framework.voice.VoiceSearchNotification;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture;
import com.navdy.hud.app.manager.InitManager;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.LocationFix;
import com.navdy.hud.app.maps.here.HereLocationFixManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ConnectionType;
import com.navdy.hud.app.presenter.NotificationPresenter;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.screen.AutoBrightnessScreen;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.screen.DialManagerScreen;
import com.navdy.hud.app.screen.DialUpdateConfirmationScreen;
import com.navdy.hud.app.screen.DialUpdateProgressScreen;
import com.navdy.hud.app.screen.FactoryResetScreen;
import com.navdy.hud.app.screen.FirstLaunchScreen;
import com.navdy.hud.app.screen.ForceUpdateScreen;
import com.navdy.hud.app.screen.GestureLearningScreen;
import com.navdy.hud.app.screen.OSUpdateConfirmationScreen;
import com.navdy.hud.app.screen.ShutDownScreen;
import com.navdy.hud.app.screen.TemperatureWarningScreen;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.hud.app.settings.MainScreenSettings;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.SystemTrayView;
import com.navdy.hud.app.ui.component.SystemTrayView.Device;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;
import com.navdy.hud.app.ui.component.destination.DestinationPickerScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.ui.framework.IScreenAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.FeatureUtil.Feature;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.util.OTAUpdateService.InstallingUpdate;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.util.ScreenOrientation;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.view.ContainerView;
import com.navdy.hud.app.view.MainView;
import com.navdy.hud.app.view.NotificationView;
import com.navdy.hud.app.view.ToastView;
import com.navdy.service.library.Version;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.events.settings.NetworkStateChange;
import com.navdy.service.library.events.settings.ReadSettingsRequest;
import com.navdy.service.library.events.settings.ReadSettingsResponse;
import com.navdy.service.library.events.settings.UpdateSettings;
import com.navdy.service.library.events.settings.ScreenConfiguration;
import com.navdy.service.library.events.ui.DismissScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import flow.Backstack;
import flow.Flow;
import flow.Flow.Callback;
import flow.Flow.Direction;
import flow.Flow.Listener;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javax.inject.Inject;
import javax.inject.Singleton;
import mortar.Blueprint;
import mortar.Mortar;

public class Main extends BaseScreen
{
    public static final String DEMO_VIDEO_PROPERTY = "persist.sys.demo.video";
    private static final String INSTALLING_OTA_TOAST_ID = "#ota#toast";
    public static final String MIME_TYPE_VIDEO = "video/mp4";
    static final String PREFERENCE_HOME_SCREEN_MODE = "PREFERENCE_LAST_HOME_SCREEN_MODE";
    private static SparseArray<Class> SCREEN_MAP;
    public static final int SNAPSHOT_TITLE_PICKER_DELAY_MILLIS = 2000;
    public static ProtocolStatus mProtocolStatus;
    private static Main mainScreen;
    private static Presenter presenter;
    private static final Logger sLogger;
    private static final boolean TRIPLE_CLICK_BRIGHTNESS = SystemProperties.getBoolean("persist.sys.hud.tripleclickbrightness", false);


    static {
        sLogger = new Logger(Main.class);
        (Main.SCREEN_MAP = (SparseArray<Class>)new SparseArray()).put(Screen.SCREEN_FIRST_LAUNCH.ordinal(), FirstLaunchScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_HOME.ordinal(), HomeScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_WELCOME.ordinal(), WelcomeScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_MAIN_MENU.ordinal(), MainMenuScreen2.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_OTA_CONFIRMATION.ordinal(), OSUpdateConfirmationScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_SHUTDOWN_CONFIRMATION.ordinal(), ShutDownScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_AUTO_BRIGHTNESS.ordinal(), AutoBrightnessScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_FACTORY_RESET.ordinal(), FactoryResetScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_DIAL_PAIRING.ordinal(), DialManagerScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_DIAL_UPDATE_PROGRESS.ordinal(), DialUpdateProgressScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_TEMPERATURE_WARNING.ordinal(), TemperatureWarningScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_FORCE_UPDATE.ordinal(), ForceUpdateScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_DIAL_UPDATE_CONFIRMATION.ordinal(), DialUpdateConfirmationScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_GESTURE_LEARNING.ordinal(), GestureLearningScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_DESTINATION_PICKER.ordinal(), DestinationPickerScreen.class);
        Main.SCREEN_MAP.put(Screen.SCREEN_MUSIC_DETAILS.ordinal(), MusicDetailsScreen.class);
        Main.mProtocolStatus = ProtocolStatus.PROTOCOL_VALID;
    }

    public Main() {
        Main.mainScreen = this;
    }

    public static void handleLibraryInitializationError(final String s) {
        Main.sLogger.e("Forcing update due to initialization failure: " + s);
        Main.mProtocolStatus = ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE;
        if (Main.presenter != null) {
            Main.presenter.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_FORCE_UPDATE).build());
        }
        else {
            Main.sLogger.e("unable to launch FORCE_UPDATE screen.");
        }
    }

    public static void saveHomeScreenPreference(final SharedPreferences sharedPreferences, final int n) {
        final boolean defaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
        final SharedPreferences localPreferencesForCurrentDriverProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
        if (!defaultProfile && localPreferencesForCurrentDriverProfile != null) {
            localPreferencesForCurrentDriverProfile.edit().putInt("PREFERENCE_LAST_HOME_SCREEN_MODE", n).apply();
        }
        if (sharedPreferences != null) {
            sharedPreferences.edit().putInt("PREFERENCE_LAST_HOME_SCREEN_MODE", n).apply();
        }
    }

    private static void showBrightnessNotification() {
        if (HUDSettings.USE_ADAPTIVE_AUTOBRIGHTNESS) {
            Main.sLogger.v("Use adaptive autobrightness");
            AdaptiveBrightnessNotification.showNotification();
        }
        else {
            Main.sLogger.v("Use non-adaptive autobrightness");
            BrightnessNotification.showNotification();
        }
    }

    public void addNotificationExtensionView(final View view) {
        if (Main.presenter != null) {
            Main.presenter.addNotificationExtensionView(view);
        }
    }

    public void clearInputFocus() {
        if (Main.presenter != null) {
            Main.presenter.clearInputFocus();
        }
    }

    public void ejectMainLowerView() {
        if (Main.presenter != null) {
            Main.presenter.ejectMainLowerView();
        }
    }

    public void enableNotificationColor(final boolean b) {
        if (Main.presenter != null) {
            Main.presenter.enableNotificationColor(b);
        }
    }

    public void enableSystemTray(final boolean b) {
        if (Main.presenter != null) {
            Main.presenter.enableSystemTray(b);
        }
    }

    @Override
    public int getAnimationIn(final Flow.Direction direction) {
        return -1;
    }

    @Override
    public int getAnimationOut(final Flow.Direction direction) {
        return -1;
    }

    @Override
    public Object getDaggerModule() {
        return new Module();
    }

    public View getExpandedNotificationCoverView() {
        View expandedNotificationCoverView;
        if (Main.presenter == null) {
            expandedNotificationCoverView = null;
        }
        else {
            expandedNotificationCoverView = Main.presenter.getExpandedNotificationCoverView();
        }
        return expandedNotificationCoverView;
    }

    public FrameLayout getExpandedNotificationView() {
        FrameLayout expandedNotificationView;
        if (Main.presenter == null) {
            expandedNotificationView = null;
        }
        else {
            expandedNotificationView = Main.presenter.getExpandedNotificationView();
        }
        return expandedNotificationView;
    }

    public InputManager.IInputHandler getInputHandler() {
        return Main.presenter;
    }

    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }

    public ViewGroup getNotificationExtensionViewHolder() {
        ViewGroup notificationExtensionView;
        if (Main.presenter != null) {
            notificationExtensionView = Main.presenter.getNotificationExtensionView();
        }
        else {
            notificationExtensionView = null;
        }
        return notificationExtensionView;
    }

    public CarouselIndicator getNotificationIndicator() {
        CarouselIndicator notificationIndicator;
        if (Main.presenter == null) {
            notificationIndicator = null;
        }
        else {
            notificationIndicator = Main.presenter.getNotificationIndicator();
        }
        return notificationIndicator;
    }

    public ProgressIndicator getNotificationScrollIndicator() {
        ProgressIndicator notificationScrollIndicator;
        if (Main.presenter == null) {
            notificationScrollIndicator = null;
        }
        else {
            notificationScrollIndicator = Main.presenter.getNotificationScrollIndicator();
        }
        return notificationScrollIndicator;
    }

    public NotificationView getNotificationView() {
        NotificationView notificationView;
        if (Main.presenter == null) {
            notificationView = null;
        }
        else {
            notificationView = Main.presenter.getNotificationView();
        }
        return notificationView;
    }

    @Override
    public Screen getScreen() {
        return null;
    }

    public void handleKey(final InputManager.CustomKeyEvent customKeyEvent) {
        if (Main.presenter != null) {
            Main.presenter.onKey(customKeyEvent);
        }
    }

    public void injectMainLowerView(final View view) {
        if (Main.presenter != null) {
            Main.presenter.injectMainLowerView(view);
        }
    }

    public boolean isMainLowerViewVisible() {
        return Main.presenter != null && Main.presenter.isMainLowerViewVisible();
    }

    public boolean isMainUIShrunk() {
        return Main.presenter != null && Main.presenter.isMainUIShrunk();
    }

    public boolean isNotificationCollapsing() {
        return Main.presenter != null && Main.presenter.isNotificationCollapsing();
    }

    public boolean isNotificationExpanding() {
        return Main.presenter != null && Main.presenter.isNotificationExpanding();
    }

    public boolean isNotificationViewShowing() {
        return Main.presenter != null && Main.presenter.isNotificationViewShowing();
    }

    public boolean isScreenAnimating() {
        return Main.presenter != null && Main.presenter.isScreenAnimating();
    }

    public void removeNotificationExtensionView() {
        if (Main.presenter != null) {
            Main.presenter.removeNotificationExtensionView();
        }
    }

    public void setInputFocus() {
        if (Main.presenter != null) {
            Main.presenter.setInputFocus();
        }
    }

    public void setSystemTrayVisibility(final int systemTrayVisibility) {
        if (Main.presenter != null) {
            Main.presenter.setSystemTrayVisibility(systemTrayVisibility);
        }
    }

    public interface INotificationExtensionView
    {
        void onStart();

        void onStop();
    }

    @dagger.Module(addsTo = ProdModule.class, injects = { MainActivity.class, MainView.class, NotificationView.class, NotificationPresenter.class, UIStateManager.class, ContainerView.class, GestureEngine.class })
    public class Module
    {
    }

    @Singleton
    public static class Presenter extends BasePresenter<MainView> implements Listener, IInputHandler
    {
        public static final String MAIN_SETTINGS = "main.settings";
        private boolean animationRunning;
        @Inject
        Bus bus;
        @Inject
        CallManager callManager;
        @Inject
        ConnectionHandler connectionHandler;
        private boolean createdScreens;
        private Screen currentScreen;
        private int defaultNotifColor;
        @Inject
        DialSimulatorMessagesHandler dialSimulatorMessagesHandler;
        @Inject
        DriverProfileManager driverProfileManager;
        Flow flow;
        @Inject
        SharedPreferences globalPreferences;
        private Handler handler;
        private HUDSettings hudSettings;
        private InitManager initManager;
        private MultipleClickGestureDetector innerEventsDetector;
        @Inject
        InputManager inputManager;
        @Inject
        MusicManager musicManager;
        private boolean notifAnimationRunning;
        private INotificationAnimationListener notificationAnimationListener;
        private boolean notificationColorEnabled;
        private NotificationManager notificationManager;
        private Queue<ScreenInfo> operationQueue;
        @Inject
        PandoraManager pandoraManager;
        @Inject
        PowerManager powerManager;
        @Inject
        SharedPreferences preferences;
        private IScreenAnimationListener screenAnimationListener;
        private DeferredServices services;
        private MainScreenSettings settings;
        @Inject
        SettingsManager settingsManager;
        private boolean showingPortrait;
        private boolean systemTrayEnabled;
        @Inject
        TripManager tripManager;
        @Inject
        UIStateManager uiStateManager;
        private VoiceAssistNotification voiceAssistNotification;
        private VoiceSearchNotification voiceSearchNotification;

        public Presenter() {
            this.systemTrayEnabled = true;
            this.notificationColorEnabled = true;
            this.innerEventsDetector = new MultipleClickGestureDetector(4, (MultipleClickGestureDetector.IMultipleClickKeyGesture)new MultipleClickGestureDetector.IMultipleClickKeyGesture() {
                @Override
                public IInputHandler nextHandler() {
                    return null;
                }

                @Override
                public boolean onGesture(final GestureEvent gestureEvent) {
                    return false;
                }

                @Override
                public boolean onKey(final CustomKeyEvent customKeyEvent) {
                    return Presenter.this.executeKeyEvent(customKeyEvent);
                }

                @Override
                public void onMultipleClick(final int n) {
                    Presenter.this.executeMultipleKeyEvent(n);
                }
            });
            this.operationQueue = new LinkedList<ScreenInfo>();
            this.handler = new Handler();
            this.screenAnimationListener = new IScreenAnimationListener() {
                @Override
                public void onStart(final BaseScreen baseScreen, final BaseScreen baseScreen2) {
                    Main.sLogger.v("screen anim start in[" + baseScreen + "]  out[" + baseScreen2 + "]");
                    Presenter.this.clearInputFocus();
                    final UIStateManager uiStateManager = Presenter.this.uiStateManager;
                    if (!UIStateManager.isFullscreenMode(baseScreen.getScreen())) {
                        Presenter.this.setNotificationColorVisibility(4);
                        Presenter.this.setSystemTrayVisibility(4);
                    }
                }

                @Override
                public void onStop(final BaseScreen baseScreen, final BaseScreen baseScreen2) {
                    Main.sLogger.v("screen anim stop in[" + baseScreen + "]  out[" + baseScreen2 + "]");
                    Presenter.this.updateSystemTrayVisibility(ScreenCategory.SCREEN, baseScreen.getScreen());
                }
            };
            this.notificationAnimationListener = new INotificationAnimationListener() {
                @Override
                public void onStart(final String s, final NotificationType notificationType, final UIStateManager.Mode mode) {
                    Main.sLogger.v("notification anim start id[" + s + "]  type[" + notificationType + "] mode[" + mode + "]");
                    if (mode == UIStateManager.Mode.EXPAND) {
                        Presenter.this.setNotificationColorVisibility(4);
                        Presenter.this.setSystemTrayVisibility(4);
                    }
                    Presenter.this.clearInputFocus();
                    Presenter.this.notifAnimationRunning = true;
                }

                @Override
                public void onStop(final String s, final NotificationType notificationType, final UIStateManager.Mode mode) {
                    Main.sLogger.v("notification anim stop id[" + s + "]  type[" + notificationType + "] mode[" + mode + "]");
                    Presenter.this.notifAnimationRunning = false;
                    if (mode == UIStateManager.Mode.COLLAPSE) {
                        Presenter.this.updateSystemTrayVisibility(ScreenCategory.NOTIFICATION, null);
                    }
                    Presenter.this.runQueuedOperation();
                }
            };
        }

        private void clearViews() {
            final MainView mainView = this.getView();
            if (mainView != null && mainView.isNotificationViewShowing() && !this.notifAnimationRunning) {
                Main.sLogger.v("NotificationManager:dismissNotification");
                mainView.dismissNotification();
            }
        }

        private boolean executeKeyEvent(final CustomKeyEvent customKeyEvent) {
            boolean b = false;
            switch (customKeyEvent) {
                case SELECT: {
                    this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_MAIN_MENU).build());
                    b = true;
                    break;
                }
                case LONG_PRESS: {
                    final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
                    if (instance.isRemoteDeviceConnected() && instance.isAppConnected()) {
                        if (DriverProfileHelper.getInstance().getCurrentProfile().getLongPressAction() == DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH) {
                            Main.sLogger.v("longpress, Place Search ");
                            this.launchVoiceSearch();
                        }
                        else {
                            this.startVoiceAssistance(false);
                        }
                    }
                    else {
                        this.connectionHandler.sendConnectionNotification();
                    }
                    b = true;
                    break;
                }
                case POWER_BUTTON_LONG_PRESS: {
                    Main.sLogger.v("power button click: show shutdown");
                    this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_SHUTDOWN_CONFIRMATION, Shutdown.Reason.POWER_BUTTON.asBundle(), false));
                    b = true;
                    break;
                }
                case POWER_BUTTON_DOUBLE_CLICK: {
                    Main.sLogger.v("power button double click: show dial pairing");
                    this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_DIAL_PAIRING).build());
                    b = true;
                    break;
                }
                case POWER_BUTTON_CLICK: {
                    Main.sLogger.v("power button click: no-op");
                    break;
                }
            }
            return b;
        }

        private void executeMultipleKeyEvent(final int n) {
            if (n == 2) {
                Main.sLogger.v("Double click");
                this.takeNotificationAction();
            }
            else if (n == 3) {
                Main.sLogger.v("Triple click");
                if (TRIPLE_CLICK_BRIGHTNESS) {
                    Main.showBrightnessNotification();
                } else {
                    final HomeScreenView homescreenView = this.uiStateManager.getHomescreenView();
                    if (homescreenView.getDisplayMode() == HomeScreen.DisplayMode.MAP) {
                        homescreenView.setDisplayMode(HomeScreen.DisplayMode.SMART_DASH);
                    }
                    else {
                        homescreenView.setDisplayMode(HomeScreen.DisplayMode.MAP);
                    }
                }
            }
            else if (n >= 4) {
                Main.sLogger.d("4 clicks, dumping a snapshot");
                ReportIssueService.dumpSnapshotAsync();
                if (!DeviceUtil.isUserBuild()) {
                    this.handler.postDelayed((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            ReportIssueService.showSnapshotMenu();
                        }
                    }, 2000L);
                }
            }
        }

        private void hideNotification() {
            final MainView mainView = this.getView();
            if (mainView != null && this.notificationManager.getCurrentNotification() != null) {
                Main.sLogger.v("NotificationManager:calling shownotification");
                mainView.dismissNotification();
            }
        }

        private boolean isPortrait() {
            final MainView mainView = this.getView();
            if (mainView != null) {
                this.showingPortrait = ScreenOrientation.isPortrait((Activity)mainView.getContext());
            }
            return this.showingPortrait;
        }

        private void launchVoiceAssistance(final boolean b) {
            final ToastManager instance = ToastManager.getInstance();
            if (instance.isToastDisplayed() && TextUtils.equals((CharSequence)instance.getCurrentToastId(), (CharSequence)"incomingcall#toast")) {
                Main.sLogger.v("startVoiceAssistance:cannot launch, phone toast");
            }
            else {
                instance.disableToasts(true);
                Main.sLogger.v("startVoiceAssistance: disable toast, current notif:" + this.notificationManager.getCurrentNotification());
                final NotificationManager notificationManager = this.notificationManager;
                INotification notification;
                if (b) {
                    notification = this.voiceSearchNotification;
                }
                else {
                    notification = this.voiceAssistNotification;
                }
                notificationManager.addNotification(notification);
            }
        }

        private void launchVoiceSearch() {
            final NotificationManager instance = NotificationManager.getInstance();
            VoiceSearchNotification voiceSearchNotification;
            if ((voiceSearchNotification = (VoiceSearchNotification)instance.getNotification("navdy#voicesearch#notif")) == null) {
                voiceSearchNotification = new VoiceSearchNotification();
            }
            instance.addNotification(voiceSearchNotification);
        }

        private static Blueprint lookupScreen(Screen screen_HOME) {
            try {
                switch (screen_HOME) {
                    case SCREEN_HYBRID_MAP:
                    case SCREEN_DASHBOARD: {
                        screen_HOME = Screen.SCREEN_HOME;
                        break;
                    }
                }
                final Class clz = (Class) Main.SCREEN_MAP.get(screen_HOME.ordinal());
                if (clz != null) {
                    return (Blueprint) clz.newInstance();
                }
            }
            catch (InstantiationException ex) {
                Main.sLogger.e("Failed to instantiate screen", ex);
            }
            catch (IllegalAccessException ex2) {
                Main.sLogger.e("Failed to instantiate screen", ex2);
            }
            return null;
        }

        private void persistSettings() {
            Main.sLogger.d("Starting to persist settings");
            final String json = new Gson().toJson(this.settings);
            this.preferences.edit().putString("main.settings", json).apply();
            Main.sLogger.d("Persisted settings:" + json);
        }

        private void runQueuedOperation() {
            final int size = this.operationQueue.size();
            if (size > 0) {
                final ScreenInfo screenInfo = this.operationQueue.remove();
                Main.sLogger.v("runQueuedOperation size:" + size + " posting:" + screenInfo.screen);
                this.handler.post((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        Presenter.this.updateScreen(screenInfo.screen, screenInfo.args, screenInfo.args2, true, screenInfo.dismissScreen);
                    }
                });
            }
            else {
                this.setInputFocus();
                this.animationRunning = false;
                Main.sLogger.v("animationRunning:false");
            }
        }

        private void setDisplayFormat(final DisplayFormat displayFormat) {
            Main.sLogger.i("Switching display format to " + displayFormat);
            final MainView mainView = this.getView();
            if (mainView != null) {
                mainView.setDisplayFormat(displayFormat);
            }
        }

        private void setupFlow() {
            final Backstack single = Backstack.single(new FirstLaunchScreen());
            this.currentScreen = Screen.SCREEN_FIRST_LAUNCH;
            this.uiStateManager.setCurrentViewMode(this.currentScreen);
            (this.flow = new Flow(single, (Flow.Listener)this)).resetTo(this.flow.getBackstack().current().getScreen());
        }

        private void signalBootComplete() {
            SystemProperties.set("dev.app_started", "1");
            SystemProperties.set("dev.late_service", "1");
        }

        private void startLateServices() {
            Main.sLogger.v("starting update service");
            final Context appContext = HudApplication.getAppContext();
            final Intent intent = new Intent(appContext, (Class)OTAUpdateService.class);
            intent.putExtra("COMMAND", "VERIFY_UPDATE");
            appContext.startService(intent);
            ReportIssueService.scheduleSync();
            Main.sLogger.v("initializing additional services");
            this.services = new DeferredServices(appContext);
            if (RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(Feature.GESTURE_ENGINE)) {
                Main.sLogger.v("gesture is supported");
                final InputPreferences inputPreferences = DriverProfileHelper.getInstance().getCurrentProfile().getInputPreferences();
                if (inputPreferences != null && inputPreferences.use_gestures) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            Main.sLogger.v("gesture is enabled, starting");
                            Presenter.this.services.gestureService.start();
                        }
                    }, 1);
                }
                else {
                    Main.sLogger.v("gesture is not enabled");
                }
            }
        }

        private void takeNotificationAction() {
            final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
            if (instance.isRemoteDeviceConnected() && instance.isAppConnected()) {
                if (this.notificationManager.isNotificationPresent("navdy#phone#call#notif")) {
                    Main.sLogger.v("show phone call");
                    this.notificationManager.expandNotification();
                }
                else {
                    Main.sLogger.v("show music");
                    this.musicManager.showMusicNotification();
                }
            }
            else {
                this.connectionHandler.sendConnectionNotification();
            }
        }

        private void updateDisplayFormat() {
            this.setDisplayFormat(this.driverProfileManager.getCurrentProfile().getDisplayFormat());
        }

        private void updateScreen(Screen screen, final Bundle bundle, final Object o, final boolean b, final boolean b2) {
            Main.sLogger.v("updateScreen screen[" + screen + "] animRunning[" + this.animationRunning + "] ignoreAnim [" + b + "] dismiss[" + b2 + "]");
            if (!b && this.animationRunning) {
                this.operationQueue.add(new ScreenInfo(screen, bundle, o, b2));
            }
            else {
                this.animationRunning = true;
                Main.sLogger.v("animationRunning:true");
                int n = 0;
                switch (screen) {
                    case SCREEN_MUSIC: {
                        if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                            this.musicManager.showMusicNotification();
                        }
                        else {
                            this.connectionHandler.sendConnectionNotification();
                        }
                        n = 1;
                        break;
                    }
                    case SCREEN_VOICE_CONTROL: {
                        if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                            this.startVoiceAssistance(bundle != null && bundle.getBoolean("extra_voice_search_notification", false));
                        }
                        else {
                            this.connectionHandler.sendConnectionNotification();
                        }
                        n = 1;
                        break;
                    }
                    case SCREEN_BRIGHTNESS: {
                        showBrightnessNotification();
                        n = 1;
                        break;
                    }
                }
                if (n != 0) {
                    Main.sLogger.v("notif screen");
                    this.runQueuedOperation();
                }
                else {
                    Screen currentViewMode;
                    if ((currentViewMode = screen) == Screen.SCREEN_BACK) {
                        currentViewMode = this.uiStateManager.getCurrentViewMode();
                    }
                    if (UIStateManager.isSidePanelMode(currentViewMode)) {
                        if (UIStateManager.isOverlayMode(this.currentScreen)) {
                            this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_BACK, null, true));
                            this.bus.post(new ShowScreenWithArgs(currentViewMode, bundle, o, true));
                        }
                        else {
                            switch (currentViewMode) {
                                case SCREEN_NOTIFICATION: {
                                    if (!b2) {
                                        this.showNotification();
                                        break;
                                    }
                                    final MainView mainView = this.getView();
                                    if (mainView != null && mainView.isNotificationViewShowing() && !this.notifAnimationRunning) {
                                        Main.sLogger.v("NotificationManager:calling dismissNotification");
                                        mainView.dismissNotification();
                                        break;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    else {
                        this.clearViews();
                        final Blueprint lookupScreen = lookupScreen(currentViewMode);
                        if (lookupScreen instanceof BaseScreen) {
                            ((BaseScreen)lookupScreen).setArguments(bundle, o);
                        }
                        screen = currentViewMode;
                        if (lookupScreen instanceof HomeScreen) {
                            switch (currentViewMode) {
                                default: {
                                    screen = currentViewMode;
                                    break;
                                }
                                case SCREEN_HYBRID_MAP: {
                                    screen = Screen.SCREEN_HOME;
                                    Main.saveHomeScreenPreference(this.globalPreferences, HomeScreen.DisplayMode.MAP.ordinal());
                                    ((HomeScreen)lookupScreen).setDisplayMode(HomeScreen.DisplayMode.MAP);
                                    break;
                                }
                                case SCREEN_DASHBOARD: {
                                    Main.saveHomeScreenPreference(this.globalPreferences, HomeScreen.DisplayMode.SMART_DASH.ordinal());
                                    ((HomeScreen)lookupScreen).setDisplayMode(HomeScreen.DisplayMode.SMART_DASH);
                                    screen = Screen.SCREEN_HOME;
                                    break;
                                }
                                case SCREEN_HOME: {
                                    final HomeScreen.DisplayMode displayMode = ((HomeScreen)lookupScreen).getDisplayMode();
                                    int n2 = displayMode.ordinal();
                                    final boolean defaultProfile = DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile();
                                    SharedPreferences sharedPreferences = DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
                                    if (defaultProfile || sharedPreferences == null) {
                                        sharedPreferences = this.globalPreferences;
                                    }
                                    if (sharedPreferences != null) {
                                        n2 = sharedPreferences.getInt("PREFERENCE_LAST_HOME_SCREEN_MODE", displayMode.ordinal());
                                    }
                                    screen = currentViewMode;
                                    if (n2 != displayMode.ordinal()) {
                                        if (n2 == HomeScreen.DisplayMode.MAP.ordinal()) {
                                            ((HomeScreen)lookupScreen).setDisplayMode(HomeScreen.DisplayMode.MAP);
                                        }
                                        else if (n2 == HomeScreen.DisplayMode.SMART_DASH.ordinal()) {
                                            ((HomeScreen)lookupScreen).setDisplayMode(HomeScreen.DisplayMode.SMART_DASH);
                                        }
                                        Main.saveHomeScreenPreference(this.globalPreferences, n2);
                                        screen = currentViewMode;
                                        break;
                                    }
                                    break;
                                }
                            }
                        }
                        if (UIStateManager.isFullscreenMode(screen)) {
                            this.uiStateManager.setCurrentViewMode(screen);
                        }
                        if (lookupScreen != null) {
                            this.currentScreen = screen;
                            this.flow.replaceTo(lookupScreen);
                        }
                    }
                }
            }
        }

        private void updateSystemTrayVisibility(final ScreenCategory screenCategory, Screen screen) {
            switch (screenCategory) {
                case NOTIFICATION: {
                    final BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
                    if (currentScreen != null) {
                        screen = currentScreen.getScreen();
                    }
                    final UIStateManager uiStateManager = this.uiStateManager;
                    if (UIStateManager.isFullscreenMode(screen)) {
                        this.setNotificationColorVisibility(0);
                        this.setSystemTrayVisibility(0);
                        break;
                    }
                    break;
                }
                case SCREEN: {
                    if (!this.notifAnimationRunning) {
                        this.runQueuedOperation();
                    }
                    if (this.notifAnimationRunning || this.isNotificationViewShowing()) {
                        break;
                    }
                    final UIStateManager uiStateManager2 = this.uiStateManager;
                    if (UIStateManager.isFullscreenMode(screen)) {
                        this.setNotificationColorVisibility(0);
                        this.setSystemTrayVisibility(0);
                        break;
                    }
                    break;
                }
            }
        }

        private boolean verifyProtocolVersion(final DeviceInfo deviceInfo) {
            while (true) {
                boolean b = false;
                int int1 = -1;
                while (true) {
                    try {
                        int1 = Integer.parseInt(deviceInfo.protocolVersion.split("\\.", 2)[0]);
                        if (Version.PROTOCOL_VERSION.majorVersion == int1 || (Version.PROTOCOL_VERSION.majorVersion == 1 && int1 == 0)) {
                            Main.mProtocolStatus = ProtocolStatus.PROTOCOL_VALID;
                            b = true;
                            return b;
                        }
                    }
                    catch (NumberFormatException ex) {
                        Main.sLogger.e("invalid remote device protocol version: " + deviceInfo.protocolVersion);
                        continue;
                    }
                    break;
                }
                if (Version.PROTOCOL_VERSION.majorVersion < int1) {
                    Main.mProtocolStatus = ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE;
                }
                else {
                    Main.mProtocolStatus = ProtocolStatus.PROTOCOL_REMOTE_NEEDS_UPDATE;
                }
                Main.sLogger.e(String.format("protocol version mismatch remote = %s, local = %s", deviceInfo.protocolVersion, Version.PROTOCOL_VERSION.toString()));
                return b;
            }
        }

        public void addNotificationExtensionView(final View view) {
            final MainView mainView = this.getView();
            if (mainView != null) {
                mainView.addNotificationExtensionView(view);
            }
        }

        public void clearInputFocus() {
            if (!(this.inputManager.getFocus() instanceof ToastView)) {
                this.inputManager.setFocus(null);
                Main.sLogger.v("[focus] cleared");
            }
        }

        public void createScreens() {
            if (!this.createdScreens) {
                final MainView mainView = this.getView();
                if (mainView != null) {
                    Main.sLogger.v("createScreens");
                    final ContainerView containerView = mainView.getContainerView();
                    this.createdScreens = true;
                    containerView.createScreens();
                }
            }
        }

        public void ejectMainLowerView() {
            final MainView mainView = this.getView();
            if (mainView != null) {
                mainView.ejectMainLowerView();
            }
        }

        void enableNotificationColor(final boolean notificationColorEnabled) {
            this.notificationColorEnabled = notificationColorEnabled;
            final MainView mainView = this.getView();
            if (mainView != null) {
                if (!this.notificationColorEnabled) {
                    mainView.setNotificationColorVisibility(4);
                    Main.sLogger.v("notif invisible");
                }
                else {
                    final BaseScreen currentScreen = this.uiStateManager.getCurrentScreen();
                    if (currentScreen != null) {
                        final UIStateManager uiStateManager = this.uiStateManager;
                        if (UIStateManager.isFullscreenMode(currentScreen.getScreen())) {
                            this.setNotificationColorVisibility(0);
                            Main.sLogger.v("notif visible");
                            return;
                        }
                    }
                    mainView.setNotificationColorVisibility(4);
                    Main.sLogger.v("notif invisible");
                }
            }
        }

        void enableSystemTray(final boolean systemTrayEnabled) {
            this.systemTrayEnabled = systemTrayEnabled;
            final MainView mainView = this.getView();
            if (mainView != null) {
                final SystemTrayView systemTray = mainView.getSystemTray();
                if (!this.systemTrayEnabled) {
                    systemTray.setVisibility(INVISIBLE);
                }
                else {
                    this.setSystemTrayVisibility(0);
                }
            }
        }

        public ConnectionHandler getConnectionHandler() {
            return this.connectionHandler;
        }

        View getExpandedNotificationCoverView() {
            final MainView mainView = this.getView();
            View expandedNotificationCoverView;
            if (mainView != null) {
                expandedNotificationCoverView = mainView.getExpandedNotificationCoverView();
            }
            else {
                expandedNotificationCoverView = null;
            }
            return expandedNotificationCoverView;
        }

        FrameLayout getExpandedNotificationView() {
            final MainView mainView = this.getView();
            FrameLayout expandedNotificationView;
            if (mainView != null) {
                expandedNotificationView = mainView.getExpandedNotificationView();
            }
            else {
                expandedNotificationView = null;
            }
            return expandedNotificationView;
        }

        ViewGroup getNotificationExtensionView() {
            final MainView mainView = this.getView();
            Object notificationExtensionView;
            if (mainView != null) {
                notificationExtensionView = mainView.getNotificationExtensionView();
            }
            else {
                notificationExtensionView = null;
            }
            return (ViewGroup)notificationExtensionView;
        }

        CarouselIndicator getNotificationIndicator() {
            final MainView mainView = this.getView();
            CarouselIndicator notificationIndicator;
            if (mainView != null) {
                notificationIndicator = mainView.getNotificationIndicator();
            }
            else {
                notificationIndicator = null;
            }
            return notificationIndicator;
        }

        ProgressIndicator getNotificationScrollIndicator() {
            final MainView mainView = this.getView();
            ProgressIndicator notificationScrollIndicator;
            if (mainView != null) {
                notificationScrollIndicator = mainView.getNotificationScrollIndicator();
            }
            else {
                notificationScrollIndicator = null;
            }
            return notificationScrollIndicator;
        }

        NotificationView getNotificationView() {
            final MainView mainView = this.getView();
            NotificationView notificationView;
            if (mainView != null) {
                notificationView = mainView.getNotificationView();
            }
            else {
                notificationView = null;
            }
            return notificationView;
        }

        public MainScreenSettings.ScreenConfiguration getScreenConfiguration() {
            MainScreenSettings.ScreenConfiguration screenConfiguration;
            if (this.isPortrait()) {
                screenConfiguration = this.settings.getPortraitConfiguration();
            }
            else {
                screenConfiguration = this.settings.getLandscapeConfiguration();
            }
            return screenConfiguration;
        }

        SystemTrayView getSystemTray() {
            final MainView mainView = this.getView();
            SystemTrayView systemTray;
            if (mainView == null) {
                systemTray = null;
            }
            else {
                systemTray = mainView.getSystemTray();
            }
            return systemTray;
        }

        @Override
        public void go(final Backstack backstack, final Direction direction, final Callback callback) {
            final MainView mainView = this.getView();
            if (mainView != null) {
                final ContainerView containerView = mainView.getContainerView();
                final BaseScreen baseScreen = (BaseScreen)backstack.current().getScreen();
                int animationOut = -1;
                final Backstack backstack2 = this.flow.getBackstack();
                if (backstack2.size() > 0) {
                    animationOut = ((BaseScreen)backstack2.current().getScreen()).getAnimationOut(Direction.FORWARD);
                }
                containerView.showScreen(baseScreen, Direction.FORWARD, baseScreen.getAnimationIn(Direction.FORWARD), animationOut);
            }
            callback.onComplete();
        }

        public void injectMainLowerView(final View view) {
            final MainView mainView = this.getView();
            if (mainView != null) {
                mainView.injectMainLowerView(view);
            }
        }

        public boolean isMainLowerViewVisible() {
            final MainView mainView = this.getView();
            return mainView != null && mainView.isMainLowerViewVisible();
        }

        boolean isMainUIShrunk() {
            final MainView mainView = this.getView();
            return mainView != null && mainView.isMainUIShrunk();
        }

        boolean isNotificationCollapsing() {
            final MainView mainView = this.getView();
            return mainView != null && mainView.isNotificationCollapsing();
        }

        boolean isNotificationExpanding() {
            final MainView mainView = this.getView();
            return mainView != null && mainView.isNotificationExpanding();
        }

        boolean isNotificationViewShowing() {
            final MainView mainView = this.getView();
            return mainView != null && mainView.isNotificationViewShowing();
        }

        boolean isScreenAnimating() {
            final MainView mainView = this.getView();
            return mainView != null && mainView.isScreenAnimating();
        }

        @Override
        public IInputHandler nextHandler() {
            return null;
        }

        @Subscribe
        public void onDeviceConnectionStateChange(final ConnectionStateChange connectionStateChange) {
            Main.sLogger.v("status:" + connectionStateChange.remoteDeviceId + " : " + connectionStateChange.state.name());
            final SystemTrayView systemTray = this.getSystemTray();
            switch (connectionStateChange.state) {
                case CONNECTION_VERIFIED: {
                    this.musicManager.initialize();
                    if (systemTray != null) {
                        systemTray.setState(SystemTrayView.Device.Phone, SystemTrayView.State.CONNECTED);
                        break;
                    }
                    break;
                }
                case CONNECTION_DISCONNECTED: {
                    if (systemTray != null) {
                        systemTray.setState(SystemTrayView.Device.Phone, SystemTrayView.State.DISCONNECTED);
                    }
                    if (Main.mProtocolStatus == ProtocolStatus.PROTOCOL_LOCAL_NEEDS_UPDATE && this.currentScreen == Screen.SCREEN_FORCE_UPDATE) {
                        this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_HOME).build());
                    }
                    Main.mProtocolStatus = ProtocolStatus.PROTOCOL_VALID;
                    break;
                }
            }
        }

        @Subscribe
        public void onDeviceInfo(final DeviceInfo deviceInfo) {
            if (this.verifyProtocolVersion(deviceInfo)) {
                final ConnectionStateChange.Builder builder = new ConnectionStateChange.Builder();
                builder.state(ConnectionStateChange.ConnectionState.CONNECTION_VERIFIED);
                builder.remoteDeviceId(deviceInfo.deviceId);
                this.bus.post(builder.build());
            }
            else {
                this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_FORCE_UPDATE).build());
            }
        }

        @Subscribe
        public void onDialBatteryEvent(final DialConstants.BatteryChangeEvent batteryChangeEvent) {
            final SystemTrayView systemTray = this.getSystemTray();
            if (systemTray != null) {
                final DialConstants.NotificationReason batteryNotificationReason = DialManager.getInstance().getBatteryNotificationReason();
                SystemTrayView.State state = null;
                if (batteryNotificationReason == null) {
                    state = SystemTrayView.State.OK_BATTERY;
                }
                else {
                    switch (batteryNotificationReason) {
                        case EXTREMELY_LOW_BATTERY: {
                            state = SystemTrayView.State.EXTREMELY_LOW_BATTERY;
                            break;
                        }
                        case LOW_BATTERY:
                        case VERY_LOW_BATTERY: {
                            state = SystemTrayView.State.LOW_BATTERY;
                            break;
                        }
                        case OK_BATTERY: {
                            state = SystemTrayView.State.OK_BATTERY;
                            break;
                        }
                    }
                }
                systemTray.setState(SystemTrayView.Device.Dial, state);
            }
        }

        @Subscribe
        public void onDialConnectionEvent(final DialConstants.DialConnectionStatus dialConnectionStatus) {
            final SystemTrayView systemTray = this.getSystemTray();
            if (systemTray != null) {
                SystemTrayView.State state;
                if (dialConnectionStatus.status == DialConstants.DialConnectionStatus.Status.CONNECTED) {
                    state = SystemTrayView.State.CONNECTED;
                }
                else {
                    state = SystemTrayView.State.DISCONNECTED;
                }
                systemTray.setState(SystemTrayView.Device.Dial, state);
            }
        }

        @Subscribe
        public void onDismissScreen(final DismissScreen dismissScreen) {
            if (dismissScreen.screen == Screen.SCREEN_NOTIFICATION) {
                this.updateScreen(Screen.SCREEN_NOTIFICATION, null, null, false, true);
            }
        }

        @Subscribe
        public void onDisplayScaleChange(final DisplayScaleChange displayScaleChange) {
            this.setDisplayFormat(displayScaleChange.format);
        }

        @Subscribe
        public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
            this.updateDisplayFormat();
        }

        @Subscribe
        public void onDriverProfileUpdate(final DriverProfileUpdated driverProfileUpdated) {
            if (driverProfileUpdated.state == DriverProfileUpdated.State.UPDATED) {
                this.updateDisplayFormat();
            }
        }

        @Override
        protected void onExitScope() {
            super.onExitScope();
            Main.sLogger.i("Shutting down main service");
            this.connectionHandler.disconnect();
        }

        @Override
        public boolean onGesture(final GestureEvent gestureEvent) {
            boolean b;
            if (gestureEvent != null && gestureEvent.gesture != null) {
                switch (gestureEvent.gesture) {
                    case GESTURE_SWIPE_LEFT: {
                        final NotificationManager instance = NotificationManager.getInstance();
                        if (instance.getNotificationCount() > 0) {
                            AnalyticsSupport.setGlanceNavigationSource("swipe");
                        }
                        instance.expandNotification();
                        break;
                    }
                }
                b = true;
            }
            else {
                b = false;
            }
            return b;
        }

        @Subscribe
        public void onGpsSatelliteData(final GpsUtils.GpsSatelliteData gpsSatelliteData) {
            if (GpsUtils.isDebugRawGpsPosEnabled()) {
                final int int1 = gpsSatelliteData.data.getInt("SAT_FIX_TYPE", 0);
                final SystemTrayView systemTray = this.getSystemTray();
                if (systemTray != null) {
                    String debugGpsMarker = null;
                    Label_0075: {
                        switch (int1) {
                            default: {
                                debugGpsMarker = debugGpsMarker;
                                break Label_0075;
                            }
                            case 6: {
                                debugGpsMarker = "DR";
                                break Label_0075;
                            }
                            case 2: {
                                debugGpsMarker = "Diff";
                                break Label_0075;
                            }
                            case 1: {
                                debugGpsMarker = "2D";
                            }
                            case 3:
                            case 4:
                            case 5: {
                                systemTray.setDebugGpsMarker(debugGpsMarker);
                                break;
                            }
                        }
                    }
                }
            }
        }

        @Subscribe
        public void onGpsSwitchEvent(final GpsUtils.GpsSwitch gpsSwitch) {
            if (GpsUtils.isDebugRawGpsPosEnabled()) {
                Main.sLogger.v("gps_switch_event:" + gpsSwitch.usingPhone);
                final SystemTrayView systemTray = this.getSystemTray();
                if (systemTray != null) {
                    String debugGpsMarker;
                    if (gpsSwitch.usingPhone) {
                        debugGpsMarker = "P";
                    }
                    else {
                        debugGpsMarker = "U";
                    }
                    systemTray.setDebugGpsMarker(debugGpsMarker);
                }
            }
        }

        @Subscribe
        public void onInitEvent(final InitPhase initPhase) {
            switch (initPhase.phase) {
                case PRE_USER_INTERACTION: {
                    if (this.powerManager.inQuietMode()) {
                        this.powerManager.enterSleepMode();
                    }
                    else {
                        HUDLightUtils.resetFrontLED(LightManager.getInstance());
                    }
                    String value = SystemProperties.get("persist.sys.demo.video");
                    boolean b;
                    if (!TextUtils.isEmpty((CharSequence)value)) {
                        b = true;
                    }
                    else {
                        b = false;
                    }
                    if (!this.preferences.getBoolean("start_video_on_boot_preference", false) && !b) {
                        break;
                    }
                    final File externalStoragePublicDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
                    this.signalBootComplete();
                    if (!b) {
                        value = "navdy-demo.mov";
                    }
                    final File file = new File(externalStoragePublicDirectory, value);
                    Main.sLogger.d("Trying to play video: " + file);
                    if (file.exists()) {
                        this.playMedia(HudApplication.getAppContext(), Uri.fromFile(file));
                        break;
                    }
                    Main.sLogger.d("Missing demo video file...");
                    break;
                }
                case POST_START: {
                    Main.sLogger.v("initializing obd manager");
                    ObdManager.getInstance();
                    if (!this.powerManager.inQuietMode()) {
                        this.startLateServices();
                    }
                    this.signalBootComplete();
                    break;
                }
            }
        }

        @Subscribe
        public void onInstallingUpdate(final OTAUpdateService.InstallingUpdate installingUpdate) {
            this.showInstallingUpdateToast();
        }

        @Override
        public boolean onKey(final CustomKeyEvent customKeyEvent) {
            return this.innerEventsDetector.onKey(customKeyEvent);
        }

        public void onLoad(Bundle savedInstanceState) {
            Main.presenter = this;
            this.notificationManager = NotificationManager.getInstance();
            this.voiceAssistNotification = new VoiceAssistNotification();
            this.voiceSearchNotification = new VoiceSearchNotification();
            this.defaultNotifColor = 0;
            this.uiStateManager.setRootScreen(Main.mainScreen);
            this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
            this.uiStateManager.addNotificationAnimationListener(this.notificationAnimationListener);
            super.onLoad(savedInstanceState);
            MainScreenSettings savedSettings = null;
            if (savedInstanceState != null) {
                savedSettings = (MainScreenSettings) savedInstanceState.getParcelable(MAIN_SETTINGS);
            }
            if (savedSettings == null) {
                String json = this.preferences.getString(MAIN_SETTINGS, null);
                try {
                    savedSettings = (MainScreenSettings) new Gson().fromJson(json, MainScreenSettings.class);
                } catch (Throwable th) {
                    Main.sLogger.e("Failed to parse json " + json);
                }
            }
            if (savedSettings == null) {
                savedSettings = new MainScreenSettings();
            }
            this.settings = savedSettings;
            this.settingsManager.addSetting(GpsUtils.SHOW_RAW_GPS);
            this.hudSettings = new HUDSettings(this.preferences);
            updateDisplayFormat();
            this.bus.register(this);
            this.connectionHandler.connect();
            setupFlow();
            updateView();
            this.bus.register(this.dialSimulatorMessagesHandler);
            this.bus.register(this.musicManager);
            this.bus.register(this.pandoraManager);
            this.bus.register(this.tripManager);
            this.initManager = new InitManager(this.bus, this.connectionHandler);
            this.initManager.start();
        }

        @Subscribe
        public void onLocationFixChangeEvent(final LocationFix locationFix) {
            final SystemTrayView systemTray = this.getSystemTray();
            if (systemTray != null) {
                if (locationFix.locationAvailable) {
                    systemTray.setState(SystemTrayView.Device.Gps, SystemTrayView.State.LOCATION_AVAILABLE);
                    if (GpsUtils.isDebugRawGpsPosEnabled()) {
                        this.onGpsSwitchEvent(new GpsUtils.GpsSwitch(locationFix.usingPhoneLocation, locationFix.usingLocalGpsLocation));
                    }
                }
                else {
                    systemTray.setState(SystemTrayView.Device.Gps, SystemTrayView.State.LOCATION_LOST);
                }
            }
        }

        @Subscribe
        public void onNetworkStateChange(final NetworkStateChange networkStateChange) {
            final SystemTrayView systemTray = this.getSystemTray();
            if (systemTray != null) {
                if (Boolean.TRUE.equals(networkStateChange.networkAvailable)) {
                    systemTray.setState(SystemTrayView.Device.Phone, SystemTrayView.State.PHONE_NETWORK_AVAILABLE);
                }
                else {
                    systemTray.setState(SystemTrayView.Device.Phone, SystemTrayView.State.NO_PHONE_NETWORK);
                }
            }
        }

        @Subscribe
        public void onNotificationChange(final NotificationManager.NotificationChange notificationChange) {
            final MainView mainView = this.getView();
            if (mainView != null) {
                this.setNotificationColorVisibility(mainView.getNotificationColorVisibility());
            }
        }

        @Subscribe
        public void onPhoneBatteryStatusEvent(final PhoneBatteryStatus phoneBatteryStatus) {
            Main.sLogger.v("PhoneBatteryStatus status[" + phoneBatteryStatus.status + "] level[" + phoneBatteryStatus.status + "] charging[" + phoneBatteryStatus.charging + "]");
            final SystemTrayView systemTray = this.getSystemTray();
            if (systemTray != null) {
                if (Boolean.TRUE.equals(phoneBatteryStatus.charging)) {
                    systemTray.setState(SystemTrayView.Device.Phone, SystemTrayView.State.OK_BATTERY);
                }
                else {
                    switch (phoneBatteryStatus.status) {
                        case BATTERY_OK: {
                            systemTray.setState(SystemTrayView.Device.Phone, SystemTrayView.State.OK_BATTERY);
                            break;
                        }
                        case BATTERY_EXTREMELY_LOW: {
                            systemTray.setState(SystemTrayView.Device.Phone, SystemTrayView.State.EXTREMELY_LOW_BATTERY);
                            break;
                        }
                        case BATTERY_LOW: {
                            systemTray.setState(SystemTrayView.Device.Phone, SystemTrayView.State.LOW_BATTERY);
                            break;
                        }
                    }
                }
            }
        }

        @Subscribe
        public void onReadSettings(final ReadSettingsRequest readSettingsRequest) {
            final RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
            if (remoteDevice != null) {
                final MainScreenSettings.ScreenConfiguration screenConfiguration = this.getScreenConfiguration();
                final Rect margins = screenConfiguration.getMargins();
                remoteDevice.postEvent(new ReadSettingsResponse(new ScreenConfiguration(margins.left, margins.top, margins.right, margins.bottom, screenConfiguration.getScaleX(), screenConfiguration.getScaleY()), this.hudSettings.readSettings(readSettingsRequest.settings)));
            }
        }

        public void onSave(final Bundle bundle) {
            bundle.putParcelable("main.settings", (Parcelable)this.settings);
            this.persistSettings();
            if (this.services != null) {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        Presenter.this.services.gestureService.stop();
                    }
                }, 1);
            }
            super.onSave(bundle);
        }

        @Subscribe
        public void onShowScreen(final ShowScreenWithArgs showScreenWithArgs) {
            if (this.uiStateManager.getRootScreen() == null) {
                Main.sLogger.w("Main screen not up, cannot show screen-args:" + showScreenWithArgs);
            }
            else {
                this.updateScreen(showScreenWithArgs.screen, showScreenWithArgs.args, showScreenWithArgs.args2, showScreenWithArgs.ignoreAnimation, false);
            }
        }

        @Subscribe
        public void onShowScreen(final ShowScreen showScreen) {
            if (this.uiStateManager.getRootScreen() == null) {
                Main.sLogger.w("Main screen not up, cannot show screen:" + showScreen);
            }
            else if (showScreen.arguments == null || showScreen.arguments.size() == 0) {
                this.updateScreen(showScreen.screen, null, null, false, false);
            }
            else {
                final List<KeyValue> arguments = showScreen.arguments;
                final Bundle bundle = new Bundle();
                for (final KeyValue keyValue : arguments) {
                    if (keyValue.key != null && keyValue.value != null) {
                        bundle.putString(keyValue.key, keyValue.value);
                    }
                }
                this.updateScreen(showScreen.screen, bundle, null, false, false);
            }
        }

        @Subscribe
        public void onShutdownEvent(final Shutdown shutdown) {
            switch (shutdown.state) {
                case CONFIRMATION: {
                    this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_SHUTDOWN_CONFIRMATION, shutdown.reason.asBundle(), false));
                    break;
                }
                case SHUTTING_DOWN: {
                    final Context appContext = HudApplication.getAppContext();
                    Main.sLogger.i("Stopping update service");
                    appContext.stopService(new Intent(appContext, (Class)OTAUpdateService.class));
                    break;
                }
            }
        }

        @Subscribe
        public void onUpdateSettings(final UpdateSettings updateSettings) {
            final ScreenConfiguration screenConfiguration = updateSettings.screenConfiguration;
            if (screenConfiguration != null) {
                final MainScreenSettings.ScreenConfiguration screenConfiguration2 = this.getScreenConfiguration();
                screenConfiguration2.setMargins(new Rect((int)screenConfiguration.marginLeft, (int)screenConfiguration.marginTop, (int)screenConfiguration.marginRight, (int)screenConfiguration.marginBottom));
                screenConfiguration2.setScaleX(screenConfiguration.scaleX);
                screenConfiguration2.setScaleY(screenConfiguration.scaleY);
            }
            this.hudSettings.updateSettings(updateSettings.settings);
            this.persistSettings();
            this.updateView();
        }

        @Subscribe
        public void onWakeUp(final Wakeup wakeup) {
            this.startLateServices();
        }

        private void showInstallingUpdateToast() {
            ToastManager toastManager = ToastManager.getInstance();
            toastManager.clearAllPendingToast();
            toastManager.disableToasts(false);
            Resources resources = HudApplication.getAppContext().getResources();
            Bundle bundle = new Bundle();
            toastManager.dismissCurrentToast(Main.INSTALLING_OTA_TOAST_ID);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE, resources.getString(R.string.warning));
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_warning);
            bundle.putInt(ToastPresenter.EXTRA_SIDE_IMAGE, R.drawable.icon_sm_spinner);
            int powerWarning = ObdManager.getInstance().getConnectionType() == ConnectionType.POWER_ONLY ? R.string.do_not_remove_power_or_turn_off_vehicle : R.string.do_not_remove_power;
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_1, resources.getString(R.string.preparing_to_install_update));
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE, R.style.installing_update_title_1);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(powerWarning));
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.installing_update_title_2);
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, (int) resources.getDimension(R.dimen.toast_installing_update_width));
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_LEFT_PADDING, (int) resources.getDimension(R.dimen.toast_installing_update_padding));
            toastManager.addToast(new ToastParams(Main.INSTALLING_OTA_TOAST_ID, bundle, new IToastCallback() {
                ObjectAnimator animator = this.animator;

                public void onStart(ToastView view) {
                    ConfirmationLayout lyt = view.getView();
                    lyt.title1.setMaxLines(2);
                    lyt.title1.setSingleLine(false);
                    lyt.title2.setMaxLines(2);
                    lyt.title2.setSingleLine(false);
                    if (animator == null) {
                        animator = ObjectAnimator.ofFloat(lyt.sideImage, View.ROTATION, new float[]{360.0f});
                        animator.setDuration(500);
                        animator.setInterpolator(new AccelerateDecelerateInterpolator());
                        animator.addListener(new DefaultAnimationListener() {
                            public void onAnimationEnd(Animator animation) {
                                if (animator != null) {
                                    animator.setStartDelay(33);
                                    animator.start();
                                }
                            }
                        });
                    }
                    if (!animator.isRunning()) {
                        animator.start();
                    }
                }

                public void onStop() {
                    if (animator != null) {
                        animator.removeAllListeners();
                        animator.cancel();
                    }
                }

                public boolean onKey(CustomKeyEvent event) {
                    return false;
                }

                public void executeChoiceItem(int pos, int id) {
                }
            }, true, true));
        }

        private void showNotification() {
            final MainView mainView = this.getView();
            if (mainView != null) {
                final INotification currentNotification = this.notificationManager.getCurrentNotification();
                if (currentNotification == null) {
                    Main.sLogger.v("no current notification to be displayed");
                    this.runQueuedOperation();
                }
                else {
                    Main.sLogger.v("NotificationManager:calling shownotification");
                    final HomeScreenView homescreenView = this.uiStateManager.getHomescreenView();
                    if (homescreenView.hasModeView() && homescreenView.isModeVisible()) {
                        Main.sLogger.v("mode-view: reset mode view show notif");
                        homescreenView.resetModeView();
                    }
                    mainView.showNotification(currentNotification.getId(), currentNotification.getType());
                }
            }
        }



        public void playMedia(final Context context, final Uri uri) {
            Main.sLogger.d("playMedia:" + uri);
            try {
                final Intent intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(uri, "video/mp4");
                intent.addFlags(268435456);
                context.startActivity(intent);
            }
            catch (Exception ex) {
                Main.sLogger.e("Unable to start video loop", ex);
            }
        }

        public void removeNotificationExtensionView() {
            final MainView mainView = this.getView();
            if (mainView != null) {
                mainView.removeNotificationExtensionView();
            }
        }

        public void setInputFocus() {
            if (this.inputManager.getFocus() instanceof ToastView) {
                Main.sLogger.v("[focus] not setting focus[toast]");
            }
            else {
                final MainView mainView = this.getView();
                if (mainView == null) {
                    Main.sLogger.v("[focus] mainview is null");
                }
                else if (this.isNotificationViewShowing()) {
                    this.inputManager.setFocus((InputManager.IInputHandler)mainView.getNotificationView());
                    Main.sLogger.v("[focus] notification");
                }
                else {
                    final ContainerView containerView = mainView.getContainerView();
                    if (containerView == null) {
                        this.inputManager.setFocus((InputManager.IInputHandler)mainView);
                        Main.sLogger.v("[focus] mainView");
                    }
                    else {
                        final View currentView = containerView.getCurrentView();
                        if (currentView instanceof IInputHandler) {
                            this.inputManager.setFocus((IInputHandler)currentView);
                            Main.sLogger.v("[focus] screen:" + ((IInputHandler)currentView).getClass().getSimpleName());
                        }
                        else {
                            this.inputManager.setFocus((InputManager.IInputHandler)mainView);
                            Main.sLogger.v("[focus] mainView");
                        }
                    }
                }
            }
        }

        public void setMargins(final int n, final int n2, final int n3, final int n4) {
            this.getScreenConfiguration().setMargins(new Rect(0, n2, 0, 0));
            this.persistSettings();
            this.updateView();
        }

        void setNotificationColorVisibility(final int n) {
            if (!this.notificationColorEnabled) {
                Main.sLogger.v("notifColor is disabled");
            }
            else {
                final MainView mainView = this.getView();
                if (mainView != null) {
                    if (n == 0) {
                        if (this.notificationManager.getCurrentNotification() != null && (this.isNotificationExpanding() || this.isNotificationViewShowing())) {
                            Main.sLogger.v("not showing notifcolor, pending notif");
                            return;
                        }
                        int notificationColor;
                        if ((notificationColor = this.notificationManager.getNotificationColor()) == -1) {
                            notificationColor = this.defaultNotifColor;
                        }
                        mainView.setNotificationColor(notificationColor);
                        mainView.setNotificationColorVisibility(n);
                    }
                    else {
                        mainView.setNotificationColor(this.defaultNotifColor);
                        mainView.setNotificationColorVisibility(n);
                    }
                    final Logger access$200 = Main.sLogger;
                    final StringBuilder append = new StringBuilder().append("notifColor: ");
                    String s;
                    if (n == 0) {
                        s = "visible";
                    }
                    else {
                        s = "invisible";
                    }
                    access$200.v(append.append(s).toString());
                }
            }
        }

        public void setSystemTrayVisibility(final int visibility) {
            if (!this.systemTrayEnabled) {
                Main.sLogger.v("systemtray is disabled");
            }
            else {
                final MainView mainView = this.getView();
                if (mainView != null) {
                    if (visibility == 0 && this.notificationManager.getCurrentNotification() != null && (this.isNotificationExpanding() || this.isNotificationViewShowing())) {
                        Main.sLogger.v("not showing systemtray, pending notif");
                    }
                    else {
                        mainView.getSystemTray().setVisibility(visibility);
                        final Logger access$200 = Main.sLogger;
                        final StringBuilder append = new StringBuilder().append("systemtray is ");
                        String s;
                        if (visibility == 0) {
                            s = "visible";
                        }
                        else {
                            s = "invisible";
                        }
                        access$200.v(append.append(s).toString());
                    }
                }
            }
        }

        public void startVoiceAssistance(final boolean b) {
            Main.sLogger.v("startVoiceAssistance , Voice search : " + b);
            if (this.notificationManager.isNotificationViewShowing()) {
                Main.sLogger.v("startVoiceAssistance:add pending");
                final NotificationManager notificationManager = this.notificationManager;
                INotification notification;
                if (b) {
                    notification = this.voiceSearchNotification;
                }
                else {
                    notification = this.voiceAssistNotification;
                }
                notificationManager.addPendingNotification(notification);
                if (this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible()) {
                    this.notificationManager.collapseExpandedNotification(true, true);
                }
                else {
                    this.notificationManager.collapseNotification();
                }
            }
            else {
                this.launchVoiceAssistance(b);
            }
        }

        public void updateView() {
            final MainView mainView = this.getView();
            if (mainView != null) {
                mainView.setVerticalOffset(this.getScreenConfiguration().getMargins().top);
                this.setNotificationColorVisibility(0);
                this.setSystemTrayVisibility(0);
                final SystemTrayView systemTray = this.getSystemTray();
                if (systemTray != null) {
                    final SystemTrayView.Device dial = SystemTrayView.Device.Dial;
                    SystemTrayView.State state;
                    if (DialManager.getInstance().isDialConnected()) {
                        state = SystemTrayView.State.CONNECTED;
                    }
                    else {
                        state = SystemTrayView.State.DISCONNECTED;
                    }
                    systemTray.setState(dial, state);
                    final SystemTrayView.Device phone = SystemTrayView.Device.Phone;
                    SystemTrayView.State state2;
                    if (RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                        state2 = SystemTrayView.State.CONNECTED;
                    }
                    else {
                        state2 = SystemTrayView.State.DISCONNECTED;
                    }
                    systemTray.setState(phone, state2);
                    final HereMapsManager instance = HereMapsManager.getInstance();
                    if (instance.isInitialized()) {
                        final HereLocationFixManager locationFixManager = instance.getLocationFixManager();
                        if (locationFixManager != null && !locationFixManager.hasLocationFix()) {
                            systemTray.setState(SystemTrayView.Device.Gps, SystemTrayView.State.LOCATION_LOST);
                        }
                    }
                }
            }
        }

        public static class DeferredServices
        {
            @Inject
            public AncsServiceConnector ancsService;
            @Inject
            public GestureServiceConnector gestureService;

            DeferredServices(final Context context) {
                Mortar.inject(context, this);
            }
        }
    }

    public enum ProtocolStatus
    {
        PROTOCOL_LOCAL_NEEDS_UPDATE,
        PROTOCOL_REMOTE_NEEDS_UPDATE,
        PROTOCOL_VALID;
    }

    private enum ScreenCategory
    {
        NOTIFICATION,
        SCREEN;
    }

    private static class ScreenInfo
    {
        Bundle args;
        Object args2;
        boolean dismissScreen;
        Screen screen;

        ScreenInfo(final Screen screen, final Bundle args, final Object args2, final boolean dismissScreen) {
            this.screen = screen;
            this.args = args;
            this.args2 = args2;
            this.dismissScreen = dismissScreen;
        }
    }
}
