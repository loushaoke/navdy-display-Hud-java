package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.screen.WelcomeScreen;
import android.os.Bundle;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.util.OTAUpdateService;
import java.util.ArrayList;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import java.util.List;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.vlist.VerticalList;

public class SettingsMenu implements IMenu
{
    private static final VerticalList.Model back;
    private static final VerticalList.Model brightness;
    private static final VerticalList.Model connectPhone;
    private static final VerticalList.Model dialFwUpdate;
    private static final VerticalList.Model displayFwUpdate;
    private static final VerticalList.Model factoryReset;
    private static final VerticalList.Model learningGestures;
    private static final Resources resources;
    private static final Logger sLogger;
    private static final String setting;
    private static final int settingColor;
    private static final VerticalList.Model shutdown;
    private static final VerticalList.Model systemInfo;
    private int backSelection;
    private int backSelectionId;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    private IMenu parent;
    private MainMenuScreen2.Presenter presenter;
    public SystemInfoMenu systemInfoMenu;
    private VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new Logger(SettingsMenu.class);
        resources = HudApplication.getAppContext().getResources();
        settingColor = SettingsMenu.resources.getColor(R.color.mm_settings);
        setting = SettingsMenu.resources.getString(R.string.carousel_menu_settings_title);
        final String string = SettingsMenu.resources.getString(R.string.back);
        final int color = SettingsMenu.resources.getColor(R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, color, MainMenu.bkColorUnselected, color, string, null);
        final String string2 = SettingsMenu.resources.getString(R.string.carousel_settings_brightness);
        final int color2 = SettingsMenu.resources.getColor(R.color.mm_settings_brightness);
        brightness = IconBkColorViewHolder.buildModel(R.id.settings_menu_brightness, R.drawable.icon_settings_brightness_2, color2, MainMenu.bkColorUnselected, color2, string2, null);
        final String string3 = SettingsMenu.resources.getString(R.string.carousel_settings_connect_phone_title);
        final int color3 = SettingsMenu.resources.getColor(R.color.mm_connnect_phone);
        connectPhone = IconBkColorViewHolder.buildModel(R.id.settings_menu_connect_phone, R.drawable.icon_settings_connect_phone_2, color3, MainMenu.bkColorUnselected, color3, string3, null);
        final String string4 = SettingsMenu.resources.getString(R.string.carousel_settings_learning_gesture_title);
        final int color4 = SettingsMenu.resources.getColor(R.color.mm_settings_learn_gestures);
        learningGestures = IconBkColorViewHolder.buildModel(R.id.settings_menu_learning_gestures, R.drawable.icon_settings_learn_gestures_2, color4, MainMenu.bkColorUnselected, color4, string4, null);
        final String string5 = SettingsMenu.resources.getString(R.string.carousel_settings_software_update_title);
        final int color5 = SettingsMenu.resources.getColor(R.color.mm_settings_update_display);
        displayFwUpdate = IconBkColorViewHolder.buildModel(R.id.settings_menu_software_update, R.drawable.icon_software_update_2, color5, MainMenu.bkColorUnselected, color5, string5, null);
        final String string6 = SettingsMenu.resources.getString(R.string.carousel_settings_dial_update_title);
        final int color6 = SettingsMenu.resources.getColor(R.color.mm_settings_update_dial);
        dialFwUpdate = IconBkColorViewHolder.buildModel(R.id.settings_menu_dial_update, R.drawable.icon_dial_update_2, color6, MainMenu.bkColorUnselected, color6, string6, null);
        final String string7 = SettingsMenu.resources.getString(R.string.carousel_settings_shutdown_title);
        final int color7 = SettingsMenu.resources.getColor(R.color.mm_settings_shutdown);
        shutdown = IconBkColorViewHolder.buildModel(R.id.settings_menu_shutdown, R.drawable.icon_settings_shutdown_2, color7, MainMenu.bkColorUnselected, color7, string7, null);
        systemInfo = IconsTwoViewHolder.buildModel(R.id.main_menu_system_info, R.drawable.icon_settings_navdy_data, R.drawable.icon_settings_navdy_data_sm, SettingsMenu.resources.getColor(R.color.mm_settings_sys_info), -1, SettingsMenu.resources.getString(R.string.carousel_menu_system_info_title), null);
        final String string8 = SettingsMenu.resources.getString(R.string.carousel_settings_factory_reset_title);
        final int color8 = SettingsMenu.resources.getColor(R.color.mm_settings_factory_reset);
        factoryReset = IconBkColorViewHolder.buildModel(R.id.settings_menu_factory_reset, R.drawable.icon_settings_factory_reset_2, color8, MainMenu.bkColorUnselected, color8, string8, null);
    }
    
    public SettingsMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        return 1;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        List<VerticalList.Model> cachedList;
        if (this.cachedList != null) {
            cachedList = this.cachedList;
        }
        else {
            cachedList = new ArrayList<VerticalList.Model>();
            cachedList.add(SettingsMenu.back);
            cachedList.add(SettingsMenu.brightness);
            cachedList.add(SettingsMenu.connectPhone);
            cachedList.add(SettingsMenu.learningGestures);
            if (OTAUpdateService.isUpdateAvailable()) {
                cachedList.add(SettingsMenu.displayFwUpdate);
            }
            final DialManager instance = DialManager.getInstance();
            if (instance.isDialConnected() && instance.getDialFirmwareUpdater().getVersions().isUpdateAvailable()) {
                cachedList.add(SettingsMenu.dialFwUpdate);
            }
            cachedList.add(SettingsMenu.shutdown);
            cachedList.add(SettingsMenu.factoryReset);
            cachedList.add(SettingsMenu.systemInfo);
            this.cachedList = cachedList;
        }
        return cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.SETTINGS;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        SettingsMenu.sLogger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            case R.id.menu_back:
                SettingsMenu.sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            case R.id.settings_menu_brightness:
                SettingsMenu.sLogger.v("brightness");
                AnalyticsSupport.recordMenuSelection("brightness");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_BRIGHTNESS));
                break;
            case R.id.settings_menu_auto_brightness:
                SettingsMenu.sLogger.v("auto brightness");
                AnalyticsSupport.recordMenuSelection("auto_brightness");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_AUTO_BRIGHTNESS));
                break;
            case R.id.settings_menu_connect_phone: {
                SettingsMenu.sLogger.v("connect phone");
                AnalyticsSupport.recordMenuSelection("connect_phone");
                final Bundle bundle = new Bundle();
                bundle.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_SWITCH_PHONE);
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_WELCOME, bundle, false));
                break;
            }
            case R.id.settings_menu_software_update: {
                SettingsMenu.sLogger.v("software update");
                AnalyticsSupport.recordMenuSelection("software_update");
                final Bundle bundle2 = new Bundle();
                bundle2.putBoolean("UPDATE_REMINDER", false);
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_OTA_CONFIRMATION, bundle2, false));
                break;
            }
            case R.id.settings_menu_shutdown:
                SettingsMenu.sLogger.v("shutdown");
                AnalyticsSupport.recordMenuSelection("shutdown");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_SHUTDOWN_CONFIRMATION, Shutdown.Reason.MENU.asBundle(), false));
                break;
            case R.id.settings_menu_factory_reset:
                SettingsMenu.sLogger.v("factory reset");
                AnalyticsSupport.recordMenuSelection("factory_reset");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_FACTORY_RESET));
                break;
            case R.id.settings_menu_dial_pairing:
                SettingsMenu.sLogger.v("Dial pairing");
                AnalyticsSupport.recordMenuSelection("dial_pairing");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_DIAL_PAIRING));
                break;
            case R.id.settings_menu_dial_update: {
                SettingsMenu.sLogger.v("dial update");
                AnalyticsSupport.recordMenuSelection("dial_update");
                final Bundle bundle3 = new Bundle();
                bundle3.putBoolean("UPDATE_REMINDER", false);
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_DIAL_UPDATE_CONFIRMATION, bundle3, false));
                break;
            }
            case R.id.settings_menu_learning_gestures:
                SettingsMenu.sLogger.v("Learning gestures");
                AnalyticsSupport.recordMenuSelection("learning_gestures");
                this.presenter.performSelectionAnimation(new VerticalAnimationUtils.ActionRunnable(this.bus, Screen.SCREEN_GESTURE_LEARNING));
                break;
            case R.id.main_menu_system_info:
                SettingsMenu.sLogger.v("system info");
                AnalyticsSupport.recordMenuSelection("system_info");
                if (this.systemInfoMenu == null) {
                    this.systemInfoMenu = new SystemInfoMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.systemInfoMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 1, true);
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int backSelectionId) {
        this.backSelectionId = backSelectionId;
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_settings_2, SettingsMenu.settingColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText((CharSequence)SettingsMenu.setting);
    }
    
    @Override
    public void showToolTip() {
    }
}
