package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.maps.here.HereMapUtil;
import android.text.TextUtils;
import com.navdy.hud.app.maps.util.RouteUtils;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.view.MainView;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.view.View;
import butterknife.ButterKnife;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import butterknife.InjectView;
import android.widget.TextView;
import com.squareup.otto.Bus;
import android.widget.RelativeLayout;

public class EtaView extends RelativeLayout
{
    private Bus bus;
    @InjectView(R.id.etaTextView)
    TextView etaTextView;
    @InjectView(R.id.etaTimeAmPm)
    TextView etaTimeAmPm;
    @InjectView(R.id.etaTimeLeft)
    TextView etaTimeLeft;
    private HomeScreenView homeScreenView;
    private String lastETA;
    private String lastETADate;
    private String lastETA_AmPm;
    private Logger logger;
    
    public EtaView(final Context context) {
        this(context, null);
    }
    
    public EtaView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public EtaView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    private String getEtaTimeLeft(int n) {
        final Resources resources = this.getResources();
        String s;
        if (n < 60) {
            s = resources.getString(R.string.eta_time_min, new Object[] { n });
        }
        else if (n < 1440) {
            final int n2 = n / 60;
            n -= n2 * 60;
            if (n > 0) {
                s = resources.getString(R.string.eta_time_hour, new Object[] { n2, n });
            }
            else {
                s = resources.getString(R.string.eta_time_hour_no_min, new Object[] { n2 });
            }
        }
        else {
            final int n3 = n / 1440;
            n = (n - n3 * 1440) / 60;
            if (n > 0) {
                s = resources.getString(R.string.eta_time_day, new Object[] { n3, n });
            }
            else {
                s = resources.getString(R.string.eta_time_day_no_hour, new Object[] { n3 });
            }
        }
        return s;
    }
    
    private void setEtaTextColor(final int textColor) {
        this.etaTextView.setTextColor(textColor);
        this.etaTimeLeft.setTextColor(textColor);
        this.etaTimeAmPm.setTextColor(textColor);
    }
    
    public void clearState() {
        this.lastETA = null;
        this.lastETA_AmPm = null;
        this.lastETADate = null;
        this.etaTextView.setText((CharSequence)"");
        this.etaTimeLeft.setText((CharSequence)"");
        this.etaTimeAmPm.setText((CharSequence)"");
        this.setEtaTextColor(-1);
    }
    
    public String getLastEtaDate() {
        return this.lastETADate;
    }
    
    public void init(final HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        if (!this.isInEditMode()) {
            if (HomeScreenView.showDriveScoreEventsOnMap()) {
                this.etaTimeLeft.setVisibility(GONE);
            }
            this.bus = RemoteDeviceManager.getInstance().getBus();
        }
    }
    
    @Subscribe
    public void onTrafficDelayDismissEvent(final MapEvents.TrafficDelayDismissEvent trafficDelayDismissEvent) {
        this.setEtaTextColor(-1);
    }
    
    @Subscribe
    public void onTrafficDelayEvent(final MapEvents.TrafficDelayEvent trafficDelayEvent) {
        this.setEtaTextColor(HomeScreenResourceValues.badTrafficColor);
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case EXPAND:
                this.setX((float)HomeScreenResourceValues.etaX);
                break;
            case SHRINK_LEFT:
                this.setX((float)HomeScreenResourceValues.etaShrinkLeftX);
                break;
        }
    }
    
    @Subscribe
    public void updateETA(final MapEvents.ManeuverDisplay maneuverDisplay) {
        if (this.homeScreenView.isNavigationActive()) {
            if (maneuverDisplay.etaDate == null) {
                this.logger.w("etaDate is not set");
            }
            else {
                final long n = maneuverDisplay.etaDate.getTime() - System.currentTimeMillis();
                long minutes;
                if (n < 0L) {
                    minutes = 0L;
                }
                else {
                    minutes = TimeUnit.MILLISECONDS.toMinutes(n);
                }
                this.etaTimeLeft.setText((CharSequence)RouteUtils.formatEtaMinutes(this.getResources(), (int)minutes));
                if (!TextUtils.equals((CharSequence)maneuverDisplay.eta, (CharSequence)this.lastETA)) {
                    this.lastETA = maneuverDisplay.eta;
                    this.lastETADate = HereMapUtil.convertDateToEta(maneuverDisplay.etaDate);
                    this.etaTextView.setText((CharSequence)this.lastETA);
                }
                if (!TextUtils.equals((CharSequence)maneuverDisplay.etaAmPm, (CharSequence)this.lastETA_AmPm)) {
                    this.lastETA_AmPm = maneuverDisplay.etaAmPm;
                    this.etaTimeAmPm.setText((CharSequence)this.lastETA_AmPm);
                }
            }
        }
    }
}
