package com.navdy.hud.app.ui.component.homescreen;

import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;

public class SmartDashViewResourceValues
{
    public static final int leftGaugeActiveX;
    public static final int leftGaugeActiveY;
    public static final int leftGaugeOpenX;
    public static final int leftGaugeOpenY;
    public static final int leftGaugeShrinkRightX;
    public static final int middleGaugeActiveHeight;
    public static final int middleGaugeActiveShrinkLeftX;
    public static final int middleGaugeActiveShrinkRightX;
    public static final int middleGaugeActiveWidth;
    public static final int middleGaugeActiveX;
    public static final int middleGaugeActiveY;
    public static final int middleGaugeLargeText;
    public static final int middleGaugeOpenHeight;
    public static final int middleGaugeOpenWidth;
    public static final int middleGaugeOpenX;
    public static final int middleGaugeOpenY;
    public static final int middleGaugeShrinkLeftX;
    public static final int middleGaugeShrinkRightX;
    public static final int middleGaugeSmallText;
    public static final int openAnimationDistance;
    public static final int rightGaugeActiveX;
    public static final int rightGaugeActiveY;
    public static final int rightGaugeOpenX;
    public static final int rightGaugeOpenY;
    public static final int rightGaugeShrinkLeftX;
    
    static {
        final Resources resources = HudApplication.getAppContext().getResources();
        leftGaugeOpenX = (int)resources.getDimension(R.dimen.smart_dash_open_left_gauge_x);
        leftGaugeOpenY = (int)resources.getDimension(R.dimen.smart_dash_open_left_gauge_y);
        middleGaugeOpenX = (int)resources.getDimension(R.dimen.smart_dash_open_middle_gauge_x);
        middleGaugeOpenY = (int)resources.getDimension(R.dimen.smart_dash_open_middle_gauge_y);
        rightGaugeOpenX = (int)resources.getDimension(R.dimen.smart_dash_open_right_gauge_x);
        rightGaugeOpenY = (int)resources.getDimension(R.dimen.smart_dash_open_right_gauge_y);
        leftGaugeActiveX = (int)resources.getDimension(R.dimen.smart_dash_active_left_gauge_x);
        leftGaugeActiveY = (int)resources.getDimension(R.dimen.smart_dash_active_left_gauge_y);
        middleGaugeActiveX = (int)resources.getDimension(R.dimen.smart_dash_active_middle_gauge_x);
        middleGaugeActiveY = (int)resources.getDimension(R.dimen.smart_dash_active_middle_gauge_y);
        rightGaugeActiveX = (int)resources.getDimension(R.dimen.smart_dash_active_right_gauge_x);
        rightGaugeActiveY = (int)resources.getDimension(R.dimen.smart_dash_active_right_gauge_y);
        middleGaugeShrinkLeftX = (int)resources.getDimension(R.dimen.smart_dash_middle_gauge_shrink_left_x);
        middleGaugeShrinkRightX = (int)resources.getDimension(R.dimen.smart_dash_middle_gauge_shrink_right_x);
        rightGaugeShrinkLeftX = (int)resources.getDimension(R.dimen.smart_dash_right_gauge_shrink_left_x);
        leftGaugeShrinkRightX = (int)resources.getDimension(R.dimen.smart_dash_left_gauge_shrink_right_x);
        middleGaugeActiveShrinkLeftX = (int)resources.getDimension(R.dimen.smart_dash_middle_gauge_active_shrink_left_x);
        middleGaugeActiveShrinkRightX = (int)resources.getDimension(R.dimen.smart_dash_middle_gauge_active_shrink_right_x);
        openAnimationDistance = (int)resources.getDimension(R.dimen.smart_dash_open_top_animation_dist);
        middleGaugeOpenWidth = (int)resources.getDimension(R.dimen.smart_dash_open_middle_gauge_w);
        middleGaugeOpenHeight = (int)resources.getDimension(R.dimen.smart_dash_open_middle_gauge_h);
        middleGaugeActiveWidth = (int)resources.getDimension(R.dimen.smart_dash_active_middle_gauge_w);
        middleGaugeActiveHeight = (int)resources.getDimension(R.dimen.smart_dash_active_middle_gauge_h);
        middleGaugeSmallText = (int)resources.getDimension(R.dimen.smart_dash_middle_gauge_small_text_size);
        middleGaugeLargeText = (int)resources.getDimension(R.dimen.smart_dash_middle_gauge_large_text_size);
    }
}
