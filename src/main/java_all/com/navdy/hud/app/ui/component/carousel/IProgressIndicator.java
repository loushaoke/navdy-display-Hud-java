package com.navdy.hud.app.ui.component.carousel;

import android.graphics.RectF;
import android.animation.AnimatorSet;

public interface IProgressIndicator
{
    int getCurrentItem();
    
    AnimatorSet getItemMoveAnimator(final int p0, final int p1);
    
    RectF getItemPos(final int p0);
    
    void setCurrentItem(final int p0);
    
    void setCurrentItem(final int p0, final int p1);
    
    void setItemCount(final int p0);
    
    void setOrientation(final CarouselIndicator.Orientation p0);
}
