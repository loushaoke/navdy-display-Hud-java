package com.navdy.hud.app.ui.component.vlist.viewholder;

import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import android.view.ViewGroup;
import android.widget.TextView;
import android.animation.Animator;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.TimeInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.View;
import android.view.LayoutInflater;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.view.ViewGroup;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import android.widget.ImageView;
import android.animation.ObjectAnimator;
import com.navdy.service.library.log.Logger;

public class LoadingViewHolder extends VerticalViewHolder
{
    private static final int LOADING_ANIMATION_DELAY = 33;
    private static final int LOADING_ANIMATION_DURATION = 500;
    private static final int loadingImageSize;
    private static final int loadingImageSizeUnselected;
    private static final Logger sLogger;
    private ObjectAnimator loadingAnimator;
    private ImageView loadingImage;
    
    static {
        sLogger = VerticalViewHolder.sLogger;
        final Resources resources = HudApplication.getAppContext().getResources();
        loadingImageSize = resources.getDimensionPixelSize(R.dimen.vlist_loading_image);
        loadingImageSizeUnselected = resources.getDimensionPixelSize(R.dimen.vlist_loading_image_unselected);
    }
    
    public LoadingViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
        this.loadingImage = (ImageView)viewGroup.findViewById(R.id.loading);
    }
    
    public static VerticalList.Model buildModel() {
        final VerticalList.Model model = new VerticalList.Model();
        model.type = VerticalList.ModelType.LOADING;
        model.id = R.id.vlist_loading;
        return model;
    }
    
    public static LoadingViewHolder buildViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        return new LoadingViewHolder((ViewGroup)LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vlist_loading_item, viewGroup, false), list, handler);
    }
    
    private void startLoadingAnimation() {
        if (this.loadingAnimator == null) {
            (this.loadingAnimator = ObjectAnimator.ofFloat(this.loadingImage, View.ROTATION, new float[] { 360.0f })).setDuration(500L);
            this.loadingAnimator.setInterpolator((TimeInterpolator)new AccelerateDecelerateInterpolator());
            this.loadingAnimator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                @Override
                public void onAnimationEnd(final Animator animator) {
                    if (LoadingViewHolder.this.loadingAnimator != null) {
                        LoadingViewHolder.this.loadingAnimator.setStartDelay(33L);
                        LoadingViewHolder.this.loadingAnimator.start();
                    }
                    else {
                        LoadingViewHolder.sLogger.v("abandon loading animation");
                    }
                }
            });
        }
        LoadingViewHolder.sLogger.v("started loading animation");
        this.loadingAnimator.start();
    }
    
    private void stopLoadingAnimation() {
        if (this.loadingAnimator != null) {
            LoadingViewHolder.sLogger.v("cancelled loading animation");
            this.loadingAnimator.removeAllListeners();
            this.loadingAnimator.cancel();
            this.loadingImage.setRotation(0.0f);
            this.loadingAnimator = null;
        }
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void clearAnimation() {
        this.stopLoadingAnimation();
    }
    
    @Override
    public void copyAndPosition(final ImageView imageView, final TextView textView, final TextView textView2, final TextView textView3, final boolean b) {
    }
    
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.LOADING;
    }
    
    @Override
    public void select(final VerticalList.Model model, final int n, final int n2) {
    }
    
    @Override
    public void setItemState(final State state, final AnimationType animationType, final int n, final boolean b) {
        int n2;
        if (state == State.SELECTED) {
            n2 = LoadingViewHolder.loadingImageSize;
        }
        else {
            n2 = LoadingViewHolder.loadingImageSizeUnselected;
        }
        switch (animationType) {
            case NONE:
            case INIT: {
                this.startLoadingAnimation();
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)this.loadingImage.getLayoutParams();
                viewGroup$MarginLayoutParams.width = n2;
                viewGroup$MarginLayoutParams.height = n2;
                break;
            }
            case MOVE: {
                this.startLoadingAnimation();
                final Animator animateDimension = VerticalAnimationUtils.animateDimension((View)this.loadingImage, n2);
                animateDimension.setDuration((long)n);
                animateDimension.setInterpolator((TimeInterpolator)this.interpolator);
                animateDimension.start();
                break;
            }
        }
    }
}
