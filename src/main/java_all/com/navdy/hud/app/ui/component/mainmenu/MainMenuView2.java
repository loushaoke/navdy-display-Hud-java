package com.navdy.hud.app.ui.component.mainmenu;

import com.navdy.service.library.events.input.GestureEvent;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import mortar.Mortar;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import javax.inject.Inject;
import butterknife.InjectView;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import android.widget.RelativeLayout;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;



public class MainMenuView2 extends RelativeLayout implements IInputHandler
{
    private static final Logger sLogger;
    private VerticalMenuComponent.Callback callback;
    @InjectView(R.id.confirmationLayout)
    public ConfirmationLayout confirmationLayout;
    @Inject
    public MainMenuScreen2.Presenter presenter;
    @InjectView(R.id.rightBackground)
    View rightBackground;
    public View scrimCover;
    VerticalMenuComponent vmenuComponent;
    
    static {
        sLogger = new Logger(MainMenuView2.class);
    }
    
    public MainMenuView2(final Context context) {
        this(context, null);
    }
    
    public MainMenuView2(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public MainMenuView2(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.callback = new VerticalMenuComponent.Callback() {
            @Override
            public void close() {
                MainMenuView2.this.presenter.close();
            }
            
            @Override
            public boolean isClosed() {
                return MainMenuView2.this.presenter.isClosed();
            }
            
            @Override
            public boolean isItemClickable(final VerticalList.ItemSelectionState itemSelectionState) {
                return MainMenuView2.this.presenter.isItemClickable(itemSelectionState);
            }
            
            @Override
            public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
                final IMenu currentMenu = MainMenuView2.this.presenter.getCurrentMenu();
                if (currentMenu != null) {
                    currentMenu.onBindToView(model, view, n, modelState);
                }
            }
            
            @Override
            public void onFastScrollEnd() {
                final IMenu currentMenu = MainMenuView2.this.presenter.getCurrentMenu();
                if (currentMenu != null) {
                    currentMenu.onFastScrollEnd();
                }
            }
            
            @Override
            public void onFastScrollStart() {
                final IMenu currentMenu = MainMenuView2.this.presenter.getCurrentMenu();
                if (currentMenu != null) {
                    currentMenu.onFastScrollStart();
                }
            }
            
            @Override
            public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
                final IMenu currentMenu = MainMenuView2.this.presenter.getCurrentMenu();
                if (currentMenu != null) {
                    currentMenu.onItemSelected(itemSelectionState);
                }
            }
            
            @Override
            public void onLoad() {
                MainMenuView2.sLogger.v("onLoad");
                MainMenuView2.this.presenter.resetSelectedItem();
            }
            
            @Override
            public void onScrollIdle() {
                final IMenu currentMenu = MainMenuView2.this.presenter.getCurrentMenu();
                if (currentMenu != null) {
                    currentMenu.onScrollIdle();
                }
            }
            
            @Override
            public void select(final VerticalList.ItemSelectionState itemSelectionState) {
                MainMenuView2.this.presenter.selectItem(itemSelectionState);
            }
            
            @Override
            public void showToolTip() {
                final IMenu currentMenu = MainMenuView2.this.presenter.getCurrentMenu();
                if (currentMenu != null) {
                    currentMenu.showToolTip();
                }
            }
        };
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler((View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        this.presenter.sendCloseEvent();
        this.vmenuComponent.verticalList.cancelLoadingAnimation(1);
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.vmenuComponent = new VerticalMenuComponent((ViewGroup)this, this.callback, true);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
        (this.scrimCover = LayoutInflater.from(this.getContext()).inflate(R.layout.screen_main_menu_2_scrim, (ViewGroup)null)).setLayoutParams((ViewGroup.LayoutParams)new ViewGroup.MarginLayoutParams(-1, -1));
        this.vmenuComponent.rightContainer.addView(this.scrimCover);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return this.confirmationLayout.getVisibility() != 0 && this.vmenuComponent.handleGesture(gestureEvent);
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b;
        if (this.confirmationLayout.getVisibility() == 0) {
            b = this.confirmationLayout.handleKey(customKeyEvent);
        }
        else {
            b = this.vmenuComponent.handleKey(customKeyEvent);
        }
        return b;
    }
    
    void performSelectionAnimation(final Runnable runnable, final int n) {
        this.vmenuComponent.performSelectionAnimation(runnable, n);
    }
}
