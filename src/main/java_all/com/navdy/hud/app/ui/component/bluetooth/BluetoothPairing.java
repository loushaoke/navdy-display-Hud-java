package com.navdy.hud.app.ui.component.bluetooth;

import android.content.res.Resources;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.view.ToastView;
import com.navdy.hud.app.manager.InputManager;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.device.dial.DialManager;
import android.os.Bundle;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.util.BluetoothUtil;
import android.bluetooth.BluetoothDevice;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import java.util.ArrayList;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.framework.toast.IToastCallback;

public class BluetoothPairing implements IToastCallback
{
    public static final String ARG_AUTO_ACCEPT = "auto";
    public static final String ARG_DEVICE = "device";
    public static final String ARG_PIN = "pin";
    public static final String ARG_VARIANT = "variant";
    private static final String BLUETOOTH_PAIRING_TOAST_ID = "bt-pairing";
    private static final Logger sLogger;
    private boolean acceptDone;
    private boolean autoAccept;
    private Bus bus;
    private ArrayList<ChoiceLayout.Choice> choices;
    private boolean confirmationRequired;
    private BluetoothDevice device;
    private boolean initialized;
    private int pin;
    private int variant;
    
    static {
        sLogger = new Logger(BluetoothPairing.class);
    }
    
    BluetoothPairing(final BluetoothDevice device, final int pin, final int variant, final boolean autoAccept, final Bus bus) {
        this.choices = new ArrayList<ChoiceLayout.Choice>(2);
        this.device = device;
        this.pin = pin;
        this.variant = variant;
        this.autoAccept = autoAccept;
        this.bus = bus;
    }
    
    private void confirm() {
        BluetoothPairing.sLogger.v("confirm()");
        BluetoothUtil.confirmPairing(this.device, this.variant, this.pin);
        this.finish();
    }
    
    private void finish() {
        BluetoothPairing.sLogger.v("finish()");
        ToastManager.getInstance().dismissCurrentToast();
    }
    
    private String formatPin(final int n, final int n2) {
        String s;
        if (n2 == 5) {
            s = String.format("%04d", n);
        }
        else {
            s = String.format("%06d", n);
        }
        return s;
    }
    
    public static void showBluetoothPairingToast(final Bundle bundle) {
        BluetoothPairing.sLogger.v("showBluetoothPairingToast");
        final BluetoothDevice bluetoothDevice = (BluetoothDevice)bundle.getParcelable("device");
        final int int1 = bundle.getInt("pin");
        final int int2 = bundle.getInt("variant");
        boolean boolean1 = bundle.getBoolean("auto");
        if (!Boolean.TRUE.equals(DialManager.getInstance().isDialConnected())) {
            boolean1 = true;
        }
        final BluetoothPairing bluetoothPairing = new BluetoothPairing(bluetoothDevice, int1, int2, boolean1, RemoteDeviceManager.getInstance().getBus());
        BluetoothPairing.sLogger.v("show()");
        bluetoothPairing.show();
    }
    
    @Override
    public void executeChoiceItem(final int n, final int n2) {
        BluetoothPairing.sLogger.v("execute:" + n);
        if (this.confirmationRequired) {
            if (n == 0) {
                this.confirm();
            }
            else {
                this.finish();
            }
        }
        else {
            this.finish();
        }
    }
    
    @Subscribe
    public void onBondStateChange(final BondStateChange bondStateChange) {
        BluetoothPairing.sLogger.v("onBondStateChange");
        if (bondStateChange.device.equals(this.device)) {
            if (bondStateChange.newState == 10 || bondStateChange.newState == 12) {
                BluetoothPairing.sLogger.i("ending pairing dialog because of bond state change");
                this.finish();
            }
            else {
                BluetoothPairing.sLogger.i("Ignoring bond state change for " + bondStateChange.device.getAddress() + " state: " + bondStateChange.newState + " dialog device:" + this.device.getAddress());
            }
        }
    }
    
    @Override
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        return false;
    }
    
    @Subscribe
    public void onPairingCancelled(final PairingCancelled pairingCancelled) {
        BluetoothPairing.sLogger.v("onPairingCancelled");
        this.finish();
    }
    
    @Override
    public void onStart(final ToastView toastView) {
        BluetoothPairing.sLogger.v("registerBus");
        this.bus.register(this);
        if (!this.acceptDone && this.autoAccept) {
            this.acceptDone = true;
            BluetoothPairing.sLogger.v("auto-accept");
            BluetoothUtil.confirmPairing(this.device, this.variant, this.pin);
        }
    }
    
    @Override
    public void onStop() {
        BluetoothPairing.sLogger.v("unregisterBus");
        this.bus.unregister(this);
    }
    
    void show() {
        final Resources resources = HudApplication.getAppContext().getResources();
        final Bundle bundle = new Bundle();
        bundle.putInt("8", R.drawable.icon_bluetooth_connecting);
        bundle.putString("1", resources.getString(R.string.bluetooth_pairing_title));
        bundle.putString("2", resources.getString(R.string.bluetooth_pairing_code));
        String s = null;
        if (this.device != null && TextUtils.isEmpty((CharSequence)(s = this.device.getName()))) {
            s = this.device.getAddress();
        }
        String string;
        if ((string = s) == null) {
            string = resources.getString(R.string.unknown_device);
        }
        bundle.putString("6", resources.getString(R.string.bluetooth_device, new Object[] { string }));
        bundle.putString("4", this.formatPin(this.pin, this.variant));
        final boolean confirmationRequired = !this.autoAccept;
        if (confirmationRequired != this.confirmationRequired || !this.initialized) {
            this.initialized = true;
            if (this.confirmationRequired = confirmationRequired) {
                this.choices.add(new ChoiceLayout.Choice(resources.getString(R.string.bluetooth_confirm), 0));
                this.choices.add(new ChoiceLayout.Choice(resources.getString(R.string.cancel), 0));
            }
            else {
                this.choices.add(new ChoiceLayout.Choice(resources.getString(R.string.cancel), 0));
            }
            bundle.putParcelableArrayList("20", (ArrayList)this.choices);
        }
        final ToastManager instance = ToastManager.getInstance();
        instance.dismissCurrentToast();
        instance.clearAllPendingToast();
        instance.addToast(new ToastManager.ToastParams("bt-pairing", bundle, this, false, true, true));
    }
    
    public static class BondStateChange
    {
        public final BluetoothDevice device;
        public final int newState;
        public final int oldState;
        
        public BondStateChange(final BluetoothDevice device, final int oldState, final int newState) {
            this.device = device;
            this.oldState = oldState;
            this.newState = newState;
        }
    }
    
    public static class PairingCancelled
    {
        public final BluetoothDevice device;
        
        public PairingCancelled(final BluetoothDevice device) {
            this.device = device;
        }
    }
}
