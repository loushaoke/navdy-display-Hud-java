package com.navdy.hud.app.ui.component.vmenu;

import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.service.library.events.ui.Screen;
import com.squareup.otto.Bus;
import android.os.Bundle;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.animation.ValueAnimator;
import android.view.ViewGroup.MarginLayoutParams;
import android.animation.Animator;
import android.view.View;

public class VerticalAnimationUtils
{
    public static Animator animateDimension(final View view, final int n) {
        final ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        final ValueAnimator ofInt = ValueAnimator.ofInt(new int[] { marginLayoutParams.width, n });
        ofInt.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                final int intValue = (int)valueAnimator.getAnimatedValue();
                marginLayoutParams.width = intValue;
                marginLayoutParams.height = intValue;
                view.requestLayout();
            }
        });
        return (Animator)ofInt;
    }
    
    public static Animator animateMargin(final View view, final int n) {
        final ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        final ValueAnimator ofInt = ValueAnimator.ofInt(new int[] { marginLayoutParams.topMargin, n });
        ofInt.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                marginLayoutParams.topMargin = (int)valueAnimator.getAnimatedValue();
                view.requestLayout();
            }
        });
        return (Animator)ofInt;
    }
    
    public static void copyImage(final ImageView imageView, final ImageView imageView2) {
        final Bitmap bitmapFromImageView = getBitmapFromImageView(imageView);
        if (bitmapFromImageView != null) {
            imageView2.setImageBitmap(bitmapFromImageView);
        }
    }
    
    public static Bitmap getBitmapFromImageView(final ImageView imageView) {
        final Drawable drawable = imageView.getDrawable();
        BitmapDrawable bitmapDrawable = null;
        if (drawable instanceof BitmapDrawable) {
            bitmapDrawable = (BitmapDrawable)drawable;
        }
        Bitmap bitmap;
        if (!(imageView instanceof IconColorImageView) && bitmapDrawable != null) {
            bitmap = bitmapDrawable.getBitmap();
        }
        else {
            final Bitmap bitmap2 = Bitmap.createBitmap(imageView.getWidth(), imageView.getHeight(), Bitmap$Config.ARGB_8888);
            imageView.draw(new Canvas(bitmap2));
            bitmap = bitmap2;
        }
        return bitmap;
    }
    
    public static void performClick(final View view, final int n, final Runnable runnable) {
        performClickDown(view, n / 2, runnable, true);
    }
    
    public static void performClickDown(final View view, final int n, final Runnable runnable, final boolean b) {
        view.animate().scaleX(0.8f).scaleY(0.8f).setDuration((long)n).withEndAction((Runnable)new Runnable() {
            @Override
            public void run() {
                if (b) {
                    VerticalAnimationUtils.performClickUp(view, n, runnable);
                }
                else if (runnable != null) {
                    runnable.run();
                }
            }
        });
    }
    
    public static void performClickUp(final View view, final int n, final Runnable runnable) {
        view.animate().scaleX(1.0f).scaleY(1.0f).setDuration((long)n).withEndAction((Runnable)new Runnable() {
            @Override
            public void run() {
                if (runnable != null) {
                    runnable.run();
                }
            }
        }).start();
    }
    
    public static class ActionRunnable implements Runnable
    {
        private Bundle args;
        private Bus bus;
        private boolean ignoreAnimation;
        private Screen screen;
        
        public ActionRunnable(final Bus bus, final Screen screen) {
            this(bus, screen, null, false);
        }
        
        public ActionRunnable(final Bus bus, final Screen screen, final Bundle args, final boolean ignoreAnimation) {
            this.bus = bus;
            this.screen = screen;
            this.args = args;
            this.ignoreAnimation = ignoreAnimation;
        }
        
        @Override
        public void run() {
            if (this.args == null) {
                this.bus.post(new ShowScreen.Builder().screen(this.screen).build());
            }
            else {
                this.bus.post(new ShowScreenWithArgs(this.screen, this.args, this.ignoreAnimation));
            }
        }
    }
}
