package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.widget.ImageView;
import android.view.LayoutInflater;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.view.ViewGroup;
import android.widget.TextView;
import com.navdy.service.library.log.Logger;

public class TitleViewHolder extends VerticalViewHolder
{
    private static final Logger sLogger;
    private TextView title;
    
    static {
        sLogger = VerticalViewHolder.sLogger;
    }
    
    public TitleViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
        this.title = (TextView)viewGroup.findViewById(R.id.title);
    }
    
    public static VerticalList.Model buildModel(final String title) {
        final VerticalList.Model model = new VerticalList.Model();
        model.type = VerticalList.ModelType.TITLE;
        model.id = R.id.vlist_title;
        model.title = title;
        return model;
    }
    
    public static TitleViewHolder buildViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        return new TitleViewHolder((ViewGroup)LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vlist_title, viewGroup, false), list, handler);
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        this.title.setText((CharSequence)model.title);
    }
    
    @Override
    public void clearAnimation() {
    }
    
    @Override
    public void copyAndPosition(final ImageView imageView, final TextView textView, final TextView textView2, final TextView textView3, final boolean b) {
    }
    
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.TITLE;
    }
    
    @Override
    public void preBind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
        this.title.setTextSize(model.fontInfo.titleFontSize);
    }
    
    @Override
    public void select(final VerticalList.Model model, final int n, final int n2) {
    }
    
    @Override
    public void setItemState(final State state, final AnimationType animationType, final int n, final boolean b) {
    }
}
