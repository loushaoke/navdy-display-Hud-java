package com.navdy.hud.app.ui.component.destination;

import android.os.Parcel;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.hud.app.framework.contacts.Contact;
import java.util.List;
import android.os.Parcelable;
import android.os.Parcelable;

public class DestinationParcelable implements Parcelable
{
    public static final Parcelable.Creator<DestinationParcelable> CREATOR;
    private static final String EMPTY = "";
    List<Contact> contacts;
    String destinationAddress;
    String destinationPlaceId;
    String destinationSubTitle;
    String destinationSubTitle2;
    boolean destinationSubTitle2Formatted;
    boolean destinationSubTitleFormatted;
    String destinationTitle;
    double displayLatitude;
    double displayLongitude;
    public String distanceStr;
    int icon;
    int iconDeselectedColor;
    int iconSelectedColor;
    int iconUnselected;
    int id;
    String identifier;
    double navLatitude;
    double navLongitude;
    List<Contact> phoneNumbers;
    PlaceType placeType;
    String routeId;
    DestinationType type;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<DestinationParcelable>() {
            public DestinationParcelable createFromParcel(final Parcel parcel) {
                return new DestinationParcelable(parcel);
            }
            
            public DestinationParcelable[] newArray(final int n) {
                return new DestinationParcelable[n];
            }
        };
    }
    
    public DestinationParcelable(final int id, final String destinationTitle, final String destinationSubTitle, final boolean destinationSubTitleFormatted, final String destinationSubTitle2, final boolean destinationSubTitle2Formatted, final String destinationAddress, final double navLatitude, final double navLongitude, final double displayLatitude, final double displayLongitude, final int icon, final int iconUnselected, final int iconSelectedColor, final int iconDeselectedColor, final DestinationType type, final PlaceType placeType) {
        this.id = id;
        this.destinationTitle = destinationTitle;
        this.destinationSubTitle = destinationSubTitle;
        this.destinationSubTitleFormatted = destinationSubTitleFormatted;
        this.destinationSubTitle2 = destinationSubTitle2;
        this.destinationSubTitle2Formatted = destinationSubTitle2Formatted;
        this.destinationAddress = destinationAddress;
        this.navLatitude = navLatitude;
        this.navLongitude = navLongitude;
        this.displayLatitude = displayLatitude;
        this.displayLongitude = displayLongitude;
        this.icon = icon;
        this.iconUnselected = iconUnselected;
        this.iconSelectedColor = iconSelectedColor;
        this.iconDeselectedColor = iconDeselectedColor;
        this.type = type;
        this.placeType = placeType;
    }
    
    protected DestinationParcelable(final Parcel parcel) {
        final boolean b = true;
        this.destinationTitle = parcel.readString();
        this.destinationSubTitle = parcel.readString();
        this.destinationSubTitleFormatted = (parcel.readByte() == 1);
        this.destinationSubTitle2 = parcel.readString();
        this.destinationSubTitle2Formatted = (parcel.readByte() == 1 && b);
        this.destinationAddress = parcel.readString();
        this.navLatitude = parcel.readDouble();
        this.navLongitude = parcel.readDouble();
        this.displayLatitude = parcel.readDouble();
        this.displayLongitude = parcel.readDouble();
        this.icon = parcel.readInt();
        this.iconUnselected = parcel.readInt();
        this.iconSelectedColor = parcel.readInt();
        this.iconDeselectedColor = parcel.readInt();
        this.routeId = parcel.readString();
        this.destinationPlaceId = parcel.readString();
        this.type = DestinationType.values()[parcel.readInt()];
        this.id = parcel.readInt();
        this.placeType = PlaceType.values()[parcel.readInt()];
        this.contacts = (List<Contact>)parcel.readArrayList((ClassLoader)null);
        this.phoneNumbers = (List<Contact>)parcel.readArrayList((ClassLoader)null);
        this.identifier = parcel.readString();
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void setContacts(final List<Contact> contacts) {
        this.contacts = contacts;
    }
    
    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }
    
    public void setPhoneNumbers(final List<Contact> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }
    
    public void setPlaceId(final String destinationPlaceId) {
        this.destinationPlaceId = destinationPlaceId;
    }
    
    public void setRouteId(final String routeId) {
        this.routeId = routeId;
    }
    
    public void writeToParcel(final Parcel parcel, int n) {
        n = 1;
        String destinationTitle;
        if (this.destinationTitle == null) {
            destinationTitle = "";
        }
        else {
            destinationTitle = this.destinationTitle;
        }
        parcel.writeString(destinationTitle);
        String destinationSubTitle;
        if (this.destinationSubTitle == null) {
            destinationSubTitle = "";
        }
        else {
            destinationSubTitle = this.destinationSubTitle;
        }
        parcel.writeString(destinationSubTitle);
        byte b;
        if (this.destinationSubTitleFormatted) {
            b = 1;
        }
        else {
            b = 0;
        }
        parcel.writeByte(b);
        String destinationSubTitle2;
        if (this.destinationSubTitle2 == null) {
            destinationSubTitle2 = "";
        }
        else {
            destinationSubTitle2 = this.destinationSubTitle2;
        }
        parcel.writeString(destinationSubTitle2);
        int n2;
        if (this.destinationSubTitle2Formatted) {
            n2 = n;
        }
        else {
            n = (n2 = 0);
        }
        parcel.writeByte((byte)n2);
        String destinationAddress;
        if (this.destinationAddress == null) {
            destinationAddress = "";
        }
        else {
            destinationAddress = this.destinationAddress;
        }
        parcel.writeString(destinationAddress);
        parcel.writeDouble(this.navLatitude);
        parcel.writeDouble(this.navLongitude);
        parcel.writeDouble(this.displayLatitude);
        parcel.writeDouble(this.displayLongitude);
        parcel.writeInt(this.icon);
        parcel.writeInt(this.iconUnselected);
        parcel.writeInt(this.iconSelectedColor);
        parcel.writeInt(this.iconDeselectedColor);
        String routeId;
        if (this.routeId == null) {
            routeId = "";
        }
        else {
            routeId = this.routeId;
        }
        parcel.writeString(routeId);
        String routeId2;
        if (this.destinationPlaceId == null) {
            routeId2 = "";
        }
        else {
            routeId2 = this.routeId;
        }
        parcel.writeString(routeId2);
        parcel.writeInt(this.type.ordinal());
        parcel.writeInt(this.id);
        parcel.writeInt(this.placeType.ordinal());
        parcel.writeList((List)this.contacts);
        parcel.writeList((List)this.phoneNumbers);
        parcel.writeString(this.identifier);
    }
    
    public enum DestinationType
    {
        DESTINATION, 
        NONE;
    }
}
