package com.navdy.hud.app.ui.component.vlist;

import android.view.View;
import android.support.v7.widget.RecyclerView.SmoothScroller;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.animation.Interpolator;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import android.support.v7.widget.LinearLayoutManager;

public class VerticalLayoutManager extends LinearLayoutManager
{
    private static final Logger sLogger;
    private VerticalList.Callback callback;
    private Context context;
    private boolean listLoaded;
    private VerticalRecyclerView recyclerView;
    private VerticalList vlist;
    
    static {
        sLogger = new Logger(VerticalLayoutManager.class);
    }
    
    public VerticalLayoutManager(final Context context, final VerticalList vlist, final VerticalList.Callback callback, final VerticalRecyclerView recyclerView) {
        super(context);
        this.context = context;
        this.vlist = vlist;
        this.callback = callback;
        this.recyclerView = recyclerView;
    }
    
    public void clear() {
        this.listLoaded = false;
    }
    
    @Override
    public void onLayoutChildren(final RecyclerView.Recycler recycler, final RecyclerView.State state) {
        super.onLayoutChildren(recycler, state);
        if (!this.listLoaded && !state.isPreLayout()) {
            this.listLoaded = true;
            this.callback.onLoad();
        }
    }
    
    public void smoothScrollToPosition(final int targetPosition, final int n) {
        final VerticalScroller verticalScroller = new VerticalScroller(this.context, this.vlist, this, this.recyclerView, targetPosition, n);
        ((RecyclerView.SmoothScroller)verticalScroller).setTargetPosition(targetPosition);
        ((RecyclerView.LayoutManager)this).startSmoothScroll(verticalScroller);
    }
    
    private static class VerticalScroller extends LinearSmoothScroller
    {
        private Interpolator interpolator;
        private VerticalLayoutManager layoutManager;
        private int position;
        private VerticalRecyclerView recyclerView;
        private int snapPreference;
        private VerticalList vlist;
        
        public VerticalScroller(final Context context, final VerticalList vlist, final VerticalLayoutManager layoutManager, final VerticalRecyclerView recyclerView, final int position, final int snapPreference) {
            super(context);
            this.vlist = vlist;
            this.layoutManager = layoutManager;
            this.recyclerView = recyclerView;
            this.position = position;
            this.snapPreference = snapPreference;
            this.interpolator = VerticalList.getInterpolator();
        }
        
        @Override
        protected float calculateSpeedPerPixel(final DisplayMetrics displayMetrics) {
            return super.calculateSpeedPerPixel(displayMetrics);
        }
        
        @Override
        protected int calculateTimeForDeceleration(final int n) {
            return this.vlist.animationDuration;
        }
        
        @Override
        protected int calculateTimeForScrolling(final int n) {
            return super.calculateTimeForScrolling(n);
        }
        
        @Override
        public PointF computeScrollVectorForPosition(final int n) {
            return this.layoutManager.computeScrollVectorForPosition(n);
        }
        
        @Override
        protected int getVerticalSnapPreference() {
            return this.snapPreference;
        }
        
        @Override
        protected void onSeekTargetStep(final int n, final int n2, final RecyclerView.State state, final Action action) {
            super.onSeekTargetStep(n, n2, state, action);
        }
        
        @Override
        protected void onStart() {
            super.onStart();
        }
        
        @Override
        protected void onStop() {
            super.onStop();
        }
        
        @Override
        protected void onTargetFound(final View view, final RecyclerView.State state, final Action action) {
            final int calculateDxToMakeVisible = this.calculateDxToMakeVisible(view, this.getHorizontalSnapPreference());
            final int calculateDyToMakeVisible = this.calculateDyToMakeVisible(view, this.getVerticalSnapPreference());
            final int calculateTimeForDeceleration = this.calculateTimeForDeceleration((int)Math.sqrt(calculateDxToMakeVisible * calculateDxToMakeVisible + calculateDyToMakeVisible * calculateDyToMakeVisible));
            if (calculateTimeForDeceleration > 0) {
                action.update(-calculateDxToMakeVisible, -calculateDyToMakeVisible, calculateTimeForDeceleration, this.interpolator);
            }
            if (VerticalLayoutManager.sLogger.isLoggable(2)) {
                VerticalLayoutManager.sLogger.v("target found dy=" + action.getDy());
            }
            this.vlist.targetFound(action.getDy());
        }
        
        @Override
        protected void updateActionForInterimTarget(final Action action) {
            super.updateActionForInterimTarget(action);
        }
    }
}
