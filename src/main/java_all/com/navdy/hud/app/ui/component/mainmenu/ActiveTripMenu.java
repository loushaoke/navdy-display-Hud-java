package com.navdy.hud.app.ui.component.mainmenu;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.navdy.service.library.events.navigation.DistanceUnit;
import android.graphics.Shader;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.navdy.hud.app.maps.util.DistanceConverter;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.Iterator;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.util.PhoneUtil;
import java.util.HashMap;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.profile.DriverProfile;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.preferences.NavigationPreferencesUpdate;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.hud.app.framework.DriverProfileHelper;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.text.SpannableStringBuilder;
import java.util.Date;
import com.here.android.mpa.routing.Route;
import com.navdy.service.library.events.navigation.NavigationTurn;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.here.android.mpa.routing.Maneuver;
import java.util.ArrayList;
import android.text.TextUtils;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import android.os.SystemClock;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoBoundingBox;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import android.os.Looper;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.service.library.events.contacts.PhoneNumber;
import com.navdy.hud.app.view.drawable.ETAProgressDrawable;
import com.navdy.hud.app.framework.contacts.Contact;
import java.util.List;
import com.squareup.otto.Bus;
import android.view.View;
import android.widget.TextView;
import java.util.HashSet;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;

public class ActiveTripMenu implements IMenu
{
    private static final int ARRIVAL_EXPAND = 100;
    private static final int MIN_MANEUVER_DISTANCE = 100;
    private static final int TITLE_UPDATE_INTERVAL = 1000;
    private static final String TOWARDS_PATTERN;
    private static final String activeTripTitle;
    private static final String arriveTitle;
    private static final String arrivedDistance;
    private static final String arrivedTitle;
    private static final VerticalList.Model back;
    private static final VerticalList.Model endTrip;
    private static final String feetLabel;
    private static final Handler handler;
    private static final VerticalList.Model infoModel;
    private static final String kiloMetersLabel;
    public static final int maneuverIconSize;
    private static final String metersLabel;
    private static final String milesLabel;
    private static final VerticalList.Model muteTbtAudio;
    private static final VerticalList.Model reportIssue;
    private static final Resources resources;
    private static final Logger sLogger;
    private static final int size_18;
    private static final int size_22;
    private static final StringBuilder timeBuilder;
    private static final TimeHelper timeHelper;
    private static final VerticalList.Model tripManeuvers;
    private static final int tripOptionsColor;
    private static final HashSet<String> turnNotShown;
    private static final VerticalList.Model unmuteTbtAudio;
    private TextView activeTripDistance;
    private TextView activeTripDuration;
    private TextView activeTripEta;
    private View activeTripProgress;
    private TextView activeTripSubTitleView;
    private TextView activeTripTitleView;
    private boolean arrived;
    private int backSelection;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    private List<Contact> contactList;
    private ContactOptionsMenu contactOptionsMenu;
    private int currentSelection;
    private ETAProgressDrawable etaProgressDrawable;
    private boolean getItemsCalled;
    private String label;
    private boolean maneuverSelected;
    private IMenu parent;
    private PhoneNumber phoneNumber;
    private boolean positionOnFirstManeuver;
    private MainMenuScreen2.Presenter presenter;
    private boolean registered;
    private ReportIssueMenu reportIssueMenu;
    private VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new Logger(ActiveTripMenu.class);
        timeBuilder = new StringBuilder();
        turnNotShown = new HashSet<String>();
        TOWARDS_PATTERN = HereManeuverDisplayBuilder.TOWARDS + " ";
        handler = new Handler(Looper.getMainLooper());
        resources = HudApplication.getAppContext().getResources();
        final int color = ActiveTripMenu.resources.getColor(R.color.mm_back);
        tripOptionsColor = ActiveTripMenu.resources.getColor(R.color.mm_active_trip);
        size_22 = ActiveTripMenu.resources.getDimensionPixelSize(R.dimen.active_trip_22);
        size_18 = ActiveTripMenu.resources.getDimensionPixelSize(R.dimen.active_trip_18);
        maneuverIconSize = ActiveTripMenu.resources.getDimensionPixelSize(R.dimen.active_trip_maneuver_icon_size);
        activeTripTitle = ActiveTripMenu.resources.getString(R.string.mm_active_trip);
        arrivedTitle = ActiveTripMenu.resources.getString(R.string.mm_active_trip_arrived_no_label);
        arriveTitle = ActiveTripMenu.resources.getString(R.string.mm_active_trip_arrive);
        metersLabel = ActiveTripMenu.resources.getString(R.string.unit_meters);
        kiloMetersLabel = ActiveTripMenu.resources.getString(R.string.unit_kilometers);
        feetLabel = ActiveTripMenu.resources.getString(R.string.unit_feet);
        milesLabel = ActiveTripMenu.resources.getString(R.string.unit_miles);
        arrivedDistance = ActiveTripMenu.resources.getString(R.string.mm_active_trip_arrived_dist);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, color, MainMenu.bkColorUnselected, color, ActiveTripMenu.resources.getString(R.string.back), null);
        infoModel = ScrollableViewHolder.buildModel(R.layout.active_trip_menu_lyt);
        final String string = ActiveTripMenu.resources.getString(R.string.end_trip);
        final int color2 = ActiveTripMenu.resources.getColor(R.color.mm_options_end_trip);
        endTrip = IconBkColorViewHolder.buildModel(R.id.main_menu_options_end_trip, R.drawable.icon_active_nav_end_2, color2, MainMenu.bkColorUnselected, color2, string, null);
        final String string2 = ActiveTripMenu.resources.getString(R.string.mute_tbt_2);
        final int color3 = ActiveTripMenu.resources.getColor(R.color.mm_options_mute_tbt_audio);
        muteTbtAudio = IconBkColorViewHolder.buildModel(R.id.main_menu_options_mute_tbt, R.drawable.icon_active_nav_mute_2, color3, MainMenu.bkColorUnselected, color3, string2, null);
        final String string3 = ActiveTripMenu.resources.getString(R.string.unmute_tbt_2);
        final int color4 = ActiveTripMenu.resources.getColor(R.color.mm_options_unmute_tbt_audio);
        unmuteTbtAudio = IconBkColorViewHolder.buildModel(R.id.main_menu_options_unmute_tbt, R.drawable.icon_active_nav_unmute_2, color4, MainMenu.bkColorUnselected, color4, string3, null);
        final String string4 = ActiveTripMenu.resources.getString(R.string.report_navigation_issue);
        final int color5 = ActiveTripMenu.resources.getColor(R.color.mm_options_report_issue);
        reportIssue = IconBkColorViewHolder.buildModel(R.id.main_menu_options_report_issue, R.drawable.icon_options_report_issue_2, color5, MainMenu.bkColorUnselected, color5, string4, null);
        tripManeuvers = TitleViewHolder.buildModel(ActiveTripMenu.resources.getString(R.string.trip_maneuvers));
        ActiveTripMenu.tripManeuvers.id = R.id.active_trip_maneuver_title;
        timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
        ActiveTripMenu.turnNotShown.add(HereManeuverDisplayBuilder.START_TURN);
        ActiveTripMenu.turnNotShown.add(HereManeuverDisplayBuilder.ARRIVED);
    }
    
    ActiveTripMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent) {
        this.currentSelection = -1;
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }
    
    private GeoBoundingBox buildArrivalBoundingBox(final NavigationRouteRequest navigationRouteRequest) {
        while (true) {
            final GeoCoordinate geoCoordinate = null;
            try {
                GeoCoordinate geoCoordinate2;
                if (navigationRouteRequest.destination.latitude != 0.0 && navigationRouteRequest.destination.longitude != 0.0) {
                    geoCoordinate2 = new GeoCoordinate(navigationRouteRequest.destination.latitude, navigationRouteRequest.destination.longitude);
                }
                else {
                    geoCoordinate2 = geoCoordinate;
                    if (navigationRouteRequest.destinationDisplay != null) {
                        geoCoordinate2 = geoCoordinate;
                        if (navigationRouteRequest.destinationDisplay.latitude != 0.0) {
                            geoCoordinate2 = geoCoordinate;
                            if (navigationRouteRequest.destinationDisplay.longitude != 0.0) {
                                geoCoordinate2 = new GeoCoordinate(navigationRouteRequest.destinationDisplay.latitude, navigationRouteRequest.destinationDisplay.longitude);
                            }
                        }
                    }
                }
                if (geoCoordinate2 != null) {
                    final GeoBoundingBox geoBoundingBox = new GeoBoundingBox(geoCoordinate2, geoCoordinate2);
                    geoBoundingBox.expand(100.0f, 100.0f);
                    return geoBoundingBox;
                }
            }
            catch (Throwable t) {
                ActiveTripMenu.sLogger.e(t);
            }
            return null;
        }
    }

    private void buildManeuvers(List<VerticalList.Model> a) {
        try {
            String s = null;
            long j = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HereNavigationManager a0 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            com.here.android.mpa.routing.Route a1 = a0.getCurrentRoute();
            String s0 = a0.getDestinationLabel();
            String s1 = a0.getFullStreetAddress();
            if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    s = s1;
                    s1 = null;
                } else {
                    s = null;
                }
            } else {
                s = s1;
                s1 = s0;
            }
            if (a1 != null) {
                java.util.ArrayList a2 = new java.util.ArrayList();
                java.util.List a3 = a1.getManeuvers();
                int i = a3.size();
                com.here.android.mpa.routing.Maneuver a4 = a0.getNavManeuver();
                if (a4 == null) {
                    sLogger.v("current maneuver not found");
                }
                com.navdy.service.library.events.navigation.NavigationRouteRequest a5 = a0.getCurrentNavigationRouteRequest();
                a.add(tripManeuvers);
                Object a6 = a;
                Object a7 = a3;
                boolean b = false;
                int i0 = 0;
                while(i0 < i) {
                    com.here.android.mpa.routing.Maneuver a8 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a7).get(i0);
                    if (!com.navdy.hud.app.maps.here.HereMapUtil.isStartManeuver(a8)) {
                        if (b) {
                            ((java.util.List)a2).add(a8);
                        } else if (a4 != null) {
                            if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual(a4, a8)) {
                                ((java.util.List)a2).add(a8);
                                b = true;
                            }
                        } else {
                            ((java.util.List)a2).add(a8);
                        }
                    }
                    i0 = i0 + 1;
                }
                int i1 = ((java.util.List)a2).size();
                com.here.android.mpa.routing.Maneuver a9 = null;
                int i2 = 0;
                while(i2 < i1) {
                    String s2 = null;
                    com.here.android.mpa.routing.Maneuver a10 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a2).get(i2);
                    int i3 = i1 - 1;
                    com.here.android.mpa.routing.Maneuver a11 = null;
                    if (i2 < i3) {
                        a11 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a2).get(i2 + 1);
                    }
                    com.navdy.hud.app.maps.MapEvents.ManeuverDisplay a12 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverDisplay(a10, a11 == null, (long)a10.getDistanceToNextManeuver(), s, a9, a5, a11, true, false, null);
                    boolean b0 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.shouldShowTurnText(a12.pendingTurn);
                    label3: {
                        label2: {
                            if (!b0) {
                                break label2;
                            }
                            if (turnNotShown.contains(a12.pendingTurn)) {
                                break label2;
                            }
                            s2 = new StringBuilder().append(a12.pendingTurn).append(" ").append(a12.pendingRoad).toString();
                            break label3;
                        }
                        s2 = (a12.pendingRoad.indexOf(TOWARDS_PATTERN) != 0) ? a12.pendingRoad : a12.pendingRoad.substring(TOWARDS_PATTERN.length());
                    }
                    com.navdy.hud.app.ui.component.vlist.VerticalList.Model a13 = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(R.id.active_trip_maneuver, a12.turnIconNowId, a12.turnIconSoonId, 0, 0, s2, a12.distanceToPendingRoadText);
                    if (i2 != i1 - 1) {
                        int i4 = a10.getDistanceToNextManeuver();
                        if (i4 >= 100) {
                            a13.state = a10.getBoundingBox();
                        } else {
                            a13.state = this.expandBoundingBox(a10.getBoundingBox(), 100 - i4);
                        }
                    } else {
                        a13.state = this.buildArrivalBoundingBox(a5);
                    }
                    a13.noTextAnimation = true;
                    a13.noImageScaleAnimation = true;
                    label1: if (a12.navigationTurn != com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END) {
                        a13.iconSize = maneuverIconSize;
                    } else {
                        a13.title = arriveTitle;
                        label0: {
                            if (s1 == null) {
                                break label0;
                            }
                            if (s == null) {
                                break label0;
                            }
                            a13.title = arriveTitle;
                            a13.subTitle = s1;
                            a13.subTitle2 = s;
                            break label1;
                        }
                        if (s1 == null) {
                            if (s == null) {
                                a13.title = arriveTitle;
                            } else {
                                a13.title = arriveTitle;
                                a13.subTitle = s;
                            }
                        } else {
                            a13.title = arriveTitle;
                            a13.subTitle = s1;
                        }
                    }
                    ((java.util.List)a6).add(a13);
                    i2 = i2 + 1;
                    a9 = a10;
                }
            }
            sLogger.v(new StringBuilder().append("time to build maneuvers:").append(android.os.SystemClock.elapsedRealtime() - j).toString());
        } catch(Throwable a14) {
            sLogger.e("addManeuversToList", a14);
        }
    }
    
    private GeoBoundingBox expandBoundingBox(final GeoBoundingBox geoBoundingBox, final int n) {
        final float n2 = n;
        final float n3 = n;
        try {
            geoBoundingBox.expand(n2, n3);
            return geoBoundingBox;
        }
        catch (Throwable t) {
            ActiveTripMenu.sLogger.e(t);
            return geoBoundingBox;
        }
    }
    
    private void fillDurationVia(final Date date) {
        final String convertDateToEta = HereMapUtil.convertDateToEta(date);
        final String currentVia = HereNavigationManager.getInstance().getCurrentVia();
        if (convertDateToEta != null) {
            final SpannableStringBuilder text = new SpannableStringBuilder();
            text.append((CharSequence)convertDateToEta);
            final int length = text.length();
            text.setSpan(new StyleSpan(1), 0, length, 34);
            text.setSpan(new AbsoluteSizeSpan(ActiveTripMenu.size_22), 0, length, 34);
            if (currentVia != null) {
                text.append((CharSequence)" ");
                final int length2 = convertDateToEta.length();
                text.append((CharSequence)ActiveTripMenu.resources.getString(R.string.via_desc, new Object[] { currentVia }));
                text.setSpan(new AbsoluteSizeSpan(ActiveTripMenu.size_18), length2, text.length(), 34);
            }
            this.activeTripDuration.setText((CharSequence)text);
        }
        else {
            this.activeTripDuration.setText((CharSequence)"");
        }
    }
    
    private int getFirstManeuverIndex() {
        int n = this.cachedList.indexOf(ActiveTripMenu.reportIssue);
        if (n >= 0 && n + 2 < this.cachedList.size()) {
            n += 2;
        }
        else {
            n = this.cachedList.indexOf(ActiveTripMenu.unmuteTbtAudio);
            if (n >= 0 && n + 2 < this.cachedList.size()) {
                n += 2;
            }
            else {
                n = this.cachedList.indexOf(ActiveTripMenu.muteTbtAudio);
                if (n >= 0 && n + 2 < this.cachedList.size()) {
                    n += 2;
                }
                else {
                    n = -1;
                }
            }
        }
        return n;
    }
    
    private boolean isMenuLoaded() {
        return this.activeTripTitleView != null;
    }
    
    private void setSpokenTurnByTurn(final boolean spokenTurnByTurn) {
        HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn = spokenTurnByTurn;
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        if (!currentProfile.isDefaultProfile()) {
            final NavigationPreferences build = new NavigationPreferences.Builder(currentProfile.getNavigationPreferences()).spokenTurnByTurn(spokenTurnByTurn).build();
            final NavigationPreferencesUpdate build2 = new NavigationPreferencesUpdate.Builder().status(RequestStatus.REQUEST_SUCCESS).serial_number(build.serial_number).preferences(build).build();
            this.bus.post(build2);
            this.bus.post(new RemoteEvent(build2));
        }
    }
    
    private void switchToArriveState() {
        if (!TextUtils.equals(this.activeTripDuration.getText(), (CharSequence)ActiveTripMenu.arrivedTitle)) {
            final SpannableStringBuilder text = new SpannableStringBuilder();
            text.append((CharSequence)ActiveTripMenu.arrivedTitle);
            final int length = text.length();
            text.setSpan(new StyleSpan(1), 0, length, 34);
            text.setSpan(new AbsoluteSizeSpan(ActiveTripMenu.size_22), 0, length, 34);
            this.activeTripDuration.setText((CharSequence)text);
            this.activeTripDistance.setText((CharSequence)ActiveTripMenu.arrivedDistance);
            this.activeTripEta.setText((CharSequence)"");
            this.etaProgressDrawable.setGaugeValue(100.0f);
        }
    }
    
    private void update() {
        if (this.isMenuLoaded()) {
            final HereNavigationManager instance = HereNavigationManager.getInstance();
            final String destinationLabel = instance.getDestinationLabel();
            String fullStreetAddress;
            final String s = fullStreetAddress = instance.getFullStreetAddress();
            String text = destinationLabel;
            if (TextUtils.isEmpty((CharSequence)destinationLabel)) {
                text = s;
                if (TextUtils.isEmpty((CharSequence)text)) {
                    text = null;
                    fullStreetAddress = s;
                }
                else {
                    fullStreetAddress = null;
                }
            }
            if (text != null) {
                this.activeTripTitleView.setText((CharSequence)text);
                this.activeTripTitleView.setVisibility(View.VISIBLE);
            }
            else {
                this.activeTripTitleView.setVisibility(GONE);
            }
            if (fullStreetAddress != null) {
                this.activeTripSubTitleView.setText((CharSequence)fullStreetAddress);
                this.activeTripSubTitleView.setVisibility(View.VISIBLE);
                this.activeTripSubTitleView.requestLayout();
            }
            else {
                this.activeTripSubTitleView.setVisibility(GONE);
            }
            if (instance.hasArrived()) {
                this.switchToArriveState();
            }
        }
    }
    
    @Subscribe
    public void arrivalEvent(final MapEvents.ArrivalEvent arrivalEvent) {
        if (this.arrived) {
            ActiveTripMenu.sLogger.v("arrival event");
        }
        else {
            this.arrived = true;
            this.currentSelection = -1;
            this.positionOnFirstManeuver = false;
            this.presenter.loadMenu(this, MenuLevel.REFRESH_CURRENT, 0, 0, true);
        }
    }
    
    VerticalList.Model buildContactModel(final Contact contact, final int n) {
        final ContactImageHelper instance = ContactImageHelper.getInstance();
        VerticalList.Model model;
        if (TextUtils.isEmpty((CharSequence)contact.name)) {
            model = IconsTwoViewHolder.buildModel(R.id.active_trip_contact, R.drawable.icon_user_bg_4, R.drawable.icon_user_numberonly, ActiveTripMenu.resources.getColor(R.color.icon_user_bg_4), -1, contact.formattedNumber, null);
        }
        else {
            final int resourceId = instance.getResourceId(contact.defaultImageIndex);
            final int resourceColor = instance.getResourceColor(contact.defaultImageIndex);
            String s = null;
            if (n == 1) {
                if (contact.numberType != NumberType.OTHER) {
                    s = contact.numberTypeStr;
                }
                else {
                    s = contact.formattedNumber;
                }
            }
            model = IconsTwoViewHolder.buildModel(R.id.active_trip_contact, resourceId, R.drawable.icon_user_grey, resourceColor, -1, ActiveTripMenu.resources.getString(R.string.contact_person, new Object[] { contact.name }), s);
        }
        (model.extras = new HashMap<String, String>()).put("INITIAL", contact.initials);
        return model;
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        Label_0056: {
            if (!this.positionOnFirstManeuver) {
                break Label_0056;
            }
            this.positionOnFirstManeuver = false;
            if (this.cachedList == null) {
                break Label_0056;
            }
            final int n = this.getFirstManeuverIndex();
            if (n <= 0) {
                break Label_0056;
            }
            ActiveTripMenu.sLogger.v("initialSelection-m:" + n);
            return n;
        }
        if (this.currentSelection != -1) {
            ActiveTripMenu.sLogger.v("initialSelection:" + this.currentSelection);
            final int n = this.currentSelection;
            this.currentSelection = -1;
            return n;
        }
        return 1;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        this.getItemsCalled = true;
        this.phoneNumber = null;
        this.contactList = null;
        this.label = null;
        final ArrayList<VerticalList.Model> cachedList = new ArrayList<VerticalList.Model>();
        cachedList.add(ActiveTripMenu.back);
        cachedList.add(ActiveTripMenu.infoModel);
        final HereNavigationManager instance = HereNavigationManager.getInstance();
        final boolean navigationModeOn = instance.isNavigationModeOn();
        if (navigationModeOn) {
            final NavigationRouteRequest currentNavigationRouteRequest = instance.getCurrentNavigationRouteRequest();
            if (currentNavigationRouteRequest != null && currentNavigationRouteRequest.requestDestination != null) {
                final List<PhoneNumber> phoneNumbers = currentNavigationRouteRequest.requestDestination.phoneNumbers;
                if (phoneNumbers != null && phoneNumbers.size() > 0) {
                    final PhoneNumber phoneNumber = phoneNumbers.get(0);
                    final int color = ActiveTripMenu.resources.getColor(R.color.mm_places);
                    if (!TextUtils.isEmpty((CharSequence)currentNavigationRouteRequest.label)) {
                        this.label = currentNavigationRouteRequest.label;
                    }
                    else if (!TextUtils.isEmpty((CharSequence)currentNavigationRouteRequest.streetAddress)) {
                        this.label = currentNavigationRouteRequest.streetAddress;
                    }
                    else {
                        this.label = "";
                    }
                    cachedList.add(IconBkColorViewHolder.buildModel(R.id.menu_call, R.drawable.icon_mm_contacts_2, color, MainMenu.bkColorUnselected, color, ActiveTripMenu.resources.getString(R.string.call_person, new Object[] { this.label }), PhoneUtil.formatPhoneNumber(phoneNumber.number)));
                    this.phoneNumber = phoneNumber;
                }
                else if (currentNavigationRouteRequest.requestDestination.contacts != null && currentNavigationRouteRequest.requestDestination.contacts.size() > 0) {
                    this.contactList = new ArrayList<Contact>(currentNavigationRouteRequest.requestDestination.contacts.size());
                    final Iterator<com.navdy.service.library.events.contacts.Contact> iterator = currentNavigationRouteRequest.requestDestination.contacts.iterator();
                    while (iterator.hasNext()) {
                        this.contactList.add(new Contact(iterator.next()));
                    }
                    cachedList.add(this.buildContactModel(this.contactList.get(0), this.contactList.size()));
                }
            }
            cachedList.add(ActiveTripMenu.endTrip);
            if (!DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
                if (HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn) {
                    ActiveTripMenu.muteTbtAudio.title = ActiveTripMenu.resources.getString(R.string.mute_tbt_2);
                    cachedList.add(ActiveTripMenu.muteTbtAudio);
                }
                else {
                    ActiveTripMenu.unmuteTbtAudio.title = ActiveTripMenu.resources.getString(R.string.unmute_tbt_2);
                    cachedList.add(ActiveTripMenu.unmuteTbtAudio);
                }
            }
        }
        if (navigationModeOn && ReportIssueService.canReportIssue()) {
            cachedList.add(ActiveTripMenu.reportIssue);
        }
        if (navigationModeOn && !HereNavigationManager.getInstance().hasArrived()) {
            this.buildManeuvers(cachedList);
        }
        this.presenter.showScrimCover();
        this.cachedList = cachedList;
        ActiveTripMenu.sLogger.v("getItems:" + cachedList.size());
        int n;
        if (!this.presenter.isMapShown()) {
            this.presenter.showMap();
            n = 1000;
        }
        else {
            this.presenter.enableMapViews();
            n = 100;
        }
        ActiveTripMenu.handler.postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                if (ActiveTripMenu.this.getItemsCalled && ActiveTripMenu.this.presenter.isMapShown()) {
                    ActiveTripMenu.this.presenter.setViewBackgroundColor(0);
                    ActiveTripMenu.this.presenter.cleanMapFluctuator();
                }
            }
        }, (long)n);
        return cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.ACTIVE_TRIP;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return true;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        boolean b = false;
        if (n2 != 1) {
            final VerticalList.Model modelfromPos = this.getModelfromPos(n2);
            if (modelfromPos != null) {
                switch (modelfromPos.id) {
                    case R.id.active_trip_maneuver:
                    case R.id.active_trip_maneuver_title:
                        return b;
                }
            }
            b = true;
        }
        return b;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
        if (model.type == VerticalList.ModelType.SCROLL_CONTENT) {
            final ViewGroup viewGroup = (ViewGroup)((ViewGroup)view).getChildAt(0);
            this.activeTripTitleView = (TextView)viewGroup.findViewById(R.id.active_trip_title);
            this.activeTripSubTitleView = (TextView)viewGroup.findViewById(R.id.active_trip_subtitle);
            this.activeTripDuration = (TextView)viewGroup.findViewById(R.id.active_trip_duration);
            this.activeTripDistance = (TextView)viewGroup.findViewById(R.id.active_trip_distance);
            this.activeTripEta = (TextView)viewGroup.findViewById(R.id.active_trip_eta);
            this.activeTripProgress = viewGroup.findViewById(R.id.active_trip_progress);
            (this.etaProgressDrawable = new ETAProgressDrawable(this.activeTripProgress.getContext())).setMinValue(0.0f);
            this.etaProgressDrawable.setMaxGaugeValue(100.0f);
            this.activeTripProgress.setBackground((Drawable)this.etaProgressDrawable);
            this.update();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                ActiveTripMenu.sLogger.v("registered bus");
            }
            HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
        if (itemSelectionState.id == R.id.active_trip_maneuver) {
            this.maneuverSelected = true;
            final VerticalList.Model modelfromPos = this.getModelfromPos(itemSelectionState.pos);
            if (modelfromPos != null && modelfromPos.id == R.id.active_trip_maneuver && modelfromPos.state instanceof GeoBoundingBox) {
                this.presenter.showBoundingBox((GeoBoundingBox)modelfromPos.state);
            }
            else {
                ActiveTripMenu.sLogger.v("leg - no bbox");
            }
        }
        else if (this.maneuverSelected) {
            this.maneuverSelected = false;
            this.presenter.showBoundingBox(this.presenter.getCurrentRouteBoundingBox());
            ActiveTripMenu.sLogger.v("route bbox");
        }
    }
    
    @Subscribe
    public void onManeuverChangeEvent(final MapEvents.ManeuverEvent maneuverEvent) {
        ActiveTripMenu.sLogger.v("onManeuverChangeEvent:" + maneuverEvent.type);
        switch (maneuverEvent.type) {
            case INTERMEDIATE: {
                final int currentSelection = this.presenter.getCurrentSelection();
                if (currentSelection > 1 && currentSelection >= this.getFirstManeuverIndex()) {
                    this.positionOnFirstManeuver = true;
                }
                this.currentSelection = currentSelection;
                this.presenter.loadMenu(this, MenuLevel.REFRESH_CURRENT, 0, 0, true);
                break;
            }
        }
    }
    
    @Subscribe
    public void onManeuverDisplayEvent(final MapEvents.ManeuverDisplay maneuverDisplay) {
        if (this.isMenuLoaded() && !maneuverDisplay.isEmpty() && maneuverDisplay.isNavigating()) {
            if (HereNavigationManager.getInstance().hasArrived()) {
                if (this.arrived) {
                    ActiveTripMenu.sLogger.v("already arrived");
                }
                else {
                    this.arrived = true;
                    this.currentSelection = -1;
                    this.presenter.loadMenu(this, MenuLevel.REFRESH_CURRENT, 0, 0, true);
                }
            }
            else if (maneuverDisplay.etaDate != null) {
                this.fillDurationVia(maneuverDisplay.etaDate);
                String text;
                if (ActiveTripMenu.timeHelper.getFormat() == DateTimeConfiguration.Clock.CLOCK_24_HOUR) {
                    text = maneuverDisplay.eta;
                }
                else {
                    ActiveTripMenu.timeBuilder.setLength(0);
                    text = ActiveTripMenu.timeHelper.formatTime12Hour(maneuverDisplay.etaDate, ActiveTripMenu.timeBuilder, false) + " " + ActiveTripMenu.timeBuilder.toString();
                }
                if (text != null) {
                    this.activeTripEta.setText((CharSequence)text);
                }
                else {
                    this.activeTripEta.setText((CharSequence)"");
                }
                final float convertToMeters = DistanceConverter.convertToMeters(maneuverDisplay.totalDistance, maneuverDisplay.totalDistanceUnit);
                final float n = convertToMeters - DistanceConverter.convertToMeters(maneuverDisplay.totalDistanceRemaining, maneuverDisplay.totalDistanceRemainingUnit);
                int n2;
                if (convertToMeters > 0.0f && convertToMeters > n) {
                    n2 = (int)(n / convertToMeters * 100.0f);
                }
                else {
                    n2 = 0;
                }
                if (this.etaProgressDrawable != null) {
                    this.etaProgressDrawable.setGaugeValue(n2);
                    this.activeTripProgress.invalidate();
                }
                String s = "";
                switch (maneuverDisplay.totalDistanceRemainingUnit) {
                    case DISTANCE_METERS:
                        s = ActiveTripMenu.metersLabel;
                        break;
                    case DISTANCE_KMS:
                        s = ActiveTripMenu.kiloMetersLabel;
                        break;
                    case DISTANCE_MILES:
                        s = ActiveTripMenu.milesLabel;
                        break;
                    case DISTANCE_FEET:
                        s = ActiveTripMenu.feetLabel;
                        break;
                }
                this.activeTripDistance.setText((CharSequence)(Float.toString(maneuverDisplay.totalDistanceRemaining) + " " + s));
            }
        }
    }
    
    @Subscribe
    public void onRouteChangeEvent(final NavigationSessionRouteChange navigationSessionRouteChange) {
        final int currentSelection = this.presenter.getCurrentSelection();
        ActiveTripMenu.sLogger.v("onRouteChangeEvent reason=" + navigationSessionRouteChange.reason + " curPos=" + currentSelection);
        if (currentSelection > 1) {
            final int firstManeuverIndex = this.getFirstManeuverIndex();
            if (currentSelection >= firstManeuverIndex) {
                this.positionOnFirstManeuver = true;
                ActiveTripMenu.sLogger.v("onRouteChangeEvent showing maneuver curPos=" + currentSelection + " maneuverPos=" + firstManeuverIndex);
            }
            else {
                ActiveTripMenu.sLogger.v("onRouteChangeEvent not showing maneuver curPos=" + currentSelection + " maneuverPos=" + firstManeuverIndex);
            }
        }
        this.currentSelection = currentSelection;
        this.presenter.loadMenu(this, MenuLevel.REFRESH_CURRENT, 0, 0, true);
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
        this.getItemsCalled = false;
        this.presenter.hideScrimCover();
        if (this.presenter.isMapShown()) {
            this.presenter.setViewBackgroundColor(-16777216);
            this.presenter.disableMapViews();
        }
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
            ActiveTripMenu.sLogger.v("unregistered bus");
        }
        this.activeTripTitleView = null;
        this.activeTripSubTitleView = null;
        this.activeTripDuration = null;
        this.activeTripDistance = null;
        this.activeTripEta = null;
        this.activeTripProgress = null;
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        ActiveTripMenu.sLogger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            case R.id.menu_back:
                ActiveTripMenu.sLogger.v("back");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0, false, IconOptionsViewHolder.getOffSetFromPos(((MainMenu)this.parent).getActivityTraySelection()));
                break;
            case R.id.main_menu_options_end_trip:
                ActiveTripMenu.sLogger.v("end trip");
                if (this.presenter.isMapShown()) {
                    this.presenter.setNavEnded();
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                AnalyticsSupport.recordMenuSelection("End_Trip");
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        ActiveTripMenu.this.presenter.close();
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                HereNavigationManager.getInstance().stopAndRemoveAllRoutes();
                            }
                        }, 3);
                    }
                });
                break;
            case R.id.main_menu_options_mute_tbt:
                ActiveTripMenu.sLogger.v("mute tbt");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                AnalyticsSupport.recordOptionSelection("mute_Turn_By_Turn");
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        ActiveTripMenu.this.presenter.close();
                    }
                }, 1000);
                ActiveTripMenu.muteTbtAudio.title = ActiveTripMenu.resources.getString(R.string.navigation_muted);
                this.vscrollComponent.verticalList.unlock(false);
                this.presenter.refreshDataforPos(itemSelectionState.pos, false);
                this.vscrollComponent.verticalList.lock();
                this.setSpokenTurnByTurn(false);
                break;
            case R.id.main_menu_options_unmute_tbt:
                ActiveTripMenu.sLogger.v("unmute tbt");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                AnalyticsSupport.recordOptionSelection("unmute_Turn_By_Turn");
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        ActiveTripMenu.this.presenter.close();
                    }
                }, 1000);
                ActiveTripMenu.unmuteTbtAudio.title = ActiveTripMenu.resources.getString(R.string.navigation_unmuted);
                this.vscrollComponent.verticalList.unlock(false);
                this.presenter.refreshDataforPos(itemSelectionState.pos, false);
                this.vscrollComponent.verticalList.lock();
                this.setSpokenTurnByTurn(true);
                break;
            case R.id.main_menu_options_report_issue:
                ActiveTripMenu.sLogger.v("report-issue");
                AnalyticsSupport.recordMenuSelection("report-issue");
                if (this.reportIssueMenu == null) {
                    this.reportIssueMenu = new ReportIssueMenu(this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.reportIssueMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
            case R.id.menu_call:
                ActiveTripMenu.sLogger.v("call place");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                this.presenter.performSelectionAnimation(new Runnable() {
                    @Override
                    public void run() {
                        ActiveTripMenu.this.presenter.close(new Runnable() {
                            @Override
                            public void run() {
                                if (ActiveTripMenu.this.phoneNumber != null) {
                                    RemoteDeviceManager.getInstance().getCallManager().dial(ActiveTripMenu.this.phoneNumber.number, null, ActiveTripMenu.this.label);
                                }
                            }
                        });
                    }
                });
                break;
            case R.id.active_trip_contact:
                ActiveTripMenu.sLogger.v("contact");
                if (this.contactOptionsMenu == null) {
                    (this.contactOptionsMenu = new ContactOptionsMenu(this.contactList, this.vscrollComponent, this.presenter, this, this.bus)).setScrollModel(true);
                }
                this.presenter.loadMenu(this.contactOptionsMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int n) {
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        final int tripOptionsColor = ActiveTripMenu.tripOptionsColor;
        final String activeTripTitle = ActiveTripMenu.activeTripTitle;
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_badge_active_trip, tripOptionsColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText((CharSequence)activeTripTitle);
    }
    
    @Override
    public void showToolTip() {
    }
}
