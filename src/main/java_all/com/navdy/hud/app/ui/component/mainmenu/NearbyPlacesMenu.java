package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.ArrayList;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.places.NearbyPlaceSearchNotification;
import com.navdy.hud.app.maps.MapsNotification;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.service.library.events.places.PlaceType;
import android.content.Context;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import android.support.v4.content.ContextCompat;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import java.util.List;
import com.squareup.otto.Bus;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.ui.component.vlist.VerticalList;

class NearbyPlacesMenu implements IMenu
{
    private static final VerticalList.Model atm;
    private static final VerticalList.Model back;
    private static final VerticalList.Model coffee;
    private static final VerticalList.Model food;
    private static final VerticalList.Model gas;
    private static final VerticalList.Model groceryStore;
    private static final Logger logger;
    private static final VerticalList.Model parking;
    private static final Resources resources;
    private static final String search;
    private static final int searchColor;
    private int backSelection;
    private int backSelectionId;
    private final Bus bus;
    private List<VerticalList.Model> cachedList;
    private final NotificationManager notificationManager;
    private final IMenu parent;
    private final MainMenuScreen2.Presenter presenter;
    private final VerticalMenuComponent vscrollComponent;
    
    static {
        logger = new Logger(NearbyPlacesMenu.class);
        final Context appContext = HudApplication.getAppContext();
        resources = appContext.getResources();
        searchColor = ContextCompat.getColor(appContext, R.color.mm_search);
        search = NearbyPlacesMenu.resources.getString(R.string.carousel_menu_search_title);
        final String string = NearbyPlacesMenu.resources.getString(R.string.back);
        final int color = ContextCompat.getColor(appContext, R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, color, MainMenu.bkColorUnselected, color, string, null);
        final String string2 = NearbyPlacesMenu.resources.getString(R.string.carousel_search_gas);
        final int color2 = ContextCompat.getColor(appContext, R.color.mm_search_gas);
        gas = IconBkColorViewHolder.buildModel(R.id.search_menu_gas, R.drawable.icon_place_gas, color2, MainMenu.bkColorUnselected, color2, string2, null);
        final String string3 = NearbyPlacesMenu.resources.getString(R.string.carousel_search_parking);
        final int color3 = ContextCompat.getColor(appContext, R.color.mm_search_parking);
        parking = IconBkColorViewHolder.buildModel(R.id.search_menu_parking, R.drawable.icon_place_parking, color3, MainMenu.bkColorUnselected, color3, string3, null);
        final String string4 = NearbyPlacesMenu.resources.getString(R.string.carousel_search_food);
        final int color4 = ContextCompat.getColor(appContext, R.color.mm_search_food);
        food = IconBkColorViewHolder.buildModel(R.id.search_menu_food, R.drawable.icon_place_restaurant, color4, MainMenu.bkColorUnselected, color4, string4, null);
        final String string5 = NearbyPlacesMenu.resources.getString(R.string.carousel_search_grocery_store);
        final int color5 = ContextCompat.getColor(appContext, R.color.mm_search_grocery_store);
        groceryStore = IconBkColorViewHolder.buildModel(R.id.search_menu_grocery_store, R.drawable.icon_place_store, color5, MainMenu.bkColorUnselected, color5, string5, null);
        final String string6 = NearbyPlacesMenu.resources.getString(R.string.carousel_search_coffee);
        final int color6 = ContextCompat.getColor(appContext, R.color.mm_search_coffee);
        coffee = IconBkColorViewHolder.buildModel(R.id.search_menu_coffee, R.drawable.icon_place_coffee, color6, MainMenu.bkColorUnselected, color6, string6, null);
        final String string7 = NearbyPlacesMenu.resources.getString(R.string.carousel_search_atm);
        final int color7 = ContextCompat.getColor(appContext, R.color.mm_search_atm);
        atm = IconBkColorViewHolder.buildModel(R.id.search_menu_atm, R.drawable.icon_place_a_t_m, color7, MainMenu.bkColorUnselected, color7, string7, null);
    }
    
    NearbyPlacesMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.notificationManager = NotificationManager.getInstance();
    }
    
    private void launchSearch(final PlaceType placeType) {
        if (!HereMapsManager.getInstance().isInitialized()) {
            NearbyPlacesMenu.logger.w("Here maps engine not initialized, exit");
            this.presenter.performSelectionAnimation(new Runnable() {
                @Override
                public void run() {
                    NearbyPlacesMenu.this.presenter.close();
                    MapsNotification.showMapsEngineNotInitializedToast();
                }
            });
        }
        else {
            this.presenter.performSelectionAnimation(new Runnable() {
                @Override
                public void run() {
                    NearbyPlacesMenu.this.presenter.close(new Runnable() {
                        @Override
                        public void run() {
                            NearbyPlacesMenu.logger.v("addNotification for quick search");
                            NearbyPlacesMenu.this.notificationManager.addNotification(new NearbyPlaceSearchNotification(placeType));
                        }
                    });
                }
            });
        }
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        return 1;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        List<VerticalList.Model> cachedList;
        if (this.cachedList != null) {
            cachedList = this.cachedList;
        }
        else {
            cachedList = new ArrayList<VerticalList.Model>();
            cachedList.add(NearbyPlacesMenu.back);
            cachedList.add(NearbyPlacesMenu.gas);
            cachedList.add(NearbyPlacesMenu.parking);
            cachedList.add(NearbyPlacesMenu.food);
            cachedList.add(NearbyPlacesMenu.groceryStore);
            cachedList.add(NearbyPlacesMenu.coffee);
            cachedList.add(NearbyPlacesMenu.atm);
            this.cachedList = cachedList;
        }
        return cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.SEARCH;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        NearbyPlacesMenu.logger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            case R.id.menu_back:
                NearbyPlacesMenu.logger.v("back");
                AnalyticsSupport.recordNearbySearchReturnMainMenu();
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                break;
            case R.id.search_menu_gas:
                NearbyPlacesMenu.logger.v("gas");
                this.launchSearch(PlaceType.PLACE_TYPE_GAS);
                break;
            case R.id.search_menu_parking:
                NearbyPlacesMenu.logger.v("parking");
                this.launchSearch(PlaceType.PLACE_TYPE_PARKING);
                break;
            case R.id.search_menu_food:
                NearbyPlacesMenu.logger.v("food");
                this.launchSearch(PlaceType.PLACE_TYPE_RESTAURANT);
                break;
            case R.id.search_menu_grocery_store:
                NearbyPlacesMenu.logger.v("grocery store");
                this.launchSearch(PlaceType.PLACE_TYPE_STORE);
                break;
            case R.id.search_menu_coffee:
                NearbyPlacesMenu.logger.v("coffee");
                this.launchSearch(PlaceType.PLACE_TYPE_COFFEE);
                break;
            case R.id.search_menu_atm:
                NearbyPlacesMenu.logger.v("atm");
                this.launchSearch(PlaceType.PLACE_TYPE_ATM);
                break;
            case R.id.search_menu_hospital:
                NearbyPlacesMenu.logger.v("hospital");
                this.launchSearch(PlaceType.PLACE_TYPE_HOSPITAL);
                break;
        }
        return false;
    }
    
    @Override
    public void setBackSelectionId(final int backSelectionId) {
        this.backSelectionId = backSelectionId;
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_search_2, NearbyPlacesMenu.searchColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText((CharSequence)NearbyPlacesMenu.search);
    }
    
    @Override
    public void showToolTip() {
    }
}
