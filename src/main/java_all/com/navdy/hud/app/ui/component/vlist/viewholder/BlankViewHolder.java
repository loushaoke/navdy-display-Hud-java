package com.navdy.hud.app.ui.component.vlist.viewholder;

import android.widget.TextView;
import android.widget.ImageView;
import android.view.LayoutInflater;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import android.view.ViewGroup;
import com.navdy.service.library.log.Logger;

public class BlankViewHolder extends VerticalViewHolder
{
    private static final Logger sLogger;
    
    static {
        sLogger = VerticalViewHolder.sLogger;
    }
    
    private BlankViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        super(viewGroup, list, handler);
    }
    
    public static VerticalList.Model buildModel() {
        final VerticalList.Model model = new VerticalList.Model();
        model.type = VerticalList.ModelType.BLANK;
        return model;
    }
    
    public static BlankViewHolder buildViewHolder(final ViewGroup viewGroup, final VerticalList list, final Handler handler) {
        return new BlankViewHolder((ViewGroup)LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vlist_blank_item, viewGroup, false), list, handler);
    }
    
    @Override
    public void bind(final VerticalList.Model model, final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void clearAnimation() {
    }
    
    @Override
    public void copyAndPosition(final ImageView imageView, final TextView textView, final TextView textView2, final TextView textView3, final boolean b) {
    }
    
    @Override
    public VerticalList.ModelType getModelType() {
        return VerticalList.ModelType.BLANK;
    }
    
    @Override
    public void select(final VerticalList.Model model, final int n, final int n2) {
    }
    
    @Override
    public void setItemState(final State state, final AnimationType animationType, final int n, final boolean b) {
    }
}
