package com.navdy.hud.app.ui.activity;

import com.navdy.hud.app.event.Wakeup;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.task.TaskManager;
import android.content.res.Resources;
import android.content.IntentFilter;
import android.preference.PreferenceManager;
import android.app.Activity;
import butterknife.ButterKnife;
import android.os.Build;
import com.navdy.hud.app.util.DeviceUtil;
import android.content.res.Configuration;
import mortar.Blueprint;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import android.os.Parcelable;
import android.os.Bundle;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.InitEvents;
import com.navdy.hud.app.ui.component.bluetooth.BluetoothPairing;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.Context;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.manager.PairingManager;
import javax.inject.Inject;
import android.widget.TextView;
import butterknife.InjectView;
import com.navdy.hud.app.view.ContainerView;
import android.content.BroadcastReceiver;

public class MainActivity extends HudBaseActivity
{
    private static final String ACTION_PAIRING_CANCEL = "android.bluetooth.device.action.PAIRING_CANCEL";
    private BroadcastReceiver bluetoothReceiver;
    @InjectView(R.id.container)
    ContainerView container;
    TextView debugText;
    @Inject
    Main.Presenter mainPresenter;
    @Inject
    PairingManager pairingManager;
    @Inject
    PowerManager powerManager;
    
    public MainActivity() {
        this.bluetoothReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                final Bus bus = MainActivity.this.mainPresenter.bus;
                if (action.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
                    final int intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
                    final int intExtra2 = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", Integer.MIN_VALUE);
                    MainActivity.this.logger.d("Bond state change - device:" + bluetoothDevice + " state:" + intExtra + " prevState:" + intExtra2);
                    bus.post(new BluetoothPairing.BondStateChange(bluetoothDevice, intExtra2, intExtra));
                }
                else if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                    final int intExtra3 = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
                    if (intExtra3 == 12 || intExtra3 == 10) {
                        bus.post(new InitEvents.BluetoothStateChanged(intExtra3 == 12));
                    }
                }
                else if (action.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
                    this.abortBroadcast();
                    if (DialManager.getInstance().isDialDevice(bluetoothDevice)) {
                        MainActivity.this.logger.w("Pairing request from dial device, ignore [" + bluetoothDevice.getName() + "]");
                    }
                    else {
                        final int intExtra4 = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_VARIANT", Integer.MIN_VALUE);
                        final int intExtra5 = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY", Integer.MIN_VALUE);
                        MainActivity.this.logger.d("Showing pairing request from " + bluetoothDevice + " of type:" + intExtra4 + " with pin:" + intExtra5);
                        final Bundle bundle = new Bundle();
                        bundle.putParcelable("device", (Parcelable)bluetoothDevice);
                        bundle.putInt("variant", intExtra4);
                        bundle.putInt("pin", intExtra5);
                        final boolean autoPairing = MainActivity.this.pairingManager.isAutoPairing();
                        final Bundle resultExtras = this.getResultExtras(false);
                        boolean boolean1 = autoPairing;
                        if (!autoPairing) {
                            boolean1 = autoPairing;
                            if (resultExtras != null) {
                                boolean1 = resultExtras.getBoolean("auto", false);
                            }
                        }
                        bundle.putBoolean("auto", boolean1);
                        BluetoothPairing.showBluetoothPairingToast(bundle);
                    }
                }
                else if (action.equals("android.bluetooth.device.action.PAIRING_CANCEL")) {
                    this.abortBroadcast();
                    bus.post(new BluetoothPairing.PairingCancelled(bluetoothDevice));
                }
            }
        };
    }
    
    @Override
    protected Blueprint getBlueprint() {
        return new Main();
    }
    
    @Override
    public void onConfigurationChanged(final Configuration configuration) {
        this.logger.v("onConfigurationChanged:" + configuration);
        super.onConfigurationChanged(configuration);
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (DeviceUtil.isNavdyDevice()) {
            this.setContentView(R.layout.main_view_lyt);
        }
        else {
            this.setContentView(R.layout.activity_fullscreen);
            this.debugText = (TextView)this.findViewById(R.id.debug_text);
            final Resources resources = this.getResources();
            final int n = (int)resources.getDimension(R.dimen.dashboard_width);
            this.debugText.setText((CharSequence)("Device:  " + Build.DEVICE + ":" + Build.MODEL + " width = " + n + " height = " + (int)resources.getDimension(R.dimen.dashboard_height) + " scale factor = " + n / 640));
        }
        ButterKnife.inject(this);
        PreferenceManager.setDefaultValues((Context)this, "HUD", 0, R.xml.developer_preferences, false);
        this.makeImmersive(!this.powerManager.inQuietMode());
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
        intentFilter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        intentFilter.addAction("android.bluetooth.device.action.PAIRING_CANCEL");
        this.registerReceiver(this.bluetoothReceiver, intentFilter);
        this.mainPresenter.bus.register(this);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(this.bluetoothReceiver);
    }
    
    @Subscribe
    public void onInitEvent(final InitEvents.InitPhase initPhase) {
        if (initPhase.phase == InitEvents.Phase.POST_START) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    ((HudApplication)MainActivity.this.getApplication()).initHudApp();
                }
            }, 1);
        }
    }
    
    @Subscribe
    public void onWakeup(final Wakeup wakeup) {
        this.logger.v("enableNormalMode(): FLAG_KEEP_SCREEN_ON set");
        this.getWindow().addFlags(128);
    }
    
    public void onWindowFocusChanged(final boolean b) {
        super.onWindowFocusChanged(b);
        if (b) {
            this.makeImmersive(!this.powerManager.inQuietMode());
        }
    }
}
