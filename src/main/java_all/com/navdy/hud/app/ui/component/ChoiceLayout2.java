package com.navdy.hud.app.ui.component;

import android.text.TextUtils;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable;
import com.navdy.hud.app.ui.component.vmenu.VerticalAnimationUtils;
import java.util.Iterator;
import android.content.res.Resources;
import com.navdy.hud.app.audio.SoundUtils;
import android.support.constraint.ConstraintSet;
import android.view.ViewGroup;
import android.graphics.Shader;
import android.view.ViewGroup;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import android.view.View;
import android.content.res.TypedArray;
import com.navdy.hud.app.R;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import java.util.List;
import com.navdy.service.library.log.Logger;
import android.support.constraint.ConstraintLayout;

public class ChoiceLayout2 extends ConstraintLayout
{
    private static final float DEFAULT_SCALE = 0.5f;
    private static final String EMPTY = "";
    private static int defaultHaloDuration;
    private static int defaultHaloEndRadius;
    private static int defaultHaloMiddleRadius;
    private static int defaultHaloStartDelay;
    private static int defaultHaloStartRadius;
    private static int defaultIconHaloSize;
    private static int defaultIconSize;
    private static int defaultItemPadding;
    private static int defaultLabelSize;
    private static int defaultTopPadding;
    private static final Logger sLogger;
    private List<Choice> choices;
    private boolean clickMode;
    private int currentSelection;
    private int haloDuration;
    private int haloEndRadius;
    private int haloMiddleRadius;
    private int haloStartDelay;
    private int haloStartRadius;
    private int iconHaloSize;
    private int iconSize;
    private int itemPadding;
    private int itemTopPadding;
    private int labelSize;
    private TextView labelView;
    private IListener listener;
    private float scale;
    private Selection selection;
    private List<ViewContainer> viewContainers;
    
    static {
        sLogger = new Logger(ChoiceLayout2.class);
        ChoiceLayout2.defaultItemPadding = -1;
    }
    
    public ChoiceLayout2(final Context context) {
        this(context, null);
    }
    
    public ChoiceLayout2(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ChoiceLayout2(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.listener = null;
        this.currentSelection = -1;
        this.viewContainers = new ArrayList<ViewContainer>();
        this.selection = new Selection();
        initDefaults(context);
        LayoutInflater.from(context).inflate(R.layout.choices2_lyt, (ViewGroup)this, true);
        this.labelView = (TextView)this.findViewById(R.id.label);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ChoiceLayout2, n, 0);
        if (obtainStyledAttributes != null) {
            this.itemPadding = obtainStyledAttributes.getDimensionPixelSize(1, ChoiceLayout2.defaultItemPadding);
            this.itemTopPadding = obtainStyledAttributes.getDimensionPixelSize(0, ChoiceLayout2.defaultTopPadding);
            this.labelSize = obtainStyledAttributes.getDimensionPixelSize(2, ChoiceLayout2.defaultLabelSize);
            this.iconSize = obtainStyledAttributes.getDimensionPixelSize(3, ChoiceLayout2.defaultIconSize);
            this.iconHaloSize = obtainStyledAttributes.getDimensionPixelSize(4, ChoiceLayout2.defaultIconHaloSize);
            this.haloStartRadius = obtainStyledAttributes.getDimensionPixelSize(5, ChoiceLayout2.defaultHaloStartRadius);
            this.haloEndRadius = obtainStyledAttributes.getDimensionPixelSize(6, ChoiceLayout2.defaultHaloEndRadius);
            this.haloMiddleRadius = obtainStyledAttributes.getDimensionPixelSize(7, ChoiceLayout2.defaultHaloMiddleRadius);
            this.haloStartDelay = obtainStyledAttributes.getInteger(8, ChoiceLayout2.defaultHaloStartDelay);
            this.haloDuration = obtainStyledAttributes.getInteger(9, ChoiceLayout2.defaultHaloDuration);
            obtainStyledAttributes.recycle();
        }
    }
    
    private void buildChoices() {
        this.removeViewContainers();
        if (this.choices != null) {
            final LayoutInflater from = LayoutInflater.from(this.getContext());
            for (int size = this.choices.size(), i = 0; i < size; ++i) {
                final Choice choice = this.choices.get(i);
                final ViewContainer viewContainer = new ViewContainer();
                (viewContainer.parent = from.inflate(R.layout.choices2_lyt_item, (ViewGroup)this, false)).setId(View.generateViewId());
                viewContainer.iconContainer = (ViewGroup)viewContainer.parent.findViewById(R.id.iconContainer);
                (viewContainer.crossFadeImageView = (CrossFadeImageView)viewContainer.parent.findViewById(R.id.crossFadeImageView)).inject(CrossFadeImageView.Mode.SMALL);
                viewContainer.big = (IconColorImageView)viewContainer.parent.findViewById(R.id.big);
                viewContainer.small = (IconColorImageView)viewContainer.parent.findViewById(R.id.small);
                viewContainer.haloView = (HaloView)viewContainer.parent.findViewById(R.id.halo);
                this.viewContainers.add(viewContainer);
                final LayoutParams layoutParams = new LayoutParams(this.iconHaloSize, this.iconHaloSize);
                if (i != 0) {
                    layoutParams.leftMargin = this.itemPadding;
                }
                final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)viewContainer.iconContainer.getLayoutParams();
                viewGroup$MarginLayoutParams.width = this.iconSize;
                viewGroup$MarginLayoutParams.height = this.iconSize;
                viewContainer.haloView.setStartRadius(this.haloStartRadius);
                viewContainer.haloView.setMiddleRadius(this.haloMiddleRadius);
                viewContainer.haloView.setEndRadius(this.haloEndRadius);
                viewContainer.haloView.setDuration(this.haloDuration);
                viewContainer.haloView.setStartDelay(this.haloStartDelay);
                viewContainer.big.setIcon(choice.resIdSelected, choice.selectedColor, null, this.scale);
                viewContainer.small.setIcon(choice.resIdUnselected, choice.unselectedColor, null, this.scale);
                viewContainer.haloView.setVisibility(INVISIBLE);
                viewContainer.haloView.setStrokeColor(choice.fluctuatorColor);
                this.addView(viewContainer.parent, (ViewGroup.LayoutParams)layoutParams);
            }
            this.labelView.setTextSize((float)this.labelSize);
            final ConstraintSet set = new ConstraintSet();
            set.clone(this);
            final int size2 = this.viewContainers.size();
            final int id = this.getId();
            final int id2 = this.labelView.getId();
            for (int j = 0; j < size2; ++j) {
                final int id3 = this.viewContainers.get(j).parent.getId();
                if (j == 0) {
                    set.setHorizontalChainStyle(id3, 2);
                    set.connect(id3, 6, id, 6, 0);
                }
                if (j == size2 - 1) {
                    set.connect(id3, 7, id, 7, 0);
                    if (size2 > 1) {
                        set.connect(id3, 1, this.viewContainers.get(j - 1).parent.getId(), 2, 0);
                    }
                }
                else if (size2 > 1) {
                    set.connect(id3, 2, this.viewContainers.get(j + 1).parent.getId(), 1, 0);
                    if (j != 0) {
                        set.connect(id3, 1, this.viewContainers.get(j - 1).parent.getId(), 2, 0);
                    }
                }
                set.connect(id3, 3, id2, 4, this.itemTopPadding);
            }
            set.applyTo(this);
            this.invalidate();
            this.requestLayout();
        }
    }
    
    private void callClickListener(final int pos) {
        if (this.listener != null) {
            SoundUtils.playSound(SoundUtils.Sound.MENU_SELECT);
            this.selection.id = this.choices.get(pos).id;
            this.selection.pos = pos;
            this.listener.executeItem(this.selection);
        }
    }
    
    private void callSelectListener(final int pos) {
        if (this.listener != null) {
            SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
            this.selection.id = this.choices.get(pos).id;
            this.selection.pos = pos;
            this.listener.itemSelected(this.selection);
        }
    }
    
    private int getItemCount() {
        int size;
        if (this.choices != null) {
            size = this.choices.size();
        }
        else {
            size = 0;
        }
        return size;
    }
    
    private static void initDefaults(final Context context) {
        if (ChoiceLayout2.defaultItemPadding == -1) {
            final Resources resources = context.getResources();
            ChoiceLayout2.defaultItemPadding = resources.getDimensionPixelSize(R.dimen.choices2_lyt_item_padding);
            ChoiceLayout2.defaultTopPadding = resources.getDimensionPixelSize(R.dimen.choices2_lyt_item_top_padding);
            ChoiceLayout2.defaultLabelSize = resources.getDimensionPixelSize(R.dimen.choices2_lyt_text_size);
            ChoiceLayout2.defaultIconSize = resources.getDimensionPixelSize(R.dimen.choices2_lyt_icon_size);
            ChoiceLayout2.defaultIconHaloSize = resources.getDimensionPixelSize(R.dimen.choices2_lyt_icon_halo_size);
            ChoiceLayout2.defaultHaloStartRadius = resources.getDimensionPixelSize(R.dimen.choices2_lyt_halo_start_radius);
            ChoiceLayout2.defaultHaloEndRadius = resources.getDimensionPixelSize(R.dimen.choices2_lyt_halo_end_radius);
            ChoiceLayout2.defaultHaloMiddleRadius = resources.getDimensionPixelSize(R.dimen.choices2_lyt_halo_middle_radius);
            ChoiceLayout2.defaultHaloStartDelay = resources.getInteger(R.integer.choices2_lyt_halo_start_delay);
            ChoiceLayout2.defaultHaloDuration = resources.getInteger(R.integer.choices2_lyt_halo_duration);
        }
    }
    
    private boolean isItemInBounds(final int n) {
        return this.choices != null && n >= 0 && n < this.choices.size();
    }
    
    private void removeViewContainers() {
        final Iterator<ViewContainer> iterator = this.viewContainers.iterator();
        while (iterator.hasNext()) {
            this.removeView(iterator.next().parent);
        }
        this.viewContainers.clear();
    }
    
    private void resetClickMode() {
        this.clickMode = false;
    }
    
    private void startFluctuator(final int n, final boolean b) {
        ChoiceLayout2.sLogger.v("startFluctuator:" + n + " current=" + this.currentSelection);
        if (this.isItemInBounds(n)) {
            final ViewContainer viewContainer = this.viewContainers.get(n);
            final Choice choice = this.choices.get(n);
            if (b) {
                viewContainer.crossFadeImageView.setMode(CrossFadeImageView.Mode.BIG);
            }
            viewContainer.haloView.setVisibility(View.VISIBLE);
            viewContainer.haloView.start();
            this.labelView.setText((CharSequence)choice.label);
        }
    }
    
    private void stopFluctuator(final int n, final boolean b) {
        ChoiceLayout2.sLogger.v("stopFluctuator:" + n + " current=" + this.currentSelection);
        if (this.isItemInBounds(n)) {
            final Choice choice = this.choices.get(n);
            final ViewContainer viewContainer = this.viewContainers.get(n);
            if (b) {
                viewContainer.crossFadeImageView.setMode(CrossFadeImageView.Mode.SMALL);
            }
            viewContainer.haloView.setVisibility(GONE);
            viewContainer.haloView.stop();
        }
    }
    
    public void clear() {
        ChoiceLayout2.sLogger.v("clear");
        this.stopFluctuator(this.currentSelection, false);
        this.resetClickMode();
    }
    
    public void executeSelectedItem() {
        if (this.clickMode) {
            ChoiceLayout2.sLogger.v("select: click mode on");
        }
        else if (this.isItemInBounds(this.currentSelection)) {
            this.clickMode = true;
            this.stopFluctuator(this.currentSelection, false);
            VerticalAnimationUtils.performClick((View)this.viewContainers.get(this.currentSelection).iconContainer, 50, new Runnable() {
                @Override
                public void run() {
                    ChoiceLayout2.this.callClickListener(ChoiceLayout2.this.currentSelection);
                }
            });
        }
    }
    
    public List<Choice> getChoices() {
        return this.choices;
    }
    
    public Choice getCurrentSelectedChoice() {
        final Selection currentSelection = this.getCurrentSelection();
        Choice choice;
        if (currentSelection != null) {
            choice = this.choices.get(currentSelection.pos);
        }
        else {
            choice = null;
        }
        return choice;
    }
    
    public Selection getCurrentSelection() {
        Selection selection;
        if (this.isItemInBounds(this.currentSelection)) {
            this.selection.id = this.choices.get(this.currentSelection).id;
            this.selection.pos = this.currentSelection;
            selection = this.selection;
        }
        else {
            selection = null;
        }
        return selection;
    }
    
    public boolean moveSelectionLeft() {
        boolean b = false;
        if (this.clickMode) {
            ChoiceLayout2.sLogger.v("left: click mode on");
        }
        else if (this.currentSelection != 0) {
            --this.currentSelection;
            this.stopFluctuator(this.currentSelection + 1, true);
            this.startFluctuator(this.currentSelection, true);
            this.callSelectListener(this.currentSelection);
            b = true;
        }
        return b;
    }
    
    public boolean moveSelectionRight() {
        boolean b = false;
        if (this.clickMode) {
            ChoiceLayout2.sLogger.v("right: click mode on");
        }
        else if (this.currentSelection != this.choices.size() - 1) {
            ++this.currentSelection;
            this.stopFluctuator(this.currentSelection - 1, true);
            this.startFluctuator(this.currentSelection, true);
            this.callSelectListener(this.currentSelection);
            b = true;
        }
        return b;
    }
    
    public void setChoices(final List<Choice> list, final int n, final IListener listener) {
        this.setChoices(list, n, listener, 0.5f);
    }
    
    public void setChoices(final List<Choice> choices, final int currentSelection, final IListener listener, final float scale) {
        this.resetClickMode();
        this.choices = choices;
        this.listener = listener;
        this.scale = scale;
        if (choices != null && choices.size() > 0) {
            if (currentSelection >= 0 && currentSelection < choices.size()) {
                this.currentSelection = currentSelection;
            }
            else {
                this.currentSelection = 0;
            }
            ChoiceLayout2.sLogger.v("setChoices:" + choices.size() + " sel:" + this.currentSelection);
            this.buildChoices();
            final Iterator<ViewContainer> iterator = this.viewContainers.iterator();
            while (iterator.hasNext()) {
                iterator.next().crossFadeImageView.setMode(CrossFadeImageView.Mode.SMALL);
            }
            this.startFluctuator(this.currentSelection, true);
        }
        else {
            ChoiceLayout2.sLogger.v("setChoices:clear");
            this.currentSelection = -1;
            this.removeViewContainers();
            this.labelView.setText((CharSequence)"");
            this.invalidate();
            this.requestLayout();
        }
    }
    
    public static class Choice implements Parcelable
    {
        public static final Parcelable.Creator<Choice> CREATOR;
        private final int fluctuatorColor;
        private final int id;
        private final String label;
        private final int resIdSelected;
        private final int resIdUnselected;
        private final int selectedColor;
        private final int unselectedColor;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<Choice>() {
                public Choice createFromParcel(final Parcel parcel) {
                    return new Choice(parcel);
                }
                
                public Choice[] newArray(final int n) {
                    return new Choice[n];
                }
            };
        }
        
        public Choice(final int id, final int resIdSelected, final int selectedColor, final int resIdUnselected, final int unselectedColor, final String label, final int fluctuatorColor) {
            this.id = id;
            this.resIdSelected = resIdSelected;
            this.selectedColor = selectedColor;
            this.resIdUnselected = resIdUnselected;
            this.unselectedColor = unselectedColor;
            this.label = label;
            this.fluctuatorColor = fluctuatorColor;
        }
        
        public Choice(final Parcel parcel) {
            this.id = parcel.readInt();
            this.resIdSelected = parcel.readInt();
            this.selectedColor = parcel.readInt();
            this.resIdUnselected = parcel.readInt();
            this.unselectedColor = parcel.readInt();
            final String string = parcel.readString();
            if (TextUtils.isEmpty((CharSequence)string)) {
                this.label = null;
            }
            else {
                this.label = string;
            }
            this.fluctuatorColor = parcel.readInt();
        }
        
        public int describeContents() {
            return 0;
        }
        
        public int getId() {
            return this.id;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeInt(this.id);
            parcel.writeInt(this.resIdSelected);
            parcel.writeInt(this.selectedColor);
            parcel.writeInt(this.resIdUnselected);
            parcel.writeInt(this.unselectedColor);
            String label;
            if (this.label != null) {
                label = this.label;
            }
            else {
                label = "";
            }
            parcel.writeString(label);
            parcel.writeInt(this.fluctuatorColor);
        }
    }
    
    public interface IListener
    {
        void executeItem(final Selection p0);
        
        void itemSelected(final Selection p0);
    }
    
    public static class Selection
    {
        public int id;
        public int pos;
    }
    
    private static class ViewContainer
    {
        IconColorImageView big;
        CrossFadeImageView crossFadeImageView;
        HaloView haloView;
        ViewGroup iconContainer;
        View parent;
        IconColorImageView small;
    }
}
