package com.navdy.hud.app.ui.component;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import org.jetbrains.annotations.Nullable;
import android.graphics.Canvas;
import kotlin.TypeCastException;
import android.animation.ValueAnimator;
import android.view.View;
import android.util.AttributeSet;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import java.util.HashMap;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0012\u0010\u0018\u001a\u00020\u00152\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001c\u001a\u00020\tH\u0016R\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\fX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000e\"\u0004\b\u0013\u0010\u0010¨\u0006\u001d" }, d2 = { "Lcom/navdy/hud/app/ui/component/SwitchHaloView;", "Lcom/navdy/hud/app/ui/component/HaloView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "maxScalingFactorX", "", "getMaxScalingFactorX", "()F", "setMaxScalingFactorX", "(F)V", "maxScalingFactorY", "getMaxScalingFactorY", "setMaxScalingFactorY", "onAnimationUpdateInternal", "", "animation", "Landroid/animation/ValueAnimator;", "onDrawInternal", "canvas", "Landroid/graphics/Canvas;", "setStrokeColor", "color", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class SwitchHaloView extends HaloView
{
    private HashMap _$_findViewCache;
    private float maxScalingFactorX;
    private float maxScalingFactorY;
    
    public SwitchHaloView(@NotNull final Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }
    
    public SwitchHaloView(@NotNull final Context context, @NotNull final AttributeSet set) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        super(context, set);
    }
    
    public SwitchHaloView(@NotNull final Context context, @NotNull final AttributeSet set, final int n) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        super(context, set, n);
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = this._$_findViewCache.get(n)) == null) {
            viewById = this.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    public final float getMaxScalingFactorX() {
        return this.maxScalingFactorX;
    }
    
    public final float getMaxScalingFactorY() {
        return this.maxScalingFactorY;
    }
    
    @Override
    public void onAnimationUpdateInternal(@NotNull final ValueAnimator valueAnimator) {
        Intrinsics.checkParameterIsNotNull(valueAnimator, "animation");
        super.onAnimationUpdateInternal(valueAnimator);
        final Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
        final float n = ((float)animatedValue - this.startRadius) / (this.endRadius - this.startRadius) * 2;
        this.maxScalingFactorX = (this.endRadius - this.startRadius) / this.getWidth();
        this.maxScalingFactorY = (this.endRadius - this.startRadius) / this.getHeight();
        this.setScaleX(1 + this.maxScalingFactorX * n);
        this.setScaleY(1 + this.maxScalingFactorY * n);
        this.setPivotX((float)(this.getWidth() / 2));
        this.setPivotY((float)(this.getHeight() / 2));
        this.invalidate();
    }
    
    @Override
    public void onDrawInternal(@Nullable final Canvas canvas) {
    }
    
    public final void setMaxScalingFactorX(final float maxScalingFactorX) {
        this.maxScalingFactorX = maxScalingFactorX;
    }
    
    public final void setMaxScalingFactorY(final float maxScalingFactorY) {
        this.maxScalingFactorY = maxScalingFactorY;
    }
    
    @Override
    public void setStrokeColor(final int n) {
        super.setStrokeColor(n);
        final Drawable background = this.getBackground();
        if (background == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
        }
        ((GradientDrawable)background).setColor(n);
    }
}
