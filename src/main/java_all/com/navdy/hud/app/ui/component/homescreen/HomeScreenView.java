package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import android.os.Bundle;
import butterknife.ButterKnife;
import android.view.LayoutInflater;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.ui.activity.Main;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.obd.ObdManager;
import android.os.SystemClock;
import android.animation.AnimatorSet;
import android.animation.Animator;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.view.MainView;
import android.view.View;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.Animator;
import mortar.Mortar;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.screen.BaseScreen;
import android.util.AttributeSet;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.ui.component.tbt.TbtViewContainer;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.view.SpeedLimitSignView;
import com.navdy.hud.app.ui.framework.IScreenAnimationListener;
import android.animation.AnimatorSet;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.os.Handler;
import android.content.SharedPreferences;
import com.navdy.hud.app.view.DriveScoreGaugePresenter;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.hud.app.maps.NavigationMode;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import butterknife.InjectView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.view.ICustomAnimator;
import android.widget.RelativeLayout;

public class HomeScreenView extends RelativeLayout implements ICustomAnimator, InputManager.IInputHandler, GestureDetector.GestureListener
{
    private static final int MIN_SPEED_LIMIT_VISIBLE_DURATION;
    static final String SYSPROP_ALWAYS_SHOW_SPEED_LIMIT_SIGN = "persist.sys.speedlimit.always";
    static final Logger sLogger;
    @InjectView(R.id.activeEtaContainer)
    EtaView activeEtaContainer;
    private boolean alwaysShowSpeedLimitSign;
    @Inject
    Bus bus;
    private NavigationMode currentNavigationMode;
    @InjectView(R.id.drive_score_events)
    DashboardWidgetView dashboardWidgetView;
    DriveScoreGaugePresenter driveScoreGaugePresenter;
    private SharedPreferences driverPreferences;
    @Inject
    SharedPreferences globalPreferences;
    private Handler handler;
    private Runnable hideSpeedLimitRunnable;
    private boolean isRecalculating;
    private boolean isTopInit;
    private boolean isTopViewAnimationOut;
    private boolean isTopViewAnimationRunning;
    @InjectView(R.id.lane_guidance_map_icon_indicator)
    ImageView laneGuidanceIconIndicator;
    @InjectView(R.id.laneGuidance)
    LaneGuidanceView laneGuidanceView;
    private long lastSpeedLimitShown;
    @InjectView(R.id.mainscreenRightSection)
    HomeScreenRightSectionView mainscreenRightSection;
    @InjectView(R.id.map_container)
    NavigationView mapContainer;
    @InjectView(R.id.map_mask)
    ImageView mapMask;
    @InjectView(R.id.map_view_speed_container)
    ViewGroup mapViewSpeedContainer;
    private HomeScreen.DisplayMode mode;
    @InjectView(R.id.navigation_views_container)
    protected RelativeLayout navigationViewsContainer;
    private NotificationManager notificationManager;
    @InjectView(R.id.openMapRoadInfoContainer)
    OpenRoadView openMapRoadInfoContainer;
    private boolean paused;
    @Inject
    HomeScreen.Presenter presenter;
    @InjectView(R.id.recalcRouteContainer)
    RecalculatingView recalcRouteContainer;
    private AnimatorSet rightScreenAnimator;
    private IScreenAnimationListener screenAnimationListener;
    private boolean showCollapsedNotif;
    @InjectView(R.id.smartDashContainer)
    BaseSmartDashView smartDashContainer;
    private int speedLimit;
    @InjectView(R.id.home_screen_speed_limit_sign)
    SpeedLimitSignView speedLimitSignView;
    private SpeedManager speedManager;
    @InjectView(R.id.speedContainer)
    SpeedView speedView;
    @InjectView(R.id.activeMapRoadInfoContainer)
    TbtViewContainer tbtView;
    @InjectView(R.id.time)
    TimeView timeContainer;
    private Runnable topViewAnimationRunnable;
    private AnimatorSet topViewAnimator;
    private UIStateManager uiStateManager;
    
    static {
        sLogger = new Logger(HomeScreenView.class);
        MIN_SPEED_LIMIT_VISIBLE_DURATION = (int)TimeUnit.SECONDS.toMillis(3L);
    }
    
    public HomeScreenView(final Context context) {
        this(context, null);
    }
    
    public HomeScreenView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public HomeScreenView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.isTopInit = true;
        this.speedManager = SpeedManager.getInstance();
        this.speedLimit = 0;
        this.alwaysShowSpeedLimitSign = false;
        this.topViewAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                HomeScreenView.sLogger.v("out animation");
                HomeScreenView.this.animateTopViewsOut();
            }
        };
        this.handler = new Handler();
        this.screenAnimationListener = new IScreenAnimationListener() {
            @Override
            public void onStart(final BaseScreen baseScreen, final BaseScreen baseScreen2) {
            }
            
            @Override
            public void onStop(final BaseScreen baseScreen, final BaseScreen baseScreen2) {
                if (HomeScreenView.this.showCollapsedNotif && baseScreen != null && baseScreen.getScreen() == Screen.SCREEN_HYBRID_MAP) {
                    HomeScreenView.this.showCollapsedNotif = false;
                    HomeScreenView.sLogger.v("expanding collapse notifi");
                    final NotificationManager instance = NotificationManager.getInstance();
                    if (instance.makeNotificationCurrent(false)) {
                        instance.showNotification();
                    }
                }
            }
        };
        this.mode = HomeScreen.DisplayMode.MAP;
        this.hideSpeedLimitRunnable = new Runnable() {
            @Override
            public void run() {
                HomeScreenView.this.hideSpeedLimit();
            }
        };
        this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
        this.notificationManager = NotificationManager.getInstance();
        this.alwaysShowSpeedLimitSign = SystemProperties.getBoolean("persist.sys.speedlimit.always", false);
        if (!this.isInEditMode()) {
            this.driveScoreGaugePresenter = new DriveScoreGaugePresenter(context, R.layout.drive_score_map_layout, false);
            Mortar.inject(context, this);
        }
    }

    @Override
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        if (this.shouldAnimateTopViews()) {
            switch (customKeyEvent) {
                case LEFT:
                    this.animateTopViewsIn();
                    break;
                case RIGHT:
                    this.animateTopViewsOut();
                    break;
            }
        }
        boolean b = false;
        switch (this.mode) {
            default:
                b = false;
                break;
            case MAP:
                b = this.mapContainer.onKey(customKeyEvent);
                break;
            case SMART_DASH:
                b = this.smartDashContainer.onKey(customKeyEvent);
                break;
        }
        return b;
    }


    private void clearRightSectionAnimation() {
        if (this.rightScreenAnimator != null && this.rightScreenAnimator.isRunning()) {
            this.rightScreenAnimator.removeAllListeners();
            this.rightScreenAnimator.cancel();
            HomeScreenView.sLogger.v("stopped rightscreen animation");
        }
        this.rightScreenAnimator = null;
    }
    
    private Animator getExpandAnimator() {
        final boolean navigationActive = this.isNavigationActive();
        final AnimatorSet set = new AnimatorSet();
        final AnimatorSet.Builder play = set.play((Animator)ValueAnimator.ofFloat(new float[] { 1.0f, 100.0f }));
        if (!this.isTopViewAnimationOut) {
            if (this.timeContainer.getAlpha() == 0.0f) {
                play.with((Animator)ObjectAnimator.ofFloat(this.timeContainer, HomeScreenView.ALPHA, new float[] { 0.0f, 1.0f }));
            }
            if (this.timeContainer.getX() != HomeScreenResourceValues.timeX) {
                play.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.timeContainer, HomeScreenResourceValues.timeX));
            }
        }
        if (this.mode == HomeScreen.DisplayMode.MAP) {
            if (this.activeEtaContainer.getX() != HomeScreenResourceValues.etaX) {
                play.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.activeEtaContainer, HomeScreenResourceValues.etaX));
            }
            if (this.activeEtaContainer.getAlpha() == 0.0f) {
                play.with((Animator)ObjectAnimator.ofFloat(this.activeEtaContainer, HomeScreenView.ALPHA, new float[] { 0.0f, 1.0f }));
            }
        }
        this.mapContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, play);
        if (this.mode == HomeScreen.DisplayMode.MAP) {
            this.mapViewSpeedContainer.setVisibility(View.VISIBLE);
        }
        this.smartDashContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, play);
        if (!navigationActive) {
            this.tbtView.setView(MainView.CustomAnimationMode.EXPAND);
            this.openMapRoadInfoContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, play);
        }
        else {
            this.openMapRoadInfoContainer.setView(MainView.CustomAnimationMode.EXPAND);
            this.tbtView.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, play);
            this.laneGuidanceView.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, play);
        }
        if (this.isRecalculating) {
            play.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.recalcRouteContainer, HomeScreenResourceValues.recalcX));
        }
        else {
            this.recalcRouteContainer.setView(MainView.CustomAnimationMode.EXPAND);
        }
        set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                final boolean navigationActive = HomeScreenView.this.isNavigationActive();
                if (navigationActive && !HomeScreenView.this.isRecalculating) {
                    if (HereNavigationManager.getInstance().hasArrived()) {
                        if (HomeScreenView.this.getDisplayMode() == HomeScreen.DisplayMode.MAP) {
                            HomeScreenView.this.timeContainer.setVisibility(View.VISIBLE);
                            HomeScreenView.this.activeEtaContainer.setVisibility(GONE);
                        }
                    }
                    else if (HomeScreenView.this.getDisplayMode() == HomeScreen.DisplayMode.MAP) {
                        HomeScreenView.this.timeContainer.setVisibility(GONE);
                        HomeScreenView.this.activeEtaContainer.setVisibility(View.VISIBLE);
                    }
                }
                if (!navigationActive && HomeScreenView.this.getDisplayMode() == HomeScreen.DisplayMode.MAP) {
                    HomeScreenView.this.timeContainer.setVisibility(View.VISIBLE);
                    HomeScreenView.this.activeEtaContainer.setVisibility(GONE);
                }
            }
        });
        this.animateTopViewsIn();
        return (Animator)set;
    }
    
    private Animator getShrinkLeftAnimator() {
        final boolean navigationActive = this.isNavigationActive();
        final AnimatorSet set = new AnimatorSet();
        final AnimatorSet.Builder play = set.play((Animator)ValueAnimator.ofFloat(new float[] { 1.0f, 100.0f }));
        this.mapContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, play);
        final AnimatorSet set2 = new AnimatorSet();
        set2.playTogether(new Animator[] { ObjectAnimator.ofFloat(this.mapViewSpeedContainer, View.ALPHA, new float[] { 1.0f, 0.0f }) });
        set2.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                HomeScreenView.this.mapViewSpeedContainer.setVisibility(INVISIBLE);
                HomeScreenView.this.mapViewSpeedContainer.setAlpha(1.0f);
            }
        });
        play.with((Animator)set2);
        this.smartDashContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, play);
        if (!navigationActive) {
            this.tbtView.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
            this.activeEtaContainer.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
            this.openMapRoadInfoContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, play);
            play.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.timeContainer, HomeScreenResourceValues.timeShrinkLeftX));
        }
        else {
            this.openMapRoadInfoContainer.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
            if (this.timeContainer.getVisibility() == 0) {
                play.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.timeContainer, HomeScreenResourceValues.timeShrinkLeftX));
                this.activeEtaContainer.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
            }
            else {
                this.timeContainer.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
                if (this.mode == HomeScreen.DisplayMode.MAP) {
                    play.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.activeEtaContainer, HomeScreenResourceValues.etaShrinkLeftX));
                }
                else {
                    this.activeEtaContainer.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
                }
            }
            this.tbtView.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, play);
            this.laneGuidanceView.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_LEFT, play);
        }
        if (this.isRecalculating) {
            play.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this.recalcRouteContainer, HomeScreenResourceValues.recalcShrinkLeftX));
        }
        else {
            this.recalcRouteContainer.setView(MainView.CustomAnimationMode.SHRINK_LEFT);
        }
        set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                if (HomeScreenView.this.isNavigationActive()) {
                    if (HereNavigationManager.getInstance().hasArrived()) {
                        if (!HomeScreenView.this.isTopViewAnimationOut) {
                            if (HomeScreenView.this.getDisplayMode() == HomeScreen.DisplayMode.MAP) {
                                HomeScreenView.this.timeContainer.setVisibility(View.VISIBLE);
                            }
                            HomeScreenView.this.timeContainer.setAlpha(1.0f);
                        }
                        HomeScreenView.this.activeEtaContainer.setVisibility(GONE);
                    }
                    else if (HomeScreenView.this.mode == HomeScreen.DisplayMode.MAP) {
                        HomeScreenView.this.activeEtaContainer.setVisibility(View.VISIBLE);
                        HomeScreenView.this.activeEtaContainer.setAlpha(1.0f);
                        HomeScreenView.this.timeContainer.setVisibility(GONE);
                    }
                }
            }
        });
        this.animateTopViewsOut();
        return (Animator)set;
    }
    
    private void hideSpeedLimit() {
        this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
        final long n = SystemClock.elapsedRealtime() - this.lastSpeedLimitShown;
        if (n > HomeScreenView.MIN_SPEED_LIMIT_VISIBLE_DURATION) {
            this.speedLimitSignView.setVisibility(GONE);
            this.speedLimitSignView.setSpeedLimit(0);
            this.lastSpeedLimitShown = 0L;
        }
        else {
            this.handler.postDelayed(this.hideSpeedLimitRunnable, n);
        }
    }
    
    private void pauseNavigationViewContainer() {
        HomeScreenView.sLogger.v("::onPause:pauseNavigationViewContainer");
        this.tbtView.onPause();
        this.recalcRouteContainer.onPause();
    }
    
    private void resetTopViewsAnimator() {
        HomeScreenView.sLogger.v("resetTopViewsAnimator");
        if (this.topViewAnimator != null && this.topViewAnimator.isRunning()) {
            this.topViewAnimator.removeAllListeners();
            this.topViewAnimator.cancel();
        }
        this.mapContainer.resetTopViewsAnimator();
        this.speedView.resetTopViewsAnimator();
        this.smartDashContainer.resetTopViewsAnimator();
        if (!this.shouldAnimateTopViews()) {
            this.handler.removeCallbacks(this.topViewAnimationRunnable);
            this.animateTopViewsOut();
        }
        else {
            this.timeContainer.setView(MainView.CustomAnimationMode.EXPAND);
            this.timeContainer.setAlpha(1.0f);
            this.activeEtaContainer.setView(MainView.CustomAnimationMode.EXPAND);
            this.activeEtaContainer.setAlpha(1.0f);
            this.isTopViewAnimationOut = false;
            this.isTopViewAnimationRunning = false;
            this.handler.removeCallbacks(this.topViewAnimationRunnable);
            this.handler.postDelayed(this.topViewAnimationRunnable, 10000L);
        }
    }
    
    private void resumeNavigationViewContainer() {
        HomeScreenView.sLogger.v("::onResume:resumeNavigationViewContainer");
        this.tbtView.onResume();
        this.recalcRouteContainer.onResume();
    }
    
    private void setDisplayMode(final boolean b) {
        HomeScreenView.sLogger.v("setDisplayMode:" + this.mode);
        switch (this.mode) {
            case MAP:
                HomeScreenView.sLogger.v("dash invisible");
                this.setViewsBackground(-16777216);
                this.mapContainer.onResume();
                this.laneGuidanceView.onResume();
                ObdManager.getInstance().enableInstantaneousMode(false);
                this.smartDashContainer.setVisibility(INVISIBLE);
                this.smartDashContainer.onPause();
                if (showDriveScoreEventsOnMap()) {
                    this.dashboardWidgetView.setVisibility(View.VISIBLE);
                    this.driveScoreGaugePresenter.onResume();
                }
                if (!this.isNavigationActive()) {
                    this.timeContainer.setVisibility(View.VISIBLE);
                    this.activeEtaContainer.setVisibility(GONE);
                }
                else {
                    this.timeContainer.setVisibility(GONE);
                    this.activeEtaContainer.setVisibility(View.VISIBLE);
                }
                if (this.uiStateManager.isMainUIShrunk()) {
                    this.mapViewSpeedContainer.setVisibility(GONE);
                }
                else {
                    this.mapViewSpeedContainer.setVisibility(View.VISIBLE);
                }
                this.navigationViewsContainer.setVisibility(View.VISIBLE);
                this.laneGuidanceView.showLastEvent();
                this.resumeNavigationViewContainer();
                break;
            case SMART_DASH:
                HomeScreenView.sLogger.v("dash visible");
                this.setViewsBackground(0);
                if (showDriveScoreEventsOnMap()) {
                    this.dashboardWidgetView.setVisibility(GONE);
                    this.driveScoreGaugePresenter.onPause();
                }
                ObdManager.getInstance().enableInstantaneousMode(true);
                this.smartDashContainer.onResume();
                this.smartDashContainer.setVisibility(View.VISIBLE);
                this.mapViewSpeedContainer.setVisibility(GONE);
                this.activeEtaContainer.setVisibility(GONE);
                this.timeContainer.setVisibility(GONE);
                this.laneGuidanceView.setVisibility(GONE);
                this.laneGuidanceIconIndicator.setVisibility(GONE);
                this.mapContainer.onPause();
                this.laneGuidanceView.onPause();
                this.navigationViewsContainer.setVisibility(View.VISIBLE);
                this.resumeNavigationViewContainer();
                break;
        }
        this.updateRoadInfoView();
        if (b) {
            this.resetTopViewsAnimator();
        }
    }
    
    private void setViewSpeed(final MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case EXPAND:
                this.mapViewSpeedContainer.setX((float)HomeScreenResourceValues.speedX);
                break;
            case SHRINK_LEFT:
                this.mapViewSpeedContainer.setX((float)HomeScreenResourceValues.speedShrinkLeftX);
                break;
        }
    }
    
    private void setViews(final MainView.CustomAnimationMode view) {
        this.activeEtaContainer.setView(view);
        this.openMapRoadInfoContainer.setView(view);
        Enum<MainView.CustomAnimationMode> shrink_MODE = view;
        if (view == MainView.CustomAnimationMode.EXPAND) {
            shrink_MODE = view;
            if (this.isModeVisible()) {
                shrink_MODE = MainView.CustomAnimationMode.SHRINK_MODE;
            }
        }
        this.mapContainer.setView((MainView.CustomAnimationMode)shrink_MODE);
        this.smartDashContainer.setView((MainView.CustomAnimationMode)shrink_MODE);
        this.setViewSpeed(view);
        this.tbtView.setView(view);
        this.recalcRouteContainer.setView(view);
        this.timeContainer.setView(view);
        this.laneGuidanceView.setView(view);
    }
    
    private void setViewsBackground(final int n) {
        HomeScreenView.sLogger.v("setViewsBackground:" + n);
        this.tbtView.setBackgroundColor(n);
        this.recalcRouteContainer.setBackgroundColor(n);
    }
    
    public static boolean showDriveScoreEventsOnMap() {
        return !DeviceUtil.isUserBuild();
    }
    
    private void updateLayoutForMode(final NavigationMode navigationMode) {
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        this.isRecalculating = false;
        final boolean navigationActive = this.isNavigationActive();
        this.mapContainer.updateLayoutForMode(navigationMode);
        this.smartDashContainer.updateLayoutForMode(navigationMode, false);
        this.mainscreenRightSection.updateLayoutForMode(navigationMode, this);
        if (navigationActive) {
            if (this.mode == HomeScreen.DisplayMode.MAP) {
                if (this.uiStateManager.isMainUIShrunk()) {
                    this.mapViewSpeedContainer.setVisibility(GONE);
                }
                else {
                    this.mapViewSpeedContainer.setVisibility(View.VISIBLE);
                }
            }
            else {
                this.mapViewSpeedContainer.setVisibility(GONE);
            }
            this.timeContainer.setVisibility(GONE);
            this.tbtView.clearState();
            this.tbtView.setVisibility(View.VISIBLE);
            this.activeEtaContainer.clearState();
            if (this.mode == HomeScreen.DisplayMode.MAP) {
                this.activeEtaContainer.setVisibility(View.VISIBLE);
                this.timeContainer.setVisibility(GONE);
            }
            this.mapContainer.clearState();
            this.speedView.clearState();
        }
        else {
            this.recalcRouteContainer.hideRecalculating();
            this.activeEtaContainer.clearState();
            this.activeEtaContainer.setVisibility(GONE);
            this.tbtView.clearState();
            this.tbtView.setVisibility(GONE);
            if (this.mode == HomeScreen.DisplayMode.MAP) {
                this.timeContainer.setVisibility(View.VISIBLE);
                if (this.uiStateManager.isMainUIShrunk()) {
                    this.mapViewSpeedContainer.setVisibility(GONE);
                }
                else {
                    this.mapViewSpeedContainer.setVisibility(View.VISIBLE);
                }
            }
            else {
                this.mapViewSpeedContainer.setVisibility(GONE);
            }
            this.mapContainer.clearState();
            this.speedView.clearState();
        }
        this.updateRoadInfoView();
        this.resetTopViewsAnimator();
        int n;
        if (this.isTopInit) {
            n = 30000;
            this.isTopInit = false;
        }
        else {
            n = 10000;
        }
        this.handler.postDelayed(this.topViewAnimationRunnable, (long)n);
    }
    
    private void updateRoadInfoView() {
        if (this.isNavigationActive()) {
            this.openMapRoadInfoContainer.setVisibility(GONE);
        }
        else {
            this.openMapRoadInfoContainer.setRoad();
            this.openMapRoadInfoContainer.setVisibility(View.VISIBLE);
        }
    }
    
    private void updateSpeedLimitSign() {
        if (this.speedLimit > 0) {
            if (this.speedManager.getCurrentSpeed() > this.speedLimit || this.alwaysShowSpeedLimitSign) {
                this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
                this.speedLimitSignView.setSpeedLimitUnit(this.speedManager.getSpeedUnit());
                this.speedLimitSignView.setSpeedLimit(this.speedLimit);
                this.speedLimitSignView.setVisibility(View.VISIBLE);
                this.lastSpeedLimitShown = SystemClock.elapsedRealtime();
            }
            else {
                this.hideSpeedLimit();
            }
        }
        else {
            this.hideSpeedLimit();
        }
    }
    
    @Subscribe
    public void GPSSpeedChangeEvent(final MapEvents.GPSSpeedEvent gpsSpeedEvent) {
        this.updateSpeedLimitSign();
    }
    
    @Subscribe
    public void ObdPidChangeEvent(final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
        switch (obdPidChangeEvent.pid.getId()) {
            case 13:
                this.updateSpeedLimitSign();
                break;
        }
    }
    
    public void animateInModeView() {
        if (this.hasModeView() && !this.isModeVisible()) {
            HomeScreenView.sLogger.v("mode-view animateInModeView");
            this.rightScreenAnimator = new AnimatorSet();
            final AnimatorSet.Builder play = this.rightScreenAnimator.play((Animator)ValueAnimator.ofFloat(new float[] { 0.0f, 0.0f }));
            this.mapContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_MODE, play);
            this.smartDashContainer.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_MODE, play);
            this.mainscreenRightSection.getCustomAnimator(MainView.CustomAnimationMode.SHRINK_MODE, play);
            this.rightScreenAnimator.start();
        }
    }
    
    public void animateOutModeView(final boolean b) {
        if (this.isModeVisible()) {
            HomeScreenView.sLogger.v("mode-view animateOutModeView:" + b);
            this.rightScreenAnimator = new AnimatorSet();
            final AnimatorSet.Builder play = this.rightScreenAnimator.play((Animator)ValueAnimator.ofFloat(new float[] { 0.0f, 0.0f }));
            this.mapContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, play);
            this.smartDashContainer.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, play);
            this.mainscreenRightSection.getCustomAnimator(MainView.CustomAnimationMode.EXPAND, play);
            if (b) {
                this.rightScreenAnimator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                    @Override
                    public void onAnimationEnd(final Animator animator) {
                        HomeScreenView.this.mainscreenRightSection.ejectRightSection();
                    }
                });
            }
            this.rightScreenAnimator.start();
        }
    }
    
    void animateTopViewsIn() {
        if (!this.isTopViewAnimationRunning && this.shouldAnimateTopViews() && this.isTopViewAnimationOut) {
            this.topViewAnimator = new AnimatorSet();
            AnimatorSet.Builder animatorSet$Builder;
            if (this.timeContainer.getVisibility() == 0) {
                animatorSet$Builder = this.topViewAnimator.play((Animator)HomeScreenUtils.getXPositionAnimator((View)this.timeContainer, HomeScreenResourceValues.topViewLeftAnimationIn));
                animatorSet$Builder.with((Animator)ObjectAnimator.ofFloat(this.timeContainer, View.ALPHA, new float[] { 0.0f, 1.0f }));
            }
            else {
                animatorSet$Builder = this.topViewAnimator.play((Animator)ValueAnimator.ofInt(new int[] { 0, 0 }));
            }
            switch (this.mode) {
                case MAP:
                    this.mapContainer.getTopAnimator(animatorSet$Builder, false);
                    this.speedView.getTopAnimator(animatorSet$Builder, false);
                    break;
                case SMART_DASH:
                    this.smartDashContainer.getTopAnimator(animatorSet$Builder, false);
                    break;
            }
            this.isTopViewAnimationRunning = true;
            this.topViewAnimator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                @Override
                public void onAnimationEnd(final Animator animator) {
                    HomeScreenView.this.isTopViewAnimationRunning = false;
                    HomeScreenView.this.isTopViewAnimationOut = false;
                    HomeScreenView.this.handler.removeCallbacks(HomeScreenView.this.topViewAnimationRunnable);
                    HomeScreenView.this.handler.postDelayed(HomeScreenView.this.topViewAnimationRunnable, 10000L);
                }
            });
            this.topViewAnimator.start();
            HomeScreenView.sLogger.v("started in animation");
        }
    }
    
    void animateTopViewsOut() {
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        if (!this.isTopViewAnimationRunning && !this.isTopViewAnimationOut) {
            this.topViewAnimator = new AnimatorSet();
            AnimatorSet.Builder animatorSet$Builder;
            if (this.timeContainer.getVisibility() == 0) {
                animatorSet$Builder = this.topViewAnimator.play((Animator)HomeScreenUtils.getXPositionAnimator((View)this.timeContainer, HomeScreenResourceValues.topViewLeftAnimationOut));
                animatorSet$Builder.with((Animator)ObjectAnimator.ofFloat(this.timeContainer, View.ALPHA, new float[] { 1.0f, 0.0f }));
            }
            else {
                animatorSet$Builder = this.topViewAnimator.play((Animator)ValueAnimator.ofInt(new int[] { 0, 0 }));
            }
            switch (this.mode) {
                case MAP:
                    this.mapContainer.getTopAnimator(animatorSet$Builder, true);
                    this.speedView.getTopAnimator(animatorSet$Builder, true);
                    break;
                case SMART_DASH:
                    this.smartDashContainer.getTopAnimator(animatorSet$Builder, true);
                    break;
            }
            this.isTopViewAnimationRunning = true;
            this.topViewAnimator.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                @Override
                public void onAnimationEnd(final Animator animator) {
                    HomeScreenView.this.isTopViewAnimationRunning = false;
                    HomeScreenView.this.isTopViewAnimationOut = true;
                }
            });
            this.topViewAnimator.start();
            HomeScreenView.sLogger.v("started out animation");
        }
    }
    
    public void ejectRightSection() {
        HomeScreenView.sLogger.v("ejectRightSection");
        this.clearRightSectionAnimation();
        if (this.mainscreenRightSection.isViewVisible()) {
            final Main rootScreen = this.uiStateManager.getRootScreen();
            if (rootScreen.isNotificationExpanding() || rootScreen.isNotificationViewShowing()) {
                HomeScreenView.sLogger.v("ejectRightSection: cannot animate");
                this.mainscreenRightSection.ejectRightSection();
            }
            else {
                this.animateOutModeView(true);
            }
        }
        else {
            this.mainscreenRightSection.ejectRightSection();
        }
    }
    
    public Animator getCustomAnimator(final MainView.CustomAnimationMode customAnimationMode) {
        Animator animator = null;
        switch (customAnimationMode) {
            default:
                animator = null;
                break;
            case SHRINK_LEFT:
                animator = this.getShrinkLeftAnimator();
                break;
            case EXPAND:
                animator = this.getExpandAnimator();
                break;
        }
        return animator;
    }
    
    public HomeScreen.DisplayMode getDisplayMode() {
        return this.mode;
    }
    
    public SharedPreferences getDriverPreferences() {
        return this.driverPreferences;
    }
    
    public EtaView getEtaView() {
        return this.activeEtaContainer;
    }
    
    public ImageView getLaneGuidanceIconIndicator() {
        return this.laneGuidanceIconIndicator;
    }
    
    public NavigationView getNavigationView() {
        return this.mapContainer;
    }
    
    public OpenRoadView getOpenRoadView() {
        return this.openMapRoadInfoContainer;
    }
    
    public RecalculatingView getRecalculatingView() {
        return this.recalcRouteContainer;
    }
    
    public BaseSmartDashView getSmartDashView() {
        return this.smartDashContainer;
    }
    
    public TbtViewContainer getTbtView() {
        return this.tbtView;
    }
    
    public boolean hasModeView() {
        return this.mainscreenRightSection.hasView();
    }
    
    public void injectRightSection(final View view) {
        HomeScreenView.sLogger.v("injectRightSection");
        this.mainscreenRightSection.injectRightSection(view);
        this.clearRightSectionAnimation();
        final Main rootScreen = this.uiStateManager.getRootScreen();
        if (!this.mainscreenRightSection.isViewVisible()) {
            if (rootScreen.isNotificationExpanding() || rootScreen.isNotificationViewShowing()) {
                HomeScreenView.sLogger.v("injectRightSection: cannot animate");
            }
            else {
                this.animateInModeView();
            }
        }
        else {
            HomeScreenView.sLogger.v("injectRightSection: right section view already visible");
        }
    }
    
    public boolean isModeVisible() {
        return this.mainscreenRightSection.isViewVisible();
    }
    
    public boolean isNavigationActive() {
        return this.currentNavigationMode == NavigationMode.MAP_ON_ROUTE || this.currentNavigationMode == NavigationMode.TBT_ON_ROUTE;
    }
    
    public boolean isRecalculating() {
        return this.isRecalculating;
    }
    
    public boolean isShowCollapsedNotification() {
        return this.showCollapsedNotif;
    }
    
    boolean isTopViewAnimationOut() {
        return this.isTopViewAnimationOut;
    }
    
    public InputManager.IInputHandler nextHandler() {
        return InputManager.nextContainingHandler((View)this);
    }
    
    @Subscribe
    public void onArrivalEvent(final MapEvents.ArrivalEvent arrivalEvent) {
        this.activeEtaContainer.setVisibility(GONE);
        if (this.getDisplayMode() == HomeScreen.DisplayMode.MAP) {
            this.timeContainer.setVisibility(View.VISIBLE);
        }
        this.resetTopViewsAnimator();
    }
    
    public void onClick() {
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        this.updateDriverPrefs();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater.from(this.getContext()).inflate(R.layout.screen_home_smartdash, (ViewGroup)this.findViewById(R.id.smart_dash_place_holder), true);
        ButterKnife.inject((View)this);
        this.updateDriverPrefs();
        this.setDisplayMode(false);
        final ImageView mapMask = this.mapMask;
        int imageResource;
        if (showDriveScoreEventsOnMap()) {
            imageResource = R.drawable.map_mask;
        }
        else {
            imageResource = R.drawable.navigation_map_mask;
        }
        mapMask.setImageResource(imageResource);
        this.mapContainer.init(this);
        this.smartDashContainer.init(this);
        this.recalcRouteContainer.init(this);
        this.tbtView.init(this);
        this.openMapRoadInfoContainer.init(this);
        this.activeEtaContainer.init(this);
        this.laneGuidanceView.init(this);
        this.mainscreenRightSection.setX((float)HomeScreenResourceValues.mainScreenRightSectionX);
        this.setViews(this.uiStateManager.getCustomAnimateMode());
        HomeScreenView.sLogger.v("setting navigation mode to MAP");
        this.setMode(NavigationMode.MAP);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (showDriveScoreEventsOnMap()) {
            final Bundle bundle = new Bundle();
            bundle.putInt("EXTRA_GRAVITY", 0);
            bundle.putBoolean("EXTRA_IS_ACTIVE", true);
            this.driveScoreGaugePresenter.setView(this.dashboardWidgetView, bundle);
            this.driveScoreGaugePresenter.setWidgetVisibleToUser(true);
        }
        this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
        this.bus.register(this);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        boolean b = true;
        if (gestureEvent.gesture == Gesture.GESTURE_SWIPE_RIGHT && this.isModeVisible() && this.hasModeView() && (this.rightScreenAnimator == null || !this.rightScreenAnimator.isRunning())) {
            HomeScreenView.sLogger.v("swipe_right animateOutModeView");
            this.animateOutModeView(true);
        }
        else {
            switch (this.mode) {
                default:
                    b = false;
                    break;
                case MAP:
                    b = this.mapContainer.onGesture(gestureEvent);
                    break;
                case SMART_DASH:
                    b = this.smartDashContainer.onGesture(gestureEvent);
                    break;
            }
        }
        return b;
    }
    

    @Subscribe
    public void onMapEvent(final MapEvents.ManeuverDisplay maneuverDisplay) {
        this.speedLimit = Math.round(SpeedManager.convert(maneuverDisplay.currentSpeedLimit, SpeedManager.SpeedUnit.METERS_PER_SECOND, SpeedManager.getInstance().getSpeedUnit()));
        this.updateSpeedLimitSign();
    }
    
    @Subscribe
    public void onNavigationModeChange(final MapEvents.NavigationModeChange navigationModeChange) {
        this.setMode(HereNavigationManager.getInstance().getCurrentNavigationMode());
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.smartDashContainer.onPause();
            this.mapContainer.onPause();
            this.pauseNavigationViewContainer();
            HomeScreenView.sLogger.v("::onPause");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            switch (this.mode) {
                case MAP:
                    this.mapContainer.onResume();
                    this.laneGuidanceView.onResume();
                    this.smartDashContainer.onPause();
                    break;
                case SMART_DASH:
                    this.mapContainer.onPause();
                    this.laneGuidanceView.onPause();
                    this.smartDashContainer.onResume();
                    break;
            }
            this.resumeNavigationViewContainer();
            HomeScreenView.sLogger.v("::onResume");
        }
    }
    
    @Subscribe
    public void onSpeedDataExpired(final SpeedManager.SpeedDataExpired speedDataExpired) {
        this.updateSpeedLimitSign();
    }
    
    @Subscribe
    public void onSpeedUnitChanged(final SpeedManager.SpeedUnitChanged speedUnitChanged) {
        this.updateSpeedLimitSign();
    }
    
    public void onTrackHand(final float n) {
    }
    
    public void resetModeView() {
        if (this.isModeVisible()) {
            HomeScreenView.sLogger.v("mode-view resetModeView");
            this.mapContainer.setView(MainView.CustomAnimationMode.EXPAND);
            this.smartDashContainer.setView(MainView.CustomAnimationMode.EXPAND);
            this.mainscreenRightSection.setView(MainView.CustomAnimationMode.EXPAND);
        }
    }
    
    public void setDisplayMode(final HomeScreen.DisplayMode mode) {
        if (this.mode != mode) {
            this.mode = mode;
            this.setDisplayMode(true);
        }
    }
    
    public void setMode(final NavigationMode currentNavigationMode) {
        if (this.currentNavigationMode != currentNavigationMode || this.mapContainer.isOverviewMapMode()) {
            HomeScreenView.sLogger.v("navigation mode changed to " + currentNavigationMode);
            this.updateLayoutForMode(this.currentNavigationMode = currentNavigationMode);
            this.mapContainer.layoutMap();
            this.bus.post(currentNavigationMode);
        }
    }
    
    public void setRecalculating(final boolean isRecalculating) {
        this.isRecalculating = isRecalculating;
    }
    
    public void setShowCollapsedNotification(final boolean showCollapsedNotif) {
        this.showCollapsedNotif = showCollapsedNotif;
    }
    
    public boolean shouldAnimateTopViews() {
        return this.mode != HomeScreen.DisplayMode.SMART_DASH || this.smartDashContainer.shouldShowTopViews();
    }
    
    void updateDriverPrefs() {
        this.driverPreferences = DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(HudApplication.getAppContext());
    }
}
