package com.navdy.hud.app.ui.component.homescreen;

import android.view.ViewGroup;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import butterknife.ButterKnife;
import android.animation.Animator;
import android.view.View;
import android.animation.AnimatorSet;
import com.navdy.hud.app.view.MainView;
import android.util.AttributeSet;
import android.content.Context;
import butterknife.InjectView;
import android.widget.TextView;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import android.widget.RelativeLayout;

public class OpenRoadView extends RelativeLayout
{
    private Bus bus;
    private HomeScreenView homeScreenView;
    private Logger logger;
    @InjectView(R.id.openMapRoadInfo)
    TextView openMapRoadInfo;
    
    public OpenRoadView(final Context context) {
        this(context, null);
    }
    
    public OpenRoadView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public OpenRoadView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    private void setRoad(final String s) {
        String text = s;
        if (s == null) {
            text = "";
        }
        this.openMapRoadInfo.setText((CharSequence)text);
    }
    
    public void getCustomAnimator(final MainView.CustomAnimationMode customAnimationMode, final AnimatorSet.Builder animatorSet$Builder) {
        switch (customAnimationMode) {
            case SHRINK_LEFT:
                animatorSet$Builder.with((Animator)HomeScreenUtils.getMarginAnimator((View)this.openMapRoadInfo, HomeScreenResourceValues.openMapRoadInfoShrinkLeft_L_Margin, HomeScreenResourceValues.openMapRoadInfoShrinkLeft_R_Margin));
                break;
            case EXPAND:
                animatorSet$Builder.with((Animator)HomeScreenUtils.getMarginAnimator((View)this.openMapRoadInfo, HomeScreenResourceValues.openMapRoadInfoMargin, HomeScreenResourceValues.openMapRoadInfoMargin));
                break;
        }
    }
    
    public void init(final HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.bus = RemoteDeviceManager.getInstance().getBus();
    }
    
    @Subscribe
    public void onManeuverDisplay(final MapEvents.ManeuverDisplay maneuverDisplay) {
        this.setRoad(maneuverDisplay.currentRoad);
    }
    
    public void setRoad() {
        this.setRoad(HereMapUtil.getCurrentRoadName());
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
        final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)this.openMapRoadInfo.getLayoutParams();
        switch (customAnimationMode) {
            case EXPAND:
                viewGroup$MarginLayoutParams.leftMargin = HomeScreenResourceValues.openMapRoadInfoMargin;
                viewGroup$MarginLayoutParams.rightMargin = HomeScreenResourceValues.openMapRoadInfoMargin;
                break;
            case SHRINK_LEFT:
                viewGroup$MarginLayoutParams.leftMargin = HomeScreenResourceValues.openMapRoadInfoShrinkLeft_L_Margin;
                viewGroup$MarginLayoutParams.rightMargin = HomeScreenResourceValues.openMapRoadInfoShrinkLeft_R_Margin;
                break;
        }
    }
}
