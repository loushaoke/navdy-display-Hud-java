package com.navdy.hud.app.ui.framework;

import com.navdy.hud.app.screen.BaseScreen;

public interface IScreenAnimationListener
{
    void onStart(final BaseScreen p0, final BaseScreen p1);
    
    void onStop(final BaseScreen p0, final BaseScreen p1);
}
