package com.navdy.hud.app.ui.framework;

import java.util.Iterator;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.screen.DialManagerScreen;
import com.navdy.hud.app.ui.component.homescreen.SmartDashView;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.hud.app.view.MainView;
import com.navdy.hud.app.view.ToastView;
import com.navdy.hud.app.ui.activity.Main;
import java.util.HashSet;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.component.homescreen.SmartDashViewConstants;
import com.navdy.service.library.events.ui.Screen;
import java.util.EnumSet;

public class UIStateManager
{
    private static final EnumSet<Screen> FULLSCREEN_MODES;
    private static final EnumSet<Screen> MAIN_SCREENS_SET;
    private static final EnumSet<Screen> OVERLAY_MODES;
    private static final EnumSet<Screen> PAUSE_HOME_SCREEN_SET;
    private static final EnumSet<Screen> SIDE_PANEL_MODES;
    private volatile SmartDashViewConstants.Type currentDashboardType;
    private BaseScreen currentScreen;
    private volatile Screen currentViewMode;
    private Screen defaultMainActiveScreen;
    private HomeScreenView homeScreenView;
    private BaseScreen mainActiveScreen;
    private int mainPanelWidth;
    private HashSet<INotificationAnimationListener> notificationListeners;
    private Main rootScreen;
    private HashSet<IScreenAnimationListener> screenListeners;
    private int sidePanelWidth;
    private ToastView toastView;
    
    static {
        SIDE_PANEL_MODES = EnumSet.<Screen>of(Screen.SCREEN_NOTIFICATION);
        OVERLAY_MODES = EnumSet.<Screen>of(Screen.SCREEN_MENU, Screen.SCREEN_MAIN_MENU, Screen.SCREEN_RECENT_CALLS, Screen.SCREEN_RECOMMENDED_PLACES, Screen.SCREEN_FAVORITE_PLACES, Screen.SCREEN_OTA_CONFIRMATION, Screen.SCREEN_BRIGHTNESS, Screen.SCREEN_FAVORITE_CONTACTS, Screen.SCREEN_SHUTDOWN_CONFIRMATION, Screen.SCREEN_SETTINGS, Screen.SCREEN_AUTO_BRIGHTNESS, Screen.SCREEN_FACTORY_RESET, Screen.SCREEN_REPORT_ISSUE, Screen.SCREEN_TOAST, Screen.SCREEN_FORCE_UPDATE, Screen.SCREEN_GESTURE_LEARNING, Screen.SCREEN_DESTINATION_PICKER, Screen.SCREEN_MUSIC_DETAILS);
        FULLSCREEN_MODES = EnumSet.<Screen>of(Screen.SCREEN_FIRST_LAUNCH, Screen.SCREEN_HOME, Screen.SCREEN_WELCOME, Screen.SCREEN_DIAL_PAIRING);
        PAUSE_HOME_SCREEN_SET = EnumSet.<Screen>of(Screen.SCREEN_MENU, Screen.SCREEN_RECENT_CALLS, Screen.SCREEN_RECOMMENDED_PLACES, Screen.SCREEN_FAVORITE_PLACES, Screen.SCREEN_OTA_CONFIRMATION, Screen.SCREEN_FAVORITE_CONTACTS, Screen.SCREEN_SHUTDOWN_CONFIRMATION, Screen.SCREEN_SETTINGS, Screen.SCREEN_AUTO_BRIGHTNESS, Screen.SCREEN_FACTORY_RESET, Screen.SCREEN_REPORT_ISSUE, Screen.SCREEN_FORCE_UPDATE, Screen.SCREEN_GESTURE_LEARNING, Screen.SCREEN_MUSIC_DETAILS);
        MAIN_SCREENS_SET = EnumSet.<Screen>of(Screen.SCREEN_HOME);
    }
    
    public UIStateManager() {
        this.defaultMainActiveScreen = Screen.SCREEN_HOME;
        this.screenListeners = new HashSet<IScreenAnimationListener>();
        this.notificationListeners = new HashSet<INotificationAnimationListener>();
    }
    
    private void checkListener(final Object o) {
        if (o == null) {
            throw new IllegalArgumentException();
        }
    }
    
    public static boolean isFullscreenMode(final Screen screen) {
        return UIStateManager.FULLSCREEN_MODES.contains(screen);
    }
    
    public static boolean isOverlayMode(final Screen screen) {
        return UIStateManager.OVERLAY_MODES.contains(screen);
    }
    
    public static boolean isPauseHomescreen(final Screen screen) {
        return UIStateManager.PAUSE_HOME_SCREEN_SET.contains(screen);
    }
    
    public static boolean isSidePanelMode(final Screen screen) {
        return UIStateManager.SIDE_PANEL_MODES.contains(screen);
    }
    
    public void addNotificationAnimationListener(final INotificationAnimationListener notificationAnimationListener) {
        final HashSet<INotificationAnimationListener> notificationListeners = this.notificationListeners;
        synchronized (notificationListeners) {
            this.checkListener(notificationAnimationListener);
            this.notificationListeners.add(notificationAnimationListener);
        }
    }
    
    public void addScreenAnimationListener(final IScreenAnimationListener screenAnimationListener) {
        this.checkListener(screenAnimationListener);
        this.screenListeners.add(screenAnimationListener);
    }
    
    public void enableNotificationColor(final boolean b) {
        if (this.rootScreen != null) {
            this.rootScreen.enableNotificationColor(b);
        }
    }
    
    public void enableSystemTray(final boolean b) {
        if (this.rootScreen != null) {
            this.rootScreen.enableSystemTray(b);
        }
    }
    
    public SmartDashViewConstants.Type getCurrentDashboardType() {
        return this.currentDashboardType;
    }
    
    public BaseScreen getCurrentScreen() {
        return this.currentScreen;
    }
    
    public Screen getCurrentViewMode() {
        return this.currentViewMode;
    }
    
    public MainView.CustomAnimationMode getCustomAnimateMode() {
        Enum<MainView.CustomAnimationMode> enum1;
        final MainView.CustomAnimationMode customAnimationMode = (MainView.CustomAnimationMode)(enum1 = MainView.CustomAnimationMode.EXPAND);
        Label_0039: {
            if (this.isMainUIShrunk()) {
                if (!this.rootScreen.isNotificationViewShowing()) {
                    enum1 = customAnimationMode;
                    if (!this.rootScreen.isNotificationExpanding()) {
                        break Label_0039;
                    }
                }
                enum1 = MainView.CustomAnimationMode.SHRINK_LEFT;
            }
        }
        Enum<MainView.CustomAnimationMode> shrink_MODE;
        if ((shrink_MODE = enum1) != null) {
            shrink_MODE = enum1;
            if (this.homeScreenView != null && (shrink_MODE = enum1) == MainView.CustomAnimationMode.EXPAND) {
                shrink_MODE = enum1;
                if (this.homeScreenView.isModeVisible()) {
                    shrink_MODE = MainView.CustomAnimationMode.SHRINK_MODE;
                }
            }
        }
        return (MainView.CustomAnimationMode)shrink_MODE;
    }
    
    public Screen getDefaultMainActiveScreen() {
        return this.defaultMainActiveScreen;
    }
    
    public HomeScreenView getHomescreenView() {
        return this.homeScreenView;
    }
    
    public BaseScreen getMainActiveScreen() {
        return this.mainActiveScreen;
    }
    
    public int getMainPanelWidth() {
        return this.mainPanelWidth;
    }
    
    public EnumSet<Screen> getMainScreensSet() {
        return UIStateManager.MAIN_SCREENS_SET;
    }
    
    public NavigationView getNavigationView() {
        NavigationView navigationView;
        if (this.homeScreenView != null) {
            navigationView = this.homeScreenView.getNavigationView();
        }
        else {
            navigationView = null;
        }
        return navigationView;
    }
    
    public Main getRootScreen() {
        return this.rootScreen;
    }
    
    public int getSidePanelWidth() {
        return this.sidePanelWidth;
    }
    
    public SmartDashView getSmartDashView() {
        SmartDashView smartDashView;
        if (this.homeScreenView != null) {
            smartDashView = (SmartDashView)this.homeScreenView.getSmartDashView();
        }
        else {
            smartDashView = null;
        }
        return smartDashView;
    }
    
    public ToastView getToastView() {
        return this.toastView;
    }
    
    public boolean isDialPairingScreenOn() {
        final BaseScreen currentScreen = this.getCurrentScreen();
        return currentScreen != null && currentScreen.getScreen() == Screen.SCREEN_DIAL_PAIRING;
    }
    
    public boolean isDialPairingScreenScanning() {
        final BaseScreen currentScreen = this.getCurrentScreen();
        return currentScreen != null && currentScreen.getScreen() == Screen.SCREEN_DIAL_PAIRING && ((DialManagerScreen)currentScreen).isScanningMode();
    }
    
    public boolean isMainUIShrunk() {
        return this.rootScreen != null && this.rootScreen.isMainUIShrunk();
    }
    
    public boolean isNavigationActive() {
        final HomeScreenView homescreenView = this.getHomescreenView();
        return homescreenView != null && homescreenView.isNavigationActive();
    }
    
    public boolean isWelcomeScreenOn() {
        final BaseScreen currentScreen = this.getCurrentScreen();
        return currentScreen != null && currentScreen.getScreen() == Screen.SCREEN_WELCOME;
    }
    
    public void postNotificationAnimationEvent(final boolean b, final String s, final NotificationType notificationType, final Mode mode) {
        while (true) {
            final HashSet<INotificationAnimationListener> notificationListeners = this.notificationListeners;
            while (true) {
                INotificationAnimationListener notificationAnimationListener = null;
                Label_0067: {
                    synchronized (notificationListeners) {
                        final Iterator<INotificationAnimationListener> iterator = this.notificationListeners.iterator();
                        while (iterator.hasNext()) {
                            notificationAnimationListener = iterator.next();
                            if (!b) {
                                break Label_0067;
                            }
                            notificationAnimationListener.onStart(s, notificationType, mode);
                        }
                        break;
                    }
                }
                notificationAnimationListener.onStop(s, notificationType, mode);
                continue;
            }
        }
    }
    // monitorexit(set)
    
    public void postScreenAnimationEvent(final boolean b, final BaseScreen baseScreen, final BaseScreen baseScreen2) {
        for (final IScreenAnimationListener screenAnimationListener : this.screenListeners) {
            if (b) {
                screenAnimationListener.onStart(baseScreen, baseScreen2);
            }
            else {
                screenAnimationListener.onStop(baseScreen, baseScreen2);
            }
        }
    }
    
    public void removeNotificationAnimationListener(final INotificationAnimationListener notificationAnimationListener) {
        // monitorenter(notificationListeners = this.notificationListeners)
        if (notificationAnimationListener == null) {
            return;
        }
        try {
            this.notificationListeners.remove(notificationAnimationListener);
        }
        finally {
        }
        // monitorexit(notificationListeners)
    }
    
    public void removeScreenAnimationListener(final IScreenAnimationListener screenAnimationListener) {
        if (screenAnimationListener != null) {
            this.screenListeners.remove(screenAnimationListener);
        }
    }
    
    public void setCurrentDashboardType(final SmartDashViewConstants.Type currentDashboardType) {
        this.currentDashboardType = currentDashboardType;
    }
    
    public void setCurrentViewMode(final Screen currentViewMode) {
        this.currentViewMode = currentViewMode;
    }
    
    public void setHomescreenView(final HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
    }
    
    public void setInputFocus() {
        if (this.rootScreen != null) {
            this.rootScreen.setInputFocus();
        }
    }
    
    public void setMainActiveScreen(final BaseScreen baseScreen) {
        if (isFullscreenMode(baseScreen.getScreen())) {
            this.mainActiveScreen = baseScreen;
            final Screen screen = baseScreen.getScreen();
            if (screen != Screen.SCREEN_WELCOME && screen != Screen.SCREEN_DIAL_PAIRING && screen != Screen.SCREEN_FIRST_LAUNCH) {
                this.defaultMainActiveScreen = screen;
            }
        }
        else {
            this.mainActiveScreen = null;
        }
        this.currentScreen = baseScreen;
    }
    
    public void setMainPanelWidth(final int mainPanelWidth) {
        this.mainPanelWidth = mainPanelWidth;
    }
    
    public void setRootScreen(final Main rootScreen) {
        this.rootScreen = rootScreen;
    }
    
    public void setSidePanelWidth(final int sidePanelWidth) {
        this.sidePanelWidth = sidePanelWidth;
    }
    
    public void setToastView(final ToastView toastView) {
        this.toastView = toastView;
    }
    
    public void showSystemTray(final int systemTrayVisibility) {
        if (this.rootScreen != null) {
            this.rootScreen.setSystemTrayVisibility(systemTrayVisibility);
        }
    }
    
    public enum Mode
    {
        COLLAPSE, 
        EXPAND;
    }
}
