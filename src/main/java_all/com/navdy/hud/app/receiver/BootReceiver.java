package com.navdy.hud.app.receiver;

import com.navdy.hud.app.ui.activity.MainActivity;
import android.content.Intent;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import android.content.BroadcastReceiver;

public class BootReceiver extends BroadcastReceiver
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(BootReceiver.class);
    }
    
    public void onReceive(final Context context, Intent intent) {
        final boolean boolean1 = context.getSharedPreferences("App", 0).getBoolean("start_on_boot_preference", true);
        BootReceiver.sLogger.d("Start on boot:" + boolean1);
        if (boolean1) {
            intent = new Intent(context, (Class)MainActivity.class);
            intent.addFlags(268435456);
            context.startActivity(intent);
        }
    }
}
