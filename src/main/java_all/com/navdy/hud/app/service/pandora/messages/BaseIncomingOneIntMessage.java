package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import java.nio.ByteBuffer;
import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.service.library.log.Logger;

public class BaseIncomingOneIntMessage extends BaseIncomingMessage
{
    private static int MESSAGE_LENGTH;
    private static Logger sLogger;
    public int value;
    
    static {
        BaseIncomingOneIntMessage.sLogger = new Logger(BaseIncomingOneIntMessage.class);
        BaseIncomingOneIntMessage.MESSAGE_LENGTH = 5;
    }
    
    public BaseIncomingOneIntMessage(final int value) {
        this.value = value;
    }
    
    protected static int parseIntValue(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        if (array.length != BaseIncomingOneIntMessage.MESSAGE_LENGTH) {
            throw new CorruptedPayloadException();
        }
        return ByteBuffer.wrap(array).getInt(1);
    }
    
    @Override
    public String toString() {
        BaseIncomingOneIntMessage.sLogger.w("toString not overwritten in BaseIncomingOneIntMessage class");
        return "One int message received with value: " + this.value;
    }
}
