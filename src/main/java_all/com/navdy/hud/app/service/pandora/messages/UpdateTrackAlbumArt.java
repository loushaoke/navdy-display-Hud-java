package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import java.nio.ByteBuffer;
import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;

public class UpdateTrackAlbumArt extends BaseIncomingMessage
{
    private static int MESSAGE_LENGTH;
    public int imageLength;
    public int trackToken;
    
    static {
        UpdateTrackAlbumArt.MESSAGE_LENGTH = 9;
    }
    
    protected static BaseIncomingMessage innerBuildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        if (array.length != UpdateTrackAlbumArt.MESSAGE_LENGTH) {
            throw new CorruptedPayloadException();
        }
        final ByteBuffer wrap = ByteBuffer.wrap(array);
        wrap.get();
        final UpdateTrackAlbumArt updateTrackAlbumArt = new UpdateTrackAlbumArt();
        updateTrackAlbumArt.trackToken = wrap.getInt();
        updateTrackAlbumArt.imageLength = wrap.getInt();
        return updateTrackAlbumArt;
    }
    
    @Override
    public String toString() {
        return "Artwork image loaded on client for track: " + this.trackToken;
    }
}
