package com.navdy.hud.app.service;

import android.util.Log;
import android.support.annotation.Nullable;
import android.os.IBinder;
import android.content.Intent;
import android.app.Service;

public class StickyService extends Service
{
    @Nullable
    public IBinder onBind(final Intent intent) {
        return null;
    }
    
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        Log.i("StickyService", "**** hud sticky-service onStartCommand ****");
        return 1;
    }
}
