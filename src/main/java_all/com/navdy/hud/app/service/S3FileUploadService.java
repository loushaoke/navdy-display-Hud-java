package com.navdy.hud.app.service;

import java.util.PriorityQueue;
import java.util.Comparator;
import android.content.Intent;
import com.amazonaws.services.s3.AmazonS3;
import java.util.concurrent.atomic.AtomicBoolean;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.navdy.service.library.util.CredentialUtil;
import com.navdy.hud.app.HudApplication;
import java.util.Iterator;
import java.io.File;
import java.util.ArrayList;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.navdy.service.library.log.Logger;
import android.app.IntentService;

public abstract class S3FileUploadService extends IntentService
{
    public static final String ACTION_SYNC = "SYNC";
    private static final String AWS_ACCOUNT_ID = "AWS_ACCESS_KEY_ID";
    private static final String AWS_SECRET = "AWS_SECRET_ACCESS_KEY";
    public static final int RETRY_DELAY = 10000;
    public static final int S3_CONNECTION_TIMEOUT = 15000;
    public static final int S3_UPLOAD_TIMEOUT = 300000;
    protected Logger logger;
    private AmazonS3Client s3Client;
    private TransferUtility transferUtility;
    
    public S3FileUploadService(final String s) {
        super(s);
        this.logger = this.getLogger();
    }
    
    public static void populateFilesQueue(final ArrayList<File> list, final UploadQueue uploadQueue, final int n) {
        if (list != null) {
            for (final File file : list) {
                if (file.isFile()) {
                    uploadQueue.add(new Request(file, null));
                    if (uploadQueue.size() != n) {
                        continue;
                    }
                    uploadQueue.pop();
                }
            }
        }
    }
    
    public abstract boolean canCompleteRequest(final Request p0);
    
    public AmazonS3Client createS3Client() {
        final BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(CredentialUtil.getCredentials(HudApplication.getAppContext(), "AWS_ACCESS_KEY_ID"), CredentialUtil.getCredentials(HudApplication.getAppContext(), "AWS_SECRET_ACCESS_KEY"));
        final ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setConnectionTimeout(15000);
        clientConfiguration.setSocketTimeout(300000);
        return new AmazonS3Client(basicAWSCredentials, clientConfiguration);
    }
    
    protected abstract String getAWSBucket();
    
    protected abstract Request getCurrentRequest();
    
    protected abstract AtomicBoolean getIsUploading();
    
    protected abstract String getKeyPrefix(final File p0);
    
    protected abstract Logger getLogger();
    
    protected abstract UploadQueue getUploadQueue();
    
    protected abstract void initialize();
    
    public void onCreate() {
        super.onCreate();
        if (this.s3Client == null) {
            this.s3Client = this.createS3Client();
            this.transferUtility = new TransferUtility(this.s3Client, HudApplication.getAppContext());
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
    }
    
    protected void onHandleIntent(final Intent p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnull          56
        //     4: aload_1        
        //     5: invokevirtual   android/content/Intent.getAction:()Ljava/lang/String;
        //     8: astore_1       
        //     9: ldc             "SYNC"
        //    11: aload_1        
        //    12: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    15: ifeq            55
        //    18: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //    21: invokestatic    com/navdy/service/library/util/SystemUtils.isConnectedToNetwork:(Landroid/content/Context;)Z
        //    24: istore_2       
        //    25: aload_0        
        //    26: getfield        com/navdy/hud/app/service/S3FileUploadService.logger:Lcom/navdy/service/library/log/Logger;
        //    29: new             Ljava/lang/StringBuilder;
        //    32: dup            
        //    33: invokespecial   java/lang/StringBuilder.<init>:()V
        //    36: ldc             "Performing sync , connected to network ? : "
        //    38: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    41: iload_2        
        //    42: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //    45: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    48: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    51: iload_2        
        //    52: ifne            62
        //    55: return         
        //    56: ldc             ""
        //    58: astore_1       
        //    59: goto            9
        //    62: aload_0        
        //    63: invokevirtual   com/navdy/hud/app/service/S3FileUploadService.initialize:()V
        //    66: aload_0        
        //    67: invokevirtual   com/navdy/hud/app/service/S3FileUploadService.getUploadQueue:()Lcom/navdy/hud/app/service/S3FileUploadService$UploadQueue;
        //    70: astore_3       
        //    71: aload_3        
        //    72: dup            
        //    73: astore          4
        //    75: monitorenter   
        //    76: aload_3        
        //    77: invokevirtual   com/navdy/hud/app/service/S3FileUploadService$UploadQueue.size:()I
        //    80: ifle            444
        //    83: aload_0        
        //    84: invokevirtual   com/navdy/hud/app/service/S3FileUploadService.getIsUploading:()Ljava/util/concurrent/atomic/AtomicBoolean;
        //    87: astore          5
        //    89: aload           5
        //    91: iconst_0       
        //    92: iconst_1       
        //    93: invokevirtual   java/util/concurrent/atomic/AtomicBoolean.compareAndSet:(ZZ)Z
        //    96: ifeq            375
        //    99: aconst_null    
        //   100: astore_1       
        //   101: aload_1        
        //   102: ifnull          148
        //   105: aload_0        
        //   106: getfield        com/navdy/hud/app/service/S3FileUploadService.logger:Lcom/navdy/service/library/log/Logger;
        //   109: astore          6
        //   111: new             Ljava/lang/StringBuilder;
        //   114: astore          7
        //   116: aload           7
        //   118: invokespecial   java/lang/StringBuilder.<init>:()V
        //   121: aload           6
        //   123: aload           7
        //   125: ldc             "File to upload "
        //   127: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   130: aload_1        
        //   131: getfield        com/navdy/hud/app/service/S3FileUploadService$Request.file:Ljava/io/File;
        //   134: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   137: ldc             ", Does not exist anymore"
        //   139: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   142: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   145: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   148: aload_3        
        //   149: invokevirtual   com/navdy/hud/app/service/S3FileUploadService$UploadQueue.size:()I
        //   152: ifle            387
        //   155: aload_3        
        //   156: invokevirtual   com/navdy/hud/app/service/S3FileUploadService$UploadQueue.pop:()Lcom/navdy/hud/app/service/S3FileUploadService$Request;
        //   159: astore          7
        //   161: aload           7
        //   163: astore_1       
        //   164: aload           7
        //   166: getfield        com/navdy/hud/app/service/S3FileUploadService$Request.file:Ljava/io/File;
        //   169: invokevirtual   java/io/File.exists:()Z
        //   172: ifeq            101
        //   175: aload_0        
        //   176: aload           7
        //   178: invokevirtual   com/navdy/hud/app/service/S3FileUploadService.setCurrentRequest:(Lcom/navdy/hud/app/service/S3FileUploadService$Request;)V
        //   181: aload           7
        //   183: getfield        com/navdy/hud/app/service/S3FileUploadService$Request.file:Ljava/io/File;
        //   186: astore          6
        //   188: aload_0        
        //   189: aload           6
        //   191: invokevirtual   com/navdy/hud/app/service/S3FileUploadService.getKeyPrefix:(Ljava/io/File;)Ljava/lang/String;
        //   194: astore          8
        //   196: aload           8
        //   198: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   201: ifne            399
        //   204: new             Ljava/lang/StringBuilder;
        //   207: astore_1       
        //   208: aload_1        
        //   209: invokespecial   java/lang/StringBuilder.<init>:()V
        //   212: aload_1        
        //   213: aload           8
        //   215: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   218: getstatic       java/io/File.separator:Ljava/lang/String;
        //   221: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   224: aload           6
        //   226: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //   229: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   232: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   235: astore_1       
        //   236: aload_0        
        //   237: getfield        com/navdy/hud/app/service/S3FileUploadService.logger:Lcom/navdy/service/library/log/Logger;
        //   240: astore          9
        //   242: new             Ljava/lang/StringBuilder;
        //   245: astore          10
        //   247: aload           10
        //   249: invokespecial   java/lang/StringBuilder.<init>:()V
        //   252: aload           9
        //   254: aload           10
        //   256: ldc             "Trying to upload : "
        //   258: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   261: aload           6
        //   263: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //   266: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   269: ldc             ", Under :"
        //   271: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   274: aload           8
        //   276: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   279: ldc_w           ", Key :"
        //   282: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   285: aload_1        
        //   286: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   289: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   292: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   295: aload_0        
        //   296: getfield        com/navdy/hud/app/service/S3FileUploadService.transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;
        //   299: aload_0        
        //   300: invokevirtual   com/navdy/hud/app/service/S3FileUploadService.getAWSBucket:()Ljava/lang/String;
        //   303: aload_1        
        //   304: aload           6
        //   306: invokevirtual   com/amazonaws/mobileconnectors/s3/transferutility/TransferUtility.upload:(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;
        //   309: astore_1       
        //   310: aload_1        
        //   311: invokevirtual   com/amazonaws/mobileconnectors/s3/transferutility/TransferObserver.getId:()I
        //   314: istore          11
        //   316: aload_0        
        //   317: getfield        com/navdy/hud/app/service/S3FileUploadService.logger:Lcom/navdy/service/library/log/Logger;
        //   320: astore          7
        //   322: new             Ljava/lang/StringBuilder;
        //   325: astore          8
        //   327: aload           8
        //   329: invokespecial   java/lang/StringBuilder.<init>:()V
        //   332: aload           7
        //   334: aload           8
        //   336: ldc_w           "Transfer id "
        //   339: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   342: iload           11
        //   344: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   347: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   350: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   353: new             Lcom/navdy/hud/app/service/S3FileUploadService$1;
        //   356: astore          7
        //   358: aload           7
        //   360: aload_0        
        //   361: aload           5
        //   363: aload           6
        //   365: aload_1        
        //   366: invokespecial   com/navdy/hud/app/service/S3FileUploadService$1.<init>:(Lcom/navdy/hud/app/service/S3FileUploadService;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/io/File;Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;)V
        //   369: aload_1        
        //   370: aload           7
        //   372: invokevirtual   com/amazonaws/mobileconnectors/s3/transferutility/TransferObserver.setTransferListener:(Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferListener;)V
        //   375: aload           4
        //   377: monitorexit    
        //   378: goto            55
        //   381: astore_1       
        //   382: aload           4
        //   384: monitorexit    
        //   385: aload_1        
        //   386: athrow         
        //   387: aload           5
        //   389: iconst_0       
        //   390: invokevirtual   java/util/concurrent/atomic/AtomicBoolean.set:(Z)V
        //   393: aload           4
        //   395: monitorexit    
        //   396: goto            55
        //   399: aload           6
        //   401: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //   404: astore_1       
        //   405: goto            236
        //   408: astore_1       
        //   409: aload_1        
        //   410: invokevirtual   java/lang/IllegalArgumentException.printStackTrace:()V
        //   413: aload           5
        //   415: iconst_0       
        //   416: invokevirtual   java/util/concurrent/atomic/AtomicBoolean.set:(Z)V
        //   419: aload_0        
        //   420: iconst_0       
        //   421: aload           6
        //   423: invokevirtual   java/io/File.getPath:()Ljava/lang/String;
        //   426: aload           7
        //   428: getfield        com/navdy/hud/app/service/S3FileUploadService$Request.userTag:Ljava/lang/String;
        //   431: invokevirtual   com/navdy/hud/app/service/S3FileUploadService.uploadFinished:(ZLjava/lang/String;Ljava/lang/String;)V
        //   434: aload_0        
        //   435: invokevirtual   com/navdy/hud/app/service/S3FileUploadService.sync:()V
        //   438: aload           4
        //   440: monitorexit    
        //   441: goto            55
        //   444: aload_0        
        //   445: getfield        com/navdy/hud/app/service/S3FileUploadService.logger:Lcom/navdy/service/library/log/Logger;
        //   448: ldc_w           "Nothing to upload"
        //   451: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   454: goto            375
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  76     99     381    387    Any
        //  105    148    381    387    Any
        //  148    161    381    387    Any
        //  164    236    381    387    Any
        //  236    295    381    387    Any
        //  295    310    408    444    Ljava/lang/IllegalArgumentException;
        //  295    310    381    387    Any
        //  310    375    381    387    Any
        //  375    378    381    387    Any
        //  382    385    381    387    Any
        //  387    396    381    387    Any
        //  399    405    381    387    Any
        //  409    441    381    387    Any
        //  444    454    381    387    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0375:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public abstract void reSchedule();
    
    protected abstract void setCurrentRequest(final Request p0);
    
    public abstract void sync();
    
    protected abstract void uploadFinished(final boolean p0, final String p1, final String p2);
    
    protected static class Request
    {
        public final File file;
        public final String userTag;
        
        public Request(final File file, final String userTag) {
            this.file = file;
            this.userTag = userTag;
        }
    }
    
    public static class RequestTimeComparator implements Comparator<Request>
    {
        @Override
        public int compare(final Request request, final Request request2) {
            return Long.compare(request2.file.lastModified(), request.file.lastModified());
        }
    }
    
    public static class UploadFinished
    {
        public final String filePath;
        public final boolean succeeded;
        public final String userTag;
        
        UploadFinished(final boolean succeeded, final String filePath, final String userTag) {
            this.succeeded = succeeded;
            this.filePath = filePath;
            this.userTag = userTag;
        }
    }
    
    public static class UploadQueue
    {
        private final PriorityQueue<Request> internalQueue;
        
        public UploadQueue() {
            this.internalQueue = new PriorityQueue<Request>(15, new RequestTimeComparator());
        }
        
        public void add(final Request request) {
            this.internalQueue.add(request);
        }
        
        public Request peek() {
            return this.internalQueue.peek();
        }
        
        public Request pop() {
            return this.internalQueue.poll();
        }
        
        public int size() {
            return this.internalQueue.size();
        }
    }
}
