package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import java.nio.ByteBuffer;

public class ReturnTrackAlbumArtSegment extends BaseIncomingMessage
{
    public byte[] data;
    public byte segmentIndex;
    public byte totalSegments;
    public int trackToken;
    
    protected static BaseIncomingMessage innerBuildFromPayload(final byte[] array) throws MessageWrongWayException, CorruptedPayloadException {
        final ByteBuffer wrap = ByteBuffer.wrap(array);
        wrap.get();
        final ReturnTrackAlbumArtSegment returnTrackAlbumArtSegment = new ReturnTrackAlbumArtSegment();
        returnTrackAlbumArtSegment.trackToken = wrap.getInt();
        returnTrackAlbumArtSegment.segmentIndex = wrap.get();
        returnTrackAlbumArtSegment.totalSegments = wrap.get();
        wrap.get(returnTrackAlbumArtSegment.data = new byte[wrap.remaining()]);
        return returnTrackAlbumArtSegment;
    }
    
    @Override
    public String toString() {
        return "Got artwork's segment " + (this.segmentIndex + 1) + "/" + this.totalSegments + " for track with token " + this.trackToken;
    }
}
