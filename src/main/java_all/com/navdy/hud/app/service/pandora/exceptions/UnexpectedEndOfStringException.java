package com.navdy.hud.app.service.pandora.exceptions;

public class UnexpectedEndOfStringException extends Exception
{
    public UnexpectedEndOfStringException() {
        super("Zero bytes (used to mark the end of string) is not allowed in strings");
    }
}
