package com.navdy.hud.app.config;

import android.content.SharedPreferences;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class SettingsManager implements IObserver
{
    private static final Logger sLogger;
    private final Bus bus;
    private final SharedPreferences preferences;
    
    static {
        sLogger = new Logger(SettingsManager.class);
    }
    
    public SettingsManager(final Bus bus, final SharedPreferences preferences) {
        this.bus = bus;
        this.preferences = preferences;
    }
    
    public void addSetting(final Setting setting) {
        SettingsManager.sLogger.i("Adding setting " + setting);
        setting.setObserver((Setting.IObserver)this);
    }
    
    @Override
    public void onChanged(final Setting setting) {
        SettingsManager.sLogger.i("Setting changed:" + setting);
        this.bus.post(new SettingsChanged(setting.getPath(), setting));
    }
    
    public static class SettingsChanged
    {
        public final String path;
        public final Setting setting;
        
        public SettingsChanged(final String path, final Setting setting) {
            this.path = path;
            this.setting = setting;
        }
    }
}
