package com.navdy.hud.app.config;

import android.text.TextUtils;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.util.DeviceUtil;

public class BooleanSetting extends Setting
{
    private boolean changed;
    private boolean defaultValue;
    private boolean enabled;
    private Scope scope;
    
    public BooleanSetting(final String s, final Scope scope, final String s2, final String s3) {
        this(s, scope, false, s2, s3);
    }
    
    public BooleanSetting(final String s, final Scope scope, final boolean defaultValue, final String s2, final String s3) {
        super(s, s2, s3);
        this.scope = scope;
        this.defaultValue = defaultValue;
        this.load();
    }
    
    @Override
    public boolean isEnabled() {
        final boolean b = true;
        boolean enabled = false;
        switch (this.scope) {
            default:
                enabled = enabled;
                return enabled;
            case ALWAYS:
                enabled = true;
                return enabled;
            case ENG:
                enabled = (!DeviceUtil.isUserBuild() && b);
                return enabled;
            case CUSTOM:
                enabled = this.enabled;
            case NEVER:
            case BETA:
                return enabled;
        }
    }
    
    public void load() {
        final String property = this.getProperty();
        if (property != null) {
            final String value = SystemProperties.get(property);
            if (!TextUtils.isEmpty((CharSequence)value)) {
                this.scope = Scope.CUSTOM;
                this.enabled = (Boolean.parseBoolean(value) || value.equalsIgnoreCase("1") || value.equalsIgnoreCase("yes"));
            }
            else {
                this.enabled = this.defaultValue;
            }
        }
    }
    
    public void save() {
        if (this.changed) {
            this.changed = false;
            final String property = this.getProperty();
            if (property != null) {
                SystemProperties.set(property, Boolean.toString(this.enabled));
            }
        }
    }
    
    public void setEnabled(final boolean enabled) {
        if (this.scope != Scope.CUSTOM) {
            this.scope = Scope.CUSTOM;
        }
        if (this.enabled != enabled) {
            this.enabled = enabled;
            this.changed = true;
            this.changed();
            this.save();
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BooleanSetting{");
        sb.append("name=").append(this.getName());
        sb.append(",");
        sb.append("enabled=").append(this.enabled);
        sb.append('}');
        return sb.toString();
    }
    
    public enum Scope
    {
        ALWAYS, 
        BETA, 
        CUSTOM, 
        ENG, 
        NEVER;
    }
}
