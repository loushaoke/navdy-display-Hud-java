package com.navdy.hud.app.analytics;

import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.service.library.events.ui.DismissScreen;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.device.PowerManager;
import com.here.android.mpa.routing.Maneuver;
import javax.inject.Inject;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.util.os.SystemProperties;
import android.os.Build;
import java.util.HashMap;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen;
import com.navdy.obd.ECU;
import com.navdy.service.library.events.audio.VoiceSearchResponse;
import com.navdy.hud.app.event.Shutdown;
import android.os.SystemClock;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.here.HereOfflineMapsVersion;
import android.text.TextUtils;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.glance.GlanceNotification;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.util.OTAUpdateService;
import android.os.Build;
import com.navdy.service.library.util.IOUtils;
import java.util.Iterator;
import java.util.Locale;
import com.navdy.obd.Pid;
import java.util.Map;
import com.navdy.service.library.task.TaskManager;
import java.util.Calendar;
import com.navdy.hud.app.util.SerialNumber;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import android.app.Application;
import android.content.Context;
import com.localytics.android.LocalyticsActivityLifecycleCallbacks;
import android.app.Application;
import android.content.SharedPreferences;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.localytics.android.Localytics;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import java.util.concurrent.TimeUnit;
import android.os.Looper;
import com.navdy.service.library.events.destination.Destination;
import java.util.List;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.places.PlaceType;
import android.os.Handler;
import java.util.ArrayList;

public class AnalyticsSupport
{
    private static final String ANALYTICS_EVENT_ADAPTIVE_BRIGHTNESS_ADJUSTMENT_CHANGE = "Adaptive_Brightness_Adjustment_Change";
    private static final String ANALYTICS_EVENT_ATTEMPT_TO_CONNECT = "Attempt_To_Connect";
    private static final String ANALYTICS_EVENT_BAD_ROUTE_POSITION = "Route_Bad_Pos";
    private static final String ANALYTICS_EVENT_BRIGHTNESS_ADJUSTMENT_CHANGE = "Brightness_Adjustment_Change";
    private static final String ANALYTICS_EVENT_BRIGHTNESS_CHANGE = "Brightness_Change";
    private static final String ANALYTICS_EVENT_CPU_OVERHEAT = "Cpu_Overheat";
    private static final String ANALYTICS_EVENT_DASH_GAUGE_SETTING = "Dash_Gauge_Setting";
    private static final String ANALYTICS_EVENT_DESTINATION_SUGGESTION = "Destination_Suggestion_Show";
    private static final String ANALYTICS_EVENT_DIAL_UPDATE_PROMPT = "Dial_Update_Prompt";
    private static final String ANALYTICS_EVENT_DISPLAY_UPDATE_PROMPT = "Display_Update_Prompt";
    private static final String ANALYTICS_EVENT_DISTANCE_TRAVELLED = "Distance_Travelled";
    private static final String ANALYTICS_EVENT_DRIVER_PREFERENCES = "Driver_Preferences";
    private static final String ANALYTICS_EVENT_FUEL_LEVEL = "Fuel_Level";
    public static final String ANALYTICS_EVENT_GLANCE_ADVANCE = "Glance_Advance";
    public static final String ANALYTICS_EVENT_GLANCE_DELETE_ALL = "Glance_Delete_All";
    public static final String ANALYTICS_EVENT_GLANCE_DISMISS = "Glance_Dismiss";
    public static final String ANALYTICS_EVENT_GLANCE_OPEN_FULL = "Glance_Open_Full";
    public static final String ANALYTICS_EVENT_GLANCE_OPEN_MINI = "Glance_Open_Mini";
    private static final String ANALYTICS_EVENT_GLYMPSE_SENT = "Glympse_Sent";
    private static final String ANALYTICS_EVENT_GLYMPSE_VIEWED = "Glympse_Viewed";
    public static final String ANALYTICS_EVENT_GPS_ACCURACY_STATISTICS = "GPS_Accuracy";
    public static final String ANALYTICS_EVENT_GPS_ACQUIRE_LOCATION = "GPS_Acquired_Location";
    public static final String ANALYTICS_EVENT_GPS_ATTEMPT_ACQUIRE_LOCATION = "GPS_Attempt_Acquire_Location";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_IMU_DONE = "GPS_Calibration_IMU_Done";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_LOST = "GPS_Calibration_Lost";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_SENSOR_DONE = "GPS_Calibration_Sensor_Done";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_START = "GPS_Calibration_Start";
    public static final String ANALYTICS_EVENT_GPS_CALIBRATION_VIN_SWITCH = "GPS_Calibration_VinSwitch";
    public static final String ANALYTICS_EVENT_GPS_LOST_LOCATION = "GPS_Lost_Signal";
    public static final String ANALYTICS_EVENT_IAP_FAILURE = "Mobile_iAP_Failure";
    public static final String ANALYTICS_EVENT_IAP_RETRY_SUCCESS = "Mobile_iAP_Retry_Success";
    private static final String ANALYTICS_EVENT_KEY_FAILED = "Key_Failed";
    private static final String ANALYTICS_EVENT_MENU_SELECTION = "Menu_Selection";
    private static final String ANALYTICS_EVENT_MILEAGE = "Navdy_Mileage";
    private static final String ANALYTICS_EVENT_MOBILE_APP_CONNECT = "Mobile_App_Connect";
    private static final String ANALYTICS_EVENT_MOBILE_APP_DISCONNECT = "Mobile_App_Disconnect";
    private static final String ANALYTICS_EVENT_MUSIC_ACTION = "Music_Action";
    private static final String ANALYTICS_EVENT_NAVIGATION_ARRIVED = "Navigation_Arrived";
    private static final String ANALYTICS_EVENT_NAVIGATION_DESTINATION_SET = "Navigation_Destination_Set";
    private static final String ANALYTICS_EVENT_NAVIGATION_MANEUVER_MISSED = "Navigation_Maneuver_Missed";
    private static final String ANALYTICS_EVENT_NAVIGATION_MANEUVER_REQUIRED = "Navigation_Maneuver_Required";
    private static final String ANALYTICS_EVENT_NAVIGATION_MANEUVER_WARNING = "Navigation_Maneuver_Warning";
    private static final String ANALYTICS_EVENT_NAVIGATION_PREFERENCES = "Navigation_Preferences";
    private static final String ANALYTICS_EVENT_NAVIGATION_REGION_CHANGE = "Map_Region_Change";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_ARRIVED = "Nearby_Search_Arrived";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_CLOSED = "Nearby_Search_Closed";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_DISMISS = "Nearby_Search_Dismiss";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_ENDED = "Nearby_Search_Ended";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_NO_RESULTS = "Nearby_Search_No_Results";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_CLOSE = "Nearby_Search_Results_Close";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_DISMISS = "Nearby_Search_Results_Dismiss";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_VIEW = "Nearby_Search_Results_View";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_RETURN_MM = "Nearby_Search_Return_Main_Menu";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_SELECTION = "Nearby_Search_Selection";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_CANCELLED = "Nearby_Search_Trip_Cancelled";
    private static final String ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_INITIATED = "Nearby_Search_Trip_Initiated";
    public static final String ANALYTICS_EVENT_NEW_DEVICE = "Analytics_New_Device";
    private static final String ANALYTICS_EVENT_OBD_CONNECTED = "OBD_Connected";
    private static final String ANALYTICS_EVENT_OBD_LISTENING_POSTED = "OBD_Listening_Posted";
    private static final String ANALYTICS_EVENT_OBD_LISTENING_STATUS = "OBD_Listening_Status";
    private static final String ANALYTICS_EVENT_OFFLINE_MAPS_UPDATED = "Offline_Maps_Updated";
    private static final String ANALYTICS_EVENT_OPTION_SELECTED = "Option_Selected";
    private static final String ANALYTICS_EVENT_OTADOWNLOAD = "OTA_Download";
    private static final String ANALYTICS_EVENT_OTAINSTALL = "OTA_Install";
    private static final String ANALYTICS_EVENT_OTA_INSTALL_RESULT = "OTA_Install_Result";
    private static final String ANALYTICS_EVENT_PHONE_DISCONNECTED = "Phone_Disconnected";
    private static final String ANALYTICS_EVENT_ROUTE_COMPLETED = "Route_Completed";
    private static final String ANALYTICS_EVENT_SHUTDOWN = "Shutdown";
    private static final String ANALYTICS_EVENT_SMS_SENT = "sms_sent";
    private static final String ANALYTICS_EVENT_SWIPED_PREFIX = "Swiped_";
    private static final String ANALYTICS_EVENT_TEMPERATURE_CHANGE = "Temperature_Change";
    private static final String ANALYTICS_EVENT_VEHICLE_TELEMETRY_DATA = "Vehicle_Telemetry_Data";
    private static final String ANALYTICS_EVENT_VOICE_SEARCH_ANDROID = "Voice_Search_Android";
    private static final String ANALYTICS_EVENT_VOICE_SEARCH_IOS = "Voice_Search_iOS";
    private static final String ANALYTICS_EVENT_WAKEUP = "Wakeup";
    private static final String ANALYTICS_EVENT_ZERO_FUEL_DATA = "OBD_Zero_Fuel_Level";
    public static final String ANALYTICS_INTENT = "com.navdy.hud.app.analytics.AnalyticsEvent";
    public static final String ANALYTICS_INTENT_EXTRA_ARGUMENTS = "arguments";
    public static final String ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO = "include_device_info";
    public static final String ANALYTICS_INTENT_EXTRA_TAG_NAME = "tag";
    private static final String ATTR_BRIGHTNESS_ADJ_NEW = "New";
    private static final String ATTR_BRIGHTNESS_ADJ_PREVIOUS = "Previous";
    private static final String ATTR_BRIGHTNESS_BRIGHTNESS = "Brightness";
    private static final String ATTR_BRIGHTNESS_LIGHT_SENSOR = "Light_Sensor";
    private static final String ATTR_BRIGHTNESS_NEW = "New";
    private static final String ATTR_BRIGHTNESS_PREVIOUS = "Previous";
    private static final String ATTR_BRIGHTNESS_RAW1_SENSOR = "Raw1_Sensor";
    private static final String ATTR_BRIGHTNESS_RAW_SENSOR = "Raw_Sensor";
    private static final String ATTR_CONN_ATTEMPT_NEW_DEVICE = "Was_New_Device";
    private static final String ATTR_CONN_ATTEMPT_TIME = "Time_To_Connect";
    private static final String ATTR_CONN_CAR_MAKE = "Car_Make";
    private static final String ATTR_CONN_CAR_MODEL = "Car_Model";
    private static final String ATTR_CONN_CAR_YEAR = "Car_Year";
    private static final String ATTR_CONN_DEVICE_ID = "Device_Id";
    private static final String ATTR_CONN_REMOTE_CLIENT_VERSION = "Remote_Client_Version";
    private static final String ATTR_CONN_REMOTE_DEVICE_ID = "Remote_Device_Id";
    private static final String ATTR_CONN_REMOTE_DEVICE_NAME = "Remote_Device_Name";
    private static final String ATTR_CONN_REMOTE_PLATFORM = "Remote_Platform";
    private static final String ATTR_CONN_VIN = "VIN";
    private static final String ATTR_CPU_OVERHEAT_STATE = "State";
    private static final String ATTR_CPU_OVERHEAT_UPTIME = "Uptime";
    private static final String ATTR_DESTINATION_SUGGESTION_ACCEPTED = "Accepted";
    private static final String ATTR_DESTINATION_SUGGESTION_TYPE = "Type";
    private static final String ATTR_DISC_CAR_MAKE = "Car_Make";
    private static final String ATTR_DISC_CAR_MODEL = "Car_Model";
    private static final String ATTR_DISC_CAR_YEAR = "Car_Year";
    private static final String ATTR_DISC_ELAPSED_TIME = "Elapsed_Time";
    private static final String ATTR_DISC_FORCE_RECONNECT = "Force_Reconnect";
    private static final String ATTR_DISC_FORCE_RECONNECT_REASON = "Force_Reconnect_Reason";
    private static final String ATTR_DISC_MOVING = "While_Moving";
    private static final String ATTR_DISC_PHONE_MAKE = "Phone_Make";
    private static final String ATTR_DISC_PHONE_MODEL = "Phone_Model";
    private static final String ATTR_DISC_PHONE_OS = "Phone_OS";
    private static final String ATTR_DISC_PHONE_TYPE = "Phone_Type";
    private static final String ATTR_DISC_REASON = "Reason";
    private static final String ATTR_DISC_STATUS = "Status";
    private static final String ATTR_DISC_VIN = "VIN";
    private static final String ATTR_DISTANCE = "Distance";
    private static final String ATTR_DRIVER_PREFERENCES_AUTO_ON = "Auto_On";
    private static final String ATTR_DRIVER_PREFERENCES_DISPLAY_FORMAT = "Display_Format";
    private static final String ATTR_DRIVER_PREFERENCES_FEATURE_MODE = "Feature_Mode";
    private static final String ATTR_DRIVER_PREFERENCES_GESTURES = "Gestures";
    private static final String ATTR_DRIVER_PREFERENCES_GLANCES = "Glances";
    private static final String ATTR_DRIVER_PREFERENCES_LIMIT_BANDWIDTH = "Limit_Bandwidth";
    private static final String ATTR_DRIVER_PREFERENCES_LOCALE = "Locale";
    private static final String ATTR_DRIVER_PREFERENCES_MANUAL_ZOOM = "Manual_Zoom";
    private static final String ATTR_DRIVER_PREFERENCES_OBD = "OBD";
    private static final String ATTR_DRIVER_PREFERENCES_UNITS = "Units";
    private static final String ATTR_DURATION = "Duration";
    private static final String ATTR_EVENT_AVERAGE_MPG = "Event_Average_Mpg";
    private static final String ATTR_EVENT_AVERAGE_RPM = "Event_Average_Rpm";
    private static final String ATTR_EVENT_ENGINE_TEMPERATURE = "Event_Engine_Temperature";
    private static final String ATTR_EVENT_FUEL_LEVEL = "Event_Fuel_Level";
    private static final String ATTR_EVENT_MAX_G = "Event_Max_G";
    private static final String ATTR_EVENT_MAX_G_ANGLE = "Event_Max_G_Angle";
    private static final String ATTR_FUEL_CONSUMPTION = "Fuel_Consumption";
    private static final String ATTR_FUEL_DISTANCE = "Distance";
    private static final String ATTR_FUEL_LEVEL = "Fuel_Level";
    private static final String ATTR_FUEL_VIN = "VIN";
    private static final String ATTR_GAUGE_ID = "Gauge_name";
    private static final String ATTR_GAUGE_SIDE = "Side";
    private static final String ATTR_GAUGE_SIDE_LEFT = "Left";
    private static final String ATTR_GAUGE_SIDE_RIGHT = "Right";
    private static final String ATTR_GLANCE_NAME = "Glance_Name";
    private static final String ATTR_GLANCE_NAV_METHOD = "Nav_Method";
    private static final String ATTR_GLYMPSE_ID = "Id";
    private static final String ATTR_GLYMPSE_MESSAGE = "Message";
    private static final String ATTR_GLYMPSE_SHARE = "Share";
    private static final String ATTR_GLYMPSE_SUCCESS = "Success";
    private static final String ATTR_KEY_FAIL_ERROR = "Error";
    private static final String ATTR_KEY_FAIL_KEY_TYPE = "Key_Type";
    private static final String ATTR_MENU_NAME = "Name";
    private static final String ATTR_MUSIC_ACTION_TYPE = "Type";
    private static final String ATTR_NAVDY_KILOMETERS = "Navdy_Kilometers";
    private static final String ATTR_NAVDY_USAGE_RATE = "Navdy_Usage_Rate";
    private static final String ATTR_NAVIGATING = "Navigating";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_AUTO_TRAINS = "Allow_Auto_Trains";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_FERRIES = "Allow_Ferries";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_HIGHWAYS = "Allow_Highways";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_HOV_LANES = "Allow_HOV_Lanes";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_TOLL_ROADS = "Allow_Toll_Roads";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_TUNNELS = "Allow_Tunnels";
    private static final String ATTR_NAVIGATION_PREFERENCES_ALLOW_UNPAVED_ROADS = "Allow_Unpaved_Roads";
    private static final String ATTR_NAVIGATION_PREFERENCES_REROUTE_FOR_TRAFFIC = "Reroute_For_Traffic";
    private static final String ATTR_NAVIGATION_PREFERENCES_ROUTING_TYPE = "Routing_Type";
    private static final String ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_NAVIGATING = "Show_Traffic_Navigating";
    private static final String ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_OPEN_MAP = "Show_Traffic_Open_Map";
    private static final String ATTR_NAVIGATION_PREFERENCES_SPOKEN_SPEED_WARNINGS = "Spoken_Speed_Warnings";
    private static final String ATTR_NAVIGATION_PREFERENCES_SPOKEN_TBT = "Spoken_Turn_By_Turn";
    private static final String ATTR_NEARBY_SEARCH_DEST_SCROLLED = "Scrolled";
    private static final String ATTR_NEARBY_SEARCH_DEST_SELECTED_POSITION = "Selected_Position";
    private static final String ATTR_NEARBY_SEARCH_NO_RESULTS_ACTION = "Action";
    private static final String ATTR_NEARBY_SEARCH_PLACE_TYPE = "Place_Type";
    private static final String ATTR_OBD_CONN_ECU = "ECU";
    private static final String ATTR_OBD_CONN_FIRMWARE_VERSION = "Obd_Chip_Firmware";
    private static final String ATTR_OBD_CONN_PROTOCOL = "Protocol";
    private static final String ATTR_OBD_CONN_VIN = "VIN";
    private static final String ATTR_OBD_DATA_CAR_MAKE = "Car_Make";
    private static final String ATTR_OBD_DATA_CAR_MODEL = "Car_Model";
    private static final String ATTR_OBD_DATA_CAR_VIN = "Vin";
    private static final String ATTR_OBD_DATA_CAR_YEAR = "Car_Year";
    private static final String ATTR_OBD_DATA_MONITORING_DATA_SIZE = "Data_Size";
    private static final String ATTR_OBD_DATA_MONITORING_SUCCESS = "Success";
    private static final String ATTR_OBD_DATA_VERSION = "Version";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_NEW_DATE = "New_Date";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_NEW_PACKAGES = "New_Packages";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_NEW_VERSION = "New_Version";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_OLD_DATE = "Old_Date";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_OLD_PACKAGES = "Old_Packages";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_OLD_VERSION = "Old_Version";
    private static final String ATTR_OFFLINE_MAPS_UPDATED_UPDATE_COUNT = "Update_Count";
    private static final String ATTR_OPTION_NAME = "Name";
    private static final String ATTR_OTA_CURRENT_VERSION = "Current_Version";
    private static final String ATTR_OTA_INSTALL_INCREMENTAL = "Incremental";
    private static final String ATTR_OTA_INSTALL_PRIOR_VERSION = "Prior_Version";
    private static final String ATTR_OTA_INSTALL_SUCCESS = "Success";
    private static final String ATTR_OTA_UPDATE_VERSION = "Update_Version";
    private static final String ATTR_PHONE_CONNECTION = "Connection";
    public static final String ATTR_PLATFORM_ANDROID = "Android";
    public static final String ATTR_PLATFORM_IOS = "iOS";
    private static final String ATTR_PROFILE_BUILD_TYPE = "Build_Type";
    private static final String ATTR_PROFILE_CAR_MAKE = "Car_Make";
    private static final String ATTR_PROFILE_CAR_MODEL = "Car_Model";
    private static final String ATTR_PROFILE_CAR_YEAR = "Car_Year";
    private static final String ATTR_PROFILE_HUD_VERSION = "Hud_Version_Number";
    private static final String ATTR_PROFILE_SERIAL_NUMBER = "Hud_Serial_Number";
    private static final String ATTR_REGION_COUNTRY = "Country";
    private static final String ATTR_REGION_STATE = "State";
    private static final String ATTR_REMOTE_DEVICE_HARDWARE = "Remote_Device_Hardware";
    private static final String ATTR_REMOTE_DEVICE_PLATFORM = "Remote_Device_Platform";
    private static final String ATTR_REMOTE_DEVICE_SW_VERSION = "Remote_Device_SW_Version";
    private static final String ATTR_ROUTE_COMPLETED_ACTUAL_DISTANCE = "Actual_Distance";
    private static final String ATTR_ROUTE_COMPLETED_ACTUAL_DURATION = "Actual_Duration";
    private static final String ATTR_ROUTE_COMPLETED_DISTANCE_VARIANCE = "Distance_PCT_Diff";
    private static final String ATTR_ROUTE_COMPLETED_DURATION_VARIANCE = "Duration_PCT_Diff";
    private static final String ATTR_ROUTE_COMPLETED_EXPECTED_DISTANCE = "Expected_Distance";
    private static final String ATTR_ROUTE_COMPLETED_EXPECTED_DURATION = "Expected_Duration";
    private static final String ATTR_ROUTE_COMPLETED_RECALCULATIONS = "Recalculations";
    private static final String ATTR_ROUTE_COMPLETED_TRAFFIC_LEVEL = "Traffic_Level";
    private static final String ATTR_ROUTE_DISPLAY_POS = "Display_Pos";
    private static final String ATTR_ROUTE_NAV_POS = "Nav_Pos";
    private static final String ATTR_ROUTE_STREET_ADDRESS = "Street_Addr";
    private static final String ATTR_SESSION_ACCELERATION_COUNT = "Session_Hard_Acceleration_Count";
    private static final String ATTR_SESSION_AVERAGE_MPG = "Session_Average_Mpg";
    private static final String ATTR_SESSION_AVERAGE_ROLLING_SPEED = "Session_Average_Rolling_Speed";
    private static final String ATTR_SESSION_AVERAGE_SPEED = "Session_Average_Speed";
    private static final String ATTR_SESSION_DISTANCE = "Session_Distance";
    private static final String ATTR_SESSION_DURATION = "Session_Duration";
    private static final String ATTR_SESSION_EXCESSIVE_SPEEDING_FREQUENCY = "Session_Excessive_Speeding_Frequency";
    private static final String ATTR_SESSION_HARD_BRAKING_COUNT = "Session_Hard_Braking_Count";
    private static final String ATTR_SESSION_HIGH_G_COUNT = "Session_High_G_Count";
    private static final String ATTR_SESSION_MAX_RPM = "Session_Max_RPM";
    private static final String ATTR_SESSION_MAX_SPEED = "Session_Max_Speed";
    private static final String ATTR_SESSION_ROLLING_DURATION = "Session_Rolling_Duration";
    private static final String ATTR_SESSION_SPEEDING_FREQUENCY = "Session_Speeding_Frequency";
    private static final String ATTR_SESSION_TROUBLE_CODE_COUNT = "Session_Trouble_Code_Count";
    private static final String ATTR_SESSION_TROUBLE_CODE_LAST = "Session_Trouble_Code_Last";
    private static final String ATTR_SHUTDOWN_BATTERY_LEVEL = "Battery_Level";
    private static final String ATTR_SHUTDOWN_REASON = "Reason";
    private static final String ATTR_SHUTDOWN_SLEEP_TIME = "Sleep_Time";
    private static final String ATTR_SHUTDOWN_TYPE = "Type";
    private static final String ATTR_SHUTDOWN_UPTIME = "Uptime";
    private static final String ATTR_SMS_MESSAGE_TYPE = "Message_Type";
    private static final String ATTR_SMS_MESSAGE_TYPE_CANNED = "Canned";
    private static final String ATTR_SMS_MESSAGE_TYPE_CUSTOM = "Custom";
    private static final String ATTR_SMS_PLATFORM = "Platform";
    private static final String ATTR_SMS_SUCCESS = "Success";
    private static final String ATTR_SPEEDING_AMOUNT = "Session_Speeding_Amount";
    private static final String ATTR_TEMP_CPU_AVERAGE = "CPU_Average";
    private static final String ATTR_TEMP_CPU_MAX = "CPU_Max";
    private static final String ATTR_TEMP_CPU_TEMP = "CPU_Temperature";
    private static final String ATTR_TEMP_DMD_AVERAGE = "DMD_Average";
    private static final String ATTR_TEMP_DMD_MAX = "DMD_Max";
    private static final String ATTR_TEMP_DMD_TEMP = "DMD_Temperature";
    private static final String ATTR_TEMP_FAN_SPEED = "Fan_Speed";
    private static final String ATTR_TEMP_INLET_AVERAGE = "Inlet_Average";
    private static final String ATTR_TEMP_INLET_MAX = "Inlet_Max";
    private static final String ATTR_TEMP_INLET_TEMP = "Inlet_Temperature";
    private static final String ATTR_TEMP_LED_AVERAGE = "LED_Average";
    private static final String ATTR_TEMP_LED_MAX = "LED_Max";
    private static final String ATTR_TEMP_LED_TEMP = "LED_Temperature";
    private static final String ATTR_TEMP_WARNING = "Warning";
    private static final String ATTR_UPDATE_ACCEPTED = "Accepted";
    private static final String ATTR_UPDATE_TO_VERSION = "Update_To_Version";
    private static final String ATTR_VEHICLE_KILO_METERS = "Vehicle_Kilo_Meters";
    private static final String ATTR_VIN = "Vin";
    private static final String ATTR_VOICE_SEARCH_ADDITIONAL_RESULTS_GLANCE = "Additional_Results_Glance";
    private static final String ATTR_VOICE_SEARCH_CANCEL_TYPE = "Cancel_Type";
    private static final String ATTR_VOICE_SEARCH_CLIENT_VERSION = "Client_Version";
    private static final String ATTR_VOICE_SEARCH_CONFIDENCE_LEVEL = "Confidence_Level";
    private static final String ATTR_VOICE_SEARCH_DESTINATION_TYPE = "Destination_Type";
    private static final String ATTR_VOICE_SEARCH_EXPLICIT_RETRY = "Explicit_Retry";
    private static final String ATTR_VOICE_SEARCH_LIST_SELECTION = "Additional_Results_List_Item_Selected";
    private static final String ATTR_VOICE_SEARCH_MICROPHONE_USED = "Microphone_Used";
    private static final String ATTR_VOICE_SEARCH_NAVIGATION_INITIATED = "Navigation_Initiated";
    private static final String ATTR_VOICE_SEARCH_NAV_CANCELLED_QUICKLY = "Navigation_Cancelled_Quickly";
    private static final String ATTR_VOICE_SEARCH_NUMBER_OF_WORDS = "Number_Of_Words";
    private static final String ATTR_VOICE_SEARCH_NUM_RESULTS = "Num_Total_Results";
    private static final String ATTR_VOICE_SEARCH_PREFIX = "Prefix";
    private static final String ATTR_VOICE_SEARCH_RESPONSE = "Response";
    private static final String ATTR_VOICE_SEARCH_RETRY_COUNT = "Retry_Count";
    private static final String ATTR_VOICE_SEARCH_SUGGESTION = "Suggestion";
    private static final String ATTR_WAKEUP_BATTERY_DRAIN = "Battery_Drain";
    private static final String ATTR_WAKEUP_BATTERY_LEVEL = "Battery_Level";
    private static final String ATTR_WAKEUP_MAX_BATTERY = "Max_Battery";
    private static final String ATTR_WAKEUP_REASON = "Reason";
    private static final String ATTR_WAKEUP_SLEEP_TIME = "Sleep_Time";
    private static final String ATTR_WAKEUP_VIN = "VIN";
    private static final int CUSTOM_DIMENSION_HW_VERSION = 0;
    private static final String DESTINATION_SUGGESTION_TYPE_CALENDAR = "Calendar";
    private static final String DESTINATION_SUGGESTION_TYPE_SMART_SUGGESTION = "SmartSuggestion";
    private static final String FALSE = "false";
    private static final long FUEL_REPORTING_INTERVAL = 180000L;
    private static final String LIGHT_SENSOR_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input";
    private static final String LIGHT_SENSOR_RAW1_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1";
    private static final String LIGHT_SENSOR_RAW_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw";
    private static final int MAX_CPU_TEMP = 93;
    private static final long MAX_DMD_HIGH_TIME_MS;
    private static final int MAX_DMD_TEMP = 65;
    private static final long MAX_INLET_HIGH_TIME_MS;
    private static final int MAX_INLET_TEMP = 72;
    private static final long NAVIGATION_CANCEL_TIMEOUT;
    private static final int NOTEMP = -1000;
    private static final String OFFLINE_MAPS_UPDATE_COUNT = "offlineMapsUpdateCount";
    private static final int PREF_CHANGE_DELAY_MS = 15000;
    private static final String PREVIOUS_OFFLINE_MAPS_DATE = "previousOfflineMapsDate";
    private static final String PREVIOUS_OFFLINE_MAPS_PACKAGES = "previousOfflineMapsPackages";
    private static final String PREVIOUS_OFFLINE_MAPS_VERSION = "previousOfflineMapsVersion";
    private static final long TEMP_WARNING_BOOT_DELAY_S;
    private static final long TEMP_WARNING_INTERVAL_MS;
    private static final String TRUE = "true";
    private static final long UPLOAD_INTERVAL;
    private static final long UPLOAD_INTERVAL_BOOT;
    private static final long UPLOAD_INTERVAL_LIMITED_BANDWIDTH;
    private static final boolean VERBOSE = false;
    private static int currentCpuAvgTemperature;
    private static ArrayList<DeferredLocalyticsEvent> deferredEvents;
    private static DeferredWakeup deferredWakeup;
    private static boolean destinationSet;
    private static boolean disconnectDueToForceReconnect;
    private static String glanceNavigationSource;
    private static Handler handler;
    private static int lastDiscReason;
    private static String lastForceReconnectReason;
    private static volatile boolean localyticsInitialized;
    private static int nearbySearchDestScrollPosition;
    private static PlaceType nearbySearchPlaceType;
    private static boolean newDevicePaired;
    private static boolean phonePaired;
    private static boolean quietMode;
    private static DeviceInfo remoteDevice;
    private static FuelMonitor sFuelMonitor;
    private static PreferenceWatcher sLastNavigationPreferences;
    private static DriverProfile sLastProfile;
    private static PreferenceWatcher sLastProfilePreferences;
    private static String sLastVIN;
    private static Logger sLogger;
    private static NotificationReceiver sNotificationReceiver;
    private static ObdManager sObdManager;
    private static String sRemoteDeviceId;
    private static RemoteDeviceManager sRemoteDeviceManager;
    private static Runnable sUploader;
    private static long startTime;
    private static double startingBatteryVoltage;
    private static int startingDistance;
    private static VoiceResultsRunnable voiceResultsRunnable;
    private static VoiceSearchAdditionalResultsAction voiceSearchAdditionalResultsAction;
    private static Integer voiceSearchConfidence;
    private static int voiceSearchCount;
    private static boolean voiceSearchExplicitRetry;
    private static Integer voiceSearchListSelection;
    private static boolean voiceSearchListeningOverBluetooth;
    private static String voiceSearchPrefix;
    private static VoiceSearchProgress voiceSearchProgress;
    private static List<Destination> voiceSearchResults;
    private static String voiceSearchWords;
    private static long welcomeScreenTime;
    
    static {
        AnalyticsSupport.sLogger = new Logger(AnalyticsSupport.class);
        AnalyticsSupport.sRemoteDeviceManager = RemoteDeviceManager.getInstance();
        AnalyticsSupport.remoteDevice = null;
        AnalyticsSupport.sLastProfile = null;
        AnalyticsSupport.lastDiscReason = 0;
        AnalyticsSupport.lastForceReconnectReason = "";
        AnalyticsSupport.disconnectDueToForceReconnect = false;
        AnalyticsSupport.sLastProfilePreferences = null;
        AnalyticsSupport.sLastNavigationPreferences = null;
        AnalyticsSupport.sLastVIN = null;
        AnalyticsSupport.sRemoteDeviceId = null;
        AnalyticsSupport.handler = new Handler(Looper.getMainLooper());
        AnalyticsSupport.localyticsInitialized = false;
        AnalyticsSupport.quietMode = false;
        AnalyticsSupport.startingBatteryVoltage = -1.0;
        UPLOAD_INTERVAL_BOOT = TimeUnit.MINUTES.toMillis(5L);
        UPLOAD_INTERVAL = TimeUnit.MINUTES.toMillis(5L);
        UPLOAD_INTERVAL_LIMITED_BANDWIDTH = TimeUnit.MINUTES.toMillis(20L);
        AnalyticsSupport.sUploader = new Runnable() {
            @Override
            public void run() {
                if (NetworkBandwidthController.getInstance().isNetworkAccessAllowed(NetworkBandwidthController.Component.LOCALYTICS)) {
                    AnalyticsSupport.sLogger.v("called Localytics.upload");
                    Localytics.upload();
                }
                AnalyticsSupport.handler.postDelayed((Runnable)this, getUpdateInterval());
            }
        };
        AnalyticsSupport.glanceNavigationSource = null;
        TEMP_WARNING_BOOT_DELAY_S = TimeUnit.MINUTES.toSeconds(3L);
        TEMP_WARNING_INTERVAL_MS = TimeUnit.MINUTES.toMillis(10L);
        MAX_INLET_HIGH_TIME_MS = TimeUnit.MINUTES.toMillis(2L);
        MAX_DMD_HIGH_TIME_MS = TimeUnit.MINUTES.toMillis(10L);
        AnalyticsSupport.currentCpuAvgTemperature = -1;
        AnalyticsSupport.nearbySearchDestScrollPosition = -1;
        AnalyticsSupport.nearbySearchPlaceType = null;
        AnalyticsSupport.welcomeScreenTime = -1L;
        AnalyticsSupport.newDevicePaired = false;
        AnalyticsSupport.phonePaired = false;
        NAVIGATION_CANCEL_TIMEOUT = TimeUnit.MINUTES.toMillis(1L);
        AnalyticsSupport.voiceSearchProgress = null;
        AnalyticsSupport.voiceSearchWords = null;
        AnalyticsSupport.voiceSearchConfidence = null;
        AnalyticsSupport.voiceSearchCount = 0;
        AnalyticsSupport.voiceSearchListeningOverBluetooth = false;
        AnalyticsSupport.voiceSearchResults = null;
        AnalyticsSupport.voiceSearchAdditionalResultsAction = null;
        AnalyticsSupport.voiceSearchListSelection = null;
        AnalyticsSupport.voiceSearchExplicitRetry = false;
        AnalyticsSupport.voiceSearchPrefix = null;
        AnalyticsSupport.voiceResultsRunnable = null;
        AnalyticsSupport.destinationSet = false;
        AnalyticsSupport.deferredWakeup = null;
        AnalyticsSupport.deferredEvents = null;
    }
    
    public static void analyticsApplicationInit(final Application application) {
        application.registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)new LocalyticsActivityLifecycleCallbacks((Context)application));
        Localytics.setPushDisabled(true);
        setupCustomDimensions();
        AnalyticsSupport.sNotificationReceiver = new NotificationReceiver();
        AnalyticsSupport.startTime = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime());
        final TemperatureReporter temperatureReporter = new TemperatureReporter();
        temperatureReporter.setName("TemperatureReporter");
        temperatureReporter.start();
        Mortar.inject(HudApplication.getAppContext(), temperatureReporter);
        AnalyticsSupport.sObdManager = ObdManager.getInstance();
        AnalyticsSupport.sFuelMonitor = new FuelMonitor();
        AnalyticsSupport.quietMode = AnalyticsSupport.sNotificationReceiver.powerManager.inQuietMode();
        maybeOpenSession();
        final IntentFilter intentFilter = new IntentFilter("android.bluetooth.device.action.ACL_DISCONNECTED");
        intentFilter.addAction("com.navdy.hud.app.force_reconnect");
        HudApplication.getAppContext().registerReceiver((BroadcastReceiver)new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if ("android.bluetooth.device.action.ACL_DISCONNECTED".equals(action)) {
                    final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                    if (bluetoothDevice != null && (bluetoothDevice.getType() == 1 || bluetoothDevice.getType() == 3)) {
                        AnalyticsSupport.lastDiscReason = intent.getIntExtra("android.bluetooth.device.extra.STATUS", 0);
                        AnalyticsSupport.sLogger.d("ACTION_ACL_DISCONNECTED, Disconnect reason " + AnalyticsSupport.lastDiscReason + " " + intent);
                    }
                    else {
                        AnalyticsSupport.sLogger.d("ACTION_ACL_DISCONNECTED, Non classic device , Disconnect reason " + intent.getIntExtra("android.bluetooth.device.extra.STATUS", 0));
                    }
                }
                else if ("com.navdy.hud.app.force_reconnect".equals(action)) {
                    AnalyticsSupport.disconnectDueToForceReconnect = true;
                    AnalyticsSupport.lastForceReconnectReason = intent.getStringExtra("force_reconnect_reason");
                    AnalyticsSupport.sLogger.d("ACTION_DEVICE_FORCE_RECONNECT, Reason : " + AnalyticsSupport.lastForceReconnectReason + " " + intent);
                }
            }
        }, intentFilter);
    }
    
    private static void clearVoiceSearch() {
        AnalyticsSupport.voiceSearchProgress = null;
        AnalyticsSupport.voiceSearchWords = null;
        AnalyticsSupport.voiceSearchConfidence = null;
        AnalyticsSupport.voiceSearchListeningOverBluetooth = false;
        AnalyticsSupport.voiceSearchResults = null;
        AnalyticsSupport.voiceSearchAdditionalResultsAction = null;
        AnalyticsSupport.voiceSearchExplicitRetry = false;
        AnalyticsSupport.voiceSearchListSelection = null;
        AnalyticsSupport.voiceSearchPrefix = null;
    }
    
    private static void closeSession() {
        synchronized (AnalyticsSupport.class) {
            if (AnalyticsSupport.localyticsInitialized) {
                sendDistance();
                AnalyticsSupport.sLogger.v("closeSession()");
                Localytics.closeSession();
                AnalyticsSupport.sRemoteDeviceId = "unknown";
            }
        }
    }
    
    private static int countWords(String trim) {
        int n = 0;
        int n2 = 0;
        if (trim != null) {
            trim = trim.trim();
            int n3 = 0;
            final int length = trim.length();
            while (true) {
                n = n2;
                if (n3 >= length) {
                    break;
                }
                final int n4 = n2 + 1;
                int n5;
                while (true) {
                    n5 = n3;
                    if (n3 >= length) {
                        break;
                    }
                    n5 = n3;
                    if (trim.charAt(n3) == ' ') {
                        break;
                    }
                    ++n3;
                }
                while (true) {
                    n2 = n4;
                    if ((n3 = n5) >= length) {
                        break;
                    }
                    n2 = n4;
                    n3 = n5;
                    if (trim.charAt(n5) != ' ') {
                        break;
                    }
                    ++n5;
                }
            }
        }
        return n;
    }
    
    private static boolean deferLocalyticsEvent(final String s, final String[] array) {
        synchronized (AnalyticsSupport.class) {
            boolean b;
            if (!AnalyticsSupport.localyticsInitialized) {
                if (AnalyticsSupport.deferredEvents == null) {
                    AnalyticsSupport.deferredEvents = new ArrayList<DeferredLocalyticsEvent>(5);
                }
                AnalyticsSupport.deferredEvents.add(new DeferredLocalyticsEvent(s, array));
                b = true;
            }
            else {
                b = false;
            }
            return b;
        }
    }
    
    private static String getClientVersion() {
        final DeviceInfo remoteDeviceInfo = AnalyticsSupport.sRemoteDeviceManager.getRemoteDeviceInfo();
        String clientVersion;
        if (remoteDeviceInfo == null) {
            clientVersion = null;
        }
        else {
            clientVersion = remoteDeviceInfo.clientVersion;
        }
        String s = clientVersion;
        if (clientVersion == null) {
            AnalyticsSupport.sLogger.e("failed to get client version");
            s = "";
        }
        return s;
    }
    
    public static int getCurrentTemperature() {
        return AnalyticsSupport.currentCpuAvgTemperature;
    }
    
    private static String getHwVersion() {
        return SerialNumber.instance.configurationCode + SerialNumber.instance.revisionCode;
    }
    
    private static long getUpdateInterval() {
        long n;
        if (NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) {
            n = AnalyticsSupport.UPLOAD_INTERVAL_LIMITED_BANDWIDTH;
        }
        else {
            n = AnalyticsSupport.UPLOAD_INTERVAL;
        }
        return n;
    }
    
    private static String getVoiceSearchEvent() {
        String string;
        if (AnalyticsSupport.remoteDevice == null) {
            string = null;
        }
        else {
            string = AnalyticsSupport.remoteDevice.platform.toString();
        }
        String s = null;
        if (string != null) {
            if (string.equals("PLATFORM_Android")) {
                s = "Voice_Search_Android";
            }
            else {
                s = "Voice_Search_iOS";
            }
        }
        return s;
    }
    
    private static String isEnabled(final boolean b) {
        String s;
        if (b) {
            s = "Enabled";
        }
        else {
            s = "Disabled";
        }
        return s;
    }
    
    private static boolean isTimeValid() {
        boolean b = true;
        if (Calendar.getInstance().get(1) < 2016) {
            b = false;
        }
        return b;
    }
    
    private static void localyticsSendEvent(final Event event, final boolean b) {
        if (b) {
            Localytics.tagEvent(event.tag, event.argMap);
        }
        else {
            TaskManager.getInstance().execute(new EventSender(event), 1);
        }
    }
    
    private static void localyticsSendEvent(final String s, final boolean b, final boolean b2, final String... array) {
        if (b2 || !deferLocalyticsEvent(s, array)) {
            final Event event = new Event(s, array);
            final Map<String, String> argMap = event.argMap;
            if (b) {
                final DeviceInfo remoteDeviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
                if (remoteDeviceInfo != null) {
                    if (remoteDeviceInfo.platform != null) {
                        argMap.put("Remote_Device_Platform", remoteDeviceInfo.platform.name());
                    }
                    argMap.put("Remote_Device_Hardware", remoteDeviceInfo.deviceMake + " " + remoteDeviceInfo.model);
                    argMap.put("Remote_Device_SW_Version", remoteDeviceInfo.deviceMake + " " + remoteDeviceInfo.systemVersion);
                }
            }
            localyticsSendEvent(event, b2);
        }
    }
    
    private static void localyticsSendEvent(final String s, final boolean b, final String... array) {
        localyticsSendEvent(s, b, false, array);
    }
    
    public static void localyticsSendEvent(final String s, final String... array) {
        localyticsSendEvent(s, false, false, array);
    }
    
    private static void maybeOpenSession() {
        if (!AnalyticsSupport.quietMode & isTimeValid()) {
            openSession();
        }
    }
    
    private static String normalizeName(final String s) {
        final char[] charArray = s.toCharArray();
        int n = 1;
        for (int i = 0; i < charArray.length; ++i) {
            char upperCase;
            final char c = upperCase = charArray[i];
            if (n != 0) {
                upperCase = Character.toUpperCase(c);
            }
            if (upperCase == '_') {
                n = 1;
            }
            else {
                n = 0;
            }
            charArray[i] = upperCase;
        }
        return new String(charArray);
    }
    
    private static void openSession() {
        synchronized (AnalyticsSupport.class) {
            if (!AnalyticsSupport.localyticsInitialized) {
                AnalyticsSupport.sLogger.i("openSession()");
                Localytics.openSession();
                AnalyticsSupport.localyticsInitialized = true;
                sendDeferredLocalyticsEvents();
                AnalyticsSupport.handler.postDelayed((Runnable)AnalyticsSupport.sFuelMonitor, 180000L);
                AnalyticsSupport.handler.postDelayed(AnalyticsSupport.sUploader, AnalyticsSupport.UPLOAD_INTERVAL_BOOT);
            }
        }
    }
    
    private static int parseTemp(final String s) {
        final int n = -1000;
        try {
            return Integer.parseInt(s);
        }
        catch (NumberFormatException ex) {
            AnalyticsSupport.sLogger.e("invalid temperature: " + s);
            return n;
        }
    }
    
    private static String pidListToHexString(final List<Pid> list) {
        final long[] array = new long[5];
        final Iterator<Pid> iterator = list.iterator();
        while (iterator.hasNext()) {
            final int id = iterator.next().getId();
            final int n = id / 64;
            array[n] |= 1L << id % 64;
        }
        String s = "";
        String string = "";
        for (int i = array.length - 1; i >= 0; --i) {
            string = string + s + String.format(Locale.US, "%08x", array[i]);
            s = " ";
        }
        return string;
    }
    
    private static void processNavigationArrivalEvent(final MapEvents.ArrivalEvent arrivalEvent) {
        if (AnalyticsSupport.nearbySearchPlaceType != null) {
            localyticsSendEvent("Nearby_Search_Arrived", "Place_Type", AnalyticsSupport.nearbySearchPlaceType.toString());
            AnalyticsSupport.nearbySearchPlaceType = null;
        }
    }
    
    private static void processNavigationRouteRequest(final NavigationRouteRequest navigationRouteRequest) {
        if (AnalyticsSupport.nearbySearchPlaceType != null) {
            localyticsSendEvent("Nearby_Search_Trip_Initiated", new String[0]);
        }
        else {
            boolean b;
            if (AnalyticsSupport.voiceSearchProgress == VoiceSearchProgress.PROGRESS_ROUTE_SELECTED || AnalyticsSupport.voiceSearchProgress == VoiceSearchProgress.PROGRESS_DESTINATION_FOUND) {
                b = true;
            }
            else {
                b = false;
            }
            if (AnalyticsSupport.voiceResultsRunnable != null) {
                final VoiceResultsRunnable voiceResultsRunnable = AnalyticsSupport.voiceResultsRunnable;
                VoiceSearchCancelReason voiceSearchCancelReason;
                if (b) {
                    voiceSearchCancelReason = VoiceSearchCancelReason.new_voice_search;
                }
                else {
                    voiceSearchCancelReason = VoiceSearchCancelReason.new_non_voice_search;
                }
                voiceResultsRunnable.cancel(voiceSearchCancelReason);
            }
            if (b) {
                AnalyticsSupport.voiceResultsRunnable = new VoiceResultsRunnable(navigationRouteRequest);
                AnalyticsSupport.handler.postDelayed((Runnable)AnalyticsSupport.voiceResultsRunnable, AnalyticsSupport.NAVIGATION_CANCEL_TIMEOUT);
                clearVoiceSearch();
            }
            else if (AnalyticsSupport.voiceSearchProgress != null) {
                AnalyticsSupport.sLogger.w("Received NavigationRouteRequest with unexpected voice progress state: " + AnalyticsSupport.voiceSearchProgress);
            }
            AnalyticsSupport.voiceSearchCount = 0;
        }
    }
    
    private static String readLightSensor(String convertFileToString) {
        final String s = "";
        try {
            convertFileToString = IOUtils.convertFileToString(convertFileToString);
            return convertFileToString.trim();
        }
        catch (Exception ex) {
            AnalyticsSupport.sLogger.e("exception reading light sensor device: " + convertFileToString, ex);
            convertFileToString = s;
            return convertFileToString.trim();
        }
    }
    
    public static void recordAdaptiveAutobrightnessAdjustmentChanged(final int n) {
        localyticsSendEvent("Adaptive_Brightness_Adjustment_Change", "Brightness", Integer.toString(n), "Light_Sensor", readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input"), "Raw_Sensor", readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw"), "Raw1_Sensor", readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1"));
    }
    
    public static void recordAutobrightnessAdjustmentChanged(final int n, final int n2, final int n3) {
        if (n2 != n3) {
            localyticsSendEvent("Brightness_Adjustment_Change", "Brightness", Integer.toString(n), "Previous", Integer.toString(n2), "New", Integer.toString(n3), "Light_Sensor", readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input"), "Raw_Sensor", readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw"), "Raw1_Sensor", readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1"));
        }
    }
    
    public static void recordBadRoutePosition(final String s, final String s2, final String s3) {
        localyticsSendEvent("Route_Bad_Pos", "Display_Pos", s, "Nav_Pos", s2, "Street_Addr", s3);
    }
    
    public static void recordBrightnessChanged(final int n, final int n2) {
        if (n != n2) {
            localyticsSendEvent("Brightness_Change", "Previous", Integer.toString(n), "New", Integer.toString(n2), "Light_Sensor", readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input"), "Raw_Sensor", readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw"), "Raw1_Sensor", readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1"));
        }
    }
    
    public static void recordCpuOverheat(final String s) {
        final String string = Long.toString(uptime());
        AnalyticsSupport.sLogger.i("Recording CPU overheat event uptime:" + string + " state:" + s);
        localyticsSendEvent("Cpu_Overheat", "Uptime", string, "State", s);
    }
    
    public static void recordDashPreference(final String s, final boolean b) {
        String s2;
        if (b) {
            s2 = "Left";
        }
        else {
            s2 = "Right";
        }
        localyticsSendEvent("Dash_Gauge_Setting", "Side", s2, "Gauge_name", s);
    }
    
    public static void recordDestinationSuggestion(final boolean b, final boolean b2) {
        String s;
        if (b) {
            s = "true";
        }
        else {
            s = "false";
        }
        String s2;
        if (b2) {
            s2 = "Calendar";
        }
        else {
            s2 = "SmartSuggestion";
        }
        localyticsSendEvent("Destination_Suggestion_Show", "Accepted", s, "Type", s2);
    }
    
    public static void recordDownloadOTAUpdate(final SharedPreferences sharedPreferences) {
        localyticsSendEvent("OTA_Download", "Current_Version", Build$VERSION.INCREMENTAL, "Update_Version", OTAUpdateService.getIncrementalUpdateVersion(sharedPreferences));
    }
    
    public static void recordGlanceAction(final String s, final INotification notification, final String s2) {
        String glanceNavigationSource = s2;
        if (s2 == null) {
            if (AnalyticsSupport.glanceNavigationSource != null) {
                glanceNavigationSource = AnalyticsSupport.glanceNavigationSource;
            }
            else {
                glanceNavigationSource = "unknown";
            }
        }
        AnalyticsSupport.glanceNavigationSource = null;
        if (notification != null) {
            final NotificationType type = notification.getType();
            String s3;
            if (notification instanceof GlanceNotification) {
                s3 = ((GlanceNotification)notification).getGlanceApp().toString();
            }
            else {
                s3 = type.toString();
            }
            localyticsSendEvent(s, "Glance_Name", s3, "Nav_Method", glanceNavigationSource);
        }
        else {
            localyticsSendEvent(s, "Nav_Method", glanceNavigationSource);
        }
    }
    
    public static void recordGlympseSent(final boolean b, final String s, final String s2, final String s3) {
        AnalyticsSupport.sLogger.v("glympse sent id=" + s3 + " sucess=" + b + " share=" + s + " msg=" + s2);
        localyticsSendEvent("Glympse_Sent", "Success", String.valueOf(b), "Share", s, "Message", s2, "Id", s3);
    }
    
    public static void recordGlympseViewed(final String s) {
        AnalyticsSupport.sLogger.v("glympse viewed id=" + s);
        localyticsSendEvent("Glympse_Viewed", "Id", s);
    }
    
    public static void recordIncorrectFuelData() {
        String carMake = null;
        String carModel = null;
        String carYear = null;
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        if (currentProfile != null) {
            carMake = currentProfile.getCarMake();
            carModel = currentProfile.getCarModel();
            carYear = currentProfile.getCarYear();
        }
        localyticsSendEvent("OBD_Zero_Fuel_Level", "Car_Make", carMake, "Car_Model", carModel, "Car_Year", carYear);
    }
    
    public static void recordInstallOTAUpdate(final SharedPreferences sharedPreferences) {
        localyticsSendEvent("OTA_Install", "Current_Version", Build$VERSION.INCREMENTAL, "Update_Version", OTAUpdateService.getIncrementalUpdateVersion(sharedPreferences));
    }
    
    public static void recordKeyFailure(final String s, final String s2) {
        localyticsSendEvent("Key_Failed", "Key_Type", s, "Error", s2);
    }
    
    public static void recordMenuSelection(final String s) {
        localyticsSendEvent("Menu_Selection", "Name", s);
    }
    
    public static void recordMusicAction(final String s) {
        localyticsSendEvent("Music_Action", "Type", s);
    }
    
    public static void recordMusicBrowsePlayAction(final String s) {
        localyticsSendEvent(s + "_played", true, new String[0]);
    }
    
    public static void recordNavdyMileageEvent(final double n, final double n2, final double n3) {
        localyticsSendEvent("Navdy_Mileage", "Vehicle_Kilo_Meters", Double.toString(n), "Navdy_Kilometers", Double.toString(n2), "Navdy_Usage_Rate", Double.toString(n3));
    }
    
    public static void recordNavigation(final boolean b) {
        if (b) {
            localyticsSendEvent("Navigation_Destination_Set", new String[0]);
            AnalyticsSupport.destinationSet = true;
        }
    }
    
    public static void recordNavigationCancelled() {
        if (AnalyticsSupport.nearbySearchPlaceType != null) {
            localyticsSendEvent("Nearby_Search_Ended", new String[0]);
            AnalyticsSupport.sLogger.v("navigation ended for nearby search trip");
            AnalyticsSupport.nearbySearchPlaceType = null;
        }
        else if (AnalyticsSupport.voiceResultsRunnable != null) {
            AnalyticsSupport.sLogger.v("navigation ended, cancelling voice search callback");
            AnalyticsSupport.voiceResultsRunnable.cancel(VoiceSearchCancelReason.end_trip);
        }
    }
    
    private static Event recordNavigationPreference(final DriverProfile driverProfile) {
        final NavigationPreferences navigationPreferences = driverProfile.getNavigationPreferences();
        String s;
        if (navigationPreferences.routingType == NavigationPreferences.RoutingType.ROUTING_FASTEST) {
            s = "Fastest";
        }
        else {
            s = "Shortest";
        }
        String s2 = "Unknown";
        switch (navigationPreferences.rerouteForTraffic) {
            case REROUTE_AUTOMATIC:
                s2 = "Automatic";
                break;
            case REROUTE_CONFIRM:
                s2 = "Confirm";
                break;
            case REROUTE_NEVER:
                s2 = "Never";
                break;
        }
        return new Event("Navigation_Preferences", new String[] { "Reroute_For_Traffic", s2, "Routing_Type", s, "Spoken_Turn_By_Turn", isEnabled(navigationPreferences.spokenTurnByTurn), "Spoken_Speed_Warnings", isEnabled(navigationPreferences.spokenSpeedLimitWarnings), "Show_Traffic_Open_Map", isEnabled(navigationPreferences.showTrafficInOpenMap), "Show_Traffic_Navigating", isEnabled(navigationPreferences.showTrafficWhileNavigating), "Allow_Highways", isEnabled(navigationPreferences.allowHighways), "Allow_Toll_Roads", isEnabled(navigationPreferences.allowTollRoads), "Allow_Ferries", isEnabled(navigationPreferences.allowFerries), "Allow_Tunnels", isEnabled(navigationPreferences.allowTunnels), "Allow_Unpaved_Roads", isEnabled(navigationPreferences.allowUnpavedRoads), "Allow_Auto_Trains", isEnabled(navigationPreferences.allowAutoTrains), "Allow_HOV_Lanes", isEnabled(navigationPreferences.allowHOVLanes) });
    }
    
    public static void recordNavigationPreferenceChange(final DriverProfile driverProfile) {
        if (AnalyticsSupport.sLastNavigationPreferences != null) {
            AnalyticsSupport.sLastNavigationPreferences.enqueue(driverProfile);
        }
    }
    
    public static void recordNearbySearchClosed() {
        localyticsSendEvent("Nearby_Search_Closed", new String[0]);
    }
    
    public static void recordNearbySearchDestinationScroll(final int nearbySearchDestScrollPosition) {
        if (nearbySearchDestScrollPosition > AnalyticsSupport.nearbySearchDestScrollPosition) {
            AnalyticsSupport.nearbySearchDestScrollPosition = nearbySearchDestScrollPosition;
        }
    }
    
    public static void recordNearbySearchDismiss() {
        localyticsSendEvent("Nearby_Search_Dismiss", new String[0]);
    }
    
    public static void recordNearbySearchNoResult(final boolean b) {
        String s;
        if (b) {
            s = "Retry";
        }
        else {
            s = "Dismiss";
        }
        localyticsSendEvent("Nearby_Search_No_Results", "Action", s);
    }
    
    public static void recordNearbySearchResultsClose() {
        localyticsSendEvent("Nearby_Search_Results_Close", new String[0]);
    }
    
    public static void recordNearbySearchResultsDismiss() {
        localyticsSendEvent("Nearby_Search_Results_Dismiss", new String[0]);
    }
    
    public static void recordNearbySearchResultsView() {
        localyticsSendEvent("Nearby_Search_Results_View", new String[0]);
        AnalyticsSupport.nearbySearchDestScrollPosition = -1;
        AnalyticsSupport.nearbySearchPlaceType = null;
    }
    
    public static void recordNearbySearchReturnMainMenu() {
        recordMenuSelection("back");
        localyticsSendEvent("Nearby_Search_Return_Main_Menu", new String[0]);
    }
    
    public static void recordNearbySearchSelection(final PlaceType nearbySearchPlaceType, final int n) {
        localyticsSendEvent("Menu_Selection", "Name", "nearby_places_results");
        final String string = nearbySearchPlaceType.toString();
        final String string2 = Integer.toString(n);
        String s;
        if (AnalyticsSupport.nearbySearchDestScrollPosition > 0) {
            s = "true";
        }
        else {
            s = "false";
        }
        localyticsSendEvent("Nearby_Search_Selection", "Place_Type", string, "Selected_Position", string2, "Scrolled", s);
        AnalyticsSupport.nearbySearchPlaceType = nearbySearchPlaceType;
    }
    
    private static void recordNewDevicePaired() {
        AnalyticsSupport.newDevicePaired = true;
    }
    
    public static void recordOTAInstallResult(final boolean b, final String s, final boolean b2) {
        String s2;
        if (b) {
            s2 = "true";
        }
        else {
            s2 = "false";
        }
        String s3;
        if (b2) {
            s3 = "true";
        }
        else {
            s3 = "false";
        }
        localyticsSendEvent("OTA_Install_Result", "Incremental", s2, "Prior_Version", s, "Success", s3);
    }
    
    public static void recordObdCanBusDataSent(final String s) {
        if (AnalyticsSupport.sLastProfile != null) {
            localyticsSendEvent("OBD_Listening_Posted", "Car_Make", AnalyticsSupport.sLastProfile.getCarMake(), "Car_Model", AnalyticsSupport.sLastProfile.getCarModel(), "Car_Year", AnalyticsSupport.sLastProfile.getCarYear(), "Version", s);
        }
        else {
            localyticsSendEvent("OBD_Listening_Posted", "Version", s);
        }
    }
    
    public static void recordObdCanBusMonitoringState(final String s, final boolean b, final int n, final int n2) {
        if (AnalyticsSupport.sLastProfile != null) {
            final String carMake = AnalyticsSupport.sLastProfile.getCarMake();
            final String carModel = AnalyticsSupport.sLastProfile.getCarModel();
            final String carYear = AnalyticsSupport.sLastProfile.getCarYear();
            String s2;
            if (b) {
                s2 = "true";
            }
            else {
                s2 = "false";
            }
            localyticsSendEvent("OBD_Listening_Status", "Vin", s, "Car_Make", carMake, "Car_Model", carModel, "Car_Year", carYear, "Success", s2, "Version", Integer.toString(n2), "Data_Size", Integer.toString(n));
        }
        else {
            String s3;
            if (b) {
                s3 = "true";
            }
            else {
                s3 = "false";
            }
            localyticsSendEvent("OBD_Listening_Status", "Vin", s, "Success", s3, "Version", Integer.toString(n2), "Data_Size", Integer.toString(n));
        }
    }
    
    private static void recordOfflineMapsChange(final SharedPreferences sharedPreferences) {
        final String string = sharedPreferences.getString("previousOfflineMapsVersion", (String)null);
        final String string2 = sharedPreferences.getString("previousOfflineMapsPackages", (String)null);
        final String string3 = sharedPreferences.getString("previousOfflineMapsDate", (String)null);
        final HereMapsManager instance = HereMapsManager.getInstance();
        if (instance.isInitialized()) {
            final HereOfflineMapsVersion offlineMapsVersion = instance.getOfflineMapsVersion();
            if (offlineMapsVersion != null) {
                final List<String> packages = offlineMapsVersion.getPackages();
                String join;
                if (packages != null) {
                    join = TextUtils.join((CharSequence)",", (Iterable)packages);
                }
                else {
                    join = "";
                }
                String version = offlineMapsVersion.getVersion();
                String rawDate = offlineMapsVersion.getRawDate();
                if (version == null) {
                    version = "";
                }
                if (rawDate == null) {
                    rawDate = "";
                }
                if (!join.equals(string2) || !rawDate.equals(string3) || !version.equals(string)) {
                    final int n = sharedPreferences.getInt("offlineMapsUpdateCount", 0) + 1;
                    final Event event = new Event("Offline_Maps_Updated", new String[] { "Old_Date", string3, "Old_Version", string, "Old_Packages", string2, "New_Version", version, "New_Packages", join, "New_Date", rawDate, "Update_Count", String.valueOf(n) });
                    AnalyticsSupport.sLogger.i("Recording offline maps change:" + event);
                    localyticsSendEvent(event, false);
                    sharedPreferences.edit().putString("previousOfflineMapsDate", rawDate).putString("previousOfflineMapsPackages", join).putString("previousOfflineMapsVersion", version).putInt("offlineMapsUpdateCount", n).apply();
                }
            }
        }
    }
    
    public static void recordOptionSelection(final String s) {
        localyticsSendEvent("Option_Selected", "Name", s);
    }
    
    public static void recordPhoneDisconnect(final boolean b, final String s) {
        final boolean b2 = SpeedManager.getInstance().getCurrentSpeed() > 0;
        if (AnalyticsSupport.remoteDevice != null) {
            String carMake = null;
            String carModel = null;
            String carYear = null;
            if (AnalyticsSupport.sLastProfile != null) {
                carMake = AnalyticsSupport.sLastProfile.getCarMake();
                carModel = AnalyticsSupport.sLastProfile.getCarModel();
                carYear = AnalyticsSupport.sLastProfile.getCarYear();
            }
            String s2;
            if (b) {
                s2 = "reconnected";
            }
            else {
                s2 = "failed";
            }
            localyticsSendEvent("Phone_Disconnected", "Status", s2, "Reason", Integer.toString(AnalyticsSupport.lastDiscReason), "Force_Reconnect", Boolean.toString(AnalyticsSupport.disconnectDueToForceReconnect), "Force_Reconnect_Reason", AnalyticsSupport.lastForceReconnectReason, "While_Moving", Boolean.toString(b2), "Phone_Type", AnalyticsSupport.remoteDevice.platform.toString(), "Phone_OS", AnalyticsSupport.remoteDevice.systemVersion, "Phone_Make", AnalyticsSupport.remoteDevice.deviceMake, "Phone_Model", AnalyticsSupport.remoteDevice.model, "Elapsed_Time", s, "Car_Make", carMake, "Car_Model", carModel, "Car_Year", carYear, "VIN", AnalyticsSupport.sLastVIN);
            AnalyticsSupport.sLastVIN = null;
            AnalyticsSupport.sLastProfile = null;
            AnalyticsSupport.remoteDevice = null;
            AnalyticsSupport.lastDiscReason = 0;
            AnalyticsSupport.lastForceReconnectReason = "";
            AnalyticsSupport.disconnectDueToForceReconnect = false;
        }
    }
    
    private static Event recordPreference(final DriverProfile driverProfile) {
        final InputPreferences inputPreferences = driverProfile.getInputPreferences();
        final NotificationPreferences notificationPreferences = driverProfile.getNotificationPreferences();
        final LocalPreferences localPreferences = driverProfile.getLocalPreferences();
        String s;
        if (driverProfile.getUnitSystem() == DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_IMPERIAL) {
            s = "Imperial";
        }
        else {
            s = "Metric";
        }
        String s2;
        if (driverProfile.getDisplayFormat() == DriverProfilePreferences.DisplayFormat.DISPLAY_FORMAT_COMPACT) {
            s2 = "Compact";
        }
        else {
            s2 = "Normal";
        }
        String s3 = "Unknown";
        switch (driverProfile.getFeatureMode()) {
            case FEATURE_MODE_BETA:
                s3 = "Beta";
                break;
            case FEATURE_MODE_RELEASE:
                s3 = "Release";
                break;
            case FEATURE_MODE_EXPERIMENTAL:
                s3 = "Experimental";
                break;
        }
        String s4;
        if (!notificationPreferences.enabled) {
            s4 = "Disabled";
        }
        else if (notificationPreferences.readAloud && !notificationPreferences.showContent) {
            s4 = "Read_Aloud";
        }
        else if (!notificationPreferences.readAloud && notificationPreferences.showContent) {
            s4 = "Show_Content";
        }
        else {
            s4 = "Read_and_Show";
        }
        final DriverProfilePreferences.ObdScanSetting obdScanSetting = driverProfile.getObdScanSetting();
        final String string = driverProfile.getLocale().toString();
        final String enabled = isEnabled(driverProfile.isAutoOnEnabled());
        final String enabled2 = isEnabled(inputPreferences.use_gestures);
        String name;
        if (obdScanSetting != null) {
            name = obdScanSetting.name();
        }
        else {
            name = "null";
        }
        return new Event("Driver_Preferences", new String[] { "Locale", string, "Units", s, "Auto_On", enabled, "Display_Format", s2, "Feature_Mode", s3, "Gestures", enabled2, "Glances", s4, "OBD", name, "Limit_Bandwidth", isEnabled(driverProfile.isLimitBandwidthModeOn()), "Manual_Zoom", isEnabled(localPreferences.manualZoom) });
    }
    
    public static void recordPreferenceChange(final DriverProfile driverProfile) {
        if (AnalyticsSupport.sLastProfilePreferences != null) {
            AnalyticsSupport.sLastProfilePreferences.enqueue(driverProfile);
        }
    }
    
    public static void recordRouteSelectionCancelled() {
        if (AnalyticsSupport.nearbySearchPlaceType != null) {
            AnalyticsSupport.sLogger.v("nearby search route calculation cancelled");
            localyticsSendEvent("Nearby_Search_Trip_Cancelled", new String[0]);
            AnalyticsSupport.nearbySearchPlaceType = null;
        }
        if (AnalyticsSupport.voiceSearchProgress == null) {
            if (AnalyticsSupport.voiceResultsRunnable != null) {
                AnalyticsSupport.sLogger.v("route calculation cancelled");
                AnalyticsSupport.voiceResultsRunnable.cancel(VoiceSearchCancelReason.cancel_route_calculation);
            }
        }
        else {
            VoiceSearchCancelReason voiceSearchCancelReason = null;
            if (AnalyticsSupport.voiceSearchProgress == VoiceSearchProgress.PROGRESS_SEARCHING) {
                voiceSearchCancelReason = VoiceSearchCancelReason.cancel_search;
            }
            else if (AnalyticsSupport.voiceSearchProgress == VoiceSearchProgress.PROGRESS_LIST_DISPLAYED) {
                voiceSearchCancelReason = VoiceSearchCancelReason.cancel_list;
            }
            if (voiceSearchCancelReason == null) {
                AnalyticsSupport.sLogger.e("unexpected progress " + AnalyticsSupport.voiceSearchProgress + " in recordRouteSelectionCancelled()");
                clearVoiceSearch();
            }
            else {
                AnalyticsSupport.sLogger.v("route selection cancelled, reason = " + voiceSearchCancelReason);
                final Integer voiceSearchConfidence = AnalyticsSupport.voiceSearchConfidence;
                final int voiceSearchCount = AnalyticsSupport.voiceSearchCount;
                String s;
                if (AnalyticsSupport.voiceSearchListeningOverBluetooth) {
                    s = "bluetooth";
                }
                else {
                    s = "phone";
                }
                sendVoiceSearchEvent(voiceSearchCancelReason, null, null, voiceSearchConfidence, voiceSearchCount - 1, s, AnalyticsSupport.voiceSearchResults, AnalyticsSupport.voiceSearchAdditionalResultsAction, AnalyticsSupport.voiceSearchExplicitRetry, null, AnalyticsSupport.voiceSearchPrefix);
            }
        }
    }
    
    public static void recordScreen(final BaseScreen baseScreen) {
        if (baseScreen.getScreen() == Screen.SCREEN_WELCOME) {
            AnalyticsSupport.welcomeScreenTime = SystemClock.elapsedRealtime();
        }
        else if (baseScreen.getScreen() == Screen.SCREEN_HOME && !AnalyticsSupport.phonePaired && AnalyticsSupport.welcomeScreenTime != -1L && AnalyticsSupport.sRemoteDeviceManager.isRemoteDeviceConnected()) {
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            String s;
            if (AnalyticsSupport.newDevicePaired) {
                s = "true";
            }
            else {
                s = "false";
            }
            localyticsSendEvent("Attempt_To_Connect", "Was_New_Device", s, "Time_To_Connect", Long.toString(elapsedRealtime - AnalyticsSupport.welcomeScreenTime));
            AnalyticsSupport.welcomeScreenTime = -1L;
            AnalyticsSupport.newDevicePaired = false;
            AnalyticsSupport.phonePaired = true;
        }
        if (AnalyticsSupport.localyticsInitialized) {
            Localytics.tagScreen(screenName(baseScreen));
        }
    }
    
    public static void recordShutdown(final Shutdown.Reason reason, final boolean b) {
        String s;
        if (b) {
            s = "quiet";
        }
        else {
            s = "full";
        }
        final double batteryVoltage = AnalyticsSupport.sObdManager.getBatteryVoltage();
        String s2;
        if (AnalyticsSupport.quietMode) {
            s2 = "Sleep_Time";
        }
        else {
            s2 = "Uptime";
        }
        localyticsSendEvent("Shutdown", false, true, "Type", s, "Reason", reason.attr, "Battery_Level", String.format(Locale.US, "%6.1f", batteryVoltage), s2, Long.toString(uptime()));
    }
    
    public static void recordSmsSent(final boolean b, final String s, final boolean b2) {
        AnalyticsSupport.sLogger.v("sms-sent success=" + b + " plaform=" + s + " canned=" + b2);
        String s2;
        if (b2) {
            s2 = "Canned";
        }
        else {
            s2 = "Custom";
        }
        localyticsSendEvent("sms_sent", "Success", String.valueOf(b), "Platform", s, "Message_Type", s2);
    }
    
    public static void recordStartingBatteryVoltage(final double startingBatteryVoltage) {
        if (AnalyticsSupport.startingBatteryVoltage == -1.0) {
            AnalyticsSupport.startingBatteryVoltage = startingBatteryVoltage;
            AnalyticsSupport.sLogger.i(String.format(Locale.US, "starting battery voltage = %6.1f", startingBatteryVoltage));
        }
    }
    
    public static void recordSwipedCalibration(String string, final String[] array) {
        string = "Swiped_" + normalizeName(string);
        for (int i = 0; i < array.length - 1; i += 2) {
            array[i] = normalizeName(array[i]);
        }
        localyticsSendEvent(string, array);
    }
    
    public static void recordUpdatePrompt(final boolean b, final boolean b2, final String s) {
        String s2;
        if (b2) {
            s2 = "Dial_Update_Prompt";
        }
        else {
            s2 = "Display_Update_Prompt";
        }
        String s3;
        if (b) {
            s3 = "true";
        }
        else {
            s3 = "false";
        }
        localyticsSendEvent(s2, "Accepted", s3, "Update_To_Version", s);
    }
    
    public static void recordVehicleTelemetryData(final long n, final long n2, final long n3, final float n4, final float n5, final float n6, final int n7, final int n8, final float n9, final float n10, final int n11, final String s, final int n12, final int n13, final int n14, final int n15, final int n16, final float n17, final float n18, final float n19, final int n20) {
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        String carMake = "";
        String carModel = "";
        String carYear = "";
        if (currentProfile != null) {
            carMake = currentProfile.getCarMake();
            carModel = currentProfile.getCarModel();
            carYear = currentProfile.getCarYear();
        }
        localyticsSendEvent("Vehicle_Telemetry_Data", "Session_Distance", Long.toString(n), "Session_Duration", Long.toString(n2), "Session_Rolling_Duration", Long.toString(n3), "Session_Average_Speed", Float.toString(n4), "Session_Average_Rolling_Speed", Float.toString(n5), "Session_Max_Speed", Float.toString(n6), "Session_Hard_Braking_Count", Integer.toString(n7), "Session_Hard_Acceleration_Count", Integer.toString(n8), "Session_Speeding_Frequency", Float.toString(n9), "Session_Excessive_Speeding_Frequency", Float.toString(n10), "Session_Trouble_Code_Count", Integer.toString(n11), "Session_Trouble_Code_Last", s, "Session_Average_Mpg", Integer.toString(n12), "Session_Max_RPM", Integer.toString(n13), "Event_Average_Mpg", Integer.toString(n14), "Event_Average_Rpm", Integer.toString(n15), "Event_Fuel_Level", Integer.toString(n16), "Event_Engine_Temperature", Float.toString(n17), "Event_Max_G", Float.toString(n18), "Event_Max_G_Angle", Float.toString(n19), "Session_High_G_Count", Integer.toString(n20), "Car_Make", carMake, "Car_Model", carModel, "Car_Year", carYear);
    }
    
    public static void recordVoiceSearchAdditionalResultsAction(final VoiceSearchAdditionalResultsAction voiceSearchAdditionalResultsAction) {
        AnalyticsSupport.voiceSearchAdditionalResultsAction = voiceSearchAdditionalResultsAction;
        if (voiceSearchAdditionalResultsAction == VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST) {
            AnalyticsSupport.voiceSearchProgress = VoiceSearchProgress.PROGRESS_LIST_DISPLAYED;
        }
        else {
            AnalyticsSupport.voiceSearchProgress = VoiceSearchProgress.PROGRESS_ROUTE_SELECTED;
        }
    }
    
    public static void recordVoiceSearchEntered() {
        if (AnalyticsSupport.voiceSearchProgress == VoiceSearchProgress.PROGRESS_LIST_DISPLAYED) {
            clearVoiceSearch();
            AnalyticsSupport.voiceSearchExplicitRetry = true;
        }
    }
    
    public static void recordVoiceSearchListItemSelection(final int n) {
        if (n == -1 || n == -2) {
            AnalyticsSupport.voiceSearchExplicitRetry = true;
        }
        else {
            AnalyticsSupport.voiceSearchListSelection = n;
            AnalyticsSupport.voiceSearchProgress = VoiceSearchProgress.PROGRESS_ROUTE_SELECTED;
        }
    }
    
    public static void recordVoiceSearchResult(final VoiceSearchResponse voiceSearchResponse) {
        switch (voiceSearchResponse.state) {
            case VOICE_SEARCH_STARTING:
                ++AnalyticsSupport.voiceSearchCount;
                if (AnalyticsSupport.voiceSearchProgress != null) {
                    AnalyticsSupport.sLogger.w("starting voice search with unexpected progress state " + AnalyticsSupport.voiceSearchProgress);
                    clearVoiceSearch();
                }
                AnalyticsSupport.sLogger.v("starting voice search");
                AnalyticsSupport.voiceSearchProgress = VoiceSearchProgress.PROGRESS_SEARCHING;
                break;
            case VOICE_SEARCH_LISTENING:
                if (voiceSearchResponse.listeningOverBluetooth != null) {
                    AnalyticsSupport.voiceSearchListeningOverBluetooth = voiceSearchResponse.listeningOverBluetooth;
                    break;
                }
                break;
            case VOICE_SEARCH_SUCCESS:
                AnalyticsSupport.sLogger.v("voice search successful");
                AnalyticsSupport.voiceSearchProgress = VoiceSearchProgress.PROGRESS_DESTINATION_FOUND;
                if (voiceSearchResponse.listeningOverBluetooth != null) {
                    AnalyticsSupport.voiceSearchListeningOverBluetooth = voiceSearchResponse.listeningOverBluetooth;
                }
                AnalyticsSupport.voiceSearchResults = voiceSearchResponse.results;
            case VOICE_SEARCH_SEARCHING:
                if (voiceSearchResponse.recognizedWords != null) {
                    AnalyticsSupport.voiceSearchWords = voiceSearchResponse.recognizedWords;
                }
                if (voiceSearchResponse.confidenceLevel != null) {
                    AnalyticsSupport.voiceSearchConfidence = voiceSearchResponse.confidenceLevel;
                }
                if (voiceSearchResponse.prefix != null) {
                    AnalyticsSupport.voiceSearchPrefix = voiceSearchResponse.prefix;
                    break;
                }
                break;
            case VOICE_SEARCH_ERROR:
                AnalyticsSupport.sLogger.v("voice search error: " + voiceSearchResponse.error);
                clearVoiceSearch();
                sendVoiceSearchErrorEvent(voiceSearchResponse.error, AnalyticsSupport.voiceSearchCount - 1);
                break;
        }
    }
    
    public static void recordVoiceSearchRetry() {
        AnalyticsSupport.voiceSearchExplicitRetry = true;
    }
    
    private static void recordWakeup(final WakeupReason wakeupReason) {
        final String string = Long.toString(uptime());
        final String format = String.format(Locale.US, "%6.1f", AnalyticsSupport.sObdManager.getBatteryVoltage());
        final String format2 = String.format(Locale.US, "%6.1f", AnalyticsSupport.sObdManager.getMaxBatteryVoltage());
        final String vin = AnalyticsSupport.sObdManager.getVin();
        double n = 0.0;
        if (AnalyticsSupport.startingBatteryVoltage != -1.0) {
            n = AnalyticsSupport.startingBatteryVoltage - AnalyticsSupport.sObdManager.getMinBatteryVoltage();
        }
        final String format3 = String.format(Locale.US, "%6.1f", n);
        if (vin != null) {
            localyticsSendEvent("Wakeup", false, true, "Reason", wakeupReason.attr, "Battery_Level", format, "Battery_Drain", format3, "Sleep_Time", string, "Max_Battery", format2, "VIN", vin);
        }
        else {
            AnalyticsSupport.deferredWakeup = new DeferredWakeup(wakeupReason, format, format3, string, format2);
        }
        AnalyticsSupport.startTime = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime());
        AnalyticsSupport.startingDistance = AnalyticsSupport.sRemoteDeviceManager.getTripManager().getTotalDistanceTravelled();
    }
    
    private static void reportObdState() {
        final List<ECU> ecus = AnalyticsSupport.sObdManager.getEcus();
        int size;
        if (ecus == null) {
            size = 0;
        }
        else {
            size = ecus.size();
        }
        if (size == 0) {
            AnalyticsSupport.sLogger.e("Obd connection with no ECUs");
        }
        AnalyticsSupport.sLastVIN = AnalyticsSupport.sObdManager.getVin();
        String obdChipFirmwareVersion = AnalyticsSupport.sObdManager.getObdChipFirmwareVersion();
        final String[] array = new String[size * 2 + 6];
        final int n = 0 + 1;
        array[0] = "Obd_Chip_Firmware";
        final int n2 = n + 1;
        if (obdChipFirmwareVersion == null) {
            obdChipFirmwareVersion = "";
        }
        array[n] = obdChipFirmwareVersion;
        final int n3 = n2 + 1;
        array[n2] = "VIN";
        final int n4 = n3 + 1;
        array[n3] = AnalyticsSupport.sLastVIN;
        int n5 = n4 + 1;
        array[n4] = "Protocol";
        array[n5] = AnalyticsSupport.sObdManager.getProtocol();
        int i = 0;
        ++n5;
        while (i < size) {
            final ECU ecu = ecus.get(i);
            final List<Pid> list = ecu.supportedPids.asList();
            final int n6 = n5 + 1;
            array[n5] = String.format(Locale.US, "%s%d", "ECU", i + 1);
            n5 = n6 + 1;
            array[n6] = String.format(Locale.US, "Address: 0x%x, Pids: %s", ecu.address, pidListToHexString(list));
            ++i;
        }
        localyticsSendEvent("OBD_Connected", array);
        if (AnalyticsSupport.deferredWakeup != null) {
            localyticsSendEvent("Wakeup", false, true, "Reason", AnalyticsSupport.deferredWakeup.reason.attr, "Battery_Level", AnalyticsSupport.deferredWakeup.batteryLevel, "Battery_Drain", AnalyticsSupport.deferredWakeup.batteryDrain, "Sleep_Time", AnalyticsSupport.deferredWakeup.sleepTime, "Max_Battery", AnalyticsSupport.deferredWakeup.maxBattery, "VIN", AnalyticsSupport.sLastVIN);
            AnalyticsSupport.deferredWakeup = null;
        }
    }
    
    private static String screenName(final BaseScreen baseScreen) {
        final Screen screen = baseScreen.getScreen();
        String s2;
        final String s = s2 = screen.toString();
        if (screen == Screen.SCREEN_HOME) {
            final HomeScreen.DisplayMode displayMode = ((HomeScreen)baseScreen).getDisplayMode();
            s2 = s;
            if (displayMode != null) {
                s2 = s;
                if (HereMapsManager.getInstance().isInitialized()) {
                    String s3;
                    if (HereNavigationManager.getInstance().isNavigationModeOn()) {
                        s3 = "_ROUTE";
                    }
                    else {
                        s3 = "";
                    }
                    s2 = displayMode.toString() + s3;
                }
            }
        }
        return s2;
    }
    
    private static void sendDeferredLocalyticsEvents() {
        while (true) {
            Label_0115: {
                synchronized (AnalyticsSupport.class) {
                    if (!AnalyticsSupport.localyticsInitialized) {
                        AnalyticsSupport.sLogger.e("sendDeferredLocalyticsEvents() called before Localytics was initialized");
                    }
                    else if (AnalyticsSupport.deferredEvents != null) {
                        AnalyticsSupport.sLogger.v("sending " + AnalyticsSupport.deferredEvents.size() + " deferred localytics events");
                        for (final DeferredLocalyticsEvent deferredLocalyticsEvent : AnalyticsSupport.deferredEvents) {
                            localyticsSendEvent(deferredLocalyticsEvent.tag, deferredLocalyticsEvent.args);
                        }
                        break Label_0115;
                    }
                    return;
                }
            }
            AnalyticsSupport.deferredEvents = null;
        }
    }
    
    private static void sendDistance() {
        final int n = RemoteDeviceManager.getInstance().getTripManager().getTotalDistanceTravelled() - AnalyticsSupport.startingDistance;
        if (n > 10 && !AnalyticsSupport.quietMode) {
            final long uptime = uptime();
            final String string = Long.toString(n);
            final String string2 = Long.toString(uptime);
            String s;
            if (AnalyticsSupport.destinationSet) {
                s = "true";
            }
            else {
                s = "false";
            }
            String s2;
            if (AnalyticsSupport.phonePaired) {
                s2 = "true";
            }
            else {
                s2 = "false";
            }
            localyticsSendEvent("Distance_Travelled", false, true, "Distance", string, "Duration", string2, "Navigating", s, "Connection", s2, "Vin", AnalyticsSupport.sLastVIN);
        }
    }
    
    private static void sendVoiceSearchErrorEvent(final VoiceSearchResponse.VoiceSearchError voiceSearchError, final int n) {
        final String voiceSearchEvent = getVoiceSearchEvent();
        if (voiceSearchEvent == null) {
            AnalyticsSupport.sLogger.e("cannot determine phone_type for voice search");
        }
        else {
            String string;
            if (voiceSearchError == null) {
                string = "";
            }
            else {
                string = voiceSearchError.toString();
            }
            localyticsSendEvent(voiceSearchEvent, "Response", string, "Retry_Count", Integer.toString(n), "Client_Version", getClientVersion());
        }
    }
    
    private static void sendVoiceSearchEvent(final VoiceSearchCancelReason voiceSearchCancelReason, final NavigationRouteRequest navigationRouteRequest, String s, final Integer n, int size, final String s2, final List<Destination> list, final VoiceSearchAdditionalResultsAction voiceSearchAdditionalResultsAction, final boolean b, final Integer n2, final String s3) {
        final String voiceSearchEvent = getVoiceSearchEvent();
        if (voiceSearchEvent == null) {
            AnalyticsSupport.sLogger.e("cannot determine phone_type for voice search");
        }
        else {
            final HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap.put("Response", "Success");
            hashMap.put("Client_Version", getClientVersion());
            if (n != null) {
                hashMap.put("Confidence_Level", n.toString());
            }
            if (s != null) {
                hashMap.put("Number_Of_Words", Integer.toString(countWords(s)));
            }
            boolean b2;
            if (voiceSearchCancelReason == VoiceSearchCancelReason.end_trip || voiceSearchCancelReason == VoiceSearchCancelReason.new_voice_search || voiceSearchCancelReason == VoiceSearchCancelReason.new_non_voice_search) {
                b2 = true;
            }
            else {
                b2 = false;
            }
            if (voiceSearchCancelReason == null || b2) {
                s = "true";
            }
            else {
                s = "false";
            }
            hashMap.put("Navigation_Initiated", s);
            if (b2) {
                s = "true";
            }
            else {
                s = "false";
            }
            hashMap.put("Navigation_Cancelled_Quickly", s);
            hashMap.put("Retry_Count", Integer.toString(size));
            hashMap.put("Microphone_Used", s2);
            if (list == null) {
                size = 0;
            }
            else {
                size = list.size();
            }
            hashMap.put("Num_Total_Results", Integer.toString(size));
            if (voiceSearchAdditionalResultsAction != null) {
                hashMap.put("Additional_Results_Glance", voiceSearchAdditionalResultsAction.tag);
            }
            if (b) {
                s = "true";
            }
            else {
                s = "false";
            }
            hashMap.put("Explicit_Retry", s);
            if (voiceSearchCancelReason != null) {
                hashMap.put("Cancel_Type", voiceSearchCancelReason.toString());
            }
            if (n2 != null) {
                hashMap.put("Additional_Results_List_Item_Selected", n2.toString());
            }
            size = 0;
            String string = "ADDRESS";
            if (navigationRouteRequest != null) {
                final Destination requestDestination = navigationRouteRequest.requestDestination;
                if (requestDestination != null && requestDestination.suggestion_type != null && requestDestination.suggestion_type != Destination.SuggestionType.SUGGESTION_NONE) {
                    size = 1;
                }
                else {
                    size = 0;
                }
                if (navigationRouteRequest.destinationType != null && navigationRouteRequest.destinationType != Destination.FavoriteType.FAVORITE_NONE) {
                    string = navigationRouteRequest.destinationType.toString();
                }
                else if (navigationRouteRequest.label != null) {
                    string = "PLACE";
                }
                hashMap.put("Destination_Type", string);
            }
            String s4;
            if (size != 0) {
                s4 = "true";
            }
            else {
                s4 = "false";
            }
            hashMap.put("Suggestion", s4);
            if (s3 != null) {
                hashMap.put("Prefix", s3);
            }
            localyticsSendEvent(new Event(voiceSearchEvent, hashMap), false);
        }
    }
    
    public static void setGlanceNavigationSource(final String glanceNavigationSource) {
        AnalyticsSupport.glanceNavigationSource = glanceNavigationSource;
    }
    
    private static void setupCustomDimensions() {
        Localytics.setCustomDimension(0, getHwVersion());
    }
    
    private static void setupProfile() {
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        String carMake = null;
        String carModel = null;
        String carYear = null;
        if (!currentProfile.isDefaultProfile()) {
            Localytics.setCustomerFullName(currentProfile.getDriverName());
            Localytics.setCustomerEmail(currentProfile.getDriverEmail());
            carMake = currentProfile.getCarMake();
            carModel = currentProfile.getCarModel();
            carYear = currentProfile.getCarYear();
            Localytics.setProfileAttribute("Hud_Serial_Number", Build.SERIAL);
            Localytics.setProfileAttribute("Car_Make", carMake);
            Localytics.setProfileAttribute("Car_Model", carModel);
            Localytics.setProfileAttribute("Car_Year", carYear);
            Localytics.setProfileAttribute("Hud_Version_Number", SystemProperties.get("ro.build.description", ""));
            Localytics.setProfileAttribute("Build_Type", Build.TYPE);
        }
        final DeviceInfo remoteDeviceInfo = AnalyticsSupport.sRemoteDeviceManager.getRemoteDeviceInfo();
        String string;
        String deviceName;
        String clientVersion;
        if (remoteDeviceInfo != null) {
            string = remoteDeviceInfo.platform.toString();
            deviceName = remoteDeviceInfo.deviceName;
            clientVersion = remoteDeviceInfo.clientVersion;
            AnalyticsSupport.sRemoteDeviceId = remoteDeviceInfo.deviceId;
        }
        else {
            clientVersion = "unknown";
            AnalyticsSupport.sRemoteDeviceId = "unknown";
            deviceName = "unknown";
            string = "unknown";
        }
        final String vin = AnalyticsSupport.sObdManager.getVin();
        localyticsSendEvent("Mobile_App_Connect", "Device_Id", Build.SERIAL, "Remote_Device_Id", AnalyticsSupport.sRemoteDeviceId, "Remote_Device_Name", deviceName, "Remote_Platform", string, "Remote_Client_Version", clientVersion, "Car_Make", carMake, "Car_Model", carModel, "Car_Year", carYear, "VIN", vin);
        AnalyticsSupport.sLogger.i("setupProfile: DeviceId = " + Build.SERIAL + ", RemoteDeviceId = " + AnalyticsSupport.sRemoteDeviceId + ", remotePlatform = " + ", RemoteDeviceName = " + deviceName + ", RemoteClientVersion = " + clientVersion + " vin = " + vin);
    }
    
    static void submitNavigationQualityReport(final NavigationQualityTracker.Report report) {
        localyticsSendEvent("Route_Completed", "Expected_Duration", String.valueOf(report.expectedDuration), "Actual_Duration", String.valueOf(report.actualDuration), "Expected_Distance", String.valueOf(report.expectedDistance), "Actual_Distance", String.valueOf(report.actualDistance), "Recalculations", String.valueOf(report.nRecalculations), "Duration_PCT_Diff", String.valueOf(report.getDurationVariance()), "Distance_PCT_Diff", String.valueOf(report.getDistanceVariance()), "Traffic_Level", report.trafficLevel.name());
    }
    
    private static long uptime() {
        return TimeUnit.NANOSECONDS.toSeconds(System.nanoTime()) - AnalyticsSupport.startTime;
    }
    
    public static class AnalyticsIntentsReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            final String stringExtra = intent.getStringExtra("tag");
            final boolean booleanExtra = intent.getBooleanExtra("include_device_info", false);
            if (stringExtra != null) {
                String[] stringArrayExtra;
                if ((stringArrayExtra = intent.getStringArrayExtra("arguments")) == null) {
                    stringArrayExtra = new String[0];
                }
                if (stringExtra.equals("Analytics_New_Device")) {
                    recordNewDevicePaired();
                }
                else {
                    localyticsSendEvent(stringExtra, booleanExtra, stringArrayExtra);
                }
            }
        }
    }
    
    private static class DeferredLocalyticsEvent
    {
        public String[] args;
        public String tag;
        
        DeferredLocalyticsEvent(final String tag, final String[] args) {
            this.tag = tag;
            this.args = args;
        }
    }
    
    private static class DeferredWakeup
    {
        String batteryDrain;
        String batteryLevel;
        String maxBattery;
        public WakeupReason reason;
        String sleepTime;
        
        DeferredWakeup(final WakeupReason reason, final String batteryLevel, final String batteryDrain, final String sleepTime, final String maxBattery) {
            this.reason = reason;
            this.batteryLevel = batteryLevel;
            this.batteryDrain = batteryDrain;
            this.sleepTime = sleepTime;
            this.maxBattery = maxBattery;
        }
    }
    
    private static class EventSender implements Runnable
    {
        private Event event;
        
        EventSender(final Event event) {
            this.event = event;
        }
        
        @Override
        public void run() {
            Localytics.tagEvent(this.event.tag, this.event.argMap);
        }
    }
    
    private static class FuelMonitor implements Runnable
    {
        @Override
        public void run() {
            try {
                final int distanceTravelled = AnalyticsSupport.sObdManager.getDistanceTravelled();
                final int fuelLevel = AnalyticsSupport.sObdManager.getFuelLevel();
                final double instantFuelConsumption = AnalyticsSupport.sObdManager.getInstantFuelConsumption();
                final String vin = AnalyticsSupport.sObdManager.getVin();
                if (fuelLevel != -1) {
                    AnalyticsSupport.localyticsSendEvent("Fuel_Level", "VIN", vin, "Fuel_Level", Integer.toString(fuelLevel), "Fuel_Consumption", Double.toString(instantFuelConsumption), "Distance", Integer.toString(distanceTravelled));
                }
            }
            finally {
                AnalyticsSupport.handler.postDelayed((Runnable)this, 180000L);
            }
        }
    }
    
    public static class NotificationReceiver implements INotificationAnimationListener
    {
        @Inject
        public SharedPreferences appPreferences;
        private Maneuver lastRequiredManeuver;
        private MapEvents.ManeuverDisplay lastWarningEvent;
        @Inject
        public PowerManager powerManager;
        
        NotificationReceiver() {
            this.lastWarningEvent = null;
            this.lastRequiredManeuver = null;
            RemoteDeviceManager.getInstance().getBus().register(this);
            RemoteDeviceManager.getInstance().getUiStateManager().addNotificationAnimationListener(this);
            Mortar.inject(HudApplication.getAppContext(), this);
        }
        
        private boolean objEquals(final Object o, final Object o2) {
            return o == o2 || (o != null && o.equals(o2));
        }
        
        @Subscribe
        public void ObdStateChangeEvent(final ObdManager.ObdConnectionStatusEvent obdConnectionStatusEvent) {
            if (obdConnectionStatusEvent.connected) {
                reportObdState();
            }
        }
        
        @Subscribe
        public void onArrived(final MapEvents.ArrivalEvent arrivalEvent) {
            processNavigationArrivalEvent(arrivalEvent);
        }
        
        @Subscribe
        public void onArrived(final MapEvents.ManeuverEvent maneuverEvent) {
            if (maneuverEvent.type == MapEvents.ManeuverEvent.Type.LAST) {
                AnalyticsSupport.sLogger.v("NAVIGATION_ARRIVED");
                AnalyticsSupport.localyticsSendEvent("Navigation_Arrived", new String[0]);
            }
        }
        
        @Subscribe
        public void onBandwidthSettingChanged(final NetworkBandwidthController.UserBandwidthSettingChanged userBandwidthSettingChanged) {
            final long access$100 = getUpdateInterval();
            AnalyticsSupport.sLogger.v("uploader changed to " + access$100);
            AnalyticsSupport.handler.removeCallbacks(AnalyticsSupport.sUploader);
            AnalyticsSupport.handler.postDelayed(AnalyticsSupport.sUploader, access$100);
        }
        
        @Subscribe
        public void onConnectionStatusChange(final ConnectionStateChange connectionStateChange) {
            if (connectionStateChange.state == ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
                AnalyticsSupport.localyticsSendEvent("Mobile_App_Disconnect", "Device_Id", Build.SERIAL, "Remote_Device_Id", AnalyticsSupport.sRemoteDeviceId);
                clearVoiceSearch();
            }
        }
        
        @Subscribe
        public void onDateTimeAvailable(final TimeHelper.DateTimeAvailableEvent dateTimeAvailableEvent) {
            maybeOpenSession();
        }
        
        @Subscribe
        public void onDeviceInfoAvailable(final DeviceInfoAvailable deviceInfoAvailable) {
            if (deviceInfoAvailable.deviceInfo != null) {
                AnalyticsSupport.remoteDevice = deviceInfoAvailable.deviceInfo;
                setupProfile();
            }
        }
        
        @Subscribe
        public void onDismissScreen(final DismissScreen dismissScreen) {
        }
        
        @Subscribe
        public void onDriverProfileChange(final DriverProfileChanged driverProfileChanged) {
            final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
            if (!currentProfile.isDefaultProfile()) {
                AnalyticsSupport.sLastProfile = currentProfile;
                AnalyticsSupport.sLogger.d("Remembering cached preferences");
                AnalyticsSupport.sLastProfilePreferences = new PreferenceWatcher(currentProfile, new PreferenceAdapter() {
                    @Override
                    public Event calculateState(final DriverProfile driverProfile) {
                        return recordPreference(driverProfile);
                    }
                });
                AnalyticsSupport.sLastNavigationPreferences = new PreferenceWatcher(currentProfile, new PreferenceAdapter() {
                    @Override
                    public Event calculateState(final DriverProfile driverProfile) {
                        return recordNavigationPreference(driverProfile);
                    }
                });
            }
        }
        
        @Subscribe
        public void onManeuverMissed(final MapEvents.RerouteEvent rerouteEvent) {
            AnalyticsSupport.localyticsSendEvent("Navigation_Maneuver_Missed", new String[0]);
        }
        
        @Subscribe
        public void onManeuverRequired(final MapEvents.ManeuverSoonEvent maneuverSoonEvent) {
            if (maneuverSoonEvent.maneuver != this.lastRequiredManeuver) {
                AnalyticsSupport.localyticsSendEvent("Navigation_Maneuver_Required", new String[0]);
                this.lastRequiredManeuver = maneuverSoonEvent.maneuver;
            }
        }
        
        @Subscribe
        public void onManeuverWarning(final MapEvents.ManeuverDisplay lastWarningEvent) {
            if (this.lastWarningEvent == null || lastWarningEvent.turnIconId != this.lastWarningEvent.turnIconId || !this.objEquals(lastWarningEvent.pendingTurn, this.lastWarningEvent.pendingTurn) || !this.objEquals(lastWarningEvent.pendingRoad, this.lastWarningEvent.pendingRoad) || !this.objEquals(lastWarningEvent.currentRoad, this.lastWarningEvent.currentRoad) || !this.objEquals(lastWarningEvent.navigationTurn, this.lastWarningEvent.navigationTurn)) {
                AnalyticsSupport.localyticsSendEvent("Navigation_Maneuver_Warning", new String[0]);
                this.lastWarningEvent = lastWarningEvent;
            }
        }
        
        @Subscribe
        public void onMapEngineInitialized(final MapEvents.MapEngineInitialize mapEngineInitialize) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    recordOfflineMapsChange(NotificationReceiver.this.appPreferences);
                }
            }, 1);
        }
        
        @Subscribe
        public void onRegionEvent(final MapEvents.RegionEvent regionEvent) {
            AnalyticsSupport.localyticsSendEvent("Map_Region_Change", "State", regionEvent.state, "Country", regionEvent.country);
        }
        
        @Subscribe
        public void onRouteSearchRequest(final NavigationRouteRequest navigationRouteRequest) {
            processNavigationRouteRequest(navigationRouteRequest);
        }
        
        @Subscribe
        public void onShowToast(final ToastManager.ShowToast showToast) {
            if (showToast.name.equals("disconnection#toast")) {
                AnalyticsSupport.recordPhoneDisconnect(false, null);
            }
        }
        
        @Subscribe
        public void onShutdown(final Shutdown shutdown) {
            if (shutdown.state == Shutdown.State.SHUTTING_DOWN) {
                closeSession();
            }
        }
        
        @Override
        public void onStart(String access$300, final NotificationType notificationType, final UIStateManager.Mode mode) {
            if (mode == UIStateManager.Mode.EXPAND) {
                final INotification currentNotification = NotificationManager.getInstance().getCurrentNotification();
                if (AnalyticsSupport.glanceNavigationSource != null) {
                    access$300 = AnalyticsSupport.glanceNavigationSource;
                }
                else {
                    access$300 = "auto";
                }
                AnalyticsSupport.recordGlanceAction("Glance_Open_Mini", currentNotification, access$300);
            }
            AnalyticsSupport.glanceNavigationSource = null;
        }
        
        @Override
        public void onStop(final String s, final NotificationType notificationType, final UIStateManager.Mode mode) {
        }
        
        @Subscribe
        public void onWakeup(final Wakeup wakeup) {
            recordWakeup(wakeup.reason);
            AnalyticsSupport.quietMode = false;
            maybeOpenSession();
        }
    }
    
    interface PreferenceAdapter
    {
        Event calculateState(final DriverProfile p0);
    }
    
    private static class PreferenceWatcher
    {
        PreferenceAdapter adapter;
        Event oldState;
        Runnable queuedUpdate;
        
        PreferenceWatcher(final DriverProfile driverProfile, final PreferenceAdapter adapter) {
            this.oldState = adapter.calculateState(driverProfile);
            this.adapter = adapter;
        }
        
        void enqueue(final DriverProfile driverProfile) {
            if (this.queuedUpdate != null) {
                AnalyticsSupport.handler.removeCallbacks(this.queuedUpdate);
            }
            this.queuedUpdate = new Runnable() {
                @Override
                public void run() {
                    final Event calculateState = PreferenceWatcher.this.adapter.calculateState(driverProfile);
                    final String changedFields = calculateState.getChangedFields(PreferenceWatcher.this.oldState);
                    if (!TextUtils.isEmpty((CharSequence)changedFields)) {
                        calculateState.argMap.put("Preferences_Updated", changedFields);
                        PreferenceWatcher.this.oldState = calculateState;
                        AnalyticsSupport.sLogger.d("Recording preference change: " + calculateState);
                        localyticsSendEvent(calculateState, false);
                    }
                    else {
                        AnalyticsSupport.sLogger.d("No change in " + calculateState.tag);
                    }
                }
            };
            AnalyticsSupport.handler.postDelayed(this.queuedUpdate, 15000L);
        }
    }
    
    public static class TemperatureReporter extends Thread
    {
        private static final String SOCKET_NAME = "tempstatus";
        @Inject
        PowerManager powerManager;
        
        @Override
        public void run() {
            // 
             This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     3: astore_1       
            //     4: aload_1        
            //     5: invokespecial   android/net/LocalSocket.<init>:()V
            //     8: new             Landroid/net/LocalSocketAddress;
            //    11: astore_2       
            //    12: aload_2        
            //    13: ldc             "tempstatus"
            //    15: getstatic       android/net/LocalSocketAddress$Namespace.RESERVED:Landroid/net/LocalSocketAddress$Namespace;
            //    18: invokespecial   android/net/LocalSocketAddress.<init>:(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V
            //    21: aload_1        
            //    22: aload_2        
            //    23: invokevirtual   android/net/LocalSocket.connect:(Landroid/net/LocalSocketAddress;)V
            //    26: aload_1        
            //    27: invokevirtual   android/net/LocalSocket.getInputStream:()Ljava/io/InputStream;
            //    30: astore_3       
            //    31: new             Ljava/io/BufferedReader;
            //    34: astore          4
            //    36: new             Ljava/io/InputStreamReader;
            //    39: astore_2       
            //    40: aload_2        
            //    41: aload_3        
            //    42: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
            //    45: aload           4
            //    47: aload_2        
            //    48: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
            //    51: iconst_0       
            //    52: istore          5
            //    54: new             Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
            //    57: astore          6
            //    59: aload           6
            //    61: bipush          72
            //    63: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$2300:()J
            //    66: invokespecial   com/navdy/hud/app/analytics/AnalyticsSupport$Threshold.<init>:(IJ)V
            //    69: new             Lcom/navdy/hud/app/analytics/AnalyticsSupport$Threshold;
            //    72: astore          7
            //    74: aload           7
            //    76: bipush          65
            //    78: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$2400:()J
            //    81: invokespecial   com/navdy/hud/app/analytics/AnalyticsSupport$Threshold.<init>:(IJ)V
            //    84: aload           4
            //    86: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
            //    89: astore_2       
            //    90: aload_2        
            //    91: ifnull          84
            //    94: aload_2        
            //    95: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
            //    98: astore          8
            //   100: aload           8
            //   102: astore_2       
            //   103: bipush          13
            //   105: anewarray       Ljava/lang/String;
            //   108: astore          9
            //   110: iconst_0       
            //   111: istore          10
            //   113: iload           10
            //   115: aload           9
            //   117: arraylength    
            //   118: iconst_1       
            //   119: isub           
            //   120: if_icmpge       169
            //   123: aload_2        
            //   124: ldc             " "
            //   126: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
            //   129: ifeq            169
            //   132: aload_2        
            //   133: ldc             " +"
            //   135: iconst_2       
            //   136: invokevirtual   java/lang/String.split:(Ljava/lang/String;I)[Ljava/lang/String;
            //   139: astore_2       
            //   140: aload           9
            //   142: iload           10
            //   144: aload_2        
            //   145: iconst_0       
            //   146: aaload         
            //   147: aastore        
            //   148: aload_2        
            //   149: iconst_1       
            //   150: aaload         
            //   151: astore_2       
            //   152: iinc            10, 1
            //   155: goto            113
            //   158: astore_2       
            //   159: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$000:()Lcom/navdy/service/library/log/Logger;
            //   162: ldc             "error connecting to temperature reporting socket"
            //   164: aload_2        
            //   165: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
            //   168: return         
            //   169: iload           10
            //   171: iconst_1       
            //   172: iadd           
            //   173: istore          11
            //   175: aload           9
            //   177: iload           10
            //   179: aload_2        
            //   180: aastore        
            //   181: iload           11
            //   183: aload           9
            //   185: arraylength    
            //   186: if_icmpeq       277
            //   189: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$000:()Lcom/navdy/service/library/log/Logger;
            //   192: getstatic       java/util/Locale.US:Ljava/util/Locale;
            //   195: ldc             "missing fields in temperature reporting data, found = %d, expected = %d data: %s"
            //   197: iconst_3       
            //   198: anewarray       Ljava/lang/Object;
            //   201: dup            
            //   202: iconst_0       
            //   203: iload           11
            //   205: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            //   208: aastore        
            //   209: dup            
            //   210: iconst_1       
            //   211: aload           9
            //   213: arraylength    
            //   214: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            //   217: aastore        
            //   218: dup            
            //   219: iconst_2       
            //   220: aload           8
            //   222: aastore        
            //   223: invokestatic    java/lang/String.format:(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
            //   226: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
            //   229: goto            84
            //   232: astore_2       
            //   233: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$000:()Lcom/navdy/service/library/log/Logger;
            //   236: ldc             "error reading from temperature reporting socket"
            //   238: aload_2        
            //   239: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
            //   242: iload           5
            //   244: iconst_1       
            //   245: iadd           
            //   246: istore          10
            //   248: iload           10
            //   250: istore          5
            //   252: iload           10
            //   254: iconst_3       
            //   255: if_icmple       84
            //   258: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$000:()Lcom/navdy/service/library/log/Logger;
            //   261: ldc             "too many errors reading temperature reporting socket, giving up"
            //   263: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
            //   266: aload_3        
            //   267: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
            //   270: aload_1        
            //   271: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
            //   274: goto            168
            //   277: aload           9
            //   279: iload           11
            //   281: iconst_1       
            //   282: isub           
            //   283: aaload         
            //   284: ldc             " "
            //   286: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
            //   289: ifeq            345
            //   292: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$000:()Lcom/navdy/service/library/log/Logger;
            //   295: astore          12
            //   297: new             Ljava/lang/StringBuilder;
            //   300: astore_2       
            //   301: aload_2        
            //   302: invokespecial   java/lang/StringBuilder.<init>:()V
            //   305: aload           12
            //   307: aload_2        
            //   308: ldc             "too many fields in temperature reporting data: "
            //   310: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   313: aload           8
            //   315: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   318: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   321: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
            //   324: aload           9
            //   326: iload           11
            //   328: iconst_1       
            //   329: isub           
            //   330: aload           9
            //   332: iload           11
            //   334: iconst_1       
            //   335: isub           
            //   336: aaload         
            //   337: ldc             " "
            //   339: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
            //   342: iconst_0       
            //   343: aaload         
            //   344: aastore        
            //   345: aload           9
            //   347: iconst_5       
            //   348: aaload         
            //   349: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$2500:(Ljava/lang/String;)I
            //   352: istore          11
            //   354: iload           11
            //   356: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$2602:(I)I
            //   359: pop            
            //   360: aload           9
            //   362: iconst_2       
            //   363: aaload         
            //   364: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$2500:(Ljava/lang/String;)I
            //   367: istore          13
            //   369: aload           9
            //   371: iconst_3       
            //   372: aaload         
            //   373: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$2500:(Ljava/lang/String;)I
            //   376: istore          10
            //   378: invokestatic    android/os/SystemClock.elapsedRealtime:()J
            //   381: lstore          14
            //   383: aload           7
            //   385: iload           10
            //   387: invokevirtual   com/navdy/hud/app/analytics/AnalyticsSupport$Threshold.update:(I)V
            //   390: aload           7
            //   392: invokevirtual   com/navdy/hud/app/analytics/AnalyticsSupport$Threshold.isHit:()Z
            //   395: istore          16
            //   397: iconst_0       
            //   398: istore          10
            //   400: aload_0        
            //   401: getfield        com/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter.powerManager:Lcom/navdy/hud/app/device/PowerManager;
            //   404: invokevirtual   com/navdy/hud/app/device/PowerManager.inQuietMode:()Z
            //   407: ifne            505
            //   410: aload           6
            //   412: iload           13
            //   414: invokevirtual   com/navdy/hud/app/analytics/AnalyticsSupport$Threshold.update:(I)V
            //   417: iload           11
            //   419: bipush          93
            //   421: if_icmpge       432
            //   424: aload           6
            //   426: invokevirtual   com/navdy/hud/app/analytics/AnalyticsSupport$Threshold.isHit:()Z
            //   429: ifeq            878
            //   432: iconst_1       
            //   433: istore          10
            //   435: invokestatic    com/navdy/hud/app/view/TemperatureWarningView.getLastDismissedTime:()J
            //   438: lstore          17
            //   440: lload           17
            //   442: lconst_0       
            //   443: lcmp           
            //   444: ifeq            476
            //   447: iload           10
            //   449: istore          11
            //   451: lload           17
            //   453: ldc2_w          -1
            //   456: lcmp           
            //   457: ifeq            479
            //   460: iload           10
            //   462: istore          11
            //   464: lload           14
            //   466: lload           17
            //   468: lsub           
            //   469: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$2700:()J
            //   472: lcmp           
            //   473: ifgt            479
            //   476: iconst_0       
            //   477: istore          11
            //   479: iload           11
            //   481: istore          10
            //   483: iload           11
            //   485: ifeq            505
            //   488: iload           11
            //   490: istore          10
            //   492: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$2800:()J
            //   495: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$2900:()J
            //   498: lcmp           
            //   499: ifge            505
            //   502: iconst_0       
            //   503: istore          10
            //   505: iload           10
            //   507: ifeq            541
            //   510: invokestatic    com/navdy/hud/app/manager/RemoteDeviceManager.getInstance:()Lcom/navdy/hud/app/manager/RemoteDeviceManager;
            //   513: invokevirtual   com/navdy/hud/app/manager/RemoteDeviceManager.getBus:()Lcom/squareup/otto/Bus;
            //   516: astore          8
            //   518: new             Lcom/navdy/service/library/events/ui/ShowScreen$Builder;
            //   521: astore_2       
            //   522: aload_2        
            //   523: invokespecial   com/navdy/service/library/events/ui/ShowScreen$Builder.<init>:()V
            //   526: aload           8
            //   528: aload_2        
            //   529: getstatic       com/navdy/service/library/events/ui/Screen.SCREEN_TEMPERATURE_WARNING:Lcom/navdy/service/library/events/ui/Screen;
            //   532: invokevirtual   com/navdy/service/library/events/ui/ShowScreen$Builder.screen:(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;
            //   535: invokevirtual   com/navdy/service/library/events/ui/ShowScreen$Builder.build:()Lcom/navdy/service/library/events/ui/ShowScreen;
            //   538: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
            //   541: aload_0        
            //   542: getfield        com/navdy/hud/app/analytics/AnalyticsSupport$TemperatureReporter.powerManager:Lcom/navdy/hud/app/device/PowerManager;
            //   545: invokevirtual   com/navdy/hud/app/device/PowerManager.inQuietMode:()Z
            //   548: ifeq            590
            //   551: iload           16
            //   553: ifeq            590
            //   556: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.access$000:()Lcom/navdy/service/library/log/Logger;
            //   559: ldc             "Shutting down due to high DMD temperature"
            //   561: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
            //   564: invokestatic    com/navdy/hud/app/manager/RemoteDeviceManager.getInstance:()Lcom/navdy/hud/app/manager/RemoteDeviceManager;
            //   567: invokevirtual   com/navdy/hud/app/manager/RemoteDeviceManager.getBus:()Lcom/squareup/otto/Bus;
            //   570: astore_2       
            //   571: new             Lcom/navdy/hud/app/event/Shutdown;
            //   574: astore          8
            //   576: aload           8
            //   578: getstatic       com/navdy/hud/app/event/Shutdown$Reason.HIGH_TEMPERATURE:Lcom/navdy/hud/app/event/Shutdown$Reason;
            //   581: invokespecial   com/navdy/hud/app/event/Shutdown.<init>:(Lcom/navdy/hud/app/event/Shutdown$Reason;)V
            //   584: aload_2        
            //   585: aload           8
            //   587: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
            //   590: aload           9
            //   592: iconst_0       
            //   593: aaload         
            //   594: astore          19
            //   596: aload           9
            //   598: iconst_1       
            //   599: aaload         
            //   600: astore          20
            //   602: aload           9
            //   604: iconst_2       
            //   605: aaload         
            //   606: astore          12
            //   608: aload           9
            //   610: iconst_3       
            //   611: aaload         
            //   612: astore          21
            //   614: aload           9
            //   616: iconst_4       
            //   617: aaload         
            //   618: astore          22
            //   620: aload           9
            //   622: iconst_5       
            //   623: aaload         
            //   624: astore          23
            //   626: aload           9
            //   628: bipush          6
            //   630: aaload         
            //   631: astore          8
            //   633: aload           9
            //   635: bipush          7
            //   637: aaload         
            //   638: astore          24
            //   640: aload           9
            //   642: bipush          8
            //   644: aaload         
            //   645: astore          25
            //   647: aload           9
            //   649: bipush          9
            //   651: aaload         
            //   652: astore          26
            //   654: aload           9
            //   656: bipush          10
            //   658: aaload         
            //   659: astore          27
            //   661: aload           9
            //   663: bipush          11
            //   665: aaload         
            //   666: astore          28
            //   668: aload           9
            //   670: bipush          12
            //   672: aaload         
            //   673: astore          9
            //   675: iload           10
            //   677: ifeq            884
            //   680: ldc             "true"
            //   682: astore_2       
            //   683: ldc             "Temperature_Change"
            //   685: bipush          28
            //   687: anewarray       Ljava/lang/String;
            //   690: dup            
            //   691: iconst_0       
            //   692: ldc             "Fan_Speed"
            //   694: aastore        
            //   695: dup            
            //   696: iconst_1       
            //   697: aload           19
            //   699: aastore        
            //   700: dup            
            //   701: iconst_2       
            //   702: ldc             "CPU_Temperature"
            //   704: aastore        
            //   705: dup            
            //   706: iconst_3       
            //   707: aload           20
            //   709: aastore        
            //   710: dup            
            //   711: iconst_4       
            //   712: ldc             "Inlet_Temperature"
            //   714: aastore        
            //   715: dup            
            //   716: iconst_5       
            //   717: aload           12
            //   719: aastore        
            //   720: dup            
            //   721: bipush          6
            //   723: ldc             "DMD_Temperature"
            //   725: aastore        
            //   726: dup            
            //   727: bipush          7
            //   729: aload           21
            //   731: aastore        
            //   732: dup            
            //   733: bipush          8
            //   735: ldc_w           "LED_Temperature"
            //   738: aastore        
            //   739: dup            
            //   740: bipush          9
            //   742: aload           22
            //   744: aastore        
            //   745: dup            
            //   746: bipush          10
            //   748: ldc_w           "CPU_Average"
            //   751: aastore        
            //   752: dup            
            //   753: bipush          11
            //   755: aload           23
            //   757: aastore        
            //   758: dup            
            //   759: bipush          12
            //   761: ldc_w           "Inlet_Average"
            //   764: aastore        
            //   765: dup            
            //   766: bipush          13
            //   768: aload           8
            //   770: aastore        
            //   771: dup            
            //   772: bipush          14
            //   774: ldc_w           "DMD_Average"
            //   777: aastore        
            //   778: dup            
            //   779: bipush          15
            //   781: aload           24
            //   783: aastore        
            //   784: dup            
            //   785: bipush          16
            //   787: ldc_w           "LED_Average"
            //   790: aastore        
            //   791: dup            
            //   792: bipush          17
            //   794: aload           25
            //   796: aastore        
            //   797: dup            
            //   798: bipush          18
            //   800: ldc_w           "CPU_Max"
            //   803: aastore        
            //   804: dup            
            //   805: bipush          19
            //   807: aload           26
            //   809: aastore        
            //   810: dup            
            //   811: bipush          20
            //   813: ldc_w           "Inlet_Max"
            //   816: aastore        
            //   817: dup            
            //   818: bipush          21
            //   820: aload           27
            //   822: aastore        
            //   823: dup            
            //   824: bipush          22
            //   826: ldc_w           "DMD_Max"
            //   829: aastore        
            //   830: dup            
            //   831: bipush          23
            //   833: aload           28
            //   835: aastore        
            //   836: dup            
            //   837: bipush          24
            //   839: ldc_w           "LED_Max"
            //   842: aastore        
            //   843: dup            
            //   844: bipush          25
            //   846: aload           9
            //   848: aastore        
            //   849: dup            
            //   850: bipush          26
            //   852: ldc_w           "Warning"
            //   855: aastore        
            //   856: dup            
            //   857: bipush          27
            //   859: aload_2        
            //   860: aastore        
            //   861: invokestatic    com/navdy/hud/app/analytics/AnalyticsSupport.localyticsSendEvent:(Ljava/lang/String;[Ljava/lang/String;)V
            //   864: goto            84
            //   867: astore_2       
            //   868: aload_3        
            //   869: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
            //   872: aload_1        
            //   873: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
            //   876: aload_2        
            //   877: athrow         
            //   878: iconst_0       
            //   879: istore          10
            //   881: goto            435
            //   884: ldc_w           "false"
            //   887: astore_2       
            //   888: goto            683
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  0      51     158    168    Ljava/lang/Exception;
            //  54     84     867    878    Any
            //  84     90     232    277    Ljava/lang/Exception;
            //  84     90     867    878    Any
            //  94     100    232    277    Ljava/lang/Exception;
            //  94     100    867    878    Any
            //  103    110    232    277    Ljava/lang/Exception;
            //  103    110    867    878    Any
            //  113    140    232    277    Ljava/lang/Exception;
            //  113    140    867    878    Any
            //  181    229    232    277    Ljava/lang/Exception;
            //  181    229    867    878    Any
            //  233    242    867    878    Any
            //  258    266    867    878    Any
            //  277    345    232    277    Ljava/lang/Exception;
            //  277    345    867    878    Any
            //  345    397    232    277    Ljava/lang/Exception;
            //  345    397    867    878    Any
            //  400    417    232    277    Ljava/lang/Exception;
            //  400    417    867    878    Any
            //  424    432    232    277    Ljava/lang/Exception;
            //  424    432    867    878    Any
            //  435    440    232    277    Ljava/lang/Exception;
            //  435    440    867    878    Any
            //  464    476    232    277    Ljava/lang/Exception;
            //  464    476    867    878    Any
            //  492    502    232    277    Ljava/lang/Exception;
            //  492    502    867    878    Any
            //  510    541    232    277    Ljava/lang/Exception;
            //  510    541    867    878    Any
            //  541    551    232    277    Ljava/lang/Exception;
            //  541    551    867    878    Any
            //  556    590    232    277    Ljava/lang/Exception;
            //  556    590    867    878    Any
            //  683    864    232    277    Ljava/lang/Exception;
            //  683    864    867    878    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.NullPointerException
            //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
            //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
            //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
            //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Unknown Source)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
    
    private static class Threshold
    {
        private final int max;
        private final long minimumTime;
        private long triggerTime;
        
        Threshold(final int max, final long minimumTime) {
            this.triggerTime = -1L;
            this.max = max;
            this.minimumTime = minimumTime;
        }
        
        boolean isHit() {
            return this.triggerTime != -1L && SystemClock.elapsedRealtime() - this.triggerTime >= this.minimumTime;
        }
        
        public void update(final int n) {
            if (n >= this.max) {
                if (this.triggerTime == -1L) {
                    this.triggerTime = SystemClock.elapsedRealtime();
                }
            }
            else {
                this.triggerTime = -1L;
            }
        }
    }
    
    private static class VoiceResultsRunnable implements Runnable
    {
        VoiceSearchAdditionalResultsAction additionalResultsAction;
        private boolean done;
        boolean explicitRetry;
        Integer listItemSelected;
        String microphoneUsed;
        String prefix;
        private NavigationRouteRequest request;
        private int retryCount;
        private Integer searchConfidence;
        List<Destination> searchResults;
        private String searchWords;
        
        VoiceResultsRunnable(final NavigationRouteRequest request) {
            this.listItemSelected = null;
            this.prefix = null;
            this.done = false;
            this.request = request;
            this.searchWords = AnalyticsSupport.voiceSearchWords;
            this.searchConfidence = AnalyticsSupport.voiceSearchConfidence;
            this.retryCount = AnalyticsSupport.voiceSearchCount - 1;
            String microphoneUsed;
            if (AnalyticsSupport.voiceSearchListeningOverBluetooth) {
                microphoneUsed = "bluetooth";
            }
            else {
                microphoneUsed = "phone";
            }
            this.microphoneUsed = microphoneUsed;
            this.searchResults = AnalyticsSupport.voiceSearchResults;
            this.additionalResultsAction = AnalyticsSupport.voiceSearchAdditionalResultsAction;
            this.explicitRetry = AnalyticsSupport.voiceSearchExplicitRetry;
            this.listItemSelected = AnalyticsSupport.voiceSearchListSelection;
            this.prefix = AnalyticsSupport.voiceSearchPrefix;
        }
        
        public void cancel(final VoiceSearchCancelReason voiceSearchCancelReason) {
            synchronized (this) {
                if (!this.done) {
                    AnalyticsSupport.handler.removeCallbacks((Runnable)this);
                    AnalyticsSupport.voiceResultsRunnable = null;
                    this.done = true;
                    AnalyticsSupport.sLogger.i("cancelling voice search reason: " + voiceSearchCancelReason + " data:" + this);
                    sendVoiceSearchEvent(voiceSearchCancelReason, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
                }
            }
        }
        
        @Override
        public void run() {
            synchronized (this) {
                if (!this.done) {
                    AnalyticsSupport.sLogger.i("sending voice search event: " + this);
                    sendVoiceSearchEvent(null, this.request, this.searchWords, this.searchConfidence, this.retryCount, this.microphoneUsed, this.searchResults, this.additionalResultsAction, this.explicitRetry, this.listItemSelected, this.prefix);
                    AnalyticsSupport.voiceResultsRunnable = null;
                    this.done = true;
                }
            }
        }
        
        @Override
        public String toString() {
            return "VoiceResultsRunnable{done=" + this.done + ", request=" + this.request + ", searchWords='" + this.searchWords + '\'' + ", searchConfidence=" + this.searchConfidence + ", retryCount=" + this.retryCount + ", microphoneUsed='" + this.microphoneUsed + '\'' + ", searchResults=" + this.searchResults + ", additionalResultsAction=" + this.additionalResultsAction + ", explicitRetry=" + this.explicitRetry + ", listItemSelected=" + this.listItemSelected + ", prefix='" + this.prefix + '\'' + '}';
        }
    }
    
    public enum VoiceSearchAdditionalResultsAction
    {
        ADDITIONAL_RESULTS_GO("go"), 
        ADDITIONAL_RESULTS_LIST("list"), 
        ADDITIONAL_RESULTS_TIMEOUT("timeout");
        
        public final String tag;
        
        private VoiceSearchAdditionalResultsAction(final String tag) {
            this.tag = tag;
        }
    }
    
    private enum VoiceSearchCancelReason
    {
        cancel_list, 
        cancel_route_calculation, 
        cancel_search, 
        end_trip, 
        new_non_voice_search, 
        new_voice_search;
    }
    
    private enum VoiceSearchProgress
    {
        PROGRESS_DESTINATION_FOUND, 
        PROGRESS_LIST_DISPLAYED, 
        PROGRESS_ROUTE_SELECTED, 
        PROGRESS_SEARCHING;
    }
    
    public enum WakeupReason
    {
        DIAL("Dial"), 
        PHONE("Phone"), 
        POWERBUTTON("Power_Button"), 
        VOLTAGE_SPIKE("Voltage_Spike");
        
        public final String attr;
        
        private WakeupReason(final String attr) {
            this.attr = attr;
        }
    }
}
