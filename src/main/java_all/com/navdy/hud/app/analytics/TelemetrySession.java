package com.navdy.hud.app.analytics;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Pair;
import java.util.LinkedList;
import com.navdy.service.library.log.Logger;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0019\n\u0002\u0018\u0002\n\u0002\b1\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b1\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001:\u0004Æ\u0001Ç\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010¼\u0001\u001a\u00030½\u0001J#\u0010¾\u0001\u001a\u00030½\u00012\u0007\u0010¿\u0001\u001a\u00020\u00042\u0007\u0010À\u0001\u001a\u00020\u00042\u0007\u0010Á\u0001\u001a\u00020\u0004J\b\u0010Â\u0001\u001a\u00030½\u0001J\b\u0010Ã\u0001\u001a\u00030½\u0001J\b\u0010Ä\u0001\u001a\u00030½\u0001J\n\u0010Å\u0001\u001a\u00030½\u0001H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082D¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u00020\u0007X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u000e\u0010\f\u001a\u00020\rX\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0082D¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\u0011X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u0011X\u0086D¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0013R \u0010\u0018\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\r8B@BX\u0082\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR \u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0017\u001a\u00020\u001b8B@BX\u0082\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u000e\u0010\u001f\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\"\u001a\u00020\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u001a\"\u0004\b$\u0010%R\u001a\u0010&\u001a\u00020'X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R$\u0010-\u001a\u00020\u00072\u0006\u0010,\u001a\u00020\u0007@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\t\"\u0004\b/\u00100R$\u00101\u001a\u00020\u00112\u0006\u0010,\u001a\u00020\u0011@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u0013\"\u0004\b3\u00104R$\u00105\u001a\u00020\u001b2\u0006\u0010,\u001a\u00020\u001b@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b6\u0010\u001e\"\u0004\b7\u00108R$\u00109\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u0004@FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u0011\u0010>\u001a\u00020\r8F¢\u0006\u0006\u001a\u0004\b?\u0010\u001aR\u001c\u0010@\u001a\u0004\u0018\u00010AX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bB\u0010C\"\u0004\bD\u0010ER\u001a\u0010F\u001a\u00020\u0007X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bG\u0010\t\"\u0004\bH\u00100R\u001a\u0010I\u001a\u00020\u0011X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bJ\u0010\u0013\"\u0004\bK\u00104R\u001a\u0010L\u001a\u00020\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bM\u0010\u001a\"\u0004\bN\u0010%R\u001a\u0010O\u001a\u00020\u0011X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bP\u0010\u0013\"\u0004\bQ\u00104R\u001c\u0010R\u001a\u00020\r8FX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bS\u0010\u001a\"\u0004\bT\u0010%R\u001a\u0010U\u001a\u00020\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bV\u0010\u001a\"\u0004\bW\u0010%R\u001a\u0010X\u001a\u00020\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bY\u0010\u001a\"\u0004\bZ\u0010%R\u001a\u0010[\u001a\u00020'X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b[\u0010)\"\u0004\b\\\u0010+R$\u0010]\u001a\u00020'2\u0006\u0010,\u001a\u00020'8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b]\u0010)\"\u0004\b^\u0010+R$\u0010_\u001a\u00020'2\u0006\u0010,\u001a\u00020'8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\b_\u0010)\"\u0004\b`\u0010+R$\u0010a\u001a\u00020'2\u0006\u0010,\u001a\u00020'8F@FX\u0086\u000e¢\u0006\f\u001a\u0004\ba\u0010)\"\u0004\bb\u0010+R\u001a\u0010c\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bd\u0010;\"\u0004\be\u0010=R\u001a\u0010f\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bg\u0010;\"\u0004\bh\u0010=R\u001a\u0010i\u001a\u00020\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bj\u0010\u001a\"\u0004\bk\u0010%R\u001a\u0010l\u001a\u00020\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bm\u0010\u001a\"\u0004\bn\u0010%R\u001a\u0010o\u001a\u00020\rX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bp\u0010\u001a\"\u0004\bq\u0010%R\u000e\u0010r\u001a\u00020sX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010t\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bu\u0010;\"\u0004\bv\u0010=R\u001a\u0010w\u001a\u00020\u0004X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bx\u0010;\"\u0004\by\u0010=R\u001a\u0010z\u001a\u00020\u0011X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b{\u0010\u0013\"\u0004\b|\u00104R\u000e\u0010}\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R&\u0010~\u001a\u0015\u0012\u0011\u0012\u000f\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\u0080\u00010\u007f¢\u0006\n\n\u0000\u001a\u0006\b\u0081\u0001\u0010\u0082\u0001R\u001d\u0010\u0083\u0001\u001a\u00020\u0007X\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0084\u0001\u0010\t\"\u0005\b\u0085\u0001\u00100R)\u0010\u0086\u0001\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00048F@BX\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0087\u0001\u0010;\"\u0005\b\u0088\u0001\u0010=R)\u0010\u0089\u0001\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00048F@BX\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u008a\u0001\u0010;\"\u0005\b\u008b\u0001\u0010=R\u001f\u0010\u008c\u0001\u001a\u00020\r8FX\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u008d\u0001\u0010\u001a\"\u0005\b\u008e\u0001\u0010%R)\u0010\u008f\u0001\u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\r8F@BX\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0090\u0001\u0010\u001a\"\u0005\b\u0091\u0001\u0010%R'\u0010\u0092\u0001\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u00048F@BX\u0086\u000e¢\u0006\u000e\u001a\u0005\b\u0093\u0001\u0010;\"\u0005\b\u0094\u0001\u0010=R\u001d\u0010\u0095\u0001\u001a\u00020\u0011X\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0096\u0001\u0010\u0013\"\u0005\b\u0097\u0001\u00104R\u001d\u0010\u0098\u0001\u001a\u00020\u0011X\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u0099\u0001\u0010\u0013\"\u0005\b\u009a\u0001\u00104R\u001d\u0010\u009b\u0001\u001a\u00020\u0011X\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u009c\u0001\u0010\u0013\"\u0005\b\u009d\u0001\u00104R\u001d\u0010\u009e\u0001\u001a\u00020\u0004X\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b\u009f\u0001\u0010;\"\u0005\b \u0001\u0010=R\u001d\u0010¡\u0001\u001a\u00020\rX\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b¢\u0001\u0010\u001a\"\u0005\b£\u0001\u0010%R\u001f\u0010¤\u0001\u001a\u00020\r8FX\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b¥\u0001\u0010\u001a\"\u0005\b¦\u0001\u0010%R'\u0010§\u0001\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u00048F@BX\u0086\u000e¢\u0006\u000e\u001a\u0005\b¨\u0001\u0010;\"\u0005\b©\u0001\u0010=R\u000f\u0010ª\u0001\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u001d\u0010«\u0001\u001a\u00020'X\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b¬\u0001\u0010)\"\u0005\b\u00ad\u0001\u0010+R\u001d\u0010®\u0001\u001a\u00020\u0011X\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b¯\u0001\u0010\u0013\"\u0005\b°\u0001\u00104R \u0010±\u0001\u001a\u00030²\u0001X\u0086\u000e¢\u0006\u0012\n\u0000\u001a\u0006\b³\u0001\u0010´\u0001\"\u0006\bµ\u0001\u0010¶\u0001R\u001f\u0010·\u0001\u001a\u00020\r8FX\u0086\u000e¢\u0006\u0010\n\u0000\u001a\u0005\b¸\u0001\u0010\u001a\"\u0005\b¹\u0001\u0010%R\u000f\u0010º\u0001\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u000f\u0010»\u0001\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006È\u0001" }, d2 = { "Lcom/navdy/hud/app/analytics/TelemetrySession;", "", "()V", "EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS", "", "G_LOWER_THRESHOLD_THRESHOLD", "HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND", "", "getHARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND", "()D", "HARD_BRAKING_THRESHOLD_METERS_PER_SECOND", "getHARD_BRAKING_THRESHOLD_METERS_PER_SECOND", "HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS", "", "HIGH_G_THRESHOLD_TIME_MILLIS", "MAX_G_THRESHOLD", "MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION", "", "getMINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION", "()I", "SPEEDING_SPEED_OVER_SPEED_LIMIT_MS", "SPEED_DATA_ROLLING_WINDOW_SIZE", "getSPEED_DATA_ROLLING_WINDOW_SIZE", "set", "_currentDistance", "get_currentDistance", "()J", "Lcom/navdy/hud/app/analytics/RawSpeed;", "_currentSpeed", "get_currentSpeed", "()Lcom/navdy/hud/app/analytics/RawSpeed;", "_excessiveSpeedingDuration", "_rollingDuration", "_speedingDuration", "confirmedHighGManeuverEndTime", "getConfirmedHighGManeuverEndTime", "setConfirmedHighGManeuverEndTime", "(J)V", "confirmedHighGManueverNotified", "", "getConfirmedHighGManueverNotified", "()Z", "setConfirmedHighGManueverNotified", "(Z)V", "value", "currentMpg", "getCurrentMpg", "setCurrentMpg", "(D)V", "currentRpm", "getCurrentRpm", "setCurrentRpm", "(I)V", "currentSpeed", "getCurrentSpeed", "setCurrentSpeed", "(Lcom/navdy/hud/app/analytics/RawSpeed;)V", "currentSpeedLimit", "getCurrentSpeedLimit", "()F", "setCurrentSpeedLimit", "(F)V", "currentTime", "getCurrentTime", "dataSource", "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "getDataSource", "()Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "setDataSource", "(Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;)V", "eventAverageMpg", "getEventAverageMpg", "setEventAverageMpg", "eventAverageRpm", "getEventAverageRpm", "setEventAverageRpm", "eventMpgSamples", "getEventMpgSamples", "setEventMpgSamples", "eventRpmSamples", "getEventRpmSamples", "setEventRpmSamples", "excessiveSpeedingDuration", "getExcessiveSpeedingDuration", "setExcessiveSpeedingDuration", "excessiveSpeedingStartTime", "getExcessiveSpeedingStartTime", "setExcessiveSpeedingStartTime", "highGManeuverStartTime", "getHighGManeuverStartTime", "setHighGManeuverStartTime", "isAboveWaterMark", "setAboveWaterMark", "isDoingHighGManeuver", "setDoingHighGManeuver", "isExcessiveSpeeding", "setExcessiveSpeeding", "isSpeeding", "setSpeeding", "lastG", "getLastG", "setLastG", "lastGAngle", "getLastGAngle", "setLastGAngle", "lastGTime", "getLastGTime", "setLastGTime", "lastHardAccelerationTime", "getLastHardAccelerationTime", "setLastHardAccelerationTime", "lastHardBrakingTime", "getLastHardBrakingTime", "setLastHardBrakingTime", "logger", "Lcom/navdy/service/library/log/Logger;", "maxG", "getMaxG", "setMaxG", "maxGAngle", "getMaxGAngle", "setMaxGAngle", "maxRpm", "getMaxRpm", "setMaxRpm", "movingStartTime", "rollingSpeedData", "Ljava/util/LinkedList;", "Lkotlin/Pair;", "getRollingSpeedData", "()Ljava/util/LinkedList;", "sessionAverageMpg", "getSessionAverageMpg", "setSessionAverageMpg", "sessionAverageRollingSpeed", "getSessionAverageRollingSpeed", "setSessionAverageRollingSpeed", "sessionAverageSpeed", "getSessionAverageSpeed", "setSessionAverageSpeed", "sessionDistance", "getSessionDistance", "setSessionDistance", "sessionDuration", "getSessionDuration", "setSessionDuration", "sessionExcessiveSpeedingPercentage", "getSessionExcessiveSpeedingPercentage", "setSessionExcessiveSpeedingPercentage", "sessionHardAccelerationCount", "getSessionHardAccelerationCount", "setSessionHardAccelerationCount", "sessionHardBrakingCount", "getSessionHardBrakingCount", "setSessionHardBrakingCount", "sessionHighGCount", "getSessionHighGCount", "setSessionHighGCount", "sessionMaxSpeed", "getSessionMaxSpeed", "setSessionMaxSpeed", "sessionMpgSamples", "getSessionMpgSamples", "setSessionMpgSamples", "sessionRollingDuration", "getSessionRollingDuration", "setSessionRollingDuration", "sessionSpeedingPercentage", "getSessionSpeedingPercentage", "setSessionSpeedingPercentage", "sessionStartTime", "sessionStarted", "getSessionStarted", "setSessionStarted", "sessionTroubleCodeCount", "getSessionTroubleCodeCount", "setSessionTroubleCodeCount", "sessionTroubleCodesReport", "", "getSessionTroubleCodesReport", "()Ljava/lang/String;", "setSessionTroubleCodesReport", "(Ljava/lang/String;)V", "speedingDuration", "getSpeedingDuration", "setSpeedingDuration", "speedingStartTime", "startDistance", "eventReported", "", "setGForce", "x", "y", "z", "speedSourceChanged", "startSession", "stopSession", "updateValuesBasedOnSpeed", "DataSource", "InterestingEvent", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class TelemetrySession
{
    private static final float EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = 0.0f;
    private static final float G_LOWER_THRESHOLD_THRESHOLD = 0.3f;
    private static final double HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND = 3.53;
    private static final double HARD_BRAKING_THRESHOLD_METERS_PER_SECOND = 3.919;
    private static final long HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 3000L;
    private static final long HIGH_G_THRESHOLD_TIME_MILLIS = 500L;
    public static final TelemetrySession INSTANCE;
    private static final float MAX_G_THRESHOLD = 0.45f;
    private static final int MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION = 2000;
    private static final float SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = 0.0f;
    private static final int SPEED_DATA_ROLLING_WINDOW_SIZE = 500;
    private static long _currentDistance;
    private static RawSpeed _currentSpeed;
    private static long _excessiveSpeedingDuration;
    private static long _rollingDuration;
    private static long _speedingDuration;
    private static long confirmedHighGManeuverEndTime;
    private static boolean confirmedHighGManueverNotified;
    private static double currentMpg;
    private static int currentRpm;
    @NotNull
    private static RawSpeed currentSpeed;
    private static float currentSpeedLimit;
    @Nullable
    private static DataSource dataSource;
    private static double eventAverageMpg;
    private static int eventAverageRpm;
    private static long eventMpgSamples;
    private static int eventRpmSamples;
    private static long excessiveSpeedingDuration;
    private static long excessiveSpeedingStartTime;
    private static long highGManeuverStartTime;
    private static boolean isAboveWaterMark;
    private static float lastG;
    private static float lastGAngle;
    private static long lastGTime;
    private static long lastHardAccelerationTime;
    private static long lastHardBrakingTime;
    private static final Logger logger;
    private static float maxG;
    private static float maxGAngle;
    private static int maxRpm;
    private static long movingStartTime;
    @NotNull
    private static final LinkedList<Pair<Long, Float>> rollingSpeedData;
    private static double sessionAverageMpg;
    private static float sessionAverageRollingSpeed;
    private static float sessionAverageSpeed;
    private static long sessionDistance;
    private static long sessionDuration;
    private static int sessionHardAccelerationCount;
    private static int sessionHardBrakingCount;
    private static int sessionHighGCount;
    private static float sessionMaxSpeed;
    private static long sessionMpgSamples;
    private static long sessionRollingDuration;
    private static long sessionStartTime;
    private static boolean sessionStarted;
    private static int sessionTroubleCodeCount;
    @NotNull
    private static String sessionTroubleCodesReport;
    private static long speedingDuration;
    private static long speedingStartTime;
    private static long startDistance;
    
    static {
        new TelemetrySession();
    }
    
    private TelemetrySession() {
        INSTANCE = this;
        logger = new Logger(TelemetrySession.INSTANCE.getClass().getSimpleName());
        SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = TelemetrySessionKt.MPHToMetersPerSecond(8.0f);
        EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS = TelemetrySessionKt.MPHToMetersPerSecond(16.0f);
        MAX_G_THRESHOLD = 0.45f;
        G_LOWER_THRESHOLD_THRESHOLD = 0.3f;
        HIGH_G_THRESHOLD_TIME_MILLIS = 500L;
        HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS = 3000L;
        TelemetrySession.sessionMpgSamples = 1L;
        TelemetrySession.sessionTroubleCodesReport = "";
        TelemetrySession._currentSpeed = new RawSpeed(-1, 0L);
        SPEED_DATA_ROLLING_WINDOW_SIZE = 500;
        HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND = 3.53;
        HARD_BRAKING_THRESHOLD_METERS_PER_SECOND = 3.919;
        MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION = 2000;
        TelemetrySession.currentSpeed = new RawSpeed(-1, 0L);
        rollingSpeedData = new LinkedList<Pair<Long, Float>>();
        TelemetrySession.eventMpgSamples = 1L;
        TelemetrySession.eventRpmSamples = 1;
    }
    
    private final long get_currentDistance() {
        final DataSource dataSource = TelemetrySession.dataSource;
        long totalDistanceTravelledWithMeters;
        if (dataSource != null) {
            totalDistanceTravelledWithMeters = dataSource.totalDistanceTravelledWithMeters();
        }
        else {
            totalDistanceTravelledWithMeters = 0L;
        }
        return totalDistanceTravelledWithMeters;
    }
    
    private final RawSpeed get_currentSpeed() {
        final DataSource dataSource = TelemetrySession.dataSource;
        if (dataSource == null) {
            return new RawSpeed(-1, 0L);
        }
        RawSpeed currentSpeed = dataSource.currentSpeed();
        if (currentSpeed == null) {
            return new RawSpeed(-1, 0L);
        }
        return currentSpeed;
        currentSpeed = new RawSpeed(-1, 0L);
        return currentSpeed;
    }
    
    private final void setSessionAverageRollingSpeed(final float sessionAverageRollingSpeed) {
        TelemetrySession.sessionAverageRollingSpeed = sessionAverageRollingSpeed;
    }
    
    private final void setSessionAverageSpeed(final float sessionAverageSpeed) {
        TelemetrySession.sessionAverageSpeed = sessionAverageSpeed;
    }
    
    private final void setSessionDuration(final long sessionDuration) {
        TelemetrySession.sessionDuration = sessionDuration;
    }
    
    private final void setSessionExcessiveSpeedingPercentage(final float n) {
    }
    
    private final void setSessionSpeedingPercentage(final float n) {
    }
    
    private final void updateValuesBasedOnSpeed() {
        if (TelemetrySession.sessionStarted) {
            final RawSpeed currentSpeed = TelemetrySession.currentSpeed;
            final long currentTime = this.getCurrentTime();
            if (currentSpeed.getSpeed() > 0) {
                if (TelemetrySession.movingStartTime == 0L) {
                    TelemetrySession.movingStartTime = currentTime;
                }
            }
            else if (TelemetrySession.movingStartTime != 0L) {
                TelemetrySession._rollingDuration += currentTime - TelemetrySession.movingStartTime;
                TelemetrySession.movingStartTime = 0L;
            }
            TelemetrySession.sessionMaxSpeed = Math.max(currentSpeed.getSpeed(), TelemetrySession.sessionMaxSpeed);
            if (TelemetrySession.currentSpeedLimit != 0.0f) {
                if (currentSpeed.getSpeed() >= TelemetrySession.currentSpeedLimit + TelemetrySession.SPEEDING_SPEED_OVER_SPEED_LIMIT_MS) {
                    if (TelemetrySession.speedingStartTime == 0L) {
                        TelemetrySession.speedingStartTime = currentTime;
                        final DataSource dataSource = TelemetrySession.dataSource;
                        if (dataSource != null) {
                            dataSource.interestingEventDetected(InterestingEvent.SPEEDING_STARTED);
                        }
                    }
                }
                else if (TelemetrySession.speedingStartTime != 0L) {
                    TelemetrySession._speedingDuration += currentTime - TelemetrySession.speedingStartTime;
                    TelemetrySession.speedingStartTime = 0L;
                    final DataSource dataSource2 = TelemetrySession.dataSource;
                    if (dataSource2 != null) {
                        dataSource2.interestingEventDetected(InterestingEvent.SPEEDING_STOPPED);
                    }
                }
                if (currentSpeed.getSpeed() >= TelemetrySession.currentSpeedLimit + TelemetrySession.EXCESSIVE_SPEEDING_SPEED_OVER_SPEED_LIMIT_MS) {
                    if (TelemetrySession.excessiveSpeedingStartTime == 0L) {
                        TelemetrySession.excessiveSpeedingStartTime = currentTime;
                        final DataSource dataSource3 = TelemetrySession.dataSource;
                        if (dataSource3 != null) {
                            dataSource3.interestingEventDetected(InterestingEvent.EXCESSIVE_SPEEDING_STARTED);
                        }
                    }
                }
                else if (TelemetrySession.excessiveSpeedingStartTime != 0L) {
                    TelemetrySession._excessiveSpeedingDuration += currentTime - TelemetrySession.excessiveSpeedingStartTime;
                    TelemetrySession.excessiveSpeedingStartTime = 0L;
                    final DataSource dataSource4 = TelemetrySession.dataSource;
                    if (dataSource4 != null) {
                        dataSource4.interestingEventDetected(InterestingEvent.EXCESSIVE_SPEEDING_STOPPED);
                    }
                }
            }
            else {
                if (TelemetrySession.speedingStartTime != 0L) {
                    TelemetrySession._speedingDuration += currentTime - TelemetrySession.speedingStartTime;
                    TelemetrySession.speedingStartTime = 0L;
                    final DataSource dataSource5 = TelemetrySession.dataSource;
                    if (dataSource5 != null) {
                        dataSource5.interestingEventDetected(InterestingEvent.SPEEDING_STOPPED);
                    }
                }
                if (TelemetrySession.excessiveSpeedingStartTime != 0L) {
                    TelemetrySession._excessiveSpeedingDuration += currentTime - TelemetrySession.excessiveSpeedingStartTime;
                    TelemetrySession.excessiveSpeedingStartTime = 0L;
                    final DataSource dataSource6 = TelemetrySession.dataSource;
                    if (dataSource6 != null) {
                        dataSource6.interestingEventDetected(InterestingEvent.EXCESSIVE_SPEEDING_STOPPED);
                    }
                }
            }
        }
    }
    
    public final void eventReported() {
        TelemetrySession.eventAverageMpg = 0.0;
        TelemetrySession.eventMpgSamples = 1L;
        TelemetrySession.eventAverageRpm = 0;
        TelemetrySession.eventRpmSamples = 1;
        TelemetrySession.maxG = 0.0f;
        TelemetrySession.maxGAngle = 0.0f;
    }
    
    public final long getConfirmedHighGManeuverEndTime() {
        return TelemetrySession.confirmedHighGManeuverEndTime;
    }
    
    public final boolean getConfirmedHighGManueverNotified() {
        return TelemetrySession.confirmedHighGManueverNotified;
    }
    
    public final double getCurrentMpg() {
        return TelemetrySession.currentMpg;
    }
    
    public final int getCurrentRpm() {
        return TelemetrySession.currentRpm;
    }
    
    @NotNull
    public final RawSpeed getCurrentSpeed() {
        return TelemetrySession.currentSpeed;
    }
    
    public final float getCurrentSpeedLimit() {
        return TelemetrySession.currentSpeedLimit;
    }
    
    public final long getCurrentTime() {
        final DataSource dataSource = TelemetrySession.dataSource;
        long n;
        if (dataSource != null) {
            n = dataSource.absoluteCurrentTime();
        }
        else {
            n = System.currentTimeMillis();
        }
        return n;
    }
    
    @Nullable
    public final DataSource getDataSource() {
        return TelemetrySession.dataSource;
    }
    
    public final double getEventAverageMpg() {
        return TelemetrySession.eventAverageMpg;
    }
    
    public final int getEventAverageRpm() {
        return TelemetrySession.eventAverageRpm;
    }
    
    public final long getEventMpgSamples() {
        return TelemetrySession.eventMpgSamples;
    }
    
    public final int getEventRpmSamples() {
        return TelemetrySession.eventRpmSamples;
    }
    
    public final long getExcessiveSpeedingDuration() {
        long n = 0L;
        final long excessiveSpeedingDuration = TelemetrySession._excessiveSpeedingDuration;
        if (TelemetrySession.excessiveSpeedingStartTime != 0L) {
            n = this.getCurrentTime() - TelemetrySession.excessiveSpeedingStartTime;
        }
        return n + excessiveSpeedingDuration;
    }
    
    public final long getExcessiveSpeedingStartTime() {
        return TelemetrySession.excessiveSpeedingStartTime;
    }
    
    public final double getHARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND() {
        return TelemetrySession.HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND;
    }
    
    public final double getHARD_BRAKING_THRESHOLD_METERS_PER_SECOND() {
        return TelemetrySession.HARD_BRAKING_THRESHOLD_METERS_PER_SECOND;
    }
    
    public final long getHighGManeuverStartTime() {
        return TelemetrySession.highGManeuverStartTime;
    }
    
    public final float getLastG() {
        return TelemetrySession.lastG;
    }
    
    public final float getLastGAngle() {
        return TelemetrySession.lastGAngle;
    }
    
    public final long getLastGTime() {
        return TelemetrySession.lastGTime;
    }
    
    public final long getLastHardAccelerationTime() {
        return TelemetrySession.lastHardAccelerationTime;
    }
    
    public final long getLastHardBrakingTime() {
        return TelemetrySession.lastHardBrakingTime;
    }
    
    public final int getMINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION() {
        return TelemetrySession.MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION;
    }
    
    public final float getMaxG() {
        return TelemetrySession.maxG;
    }
    
    public final float getMaxGAngle() {
        return TelemetrySession.maxGAngle;
    }
    
    public final int getMaxRpm() {
        return TelemetrySession.maxRpm;
    }
    
    @NotNull
    public final LinkedList<Pair<Long, Float>> getRollingSpeedData() {
        return TelemetrySession.rollingSpeedData;
    }
    
    public final int getSPEED_DATA_ROLLING_WINDOW_SIZE() {
        return TelemetrySession.SPEED_DATA_ROLLING_WINDOW_SIZE;
    }
    
    public final double getSessionAverageMpg() {
        return TelemetrySession.sessionAverageMpg;
    }
    
    public final float getSessionAverageRollingSpeed() {
        float n;
        if (this.getSessionRollingDuration() > 0) {
            n = this.getSessionDistance() / (this.getSessionRollingDuration() / 1000);
        }
        else {
            n = 0.0f;
        }
        return n;
    }
    
    public final float getSessionAverageSpeed() {
        float n;
        if (this.getSessionDuration() > 0) {
            n = this.getSessionDistance() / (this.getSessionDuration() / 1000);
        }
        else {
            n = 0.0f;
        }
        return n;
    }
    
    public final long getSessionDistance() {
        return this.get_currentDistance() - TelemetrySession.startDistance;
    }
    
    public final long getSessionDuration() {
        return this.getCurrentTime() - TelemetrySession.sessionStartTime;
    }
    
    public final float getSessionExcessiveSpeedingPercentage() {
        float n;
        if (this.getSessionRollingDuration() == 0L) {
            n = 0.0f;
        }
        else {
            n = this.getExcessiveSpeedingDuration() / this.getSessionRollingDuration();
        }
        return n;
    }
    
    public final int getSessionHardAccelerationCount() {
        return TelemetrySession.sessionHardAccelerationCount;
    }
    
    public final int getSessionHardBrakingCount() {
        return TelemetrySession.sessionHardBrakingCount;
    }
    
    public final int getSessionHighGCount() {
        return TelemetrySession.sessionHighGCount;
    }
    
    public final float getSessionMaxSpeed() {
        return TelemetrySession.sessionMaxSpeed;
    }
    
    public final long getSessionMpgSamples() {
        return TelemetrySession.sessionMpgSamples;
    }
    
    public final long getSessionRollingDuration() {
        long n = 0L;
        final long rollingDuration = TelemetrySession._rollingDuration;
        if (TelemetrySession.movingStartTime != 0L) {
            n = this.getCurrentTime() - TelemetrySession.movingStartTime;
        }
        return n + rollingDuration;
    }
    
    public final float getSessionSpeedingPercentage() {
        float n;
        if (this.getSessionRollingDuration() == 0L) {
            n = 0.0f;
        }
        else {
            n = this.getSpeedingDuration() / this.getSessionRollingDuration();
        }
        return n;
    }
    
    public final boolean getSessionStarted() {
        return TelemetrySession.sessionStarted;
    }
    
    public final int getSessionTroubleCodeCount() {
        return TelemetrySession.sessionTroubleCodeCount;
    }
    
    @NotNull
    public final String getSessionTroubleCodesReport() {
        return TelemetrySession.sessionTroubleCodesReport;
    }
    
    public final long getSpeedingDuration() {
        long n = 0L;
        final long speedingDuration = TelemetrySession._speedingDuration;
        if (TelemetrySession.speedingStartTime != 0L) {
            n = this.getCurrentTime() - TelemetrySession.speedingStartTime;
        }
        return n + speedingDuration;
    }
    
    public final boolean isAboveWaterMark() {
        return TelemetrySession.isAboveWaterMark;
    }
    
    public final boolean isDoingHighGManeuver() {
        return TelemetrySession.confirmedHighGManueverNotified;
    }
    
    public final boolean isExcessiveSpeeding() {
        boolean b = false;
        if (TelemetrySession.excessiveSpeedingStartTime > 0) {
            b = true;
        }
        return b;
    }
    
    public final boolean isSpeeding() {
        boolean b = false;
        if (TelemetrySession.speedingStartTime > 0) {
            b = true;
        }
        return b;
    }
    
    public final void setAboveWaterMark(final boolean isAboveWaterMark) {
        TelemetrySession.isAboveWaterMark = isAboveWaterMark;
    }
    
    public final void setConfirmedHighGManeuverEndTime(final long confirmedHighGManeuverEndTime) {
        TelemetrySession.confirmedHighGManeuverEndTime = confirmedHighGManeuverEndTime;
    }
    
    public final void setConfirmedHighGManueverNotified(final boolean confirmedHighGManueverNotified) {
        TelemetrySession.confirmedHighGManueverNotified = confirmedHighGManueverNotified;
    }
    
    public final void setCurrentMpg(final double currentMpg) {
        TelemetrySession.currentMpg = currentMpg;
        if (currentMpg > 0) {
            TelemetrySession.sessionAverageMpg += (currentMpg - TelemetrySession.sessionAverageMpg) / TelemetrySession.sessionMpgSamples;
            ++TelemetrySession.sessionMpgSamples;
            TelemetrySession.eventAverageMpg += (currentMpg - TelemetrySession.eventAverageMpg) / TelemetrySession.eventMpgSamples;
            ++TelemetrySession.eventMpgSamples;
        }
    }
    
    public final void setCurrentRpm(final int currentRpm) {
        TelemetrySession.currentRpm = currentRpm;
        if (currentRpm > 0) {
            TelemetrySession.maxRpm = Math.max(currentRpm, TelemetrySession.maxRpm);
            TelemetrySession.eventAverageRpm += (currentRpm - TelemetrySession.eventAverageRpm) / TelemetrySession.eventRpmSamples;
            ++TelemetrySession.eventRpmSamples;
        }
    }
    
    public final void setCurrentSpeed(@NotNull final RawSpeed currentSpeed) {
        Intrinsics.checkParameterIsNotNull(currentSpeed, "value");
        TelemetrySession.currentSpeed = currentSpeed;
        this.updateValuesBasedOnSpeed();
        if ((int)currentSpeed.getSpeed() == -1) {
            TelemetrySession.rollingSpeedData.clear();
        }
        else {
            final DataSource dataSource = TelemetrySession.dataSource;
            if (dataSource != null && dataSource.isHighAccuracySpeedAvailable()) {
                final long timeStamp = currentSpeed.getTimeStamp();
                TelemetrySession.rollingSpeedData.add(new Pair<Long, Float>(timeStamp, currentSpeed.getSpeed()));
                if (TelemetrySession.rollingSpeedData.size() != 1) {
                    int n = 1;
                    Pair<Long, Float> pair = null;
                    int n2;
                    do {
                        final Pair<Long, Float> pair2 = TelemetrySession.rollingSpeedData.peek();
                        Pair<Long, Float> pair3;
                        if (pair2 != null && timeStamp - pair2.getFirst().longValue() > TelemetrySession.SPEED_DATA_ROLLING_WINDOW_SIZE) {
                            pair3 = TelemetrySession.rollingSpeedData.remove();
                            n2 = n;
                        }
                        else {
                            final boolean b = false;
                            pair3 = pair;
                            n2 = (b ? 1 : 0);
                            if (pair != null) {
                                TelemetrySession.rollingSpeedData.addFirst(pair);
                                pair3 = pair;
                                n2 = (b ? 1 : 0);
                            }
                        }
                        pair = pair3;
                    } while ((n = n2) != 0);
                    final Pair<Long, Float> pair4 = TelemetrySession.rollingSpeedData.peek();
                    final double n3 = (timeStamp - pair4.getFirst().longValue()) / 1000;
                    if (n3 >= 0.5f) {
                        if (currentSpeed.getSpeed() > pair4.getSecond().floatValue()) {
                            final double n4 = (currentSpeed.getSpeed() - pair4.getSecond().floatValue()) / n3;
                            final DataSource dataSource2 = TelemetrySession.dataSource;
                            if (dataSource2 != null && dataSource2.isVerboseLoggingNeeded()) {
                                TelemetrySession.logger.d("RawSpeed " + currentSpeed + ", Change is speed : " + (currentSpeed.getSpeed() - pair4.getSecond().floatValue()) + " Acceleration : " + n4 + " " + "\n" + " Rolling data : " + TelemetrySession.rollingSpeedData);
                            }
                            if (n4 > TelemetrySession.HARD_ACCELERATION_THRESHOLD_METERS_PER_SECOND && (TelemetrySession.lastHardAccelerationTime == 0L || timeStamp - TelemetrySession.lastHardAccelerationTime > TelemetrySession.MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION)) {
                                TelemetrySession.logger.d("Hard Acceleration detected, Acceleration " + n4 + ", In time :" + n3);
                                TelemetrySession.logger.d("Current RawSpeed : " + currentSpeed + ", Old RawSpeed : " + pair4.getSecond().floatValue());
                                TelemetrySession.logger.d("Rolling Window : " + TelemetrySession.rollingSpeedData);
                                TelemetrySession.logger.d("Last G , g : " + TelemetrySession.lastG + " , gAngle : " + TelemetrySession.lastGAngle + " , time : " + TelemetrySession.lastGTime);
                                TelemetrySession.lastHardAccelerationTime = timeStamp;
                                ++TelemetrySession.sessionHardAccelerationCount;
                                final DataSource dataSource3 = TelemetrySession.dataSource;
                                if (dataSource3 != null) {
                                    dataSource3.interestingEventDetected(InterestingEvent.HARD_ACCELERATION);
                                }
                            }
                        }
                        else {
                            final double n5 = (pair4.getSecond().floatValue() - currentSpeed.getSpeed()) / n3;
                            final DataSource dataSource4 = TelemetrySession.dataSource;
                            if (dataSource4 != null && dataSource4.isVerboseLoggingNeeded()) {
                                TelemetrySession.logger.d("RawSpeed " + currentSpeed + ", Change is speed : " + (currentSpeed.getSpeed() - pair4.getSecond().floatValue()) + " Braking : " + n5 + " " + "\n" + " Rolling data : " + TelemetrySession.rollingSpeedData);
                            }
                            if (n5 > TelemetrySession.HARD_BRAKING_THRESHOLD_METERS_PER_SECOND && (TelemetrySession.lastHardBrakingTime == 0L || timeStamp - TelemetrySession.lastHardBrakingTime > TelemetrySession.MINIMUM_TIME_BETWEEN_HARD_ACCELERATION_DECELERATION)) {
                                TelemetrySession.logger.d("Hard Deceleration detected, Acceleration " + n5 + ", In time :" + n3);
                                TelemetrySession.logger.d("Current RawSpeed : " + currentSpeed + ", Old RawSpeed : " + pair4.getSecond().floatValue());
                                TelemetrySession.logger.d("Rolling Window : " + TelemetrySession.rollingSpeedData);
                                TelemetrySession.logger.d("Last G , g : " + TelemetrySession.lastG + " , gAngle : " + TelemetrySession.lastGAngle + " , time : " + TelemetrySession.lastGTime);
                                TelemetrySession.lastHardBrakingTime = timeStamp;
                                ++TelemetrySession.sessionHardBrakingCount;
                                final DataSource dataSource5 = TelemetrySession.dataSource;
                                if (dataSource5 != null) {
                                    dataSource5.interestingEventDetected(InterestingEvent.HARD_BRAKING);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    public final void setCurrentSpeedLimit(float currentSpeedLimit) {
        if (currentSpeedLimit <= 0) {
            currentSpeedLimit = 0.0f;
        }
        TelemetrySession.currentSpeedLimit = currentSpeedLimit;
        this.updateValuesBasedOnSpeed();
    }
    
    public final void setDataSource(@Nullable final DataSource dataSource) {
        TelemetrySession.dataSource = dataSource;
    }
    
    public final void setDoingHighGManeuver(final boolean b) {
    }
    
    public final void setEventAverageMpg(final double eventAverageMpg) {
        TelemetrySession.eventAverageMpg = eventAverageMpg;
    }
    
    public final void setEventAverageRpm(final int eventAverageRpm) {
        TelemetrySession.eventAverageRpm = eventAverageRpm;
    }
    
    public final void setEventMpgSamples(final long eventMpgSamples) {
        TelemetrySession.eventMpgSamples = eventMpgSamples;
    }
    
    public final void setEventRpmSamples(final int eventRpmSamples) {
        TelemetrySession.eventRpmSamples = eventRpmSamples;
    }
    
    public final void setExcessiveSpeeding(final boolean b) {
    }
    
    public final void setExcessiveSpeedingDuration(final long excessiveSpeedingDuration) {
        TelemetrySession.excessiveSpeedingDuration = excessiveSpeedingDuration;
    }
    
    public final void setExcessiveSpeedingStartTime(final long excessiveSpeedingStartTime) {
        TelemetrySession.excessiveSpeedingStartTime = excessiveSpeedingStartTime;
    }
    
    public final void setGForce(float n, float n2, float n3) {
        final float max = Math.max(Math.abs(n), Math.abs(n2));
        n3 = (n2 = (float)(Math.atan(-n2 / n) * 180.0f / 3.141592653589793));
        if (n < 0) {
            n2 = n3 + 180.0f;
        }
        n = (360 + n2) % 360;
        TelemetrySession.lastG = max;
        TelemetrySession.lastGAngle = n;
        final long n4 = TelemetrySession.lastGTime = this.getCurrentTime();
        if (max > TelemetrySession.maxG) {
            TelemetrySession.maxG = max;
            TelemetrySession.maxGAngle = n;
        }
        if (max > TelemetrySession.MAX_G_THRESHOLD) {
            TelemetrySession.logger.d("G " + max + " , Angle " + n);
            int n5;
            if (125.0f <= n && n <= 235.0f) {
                n5 = 1;
            }
            else {
                n5 = 0;
            }
            int n8 = 0;
            Label_0216: {
                if (n5 == 0) {
                    int n6;
                    if (305.0f <= n && n <= 360.0f) {
                        n6 = 1;
                    }
                    else {
                        n6 = 0;
                    }
                    if (n6 == 0) {
                        int n7;
                        if (0.0f <= n && n <= 55.0f) {
                            n7 = 1;
                        }
                        else {
                            n7 = 0;
                        }
                        if (n7 == 0) {
                            n8 = 0;
                            break Label_0216;
                        }
                    }
                }
                n8 = 1;
            }
            if (n8 != 0) {
                if (!TelemetrySession.isAboveWaterMark && TelemetrySessionKt.timeSince(n4, TelemetrySession.confirmedHighGManeuverEndTime) > TelemetrySession.HIGH_G_MINIMUM_INTERVAL_BETWEEN_EVENTS_MILLIS) {
                    TelemetrySession.logger.d("High G maneuver started ");
                    TelemetrySession.highGManeuverStartTime = n4;
                }
                if (TelemetrySession.isAboveWaterMark && !TelemetrySession.confirmedHighGManueverNotified && TelemetrySession.highGManeuverStartTime != 0L && TelemetrySessionKt.timeSince(n4, TelemetrySession.highGManeuverStartTime) > TelemetrySession.HIGH_G_THRESHOLD_TIME_MILLIS) {
                    ++TelemetrySession.sessionHighGCount;
                    TelemetrySession.confirmedHighGManueverNotified = true;
                    final DataSource dataSource = TelemetrySession.dataSource;
                    if (dataSource != null) {
                        dataSource.interestingEventDetected(InterestingEvent.HIGH_G_STARTED);
                    }
                    TelemetrySession.logger.d("High G maneuver confirmed and notified");
                }
                TelemetrySession.isAboveWaterMark = true;
            }
        }
        else if (max < TelemetrySession.G_LOWER_THRESHOLD_THRESHOLD) {
            if (TelemetrySession.highGManeuverStartTime != 0L) {
                TelemetrySession.highGManeuverStartTime = 0L;
                TelemetrySession.logger.d("High G maneuver ended");
                if (TelemetrySession.confirmedHighGManueverNotified) {
                    TelemetrySession.confirmedHighGManeuverEndTime = n4;
                    TelemetrySession.confirmedHighGManueverNotified = false;
                    final DataSource dataSource2 = TelemetrySession.dataSource;
                    if (dataSource2 != null) {
                        dataSource2.interestingEventDetected(InterestingEvent.HIGH_G_ENDED);
                    }
                }
            }
            TelemetrySession.isAboveWaterMark = false;
        }
    }
    
    public final void setHighGManeuverStartTime(final long highGManeuverStartTime) {
        TelemetrySession.highGManeuverStartTime = highGManeuverStartTime;
    }
    
    public final void setLastG(final float lastG) {
        TelemetrySession.lastG = lastG;
    }
    
    public final void setLastGAngle(final float lastGAngle) {
        TelemetrySession.lastGAngle = lastGAngle;
    }
    
    public final void setLastGTime(final long lastGTime) {
        TelemetrySession.lastGTime = lastGTime;
    }
    
    public final void setLastHardAccelerationTime(final long lastHardAccelerationTime) {
        TelemetrySession.lastHardAccelerationTime = lastHardAccelerationTime;
    }
    
    public final void setLastHardBrakingTime(final long lastHardBrakingTime) {
        TelemetrySession.lastHardBrakingTime = lastHardBrakingTime;
    }
    
    public final void setMaxG(final float maxG) {
        TelemetrySession.maxG = maxG;
    }
    
    public final void setMaxGAngle(final float maxGAngle) {
        TelemetrySession.maxGAngle = maxGAngle;
    }
    
    public final void setMaxRpm(final int maxRpm) {
        TelemetrySession.maxRpm = maxRpm;
    }
    
    public final void setSessionAverageMpg(final double sessionAverageMpg) {
        TelemetrySession.sessionAverageMpg = sessionAverageMpg;
    }
    
    public final void setSessionDistance(final long sessionDistance) {
        TelemetrySession.sessionDistance = sessionDistance;
    }
    
    public final void setSessionHardAccelerationCount(final int sessionHardAccelerationCount) {
        TelemetrySession.sessionHardAccelerationCount = sessionHardAccelerationCount;
    }
    
    public final void setSessionHardBrakingCount(final int sessionHardBrakingCount) {
        TelemetrySession.sessionHardBrakingCount = sessionHardBrakingCount;
    }
    
    public final void setSessionHighGCount(final int sessionHighGCount) {
        TelemetrySession.sessionHighGCount = sessionHighGCount;
    }
    
    public final void setSessionMaxSpeed(final float sessionMaxSpeed) {
        TelemetrySession.sessionMaxSpeed = sessionMaxSpeed;
    }
    
    public final void setSessionMpgSamples(final long sessionMpgSamples) {
        TelemetrySession.sessionMpgSamples = sessionMpgSamples;
    }
    
    public final void setSessionRollingDuration(final long sessionRollingDuration) {
        TelemetrySession.sessionRollingDuration = sessionRollingDuration;
    }
    
    public final void setSessionStarted(final boolean sessionStarted) {
        TelemetrySession.sessionStarted = sessionStarted;
    }
    
    public final void setSessionTroubleCodeCount(final int sessionTroubleCodeCount) {
        TelemetrySession.sessionTroubleCodeCount = sessionTroubleCodeCount;
    }
    
    public final void setSessionTroubleCodesReport(@NotNull final String sessionTroubleCodesReport) {
        Intrinsics.checkParameterIsNotNull(sessionTroubleCodesReport, "set");
        TelemetrySession.sessionTroubleCodesReport = sessionTroubleCodesReport;
    }
    
    public final void setSpeeding(final boolean b) {
    }
    
    public final void setSpeedingDuration(final long speedingDuration) {
        TelemetrySession.speedingDuration = speedingDuration;
    }
    
    public final void speedSourceChanged() {
        if (TelemetrySession.sessionStarted) {
            TelemetrySession.rollingSpeedData.clear();
        }
    }
    
    public final void startSession() {
        TelemetrySession.sessionStartTime = this.getCurrentTime();
        TelemetrySession.sessionDuration = 0L;
        TelemetrySession.startDistance = this.get_currentDistance();
        TelemetrySession.rollingSpeedData.clear();
        this.setCurrentSpeed(this.get_currentSpeed());
        TelemetrySession.sessionRollingDuration = 0L;
        TelemetrySession._speedingDuration = 0L;
        TelemetrySession._rollingDuration = 0L;
        TelemetrySession._excessiveSpeedingDuration = 0L;
        TelemetrySession.excessiveSpeedingStartTime = 0L;
        TelemetrySession.sessionMaxSpeed = 0.0f;
        TelemetrySession.movingStartTime = 0L;
        TelemetrySession.speedingStartTime = 0L;
        TelemetrySession.sessionStarted = true;
        TelemetrySession.sessionHighGCount = 0;
    }
    
    public final void stopSession() {
        TelemetrySession.sessionStarted = false;
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH&J\b\u0010\n\u001a\u00020\u000bH&J\b\u0010\f\u001a\u00020\u000bH&J\b\u0010\r\u001a\u00020\u0003H&¨\u0006\u000e" }, d2 = { "Lcom/navdy/hud/app/analytics/TelemetrySession$DataSource;", "", "absoluteCurrentTime", "", "currentSpeed", "Lcom/navdy/hud/app/analytics/RawSpeed;", "interestingEventDetected", "", "event", "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "isHighAccuracySpeedAvailable", "", "isVerboseLoggingNeeded", "totalDistanceTravelledWithMeters", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public interface DataSource
    {
        long absoluteCurrentTime();
        
        @NotNull
        RawSpeed currentSpeed();
        
        void interestingEventDetected(@NotNull final InterestingEvent p0);
        
        boolean isHighAccuracySpeedAvailable();
        
        boolean isVerboseLoggingNeeded();
        
        long totalDistanceTravelledWithMeters();
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u000b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f" }, d2 = { "Lcom/navdy/hud/app/analytics/TelemetrySession$InterestingEvent;", "", "(Ljava/lang/String;I)V", "NONE", "HARD_BRAKING", "HARD_ACCELERATION", "HIGH_G_STARTED", "HIGH_G_ENDED", "SPEEDING_STARTED", "SPEEDING_STOPPED", "EXCESSIVE_SPEEDING_STARTED", "EXCESSIVE_SPEEDING_STOPPED", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public enum InterestingEvent
    {
        EXCESSIVE_SPEEDING_STARTED, 
        EXCESSIVE_SPEEDING_STOPPED, 
        HARD_ACCELERATION, 
        HARD_BRAKING, 
        HIGH_G_ENDED, 
        HIGH_G_STARTED, 
        NONE, 
        SPEEDING_STARTED, 
        SPEEDING_STOPPED;
    }
}
