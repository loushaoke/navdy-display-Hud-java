package com.navdy.hud.app.bluetooth.pbap;

import android.util.Log;
import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters;

class BluetoothPbapRequestPullVcardListingSize extends BluetoothPbapRequest
{
    private static final String TAG = "BTPbapReqPullVcardLSize";
    private static final String TYPE = "x-bt/vcard-listing";
    private int mSize;
    
    public BluetoothPbapRequestPullVcardListingSize(final String s) {
        this.mHeaderSet.setHeader(1, s);
        this.mHeaderSet.setHeader(66, "x-bt/vcard-listing");
        final ObexAppParameters obexAppParameters = new ObexAppParameters();
        obexAppParameters.add((byte)4, (short)0);
        obexAppParameters.addToHeaderSet(this.mHeaderSet);
    }
    
    public int getSize() {
        return this.mSize;
    }
    
    @Override
    protected void readResponseHeaders(final HeaderSet set) {
        Log.v("BTPbapReqPullVcardLSize", "readResponseHeaders");
        this.mSize = ObexAppParameters.fromHeaderSet(set).getShort((byte)8);
    }
}
