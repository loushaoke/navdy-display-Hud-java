package com.navdy.hud.app.bluetooth.vcard;

import java.util.Iterator;
import android.provider.ContactsContract;
import android.provider.ContactsContract;
import android.content.ContentValues;
import java.util.ArrayList;
import android.content.Entity;
import android.content.Entity;
import android.provider.ContactsContract;
import java.lang.reflect.InvocationTargetException;
import android.content.EntityIterator;
import java.util.List;
import java.lang.reflect.Method;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.text.TextUtils;
import android.content.Context;
import java.util.HashMap;
import android.database.Cursor;
import android.net.Uri;
import android.content.ContentResolver;
import java.util.Map;

public class VCardComposer
{
    private static final boolean DEBUG = false;
    public static final String FAILURE_REASON_FAILED_TO_GET_DATABASE_INFO = "Failed to get database information";
    public static final String FAILURE_REASON_NOT_INITIALIZED = "The vCard composer object is not correctly initialized";
    public static final String FAILURE_REASON_NO_ENTRY = "There's no exportable in the database";
    public static final String FAILURE_REASON_UNSUPPORTED_URI = "The Uri vCard composer received is not supported by the composer.";
    private static final String LOG_TAG = "VCardComposer";
    public static final String NO_ERROR = "No error";
    private static final String SHIFT_JIS = "SHIFT_JIS";
    private static final String UTF_8 = "UTF-8";
    private static final String[] sContactsProjection;
    private static final Map<Integer, String> sImMap;
    private final String mCharset;
    private final ContentResolver mContentResolver;
    private Uri mContentUriForRawContactsEntity;
    private Cursor mCursor;
    private boolean mCursorSuppliedFromOutside;
    private String mErrorReason;
    private boolean mFirstVCardEmittedInDoCoMoCase;
    private int mIdColumn;
    private boolean mInitDone;
    private final boolean mIsDoCoMo;
    private VCardPhoneNumberTranslationCallback mPhoneTranslationCallback;
    private boolean mTerminateCalled;
    private final int mVCardType;
    
    static {
        (sImMap = new HashMap<Integer, String>()).put(0, "X-AIM");
        VCardComposer.sImMap.put(1, "X-MSN");
        VCardComposer.sImMap.put(2, "X-YAHOO");
        VCardComposer.sImMap.put(6, "X-ICQ");
        VCardComposer.sImMap.put(7, "X-JABBER");
        VCardComposer.sImMap.put(3, "X-SKYPE-USERNAME");
        sContactsProjection = new String[] { "_id" };
    }
    
    public VCardComposer(final Context context) {
        this(context, VCardConfig.VCARD_TYPE_DEFAULT, null, true);
    }
    
    public VCardComposer(final Context context, final int n) {
        this(context, n, null, true);
    }
    
    public VCardComposer(final Context context, final int n, final String s) {
        this(context, n, s, true);
    }
    
    public VCardComposer(final Context context, final int n, final String s, final boolean b) {
        this(context, context.getContentResolver(), n, s, b);
    }
    
    public VCardComposer(final Context context, final int n, final boolean b) {
        this(context, n, null, b);
    }
    
    public VCardComposer(final Context context, final ContentResolver mContentResolver, final int mvCardType, final String s, final boolean b) {
        final boolean b2 = true;
        this.mErrorReason = "No error";
        this.mTerminateCalled = true;
        this.mVCardType = mvCardType;
        this.mContentResolver = mContentResolver;
        this.mIsDoCoMo = VCardConfig.isDoCoMo(mvCardType);
        String mCharset = s;
        if (TextUtils.isEmpty((CharSequence)s)) {
            mCharset = "UTF-8";
        }
        boolean b3 = b2;
        if (VCardConfig.isVersion30(mvCardType)) {
            b3 = (!"UTF-8".equalsIgnoreCase(mCharset) && b2);
        }
        if (this.mIsDoCoMo || b3) {
            if ("SHIFT_JIS".equalsIgnoreCase(mCharset)) {
                this.mCharset = mCharset;
            }
            else if (TextUtils.isEmpty((CharSequence)mCharset)) {
                this.mCharset = "SHIFT_JIS";
            }
            else {
                this.mCharset = mCharset;
            }
        }
        else if (TextUtils.isEmpty((CharSequence)mCharset)) {
            this.mCharset = "UTF-8";
        }
        else {
            this.mCharset = mCharset;
        }
        Log.d("VCardComposer", "Use the charset \"" + this.mCharset + "\"");
    }
    
    private void closeCursorIfAppropriate() {
        if (this.mCursorSuppliedFromOutside || this.mCursor == null) {
            return;
        }
        while (true) {
            try {
                this.mCursor.close();
                this.mCursor = null;
            }
            catch (SQLiteException ex) {
                Log.e("VCardComposer", "SQLiteException on Cursor#close(): " + ex.getMessage());
                continue;
            }
            break;
        }
    }
    
    private String createOneEntryInternal(String buildVCard, Method entityIterator) {
        while (true) {
            final HashMap<Object, List<?>> hashMap = new HashMap<Object, List<?>>();
            Object o = null;
            Object values = null;
            Object o2 = o;
        Label_0306:
            while (true) {
                Object o3;
                Object asString;
                try {
                    o3 = this.mContentUriForRawContactsEntity;
                    o2 = o;
                    asString = new String[] { buildVCard };
                    if (entityIterator != null) {
                        o2 = o;
                        try {
                            entityIterator = (IllegalArgumentException)((Method)entityIterator).invoke(null, this.mContentResolver, o3, "contact_id=?", asString, null);
                            if (entityIterator == null) {
                                o2 = entityIterator;
                                Log.e("VCardComposer", "EntityIterator is null");
                                o2 = (buildVCard = "");
                                if (entityIterator != null) {
                                    ((EntityIterator)entityIterator).close();
                                    buildVCard = (String)o2;
                                }
                                return buildVCard;
                            }
                            break Label_0306;
                        }
                        catch (IllegalArgumentException entityIterator) {
                            o2 = o;
                            o2 = o;
                            asString = new StringBuilder();
                            o2 = o;
                            Log.e("VCardComposer", ((StringBuilder)asString).append("IllegalArgumentException has been thrown: ").append(entityIterator.getMessage()).toString());
                            entityIterator = (IllegalArgumentException)values;
                        }
                        catch (IllegalAccessException entityIterator) {
                            o2 = o;
                            o2 = o;
                            asString = new StringBuilder();
                            o2 = o;
                            Log.e("VCardComposer", ((StringBuilder)asString).append("IllegalAccessException has been thrown: ").append(entityIterator.getMessage()).toString());
                            entityIterator = (IllegalArgumentException)values;
                        }
                        catch (InvocationTargetException ex) {
                            o2 = o;
                            Log.e("VCardComposer", "InvocationTargetException has been thrown: ", (Throwable)ex);
                            o2 = o;
                            o2 = o;
                            final RuntimeException ex2 = new RuntimeException("InvocationTargetException has been thrown");
                            o2 = o;
                            throw ex2;
                        }
                    }
                }
                finally {}
                o2 = o;
                entityIterator = (IllegalArgumentException)ContactsContract$RawContacts.newEntityIterator(this.mContentResolver.query((Uri)o3, (String[])null, "contact_id=?", (String[])asString, (String)null));
                continue;
            }
            if (((EntityIterator)entityIterator).hasNext()) {
                while (true) {
                    o2 = entityIterator;
                    if (!((EntityIterator)entityIterator).hasNext()) {
                        break;
                    }
                    final Object o3 = ((Entity)((EntityIterator)entityIterator).next()).getSubValues().iterator();
                    while (((Iterator)o3).hasNext()) {
                        values = ((Iterator<Entity$NamedContentValues>)o3).next().values;
                        final Object asString = ((ContentValues)values).getAsString("mimetype");
                        if (asString != null) {
                            o = hashMap.get(asString);
                            List<ContentValues> list;
                            if ((list = (List<ContentValues>)o) == null) {
                                list = new ArrayList<ContentValues>();
                                hashMap.put(asString, list);
                            }
                            list.add((ContentValues)values);
                        }
                    }
                }
                if (entityIterator != null) {
                    ((EntityIterator)entityIterator).close();
                }
                buildVCard = this.buildVCard((Map<String, List<ContentValues>>)hashMap);
                return buildVCard;
            }
            o = new StringBuilder();
            final String s;
            Log.w("VCardComposer", ((StringBuilder)o).append("Data does not exist. contactId: ").append(s).toString());
            o2 = (buildVCard = "");
            if (entityIterator != null) {
                ((EntityIterator)entityIterator).close();
                buildVCard = (String)o2;
                return buildVCard;
            }
            return buildVCard;
        }
    }
    
    private boolean initInterCursorCreationPart(final Uri uri, final String[] array, final String s, final String[] array2, final String s2) {
        this.mCursorSuppliedFromOutside = false;
        this.mCursor = this.mContentResolver.query(uri, array, s, array2, s2);
        boolean b;
        if (this.mCursor == null) {
            Log.e("VCardComposer", String.format("Cursor became null unexpectedly", new Object[0]));
            this.mErrorReason = "Failed to get database information";
            b = false;
        }
        else {
            b = true;
        }
        return b;
    }
    
    private boolean initInterFirstPart(Uri content_URI) {
        if (content_URI == null) {
            content_URI = ContactsContract$RawContactsEntity.CONTENT_URI;
        }
        this.mContentUriForRawContactsEntity = content_URI;
        boolean b;
        if (this.mInitDone) {
            Log.e("VCardComposer", "init() is already called");
            b = false;
        }
        else {
            b = true;
        }
        return b;
    }
    
    private boolean initInterLastPart() {
        this.mInitDone = true;
        this.mTerminateCalled = false;
        return true;
    }
    
    private boolean initInterMainPart() {
        boolean b = false;
        if (this.mCursor.getCount() == 0 || !this.mCursor.moveToFirst()) {
            this.closeCursorIfAppropriate();
        }
        else {
            this.mIdColumn = this.mCursor.getColumnIndex("_id");
            if (this.mIdColumn >= 0) {
                b = true;
            }
        }
        return b;
    }
    
    public String buildVCard(final Map<String, List<ContentValues>> map) {
        String string;
        if (map == null) {
            Log.e("VCardComposer", "The given map is null. Ignore and return empty String");
            string = "";
        }
        else {
            final VCardBuilder vCardBuilder = new VCardBuilder(this.mVCardType, this.mCharset);
            vCardBuilder.appendNameProperties(map.get("vnd.android.cursor.item/name")).appendNickNames(map.get("vnd.android.cursor.item/nickname")).appendPhones(map.get("vnd.android.cursor.item/phone_v2"), this.mPhoneTranslationCallback).appendEmails(map.get("vnd.android.cursor.item/email_v2")).appendPostals(map.get("vnd.android.cursor.item/postal-address_v2")).appendOrganizations(map.get("vnd.android.cursor.item/organization")).appendWebsites(map.get("vnd.android.cursor.item/website"));
            if ((this.mVCardType & 0x800000) == 0x0) {
                vCardBuilder.appendPhotos(map.get("vnd.android.cursor.item/photo"));
            }
            vCardBuilder.appendNotes(map.get("vnd.android.cursor.item/note")).appendEvents(map.get("vnd.android.cursor.item/contact_event")).appendIms(map.get("vnd.android.cursor.item/im")).appendSipAddresses(map.get("vnd.android.cursor.item/sip_address")).appendRelation(map.get("vnd.android.cursor.item/relation"));
            string = vCardBuilder.toString();
        }
        return string;
    }
    
    public String createOneEntry() {
        return this.createOneEntry(null);
    }
    
    public String createOneEntry(final Method method) {
        if (this.mIsDoCoMo && !this.mFirstVCardEmittedInDoCoMoCase) {
            this.mFirstVCardEmittedInDoCoMoCase = true;
        }
        final String oneEntryInternal = this.createOneEntryInternal(this.mCursor.getString(this.mIdColumn), method);
        if (!this.mCursor.moveToNext()) {
            Log.e("VCardComposer", "Cursor#moveToNext() returned false");
        }
        return oneEntryInternal;
    }
    
    @Override
    protected void finalize() throws Throwable {
        try {
            if (!this.mTerminateCalled) {
                Log.e("VCardComposer", "finalized() is called before terminate() being called");
            }
        }
        finally {
            super.finalize();
        }
    }
    
    public int getCount() {
        int count;
        if (this.mCursor == null) {
            Log.w("VCardComposer", "This object is not ready yet.");
            count = 0;
        }
        else {
            count = this.mCursor.getCount();
        }
        return count;
    }
    
    public String getErrorReason() {
        return this.mErrorReason;
    }
    
    public boolean init() {
        return this.init(null, null);
    }
    
    public boolean init(final Cursor mCursor) {
        boolean initInterLastPart = false;
        if (this.initInterFirstPart(null)) {
            this.mCursorSuppliedFromOutside = true;
            this.mCursor = mCursor;
            if (this.initInterMainPart()) {
                initInterLastPart = this.initInterLastPart();
            }
        }
        return initInterLastPart;
    }
    
    public boolean init(final Uri uri, final String s, final String[] array, final String s2) {
        return this.init(uri, VCardComposer.sContactsProjection, s, array, s2, null);
    }
    
    public boolean init(final Uri uri, final String s, final String[] array, final String s2, final Uri uri2) {
        return this.init(uri, VCardComposer.sContactsProjection, s, array, s2, uri2);
    }
    
    public boolean init(final Uri uri, final String[] array, final String s, final String[] array2, final String s2, final Uri uri2) {
        final boolean b = false;
        boolean initInterLastPart;
        if (!"com.android.contacts".equals(uri.getAuthority())) {
            this.mErrorReason = "The Uri vCard composer received is not supported by the composer.";
            initInterLastPart = b;
        }
        else {
            initInterLastPart = b;
            if (this.initInterFirstPart(uri2)) {
                initInterLastPart = b;
                if (this.initInterCursorCreationPart(uri, array, s, array2, s2)) {
                    initInterLastPart = b;
                    if (this.initInterMainPart()) {
                        initInterLastPart = this.initInterLastPart();
                    }
                }
            }
        }
        return initInterLastPart;
    }
    
    public boolean init(final String s, final String[] array) {
        return this.init(ContactsContract$Contacts.CONTENT_URI, VCardComposer.sContactsProjection, s, array, null, null);
    }
    
    @Deprecated
    public boolean initWithRawContactsEntityUri(final Uri uri) {
        return this.init(ContactsContract$Contacts.CONTENT_URI, VCardComposer.sContactsProjection, null, null, null, uri);
    }
    
    public boolean isAfterLast() {
        boolean afterLast;
        if (this.mCursor == null) {
            Log.w("VCardComposer", "This object is not ready yet.");
            afterLast = false;
        }
        else {
            afterLast = this.mCursor.isAfterLast();
        }
        return afterLast;
    }
    
    public void setPhoneNumberTranslationCallback(final VCardPhoneNumberTranslationCallback mPhoneTranslationCallback) {
        this.mPhoneTranslationCallback = mPhoneTranslationCallback;
    }
    
    public void terminate() {
        this.closeCursorIfAppropriate();
        this.mTerminateCalled = true;
    }
}
