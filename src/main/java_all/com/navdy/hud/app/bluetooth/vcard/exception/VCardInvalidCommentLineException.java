package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardInvalidCommentLineException extends VCardInvalidLineException
{
    public VCardInvalidCommentLineException() {
    }
    
    public VCardInvalidCommentLineException(final String s) {
        super(s);
    }
}
