package com.navdy.hud.app.bluetooth.vcard;

import java.util.Set;

class VCardParserImpl_V40 extends VCardParserImpl_V30
{
    public VCardParserImpl_V40() {
    }
    
    public VCardParserImpl_V40(final int n) {
        super(n);
    }
    
    public static String unescapeCharacter(final char c) {
        String value;
        if (c == 'n' || c == 'N') {
            value = "\n";
        }
        else {
            value = String.valueOf(c);
        }
        return value;
    }
    
    public static String unescapeText(final String s) {
        final StringBuilder sb = new StringBuilder();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '\\' && i < length - 1) {
                ++i;
                final char char2 = s.charAt(i);
                if (char2 == 'n' || char2 == 'N') {
                    sb.append("\n");
                }
                else {
                    sb.append(char2);
                }
            }
            else {
                sb.append(char1);
            }
        }
        return sb.toString();
    }
    
    @Override
    protected Set<String> getKnownPropertyNameSet() {
        return VCardParser_V40.sKnownPropertyNameSet;
    }
    
    @Override
    protected int getVersion() {
        return 2;
    }
    
    @Override
    protected String getVersionString() {
        return "4.0";
    }
    
    @Override
    protected String maybeUnescapeText(final String s) {
        return unescapeText(s);
    }
}
