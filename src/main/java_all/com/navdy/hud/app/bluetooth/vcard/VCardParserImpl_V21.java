package com.navdy.hud.app.bluetooth.vcard;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.InputStream;
import com.navdy.hud.app.bluetooth.vcard.exception.VCardAgentNotSupportedException;
import com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidLineException;
import com.navdy.hud.app.bluetooth.vcard.exception.VCardInvalidCommentLineException;
import android.util.Log;
import com.navdy.hud.app.bluetooth.vcard.exception.VCardVersionException;
import java.util.Iterator;
import com.navdy.hud.app.bluetooth.vcard.exception.VCardException;
import java.io.IOException;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;
import java.util.List;

class VCardParserImpl_V21
{
    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final String DEFAULT_ENCODING = "8BIT";
    private static final String LOG_TAG = "vCard";
    private static final int STATE_GROUP_OR_PROPERTY_NAME = 0;
    private static final int STATE_PARAMS = 1;
    private static final int STATE_PARAMS_IN_DQUOTE = 2;
    private boolean mCanceled;
    protected String mCurrentCharset;
    protected String mCurrentEncoding;
    protected final String mIntermediateCharset;
    private final List<VCardInterpreter> mInterpreterList;
    protected CustomBufferedReader mReader;
    protected final Set<String> mUnknownTypeSet;
    protected final Set<String> mUnknownValueSet;
    
    public VCardParserImpl_V21() {
        this(VCardConfig.VCARD_TYPE_DEFAULT);
    }
    
    public VCardParserImpl_V21(final int n) {
        this.mInterpreterList = new ArrayList<VCardInterpreter>();
        this.mUnknownTypeSet = new HashSet<String>();
        this.mUnknownValueSet = new HashSet<String>();
        this.mIntermediateCharset = "ISO-8859-1";
    }
    
    private String getPotentialMultiline(String peekLine) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append(peekLine);
        while (true) {
            peekLine = this.peekLine();
            if (peekLine == null || peekLine.length() == 0 || this.getPropertyNameUpperCase(peekLine) != null) {
                break;
            }
            this.getLine();
            sb.append(" ").append(peekLine);
        }
        return sb.toString();
    }
    
    private String getPropertyNameUpperCase(String upperCase) {
        int n = upperCase.indexOf(":");
        if (n > -1) {
            final int index = upperCase.indexOf(";");
            if (n == -1) {
                n = index;
            }
            else if (index != -1) {
                n = Math.min(n, index);
            }
            upperCase = upperCase.substring(0, n).toUpperCase();
        }
        else {
            upperCase = null;
        }
        return upperCase;
    }
    
    private String getQuotedPrintablePart(String line) throws IOException, VCardException {
        String string = line;
        if (line.trim().endsWith("=")) {
            final int n = line.length() - 1;
            while (line.charAt(n) != '=') {}
            final StringBuilder sb = new StringBuilder();
            sb.append(line.substring(0, n + 1));
            sb.append("\r\n");
            while (true) {
                line = this.getLine();
                if (line == null) {
                    throw new VCardException("File ended during parsing a Quoted-Printable String");
                }
                if (!line.trim().endsWith("=")) {
                    sb.append(line);
                    string = sb.toString();
                    break;
                }
                final int n2 = line.length() - 1;
                while (line.charAt(n2) != '=') {}
                sb.append(line.substring(0, n2 + 1));
                sb.append("\r\n");
            }
        }
        return string;
    }
    
    private void handleAdrOrgN(final VCardProperty vCardProperty, String quotedPrintablePart, final String s, final String s2) throws VCardException, IOException {
        final ArrayList<String> values = new ArrayList<String>();
        if (this.mCurrentEncoding.equals("QUOTED-PRINTABLE")) {
            quotedPrintablePart = this.getQuotedPrintablePart(quotedPrintablePart);
            vCardProperty.setRawValue(quotedPrintablePart);
            final Iterator<String> iterator = VCardUtils.constructListFromValue(quotedPrintablePart, this.getVersion()).iterator();
            while (iterator.hasNext()) {
                values.add(VCardUtils.parseQuotedPrintable(iterator.next(), false, s, s2));
            }
        }
        else {
            final Iterator<String> iterator2 = VCardUtils.constructListFromValue(this.getPotentialMultiline(quotedPrintablePart), this.getVersion()).iterator();
            while (iterator2.hasNext()) {
                values.add(VCardUtils.convertStringCharset(iterator2.next(), s, s2));
            }
        }
        vCardProperty.setValues(values);
        final Iterator<VCardInterpreter> iterator3 = this.mInterpreterList.iterator();
        while (iterator3.hasNext()) {
            iterator3.next().onPropertyCreated(vCardProperty);
        }
    }
    
    private void handleNest() throws IOException, VCardException {
        final Iterator<VCardInterpreter> iterator = this.mInterpreterList.iterator();
        while (iterator.hasNext()) {
            iterator.next().onEntryStarted();
        }
        this.parseItems();
        final Iterator<VCardInterpreter> iterator2 = this.mInterpreterList.iterator();
        while (iterator2.hasNext()) {
            iterator2.next().onEntryEnded();
        }
    }
    
    private boolean isAsciiLetter(final char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }
    
    private void parseItemInter(final VCardProperty vCardProperty, final String s) throws IOException, VCardException {
        final String rawValue = vCardProperty.getRawValue();
        if (s.equals("AGENT")) {
            this.handleAgent(vCardProperty);
        }
        else {
            if (!this.isValidPropertyName(s)) {
                throw new VCardException("Unknown property name: \"" + s + "\"");
            }
            if (s.equals("VERSION") && !rawValue.equals(this.getVersionString())) {
                throw new VCardVersionException("Incompatible version: " + rawValue + " != " + this.getVersionString());
            }
            this.handlePropertyValue(vCardProperty, s);
        }
    }
    
    private boolean parseOneVCard() throws IOException, VCardException {
        this.mCurrentEncoding = "8BIT";
        this.mCurrentCharset = "UTF-8";
        boolean b;
        if (!this.readBeginVCard(false)) {
            b = false;
        }
        else {
            final Iterator<VCardInterpreter> iterator = this.mInterpreterList.iterator();
            while (iterator.hasNext()) {
                iterator.next().onEntryStarted();
            }
            this.parseItems();
            final Iterator<VCardInterpreter> iterator2 = this.mInterpreterList.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().onEntryEnded();
            }
            b = true;
        }
        return b;
    }
    
    static String unescapeCharacter(final char c) {
        String value;
        if (c == '\\' || c == ';' || c == ':' || c == ',') {
            value = String.valueOf(c);
        }
        else {
            value = null;
        }
        return value;
    }
    
    public void addInterpreter(final VCardInterpreter vCardInterpreter) {
        this.mInterpreterList.add(vCardInterpreter);
    }
    
    public final void cancel() {
        synchronized (this) {
            Log.i("vCard", "ParserImpl received cancel operation.");
            this.mCanceled = true;
        }
    }
    
    protected VCardProperty constructPropertyData(String s) throws VCardException {
        final VCardProperty vCardProperty = new VCardProperty();
        final int length = s.length();
        if (length > 0 && s.charAt(0) == '#') {
            throw new VCardInvalidCommentLineException();
        }
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        for (int i = 0; i < length; ++i, n2 = n3) {
            final char char1 = s.charAt(i);
            switch (n) {
                default:
                    n3 = n2;
                    break;
                case 0:
                    if (char1 == ':') {
                        vCardProperty.setName(s.substring(n2, i));
                        if (i < length - 1) {
                            s = s.substring(i + 1);
                        }
                        else {
                            s = "";
                        }
                        vCardProperty.setRawValue(s);
                        return vCardProperty;
                    }
                    if (char1 == '.') {
                        final String substring = s.substring(n2, i);
                        if (substring.length() == 0) {
                            Log.w("vCard", "Empty group found. Ignoring.");
                        }
                        else {
                            vCardProperty.addGroup(substring);
                        }
                        n3 = i + 1;
                        break;
                    }
                    n3 = n2;
                    if (char1 == ';') {
                        vCardProperty.setName(s.substring(n2, i));
                        n3 = i + 1;
                        n = 1;
                        break;
                    }
                    break;
                case 1:
                    if (char1 == '\"') {
                        if ("2.1".equalsIgnoreCase(this.getVersionString())) {
                            Log.w("vCard", "Double-quoted params found in vCard 2.1. Silently allow it");
                        }
                        n = 2;
                        n3 = n2;
                        break;
                    }
                    if (char1 == ';') {
                        this.handleParams(vCardProperty, s.substring(n2, i));
                        n3 = i + 1;
                        break;
                    }
                    n3 = n2;
                    if (char1 == ':') {
                        this.handleParams(vCardProperty, s.substring(n2, i));
                        if (i < length - 1) {
                            s = s.substring(i + 1);
                        }
                        else {
                            s = "";
                        }
                        vCardProperty.setRawValue(s);
                        return vCardProperty;
                    }
                    break;
                case 2:
                    n3 = n2;
                    if (char1 == '\"') {
                        if ("2.1".equalsIgnoreCase(this.getVersionString())) {
                            Log.w("vCard", "Double-quoted params found in vCard 2.1. Silently allow it");
                        }
                        n = 1;
                        n3 = n2;
                        break;
                    }
                    break;
            }
        }
        throw new VCardInvalidLineException("Invalid line: \"" + s + "\"");
    }
    
    protected Set<String> getAvailableEncodingSet() {
        return VCardParser_V21.sAvailableEncoding;
    }
    
    protected String getBase64(String propertyNameUpperCase) throws IOException, VCardException {
        final StringBuilder sb = new StringBuilder();
        sb.append(propertyNameUpperCase);
        while (true) {
            final String peekLine = this.peekLine();
            if (peekLine == null) {
                throw new VCardException("File ended during parsing BASE64 binary");
            }
            propertyNameUpperCase = this.getPropertyNameUpperCase(peekLine);
            if (this.getKnownPropertyNameSet().contains(propertyNameUpperCase) || "X-ANDROID-CUSTOM".equals(propertyNameUpperCase)) {
                Log.w("vCard", "Found a next property during parsing a BASE64 string, which must not contain semi-colon or colon. Treat the line as next property.");
                Log.w("vCard", "Problematic line: " + peekLine.trim());
                break;
            }
            this.getLine();
            if (peekLine.length() == 0) {
                break;
            }
            sb.append(peekLine.trim());
        }
        return sb.toString();
    }
    
    protected String getCurrentCharset() {
        return this.mCurrentCharset;
    }
    
    protected String getDefaultCharset() {
        return "UTF-8";
    }
    
    protected String getDefaultEncoding() {
        return "8BIT";
    }
    
    protected Set<String> getKnownPropertyNameSet() {
        return VCardParser_V21.sKnownPropertyNameSet;
    }
    
    protected Set<String> getKnownTypeSet() {
        return VCardParser_V21.sKnownTypeSet;
    }
    
    protected Set<String> getKnownValueSet() {
        return VCardParser_V21.sKnownValueSet;
    }
    
    protected String getLine() throws IOException {
        return this.mReader.readLine();
    }
    
    protected String getNonEmptyLine() throws IOException, VCardException {
        String line;
        do {
            line = this.getLine();
            if (line == null) {
                throw new VCardException("Reached end of buffer.");
            }
        } while (line.trim().length() <= 0);
        return line;
    }
    
    protected int getVersion() {
        return 0;
    }
    
    protected String getVersionString() {
        return "2.1";
    }
    
    protected void handleAgent(final VCardProperty vCardProperty) throws VCardException {
        if (!vCardProperty.getRawValue().toUpperCase().contains("BEGIN:VCARD")) {
            final Iterator<VCardInterpreter> iterator = this.mInterpreterList.iterator();
            while (iterator.hasNext()) {
                iterator.next().onPropertyCreated(vCardProperty);
            }
            return;
        }
        throw new VCardAgentNotSupportedException("AGENT Property is not supported now.");
    }
    
    protected void handleAnyParam(final VCardProperty vCardProperty, final String s, final String s2) {
        vCardProperty.addParameter(s, s2);
    }
    
    protected void handleCharset(final VCardProperty vCardProperty, final String mCurrentCharset) {
        vCardProperty.addParameter("CHARSET", this.mCurrentCharset = mCurrentCharset);
    }
    
    protected void handleEncoding(final VCardProperty vCardProperty, final String s) throws VCardException {
        if (this.getAvailableEncodingSet().contains(s) || s.startsWith("X-")) {
            vCardProperty.addParameter("ENCODING", s);
            this.mCurrentEncoding = s.toUpperCase();
            return;
        }
        throw new VCardException("Unknown encoding \"" + s + "\"");
    }
    
    protected void handleLanguage(final VCardProperty vCardProperty, final String s) throws VCardException {
        final String[] split = s.split("-");
        if (split.length != 2) {
            throw new VCardException("Invalid Language: \"" + s + "\"");
        }
        final String s2 = split[0];
        for (int length = s2.length(), i = 0; i < length; ++i) {
            if (!this.isAsciiLetter(s2.charAt(i))) {
                throw new VCardException("Invalid Language: \"" + s + "\"");
            }
        }
        final String s3 = split[1];
        for (int length2 = s3.length(), j = 0; j < length2; ++j) {
            if (!this.isAsciiLetter(s3.charAt(j))) {
                throw new VCardException("Invalid Language: \"" + s + "\"");
            }
        }
        vCardProperty.addParameter("LANGUAGE", s);
    }
    
    protected void handleParamWithoutName(final VCardProperty vCardProperty, final String s) {
        this.handleType(vCardProperty, s);
    }
    
    protected void handleParams(final VCardProperty vCardProperty, String upperCase) throws VCardException {
        final String[] split = upperCase.split("=", 2);
        if (split.length == 2) {
            upperCase = split[0].trim().toUpperCase();
            final String trim = split[1].trim();
            if (upperCase.equals("TYPE")) {
                this.handleType(vCardProperty, trim);
            }
            else if (upperCase.equals("VALUE")) {
                this.handleValue(vCardProperty, trim);
            }
            else if (upperCase.equals("ENCODING")) {
                this.handleEncoding(vCardProperty, trim.toUpperCase());
            }
            else if (upperCase.equals("CHARSET")) {
                this.handleCharset(vCardProperty, trim);
            }
            else if (upperCase.equals("LANGUAGE")) {
                this.handleLanguage(vCardProperty, trim);
            }
            else {
                if (!upperCase.startsWith("X-")) {
                    throw new VCardException("Unknown type \"" + upperCase + "\"");
                }
                this.handleAnyParam(vCardProperty, upperCase, trim);
            }
        }
        else {
            this.handleParamWithoutName(vCardProperty, split[0]);
        }
    }
    
    protected void handlePropertyValue(final VCardProperty p0, final String p1) throws IOException, VCardException {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardProperty.getName:()Ljava/lang/String;
        //     4: invokevirtual   java/lang/String.toUpperCase:()Ljava/lang/String;
        //     7: astore_3       
        //     8: aload_1        
        //     9: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardProperty.getRawValue:()Ljava/lang/String;
        //    12: astore          4
        //    14: aload_1        
        //    15: ldc_w           "CHARSET"
        //    18: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardProperty.getParameters:(Ljava/lang/String;)Ljava/util/Collection;
        //    21: astore_2       
        //    22: aload_2        
        //    23: ifnull          97
        //    26: aload_2        
        //    27: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //    32: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    37: checkcast       Ljava/lang/String;
        //    40: astore_2       
        //    41: aload_2        
        //    42: astore          5
        //    44: aload_2        
        //    45: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    48: ifeq            55
        //    51: ldc             "UTF-8"
        //    53: astore          5
        //    55: aload_3        
        //    56: ldc_w           "ADR"
        //    59: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    62: ifne            85
        //    65: aload_3        
        //    66: ldc_w           "ORG"
        //    69: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    72: ifne            85
        //    75: aload_3        
        //    76: ldc_w           "N"
        //    79: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    82: ifeq            102
        //    85: aload_0        
        //    86: aload_1        
        //    87: aload           4
        //    89: ldc             "ISO-8859-1"
        //    91: aload           5
        //    93: invokespecial   com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.handleAdrOrgN:(Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //    96: return         
        //    97: aconst_null    
        //    98: astore_2       
        //    99: goto            41
        //   102: aload_0        
        //   103: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mCurrentEncoding:Ljava/lang/String;
        //   106: ldc             "QUOTED-PRINTABLE"
        //   108: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   111: ifne            142
        //   114: aload_3        
        //   115: ldc_w           "FN"
        //   118: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   121: ifeq            215
        //   124: aload_1        
        //   125: ldc_w           "ENCODING"
        //   128: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardProperty.getParameters:(Ljava/lang/String;)Ljava/util/Collection;
        //   131: ifnonnull       215
        //   134: aload           4
        //   136: invokestatic    com/navdy/hud/app/bluetooth/vcard/VCardUtils.appearsLikeAndroidVCardQuotedPrintable:(Ljava/lang/String;)Z
        //   139: ifeq            215
        //   142: aload_0        
        //   143: aload           4
        //   145: invokespecial   com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.getQuotedPrintablePart:(Ljava/lang/String;)Ljava/lang/String;
        //   148: astore_2       
        //   149: aload_2        
        //   150: iconst_0       
        //   151: ldc             "ISO-8859-1"
        //   153: aload           5
        //   155: invokestatic    com/navdy/hud/app/bluetooth/vcard/VCardUtils.parseQuotedPrintable:(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   158: astore          5
        //   160: aload_1        
        //   161: aload_2        
        //   162: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardProperty.setRawValue:(Ljava/lang/String;)V
        //   165: aload_1        
        //   166: iconst_1       
        //   167: anewarray       Ljava/lang/String;
        //   170: dup            
        //   171: iconst_0       
        //   172: aload           5
        //   174: aastore        
        //   175: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardProperty.setValues:([Ljava/lang/String;)V
        //   178: aload_0        
        //   179: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mInterpreterList:Ljava/util/List;
        //   182: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   187: astore_2       
        //   188: aload_2        
        //   189: invokeinterface java/util/Iterator.hasNext:()Z
        //   194: ifeq            96
        //   197: aload_2        
        //   198: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   203: checkcast       Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;
        //   206: aload_1        
        //   207: invokeinterface com/navdy/hud/app/bluetooth/vcard/VCardInterpreter.onPropertyCreated:(Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;)V
        //   212: goto            188
        //   215: aload_0        
        //   216: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mCurrentEncoding:Ljava/lang/String;
        //   219: ldc_w           "BASE64"
        //   222: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   225: ifne            241
        //   228: aload_0        
        //   229: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mCurrentEncoding:Ljava/lang/String;
        //   232: ldc_w           "B"
        //   235: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   238: ifeq            378
        //   241: aload_0        
        //   242: aload           4
        //   244: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.getBase64:(Ljava/lang/String;)Ljava/lang/String;
        //   247: astore_2       
        //   248: aload_1        
        //   249: aload_2        
        //   250: iconst_0       
        //   251: invokestatic    android/util/Base64.decode:(Ljava/lang/String;I)[B
        //   254: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardProperty.setByteValue:([B)V
        //   257: aload_0        
        //   258: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mInterpreterList:Ljava/util/List;
        //   261: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   266: astore_2       
        //   267: aload_2        
        //   268: invokeinterface java/util/Iterator.hasNext:()Z
        //   273: ifeq            96
        //   276: aload_2        
        //   277: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   282: checkcast       Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;
        //   285: aload_1        
        //   286: invokeinterface com/navdy/hud/app/bluetooth/vcard/VCardInterpreter.onPropertyCreated:(Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;)V
        //   291: goto            267
        //   294: astore_2       
        //   295: ldc             "vCard"
        //   297: ldc_w           "OutOfMemoryError happened during parsing BASE64 data!"
        //   300: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   303: pop            
        //   304: aload_0        
        //   305: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mInterpreterList:Ljava/util/List;
        //   308: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   313: astore_2       
        //   314: aload_2        
        //   315: invokeinterface java/util/Iterator.hasNext:()Z
        //   320: ifeq            96
        //   323: aload_2        
        //   324: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   329: checkcast       Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;
        //   332: aload_1        
        //   333: invokeinterface com/navdy/hud/app/bluetooth/vcard/VCardInterpreter.onPropertyCreated:(Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;)V
        //   338: goto            314
        //   341: astore_2       
        //   342: new             Lcom/navdy/hud/app/bluetooth/vcard/exception/VCardException;
        //   345: astore_2       
        //   346: new             Ljava/lang/StringBuilder;
        //   349: astore          5
        //   351: aload           5
        //   353: invokespecial   java/lang/StringBuilder.<init>:()V
        //   356: aload_2        
        //   357: aload           5
        //   359: ldc_w           "Decode error on base64 photo: "
        //   362: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   365: aload           4
        //   367: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   370: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   373: invokespecial   com/navdy/hud/app/bluetooth/vcard/exception/VCardException.<init>:(Ljava/lang/String;)V
        //   376: aload_2        
        //   377: athrow         
        //   378: aload_0        
        //   379: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mCurrentEncoding:Ljava/lang/String;
        //   382: ldc_w           "7BIT"
        //   385: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   388: ifne            446
        //   391: aload_0        
        //   392: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mCurrentEncoding:Ljava/lang/String;
        //   395: ldc             "8BIT"
        //   397: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   400: ifne            446
        //   403: aload_0        
        //   404: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mCurrentEncoding:Ljava/lang/String;
        //   407: ldc_w           "X-"
        //   410: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   413: ifne            446
        //   416: ldc             "vCard"
        //   418: ldc_w           "The encoding \"%s\" is unsupported by vCard %s"
        //   421: iconst_2       
        //   422: anewarray       Ljava/lang/Object;
        //   425: dup            
        //   426: iconst_0       
        //   427: aload_0        
        //   428: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mCurrentEncoding:Ljava/lang/String;
        //   431: aastore        
        //   432: dup            
        //   433: iconst_1       
        //   434: aload_0        
        //   435: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.getVersionString:()Ljava/lang/String;
        //   438: aastore        
        //   439: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   442: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   445: pop            
        //   446: aload           4
        //   448: astore_3       
        //   449: aload_0        
        //   450: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.getVersion:()I
        //   453: ifne            551
        //   456: aconst_null    
        //   457: astore_2       
        //   458: aload_0        
        //   459: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.peekLine:()Ljava/lang/String;
        //   462: astore          6
        //   464: aload           6
        //   466: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   469: ifne            539
        //   472: aload           6
        //   474: iconst_0       
        //   475: invokevirtual   java/lang/String.charAt:(I)C
        //   478: bipush          32
        //   480: if_icmpne       539
        //   483: ldc_w           "END:VCARD"
        //   486: aload           6
        //   488: invokevirtual   java/lang/String.toUpperCase:()Ljava/lang/String;
        //   491: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   494: ifne            539
        //   497: aload_0        
        //   498: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.getLine:()Ljava/lang/String;
        //   501: pop            
        //   502: aload_2        
        //   503: astore_3       
        //   504: aload_2        
        //   505: ifnonnull       523
        //   508: new             Ljava/lang/StringBuilder;
        //   511: dup            
        //   512: invokespecial   java/lang/StringBuilder.<init>:()V
        //   515: astore_3       
        //   516: aload_3        
        //   517: aload           4
        //   519: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   522: pop            
        //   523: aload_3        
        //   524: aload           6
        //   526: iconst_1       
        //   527: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   530: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   533: pop            
        //   534: aload_3        
        //   535: astore_2       
        //   536: goto            458
        //   539: aload           4
        //   541: astore_3       
        //   542: aload_2        
        //   543: ifnull          551
        //   546: aload_2        
        //   547: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   550: astore_3       
        //   551: new             Ljava/util/ArrayList;
        //   554: dup            
        //   555: invokespecial   java/util/ArrayList.<init>:()V
        //   558: astore_2       
        //   559: aload_2        
        //   560: aload_0        
        //   561: aload_3        
        //   562: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.maybeUnescapeText:(Ljava/lang/String;)Ljava/lang/String;
        //   565: ldc             "ISO-8859-1"
        //   567: aload           5
        //   569: invokestatic    com/navdy/hud/app/bluetooth/vcard/VCardUtils.convertStringCharset:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   572: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   575: pop            
        //   576: aload_1        
        //   577: aload_2        
        //   578: invokevirtual   com/navdy/hud/app/bluetooth/vcard/VCardProperty.setValues:(Ljava/util/List;)V
        //   581: aload_0        
        //   582: getfield        com/navdy/hud/app/bluetooth/vcard/VCardParserImpl_V21.mInterpreterList:Ljava/util/List;
        //   585: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   590: astore_2       
        //   591: aload_2        
        //   592: invokeinterface java/util/Iterator.hasNext:()Z
        //   597: ifeq            96
        //   600: aload_2        
        //   601: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   606: checkcast       Lcom/navdy/hud/app/bluetooth/vcard/VCardInterpreter;
        //   609: aload_1        
        //   610: invokeinterface com/navdy/hud/app/bluetooth/vcard/VCardInterpreter.onPropertyCreated:(Lcom/navdy/hud/app/bluetooth/vcard/VCardProperty;)V
        //   615: goto            591
        //    Exceptions:
        //  throws java.io.IOException
        //  throws com.navdy.hud.app.bluetooth.vcard.exception.VCardException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  241    248    294    341    Ljava/lang/OutOfMemoryError;
        //  248    257    341    378    Ljava/lang/IllegalArgumentException;
        //  248    257    294    341    Ljava/lang/OutOfMemoryError;
        //  257    267    294    341    Ljava/lang/OutOfMemoryError;
        //  267    291    294    341    Ljava/lang/OutOfMemoryError;
        //  342    378    294    341    Ljava/lang/OutOfMemoryError;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0267:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected void handleType(final VCardProperty vCardProperty, final String s) {
        if (!this.getKnownTypeSet().contains(s.toUpperCase()) && !s.startsWith("X-") && !this.mUnknownTypeSet.contains(s)) {
            this.mUnknownTypeSet.add(s);
            Log.w("vCard", String.format("TYPE unsupported by %s: ", this.getVersion(), s));
        }
        vCardProperty.addParameter("TYPE", s);
    }
    
    protected void handleValue(final VCardProperty vCardProperty, final String s) {
        if (!this.getKnownValueSet().contains(s.toUpperCase()) && !s.startsWith("X-") && !this.mUnknownValueSet.contains(s)) {
            this.mUnknownValueSet.add(s);
            Log.w("vCard", String.format("The value unsupported by TYPE of %s: ", this.getVersion(), s));
        }
        vCardProperty.addParameter("VALUE", s);
    }
    
    protected boolean isValidPropertyName(final String s) {
        if (!this.getKnownPropertyNameSet().contains(s.toUpperCase()) && !s.startsWith("X-") && !this.mUnknownTypeSet.contains(s)) {
            this.mUnknownTypeSet.add(s);
            Log.w("vCard", "Property name unsupported by vCard 2.1: " + s);
        }
        return true;
    }
    
    protected String maybeUnescapeCharacter(final char c) {
        return unescapeCharacter(c);
    }
    
    protected String maybeUnescapeText(final String s) {
        return s;
    }
    
    public void parse(final InputStream inputStream) throws IOException, VCardException {
        if (inputStream == null) {
            throw new NullPointerException("InputStream must not be null.");
        }
        this.mReader = new CustomBufferedReader(new InputStreamReader(inputStream, this.mIntermediateCharset));
        System.currentTimeMillis();
        final Iterator<VCardInterpreter> iterator = this.mInterpreterList.iterator();
        while (iterator.hasNext()) {
            iterator.next().onVCardStarted();
        }
        while (true) {
            synchronized (this) {
                if (this.mCanceled) {
                    Log.i("vCard", "Cancel request has come. exitting parse operation.");
                }
                // monitorexit(this)
                else if (this.parseOneVCard()) {
                    continue;
                }
                final Iterator<VCardInterpreter> iterator2 = this.mInterpreterList.iterator();
                while (iterator2.hasNext()) {
                    iterator2.next().onVCardEnded();
                }
            }
            break;
        }
    }
    
    protected boolean parseItem() throws IOException, VCardException {
        this.mCurrentEncoding = "8BIT";
        final VCardProperty constructPropertyData = this.constructPropertyData(this.getNonEmptyLine());
        final String upperCase = constructPropertyData.getName().toUpperCase();
        final String rawValue = constructPropertyData.getRawValue();
        if (upperCase.equals("BEGIN")) {
            if (!rawValue.equalsIgnoreCase("VCARD")) {
                throw new VCardException("Unknown BEGIN type: " + rawValue);
            }
            this.handleNest();
        }
        else if (upperCase.equals("END")) {
            if (rawValue.equalsIgnoreCase("VCARD")) {
                return true;
            }
            throw new VCardException("Unknown END type: " + rawValue);
        }
        else {
            this.parseItemInter(constructPropertyData, upperCase);
        }
        return false;
    }
    
    protected void parseItems() throws IOException, VCardException {
        boolean item = false;
        while (true) {
            try {
                boolean b = this.parseItem();
                while (!b) {
                    final VCardParserImpl_V21 vCardParserImpl_V21 = this;
                    item = vCardParserImpl_V21.parseItem();
                    final boolean b2 = b = item;
                }
                return;
            }
            catch (VCardInvalidCommentLineException ex) {
                Log.e("vCard", "Invalid line which looks like some comment was found. Ignored.");
                final boolean b = item;
                continue;
            }
            continue;
            try {
                final VCardParserImpl_V21 vCardParserImpl_V21 = this;
                final boolean b;
                item = (b = vCardParserImpl_V21.parseItem());
                continue;
            }
            catch (VCardInvalidCommentLineException ex2) {
                Log.e("vCard", "Invalid line which looks like some comment was found. Ignored.");
                continue;
            }
            continue;
        }
    }
    
    public void parseOne(final InputStream inputStream) throws IOException, VCardException {
        if (inputStream == null) {
            throw new NullPointerException("InputStream must not be null.");
        }
        this.mReader = new CustomBufferedReader(new InputStreamReader(inputStream, this.mIntermediateCharset));
        System.currentTimeMillis();
        final Iterator<VCardInterpreter> iterator = this.mInterpreterList.iterator();
        while (iterator.hasNext()) {
            iterator.next().onVCardStarted();
        }
        this.parseOneVCard();
        final Iterator<VCardInterpreter> iterator2 = this.mInterpreterList.iterator();
        while (iterator2.hasNext()) {
            iterator2.next().onVCardEnded();
        }
    }
    
    protected String peekLine() throws IOException {
        return this.mReader.peekLine();
    }
    
    protected boolean readBeginVCard(final boolean b) throws IOException, VCardException {
        final boolean b2 = false;
        boolean b3;
        while (true) {
            final String line = this.getLine();
            if (line == null) {
                b3 = b2;
                break;
            }
            if (line.trim().length() <= 0) {
                continue;
            }
            final String[] split = line.split(":", 2);
            if (split.length == 2 && split[0].trim().equalsIgnoreCase("BEGIN") && split[1].trim().equalsIgnoreCase("VCARD")) {
                b3 = true;
                break;
            }
            if (!b) {
                throw new VCardException("Expected String \"BEGIN:VCARD\" did not come (Instead, \"" + line + "\" came)");
            }
            if (!b) {
                throw new VCardException("Reached where must not be reached.");
            }
        }
        return b3;
    }
    
    protected static final class CustomBufferedReader extends BufferedReader
    {
        private String mNextLine;
        private boolean mNextLineIsValid;
        private long mTime;
        
        public CustomBufferedReader(final Reader reader) {
            super(reader);
        }
        
        public long getTotalmillisecond() {
            return this.mTime;
        }
        
        public String peekLine() throws IOException {
            if (!this.mNextLineIsValid) {
                final long currentTimeMillis = System.currentTimeMillis();
                final String line = super.readLine();
                this.mTime += System.currentTimeMillis() - currentTimeMillis;
                this.mNextLine = line;
                this.mNextLineIsValid = true;
            }
            return this.mNextLine;
        }
        
        @Override
        public String readLine() throws IOException {
            String s;
            if (this.mNextLineIsValid) {
                s = this.mNextLine;
                this.mNextLine = null;
                this.mNextLineIsValid = false;
            }
            else {
                final long currentTimeMillis = System.currentTimeMillis();
                s = super.readLine();
                this.mTime += System.currentTimeMillis() - currentTimeMillis;
            }
            return s;
        }
    }
}
