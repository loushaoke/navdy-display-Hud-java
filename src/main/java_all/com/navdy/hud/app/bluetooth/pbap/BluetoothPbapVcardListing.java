package com.navdy.hud.app.bluetooth.pbap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import android.util.Log;
import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

class BluetoothPbapVcardListing
{
    private static final String TAG = "BTPbapVcardList";
    ArrayList<BluetoothPbapCard> mCards;
    
    public BluetoothPbapVcardListing(final InputStream inputStream) throws IOException {
        this.mCards = new ArrayList<BluetoothPbapCard>();
        this.parse(inputStream);
    }
    
    private void parse(final InputStream inputStream) throws IOException {
        final XmlPullParser pullParser = Xml.newPullParser();
        try {
            pullParser.setInput(inputStream, "UTF-8");
            for (int i = pullParser.getEventType(); i != 1; i = pullParser.next()) {
                if (i == 2 && pullParser.getName().equals("card")) {
                    this.mCards.add(new BluetoothPbapCard(pullParser.getAttributeValue((String)null, "handle"), pullParser.getAttributeValue((String)null, "name")));
                }
            }
        }
        catch (XmlPullParserException ex) {
            Log.e("BTPbapVcardList", "XML parser error when parsing XML", (Throwable)ex);
        }
    }
    
    public ArrayList<BluetoothPbapCard> getList() {
        return this.mCards;
    }
}
