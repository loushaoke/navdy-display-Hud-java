package com.navdy.hud.app.bluetooth.vcard;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Set;
import com.navdy.hud.app.bluetooth.vcard.exception.VCardException;
import java.io.IOException;
import android.util.Log;

class VCardParserImpl_V30 extends VCardParserImpl_V21
{
    private static final String LOG_TAG = "vCard";
    private boolean mEmittedAgentWarning;
    private String mPreviousLine;
    
    public VCardParserImpl_V30() {
        this.mEmittedAgentWarning = false;
    }
    
    public VCardParserImpl_V30(final int n) {
        super(n);
        this.mEmittedAgentWarning = false;
    }
    
    private void splitAndPutParam(final VCardProperty vCardProperty, final String s, final String s2) {
        StringBuilder sb = null;
        int n = 0;
        for (int length = s2.length(), i = 0; i < length; ++i) {
            final char char1 = s2.charAt(i);
            if (char1 == '\"') {
                if (n != 0) {
                    vCardProperty.addParameter(s, this.encodeParamValue(sb.toString()));
                    sb = null;
                    n = 0;
                }
                else {
                    if (sb != null) {
                        if (sb.length() > 0) {
                            Log.w("vCard", "Unexpected Dquote inside property.");
                        }
                        else {
                            vCardProperty.addParameter(s, this.encodeParamValue(sb.toString()));
                        }
                    }
                    n = 1;
                }
            }
            else if (char1 == ',' && n == 0) {
                if (sb == null) {
                    Log.w("vCard", "Comma is used before actual string comes. (" + s2 + ")");
                }
                else {
                    vCardProperty.addParameter(s, this.encodeParamValue(sb.toString()));
                    sb = null;
                }
            }
            else {
                StringBuilder sb2;
                if ((sb2 = sb) == null) {
                    sb2 = new StringBuilder();
                }
                sb2.append(char1);
                sb = sb2;
            }
        }
        if (n != 0) {
            Log.d("vCard", "Dangling Dquote.");
        }
        if (sb != null) {
            if (sb.length() == 0) {
                Log.w("vCard", "Unintended behavior. We must not see empty StringBuilder at the end of parameter value parsing.");
            }
            else {
                vCardProperty.addParameter(s, this.encodeParamValue(sb.toString()));
            }
        }
    }
    
    public static String unescapeCharacter(final char c) {
        String value;
        if (c == 'n' || c == 'N') {
            value = "\n";
        }
        else {
            value = String.valueOf(c);
        }
        return value;
    }
    
    public static String unescapeText(final String s) {
        final StringBuilder sb = new StringBuilder();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '\\' && i < length - 1) {
                ++i;
                final char char2 = s.charAt(i);
                if (char2 == 'n' || char2 == 'N') {
                    sb.append("\n");
                }
                else {
                    sb.append(char2);
                }
            }
            else {
                sb.append(char1);
            }
        }
        return sb.toString();
    }
    
    protected String encodeParamValue(final String s) {
        return VCardUtils.convertStringCharset(s, "ISO-8859-1", "UTF-8");
    }
    
    @Override
    protected String getBase64(final String s) throws IOException, VCardException {
        return s;
    }
    
    @Override
    protected Set<String> getKnownPropertyNameSet() {
        return VCardParser_V30.sKnownPropertyNameSet;
    }
    
    @Override
    protected String getLine() throws IOException {
        String s;
        if (this.mPreviousLine != null) {
            s = this.mPreviousLine;
            this.mPreviousLine = null;
        }
        else {
            s = this.mReader.readLine();
        }
        return s;
    }
    
    @Override
    protected String getNonEmptyLine() throws IOException, VCardException {
        StringBuilder sb = null;
        String line;
        while (true) {
            line = this.mReader.readLine();
            if (line == null) {
                break;
            }
            if (line.length() == 0) {
                continue;
            }
            if (line.charAt(0) == ' ' || line.charAt(0) == '\t') {
                StringBuilder sb2;
                if ((sb2 = sb) == null) {
                    sb2 = new StringBuilder();
                }
                if (this.mPreviousLine != null) {
                    sb2.append(this.mPreviousLine);
                    this.mPreviousLine = null;
                }
                sb2.append(line.substring(1));
                sb = sb2;
            }
            else {
                if (sb != null || this.mPreviousLine != null) {
                    break;
                }
                this.mPreviousLine = line;
            }
        }
        final String s = null;
        String s2;
        if (sb != null) {
            s2 = sb.toString();
        }
        else {
            s2 = s;
            if (this.mPreviousLine != null) {
                s2 = this.mPreviousLine;
            }
        }
        this.mPreviousLine = line;
        if (s2 == null) {
            throw new VCardException("Reached end of buffer.");
        }
        return s2;
    }
    
    @Override
    protected int getVersion() {
        return 1;
    }
    
    @Override
    protected String getVersionString() {
        return "3.0";
    }
    
    @Override
    protected void handleAgent(final VCardProperty vCardProperty) {
        if (!this.mEmittedAgentWarning) {
            Log.w("vCard", "AGENT in vCard 3.0 is not supported yet. Ignore it");
            this.mEmittedAgentWarning = true;
        }
    }
    
    @Override
    protected void handleAnyParam(final VCardProperty vCardProperty, final String s, final String s2) {
        this.splitAndPutParam(vCardProperty, s, s2);
    }
    
    @Override
    protected void handleParamWithoutName(final VCardProperty vCardProperty, final String s) {
        this.handleType(vCardProperty, s);
    }
    
    @Override
    protected void handleParams(final VCardProperty vCardProperty, final String s) throws VCardException {
        try {
            super.handleParams(vCardProperty, s);
        }
        catch (VCardException ex) {
            final String[] split = s.split("=", 2);
            if (split.length == 2) {
                this.handleAnyParam(vCardProperty, split[0], split[1]);
                return;
            }
            throw new VCardException("Unknown params value: " + s);
        }
    }
    
    @Override
    protected void handleType(final VCardProperty vCardProperty, final String s) {
        this.splitAndPutParam(vCardProperty, "TYPE", s);
    }
    
    @Override
    protected String maybeUnescapeCharacter(final char c) {
        return unescapeCharacter(c);
    }
    
    @Override
    protected String maybeUnescapeText(final String s) {
        return unescapeText(s);
    }
    
    @Override
    protected boolean readBeginVCard(final boolean b) throws IOException, VCardException {
        return super.readBeginVCard(b);
    }
}
