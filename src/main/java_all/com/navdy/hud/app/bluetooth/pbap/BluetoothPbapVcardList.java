package com.navdy.hud.app.bluetooth.pbap;

import com.navdy.hud.app.bluetooth.vcard.VCardParser;
import com.navdy.hud.app.bluetooth.vcard.exception.VCardException;
import com.navdy.hud.app.bluetooth.vcard.VCardParser_V21;
import com.navdy.hud.app.bluetooth.vcard.VCardInterpreter;
import com.navdy.hud.app.bluetooth.vcard.VCardEntryHandler;
import com.navdy.hud.app.bluetooth.vcard.VCardEntryCounter;
import com.navdy.hud.app.bluetooth.vcard.VCardEntryConstructor;
import com.navdy.hud.app.bluetooth.vcard.VCardParser_V30;
import java.io.IOException;
import java.io.InputStream;
import com.navdy.hud.app.bluetooth.vcard.VCardEntry;
import java.util.ArrayList;

class BluetoothPbapVcardList
{
    private final ArrayList<VCardEntry> mCards;
    
    public BluetoothPbapVcardList(final InputStream inputStream, final byte b) throws IOException {
        this.mCards = new ArrayList<VCardEntry>();
        this.parse(inputStream, b);
    }
    
    private void parse(final InputStream inputStream, final byte b) throws IOException {
        Label_0062: {
            if (b != 1) {
                break Label_0062;
            }
            VCardParser vCardParser = new VCardParser_V30();
            while (true) {
                final VCardEntryConstructor vCardEntryConstructor = new VCardEntryConstructor();
                final VCardEntryCounter vCardEntryCounter = new VCardEntryCounter();
                vCardEntryConstructor.addEntryHandler(new CardEntryHandler());
                vCardParser.addInterpreter(vCardEntryConstructor);
                vCardParser.addInterpreter(vCardEntryCounter);
                try {
                    vCardParser.parse(inputStream);
                    return;
                    vCardParser = new VCardParser_V21();
                }
                catch (VCardException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    public int getCount() {
        return this.mCards.size();
    }
    
    public VCardEntry getFirst() {
        return this.mCards.get(0);
    }
    
    public ArrayList<VCardEntry> getList() {
        return this.mCards;
    }
    
    class CardEntryHandler implements VCardEntryHandler
    {
        @Override
        public void onEnd() {
        }
        
        @Override
        public void onEntryCreated(final VCardEntry vCardEntry) {
            BluetoothPbapVcardList.this.mCards.add(vCardEntry);
        }
        
        @Override
        public void onStart() {
        }
    }
}
