package com.navdy.hud.app.bluetooth.vcard;

import android.content.ContentProviderResult;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.util.Log;
import android.content.ContentProviderOperation;
import android.net.Uri;
import java.util.ArrayList;
import android.content.ContentResolver;

public class VCardEntryCommitter implements VCardEntryHandler
{
    public static String LOG_TAG;
    private final ContentResolver mContentResolver;
    private int mCounter;
    private final ArrayList<Uri> mCreatedUris;
    private ArrayList<ContentProviderOperation> mOperationList;
    private long mTimeToCommit;
    
    static {
        VCardEntryCommitter.LOG_TAG = "vCard";
    }
    
    public VCardEntryCommitter(final ContentResolver mContentResolver) {
        this.mCreatedUris = new ArrayList<Uri>();
        this.mContentResolver = mContentResolver;
    }
    
    private Uri pushIntoContentResolver(final ArrayList<ContentProviderOperation> list) {
        final Uri uri = null;
        Uri uri2;
        try {
            final ContentProviderResult[] applyBatch = this.mContentResolver.applyBatch("com.android.contacts", (ArrayList)list);
            uri2 = uri;
            if (applyBatch != null) {
                uri2 = uri;
                if (applyBatch.length != 0) {
                    if (applyBatch[0] == null) {
                        uri2 = uri;
                    }
                    else {
                        uri2 = applyBatch[0].uri;
                    }
                }
            }
            return uri2;
        }
        catch (RemoteException ex) {
            Log.e(VCardEntryCommitter.LOG_TAG, String.format("%s: %s", ex.toString(), ex.getMessage()));
            uri2 = uri;
            return uri2;
        }
        catch (OperationApplicationException ex2) {
            Log.e(VCardEntryCommitter.LOG_TAG, String.format("%s: %s", ex2.toString(), ex2.getMessage()));
            uri2 = uri;
            return uri2;
        }
        return uri2;
    }
    
    public ArrayList<Uri> getCreatedUris() {
        return this.mCreatedUris;
    }
    
    @Override
    public void onEnd() {
        if (this.mOperationList != null) {
            this.mCreatedUris.add(this.pushIntoContentResolver(this.mOperationList));
        }
        if (VCardConfig.showPerformanceLog()) {
            Log.d(VCardEntryCommitter.LOG_TAG, String.format("time to commit entries: %d ms", this.mTimeToCommit));
        }
    }
    
    @Override
    public void onEntryCreated(final VCardEntry vCardEntry) {
        final long currentTimeMillis = System.currentTimeMillis();
        this.mOperationList = vCardEntry.constructInsertOperations(this.mContentResolver, this.mOperationList);
        ++this.mCounter;
        if (this.mCounter >= 20) {
            this.mCreatedUris.add(this.pushIntoContentResolver(this.mOperationList));
            this.mCounter = 0;
            this.mOperationList = null;
        }
        this.mTimeToCommit += System.currentTimeMillis() - currentTimeMillis;
    }
    
    @Override
    public void onStart() {
    }
}
