package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardNestedException extends VCardNotSupportedException
{
    public VCardNestedException() {
    }
    
    public VCardNestedException(final String s) {
        super(s);
    }
}
