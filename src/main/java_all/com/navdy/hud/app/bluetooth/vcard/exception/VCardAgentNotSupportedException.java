package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardAgentNotSupportedException extends VCardNotSupportedException
{
    public VCardAgentNotSupportedException() {
    }
    
    public VCardAgentNotSupportedException(final String s) {
        super(s);
    }
}
