package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntryCounter implements VCardInterpreter
{
    private int mCount;
    
    public int getCount() {
        return this.mCount;
    }
    
    @Override
    public void onEntryEnded() {
        ++this.mCount;
    }
    
    @Override
    public void onEntryStarted() {
    }
    
    @Override
    public void onPropertyCreated(final VCardProperty vCardProperty) {
    }
    
    @Override
    public void onVCardEnded() {
    }
    
    @Override
    public void onVCardStarted() {
    }
}
