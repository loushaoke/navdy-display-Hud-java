package com.navdy.hud.app.bluetooth.vcard;

import com.navdy.hud.app.bluetooth.vcard.exception.VCardException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;

public class VCardParser_V40 extends VCardParser
{
    static final Set<String> sAcceptableEncoding;
    static final Set<String> sKnownPropertyNameSet;
    private final VCardParserImpl_V40 mVCardParserImpl;
    
    static {
        sKnownPropertyNameSet = Collections.<String>unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.<String>asList("BEGIN", "END", "VERSION", "SOURCE", "KIND", "FN", "N", "NICKNAME", "PHOTO", "BDAY", "ANNIVERSARY", "GENDER", "ADR", "TEL", "EMAIL", "IMPP", "LANG", "TZ", "GEO", "TITLE", "ROLE", "LOGO", "ORG", "MEMBER", "RELATED", "CATEGORIES", "NOTE", "PRODID", "REV", "SOUND", "UID", "CLIENTPIDMAP", "URL", "KEY", "FBURL", "CALENDRURI", "CALURI", "XML")));
        sAcceptableEncoding = Collections.<String>unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.<String>asList("8BIT", "B")));
    }
    
    public VCardParser_V40() {
        this.mVCardParserImpl = new VCardParserImpl_V40();
    }
    
    public VCardParser_V40(final int n) {
        this.mVCardParserImpl = new VCardParserImpl_V40(n);
    }
    
    @Override
    public void addInterpreter(final VCardInterpreter vCardInterpreter) {
        this.mVCardParserImpl.addInterpreter(vCardInterpreter);
    }
    
    @Override
    public void cancel() {
        this.mVCardParserImpl.cancel();
    }
    
    @Override
    public void parse(final InputStream inputStream) throws IOException, VCardException {
        this.mVCardParserImpl.parse(inputStream);
    }
    
    @Override
    public void parseOne(final InputStream inputStream) throws IOException, VCardException {
        this.mVCardParserImpl.parseOne(inputStream);
    }
}
