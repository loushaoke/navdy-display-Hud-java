package com.navdy.hud.app.common;

import android.os.SystemClock;
import com.navdy.hud.app.util.DeviceUtil;
import android.os.Looper;
import android.os.Handler;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.debug.BusLeakDetector;
import com.squareup.otto.Bus;

public class MainThreadBus extends Bus
{
    private BusLeakDetector busLeakDetector;
    private boolean isEngBuild;
    private final Logger logger;
    private final Handler mHandler;
    public int threshold;
    
    public MainThreadBus() {
        this.mHandler = new Handler(Looper.getMainLooper());
        this.logger = new Logger(MainThreadBus.class);
        this.isEngBuild = !DeviceUtil.isUserBuild();
        if (this.isEngBuild) {
            this.busLeakDetector = BusLeakDetector.getInstance();
        }
    }
    
    @Override
    public void post(final Object o) {
        if (o != null) {
            this.mHandler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    long elapsedRealtime = 0L;
                    if (MainThreadBus.this.threshold > 0) {
                        elapsedRealtime = SystemClock.elapsedRealtime();
                    }
                    Bus.this.post(o);
                    if (MainThreadBus.this.threshold > 0) {
                        final long n = SystemClock.elapsedRealtime() - elapsedRealtime;
                        if (n >= MainThreadBus.this.threshold) {
                            MainThreadBus.this.logger.v("[" + n + "] MainThreadBus-event [" + o + "]");
                        }
                    }
                }
            });
        }
    }
    
    @Override
    public void register(final Object o) {
        if (o != null) {
            this.mHandler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    if (MainThreadBus.this.isEngBuild) {
                        MainThreadBus.this.busLeakDetector.addReference(o.getClass());
                    }
                    Bus.this.register(o);
                }
            });
        }
    }
    
    @Override
    public void unregister(final Object o) {
        if (o != null) {
            this.mHandler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    if (MainThreadBus.this.isEngBuild) {
                        MainThreadBus.this.busLeakDetector.removeReference(o.getClass());
                    }
                    Bus.this.unregister(o);
                }
            });
        }
    }
}
