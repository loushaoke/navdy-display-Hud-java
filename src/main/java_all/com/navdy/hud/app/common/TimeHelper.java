package com.navdy.hud.app.common;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.TimeZone;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import java.util.Date;
import android.content.res.Resources;
import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class TimeHelper
{
    public static final UpdateClock CLOCK_CHANGED;
    public static final DateTimeAvailableEvent DATE_TIME_AVAILABLE_EVENT;
    private static final Logger sLogger;
    private String am;
    private String amCompact;
    private Bus bus;
    private Calendar calendar;
    private SimpleDateFormat currentTimeFormat;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dayFormat;
    private String pm;
    private String pmCompact;
    private SimpleDateFormat timeFormat12;
    private SimpleDateFormat timeFormat24;
    
    static {
        sLogger = new Logger(TimeHelper.class);
        CLOCK_CHANGED = new UpdateClock();
        DATE_TIME_AVAILABLE_EVENT = new DateTimeAvailableEvent();
    }
    
    public TimeHelper(final Context context, final Bus bus) {
        this.bus = bus;
        final Resources resources = context.getResources();
        this.pmCompact = resources.getString(R.string.pm_marker);
        this.pm = resources.getString(R.string.pm);
        this.amCompact = resources.getString(R.string.am_marker);
        this.am = resources.getString(R.string.am);
        this.updateLocale();
    }
    
    public String formatTime(final Date time, final StringBuilder sb) {
        synchronized (this) {
            final String format = this.currentTimeFormat.format(time);
            if (sb != null) {
                sb.setLength(0);
                if (this.currentTimeFormat == this.timeFormat12) {
                    this.calendar.setTime(time);
                    if (this.calendar.get(9) == 1) {
                        sb.append(this.pmCompact);
                    }
                }
            }
            return format;
        }
    }
    
    public String formatTime12Hour(final Date time, final StringBuilder sb, final boolean b) {
        while (true) {
            final String format;
            Label_0082: {
                synchronized (this) {
                    format = this.timeFormat12.format(time);
                    if (sb != null) {
                        sb.setLength(0);
                        this.calendar.setTime(time);
                        if (this.calendar.get(9) != 1) {
                            break Label_0082;
                        }
                        if (b) {
                            sb.append(this.pmCompact);
                        }
                        else {
                            sb.append(this.pm);
                        }
                    }
                    return format;
                }
            }
            if (b) {
                sb.append(this.amCompact);
                return format;
            }
            sb.append(this.am);
            return format;
        }
    }
    
    public String getDate() {
        synchronized (this) {
            return this.dateFormat.format(new Date());
        }
    }
    
    public String getDay() {
        synchronized (this) {
            return this.dayFormat.format(new Date());
        }
    }
    
    public DateTimeConfiguration.Clock getFormat() {
        DateTimeConfiguration.Clock clock;
        if (this.currentTimeFormat == this.timeFormat12) {
            clock = DateTimeConfiguration.Clock.CLOCK_12_HOUR;
        }
        else {
            clock = DateTimeConfiguration.Clock.CLOCK_24_HOUR;
        }
        return clock;
    }
    
    public TimeZone getTimeZone() {
        synchronized (this) {
            final String id = TimeZone.getDefault().getID();
            TimeHelper.sLogger.v("timezone is " + id);
            return TimeZone.getTimeZone(id);
        }
    }
    
    public void setFormat(final DateTimeConfiguration.Clock clock) {
        switch (clock) {
            case CLOCK_24_HOUR:
                this.currentTimeFormat = this.timeFormat24;
                break;
            case CLOCK_12_HOUR:
                this.currentTimeFormat = this.timeFormat12;
                break;
        }
        this.calendar = Calendar.getInstance();
    }
    
    public void updateLocale() {
        synchronized (this) {
            final String id = TimeZone.getDefault().getID();
            TimeHelper.sLogger.v("timezone is " + id);
            final TimeZone timeZone = TimeZone.getTimeZone(id);
            (this.dayFormat = new SimpleDateFormat("EEE")).setTimeZone(timeZone);
            (this.dateFormat = new SimpleDateFormat("d MMM")).setTimeZone(timeZone);
            boolean b = true;
            if (this.currentTimeFormat == this.timeFormat24) {
                b = false;
            }
            (this.timeFormat24 = new SimpleDateFormat("H:mm")).setTimeZone(timeZone);
            (this.timeFormat12 = new SimpleDateFormat("h:mm")).setTimeZone(timeZone);
            (this.calendar = Calendar.getInstance()).setTimeZone(timeZone);
            if (b) {
                this.currentTimeFormat = this.timeFormat12;
            }
            else {
                this.currentTimeFormat = this.timeFormat24;
            }
            this.bus.post(TimeHelper.CLOCK_CHANGED);
        }
    }
    
    public static class DateTimeAvailableEvent
    {
    }
    
    public static class UpdateClock
    {
    }
}
