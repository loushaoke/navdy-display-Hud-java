package com.navdy.hud.app.view;

import android.widget.TextView;
import android.util.TypedValue;
import android.os.Build;
import android.os.SystemClock;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import com.navdy.hud.app.manager.SpeedManager;
import android.animation.ValueAnimator;
import com.navdy.hud.app.view.drawable.SpeedoMeterGaugeDrawable2;
import android.os.Handler;

public class SpeedometerGaugePresenter2 extends GaugeViewPresenter implements SerialValueAnimatorAdapter
{
    private static final boolean ALWAYS_SHOW_SPEED_LIMIT = true;
    public static final String ID = "SPEEDO_METER_WIDGET_2";
    private static final int MAX_ANIMATION_DURATION = 100;
    public static final int MAX_SPEED = 158;
    private static final int MIN_ANIMATION_DURATION = 20;
    public static final int RESAMPLE_INTERVAL = 1000;
    public static final int SAFE = 0;
    private static final int SPEED_LIMIT_ANIMATION_DELAY = 500;
    private static final int SPEED_LIMIT_ANIMATION_DURATION = 1000;
    public static final int SPEED_LIMIT_EXCEEDED = 1;
    public static final int SPEED_LIMIT_TRESHOLD_EXCEEDED = 2;
    private int animationDuration;
    private Handler handler;
    private final String kmhString;
    private int lastSpeed;
    private long lastSpeedSampleArrivalTime;
    private SerialValueAnimator mSerialValueAnimator;
    private int mSpeed;
    private int mSpeedLimit;
    private SpeedoMeterGaugeDrawable2 mSpeedoMeterGaugeDrawable;
    private final String mphString;
    private boolean showingSpeedLimitAnimation;
    private int speedLimitAlpha;
    private Runnable speedLimitFadeOutAnimationRunnable;
    private ValueAnimator speedLimitFadeOutAnimator;
    private boolean speedLimitMarkerNeedsTobeRemoved;
    private SpeedManager speedManager;
    private final String speedoMeterWidgetName;
    private int textSize2Chars;
    private int textSize3Chars;
    private boolean updateText;
    
    public SpeedometerGaugePresenter2(final Context context) {
        this.mSpeedLimit = 0;
        this.speedLimitMarkerNeedsTobeRemoved = false;
        this.updateText = true;
        this.speedManager = SpeedManager.getInstance();
        this.lastSpeed = Integer.MIN_VALUE;
        this.lastSpeedSampleArrivalTime = 0L;
        this.animationDuration = 100;
        this.handler = new Handler();
        (this.mSpeedoMeterGaugeDrawable = new SpeedoMeterGaugeDrawable2(context, 0, R.array.smart_dash_speedo_meter_color_table, R.dimen.speedo_meter_guage_inner_bar_width)).setMaxGaugeValue(158.0f);
        this.mSerialValueAnimator = new SerialValueAnimator((SerialValueAnimator.SerialValueAnimatorAdapter)this, 100);
        this.speedLimitFadeOutAnimator = new ValueAnimator();
        this.speedLimitFadeOutAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                if (SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.isRunning()) {
                    SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.cancel();
                }
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.setIntValues(new int[] { 255, 0 });
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.setDuration(1000L);
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                        SpeedometerGaugePresenter2.this.speedLimitAlpha = (int)valueAnimator.getAnimatedValue();
                        SpeedometerGaugePresenter2.this.reDraw();
                    }
                });
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.addListener((Animator.AnimatorListener)new Animator.AnimatorListener() {
                    public void onAnimationCancel(final Animator animator) {
                    }
                    
                    public void onAnimationEnd(final Animator animator) {
                        SpeedometerGaugePresenter2.this.speedLimitAlpha = 0;
                        SpeedometerGaugePresenter2.this.showingSpeedLimitAnimation = false;
                        SpeedometerGaugePresenter2.this.reDraw();
                    }
                    
                    public void onAnimationRepeat(final Animator animator) {
                    }
                    
                    public void onAnimationStart(final Animator animator) {
                    }
                });
                SpeedometerGaugePresenter2.this.speedLimitFadeOutAnimator.start();
            }
        };
        final Resources resources = context.getResources();
        this.mphString = resources.getString(R.string.mph);
        this.kmhString = resources.getString(R.string.kilometers_per_hour);
        this.textSize2Chars = resources.getDimensionPixelSize(R.dimen.tachometer_guage_speed_text_size);
        this.textSize3Chars = resources.getDimensionPixelSize(R.dimen.tachometer_guage_speed_text_size_2);
        this.speedoMeterWidgetName = resources.getString(R.string.gauge_speedo_meter);
    }
    
    private void setSpeedValueInternal(final float n) {
        if (this.mSpeed >= this.mSpeedLimit) {
            this.showSpeedLimitText();
        }
        else if (!this.showingSpeedLimitAnimation && this.speedLimitMarkerNeedsTobeRemoved) {
            this.showSpeedLimitTextAndFadeOut();
        }
        this.reDraw();
    }
    
    private void showSpeedLimitText() {
        this.handler.removeCallbacks(this.speedLimitFadeOutAnimationRunnable);
        this.speedLimitAlpha = 255;
    }
    
    private void showSpeedLimitTextAndFadeOut() {
        this.speedLimitAlpha = 255;
    }
    
    @Override
    public void animationComplete(final float speedValueInternal) {
        this.updateText = true;
        this.setSpeedValueInternal(speedValueInternal);
    }
    
    @Override
    public Drawable getDrawable() {
        return this.mSpeedoMeterGaugeDrawable;
    }
    
    @Override
    public float getValue() {
        return this.mSpeed;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "SPEEDO_METER_WIDGET_2";
    }
    
    @Override
    public String getWidgetName() {
        return this.speedoMeterWidgetName;
    }
    
    public void setSpeed(int lastSpeed) {
        if (lastSpeed != -1) {
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            final long n = elapsedRealtime - this.lastSpeedSampleArrivalTime;
            this.lastSpeedSampleArrivalTime = elapsedRealtime;
            if (n > 1000L) {
                this.animationDuration = 100;
            }
            else {
                this.animationDuration = (int)Math.max(20L, Math.min(this.animationDuration, n));
            }
            if (this.lastSpeed != lastSpeed) {
                if ((this.lastSpeed = lastSpeed) > 158) {
                    lastSpeed = 158;
                }
                this.mSerialValueAnimator.setDuration(this.animationDuration);
                this.mSerialValueAnimator.setValue(lastSpeed);
            }
        }
    }
    
    public void setSpeedLimit(final int mSpeedLimit) {
        if (mSpeedLimit != this.mSpeedLimit) {
            this.mSpeedLimit = mSpeedLimit;
            this.showSpeedLimitText();
            if (this.mSpeed < this.mSpeedLimit) {
                this.showSpeedLimitTextAndFadeOut();
            }
            this.reDraw();
        }
    }
    
    @Override
    public void setValue(final float n) {
        final int mSpeed = (int)n;
        if (mSpeed != this.mSpeed) {
            this.mSpeed = mSpeed;
            this.updateText = false;
            this.setSpeedValueInternal(this.mSpeed);
        }
    }
    
    @Override
    public void setView(final DashboardWidgetView view) {
        if (view != null) {
            view.setContentView(R.layout.speedo_meter_gauge_2);
        }
        super.setView(view);
        if (this.mGaugeView != null) {
            if (Build$VERSION.SDK_INT >= 21) {
                final TypedValue typedValue = new TypedValue();
                this.mGaugeView.getResources().getValue(R.dimen.speedometer_text_letter_spacing, typedValue, true);
                this.mGaugeView.getValueTextView().setLetterSpacing(typedValue.getFloat());
            }
            this.reDraw();
        }
    }
    
    public void updateGauge() {
        if (this.mGaugeView != null) {
            final Resources resources = this.mGaugeView.getResources();
            this.mSpeedoMeterGaugeDrawable.setGaugeValue(this.mSpeed);
            this.mSpeedoMeterGaugeDrawable.setSpeedLimit(this.mSpeedLimit);
            final SpeedManager.SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
            final String mphString = this.mphString;
            int n = 0;
            String unitText = null;
            switch (speedUnit) {
                default:
                    n = 8;
                    unitText = this.mphString;
                    break;
                case KILOMETERS_PER_HOUR:
                    unitText = this.kmhString;
                    n = 13;
                    break;
            }
            if (this.updateText) {
                final TextView valueTextView = this.mGaugeView.getValueTextView();
                float n2;
                if (this.mSpeed >= 100) {
                    n2 = this.textSize3Chars;
                }
                else {
                    n2 = this.textSize2Chars;
                }
                valueTextView.setTextSize(2, n2);
                this.mGaugeView.setValueText(Integer.toString(this.mSpeed));
            }
            int state = 0;
            if (this.mSpeedLimit == 0) {
                state = 0;
            }
            else {
                if (this.mSpeed > this.mSpeedLimit) {
                    state = 1;
                }
                if (this.mSpeed > this.mSpeedLimit + n) {
                    state = 2;
                }
            }
            int n3 = R.color.hud_white;
            int n4 = 0;
            switch (state) {
                case 0:
                    n3 = R.color.hud_white;
                    break;
                case 1:
                    n3 = R.color.cyan;
                    n4 = 1;
                    break;
                case 2:
                    n3 = R.color.speed_very_fast_warning_color;
                    n4 = 1;
                    break;
            }
            this.mGaugeView.getUnitTextView().setTextColor(resources.getColor(n3));
            this.mSpeedoMeterGaugeDrawable.setState(state);
            this.mSpeedoMeterGaugeDrawable.speedLimitTextAlpha = this.speedLimitAlpha;
            if (n4 != 0) {
                this.mGaugeView.setUnitText(resources.getString(R.string.speed_limit_with_unit, new Object[] { this.mSpeedLimit, unitText }));
            }
            else {
                this.mGaugeView.setUnitText(unitText);
            }
        }
    }
}
