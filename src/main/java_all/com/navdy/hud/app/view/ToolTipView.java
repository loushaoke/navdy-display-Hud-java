package com.navdy.hud.app.view;

import android.text.TextPaint;
import android.view.ViewGroup.MarginLayoutParams;
import android.text.StaticLayout;
import android.text.Layout;
import android.content.res.Resources;
import butterknife.ButterKnife;
import android.view.LayoutInflater;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import android.view.View;
import butterknife.InjectView;
import android.view.ViewGroup;
import com.navdy.service.library.log.Logger;
import android.widget.LinearLayout;

public class ToolTipView extends LinearLayout
{
    private static final Logger sLogger;
    private int iconWidth;
    private int margin;
    private int maxIcons;
    private int maxTextWidth;
    private int minTextWidth;
    private int padding;
    private int sideMargin;
    @InjectView(R.id.tooltip_container)
    ViewGroup toolTipContainer;
    @InjectView(R.id.tooltip_scrim)
    View toolTipScrim;
    @InjectView(R.id.tooltip_text)
    TextView toolTipTextView;
    @InjectView(R.id.tooltip_triangle)
    View toolTipTriangle;
    private int totalWidth;
    private int triangleWidth;
    private TextView widthCalcTextView;
    
    static {
        sLogger = new Logger(ToolTipView.class);
    }
    
    public ToolTipView(final Context context) {
        this(context, null);
    }
    
    public ToolTipView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ToolTipView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init();
    }
    
    private void init() {
        final Context context = this.getContext();
        (this.widthCalcTextView = new TextView(context)).setTextAppearance(context, R.style.tool_tip);
        final Resources resources = context.getResources();
        this.maxTextWidth = resources.getDimensionPixelSize(R.dimen.tool_tip_max_text_width);
        this.minTextWidth = resources.getDimensionPixelSize(R.dimen.tool_tip_min_text_width);
        this.padding = resources.getDimensionPixelSize(R.dimen.tool_tip_padding);
        this.sideMargin = resources.getDimensionPixelSize(R.dimen.tool_tip_margin);
        this.triangleWidth = resources.getDimensionPixelSize(R.dimen.tool_tip_triangle_width);
        this.setOrientation(1);
        LayoutInflater.from(context).inflate(R.layout.tooltip_lyt, (ViewGroup)this, true);
        ButterKnife.inject((View)this);
    }
    
    public void hide() {
        this.setVisibility(GONE);
    }
    
    public void setParams(final int iconWidth, final int margin, final int maxIcons) {
        this.iconWidth = iconWidth;
        this.margin = margin;
        this.maxIcons = maxIcons;
        this.totalWidth = iconWidth * maxIcons + (maxIcons - 1) * margin;
    }
    
    public void show(int n, final String text) {
        final TextPaint paint = this.widthCalcTextView.getPaint();
        final StaticLayout staticLayout = new StaticLayout((CharSequence)text, paint, this.maxTextWidth, Layout$Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int n2 = 0;
        int n3;
        if (staticLayout.getLineCount() == 1) {
            n3 = (int)paint.measureText(text);
            if (n3 < this.minTextWidth) {
                n3 = this.minTextWidth;
            }
        }
        else {
            n3 = this.maxTextWidth;
            n2 = 1;
        }
        final ViewGroup.MarginLayoutParams ViewGroup.MarginLayoutParams = (ViewGroup.MarginLayoutParams)this.getLayoutParams();
        final int width = n3 + this.padding * 2;
        ViewGroup.MarginLayoutParams.width = width;
        final int n4 = this.iconWidth * n + this.margin * n + this.iconWidth / 2;
        final int n5 = this.triangleWidth / 2;
        switch (n) {
            default:
                n = 0;
                break;
            case 0:
            case 1:
                n = this.sideMargin;
                if (width / 2 > n4 - n) {
                    n = this.sideMargin;
                    break;
                }
                n = n4 - width / 2;
                break;
            case 2:
            case 3:
                n = this.totalWidth - n4 - this.sideMargin;
                if (width / 2 > n) {
                    n = n4 - (width - n);
                    break;
                }
                n = n4 - width / 2;
                break;
        }
        ((ViewGroup.MarginLayoutParams)this.toolTipTriangle.getLayoutParams()).leftMargin = n4 - n5 - n;
        this.toolTipTextView.setText((CharSequence)text);
        this.toolTipTextView.requestLayout();
        if (n2 != 0) {
            this.toolTipScrim.setVisibility(View.VISIBLE);
        }
        else {
            this.toolTipScrim.setVisibility(GONE);
        }
        this.setX((float)n);
        this.requestLayout();
        this.setVisibility(View.VISIBLE);
    }
}
