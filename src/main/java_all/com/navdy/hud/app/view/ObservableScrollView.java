package com.navdy.hud.app.view;

import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import android.widget.ScrollView;

public class ObservableScrollView extends ScrollView
{
    private View child;
    private IScrollListener listener;
    
    public ObservableScrollView(final Context context) {
        super(context);
    }
    
    public ObservableScrollView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public ObservableScrollView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    protected void onScrollChanged(final int n, final int n2, final int n3, final int n4) {
        if (this.child == null) {
            this.child = this.getChildAt(0);
        }
        if (this.child == null) {
            super.onScrollChanged(n, n2, n3, n4);
        }
        else if (n2 <= 0) {
            if (this.listener != null) {
                this.listener.onTop();
            }
            super.onScrollChanged(n, n2, n3, n4);
        }
        else if (this.child.getBottom() - (this.getHeight() + this.getScrollY()) == 0) {
            if (this.listener != null) {
                this.listener.onBottom();
            }
            super.onScrollChanged(n, n2, n3, n4);
        }
        else {
            if (this.listener != null) {
                this.listener.onScroll(n, n2, n3, n4);
            }
            super.onScrollChanged(n, n2, n3, n4);
        }
    }
    
    public void setScrollListener(final IScrollListener listener) {
        this.listener = listener;
    }
    
    public interface IScrollListener
    {
        void onBottom();
        
        void onScroll(final int p0, final int p1, final int p2, final int p3);
        
        void onTop();
    }
}
