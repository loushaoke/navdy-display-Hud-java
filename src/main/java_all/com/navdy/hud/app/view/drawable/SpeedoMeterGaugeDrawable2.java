package com.navdy.hud.app.view.drawable;

import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Canvas;
import android.content.Context;

public class SpeedoMeterGaugeDrawable2 extends GaugeDrawable
{
    private static final float GAUGE_START_ANGLE = 146.2f;
    private static final float SPEED_LIMIT_MARKER_ANGLE_WIDTH = 4.0f;
    public static final int TEXT_MARGIN = 15;
    private int mGaugeMargin;
    private int mOuterRingColor;
    private int mSpeedLimit;
    private int mSpeedLimitMarkerColor;
    private int mSpeedLimitTextColor;
    private int mSpeedLimitTextSize;
    private int mValueRingWidth;
    public int speedLimitTextAlpha;
    
    public SpeedoMeterGaugeDrawable2(final Context context, final int n, final int n2, final int n3) {
        super(context, n, n2);
        this.speedLimitTextAlpha = 0;
        this.mValueRingWidth = context.getResources().getDimensionPixelSize(n3);
        this.mGaugeMargin = context.getResources().getDimensionPixelSize(R.dimen.speedometer_gauge_margin);
        this.mSpeedLimitTextColor = context.getResources().getColor(R.color.hud_white);
        this.mOuterRingColor = this.mColorTable[3];
        this.mSpeedLimitMarkerColor = this.mColorTable[4];
        this.mSpeedLimitTextSize = context.getResources().getDimensionPixelSize(R.dimen.speedometer_gauge_speed_limit_text_size);
    }
    
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final Rect bounds = this.getBounds();
        final int width = bounds.width();
        final int n = this.mValueRingWidth / 2;
        final RectF rectF = new RectF((float)(bounds.left + n), (float)(bounds.top + n), (float)(bounds.right - n), (float)(bounds.top + width - n));
        rectF.inset((float)this.mGaugeMargin, (float)this.mGaugeMargin);
        this.mPaint.setStrokeWidth((float)this.mValueRingWidth);
        this.mPaint.setColor(this.mOuterRingColor);
        this.mPaint.setStyle(Paint$Style.STROKE);
        canvas.drawArc(rectF, 146.2f, 247.6f, false, this.mPaint);
        this.mPaint.setStrokeWidth((float)this.mValueRingWidth);
        this.mPaint.setStyle(Paint$Style.STROKE);
        this.mPaint.setColor(this.mDefaultColor);
        canvas.drawArc(rectF, 146.2f, this.mValue / this.mMaxValue * 247.6f, false, this.mPaint);
        if (this.mSpeedLimit > 0) {
            this.mPaint.setColor(this.mSpeedLimitMarkerColor);
            final float n2 = (146.2f + this.mSpeedLimit / this.mMaxValue * 247.6f - 2.0f) % 360.0f;
            canvas.drawArc(rectF, n2, 4.0f, false, this.mPaint);
            final RectF rectF2 = new RectF(bounds);
            rectF2.inset(15.0f, 15.0f);
            final int n3 = (int)(rectF2.width() / 2.0f);
            final int alpha = this.mPaint.getAlpha();
            this.mPaint.setStyle(Paint$Style.FILL);
            this.mPaint.setColor(this.mSpeedLimitTextColor);
            this.mPaint.setTextSize((float)this.mSpeedLimitTextSize);
            this.mPaint.setAlpha(this.speedLimitTextAlpha);
            this.mPaint.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
            final String string = Integer.toString(this.mSpeedLimit);
            final Rect rect = new Rect();
            this.mPaint.getTextBounds(string, 0, string.length(), rect);
            canvas.drawText(string, (float)((int)(rectF2.centerX() + n3 * Math.cos(Math.toRadians(n2))) - rect.width() / 2), (float)(rect.height() / 2 + (int)(rectF2.centerY() + n3 * Math.sin(Math.toRadians(n2)))), this.mPaint);
            this.mPaint.setAlpha(alpha);
        }
    }
    
    public void setSpeedLimit(final int mSpeedLimit) {
        this.mSpeedLimit = mSpeedLimit;
    }
    
    @Override
    public void setState(final int state) {
        super.setState(state);
        switch (state) {
            case 0:
                this.mSpeedLimitMarkerColor = this.mColorTable[1];
                break;
            case 1:
                this.mSpeedLimitMarkerColor = this.mColorTable[4];
                break;
            case 2:
                this.mSpeedLimitMarkerColor = this.mColorTable[4];
                break;
        }
    }
}
