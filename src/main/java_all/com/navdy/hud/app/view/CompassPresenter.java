package com.navdy.hud.app.view;

import android.graphics.drawable.Drawable;
import android.content.Context;

public class CompassPresenter extends DashboardWidgetPresenter implements SerialValueAnimatorAdapter
{
    private SerialValueAnimator animator;
    private String compassGaugeName;
    CompassDrawable mCompassDrawable;
    float mHeadingAngle;
    
    public CompassPresenter(final Context context) {
        (this.mCompassDrawable = new CompassDrawable(context)).setMaxGaugeValue(360.0f);
        this.mCompassDrawable.setMinValue(0.0f);
        this.animator = new SerialValueAnimator((SerialValueAnimator.SerialValueAnimatorAdapter)this, 50);
        this.compassGaugeName = context.getResources().getString(R.string.widget_compass);
    }
    
    @Override
    public void animationComplete(final float n) {
    }
    
    @Override
    public Drawable getDrawable() {
        return this.mCompassDrawable;
    }
    
    @Override
    public float getValue() {
        return this.mHeadingAngle;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "COMPASS_WIDGET";
    }
    
    @Override
    public String getWidgetName() {
        return this.compassGaugeName;
    }
    
    public void setHeadingAngle(final double n) {
        if (!this.isDashActive || !this.isWidgetVisibleToUser || this.mWidgetView == null) {
            this.mHeadingAngle = (float)n;
        }
        else if (n == 0.0) {
            this.mHeadingAngle = 0.0f;
            this.reDraw();
        }
        else {
            this.animator.setValue((float)(n % 360.0));
        }
    }
    
    @Override
    public void setValue(final float mHeadingAngle) {
        this.mHeadingAngle = mHeadingAngle;
        this.reDraw();
    }
    
    @Override
    public void setView(final DashboardWidgetView view) {
        if (view != null) {
            view.setContentView(R.layout.smart_dash_widget_circular_content_layout);
        }
        super.setView(view);
    }
    
    @Override
    protected void updateGauge() {
        this.mCompassDrawable.setGaugeValue(this.mHeadingAngle);
    }
}
