package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.Gesture;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.service.library.events.input.GestureEvent;
import android.content.res.Resources;
import java.util.List;
import java.util.ArrayList;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import butterknife.ButterKnife;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.ViewGroup;
import mortar.Mortar;
import com.navdy.hud.app.util.os.SystemProperties;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import android.animation.ValueAnimator;
import com.navdy.hud.app.screen.GestureLearningScreen;
import android.widget.LinearLayout;
import android.animation.AnimatorSet;
import android.os.Handler;
import android.view.View;
import butterknife.InjectView;
import android.widget.ImageView;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import android.widget.RelativeLayout;

public class GestureLearningView extends RelativeLayout implements IListener, IInputHandler
{
    public static final int MOVE_IN_DURATION = 600;
    public static final int MOVE_OUT_DURATION = 250;
    private static final int PROGRESS_INDICATOR_HIDE_INTERVAL = 100;
    private static final int SCROLL_DURATION = 1000;
    private static final int SCROLL_INTERVAL = 4000;
    private static final int SENSOR_BLOCKED_MESSAGE_DELAY = 30000;
    private static final String SMOOTHING_COEFF_KEY = "persist.sys.gesture_smooth";
    public static final int TAG_CAPTURE = 2;
    public static final int TAG_DONE = 0;
    public static final int TAG_TIPS = 1;
    private String[] TIPS;
    @Inject
    Bus mBus;
    @InjectView(R.id.center_image)
    ImageView mCenterImage;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    private int mCurrentSmallTipIndex;
    private float mGestureIndicatorHalfWidth;
    private float mGestureIndicatorProgressSpan;
    @InjectView(R.id.gesture_progress_indicator)
    View mGestureProgressIndicator;
    private float mHandIconMargin;
    private Handler mHandler;
    private AnimatorSet mLeftGestureAnimation;
    @InjectView(R.id.lyt_left_swipe)
    LinearLayout mLeftSwipeLayout;
    private final float mLowPassCoeff;
    private float mNeutralX;
    @Inject
    GestureLearningScreen.Presenter mPresenter;
    private Runnable mProgressIndicatorRunnable;
    private AnimatorSet mRightGestureAnimation;
    @InjectView(R.id.lyt_right_swipe)
    LinearLayout mRightSwipeLayout;
    private ValueAnimator mScrollAnimator;
    private Runnable mShowSensorBlockedMessageRunnable;
    private float mSmoothedProgress;
    private boolean mStopAnimation;
    @InjectView(R.id.tips_scroller)
    LinearLayout mTipsScroller;
    int mTipsScrollerHeight;
    private Runnable mTipsScrollerRunnable;
    @InjectView(R.id.tipsText1)
    TextView mTipsTextView1;
    @InjectView(R.id.tipsText2)
    TextView mTipsTextView2;
    
    public GestureLearningView(final Context context) {
        this(context, null);
    }
    
    public GestureLearningView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public GestureLearningView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mCurrentSmallTipIndex = 0;
        this.mStopAnimation = false;
        this.mLowPassCoeff = Float.parseFloat(SystemProperties.get("persist.sys.gesture_smooth", "1.0"));
        this.mSmoothedProgress = 0.0f;
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    private AnimatorSet prepareGestureAnimation(final boolean b, final ViewGroup viewGroup) {
        TextView textView;
        ImageView imageView;
        if (b) {
            textView = (TextView)viewGroup.findViewById(R.id.txt_right);
            imageView = (ImageView)viewGroup.findViewById(R.id.img_right_hand_overlay);
        }
        else {
            textView = (TextView)viewGroup.findViewById(R.id.txt_left);
            imageView = (ImageView)viewGroup.findViewById(R.id.img_left_hand_overlay);
        }
        final AnimatorSet set = new AnimatorSet();
        final ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setDuration(250L);
        final int dimensionPixelSize = this.getResources().getDimensionPixelSize(R.dimen.gesture_icon_animate_distance);
        valueAnimator.setIntValues(new int[] { (int)this.mHandIconMargin, (int)this.mHandIconMargin + dimensionPixelSize });
        valueAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                final ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)viewGroup.getLayoutParams();
                final int intValue = (int)valueAnimator.getAnimatedValue();
                if (b) {
                    layoutParams.leftMargin = intValue;
                }
                else {
                    layoutParams.rightMargin = intValue;
                }
                viewGroup.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            }
        });
        final ValueAnimator valueAnimator2 = new ValueAnimator();
        valueAnimator2.setDuration(600L);
        valueAnimator2.setIntValues(new int[] { (int)this.mHandIconMargin + dimensionPixelSize, (int)this.mHandIconMargin });
        valueAnimator2.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                final ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)viewGroup.getLayoutParams();
                final int intValue = (int)valueAnimator.getAnimatedValue();
                if (b) {
                    layoutParams.leftMargin = intValue;
                }
                else {
                    layoutParams.rightMargin = intValue;
                }
                viewGroup.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            }
        });
        final ValueAnimator valueAnimator3 = new ValueAnimator();
        valueAnimator3.setDuration(600L);
        valueAnimator3.setFloatValues(new float[] { 1.0f, 0.0f });
        valueAnimator3.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                textView.setAlpha((float)valueAnimator.getAnimatedValue());
                imageView.setAlpha((float)valueAnimator.getAnimatedValue());
            }
        });
        set.play((Animator)valueAnimator).before((Animator)valueAnimator3).before((Animator)valueAnimator2);
        set.addListener((Animator.AnimatorListener)new Animator.AnimatorListener() {
            public void onAnimationCancel(final Animator animator) {
            }
            
            public void onAnimationEnd(final Animator animator) {
                imageView.setAlpha(0.0f);
                textView.setAlpha(0.0f);
            }
            
            public void onAnimationRepeat(final Animator animator) {
            }
            
            public void onAnimationStart(final Animator animator) {
                imageView.setAlpha(1.0f);
                textView.setAlpha(1.0f);
            }
        });
        return set;
    }
    
    public void executeItem(final int n, final int n2) {
        switch (n2) {
            case 0:
                this.mPresenter.finish();
                break;
            case 1:
                this.mPresenter.showTips();
                break;
            case 2:
                this.mPresenter.showCaptureView();
                break;
        }
    }
    
    public void itemSelected(final int n, final int n2) {
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mBus.unregister(this);
        this.mStopAnimation = true;
        this.mScrollAnimator.cancel();
        this.mHandler.removeCallbacks(this.mTipsScrollerRunnable);
        this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.mChoiceLayout.setHighlightPersistent(true);
        final Resources resources = this.getResources();
        this.mTipsScrollerHeight = resources.getDimensionPixelSize(R.dimen.gesture_learning_tips_text_scroll_height);
        this.mHandIconMargin = resources.getDimension(R.dimen.gesture_icon_margin_from_center_image);
        this.mLeftGestureAnimation = this.prepareGestureAnimation(false, (ViewGroup)this.mLeftSwipeLayout);
        this.mRightGestureAnimation = this.prepareGestureAnimation(true, (ViewGroup)this.mRightSwipeLayout);
        this.TIPS = resources.getStringArray(R.array.gesture_small_tips);
        this.mHandler = new Handler();
        (this.mScrollAnimator = new ValueAnimator()).setIntValues(new int[] { 0, -this.mTipsScrollerHeight });
        this.mScrollAnimator.setDuration(1000L);
        this.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                GestureLearningView.this.mNeutralX = GestureLearningView.this.getWidth() / 2;
                GestureLearningView.this.mGestureProgressIndicator.setY(GestureLearningView.this.mCenterImage.getY() + GestureLearningView.this.mCenterImage.getHeight() / 2 - GestureLearningView.this.mGestureProgressIndicator.getHeight() / 2);
                GestureLearningView.this.mGestureIndicatorProgressSpan = GestureLearningView.this.mHandIconMargin + GestureLearningView.this.mCenterImage.getWidth() / 2 - GestureLearningView.this.mGestureProgressIndicator.getWidth() / 2;
                GestureLearningView.this.mGestureIndicatorHalfWidth = GestureLearningView.this.mGestureProgressIndicator.getWidth() / 2;
                GestureLearningView.this.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener)this);
            }
        });
        this.mScrollAnimator.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                final ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)GestureLearningView.this.mTipsScroller.getLayoutParams();
                layoutParams.topMargin = (int)valueAnimator.getAnimatedValue();
                GestureLearningView.this.mTipsScroller.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            }
        });
        this.mScrollAnimator.addListener((Animator.AnimatorListener)new Animator.AnimatorListener() {
            public void onAnimationCancel(final Animator animator) {
            }
            
            public void onAnimationEnd(final Animator animator) {
                GestureLearningView.this.mCurrentSmallTipIndex = (GestureLearningView.this.mCurrentSmallTipIndex + 1) % GestureLearningView.this.TIPS.length;
                final String text = GestureLearningView.this.TIPS[GestureLearningView.this.mCurrentSmallTipIndex];
                final String text2 = GestureLearningView.this.TIPS[(GestureLearningView.this.mCurrentSmallTipIndex + 1) % GestureLearningView.this.TIPS.length];
                GestureLearningView.this.mTipsTextView1.setText((CharSequence)text);
                final ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams)GestureLearningView.this.mTipsScroller.getLayoutParams();
                layoutParams.topMargin = 0;
                GestureLearningView.this.mTipsScroller.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
                GestureLearningView.this.mTipsTextView2.setText((CharSequence)text2);
                if (!GestureLearningView.this.mStopAnimation) {
                    GestureLearningView.this.mHandler.postDelayed(GestureLearningView.this.mTipsScrollerRunnable, 4000L);
                }
            }
            
            public void onAnimationRepeat(final Animator animator) {
            }
            
            public void onAnimationStart(final Animator animator) {
            }
        });
        final String text = this.TIPS[this.mCurrentSmallTipIndex];
        final String text2 = this.TIPS[(this.mCurrentSmallTipIndex + 1) % this.TIPS.length];
        this.mTipsTextView1.setText((CharSequence)text);
        this.mTipsTextView2.setText((CharSequence)text2);
        this.mTipsScrollerRunnable = new Runnable() {
            @Override
            public void run() {
                GestureLearningView.this.mScrollAnimator.start();
            }
        };
        this.mProgressIndicatorRunnable = new Runnable() {
            @Override
            public void run() {
                GestureLearningView.this.mGestureProgressIndicator.setVisibility(INVISIBLE);
            }
        };
        this.mShowSensorBlockedMessageRunnable = new Runnable() {
            @Override
            public void run() {
                GestureLearningView.this.mPresenter.showCameraSensorBlocked();
            }
        };
        this.mHandler.postDelayed(this.mTipsScrollerRunnable, 4000L);
        final ArrayList<Choice> list = new ArrayList<Choice>();
        list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.done), 0));
        list.add(new ChoiceLayout.Choice(this.getContext().getString(R.string.tips), 1));
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, (ChoiceLayout.IListener)this);
        if (!this.isInEditMode()) {
            this.mBus.register(this);
        }
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
        switch (gestureEvent.gesture) {
            case GESTURE_SWIPE_LEFT:
                this.mLeftGestureAnimation.start();
                break;
            case GESTURE_SWIPE_RIGHT:
                this.mRightGestureAnimation.start();
                break;
        }
        return true;
    }
    
    @Subscribe
    public void onGestureProgress(final GestureServiceConnector.GestureProgress gestureProgress) {
        this.mHandler.removeCallbacks(this.mProgressIndicatorRunnable);
        float progress = gestureProgress.progress;
        if (gestureProgress.direction == GestureServiceConnector.GestureDirection.LEFT) {
            progress *= -1.0f;
        }
        final float mSmoothedProgress = this.mLowPassCoeff * progress + this.mSmoothedProgress * (1.0f - this.mLowPassCoeff);
        if (mSmoothedProgress != this.mSmoothedProgress) {
            this.mGestureProgressIndicator.setVisibility(View.VISIBLE);
            this.mSmoothedProgress = mSmoothedProgress;
            this.mGestureProgressIndicator.setX(this.mNeutralX + this.mGestureIndicatorProgressSpan * this.mSmoothedProgress - this.mGestureIndicatorHalfWidth);
        }
        this.mHandler.postDelayed(this.mProgressIndicatorRunnable, 100L);
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        switch (customKeyEvent) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                break;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                break;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                break;
        }
        return false;
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        if (visibility == 8) {
            this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
        }
        else if (visibility == 0) {
            this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
            this.mHandler.postDelayed(this.mShowSensorBlockedMessageRunnable, 30000L);
        }
    }
}
