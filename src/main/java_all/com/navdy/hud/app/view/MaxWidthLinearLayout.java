package com.navdy.hud.app.view;

import android.view.View;
import android.content.res.TypedArray;
import com.navdy.hud.app.R;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.LinearLayout;

public class MaxWidthLinearLayout extends LinearLayout
{
    private int maxWidth;
    
    public MaxWidthLinearLayout(final Context context) {
        super(context);
        this.maxWidth = 0;
    }
    
    public MaxWidthLinearLayout(final Context context, final AttributeSet set) {
        super(context, set);
        final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.MaxWidthLinearLayout);
        this.maxWidth = obtainStyledAttributes.getDimensionPixelSize(0, Integer.MAX_VALUE);
        obtainStyledAttributes.recycle();
    }
    
    public int getMaxWidth() {
        return this.maxWidth;
    }
    
    protected void onMeasure(int mode, final int n) {
        final int size = View$MeasureSpec.getSize(mode);
        int measureSpec = mode;
        if (this.maxWidth > 0) {
            measureSpec = mode;
            if (this.maxWidth < size) {
                mode = View$MeasureSpec.getMode(mode);
                measureSpec = View$MeasureSpec.makeMeasureSpec(this.maxWidth, mode);
            }
        }
        super.onMeasure(measureSpec, n);
    }
    
    public void setMaxWidth(final int maxWidth) {
        this.maxWidth = maxWidth;
        this.invalidate();
    }
}
