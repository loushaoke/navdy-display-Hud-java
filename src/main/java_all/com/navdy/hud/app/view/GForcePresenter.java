package com.navdy.hud.app.view;

import com.squareup.otto.Subscribe;
import com.navdy.hud.app.device.gps.CalibratedGForceData;
import android.graphics.drawable.Drawable;
import android.content.Context;

public class GForcePresenter extends DashboardWidgetPresenter
{
    GForceDrawable drawable;
    private String gMeterGaugeName;
    private boolean registered;
    private float xAccel;
    private float yAccel;
    private float zAccel;
    
    public GForcePresenter(final Context context) {
        this.drawable = new GForceDrawable(context);
        this.gMeterGaugeName = context.getResources().getString(R.string.widget_g_meter);
    }
    
    @Override
    public Drawable getDrawable() {
        return this.drawable;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "GFORCE_WIDGET";
    }
    
    @Override
    public String getWidgetName() {
        return this.gMeterGaugeName;
    }
    
    @Override
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    @Subscribe
    public void onCalibratedGForceData(final CalibratedGForceData calibratedGForceData) {
        this.xAccel = calibratedGForceData.getXAccel();
        this.yAccel = calibratedGForceData.getYAccel();
        this.zAccel = calibratedGForceData.getZAccel();
        this.reDraw();
    }
    
    @Override
    public void setView(final DashboardWidgetView view) {
        if (view != null) {
            view.setContentView(R.layout.smart_dash_widget_circular_content_layout);
        }
        super.setView(view);
    }
    
    @Override
    protected void updateGauge() {
        this.drawable.setAcceleration(this.xAccel, this.yAccel, this.zAccel);
    }
}
