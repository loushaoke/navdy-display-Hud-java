package com.navdy.hud.app.view;

import android.graphics.Path;
import android.graphics.Canvas;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint;
import android.graphics.Paint;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.view.drawable.GaugeDrawable;

public class CompassDrawable extends GaugeDrawable
{
    private static final double SEGMENT_ANGLE = 22.5;
    private static final int TEXT_PADDING = 6;
    private static final int TOTAL_SEGMENTS = 16;
    private static String[] headings;
    private static int majorTickHeight;
    private static int minorTickHeight;
    private static int[] widths;
    private Drawable mIndicatorDrawable;
    private int mIndicatorHeight;
    private int mIndicatorTopMargin;
    private int mIndicatorWidth;
    private Path mPath;
    private int mStripHeight;
    private int mStripTopMargin;
    private int mStripWidth;
    private Paint mTextPaint;
    
    static {
        CompassDrawable.majorTickHeight = 10;
        CompassDrawable.minorTickHeight = 5;
    }
    
    public CompassDrawable(final Context context) {
        super(context, 0);
        final Resources resources = context.getResources();
        this.mIndicatorDrawable = resources.getDrawable(R.drawable.icon_gauge_compass_heading_indicator);
        this.mIndicatorWidth = resources.getDimensionPixelSize(R.dimen.compass_indicator_width);
        this.mIndicatorHeight = resources.getDimensionPixelSize(R.dimen.compass_indicator_height);
        this.mIndicatorTopMargin = resources.getDimensionPixelSize(R.dimen.compass_indicator_top_margin);
        this.mStripWidth = resources.getDimensionPixelSize(R.dimen.compass_strip_width);
        this.mStripTopMargin = resources.getDimensionPixelSize(R.dimen.compass_strip_margin_top);
        this.mStripHeight = resources.getDimensionPixelSize(R.dimen.compass_strip_height);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(Paint$Style.FILL);
        this.mPaint.setColor(resources.getColor(R.color.compass_frame_color));
        (this.mTextPaint = new Paint()).setStrokeWidth(0.0f);
        this.mTextPaint.setColor(-1);
        this.mTextPaint.setAntiAlias(false);
        this.mTextPaint.setTextAlign(Paint$Align.LEFT);
        this.mTextPaint.setTypeface(Typeface.create("sans-serif", 1));
        this.mTextPaint.setTextSize((float)resources.getDimensionPixelSize(R.dimen.compass_text_height));
        this.mPath = new Path();
        final Rect rect = new Rect();
        if (CompassDrawable.headings == null) {
            CompassDrawable.majorTickHeight = resources.getDimensionPixelSize(R.dimen.compass_major_tick_height);
            CompassDrawable.minorTickHeight = resources.getDimensionPixelSize(R.dimen.compass_minor_tick_height);
            CompassDrawable.headings = resources.getStringArray(R.array.compass_points);
            CompassDrawable.widths = new int[CompassDrawable.headings.length];
            for (int i = 0; i < CompassDrawable.widths.length; ++i) {
                this.mTextPaint.getTextBounds(CompassDrawable.headings[i], 0, CompassDrawable.headings[i].length(), rect);
                CompassDrawable.widths[i] = rect.width();
            }
        }
    }
    
    private void drawCompassPoints(final Canvas canvas, float n, int i, int n2, int n3, final int n4, final boolean b) {
        n2 = n3 - i;
        final float n5 = n2 / 2 + i;
        final int n6 = this.mStripWidth / 16;
        n2 = (int)Math.ceil(n2 / n6) + 1;
        final float n7 = n % 360.0f / 22.5f;
        final int n8 = (int)n7;
        n = n8;
        int n9;
        int n10;
        String s;
        for (i = -n2 / 2; i < n2 / 2; ++i) {
            n9 = (int)((i + (n7 - n)) * n6 + n5 + 0.5);
            n10 = (n8 - i + 16) % 16;
            if ((n10 & 0x1) == 0x0) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            if (n2 != 0) {
                n3 = CompassDrawable.majorTickHeight;
            }
            else {
                n3 = CompassDrawable.minorTickHeight;
            }
            this.mTextPaint.setAntiAlias(false);
            canvas.drawLine((float)n9, (float)(n4 - n3), (float)n9, (float)n4, this.mTextPaint);
            if (n2 != 0 && b) {
                n2 = n10 / 2;
                s = CompassDrawable.headings[n2];
                this.mTextPaint.setAntiAlias(true);
                canvas.drawText(s, (float)(n9 - CompassDrawable.widths[n2] / 2), (float)(n4 - n3 - 6), this.mTextPaint);
            }
        }
    }
    
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final Rect bounds = this.getBounds();
        final int width = bounds.width();
        final int height = bounds.height();
        canvas.clipRect(bounds);
        canvas.drawArc(0.0f, 0.0f, (float)width, (float)height, 0.0f, 360.0f, true, this.mPaint);
        final int left = bounds.left;
        final int n = width / 2;
        final int n2 = this.mIndicatorWidth / 2;
        final int n3 = bounds.top + this.mIndicatorTopMargin;
        this.mIndicatorDrawable.setBounds(left + n - n2, n3, bounds.left + width / 2 + this.mIndicatorWidth / 2, n3 + this.mIndicatorHeight);
        this.mIndicatorDrawable.draw(canvas);
        canvas.clipPath(this.mPath);
        this.drawCompassPoints(canvas, this.mValue, 0, this.mStripTopMargin, width, this.mStripTopMargin + this.mStripHeight, this.mValue != 0.0f);
    }
    
    protected void onBoundsChange(final Rect rect) {
        super.onBoundsChange(rect);
        this.mPath.reset();
        this.mPath.addOval(0.0f, 0.0f, (float)rect.width(), (float)rect.height(), Path$Direction.CW);
    }
}
