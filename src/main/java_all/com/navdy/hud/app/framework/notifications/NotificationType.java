package com.navdy.hud.app.framework.notifications;

public enum NotificationType
{
    BRIGHTNESS(1), 
    ETA_DELAY(96), 
    FASTER_ROUTE(97), 
    GLANCE(50), 
    GLYMPSE(100), 
    LOW_FUEL(97), 
    MUSIC(1), 
    PHONE_CALL(99), 
    PLACE_TYPE_SEARCH(98), 
    ROUTE_CALC(98), 
    SMS(100), 
    TRAFFIC_EVENT(95), 
    TRAFFIC_JAM(94), 
    VOICE_ASSIST(1), 
    VOICE_SEARCH(98);
    
    int priority;
    
    private NotificationType(final int priority) {
        this.priority = priority;
    }
    
    public int getPriority() {
        return this.priority;
    }
}
