package com.navdy.hud.app.framework.network;

import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import android.os.Process;
import android.content.Intent;
import android.content.Context;
import com.navdy.hud.app.util.DeviceUtil;
import mortar.Mortar;
import android.net.NetworkInfo;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import android.net.ConnectivityManager;
import com.navdy.service.library.task.TaskManager;
import android.os.Looper;
import com.navdy.service.library.events.settings.NetworkStateChange;
import android.os.Handler;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class NetworkStateManager
{
    private static final String NAVDY_NETWORK_DOWN_INTENT = "com.navdy.hud.NetworkDown";
    private static final String NAVDY_NETWORK_UP_INTENT = "com.navdy.hud.NetworkUp";
    private static final Logger sLogger;
    private static final NetworkStateManager singleton;
    @Inject
    Bus bus;
    private int countDown;
    private Handler handler;
    private NetworkStateChange lastPhoneNetworkState;
    private Runnable networkCheck;
    private volatile boolean networkStateInitialized;
    private Runnable triggerNetworkCheck;
    
    static {
        sLogger = new Logger(NetworkStateManager.class);
        singleton = new NetworkStateManager();
    }
    
    private NetworkStateManager() {
        this.handler = new Handler(Looper.getMainLooper());
        this.countDown = 3;
        this.triggerNetworkCheck = new Runnable() {
            @Override
            public void run() {
                if (!NetworkStateManager.this.networkStateInitialized) {
                    TaskManager.getInstance().execute(NetworkStateManager.this.networkCheck, 10);
                }
            }
        };
        this.networkCheck = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    while (true) {
                        int access$300 = 0;
                        Label_0390: {
                            while (true) {
                                try {
                                    NetworkStateManager.sLogger.v("checking n/w state");
                                    NetworkInfo[] allNetworkInfo = ((ConnectivityManager)HudApplication.getAppContext().getSystemService("connectivity")).getAllNetworkInfo();
                                    if (allNetworkInfo == null || allNetworkInfo.length == 0) {
                                        NetworkStateManager.sLogger.v("no n/w info");
                                    }
                                    else {
                                        access$300 = 0;
                                        if (access$300 < allNetworkInfo.length) {
                                            if (allNetworkInfo[access$300].getType() != 8) {
                                                break Label_0390;
                                            }
                                            NetworkStateManager.sLogger.v("found dummy network we are up:" + allNetworkInfo[access$300].getTypeName() + " count down=" + NetworkStateManager.this.countDown);
                                            --NetworkStateManager.this.countDown;
                                            access$300 = NetworkStateManager.this.countDown;
                                            if (access$300 > 0) {
                                                if (!NetworkStateManager.this.networkStateInitialized) {
                                                    NetworkStateManager.this.handler.postDelayed(NetworkStateManager.this.triggerNetworkCheck, 1000L);
                                                }
                                                return;
                                            }
                                            else {
                                                NetworkStateManager.sLogger.v("found dummy network starting init");
                                                NetworkStateManager.this.networkStateInitialized = true;
                                                if (!RemoteDeviceManager.getInstance().isRemoteDeviceConnected() || !RemoteDeviceManager.getInstance().isNetworkLinkReady()) {
                                                    break Label_0390;
                                                }
                                                if (NetworkStateManager.this.lastPhoneNetworkState == null || !NetworkStateManager.this.lastPhoneNetworkState.networkAvailable) {
                                                    NetworkStateManager.this.networkNotAvailable();
                                                }
                                                else {
                                                    NetworkStateManager.this.networkAvailable();
                                                }
                                                final Bus bus = NetworkStateManager.this.bus;
                                                allNetworkInfo = (NetworkInfo[])new HudNetworkInitialized();
                                                bus.post(allNetworkInfo);
                                            }
                                        }
                                        if (!NetworkStateManager.this.networkStateInitialized) {
                                            NetworkStateManager.sLogger.v("network state not initialized");
                                        }
                                        if (!NetworkStateManager.this.networkStateInitialized) {
                                            NetworkStateManager.this.handler.postDelayed(NetworkStateManager.this.triggerNetworkCheck, 1000L);
                                        }
                                    }
                                    return;
                                }
                                catch (Throwable t) {
                                    NetworkStateManager.sLogger.e(t);
                                    if (!NetworkStateManager.this.networkStateInitialized) {
                                        NetworkStateManager.this.handler.postDelayed(NetworkStateManager.this.triggerNetworkCheck, 1000L);
                                        return;
                                    }
                                    return;
                                    NetworkStateManager.this.networkNotAvailable();
                                    continue;
                                }
                                finally {
                                    if (!NetworkStateManager.this.networkStateInitialized) {
                                        NetworkStateManager.this.handler.postDelayed(NetworkStateManager.this.triggerNetworkCheck, 1000L);
                                    }
                                }
                                break;
                            }
                        }
                        ++access$300;
                        continue;
                    }
                }
            }
        };
        Mortar.inject(HudApplication.getAppContext(), this);
        this.bus.register(this);
        if (DeviceUtil.isNavdyDevice()) {
            this.handler.post(this.triggerNetworkCheck);
        }
        else {
            this.networkStateInitialized = true;
            this.bus.post(new HudNetworkInitialized());
        }
    }
    
    public static NetworkStateManager getInstance() {
        return NetworkStateManager.singleton;
    }
    
    public static boolean isConnectedToCellNetwork(final Context context) {
        final boolean b = false;
        final NetworkInfo networkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getNetworkInfo(0);
        boolean b2 = b;
        if (networkInfo != null) {
            b2 = b;
            if (networkInfo.isConnected()) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public static boolean isConnectedToNetwork(final Context context) {
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        final Logger sLogger = NetworkStateManager.sLogger;
        final StringBuilder append = new StringBuilder().append("setEngineOnlineState active network=");
        String typeName;
        if (activeNetworkInfo != null) {
            typeName = activeNetworkInfo.getTypeName();
        }
        else {
            typeName = " no active network ";
        }
        sLogger.v(append.append(typeName).toString());
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
    
    public static boolean isConnectedToWifi(final Context context) {
        boolean b = true;
        final NetworkInfo networkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getNetworkInfo(1);
        if (networkInfo == null || !networkInfo.isConnected()) {
            b = false;
        }
        return b;
    }
    
    public NetworkStateChange getLastPhoneNetworkState() {
        return this.lastPhoneNetworkState;
    }
    
    public boolean isNetworkStateInitialized() {
        return this.networkStateInitialized;
    }
    
    public void networkAvailable() {
        if (!this.networkStateInitialized) {
            NetworkStateManager.sLogger.v("network not initialized yet");
        }
        else if (NetworkBandwidthController.getInstance().isNetworkDisabled()) {
            NetworkStateManager.sLogger.i("network is permanently disabled");
            this.networkNotAvailable();
        }
        else {
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent("com.navdy.hud.NetworkUp"), Process.myUserHandle());
            NetworkStateManager.sLogger.v("DummyNetNetworkFactory:sent:com.navdy.hud.NetworkUp");
        }
    }
    
    public void networkNotAvailable() {
        if (!this.networkStateInitialized) {
            NetworkStateManager.sLogger.v("network not initialized yet");
        }
        else {
            HudApplication.getAppContext().sendBroadcastAsUser(new Intent("com.navdy.hud.NetworkDown"), Process.myUserHandle());
            NetworkStateManager.sLogger.v("DummyNetNetworkFactory:sent:com.navdy.hud.NetworkDown");
        }
    }
    
    @Subscribe
    public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
        NetworkStateManager.sLogger.d("ConnectionStateChange - " + connectionStateChange.state);
        switch (connectionStateChange.state) {
            case CONNECTION_DISCONNECTED:
                this.lastPhoneNetworkState = null;
                break;
        }
    }
    
    @Subscribe
    public void onNetworkStateChange(final NetworkStateChange lastPhoneNetworkState) {
        try {
            this.lastPhoneNetworkState = lastPhoneNetworkState;
            NetworkStateManager.sLogger.v("NetworkStateChange available[" + lastPhoneNetworkState.networkAvailable + "] cell[" + lastPhoneNetworkState.cellNetwork + "] wifi[" + lastPhoneNetworkState.wifiNetwork + "] reach[" + lastPhoneNetworkState.reachability + "]");
            if (Boolean.TRUE.equals(lastPhoneNetworkState.networkAvailable)) {
                NetworkStateManager.sLogger.v("DummyNetNetworkFactory:phone n/w avaialble");
                this.networkAvailable();
            }
            else {
                NetworkStateManager.sLogger.v("DummyNetNetworkFactory:phone n/w lost");
                this.networkNotAvailable();
            }
        }
        catch (Throwable t) {
            NetworkStateManager.sLogger.e(t);
        }
    }
    
    public static class HudNetworkInitialized
    {
    }
}
