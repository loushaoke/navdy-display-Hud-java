package com.navdy.hud.app.framework.network;

import java.util.HashMap;

class DnsCache
{
    private HashMap<String, String> IPtoHostnameMap;
    
    DnsCache() {
        this.IPtoHostnameMap = new HashMap<String, String>();
    }
    
    public void addEntry(final String s, final String s2) {
        synchronized (this) {
            this.IPtoHostnameMap.put(s, s2);
        }
    }
    
    public void clear(final String s, final String s2) {
        synchronized (this) {
            this.IPtoHostnameMap.clear();
        }
    }
    
    public String getHostnamefromIP(String s) {
        synchronized (this) {
            s = this.IPtoHostnameMap.get(s);
            return s;
        }
    }
}
