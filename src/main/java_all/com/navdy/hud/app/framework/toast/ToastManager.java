package com.navdy.hud.app.framework.toast;

import android.os.Bundle;
import com.navdy.hud.app.util.GenericUtil;
import java.util.Iterator;
import android.text.TextUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Looper;
import com.navdy.hud.app.ui.framework.UIStateManager;
import java.util.concurrent.LinkedBlockingDeque;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class ToastManager
{
    private static final ToastManager sInstance;
    private static final Logger sLogger;
    private Bus bus;
    private ToastInfo currentToast;
    private Handler handler;
    private boolean isToastDisplayDisabled;
    private boolean isToastScreenDisplayed;
    private Runnable nextToast;
    private LinkedBlockingDeque<ToastInfo> queue;
    private UIStateManager uiStateManager;
    
    static {
        sLogger = new Logger(ToastManager.class);
        sInstance = new ToastManager();
    }
    
    private ToastManager() {
        this.queue = new LinkedBlockingDeque<ToastInfo>();
        this.isToastDisplayDisabled = false;
        this.handler = new Handler(Looper.getMainLooper());
        this.nextToast = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    final LinkedBlockingDeque access$000 = ToastManager.this.queue;
                    Label_0064: {
                        synchronized (access$000) {
                            if (ToastManager.this.queue.size() == 0) {
                                ToastManager.sLogger.v("no pending toast");
                            }
                            else {
                                if (!ToastManager.this.isToastScreenDisplayed) {
                                    break Label_0064;
                                }
                                ToastManager.sLogger.v("cannot display toast screen still on");
                            }
                            // monitorexit(access$000)
                            return;
                        }
                    }
                    if (ToastManager.this.isToastDisplayDisabled) {
                        ToastManager.sLogger.v("toasts disabled");
                        // monitorexit(linkedBlockingDeque)
                        return;
                    }
                    final ToastInfo toastInfo = ToastManager.this.queue.remove();
                    ToastManager.this.currentToast = toastInfo;
                    ToastManager.this.showToast(toastInfo);
                }
                // monitorexit(linkedBlockingDeque)
            }
        };
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        this.bus = instance.getBus();
        this.uiStateManager = instance.getUiStateManager();
    }
    
    private void addToastInternal(final ToastParams toastParams) {
        if (toastParams.id == null || toastParams.data == null) {
            ToastManager.sLogger.w("invalid toast passed");
        }
        else if (this.uiStateManager.getRootScreen() == null) {
            ToastManager.sLogger.w("toast [" + toastParams.id + "] not accepted, Main screen not on");
        }
        else if (this.isLocked()) {
            ToastManager.sLogger.v("locked, cannot add toast[" + toastParams.id + "]");
        }
        else {
            ToastInfo currentToast;
            while (true) {
                toastParams.data.putString("id", toastParams.id);
                currentToast = new ToastInfo(toastParams);
                final LinkedBlockingDeque<ToastInfo> queue = this.queue;
                synchronized (queue) {
                    if (!toastParams.makeCurrent) {
                        break;
                    }
                    if (this.currentToast != null && !this.currentToast.dismissed) {
                        this.queue.addFirst(this.currentToast);
                        ToastManager.sLogger.v("replace current, item to front:" + this.currentToast.toastParams.id);
                        this.queue.addFirst(currentToast);
                        this.dismissCurrentToast();
                        return;
                    }
                }
                ToastManager.sLogger.v("replace current not reqd");
                this.queue.addFirst(currentToast);
                if (!this.isToastScreenDisplayed) {
                    ToastManager.sLogger.v("replace called next:run");
                    this.nextToast.run();
                }
                return;
            }
            if (this.queue.size() > 0 || this.isToastScreenDisplayed || this.currentToast != null) {
                final ToastParams toastParams2;
                ToastManager.sLogger.v("[" + toastParams2.id + "] wait for display to be cleared size[" + this.queue.size() + "] current[" + this.currentToast + "] displayed[" + this.isToastScreenDisplayed + "]");
                this.queue.add(currentToast);
            }
            // monitorexit(linkedBlockingDeque)
            else if (this.isToastDisplayDisabled) {
                final ToastParams toastParams2;
                ToastManager.sLogger.v("[" + toastParams2.id + "] toasts disabled");
                this.queue.add(currentToast);
            }
            // monitorexit(linkedBlockingDeque)
            else {
                this.currentToast = currentToast;
                ToastManager.sLogger.v("push toast to display");
                this.showToast(currentToast);
            }
            // monitorexit(linkedBlockingDeque)
        }
    }
    
    private void clearPendingToastInternal(final String s) {
        // monitorenter(queue = this.queue)
        Label_0071: {
            if (s != null) {
                break Label_0071;
            }
            try {
                ToastManager.sLogger.v("removed-all:" + this.queue.size());
                this.queue.clear();
                this.currentToast = null;
                this.isToastScreenDisplayed = false;
                Label_0068: {
                    return;
                }
                while (true) {
                    final Iterator<ToastInfo> iterator;
                    final String id = iterator.next().toastParams.id;
                    ToastManager.sLogger.v("removed:" + id);
                    iterator.remove();
                    continue;
                    iterator = this.queue.iterator();
                    continue;
                }
            }
            // iftrue(Label_0068:, !iterator.hasNext())
            // iftrue(Label_0080:, !TextUtils.equals((CharSequence)s, (CharSequence)id))
            finally {
            }
            // monitorexit(queue)
        }
    }
    
    private void dismissCurrentToast(final String s, final boolean b) {
        if (GenericUtil.isMainThread()) {
            this.dismissCurrentToastInternal(s, b);
        }
        else {
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    ToastManager.this.dismissCurrentToastInternal(s, b);
                }
            });
        }
    }
    
    private void dismissCurrentToastInternal(final String s, final boolean b) {
        if (this.currentToast != null) {
            if (s != null && !TextUtils.equals((CharSequence)s, (CharSequence)this.currentToast.toastParams.id)) {
                ToastManager.sLogger.v("current toast[" + this.currentToast.toastParams.id + "] does not match[" + s + "]");
            }
            else {
                if (b && this.currentToast.toastParams.removeOnDisable) {
                    this.currentToast = null;
                    ToastManager.sLogger.v("dismiss current toast[" + s + "] removed");
                }
                else {
                    this.currentToast.dismissed = true;
                    ToastManager.sLogger.v("dismiss current toast[" + s + "] saved");
                }
                ToastPresenter.dismiss();
            }
        }
    }
    
    public static ToastManager getInstance() {
        return ToastManager.sInstance;
    }
    
    private boolean isLocked() {
        return this.currentToast != null && this.currentToast.toastParams.lock;
    }
    
    private void showToast(final ToastInfo toastInfo) {
        ToastManager.sLogger.v("[show-toast] id[" + toastInfo.toastParams.id + "]");
        ToastPresenter.present(this.uiStateManager.getToastView(), toastInfo);
    }
    
    public void addToast(final ToastParams toastParams) {
        if (toastParams == null) {
            ToastManager.sLogger.w("invalid toast params");
        }
        else if (GenericUtil.isMainThread()) {
            this.addToastInternal(toastParams);
        }
        else {
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    ToastManager.this.addToastInternal(toastParams);
                }
            });
        }
    }
    
    public void clearAllPendingToast() {
        if (GenericUtil.isMainThread()) {
            this.clearPendingToastInternal(null);
        }
        else {
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    ToastManager.this.clearPendingToastInternal(null);
                }
            });
        }
    }
    
    public void clearPendingToast(final String s) {
        if (GenericUtil.isMainThread()) {
            this.clearPendingToastInternal(s);
        }
        else {
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    ToastManager.this.clearPendingToastInternal(s);
                }
            });
        }
    }
    
    public void clearPendingToast(final String... array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            this.clearPendingToast(array[i]);
        }
    }
    
    public void disableToasts(final boolean isToastDisplayDisabled) {
        ToastManager.sLogger.v("disableToast [" + isToastDisplayDisabled + "]");
        if (this.isToastDisplayDisabled == isToastDisplayDisabled) {
            ToastManager.sLogger.v("disableToast same state as before");
        }
        else {
            this.isToastDisplayDisabled = isToastDisplayDisabled;
            if (isToastDisplayDisabled) {
                if (this.isLocked()) {
                    ToastManager.sLogger.v("disableToast locked, cannot disable toast");
                }
                else {
                    this.dismissCurrentToast(true);
                }
            }
            else {
                while (true) {
                    final LinkedBlockingDeque<ToastInfo> queue = this.queue;
                    while (true) {
                        synchronized (queue) {
                            if (this.currentToast != null) {
                                this.showToast(this.currentToast);
                                break;
                            }
                        }
                        this.handler.post(this.nextToast);
                        continue;
                    }
                }
            }
        }
    }
    
    public void dismissCurrentToast() {
        this.dismissCurrentToast(this.getCurrentToastId(), false);
    }
    
    public void dismissCurrentToast(final String s) {
        this.dismissCurrentToast(s, false);
    }
    
    void dismissCurrentToast(final boolean b) {
        this.dismissCurrentToast(this.getCurrentToastId(), b);
    }
    
    public void dismissCurrentToast(final String... array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            this.dismissCurrentToast(array[i]);
        }
    }
    
    public void dismissToast(final String s) {
        this.dismissCurrentToast(s);
        this.clearPendingToast(s);
    }
    
    public IToastCallback getCallback(final String s) {
        IToastCallback cb;
        if (s == null || this.currentToast == null || !TextUtils.equals((CharSequence)s, (CharSequence)this.currentToast.toastParams.id)) {
            cb = null;
        }
        else {
            cb = this.currentToast.toastParams.cb;
        }
        return cb;
    }
    
    public String getCurrentToastId() {
        String id;
        if (this.currentToast == null) {
            id = null;
        }
        else {
            id = this.currentToast.toastParams.id;
        }
        return id;
    }
    
    public boolean isCurrentToast(final String s) {
        return s != null && this.currentToast != null && TextUtils.equals((CharSequence)this.currentToast.toastParams.id, (CharSequence)s);
    }
    
    public boolean isToastDisplayed() {
        return this.isToastScreenDisplayed;
    }
    
    public void setToastDisplayFlag(final boolean isToastScreenDisplayed) {
        if (!(this.isToastScreenDisplayed = isToastScreenDisplayed)) {
            this.currentToast = null;
            this.handler.removeCallbacks(this.nextToast);
            this.handler.post(this.nextToast);
        }
    }
    
    public static class DismissedToast
    {
        public String name;
        
        public DismissedToast(final String name) {
            this.name = name;
        }
    }
    
    public static class ShowToast
    {
        public String name;
        
        public ShowToast(final String name) {
            this.name = name;
        }
    }
    
    public static class ToastInfo
    {
        boolean dismissed;
        ToastParams toastParams;
        boolean ttsDone;
        
        ToastInfo(final ToastParams toastParams) {
            this.toastParams = toastParams;
        }
    }
    
    public static class ToastParams
    {
        IToastCallback cb;
        Bundle data;
        String id;
        boolean lock;
        boolean makeCurrent;
        boolean removeOnDisable;
        
        public ToastParams(final String s, final Bundle bundle, final IToastCallback toastCallback, final boolean b, final boolean b2) {
            this(s, bundle, toastCallback, b, b2, false);
        }
        
        public ToastParams(final String id, final Bundle data, final IToastCallback cb, final boolean removeOnDisable, final boolean makeCurrent, final boolean lock) {
            this.id = id;
            this.data = data;
            this.cb = cb;
            this.removeOnDisable = removeOnDisable;
            this.makeCurrent = makeCurrent;
            this.lock = lock;
        }
    }
}
