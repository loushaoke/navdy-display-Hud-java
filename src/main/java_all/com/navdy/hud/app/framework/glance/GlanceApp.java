package com.navdy.hud.app.framework.glance;

import com.navdy.hud.app.framework.notifications.NotificationType;

public enum GlanceApp
{
    APPLE_CALENDAR(GlanceConstants.colorAppleCalendar, R.drawable.icon_glance_calendar, -1, false, NotificationType.GLANCE), 
    APPLE_MAIL(GlanceConstants.colorAppleMail, R.drawable.icon_glance_email, -1, true, NotificationType.GLANCE), 
    FACEBOOK(GlanceConstants.colorFacebook, R.drawable.icon_glance_facebook, R.drawable.icon_glance_large_generic, false, NotificationType.GLANCE), 
    FACEBOOK_MESSENGER(GlanceConstants.colorFacebookMessenger, R.drawable.icon_glance_facebook_messenger, -1, true, NotificationType.GLANCE), 
    FUEL(GlanceConstants.colorFuelLevel, R.drawable.icon_car_alert, R.drawable.icon_glance_fuel_low, false, NotificationType.LOW_FUEL), 
    GENERIC(GlanceConstants.colorGeneric, R.drawable.icon_glance_generic, R.drawable.icon_glance_large_generic, false, NotificationType.GLANCE), 
    GENERIC_CALENDAR(GlanceConstants.colorAppleCalendar, R.drawable.icon_glance_calendar, -1, false, NotificationType.GLANCE), 
    GENERIC_MAIL(GlanceConstants.colorAppleMail, R.drawable.icon_glance_email, -1, true, NotificationType.GLANCE), 
    GENERIC_MESSAGE(GlanceConstants.colorSms, R.drawable.icon_message_blue, -1, true, NotificationType.GLANCE), 
    GENERIC_SOCIAL(GlanceConstants.colorTwitter, R.drawable.icon_social_blue, -1, true, NotificationType.GLANCE), 
    GOOGLE_CALENDAR(GlanceConstants.colorGoogleCalendar, R.drawable.icon_glance_google_calendar, -1, false, NotificationType.GLANCE), 
    GOOGLE_HANGOUT(GlanceConstants.colorGoogleHangout, R.drawable.icon_glance_hangout, -1, true, NotificationType.GLANCE), 
    GOOGLE_INBOX(GlanceConstants.colorGoogleInbox, R.drawable.icon_glance_google_inbox, -1, true, NotificationType.GLANCE), 
    GOOGLE_MAIL(GlanceConstants.colorGoogleMail, R.drawable.icon_glance_gmail, -1, true, NotificationType.GLANCE), 
    IMESSAGE(GlanceConstants.colorIMessage, R.drawable.icon_message_blue, -1, true, NotificationType.GLANCE), 
    SLACK(GlanceConstants.colorSlack, R.drawable.icon_glance_slack, -1, true, NotificationType.GLANCE), 
    SMS(GlanceConstants.colorSms, R.drawable.icon_message_blue, -1, true, NotificationType.GLANCE), 
    TWITTER(GlanceConstants.colorTwitter, R.drawable.icon_glance_twitter, -1, true, NotificationType.GLANCE), 
    WHATS_APP(GlanceConstants.colorWhatsapp, R.drawable.icon_glance_whatsapp, -1, true, NotificationType.GLANCE);
    
    int color;
    boolean defaultPhotoBasedOnId;
    int mainIcon;
    NotificationType notificationType;
    int sideIcon;
    
    private GlanceApp(final int color, final int sideIcon, final int mainIcon, final boolean defaultPhotoBasedOnId, final NotificationType notificationType) {
        this.color = color;
        this.sideIcon = sideIcon;
        this.mainIcon = mainIcon;
        this.defaultPhotoBasedOnId = defaultPhotoBasedOnId;
        this.notificationType = notificationType;
    }
    
    public int getColor() {
        return this.color;
    }
    
    public int getMainIcon() {
        return this.mainIcon;
    }
    
    public int getSideIcon() {
        return this.sideIcon;
    }
    
    public boolean isDefaultIconBasedOnId() {
        return this.defaultPhotoBasedOnId;
    }
}
