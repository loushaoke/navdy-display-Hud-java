package com.navdy.hud.app.framework.voice;

import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.audio.VoiceAssistResponse;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.service.library.events.audio.VoiceAssistRequest;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.manager.InputManager;
import android.animation.AnimatorSet;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.service.library.events.DeviceInfo;
import android.view.View;
import android.content.Context;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.input.MediaRemoteKeyEvent;
import com.navdy.service.library.events.input.KeyEvent;
import com.navdy.service.library.events.input.MediaRemoteKey;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import android.content.res.Resources;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import android.widget.TextView;
import android.os.Handler;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.navdy.hud.app.framework.notifications.INotification;

public class VoiceAssistNotification implements INotification
{
    private static final int DEFAULT_VOICE_ASSIST_TIMEOUT = 4000;
    private static final String EMPTY = "";
    private static final int SIRI_STARTING_TIMEOUT = 5000;
    private static final int SIRI_TIMEOUT = 200000;
    private static final Bus bus;
    private static int gnow_color;
    private static String google_now;
    private static final Logger sLogger;
    private static String siri;
    private static String siriIsActive;
    private static String siriIsStarting;
    private static int siri_color;
    private static String voice_assist_disabled;
    private static String voice_assist_na;
    private INotificationController controller;
    private FluctuatorAnimatorView fluctuator;
    private ImageView image;
    private ViewGroup layout;
    private Handler mSiriTimeoutHandler;
    private Runnable mSiriTimeoutRunnable;
    private TextView status;
    private TextView subStatus;
    private boolean waitingAfterTrigger;
    
    static {
        sLogger = new Logger(VoiceAssistNotification.class);
        final Resources resources = HudApplication.getAppContext().getResources();
        VoiceAssistNotification.google_now = resources.getString(R.string.google_now);
        VoiceAssistNotification.siri = resources.getString(R.string.siri);
        VoiceAssistNotification.voice_assist_na = resources.getString(R.string.voice_assist_not_available);
        VoiceAssistNotification.voice_assist_disabled = resources.getString(R.string.voice_assist_disabled);
        VoiceAssistNotification.siriIsActive = resources.getString(R.string.siri_active);
        VoiceAssistNotification.siriIsStarting = resources.getString(R.string.siri_starting);
        VoiceAssistNotification.gnow_color = resources.getColor(R.color.grey_fluctuator);
        VoiceAssistNotification.siri_color = resources.getColor(R.color.hud_white);
        bus = RemoteDeviceManager.getInstance().getBus();
    }
    
    public VoiceAssistNotification() {
        this.waitingAfterTrigger = false;
    }
    
    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification("navdy#voiceassist#notif");
    }
    
    private void sendSiriKeyEvents() {
        VoiceAssistNotification.sLogger.i("sending siri key events");
        VoiceAssistNotification.bus.post(new RemoteEvent(new MediaRemoteKeyEvent(MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI, KeyEvent.KEY_DOWN)));
        VoiceAssistNotification.bus.post(new RemoteEvent(new MediaRemoteKeyEvent(MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI, KeyEvent.KEY_UP)));
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return false;
    }
    
    @Override
    public boolean expandNotification() {
        return false;
    }
    
    @Override
    public int getColor() {
        return 0;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return "navdy#voiceassist#notif";
    }
    
    @Override
    public int getTimeout() {
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        final boolean remoteDeviceConnected = instance.isRemoteDeviceConnected();
        DeviceInfo.Platform remoteDevicePlatform;
        if (remoteDeviceConnected) {
            remoteDevicePlatform = instance.getRemoteDevicePlatform();
        }
        else {
            remoteDevicePlatform = null;
        }
        int n2;
        final int n = n2 = 4000;
        if (remoteDeviceConnected) {
            n2 = n;
            if (remoteDevicePlatform != null) {
                switch (remoteDevicePlatform) {
                    default:
                        n2 = n;
                        break;
                    case PLATFORM_iOS:
                        n2 = 200000;
                        break;
                }
            }
        }
        return n2;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.VOICE_ASSIST;
    }
    
    @Override
    public View getView(final Context context) {
        if (this.layout == null) {
            this.layout = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.notification_voice_assist, (ViewGroup)null);
            this.status = (TextView)this.layout.findViewById(R.id.status);
            this.subStatus = (TextView)this.layout.findViewById(R.id.subStatus);
            this.fluctuator = (FluctuatorAnimatorView)this.layout.findViewById(R.id.fluctuator);
            this.image = (ImageView)this.layout.findViewById(R.id.image);
        }
        return (View)this.layout;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    @Override
    public boolean isAlive() {
        return false;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }
    
    @Override
    public IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onClick() {
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Override
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = false;
        switch (customKeyEvent) {
            default:
                b = false;
                break;
            case LONG_PRESS:
                if (!this.waitingAfterTrigger) {
                    this.sendSiriKeyEvents();
                }
                b = true;
                break;
        }
        return b;
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        if (this.layout != null) {
            this.controller = controller;
            VoiceAssistNotification.bus.register(this);
            final int n = 0;
            int n2 = 0;
            final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
            final boolean remoteDeviceConnected = instance.isRemoteDeviceConnected();
            DeviceInfo.Platform remoteDevicePlatform;
            if (remoteDeviceConnected) {
                remoteDevicePlatform = instance.getRemoteDevicePlatform();
            }
            else {
                remoteDevicePlatform = null;
            }
            if (!remoteDeviceConnected || remoteDevicePlatform == null) {
                this.status.setText((CharSequence)VoiceAssistNotification.voice_assist_na);
                this.subStatus.setText((CharSequence)"");
                this.image.setImageResource(R.drawable.icon_voice);
            }
            else {
                switch (remoteDevicePlatform) {
                    default:
                        n2 = n;
                        break;
                    case PLATFORM_Android:
                        this.status.setText((CharSequence)VoiceAssistNotification.google_now);
                        this.subStatus.setText((CharSequence)"");
                        this.image.setImageResource(R.drawable.icon_googlenow_off);
                        n2 = VoiceAssistNotification.gnow_color;
                        break;
                    case PLATFORM_iOS:
                        n2 = VoiceAssistNotification.siri_color;
                        this.status.setText((CharSequence)VoiceAssistNotification.siri);
                        this.subStatus.setText((CharSequence)VoiceAssistNotification.siriIsStarting);
                        this.image.setImageResource(R.drawable.icon_siri);
                        break;
                }
                VoiceAssistNotification.sLogger.i("sending VoiceAssistRequest");
                VoiceAssistNotification.bus.post(new RemoteEvent(new VoiceAssistRequest(Boolean.valueOf(false))));
            }
            this.fluctuator.setFillColor(n2);
            this.fluctuator.setStrokeColor(n2);
            if (false) {
                this.fluctuator.setVisibility(View.VISIBLE);
                this.fluctuator.start();
            }
            else {
                this.fluctuator.setVisibility(INVISIBLE);
                this.fluctuator.stop();
            }
        }
    }
    
    @Override
    public void onStop() {
        this.controller = null;
        VoiceAssistNotification.bus.unregister(this);
        if (this.layout != null) {
            this.fluctuator.stop();
        }
        if (this.mSiriTimeoutHandler != null) {
            this.mSiriTimeoutHandler.removeCallbacks(this.mSiriTimeoutRunnable);
        }
        ToastManager.getInstance().disableToasts(false);
        VoiceAssistNotification.sLogger.v("startVoiceAssistance: enabled toast");
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public void onUpdate() {
    }
    
    @Subscribe
    public void onVoiceAssistResponse(final VoiceAssistResponse voiceAssistResponse) {
        if (this.controller != null && this.layout != null) {
            if (this.mSiriTimeoutHandler != null) {
                this.mSiriTimeoutHandler.removeCallbacks(this.mSiriTimeoutRunnable);
            }
            final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
            final boolean remoteDeviceConnected = instance.isRemoteDeviceConnected();
            DeviceInfo.Platform remoteDevicePlatform;
            if (remoteDeviceConnected) {
                remoteDevicePlatform = instance.getRemoteDevicePlatform();
            }
            else {
                remoteDevicePlatform = null;
            }
            final String s = "";
            String text = "";
            final boolean b = false;
            int imageResource = 0;
            String text2 = null;
            int n = 0;
            if (!remoteDeviceConnected || remoteDevicePlatform == null) {
                text2 = VoiceAssistNotification.voice_assist_na;
                imageResource = R.drawable.icon_voice;
                this.fluctuator.stop();
                this.fluctuator.setVisibility(INVISIBLE);
                n = (b ? 1 : 0);
            }
            else {
                VoiceAssistNotification.sLogger.i("received response " + voiceAssistResponse.state);
                switch (voiceAssistResponse.state) {
                    default:
                        n = (b ? 1 : 0);
                        text2 = s;
                        break;
                    case VOICE_ASSIST_AVAILABLE:
                        n = (b ? 1 : 0);
                        text2 = s;
                        if (remoteDevicePlatform == DeviceInfo.Platform.PLATFORM_iOS) {
                            text2 = VoiceAssistNotification.siri;
                            text = VoiceAssistNotification.siriIsStarting;
                            imageResource = R.drawable.icon_siri;
                            this.waitingAfterTrigger = true;
                            this.sendSiriKeyEvents();
                            if (this.mSiriTimeoutHandler == null) {
                                this.mSiriTimeoutHandler = new Handler();
                            }
                            this.mSiriTimeoutRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    if (VoiceAssistNotification.this.layout != null) {
                                        VoiceAssistNotification.sLogger.d("Siri request did not get any response and timed out");
                                        VoiceAssistNotification.this.fluctuator.setVisibility(INVISIBLE);
                                        VoiceAssistNotification.this.fluctuator.stop();
                                        VoiceAssistNotification.this.subStatus.setText((CharSequence)VoiceAssistNotification.voice_assist_disabled);
                                        VoiceAssistNotification.this.image.setImageResource(R.drawable.icon_siri);
                                    }
                                }
                            };
                            this.mSiriTimeoutHandler.postDelayed(this.mSiriTimeoutRunnable, 5000L);
                            n = (b ? 1 : 0);
                            break;
                        }
                        break;
                    case VOICE_ASSIST_NOT_AVAILABLE:
                        if (remoteDevicePlatform == DeviceInfo.Platform.PLATFORM_Android) {
                            text2 = VoiceAssistNotification.google_now;
                            text = VoiceAssistNotification.voice_assist_disabled;
                            imageResource = R.drawable.icon_googlenow_off;
                        }
                        else {
                            text2 = VoiceAssistNotification.siri;
                            text = VoiceAssistNotification.voice_assist_disabled;
                            imageResource = R.drawable.icon_siri;
                        }
                        this.fluctuator.setVisibility(INVISIBLE);
                        this.fluctuator.stop();
                        this.waitingAfterTrigger = false;
                        n = (b ? 1 : 0);
                        break;
                    case VOICE_ASSIST_STOPPED:
                        this.fluctuator.setVisibility(INVISIBLE);
                        this.fluctuator.stop();
                        this.dismissNotification();
                        this.waitingAfterTrigger = false;
                        return;
                    case VOICE_ASSIST_LISTENING:
                        if (remoteDevicePlatform == DeviceInfo.Platform.PLATFORM_Android) {
                            text2 = VoiceAssistNotification.google_now;
                            imageResource = R.drawable.icon_googlenow_on;
                            n = 1;
                            break;
                        }
                        text2 = VoiceAssistNotification.siri;
                        text = VoiceAssistNotification.siriIsActive;
                        imageResource = R.drawable.icon_siri;
                        n = 1;
                        if (this.waitingAfterTrigger) {
                            this.waitingAfterTrigger = false;
                            break;
                        }
                        this.sendSiriKeyEvents();
                        break;
                }
            }
            this.status.setText((CharSequence)text2);
            this.subStatus.setText((CharSequence)text);
            this.image.setImageResource(imageResource);
            if (n != 0) {
                this.fluctuator.setVisibility(View.VISIBLE);
                this.fluctuator.start();
            }
            else {
                this.fluctuator.setVisibility(INVISIBLE);
                this.fluctuator.stop();
            }
        }
    }
    
    @Override
    public boolean supportScroll() {
        return false;
    }
}
