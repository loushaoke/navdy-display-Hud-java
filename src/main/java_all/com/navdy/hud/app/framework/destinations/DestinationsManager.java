package com.navdy.hud.app.framework.destinations;

import com.squareup.wire.ProtoEnum;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.events.places.PlaceType;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import java.util.Collection;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.util.FeatureUtil;
import android.support.annotation.NonNull;
import com.navdy.service.library.events.destination.RecommendedDestinationsRequest;
import com.navdy.service.library.events.FavoriteDestinationsRequest;
import java.util.Iterator;
import java.util.ArrayList;
import com.navdy.service.library.events.MessageStore;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.hud.app.util.RemoteCapabilitiesUtil;
import com.navdy.hud.app.util.DateUtil;
import java.util.Date;
import java.util.UUID;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.destination.RecommendedDestinationsUpdate;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.places.SuggestedDestination;
import android.content.res.Resources;
import com.navdy.service.library.events.places.FavoriteDestinationsUpdate;
import java.util.List;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class DestinationsManager
{
    private static final FavoriteDestinationsChanged FAVORITE_DESTINATIONS_CHANGED;
    public static final String FAVORITE_DESTINATIONS_FILENAME = "favoriteDestinations.pb";
    private static final char[] INITIALS_ARRAY;
    public static final int MAX_DESTINATIONS = 30;
    private static final int MAX_INITIALS = 4;
    private static final RecentDestinationsChanged RECENT_DESTINATIONS_CHANGED;
    private static final SuggestedDestinationsChanged SUGGESTED_DESTINATIONS_CHANGED;
    private static final DestinationsManager sInstance;
    private static final Logger sLogger;
    private final Bus bus;
    private volatile List<Destination> favoriteDestinations;
    private FavoriteDestinationsUpdate lastUpdate;
    private volatile List<Destination> recentDestinations;
    Resources resources;
    private SuggestedDestination suggestedDestination;
    private volatile List<Destination> suggestedDestinations;
    
    static {
        sLogger = new Logger(DestinationsManager.class);
        FAVORITE_DESTINATIONS_CHANGED = new FavoriteDestinationsChanged();
        RECENT_DESTINATIONS_CHANGED = new RecentDestinationsChanged();
        SUGGESTED_DESTINATIONS_CHANGED = new SuggestedDestinationsChanged();
        INITIALS_ARRAY = new char[4];
        sInstance = new DestinationsManager();
    }
    
    private DestinationsManager() {
        this.resources = HudApplication.getAppContext().getResources();
        (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
    }
    
    private void buildDestinations() {
        if (GenericUtil.isMainThread()) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    DestinationsManager.this.load();
                }
            }, 10);
        }
        else {
            this.load();
        }
    }
    
    private void clearDestinations() {
        this.lastUpdate = null;
        this.setFavoriteDestinations(null);
        this.setRecentDestinations(null);
        this.setSuggestedDestinations(null);
    }
    
    private static String getDestinationInitials(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //     4: ifeq            11
        //     7: aconst_null    
        //     8: astore_0       
        //     9: aload_0        
        //    10: areturn        
        //    11: aload_0        
        //    12: invokestatic    com/navdy/hud/app/util/GenericUtil.removePunctuation:(Ljava/lang/String;)Ljava/lang/String;
        //    15: invokevirtual   java/lang/String.toUpperCase:()Ljava/lang/String;
        //    18: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //    21: astore_1       
        //    22: aconst_null    
        //    23: astore_0       
        //    24: iconst_0       
        //    25: istore_2       
        //    26: aload_1        
        //    27: ldc             " "
        //    29: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //    32: istore_3       
        //    33: iload_3        
        //    34: iflt            47
        //    37: aload_1        
        //    38: iconst_0       
        //    39: iload_3        
        //    40: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //    43: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //    46: astore_0       
        //    47: aload_0        
        //    48: ifnull          97
        //    51: aload_0        
        //    52: astore          4
        //    54: aload           4
        //    56: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    59: pop            
        //    60: iconst_1       
        //    61: istore_3       
        //    62: iload_3        
        //    63: istore_2       
        //    64: aload_0        
        //    65: astore          4
        //    67: aload_0        
        //    68: ifnonnull       76
        //    71: aload_1        
        //    72: astore          4
        //    74: iload_3        
        //    75: istore_2       
        //    76: iload_2        
        //    77: ifeq            103
        //    80: aload           4
        //    82: astore_0       
        //    83: aload           4
        //    85: invokevirtual   java/lang/String.length:()I
        //    88: iconst_4       
        //    89: if_icmple       9
        //    92: aconst_null    
        //    93: astore_0       
        //    94: goto            9
        //    97: aload_1        
        //    98: astore          4
        //   100: goto            54
        //   103: aload           4
        //   105: ifnonnull       118
        //   108: aload_1        
        //   109: iconst_0       
        //   110: iconst_1       
        //   111: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   114: astore_0       
        //   115: goto            9
        //   118: getstatic       com/navdy/hud/app/framework/destinations/DestinationsManager.INITIALS_ARRAY:[C
        //   121: astore          4
        //   123: aload           4
        //   125: dup            
        //   126: astore          5
        //   128: monitorenter   
        //   129: new             Ljava/util/StringTokenizer;
        //   132: astore_0       
        //   133: aload_0        
        //   134: aload_1        
        //   135: ldc             " "
        //   137: invokespecial   java/util/StringTokenizer.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   140: iconst_0       
        //   141: istore_2       
        //   142: aload_0        
        //   143: invokevirtual   java/util/StringTokenizer.hasMoreElements:()Z
        //   146: ifeq            200
        //   149: aload_0        
        //   150: invokevirtual   java/util/StringTokenizer.nextElement:()Ljava/lang/Object;
        //   153: checkcast       Ljava/lang/String;
        //   156: astore          6
        //   158: getstatic       com/navdy/hud/app/framework/destinations/DestinationsManager.INITIALS_ARRAY:[C
        //   161: astore_1       
        //   162: iload_2        
        //   163: iconst_1       
        //   164: iadd           
        //   165: istore_3       
        //   166: aload_1        
        //   167: iload_2        
        //   168: aload           6
        //   170: iconst_0       
        //   171: invokevirtual   java/lang/String.charAt:(I)C
        //   174: castore        
        //   175: iload_3        
        //   176: iconst_4       
        //   177: if_icmpne       195
        //   180: aload_0        
        //   181: invokevirtual   java/util/StringTokenizer.hasMoreElements:()Z
        //   184: ifeq            195
        //   187: aload           5
        //   189: monitorexit    
        //   190: aconst_null    
        //   191: astore_0       
        //   192: goto            9
        //   195: iload_3        
        //   196: istore_2       
        //   197: goto            142
        //   200: new             Ljava/lang/String;
        //   203: astore_0       
        //   204: aload_0        
        //   205: getstatic       com/navdy/hud/app/framework/destinations/DestinationsManager.INITIALS_ARRAY:[C
        //   208: iconst_0       
        //   209: iload_2        
        //   210: invokespecial   java/lang/String.<init>:([CII)V
        //   213: aload           5
        //   215: monitorexit    
        //   216: goto            9
        //   219: astore_0       
        //   220: aload           5
        //   222: monitorexit    
        //   223: aload_0        
        //   224: athrow         
        //   225: astore_0       
        //   226: getstatic       com/navdy/hud/app/framework/destinations/DestinationsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   229: aload_0        
        //   230: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   233: aconst_null    
        //   234: astore_0       
        //   235: goto            9
        //   238: astore          4
        //   240: aload_0        
        //   241: astore          4
        //   243: goto            76
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  0      7      225    238    Ljava/lang/Throwable;
        //  11     22     225    238    Ljava/lang/Throwable;
        //  26     33     225    238    Ljava/lang/Throwable;
        //  37     47     225    238    Ljava/lang/Throwable;
        //  54     60     238    246    Ljava/lang/Throwable;
        //  83     92     225    238    Ljava/lang/Throwable;
        //  108    115    225    238    Ljava/lang/Throwable;
        //  118    129    225    238    Ljava/lang/Throwable;
        //  129    140    219    225    Any
        //  142    162    219    225    Any
        //  166    175    219    225    Any
        //  180    190    219    225    Any
        //  200    216    219    225    Any
        //  220    223    219    225    Any
        //  223    225    225    238    Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0054:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private com.navdy.service.library.events.destination.Destination.FavoriteType getDestinationType(final Destination.FavoriteDestinationType favoriteDestinationType) {
        com.navdy.service.library.events.destination.Destination.FavoriteType favoriteType = null;
        switch (favoriteDestinationType) {
            default:
                favoriteType = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE;
                break;
            case FAVORITE_HOME:
                favoriteType = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_HOME;
                break;
            case FAVORITE_WORK:
                favoriteType = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_WORK;
                break;
            case FAVORITE_CUSTOM:
                favoriteType = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CUSTOM;
                break;
        }
        return favoriteType;
    }
    
    public static String getInitials(String s, final Destination.FavoriteDestinationType favoriteDestinationType) {
        if (favoriteDestinationType != null) {
            switch (favoriteDestinationType) {
                case FAVORITE_NONE:
                case FAVORITE_CUSTOM:
                    s = getDestinationInitials(s);
                    return s;
                case FAVORITE_CONTACT:
                    s = ContactUtil.getInitials(s);
                    return s;
            }
        }
        s = null;
        return s;
    }
    
    public static DestinationsManager getInstance() {
        return DestinationsManager.sInstance;
    }
    
    public static NavigationRouteRequest getNavigationRouteRequestForDestination(final com.navdy.service.library.events.destination.Destination destination) {
        Coordinate build = null;
        Coordinate coordinate;
        if (destination.navigation_position == null || (destination.navigation_position.latitude == 0.0 && destination.navigation_position.longitude == 0.0)) {
            coordinate = new Coordinate.Builder().latitude(destination.display_position.latitude).longitude(destination.display_position.longitude).build();
        }
        else {
            coordinate = new Coordinate.Builder().latitude(destination.display_position.latitude).longitude(destination.display_position.longitude).build();
            build = new Coordinate.Builder().latitude(destination.navigation_position.latitude).longitude(destination.navigation_position.longitude).build();
        }
        final NavigationRouteRequest.Builder requestId = new NavigationRouteRequest.Builder().destination(coordinate).label(destination.destination_title).streetAddress(destination.full_address).destination_identifier(destination.identifier).originDisplay(true).geoCodeStreetAddress(false).autoNavigate(true).requestId(UUID.randomUUID().toString());
        if (build != null) {
            requestId.destinationDisplay(build);
        }
        return requestId.build();
    }
    
    private Destination.PlaceCategory getPlaceCategory(final com.navdy.service.library.events.destination.Destination destination) {
        boolean b = false;
        if (Boolean.TRUE.equals(destination.is_recommendation)) {
            b = true;
        }
        boolean b3;
        final boolean b2 = b3 = false;
        if (destination.suggestion_type != null) {
            b3 = b2;
            if (destination.suggestion_type == com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT) {
                b3 = true;
            }
        }
        Enum<Destination.PlaceCategory> enum1;
        if (b && b3) {
            enum1 = Destination.PlaceCategory.SUGGESTED_RECENT;
        }
        else if (b) {
            enum1 = Destination.PlaceCategory.SUGGESTED;
        }
        else if (b3) {
            enum1 = Destination.PlaceCategory.RECENT;
        }
        else {
            enum1 = null;
        }
        return (Destination.PlaceCategory)enum1;
    }
    
    private String getRecentTimeLabel(final com.navdy.service.library.events.destination.Destination destination) {
        String dateLabel;
        final String s = dateLabel = null;
        if (destination.suggestion_type != null) {
            dateLabel = s;
            if (destination.suggestion_type == com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT) {
                dateLabel = s;
                if (destination.last_navigated_to != null) {
                    dateLabel = s;
                    if (destination.last_navigated_to > 0L) {
                        dateLabel = DateUtil.getDateLabel(new Date(destination.last_navigated_to));
                    }
                }
            }
        }
        return dateLabel;
    }
    
    private int getSuggestionLabelColor(final com.navdy.service.library.events.destination.Destination destination, final Destination.FavoriteDestinationType favoriteDestinationType, final Destination.PlaceCategory placeCategory) {
        int n = 0;
        if (Boolean.TRUE.equals(destination.is_recommendation)) {
            final Resources resources = HudApplication.getAppContext().getResources();
            if (placeCategory == Destination.PlaceCategory.SUGGESTED_RECENT) {
                n = resources.getColor(R.color.suggested_dest_work);
            }
            else {
                switch (favoriteDestinationType) {
                    default:
                        n = resources.getColor(R.color.suggested_dest_fav);
                        break;
                    case FAVORITE_HOME:
                        n = resources.getColor(R.color.suggested_dest_home);
                        break;
                    case FAVORITE_WORK:
                        n = resources.getColor(R.color.suggested_dest_work);
                        break;
                    case FAVORITE_CONTACT:
                        n = resources.getColor(R.color.suggested_dest_contact);
                        break;
                    case FAVORITE_CALENDAR:
                        n = resources.getColor(R.color.suggested_dest_cal);
                        break;
                }
            }
        }
        else {
            n = -1;
        }
        return n;
    }
    
    private void handleDefaultDestination(final Destination destination, final boolean b) {
        DestinationsManager.sLogger.v("handleDefaultDestination [" + destination.toString() + "] lookup=" + b);
        if (b) {
            if (RemoteCapabilitiesUtil.supportsNavigationCoordinateLookup()) {
                if (destination.navigationPositionLatitude == 0.0 || destination.navigationPositionLongitude == 0.0) {
                    DestinationsManager.sLogger.v("handleDefaultDestination do nav lookup");
                    HereRouteManager.startNavigationLookup(destination);
                    return;
                }
                DestinationsManager.sLogger.v("handleDefaultDestination nav coordinate exists already");
            }
            else {
                DestinationsManager.sLogger.v("handleDefaultDestination client does not support nav lookup");
            }
        }
        Coordinate build = null;
        Coordinate coordinate;
        if (destination.navigationPositionLatitude == 0.0 && destination.navigationPositionLongitude == 0.0) {
            coordinate = new Coordinate.Builder().latitude(destination.displayPositionLatitude).longitude(destination.displayPositionLongitude).build();
        }
        else {
            coordinate = new Coordinate.Builder().latitude(destination.navigationPositionLatitude).longitude(destination.navigationPositionLongitude).build();
            build = new Coordinate.Builder().latitude(destination.displayPositionLatitude).longitude(destination.displayPositionLongitude).build();
        }
        final NavigationRouteRequest.Builder requestId = new NavigationRouteRequest.Builder().destination(coordinate).label(destination.destinationTitle).streetAddress(destination.fullAddress).destination_identifier(destination.identifier).requestDestination(this.transformToProtoDestination(destination)).originDisplay(true).geoCodeStreetAddress(false).destinationType(this.getDestinationType(destination.favoriteDestinationType)).requestId(UUID.randomUUID().toString());
        if (build != null) {
            requestId.destinationDisplay(build);
        }
        final NavigationRouteRequest build2 = requestId.build();
        DestinationsManager.sLogger.v("launched navigation route request");
        this.bus.post(new RemoteEvent(build2));
        this.bus.post(build2);
    }
    
    private void handleFindGasDestination(final Destination destination) {
        NotificationManager.getInstance().removeNotification(FuelRoutingManager.LOW_FUEL_ID);
        final FuelRoutingManager instance = FuelRoutingManager.getInstance();
        instance.showFindingGasStationToast();
        instance.findGasStations(-1, false);
    }
    
    private void load() {
        try {
            this.parseDestinations(new MessageStore(DriverProfileHelper.getInstance().getCurrentProfile().getPreferencesDirectory()).<FavoriteDestinationsUpdate>readMessage("favoriteDestinations.pb", FavoriteDestinationsUpdate.class));
        }
        catch (Throwable t) {
            this.setFavoriteDestinations(null);
            DestinationsManager.sLogger.e(t);
        }
    }
    
    private void parseDestinations(final RecommendedDestinationsUpdate recommendedDestinationsUpdate) {
        DestinationsManager.sLogger.v("parsing RecommendedDestinationsUpdate");
        if (recommendedDestinationsUpdate.destinations != null) {
            final List<Destination> transform = this.transform(recommendedDestinationsUpdate.destinations);
            final ArrayList<Destination> recentDestinations = new ArrayList<Destination>();
            final ArrayList<Destination> suggestedDestinations = new ArrayList<Destination>();
            if (transform.size() > 0) {
                for (final Destination destination : transform) {
                    if (destination.recommendation) {
                        suggestedDestinations.add(destination);
                    }
                    else {
                        recentDestinations.add(destination);
                    }
                }
            }
            this.setRecentDestinations(recentDestinations);
            this.setSuggestedDestinations(suggestedDestinations);
        }
        else {
            DestinationsManager.sLogger.w("rec-destination list returned is null");
        }
    }
    
    private void parseDestinations(final FavoriteDestinationsUpdate lastUpdate) {
        DestinationsManager.sLogger.v("parsing FavoriteDestinationsUpdate");
        this.lastUpdate = lastUpdate;
        final List<Destination> list = null;
        List<Destination> transform;
        if (lastUpdate != null && lastUpdate.destinations != null) {
            new MessageStore(DriverProfileHelper.getInstance().getCurrentProfile().getPreferencesDirectory()).writeMessage(lastUpdate, "favoriteDestinations.pb");
            transform = this.transform(lastUpdate.destinations);
        }
        else {
            DestinationsManager.sLogger.w("fav-destination list returned is null");
            transform = list;
        }
        this.setFavoriteDestinations(transform);
    }
    
    private void requestDestinations() {
        GenericUtil.checkNotOnMainThread();
        long longValue;
        if (this.lastUpdate != null) {
            longValue = this.lastUpdate.serial_number;
        }
        else {
            longValue = 0L;
        }
        this.bus.post(new RemoteEvent(new FavoriteDestinationsRequest(Long.valueOf(longValue))));
        this.bus.post(new RemoteEvent(new RecommendedDestinationsRequest(Long.valueOf(0L))));
        DestinationsManager.sLogger.v("sent favorite destinations request with current version=" + longValue);
        DestinationsManager.sLogger.v("sent recommended destinations request with current version");
    }
    
    private void setFavoriteDestinations(final List<Destination> favoriteDestinations) {
        if (favoriteDestinations != this.favoriteDestinations) {
            this.favoriteDestinations = favoriteDestinations;
            this.bus.post(DestinationsManager.FAVORITE_DESTINATIONS_CHANGED);
        }
    }
    
    @NonNull
    private List<Destination> transform(final List<com.navdy.service.library.events.destination.Destination> list) {
        final ArrayList<Destination> list2 = new ArrayList<Destination>();
        final Iterator<com.navdy.service.library.events.destination.Destination> iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(this.transformToInternalDestination(iterator.next()));
            if (list2.size() == 30) {
                DestinationsManager.sLogger.v("exceeded max size");
                break;
            }
        }
        return list2;
    }
    
    public void clearSuggestedDestination() {
        DestinationsManager.sLogger.v("clearSuggestedDestination");
        DestinationSuggestionToast.dismissSuggestionToast();
        this.suggestedDestination = null;
    }
    
    public List<Destination> getFavoriteDestinations() {
        return this.favoriteDestinations;
    }
    
    public GasDestination getGasDestination() {
        GasDestination gasDestination2;
        final GasDestination gasDestination = gasDestination2 = null;
        if (RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(FeatureUtil.Feature.FUEL_ROUTING)) {
            gasDestination2 = gasDestination;
            if (FuelRoutingManager.getInstance().isAvailable()) {
                final FuelRoutingManager instance = FuelRoutingManager.getInstance();
                if (instance.isBusy()) {
                    DestinationsManager.sLogger.v("fuel route manager is busy");
                    gasDestination2 = gasDestination;
                }
                else {
                    DestinationsManager.sLogger.v("fuel route manager is not busy");
                    final boolean b = false;
                    final ObdManager instance2 = ObdManager.getInstance();
                    boolean b2 = b;
                    if (instance2.isConnected()) {
                        b2 = b;
                        if (instance2.getFuelLevel() != -1) {
                            b2 = true;
                        }
                    }
                    final Destination gasDestination3 = Destination.getGasDestination();
                    int n;
                    if (!b2) {
                        n = 0;
                    }
                    else if (instance.getFuelGlanceDismissTime() > 0L) {
                        n = 1;
                    }
                    else {
                        n = 0;
                    }
                    gasDestination2 = new GasDestination();
                    gasDestination2.destination = gasDestination3;
                    if (n != 0) {
                        gasDestination2.showFirst = true;
                    }
                    else {
                        gasDestination2.showFirst = false;
                    }
                }
            }
        }
        return gasDestination2;
    }
    
    public List<Destination> getRecentDestinations() {
        return this.recentDestinations;
    }
    
    public List<Destination> getSuggestedDestinations() {
        final ArrayList<Object> list = (ArrayList<Object>)new ArrayList<Destination>();
        if (this.suggestedDestinations != null) {
            list.addAll(this.suggestedDestinations);
        }
        return (List<Destination>)list;
    }
    
    public void goToSuggestedDestination() {
        if (this.suggestedDestination == null) {
            DestinationsManager.sLogger.e("gotToSuggestedDestination : Suggested destination is null, cannot to navigate");
        }
        else {
            final com.navdy.service.library.events.destination.Destination destination = this.suggestedDestination.destination;
            if (destination == null) {
                DestinationsManager.sLogger.e("gotToSuggestedDestination : Destination in Suggested destination is null, cannot to navigate");
            }
            else if (HereNavigationManager.getInstance().isNavigationModeOn()) {
                DestinationsManager.sLogger.e("gotToSuggestedDestination: Cannot navigate as the user is already navigating");
            }
            else {
                final NavigationRouteRequest navigationRouteRequestForDestination = getNavigationRouteRequestForDestination(destination);
                DestinationsManager.sLogger.v("launched navigation route request");
                this.bus.post(new RemoteEvent(navigationRouteRequestForDestination));
                this.bus.post(navigationRouteRequestForDestination);
            }
        }
    }
    
    public boolean launchSuggestedDestination() {
        boolean b;
        if (this.suggestedDestination != null) {
            DestinationsManager.sLogger.v("launchSuggestedDestination: suggested destination toast");
            DestinationSuggestionToast.showSuggestion(this.suggestedDestination);
            b = true;
        }
        else {
            DestinationsManager.sLogger.v("launchSuggestedDestination: no suggested destination available");
            b = false;
        }
        return b;
    }
    
    @Subscribe
    public void onConnectionStatusChange(final ConnectionStateChange connectionStateChange) {
        if (connectionStateChange.state == ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
            this.clearDestinations();
        }
    }
    
    @Subscribe
    public void onDestinationSuggestion(final SuggestedDestination suggestedDestination) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                DestinationsManager.sLogger.v("onDestinationSuggestion:" + suggestedDestination);
                final HereMapsManager instance = HereMapsManager.getInstance();
                if (!instance.isInitialized()) {
                    DestinationsManager.sLogger.e("onDestinationSuggestion: Map engine not initialized, cannot process suggestion, at this time");
                    DestinationsManager.this.suggestedDestination = suggestedDestination;
                }
                else if (HereNavigationManager.getInstance().isNavigationModeOn()) {
                    DestinationsManager.sLogger.e("onDestinationSuggestion: nav mode is on");
                }
                else if (RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView() != null && HereRouteManager.isUIShowingRouteCalculation()) {
                    DestinationsManager.sLogger.e("onDestinationSuggestion: route calc is on");
                }
                else {
                    DestinationsManager.this.suggestedDestination = suggestedDestination;
                    if (HereMapUtil.getSavedRouteData(DestinationsManager.sLogger).navigationRouteRequest != null) {
                        DestinationsManager.sLogger.e("onDestinationSuggestion: has saved route data");
                    }
                    else if (instance.getLocationFixManager().getLastGeoCoordinate() == null) {
                        DestinationsManager.sLogger.e("onDestinationSuggestion: no current position");
                    }
                    else {
                        DestinationsManager.this.launchSuggestedDestination();
                    }
                }
            }
        }, 3);
    }
    
    @Subscribe
    public void onDeviceSyncRequired(final ConnectionHandler.DeviceSyncEvent deviceSyncEvent) {
        DestinationsManager.sLogger.v("syncDestinations");
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                DestinationsManager.this.requestDestinations();
            }
        }, 10);
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        this.buildDestinations();
    }
    
    @Subscribe
    public void onFavoriteDestinationsUpdate(final FavoriteDestinationsUpdate favoriteDestinationsUpdate) {
        DestinationsManager.sLogger.v("received FavoriteDestinationsUpdate: " + favoriteDestinationsUpdate);
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Label_0092: {
                        try {
                            if (favoriteDestinationsUpdate.status == RequestStatus.REQUEST_SUCCESS) {
                                DestinationsManager.this.parseDestinations(favoriteDestinationsUpdate);
                            }
                            else {
                                if (favoriteDestinationsUpdate.status != RequestStatus.REQUEST_VERSION_IS_CURRENT) {
                                    break Label_0092;
                                }
                                DestinationsManager.sLogger.v("fav-destination response: version" + favoriteDestinationsUpdate.serial_number + " is current");
                            }
                            return;
                        }
                        catch (Throwable t) {
                            DestinationsManager.sLogger.e(t);
                            return;
                        }
                    }
                    DestinationsManager.sLogger.e("sent fav-destination response error: " + favoriteDestinationsUpdate.status);
                }
            }
        }, 1);
    }
    
    @Subscribe
    public void onRecommendedDestinationsUpdate(final RecommendedDestinationsUpdate recommendedDestinationsUpdate) {
        DestinationsManager.sLogger.v("received RecommendedDestinationsUpdate:" + recommendedDestinationsUpdate);
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Label_0092: {
                        try {
                            if (recommendedDestinationsUpdate.status == RequestStatus.REQUEST_SUCCESS) {
                                DestinationsManager.this.parseDestinations(recommendedDestinationsUpdate);
                            }
                            else {
                                if (recommendedDestinationsUpdate.status != RequestStatus.REQUEST_VERSION_IS_CURRENT) {
                                    break Label_0092;
                                }
                                DestinationsManager.sLogger.v("fav-destination response: version" + recommendedDestinationsUpdate.serial_number + " is current");
                            }
                            return;
                        }
                        catch (Throwable t) {
                            DestinationsManager.sLogger.e(t);
                            return;
                        }
                    }
                    DestinationsManager.sLogger.e("sent fav-destination response error: " + recommendedDestinationsUpdate.status);
                }
            }
        }, 1);
    }
    
    public void requestNavigation(final Destination destination) {
        switch (destination.destinationType) {
            case DEFAULT:
                this.handleDefaultDestination(destination, false);
                break;
            case FIND_GAS:
                this.handleFindGasDestination(destination);
                break;
        }
    }
    
    public void requestNavigationWithNavLookup(final Destination destination) {
        this.handleDefaultDestination(destination, true);
    }
    
    public void setRecentDestinations(final List<Destination> recentDestinations) {
        this.recentDestinations = recentDestinations;
        this.bus.post(DestinationsManager.RECENT_DESTINATIONS_CHANGED);
    }
    
    public void setSuggestedDestinations(final List<Destination> suggestedDestinations) {
        this.suggestedDestinations = suggestedDestinations;
        this.bus.post(DestinationsManager.SUGGESTED_DESTINATIONS_CHANGED);
    }
    
    public Destination transformToInternalDestination(final com.navdy.service.library.events.destination.Destination destination) {
        Destination destination2;
        if (destination == null) {
            destination2 = null;
        }
        else {
            boolean equals = Boolean.TRUE.equals(destination.is_recommendation);
            int n = -1;
            Destination.FavoriteDestinationType favoriteDestinationType;
            if (destination.suggestion_type != null && destination.suggestion_type == com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_CALENDAR) {
                equals = true;
                n = this.resources.getColor(R.color.suggested_dest_cal);
                favoriteDestinationType = Destination.FavoriteDestinationType.FAVORITE_CALENDAR;
            }
            else {
                favoriteDestinationType = Destination.FavoriteDestinationType.buildFromValue(destination.favorite_type.getValue());
            }
            final String initials = getInitials(destination.destination_title, favoriteDestinationType);
            String recentTimeLabel = null;
            final Destination.PlaceCategory placeCategory = this.getPlaceCategory(destination);
            if (placeCategory == Destination.PlaceCategory.RECENT) {
                recentTimeLabel = this.getRecentTimeLabel(destination);
            }
            if (equals) {
                n = this.getSuggestionLabelColor(destination, favoriteDestinationType, placeCategory);
            }
            double doubleValue;
            if (destination.navigation_position != null) {
                doubleValue = destination.navigation_position.latitude;
            }
            else {
                doubleValue = 0.0;
            }
            double doubleValue2;
            if (destination.navigation_position != null) {
                doubleValue2 = destination.navigation_position.longitude;
            }
            else {
                doubleValue2 = 0.0;
            }
            double doubleValue3;
            if (destination.display_position != null) {
                doubleValue3 = destination.display_position.latitude;
            }
            else {
                doubleValue3 = 0.0;
            }
            double doubleValue4;
            if (destination.display_position != null) {
                doubleValue4 = destination.display_position.longitude;
            }
            else {
                doubleValue4 = 0.0;
            }
            final String full_address = destination.full_address;
            final String destination_title = destination.destination_title;
            final String destination_subtitle = destination.destination_subtitle;
            final String identifier = destination.identifier;
            final Destination.DestinationType default1 = Destination.DestinationType.DEFAULT;
            int intValue;
            if (destination.destinationIcon != null) {
                intValue = destination.destinationIcon;
            }
            else {
                intValue = 0;
            }
            int intValue2;
            if (destination.destinationIconBkColor != null) {
                intValue2 = destination.destinationIconBkColor;
            }
            else {
                intValue2 = 0;
            }
            destination2 = new Destination(doubleValue, doubleValue2, doubleValue3, doubleValue4, full_address, destination_title, destination_subtitle, identifier, favoriteDestinationType, default1, initials, placeCategory, recentTimeLabel, n, equals, intValue, intValue2, destination.place_id, destination.place_type, destination.destinationDistance, ContactUtil.fromPhoneNumbers(destination.phoneNumbers), ContactUtil.fromContacts(destination.contacts));
        }
        return destination2;
    }
    
    public com.navdy.service.library.events.destination.Destination transformToProtoDestination(final Destination destination) {
        final Boolean b = null;
        final com.navdy.service.library.events.destination.Destination.FavoriteType favorite_NONE = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE;
        final com.navdy.service.library.events.destination.Destination.SuggestionType suggestionType = null;
        ProtoEnum protoEnum = favorite_NONE;
        ProtoEnum suggestion_CALENDAR = suggestionType;
        Label_0115: {
            if (destination.favoriteDestinationType != null) {
                switch (destination.favoriteDestinationType) {
                    default:
                        protoEnum = favorite_NONE;
                        suggestion_CALENDAR = suggestionType;
                        switch (destination.favoriteDestinationType) {
                            default:
                                suggestion_CALENDAR = suggestionType;
                                protoEnum = favorite_NONE;
                                break Label_0115;
                            case FAVORITE_CUSTOM:
                                protoEnum = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CUSTOM;
                                suggestion_CALENDAR = suggestionType;
                                break Label_0115;
                            case FAVORITE_CONTACT:
                                protoEnum = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CONTACT;
                                suggestion_CALENDAR = suggestionType;
                                break Label_0115;
                            case FAVORITE_WORK:
                                protoEnum = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_WORK;
                                suggestion_CALENDAR = suggestionType;
                                break Label_0115;
                            case FAVORITE_HOME:
                                protoEnum = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_HOME;
                                suggestion_CALENDAR = suggestionType;
                            case FAVORITE_NONE:
                            case FAVORITE_CALENDAR:
                                break Label_0115;
                        }
                        break;
                    case FAVORITE_CALENDAR:
                        suggestion_CALENDAR = com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_CALENDAR;
                        protoEnum = favorite_NONE;
                        break;
                }
            }
        }
        Boolean b2 = b;
        ProtoEnum suggestion_RECENT = suggestion_CALENDAR;
        if (destination.placeCategory != null) {
            switch (destination.placeCategory) {
                default:
                    suggestion_RECENT = suggestion_CALENDAR;
                    b2 = b;
                    break;
                case SUGGESTED:
                    b2 = true;
                    suggestion_RECENT = suggestion_CALENDAR;
                    break;
                case SUGGESTED_RECENT:
                    b2 = true;
                    suggestion_RECENT = com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT;
                    break;
            }
        }
        PlaceType placeType = PlaceType.PLACE_TYPE_UNKNOWN;
        if (destination.placeType != null) {
            placeType = destination.placeType;
        }
        final com.navdy.service.library.events.destination.Destination.Builder phoneNumbers = new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(new LatLong(Double.valueOf(destination.navigationPositionLatitude), Double.valueOf(destination.navigationPositionLongitude))).display_position(new LatLong(Double.valueOf(destination.displayPositionLatitude), Double.valueOf(destination.displayPositionLongitude))).full_address(destination.fullAddress).destination_title(destination.destinationTitle).destination_subtitle(destination.destinationSubtitle).identifier(destination.identifier).is_recommendation(b2).place_type(placeType).suggestion_type((com.navdy.service.library.events.destination.Destination.SuggestionType)suggestion_RECENT).place_id(destination.destinationPlaceId).favorite_type((com.navdy.service.library.events.destination.Destination.FavoriteType)protoEnum).contacts(ContactUtil.toContacts(destination.contacts)).phoneNumbers(ContactUtil.toPhoneNumbers(destination.phoneNumbers));
        if (destination.destinationIcon != 0) {
            phoneNumbers.destinationIcon(destination.destinationIcon);
        }
        if (destination.destinationIconBkColor != 0) {
            phoneNumbers.destinationIconBkColor(destination.destinationIconBkColor);
        }
        if (destination.distanceStr != null) {
            phoneNumbers.destinationDistance(destination.distanceStr);
        }
        return phoneNumbers.build();
    }
    
    public static class FavoriteDestinationsChanged
    {
    }
    
    public static class GasDestination
    {
        public Destination destination;
        public boolean showFirst;
    }
    
    public static class RecentDestinationsChanged
    {
    }
    
    public static class SuggestedDestinationsChanged
    {
    }
}
