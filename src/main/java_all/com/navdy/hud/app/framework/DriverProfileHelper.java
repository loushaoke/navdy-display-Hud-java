package com.navdy.hud.app.framework;

import com.navdy.hud.app.profile.DriverProfile;
import java.util.Locale;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import javax.inject.Inject;
import com.navdy.hud.app.profile.DriverProfileManager;

public class DriverProfileHelper
{
    private static final DriverProfileHelper sInstance;
    @Inject
    DriverProfileManager driverProfileManager;
    
    static {
        sInstance = new DriverProfileHelper();
    }
    
    private DriverProfileHelper() {
        Mortar.inject(HudApplication.getAppContext(), this);
    }
    
    public static DriverProfileHelper getInstance() {
        return DriverProfileHelper.sInstance;
    }
    
    public Locale getCurrentLocale() {
        return this.driverProfileManager.getCurrentLocale();
    }
    
    public DriverProfile getCurrentProfile() {
        return this.driverProfileManager.getCurrentProfile();
    }
    
    public DriverProfileManager getDriverProfileManager() {
        return this.driverProfileManager;
    }
}
