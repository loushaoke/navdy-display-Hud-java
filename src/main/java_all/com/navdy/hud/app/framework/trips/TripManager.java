package com.navdy.hud.app.framework.trips;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.here.android.mpa.common.GeoPosition;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.hud.app.analytics.TelemetrySession;
import android.os.SystemClock;
import com.navdy.hud.app.maps.util.MapUtils;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.hud.app.manager.SpeedManager;
import android.content.SharedPreferences;
import com.navdy.service.library.events.TripUpdate;
import android.location.Location;
import com.here.android.mpa.common.GeoCoordinate;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import javax.inject.Singleton;

@Singleton
public class TripManager
{
    public static final int MAX_SPEED_METERS_PER_SECOND_THRESHOLD = 200;
    private static final long MIN_TIME_THRESHOLD = 1000L;
    public static final String PREFERENCE_TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY = "trip_manager_total_distance_travelled_with_navdy";
    public static final long TOTAL_DISTANCE_PERSIST_INTERVAL = 30000L;
    private static final Logger sLogger;
    private final Bus bus;
    private int currentDistanceTraveledInMeters;
    private int currentSequenceNumber;
    private volatile State currentState;
    private long currentTripNumber;
    private long currentTripStartTime;
    private boolean isClientConnected;
    private GeoCoordinate lastCoords;
    private Location lastLocation;
    private long lastTotalDistancePersistTime;
    private long lastTotalDistancePersistedValue;
    private long lastTrackingTime;
    private TripUpdate.Builder lastTripUpdateBuilder;
    private StringBuilder logBuilder;
    SharedPreferences preferences;
    private final SpeedManager speedManager;
    private int totalDistanceTravelledInMeters;
    
    static {
        sLogger = new Logger(TripManager.class);
    }
    
    public TripManager(final Bus bus, final SharedPreferences preferences) {
        this.lastTotalDistancePersistTime = 0L;
        this.lastTotalDistancePersistedValue = 0L;
        this.bus = bus;
        this.speedManager = SpeedManager.getInstance();
        this.preferences = preferences;
        this.currentState = State.STOPPED;
        this.logBuilder = new StringBuilder();
        this.isClientConnected = false;
        this.totalDistanceTravelledInMeters = 0;
    }
    
    private void createNewTrip() {
        GenericUtil.checkNotOnMainThread();
        this.currentTripNumber = System.currentTimeMillis();
        this.currentSequenceNumber = 0;
        this.currentDistanceTraveledInMeters = 0;
        this.currentTripStartTime = System.currentTimeMillis();
    }
    
    private Coordinate toCoordinate(final Location location) {
        Coordinate build;
        if (location == null) {
            build = null;
        }
        else {
            build = new Coordinate.Builder().accuracy(location.getAccuracy()).altitude(location.getAltitude()).bearing(location.getBearing()).latitude(location.getLatitude()).longitude(location.getLongitude()).provider(location.getProvider()).speed(location.getSpeed()).timestamp(location.getTime()).build();
        }
        return build;
    }
    
    public int getCurrentDistanceTraveled() {
        return this.currentDistanceTraveledInMeters;
    }
    
    public long getCurrentTripStartTime() {
        return this.currentTripStartTime;
    }
    
    public int getTotalDistanceTravelled() {
        return this.totalDistanceTravelledInMeters;
    }
    
    public long getTotalDistanceTravelledWithNavdy() {
        return this.preferences.getLong("trip_manager_total_distance_travelled_with_navdy", 0L) + (this.totalDistanceTravelledInMeters - this.lastTotalDistancePersistedValue);
    }
    
    @Subscribe
    public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
        if (connectionStateChange.state == ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
            this.isClientConnected = false;
        }
        else if (connectionStateChange.state == ConnectionStateChange.ConnectionState.CONNECTION_VERIFIED) {
            this.isClientConnected = true;
        }
    }
    
    @Subscribe
    public void onFinishedTripRouteEvent(final FinishedTripRouteEvent finishedTripRouteEvent) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (TripManager.this.lastTripUpdateBuilder != null && TripManager.this.lastTripUpdateBuilder.chosen_destination_id != null) {
                    TripManager.this.lastTripUpdateBuilder.arrived_at_destination_id(TripManager.this.lastTripUpdateBuilder.chosen_destination_id);
                    if (TripManager.this.currentState != State.STOPPED) {
                        TripManager.this.bus.post(new RemoteEvent(TripManager.this.lastTripUpdateBuilder.build()));
                    }
                }
            }
        }, 8);
    }
    
    @Subscribe
    public void onLocation(final Location lastLocation) {
        this.lastLocation = lastLocation;
    }
    
    @Subscribe
    public void onNewTrip(final NewTripEvent newTripEvent) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                TripManager.this.currentState = State.STOPPED;
            }
        }, 8);
    }
    
    @Subscribe
    public void onTrack(final TrackingEvent trackingEvent) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (TripManager.this.currentState == State.STOPPED) {
                    TripManager.this.createNewTrip();
                    TripManager.this.currentState = State.STARTED;
                }
                final GeoCoordinate coordinate = trackingEvent.geoPosition.getCoordinate();
                final double latitude = coordinate.getLatitude();
                final double longitude = coordinate.getLongitude();
                final double altitude = coordinate.getAltitude();
                if (MapUtils.sanitizeCoords(coordinate) == null) {
                    TripManager.sLogger.v("filtering out bad coords: " + latitude + ", " + longitude + ", " + altitude);
                }
                else {
                    if (TripManager.this.lastCoords != null) {
                        final long elapsedRealtime = SystemClock.elapsedRealtime();
                        final long n = elapsedRealtime - TripManager.this.lastTrackingTime;
                        if (n < 1000L) {
                            return;
                        }
                        TripManager.this.lastTrackingTime = elapsedRealtime;
                        final int n2 = (int)trackingEvent.geoPosition.getCoordinate().distanceTo(TripManager.this.lastCoords);
                        if (n2 / (n / 1000.0) <= 200.0) {
                            TripManager.this.currentDistanceTraveledInMeters += n2;
                            TripManager.this.totalDistanceTravelledInMeters += n2;
                            if (TripManager.this.lastTotalDistancePersistTime == 0L || elapsedRealtime - TripManager.this.lastTotalDistancePersistTime > 30000L) {
                                TripManager.this.lastTotalDistancePersistTime = elapsedRealtime;
                                TripManager.this.preferences.edit().putLong("trip_manager_total_distance_travelled_with_navdy", TripManager.this.getTotalDistanceTravelledWithNavdy()).apply();
                                TripManager.this.lastTotalDistancePersistedValue = TripManager.this.totalDistanceTravelledInMeters;
                            }
                        }
                        else {
                            TripManager.sLogger.d("Unusual jump between co ordinates, Last lat :" + TripManager.this.lastCoords.getLatitude() + ", " + "Lon :" + TripManager.this.lastCoords.getLongitude() + ", " + "Current Lat:" + trackingEvent.geoPosition.getCoordinate().getLatitude() + ", " + "Lon :" + trackingEvent.geoPosition.getCoordinate().getLongitude());
                        }
                    }
                    TripManager.this.lastCoords = trackingEvent.geoPosition.getCoordinate();
                    if (TripManager.this.isClientConnected) {
                        ++TripManager.this.currentSequenceNumber;
                        final TelemetrySession instance = TelemetrySession.INSTANCE;
                        final TripUpdate.Builder last_raw_coordinate = new TripUpdate.Builder().trip_number(TripManager.this.currentTripNumber).sequence_number(TripManager.this.currentSequenceNumber).timestamp(System.currentTimeMillis()).distance_traveled(TripManager.this.currentDistanceTraveledInMeters).current_position(new LatLong(Double.valueOf(latitude), Double.valueOf(longitude))).elevation(trackingEvent.geoPosition.getCoordinate().getAltitude()).bearing((float)trackingEvent.geoPosition.getHeading()).gps_speed((float)trackingEvent.geoPosition.getSpeed()).obd_speed(TripManager.this.speedManager.getObdSpeed()).hard_acceleration_count(instance.getSessionHardAccelerationCount()).hard_breaking_count(instance.getSessionHardBrakingCount()).high_g_count(instance.getSessionHighGCount()).speeding_ratio((double)instance.getSessionSpeedingPercentage()).excessive_speeding_ratio((double)instance.getSessionExcessiveSpeedingPercentage()).meters_traveled_since_boot((int)instance.getSessionDistance()).horizontal_accuracy((trackingEvent.geoPosition.getLatitudeAccuracy() + trackingEvent.geoPosition.getLongitudeAccuracy()) / 2.0f).elevation_accuracy(trackingEvent.geoPosition.getAltitudeAccuracy()).last_raw_coordinate(TripManager.this.toCoordinate(TripManager.this.lastLocation));
                        if (TripManager.sLogger.isLoggable(2)) {
                            TripManager.this.logBuilder.setLength(0);
                            TripManager.this.logBuilder.append("TripUpdate: tripNumber=" + TripManager.this.currentTripNumber + "; sequenceNumber=" + TripManager.this.currentSequenceNumber + "; timestamp=" + System.currentTimeMillis() + "; distanceTraveled=" + TripManager.this.currentDistanceTraveledInMeters + "; currentPosition=" + latitude + "," + longitude + "; elevation=" + trackingEvent.geoPosition.getCoordinate().getAltitude() + "; bearing=" + trackingEvent.geoPosition.getHeading() + "; gpsSpeed=" + trackingEvent.geoPosition.getSpeed() + "; obdSpeed=" + TripManager.this.speedManager.getObdSpeed());
                        }
                        if (trackingEvent.isRouteTracking()) {
                            last_raw_coordinate.chosen_destination_id(trackingEvent.chosenDestinationId).estimated_time_remaining(trackingEvent.estimatedTimeRemaining).distance_to_destination(trackingEvent.distanceToDestination);
                            if (TripManager.sLogger.isLoggable(2)) {
                                TripManager.this.logBuilder.append("; chosenDestinationId=" + trackingEvent.chosenDestinationId + "; estimatedTimeRemaining=" + trackingEvent.estimatedTimeRemaining + "; distanceToDestination=" + trackingEvent.distanceToDestination);
                            }
                        }
                        if (TripManager.sLogger.isLoggable(2)) {
                            TripManager.sLogger.v(TripManager.this.logBuilder.toString());
                        }
                        TripManager.this.lastTripUpdateBuilder = last_raw_coordinate;
                        if (TripManager.this.currentState != State.STOPPED) {
                            TripManager.this.bus.post(new RemoteEvent(TripManager.this.lastTripUpdateBuilder.build()));
                        }
                    }
                }
            }
        }, 8);
    }
    
    public static class FinishedTripRouteEvent
    {
    }
    
    public static class NewTripEvent
    {
    }
    
    public enum State
    {
        STARTED, 
        STOPPED;
    }
    
    public static class TrackingEvent
    {
        private final String chosenDestinationId;
        private final int distanceToDestination;
        private final int estimatedTimeRemaining;
        private final GeoPosition geoPosition;
        
        public TrackingEvent(final GeoPosition geoPosition) {
            this.geoPosition = geoPosition;
            this.chosenDestinationId = null;
            this.estimatedTimeRemaining = -1;
            this.distanceToDestination = -1;
        }
        
        public TrackingEvent(final GeoPosition geoPosition, final String chosenDestinationId, final int estimatedTimeRemaining, final int distanceToDestination) {
            this.geoPosition = geoPosition;
            this.chosenDestinationId = chosenDestinationId;
            this.estimatedTimeRemaining = estimatedTimeRemaining;
            this.distanceToDestination = distanceToDestination;
        }
        
        public boolean isRouteTracking() {
            if (TripManager.sLogger.isLoggable(2)) {
                TripManager.sLogger.v("[routeTracking] chosenDestinationId=" + this.chosenDestinationId + "; estimatedTimeRemaining=" + this.estimatedTimeRemaining + "; distanceToDestination=" + this.distanceToDestination);
            }
            return this.chosenDestinationId != null && this.estimatedTimeRemaining >= 0 && this.distanceToDestination >= 0;
        }
    }
}
