package com.navdy.hud.app.settings;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class MainScreenSettings implements Parcelable
{
    public static final Parcelable.Creator<MainScreenSettings> CREATOR;
    protected ScreenConfiguration landscapeConfiguration;
    protected ScreenConfiguration portraitConfiguration;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<MainScreenSettings>() {
            public MainScreenSettings createFromParcel(final Parcel parcel) {
                return new MainScreenSettings(parcel);
            }
            
            public MainScreenSettings[] newArray(final int n) {
                return new MainScreenSettings[n];
            }
        };
    }
    
    public MainScreenSettings() {
        this(new ScreenConfiguration(), new ScreenConfiguration());
    }
    
    public MainScreenSettings(final Parcel parcel) {
        this((ScreenConfiguration)parcel.readParcelable(MainScreenSettings.class.getClassLoader()), (ScreenConfiguration)parcel.readParcelable(MainScreenSettings.class.getClassLoader()));
    }
    
    public MainScreenSettings(final ScreenConfiguration portraitConfiguration, final ScreenConfiguration landscapeConfiguration) {
        this.portraitConfiguration = portraitConfiguration;
        this.landscapeConfiguration = landscapeConfiguration;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public ScreenConfiguration getLandscapeConfiguration() {
        return this.landscapeConfiguration;
    }
    
    public ScreenConfiguration getPortraitConfiguration() {
        return this.portraitConfiguration;
    }
    
    public void setLandscapeConfiguration(final ScreenConfiguration landscapeConfiguration) {
        this.landscapeConfiguration = landscapeConfiguration;
    }
    
    public void setPortraitConfiguration(final ScreenConfiguration portraitConfiguration) {
        this.portraitConfiguration = portraitConfiguration;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeParcelable((Parcelable)this.portraitConfiguration, 0);
        parcel.writeParcelable((Parcelable)this.landscapeConfiguration, 0);
    }
    
    public static class ScreenConfiguration implements Parcelable
    {
        public static final Parcelable.Creator<ScreenConfiguration> CREATOR;
        protected Rect margins;
        protected float scaleX;
        protected float scaleY;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<ScreenConfiguration>() {
                public ScreenConfiguration createFromParcel(final Parcel parcel) {
                    return new ScreenConfiguration(parcel);
                }
                
                public ScreenConfiguration[] newArray(final int n) {
                    return new ScreenConfiguration[n];
                }
            };
        }
        
        public ScreenConfiguration() {
            this(new Rect(), 1.0f, 1.0f);
        }
        
        public ScreenConfiguration(final Rect margins, final float scaleX, final float scaleY) {
            this.margins = margins;
            this.scaleX = scaleX;
            this.scaleY = scaleY;
        }
        
        public ScreenConfiguration(final Parcel parcel) {
            this((Rect)parcel.readParcelable(ScreenConfiguration.class.getClassLoader()), parcel.readFloat(), parcel.readFloat());
        }
        
        public int describeContents() {
            return 0;
        }
        
        public Rect getMargins() {
            return this.margins;
        }
        
        public float getScaleX() {
            return this.scaleX;
        }
        
        public float getScaleY() {
            return this.scaleY;
        }
        
        public void setMargins(final Rect margins) {
            this.margins = margins;
        }
        
        public void setScaleX(final float scaleX) {
            this.scaleX = scaleX;
        }
        
        public void setScaleY(final float scaleY) {
            this.scaleY = scaleY;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeParcelable((Parcelable)this.margins, 0);
            parcel.writeFloat(this.scaleX);
            parcel.writeFloat(this.scaleY);
        }
    }
}
