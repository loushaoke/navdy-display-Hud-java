package com.navdy.hud.app.settings;

import java.util.Map;
import com.navdy.service.library.events.settings.Setting;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import android.content.SharedPreferences;
import com.navdy.hud.app.util.os.SystemProperties;
import android.content.SharedPreferences;
import com.navdy.service.library.log.Logger;

public class HUDSettings
{
    private static final String ADAPTIVE_AUTOBRIGHTNESS_PROP = "persist.sys.autobright_adaptive";
    public static final String AUTO_BRIGHTNESS = "screen.auto_brightness";
    public static final String AUTO_BRIGHTNESS_ADJUSTMENT = "screen.auto_brightness_adj";
    public static final String BRIGHTNESS = "screen.brightness";
    public static final float BRIGHTNESS_SCALE = 255.0f;
    public static final String GESTURE_ENGINE = "gesture.engine";
    public static final String GESTURE_PREVIEW = "gesture.preview";
    public static final String LED_BRIGHTNESS = "screen.led_brightness";
    public static final String MAP_ANIMATION_MODE = "map.animation.mode";
    public static final String MAP_SCHEME = "map.scheme";
    public static final String MAP_TILT = "map.tilt";
    public static final String MAP_ZOOM = "map.zoom";
    public static final boolean USE_ADAPTIVE_AUTOBRIGHTNESS;
    private static Logger sLogger;
    private SharedPreferences preferences;
    
    static {
        USE_ADAPTIVE_AUTOBRIGHTNESS = SystemProperties.getBoolean("persist.sys.autobright_adaptive", true);
        HUDSettings.sLogger = new Logger(HUDSettings.class);
    }
    
    public HUDSettings(final SharedPreferences preferences) {
        this.preferences = preferences;
    }
    
    private Object get(final String s) {
        return this.preferences.getAll().get(s);
    }
    
    private void set(final SharedPreferences$Editor sharedPreferences$Editor, final String s, final String s2) {
        final Object value = this.get(s);
        if (value instanceof String) {
            sharedPreferences$Editor.putString(s, s2);
        }
        else if (value instanceof Boolean) {
            sharedPreferences$Editor.putBoolean(s, (boolean)Boolean.valueOf(s2));
        }
    }
    
    public List<String> availableSettings() {
        final ArrayList<Object> list = (ArrayList<Object>)new ArrayList<String>();
        list.addAll(this.preferences.getAll().keySet());
        return (List<String>)list;
    }
    
    public List<Setting> readSettings(final List<String> list) {
        final ArrayList<Setting> list2 = new ArrayList<Setting>();
        final Map all = this.preferences.getAll();
        for (int i = 0; i < list.size(); ++i) {
            final String s = list.get(i);
            final Object value = all.get(s);
            if (value != null) {
                list2.add(new Setting(s, value.toString()));
            }
        }
        return list2;
    }
    
    public void updateSettings(final List<Setting> list) {
        final SharedPreferences$Editor edit = this.preferences.edit();
        for (int i = 0; i < list.size(); ++i) {
            final Setting setting = list.get(i);
            if (setting.value.equals("-1")) {
                final String key = setting.key;
                switch (key) {
                    case "map.tilt":
                        this.set(edit, setting.key, String.valueOf(60.0f));
                        break;
                    case "map.zoom":
                        this.set(edit, setting.key, String.valueOf(16.5f));
                        break;
                }
            }
            else {
                this.set(edit, setting.key, setting.value);
            }
        }
        edit.apply();
    }
}
