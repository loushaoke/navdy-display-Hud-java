package com.navdy.hud.app.settings;

import android.content.ContentResolver;
import android.view.WindowManager;
import android.app.Activity;
import android.provider.Settings;
import android.util.Log;
import com.navdy.service.library.task.TaskManager;
import android.provider.Settings;
import android.content.SharedPreferences;
import com.navdy.hud.app.util.os.SystemProperties;
import android.content.SharedPreferences;
import android.os.Handler;
import android.content.Context;
import com.squareup.otto.Bus;
import android.content.SharedPreferences;

public class BrightnessControl implements SharedPreferences$OnSharedPreferenceChangeListener
{
    public static final String AUTOBRIGHTNESSD_PROPERTY = "hw.navdy.autobrightnessd";
    public static final String AUTO_BRIGHTNESS_PROPERTY = "persist.sys.autobrightness";
    public static final String DEFAULT_AUTO_BRIGHTNESS = "false";
    public static final String DEFAULT_AUTO_BRIGHTNESS_ADJUSTMENT = "0";
    public static final String DEFAULT_BRIGHTNESS = "128";
    public static final String DEFAULT_LED_BRIGHTNESS = "255";
    public static final String DISABLED = "disabled";
    public static final String ENABLED = "enabled";
    public static final int MAX_BRIGHTNESS_ADJUSTMENT = 64;
    public static final int MIN_BRIGHTNESS_ADJUSTMENT = -64;
    private static final int MIN_INITIAL_BRIGHTNESS = 128;
    public static final String OFF = "off";
    public static final String ON = "on";
    private static final String TAG;
    private String autoBrightnessAdjustmentKey;
    private String autoBrightnessKey;
    private String brightnessKey;
    private Bus bus;
    private Context context;
    private float lastBrightness;
    private String ledBrightnessKey;
    private Handler mainHandler;
    private SharedPreferences preferences;
    
    static {
        TAG = BrightnessControl.class.getName();
    }
    
    public BrightnessControl(final Context context, final Bus bus, final SharedPreferences preferences, final String brightnessKey, final String autoBrightnessKey, final String autoBrightnessAdjustmentKey, final String ledBrightnessKey) {
        this.lastBrightness = 0.5f;
        this.context = context;
        (this.bus = bus).register(this);
        this.preferences = preferences;
        this.mainHandler = new Handler(context.getMainLooper());
        preferences.registerOnSharedPreferenceChangeListener((SharedPreferences$OnSharedPreferenceChangeListener)this);
        this.brightnessKey = brightnessKey;
        this.autoBrightnessKey = autoBrightnessKey;
        this.autoBrightnessAdjustmentKey = autoBrightnessAdjustmentKey;
        this.ledBrightnessKey = ledBrightnessKey;
        SystemProperties.set("hw.navdy.autobrightnessd", "0");
        final boolean autoBrightnessProperty = this.getAutoBrightnessProperty();
        final SharedPreferences$Editor edit = preferences.edit();
        String s;
        if (autoBrightnessProperty) {
            s = "true";
        }
        else {
            s = "false";
        }
        edit.putString(autoBrightnessKey, s).apply();
        if (!autoBrightnessProperty) {
            if (this.getBrightnessPreference() < 128) {
                preferences.edit().putString(brightnessKey, String.valueOf(128)).apply();
            }
            this.onSharedPreferenceChanged(preferences, this.brightnessKey);
        }
        this.onSharedPreferenceChanged(preferences, autoBrightnessKey);
        this.onSharedPreferenceChanged(preferences, ledBrightnessKey);
    }
    
    private int getAutoBrightnessAdjustmentPreference() {
        return Integer.parseInt(this.preferences.getString(this.autoBrightnessAdjustmentKey, "0"));
    }
    
    private final boolean getAutoBrightnessProperty() {
        final String value = SystemProperties.get("persist.sys.autobrightness", "on");
        return value.equals("on") || value.equals("enabled");
    }
    
    private int getBrightnessPreference() {
        return Integer.parseInt(this.preferences.getString(this.brightnessKey, "128"));
    }
    
    private void setAutoBrightnessAdjustment(final int n) {
        if (n >= -64 && n <= 64) {
            try {
                if (Settings$System.getInt(this.context.getContentResolver(), "screen_brightness_mode") == 1) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            final float n = n / 255.0f;
                            Log.d("TEST", "Normalized : " + n);
                            Settings$System.putFloat(BrightnessControl.this.context.getContentResolver(), "screen_auto_brightness_adj", n);
                        }
                    }, 1);
                }
            }
            catch (Settings$SettingNotFoundException ex) {
                Log.e(BrightnessControl.TAG, "", (Throwable)ex);
            }
        }
    }
    
    private void updateWindowBrightness(final float screenBrightness) {
        final WindowManager$LayoutParams attributes = ((Activity)this.context).getWindow().getAttributes();
        attributes.screenBrightness = screenBrightness;
        ((Activity)this.context).getWindow().setAttributes(attributes);
    }
    
    public String getValue() {
        return Integer.toString(Settings$System.getInt(this.context.getContentResolver(), "screen_brightness", -1));
    }
    
    public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String s) {
        if (s.equals(this.brightnessKey)) {
            this.setBrightnessValue(this.getBrightnessPreference());
        }
        else if (s.equals(this.autoBrightnessAdjustmentKey)) {
            this.setAutoBrightnessAdjustment(this.getAutoBrightnessAdjustmentPreference());
        }
        else if (s.equals(this.autoBrightnessKey)) {
            this.toggleAutoBrightness(Boolean.valueOf(sharedPreferences.getString(s, "false")));
        }
        else if (s.equals(this.ledBrightnessKey)) {
            this.setLEDValue(sharedPreferences.getString(s, "255"));
        }
    }
    
    public void setBrightnessValue(final int n) {
        if (n >= 0 && n <= 255) {
            this.lastBrightness = n / 255.0f;
            try {
                if (Settings$System.getInt(this.context.getContentResolver(), "screen_brightness_mode") != 1) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            Settings$System.putInt(BrightnessControl.this.context.getContentResolver(), "screen_brightness", n);
                        }
                    }, 1);
                    this.updateWindowBrightness(this.lastBrightness);
                }
            }
            catch (Settings$SettingNotFoundException ex) {
                Log.e(BrightnessControl.TAG, "", (Throwable)ex);
            }
        }
    }
    
    public void setLEDValue(final String s) {
        final int int1 = Integer.parseInt(s);
        if (int1 >= 0 && int1 <= 255) {
            SystemProperties.set("hw.navdy.led_max_brightness", String.format("%f", int1 / 255.0f));
        }
    }
    
    public void toggleAutoBrightness(final boolean b) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final ContentResolver contentResolver = BrightnessControl.this.context.getContentResolver();
                int n;
                if (b) {
                    n = 1;
                }
                else {
                    n = 0;
                }
                Settings$System.putInt(contentResolver, "screen_brightness_mode", n);
                BrightnessControl.this.mainHandler.post((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        final BrightnessControl this$0 = BrightnessControl.this;
                        float access$100;
                        if (b) {
                            access$100 = -1.0f;
                        }
                        else {
                            access$100 = BrightnessControl.this.lastBrightness;
                        }
                        this$0.updateWindowBrightness(access$100);
                    }
                });
            }
        }, 1);
    }
}
