package com.navdy.hud.app.gesture;

import com.navdy.service.library.events.input.GestureEvent;
import java.util.ArrayList;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import java.util.List;

public class FlickTurnGestureDetector extends MultipleClickGestureDetector
{
    private static final List<CustomKeyEvent> FLICKABLE_EVENTS;
    private static final int FLICK_EVENT_COUNT = 4;
    private static final int FLICK_PAIRED_EVENT_TIMEOUT_MS = 500;
    private static final Logger sLogger;
    private short consecutiveTurnEventCount;
    private IFlickTurnKeyGesture listener;
    private CustomKeyEvent previousKeyEvent;
    private long previousKeyEventTime;
    
    static {
        sLogger = new Logger(FlickTurnGestureDetector.class);
        FLICKABLE_EVENTS = new ArrayList() {
            {
                this.add(CustomKeyEvent.LEFT);
                this.add(CustomKeyEvent.RIGHT);
            }
        };
    }
    
    public FlickTurnGestureDetector(final IMultipleClickKeyGesture multipleClickKeyGesture) {
        super(2, multipleClickKeyGesture);
        this.previousKeyEvent = null;
        this.previousKeyEventTime = 0L;
        this.consecutiveTurnEventCount = 0;
        if (multipleClickKeyGesture == null || !(multipleClickKeyGesture instanceof IFlickTurnKeyGesture)) {
            throw new IllegalArgumentException();
        }
        this.listener = (IFlickTurnKeyGesture)multipleClickKeyGesture;
    }
    
    @Override
    public IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Override
    public boolean onKey(final CustomKeyEvent previousKeyEvent) {
        final boolean onKey = super.onKey(previousKeyEvent);
        final long currentTimeMillis = System.currentTimeMillis();
        if (FlickTurnGestureDetector.FLICKABLE_EVENTS.contains(previousKeyEvent) && this.previousKeyEvent == previousKeyEvent && currentTimeMillis < this.previousKeyEventTime + 500L) {
            final short n = (short)(this.consecutiveTurnEventCount + 1);
            this.consecutiveTurnEventCount = n;
            if (n == 4) {
                FlickTurnGestureDetector.sLogger.d("Combined key event recognized from a single " + previousKeyEvent);
                this.consecutiveTurnEventCount = 0;
                if (CustomKeyEvent.LEFT.equals(previousKeyEvent)) {
                    this.listener.onFlickLeft();
                }
                else {
                    if (!CustomKeyEvent.RIGHT.equals(previousKeyEvent)) {
                        throw new UnsupportedOperationException("Flick for " + previousKeyEvent + " not handled");
                    }
                    this.listener.onFlickRight();
                }
            }
        }
        else {
            this.consecutiveTurnEventCount = 1;
        }
        this.previousKeyEvent = previousKeyEvent;
        this.previousKeyEventTime = currentTimeMillis;
        return onKey;
    }
    
    public interface IFlickTurnKeyGesture extends IMultipleClickKeyGesture
    {
        void onFlickLeft();
        
        void onFlickRight();
    }
}
