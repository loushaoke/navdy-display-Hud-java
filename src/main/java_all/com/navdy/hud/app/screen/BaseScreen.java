package com.navdy.hud.app.screen;

import com.navdy.service.library.events.ui.Screen;
import flow.Flow;
import com.navdy.hud.app.ui.activity.Main;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import java.lang.reflect.Field;
import mortar.MortarScope;
import android.os.Bundle;
import com.navdy.service.library.log.Logger;
import mortar.Blueprint;

public abstract class BaseScreen implements Blueprint
{
    public static final int ANIMATION_NONE = -1;
    private static final Logger sLogger;
    protected Bundle arguments;
    protected Object arguments2;
    
    static {
        sLogger = new Logger(BaseScreen.class);
    }
    
    private Bundle getInstanceState(final MortarScope mortarScope) {
        try {
            final Field declaredField = mortarScope.getClass().getDeclaredField("latestSavedInstanceState");
            declaredField.setAccessible(true);
            return (Bundle)declaredField.get(mortarScope);
        }
        catch (Throwable t) {
            BaseScreen.sLogger.e("failed to get screen instance state", t);
            return null;
        }
    }
    
    private void setInstanceState(final MortarScope mortarScope, final Bundle bundle) {
        try {
            final Field declaredField = mortarScope.getClass().getDeclaredField("latestSavedInstanceState");
            declaredField.setAccessible(true);
            declaredField.set(mortarScope, bundle);
        }
        catch (Throwable t) {
            BaseScreen.sLogger.e("failed to set screen instance state", t);
        }
    }
    
    private void updateInstanceState() {
        final MortarScope child = Mortar.getScope(HudApplication.getAppContext()).findChild(Main.class.getName());
        if (child != null) {
            final MortarScope requireChild = child.requireChild(this);
            Bundle instanceState;
            if ((instanceState = this.getInstanceState(requireChild)) == null) {
                instanceState = new Bundle();
                this.setInstanceState(requireChild, instanceState);
            }
            instanceState.putBundle(this.getMortarScopeName() + "$Presenter", this.arguments);
        }
    }
    
    public int getAnimationIn(final Flow.Direction direction) {
        int n;
        if (direction == Flow.Direction.FORWARD) {
            n = R.anim.slide_in_down;
        }
        else {
            n = R.anim.slide_in_up;
        }
        return n;
    }
    
    public int getAnimationOut(final Flow.Direction direction) {
        int n;
        if (direction == Flow.Direction.FORWARD) {
            n = R.anim.slide_out_down;
        }
        else {
            n = R.anim.slide_out_up;
        }
        return n;
    }
    
    public Bundle getArguments() {
        return this.arguments;
    }
    
    public Object getArguments2() {
        return this.arguments2;
    }
    
    public abstract Screen getScreen();
    
    public void onAnimationInEnd() {
    }
    
    public void onAnimationInStart() {
    }
    
    public void onAnimationOutEnd() {
    }
    
    public void onAnimationOutStart() {
    }
    
    public void setArguments(final Bundle arguments, final Object arguments2) {
        this.arguments = arguments;
        this.arguments2 = arguments2;
        this.updateInstanceState();
    }
}
