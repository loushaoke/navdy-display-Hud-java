package com.navdy.hud.app.screen;

import com.navdy.hud.app.service.ShutdownMonitor;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import android.os.Bundle;
import android.content.Intent;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.obd.ObdManager;
import android.content.SharedPreferences;
import com.squareup.otto.Bus;
import javax.inject.Inject;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.ShutDownConfirmationView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.shutdown_confirmation_lyt)
public class ShutDownScreen extends BaseScreen
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(ShutDownScreen.class);
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return ShutDownScreen.class.getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_SHUTDOWN_CONFIRMATION;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { ShutDownConfirmationView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<ShutDownConfirmationView>
    {
        @Inject
        GestureServiceConnector gestureServiceConnector;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;
        ObdManager obdManager;
        Shutdown.Reason shutDownCause;
        @Inject
        UIStateManager uiStateManager;
        
        public void finish() {
            this.mBus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
            final Shutdown shutdown = new Shutdown(this.shutDownCause, Shutdown.State.CANCELED);
            ShutDownScreen.sLogger.i("Posting " + shutdown);
            this.mBus.post(shutdown);
        }
        
        public String getCurrentVersion() {
            return OTAUpdateService.getCurrentVersion();
        }
        
        public Shutdown.Reason getShutDownCause() {
            return this.shutDownCause;
        }
        
        public String getUpdateVersion() {
            return OTAUpdateService.getSWUpdateVersion();
        }
        
        public void installAndShutDown() {
            String s = "INSTALL_UPDATE_SHUTDOWN";
            if (this.obdManager.getConnectionType() == ObdManager.ConnectionType.OBD) {
                s = s;
                if (this.obdManager.getBatteryVoltage() < 13.100000381469727) {
                    s = "INSTALL_UPDATE_REBOOT_QUIET";
                }
            }
            final Intent intent = new Intent(HudApplication.getAppContext(), (Class)OTAUpdateService.class);
            intent.putExtra("COMMAND", s);
            HudApplication.getAppContext().startService(intent);
        }
        
        public void installDialUpdateAndShutDown() {
            final Bundle bundle = new Bundle();
            bundle.putInt("PROGRESS_CAUSE", 2);
            this.mBus.post(new ShowScreenWithArgs(Screen.SCREEN_DIAL_UPDATE_PROGRESS, bundle, false));
        }
        
        public boolean isDialFirmwareUpdatePending() {
            final DialManager instance = DialManager.getInstance();
            return instance.isDialConnected() && instance.getDialFirmwareUpdater().getVersions().isUpdateAvailable();
        }
        
        public boolean isSoftwareUpdatePending() {
            return OTAUpdateService.isUpdateAvailable();
        }
        
        @Subscribe
        public void onDriving(final DrivingStateChange drivingStateChange) {
            if (drivingStateChange.driving && this.getShutDownCause() == Shutdown.Reason.INACTIVITY) {
                this.finish();
            }
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            super.onLoad(bundle);
            this.obdManager = ObdManager.getInstance();
            this.mBus.register(this);
            Label_0043: {
                if (bundle == null) {
                    break Label_0043;
                }
                while (true) {
                    try {
                        final String string = bundle.getString("SHUTDOWN_CAUSE");
                        if (string != null) {
                            this.shutDownCause = Shutdown.Reason.valueOf(string);
                        }
                        if (this.shutDownCause == null) {
                            ShutDownScreen.sLogger.e("defaulting to inactivity reason");
                            this.shutDownCause = Shutdown.Reason.INACTIVITY;
                        }
                        this.uiStateManager.enableSystemTray(false);
                        this.uiStateManager.enableNotificationColor(false);
                        NotificationManager.getInstance().enableNotifications(false);
                        ShutdownMonitor.getInstance().disableInactivityShutdown(true);
                    }
                    catch (Exception ex) {
                        ShutDownScreen.sLogger.e("failed to parse shutdown reason " + ex.getMessage());
                        if (this.shutDownCause == null) {
                            ShutDownScreen.sLogger.e("defaulting to inactivity reason");
                            this.shutDownCause = Shutdown.Reason.INACTIVITY;
                        }
                        continue;
                    }
                    finally {
                        if (this.shutDownCause == null) {
                            ShutDownScreen.sLogger.e("defaulting to inactivity reason");
                            this.shutDownCause = Shutdown.Reason.INACTIVITY;
                        }
                    }
                    break;
                }
            }
        }
        
        @Override
        protected void onUnload() {
            ShutDownScreen.sLogger.v("onUnload");
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            NotificationManager.getInstance().enableNotifications(true);
            ShutdownMonitor.getInstance().disableInactivityShutdown(false);
            this.mBus.unregister(this);
            super.onUnload();
        }
        
        public void shutDown() {
            final Shutdown shutdown = new Shutdown(this.shutDownCause, Shutdown.State.CONFIRMED);
            ShutDownScreen.sLogger.i("Posting " + shutdown);
            this.mBus.post(shutdown);
        }
    }
}
