package com.navdy.hud.app.screen;

import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import android.os.Bundle;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Looper;
import com.navdy.hud.app.ui.framework.UIStateManager;
import java.util.concurrent.atomic.AtomicBoolean;
import android.os.Handler;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.device.dial.DialManager;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.DialManagerView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import flow.Flow;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.screen_dial_manager)
public class DialManagerScreen extends BaseScreen
{
    private static Presenter presenter;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(DialManagerScreen.class);
    }
    
    @Override
    public int getAnimationIn(final Flow.Direction direction) {
        return -1;
    }
    
    @Override
    public int getAnimationOut(final Flow.Direction direction) {
        return -1;
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_DIAL_PAIRING;
    }
    
    public boolean isScanningMode() {
        return DialManagerScreen.presenter != null && DialManagerScreen.presenter.isScanningMode();
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { DialManagerView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<DialManagerView>
    {
        @Inject
        Bus bus;
        private DialManager dialManager;
        @Inject
        GestureServiceConnector gestureServiceConnector;
        private Handler handler;
        private AtomicBoolean isLedTurnedBlue;
        private boolean scanningMode;
        @Inject
        UIStateManager uiStateManager;
        
        public Presenter() {
            this.handler = new Handler(Looper.getMainLooper());
            this.isLedTurnedBlue = new AtomicBoolean(false);
        }
        
        private void exitScreen() {
            if (!RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                DialManagerScreen.sLogger.v("go to welcome screen");
                final Bundle bundle = new Bundle();
                bundle.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_RECONNECT);
                this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_WELCOME, bundle, false));
            }
            else {
                DialManagerScreen.sLogger.v("go to default screen");
                this.bus.post(new ShowScreen.Builder().screen(RemoteDeviceManager.getInstance().getUiStateManager().getDefaultMainActiveScreen()).build());
            }
        }
        
        public void dismiss() {
            this.exitScreen();
        }
        
        public Bus getBus() {
            return this.bus;
        }
        
        public Handler getHandler() {
            return this.handler;
        }
        
        public boolean isScanningMode() {
            return this.scanningMode;
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            DialManagerScreen.sLogger.v("onLoad");
            DialManagerScreen.presenter = this;
            this.dialManager = DialManager.getInstance();
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            DialManagerScreen.sLogger.v("systemtray:invisible");
            String string = null;
            if (bundle != null) {
                string = bundle.getString("OtaDialNameKey", (String)null);
            }
            this.updateView(string);
            super.onLoad(bundle);
        }
        
        @Override
        protected void onUnload() {
            DialManagerScreen.sLogger.v("onUnload");
            DialManagerScreen.presenter = null;
            this.showPairingLed(false);
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            NotificationManager.getInstance().enableNotifications(true);
            DialManagerScreen.sLogger.v("systemtray:visible");
            super.onUnload();
        }
        
        public void showPairingLed(final boolean b) {
            if (b) {
                if (this.isLedTurnedBlue.compareAndSet(false, true)) {
                    HUDLightUtils.showPairing(HudApplication.getAppContext(), LightManager.getInstance(), true);
                }
            }
            else if (this.isLedTurnedBlue.compareAndSet(true, false)) {
                HUDLightUtils.showPairing(HudApplication.getAppContext(), LightManager.getInstance(), false);
            }
        }
        
        public void updateView(final String s) {
            final DialManagerView dialManagerView = this.getView();
            if (dialManagerView != null) {
                if (s != null) {
                    DialManagerScreen.sLogger.v("show searching for dial view");
                    this.scanningMode = true;
                    NotificationManager.getInstance().enableNotifications(false);
                    dialManagerView.showDialSearching(s);
                    this.showPairingLed(true);
                }
                else if (this.dialManager.isDialConnected()) {
                    DialManagerScreen.sLogger.v("show dial connected view");
                    dialManagerView.showDialConnected();
                }
                else {
                    DialManagerScreen.sLogger.v("show dial pairing view");
                    this.scanningMode = true;
                    NotificationManager.getInstance().enableNotifications(false);
                    dialManagerView.showDialPairing();
                    this.showPairingLed(true);
                }
            }
        }
    }
}
