package com.navdy.hud.app.util.os;

import com.navdy.service.library.task.TaskManager;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import com.navdy.service.library.util.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import android.support.annotation.NonNull;
import com.navdy.service.library.log.Logger;
import java.util.regex.Pattern;

public class PropsFileUpdater
{
    private static final String CHAR_SET = "UTF-8";
    private static final String COMMAND_GETPROP = "getprop";
    private static final int PROPS_FILE_INTENT = 2;
    private static final String PROPS_FILE_NAME = "system_info.json";
    private static final String PROPS_FILE_PATH_PROP_NAME = "ro.maps_partition";
    private static final Pattern PROPS_KEY_VALUE_SEPARATOR;
    private static final int PROPS_READOUT_DELAY = 15000;
    private static final String PROPS_TEMP_FILE_SUFFIX = "~";
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(PropsFileUpdater.class);
        PROPS_KEY_VALUE_SEPARATOR = Pattern.compile("\\]: \\[");
    }
    
    @NonNull
    private static JSONObject propsToJSON(@NonNull String s) {
        int i = 0;
        final String[] split = s.split(System.getProperty("line.separator"));
        final JSONObject jsonObject = new JSONObject();
    Label_0074_Outer:
        while (i < split.length) {
            s = split[i];
            final String[] split2 = PropsFileUpdater.PROPS_KEY_VALUE_SEPARATOR.split(s.trim().substring(1, s.length() - 1), 2);
            while (true) {
                try {
                    jsonObject.put(split2[0], split2[1]);
                    ++i;
                    continue Label_0074_Outer;
                }
                catch (JSONException ex) {
                    PropsFileUpdater.sLogger.e("Cannot create json for prop: " + s + " - skipping", (Throwable)ex);
                    continue;
                }
                break;
            }
            break;
        }
        return jsonObject;
    }
    
    @Nullable
    public static String readProps() {
        final String s = null;
        final String s2 = null;
        final String s3 = null;
        Object start = s2;
        Object convertInputStreamToString = s;
        try {
            start = s2;
            convertInputStreamToString = s;
            final ProcessBuilder processBuilder = new ProcessBuilder(new String[0]);
            start = s2;
            convertInputStreamToString = s;
            final Process process = (Process)(convertInputStreamToString = (start = processBuilder.command("getprop").start()));
            process.waitFor();
            start = process;
            convertInputStreamToString = process;
            final Logger sLogger = PropsFileUpdater.sLogger;
            start = process;
            convertInputStreamToString = process;
            start = process;
            convertInputStreamToString = process;
            final StringBuilder sb = new StringBuilder();
            start = process;
            convertInputStreamToString = process;
            sLogger.d(sb.append("Reading props process ended with exit value: ").append(process.exitValue()).toString());
            start = process;
            convertInputStreamToString = process;
            start = (convertInputStreamToString = IOUtils.convertInputStreamToString(process.getInputStream(), "UTF-8"));
            if (process != null) {
                process.destroy();
                convertInputStreamToString = start;
            }
            return (String)convertInputStreamToString;
        }
        catch (Throwable t) {
            convertInputStreamToString = start;
            PropsFileUpdater.sLogger.e("Cannot execute getprop command", t);
            convertInputStreamToString = s3;
            if (start != null) {
                ((Process)start).destroy();
                convertInputStreamToString = s3;
                return (String)convertInputStreamToString;
            }
            return (String)convertInputStreamToString;
        }
        finally {
            if (convertInputStreamToString != null) {
                ((Process)convertInputStreamToString).destroy();
            }
        }
    }
    
    public static void run() {
        new Handler(Looper.getMainLooper()).postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(new UpdatePropsFileRunnable(), 1);
            }
            
            class UpdatePropsFileRunnable implements Runnable
            {
                @Override
                public void run() {
                    try {
                        updatePropsFile(PropsFileUpdater.readProps());
                    }
                    catch (Throwable t) {
                        PropsFileUpdater.sLogger.e("Error while updating props file", t);
                    }
                }
            }
        }, 15000L);
    }
    
    private static void updatePropsFile(@Nullable final String s) {
        if (s == null) {
            PropsFileUpdater.sLogger.w("Cannot update file with empty props string");
        }
        else {
            try {
                final JSONObject propsToJSON = propsToJSON(s);
                updatePropsFile(propsToJSON.getString("ro.maps_partition"), propsToJSON.toString(2));
            }
            catch (JSONException ex) {
                PropsFileUpdater.sLogger.e("Cannot build JSON string with props or read path for output file", (Throwable)ex);
            }
        }
    }
    
    private static void updatePropsFile(@NonNull final String p0, @NonNull final String p1) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_0        
        //     5: ldc             "system_info.json"
        //     7: invokespecial   java/io/File.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //    10: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //    13: astore_2       
        //    14: new             Ljava/lang/StringBuilder;
        //    17: dup            
        //    18: invokespecial   java/lang/StringBuilder.<init>:()V
        //    21: aload_2        
        //    22: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    25: ldc             "~"
        //    27: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    30: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    33: astore_0       
        //    34: new             Ljava/io/ByteArrayInputStream;
        //    37: astore_3       
        //    38: aload_3        
        //    39: aload_1        
        //    40: invokevirtual   java/lang/String.getBytes:()[B
        //    43: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
        //    46: aload_0        
        //    47: aload_3        
        //    48: invokestatic    com/navdy/service/library/util/IOUtils.copyFile:(Ljava/lang/String;Ljava/io/InputStream;)I
        //    51: pop            
        //    52: aload_0        
        //    53: aload_2        
        //    54: invokestatic    android/system/Os.rename:(Ljava/lang/String;Ljava/lang/String;)V
        //    57: getstatic       com/navdy/hud/app/util/os/PropsFileUpdater.sLogger:Lcom/navdy/service/library/log/Logger;
        //    60: ldc             "Props file updated successfully"
        //    62: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //    65: return         
        //    66: astore_1       
        //    67: getstatic       com/navdy/hud/app/util/os/PropsFileUpdater.sLogger:Lcom/navdy/service/library/log/Logger;
        //    70: new             Ljava/lang/StringBuilder;
        //    73: dup            
        //    74: invokespecial   java/lang/StringBuilder.<init>:()V
        //    77: ldc             "Exception while writing into file: "
        //    79: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    82: aload_0        
        //    83: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    86: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    89: aload_1        
        //    90: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    93: goto            65
        //    96: astore_0       
        //    97: getstatic       com/navdy/hud/app/util/os/PropsFileUpdater.sLogger:Lcom/navdy/service/library/log/Logger;
        //   100: ldc             "Cannot overwrite props file with the new one"
        //   102: aload_0        
        //   103: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   106: goto            65
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  34     52     66     96     Ljava/io/IOException;
        //  52     65     96     109    Landroid/system/ErrnoException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0065:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
