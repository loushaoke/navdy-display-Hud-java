package com.navdy.hud.app.util.os;

import android.util.Log;

public class SystemProperties
{
    private static Class<?> CLASS;
    public static final String TAG;
    
    static {
        TAG = SystemProperties.class.getName();
        try {
            SystemProperties.CLASS = Class.forName("android.os.SystemProperties");
        }
        catch (ClassNotFoundException ex) {}
    }
    
    public static String get(String s) {
        try {
            s = (String)SystemProperties.CLASS.getMethod("get", String.class).invoke(null, s);
            return s;
        }
        catch (Exception ex) {
            s = null;
            return s;
        }
    }
    
    public static String get(String s, final String s2) {
        try {
            s = (String)SystemProperties.CLASS.getMethod("get", String.class, String.class).invoke(null, s, s2);
            return s;
        }
        catch (Exception ex) {
            s = s2;
            return s;
        }
    }
    
    public static boolean getBoolean(final String s, boolean booleanValue) {
        try {
            booleanValue = (boolean)SystemProperties.CLASS.getMethod("getBoolean", String.class, Boolean.TYPE).invoke(null, s, booleanValue);
            return booleanValue;
        }
        catch (Exception ex) {
            return booleanValue;
        }
    }
    
    public static float getFloat(final String s, final float n) {
        try {
            final String value = get(s);
            float float1 = n;
            if (value != null) {
                float1 = Float.parseFloat(value);
            }
            return float1;
        }
        catch (Exception ex) {
            Log.w(SystemProperties.TAG, "Failed to parse " + s + " as float - " + ex.getMessage());
            return n;
        }
    }
    
    public static int getInt(final String s, int intValue) {
        try {
            intValue = (int)SystemProperties.CLASS.getMethod("getInt", String.class, Integer.TYPE).invoke(null, s, intValue);
            return intValue;
        }
        catch (Exception ex) {
            return intValue;
        }
    }
    
    public static long getLong(final String s, long longValue) {
        try {
            longValue = (long)SystemProperties.CLASS.getMethod("getLong", String.class, Long.TYPE).invoke(null, s, longValue);
            return longValue;
        }
        catch (Exception ex) {
            return longValue;
        }
    }
    
    public static void set(final String s, final String s2) {
        try {
            SystemProperties.CLASS.getMethod("set", String.class, String.class).invoke(null, s, s2);
        }
        catch (Exception ex) {
            Log.e(SystemProperties.TAG, "Unable to set prop " + s + " to value:" + s2 + " because of:" + ex.getMessage());
        }
    }
}
