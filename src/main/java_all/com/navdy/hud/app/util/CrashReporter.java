package com.navdy.hud.app.util;

import net.hockeyapp.android.ExceptionHandler;
import com.navdy.service.library.util.LogUtils;
import android.app.Application;
import net.hockeyapp.android.metrics.MetricsManager;
import java.io.FilenameFilter;
import android.text.TextUtils;
import java.lang.reflect.Field;
import android.util.Log;
import java.util.regex.Matcher;
import com.navdy.service.library.util.IOUtils;
import net.hockeyapp.android.CrashManagerListener;
import net.hockeyapp.android.CrashManager;
import android.content.Context;
import java.lang.ref.WeakReference;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import java.io.File;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.service.library.task.TaskManager;
import android.os.Looper;
import android.os.Build;
import java.util.concurrent.TimeUnit;
import android.os.Handler;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.profile.DriverProfileManager;
import java.util.regex.Pattern;

public final class CrashReporter
{
    private static final int ANR_LOG_LIMIT = 32768;
    private static final String ANR_PATH = "/data/anr/traces.txt";
    public static final String HOCKEY_APP_ID = "20625a4f769df641ecf46825a71c1911";
    public static final String HOCKEY_APP_STABLE_ID = "4dd8d996ee304ef2994965aa7f6d4f15";
    public static final String HOCKEY_APP_UNSTABLE_ID = "ad4b9dbcbb3b4083a49af48878026e5a";
    private static final int KERNEL_CRASH_INFO_LIMIT = 32768;
    private static final String LOG_ANR_MARKER = "Wrote stack traces to '/data/anr/traces.txt'";
    private static final String LOG_BEGIN_MARKER = "--------- beginning of main";
    private static final int PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL;
    private static final int PERIOD_DELIVERY_CHECK_INTERVAL;
    private static final Pattern START_OF_OOPS;
    private static final int SYSTEM_LOG_LIMIT = 32768;
    private static final String TOMBSTONE_FILE_PATTERN = "tombstone_light_";
    private static final String TOMBSTONE_PATH = "/data/tombstones";
    public static final Pattern USER_REBOOT;
    private static DriverProfileManager driverProfileManager;
    private static final CrashReporter sInstance;
    private static final Logger sLogger;
    private static volatile boolean stopCrashReporting;
    private static String userId;
    private NavdyCrashListener crashManagerListener;
    private Runnable deliveryRunnable;
    private Handler handler;
    private boolean installed;
    private OTAUpdateService.OTAFailedException otaFailure;
    private Runnable periodicRunnable;
    
    static {
        sLogger = new Logger(CrashReporter.class);
        START_OF_OOPS = Pattern.compile("(Unable to handle)|(Internal error:)");
        USER_REBOOT = Pattern.compile("(reboot: Restarting system with command)|(do_powerctl:)");
        PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL = (int)TimeUnit.MINUTES.toMillis(2L);
        PERIOD_DELIVERY_CHECK_INTERVAL = (int)TimeUnit.MINUTES.toMillis(5L);
        CrashReporter.userId = Build.SERIAL;
        sInstance = new CrashReporter();
    }
    
    private CrashReporter() {
        this.crashManagerListener = new NavdyCrashListener();
        this.handler = new Handler(Looper.getMainLooper());
        this.periodicRunnable = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(CrashReporter.this.deliveryRunnable, 1);
            }
        };
        this.deliveryRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    if (CrashReporter.this.checkForCrashes()) {
                        CrashReporter.this.handler.postDelayed(CrashReporter.this.periodicRunnable, (long)CrashReporter.PERIOD_DELIVERY_CHECK_INTERVAL);
                    }
                }
                catch (Throwable t) {
                    CrashReporter.sLogger.e(t);
                    if (true) {
                        CrashReporter.this.handler.postDelayed(CrashReporter.this.periodicRunnable, (long)CrashReporter.PERIOD_DELIVERY_CHECK_INTERVAL);
                    }
                }
                finally {
                    if (true) {
                        CrashReporter.this.handler.postDelayed(CrashReporter.this.periodicRunnable, (long)CrashReporter.PERIOD_DELIVERY_CHECK_INTERVAL);
                    }
                }
            }
        };
        this.otaFailure = null;
        this.handler.postDelayed(this.periodicRunnable, (long)CrashReporter.PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL);
        CrashReporter.driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
    }
    
    private boolean checkForCrashes() throws Throwable {
        if (NetworkBandwidthController.getInstance().isNetworkAccessAllowed(NetworkBandwidthController.Component.HOCKEY)) {
            try {
                CrashManager.submitStackTraces(new WeakReference<Context>(HudApplication.getAppContext()), this.crashManagerListener);
            }
            catch (Throwable t) {
                CrashReporter.sLogger.e(t);
            }
        }
        return true;
    }
    
    private void cleanupAnrFiles() {
        final File file = new File("/data/anr/traces.txt");
        if (file.exists()) {
            CrashReporter.sLogger.v("delete /data/anr/traces.txt :" + file.delete());
        }
    }
    
    private void cleanupKernelCrashes(final String[] array) {
        for (final String s : array) {
            final File file = new File(s);
            if (file.exists()) {
                CrashReporter.sLogger.v("Trying to delete " + s);
                if (!file.delete()) {
                    CrashReporter.sLogger.e("Unable to delete kernel crash file: " + s);
                }
            }
        }
    }
    
    private String filterKernelCrash(final File file) {
        String s = null;
        String s3;
        try {
            final String s2 = s3 = IOUtils.convertFileToString(file.getAbsolutePath());
            if (s2 != null) {
                s = s2;
                if (!CrashReporter.USER_REBOOT.matcher(s2).find()) {
                    s = s2;
                    final Matcher matcher = CrashReporter.START_OF_OOPS.matcher(s2);
                    s = s2;
                    int start;
                    final int n = start = s2.length() - 32768;
                    s = s2;
                    if (matcher.find()) {
                        start = n;
                        s = s2;
                        if (matcher.start() < n) {
                            s = s2;
                            start = matcher.start();
                        }
                    }
                    s = s2;
                    final int max = Math.max(0, start);
                    s = s2;
                    s3 = s2.substring(max, Math.min(s2.length() - max, 32768));
                }
                else {
                    s = s2;
                    CrashReporter.sLogger.v("Ignoring user reboot log");
                    s3 = null;
                }
            }
            return s3;
        }
        catch (Throwable t) {
            CrashReporter.sLogger.e(t);
            s3 = s;
            return s3;
        }
        return s3;
    }
    
    public static CrashReporter getInstance() {
        return CrashReporter.sInstance;
    }
    
    private void handleUncaughtException(final Thread thread, final Throwable t, final Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        if (CrashReporter.stopCrashReporting) {
            CrashReporter.sLogger.i("FATAL-reporting-turned-off", t);
        }
        else {
            CrashReporter.stopCrashReporting = true;
            Log.e("FATAL-CRASH", "FATAL-CRASH" + " Uncaught exception - " + thread.getName() + ":" + thread.getId() + " ui thread id:" + Looper.getMainLooper().getThread().getId(), t);
            CrashReporter.sLogger.i("closing logger");
            Logger.close();
            if (uncaughtExceptionHandler != null) {
                uncaughtExceptionHandler.uncaughtException(thread, t);
            }
        }
    }
    
    public static boolean isEnabled() {
        return DeviceUtil.isNavdyDevice() && !HudApplication.isDeveloperBuild();
    }
    
    private void saveHockeyAppException(final Throwable t) {
        try {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        CrashReporter.this.crashManagerListener.saveException(t);
                    }
                    catch (Throwable t) {
                        CrashReporter.sLogger.e(t);
                    }
                }
            }, 1);
        }
        catch (Throwable t) {
            CrashReporter.sLogger.e(t);
        }
    }
    
    private static void setCrashReporting(final boolean stopCrashReporting) {
        CrashReporter.stopCrashReporting = stopCrashReporting;
    }
    
    private void setHockeyAppCrashUploading(final boolean b) {
        boolean b2 = true;
        try {
            CrashReporter.sLogger.v("setHockeyAppCrashUploading: try to enabled:" + b);
            final Field declaredField = CrashManager.class.getDeclaredField("submitting");
            declaredField.setAccessible(true);
            if (b) {
                b2 = false;
            }
            declaredField.set(null, b2);
            CrashReporter.sLogger.v("setHockeyAppCrashUploading: enabled=" + b);
        }
        catch (Throwable t) {
            CrashReporter.sLogger.e("setHockeyAppCrashUploading", t);
        }
    }
    
    public void checkForAnr() {
        GenericUtil.checkNotOnMainThread();
        try {
            CrashReporter.sLogger.v("checking for anr's");
            if (new File("/data/anr/traces.txt").exists()) {
                final String convertFileToString = IOUtils.convertFileToString("/data/anr/traces.txt");
                if (!TextUtils.isEmpty((CharSequence)convertFileToString)) {
                    this.reportNonFatalException(new AnrException(convertFileToString));
                    CrashReporter.sLogger.v("anr crash created size[" + convertFileToString.length() + "]");
                }
                this.cleanupAnrFiles();
            }
            else {
                CrashReporter.sLogger.v("no anr files");
            }
        }
        catch (Throwable t) {
            CrashReporter.sLogger.e(t);
        }
    }
    
    public void checkForKernelCrashes(final String[] array) {
        if (array != null && array.length != 0) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        CrashReporter.sLogger.v("checking for kernel crashes");
                        final File file = null;
                        final String[] val$kernelCrashFiles = array;
                        final int length = val$kernelCrashFiles.length;
                        int n = 0;
                        File file2;
                        while (true) {
                            file2 = file;
                            if (n >= length) {
                                break;
                            }
                            final File file3 = new File(val$kernelCrashFiles[n]);
                            if (file3.exists()) {
                                file2 = file3;
                                CrashReporter.sLogger.v("found file:" + file3.getAbsolutePath());
                                break;
                            }
                            ++n;
                        }
                        if (file2 != null) {
                            final String access$900 = CrashReporter.this.filterKernelCrash(file2);
                            if (!TextUtils.isEmpty((CharSequence)access$900)) {
                                CrashReporter.this.reportNonFatalException(new KernelCrashException(access$900));
                                CrashReporter.sLogger.v("kernel crash created");
                            }
                            CrashReporter.this.cleanupKernelCrashes(array);
                        }
                        else {
                            CrashReporter.sLogger.v("no kernel crash files");
                        }
                    }
                    catch (Throwable t) {
                        CrashReporter.sLogger.e(t);
                    }
                }
            }, 1);
        }
    }
    
    public void checkForOTAFailure() {
        synchronized (this) {
            if (this.otaFailure != null) {
                this.reportNonFatalException(this.otaFailure);
                this.otaFailure = null;
            }
        }
    }
    
    public void checkForTombstones() {
        GenericUtil.checkNotOnMainThread();
        try {
            CrashReporter.sLogger.v("checking for tombstones");
            final File file = new File("/data/tombstones");
            if (file.exists() && file.isDirectory()) {
                final String[] list = file.list(new FilenameFilter() {
                    @Override
                    public boolean accept(final File file, final String s) {
                        return s.contains("tombstone_light_");
                    }
                });
                if (list == null || list.length == 0) {
                    CrashReporter.sLogger.v("no tombstones");
                }
                else {
                    for (int i = 0; i < list.length; ++i) {
                        final File file2 = new File("/data/tombstones" + File.separator + list[i]);
                        if (file2.exists()) {
                            final String absolutePath = file2.getAbsolutePath();
                            final String convertFileToString = IOUtils.convertFileToString(absolutePath);
                            if (!TextUtils.isEmpty((CharSequence)convertFileToString)) {
                                this.reportNonFatalException(new TombstoneException(convertFileToString));
                                CrashReporter.sLogger.v("tombstone exception logged file[" + absolutePath + "] size[" + convertFileToString.length() + "]");
                            }
                            CrashReporter.sLogger.v("tombstone deleted: " + file2.delete());
                        }
                    }
                }
            }
            else {
                CrashReporter.sLogger.v("no tombstones");
            }
        }
        catch (Throwable t) {
            CrashReporter.sLogger.e(t);
        }
    }
    
    public void installCrashHandler(Context defaultUncaughtExceptionHandler) {
        Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler2;
        String s;
        Label_0119_Outer:Label_0201_Outer:
        while (true) {
            while (true) {
            Label_0250:
                while (true) {
                Label_0243:
                    while (true) {
                        Label_0231: {
                            synchronized (this) {
                                if (!this.installed) {
                                    if (!DeviceUtil.isUserBuild()) {
                                        CrashReporter.userId = Build.SERIAL + "-eng";
                                    }
                                    defaultUncaughtExceptionHandler2 = Thread.getDefaultUncaughtExceptionHandler();
                                    if (defaultUncaughtExceptionHandler2 == null) {
                                        break Label_0231;
                                    }
                                    CrashReporter.sLogger.v("default uncaught handler:" + defaultUncaughtExceptionHandler2.getClass().getName());
                                    this.setHockeyAppCrashUploading(false);
                                    if (!DeviceUtil.isUserBuild()) {
                                        break Label_0243;
                                    }
                                    s = "20625a4f769df641ecf46825a71c1911";
                                    CrashManager.register(defaultUncaughtExceptionHandler, s, this.crashManagerListener);
                                    MetricsManager.register(defaultUncaughtExceptionHandler, HudApplication.getApplication(), s);
                                    CrashReporter.sLogger.v("enabled metrics manager");
                                    this.setHockeyAppCrashUploading(true);
                                    defaultUncaughtExceptionHandler = (Context)Thread.getDefaultUncaughtExceptionHandler();
                                    if (defaultUncaughtExceptionHandler == null) {
                                        break Label_0250;
                                    }
                                    CrashReporter.sLogger.v("hockey uncaught handler:" + defaultUncaughtExceptionHandler.getClass().getName());
                                    Thread.setDefaultUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)new Thread.UncaughtExceptionHandler() {
                                        @Override
                                        public void uncaughtException(final Thread thread, final Throwable t) {
                                            CrashReporter.this.handleUncaughtException(thread, t, defaultUncaughtExceptionHandler);
                                        }
                                    });
                                    this.installed = true;
                                }
                                return;
                            }
                        }
                        CrashReporter.sLogger.v("default uncaught handler is null");
                        continue Label_0119_Outer;
                    }
                    s = "4dd8d996ee304ef2994965aa7f6d4f15";
                    continue Label_0201_Outer;
                }
                CrashReporter.sLogger.v("default hockey handler is null");
                continue;
            }
        }
    }
    
    public boolean isInstalled() {
        return this.installed;
    }
    
    public void log(final String s) {
    }
    
    public void reportNonFatalException(final Throwable t) {
        if (!this.installed) {
            return;
        }
        try {
            this.saveHockeyAppException(t);
        }
        catch (Throwable t) {
            CrashReporter.sLogger.e(t);
        }
    }
    
    public void reportOTAFailure(final OTAUpdateService.OTAFailedException otaFailure) {
        synchronized (this) {
            if (this.installed) {
                this.reportNonFatalException(otaFailure);
            }
            else {
                if (this.otaFailure != null) {
                    CrashReporter.sLogger.e("multiple OTAFailedException reports");
                }
                this.otaFailure = otaFailure;
            }
        }
    }
    
    public void stopCrashReporting(final boolean crashReporting) {
        setCrashReporting(crashReporting);
    }
    
    public static class AnrException extends Exception
    {
        public AnrException(final String s) {
            super(s);
        }
    }
    
    public static class CrashException extends Exception
    {
        public CrashException(final String s) {
            super(s);
        }
        
        String extractCrashLocation(final String s) {
            final String s2 = "";
            final Pattern locationPattern = this.getLocationPattern();
            String s3 = s2;
            if (locationPattern != null) {
                s3 = s2;
                if (s != null) {
                    final Matcher matcher = locationPattern.matcher(s);
                    s3 = s2;
                    if (matcher.find()) {
                        if (matcher.groupCount() >= 1) {
                            int n = 1;
                            while (true) {
                                s3 = s2;
                                if (n > matcher.groupCount()) {
                                    break;
                                }
                                if (matcher.group(n) != null) {
                                    s3 = matcher.group(n);
                                    break;
                                }
                                ++n;
                            }
                        }
                        else {
                            s3 = matcher.group();
                        }
                    }
                }
            }
            return s3;
        }
        
        public String filter(final String s) {
            return s;
        }
        
        public String getCrashLocation() {
            return this.extractCrashLocation(this.getLocalizedMessage());
        }
        
        public Pattern getLocationPattern() {
            return null;
        }
        
        @Override
        public String toString() {
            final String simpleName = this.getClass().getSimpleName();
            final String localizedMessage = this.getLocalizedMessage();
            String filter;
            if (localizedMessage != null) {
                filter = this.filter(localizedMessage);
            }
            else {
                filter = "";
            }
            return simpleName + ": " + this.getCrashLocation() + filter;
        }
    }
    
    public static class KernelCrashException extends CrashException
    {
        private static final Pattern stackMatcher;
        
        static {
            stackMatcher = Pattern.compile("PC is at\\s+([^\n]+\n)");
        }
        
        public KernelCrashException(final String s) {
            super(s);
        }
        
        @Override
        public Pattern getLocationPattern() {
            return KernelCrashException.stackMatcher;
        }
    }
    
    public static class NavdyCrashListener extends CrashManagerListener
    {
        public Throwable exception;
        
        public NavdyCrashListener() {
            this.exception = null;
        }
        
        @Override
        public String getContact() {
            return CrashReporter.driverProfileManager.getLastUserEmail();
        }
        
        @Override
        public String getDescription() {
            String s;
            if (this.exception instanceof TombstoneException) {
                s = ((TombstoneException)this.exception).description;
            }
            else if (this.exception instanceof AnrException) {
                s = LogUtils.systemLogStr(32768, "Wrote stack traces to '/data/anr/traces.txt'");
            }
            else if (this.exception instanceof KernelCrashException) {
                s = LogUtils.systemLogStr(32768, "--------- beginning of main");
            }
            else if (this.exception instanceof OTAUpdateService.OTAFailedException) {
                final OTAUpdateService.OTAFailedException ex = (OTAUpdateService.OTAFailedException)this.exception;
                s = ex.last_install + "\n" + ex.last_log;
            }
            else {
                s = LogUtils.systemLogStr(32768, null);
            }
            return s;
        }
        
        @Override
        public String getUserID() {
            return CrashReporter.userId;
        }
        
        public void saveException(final Throwable exception) {
            ExceptionHandler.saveException(this.exception = exception, this);
            this.exception = null;
        }
        
        @Override
        public boolean shouldAutoUploadCrashes() {
            return true;
        }
    }
    
    public static class TombstoneException extends CrashException
    {
        private static final Pattern stackMatcher;
        public final String description;
        
        static {
            stackMatcher = Pattern.compile("Abort message: ('[^']+'\n)|#00 pc \\w+\\s+([^\n]+\n)");
        }
        
        public TombstoneException(final String description) {
            super("");
            this.description = description;
        }
        
        @Override
        public String getCrashLocation() {
            return ((CrashException)this).extractCrashLocation(this.description);
        }
        
        @Override
        public Pattern getLocationPattern() {
            return TombstoneException.stackMatcher;
        }
    }
}
