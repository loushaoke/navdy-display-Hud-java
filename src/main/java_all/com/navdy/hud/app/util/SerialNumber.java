package com.navdy.hud.app.util;

import android.util.Log;
import android.os.Build;

public class SerialNumber
{
    public static SerialNumber instance;
    public final String configurationCode;
    public final int dayOfWeek;
    public final String factory;
    public final String revisionCode;
    public final String serialNo;
    public final int week;
    public final int year;
    
    static {
        SerialNumber.instance = parse(Build.SERIAL);
    }
    
    public SerialNumber(final String factory, final int year, final int week, final int dayOfWeek, final String serialNo, final String configurationCode, final String revisionCode) {
        this.factory = factory;
        this.year = year;
        this.week = week;
        this.dayOfWeek = dayOfWeek;
        this.serialNo = serialNo;
        this.configurationCode = configurationCode;
        this.revisionCode = revisionCode;
    }
    
    public static SerialNumber parse(final String s) {
        SerialNumber serialNumber;
        if (s == null || s.length() != 16) {
            serialNumber = new SerialNumber("NDY", 0, 0, 0, "0", "0", "0");
        }
        else {
            final String substring = s.substring(0, 3);
            final int number = parseNumber(s.substring(3, 4), 0, "year");
            final int number2 = parseNumber(s.substring(4, 6), 1, "week");
            int n;
            if (number2 < 1 || (n = number2) > 52) {
                n = 1;
            }
            final int number3 = parseNumber(s.substring(6, 7), 1, "dayOfWeek");
            int n2;
            if (number3 < 1 || (n2 = number3) > 7) {
                n2 = 1;
            }
            serialNumber = new SerialNumber(substring, number, n, n2, s.substring(7, 10), s.substring(10, 14), s.substring(14, 15));
        }
        return serialNumber;
    }
    
    private static int parseNumber(final String s, int int1, final String s2) {
        try {
            int1 = Integer.parseInt(s);
            return int1;
        }
        catch (NumberFormatException ex) {
            Log.e("SERIAL", "Invalid serial number field " + s2 + " value:" + s);
            return int1;
        }
    }
}
