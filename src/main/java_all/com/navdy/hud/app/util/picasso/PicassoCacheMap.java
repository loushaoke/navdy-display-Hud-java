package com.navdy.hud.app.util.picasso;

import java.util.LinkedHashMap;

public class PicassoCacheMap<K, V> extends LinkedHashMap<K, V>
{
    private PicassoItemCacheListener listener;
    
    PicassoCacheMap(final int n, final float n2) {
        super(n, n2, true);
    }
    
    @Override
    public V put(final K k, final V v) {
        final V put = super.put(k, v);
        this.listener.itemAdded(k);
        return put;
    }
    
    @Override
    public V remove(final Object o) {
        this.listener.itemRemoved(o);
        return super.remove(o);
    }
    
    void setListener(final PicassoItemCacheListener listener) {
        this.listener = listener;
    }
}
