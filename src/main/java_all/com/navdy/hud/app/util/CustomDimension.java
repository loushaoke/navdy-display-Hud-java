package com.navdy.hud.app.util;

import android.content.res.TypedArray;
import android.view.View;
import android.util.TypedValue;

public class CustomDimension
{
    private TypedValue mAttribute;
    private float mValue;
    
    public CustomDimension(final float mValue) {
        this.mValue = mValue;
    }
    
    public CustomDimension(final TypedValue mAttribute) {
        this.mAttribute = mAttribute;
    }
    
    public static CustomDimension getDimension(final View view, final TypedArray typedArray, final int n, final float n2) {
        CustomDimension customDimension;
        if (view.isInEditMode()) {
            final String string = typedArray.getString(n);
            if (string == null) {
                customDimension = new CustomDimension(n2);
            }
            else if (string.endsWith("%") || string.endsWith("%p")) {
                final TypedValue typedValue = new TypedValue();
                typedArray.getValue(n, typedValue);
                customDimension = new CustomDimension(typedValue);
            }
            else {
                customDimension = new CustomDimension(typedArray.getDimension(n, n2));
            }
        }
        else {
            final TypedValue typedValue2 = new TypedValue();
            typedArray.getValue(n, typedValue2);
            if (typedValue2.type == 0) {
                customDimension = new CustomDimension(n2);
            }
            else if (typedValue2.type == 5) {
                customDimension = new CustomDimension(typedArray.getDimension(n, n2));
            }
            else {
                customDimension = new CustomDimension(typedValue2);
            }
        }
        return customDimension;
    }
    
    public static boolean hasDimension(final View view, final TypedArray typedArray, final int n) {
        boolean b = true;
        if (view.isInEditMode()) {
            if (typedArray.getString(n) == null) {
                b = false;
            }
        }
        else {
            final TypedValue typedValue = new TypedValue();
            typedArray.getValue(n, typedValue);
            if (typedValue.type == 0) {
                b = false;
            }
        }
        return b;
    }
    
    public float getSize(final View view, float n, final float n2) {
        if (this.mAttribute != null) {
            switch (this.mAttribute.type) {
                default:
                    throw new IllegalArgumentException("Attribute must have type fraction or float - it is " + this.mAttribute.type);
                case 6:
                    n = this.mAttribute.getFraction(n, n2);
                    break;
                case 4:
                    n *= this.mAttribute.getFloat();
                    break;
            }
        }
        else {
            n = this.mValue;
        }
        return n;
    }
}
