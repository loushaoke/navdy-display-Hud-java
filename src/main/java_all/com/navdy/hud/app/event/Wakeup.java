package com.navdy.hud.app.event;

import com.navdy.hud.app.analytics.AnalyticsSupport;

public class Wakeup
{
    public AnalyticsSupport.WakeupReason reason;
    
    public Wakeup(final AnalyticsSupport.WakeupReason reason) {
        this.reason = reason;
    }
}
