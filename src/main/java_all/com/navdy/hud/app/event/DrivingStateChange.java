package com.navdy.hud.app.event;

public class DrivingStateChange
{
    public final boolean driving;
    
    public DrivingStateChange(final boolean driving) {
        this.driving = driving;
    }
}
