package com.navdy.hud.app.obd;

import com.navdy.hud.app.service.ObdCANBusDataUploadService;
import java.util.ArrayList;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.text.TextUtils;
import com.navdy.service.library.util.IOUtils;
import com.navdy.hud.app.HudApplication;
import java.io.IOException;
import java.util.Date;
import com.navdy.hud.app.util.DeviceUtil;
import java.util.Comparator;
import com.navdy.hud.app.util.CrashReportService;
import com.navdy.hud.app.framework.trips.TripManager;
import android.content.SharedPreferences;
import android.os.Handler;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.PriorityBlockingQueue;
import java.text.SimpleDateFormat;
import java.io.File;

public class ObdCanBusRecordingPolicy
{
    private static final long CAN_BUS_MONITORING_DISTANCE_LIMIT_METERS = 40234L;
    public static final String CAN_PROTOCOL_PREFIX = "ISO 15765-4";
    public static final String FILES_TO_UPLOAD_DIRECTORY = "/sdcard/.canBusLogs/upload";
    public static final int MAX_META_DATA_FILES = 10;
    public static final int MAX_UPLOAD_FILES = 5;
    public static final String META_DATA_FILE = "/sdcard/.canBusLogs/upload/meta_data_";
    public static final int MINIMUM_TIME_FOR_MOTION_DETECTION = 10000;
    public static final int MINIMUM_TIME_FOR_STOP_DETECTION = 10000;
    private static final float MOVING_SPEED_METERS_PER_SECOND = 1.34112f;
    public static final String PREFERENCE_NAVDY_MILES_WHEN_LISTENING_STARTED = "preference_navdy_miles_when_listening_started";
    public static final String PREF_CAN_BUS_DATA_RECORDED_AND_SENT = "canBusDataRecordedAndSent";
    public static final String PREF_CAN_BUS_DATA_SENT_VERSION = "canBusDataSentVersion";
    public static final String PREF_CAN_BUS_DATA_STATUS_REPORTED_VERSION = "canBusDataStatusReportedVersion";
    private static final File UPLOAD_DIRECTORY_FILE;
    private static final SimpleDateFormat dateFormat;
    private static PriorityBlockingQueue<File> metaDataFiles;
    private static final Logger sLogger;
    private static PriorityBlockingQueue<File> uploadFiles;
    private int canBusMonitoringDataSize;
    private CanBusMonitoringState canBusMonitoringState;
    private Context context;
    private File currentMetaDataFile;
    private long firstTimeMovementDetected;
    private long firstTimeStoppingDetected;
    private Handler handler;
    private boolean isCanBusMonitoringLimitReached;
    private boolean isCanProtocol;
    private boolean isEngineeringBuild;
    private boolean isInstantaneousModeOn;
    private boolean isObdConnected;
    int lastSpeed;
    private String make;
    private String model;
    private boolean motionDetected;
    private ObdManager obdManager;
    private int requiredObdDataVersion;
    private SharedPreferences sharedPreferences;
    private TripManager tripManager;
    private String vin;
    private String year;
    
    static {
        sLogger = new Logger(ObdCanBusRecordingPolicy.class);
        UPLOAD_DIRECTORY_FILE = new File("/sdcard/.canBusLogs/upload");
        dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
        ObdCanBusRecordingPolicy.metaDataFiles = new PriorityBlockingQueue<File>(10, new CrashReportService.FilesModifiedTimeComparator());
        ObdCanBusRecordingPolicy.uploadFiles = new PriorityBlockingQueue<File>(10, new CrashReportService.FilesModifiedTimeComparator());
    }
    
    public ObdCanBusRecordingPolicy(final Context context, final SharedPreferences sharedPreferences, final ObdManager obdManager, final TripManager tripManager) {
        this.isEngineeringBuild = !DeviceUtil.isUserBuild();
        this.firstTimeMovementDetected = 0L;
        this.firstTimeStoppingDetected = 0L;
        this.isObdConnected = false;
        this.isCanProtocol = false;
        this.isInstantaneousModeOn = false;
        this.motionDetected = false;
        this.lastSpeed = -1;
        this.isCanBusMonitoringLimitReached = false;
        this.canBusMonitoringState = CanBusMonitoringState.UNKNOWN;
        this.canBusMonitoringDataSize = 0;
        this.obdManager = obdManager;
        this.context = context;
        this.tripManager = tripManager;
        this.sharedPreferences = sharedPreferences;
        this.requiredObdDataVersion = context.getResources().getInteger(R.integer.obd_data_version);
        if (!ObdCanBusRecordingPolicy.UPLOAD_DIRECTORY_FILE.exists()) {
            ObdCanBusRecordingPolicy.UPLOAD_DIRECTORY_FILE.mkdirs();
        }
        this.populateFilesQueue();
        this.handler = new Handler();
    }
    
    private File getMetaDataFile() {
        final File file = new File("/sdcard/.canBusLogs/upload/meta_data_" + ObdCanBusRecordingPolicy.dateFormat.format(new Date(System.currentTimeMillis())) + ".txt");
        final File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            ObdCanBusRecordingPolicy.sLogger.d("Upload directory not found, creating");
            parentFile.mkdirs();
        }
        ObdCanBusRecordingPolicy.sLogger.d("Creating Meta data file " + file.getName());
        if (file.exists()) {
            return file;
        }
        try {
            file.createNewFile();
            return file;
        }
        catch (IOException ex) {
            ObdCanBusRecordingPolicy.sLogger.e("IOException while creating file ", ex);
            return file;
        }
    }
    
    private void populateFilesQueue() {
        ObdCanBusRecordingPolicy.metaDataFiles.clear();
        ObdCanBusRecordingPolicy.uploadFiles.clear();
        for (final File file : ObdCanBusRecordingPolicy.UPLOAD_DIRECTORY_FILE.listFiles()) {
            if (file.isFile() && file.getName().startsWith("meta_data")) {
                ObdCanBusRecordingPolicy.metaDataFiles.add(file);
                if (ObdCanBusRecordingPolicy.metaDataFiles.size() > 10) {
                    final File file2 = ObdCanBusRecordingPolicy.metaDataFiles.poll();
                    if (file2 != null) {
                        ObdCanBusRecordingPolicy.sLogger.d("Deleting meta data file " + file2.getName() + ", As its old");
                        IOUtils.deleteFile(HudApplication.getAppContext(), file2.getAbsolutePath());
                    }
                }
            }
            else if (file.isFile() && file.getName().startsWith("CAN_BUS_DATA_")) {
                ObdCanBusRecordingPolicy.uploadFiles.add(file);
                if (ObdCanBusRecordingPolicy.uploadFiles.size() > 5) {
                    final File file3 = ObdCanBusRecordingPolicy.uploadFiles.poll();
                    if (file3 != null) {
                        ObdCanBusRecordingPolicy.sLogger.d("Deleting upload file " + file3.getName() + ", As its old");
                        IOUtils.deleteFile(HudApplication.getAppContext(), file3.getAbsolutePath());
                    }
                }
            }
        }
    }
    
    private void reportBusMonitoringState() {
        final boolean b = true;
        int canBusMonitoringDataSize = 0;
        final boolean b2 = this.sharedPreferences.getInt("canBusDataStatusReportedVersion", -1) == this.requiredObdDataVersion;
        ObdCanBusRecordingPolicy.sLogger.d("reportBusMonitoringState, Already reported ? : " + b2);
        if (!b2) {
            ObdCanBusRecordingPolicy.sLogger.d("Reporting, Make " + this.make + ", Model : " + this.model + ", Year : " + this.year + ", State : " + this.canBusMonitoringState);
            if (!TextUtils.isEmpty((CharSequence)this.make) && !TextUtils.isEmpty((CharSequence)this.model) && !TextUtils.isEmpty((CharSequence)this.year) && this.canBusMonitoringState != CanBusMonitoringState.UNKNOWN) {
                final String vin = this.vin;
                final boolean b3 = this.canBusMonitoringState == CanBusMonitoringState.SUCCESS && b;
                if (this.canBusMonitoringState == CanBusMonitoringState.SUCCESS) {
                    canBusMonitoringDataSize = this.canBusMonitoringDataSize;
                }
                AnalyticsSupport.recordObdCanBusMonitoringState(vin, b3, canBusMonitoringDataSize, this.requiredObdDataVersion);
                this.sharedPreferences.edit().putInt("canBusDataStatusReportedVersion", this.requiredObdDataVersion).apply();
            }
        }
    }
    
    public boolean isCanBusMonitoringLimitReached() {
        return this.isCanBusMonitoringLimitReached;
    }
    
    public boolean isCanBusMonitoringNeeded() {
        final boolean boolean1 = this.sharedPreferences.getBoolean("canBusDataRecordedAndSent", false);
        final int int1 = this.sharedPreferences.getInt("canBusDataSentVersion", -1);
        ObdCanBusRecordingPolicy.sLogger.d("IsCanBusMonitoringNeeded : ? Is Engineering Build " + this.isEngineeringBuild + "Data record sent : " + boolean1 + ", Sent Data Version : " + int1 + ", isMoving : " + this.motionDetected + ", InstantaneousMode : " + this.isInstantaneousModeOn + ", Obd Connected : " + this.isObdConnected + ", Is CAN protocol :" + this.isCanProtocol);
        final boolean b = this.isEngineeringBuild && (!boolean1 || int1 != this.requiredObdDataVersion) && this.motionDetected && !this.isInstantaneousModeOn && this.isObdConnected && this.isCanProtocol;
        if (b) {
            final long long1 = this.sharedPreferences.getLong("preference_navdy_miles_when_listening_started", -1L);
            final long totalDistanceTravelledWithNavdy = this.tripManager.getTotalDistanceTravelledWithNavdy();
            if (long1 == -1L) {
                this.sharedPreferences.edit().putLong("preference_navdy_miles_when_listening_started", totalDistanceTravelledWithNavdy).apply();
            }
            else if (totalDistanceTravelledWithNavdy - long1 > 40234L) {
                ObdCanBusRecordingPolicy.sLogger.d("CAN bus monitoring limit reached, Distance recorded when listening started " + long1 + ", Current distance travelled " + totalDistanceTravelledWithNavdy);
                this.isCanBusMonitoringLimitReached = true;
            }
        }
        else {
            this.isCanBusMonitoringLimitReached = false;
        }
        return b;
    }
    
    public void onCanBusMonitorSuccess(final int canBusMonitoringDataSize) {
        this.canBusMonitoringState = CanBusMonitoringState.SUCCESS;
        this.canBusMonitoringDataSize = canBusMonitoringDataSize;
        this.reportBusMonitoringState();
    }
    
    public void onCanBusMonitoringFailed() {
        final double n = SpeedManager.getInstance().getObdSpeed();
        final double n2 = this.obdManager.getEngineRpm();
        ObdCanBusRecordingPolicy.sLogger.d("onCanBusMonitoringFailed , RawSpeed : " + n + ", RPM : " + n2);
        if (n > 0.0 || n2 > 0.0) {
            this.canBusMonitoringState = CanBusMonitoringState.FAILURE;
            this.reportBusMonitoringState();
        }
    }
    
    public void onCarDetailsAvailable(final String p0, final String p1, final String p2) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: dup            
        //     2: astore          4
        //     4: monitorenter   
        //     5: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.sLogger:Lcom/navdy/service/library/log/Logger;
        //     8: astore          5
        //    10: new             Ljava/lang/StringBuilder;
        //    13: astore          6
        //    15: aload           6
        //    17: invokespecial   java/lang/StringBuilder.<init>:()V
        //    20: aload           5
        //    22: aload           6
        //    24: ldc_w           "onCarDetailsAvailable Make : "
        //    27: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    30: aload_1        
        //    31: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    34: ldc_w           " , Model : "
        //    37: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    40: aload_2        
        //    41: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    44: ldc_w           ", Year : "
        //    47: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    50: aload_3        
        //    51: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    54: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    57: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    60: aload_0        
        //    61: invokespecial   com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.reportBusMonitoringState:()V
        //    64: aload_0        
        //    65: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.make:Ljava/lang/String;
        //    68: aload_1        
        //    69: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    72: ifeq            97
        //    75: aload_0        
        //    76: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.model:Ljava/lang/String;
        //    79: aload_2        
        //    80: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    83: ifeq            97
        //    86: aload_0        
        //    87: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.year:Ljava/lang/String;
        //    90: aload_3        
        //    91: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    94: ifne            391
        //    97: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.sLogger:Lcom/navdy/service/library/log/Logger;
        //   100: ldc_w           "Car details changed"
        //   103: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   106: aload_0        
        //   107: aload_1        
        //   108: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.make:Ljava/lang/String;
        //   111: aload_0        
        //   112: aload_2        
        //   113: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.model:Ljava/lang/String;
        //   116: aload_0        
        //   117: aload_3        
        //   118: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.year:Ljava/lang/String;
        //   121: aload_0        
        //   122: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.isObdConnected:Z
        //   125: ifeq            335
        //   128: aload_0        
        //   129: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.currentMetaDataFile:Ljava/io/File;
        //   132: astore          5
        //   134: aload           5
        //   136: ifnull          335
        //   139: aconst_null    
        //   140: astore          7
        //   142: aconst_null    
        //   143: astore          8
        //   145: aconst_null    
        //   146: astore          9
        //   148: aconst_null    
        //   149: astore          10
        //   151: aconst_null    
        //   152: astore          11
        //   154: aload           9
        //   156: astore          6
        //   158: aload           7
        //   160: astore          12
        //   162: new             Ljava/io/FileWriter;
        //   165: astore          5
        //   167: aload           9
        //   169: astore          6
        //   171: aload           7
        //   173: astore          12
        //   175: aload           5
        //   177: aload_0        
        //   178: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.currentMetaDataFile:Ljava/io/File;
        //   181: iconst_1       
        //   182: invokespecial   java/io/FileWriter.<init>:(Ljava/io/File;Z)V
        //   185: new             Ljava/io/BufferedWriter;
        //   188: astore          6
        //   190: aload           6
        //   192: aload           5
        //   194: invokespecial   java/io/BufferedWriter.<init>:(Ljava/io/Writer;)V
        //   197: aload_1        
        //   198: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   201: ifne            240
        //   204: new             Ljava/lang/StringBuilder;
        //   207: astore          12
        //   209: aload           12
        //   211: invokespecial   java/lang/StringBuilder.<init>:()V
        //   214: aload           6
        //   216: aload           12
        //   218: ldc_w           "Make : "
        //   221: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   224: aload_1        
        //   225: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   228: ldc_w           "\n"
        //   231: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   234: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   237: invokevirtual   java/io/BufferedWriter.write:(Ljava/lang/String;)V
        //   240: aload_2        
        //   241: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   244: ifne            280
        //   247: new             Ljava/lang/StringBuilder;
        //   250: astore_1       
        //   251: aload_1        
        //   252: invokespecial   java/lang/StringBuilder.<init>:()V
        //   255: aload           6
        //   257: aload_1        
        //   258: ldc_w           "Model : "
        //   261: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   264: aload_2        
        //   265: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   268: ldc_w           "\n"
        //   271: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   274: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   277: invokevirtual   java/io/BufferedWriter.write:(Ljava/lang/String;)V
        //   280: aload_3        
        //   281: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   284: ifne            320
        //   287: new             Ljava/lang/StringBuilder;
        //   290: astore_1       
        //   291: aload_1        
        //   292: invokespecial   java/lang/StringBuilder.<init>:()V
        //   295: aload           6
        //   297: aload_1        
        //   298: ldc_w           "Year : "
        //   301: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   304: aload_3        
        //   305: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   308: ldc_w           "\n"
        //   311: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   314: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   317: invokevirtual   java/io/BufferedWriter.write:(Ljava/lang/String;)V
        //   320: aload           6
        //   322: invokevirtual   java/io/BufferedWriter.flush:()V
        //   325: aload           5
        //   327: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   330: aload           6
        //   332: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   335: aload           4
        //   337: monitorexit    
        //   338: return         
        //   339: astore_1       
        //   340: aload           8
        //   342: astore_1       
        //   343: aload           11
        //   345: astore_2       
        //   346: aload_2        
        //   347: astore          6
        //   349: aload_1        
        //   350: astore          12
        //   352: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.sLogger:Lcom/navdy/service/library/log/Logger;
        //   355: ldc_w           "Error writing to the metadata file"
        //   358: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   361: aload_1        
        //   362: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   365: aload_2        
        //   366: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   369: goto            335
        //   372: astore_1       
        //   373: aload           4
        //   375: monitorexit    
        //   376: aload_1        
        //   377: athrow         
        //   378: astore_1       
        //   379: aload           12
        //   381: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   384: aload           6
        //   386: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   389: aload_1        
        //   390: athrow         
        //   391: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.sLogger:Lcom/navdy/service/library/log/Logger;
        //   394: ldc_w           "Car details same"
        //   397: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   400: goto            335
        //   403: astore_1       
        //   404: aload           10
        //   406: astore          6
        //   408: aload           5
        //   410: astore          12
        //   412: goto            379
        //   415: astore_1       
        //   416: aload           5
        //   418: astore          12
        //   420: goto            379
        //   423: astore_1       
        //   424: aload           5
        //   426: astore_1       
        //   427: aload           11
        //   429: astore_2       
        //   430: goto            346
        //   433: astore_1       
        //   434: aload           6
        //   436: astore_2       
        //   437: aload           5
        //   439: astore_1       
        //   440: goto            346
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  5      97     372    378    Any
        //  97     134    372    378    Any
        //  162    167    339    346    Ljava/io/IOException;
        //  162    167    378    379    Any
        //  175    185    339    346    Ljava/io/IOException;
        //  175    185    378    379    Any
        //  185    197    423    433    Ljava/io/IOException;
        //  185    197    403    415    Any
        //  197    240    433    443    Ljava/io/IOException;
        //  197    240    415    423    Any
        //  240    280    433    443    Ljava/io/IOException;
        //  240    280    415    423    Any
        //  280    320    433    443    Ljava/io/IOException;
        //  280    320    415    423    Any
        //  320    325    433    443    Ljava/io/IOException;
        //  320    325    415    423    Any
        //  325    335    372    378    Any
        //  352    361    378    379    Any
        //  361    369    372    378    Any
        //  379    391    372    378    Any
        //  391    400    372    378    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 208, Size: 208
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Subscribe
    public void onDrivingStateChanged(final DrivingStateChange drivingStateChange) {
        final boolean driving = drivingStateChange.driving;
        if (this.motionDetected != driving) {
            final boolean canBusMonitoringNeeded = this.isCanBusMonitoringNeeded();
            this.motionDetected = driving;
            if (canBusMonitoringNeeded != this.isCanBusMonitoringNeeded()) {
                ObdCanBusRecordingPolicy.sLogger.d("Can BUS monitoring need changed, update listener");
                this.obdManager.updateListener();
            }
            else {
                ObdCanBusRecordingPolicy.sLogger.d("Can BUS monitoring need is not changed due to driving state change");
            }
        }
    }
    
    public void onFileUploaded(final File file) {
        ObdCanBusRecordingPolicy.sLogger.d("File successfully uploaded " + file.getAbsolutePath());
        this.sharedPreferences.edit().putBoolean("canBusDataRecordedAndSent", true).commit();
        this.sharedPreferences.edit().putInt("canBusDataSentVersion", this.requiredObdDataVersion).commit();
        this.sharedPreferences.edit().putLong("preference_navdy_miles_when_listening_started", -1L);
        this.populateFilesQueue();
        AnalyticsSupport.recordObdCanBusDataSent(Integer.toString(this.requiredObdDataVersion));
        this.obdManager.updateListener();
    }
    
    public void onInstantaneousModeChanged(final boolean isInstantaneousModeOn) {
        this.isInstantaneousModeOn = isInstantaneousModeOn;
    }
    
    public void onNewDataAvailable(String format) {
        ObdCanBusRecordingPolicy.sLogger.d("onNewDataAvailable , File path : " + format);
        final File file = new File(format);
        format = ObdCanBusRecordingPolicy.dateFormat.format(new Date(System.currentTimeMillis()));
        if (file.exists()) {
            final File file2 = new File("/sdcard/.canBusLogs/upload" + File.separator + file.getName() + "" + format + ".log");
            ObdCanBusRecordingPolicy.sLogger.d("Moving From : " + file.getAbsolutePath() + ", TO : " + file2.getAbsolutePath());
            file.renameTo(file2);
        }
        final File[] listFiles = new File("/sdcard/.canBusLogs/upload").listFiles();
        final ArrayList<File> list = new ArrayList<File>();
        if (listFiles != null) {
            for (final File file3 : listFiles) {
                ObdCanBusRecordingPolicy.sLogger.d("Child " + file3.getName());
                if (file3.isFile() && !file3.getName().endsWith(".zip")) {
                    list.add(file3);
                }
            }
            if (list.size() > 0) {
                final File[] array = new File[list.size()];
                list.<File>toArray(array);
                final StringBuilder sb = new StringBuilder();
                sb.append("CAN_BUS_DATA_");
                if (!TextUtils.isEmpty((CharSequence)this.make)) {
                    sb.append(this.make + "_");
                }
                if (!TextUtils.isEmpty((CharSequence)this.model)) {
                    sb.append(this.model + "_");
                }
                if (!TextUtils.isEmpty((CharSequence)this.year)) {
                    sb.append(this.year + "_");
                }
                if (!TextUtils.isEmpty((CharSequence)this.vin)) {
                    sb.append(this.vin + "_");
                }
                sb.append("V(" + this.requiredObdDataVersion + ")");
                sb.append(format).append(".zip");
                final File file4 = new File("/sdcard/.canBusLogs/upload" + File.separator + sb.toString());
                ObdCanBusRecordingPolicy.sLogger.d("Compressing the files to " + file4.getName());
                IOUtils.compressFilesToZip(HudApplication.getAppContext(), array, file4.getAbsolutePath());
                for (int length2 = array.length, j = 0; j < length2; ++j) {
                    IOUtils.deleteFile(HudApplication.getAppContext(), array[j].getAbsolutePath());
                }
                ObdCanBusRecordingPolicy.uploadFiles.add(file4);
                if (ObdCanBusRecordingPolicy.uploadFiles.size() > 5) {
                    final File file5 = ObdCanBusRecordingPolicy.uploadFiles.poll();
                    if (file5 != null) {
                        ObdCanBusRecordingPolicy.sLogger.d("Deleting upload file " + file5.getName() + ", As its old");
                        IOUtils.deleteFile(HudApplication.getAppContext(), file5.getAbsolutePath());
                    }
                }
                ObdCANBusDataUploadService.addObdDataFileToQueue(file4);
                ObdCANBusDataUploadService.syncNow();
            }
        }
        else {
            ObdCanBusRecordingPolicy.sLogger.d("No files found in the upload directory");
        }
    }
    
    public void onObdConnectionStateChanged(final boolean p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: dup            
        //     2: astore_2       
        //     3: monitorenter   
        //     4: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.sLogger:Lcom/navdy/service/library/log/Logger;
        //     7: astore_3       
        //     8: new             Ljava/lang/StringBuilder;
        //    11: astore          4
        //    13: aload           4
        //    15: invokespecial   java/lang/StringBuilder.<init>:()V
        //    18: aload_3        
        //    19: aload           4
        //    21: ldc_w           "onObdConnectionStateChanged , Connected : "
        //    24: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    27: iload_1        
        //    28: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //    31: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    34: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    37: aload_0        
        //    38: iload_1        
        //    39: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.isObdConnected:Z
        //    42: iload_1        
        //    43: ifeq            608
        //    46: aload_0        
        //    47: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.obdManager:Lcom/navdy/hud/app/obd/ObdManager;
        //    50: invokevirtual   com/navdy/hud/app/obd/ObdManager.getProtocol:()Ljava/lang/String;
        //    53: astore_3       
        //    54: aload_3        
        //    55: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    58: ifne            595
        //    61: aload_3        
        //    62: ldc             "ISO 15765-4"
        //    64: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //    67: ifeq            595
        //    70: aload_0        
        //    71: iconst_1       
        //    72: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.isCanProtocol:Z
        //    75: aload_0        
        //    76: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState.UNKNOWN:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;
        //    79: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.canBusMonitoringState:Lcom/navdy/hud/app/obd/ObdCanBusRecordingPolicy$CanBusMonitoringState;
        //    82: aload_0        
        //    83: iconst_0       
        //    84: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.canBusMonitoringDataSize:I
        //    87: iload_1        
        //    88: ifeq            718
        //    91: invokestatic    java/lang/System.currentTimeMillis:()J
        //    94: lstore          5
        //    96: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;
        //    99: invokevirtual   java/util/concurrent/PriorityBlockingQueue.size:()I
        //   102: bipush          10
        //   104: if_icmple       178
        //   107: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.metaDataFiles:Ljava/util/concurrent/PriorityBlockingQueue;
        //   110: invokevirtual   java/util/concurrent/PriorityBlockingQueue.poll:()Ljava/lang/Object;
        //   113: checkcast       Ljava/io/File;
        //   116: astore          7
        //   118: aload           7
        //   120: ifnull          178
        //   123: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.sLogger:Lcom/navdy/service/library/log/Logger;
        //   126: astore_3       
        //   127: new             Ljava/lang/StringBuilder;
        //   130: astore          4
        //   132: aload           4
        //   134: invokespecial   java/lang/StringBuilder.<init>:()V
        //   137: aload_3        
        //   138: aload           4
        //   140: ldc_w           "Deleting file "
        //   143: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   146: aload           7
        //   148: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //   151: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   154: ldc_w           ", As its old"
        //   157: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   160: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   163: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   166: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //   169: aload           7
        //   171: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   174: invokestatic    com/navdy/service/library/util/IOUtils.deleteFile:(Landroid/content/Context;Ljava/lang/String;)Z
        //   177: pop            
        //   178: aload_0        
        //   179: aload_0        
        //   180: invokespecial   com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.getMetaDataFile:()Ljava/io/File;
        //   183: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.currentMetaDataFile:Ljava/io/File;
        //   186: aconst_null    
        //   187: astore          8
        //   189: aconst_null    
        //   190: astore          9
        //   192: aconst_null    
        //   193: astore          10
        //   195: aconst_null    
        //   196: astore          11
        //   198: aconst_null    
        //   199: astore          12
        //   201: new             Lorg/json/JSONObject;
        //   204: astore          13
        //   206: aload           13
        //   208: invokespecial   org/json/JSONObject.<init>:()V
        //   211: aload           10
        //   213: astore          4
        //   215: aload           8
        //   217: astore          7
        //   219: new             Ljava/io/FileWriter;
        //   222: astore_3       
        //   223: aload           10
        //   225: astore          4
        //   227: aload           8
        //   229: astore          7
        //   231: aload_3        
        //   232: aload_0        
        //   233: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.currentMetaDataFile:Ljava/io/File;
        //   236: iconst_0       
        //   237: invokespecial   java/io/FileWriter.<init>:(Ljava/io/File;Z)V
        //   240: new             Ljava/io/BufferedWriter;
        //   243: astore          4
        //   245: aload           4
        //   247: aload_3        
        //   248: invokespecial   java/io/BufferedWriter.<init>:(Ljava/io/Writer;)V
        //   251: aload_0        
        //   252: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.obdManager:Lcom/navdy/hud/app/obd/ObdManager;
        //   255: invokevirtual   com/navdy/hud/app/obd/ObdManager.getCarService:()Lcom/navdy/obd/ICarService;
        //   258: astore          7
        //   260: aload_0        
        //   261: aload_0        
        //   262: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.obdManager:Lcom/navdy/hud/app/obd/ObdManager;
        //   265: invokevirtual   com/navdy/hud/app/obd/ObdManager.getVin:()Ljava/lang/String;
        //   268: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.vin:Ljava/lang/String;
        //   271: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.dateFormat:Ljava/text/SimpleDateFormat;
        //   274: astore          11
        //   276: new             Ljava/util/Date;
        //   279: astore          12
        //   281: aload           12
        //   283: lload           5
        //   285: invokespecial   java/util/Date.<init>:(J)V
        //   288: aload           11
        //   290: aload           12
        //   292: invokevirtual   java/text/SimpleDateFormat.format:(Ljava/util/Date;)Ljava/lang/String;
        //   295: astore          12
        //   297: aload           13
        //   299: ldc_w           "time"
        //   302: aload           12
        //   304: invokevirtual   org/json/JSONObject.accumulate:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   307: pop            
        //   308: aload           13
        //   310: ldc_w           "vin"
        //   313: aload_0        
        //   314: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.vin:Ljava/lang/String;
        //   317: invokevirtual   org/json/JSONObject.accumulate:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   320: pop            
        //   321: aload           7
        //   323: ifnull          499
        //   326: aload           13
        //   328: ldc_w           "protocol"
        //   331: aload           7
        //   333: invokeinterface com/navdy/obd/ICarService.getProtocol:()Ljava/lang/String;
        //   338: invokevirtual   org/json/JSONObject.accumulate:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   341: pop            
        //   342: aload           7
        //   344: invokeinterface com/navdy/obd/ICarService.getEcus:()Ljava/util/List;
        //   349: astore          11
        //   351: new             Lorg/json/JSONArray;
        //   354: astore          12
        //   356: aload           12
        //   358: invokespecial   org/json/JSONArray.<init>:()V
        //   361: aload           11
        //   363: ifnull          499
        //   366: iconst_0       
        //   367: istore          14
        //   369: iload           14
        //   371: aload           11
        //   373: invokeinterface java/util/List.size:()I
        //   378: if_icmpge       641
        //   381: new             Lorg/json/JSONObject;
        //   384: astore          7
        //   386: aload           7
        //   388: invokespecial   org/json/JSONObject.<init>:()V
        //   391: aload           11
        //   393: iload           14
        //   395: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   400: checkcast       Lcom/navdy/obd/ECU;
        //   403: astore          9
        //   405: aload           7
        //   407: ldc_w           "Address "
        //   410: aload           9
        //   412: getfield        com/navdy/obd/ECU.address:I
        //   415: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   418: invokevirtual   org/json/JSONObject.accumulate:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   421: pop            
        //   422: aload           9
        //   424: getfield        com/navdy/obd/ECU.supportedPids:Lcom/navdy/obd/PidSet;
        //   427: invokevirtual   com/navdy/obd/PidSet.asList:()Ljava/util/List;
        //   430: astore          8
        //   432: new             Lorg/json/JSONArray;
        //   435: astore          9
        //   437: aload           9
        //   439: invokespecial   org/json/JSONArray.<init>:()V
        //   442: aload           8
        //   444: ifnull          616
        //   447: aload           8
        //   449: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   454: astore          8
        //   456: aload           8
        //   458: invokeinterface java/util/Iterator.hasNext:()Z
        //   463: ifeq            616
        //   466: aload           9
        //   468: aload           8
        //   470: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   475: checkcast       Lcom/navdy/obd/Pid;
        //   478: invokevirtual   com/navdy/obd/Pid.getId:()I
        //   481: invokevirtual   org/json/JSONArray.put:(I)Lorg/json/JSONArray;
        //   484: pop            
        //   485: goto            456
        //   488: astore          7
        //   490: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.sLogger:Lcom/navdy/service/library/log/Logger;
        //   493: ldc_w           "Error get the data from obd service"
        //   496: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   499: aload_0        
        //   500: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.make:Ljava/lang/String;
        //   503: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   506: ifne            522
        //   509: aload           13
        //   511: ldc_w           "Make"
        //   514: aload_0        
        //   515: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.make:Ljava/lang/String;
        //   518: invokevirtual   org/json/JSONObject.accumulate:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   521: pop            
        //   522: aload_0        
        //   523: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.model:Ljava/lang/String;
        //   526: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   529: ifne            545
        //   532: aload           13
        //   534: ldc_w           "Model"
        //   537: aload_0        
        //   538: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.model:Ljava/lang/String;
        //   541: invokevirtual   org/json/JSONObject.accumulate:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   544: pop            
        //   545: aload_0        
        //   546: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.year:Ljava/lang/String;
        //   549: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   552: ifne            568
        //   555: aload           13
        //   557: ldc_w           "Year"
        //   560: aload_0        
        //   561: getfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.year:Ljava/lang/String;
        //   564: invokevirtual   org/json/JSONObject.accumulate:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   567: pop            
        //   568: aload           4
        //   570: aload           13
        //   572: invokevirtual   org/json/JSONObject.toString:()Ljava/lang/String;
        //   575: invokevirtual   java/io/BufferedWriter.write:(Ljava/lang/String;)V
        //   578: aload           4
        //   580: invokevirtual   java/io/BufferedWriter.flush:()V
        //   583: aload_3        
        //   584: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   587: aload           4
        //   589: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   592: aload_2        
        //   593: monitorexit    
        //   594: return         
        //   595: aload_0        
        //   596: iconst_0       
        //   597: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.isCanProtocol:Z
        //   600: goto            75
        //   603: astore_3       
        //   604: aload_2        
        //   605: monitorexit    
        //   606: aload_3        
        //   607: athrow         
        //   608: aload_0        
        //   609: iconst_0       
        //   610: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.isCanProtocol:Z
        //   613: goto            75
        //   616: aload           7
        //   618: ldc_w           "PIDs"
        //   621: aload           9
        //   623: invokevirtual   org/json/JSONObject.accumulate:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   626: pop            
        //   627: aload           12
        //   629: aload           7
        //   631: invokevirtual   org/json/JSONArray.put:(Ljava/lang/Object;)Lorg/json/JSONArray;
        //   634: pop            
        //   635: iinc            14, 1
        //   638: goto            369
        //   641: aload           13
        //   643: ldc_w           "ecus"
        //   646: aload           12
        //   648: invokevirtual   org/json/JSONObject.accumulate:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   651: pop            
        //   652: goto            499
        //   655: astore          7
        //   657: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.sLogger:Lcom/navdy/service/library/log/Logger;
        //   660: ldc_w           "Error writing meta data JSON "
        //   663: aload           7
        //   665: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   668: goto            568
        //   671: astore          7
        //   673: aload           4
        //   675: astore          12
        //   677: aload           12
        //   679: astore          4
        //   681: aload_3        
        //   682: astore          7
        //   684: getstatic       com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.sLogger:Lcom/navdy/service/library/log/Logger;
        //   687: ldc_w           "Error writing to the metadata file"
        //   690: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   693: aload_3        
        //   694: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   697: aload           12
        //   699: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   702: goto            592
        //   705: astore_3       
        //   706: aload           7
        //   708: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   711: aload           4
        //   713: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   716: aload_3        
        //   717: athrow         
        //   718: aload_0        
        //   719: aconst_null    
        //   720: putfield        com/navdy/hud/app/obd/ObdCanBusRecordingPolicy.currentMetaDataFile:Ljava/io/File;
        //   723: goto            592
        //   726: astore          12
        //   728: aload           11
        //   730: astore          4
        //   732: aload_3        
        //   733: astore          7
        //   735: aload           12
        //   737: astore_3       
        //   738: goto            706
        //   741: astore          12
        //   743: aload_3        
        //   744: astore          7
        //   746: aload           12
        //   748: astore_3       
        //   749: goto            706
        //   752: astore_3       
        //   753: aload           9
        //   755: astore_3       
        //   756: goto            677
        //   759: astore          4
        //   761: goto            677
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  4      42     603    608    Any
        //  46     75     603    608    Any
        //  75     87     603    608    Any
        //  91     118    603    608    Any
        //  123    178    603    608    Any
        //  178    186    603    608    Any
        //  201    211    603    608    Any
        //  219    223    752    759    Ljava/io/IOException;
        //  219    223    705    706    Any
        //  231    240    752    759    Ljava/io/IOException;
        //  231    240    705    706    Any
        //  240    251    759    764    Ljava/io/IOException;
        //  240    251    726    741    Any
        //  251    297    671    677    Ljava/io/IOException;
        //  251    297    741    752    Any
        //  297    321    655    671    Lorg/json/JSONException;
        //  297    321    671    677    Ljava/io/IOException;
        //  297    321    741    752    Any
        //  326    361    488    499    Landroid/os/RemoteException;
        //  326    361    655    671    Lorg/json/JSONException;
        //  326    361    671    677    Ljava/io/IOException;
        //  326    361    741    752    Any
        //  369    442    488    499    Landroid/os/RemoteException;
        //  369    442    655    671    Lorg/json/JSONException;
        //  369    442    671    677    Ljava/io/IOException;
        //  369    442    741    752    Any
        //  447    456    488    499    Landroid/os/RemoteException;
        //  447    456    655    671    Lorg/json/JSONException;
        //  447    456    671    677    Ljava/io/IOException;
        //  447    456    741    752    Any
        //  456    485    488    499    Landroid/os/RemoteException;
        //  456    485    655    671    Lorg/json/JSONException;
        //  456    485    671    677    Ljava/io/IOException;
        //  456    485    741    752    Any
        //  490    499    655    671    Lorg/json/JSONException;
        //  490    499    671    677    Ljava/io/IOException;
        //  490    499    741    752    Any
        //  499    522    655    671    Lorg/json/JSONException;
        //  499    522    671    677    Ljava/io/IOException;
        //  499    522    741    752    Any
        //  522    545    655    671    Lorg/json/JSONException;
        //  522    545    671    677    Ljava/io/IOException;
        //  522    545    741    752    Any
        //  545    568    655    671    Lorg/json/JSONException;
        //  545    568    671    677    Ljava/io/IOException;
        //  545    568    741    752    Any
        //  568    583    671    677    Ljava/io/IOException;
        //  568    583    741    752    Any
        //  583    592    603    608    Any
        //  595    600    603    608    Any
        //  608    613    603    608    Any
        //  616    635    488    499    Landroid/os/RemoteException;
        //  616    635    655    671    Lorg/json/JSONException;
        //  616    635    671    677    Ljava/io/IOException;
        //  616    635    741    752    Any
        //  641    652    488    499    Landroid/os/RemoteException;
        //  641    652    655    671    Lorg/json/JSONException;
        //  641    652    671    677    Ljava/io/IOException;
        //  641    652    741    752    Any
        //  657    668    671    677    Ljava/io/IOException;
        //  657    668    741    752    Any
        //  684    693    705    706    Any
        //  693    702    603    608    Any
        //  706    718    603    608    Any
        //  718    723    603    608    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0369:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    enum CanBusMonitoringState
    {
        FAILURE, 
        SUCCESS, 
        UNKNOWN;
    }
}
