package com.navdy.hud.app.debug;

import android.preference.PreferenceFragment;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import java.util.List;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity
{
    public static final String RESTART_ON_CRASH_KEY = "restart_on_crash_preference";
    public static final String START_ON_BOOT_KEY = "start_on_boot_preference";
    public static final String START_VIDEO_ON_BOOT_KEY = "start_video_on_boot_preference";
    
    public void onBuildHeaders(final List<PreferenceActivity$Header> list) {
        this.loadHeadersFromResource(R.xml.preference_headers, (List)list);
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
    }
    
    public static class DeveloperPreferencesFragment extends PreferenceFragment
    {
        public void onCreate(final Bundle bundle) {
            super.onCreate(bundle);
            this.addPreferencesFromResource(R.xml.developer_preferences);
        }
    }
}
