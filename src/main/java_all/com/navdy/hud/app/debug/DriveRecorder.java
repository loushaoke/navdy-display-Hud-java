package com.navdy.hud.app.debug;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.navdy.service.library.events.debug.StopDriveRecordingEvent;
import com.navdy.service.library.events.debug.StopDrivePlaybackEvent;
import com.navdy.service.library.events.debug.StartDrivePlaybackEvent;
import android.content.ComponentName;
import com.navdy.hud.app.maps.MapEvents;
import android.view.KeyEvent;
import com.navdy.hud.app.analytics.TelemetrySession;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.device.gps.CalibratedGForceData;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.squareup.wire.Message;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.util.IOUtils;
import java.util.Comparator;
import java.util.Arrays;
import java.io.File;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.service.library.task.TaskManager;
import com.navdy.obd.PidSet;
import com.navdy.hud.app.manager.SpeedManager;
import java.io.IOException;
import android.content.Intent;
import android.os.Looper;
import com.navdy.hud.app.HudApplication;
import java.util.Iterator;
import android.os.RemoteException;
import android.os.IBinder;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.obd.Pid;
import java.util.Map;
import java.util.List;
import com.navdy.obd.IPidListener;
import android.os.HandlerThread;
import android.os.Handler;
import android.content.Context;
import com.navdy.hud.app.service.ConnectionHandler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.content.ServiceConnection;

public class DriveRecorder implements ServiceConnection, SerialExecutor
{
    private static final String AUTO_DRIVERECORD_PROP = "persist.sys.driverecord.auto";
    public static final String AUTO_RECORD_LABEL = "auto";
    public static final int BUFFER_SIZE = 500;
    public static final int BUFFER_SIZE_OBD_LOG = 250;
    public static final String DEMO_LOG_FILE_NAME = "demo_log.log";
    public static final String DRIVE_LOGS_FOLDER = "drive_logs";
    public static final int MAXIMUM_FILES_IN_THE_FOLDER = 20;
    public static final Logger sLogger;
    Bus bus;
    private ConnectionHandler connectionHandler;
    private final Context context;
    private volatile int currentObdDataInjectionIndex;
    boolean demoObdLogFileExists;
    boolean demoRouteLogFileExists;
    AsyncBufferedFileWriter driveScoreDataBufferedFileWriter;
    private StringBuilder gforceDataBuilder;
    private final Handler handler;
    private final Runnable injectFakeObdData;
    private boolean isEngineInitialized;
    private volatile boolean isLooping;
    private volatile boolean isRecording;
    private volatile boolean isSupportedPidsWritten;
    AsyncBufferedFileWriter obdAsyncBufferedFileWriter;
    private Handler obdDataPlaybackHandler;
    private HandlerThread obdPlaybackThread;
    private final IPidListener pidListener;
    private volatile State playbackState;
    private volatile long preparedFileLastModifiedTime;
    private volatile String preparedFileName;
    volatile boolean realObdConnected;
    private List<Map.Entry<Long, List<Pid>>> recordedObdData;
    private IRouteRecorder routeRecorder;
    
    static {
        sLogger = new Logger(DriveRecorder.class);
    }
    
    public DriveRecorder(final Bus bus, final ConnectionHandler connectionHandler) {
        this.gforceDataBuilder = new StringBuilder(40);
        this.playbackState = State.STOPPED;
        this.isLooping = false;
        this.realObdConnected = false;
        this.demoRouteLogFileExists = false;
        this.demoObdLogFileExists = false;
        this.injectFakeObdData = new Runnable() {
            @Override
            public void run() {
                if (DriveRecorder.this.recordedObdData.size() > DriveRecorder.this.currentObdDataInjectionIndex + 1) {
                    final Map.Entry<Long, V> entry = DriveRecorder.this.recordedObdData.get(DriveRecorder.this.currentObdDataInjectionIndex);
                    final long longValue = entry.getKey();
                    ObdManager.getInstance().injectObdData((List<Pid>)entry.getValue(), (List<Pid>)entry.getValue());
                    if (DriveRecorder.this.recordedObdData.size() > DriveRecorder.this.currentObdDataInjectionIndex + 1) {
                        DriveRecorder.this.obdDataPlaybackHandler.postDelayed(DriveRecorder.this.injectFakeObdData, DriveRecorder.this.recordedObdData.get(++DriveRecorder.this.currentObdDataInjectionIndex).getKey() - longValue);
                    }
                }
                else if (DriveRecorder.this.isLooping) {
                    DriveRecorder.this.currentObdDataInjectionIndex = 0;
                    DriveRecorder.this.obdDataPlaybackHandler.post(DriveRecorder.this.injectFakeObdData);
                }
                else {
                    DriveRecorder.sLogger.d("Done playing the recorded data. Stopping the playback");
                    DriveRecorder.this.stopPlayback();
                }
            }
        };
        this.pidListener = new IPidListener() {
            public IBinder asBinder() {
                return null;
            }
            
            @Override
            public void onConnectionStateChange(final int n) throws RemoteException {
            }
            
            @Override
            public void pidsChanged(final List<Pid> list) throws RemoteException {
            }
            
            @Override
            public void pidsRead(final List<Pid> list, final List<Pid> list2) throws RemoteException {
                if (list2 != null && DriveRecorder.this.isRecording) {
                    Label_0052: {
                        if (DriveRecorder.this.isSupportedPidsWritten) {
                            break Label_0052;
                        }
                    Label_0090_Outer:
                        while (true) {
                            ObdManager.getInstance().setRecordingPidListener(null);
                            while (true) {
                            Label_0193:
                                while (true) {
                                    try {
                                        DriveRecorder.this.bRecordSupportedPids();
                                        ObdManager.getInstance().setRecordingPidListener(DriveRecorder.this.pidListener);
                                        DriveRecorder.this.obdAsyncBufferedFileWriter.write(System.currentTimeMillis() + ",");
                                        for (final Pid pid : list2) {
                                            DriveRecorder.this.obdAsyncBufferedFileWriter.write(pid.getId() + ":" + pid.getValue());
                                            if (pid == list2.get(list2.size() - 1)) {
                                                break Label_0193;
                                            }
                                            DriveRecorder.this.obdAsyncBufferedFileWriter.write(",");
                                        }
                                        break;
                                    }
                                    catch (Throwable t) {
                                        DriveRecorder.sLogger.d("Error while recording the supported PIDs");
                                        continue Label_0090_Outer;
                                    }
                                    break;
                                }
                                DriveRecorder.this.obdAsyncBufferedFileWriter.write("\n");
                                continue;
                            }
                        }
                    }
                }
            }
        };
        this.context = HudApplication.getAppContext();
        this.handler = new Handler(Looper.getMainLooper());
        this.connectionHandler = connectionHandler;
        this.isRecording = false;
        this.playbackState = State.STOPPED;
        this.isSupportedPidsWritten = false;
        (this.bus = bus).register(this);
        ObdManager.getInstance().setDriveRecorder(this);
        this.context.bindService(new Intent(this.context, (Class)RouteRecorderService.class), (ServiceConnection)this, 1);
    }
    
    private void bRecordSupportedPids() throws IOException {
        final ObdManager instance = ObdManager.getInstance();
        final PidSet supportedPids = instance.getSupportedPids();
        if (supportedPids != null) {
            final List<Pid> list = supportedPids.asList();
            if (list != null && list.size() > 0) {
                for (final Pid pid : list) {
                    if (pid != list.get(list.size() - 1)) {
                        this.obdAsyncBufferedFileWriter.write(pid.getId() + ",");
                    }
                    else {
                        this.obdAsyncBufferedFileWriter.write(pid.getId() + "\n");
                    }
                }
            }
            else {
                this.obdAsyncBufferedFileWriter.write("-1\n");
            }
            this.obdAsyncBufferedFileWriter.write(System.currentTimeMillis() + ",");
            this.obdAsyncBufferedFileWriter.write("47:" + instance.getFuelLevel() + ",");
            this.obdAsyncBufferedFileWriter.write("13:" + SpeedManager.getInstance().getObdSpeed() + ",");
            this.obdAsyncBufferedFileWriter.write("12:" + instance.getEngineRpm() + ",");
            this.obdAsyncBufferedFileWriter.write("256:" + instance.getInstantFuelConsumption() + "\n", true);
            this.isSupportedPidsWritten = true;
            DriveRecorder.sLogger.d("Obd data record created successfully and starting to listening to the PID changes");
        }
    }
    
    private void checkAndStartAutoRecording() {
        if (isAutoRecordingEnabled()) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    DriveRecorder.sLogger.d("Auto recording is enabled so starting the auto record");
                    final File[] listFiles = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + "drive_logs").listFiles();
                    if (listFiles != null && listFiles.length > 20) {
                        Arrays.<File>sort(listFiles, new FilesModifiedTimeComparator());
                        for (int i = 0; i < listFiles.length - 20; ++i) {
                            final File file = listFiles[i];
                            DriveRecorder.sLogger.d("Deleting the drive record " + file.getAbsolutePath() + " as its old ");
                            IOUtils.deleteFile(HudApplication.getAppContext(), file.getAbsolutePath());
                        }
                    }
                    DriveRecorder.this.connectionHandler.sendLocalMessage(new StartDriveRecordingEvent("auto"));
                }
            }, 9);
        }
    }
    
    private Runnable createDriveRecord(final String s) {
        return new Runnable() {
            @Override
            public void run() {
                final File driveLogsDir = DriveRecorder.getDriveLogsDir(s);
                if (!driveLogsDir.exists()) {
                    driveLogsDir.mkdirs();
                }
                final File file = new File(driveLogsDir, s);
                final File file2 = new File(file.getAbsolutePath() + ".obd");
                final File file3 = new File(file.getAbsolutePath() + ".drive");
                try {
                    if (file2.createNewFile()) {
                        DriveRecorder.this.obdAsyncBufferedFileWriter = new AsyncBufferedFileWriter(file2.getAbsolutePath(), DriveRecorder.this, 250);
                        ObdManager.getInstance().setRecordingPidListener(null);
                        DriveRecorder.this.bRecordSupportedPids();
                        ObdManager.getInstance().setRecordingPidListener(DriveRecorder.this.pidListener);
                    }
                    if (file3.createNewFile()) {
                        DriveRecorder.this.driveScoreDataBufferedFileWriter = new AsyncBufferedFileWriter(file3.getAbsolutePath(), DriveRecorder.this, 500);
                    }
                }
                catch (IOException ex) {
                    DriveRecorder.sLogger.e(ex);
                    DriveRecorder.this.isRecording = false;
                    DriveRecorder.this.isSupportedPidsWritten = false;
                }
            }
        };
    }
    
    public static File getDriveLogsDir(final String s) {
        File file;
        if ("demo_log.log".equals(s)) {
            file = new File(PathManager.getInstance().getMapsPartitionPath());
        }
        else {
            file = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + "drive_logs");
        }
        return file;
    }
    
    private static double getLatitude() {
        if (!HereMapsManager.getInstance().isInitialized()) {
            return 0.0;
        }
        final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        if (lastGeoCoordinate == null) {
            return 0.0;
        }
        return lastGeoCoordinate.getLatitude();
        latitude = 0.0;
        return latitude;
    }
    
    private static double getLongitude() {
        if (!HereMapsManager.getInstance().isInitialized()) {
            return 0.0;
        }
        final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        if (lastGeoCoordinate == null) {
            return 0.0;
        }
        return lastGeoCoordinate.getLongitude();
        longitude = 0.0;
        return longitude;
    }
    
    public static boolean isAutoRecordingEnabled() {
        boolean b = false;
        if (SystemProperties.getBoolean("persist.sys.driverecord.auto", false) || !DeviceUtil.isUserBuild()) {
            b = true;
        }
        return b;
    }
    
    private boolean isPaused() {
        return this.playbackState == State.PAUSED;
    }
    
    private boolean isPlaying() {
        return this.playbackState == State.PLAYING;
    }
    
    private boolean isStopped() {
        return this.playbackState == State.STOPPED;
    }
    
    private void pauseDemo() {
        this.stopRecordingBeforeDemo();
        if (this.isPlaying()) {
            this.pausePlayback();
        }
        try {
            if (this.routeRecorder.isPlaying()) {
                this.routeRecorder.pausePlayback();
            }
        }
        catch (Exception ex) {
            DriveRecorder.sLogger.e("Error pausing the route playback");
        }
    }
    
    private void playDemo(final boolean b) {
        this.stopRecordingBeforeDemo();
        Label_0044: {
            if (!this.isPlaying()) {
                if (!this.isPaused()) {
                    break Label_0044;
                }
                this.resumePlayback();
            }
            while (true) {
                try {
                    if (this.routeRecorder.isPaused()) {
                        this.routeRecorder.resumePlayback();
                    }
                    else if (!this.routeRecorder.isPlaying()) {
                        this.routeRecorder.startPlayback("demo_log.log", false, b);
                    }
                    return;
                    this.startPlayback("demo_log.log", b);
                    continue;
                }
                catch (Throwable t) {
                    DriveRecorder.sLogger.e("Error playing the route playback", t);
                }
            }
        }
    }
    
    private void prepareDemo() {
        this.prepare("demo_log.log");
        try {
            this.routeRecorder.prepare("demo_log.log", false);
        }
        catch (Throwable t) {
            DriveRecorder.sLogger.e("Error preparing the route recorder for demo", t);
        }
    }
    
    private void restartDemo(final boolean b) {
        this.stopRecordingBeforeDemo();
        if (!this.restartPlayback()) {
            this.startPlayback("demo_log.log", b);
        }
        try {
            if (!this.routeRecorder.restartPlayback()) {
                this.routeRecorder.startPlayback("demo_log.log", false, b);
            }
        }
        catch (Exception ex) {
            DriveRecorder.sLogger.e("Error restarting the route playback", ex);
        }
    }
    
    private void resumeDemo() {
        this.stopRecordingBeforeDemo();
        if (this.isPaused()) {
            this.resumePlayback();
        }
        try {
            if (this.routeRecorder.isPaused()) {
                this.routeRecorder.resumePlayback();
            }
        }
        catch (Exception ex) {
            DriveRecorder.sLogger.e("Error resuming the route playback", ex);
        }
    }
    
    private void stopDemo() {
        if (this.isPlaying()) {
            this.stopPlayback();
        }
        try {
            if (this.routeRecorder.isPlaying() || this.routeRecorder.isPaused()) {
                this.routeRecorder.stopPlayback();
            }
        }
        catch (Exception ex) {
            DriveRecorder.sLogger.e("Error stopping the route playback", ex);
        }
    }
    
    private void stopRecordingBeforeDemo() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/navdy/hud/app/debug/DriveRecorder.isRecording:Z
        //     4: ifeq            17
        //     7: aload_0        
        //     8: invokevirtual   com/navdy/hud/app/debug/DriveRecorder.stopRecording:()V
        //    11: ldc2_w          1000
        //    14: invokestatic    java/lang/Thread.sleep:(J)V
        //    17: aload_0        
        //    18: getfield        com/navdy/hud/app/debug/DriveRecorder.routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;
        //    21: invokeinterface com/navdy/hud/app/debug/IRouteRecorder.isRecording:()Z
        //    26: ifeq            44
        //    29: aload_0        
        //    30: getfield        com/navdy/hud/app/debug/DriveRecorder.routeRecorder:Lcom/navdy/hud/app/debug/IRouteRecorder;
        //    33: invokeinterface com/navdy/hud/app/debug/IRouteRecorder.stopRecording:()V
        //    38: ldc2_w          1000
        //    41: invokestatic    java/lang/Thread.sleep:(J)V
        //    44: return         
        //    45: astore_1       
        //    46: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //    49: ldc_w           "Interrupted "
        //    52: aload_1        
        //    53: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    56: goto            17
        //    59: astore_1       
        //    60: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //    63: ldc_w           "Interrupted while stopping the recording"
        //    66: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //    69: goto            44
        //    72: astore_1       
        //    73: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //    76: ldc_w           "Exception while stopping the recording"
        //    79: aload_1        
        //    80: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    83: goto            44
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  11     17     45     59     Ljava/lang/InterruptedException;
        //  17     38     72     86     Ljava/lang/Exception;
        //  38     44     59     72     Ljava/lang/InterruptedException;
        //  38     44     72     86     Ljava/lang/Exception;
        //  60     69     72     86     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0017:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public boolean bPrepare(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_1        
        //     5: invokestatic    com/navdy/hud/app/debug/DriveRecorder.getDriveLogsDir:(Ljava/lang/String;)Ljava/io/File;
        //     8: new             Ljava/lang/StringBuilder;
        //    11: dup            
        //    12: invokespecial   java/lang/StringBuilder.<init>:()V
        //    15: aload_1        
        //    16: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    19: ldc_w           ".obd"
        //    22: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    25: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    28: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    31: astore_2       
        //    32: aload_2        
        //    33: invokevirtual   java/io/File.exists:()Z
        //    36: ifne            43
        //    39: iconst_0       
        //    40: istore_3       
        //    41: iload_3        
        //    42: ireturn        
        //    43: aload_2        
        //    44: invokevirtual   java/io/File.lastModified:()J
        //    47: lstore          4
        //    49: aload_0        
        //    50: getfield        com/navdy/hud/app/debug/DriveRecorder.preparedFileName:Ljava/lang/String;
        //    53: aload_1        
        //    54: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    57: ifeq            84
        //    60: aload_0        
        //    61: getfield        com/navdy/hud/app/debug/DriveRecorder.preparedFileLastModifiedTime:J
        //    64: lload           4
        //    66: lcmp           
        //    67: ifne            84
        //    70: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //    73: ldc_w           "File already prepared"
        //    76: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    79: iconst_1       
        //    80: istore_3       
        //    81: goto            41
        //    84: aload_0        
        //    85: aload_1        
        //    86: putfield        com/navdy/hud/app/debug/DriveRecorder.preparedFileName:Ljava/lang/String;
        //    89: aload_0        
        //    90: lload           4
        //    92: putfield        com/navdy/hud/app/debug/DriveRecorder.preparedFileLastModifiedTime:J
        //    95: aconst_null    
        //    96: astore          6
        //    98: aconst_null    
        //    99: astore          7
        //   101: aload           7
        //   103: astore          8
        //   105: aload           6
        //   107: astore_1       
        //   108: new             Ljava/io/BufferedReader;
        //   111: astore          9
        //   113: aload           7
        //   115: astore          8
        //   117: aload           6
        //   119: astore_1       
        //   120: new             Ljava/io/InputStreamReader;
        //   123: astore          10
        //   125: aload           7
        //   127: astore          8
        //   129: aload           6
        //   131: astore_1       
        //   132: new             Ljava/io/FileInputStream;
        //   135: astore          11
        //   137: aload           7
        //   139: astore          8
        //   141: aload           6
        //   143: astore_1       
        //   144: aload           11
        //   146: aload_2        
        //   147: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   150: aload           7
        //   152: astore          8
        //   154: aload           6
        //   156: astore_1       
        //   157: aload           10
        //   159: aload           11
        //   161: ldc_w           "utf-8"
        //   164: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;Ljava/lang/String;)V
        //   167: aload           7
        //   169: astore          8
        //   171: aload           6
        //   173: astore_1       
        //   174: aload           9
        //   176: aload           10
        //   178: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //   181: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //   184: ldc_w           "startPlayback of Obd data"
        //   187: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   190: new             Ljava/util/ArrayList;
        //   193: astore_1       
        //   194: aload_1        
        //   195: invokespecial   java/util/ArrayList.<init>:()V
        //   198: aload_0        
        //   199: aload_1        
        //   200: putfield        com/navdy/hud/app/debug/DriveRecorder.recordedObdData:Ljava/util/List;
        //   203: new             Ljava/io/BufferedReader;
        //   206: astore          7
        //   208: new             Ljava/io/InputStreamReader;
        //   211: astore          8
        //   213: new             Ljava/io/FileInputStream;
        //   216: astore_1       
        //   217: aload_1        
        //   218: aload_2        
        //   219: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   222: aload           8
        //   224: aload_1        
        //   225: ldc_w           "utf-8"
        //   228: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;Ljava/lang/String;)V
        //   231: aload           7
        //   233: aload           8
        //   235: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //   238: aload           7
        //   240: astore          8
        //   242: aload           7
        //   244: astore_1       
        //   245: aload           7
        //   247: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //   250: astore          10
        //   252: aload           7
        //   254: astore          8
        //   256: aload           7
        //   258: astore_1       
        //   259: new             Lcom/navdy/obd/PidSet;
        //   262: astore          9
        //   264: aload           7
        //   266: astore          8
        //   268: aload           7
        //   270: astore_1       
        //   271: aload           9
        //   273: invokespecial   com/navdy/obd/PidSet.<init>:()V
        //   276: aload           10
        //   278: ifnull          379
        //   281: aload           7
        //   283: astore          8
        //   285: aload           7
        //   287: astore_1       
        //   288: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //   291: astore          6
        //   293: aload           7
        //   295: astore          8
        //   297: aload           7
        //   299: astore_1       
        //   300: new             Ljava/lang/StringBuilder;
        //   303: astore_2       
        //   304: aload           7
        //   306: astore          8
        //   308: aload           7
        //   310: astore_1       
        //   311: aload_2        
        //   312: invokespecial   java/lang/StringBuilder.<init>:()V
        //   315: aload           7
        //   317: astore          8
        //   319: aload           7
        //   321: astore_1       
        //   322: aload           6
        //   324: aload_2        
        //   325: ldc_w           "Supported pids recorded "
        //   328: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   331: aload           10
        //   333: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   336: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   339: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   342: aload           7
        //   344: astore          8
        //   346: aload           7
        //   348: astore_1       
        //   349: aload           10
        //   351: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //   354: ldc_w           "-1"
        //   357: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   360: ifeq            678
        //   363: aload           7
        //   365: astore          8
        //   367: aload           7
        //   369: astore_1       
        //   370: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //   373: ldc_w           "There are no supported pids when trying to playback the data"
        //   376: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //   379: aload           7
        //   381: astore          8
        //   383: aload           7
        //   385: astore_1       
        //   386: invokestatic    com/navdy/hud/app/obd/ObdManager.getInstance:()Lcom/navdy/hud/app/obd/ObdManager;
        //   389: aload           9
        //   391: invokevirtual   com/navdy/hud/app/obd/ObdManager.setSupportedPidSet:(Lcom/navdy/obd/PidSet;)V
        //   394: aload           7
        //   396: astore          8
        //   398: aload           7
        //   400: astore_1       
        //   401: aload           7
        //   403: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //   406: astore          9
        //   408: aload           9
        //   410: ifnull          831
        //   413: aload           7
        //   415: astore          8
        //   417: aload           7
        //   419: astore_1       
        //   420: aload           9
        //   422: ldc_w           ","
        //   425: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //   428: astore          6
        //   430: aload           6
        //   432: ifnull          394
        //   435: aload           7
        //   437: astore          8
        //   439: aload           7
        //   441: astore_1       
        //   442: aload           6
        //   444: arraylength    
        //   445: istore          12
        //   447: iload           12
        //   449: iconst_1       
        //   450: if_icmple       394
        //   453: aload           7
        //   455: astore          8
        //   457: aload           7
        //   459: astore_1       
        //   460: aload           6
        //   462: iconst_0       
        //   463: aaload         
        //   464: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   467: lstore          4
        //   469: aload           7
        //   471: astore          8
        //   473: aload           7
        //   475: astore_1       
        //   476: new             Ljava/util/ArrayList;
        //   479: astore          9
        //   481: aload           7
        //   483: astore          8
        //   485: aload           7
        //   487: astore_1       
        //   488: aload           9
        //   490: invokespecial   java/util/ArrayList.<init>:()V
        //   493: iconst_1       
        //   494: istore          12
        //   496: aload           7
        //   498: astore          8
        //   500: aload           7
        //   502: astore_1       
        //   503: iload           12
        //   505: aload           6
        //   507: arraylength    
        //   508: if_icmpge       394
        //   511: aload           7
        //   513: astore          8
        //   515: aload           7
        //   517: astore_1       
        //   518: aload           6
        //   520: iload           12
        //   522: aaload         
        //   523: ldc_w           ":"
        //   526: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //   529: astore_2       
        //   530: aload           7
        //   532: astore          8
        //   534: aload           7
        //   536: astore_1       
        //   537: aload_2        
        //   538: iconst_0       
        //   539: aaload         
        //   540: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   543: istore          13
        //   545: aload           7
        //   547: astore          8
        //   549: aload           7
        //   551: astore_1       
        //   552: aload_2        
        //   553: iconst_1       
        //   554: aaload         
        //   555: invokestatic    java/lang/Double.parseDouble:(Ljava/lang/String;)D
        //   558: dstore          14
        //   560: aload           7
        //   562: astore          8
        //   564: aload           7
        //   566: astore_1       
        //   567: new             Lcom/navdy/obd/Pid;
        //   570: astore_2       
        //   571: aload           7
        //   573: astore          8
        //   575: aload           7
        //   577: astore_1       
        //   578: aload_2        
        //   579: iload           13
        //   581: invokespecial   com/navdy/obd/Pid.<init>:(I)V
        //   584: aload           7
        //   586: astore          8
        //   588: aload           7
        //   590: astore_1       
        //   591: aload_2        
        //   592: dload           14
        //   594: invokevirtual   com/navdy/obd/Pid.setValue:(D)V
        //   597: aload           7
        //   599: astore          8
        //   601: aload           7
        //   603: astore_1       
        //   604: aload           9
        //   606: aload_2        
        //   607: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   612: pop            
        //   613: aload           7
        //   615: astore          8
        //   617: aload           7
        //   619: astore_1       
        //   620: aload_0        
        //   621: getfield        com/navdy/hud/app/debug/DriveRecorder.recordedObdData:Ljava/util/List;
        //   624: astore_2       
        //   625: aload           7
        //   627: astore          8
        //   629: aload           7
        //   631: astore_1       
        //   632: new             Ljava/util/AbstractMap$SimpleEntry;
        //   635: astore          10
        //   637: aload           7
        //   639: astore          8
        //   641: aload           7
        //   643: astore_1       
        //   644: aload           10
        //   646: lload           4
        //   648: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   651: aload           9
        //   653: invokespecial   java/util/AbstractMap$SimpleEntry.<init>:(Ljava/lang/Object;Ljava/lang/Object;)V
        //   656: aload           7
        //   658: astore          8
        //   660: aload           7
        //   662: astore_1       
        //   663: aload_2        
        //   664: aload           10
        //   666: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   671: pop            
        //   672: iinc            12, 1
        //   675: goto            496
        //   678: aload           7
        //   680: astore          8
        //   682: aload           7
        //   684: astore_1       
        //   685: aload           10
        //   687: ldc_w           ","
        //   690: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //   693: astore          6
        //   695: aload           7
        //   697: astore          8
        //   699: aload           7
        //   701: astore_1       
        //   702: aload           6
        //   704: arraylength    
        //   705: istore          13
        //   707: iconst_0       
        //   708: istore          12
        //   710: iload           12
        //   712: iload           13
        //   714: if_icmpge       379
        //   717: aload           6
        //   719: iload           12
        //   721: aaload         
        //   722: astore_2       
        //   723: aload           7
        //   725: astore          8
        //   727: aload           7
        //   729: astore_1       
        //   730: aload           9
        //   732: aload_2        
        //   733: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   736: invokevirtual   com/navdy/obd/PidSet.add:(I)V
        //   739: iinc            12, 1
        //   742: goto            710
        //   745: astore_1       
        //   746: aload           7
        //   748: astore          8
        //   750: aload           7
        //   752: astore_1       
        //   753: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //   756: ldc_w           "Exception while parsing the supported Pids"
        //   759: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   762: goto            739
        //   765: astore          7
        //   767: aload           8
        //   769: astore_1       
        //   770: aload_0        
        //   771: invokevirtual   com/navdy/hud/app/debug/DriveRecorder.stopPlayback:()V
        //   774: aload           8
        //   776: astore_1       
        //   777: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //   780: aload           7
        //   782: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
        //   785: iconst_0       
        //   786: istore_3       
        //   787: aload           8
        //   789: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   792: goto            41
        //   795: astore          9
        //   797: aload           7
        //   799: astore          8
        //   801: aload           7
        //   803: astore_1       
        //   804: getstatic       com/navdy/hud/app/debug/DriveRecorder.sLogger:Lcom/navdy/service/library/log/Logger;
        //   807: ldc_w           "Error parsing the Obd data from the file "
        //   810: aload           9
        //   812: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   815: goto            394
        //   818: astore          7
        //   820: aload_1        
        //   821: astore          8
        //   823: aload           8
        //   825: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   828: aload           7
        //   830: athrow         
        //   831: iconst_1       
        //   832: istore_3       
        //   833: aload           7
        //   835: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   838: goto            41
        //   841: astore_1       
        //   842: aload           9
        //   844: astore          8
        //   846: aload_1        
        //   847: astore          7
        //   849: goto            823
        //   852: astore_1       
        //   853: aload           9
        //   855: astore          8
        //   857: aload_1        
        //   858: astore          7
        //   860: goto            767
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  108    113    765    767    Ljava/lang/Throwable;
        //  108    113    818    823    Any
        //  120    125    765    767    Ljava/lang/Throwable;
        //  120    125    818    823    Any
        //  132    137    765    767    Ljava/lang/Throwable;
        //  132    137    818    823    Any
        //  144    150    765    767    Ljava/lang/Throwable;
        //  144    150    818    823    Any
        //  157    167    765    767    Ljava/lang/Throwable;
        //  157    167    818    823    Any
        //  174    181    765    767    Ljava/lang/Throwable;
        //  174    181    818    823    Any
        //  181    238    852    863    Ljava/lang/Throwable;
        //  181    238    841    852    Any
        //  245    252    765    767    Ljava/lang/Throwable;
        //  245    252    818    823    Any
        //  259    264    765    767    Ljava/lang/Throwable;
        //  259    264    818    823    Any
        //  271    276    765    767    Ljava/lang/Throwable;
        //  271    276    818    823    Any
        //  288    293    765    767    Ljava/lang/Throwable;
        //  288    293    818    823    Any
        //  300    304    765    767    Ljava/lang/Throwable;
        //  300    304    818    823    Any
        //  311    315    765    767    Ljava/lang/Throwable;
        //  311    315    818    823    Any
        //  322    342    765    767    Ljava/lang/Throwable;
        //  322    342    818    823    Any
        //  349    363    765    767    Ljava/lang/Throwable;
        //  349    363    818    823    Any
        //  370    379    765    767    Ljava/lang/Throwable;
        //  370    379    818    823    Any
        //  386    394    765    767    Ljava/lang/Throwable;
        //  386    394    818    823    Any
        //  401    408    765    767    Ljava/lang/Throwable;
        //  401    408    818    823    Any
        //  420    430    765    767    Ljava/lang/Throwable;
        //  420    430    818    823    Any
        //  442    447    765    767    Ljava/lang/Throwable;
        //  442    447    818    823    Any
        //  460    469    795    818    Ljava/lang/NumberFormatException;
        //  460    469    765    767    Ljava/lang/Throwable;
        //  460    469    818    823    Any
        //  476    481    795    818    Ljava/lang/NumberFormatException;
        //  476    481    765    767    Ljava/lang/Throwable;
        //  476    481    818    823    Any
        //  488    493    795    818    Ljava/lang/NumberFormatException;
        //  488    493    765    767    Ljava/lang/Throwable;
        //  488    493    818    823    Any
        //  503    511    795    818    Ljava/lang/NumberFormatException;
        //  503    511    765    767    Ljava/lang/Throwable;
        //  503    511    818    823    Any
        //  518    530    795    818    Ljava/lang/NumberFormatException;
        //  518    530    765    767    Ljava/lang/Throwable;
        //  518    530    818    823    Any
        //  537    545    795    818    Ljava/lang/NumberFormatException;
        //  537    545    765    767    Ljava/lang/Throwable;
        //  537    545    818    823    Any
        //  552    560    795    818    Ljava/lang/NumberFormatException;
        //  552    560    765    767    Ljava/lang/Throwable;
        //  552    560    818    823    Any
        //  567    571    795    818    Ljava/lang/NumberFormatException;
        //  567    571    765    767    Ljava/lang/Throwable;
        //  567    571    818    823    Any
        //  578    584    795    818    Ljava/lang/NumberFormatException;
        //  578    584    765    767    Ljava/lang/Throwable;
        //  578    584    818    823    Any
        //  591    597    795    818    Ljava/lang/NumberFormatException;
        //  591    597    765    767    Ljava/lang/Throwable;
        //  591    597    818    823    Any
        //  604    613    795    818    Ljava/lang/NumberFormatException;
        //  604    613    765    767    Ljava/lang/Throwable;
        //  604    613    818    823    Any
        //  620    625    795    818    Ljava/lang/NumberFormatException;
        //  620    625    765    767    Ljava/lang/Throwable;
        //  620    625    818    823    Any
        //  632    637    795    818    Ljava/lang/NumberFormatException;
        //  632    637    765    767    Ljava/lang/Throwable;
        //  632    637    818    823    Any
        //  644    656    795    818    Ljava/lang/NumberFormatException;
        //  644    656    765    767    Ljava/lang/Throwable;
        //  644    656    818    823    Any
        //  663    672    795    818    Ljava/lang/NumberFormatException;
        //  663    672    765    767    Ljava/lang/Throwable;
        //  663    672    818    823    Any
        //  685    695    765    767    Ljava/lang/Throwable;
        //  685    695    818    823    Any
        //  702    707    765    767    Ljava/lang/Throwable;
        //  702    707    818    823    Any
        //  730    739    745    765    Ljava/lang/Exception;
        //  730    739    765    767    Ljava/lang/Throwable;
        //  730    739    818    823    Any
        //  753    762    765    767    Ljava/lang/Throwable;
        //  753    762    818    823    Any
        //  770    774    818    823    Any
        //  777    785    818    823    Any
        //  804    815    765    767    Ljava/lang/Throwable;
        //  804    815    818    823    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0379:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void execute(final Runnable runnable) {
        TaskManager.getInstance().execute(runnable, 9);
    }
    
    public void flushRecordings() {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                if (DriveRecorder.this.isRecording) {
                    DriveRecorder.this.obdAsyncBufferedFileWriter.flush();
                    DriveRecorder.this.driveScoreDataBufferedFileWriter.flush();
                }
            }
        });
    }
    
    public boolean isDemoAvailable() {
        boolean b;
        if (!this.demoRouteLogFileExists || this.realObdConnected) {
            final Logger sLogger = DriveRecorder.sLogger;
            String s;
            if (!this.demoRouteLogFileExists) {
                s = "Demo log file does not exist in the map partition. So Demo preference : NA";
            }
            else {
                s = "Obd has been connected so skipping the demo";
            }
            sLogger.d(s);
            b = false;
        }
        else {
            b = true;
        }
        return b;
    }
    
    public boolean isDemoPaused() {
        boolean b = false;
        try {
            if (this.isPaused() || this.routeRecorder.isPaused()) {
                b = true;
            }
            return b;
        }
        catch (Throwable t) {
            DriveRecorder.sLogger.e(t);
            return b;
        }
    }
    
    public boolean isDemoPlaying() {
        boolean b = false;
        try {
            if (this.isPlaying() || this.routeRecorder.isPlaying()) {
                b = true;
            }
            return b;
        }
        catch (Throwable t) {
            DriveRecorder.sLogger.e(t);
            return b;
        }
    }
    
    public boolean isDemoStopped() {
        boolean b2;
        final boolean b = b2 = false;
        try {
            if (this.isStopped()) {
                final boolean stopped = this.routeRecorder.isStopped();
                b2 = b;
                if (stopped) {
                    b2 = true;
                }
            }
            return b2;
        }
        catch (Throwable t) {
            DriveRecorder.sLogger.e(t);
            b2 = b;
            return b2;
        }
    }
    
    public void load() {
        DriveRecorder.sLogger.d("Loading the Obd Data recorder");
        this.demoRouteLogFileExists = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + "demo_log.log").exists();
        DriveRecorder.sLogger.d("Demo file exists : " + this.demoRouteLogFileExists);
        this.demoObdLogFileExists = new File(PathManager.getInstance().getMapsPartitionPath() + File.separator + "demo_log.log" + ".obd").exists();
        DriveRecorder.sLogger.d("Demo obd file exists : " + this.demoObdLogFileExists);
    }
    
    @Subscribe
    public void onCalibratedGForceData(final CalibratedGForceData calibratedGForceData) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            this.driveScoreDataBufferedFileWriter.write(this.gforceDataBuilder.append(System.currentTimeMillis()).append(",").append("G,").append(calibratedGForceData.getXAccel()).append(",").append(calibratedGForceData.getYAccel()).append(",").append(calibratedGForceData.getZAccel()).append("\n").toString());
        }
    }
    
    @Subscribe
    public void onDriveScoreUpdatedEvent(final TelemetryDataManager.DriveScoreUpdated driveScoreUpdated) {
        if (this.isRecording && this.driveScoreDataBufferedFileWriter != null) {
            this.gforceDataBuilder.setLength(0);
            this.driveScoreDataBufferedFileWriter.write(this.gforceDataBuilder.append(System.currentTimeMillis()).append(",").append("D,").append("DS:").append(driveScoreUpdated.getDriveScore()).append(",").append("DE:").append(driveScoreUpdated.getInterestingEvent()).append(",").append("SD:").append(TelemetrySession.INSTANCE.getSessionDuration()).append(",").append("RD:").append(TelemetrySession.INSTANCE.getSessionRollingDuration()).append(",").append("SPD:").append(TelemetrySession.INSTANCE.getSpeedingDuration()).append(",").append("ESD:").append(TelemetrySession.INSTANCE.getExcessiveSpeedingDuration()).append(",").append("HA:").append(TelemetrySession.INSTANCE.getSessionHardAccelerationCount()).append(",").append("HB:").append(TelemetrySession.INSTANCE.getSessionHardBrakingCount()).append(",").append("HG:").append(TelemetrySession.INSTANCE.getSessionHighGCount()).append(",").append("LT:").append(getLatitude()).append(",").append("LN:").append(getLongitude()).append("\n").toString());
        }
    }
    
    @Subscribe
    public void onKeyEvent(final KeyEvent keyEvent) {
        if (keyEvent != null && keyEvent.getKeyCode() == 36 && keyEvent.getAction() == 1) {
            this.performDemoPlaybackAction(Action.STOP, false);
        }
        else if (keyEvent != null && keyEvent.getKeyCode() == 53 && keyEvent.getAction() == 1) {
            this.performDemoPlaybackAction(Action.RESTART, false);
        }
        else if (keyEvent != null && keyEvent.getKeyCode() == 44 && keyEvent.getAction() == 1) {
            this.performDemoPlaybackAction(Action.PRELOAD, false);
        }
    }
    
    @Subscribe
    public void onMapEngineInitialized(final MapEvents.MapEngineInitialize mapEngineInitialize) {
        DriveRecorder.sLogger.d("onMapEnigneInitialzed : message received. Initialized :" + mapEngineInitialize.initialized);
        this.isEngineInitialized = mapEngineInitialize.initialized;
        this.checkAndStartAutoRecording();
    }
    
    public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        this.routeRecorder = IRouteRecorder.Stub.asInterface(binder);
    }
    
    public void onServiceDisconnected(final ComponentName componentName) {
        this.routeRecorder = null;
    }
    
    @Subscribe
    public void onStartPlayback(final StartDrivePlaybackEvent startDrivePlaybackEvent) {
        DriveRecorder.sLogger.d("Start playback event received");
        this.startPlayback(startDrivePlaybackEvent.label, false);
    }
    
    @Subscribe
    public void onStartRecordingEvent(final StartDriveRecordingEvent startDriveRecordingEvent) {
        DriveRecorder.sLogger.d("Start recording event received");
        this.startRecording(startDriveRecordingEvent.label);
    }
    
    @Subscribe
    public void onStopPlayback(final StopDrivePlaybackEvent stopDrivePlaybackEvent) {
        DriveRecorder.sLogger.d("Stop playback event received");
        this.stopPlayback();
    }
    
    @Subscribe
    public void onStopRecordingEvent(final StopDriveRecordingEvent stopDriveRecordingEvent) {
        DriveRecorder.sLogger.d("Stop recording event received");
        this.stopRecording();
    }
    
    public void pausePlayback() {
        if (!this.isPlaying()) {
            DriveRecorder.sLogger.d("Playback is not happening so not pausing");
        }
        else {
            DriveRecorder.sLogger.d("Pausing the playback");
            this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            this.playbackState = State.PAUSED;
        }
    }
    
    public void performDemoPlaybackAction(final Action action, final boolean b) {
        DriveRecorder.sLogger.d(":performDemoPlaybackAction");
        if (!this.isEngineInitialized) {
            DriveRecorder.sLogger.e("Engine is not initialized so can not start the playback");
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    if (DriveRecorder.this.isDemoAvailable()) {
                        switch (action) {
                            case PLAY:
                                DriveRecorder.this.playDemo(b);
                                break;
                            case PAUSE:
                                DriveRecorder.this.pauseDemo();
                                break;
                            case RESUME:
                                DriveRecorder.this.resumeDemo();
                                break;
                            case RESTART:
                                DriveRecorder.this.restartDemo(b);
                                break;
                            case STOP:
                                DriveRecorder.this.stopDemo();
                                break;
                            case PRELOAD:
                                DriveRecorder.this.prepareDemo();
                                break;
                        }
                    }
                }
            }, 1);
        }
    }
    
    public void prepare(final String s) {
        if (this.isPlaying() || this.isPaused()) {
            DriveRecorder.sLogger.e("Cannot prepare for playback as the playback is in progress");
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    if (DriveRecorder.this.bPrepare(s)) {
                        DriveRecorder.sLogger.d("Prepare succeeded");
                    }
                    else {
                        DriveRecorder.sLogger.e("Prepare failed");
                    }
                }
            }, 9);
        }
    }
    
    public void release() {
        this.stopPlayback();
        if (this.recordedObdData != null) {
            this.recordedObdData = null;
        }
        this.preparedFileName = null;
        this.preparedFileLastModifiedTime = -1L;
    }
    
    public boolean restartPlayback() {
        boolean b = false;
        if (this.isPlaying() || this.isPaused()) {
            this.playbackState = State.PLAYING;
            this.currentObdDataInjectionIndex = 0;
            this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            this.obdDataPlaybackHandler.post(this.injectFakeObdData);
            b = true;
        }
        return b;
    }
    
    public void resumePlayback() {
        if (this.isPaused()) {
            this.playbackState = State.PLAYING;
            this.obdDataPlaybackHandler.post(this.injectFakeObdData);
        }
    }
    
    public void setRealObdConnected(final boolean realObdConnected) {
        this.realObdConnected = realObdConnected;
        DriveRecorder.sLogger.d("setRealObdConnected : state " + realObdConnected);
        if (this.realObdConnected) {
            this.performDemoPlaybackAction(Action.STOP, true);
        }
    }
    
    public void startPlayback(final String s, final boolean isLooping) {
        if (this.isPlaying() || this.isRecording) {
            DriveRecorder.sLogger.v("already busy, no-op");
        }
        else {
            DriveRecorder.sLogger.d("Starting the playback");
            this.playbackState = State.PLAYING;
            this.isLooping = isLooping;
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    if (!DriveRecorder.this.bPrepare(s)) {
                        DriveRecorder.sLogger.e("Failed to prepare for playback , file : " + s);
                        DriveRecorder.this.stopPlayback();
                    }
                    else {
                        DriveRecorder.sLogger.d("Prepare succeeded");
                        try {
                            DriveRecorder.this.currentObdDataInjectionIndex = 0;
                            DriveRecorder.this.obdPlaybackThread = new HandlerThread("ObdPlayback");
                            DriveRecorder.this.obdPlaybackThread.start();
                            DriveRecorder.this.obdDataPlaybackHandler = new Handler(DriveRecorder.this.obdPlaybackThread.getLooper());
                            DriveRecorder.this.obdDataPlaybackHandler.post(DriveRecorder.this.injectFakeObdData);
                        }
                        catch (Throwable t) {
                            DriveRecorder.this.stopPlayback();
                            DriveRecorder.sLogger.e(t);
                        }
                    }
                }
            }, 9);
        }
    }
    
    public void startRecording(final String s) {
        if (this.isRecording || this.isPlaying()) {
            DriveRecorder.sLogger.v("Obd data recorder is already busy");
        }
        else {
            DriveRecorder.sLogger.d("Starting the recording");
            this.isRecording = true;
            this.isSupportedPidsWritten = false;
            TaskManager.getInstance().execute(this.createDriveRecord(s), 9);
        }
    }
    
    public void stopPlayback() {
        if (!this.isPlaying()) {
            DriveRecorder.sLogger.v("already stopped, no-op");
        }
        else {
            DriveRecorder.sLogger.d("Stopping the playback");
            this.currentObdDataInjectionIndex = 0;
            if (this.obdDataPlaybackHandler != null) {
                this.obdDataPlaybackHandler.removeCallbacks(this.injectFakeObdData);
            }
            if (this.obdPlaybackThread != null && this.obdPlaybackThread.isAlive()) {
                DriveRecorder.sLogger.v("obd handler thread quit:" + this.obdPlaybackThread.quit());
            }
            this.obdPlaybackThread = null;
            this.isLooping = false;
            this.playbackState = State.STOPPED;
        }
    }
    
    public void stopRecording() {
        if (!this.isRecording) {
            DriveRecorder.sLogger.v("already stopped, no-op");
        }
        else {
            DriveRecorder.sLogger.d("Stopping the Obd recording");
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    DriveRecorder.this.isRecording = false;
                    DriveRecorder.this.isSupportedPidsWritten = false;
                    ObdManager.getInstance().setRecordingPidListener(null);
                    DriveRecorder.this.obdAsyncBufferedFileWriter.flushAndClose();
                    DriveRecorder.this.driveScoreDataBufferedFileWriter.flushAndClose();
                }
            });
        }
    }
    
    public enum Action
    {
        PAUSE, 
        PLAY, 
        PRELOAD, 
        RESTART, 
        RESUME, 
        STOP;
    }
    
    public enum DemoPreference
    {
        DISABLED, 
        ENABLED, 
        NA;
    }
    
    public static class FilesModifiedTimeComparator implements Comparator<File>
    {
        @Override
        public int compare(final File file, final File file2) {
            return Long.compare(file.lastModified(), file2.lastModified());
        }
    }
    
    enum State
    {
        PAUSED, 
        PLAYING, 
        STOPPED;
    }
}
