package com.navdy.hud.app.debug;

import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IRouteRecorder extends IInterface
{
    boolean isPaused() throws RemoteException;
    
    boolean isPlaying() throws RemoteException;
    
    boolean isRecording() throws RemoteException;
    
    boolean isStopped() throws RemoteException;
    
    void pausePlayback() throws RemoteException;
    
    void prepare(final String p0, final boolean p1) throws RemoteException;
    
    boolean restartPlayback() throws RemoteException;
    
    void resumePlayback() throws RemoteException;
    
    void startPlayback(final String p0, final boolean p1, final boolean p2) throws RemoteException;
    
    String startRecording(final String p0) throws RemoteException;
    
    void stopPlayback() throws RemoteException;
    
    void stopRecording() throws RemoteException;
    
    public abstract static class Stub extends Binder implements IRouteRecorder
    {
        private static final String DESCRIPTOR = "com.navdy.hud.app.debug.IRouteRecorder";
        static final int TRANSACTION_isPaused = 11;
        static final int TRANSACTION_isPlaying = 10;
        static final int TRANSACTION_isRecording = 4;
        static final int TRANSACTION_isStopped = 12;
        static final int TRANSACTION_pausePlayback = 6;
        static final int TRANSACTION_prepare = 1;
        static final int TRANSACTION_restartPlayback = 9;
        static final int TRANSACTION_resumePlayback = 7;
        static final int TRANSACTION_startPlayback = 5;
        static final int TRANSACTION_startRecording = 2;
        static final int TRANSACTION_stopPlayback = 8;
        static final int TRANSACTION_stopRecording = 3;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.hud.app.debug.IRouteRecorder");
        }
        
        public static IRouteRecorder asInterface(final IBinder binder) {
            IRouteRecorder routeRecorder;
            if (binder == null) {
                routeRecorder = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.hud.app.debug.IRouteRecorder");
                if (queryLocalInterface != null && queryLocalInterface instanceof IRouteRecorder) {
                    routeRecorder = (IRouteRecorder)queryLocalInterface;
                }
                else {
                    routeRecorder = new Proxy(binder);
                }
            }
            return routeRecorder;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            final int n3 = 0;
            final int n4 = 0;
            final int n5 = 0;
            final int n6 = 0;
            final int n7 = 0;
            final boolean b = true;
            boolean onTransact = false;
            switch (n) {
                default:
                    onTransact = super.onTransact(n, parcel, parcel2, n2);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.hud.app.debug.IRouteRecorder");
                    onTransact = b;
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    this.prepare(parcel.readString(), parcel.readInt() != 0);
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 2: {
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    final String startRecording = this.startRecording(parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeString(startRecording);
                    onTransact = b;
                    break;
                }
                case 3:
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    this.stopRecording();
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 4: {
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    final boolean recording = this.isRecording();
                    parcel2.writeNoException();
                    n = n7;
                    if (recording) {
                        n = 1;
                    }
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                }
                case 5:
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    this.startPlayback(parcel.readString(), parcel.readInt() != 0, parcel.readInt() != 0);
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 6:
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    this.pausePlayback();
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 7:
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    this.resumePlayback();
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 8:
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    this.stopPlayback();
                    parcel2.writeNoException();
                    onTransact = b;
                    break;
                case 9: {
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    final boolean restartPlayback = this.restartPlayback();
                    parcel2.writeNoException();
                    n = n3;
                    if (restartPlayback) {
                        n = 1;
                    }
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                }
                case 10: {
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    final boolean playing = this.isPlaying();
                    parcel2.writeNoException();
                    n = n4;
                    if (playing) {
                        n = 1;
                    }
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                }
                case 11: {
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    final boolean paused = this.isPaused();
                    parcel2.writeNoException();
                    n = n5;
                    if (paused) {
                        n = 1;
                    }
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                }
                case 12: {
                    parcel.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                    final boolean stopped = this.isStopped();
                    parcel2.writeNoException();
                    n = n6;
                    if (stopped) {
                        n = 1;
                    }
                    parcel2.writeInt(n);
                    onTransact = b;
                    break;
                }
            }
            return onTransact;
        }
        
        private static class Proxy implements IRouteRecorder
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.hud.app.debug.IRouteRecorder";
            }
            
            @Override
            public boolean isPaused() throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean isPlaying() throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean isRecording() throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean isStopped() throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void pausePlayback() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void prepare(final String s, final boolean b) throws RemoteException {
                int n = 1;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    obtain.writeString(s);
                    if (!b) {
                        n = 0;
                    }
                    obtain.writeInt(n);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean restartPlayback() throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void resumePlayback() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void startPlayback(final String s, final boolean b, final boolean b2) throws RemoteException {
                final boolean b3 = true;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    obtain.writeString(s);
                    int n;
                    if (b) {
                        n = 1;
                    }
                    else {
                        n = 0;
                    }
                    obtain.writeInt(n);
                    int n2;
                    if (b2) {
                        n2 = (b3 ? 1 : 0);
                    }
                    else {
                        n2 = 0;
                    }
                    obtain.writeInt(n2);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String startRecording(String string) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    obtain.writeString(string);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    string = obtain2.readString();
                    return string;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void stopPlayback() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void stopRecording() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
