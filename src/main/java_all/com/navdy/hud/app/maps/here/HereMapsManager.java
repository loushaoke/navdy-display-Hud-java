package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.event.Wakeup;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import java.lang.ref.WeakReference;
import com.here.android.mpa.mapping.MapRoute;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.here.android.mpa.routing.RouteOptions;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import com.navdy.hud.app.storage.PathManager;
import com.here.android.mpa.mapping.MapTransitLayer;
import com.here.android.mpa.mapping.TrafficEvent;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.device.gps.GpsUtils;
import android.os.Bundle;
import android.content.IntentFilter;
import android.content.Intent;
import android.content.BroadcastReceiver;
import java.util.Iterator;
import java.util.List;
import com.here.android.mpa.mapping.MapTrafficLayer;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.service.library.util.SystemUtils;
import android.location.LocationManager;
import java.util.EnumSet;
import com.here.android.mpa.odml.MapPackage;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import com.here.android.mpa.common.MatchedGeoPosition;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.maps.notification.TrafficNotificationManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.task.TaskManager;
import com.here.android.mpa.common.Version;
import android.os.SystemClock;
import android.os.Looper;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.hud.app.manager.SpeedManager;
import android.content.SharedPreferences;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.maps.notification.RouteCalculationEventHandler;
import com.navdy.hud.app.device.PowerManager;
import com.here.android.mpa.common.PositioningManager;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.here.android.mpa.mapping.MapView;
import com.here.android.mpa.odml.MapLoader;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.mapping.Map;
import com.navdy.hud.app.profile.DriverProfileManager;
import android.content.SharedPreferences;
import com.here.android.mpa.common.GeoPosition;
import android.os.Handler;
import com.here.android.mpa.common.OnEngineInitListener;
import android.content.Context;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import android.os.HandlerThread;
import com.navdy.service.library.log.Logger;

public class HereMapsManager
{
    public static final double DEFAULT_LATITUDE = 37.802086;
    public static final double DEFAULT_LONGITUDE = -122.419015;
    public static final float DEFAULT_TILT = 60.0f;
    public static final float DEFAULT_ZOOM_LEVEL = 16.5f;
    private static final String DISABLE_MAPS_PROPERTY = "persist.sys.map.disable";
    private static final boolean ENABLE_MAPS;
    private static final String ENROUTE_MAP_SCHEME = "hybrid.night";
    private static final int GPS_SPEED_LAST_LOCATION_THRESHOLD = 2000;
    static final int MIN_SAME_POSITION_UPDATE_THRESHOLD = 500;
    private static final String MW_CONFIG_EXCEPTION_MSG = "Invalid configuration file. Check MWConfig!";
    private static final String NAVDY_HERE_MAP_SERVICE_NAME = "com.navdy.HereMapService";
    public static final float ROUTE_CALC_START_ZOOM_LEVEL = 15.5f;
    private static final String TRACKING_MAP_SCHEME = "carnav.night";
    private static final String TRAFFIC_REROUTE_PROPERTY = "persist.sys.hud_traffic_reroute";
    private static boolean recalcCurrentRouteForTraffic;
    private static final Logger sLogger;
    private static final HereMapsManager singleton;
    private final HandlerThread bkLocationReceiverHandlerThread;
    @Inject
    Bus bus;
    private Context context;
    private OnEngineInitListener engineInitListener;
    private volatile boolean extrapolationOn;
    private Handler handler;
    private HereLocationFixManager hereLocationFixManager;
    private HereMapAnimator hereMapAnimator;
    private Object initLock;
    private boolean initialMapRendering;
    private GeoPosition lastGeoPosition;
    private long lastGeoPositionTime;
    private SharedPreferences$OnSharedPreferenceChangeListener listener;
    private volatile boolean lowBandwidthDetected;
    @Inject
    DriverProfileManager mDriverProfileManager;
    private Map map;
    private HereMapController mapController;
    private boolean mapDataVerified;
    private MapEngine mapEngine;
    private long mapEngineStartTime;
    private OnEngineInitListener.Error mapError;
    private boolean mapInitLoaderComplete;
    private boolean mapInitialized;
    private boolean mapInitializing;
    private MapLoader mapLoader;
    private int mapPackageCount;
    private MapView mapView;
    private NavigationView navigationView;
    private HereOfflineMapsVersion offlineMapsVersion;
    private PositioningManager.OnPositionChangedListener positionChangedListener;
    private PositioningManager positioningManager;
    private boolean positioningManagerInstalled;
    @Inject
    PowerManager powerManager;
    private RouteCalculationEventHandler routeCalcEventHandler;
    private GeoCoordinate routeStartPoint;
    @Inject
    SharedPreferences sharedPreferences;
    private SpeedManager speedManager;
    private volatile boolean turnEngineOn;
    private boolean voiceSkinsLoaded;
    
    static {
        sLogger = new Logger(HereMapsManager.class);
        ENABLE_MAPS = !SystemProperties.getBoolean("persist.sys.map.disable", false);
        HereMapsManager.recalcCurrentRouteForTraffic = false;
        singleton = new HereMapsManager();
    }
    
    private HereMapsManager() {
        this.initLock = new Object();
        this.initialMapRendering = true;
        this.lowBandwidthDetected = false;
        this.turnEngineOn = false;
        this.mapInitializing = true;
        this.handler = new Handler(Looper.getMainLooper());
        this.engineInitListener = new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(final Error error) {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                HereMapsManager.sLogger.v("MAP-ENGINE-INIT took [" + (elapsedRealtime - HereMapsManager.this.mapEngineStartTime) + "]");
                HereMapsManager.sLogger.v("MAP-ENGINE-INIT HERE-SDK version:" + Version.getSdkVersion());
                if (error == Error.NONE) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            HereMapsManager.this.mapEngine.onResume();
                            HereMapsManager.sLogger.v(":initializing maps config");
                            HereMapsConfigurator.getInstance().updateMapsConfig();
                            HereMapsManager.sLogger.v("creating geopos");
                            new GeoPosition(new GeoCoordinate(37.802086, -122.419015));
                            HereMapsManager.sLogger.v("created geopos");
                            if (!DeviceUtil.isUserBuild()) {
                                final long elapsedRealtime = SystemClock.elapsedRealtime();
                                HereLaneInfoBuilder.init();
                                HereMapsManager.sLogger.v("time to init HereLaneInfoBuilder [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                            }
                            HereMapsManager.sLogger.v("engine initialized refcount:" + HereMapsManager.this.mapEngine.getResourceReferenceCount());
                            while (true) {
                                try {
                                    HereMapsManager.this.map = new Map();
                                    HereMapsManager.this.map.setProjectionMode(Map.Projection.MERCATOR);
                                    HereMapsManager.this.map.setTrafficInfoVisible(true);
                                    HereMapsManager.this.mapController = new HereMapController(HereMapsManager.this.map, HereMapController.State.NONE);
                                    HereMapsManager.this.setMapAttributes(new GeoCoordinate(37.802086, -122.419015));
                                    HereMapsManager.sLogger.v("setting default map attributes");
                                    HereMapsManager.this.setMapTraffic();
                                    HereMapsManager.this.setMapPoiLayer(HereMapsManager.this.map);
                                    HereMapsManager.this.positioningManager = PositioningManager.getInstance();
                                    HereMapsManager.sLogger.v("MAP-ENGINE-INIT-2 took [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                                    HereMapsManager.this.handler.post((Runnable)new Runnable() {
                                        final /* synthetic */ long val$t2 = SystemClock.elapsedRealtime();
                                        
                                        @Override
                                        public void run() {
                                            HereMapsManager.this.positioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK);
                                            HereMapsManager.sLogger.v("position manager started");
                                            HereMapsManager.sLogger.v("MAP-ENGINE-INIT-3 took [" + (SystemClock.elapsedRealtime() - this.val$t2) + "]");
                                            TaskManager.getInstance().execute(new Runnable() {
                                                final /* synthetic */ long val$t3 = SystemClock.elapsedRealtime();
                                                
                                                @Override
                                                public void run() {
                                                    final HereNavigationManager instance = HereNavigationManager.getInstance();
                                                    HereMapsManager.this.registerConnectivityReceiver();
                                                    HereMapsManager.sLogger.v("map created");
                                                    TrafficNotificationManager.getInstance();
                                                    Label_0569: {
                                                        if (!MapSettings.isCustomAnimationEnabled()) {
                                                            break Label_0569;
                                                        }
                                                        Object o = HereMapAnimator.AnimationMode.ANIMATION;
                                                    Block_6_Outer:
                                                        while (true) {
                                                            HereMapsManager.sLogger.v("HereMapAnimator mode is [" + o + "]");
                                                            HereMapsManager.this.hereMapAnimator = new HereMapAnimator((HereMapAnimator.AnimationMode)o, HereMapsManager.this.mapController, instance.getNavController());
                                                            if (!HereMapsManager.this.initialMapRendering) {
                                                                HereMapsManager.sLogger.v("HereMapAnimator MapRendering initial disabled");
                                                                HereMapsManager.this.hereMapAnimator.stopMapRendering();
                                                            }
                                                            HereMapCameraManager.getInstance().initialize(HereMapsManager.this.mapController, HereMapsManager.this.bus, HereMapsManager.this.hereMapAnimator);
                                                            HereMapsManager.this.hereLocationFixManager = new HereLocationFixManager(HereMapsManager.this.bus);
                                                            HereMapsManager.sLogger.v("MAP-ENGINE-INIT-4 took [" + (SystemClock.elapsedRealtime() - this.val$t3) + "]");
                                                            HereMapsManager.this.routeCalcEventHandler = new RouteCalculationEventHandler(HereMapsManager.this.bus);
                                                            HereMapsManager.this.initMapLoader();
                                                            HereMapsManager.this.offlineMapsVersion = new HereOfflineMapsVersion(HereMapsManager.this.getOfflineMapsData());
                                                            o = HereMapsManager.this.initLock;
                                                            synchronized (o) {
                                                                HereMapsManager.this.mapInitializing = false;
                                                                HereMapsManager.this.mapInitialized = true;
                                                                HereMapsManager.this.bus.post(new MapEvents.MapEngineInitialize(HereMapsManager.this.mapInitialized));
                                                                HereMapsManager.sLogger.v("MAP-ENGINE-INIT event sent");
                                                                // monitorexit(o)
                                                                HereMapsManager.sLogger.v("setEngineOnlineState initial");
                                                                HereMapsManager.this.setEngineOnlineState();
                                                                if (!HereMapsManager.this.powerManager.inQuietMode()) {
                                                                    HereMapsManager.this.startTrafficUpdater();
                                                                }
                                                                return;
                                                                // iftrue(Label_0582:, !MapSettings.isFullCustomAnimatonEnabled())
                                                                while (true) {
                                                                    o = HereMapAnimator.AnimationMode.ZOOM_TILT_ANIMATION;
                                                                    continue Block_6_Outer;
                                                                    continue;
                                                                }
                                                                Label_0582: {
                                                                    o = HereMapAnimator.AnimationMode.NONE;
                                                                }
                                                                continue Block_6_Outer;
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                            }, 3);
                                        }
                                    });
                                }
                                catch (Throwable t) {
                                    if (t.getMessage().contains("Invalid configuration file. Check MWConfig!")) {
                                        HereMapsManager.sLogger.e("MWConfig is corrupted! Cleaning up and restarting HUD app.");
                                        HereMapsConfigurator.getInstance().removeMWConfigFolder();
                                    }
                                    HereMapsManager.this.handler.post((Runnable)new Runnable() {
                                        @Override
                                        public void run() {
                                            throw new MWConfigCorruptException(t);
                                        }
                                    });
                                    continue;
                                }
                                break;
                            }
                        }
                    }, 3);
                }
                else {
                    final String string = error.toString();
                    AnalyticsSupport.recordKeyFailure("HereMaps", string);
                    HereMapsManager.sLogger.e("MAP-ENGINE-INIT engine NOT initialized:" + error);
                    HereMapsManager.this.mapError = error;
                    final Object access$1700 = HereMapsManager.this.initLock;
                    synchronized (access$1700) {
                        HereMapsManager.this.mapInitializing = false;
                        HereMapsManager.this.bus.post(new MapEvents.MapEngineInitialize(HereMapsManager.this.mapInitialized));
                        // monitorexit(access$1700)
                        if (error == Error.USAGE_EXPIRED || error == Error.MISSING_APP_CREDENTIAL) {
                            Main.handleLibraryInitializationError("Here Maps initialization failure: " + string);
                        }
                    }
                }
            }
        };
        this.positionChangedListener = new PositioningManager.OnPositionChangedListener() {
            @Override
            public void onPositionFixChanged(final LocationMethod locationMethod, final LocationStatus locationStatus) {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (HereMapsManager.sLogger.isLoggable(2)) {
                                HereMapsManager.sLogger.d("onPositionFixChanged: method:" + locationMethod + " status: " + locationStatus);
                            }
                            if (locationMethod == LocationMethod.GPS && HereMapUtil.isInTunnel(HereMapsManager.this.positioningManager.getRoadElement()) && !HereMapsManager.this.extrapolationOn) {
                                HereMapsManager.sLogger.d("TUNNEL extrapolation on");
                                HereMapsManager.this.extrapolationOn = true;
                                HereMapsManager.this.sendExtrapolationEvent();
                            }
                        }
                        catch (Throwable t) {
                            HereMapsManager.sLogger.e(t);
                        }
                    }
                }, 18);
            }
            
            @Override
            public void onPositionUpdated(final LocationMethod locationMethod, final GeoPosition geoPosition, final boolean b) {
                if (geoPosition != null) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            while (true) {
                            Label_0267:
                                while (true) {
                                    try {
                                        if (!(geoPosition instanceof MatchedGeoPosition)) {
                                            final GeoCoordinate coordinate = geoPosition.getCoordinate();
                                            HereMapsManager.sLogger.v("position not map-matched lat:" + coordinate.getLatitude() + " lng:" + coordinate.getLongitude() + " speed:" + geoPosition.getSpeed());
                                        }
                                        else {
                                            if (HereMapsManager.this.extrapolationOn) {
                                                if (!HereMapUtil.isInTunnel(HereMapsManager.this.positioningManager.getRoadElement())) {
                                                    HereMapsManager.sLogger.i("TUNNEL extrapolation off");
                                                    HereMapsManager.this.extrapolationOn = false;
                                                    HereMapsManager.this.sendExtrapolationEvent();
                                                }
                                                else {
                                                    HereMapsManager.this.sendExtrapolationEvent();
                                                }
                                            }
                                            if (HereMapsManager.this.lastGeoPosition == null) {
                                                break Label_0267;
                                            }
                                            final long n = SystemClock.elapsedRealtime() - HereMapsManager.this.lastGeoPositionTime;
                                            if (n >= 500L || !HereMapUtil.isCoordinateEqual(HereMapsManager.this.lastGeoPosition.getCoordinate(), geoPosition.getCoordinate())) {
                                                break Label_0267;
                                            }
                                            if (HereMapsManager.sLogger.isLoggable(2)) {
                                                HereMapsManager.sLogger.v("GEO-Here same pos as last:" + n);
                                            }
                                        }
                                        return;
                                    }
                                    catch (Throwable t) {
                                        HereMapsManager.sLogger.e(t);
                                        continue;
                                    }
                                    break;
                                }
                                HereMapsManager.this.lastGeoPosition = geoPosition;
                                HereMapsManager.this.lastGeoPositionTime = SystemClock.elapsedRealtime();
                                HereMapsManager.this.hereMapAnimator.setGeoPosition(geoPosition);
                                HereMapsManager.this.hereLocationFixManager.onHerePositionUpdated(locationMethod, geoPosition, b);
                                HereMapCameraManager.getInstance().onGeoPositionChange(geoPosition);
                                if (HereMapsManager.this.speedManager.getObdSpeed() == -1 && SystemClock.elapsedRealtime() - HereMapsManager.this.hereLocationFixManager.getLastLocationTime() >= 2000L && geoPosition instanceof MatchedGeoPosition && ((MatchedGeoPosition)geoPosition).isExtrapolated() && HereMapsManager.this.speedManager.setGpsSpeed((float)geoPosition.getSpeed(), SystemClock.elapsedRealtimeNanos() / 1000000L)) {
                                    HereMapsManager.this.bus.post(new MapEvents.GPSSpeedEvent());
                                }
                                HereMapsManager.this.bus.post(geoPosition);
                                if (HereMapsManager.sLogger.isLoggable(2)) {
                                    final GeoCoordinate coordinate2 = geoPosition.getCoordinate();
                                    HereMapsManager.sLogger.v("GEO-Here speed-mps[" + geoPosition.getSpeed() + "] " + "] lat=[" + coordinate2.getLatitude() + "] lon=[" + coordinate2.getLongitude() + "] provider=[" + locationMethod.name() + "] timestamp:[" + geoPosition.getTimestamp().getTime() + "]");
                                }
                            }
                        }
                    }, 18);
                }
            }
        };
        this.listener = (SharedPreferences$OnSharedPreferenceChangeListener)new SharedPreferences$OnSharedPreferenceChangeListener() {
            public void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String s) {
                HereMapsManager.sLogger.d("onSharedPreferenceChanged: " + s);
                if (!HereMapsManager.this.isInitialized()) {
                    HereMapsManager.sLogger.w("onSharedPreferenceChanged: map engine not intialized:" + s);
                }
                else {
                    switch (s) {
                        case "map.tilt": {
                            final String string = sharedPreferences.getString(s, (String)null);
                            if (string == null) {
                                HereMapsManager.sLogger.v("onSharedPreferenceChanged:no tilt");
                                break;
                            }
                            try {
                                final float float1 = Float.parseFloat(string);
                                HereMapsManager.sLogger.w("onSharedPreferenceChanged:tilt:" + float1);
                                HereMapsManager.this.mapController.setTilt(float1);
                            }
                            catch (Throwable t) {
                                HereMapsManager.sLogger.e("onSharedPreferenceChanged:tilt", t);
                            }
                            break;
                        }
                        case "map.zoom": {
                            final String string2 = sharedPreferences.getString(s, (String)null);
                            if (string2 == null) {
                                HereMapsManager.sLogger.v("onSharedPreferenceChanged:no zoom");
                                break;
                            }
                            try {
                                final float float2 = Float.parseFloat(string2);
                                HereMapsManager.sLogger.w("onSharedPreferenceChanged:zoom:" + float2);
                                HereMapsManager.this.mapController.setZoomLevel(float2);
                            }
                            catch (Throwable t2) {
                                HereMapsManager.sLogger.e("onSharedPreferenceChanged:zoom", t2);
                            }
                            break;
                        }
                        case "map.scheme":
                            TaskManager.getInstance().execute(new Runnable() {
                                @Override
                                public void run() {
                                    if (!HereMapsManager.this.mapController.getMapScheme().equals("hybrid.night")) {
                                        HereMapsManager.this.mapController.setMapScheme(HereMapsManager.this.getTrackingMapScheme());
                                    }
                                }
                            }, 2);
                            break;
                    }
                }
            }
        };
        Mortar.inject(this.context = HudApplication.getAppContext(), this);
        this.speedManager = SpeedManager.getInstance();
        if (HereMapsManager.ENABLE_MAPS) {
            this.checkforNetworkProvider();
        }
        (this.bkLocationReceiverHandlerThread = new HandlerThread("here_bk_location")).start();
        this.bus.register(this);
    }
    
    private void checkforNetworkProvider() {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                boolean b = false;
                try {
                    if (((LocationManager)HudApplication.getAppContext().getSystemService("location")).getProvider("network") == null) {
                        HereMapsManager.sLogger.v("n/w provider not found yet, check again");
                        b = true;
                    }
                    else {
                        HereMapsManager.sLogger.v("n/w provider found, initialize here");
                        TaskManager.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                HereMapsManager.this.initialize();
                            }
                        }, 2);
                    }
                    if (b) {
                        HereMapsManager.this.handler.postDelayed((Runnable)this, 1000L);
                    }
                }
                catch (Throwable t) {
                    HereMapsManager.sLogger.e(t);
                    if (true) {
                        HereMapsManager.this.handler.postDelayed((Runnable)this, 1000L);
                    }
                }
                finally {
                    if (false) {
                        HereMapsManager.this.handler.postDelayed((Runnable)this, 1000L);
                    }
                }
            }
        });
    }
    
    private float getDefaultTiltLevel() {
        return 60.0f;
    }
    
    private double getDefaultZoomLevel() {
        return 16.5;
    }
    
    public static HereMapsManager getInstance() {
        return HereMapsManager.singleton;
    }
    
    private void initMapLoader() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final long elapsedRealtime = SystemClock.elapsedRealtime();
                HereMapsManager.this.mapLoader = MapLoader.getInstance();
                HereMapsManager.sLogger.v("initMapLoader took [" + (SystemClock.elapsedRealtime() - elapsedRealtime) + "]");
                HereMapsManager.this.mapLoader.addListener((MapLoader.Listener)new MapLoader.Listener() {
                    @Override
                    public void onCheckForUpdateComplete(final boolean b, final String s, final String s2, final ResultCode resultCode) {
                        HereMapsManager.sLogger.v("MapLoader: updateAvailable[" + b + "] currentMapVersion[" + s + "] newestMapVersion[" + s2 + "] resultCode[" + resultCode + "]");
                    }
                    
                    @Override
                    public void onGetMapPackagesComplete(final MapPackage mapPackage, final ResultCode resultCode) {
                        if (resultCode != ResultCode.OPERATION_SUCCESSFUL) {
                            HereMapsManager.sLogger.e("initMapLoader: get map package failed:" + resultCode.name());
                        }
                        else {
                            HereMapsManager.sLogger.v("initMapLoader: get map package success");
                            TaskManager.getInstance().execute(new Runnable() {
                                @Override
                                public void run() {
                                    HereMapsManager.this.printMapPackages(mapPackage, EnumSet.<MapPackage.InstallationState>of(MapPackage.InstallationState.INSTALLED, MapPackage.InstallationState.PARTIALLY_INSTALLED));
                                    HereMapsManager.sLogger.v("initMapLoader: map package count:" + HereMapsManager.this.mapPackageCount);
                                    HereMapsManager.this.mapDataVerified = true;
                                }
                            }, 2);
                        }
                    }
                    
                    @Override
                    public void onInstallMapPackagesComplete(final MapPackage mapPackage, final ResultCode resultCode) {
                        final Logger access$100 = HereMapsManager.sLogger;
                        final StringBuilder append = new StringBuilder().append("MapLoader: onInstallMapPackagesComplete result[").append(resultCode.name()).append("] package[");
                        String title;
                        if (mapPackage != null) {
                            title = mapPackage.getTitle();
                        }
                        else {
                            title = "null";
                        }
                        access$100.v(append.append(title).append("]").toString());
                    }
                    
                    @Override
                    public void onInstallationSize(final long n, final long n2) {
                        HereMapsManager.sLogger.v("MapLoader: onInstallationSize disk[" + n + "] network[" + n2 + "]");
                    }
                    
                    @Override
                    public void onPerformMapDataUpdateComplete(final MapPackage mapPackage, final ResultCode resultCode) {
                        final Logger access$100 = HereMapsManager.sLogger;
                        final StringBuilder append = new StringBuilder().append("MapLoader: onPerformMapDataUpdateComplete result[").append(resultCode.name()).append("] package[");
                        String title;
                        if (mapPackage != null) {
                            title = mapPackage.getTitle();
                        }
                        else {
                            title = "null";
                        }
                        access$100.v(append.append(title).append("]").toString());
                    }
                    
                    @Override
                    public void onProgress(final int n) {
                        HereMapsManager.sLogger.v("MapLoader: progress[" + n + "]");
                    }
                    
                    @Override
                    public void onUninstallMapPackagesComplete(final MapPackage mapPackage, final ResultCode resultCode) {
                    }
                });
                HereMapsManager.this.mapPackageCount = 0;
                HereMapsManager.this.invokeMapLoader();
            }
        }, 2);
    }
    
    private void initialize() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ldc_w           ":initializing voice skins"
        //     6: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //     9: invokestatic    com/navdy/hud/app/maps/here/VoiceSkinsConfigurator.updateVoiceSkins:()V
        //    12: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //    15: ldc_w           ":initializing event handlers"
        //    18: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    21: invokestatic    com/navdy/hud/app/maps/MapsEventHandler.getInstance:()Lcom/navdy/hud/app/maps/MapsEventHandler;
        //    24: pop            
        //    25: invokestatic    com/navdy/hud/app/maps/MapSettings.isDebugHereLocation:()Z
        //    28: ifeq            35
        //    31: iconst_1       
        //    32: invokestatic    com/navdy/hud/app/debug/DebugReceiver.setHereDebugLocation:(Z)V
        //    35: invokestatic    com/navdy/hud/app/maps/MapSettings.getMapFps:()I
        //    38: invokestatic    com/here/android/mpa/mapping/Map.setMaximumFps:(I)V
        //    41: iconst_1       
        //    42: invokestatic    com/here/android/mpa/mapping/Map.enableMaximumFpsLimit:(Z)V
        //    45: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //    48: new             Ljava/lang/StringBuilder;
        //    51: dup            
        //    52: invokespecial   java/lang/StringBuilder.<init>:()V
        //    55: ldc_w           "map-fps ["
        //    58: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    61: invokestatic    com/here/android/mpa/mapping/Map.getMaximumFps:()I
        //    64: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    67: ldc_w           "] enabled["
        //    70: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    73: invokestatic    com/here/android/mpa/mapping/Map.isMaximumFpsLimited:()Z
        //    76: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //    79: ldc_w           "]"
        //    82: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    85: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    88: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //    91: ldc_w           Lcom/nokia/maps/MapSettings;.class
        //    94: ldc_w           "g"
        //    97: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //   100: astore_1       
        //   101: aload_1        
        //   102: iconst_1       
        //   103: invokevirtual   java/lang/reflect/Field.setAccessible:(Z)V
        //   106: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   109: astore_2       
        //   110: new             Ljava/lang/StringBuilder;
        //   113: astore_3       
        //   114: aload_3        
        //   115: invokespecial   java/lang/StringBuilder.<init>:()V
        //   118: aload_2        
        //   119: aload_3        
        //   120: ldc_w           "enable worker thread before is "
        //   123: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   126: aload_1        
        //   127: aconst_null    
        //   128: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   131: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   134: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   137: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   140: aload_1        
        //   141: aconst_null    
        //   142: getstatic       com/nokia/maps/MapSettings$b.a:Lcom/nokia/maps/MapSettings$b;
        //   145: invokevirtual   java/lang/reflect/Field.set:(Ljava/lang/Object;Ljava/lang/Object;)V
        //   148: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   151: astore_3       
        //   152: new             Ljava/lang/StringBuilder;
        //   155: astore_2       
        //   156: aload_2        
        //   157: invokespecial   java/lang/StringBuilder.<init>:()V
        //   160: aload_3        
        //   161: aload_2        
        //   162: ldc_w           "enable worker thread after is "
        //   165: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   168: aload_1        
        //   169: aconst_null    
        //   170: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   173: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   176: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   179: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   182: aload_0        
        //   183: invokestatic    com/here/android/mpa/common/MapEngine.getInstance:()Lcom/here/android/mpa/common/MapEngine;
        //   186: putfield        com/navdy/hud/app/maps/here/HereMapsManager.mapEngine:Lcom/here/android/mpa/common/MapEngine;
        //   189: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   192: ldc_w           "calling maps engine init"
        //   195: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   198: aload_0        
        //   199: getfield        com/navdy/hud/app/maps/here/HereMapsManager.sharedPreferences:Landroid/content/SharedPreferences;
        //   202: aload_0        
        //   203: getfield        com/navdy/hud/app/maps/here/HereMapsManager.listener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
        //   206: invokeinterface android/content/SharedPreferences.registerOnSharedPreferenceChangeListener:(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
        //   211: ldc             "persist.sys.hud_traffic_reroute"
        //   213: iconst_0       
        //   214: invokestatic    com/navdy/hud/app/util/os/SystemProperties.getBoolean:(Ljava/lang/String;Z)Z
        //   217: putstatic       com/navdy/hud/app/maps/here/HereMapsManager.recalcCurrentRouteForTraffic:Z
        //   220: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   223: new             Ljava/lang/StringBuilder;
        //   226: dup            
        //   227: invokespecial   java/lang/StringBuilder.<init>:()V
        //   230: ldc_w           "persist.sys.hud_traffic_reroute="
        //   233: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   236: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.recalcCurrentRouteForTraffic:Z
        //   239: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   242: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   245: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   248: invokestatic    com/navdy/hud/app/storage/PathManager.getInstance:()Lcom/navdy/hud/app/storage/PathManager;
        //   251: invokevirtual   com/navdy/hud/app/storage/PathManager.getMapsPartitionPath:()Ljava/lang/String;
        //   254: astore_1       
        //   255: aload_1        
        //   256: invokevirtual   java/lang/String.isEmpty:()Z
        //   259: ifne            307
        //   262: new             Ljava/lang/StringBuilder;
        //   265: astore_3       
        //   266: aload_3        
        //   267: invokespecial   java/lang/StringBuilder.<init>:()V
        //   270: aload_3        
        //   271: aload_1        
        //   272: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   275: getstatic       java/io/File.separator:Ljava/lang/String;
        //   278: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   281: ldc_w           ".here-maps"
        //   284: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   287: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   290: ldc             "com.navdy.HereMapService"
        //   292: invokestatic    com/here/android/mpa/common/MapSettings.setIsolatedDiskCacheRootPath:(Ljava/lang/String;Ljava/lang/String;)Z
        //   295: ifne            307
        //   298: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   301: ldc_w           "setIsolatedDiskCacheRootPath() failed"
        //   304: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
        //   307: aload_0        
        //   308: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //   311: putfield        com/navdy/hud/app/maps/here/HereMapsManager.mapEngineStartTime:J
        //   314: aload_0        
        //   315: getfield        com/navdy/hud/app/maps/here/HereMapsManager.mapEngine:Lcom/here/android/mpa/common/MapEngine;
        //   318: aload_0        
        //   319: getfield        com/navdy/hud/app/maps/here/HereMapsManager.context:Landroid/content/Context;
        //   322: aload_0        
        //   323: getfield        com/navdy/hud/app/maps/here/HereMapsManager.engineInitListener:Lcom/here/android/mpa/common/OnEngineInitListener;
        //   326: invokevirtual   com/here/android/mpa/common/MapEngine.init:(Landroid/content/Context;Lcom/here/android/mpa/common/OnEngineInitListener;)V
        //   329: return         
        //   330: astore_1       
        //   331: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   334: ldc_w           "enable worker thread"
        //   337: aload_1        
        //   338: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   341: goto            182
        //   344: astore_1       
        //   345: getstatic       com/navdy/hud/app/maps/here/HereMapsManager.sLogger:Lcom/navdy/service/library/log/Logger;
        //   348: ldc_w           "exception in setIsolatedDiskCacheRootPath()"
        //   351: aload_1        
        //   352: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   355: goto            307
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  91     182    330    344    Ljava/lang/Throwable;
        //  262    307    344    358    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0307:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void invokeMapLoader() {
        if (this.mapLoader == null) {
            HereMapsManager.sLogger.v("invokeMapLoader: no maploader");
        }
        else if (!SystemUtils.isConnectedToNetwork(HudApplication.getAppContext())) {
            HereMapsManager.sLogger.v("invokeMapLoader: not connected to n/w");
        }
        else if (!this.mapInitLoaderComplete) {
            this.mapInitLoaderComplete = true;
            HereMapsManager.sLogger.v("initMapLoader called getMapPackages:" + this.mapLoader.getMapPackages());
        }
        else {
            HereMapsManager.sLogger.v("invokeMapLoader: already complete");
        }
    }
    
    private boolean isTrafficOverlayEnabled(final NavigationMode navigationMode) {
        final boolean b = false;
        final MapTrafficLayer mapTrafficLayer = this.map.getMapTrafficLayer();
        boolean enabled;
        if (navigationMode == NavigationMode.MAP) {
            enabled = b;
            if (mapTrafficLayer.isEnabled(MapTrafficLayer.RenderLayer.FLOW)) {
                enabled = b;
                if (mapTrafficLayer.isEnabled(MapTrafficLayer.RenderLayer.INCIDENT)) {
                    enabled = true;
                }
            }
        }
        else {
            enabled = b;
            if (navigationMode == NavigationMode.MAP_ON_ROUTE) {
                enabled = mapTrafficLayer.isEnabled(MapTrafficLayer.RenderLayer.ONROUTE);
            }
        }
        return enabled;
    }
    
    private boolean isTrafficOverlayVisible() {
        return this.isTrafficOverlayEnabled(HereNavigationManager.getInstance().getCurrentNavigationMode());
    }
    
    private void printMapPackages(final MapPackage mapPackage, final EnumSet<MapPackage.InstallationState> set) {
        if (mapPackage != null) {
            if (set.contains(mapPackage.getInstallationState())) {
                ++this.mapPackageCount;
            }
            final List<MapPackage> children = mapPackage.getChildren();
            if (children != null) {
                final Iterator<MapPackage> iterator = children.iterator();
                while (iterator.hasNext()) {
                    this.printMapPackages(iterator.next(), set);
                }
            }
        }
    }
    
    private void registerConnectivityReceiver() {
        this.context.registerReceiver((BroadcastReceiver)new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                    HereMapsManager.sLogger.v("setEngineOnlineState n/w state change");
                    HereMapsManager.this.setEngineOnlineState();
                }
            }
        }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }
    
    private void sendExtrapolationEvent() {
        final Bundle bundle = new Bundle();
        bundle.putBoolean("EXTRAPOLATION_FLAG", this.extrapolationOn);
        GpsUtils.sendEventBroadcast("EXTRAPOLATION", bundle);
    }
    
    private void setEngineOnlineState() {
        final boolean connectedToNetwork = NetworkStateManager.isConnectedToNetwork(this.context);
        this.lowBandwidthDetected = false;
        HereMapsManager.sLogger.i("setEngineOnlineState:setting maps engine online state:" + connectedToNetwork);
        if (connectedToNetwork) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    HereMapsManager.this.turnOnline();
                }
            }, 1);
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    HereMapsManager.this.turnOffline();
                }
            }, 1);
        }
    }
    
    private void setMapAttributes(final GeoCoordinate geoCoordinate) {
        this.map.setExtrudedBuildingsVisible(false);
        this.map.setLandmarksVisible(false);
        this.map.setStreetLevelCoverageVisible(false);
        this.map.setMapScheme(this.getTrackingMapScheme());
        if (geoCoordinate != null) {
            this.map.setCenter(geoCoordinate, Map.Animation.NONE, this.getDefaultZoomLevel(), -1.0f, this.getDefaultTiltLevel());
            HereMapsManager.sLogger.v("setcenterDefault:" + geoCoordinate);
        }
        else {
            HereMapsManager.sLogger.v("geoCoordinate not available");
        }
    }
    
    private void setMapOnRouteTraffic() {
        this.mapController.execute(new Runnable() {
            @Override
            public void run() {
                final MapTrafficLayer mapTrafficLayer = HereMapsManager.this.map.getMapTrafficLayer();
                mapTrafficLayer.setDisplayFilter(TrafficEvent.Severity.NORMAL);
                mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.ONROUTE, true);
                mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.FLOW, true);
                mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.INCIDENT, true);
                HereMapsManager.this.map.setTrafficInfoVisible(true);
            }
        });
    }
    
    private void setMapPoiLayer(final Map map) {
        map.setCartoMarkersVisible(false);
        map.getMapTransitLayer().setMode(MapTransitLayer.Mode.NOTHING);
        map.setVisibleLayers(EnumSet.<Map.LayerCategory>of(Map.LayerCategory.ICON_PUBLIC_TRANSIT_STATION, Map.LayerCategory.PUBLIC_TRANSIT_LINE, Map.LayerCategory.LABEL_PUBLIC_TRANSIT_STATION, Map.LayerCategory.LABEL_PUBLIC_TRANSIT_LINE, Map.LayerCategory.BEACH, Map.LayerCategory.WOODLAND, Map.LayerCategory.DESERT, Map.LayerCategory.GLACIER, Map.LayerCategory.AMUSEMENT_PARK, Map.LayerCategory.ANIMAL_PARK, Map.LayerCategory.BUILTUP, Map.LayerCategory.CEMETERY, Map.LayerCategory.BUILDING, Map.LayerCategory.NEIGHBORHOOD_AREA, Map.LayerCategory.NATIONAL_PARK, Map.LayerCategory.NATIVE_RESERVATION), false);
        final EnumSet<Map.LayerCategory> visibleLayers = map.getVisibleLayers();
        HereMapsManager.sLogger.v("==== visible layers == [" + visibleLayers.size() + "]");
        final StringBuilder sb = new StringBuilder();
        int n = 1;
        for (final Map.LayerCategory layerCategory : visibleLayers) {
            if (n == 0) {
                sb.append(", ");
            }
            sb.append(layerCategory.name());
            n = 0;
        }
        HereMapsManager.sLogger.v(sb.toString());
    }
    
    private void setMapTraffic() {
        this.mapController.execute(new Runnable() {
            @Override
            public void run() {
                final MapTrafficLayer mapTrafficLayer = HereMapsManager.this.map.getMapTrafficLayer();
                mapTrafficLayer.setDisplayFilter(TrafficEvent.Severity.NORMAL);
                mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.FLOW, true);
                mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.INCIDENT, true);
                mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.ONROUTE, true);
                HereMapsManager.this.map.setTrafficInfoVisible(true);
            }
        });
    }
    
    private void startTrafficUpdater() {
        try {
            HereNavigationManager.getInstance().startTrafficUpdater();
        }
        catch (Throwable t) {
            HereMapsManager.sLogger.e(t);
        }
    }
    
    public void checkForMapDataUpdate() {
        while (true) {
            Label_0047: {
                try {
                    if (!this.isInitialized()) {
                        HereMapsManager.sLogger.v("checkForMapDataUpdate MapLoader: engine not initialized");
                    }
                    else {
                        if (this.mapLoader != null) {
                            break Label_0047;
                        }
                        HereMapsManager.sLogger.v("checkForMapDataUpdate MapLoader: not available");
                    }
                    return;
                }
                catch (Throwable t) {
                    HereMapsManager.sLogger.e(t);
                    return;
                }
            }
            if (!this.mapInitLoaderComplete) {
                HereMapsManager.sLogger.v("checkForMapDataUpdate MapLoader: loader not initialized");
                return;
            }
            HereMapsManager.sLogger.v("checkForMapDataUpdate MapLoader: called checkForMapDataUpdate:" + this.mapLoader.checkForMapDataUpdate());
        }
    }
    
    void clearMapTraffic() {
        this.mapController.execute(new Runnable() {
            @Override
            public void run() {
                HereMapsManager.this.map.setTrafficInfoVisible(false);
                final MapTrafficLayer mapTrafficLayer = HereMapsManager.this.map.getMapTrafficLayer();
                mapTrafficLayer.setDisplayFilter(TrafficEvent.Severity.NORMAL);
                mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.ONROUTE, false);
                mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.FLOW, false);
                mapTrafficLayer.setEnabled(MapTrafficLayer.RenderLayer.INCIDENT, false);
            }
        });
    }
    
    public void clearTrafficOverlay() {
        this.clearMapTraffic();
    }
    
    public Looper getBkLocationReceiverLooper() {
        return this.bkLocationReceiverHandlerThread.getLooper();
    }
    
    String getEnrouteMapScheme() {
        return "hybrid.night";
    }
    
    public OnEngineInitListener.Error getError() {
        return this.mapError;
    }
    
    public GeoPosition getLastGeoPosition() {
        return this.lastGeoPosition;
    }
    
    public HereLocationFixManager getLocationFixManager() {
        return this.hereLocationFixManager;
    }
    
    public HereMapAnimator getMapAnimator() {
        return this.hereMapAnimator;
    }
    
    public HereMapController getMapController() {
        return this.mapController;
    }
    
    public int getMapPackageCount() {
        return this.mapPackageCount;
    }
    
    public MapView getMapView() {
        return this.mapView;
    }
    
    public String getOfflineMapsData() {
        final String s = null;
        String convertFileToString;
        try {
            final File file = new File(PathManager.getInstance().getHereMapsDataDirectory());
            if (!file.exists()) {
                convertFileToString = s;
            }
            else {
                final File file2 = new File(file, "meta.json");
                convertFileToString = s;
                if (file2.exists()) {
                    convertFileToString = IOUtils.convertFileToString(file2.getAbsolutePath());
                }
            }
            return convertFileToString;
        }
        catch (Throwable t) {
            HereMapsManager.sLogger.e(t);
            convertFileToString = s;
            return convertFileToString;
        }
        return convertFileToString;
    }
    
    public HereOfflineMapsVersion getOfflineMapsVersion() {
        return this.offlineMapsVersion;
    }
    
    public PositioningManager getPositioningManager() {
        return this.positioningManager;
    }
    
    public RouteCalculationEventHandler getRouteCalcEventHandler() {
        return this.routeCalcEventHandler;
    }
    
    public RouteOptions getRouteOptions() {
        final RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        final NavigationPreferences navigationPreferences = this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
        switch (navigationPreferences.routingType) {
            case ROUTING_FASTEST:
                routeOptions.setRouteType(RouteOptions.Type.FASTEST);
                break;
            case ROUTING_SHORTEST:
                routeOptions.setRouteType(RouteOptions.Type.SHORTEST);
                break;
        }
        routeOptions.setHighwaysAllowed(Boolean.TRUE.equals(navigationPreferences.allowHighways));
        routeOptions.setTollRoadsAllowed(Boolean.TRUE.equals(navigationPreferences.allowTollRoads));
        routeOptions.setFerriesAllowed(false);
        routeOptions.setTunnelsAllowed(Boolean.TRUE.equals(navigationPreferences.allowTunnels));
        routeOptions.setDirtRoadsAllowed(Boolean.TRUE.equals(navigationPreferences.allowUnpavedRoads));
        routeOptions.setCarShuttleTrainsAllowed(Boolean.TRUE.equals(navigationPreferences.allowAutoTrains));
        routeOptions.setCarpoolAllowed(false);
        return routeOptions;
    }
    
    public GeoCoordinate getRouteStartPoint() {
        return this.routeStartPoint;
    }
    
    String getTrackingMapScheme() {
        return "carnav.night";
    }
    
    public String getVersion() {
        String sdkVersion;
        if (this.isInitialized()) {
            sdkVersion = Version.getSdkVersion();
        }
        else {
            sdkVersion = null;
        }
        return sdkVersion;
    }
    
    public void handleBandwidthPreferenceChange() {
        if (this.isInitialized()) {
            HereNavigationManager.getInstance().setBandwidthPreferences();
        }
    }
    
    public void hideTraffic() {
        if (this.isInitialized()) {
            if (DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        HereMapsManager.this.mapController.setTrafficInfoVisible(false);
                        HereMapsManager.sLogger.v("hidetraffic: map off");
                        final MapRoute currentMapRoute = HereNavigationManager.getInstance().getCurrentMapRoute();
                        if (currentMapRoute != null) {
                            currentMapRoute.setTrafficEnabled(false);
                            HereMapsManager.sLogger.v("hidetraffic: route off");
                        }
                        HereMapsManager.sLogger.v("hidetraffic: traffic is disabled");
                    }
                }, 3);
            }
            else {
                HereMapsManager.sLogger.v("hidetraffic: traffic is not enabled");
            }
        }
    }
    
    public void initNavigation() {
        if (this.isInitialized()) {
            HereNavigationManager.getInstance().getNavController().initialize();
        }
    }
    
    public void installPositionListener() {
        synchronized (this) {
            if (!this.positioningManagerInstalled) {
                this.positioningManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(this.positionChangedListener));
                HereMapsManager.sLogger.v("position manager listener installed");
                this.positioningManagerInstalled = true;
            }
        }
    }
    
    public boolean isEngineOnline() {
        return this.isInitialized() && this.mapEngine != null && MapEngine.isOnlineEnabled();
    }
    
    public boolean isInitialized() {
        final Object initLock = this.initLock;
        synchronized (initLock) {
            return this.mapInitialized;
        }
    }
    
    public boolean isInitializing() {
        final Object initLock = this.initLock;
        synchronized (initLock) {
            return this.mapInitializing;
        }
    }
    
    public boolean isMapDataVerified() {
        return this.mapDataVerified;
    }
    
    public boolean isNavigationModeOn() {
        return this.isInitialized() && HereNavigationManager.getInstance().isNavigationModeOn();
    }
    
    public boolean isNavigationPossible() {
        boolean b = false;
        if (this.isInitialized()) {
            b = b;
            if (this.hereLocationFixManager.getLastGeoCoordinate() != null) {
                b = true;
            }
        }
        return b;
    }
    
    public boolean isRecalcCurrentRouteForTrafficEnabled() {
        return HereMapsManager.recalcCurrentRouteForTraffic;
    }
    
    public boolean isRenderingAllowed() {
        return !this.powerManager.inQuietMode();
    }
    
    public boolean isVoiceSkinsLoaded() {
        return this.voiceSkinsLoaded;
    }
    
    @Subscribe
    public void onLinkPropertiesChanged(final LinkPropertiesChanged linkPropertiesChanged) {
        if (linkPropertiesChanged != null && linkPropertiesChanged.bandwidthLevel != null) {
            HereMapsManager.sLogger.d("Link Properties changed");
            if (linkPropertiesChanged.bandwidthLevel <= 0) {
                HereMapsManager.sLogger.d("Switching to low bandwidth mode");
                this.lowBandwidthDetected = true;
                this.updateEngineOnlineState();
            }
            else {
                HereMapsManager.sLogger.d("Switching back to normal bandwidth mode");
                this.lowBandwidthDetected = false;
                this.updateEngineOnlineState();
            }
        }
    }
    
    @Subscribe
    public void onWakeup(final Wakeup wakeup) {
        if (this.isInitialized()) {
            this.startTrafficUpdater();
        }
    }
    
    public void postManeuverDisplay() {
        if (this.isInitialized()) {
            HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }
    
    public void requestMapsEngineOnlineStateChange(final boolean turnEngineOn) {
        HereMapsManager.sLogger.d("Request to set online state " + turnEngineOn);
        this.turnEngineOn = turnEngineOn;
        this.updateEngineOnlineState();
    }
    
    public void setMapView(final MapView mapView, final NavigationView navigationView) {
        this.mapView = mapView;
        this.navigationView = navigationView;
        HereNavigationManager.getInstance().setMapView(mapView, navigationView);
        final HereMapAnimator.AnimationMode animationMode = this.hereMapAnimator.getAnimationMode();
        if (animationMode != HereMapAnimator.AnimationMode.NONE) {
            HereMapsManager.sLogger.v("installing map render listener:" + animationMode);
            this.mapView.addOnMapRenderListener(this.hereMapAnimator.getMapRenderListener());
        }
        else {
            HereMapsManager.sLogger.v("not installing map render listener:" + animationMode);
        }
    }
    
    public void setRouteStartPoint(final GeoCoordinate routeStartPoint) {
        this.routeStartPoint = routeStartPoint;
    }
    
    public void setTrafficOverlay(final NavigationMode navigationMode) {
        if (navigationMode == NavigationMode.MAP) {
            this.setMapTraffic();
        }
        else if (navigationMode == NavigationMode.MAP_ON_ROUTE) {
            this.setMapOnRouteTraffic();
        }
    }
    
    public void setVoiceSkinsLoaded() {
        this.voiceSkinsLoaded = true;
    }
    
    public void showTraffic() {
        if (this.isInitialized()) {
            if (DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled()) {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        HereNavigationManager.getInstance().setTrafficToMode();
                        HereMapsManager.sLogger.v("showTraffic: traffic is enabled");
                    }
                }, 3);
            }
            else {
                HereMapsManager.sLogger.v("showTraffic: traffic is not enabled");
            }
        }
    }
    
    public void startMapRendering() {
        if (this.hereMapAnimator != null) {
            this.hereMapAnimator.startMapRendering();
        }
        else {
            this.initialMapRendering = true;
        }
    }
    
    public void stopMapRendering() {
        if (this.hereMapAnimator != null) {
            this.hereMapAnimator.stopMapRendering();
        }
        else {
            this.initialMapRendering = false;
        }
    }
    
    public void turnOffline() {
        synchronized (this) {
            if (!this.isInitialized()) {
                HereMapsManager.sLogger.i("turnOffline: engine not yet initialized");
            }
            else {
                this.requestMapsEngineOnlineStateChange(false);
                HereNavigationManager.getInstance().stopTrafficRerouteListener();
                HereMapsManager.sLogger.v("isOnline:" + MapEngine.isOnlineEnabled());
            }
        }
    }
    
    public void turnOnline() {
        while (true) {
            while (true) {
                Label_0111: {
                    synchronized (this) {
                        if (!this.isInitialized()) {
                            HereMapsManager.sLogger.i("turnOnline: engine not yet initialized");
                        }
                        else {
                            HereMapsManager.sLogger.i("turnOnline: enabling traffic info");
                            this.requestMapsEngineOnlineStateChange(true);
                            if (!GlanceHelper.isTrafficNotificationEnabled()) {
                                break Label_0111;
                            }
                            HereMapsManager.sLogger.i("turnOnline: enabling traffic notifications");
                            HereNavigationManager.getInstance().startTrafficRerouteListener();
                            HereMapsManager.sLogger.v("turnOnline invokeMapLoader");
                            this.invokeMapLoader();
                            HereMapsManager.sLogger.v("isOnline:" + MapEngine.isOnlineEnabled());
                        }
                        return;
                    }
                }
                HereMapsManager.sLogger.i("turnOnline: not enabling traffic notifications, glance off");
                continue;
            }
        }
    }
    
    public void updateEngineOnlineState() {
        final boolean b = true;
        synchronized (this) {
            HereMapsManager.sLogger.d("Updating the engine online state LowBandwidthDetected :" + this.lowBandwidthDetected + ", TurnEngineOn :" + this.turnEngineOn);
            if (this.isInitialized()) {
                HereMapsManager.sLogger.d("Turning engine on :" + (!this.lowBandwidthDetected && this.turnEngineOn));
                MapEngine.setOnline(!this.lowBandwidthDetected && this.turnEngineOn && b);
            }
        }
    }
    
    public static class MWConfigCorruptException extends RuntimeException
    {
        public MWConfigCorruptException(final Throwable t) {
            super(t);
        }
    }
}
