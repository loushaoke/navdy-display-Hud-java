package com.navdy.hud.app.maps.here;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.here.android.mpa.routing.Signpost;
import java.util.ArrayList;
import com.here.android.mpa.routing.Route;
import com.navdy.service.library.events.location.Coordinate;
import com.here.android.mpa.common.RoadElement;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.DistanceUnit;
import java.util.List;
import com.here.android.mpa.common.GeoCoordinate;
import android.text.TextUtils;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.manager.SpeedManager;
import java.util.Map;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.events.navigation.NavigationTurn;
import java.util.HashMap;
import android.content.res.Resources;
import com.here.android.mpa.routing.Maneuver;
import java.util.HashSet;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.maps.util.DistanceConverter;

public class HereManeuverDisplayBuilder
{
    public static final String ARRIVED;
    public static final String CLOSE_BRACKET = ")";
    public static final boolean COMBINE_HIGHWAY_MANEUVER = true;
    public static final String COMMA = ",";
    static final String DESTINATION_LEFT;
    private static final int DESTINATION_POINT_A_MIN_DISTANCE = 30;
    static final String DESTINATION_RIGHT;
    private static final DistanceConverter.Distance DISTANCE;
    public static final String EMPTY = "";
    public static final MapEvents.ManeuverDisplay EMPTY_MANEUVER_DISPLAY;
    private static final String END_TURN;
    private static final String ENTER_HIGHWAY;
    private static final String EXIT;
    public static final String EXIT_NUMBER;
    private static final String FEET_METER_FORMAT = "%.0f %s";
    public static final float FT_IN_METER = 3.28084f;
    private static final String HYPHEN;
    public static final float KM_DISPLAY_THRESHOLD = 400.0f;
    private static final String KM_FORMAT = "%.1f %s";
    private static final String KM_FORMAT_SHORT = "%.0f %s";
    public static final float METERS_IN_KM = 1000.0f;
    public static final float METERS_IN_MI = 1609.34f;
    public static final float MILES_DISPLAY_THRESHOLD = 160.934f;
    private static final String MILES_FORMAT = "%.1f %s";
    private static final String MILES_FORMAT_SHORT = "%.0f %s";
    private static final int MIN_STEP_FEET = 10;
    private static final int MIN_STEP_METERS = 5;
    private static final long NEXT_DISTANCE_THRESHOLD = 6437L;
    private static final long NEXT_DISTANCE_THRESHOLD_HIGHWAY = 16093L;
    private static final long NOW_DISTANCE_THRESHOLD = 30L;
    private static final long NOW_DISTANCE_THRESHOLD_HIGHWAY = 100L;
    private static final String ONTO;
    public static final String OPEN_BRACKET = "(";
    public static final int SHORT_DISTANCE_DISPLAY_THRESHOLD = 10;
    public static final boolean SHOW_DESTINATION_DIRECTION = true;
    public static final String SLASH = "/";
    public static final char SLASH_CHAR = '/';
    private static final float SMALL_UNITS_THRESHOLD = 0.1f;
    public static final boolean SMART_MANEUVER_CALCULATION = true;
    private static final long SOON_DISTANCE_THRESHOLD = 1287L;
    private static final long SOON_DISTANCE_THRESHOLD_HIGHWAY = 3218L;
    public static final String SPACE = " ";
    public static final char SPACE_CHAR = ' ';
    public static final String START_TURN;
    private static final String STAY_ON;
    private static final long THEN_MANEUVER_THRESHOLD = 250L;
    private static final long THEN_MANEUVER_THRESHOLD_HIGHWAY = 1000L;
    public static final String TOWARDS;
    private static final String UNIT_FEET;
    private static final String UNIT_FEET_EXTENDED;
    private static final String UNIT_FEET_EXTENDED_SINGULAR;
    private static final String UNIT_KILOMETERS;
    private static final String UNIT_KILOMETERS_EXTENDED;
    private static final String UNIT_KILOMETERS_EXTENDED_SINGULAR;
    private static final String UNIT_METERS;
    private static final String UNIT_METERS_EXTENDED;
    private static final String UNIT_METERS_EXTENDED_SINGULAR;
    private static final String UNIT_MILES;
    private static final String UNIT_MILES_EXTENDED;
    private static final String UNIT_MILES_EXTENDED_SINGULAR;
    private static final String UTURN;
    private static final HashSet<String> allowedTurnTextMap;
    private static Maneuver lastStayManeuver;
    private static String lastStayManeuverRoadName;
    private static final StringBuilder mainSignPostBuilder;
    private static final Resources resources;
    private static final HashMap<Maneuver.Icon, NavigationTurn> sHereIconToNavigationIconMap;
    private static final HashMap<Maneuver.Turn, String> sHereTurnToNavigationTurnMap;
    private static final Logger sLogger;
    private static final Map<NavigationTurn, Integer> sTurnIconImageMap;
    private static final Map<NavigationTurn, Integer> sTurnIconSoonImageMap;
    private static final Map<NavigationTurn, Integer> sTurnIconSoonImageMapGrey;
    private static final SpeedManager speedManager;
    
    static {
        sLogger = new Logger(HereManeuverDisplayBuilder.class);
        resources = HudApplication.getAppContext().getResources();
        mainSignPostBuilder = new StringBuilder();
        speedManager = SpeedManager.getInstance();
        allowedTurnTextMap = new HashSet<String>();
        EMPTY_MANEUVER_DISPLAY = new MapEvents.ManeuverDisplay();
        (sHereIconToNavigationIconMap = new HashMap<Maneuver.Icon, NavigationTurn>()).put(Maneuver.Icon.UNDEFINED, NavigationTurn.NAV_TURN_STRAIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.GO_STRAIGHT, NavigationTurn.NAV_TURN_STRAIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.KEEP_MIDDLE, NavigationTurn.NAV_TURN_STRAIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.KEEP_RIGHT, NavigationTurn.NAV_TURN_KEEP_RIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.LIGHT_RIGHT, NavigationTurn.NAV_TURN_EASY_RIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.QUITE_RIGHT, NavigationTurn.NAV_TURN_RIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.HEAVY_RIGHT, NavigationTurn.NAV_TURN_SHARP_RIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.KEEP_LEFT, NavigationTurn.NAV_TURN_KEEP_LEFT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.LIGHT_LEFT, NavigationTurn.NAV_TURN_EASY_LEFT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.QUITE_LEFT, NavigationTurn.NAV_TURN_LEFT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.HEAVY_LEFT, NavigationTurn.NAV_TURN_SHARP_LEFT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.UTURN_RIGHT, NavigationTurn.NAV_TURN_UTURN_RIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.UTURN_LEFT, NavigationTurn.NAV_TURN_UTURN_LEFT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_1, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_2, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_3, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_4, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_5, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_6, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_7, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_8, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_9, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_10, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_11, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_12, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ENTER_HIGHWAY_RIGHT_LANE, NavigationTurn.NAV_TURN_MERGE_LEFT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ENTER_HIGHWAY_LEFT_LANE, NavigationTurn.NAV_TURN_MERGE_RIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.LEAVE_HIGHWAY_RIGHT_LANE, NavigationTurn.NAV_TURN_EXIT_RIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.LEAVE_HIGHWAY_LEFT_LANE, NavigationTurn.NAV_TURN_EXIT_LEFT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.HIGHWAY_KEEP_RIGHT, NavigationTurn.NAV_TURN_KEEP_RIGHT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.HIGHWAY_KEEP_LEFT, NavigationTurn.NAV_TURN_KEEP_LEFT);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_1_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_2_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_3_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_4_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_5_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_6_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_7_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_8_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_9_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_10_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_11_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.ROUNDABOUT_12_LH, NavigationTurn.NAV_TURN_ROUNDABOUT_N);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.START, NavigationTurn.NAV_TURN_START);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.END, NavigationTurn.NAV_TURN_END);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.FERRY, NavigationTurn.NAV_TURN_FERRY);
        HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.put(Maneuver.Icon.HEAD_TO, NavigationTurn.NAV_TURN_STRAIGHT);
        ENTER_HIGHWAY = HereManeuverDisplayBuilder.resources.getString(R.string.enter_highway);
        EXIT = HereManeuverDisplayBuilder.resources.getString(R.string.exit);
        UTURN = HereManeuverDisplayBuilder.resources.getString(R.string.uturn);
        EXIT_NUMBER = HereManeuverDisplayBuilder.resources.getString(R.string.exit_number);
        START_TURN = HereManeuverDisplayBuilder.resources.getString(R.string.start_maneuver_turn);
        END_TURN = HereManeuverDisplayBuilder.resources.getString(R.string.end_maneuver_turn);
        TOWARDS = HereManeuverDisplayBuilder.resources.getString(R.string.toward);
        ONTO = HereManeuverDisplayBuilder.resources.getString(R.string.onto);
        STAY_ON = HereManeuverDisplayBuilder.resources.getString(R.string.stay_on);
        HYPHEN = HereManeuverDisplayBuilder.resources.getString(R.string.hyphen);
        DESTINATION_LEFT = HereManeuverDisplayBuilder.resources.getString(R.string.left);
        DESTINATION_RIGHT = HereManeuverDisplayBuilder.resources.getString(R.string.right);
        ARRIVED = HereManeuverDisplayBuilder.resources.getString(R.string.arrived);
        (sHereTurnToNavigationTurnMap = new HashMap<Maneuver.Turn, String>()).put(Maneuver.Turn.UNDEFINED, HereManeuverDisplayBuilder.resources.getString(R.string.undefined_turn));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.NO_TURN, HereManeuverDisplayBuilder.resources.getString(R.string.no_turn));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.KEEP_MIDDLE, HereManeuverDisplayBuilder.resources.getString(R.string.keep_middle));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.KEEP_RIGHT, HereManeuverDisplayBuilder.resources.getString(R.string.keep_right));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.LIGHT_RIGHT, HereManeuverDisplayBuilder.resources.getString(R.string.slight_right));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.QUITE_RIGHT, HereManeuverDisplayBuilder.resources.getString(R.string.quite_right));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.HEAVY_RIGHT, HereManeuverDisplayBuilder.resources.getString(R.string.quite_right));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.KEEP_LEFT, HereManeuverDisplayBuilder.resources.getString(R.string.keep_left));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.LIGHT_LEFT, HereManeuverDisplayBuilder.resources.getString(R.string.slight_left));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.QUITE_LEFT, HereManeuverDisplayBuilder.resources.getString(R.string.quite_left));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.HEAVY_LEFT, HereManeuverDisplayBuilder.resources.getString(R.string.quite_left));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.RETURN, HereManeuverDisplayBuilder.UTURN);
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_1, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_1));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_2, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_2));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_3, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_3));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_4, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_4));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_5, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_5));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_6, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_6));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_7, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_7));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_8, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_8));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_9, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_9));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_10, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_10));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_11, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_11));
        HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.put(Maneuver.Turn.ROUNDABOUT_12, HereManeuverDisplayBuilder.resources.getString(R.string.roundabout_12));
        UNIT_MILES = HereManeuverDisplayBuilder.resources.getString(R.string.unit_miles);
        UNIT_FEET = HereManeuverDisplayBuilder.resources.getString(R.string.unit_feet);
        UNIT_METERS = HereManeuverDisplayBuilder.resources.getString(R.string.unit_meters);
        UNIT_KILOMETERS = HereManeuverDisplayBuilder.resources.getString(R.string.unit_kilometers);
        UNIT_MILES_EXTENDED = HereManeuverDisplayBuilder.resources.getString(R.string.unit_miles_ext);
        UNIT_FEET_EXTENDED = HereManeuverDisplayBuilder.resources.getString(R.string.unit_feet_ext);
        UNIT_METERS_EXTENDED = HereManeuverDisplayBuilder.resources.getString(R.string.unit_meters_ext);
        UNIT_KILOMETERS_EXTENDED = HereManeuverDisplayBuilder.resources.getString(R.string.unit_kilometers_ext);
        UNIT_MILES_EXTENDED_SINGULAR = HereManeuverDisplayBuilder.resources.getString(R.string.unit_miles_ext_singular);
        UNIT_FEET_EXTENDED_SINGULAR = HereManeuverDisplayBuilder.resources.getString(R.string.unit_feet_ext_singular);
        UNIT_METERS_EXTENDED_SINGULAR = HereManeuverDisplayBuilder.resources.getString(R.string.unit_meters_ext_singular);
        UNIT_KILOMETERS_EXTENDED_SINGULAR = HereManeuverDisplayBuilder.resources.getString(R.string.unit_kilometers_ext_singular);
        (sTurnIconImageMap = new HashMap<NavigationTurn, Integer>()).put(NavigationTurn.NAV_TURN_START, R.drawable.tbt_straight);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_LEFT, R.drawable.tbt_turn_left);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_RIGHT, R.drawable.tbt_turn_right);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_EASY_RIGHT, R.drawable.tbt_easy_right);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_EASY_LEFT, R.drawable.tbt_easy_left);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_SHARP_LEFT, R.drawable.tbt_sharp_left);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_SHARP_RIGHT, R.drawable.tbt_sharp_right);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_EXIT_RIGHT, R.drawable.tbt_exit_right);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_EXIT_LEFT, R.drawable.tbt_exit_left);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_KEEP_LEFT, R.drawable.tbt_stay_left);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_KEEP_RIGHT, R.drawable.tbt_stay_right);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_END, R.drawable.tbt_end);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_STRAIGHT, R.drawable.tbt_go_straight);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_UTURN_LEFT, R.drawable.tbt_u_turn_left);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_UTURN_RIGHT, R.drawable.tbt_u_turn_right);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_ROUNDABOUT_S, R.drawable.tbt_left_roundabout);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_ROUNDABOUT_N, R.drawable.tbt_right_roundabout);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_MERGE_LEFT, R.drawable.tbt_merge_left);
        HereManeuverDisplayBuilder.sTurnIconImageMap.put(NavigationTurn.NAV_TURN_MERGE_RIGHT, R.drawable.tbt_merge_right);
        (sTurnIconSoonImageMap = new HashMap<NavigationTurn, Integer>()).put(NavigationTurn.NAV_TURN_START, R.drawable.tbt_straight_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_LEFT, R.drawable.tbt_turn_left_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_RIGHT, R.drawable.tbt_turn_right_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_EASY_RIGHT, R.drawable.tbt_easy_right_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_EASY_LEFT, R.drawable.tbt_easy_left_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_SHARP_LEFT, R.drawable.tbt_sharp_left_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_SHARP_RIGHT, R.drawable.tbt_sharp_right_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_EXIT_RIGHT, R.drawable.tbt_exit_right_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_EXIT_LEFT, R.drawable.tbt_exit_left_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_KEEP_LEFT, R.drawable.tbt_stay_left_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_KEEP_RIGHT, R.drawable.tbt_stay_right_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_END, R.drawable.tbt_end_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_STRAIGHT, R.drawable.tbt_go_straight_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_UTURN_LEFT, R.drawable.tbt_u_turn_left_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_UTURN_RIGHT, R.drawable.tbt_u_turn_right_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_ROUNDABOUT_S, R.drawable.tbt_left_roundabout_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_ROUNDABOUT_N, R.drawable.tbt_right_roundabout_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_MERGE_LEFT, R.drawable.tbt_merge_left_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMap.put(NavigationTurn.NAV_TURN_MERGE_RIGHT, R.drawable.tbt_merge_right_soon);
        (sTurnIconSoonImageMapGrey = new HashMap<NavigationTurn, Integer>()).put(NavigationTurn.NAV_TURN_START, R.drawable.icon_tbt_start_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_LEFT, R.drawable.icon_tbt_quite_left_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_RIGHT, R.drawable.icon_tbt_quite_right_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_EASY_RIGHT, R.drawable.icon_tbt_light_right_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_EASY_LEFT, R.drawable.icon_tbt_light_left_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_SHARP_LEFT, R.drawable.icon_tbt_heavy_left_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_SHARP_RIGHT, R.drawable.icon_tbt_heavy_right_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_EXIT_RIGHT, R.drawable.icon_tbt_leave_highway_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_EXIT_LEFT, R.drawable.icon_tbt_leave_highway_left_lane_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_KEEP_LEFT, R.drawable.icon_tbt_keep_left_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_KEEP_RIGHT, R.drawable.icon_tbt_keep_right_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_END, R.drawable.icon_tbt_arrive_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_STRAIGHT, R.drawable.icon_tbt_go_straight_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_UTURN_LEFT, R.drawable.tbt_icon_return_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_UTURN_RIGHT, R.drawable.icon_tbt_uturn_right_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_ROUNDABOUT_S, R.drawable.tbt_left_roundabout_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_ROUNDABOUT_N, R.drawable.tbt_right_roundabout_soon);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_MERGE_LEFT, R.drawable.icon_tbt_merge_left_grey);
        HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.put(NavigationTurn.NAV_TURN_MERGE_RIGHT, R.drawable.icon_tbt_merge_right_grey);
        HereManeuverDisplayBuilder.EMPTY_MANEUVER_DISPLAY.empty = true;
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.EXIT);
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.UTURN);
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.START_TURN);
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.END_TURN);
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.ARRIVED);
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_1));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_2));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_3));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_4));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_5));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_6));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_7));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_8));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_9));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_10));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_11));
        HereManeuverDisplayBuilder.allowedTurnTextMap.add(HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(Maneuver.Turn.ROUNDABOUT_12));
        DISTANCE = new DistanceConverter.Distance();
    }
    
    public static boolean canShowTurnText(final String s) {
        return !MapSettings.doNotShowTurnTextInTBT() || HereManeuverDisplayBuilder.allowedTurnTextMap.contains(s);
    }
    
    public static MapEvents.ManeuverDisplay getArrivedManeuverDisplay() {
        final MapEvents.ManeuverDisplay maneuverDisplay = new MapEvents.ManeuverDisplay();
        maneuverDisplay.maneuverId = "arrived";
        maneuverDisplay.turnIconId = R.drawable.icon_tbt_arrive;
        maneuverDisplay.pendingTurn = HereManeuverDisplayBuilder.ARRIVED;
        final HereNavigationManager instance = HereNavigationManager.getInstance();
        String pendingRoad;
        if (TextUtils.isEmpty((CharSequence)(pendingRoad = instance.getDestinationLabel())) && TextUtils.isEmpty((CharSequence)(pendingRoad = instance.getDestinationStreetAddress()))) {
            pendingRoad = "";
        }
        maneuverDisplay.pendingRoad = pendingRoad;
        return maneuverDisplay;
    }
    
    public static String getDestinationDirection(final MapEvents.DestinationDirection destinationDirection) {
        String s = null;
        switch (destinationDirection) {
            default:
                s = null;
                break;
            case LEFT:
                s = HereManeuverDisplayBuilder.DESTINATION_LEFT;
                break;
            case RIGHT:
                s = HereManeuverDisplayBuilder.DESTINATION_RIGHT;
                break;
        }
        return s;
    }
    
    public static int getDestinationIcon(final MapEvents.DestinationDirection destinationDirection) {
        int n = 0;
        switch (destinationDirection) {
            default:
                n = R.drawable.icon_pin_dot_destination;
                break;
            case LEFT:
                n = R.drawable.icon_pin_dot_destination_left;
                break;
            case RIGHT:
                n = R.drawable.icon_pin_dot_destination_right;
                break;
        }
        return n;
    }
    
    private static GeoCoordinate getEstimatedPointAfromManeuver(final Maneuver maneuver, final GeoCoordinate geoCoordinate) {
        GeoCoordinate geoCoordinate2;
        if (maneuver == null || geoCoordinate == null) {
            geoCoordinate2 = null;
        }
        else {
            final GeoCoordinate geoCoordinate3 = null;
            final List<GeoCoordinate> maneuverGeometry = maneuver.getManeuverGeometry();
            int i = maneuverGeometry.size() - 1;
            geoCoordinate2 = geoCoordinate3;
            while (i >= 0) {
                geoCoordinate2 = maneuverGeometry.get(i);
                final int n = (int)geoCoordinate2.distanceTo(geoCoordinate);
                if (n >= 30) {
                    HereManeuverDisplayBuilder.sLogger.v("remaining=" + n);
                    return geoCoordinate2;
                }
                --i;
            }
            if (geoCoordinate2 != null) {
                HereManeuverDisplayBuilder.sLogger.v("remaining=" + (int)geoCoordinate2.distanceTo(geoCoordinate));
            }
        }
        return geoCoordinate2;
    }
    
    public static String getFormattedDistance(final float n, final DistanceUnit distanceUnit, final boolean b) {
        String s = null;
        switch (distanceUnit) {
            default:
                s = String.valueOf((int)n);
                break;
            case DISTANCE_MILES: {
                String s2;
                if (n < 10.0f) {
                    s2 = "%.1f %s";
                }
                else {
                    s2 = "%.0f %s";
                }
                String s3;
                if (!b) {
                    s3 = HereManeuverDisplayBuilder.UNIT_MILES;
                }
                else if (n <= 1.0f) {
                    s3 = HereManeuverDisplayBuilder.UNIT_MILES_EXTENDED_SINGULAR;
                }
                else {
                    s3 = HereManeuverDisplayBuilder.UNIT_MILES_EXTENDED;
                }
                s = String.format(s2, n, s3);
                break;
            }
            case DISTANCE_FEET: {
                final float roundToIntegerStep = HereMapUtil.roundToIntegerStep(10, n);
                String s4;
                if (!b) {
                    s4 = HereManeuverDisplayBuilder.UNIT_FEET;
                }
                else if (n <= 1.0f) {
                    s4 = HereManeuverDisplayBuilder.UNIT_FEET_EXTENDED_SINGULAR;
                }
                else {
                    s4 = HereManeuverDisplayBuilder.UNIT_FEET_EXTENDED;
                }
                s = String.format("%.0f %s", roundToIntegerStep, s4);
                break;
            }
            case DISTANCE_KMS: {
                String s5;
                if (n < 10.0f) {
                    s5 = "%.1f %s";
                }
                else {
                    s5 = "%.0f %s";
                }
                String s6;
                if (!b) {
                    s6 = HereManeuverDisplayBuilder.UNIT_KILOMETERS;
                }
                else if (n <= 1.0f) {
                    s6 = HereManeuverDisplayBuilder.UNIT_KILOMETERS_EXTENDED_SINGULAR;
                }
                else {
                    s6 = HereManeuverDisplayBuilder.UNIT_KILOMETERS_EXTENDED;
                }
                s = String.format(s5, n, s6);
                break;
            }
            case DISTANCE_METERS: {
                final float roundToIntegerStep2 = HereMapUtil.roundToIntegerStep(5, n);
                String s7;
                if (!b) {
                    s7 = HereManeuverDisplayBuilder.UNIT_METERS;
                }
                else if (n <= 1.0f) {
                    s7 = HereManeuverDisplayBuilder.UNIT_METERS_EXTENDED_SINGULAR;
                }
                else {
                    s7 = HereManeuverDisplayBuilder.UNIT_METERS_EXTENDED;
                }
                s = String.format("%.0f %s", roundToIntegerStep2, s7);
                break;
            }
        }
        return s;
    }
    
    public static MapEvents.ManeuverDisplay getManeuverDisplay(Maneuver lastStayManeuver, final boolean b, long n, String pendingRoad, final Maneuver maneuver, final NavigationRouteRequest navigationRouteRequest, final Maneuver maneuver2, final boolean b2, final boolean b3, final MapEvents.DestinationDirection destinationDirection) {
        final PendingRoadInfo pendingRoadInfo = new PendingRoadInfo();
        final MapEvents.ManeuverDisplay maneuverDisplay = new MapEvents.ManeuverDisplay();
        String value;
        if (lastStayManeuver != null) {
            value = String.valueOf(System.identityHashCode(lastStayManeuver));
        }
        else {
            value = "unknown";
        }
        maneuverDisplay.maneuverId = value;
        final RoadElement currentRoadElement = HereMapUtil.getCurrentRoadElement();
        boolean b4;
        if (!(b4 = HereMapUtil.isCurrentRoadHighway(lastStayManeuver))) {
            b4 = HereMapUtil.isCurrentRoadHighway(currentRoadElement);
        }
        if (b3) {
            long n2 = n;
            if (n >= 2147483647L) {
                n2 = 0L;
            }
            maneuverDisplay.maneuverState = getManeuverState(n2, b4);
            n = n2;
        }
        else {
            maneuverDisplay.maneuverState = ManeuverState.NOW;
        }
        final ManeuverState maneuverState = maneuverDisplay.maneuverState;
        Maneuver maneuver3;
        if (b2) {
            maneuver3 = maneuver2;
        }
        else {
            maneuver3 = null;
        }
        setNavigationTurnIconId(lastStayManeuver, n, maneuverState, maneuverDisplay, maneuver3, b4, destinationDirection);
        setNavigationDistance(n, maneuverDisplay, false, !b3);
        if (maneuverDisplay.maneuverState == ManeuverState.STAY) {
            if (currentRoadElement == null) {
                maneuverDisplay.currentRoad = null;
            }
            else {
                maneuverDisplay.currentRoad = HereMapUtil.concatText(currentRoadElement.getRouteName(), currentRoadElement.getRoadName(), true, b4);
                if (!TextUtils.isEmpty((CharSequence)maneuverDisplay.currentRoad)) {
                    HereManeuverDisplayBuilder.lastStayManeuverRoadName = maneuverDisplay.currentRoad;
                    HereManeuverDisplayBuilder.lastStayManeuver = lastStayManeuver;
                }
            }
            if (TextUtils.isEmpty((CharSequence)maneuverDisplay.currentRoad)) {
                if (HereManeuverDisplayBuilder.lastStayManeuver == lastStayManeuver) {
                    if (HereManeuverDisplayBuilder.lastStayManeuverRoadName != null) {
                        maneuverDisplay.currentRoad = HereManeuverDisplayBuilder.lastStayManeuverRoadName;
                    }
                }
                else {
                    HereManeuverDisplayBuilder.lastStayManeuver = null;
                    HereManeuverDisplayBuilder.lastStayManeuverRoadName = null;
                }
            }
            if (TextUtils.isEmpty((CharSequence)maneuverDisplay.currentRoad)) {
                if (maneuver != null) {
                    maneuverDisplay.currentRoad = HereMapUtil.concatText(maneuver.getNextRoadNumber(), maneuver.getNextRoadName(), true, b4);
                }
                if (maneuverDisplay.currentRoad == null) {
                    maneuverDisplay.currentRoad = "";
                }
            }
        }
        else {
            HereManeuverDisplayBuilder.lastStayManeuver = null;
            HereManeuverDisplayBuilder.lastStayManeuverRoadName = null;
            maneuverDisplay.currentRoad = HereMapUtil.concatText(lastStayManeuver.getRoadNumber(), lastStayManeuver.getRoadName(), true, b4);
            if (TextUtils.isEmpty((CharSequence)maneuverDisplay.currentRoad)) {
                maneuverDisplay.currentRoad = getPendingRoadText(lastStayManeuver, maneuver, maneuver2, pendingRoadInfo);
            }
        }
        maneuverDisplay.currentSpeedLimit = HereMapUtil.getCurrentSpeedLimit();
        if (b) {
            maneuverDisplay.pendingTurn = HereManeuverDisplayBuilder.END_TURN;
            String s;
            if (TextUtils.isEmpty((CharSequence)(s = HereNavigationManager.getInstance().getDestinationLabel()))) {
                if (pendingRoad != null) {
                    pendingRoad = HereMapUtil.parseStreetAddress(pendingRoad);
                }
                else {
                    pendingRoad = HereNavigationManager.getInstance().getDestinationStreetAddress();
                }
                s = pendingRoad;
                if (TextUtils.isEmpty((CharSequence)pendingRoad)) {
                    s = lastStayManeuver.getRoadName();
                }
            }
            Enum<MapEvents.DestinationDirection> direction;
            final Enum<MapEvents.DestinationDirection> enum1 = direction = null;
            if (navigationRouteRequest != null) {
                final Coordinate destination = navigationRouteRequest.destination;
                final Coordinate destinationDisplay = navigationRouteRequest.destinationDisplay;
                if (destination != null && destinationDisplay != null) {
                    final GeoCoordinate geoCoordinate = new GeoCoordinate(navigationRouteRequest.destinationDisplay.latitude, navigationRouteRequest.destinationDisplay.longitude);
                    final GeoCoordinate geoCoordinate2 = new GeoCoordinate(navigationRouteRequest.destination.latitude, navigationRouteRequest.destination.longitude);
                    if ((lastStayManeuver = maneuver) == null) {
                        final Route currentRoute = HereNavigationManager.getInstance().getCurrentRoute();
                        lastStayManeuver = maneuver;
                        if (currentRoute != null) {
                            final List<Maneuver> maneuvers = currentRoute.getManeuvers();
                            lastStayManeuver = maneuver;
                            if (maneuvers != null) {
                                lastStayManeuver = maneuver;
                                if (maneuvers.size() == 2) {
                                    lastStayManeuver = maneuvers.get(0);
                                }
                            }
                        }
                    }
                    final GeoCoordinate estimatedPointAfromManeuver = getEstimatedPointAfromManeuver(lastStayManeuver, geoCoordinate2);
                    if (estimatedPointAfromManeuver != null) {
                        final double sin = Math.sin((geoCoordinate2.getLatitude() - estimatedPointAfromManeuver.getLatitude()) * (geoCoordinate.getLongitude() - estimatedPointAfromManeuver.getLongitude()) - (geoCoordinate2.getLongitude() - estimatedPointAfromManeuver.getLongitude()) * (geoCoordinate.getLatitude() - estimatedPointAfromManeuver.getLatitude()));
                        if (sin >= 0.0) {
                            direction = MapEvents.DestinationDirection.RIGHT;
                        }
                        else if (sin < 0.0) {
                            direction = MapEvents.DestinationDirection.LEFT;
                        }
                        else {
                            direction = MapEvents.DestinationDirection.UNKNOWN;
                        }
                        HereManeuverDisplayBuilder.sLogger.v("dest(1) position=" + sin + " DIRECTION = " + direction);
                        maneuverDisplay.direction = (MapEvents.DestinationDirection)direction;
                        if (direction != MapEvents.DestinationDirection.UNKNOWN) {
                            maneuverDisplay.destinationIconId = getDestinationIcon((MapEvents.DestinationDirection)direction);
                        }
                        final double heading = geoCoordinate2.getHeading(geoCoordinate);
                        final double heading2 = estimatedPointAfromManeuver.getHeading(geoCoordinate2);
                        final int n3 = (int)(heading - heading2);
                        HereManeuverDisplayBuilder.sLogger.v("dest(2): navToDisplayAngle=" + (int)heading + " currentHereAngle=" + (int)heading2 + " diffAngle =" + n3);
                        int n4;
                        if ((n4 = n3) < 0) {
                            n4 = n3 + 360;
                        }
                        MapEvents.DestinationDirection destinationDirection2;
                        if (n4 >= 1 && n4 <= 179) {
                            destinationDirection2 = MapEvents.DestinationDirection.RIGHT;
                        }
                        else if (n4 >= 181 && n4 <= 359) {
                            destinationDirection2 = MapEvents.DestinationDirection.LEFT;
                        }
                        else {
                            destinationDirection2 = MapEvents.DestinationDirection.UNKNOWN;
                        }
                        HereManeuverDisplayBuilder.sLogger.v("dest(2): diffAngle=" + n4 + " direction=" + destinationDirection2);
                    }
                    else {
                        maneuverDisplay.direction = MapEvents.DestinationDirection.UNKNOWN;
                        HereManeuverDisplayBuilder.sLogger.v("dest(1) cannot calculate direction, A pos not available");
                        direction = enum1;
                    }
                }
                else {
                    maneuverDisplay.direction = MapEvents.DestinationDirection.UNKNOWN;
                    HereManeuverDisplayBuilder.sLogger.v("dest(1) cannot calculate direction pos not available");
                    direction = enum1;
                }
            }
            if (direction != null) {
                final Resources resources = HereManeuverDisplayBuilder.resources;
                pendingRoad = s;
                if (TextUtils.isEmpty((CharSequence)s)) {
                    pendingRoad = HereManeuverDisplayBuilder.resources.getString(R.string.end_maneuver_direction_no_road);
                }
                maneuverDisplay.pendingRoad = resources.getString(R.string.end_maneuver_direction, new Object[] { pendingRoad, getDestinationDirection((MapEvents.DestinationDirection)direction) });
            }
            else {
                final Resources resources2 = HereManeuverDisplayBuilder.resources;
                String string = s;
                if (TextUtils.isEmpty((CharSequence)s)) {
                    string = HereManeuverDisplayBuilder.resources.getString(R.string.end_maneuver_direction_no_road);
                }
                maneuverDisplay.pendingRoad = resources2.getString(R.string.end_maneuver, new Object[] { string });
            }
        }
        else {
            maneuverDisplay.pendingTurn = getTurnText(lastStayManeuver, pendingRoadInfo, maneuverDisplay.maneuverState, maneuver, maneuver2);
            if (maneuverDisplay.maneuverState == ManeuverState.STAY) {
                maneuverDisplay.pendingRoad = maneuverDisplay.currentRoad;
            }
            else {
                if (TextUtils.isEmpty((CharSequence)(pendingRoad = getPendingRoadText(lastStayManeuver, maneuver, maneuver2, pendingRoadInfo)))) {
                    pendingRoad = HereMapUtil.getRoadName(lastStayManeuver.getRoadElements());
                }
                if (!TextUtils.isEmpty((CharSequence)pendingRoad)) {
                    maneuverDisplay.pendingRoad = pendingRoad;
                }
                else {
                    maneuverDisplay.pendingRoad = "";
                }
            }
        }
        return maneuverDisplay;
    }
    
    static ManeuverState getManeuverState(final long n, final boolean b) {
        double n2;
        double n3;
        long n4;
        if (b) {
            n2 = 16093.0;
            n3 = 3218.0;
            n4 = 100L;
        }
        else {
            n2 = 6437.0;
            n3 = 1287.0;
            n4 = 30L;
        }
        ManeuverState maneuverState;
        if (n <= n4) {
            maneuverState = ManeuverState.NOW;
        }
        else if (n <= n3) {
            maneuverState = ManeuverState.SOON;
        }
        else if (n <= n2) {
            maneuverState = ManeuverState.NEXT;
        }
        else {
            maneuverState = ManeuverState.STAY;
        }
        if (HereManeuverDisplayBuilder.sLogger.isLoggable(2)) {
            HereManeuverDisplayBuilder.sLogger.v("maneuverState=" + maneuverState + " distance=" + n + " isHighway=" + b);
        }
        return maneuverState;
    }
    
    private static NavigationTurn getNavigationTurn(final Maneuver maneuver) {
        final Maneuver.Icon icon = maneuver.getIcon();
        NavigationTurn navigationTurn;
        if (icon == null) {
            navigationTurn = null;
        }
        else {
            navigationTurn = HereManeuverDisplayBuilder.sHereIconToNavigationIconMap.get(icon);
        }
        return navigationTurn;
    }
    
    private static String getPendingRoadText(final Maneuver maneuver, final Maneuver maneuver2, Maneuver string, final PendingRoadInfo pendingRoadInfo) {
        Maneuver maneuver3 = maneuver;
        Maneuver maneuver4 = maneuver2;
        Maneuver maneuver5 = string;
        if (pendingRoadInfo != null) {
            maneuver3 = maneuver;
            maneuver4 = maneuver2;
            maneuver5 = string;
            if (pendingRoadInfo.usePrevManeuverInfo) {
                maneuver3 = maneuver;
                maneuver4 = maneuver2;
                maneuver5 = string;
                if (maneuver2 != null) {
                    maneuver4 = null;
                    maneuver5 = null;
                    maneuver3 = maneuver2;
                }
            }
        }
        final String nextRoadNumber = maneuver3.getNextRoadNumber();
        final String nextRoadName = maneuver3.getNextRoadName();
        final Signpost signpost = maneuver3.getSignpost();
        final HereMapUtil.SignPostInfo signPostInfo = new HereMapUtil.SignPostInfo();
        Object o;
        final String s = (String)(o = HereMapUtil.getSignpostText(signpost, maneuver3, signPostInfo, null));
        Label_0137: {
            if (pendingRoadInfo != null) {
                o = s;
                if (pendingRoadInfo.dontUseSignpostText) {
                    if (TextUtils.isEmpty((CharSequence)nextRoadNumber)) {
                        o = s;
                        if (TextUtils.isEmpty((CharSequence)nextRoadName)) {
                            break Label_0137;
                        }
                    }
                    o = null;
                    signPostInfo.hasNameInSignPost = false;
                    signPostInfo.hasNumberInSignPost = false;
                }
            }
        }
        int n = 0;
        int index = 0;
        final int n2 = 0;
        final int n3 = 0;
        int n4 = 0;
        final int n5 = 0;
        final int n6 = 0;
        final int n7 = 0;
        int n8 = 0;
        final int n9 = 0;
        boolean b = HereMapUtil.hasSameSignpostAndToRoad(maneuver3, maneuver4);
        Label_0934: {
            if (!b) {
                break Label_0934;
            }
            int n10 = 1;
            StringBuilder mainSignPostBuilder;
            int n11;
            int n12;
            Object o2;
            String s2;
            String s3;
            Maneuver maneuver6;
            CharSequence charSequence;
            Object trim;
            String string2;
            boolean b2;
            String string3;
            String string4;
            Label_0473_Outer:Label_0572_Outer:Label_0845_Outer:Label_0922_Outer:
            while (true) {
                mainSignPostBuilder = HereManeuverDisplayBuilder.mainSignPostBuilder;
                while (true) {
                Label_1192:
                    while (true) {
                    Label_1103:
                        while (true) {
                        Label_1032:
                            while (true) {
                                Label_1003: {
                                    synchronized (mainSignPostBuilder) {
                                        HereManeuverDisplayBuilder.mainSignPostBuilder.setLength(0);
                                        if (!TextUtils.isEmpty((CharSequence)nextRoadNumber) && !signPostInfo.hasNumberInSignPost) {
                                            HereManeuverDisplayBuilder.mainSignPostBuilder.append(nextRoadNumber);
                                        }
                                        n11 = 0;
                                        string = (Maneuver)o;
                                        if (!TextUtils.isEmpty((CharSequence)o)) {
                                            n12 = (n = 1);
                                            o2 = o;
                                            if (b) {
                                                n = n12;
                                                o2 = o;
                                                if (index == 0) {
                                                    n = 0;
                                                    o2 = "";
                                                }
                                            }
                                            index = (n11 = 1);
                                            string = (Maneuver)o2;
                                            if (n != 0) {
                                                if (HereMapUtil.needSeparator(HereManeuverDisplayBuilder.mainSignPostBuilder)) {
                                                    HereManeuverDisplayBuilder.mainSignPostBuilder.append("/");
                                                }
                                                HereManeuverDisplayBuilder.mainSignPostBuilder.append((String)o2);
                                                string = (Maneuver)o2;
                                                n11 = index;
                                            }
                                        }
                                        s2 = null;
                                        s3 = null;
                                        maneuver6 = null;
                                        index = n4;
                                        if (!TextUtils.isEmpty((CharSequence)nextRoadName)) {
                                            n = n7;
                                            charSequence = s3;
                                            n4 = n3;
                                            if (n11 != 0) {
                                                n = n7;
                                                charSequence = s3;
                                                n4 = n3;
                                                if (!((String)string).contains(nextRoadName)) {
                                                    n = (index = 1);
                                                    if (b) {
                                                        index = n;
                                                        if (n10 == 0) {
                                                            index = 0;
                                                        }
                                                    }
                                                    n = n6;
                                                    charSequence = s2;
                                                    n4 = n2;
                                                    if (index != 0) {
                                                        if (pendingRoadInfo != null && pendingRoadInfo.hasTo) {
                                                            o = HereManeuverDisplayBuilder.mainSignPostBuilder.toString();
                                                            HereManeuverDisplayBuilder.mainSignPostBuilder.setLength(0);
                                                        }
                                                        else {
                                                            o = maneuver6;
                                                            if (HereMapUtil.needSeparator(HereManeuverDisplayBuilder.mainSignPostBuilder)) {
                                                                HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                                                                o = maneuver6;
                                                            }
                                                        }
                                                        if (n10 != 0) {
                                                            break Label_1003;
                                                        }
                                                        index = n9;
                                                        if (!MapSettings.isTbtOntoDisabled()) {
                                                            HereManeuverDisplayBuilder.mainSignPostBuilder.append(HereManeuverDisplayBuilder.ONTO);
                                                            HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                                                            index = 1;
                                                        }
                                                        n10 = n5;
                                                        if (TextUtils.isEmpty((CharSequence)o)) {
                                                            n10 = n5;
                                                            if (pendingRoadInfo != null) {
                                                                n10 = n5;
                                                                if (pendingRoadInfo.hasTo) {
                                                                    pendingRoadInfo.hasTo = false;
                                                                    if (!MapSettings.doNotShowTurnTextInTBT()) {
                                                                        HereManeuverDisplayBuilder.mainSignPostBuilder.append(HereManeuverDisplayBuilder.TOWARDS);
                                                                        HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                                                                    }
                                                                    n10 = 1;
                                                                }
                                                            }
                                                        }
                                                        if (MapSettings.isTbtOntoDisabled()) {
                                                            break Label_1032;
                                                        }
                                                        HereManeuverDisplayBuilder.mainSignPostBuilder.append(HereMapUtil.concatText(nextRoadNumber, nextRoadName, true, HereMapUtil.isCurrentRoadHighway(maneuver3)));
                                                        n4 = n10;
                                                        charSequence = (CharSequence)o;
                                                        n = index;
                                                    }
                                                    signPostInfo.hasNameInSignPost = true;
                                                }
                                            }
                                            if (!signPostInfo.hasNameInSignPost) {
                                                n10 = (index = 1);
                                                if (HereManeuverDisplayBuilder.mainSignPostBuilder.length() > 0) {
                                                    index = n10;
                                                    if (HereMapUtil.isCurrentRoadHighway(maneuver3)) {
                                                        index = 0;
                                                    }
                                                }
                                                if (index != 0) {
                                                    index = 0;
                                                    if (HereMapUtil.needSeparator(HereManeuverDisplayBuilder.mainSignPostBuilder)) {
                                                        HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                                                    }
                                                    if (!TextUtils.isEmpty((CharSequence)nextRoadNumber)) {
                                                        index = 1;
                                                        HereManeuverDisplayBuilder.mainSignPostBuilder.append("(");
                                                    }
                                                    HereManeuverDisplayBuilder.mainSignPostBuilder.append(nextRoadName);
                                                    if (index != 0) {
                                                        HereManeuverDisplayBuilder.mainSignPostBuilder.append(")");
                                                    }
                                                }
                                            }
                                            if (TextUtils.isEmpty(charSequence)) {
                                                break Label_1103;
                                            }
                                            string = (Maneuver)HereManeuverDisplayBuilder.mainSignPostBuilder.toString();
                                            index = ((String)string).indexOf(HereManeuverDisplayBuilder.ONTO + " ");
                                            trim = string;
                                            if (index != -1) {
                                                trim = ((String)string).substring(HereManeuverDisplayBuilder.ONTO.length() + index + " ".length()).trim();
                                            }
                                            if (!TextUtils.isEmpty((CharSequence)trim) && ((String)charSequence).contains((CharSequence)trim)) {
                                                HereManeuverDisplayBuilder.mainSignPostBuilder.setLength(0);
                                            }
                                            if (HereMapUtil.needSeparator(HereManeuverDisplayBuilder.mainSignPostBuilder)) {
                                                HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                                            }
                                            if (!MapSettings.doNotShowTurnTextInTBT()) {
                                                HereManeuverDisplayBuilder.mainSignPostBuilder.append(HereManeuverDisplayBuilder.TOWARDS);
                                                HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                                            }
                                            HereManeuverDisplayBuilder.mainSignPostBuilder.append((String)charSequence);
                                            index = 1;
                                            pendingRoadInfo.hasTo = false;
                                            n8 = n;
                                        }
                                        if (pendingRoadInfo != null) {
                                            if (MapSettings.isTbtOntoDisabled() || !pendingRoadInfo.enterHighway || n8 != 0 || index != 0 || HereManeuverDisplayBuilder.mainSignPostBuilder.length() <= 0) {
                                                break Label_1192;
                                            }
                                            string2 = HereManeuverDisplayBuilder.mainSignPostBuilder.toString();
                                            HereManeuverDisplayBuilder.mainSignPostBuilder.setLength(0);
                                            HereManeuverDisplayBuilder.mainSignPostBuilder.append(HereManeuverDisplayBuilder.ONTO);
                                            HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                                            HereManeuverDisplayBuilder.mainSignPostBuilder.append(string2);
                                        }
                                        return HereManeuverDisplayBuilder.mainSignPostBuilder.toString();
                                        while (true) {
                                            index = 1;
                                            b = b2;
                                            n10 = n;
                                            continue Label_0473_Outer;
                                            b2 = (b = HereMapUtil.hasSameSignpostAndToRoad(maneuver3, maneuver5));
                                            n10 = n;
                                            continue;
                                        }
                                    }
                                    // iftrue(Label_0184:, !b2)
                                }
                                index = n9;
                                if (HereMapUtil.needSeparator(HereManeuverDisplayBuilder.mainSignPostBuilder)) {
                                    HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                                    index = n9;
                                    continue Label_0572_Outer;
                                }
                                continue Label_0572_Outer;
                            }
                            if (pendingRoadInfo != null) {
                                n = index;
                                charSequence = (CharSequence)o;
                                n4 = n10;
                                if (pendingRoadInfo.hasTo) {
                                    continue Label_0845_Outer;
                                }
                            }
                            n = index;
                            charSequence = (CharSequence)o;
                            n4 = n10;
                            if (TextUtils.isEmpty((CharSequence)string)) {
                                HereManeuverDisplayBuilder.mainSignPostBuilder.append(HereMapUtil.concatText(nextRoadNumber, nextRoadName, true, HereMapUtil.isCurrentRoadHighway(maneuver3)));
                                n = index;
                                charSequence = (CharSequence)o;
                                n4 = n10;
                                continue Label_0845_Outer;
                            }
                            continue Label_0845_Outer;
                        }
                        n8 = n;
                        index = n4;
                        if (pendingRoadInfo == null) {
                            continue Label_0922_Outer;
                        }
                        n8 = n;
                        index = n4;
                        if (pendingRoadInfo.hasTo) {
                            string3 = HereManeuverDisplayBuilder.mainSignPostBuilder.toString();
                            HereManeuverDisplayBuilder.mainSignPostBuilder.setLength(0);
                            if (!MapSettings.doNotShowTurnTextInTBT()) {
                                HereManeuverDisplayBuilder.mainSignPostBuilder.append(HereManeuverDisplayBuilder.TOWARDS);
                                HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                            }
                            HereManeuverDisplayBuilder.mainSignPostBuilder.append(string3);
                            index = 1;
                            pendingRoadInfo.hasTo = false;
                            n8 = n;
                            continue Label_0922_Outer;
                        }
                        continue Label_0922_Outer;
                    }
                    if (pendingRoadInfo.hasTo && HereManeuverDisplayBuilder.mainSignPostBuilder.length() > 0) {
                        string4 = HereManeuverDisplayBuilder.mainSignPostBuilder.toString();
                        HereManeuverDisplayBuilder.mainSignPostBuilder.setLength(0);
                        if (!MapSettings.doNotShowTurnTextInTBT()) {
                            HereManeuverDisplayBuilder.mainSignPostBuilder.append(HereManeuverDisplayBuilder.TOWARDS);
                            HereManeuverDisplayBuilder.mainSignPostBuilder.append(" ");
                        }
                        HereManeuverDisplayBuilder.mainSignPostBuilder.append(string4);
                        continue;
                    }
                    continue;
                }
            }
        }
    }
    
    public static MapEvents.ManeuverDisplay getStartManeuverDisplay(final Maneuver maneuver, final Maneuver maneuver2) {
        final MapEvents.ManeuverDisplay maneuverDisplay = new MapEvents.ManeuverDisplay();
        final boolean currentRoadHighway = HereMapUtil.isCurrentRoadHighway(maneuver);
        final boolean currentRoadHighway2 = HereMapUtil.isCurrentRoadHighway(maneuver2);
        final String concatText = HereMapUtil.concatText(maneuver.getRoadNumber(), maneuver.getRoadName(), true, currentRoadHighway);
        final String concatText2 = HereMapUtil.concatText(maneuver2.getRoadNumber(), maneuver2.getRoadName(), true, currentRoadHighway2);
        String s = concatText;
        String s2 = concatText2;
        if (TextUtils.isEmpty((CharSequence)concatText)) {
            final String s3 = concatText2;
            final String concatText3 = HereMapUtil.concatText(maneuver2.getNextRoadNumber(), maneuver2.getNextRoadName(), currentRoadHighway2, currentRoadHighway2);
            s = s3;
            s2 = concatText2;
            if (!TextUtils.isEmpty((CharSequence)concatText3)) {
                s2 = concatText3;
                s = s3;
            }
        }
        String pendingRoadText = s;
        if (TextUtils.isEmpty((CharSequence)s)) {
            pendingRoadText = getPendingRoadText(maneuver, null, null, null);
        }
        maneuverDisplay.turnIconId = HereManeuverDisplayBuilder.sTurnIconImageMap.get(NavigationTurn.NAV_TURN_START);
        maneuverDisplay.pendingTurn = HereManeuverDisplayBuilder.START_TURN;
        maneuverDisplay.currentRoad = HereMapUtil.concatText(maneuver.getRoadNumber(), maneuver.getRoadName(), true, currentRoadHighway);
        maneuverDisplay.currentSpeedLimit = HereMapUtil.getCurrentSpeedLimit();
        if (TextUtils.equals((CharSequence)pendingRoadText, (CharSequence)s2)) {
            if (TextUtils.isEmpty((CharSequence)pendingRoadText)) {
                maneuverDisplay.pendingRoad = "";
            }
            else {
                maneuverDisplay.pendingRoad = HereManeuverDisplayBuilder.resources.getString(R.string.start_maneuver_no_next_road, new Object[] { pendingRoadText });
            }
        }
        else {
            maneuverDisplay.pendingRoad = HereManeuverDisplayBuilder.resources.getString(R.string.start_maneuver, new Object[] { pendingRoadText, s2 });
        }
        setNavigationDistance(0L, maneuverDisplay, false, false);
        maneuverDisplay.totalDistanceRemaining = maneuverDisplay.totalDistance;
        maneuverDisplay.totalDistanceRemainingUnit = maneuverDisplay.totalDistanceUnit;
        maneuverDisplay.distanceToPendingRoadText = "";
        maneuverDisplay.navigationTurn = NavigationTurn.NAV_TURN_START;
        String value;
        if (maneuver != null) {
            value = String.valueOf(System.identityHashCode(maneuver));
        }
        else {
            value = "start";
        }
        maneuverDisplay.maneuverId = value;
        return maneuverDisplay;
    }
    
    private static String getTurnText(final Maneuver maneuver, final PendingRoadInfo pendingRoadInfo, final ManeuverState maneuverState, final Maneuver maneuver2, final Maneuver maneuver3) {
        final Maneuver.Action action = maneuver.getAction();
        final Maneuver.Turn turn = maneuver.getTurn();
        String s;
        if (maneuverState == ManeuverState.STAY) {
            s = HereManeuverDisplayBuilder.STAY_ON;
        }
        else {
            Enum<Maneuver.Turn> turn2;
            if (HereMapUtil.isHighwayAction(maneuver)) {
                if (action == Maneuver.Action.ENTER_HIGHWAY_FROM_LEFT || action == Maneuver.Action.ENTER_HIGHWAY_FROM_RIGHT) {
                    s = HereManeuverDisplayBuilder.ENTER_HIGHWAY;
                    return s;
                }
                turn2 = turn;
                if (maneuver2 != null) {
                    turn2 = turn;
                    if (maneuver3 != null) {
                        if (HereMapUtil.isHighwayAction(maneuver2)) {
                            pendingRoadInfo.enterHighway = true;
                            pendingRoadInfo.usePrevManeuverInfo = true;
                            turn2 = maneuver2.getTurn();
                        }
                        else {
                            turn2 = turn;
                            if (HereMapUtil.isHighwayAction(maneuver3)) {
                                pendingRoadInfo.enterHighway = true;
                                turn2 = turn;
                            }
                        }
                    }
                }
            }
            else if (action == Maneuver.Action.LEAVE_HIGHWAY) {
                final String roadName = HereMapUtil.getRoadName(maneuver);
                final String nextRoadName = HereMapUtil.getNextRoadName(maneuver);
                if (!TextUtils.equals((CharSequence)roadName, (CharSequence)nextRoadName)) {
                    s = HereManeuverDisplayBuilder.EXIT;
                    return s;
                }
                HereManeuverDisplayBuilder.sLogger.v("ignoring action[" + action + "] using[" + turn + "] from[" + roadName + "] to[" + nextRoadName + "]");
                turn2 = turn;
            }
            else {
                turn2 = turn;
                if (action == Maneuver.Action.UTURN) {
                    s = HereManeuverDisplayBuilder.UTURN;
                    return s;
                }
            }
            switch (turn2) {
                case QUITE_LEFT:
                case QUITE_RIGHT:
                case HEAVY_LEFT:
                case HEAVY_RIGHT:
                    pendingRoadInfo.dontUseSignpostText = true;
                    break;
                case KEEP_RIGHT:
                case KEEP_MIDDLE:
                case KEEP_LEFT:
                    pendingRoadInfo.hasTo = true;
                    break;
            }
            s = HereManeuverDisplayBuilder.sHereTurnToNavigationTurnMap.get(turn2);
        }
        return s;
    }
    
    public static String getUnitDisplayStr(final SpeedManager.SpeedUnit speedUnit) {
        String s = null;
        switch (speedUnit) {
            default:
                s = "";
                break;
            case MILES_PER_HOUR:
                s = HereManeuverDisplayBuilder.UNIT_MILES;
                break;
            case KILOMETERS_PER_HOUR:
                s = HereManeuverDisplayBuilder.UNIT_KILOMETERS;
                break;
            case METERS_PER_SECOND:
                s = HereManeuverDisplayBuilder.UNIT_METERS;
                break;
        }
        return s;
    }
    
    public static void setNavigationDistance(final long n, final MapEvents.ManeuverDisplay maneuverDisplay, final boolean b, final boolean b2) {
        final DistanceConverter.Distance distance = HereManeuverDisplayBuilder.DISTANCE;
        synchronized (distance) {
            final SpeedManager.SpeedUnit speedUnit = HereManeuverDisplayBuilder.speedManager.getSpeedUnit();
            long distanceInMeters = n;
            if (n >= 2147483647L) {
                distanceInMeters = 0L;
            }
            maneuverDisplay.distanceInMeters = distanceInMeters;
            DistanceConverter.convertToDistance(speedUnit, distanceInMeters, HereManeuverDisplayBuilder.DISTANCE);
            maneuverDisplay.distance = HereManeuverDisplayBuilder.DISTANCE.value;
            maneuverDisplay.distanceUnit = HereManeuverDisplayBuilder.DISTANCE.unit;
            maneuverDisplay.distanceToPendingRoadText = getFormattedDistance(maneuverDisplay.distance, maneuverDisplay.distanceUnit, b);
            if (!b2) {
                final float n2 = -1.0f;
                final float n3 = -1.0f;
                final HereNavigationManager instance = HereNavigationManager.getInstance();
                float n4 = n3;
                float n5 = n2;
                if (instance.isNavigationModeOn()) {
                    final Route currentRoute = instance.getCurrentRoute();
                    n4 = n3;
                    n5 = n2;
                    if (currentRoute != null) {
                        if ((n4 = instance.getNavController().getDestinationDistance()) < 0.0f) {
                            n4 = 0.0f;
                        }
                        n5 = currentRoute.getLength();
                    }
                }
                if (n5 != -1.0f) {
                    DistanceConverter.convertToDistance(speedUnit, n5, HereManeuverDisplayBuilder.DISTANCE);
                    maneuverDisplay.totalDistance = HereManeuverDisplayBuilder.DISTANCE.value;
                    maneuverDisplay.totalDistanceUnit = HereManeuverDisplayBuilder.DISTANCE.unit;
                    DistanceConverter.convertToDistance(speedUnit, n4, HereManeuverDisplayBuilder.DISTANCE);
                    maneuverDisplay.totalDistanceRemaining = HereManeuverDisplayBuilder.DISTANCE.value;
                    maneuverDisplay.totalDistanceRemainingUnit = HereManeuverDisplayBuilder.DISTANCE.unit;
                }
            }
            // monitorexit(distance)
        }
    }
    
    private static void setNavigationTurnIconId(final Maneuver maneuver, long n, final ManeuverState maneuverState, final MapEvents.ManeuverDisplay maneuverDisplay, final Maneuver maneuver2, final boolean b, final MapEvents.DestinationDirection destinationDirection) {
        final Integer n2 = null;
        Integer n3 = null;
        final Integer n4 = null;
        Integer n5 = null;
        Integer value = null;
        switch (maneuverState) {
            default:
                n5 = n4;
                value = n2;
                break;
            case STAY:
                value = R.drawable.tbt_go_straight_soon;
                maneuverDisplay.navigationTurn = NavigationTurn.NAV_TURN_STRAIGHT;
                n5 = n4;
                break;
            case NEXT:
            case SOON:
            case NOW: {
                final NavigationTurn navigationTurn = getNavigationTurn(maneuver);
                maneuverDisplay.navigationTurn = navigationTurn;
                Integer n6;
                if (maneuverState == ManeuverState.NOW || maneuverState == ManeuverState.SOON) {
                    n6 = HereManeuverDisplayBuilder.sTurnIconImageMap.get(navigationTurn);
                }
                else {
                    n6 = HereManeuverDisplayBuilder.sTurnIconSoonImageMap.get(navigationTurn);
                }
                final Integer n7 = HereManeuverDisplayBuilder.sTurnIconImageMap.get(navigationTurn);
                final Integer n8 = HereManeuverDisplayBuilder.sTurnIconSoonImageMapGrey.get(navigationTurn);
                Integer n9 = n6;
                if (maneuverDisplay.navigationTurn == NavigationTurn.NAV_TURN_END) {
                    n9 = n6;
                    if (destinationDirection != null) {
                        n9 = n6;
                        if (destinationDirection != MapEvents.DestinationDirection.UNKNOWN) {
                            if (maneuverState == ManeuverState.NOW) {
                                switch (destinationDirection) {
                                    default:
                                        n9 = n6;
                                        break;
                                    case LEFT:
                                        n9 = R.drawable.tbt_arrive_left;
                                        break;
                                    case RIGHT:
                                        n9 = R.drawable.tbt_arrive_right;
                                        break;
                                }
                            }
                            else {
                                switch (destinationDirection) {
                                    default:
                                        n9 = n6;
                                        break;
                                    case LEFT:
                                        n9 = R.drawable.tbt_arrive_left_soon;
                                        break;
                                    case RIGHT:
                                        n9 = R.drawable.tbt_arrive_right_soon;
                                        break;
                                }
                            }
                        }
                    }
                }
                if (maneuver2 == null) {
                    maneuverDisplay.turnIconId = -1;
                    maneuverDisplay.turnIconNowId = 0;
                    maneuverDisplay.turnIconSoonId = 0;
                    value = n9;
                    n5 = n7;
                    n3 = n8;
                    break;
                }
                long n10;
                if (b) {
                    n10 = 3218L;
                }
                else {
                    n10 = 1287L;
                }
                if (n > n10) {
                    maneuverDisplay.nextTurnIconId = -1;
                    value = n9;
                    n5 = n7;
                    n3 = n8;
                    break;
                }
                final long n11 = maneuver2.getDistanceFromPreviousManeuver();
                if (b) {
                    n = 1000L;
                }
                else {
                    n = 250L;
                }
                value = n9;
                n5 = n7;
                n3 = n8;
                if (n11 <= n) {
                    final Integer n12 = HereManeuverDisplayBuilder.sTurnIconSoonImageMap.get(getNavigationTurn(maneuver2));
                    if (n9 == null) {
                        maneuverDisplay.nextTurnIconId = -1;
                    }
                    else {
                        maneuverDisplay.nextTurnIconId = n12;
                    }
                    HereManeuverDisplayBuilder.sLogger.v("distance is less than threshold " + n11);
                    value = n9;
                    n5 = n7;
                    n3 = n8;
                    break;
                }
                break;
            }
        }
        if (value == null) {
            maneuverDisplay.turnIconId = -1;
            maneuverDisplay.turnIconNowId = 0;
            maneuverDisplay.turnIconSoonId = 0;
        }
        else {
            maneuverDisplay.turnIconId = value;
            int intValue;
            if (n3 == null) {
                intValue = 0;
            }
            else {
                intValue = n3;
            }
            maneuverDisplay.turnIconSoonId = intValue;
            int intValue2;
            if (n5 == null) {
                intValue2 = 0;
            }
            else {
                intValue2 = n5;
            }
            maneuverDisplay.turnIconNowId = intValue2;
        }
    }
    
    public static boolean shouldShowTurnText(final String s) {
        return HereManeuverDisplayBuilder.allowedTurnTextMap.contains(s);
    }
    
    public enum ManeuverState
    {
        NEXT, 
        NOW, 
        SOON, 
        STAY;
    }
    
    public static class PendingRoadInfo
    {
        public boolean dontUseSignpostText;
        public boolean enterHighway;
        public boolean hasTo;
        public boolean usePrevManeuverInfo;
    }
}
