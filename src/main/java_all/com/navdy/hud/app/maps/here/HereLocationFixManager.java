package com.navdy.hud.app.maps.here;

import com.navdy.service.library.events.debug.StopDriveRecordingEvent;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import com.navdy.hud.app.debug.DriveRecorder;
import com.squareup.otto.Subscribe;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.MatchedGeoPosition;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.HudApplication;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.hud.app.maps.MapEvents;
import android.os.SystemClock;
import android.os.Looper;
import java.util.Locale;
import com.navdy.hud.app.manager.SpeedManager;
import java.io.FileOutputStream;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.common.PositioningManager;
import com.navdy.hud.app.device.gps.GpsUtils;
import android.location.Location;
import android.os.Handler;
import com.here.android.mpa.common.GeoPosition;
import com.squareup.otto.Bus;
import android.location.LocationListener;
import com.navdy.service.library.log.Logger;
import java.text.SimpleDateFormat;

public class HereLocationFixManager
{
    private static final SimpleDateFormat DATE_FORMAT;
    private static final byte[] RECORD_MARKER;
    private static final Logger sLogger;
    private LocationListener androidGpsLocationListener;
    private final Bus bus;
    private Runnable checkLocationFix;
    private long counter;
    private LocationListener debugTTSLocationListener;
    private boolean firstLocationFixCheck;
    private GeoPosition geoPosition;
    private Handler handler;
    private volatile boolean hereGpsSignal;
    private volatile boolean isRecording;
    private Location lastAndroidLocation;
    private long lastAndroidLocationPostTime;
    private long lastAndroidLocationTime;
    private GpsUtils.GpsSwitch lastGpsSwitchEvent;
    private long lastHerelocationUpdateTime;
    private volatile long lastLocationTime;
    private volatile boolean locationFix;
    private final LocationListener locationListener;
    private PositioningManager.LocationMethod locationMethod;
    private MapMarker rawLocationMarker;
    private boolean rawLocationMarkerAdded;
    private FileOutputStream recordingFile;
    private volatile String recordingLabel;
    private final SpeedManager speedManager;
    
    static {
        sLogger = new Logger(HereLocationFixManager.class);
        DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'_'HH_mm_ss.SSS", Locale.US);
        RECORD_MARKER = "<< Marker >>\n".getBytes();
    }
    
    HereLocationFixManager(final Bus bus) {
        this.handler = new Handler(Looper.getMainLooper());
        this.firstLocationFixCheck = true;
        this.checkLocationFix = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    long n = 0L;
                    Label_0223: {
                        try {
                            n = SystemClock.elapsedRealtime() - HereLocationFixManager.this.lastAndroidLocationTime;
                            if (HereLocationFixManager.this.locationFix) {
                                if (n >= 3000L) {
                                    HereLocationFixManager.sLogger.i("lost location fix[" + n + "]");
                                    HereLocationFixManager.this.locationFix = false;
                                    HereLocationFixManager.this.bus.post(new MapEvents.LocationFix(false, false, false));
                                }
                                else if (HereLocationFixManager.sLogger.isLoggable(2)) {
                                    HereLocationFixManager.sLogger.v("still has location fix [" + n + "]");
                                }
                                return;
                            }
                            break Label_0223;
                        }
                        catch (Throwable t) {
                            HereLocationFixManager.sLogger.e(t);
                            HereLocationFixManager.this.handler.postDelayed(HereLocationFixManager.this.checkLocationFix, 1000L);
                            HereLocationFixManager.this.firstLocationFixCheck = false;
                            return;
                            // iftrue(Label_0351:, HereLocationFixManager.access$400(this.this$0) == null || n > 3000L)
                            HereLocationFixManager.sLogger.v("got location fix [" + n + "] " + HereLocationFixManager.this.lastAndroidLocation);
                            HereLocationFixManager.this.locationFix = true;
                            HereLocationFixManager.this.bus.post(HereLocationFixManager.this.getLocationFixEvent());
                            return;
                        }
                        finally {
                            HereLocationFixManager.this.handler.postDelayed(HereLocationFixManager.this.checkLocationFix, 1000L);
                            HereLocationFixManager.this.firstLocationFixCheck = false;
                        }
                    }
                    Label_0351: {
                        if (HereLocationFixManager.sLogger.isLoggable(2)) {
                            HereLocationFixManager.sLogger.v("still don't have location fix [" + n + "]");
                        }
                    }
                    if (HereLocationFixManager.this.firstLocationFixCheck) {
                        HereLocationFixManager.this.bus.post(new MapEvents.LocationFix(false, false, false));
                    }
                }
            }
        };
        this.locationListener = (LocationListener)new LocationListener() {
            public void onLocationChanged(final Location maptoLocation) {
                if (!HereLocationFixManager.this.locationFix) {
                    HereLocationFixManager.this.setMaptoLocation(maptoLocation);
                }
            Label_0074:
                while (true) {
                    if (!GpsUtils.isDebugRawGpsPosEnabled()) {
                        break Label_0074;
                    }
                    while (true) {
                        try {
                            if (HereLocationFixManager.this.rawLocationMarker != null && HereLocationFixManager.this.rawLocationMarkerAdded) {
                                HereLocationFixManager.this.rawLocationMarker.setCoordinate(new GeoCoordinate(maptoLocation.getLatitude(), maptoLocation.getLongitude()));
                            }
                            if (HereLocationFixManager.this.isRecording) {
                                HereLocationFixManager.this.recordRawLocation(maptoLocation);
                            }
                            HereLocationFixManager.this.lastAndroidLocationTime = SystemClock.elapsedRealtime();
                            HereLocationFixManager.this.lastAndroidLocation = maptoLocation;
                            if (HereLocationFixManager.this.lastAndroidLocationTime - HereLocationFixManager.this.lastAndroidLocationPostTime < 1000L) {
                                return;
                            }
                        }
                        catch (Throwable t) {
                            HereLocationFixManager.sLogger.e(t);
                            continue Label_0074;
                        }
                        if (!TextUtils.equals((CharSequence)maptoLocation.getProvider(), (CharSequence)"NAVDY_GPS_PROVIDER")) {
                            HereLocationFixManager.this.sendLocation(maptoLocation);
                        }
                        HereLocationFixManager.this.lastAndroidLocationPostTime = HereLocationFixManager.this.lastAndroidLocationTime;
                    }
                    break;
                }
            }
            
            public void onProviderDisabled(final String s) {
            }
            
            public void onProviderEnabled(final String s) {
            }
            
            public void onStatusChanged(final String s, final int n, final Bundle bundle) {
            }
        };
        this.androidGpsLocationListener = null;
        this.debugTTSLocationListener = null;
        this.bus = bus;
        this.speedManager = SpeedManager.getInstance();
        final LocationManager locationManager = (LocationManager)HudApplication.getAppContext().getSystemService("location");
        final Looper bkLocationReceiverLooper = HereMapsManager.getInstance().getBkLocationReceiverLooper();
        if (TTSUtils.isDebugTTSEnabled() && DeviceUtil.isNavdyDevice() && locationManager.getProvider("gps") != null) {
            locationManager.requestLocationUpdates("gps", 0L, 0.0f, this.debugTTSLocationListener = (LocationListener)new LocationListener() {
                boolean toastSent = false;
                final /* synthetic */ long val$t1 = SystemClock.elapsedRealtime();
                
                public void onLocationChanged(final Location location) {
                    if (this.toastSent) {
                        HereLocationFixManager.sLogger.v("ublox first fix toast already posted");
                    }
                    else {
                        locationManager.removeUpdates((LocationListener)this);
                        this.toastSent = true;
                        final long n = SystemClock.elapsedRealtime() - this.val$t1;
                        HereLocationFixManager.sLogger.v("got first u-blox fix, unregister (" + n + ")");
                        if (n < 10000L) {
                            HereLocationFixManager.this.handler.postDelayed((Runnable)new Runnable() {
                                @Override
                                public void run() {
                                    TTSUtils.debugShowGotUbloxFix();
                                }
                            }, 10000L - n);
                        }
                        else {
                            TTSUtils.debugShowGotUbloxFix();
                        }
                    }
                }
                
                public void onProviderDisabled(final String s) {
                }
                
                public void onProviderEnabled(final String s) {
                }
                
                public void onStatusChanged(final String s, final int n, final Bundle bundle) {
                }
            }, bkLocationReceiverLooper);
        }
        Location lastKnownLocation = null;
        if (locationManager.getProvider("NAVDY_GPS_PROVIDER") != null) {
            lastKnownLocation = locationManager.getLastKnownLocation("NAVDY_GPS_PROVIDER");
            locationManager.requestLocationUpdates("NAVDY_GPS_PROVIDER", 0L, 0.0f, this.locationListener, bkLocationReceiverLooper);
            HereLocationFixManager.sLogger.v("installed gps listener");
        }
        if (locationManager.getProvider("gps") != null) {
            locationManager.requestLocationUpdates("gps", 0L, 0.0f, this.androidGpsLocationListener = (LocationListener)new LocationListener() {
                public void onLocationChanged(final Location location) {
                    if (HereLocationFixManager.this.isRecording && !DeviceUtil.isNavdyDevice()) {
                        HereLocationFixManager.this.recordRawLocation(location);
                    }
                    HereLocationFixManager.this.sendLocation(location);
                }
                
                public void onProviderDisabled(final String s) {
                }
                
                public void onProviderEnabled(final String s) {
                }
                
                public void onStatusChanged(final String s, final int n, final Bundle bundle) {
                }
            }, bkLocationReceiverLooper);
        }
        HereLocationFixManager.sLogger.v("installed gps listener");
        Location lastKnownLocation2 = lastKnownLocation;
        if (locationManager.getProvider("network") != null) {
            if ((lastKnownLocation2 = lastKnownLocation) == null) {
                lastKnownLocation2 = locationManager.getLastKnownLocation("network");
            }
            locationManager.requestLocationUpdates("network", 0L, 0.0f, this.locationListener, bkLocationReceiverLooper);
            HereLocationFixManager.sLogger.v("installed n/w listener");
        }
        HereLocationFixManager.sLogger.v("got last location from location manager:" + lastKnownLocation2);
        if (!this.locationFix && lastKnownLocation2 != null) {
            HereLocationFixManager.sLogger.v("sending to map");
            this.setMaptoLocation(lastKnownLocation2);
        }
        this.bus.register(this);
        this.handler.postDelayed(this.checkLocationFix, 1000L);
        final StartDriveRecordingEvent driverRecordingEvent = RemoteDeviceManager.getInstance().getConnectionHandler().getDriverRecordingEvent();
        if (driverRecordingEvent != null) {
            this.onStartDriveRecording(driverRecordingEvent);
        }
        HereLocationFixManager.sLogger.v("initialized");
    }
    
    private void recordHereLocation(final GeoPosition geoPosition, final PositioningManager.LocationMethod locationMethod, final boolean b) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final GeoCoordinate coordinate = geoPosition.getCoordinate();
                    String string = "";
                    if (geoPosition instanceof MatchedGeoPosition) {
                        final MatchedGeoPosition matchedGeoPosition = (MatchedGeoPosition)geoPosition;
                        string = "," + matchedGeoPosition.getMatchQuality() + "," + matchedGeoPosition.isExtrapolated() + "," + matchedGeoPosition.isOnStreet();
                    }
                    HereLocationFixManager.this.recordingFile.write((coordinate.getLatitude() + "," + coordinate.getLongitude() + "," + geoPosition.getHeading() + "," + geoPosition.getSpeed() + "," + geoPosition.getLatitudeAccuracy() + "," + coordinate.getAltitude() + "," + geoPosition.getTimestamp().getTime() + "," + "here" + "," + locationMethod.name() + "," + b + string + "\n").getBytes());
                }
                catch (Throwable t) {
                    HereLocationFixManager.sLogger.e(t);
                }
            }
        }, 9);
    }
    
    private void recordRawLocation(final Location location) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String provider;
                    if ("NAVDY_GPS_PROVIDER".equals(provider = location.getProvider())) {
                        provider = "gps";
                    }
                    HereLocationFixManager.this.recordingFile.write((location.getLatitude() + "," + location.getLongitude() + "," + location.getBearing() + "," + location.getSpeed() + "," + location.getAccuracy() + "," + location.getAltitude() + "," + location.getTime() + "," + "raw" + "," + provider + "\n").getBytes());
                }
                catch (Throwable t) {
                    HereLocationFixManager.sLogger.e(t);
                }
            }
        }, 9);
    }
    
    private void sendLocation(final Location location) {
        if (this.speedManager.setGpsSpeed(location.getSpeed(), location.getElapsedRealtimeNanos() / 1000000L)) {
            this.bus.post(new MapEvents.GPSSpeedEvent());
        }
        this.lastLocationTime = SystemClock.elapsedRealtime();
        this.bus.post(location);
    }
    
    public void addMarkers(final HereMapController hereMapController) {
        if (GpsUtils.isDebugRawGpsPosEnabled()) {
            try {
                if (this.rawLocationMarker == null) {
                    this.rawLocationMarker = new MapMarker();
                    final Image icon = new Image();
                    icon.setImageResource(R.drawable.icon_position_raw_gps);
                    this.rawLocationMarker.setIcon(icon);
                    HereLocationFixManager.sLogger.v("marker created");
                }
                if (!this.rawLocationMarkerAdded) {
                    hereMapController.addMapObject(this.rawLocationMarker);
                    this.rawLocationMarkerAdded = true;
                    HereLocationFixManager.sLogger.v("marker added");
                }
            }
            catch (Throwable t) {
                HereLocationFixManager.sLogger.e(t);
            }
        }
    }
    
    public GeoCoordinate getLastGeoCoordinate() {
        GeoCoordinate coordinate;
        if (this.geoPosition == null) {
            coordinate = null;
        }
        else {
            coordinate = this.geoPosition.getCoordinate();
        }
        return coordinate;
    }
    
    public long getLastLocationTime() {
        return this.lastLocationTime;
    }
    
    public MapEvents.LocationFix getLocationFixEvent() {
        Object o;
        if (this.locationFix) {
            final boolean b = false;
            boolean b2 = false;
            boolean b3 = b;
            if (this.lastAndroidLocation != null) {
                if (TextUtils.equals((CharSequence)this.lastAndroidLocation.getProvider(), (CharSequence)"NAVDY_GPS_PROVIDER")) {
                    b2 = true;
                    b3 = b;
                }
                else {
                    b3 = true;
                    b2 = b2;
                }
            }
            o = new MapEvents.LocationFix(true, b3, b2);
        }
        else {
            o = new MapEvents.LocationFix(false, false, false);
        }
        return (MapEvents.LocationFix)o;
    }
    
    public boolean hasLocationFix() {
        return this.locationFix;
    }
    
    public boolean isRecording() {
        return this.isRecording;
    }
    
    @Subscribe
    public void onGpsEventSwitch(final GpsUtils.GpsSwitch lastGpsSwitchEvent) {
        HereLocationFixManager.sLogger.v("Gps-Switch phone=" + lastGpsSwitchEvent.usingPhone + " ublox=" + lastGpsSwitchEvent.usingUblox);
        this.lastGpsSwitchEvent = lastGpsSwitchEvent;
    }
    
    @Subscribe
    public void onHereGpsEvent(final MapEvents.GpsStatusChange gpsStatusChange) {
        this.hereGpsSignal = gpsStatusChange.connected;
        HereLocationFixManager.sLogger.v("here gps event connected [" + this.hereGpsSignal + "]");
    }
    
    public void onHerePositionUpdated(final PositioningManager.LocationMethod locationMethod, final GeoPosition geoPosition, final boolean b) {
        if (HereLocationFixManager.sLogger.isLoggable(2)) {
            final Logger sLogger = HereLocationFixManager.sLogger;
            final StringBuilder append = new StringBuilder().append("onHerePositionUpdated [");
            final long counter = this.counter + 1L;
            this.counter = counter;
            sLogger.v(append.append(counter).append("] geoPosition[").append(geoPosition.getCoordinate()).append("] method [").append(locationMethod.name()).append("] valid[").append(geoPosition.isValid()).append("]").toString());
        }
        if (locationMethod != null && geoPosition != null) {
            this.locationMethod = locationMethod;
            this.geoPosition = geoPosition;
            this.lastHerelocationUpdateTime = SystemClock.elapsedRealtime();
            if (this.isRecording) {
                this.recordHereLocation(geoPosition, locationMethod, b);
            }
        }
    }
    
    @Subscribe
    public void onStartDriveRecording(final StartDriveRecordingEvent startDriveRecordingEvent) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (!HereLocationFixManager.this.isRecording) {
                        HereLocationFixManager.this.recordingLabel = startDriveRecordingEvent.label;
                        HereLocationFixManager.this.isRecording = true;
                        final File driveLogsDir = DriveRecorder.getDriveLogsDir(HereLocationFixManager.this.recordingLabel);
                        if (!driveLogsDir.exists()) {
                            driveLogsDir.mkdirs();
                        }
                        final File file = new File(driveLogsDir, HereLocationFixManager.this.recordingLabel + ".here");
                        HereLocationFixManager.this.recordingFile = new FileOutputStream(file);
                        HereLocationFixManager.sLogger.v("created recording file [" + HereLocationFixManager.this.recordingLabel + "] path:" + file.getAbsolutePath());
                    }
                    else {
                        HereLocationFixManager.sLogger.v("already recording");
                    }
                }
                catch (Throwable t) {
                    HereLocationFixManager.sLogger.e(t);
                    HereLocationFixManager.this.isRecording = false;
                    HereLocationFixManager.this.recordingLabel = null;
                    IOUtils.closeStream(HereLocationFixManager.this.recordingFile);
                    HereLocationFixManager.this.recordingFile = null;
                }
            }
        }, 9);
    }
    
    @Subscribe
    public void onStopDriveRecording(final StopDriveRecordingEvent stopDriveRecordingEvent) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (HereLocationFixManager.this.isRecording) {
                        HereLocationFixManager.this.isRecording = false;
                        IOUtils.fileSync(HereLocationFixManager.this.recordingFile);
                        IOUtils.closeStream(HereLocationFixManager.this.recordingFile);
                        HereLocationFixManager.this.recordingFile = null;
                        HereLocationFixManager.sLogger.v("stopped recording file [" + HereLocationFixManager.this.recordingLabel + "]");
                        HereLocationFixManager.this.recordingLabel = null;
                    }
                    else {
                        HereLocationFixManager.sLogger.v("not recording");
                    }
                }
                catch (Throwable t) {
                    HereLocationFixManager.sLogger.e(t);
                }
            }
        }, 9);
    }
    
    public void recordMarker() {
        if (this.isRecording) {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        HereLocationFixManager.this.recordingFile.write(HereLocationFixManager.RECORD_MARKER);
                    }
                    catch (Throwable t) {
                        HereLocationFixManager.sLogger.e(t);
                    }
                }
            }, 9);
        }
    }
    
    public void removeMarkers(final HereMapController hereMapController) {
        if (GpsUtils.isDebugRawGpsPosEnabled() && this.rawLocationMarker != null && this.rawLocationMarkerAdded) {
            hereMapController.removeMapObject(this.rawLocationMarker);
            this.rawLocationMarkerAdded = false;
            HereLocationFixManager.sLogger.v("marker removed");
        }
    }
    
    public void setMaptoLocation(final Location location) {
        try {
            if (!this.locationFix && this.geoPosition == null) {
                HereLocationFixManager.sLogger.v("marking location fix:" + location);
                this.geoPosition = new GeoPosition(new GeoCoordinate(location.getLatitude(), location.getLongitude(), location.getAltitude()));
                this.locationMethod = PositioningManager.LocationMethod.NETWORK;
                this.lastHerelocationUpdateTime = SystemClock.elapsedRealtime();
                this.locationFix = true;
                this.bus.post(new MapEvents.LocationFix(true, true, false));
            }
        }
        catch (Throwable t) {
            HereLocationFixManager.sLogger.e(t);
        }
    }
    
    public void shutdown() {
        final LocationManager locationManager = (LocationManager)HudApplication.getAppContext().getSystemService("location");
        locationManager.removeUpdates(this.locationListener);
        if (this.androidGpsLocationListener != null) {
            locationManager.removeUpdates(this.androidGpsLocationListener);
        }
        if (this.debugTTSLocationListener != null) {
            locationManager.removeUpdates(this.debugTTSLocationListener);
        }
    }
}
