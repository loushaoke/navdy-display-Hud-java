package com.navdy.hud.app.maps.here;

import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.search.Address;
import com.navdy.hud.app.util.CrashReporter;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.ReverseGeocodeRequest2;
import android.os.Looper;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import android.os.Handler;
import com.navdy.hud.app.maps.MapEvents;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class HereRegionManager
{
    private static final long REGION_CHECK_INTERVAL = 900000L;
    private static final HereRegionManager sInstance;
    private static final Logger sLogger;
    @Inject
    Bus bus;
    private final Runnable checkRegionRunnable;
    private MapEvents.RegionEvent currentRegionEvent;
    private final Handler handler;
    private final HereMapsManager hereMapsManager;
    
    static {
        sLogger = new Logger(HereRegionManager.class);
        sInstance = new HereRegionManager();
    }
    
    private HereRegionManager() {
        this.checkRegionRunnable = new Runnable() {
            @Override
            public void run() {
                HereRegionManager.this.checkRegion();
            }
        };
        Mortar.inject(HudApplication.getAppContext(), this);
        this.hereMapsManager = HereMapsManager.getInstance();
        (this.handler = new Handler(Looper.getMainLooper())).postDelayed(this.checkRegionRunnable, 900000L);
    }
    
    private void checkRegion() {
        try {
            final GeoPosition lastGeoPosition = this.hereMapsManager.getLastGeoPosition();
            if (lastGeoPosition == null) {
                HereRegionManager.sLogger.v("invalid checkRegion call with unknown geoPosition");
            }
            else {
                final ErrorCode execute = new ReverseGeocodeRequest2(lastGeoPosition.getCoordinate()).execute(new ResultListener<Location>() {
                    @Override
                    public void onCompleted(final Location location, final ErrorCode errorCode) {
                        while (true) {
                            Label_0078: {
                                try {
                                    if (errorCode != ErrorCode.NONE) {
                                        HereRegionManager.sLogger.e("reverseGeoCode:onComplete: Error[" + errorCode.name() + "]");
                                    }
                                    else {
                                        if (location != null) {
                                            break Label_0078;
                                        }
                                        HereRegionManager.sLogger.e("reverseGeoCode:onComplete: address is null");
                                    }
                                    return;
                                }
                                catch (Throwable t) {
                                    HereRegionManager.sLogger.e(t);
                                    CrashReporter.getInstance().reportNonFatalException(t);
                                    return;
                                }
                            }
                            final Address address = location.getAddress();
                            if (address == null) {
                                return;
                            }
                            final MapEvents.RegionEvent regionEvent = new MapEvents.RegionEvent(address.getState(), address.getCountryName());
                            if (!regionEvent.equals(HereRegionManager.this.currentRegionEvent)) {
                                HereRegionManager.sLogger.v("" + regionEvent);
                                HereRegionManager.this.currentRegionEvent = regionEvent;
                                HereRegionManager.this.bus.post(HereRegionManager.this.currentRegionEvent);
                            }
                        }
                    }
                });
                if (execute != ErrorCode.NONE) {
                    HereRegionManager.sLogger.e("reverseGeoCode:execute: Error[" + execute.name() + "]");
                }
                this.handler.postDelayed(this.checkRegionRunnable, 900000L);
            }
        }
        catch (Throwable t) {
            HereRegionManager.sLogger.e(t);
            CrashReporter.getInstance().reportNonFatalException(t);
            this.handler.postDelayed(this.checkRegionRunnable, 900000L);
        }
        finally {
            this.handler.postDelayed(this.checkRegionRunnable, 900000L);
        }
    }
    
    public static HereRegionManager getInstance() {
        return HereRegionManager.sInstance;
    }
}
