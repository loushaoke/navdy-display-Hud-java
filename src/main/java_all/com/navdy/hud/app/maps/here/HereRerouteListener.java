package com.navdy.hud.app.maps.here;

import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import android.text.TextUtils;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.maps.MapSettings;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.analytics.NavigationQualityTracker;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.navdy.hud.app.maps.MapEvents;
import com.here.android.mpa.guidance.NavigationManager;

public class HereRerouteListener extends RerouteListener
{
    public static final MapEvents.RerouteEvent REROUTE_FAILED;
    public static final MapEvents.RerouteEvent REROUTE_FINISHED;
    public static final MapEvents.RerouteEvent REROUTE_STARTED;
    private Bus bus;
    private HereNavigationManager hereNavigationManager;
    private Logger logger;
    private final NavigationQualityTracker navigationQualityTracker;
    private String routeCalcId;
    private volatile boolean routeCalculationOn;
    private String tag;
    
    static {
        REROUTE_STARTED = new MapEvents.RerouteEvent(MapEvents.RouteEventType.STARTED);
        REROUTE_FINISHED = new MapEvents.RerouteEvent(MapEvents.RouteEventType.FINISHED);
        REROUTE_FAILED = new MapEvents.RerouteEvent(MapEvents.RouteEventType.FAILED);
    }
    
    HereRerouteListener(final Logger logger, final String tag, final HereNavigationManager hereNavigationManager, final Bus bus) {
        this.logger = new Logger(HereRerouteListener.class);
        this.tag = tag;
        this.hereNavigationManager = hereNavigationManager;
        this.bus = bus;
        this.navigationQualityTracker = NavigationQualityTracker.getInstance();
    }
    
    public boolean isRecalculating() {
        return this.routeCalculationOn;
    }
    
    @Override
    public void onRerouteBegin() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                HereRerouteListener.this.routeCalcId = HereRerouteListener.this.hereNavigationManager.getCurrentRouteId();
                HereRerouteListener.this.logger.i(HereRerouteListener.this.tag + " reroute-begin: id=" + HereRerouteListener.this.routeCalcId);
                HereRerouteListener.this.routeCalculationOn = true;
                if (HereRerouteListener.this.hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn && !HereRerouteListener.this.hereNavigationManager.hasArrived() && !MapSettings.isTtsRecalculationDisabled()) {
                    HereRerouteListener.this.bus.post(HereRerouteListener.this.hereNavigationManager.TTS_REROUTING);
                }
                HereRerouteListener.this.bus.post(HereRerouteListener.REROUTE_STARTED);
                HereRerouteListener.this.bus.post(HereManeuverDisplayBuilder.EMPTY_MANEUVER_DISPLAY);
            }
        }, 20);
    }
    
    @Override
    public void onRerouteEnd(final Route route) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final String currentRouteId = HereRerouteListener.this.hereNavigationManager.getCurrentRouteId();
                final String access$000 = HereRerouteListener.this.routeCalcId;
                HereRerouteListener.this.logger.i(HereRerouteListener.this.tag + " reroute-end: " + route + " id=" + HereRerouteListener.this.routeCalcId + " current=" + currentRouteId);
                HereRerouteListener.this.routeCalcId = null;
                HereRerouteListener.this.routeCalculationOn = false;
                HereRerouteListener.this.bus.post(HereRerouteListener.REROUTE_FINISHED);
                if (route != null) {
                    if (!HereRerouteListener.this.hereNavigationManager.isNavigationModeOn()) {
                        HereRerouteListener.this.logger.i(HereRerouteListener.this.tag + "reroute-end: navigation already ended, not adding route");
                    }
                    else if (!TextUtils.equals((CharSequence)access$000, (CharSequence)currentRouteId)) {
                        HereRerouteListener.this.logger.i("route change before recalc completed: cannot use");
                    }
                    else {
                        HereRerouteListener.this.hereNavigationManager.setReroute(route, NavigationSessionRouteChange.RerouteReason.NAV_SESSION_OFF_REROUTE, null, null, false, HereRerouteListener.this.hereNavigationManager.isTrafficConsidered(currentRouteId));
                        HereRerouteListener.this.navigationQualityTracker.trackTripRecalculated();
                    }
                }
            }
        }, 20);
    }
    
    @Override
    public void onRerouteFailed() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                HereRerouteListener.this.routeCalculationOn = false;
                HereRerouteListener.this.routeCalcId = null;
                HereRerouteListener.this.logger.w(HereRerouteListener.this.tag + " reroute-failed");
                if (HereRerouteListener.this.hereNavigationManager.getNavigationSessionPreference().spokenTurnByTurn && !HereRerouteListener.this.hereNavigationManager.hasArrived() && !MapSettings.isTtsRecalculationDisabled()) {
                    HereRerouteListener.this.bus.post(HereRerouteListener.this.hereNavigationManager.TTS_REROUTING_FAILED);
                }
                HereRerouteListener.this.bus.post(HereRerouteListener.REROUTE_FAILED);
            }
        }, 20);
    }
}
