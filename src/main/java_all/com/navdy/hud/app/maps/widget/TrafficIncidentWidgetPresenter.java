package com.navdy.hud.app.maps.widget;

import android.os.Bundle;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.squareup.otto.Subscribe;
import android.graphics.Bitmap;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.text.SpannableStringBuilder;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.maps.MapEvents;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Looper;
import android.widget.TextView;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import com.navdy.hud.app.view.DashboardWidgetPresenter;

public class TrafficIncidentWidgetPresenter extends DashboardWidgetPresenter
{
    private static Handler handler;
    private static final Logger sLogger;
    private Bus bus;
    private boolean registered;
    private TextView textView;
    
    static {
        sLogger = new Logger("TrafficIncidentWidget");
        TrafficIncidentWidgetPresenter.handler = new Handler(Looper.getMainLooper());
    }
    
    public TrafficIncidentWidgetPresenter() {
        this.bus = RemoteDeviceManager.getInstance().getBus();
    }
    
    private void setText() {
        this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_1);
        if (HereMapsManager.getInstance().isInitialized() && HereNavigationManager.getInstance().isTrafficUpdaterRunning()) {
            this.textView.setText((CharSequence)"NORMAL");
        }
        else {
            this.textView.setText((CharSequence)"INACTIVE");
        }
    }
    
    private void unregister() {
        if (this.registered) {
            TrafficIncidentWidgetPresenter.sLogger.v("unregister");
            this.bus.unregister(this);
            this.registered = false;
        }
    }
    
    @Override
    public Drawable getDrawable() {
        return null;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "TRAFFIC_INCIDENT_GAUGE_ID";
    }
    
    @Override
    public String getWidgetName() {
        return null;
    }
    
    @Subscribe
    public void onDisplayTrafficIncident(final MapEvents.DisplayTrafficIncident displayTrafficIncident) {
        while (true) {
            Label_0253: {
                Label_0229: {
                    try {
                        if (this.textView != null) {
                            TrafficIncidentWidgetPresenter.sLogger.v("DisplayTrafficIncident type:" + displayTrafficIncident.type + ", category:" + displayTrafficIncident.category + ", title:" + displayTrafficIncident.title + ", description:" + displayTrafficIncident.description + ", affectedStreet:" + displayTrafficIncident.affectedStreet + ", distanceToIncident:" + displayTrafficIncident.distanceToIncident + ", reported:" + displayTrafficIncident.reported + ", updated:" + displayTrafficIncident.updated + ", icon:" + displayTrafficIncident.icon);
                            switch (displayTrafficIncident.type) {
                                default:
                                    TaskManager.getInstance().execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                                            Bitmap bitmap = null;
                                            if (displayTrafficIncident.icon != null) {
                                                bitmap = displayTrafficIncident.icon.getBitmap(40, 40);
                                            }
                                            spannableStringBuilder.append((CharSequence)displayTrafficIncident.type.name());
                                            spannableStringBuilder.setSpan(new StyleSpan(1), 0, spannableStringBuilder.length(), 33);
                                            spannableStringBuilder.append((CharSequence)" ");
                                            final int length = spannableStringBuilder.length();
                                            if (bitmap != null) {
                                                final ImageSpan imageSpan = new ImageSpan(HudApplication.getAppContext(), bitmap);
                                                spannableStringBuilder.append((CharSequence)"IMG");
                                                spannableStringBuilder.setSpan(imageSpan, length, spannableStringBuilder.length(), 33);
                                                spannableStringBuilder.append((CharSequence)" ");
                                            }
                                            if (!TextUtils.isEmpty((CharSequence)displayTrafficIncident.title)) {
                                                spannableStringBuilder.append((CharSequence)"(");
                                                spannableStringBuilder.append((CharSequence)displayTrafficIncident.title);
                                                spannableStringBuilder.append((CharSequence)") ,");
                                            }
                                            if (!TextUtils.isEmpty((CharSequence)displayTrafficIncident.description)) {
                                                spannableStringBuilder.append((CharSequence)displayTrafficIncident.description);
                                                spannableStringBuilder.append((CharSequence)" , ");
                                            }
                                            if (!TextUtils.isEmpty((CharSequence)displayTrafficIncident.affectedStreet)) {
                                                spannableStringBuilder.append((CharSequence)"street=");
                                                spannableStringBuilder.append((CharSequence)displayTrafficIncident.affectedStreet);
                                                spannableStringBuilder.append((CharSequence)" , ");
                                            }
                                            if (displayTrafficIncident.distanceToIncident != -1L) {
                                                spannableStringBuilder.append((CharSequence)"dist=");
                                                spannableStringBuilder.append((CharSequence)String.valueOf(displayTrafficIncident.distanceToIncident));
                                                spannableStringBuilder.append((CharSequence)" , ");
                                            }
                                            if (displayTrafficIncident.updated != null) {
                                                spannableStringBuilder.append((CharSequence)(String.valueOf(TimeUnit.MILLISECONDS.toMinutes(new Date().getTime() - displayTrafficIncident.updated.getTime())) + " minutes ago"));
                                            }
                                            TrafficIncidentWidgetPresenter.handler.post((Runnable)new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (TrafficIncidentWidgetPresenter.this.textView != null) {
                                                        TrafficIncidentWidgetPresenter.this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_2);
                                                        TrafficIncidentWidgetPresenter.this.textView.setText((CharSequence)spannableStringBuilder);
                                                    }
                                                }
                                            });
                                        }
                                    }, 1);
                                    break;
                                case NORMAL:
                                    this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_1);
                                    this.textView.setText((CharSequence)"NORMAL");
                                    break;
                                case INACTIVE:
                                    break Label_0229;
                                case FAILED:
                                    break Label_0253;
                            }
                        }
                        return;
                    }
                    catch (Throwable t) {
                        TrafficIncidentWidgetPresenter.sLogger.e(t);
                        return;
                    }
                }
                this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_1);
                this.textView.setText((CharSequence)"INACTIVE");
                return;
            }
            this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_1);
            this.textView.setText((CharSequence)"FAILED");
        }
    }
    
    @Override
    public void setView(final DashboardWidgetView dashboardWidgetView, final Bundle bundle) {
        if (dashboardWidgetView != null) {
            boolean boolean1 = false;
            if (bundle != null) {
                boolean1 = bundle.getBoolean("EXTRA_IS_ACTIVE", false);
            }
            dashboardWidgetView.setContentView(R.layout.maps_traffic_incident_widget);
            this.textView = (TextView)dashboardWidgetView.findViewById(R.id.incidentInfo);
            this.setText();
            super.setView(dashboardWidgetView, bundle);
            if (!boolean1) {
                TrafficIncidentWidgetPresenter.sLogger.v("not active");
                this.unregister();
            }
            else if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                TrafficIncidentWidgetPresenter.sLogger.v("register");
            }
            else {
                TrafficIncidentWidgetPresenter.sLogger.v("already register");
            }
        }
        else {
            this.unregister();
            this.textView = null;
            super.setView(dashboardWidgetView, bundle);
        }
    }
    
    @Override
    protected void updateGauge() {
    }
}
