package com.navdy.hud.app.maps.notification;

import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.framework.notifications.INotification;

public abstract class BaseTrafficNotification implements INotification
{
    protected static final String EMPTY = "";
    public static final float IMAGE_SCALE = 0.5f;
    protected ChoiceLayout2 choiceLayout;
    protected INotificationController controller;
    protected Logger logger;
    
    public BaseTrafficNotification() {
        this.logger = new Logger(this.getClass());
    }
    
    protected void dismissNotification() {
        NotificationManager.getInstance().removeNotification(this.getId());
    }
    
    @Override
    public int getTimeout() {
        return 0;
    }
    
    @Override
    public IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onClick() {
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Override
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = false;
        if (this.choiceLayout != null) {
            switch (customKeyEvent) {
                case LEFT:
                    this.choiceLayout.moveSelectionLeft();
                    b = true;
                    break;
                case RIGHT:
                    this.choiceLayout.moveSelectionRight();
                    b = true;
                    break;
                case SELECT:
                    this.choiceLayout.executeSelectedItem();
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        this.controller = controller;
    }
    
    @Override
    public void onStop() {
        this.controller = null;
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public boolean supportScroll() {
        return false;
    }
}
