package com.navdy.hud.app.maps.notification;

import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.squareup.otto.Subscribe;
import android.view.View;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.navdy.hud.app.util.ImageUtil;
import android.os.SystemClock;
import android.graphics.Bitmap;
import com.here.android.mpa.common.Image;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.service.library.task.TaskManager;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.os.Looper;
import com.navdy.hud.app.maps.MapEvents;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class TrafficNotificationManager
{
    private static final TrafficNotificationManager sInstance;
    public static final Logger sLogger;
    private Bus bus;
    private Handler handler;
    private final int junctionViewH;
    private final int junctionViewInflateH;
    private final int junctionViewInflateW;
    private final int junctionViewW;
    private MapEvents.DisplayJunction lastJunctionEvent;
    
    static {
        sLogger = new Logger(TrafficNotificationManager.class);
        sInstance = new TrafficNotificationManager();
    }
    
    private TrafficNotificationManager() {
        this.handler = new Handler(Looper.getMainLooper());
        this.bus = RemoteDeviceManager.getInstance().getBus();
        final Resources resources = HudApplication.getAppContext().getResources();
        this.junctionViewW = resources.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_w);
        this.junctionViewH = resources.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_h);
        this.junctionViewInflateW = resources.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_inflate_w);
        this.junctionViewInflateH = resources.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_inflate_h);
        this.bus.register(this);
    }
    
    public static TrafficNotificationManager getInstance() {
        return TrafficNotificationManager.sInstance;
    }
    
    private void removeJunctionView() {
        this.handler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                try {
                    RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView().ejectRightSection();
                    TrafficNotificationManager.sLogger.i("junction view mode Ejected");
                }
                catch (Throwable t) {
                    TrafficNotificationManager.sLogger.e("junction view mode", t);
                }
            }
        });
    }
    
    @Subscribe
    public void onDisplayJunction(final MapEvents.DisplayJunction lastJunctionEvent) {
        try {
            TrafficNotificationManager.sLogger.v("onDisplayJunction");
            if (lastJunctionEvent.junction == null) {
                TrafficNotificationManager.sLogger.v("no junction image");
            }
            else {
                this.removeJunctionView();
                this.lastJunctionEvent = lastJunctionEvent;
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        HereMapUtil.loadImage(lastJunctionEvent.junction, TrafficNotificationManager.this.junctionViewInflateW, TrafficNotificationManager.this.junctionViewInflateH, (HereMapUtil.IImageLoadCallback)new HereMapUtil.IImageLoadCallback() {
                            @Override
                            public void result(final Image image, final Bitmap bitmap) {
                                Label_0013: {
                                    if (bitmap != null) {
                                        break Label_0013;
                                    }
                                    while (true) {
                                        try {
                                            TrafficNotificationManager.sLogger.w("junction view bitmap not loaded");
                                            Label_0012: {
                                                return;
                                            }
                                            // iftrue(Label_0156:, hueShift != null)
                                            // iftrue(Label_0012:, TrafficNotificationManager.access$200(this.this$1.this$0) == null)
                                        Block_6:
                                            while (true) {
                                                final long elapsedRealtime = SystemClock.elapsedRealtime();
                                                final Bitmap hueShift = ImageUtil.hueShift(bitmap, 90.0f);
                                                break Block_6;
                                                TrafficNotificationManager.sLogger.w("junction view bitmap loaded w=" + bitmap.getWidth() + " h=" + bitmap.getHeight() + " orig w=" + lastJunctionEvent.junction.getWidth() + " h=" + lastJunctionEvent.junction.getHeight());
                                                continue;
                                            }
                                            TrafficNotificationManager.sLogger.w("hue shift not performed");
                                            bitmap.recycle();
                                            return;
                                        }
                                        catch (Throwable t) {
                                            TrafficNotificationManager.sLogger.e("junction view mode", t);
                                            return;
                                        }
                                        return;
                                        final long elapsedRealtime2;
                                        Label_0156: {
                                            elapsedRealtime2 = SystemClock.elapsedRealtime();
                                        }
                                        final Bitmap applySaturation = ImageUtil.applySaturation(bitmap, 0.0f);
                                        Bitmap hueShift = null;
                                        if (applySaturation == null) {
                                            TrafficNotificationManager.sLogger.w("saturation not performed");
                                            bitmap.recycle();
                                            hueShift.recycle();
                                            return;
                                        }
                                        final long elapsedRealtime3 = SystemClock.elapsedRealtime();
                                        bitmap.recycle();
                                        final Bitmap blend = ImageUtil.blend(hueShift, applySaturation);
                                        applySaturation.recycle();
                                        hueShift.recycle();
                                        if (blend == null) {
                                            TrafficNotificationManager.sLogger.w("combine not performed");
                                            return;
                                        }
                                        final long elapsedRealtime4 = SystemClock.elapsedRealtime();
                                        long elapsedRealtime = 0L;
                                        TrafficNotificationManager.sLogger.i("junction view bitmap took[" + (elapsedRealtime4 - elapsedRealtime) + "] hue [" + (elapsedRealtime2 - elapsedRealtime) + "] sat[" + (elapsedRealtime3 - elapsedRealtime2) + "] blend[" + (elapsedRealtime4 - elapsedRealtime3) + "]");
                                        TrafficNotificationManager.this.handler.post((Runnable)new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    final HomeScreenView homescreenView = RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                                                    final View inflate = LayoutInflater.from(homescreenView.getContext()).inflate(R.layout.junction_view_lyt, (ViewGroup)null);
                                                    final ImageView imageView = (ImageView)inflate.findViewById(R.id.image);
                                                    final FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(TrafficNotificationManager.this.junctionViewW, TrafficNotificationManager.this.junctionViewH);
                                                    layoutParams.gravity = 83;
                                                    inflate.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
                                                    imageView.setImageBitmap(blend);
                                                    homescreenView.injectRightSection(inflate);
                                                    TrafficNotificationManager.sLogger.i("junction view mode injected");
                                                }
                                                catch (Throwable t) {
                                                    TrafficNotificationManager.sLogger.e("junction view mode", t);
                                                }
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    }
                }, 10);
            }
        }
        catch (Throwable t) {
            TrafficNotificationManager.sLogger.e(t);
        }
    }
    
    @Subscribe
    public void onHideJunctionSignPost(final MapEvents.HideSignPostJunction hideSignPostJunction) {
        TrafficNotificationManager.sLogger.v("onHideJunctionSignPost");
        this.lastJunctionEvent = null;
        this.removeJunctionView();
    }
    
    @Subscribe
    public void onLiveTrafficDismiss(final MapEvents.LiveTrafficDismissEvent liveTrafficDismissEvent) {
        NotificationManager.getInstance().removeNotification("navdy#traffic#event#notif");
    }
    
    @Subscribe
    public void onLiveTrafficEvent(final MapEvents.LiveTrafficEvent trafficEvent) {
        if (!GlanceHelper.isTrafficNotificationEnabled()) {
            TrafficNotificationManager.sLogger.v("traffic notification disabled:" + trafficEvent);
        }
        else {
            final NotificationManager instance = NotificationManager.getInstance();
            int n;
            if (instance.isNotificationPresent("navdy#traffic#reroute#notif") || instance.isNotificationPresent("navdy#traffic#delay#notif") || instance.isNotificationPresent("navdy#traffic#jam#notif")) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n != 0) {
                TrafficNotificationManager.sLogger.v("higher priority notification active ignore:" + trafficEvent.type.name());
            }
            else {
                TrafficEventNotification trafficEventNotification;
                if ((trafficEventNotification = (TrafficEventNotification)instance.getNotification("navdy#traffic#event#notif")) == null) {
                    trafficEventNotification = new TrafficEventNotification(this.bus);
                }
                trafficEventNotification.setTrafficEvent(trafficEvent);
                instance.addNotification(trafficEventNotification);
            }
        }
    }
    
    @Subscribe
    public void onTrafficDelayDismiss(final MapEvents.TrafficDelayDismissEvent trafficDelayDismissEvent) {
        NotificationManager.getInstance().removeNotification("navdy#traffic#delay#notif");
    }
    
    @Subscribe
    public void onTrafficDelayEvent(final MapEvents.TrafficDelayEvent trafficDelayEvent) {
    }
    
    @Subscribe
    public void onTrafficJamProgress(final MapEvents.TrafficJamProgressEvent trafficEvent) {
        if (!GlanceHelper.isTrafficNotificationEnabled()) {
            TrafficNotificationManager.sLogger.v("traffic notification disabled:" + trafficEvent);
        }
        else {
            final NotificationManager instance = NotificationManager.getInstance();
            if (instance.isNotificationPresent("navdy#traffic#reroute#notif")) {
                TrafficNotificationManager.sLogger.v("reroute notification active ignore jam event:" + trafficEvent.remainingTime);
            }
            else {
                instance.removeNotification("navdy#traffic#event#notif");
                instance.removeNotification("navdy#traffic#delay#notif");
                TrafficJamNotification trafficJamNotification;
                if ((trafficJamNotification = (TrafficJamNotification)instance.getNotification("navdy#traffic#jam#notif")) == null) {
                    trafficJamNotification = new TrafficJamNotification(this.bus, trafficEvent);
                }
                trafficJamNotification.setTrafficEvent(trafficEvent);
                instance.addNotification(trafficJamNotification);
            }
        }
    }
    
    @Subscribe
    public void onTrafficJamProgressDismiss(final MapEvents.TrafficJamDismissEvent trafficJamDismissEvent) {
        NotificationManager.getInstance().removeNotification("navdy#traffic#jam#notif");
    }
    
    @Subscribe
    public void onTrafficReroutePrompt(final MapEvents.TrafficRerouteEvent fasterRouteEvent) {
        if (!GlanceHelper.isTrafficNotificationEnabled()) {
            TrafficNotificationManager.sLogger.v("traffic notification disabled:" + fasterRouteEvent);
        }
        else {
            final NotificationManager instance = NotificationManager.getInstance();
            instance.removeNotification("navdy#traffic#event#notif");
            instance.removeNotification("navdy#traffic#delay#notif");
            instance.removeNotification("navdy#traffic#jam#notif");
            RouteTimeUpdateNotification routeTimeUpdateNotification;
            if ((routeTimeUpdateNotification = (RouteTimeUpdateNotification)instance.getNotification("navdy#traffic#reroute#notif")) == null) {
                routeTimeUpdateNotification = new RouteTimeUpdateNotification("navdy#traffic#reroute#notif", NotificationType.FASTER_ROUTE, this.bus);
            }
            routeTimeUpdateNotification.setFasterRouteEvent(fasterRouteEvent);
            instance.addNotification(routeTimeUpdateNotification);
        }
    }
    
    @Subscribe
    public void onTrafficReroutePromptDismiss(final MapEvents.TrafficRerouteDismissEvent trafficRerouteDismissEvent) {
        this.removeFasterRouteNotifiation();
    }
    
    public void removeFasterRouteNotifiation() {
        NotificationManager.getInstance().removeNotification("navdy#traffic#reroute#notif");
    }
}
