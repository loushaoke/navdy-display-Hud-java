package com.navdy.hud.app.maps.here;

import java.io.File;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.service.library.task.TaskManager;
import com.here.android.mpa.common.OnScreenCaptureListener;
import com.here.android.mpa.common.GeoCoordinate;
import android.graphics.Bitmap;
import com.navdy.hud.app.HudApplication;
import android.os.Looper;
import com.here.android.mpa.mapping.MapOffScreenRenderer;
import com.here.android.mpa.mapping.Map;
import android.os.Handler;
import com.navdy.service.library.log.Logger;

public class HereMapImageGenerator
{
    private static final Logger sLogger;
    private static final HereMapImageGenerator singleton;
    private Handler handler;
    private final Map map;
    private final MapOffScreenRenderer mapOffScreenRenderer;
    private boolean renderRunning;
    private Object waitForRender;
    
    static {
        sLogger = new Logger(HereMapImageGenerator.class);
        singleton = new HereMapImageGenerator();
    }
    
    private HereMapImageGenerator() {
        this.waitForRender = new Object();
        this.handler = new Handler(Looper.getMainLooper());
        this.map = new Map();
        this.mapOffScreenRenderer = new MapOffScreenRenderer(HudApplication.getAppContext());
        this.map.setMapScheme("terrain.day");
        this.map.setExtrudedBuildingsVisible(false);
        this.map.setFadingAnimations(false);
        this.map.getPositionIndicator().setVisible(false);
        this.mapOffScreenRenderer.setMap(this.map);
        this.mapOffScreenRenderer.setBlockingRendering(true);
    }
    
    private void generateMapSnapshotInternal(final MapGeneratorParams mapGeneratorParams) {
        boolean b2;
        final boolean b = b2 = false;
        try {
            final Logger sLogger = HereMapImageGenerator.sLogger;
            b2 = b;
            b2 = b;
            final StringBuilder sb = new StringBuilder();
            b2 = b;
            sLogger.e(sb.append("generateMapSnapshotInternal lat:").append(mapGeneratorParams.latitude).append(" lon:").append(mapGeneratorParams.longitude).toString());
            b2 = b;
            b2 = b;
            final GeoCoordinate geoCoordinate = new GeoCoordinate(mapGeneratorParams.latitude, mapGeneratorParams.longitude);
            b2 = b;
            this.map.setCenter(geoCoordinate, Map.Animation.NONE);
            b2 = b;
            this.mapOffScreenRenderer.setSize(mapGeneratorParams.width, mapGeneratorParams.height);
            if (!false) {
                b2 = true;
                this.mapOffScreenRenderer.start();
            }
            final boolean b3 = b2 = true;
            final MapOffScreenRenderer mapOffScreenRenderer = this.mapOffScreenRenderer;
            b2 = b3;
            b2 = b3;
            final OnScreenCaptureListener onScreenCaptureListener = new OnScreenCaptureListener() {
                @Override
                public void onScreenCaptured(final Bitmap bitmap) {
                    try {
                        HereMapImageGenerator.this.stopOffscreenRenderer();
                        HereMapImageGenerator.sLogger.e("onScreenCaptured bitmap:" + bitmap);
                        HereMapImageGenerator.this.saveFileToDisk(mapGeneratorParams, bitmap);
                    }
                    catch (Throwable t) {
                        HereMapImageGenerator.sLogger.e("onScreenCaptured", t);
                    }
                }
            };
            b2 = b3;
            mapOffScreenRenderer.getScreenCapture(onScreenCaptureListener);
        }
        catch (Throwable t) {
            HereMapImageGenerator.sLogger.e("renderMapSnapshot", t);
            if (b2) {
                this.stopOffscreenRenderer();
            }
        }
    }
    
    public static HereMapImageGenerator getInstance() {
        return HereMapImageGenerator.singleton;
    }
    
    private void notifyWaitingRenders() {
        final Object waitForRender = this.waitForRender;
        synchronized (waitForRender) {
            this.renderRunning = false;
            this.waitForRender.notifyAll();
        }
    }
    
    private void saveFileToDisk(final MapGeneratorParams mapGeneratorParams, final Bitmap bitmap) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                // 
                This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     1: astore_1       
                //     2: aload_1        
                //     3: astore_2       
                //     4: aload_0        
                //     5: getfield        com/navdy/hud/app/maps/here/HereMapImageGenerator$2.val$bitmap:Landroid/graphics/Bitmap;
                //     8: ifnull          174
                //    11: aload_1        
                //    12: astore_2       
                //    13: aload_0        
                //    14: getfield        com/navdy/hud/app/maps/here/HereMapImageGenerator$2.this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;
                //    17: aload_0        
                //    18: getfield        com/navdy/hud/app/maps/here/HereMapImageGenerator$2.val$mapGeneratorParams:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;
                //    21: getfield        com/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams.id:Ljava/lang/String;
                //    24: invokevirtual   com/navdy/hud/app/maps/here/HereMapImageGenerator.getMapImageFile:(Ljava/lang/String;)Ljava/lang/String;
                //    27: astore_3       
                //    28: aload_1        
                //    29: astore_2       
                //    30: new             Ljava/io/FileOutputStream;
                //    33: astore          4
                //    35: aload_1        
                //    36: astore_2       
                //    37: aload           4
                //    39: aload_3        
                //    40: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
                //    43: aload_0        
                //    44: getfield        com/navdy/hud/app/maps/here/HereMapImageGenerator$2.val$bitmap:Landroid/graphics/Bitmap;
                //    47: getstatic       android/graphics/Bitmap$CompressFormat.JPEG:Landroid/graphics/Bitmap$CompressFormat;
                //    50: bipush          100
                //    52: aload           4
                //    54: invokevirtual   android/graphics/Bitmap.compress:(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
                //    57: pop            
                //    58: aload           4
                //    60: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                //    63: aload           4
                //    65: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //    68: aconst_null    
                //    69: astore          4
                //    71: aload           4
                //    73: astore_2       
                //    74: invokestatic    com/navdy/hud/app/maps/here/HereMapImageGenerator.access$100:()Lcom/navdy/service/library/log/Logger;
                //    77: astore          5
                //    79: aload           4
                //    81: astore_2       
                //    82: new             Ljava/lang/StringBuilder;
                //    85: astore_1       
                //    86: aload           4
                //    88: astore_2       
                //    89: aload_1        
                //    90: invokespecial   java/lang/StringBuilder.<init>:()V
                //    93: aload           4
                //    95: astore_2       
                //    96: aload           5
                //    98: aload_1        
                //    99: ldc             "bitmap saved:"
                //   101: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   104: aload_3        
                //   105: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   108: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   111: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                //   114: aconst_null    
                //   115: astore          4
                //   117: aload           4
                //   119: astore_2       
                //   120: aload_0        
                //   121: getfield        com/navdy/hud/app/maps/here/HereMapImageGenerator$2.val$mapGeneratorParams:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams;
                //   124: getfield        com/navdy/hud/app/maps/here/HereMapImageGenerator$MapGeneratorParams.callback:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$ICallback;
                //   127: ifnull          165
                //   130: aload           4
                //   132: astore_2       
                //   133: aload_0        
                //   134: getfield        com/navdy/hud/app/maps/here/HereMapImageGenerator$2.this$0:Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;
                //   137: invokestatic    com/navdy/hud/app/maps/here/HereMapImageGenerator.access$300:(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator;)Landroid/os/Handler;
                //   140: astore_3       
                //   141: aload           4
                //   143: astore_2       
                //   144: new             Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2$1;
                //   147: astore_1       
                //   148: aload           4
                //   150: astore_2       
                //   151: aload_1        
                //   152: aload_0        
                //   153: invokespecial   com/navdy/hud/app/maps/here/HereMapImageGenerator$2$1.<init>:(Lcom/navdy/hud/app/maps/here/HereMapImageGenerator$2;)V
                //   156: aload           4
                //   158: astore_2       
                //   159: aload_3        
                //   160: aload_1        
                //   161: invokevirtual   android/os/Handler.post:(Ljava/lang/Runnable;)Z
                //   164: pop            
                //   165: aconst_null    
                //   166: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                //   169: aconst_null    
                //   170: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //   173: return         
                //   174: aload_1        
                //   175: astore_2       
                //   176: invokestatic    com/navdy/hud/app/maps/here/HereMapImageGenerator.access$100:()Lcom/navdy/service/library/log/Logger;
                //   179: ldc             "bitmap not saved:"
                //   181: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;)V
                //   184: goto            114
                //   187: aconst_null    
                //   188: astore          4
                //   190: astore_1       
                //   191: aload           4
                //   193: astore_2       
                //   194: invokestatic    com/navdy/hud/app/maps/here/HereMapImageGenerator.access$100:()Lcom/navdy/service/library/log/Logger;
                //   197: aload_1        
                //   198: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
                //   201: aload           4
                //   203: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                //   206: aload           4
                //   208: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //   211: goto            173
                //   214: astore_1       
                //   215: aload_2        
                //   216: astore          4
                //   218: aload           4
                //   220: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                //   223: aload           4
                //   225: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //   228: aload_1        
                //   229: athrow         
                //   230: astore_2       
                //   231: aload_2        
                //   232: astore_1       
                //   233: goto            218
                //   236: astore_1       
                //   237: goto            191
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                 
                //  -----  -----  -----  -----  ---------------------
                //  4      11     187    191    Ljava/lang/Throwable;
                //  4      11     214    218    Any
                //  13     28     187    191    Ljava/lang/Throwable;
                //  13     28     214    218    Any
                //  30     35     187    191    Ljava/lang/Throwable;
                //  30     35     214    218    Any
                //  37     43     187    191    Ljava/lang/Throwable;
                //  37     43     214    218    Any
                //  43     68     236    240    Ljava/lang/Throwable;
                //  43     68     230    236    Any
                //  74     79     187    191    Ljava/lang/Throwable;
                //  74     79     214    218    Any
                //  82     86     187    191    Ljava/lang/Throwable;
                //  82     86     214    218    Any
                //  89     93     187    191    Ljava/lang/Throwable;
                //  89     93     214    218    Any
                //  96     114    187    191    Ljava/lang/Throwable;
                //  96     114    214    218    Any
                //  120    130    187    191    Ljava/lang/Throwable;
                //  120    130    214    218    Any
                //  133    141    187    191    Ljava/lang/Throwable;
                //  133    141    214    218    Any
                //  144    148    187    191    Ljava/lang/Throwable;
                //  144    148    214    218    Any
                //  151    156    187    191    Ljava/lang/Throwable;
                //  151    156    214    218    Any
                //  159    165    187    191    Ljava/lang/Throwable;
                //  159    165    214    218    Any
                //  176    184    187    191    Ljava/lang/Throwable;
                //  176    184    214    218    Any
                //  194    201    214    218    Any
                // 
                // The error that occurred was:
                // 
                // java.util.ConcurrentModificationException
                //     at java.util.ArrayList$Itr.checkForComodification(Unknown Source)
                //     at java.util.ArrayList$Itr.next(Unknown Source)
                //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2863)
                //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2445)
                //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Unknown Source)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        }, 1);
    }
    
    private void stopOffscreenRenderer() {
        try {
            this.mapOffScreenRenderer.stop();
        }
        catch (Throwable t) {
            HereMapImageGenerator.sLogger.e(t);
            this.notifyWaitingRenders();
        }
        finally {
            this.notifyWaitingRenders();
        }
    }
    
    public void generateMapSnapshot(final MapGeneratorParams mapGeneratorParams) {
        if (mapGeneratorParams == null) {
            throw new IllegalArgumentException();
        }
        if (!HereMapsManager.getInstance().isInitialized()) {
            HereMapImageGenerator.sLogger.e("map engine not initialized");
            if (mapGeneratorParams.callback != null) {
                mapGeneratorParams.callback.result(null);
            }
        }
    }
    
    public String getMapImageFile(final String s) {
        return DriverProfileHelper.getInstance().getCurrentProfile().getPlacesImageDir().getAbsolutePath() + File.separator + s + ".jpg";
    }
    
    public interface ICallback
    {
        void result(final Bitmap p0);
    }
    
    public static class MapGeneratorParams
    {
        public ICallback callback;
        public int height;
        public String id;
        public double latitude;
        public double longitude;
        public int width;
        public double zoomLevel;
    }
}
