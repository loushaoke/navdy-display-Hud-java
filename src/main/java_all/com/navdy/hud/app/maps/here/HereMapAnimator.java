package com.navdy.hud.app.maps.here;

import com.here.android.mpa.mapping.Map;
import com.navdy.service.library.task.TaskManager;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import com.here.android.mpa.common.GeoCoordinate;
import android.support.annotation.Nullable;
import com.navdy.hud.app.maps.MapSettings;
import com.here.android.mpa.mapping.OnMapRenderListener;
import com.here.android.mpa.common.GeoPosition;
import com.navdy.service.library.log.Logger;

public class HereMapAnimator
{
    private static final int DRASTIC_HEADING_CHANGE = 45;
    private static final int INVALID_STEP = -1;
    private static final int INVALID_TILT = -1;
    private static final int INVALID_ZOOM = -1;
    private static final int MAX_DIFFERENCE_HEADING_FILTER = 120;
    private static final int MAX_HEADING = 360;
    private static final long MAX_INTERVAL_HEADING_UPDATE = 3000L;
    private static final int MIN_HEADING = 0;
    private static final double MIN_SPEED_FILTER = 0.44704;
    private static final double MPH_TO_MS = 0.44704;
    private static final long REFRESH_TIME_THROTTLE;
    private static final Object geoPositionLock;
    private static final Logger logger;
    private static final Object tiltLock;
    private static final Object zoomLock;
    private GeoPosition currentGeoPosition;
    private float currentTilt;
    private double currentZoom;
    private long geoPositionUpdateInterval;
    private long lastGeoPositionUpdateTime;
    private volatile float lastHeading;
    private long lastPreDrawTime;
    private long lastTiltUpdateTime;
    private long lastZoomUpdateTime;
    private HereMapController mapController;
    private OnMapRenderListener mapRenderListener;
    private AnimationMode mode;
    private HereNavController navController;
    private long preDrawFinishTime;
    private volatile boolean renderingEnabled;
    private long tiltUpdateInterval;
    private long zoomUpdateInterval;
    
    static {
        logger = new Logger(HereMapAnimator.class);
        REFRESH_TIME_THROTTLE = 1000 / MapSettings.getMapFps();
        geoPositionLock = new Object();
        zoomLock = new Object();
        tiltLock = new Object();
    }
    
    HereMapAnimator(final AnimationMode mode, final HereMapController mapController, final HereNavController navController) {
        this.mode = AnimationMode.NONE;
        this.lastHeading = -1.0f;
        this.renderingEnabled = true;
        this.mode = mode;
        if (mode != AnimationMode.NONE) {
            HereMapAnimator.logger.v("animation throttle = " + HereMapAnimator.REFRESH_TIME_THROTTLE);
            this.mapRenderListener = new OnMapRenderListener() {
                @Override
                public void onGraphicsDetached() {
                }
                
                @Override
                public void onPostDraw(final boolean b, final long n) {
                    HereMapAnimator.this.onPostDraw(b, n);
                }
                
                @Override
                public void onPreDraw() {
                    HereMapAnimator.this.onPreDraw();
                }
                
                @Override
                public void onRenderBufferCreated() {
                }
                
                @Override
                public void onSizeChanged(final int n, final int n2) {
                }
            };
        }
        this.mapController = mapController;
        this.navController = navController;
        this.currentGeoPosition = null;
        this.currentZoom = mapController.getZoomLevel();
        this.currentTilt = mapController.getTilt();
    }
    
    private GeoCoordinate getNewCenter(@Nullable final GeoPosition geoPosition, final double n) {
        GeoCoordinate center;
        final GeoCoordinate geoCoordinate = center = this.mapController.getCenter();
        if (geoPosition != null) {
            if (n == -1.0) {
                center = geoCoordinate;
            }
            else {
                final GeoCoordinate coordinate = geoPosition.getCoordinate();
                center = new GeoCoordinate(geoCoordinate.getLatitude() + (coordinate.getLatitude() - geoCoordinate.getLatitude()) * n, geoCoordinate.getLongitude() + (coordinate.getLongitude() - geoCoordinate.getLongitude()) * n);
            }
        }
        return center;
    }
    
    private float getNewHeading(@Nullable final GeoPosition geoPosition, double n) {
        float orientation;
        final float n2 = orientation = this.mapController.getOrientation();
        if (geoPosition != null) {
            if (n == -1.0) {
                orientation = n2;
            }
            else {
                final double n3 = n2 + n * this.getOrientationDiff((float)geoPosition.getHeading(), n2);
                if (n3 > 360.0) {
                    n = n3 - 360.0;
                }
                else {
                    n = n3;
                    if (n3 < 0.0) {
                        n = n3 + 360.0;
                    }
                }
                orientation = (float)n;
            }
        }
        return orientation;
    }
    
    private float getNewTilt(final float n, final double n2) {
        float n4;
        final float n3 = n4 = -1.0f;
        if (this.mode == AnimationMode.ZOOM_TILT_ANIMATION) {
            n4 = n3;
            if (n != -1.0f) {
                if (n2 == -1.0) {
                    n4 = n3;
                }
                else {
                    final float tilt = this.mapController.getTilt();
                    n4 = (float)(tilt + (n - tilt) * n2);
                    HereMapAnimator.logger.v("animatorTilt: " + n4);
                }
            }
        }
        return n4;
    }
    
    private double getNewZoomLevel(final double n, final double n2) {
        double n4;
        final double n3 = n4 = -1.0;
        if (this.mode == AnimationMode.ZOOM_TILT_ANIMATION) {
            n4 = n3;
            if (n != -1.0) {
                if (n2 == -1.0) {
                    n4 = n3;
                }
                else {
                    final double zoomLevel = this.mapController.getZoomLevel();
                    n4 = zoomLevel + (n - zoomLevel) * n2;
                    HereMapAnimator.logger.v("animatorZoom: " + n4);
                }
            }
        }
        return n4;
    }
    
    private double getOrientationDiff(double n, double n2) {
        n2 = n - n2;
        if (n2 > 180.0) {
            n = n2 - 360.0;
        }
        else {
            n = n2;
            if (n2 < -180.0) {
                n = n2 + 360.0;
            }
        }
        return n;
    }
    
    private boolean isValidGeoPosition(@NonNull final GeoPosition geoPosition) {
        while (true) {
            final boolean b = true;
            final Object geoPositionLock = HereMapAnimator.geoPositionLock;
            final GeoPosition currentGeoPosition;
            final long geoPositionUpdateInterval;
            synchronized (geoPositionLock) {
                currentGeoPosition = this.currentGeoPosition;
                geoPositionUpdateInterval = this.geoPositionUpdateInterval;
                // monitorexit(geoPositionLock)
                if (currentGeoPosition == null) {
                    return b;
                }
            }
            final GeoPosition geoPosition2;
            final double orientationDiff = this.getOrientationDiff(geoPosition2.getHeading(), currentGeoPosition.getHeading());
            boolean b2 = b;
            if (geoPosition2.getSpeed() >= 0.44704) {
                return b2;
            }
            b2 = b;
            if (Math.abs(orientationDiff) <= 120.0) {
                return b2;
            }
            b2 = b;
            if (geoPositionUpdateInterval < 3000L) {
                b2 = false;
                return b2;
            }
            return b2;
        }
    }
    
    private boolean isValidTilt(final float n) {
        return n >= 0.0f && n < 90.0f;
    }
    
    private boolean isValidZoom(final double n) {
        return n >= 0.0 && n <= 20.0;
    }
    
    private void onPostDraw(final boolean b, long n) {
        final long n2 = SystemClock.elapsedRealtime() - this.preDrawFinishTime;
        n = HereMapAnimator.REFRESH_TIME_THROTTLE - n2;
        if (HereMapAnimator.logger.isLoggable(2)) {
            HereMapAnimator.logger.v("last render took " + n2 + " ms");
        }
        Label_0126: {
            if (n <= 0L) {
                break Label_0126;
            }
            if (HereMapAnimator.logger.isLoggable(2)) {
                HereMapAnimator.logger.v("sleeping render thread for " + n + " ms");
            }
            try {
                Thread.sleep(n);
                return;
            }
            catch (InterruptedException ex) {
                HereMapAnimator.logger.e(ex);
                return;
            }
        }
        if (HereMapAnimator.logger.isLoggable(2)) {
            HereMapAnimator.logger.v("render thread could not sleep");
        }
    }
    
    private void onPreDraw() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
        //     4: ifnonnull       8
        //     7: return         
        //     8: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //    11: lstore_1       
        //    12: aload_0        
        //    13: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.lastPreDrawTime:J
        //    16: lconst_0       
        //    17: lcmp           
        //    18: ifle            263
        //    21: lload_1        
        //    22: aload_0        
        //    23: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.lastPreDrawTime:J
        //    26: lsub           
        //    27: lstore_3       
        //    28: aload_0        
        //    29: lload_1        
        //    30: putfield        com/navdy/hud/app/maps/here/HereMapAnimator.lastPreDrawTime:J
        //    33: aconst_null    
        //    34: astore          5
        //    36: ldc2_w          -1.0
        //    39: dstore          6
        //    41: ldc             -1.0
        //    43: fstore          8
        //    45: ldc2_w          -1.0
        //    48: dstore          9
        //    50: ldc2_w          -1.0
        //    53: dstore          11
        //    55: ldc2_w          -1.0
        //    58: dstore          13
        //    60: getstatic       com/navdy/hud/app/maps/here/HereMapAnimator.geoPositionLock:Ljava/lang/Object;
        //    63: astore          15
        //    65: aload           15
        //    67: dup            
        //    68: astore          16
        //    70: monitorenter   
        //    71: aload_0        
        //    72: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.geoPositionUpdateInterval:J
        //    75: lstore          17
        //    77: aload_0        
        //    78: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.currentGeoPosition:Lcom/here/android/mpa/common/GeoPosition;
        //    81: astore          19
        //    83: aload           16
        //    85: monitorexit    
        //    86: getstatic       com/navdy/hud/app/maps/here/HereMapAnimator.zoomLock:Ljava/lang/Object;
        //    89: astore          15
        //    91: aload           15
        //    93: dup            
        //    94: astore          16
        //    96: monitorenter   
        //    97: aload_0        
        //    98: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.zoomUpdateInterval:J
        //   101: lstore          20
        //   103: aload_0        
        //   104: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.currentZoom:D
        //   107: dstore          22
        //   109: aload           16
        //   111: monitorexit    
        //   112: getstatic       com/navdy/hud/app/maps/here/HereMapAnimator.tiltLock:Ljava/lang/Object;
        //   115: astore          15
        //   117: aload           15
        //   119: dup            
        //   120: astore          16
        //   122: monitorenter   
        //   123: aload_0        
        //   124: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.tiltUpdateInterval:J
        //   127: lstore          24
        //   129: aload_0        
        //   130: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.currentTilt:F
        //   133: fstore          26
        //   135: aload           16
        //   137: monitorexit    
        //   138: aload           19
        //   140: ifnull          295
        //   143: lload           17
        //   145: lconst_0       
        //   146: lcmp           
        //   147: ifle            295
        //   150: iconst_1       
        //   151: istore          27
        //   153: dload           22
        //   155: dconst_0       
        //   156: dcmpl          
        //   157: iflt            301
        //   160: lload           20
        //   162: lconst_0       
        //   163: lcmp           
        //   164: ifle            301
        //   167: iconst_1       
        //   168: istore          28
        //   170: fload           26
        //   172: fconst_0       
        //   173: fcmpl          
        //   174: iflt            307
        //   177: lload           24
        //   179: lconst_0       
        //   180: lcmp           
        //   181: ifle            307
        //   184: iconst_1       
        //   185: istore          29
        //   187: iload           27
        //   189: ifeq            204
        //   192: aload           19
        //   194: astore          5
        //   196: lload_3        
        //   197: l2d            
        //   198: lload           17
        //   200: l2d            
        //   201: ddiv           
        //   202: dstore          9
        //   204: iload           28
        //   206: ifeq            221
        //   209: dload           22
        //   211: dstore          6
        //   213: lload_3        
        //   214: l2d            
        //   215: lload           20
        //   217: l2d            
        //   218: ddiv           
        //   219: dstore          11
        //   221: iload           29
        //   223: ifeq            238
        //   226: fload           26
        //   228: fstore          8
        //   230: lload_3        
        //   231: l2d            
        //   232: lload           24
        //   234: l2d            
        //   235: ddiv           
        //   236: dstore          13
        //   238: iload           27
        //   240: ifne            313
        //   243: iload           28
        //   245: ifne            313
        //   248: iload           29
        //   250: ifne            313
        //   253: aload_0        
        //   254: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //   257: putfield        com/navdy/hud/app/maps/here/HereMapAnimator.preDrawFinishTime:J
        //   260: goto            7
        //   263: aload_0        
        //   264: lload_1        
        //   265: putfield        com/navdy/hud/app/maps/here/HereMapAnimator.lastPreDrawTime:J
        //   268: goto            7
        //   271: astore          5
        //   273: aload           16
        //   275: monitorexit    
        //   276: aload           5
        //   278: athrow         
        //   279: astore          5
        //   281: aload           16
        //   283: monitorexit    
        //   284: aload           5
        //   286: athrow         
        //   287: astore          5
        //   289: aload           16
        //   291: monitorexit    
        //   292: aload           5
        //   294: athrow         
        //   295: iconst_0       
        //   296: istore          27
        //   298: goto            153
        //   301: iconst_0       
        //   302: istore          28
        //   304: goto            170
        //   307: iconst_0       
        //   308: istore          29
        //   310: goto            187
        //   313: dload           9
        //   315: dload           11
        //   317: dload           13
        //   319: invokestatic    java/lang/Math.max:(DD)D
        //   322: invokestatic    java/lang/Math.max:(DD)D
        //   325: dstore          11
        //   327: dload           11
        //   329: dstore          9
        //   331: dload           11
        //   333: dconst_1       
        //   334: dcmpl          
        //   335: ifle            341
        //   338: dconst_1       
        //   339: dstore          9
        //   341: aload_0        
        //   342: aload           5
        //   344: dload           9
        //   346: invokespecial   com/navdy/hud/app/maps/here/HereMapAnimator.getNewCenter:(Lcom/here/android/mpa/common/GeoPosition;D)Lcom/here/android/mpa/common/GeoCoordinate;
        //   349: astore          19
        //   351: aload_0        
        //   352: aload           5
        //   354: dload           9
        //   356: invokespecial   com/navdy/hud/app/maps/here/HereMapAnimator.getNewHeading:(Lcom/here/android/mpa/common/GeoPosition;D)F
        //   359: fstore          26
        //   361: aload_0        
        //   362: dload           6
        //   364: dload           9
        //   366: invokespecial   com/navdy/hud/app/maps/here/HereMapAnimator.getNewZoomLevel:(DD)D
        //   369: dstore          6
        //   371: aload_0        
        //   372: fload           8
        //   374: dload           9
        //   376: invokespecial   com/navdy/hud/app/maps/here/HereMapAnimator.getNewTilt:(FD)F
        //   379: fstore          8
        //   381: aload_0        
        //   382: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.renderingEnabled:Z
        //   385: ifeq            419
        //   388: aload_0        
        //   389: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //   392: invokevirtual   com/navdy/hud/app/maps/here/HereMapController.getState:()Lcom/navdy/hud/app/maps/here/HereMapController$State;
        //   395: getstatic       com/navdy/hud/app/maps/here/HereMapController$State.AR_MODE:Lcom/navdy/hud/app/maps/here/HereMapController$State;
        //   398: if_acmpne       419
        //   401: aload_0        
        //   402: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.mapController:Lcom/navdy/hud/app/maps/here/HereMapController;
        //   405: aload           19
        //   407: getstatic       com/here/android/mpa/mapping/Map$Animation.NONE:Lcom/here/android/mpa/mapping/Map$Animation;
        //   410: dload           6
        //   412: fload           26
        //   414: fload           8
        //   416: invokevirtual   com/navdy/hud/app/maps/here/HereMapController.setCenter:(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;DFF)V
        //   419: aload_0        
        //   420: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //   423: putfield        com/navdy/hud/app/maps/here/HereMapAnimator.preDrawFinishTime:J
        //   426: getstatic       com/navdy/hud/app/maps/here/HereMapAnimator.logger:Lcom/navdy/service/library/log/Logger;
        //   429: iconst_2       
        //   430: invokevirtual   com/navdy/service/library/log/Logger.isLoggable:(I)Z
        //   433: ifeq            7
        //   436: getstatic       com/navdy/hud/app/maps/here/HereMapAnimator.logger:Lcom/navdy/service/library/log/Logger;
        //   439: new             Ljava/lang/StringBuilder;
        //   442: dup            
        //   443: invokespecial   java/lang/StringBuilder.<init>:()V
        //   446: ldc_w           "predraw logic took "
        //   449: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   452: aload_0        
        //   453: getfield        com/navdy/hud/app/maps/here/HereMapAnimator.preDrawFinishTime:J
        //   456: lload_1        
        //   457: lsub           
        //   458: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   461: ldc_w           " ms"
        //   464: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   467: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   470: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
        //   473: goto            7
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  71     86     271    279    Any
        //  97     112    279    287    Any
        //  123    138    287    295    Any
        //  273    276    271    279    Any
        //  281    284    279    287    Any
        //  289    292    287    295    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index: 242, Size: 242
        //     at java.util.ArrayList.rangeCheck(Unknown Source)
        //     at java.util.ArrayList.get(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void clearState() {
        HereMapAnimator.logger.v("clearState");
        this.lastHeading = -1.0f;
    }
    
    public AnimationMode getAnimationMode() {
        return this.mode;
    }
    
    public OnMapRenderListener getMapRenderListener() {
        return this.mapRenderListener;
    }
    
    public void setGeoPosition(@NonNull final GeoPosition currentGeoPosition) {
        switch (this.mode) {
            default:
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        long n = 0L;
                        if (HereMapAnimator.this.lastGeoPositionUpdateTime > 0L) {
                            n = SystemClock.elapsedRealtime() - HereMapAnimator.this.lastGeoPositionUpdateTime;
                        }
                        if (!HereMapAnimator.this.isValidGeoPosition(currentGeoPosition)) {
                            HereMapAnimator.logger.v("filtering out spurious heading: " + currentGeoPosition.getHeading() + " speed:" + currentGeoPosition.getSpeed());
                        }
                        else {
                            HereMapAnimator.this.lastGeoPositionUpdateTime = SystemClock.elapsedRealtime();
                            final Object access$500 = HereMapAnimator.geoPositionLock;
                            synchronized (access$500) {
                                HereMapAnimator.this.geoPositionUpdateInterval = n;
                                final GeoPosition access$501 = HereMapAnimator.this.currentGeoPosition;
                                HereMapAnimator.this.currentGeoPosition = currentGeoPosition;
                                // monitorexit(access$500)
                                if (access$501 == null && HereMapAnimator.this.renderingEnabled) {
                                    HereMapAnimator.logger.v("initial setCenter");
                                    HereMapAnimator.this.mapController.setCenter(HereMapAnimator.this.currentGeoPosition.getCoordinate(), Map.Animation.NONE, -1.0, (float)HereMapAnimator.this.currentGeoPosition.getHeading(), -1.0f);
                                }
                            }
                        }
                    }
                }, 17);
                break;
            case NONE: {
                final double heading = currentGeoPosition.getHeading();
                boolean b = false;
                if (this.lastHeading != -1.0f) {
                    final double orientationDiff = this.getOrientationDiff(heading, this.lastHeading);
                    final double speed = currentGeoPosition.getSpeed();
                    if (speed <= 0.44704 && Math.abs(orientationDiff) >= 120.0) {
                        HereMapAnimator.logger.v("filtering out spurious heading last: " + this.lastHeading + " new:" + heading + " speed:" + speed);
                        break;
                    }
                    b = b;
                    if (Math.abs(orientationDiff) >= 45.0) {
                        b = true;
                    }
                }
                this.currentGeoPosition = currentGeoPosition;
                this.lastHeading = (float)heading;
                if (this.renderingEnabled && this.mapController.getState() == HereMapController.State.AR_MODE) {
                    final HereMapController mapController = this.mapController;
                    final GeoCoordinate coordinate = currentGeoPosition.getCoordinate();
                    Map.Animation animation;
                    if (b) {
                        animation = Map.Animation.BOW;
                    }
                    else {
                        animation = Map.Animation.NONE;
                    }
                    mapController.setCenter(coordinate, animation, -1.0, this.lastHeading, -1.0f);
                    break;
                }
                break;
            }
        }
    }
    
    void setTilt(final float n) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                long n = 0L;
                if (HereMapAnimator.this.lastTiltUpdateTime > 0L) {
                    n = SystemClock.elapsedRealtime() - HereMapAnimator.this.lastTiltUpdateTime;
                }
                if (!HereMapAnimator.this.isValidTilt(n)) {
                    HereMapAnimator.logger.v("filtering out spurious tilt: " + n);
                }
                else {
                    HereMapAnimator.this.lastTiltUpdateTime = SystemClock.elapsedRealtime();
                    final Object access$1700 = HereMapAnimator.tiltLock;
                    synchronized (access$1700) {
                        HereMapAnimator.this.tiltUpdateInterval = n;
                        HereMapAnimator.this.currentTilt = n;
                    }
                }
            }
        }, 17);
    }
    
    void setZoom(final double n) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                long n = 0L;
                if (HereMapAnimator.this.lastZoomUpdateTime > 0L) {
                    n = SystemClock.elapsedRealtime() - HereMapAnimator.this.lastZoomUpdateTime;
                }
                if (!HereMapAnimator.this.isValidZoom(n)) {
                    HereMapAnimator.logger.v("filtering out spurious zoom: " + n);
                }
                else {
                    HereMapAnimator.this.lastZoomUpdateTime = SystemClock.elapsedRealtime();
                    final Object access$1200 = HereMapAnimator.zoomLock;
                    synchronized (access$1200) {
                        HereMapAnimator.this.zoomUpdateInterval = n;
                        HereMapAnimator.this.currentZoom = n;
                    }
                }
            }
        }, 17);
    }
    
    public void startMapRendering() {
        HereMapAnimator.logger.v("startMapRendering: rendering enabled");
        this.renderingEnabled = true;
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final GeoPosition access$700 = HereMapAnimator.this.currentGeoPosition;
                if (access$700 != null) {
                    if (HereMapAnimator.this.mode == AnimationMode.NONE) {
                        HereMapAnimator.this.setGeoPosition(access$700);
                        HereMapAnimator.logger.v("rendering: set last pos");
                    }
                    else {
                        HereMapAnimator.logger.v("rendering: set center");
                        HereMapAnimator.this.mapController.setCenter(access$700.getCoordinate(), Map.Animation.NONE, -1.0, (float)access$700.getHeading(), -1.0f);
                    }
                }
            }
        }, 17);
    }
    
    public void stopMapRendering() {
        HereMapAnimator.logger.v("stopMapRendering: rendering disabled");
        this.renderingEnabled = false;
    }
    
    enum AnimationMode
    {
        ANIMATION, 
        NONE, 
        ZOOM_TILT_ANIMATION;
    }
}
