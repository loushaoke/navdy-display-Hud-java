package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.storage.PathManager;
import java.util.Iterator;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import com.navdy.hud.app.HudApplication;
import android.text.TextUtils;
import java.util.List;
import com.navdy.service.library.log.Logger;

class HereMapsConfigurator
{
    private static final HereMapsConfigurator sInstance;
    private static final Logger sLogger;
    private volatile boolean isAlreadyConfigured;
    
    static {
        sLogger = new Logger(HereMapsConfigurator.class);
        sInstance = new HereMapsConfigurator();
    }
    
    public static HereMapsConfigurator getInstance() {
        return HereMapsConfigurator.sInstance;
    }
    
    private void removeOldHereMapsFolders(final List<String> list, final String s) {
        for (final String s2 : list) {
            if (!TextUtils.equals((CharSequence)s2, (CharSequence)s)) {
                IOUtils.deleteDirectory(HudApplication.getAppContext(), new File(s2));
            }
        }
    }
    
    void removeMWConfigFolder() {
        IOUtils.deleteDirectory(HudApplication.getAppContext(), new File(PathManager.getInstance().getLatestHereMapsConfigPath()));
    }
    
    void updateMapsConfig() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: astore_1       
        //     3: monitorenter   
        //     4: aload_0        
        //     5: getfield        com/navdy/hud/app/maps/here/HereMapsConfigurator.isAlreadyConfigured:Z
        //     8: ifeq            22
        //    11: getstatic       com/navdy/hud/app/maps/here/HereMapsConfigurator.sLogger:Lcom/navdy/service/library/log/Logger;
        //    14: ldc             "MWConfig is already configured"
        //    16: invokevirtual   com/navdy/service/library/log/Logger.w:(Ljava/lang/String;)V
        //    19: aload_1        
        //    20: monitorexit    
        //    21: return         
        //    22: invokestatic    com/navdy/hud/app/util/GenericUtil.checkNotOnMainThread:()V
        //    25: invokestatic    com/navdy/hud/app/HudApplication.getAppContext:()Landroid/content/Context;
        //    28: astore_2       
        //    29: invokestatic    com/navdy/hud/app/storage/PathManager.getInstance:()Lcom/navdy/hud/app/storage/PathManager;
        //    32: invokevirtual   com/navdy/hud/app/storage/PathManager.getLatestHereMapsConfigPath:()Ljava/lang/String;
        //    35: astore_3       
        //    36: getstatic       com/navdy/hud/app/maps/here/HereMapsConfigurator.sLogger:Lcom/navdy/service/library/log/Logger;
        //    39: ldc             "MWConfig: starting"
        //    41: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //    44: invokestatic    com/navdy/hud/app/storage/PathManager.getInstance:()Lcom/navdy/hud/app/storage/PathManager;
        //    47: invokevirtual   com/navdy/hud/app/storage/PathManager.getHereMapsConfigDirs:()Ljava/util/List;
        //    50: astore          4
        //    52: new             Lcom/navdy/hud/app/util/PackagedResource;
        //    55: astore          5
        //    57: aload           5
        //    59: ldc             "mwconfig_client"
        //    61: aload_3        
        //    62: invokespecial   com/navdy/hud/app/util/PackagedResource.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //    65: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //    68: lstore          6
        //    70: aload           5
        //    72: aload_2        
        //    73: ldc             R.raw.mwconfig_hud
        //    75: ldc             R.raw.mwconfig_hud_md5
        //    77: invokevirtual   com/navdy/hud/app/util/PackagedResource.updateFromResources:(Landroid/content/Context;II)V
        //    80: aload_0        
        //    81: aload           4
        //    83: aload_3        
        //    84: invokespecial   com/navdy/hud/app/maps/here/HereMapsConfigurator.removeOldHereMapsFolders:(Ljava/util/List;Ljava/lang/String;)V
        //    87: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //    90: lstore          8
        //    92: getstatic       com/navdy/hud/app/maps/here/HereMapsConfigurator.sLogger:Lcom/navdy/service/library/log/Logger;
        //    95: astore          5
        //    97: new             Ljava/lang/StringBuilder;
        //   100: astore          4
        //   102: aload           4
        //   104: invokespecial   java/lang/StringBuilder.<init>:()V
        //   107: aload           5
        //   109: aload           4
        //   111: ldc             "MWConfig: time took "
        //   113: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   116: lload           8
        //   118: lload           6
        //   120: lsub           
        //   121: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   124: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   127: invokevirtual   com/navdy/service/library/log/Logger.i:(Ljava/lang/String;)V
        //   130: aload_0        
        //   131: iconst_1       
        //   132: putfield        com/navdy/hud/app/maps/here/HereMapsConfigurator.isAlreadyConfigured:Z
        //   135: aload_2        
        //   136: aload_3        
        //   137: ldc             R.raw.here_mwconfig_integrity
        //   139: invokestatic    com/navdy/service/library/util/IOUtils.checkIntegrity:(Landroid/content/Context;Ljava/lang/String;I)V
        //   142: goto            19
        //   145: astore_3       
        //   146: aload_1        
        //   147: monitorexit    
        //   148: aload_3        
        //   149: athrow         
        //   150: astore          4
        //   152: getstatic       com/navdy/hud/app/maps/here/HereMapsConfigurator.sLogger:Lcom/navdy/service/library/log/Logger;
        //   155: ldc             "MWConfig error"
        //   157: aload           4
        //   159: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   162: goto            135
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  4      19     145    150    Any
        //  22     65     145    150    Any
        //  65     135    150    165    Ljava/lang/Throwable;
        //  65     135    145    150    Any
        //  135    142    145    150    Any
        //  152    162    145    150    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0135:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
