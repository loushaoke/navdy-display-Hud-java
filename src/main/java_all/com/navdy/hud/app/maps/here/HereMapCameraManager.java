package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.event.DriverProfileChanged;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.profile.DriverProfile;
import com.squareup.otto.Bus;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.here.android.mpa.mapping.Map;
import android.os.Looper;
import android.os.SystemClock;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.ui.component.homescreen.NavigationView;
import com.navdy.service.library.events.preferences.LocalPreferences;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import android.os.Handler;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.service.library.log.Logger;

public final class HereMapCameraManager
{
    public static final float DEFAULT_TILT = 60.0f;
    public static final double DEFAULT_ZOOM_LEVEL = 16.5;
    private static final int EASE_OUT_DYNAMIC_ZOOM_TILT_INTERVAL = 10000;
    private static final double HIGH_SPEED_THRESHOLD = 29.0576;
    private static final float HIGH_SPEED_TILT = 70.0f;
    private static final double HIGH_SPEED_ZOOM = 15.25;
    private static final double LOW_SPEED_THRESHOLD = 8.9408;
    private static final float LOW_SPEED_TILT = 60.0f;
    private static final double LOW_SPEED_ZOOM = 16.5;
    private static final double MAX_ZOOM_LEVEL_DIAL = 16.5;
    private static final double MEDIUM_SPEED_THRESHOLD = 20.1168;
    private static final double MEDIUM_SPEED_ZOOM = 16.0;
    private static final double MIN_SPEED_DIFFERENCE = 1.5;
    private static final float MIN_TILT_DIFFERENCE_TRIGGER = 5.0f;
    private static final double MIN_ZOOM_DIFFERENCE_TRIGGER = 0.25;
    public static final double MIN_ZOOM_LEVEL_DIAL = 12.0;
    private static final double MPH_TO_MS = 0.44704;
    private static final int N_ZOOM_TILT_ANIMATION_STEPS = 5;
    private static final float OVERVIEW_MAP_ZOOM_LEVEL = 10000.0f;
    private static final double VERY_HIGH_SPEED_THRESHOLD = 33.528;
    private static final double VERY_HIGH_SPEED_ZOOM = 14.75;
    private static final long ZOOM_LOCK_DURATION = 10000L;
    private static final double ZOOM_STEP = 0.25;
    private static final int ZOOM_TILT_ANIMATION_INTERVAL = 200;
    private static HereMapCameraManager sInstance;
    private static final Logger sLogger;
    private volatile boolean animationOn;
    private float currentDynamicTilt;
    private double currentDynamicZoom;
    private GeoCoordinate currentGeoCoordinate;
    private GeoPosition currentGeoPosition;
    private Runnable easeOutDynamicZoomTilt;
    private Runnable easeOutDynamicZoomTiltBk;
    private volatile boolean goBackfromOverviewOn;
    private volatile boolean goToOverviewOn;
    private final Handler handler;
    private HereMapAnimator hereMapAnimator;
    private final HereNavigationManager hereNavigationManager;
    private HomeScreenView homeScreenView;
    private boolean init;
    private boolean initialZoom;
    private boolean isFullAnimationSettingEnabled;
    private long lastDynamicZoomTime;
    private double lastRecordedSpeed;
    private LocalPreferences localPreferences;
    private HereMapController mapController;
    private boolean mapUIReady;
    private volatile int nTimesAnimated;
    private NavigationView navigationView;
    private SpeedManager speedManager;
    private Runnable unlockZoom;
    private volatile boolean zoomActionOn;
    
    static {
        sLogger = new Logger(HereMapCameraManager.class);
        HereMapCameraManager.sInstance = new HereMapCameraManager();
    }
    
    private HereMapCameraManager() {
        this.unlockZoom = new Runnable() {
            @Override
            public void run() {
                HereMapCameraManager.this.handler.removeCallbacks(HereMapCameraManager.this.easeOutDynamicZoomTilt);
                HereMapCameraManager.this.zoomActionOn = false;
                if (HereMapCameraManager.this.mapController.getState() == HereMapController.State.ROUTE_PICKER) {
                    HereMapCameraManager.sLogger.v("unlocking: in route search, no-op");
                }
                else {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            if (!HereMapCameraManager.this.isManualZoom()) {
                                HereMapCameraManager.sLogger.v("unlocking dial zoom:auto");
                                HereMapCameraManager.this.zoomToDefault();
                            }
                            else {
                                HereMapCameraManager.sLogger.v("unlocking dial zoom:manual");
                                float n;
                                if (HereMapCameraManager.this.isOverViewMapVisible()) {
                                    n = 10000.0f;
                                }
                                else {
                                    n = (float)HereMapCameraManager.this.mapController.getZoomLevel();
                                }
                                HereMapCameraManager.this.localPreferences = new LocalPreferences.Builder(HereMapCameraManager.this.localPreferences).manualZoomLevel(n).build();
                                DriverProfileHelper.getInstance().getDriverProfileManager().updateLocalPreferences(HereMapCameraManager.this.localPreferences);
                                HereMapCameraManager.sLogger.v("unlocking dial zoom:manual pref saved");
                            }
                        }
                    }, 2);
                }
            }
        };
        this.easeOutDynamicZoomTilt = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(HereMapCameraManager.this.easeOutDynamicZoomTiltBk, 16);
            }
        };
        this.easeOutDynamicZoomTiltBk = new Runnable() {
            @Override
            public void run() {
                if (HereMapCameraManager.this.zoomActionOn) {
                    HereMapCameraManager.sLogger.v("easeout user zoom action is on");
                }
                else {
                    final boolean b = Math.abs(HereMapCameraManager.this.mapController.getZoomLevel() - HereMapCameraManager.this.currentDynamicZoom) >= 0.25;
                    final boolean b2 = Math.abs(HereMapCameraManager.this.mapController.getTilt() - HereMapCameraManager.this.currentDynamicTilt) > 5.0f;
                    if (b || b2) {
                        HereMapCameraManager.this.lastDynamicZoomTime = SystemClock.elapsedRealtime();
                        HereMapCameraManager.this.animateCamera();
                        HereMapCameraManager.sLogger.v("easeout zoom=" + b + " tilt=" + b2 + " current-zoom=" + HereMapCameraManager.this.currentDynamicZoom + " current-tilt=" + HereMapCameraManager.this.currentDynamicTilt);
                    }
                    else {
                        HereMapCameraManager.sLogger.v("easeout not significant current-zoom=" + HereMapCameraManager.this.currentDynamicZoom + " current-tilt=" + HereMapCameraManager.this.currentDynamicTilt);
                    }
                }
            }
        };
        this.handler = new Handler(Looper.getMainLooper());
        this.hereNavigationManager = HereNavigationManager.getInstance();
        this.speedManager = SpeedManager.getInstance();
    }
    
    private void animateCamera() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (HereMapCameraManager.this.isFullAnimationSettingEnabled) {
                    HereMapCameraManager.this.hereMapAnimator.setZoom(HereMapCameraManager.this.currentDynamicZoom);
                    HereMapCameraManager.this.hereMapAnimator.setTilt(HereMapCameraManager.this.currentDynamicTilt);
                }
                else {
                    HereMapCameraManager.this.nTimesAnimated = 0;
                    HereMapCameraManager.this.animationOn = true;
                    HereMapCameraManager.this.handler.post(HereMapCameraManager.this.getAnimateCameraRunnable((HereMapCameraManager.this.currentDynamicZoom - HereMapCameraManager.this.mapController.getZoomLevel()) / 5.0, (HereMapCameraManager.this.currentDynamicTilt - HereMapCameraManager.this.mapController.getTilt()) / 5.0f));
                }
            }
        }, 2);
    }
    
    private Runnable getAnimateCameraRunnable(final double n, final float n2) {
        return new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        GeoCoordinate geoCoordinate;
                        if (HereMapCameraManager.this.currentGeoPosition == null) {
                            geoCoordinate = HereMapCameraManager.this.mapController.getCenter();
                        }
                        else {
                            geoCoordinate = HereMapCameraManager.this.currentGeoPosition.getCoordinate();
                        }
                        final double n = HereMapCameraManager.this.mapController.getZoomLevel() + n;
                        final float n2 = HereMapCameraManager.this.mapController.getTilt() + n2;
                        double n3 = n;
                        if (n > 16.5) {
                            n3 = 16.5;
                        }
                        float n4 = n2;
                        if (n2 > 70.0f) {
                            n4 = 70.0f;
                        }
                        HereMapCameraManager.this.mapController.setCenter(geoCoordinate, Map.Animation.NONE, n3, -1.0f, n4);
                        HereMapCameraManager.this.nTimesAnimated++;
                        if (HereMapCameraManager.this.nTimesAnimated == 5) {
                            HereMapCameraManager.this.animationOn = false;
                            HereMapCameraManager.this.nTimesAnimated = 0;
                        }
                        else {
                            HereMapCameraManager.this.handler.postDelayed(HereMapCameraManager.this.getAnimateCameraRunnable(n, n2), 200L);
                        }
                    }
                }, 17);
            }
        };
    }
    
    private float getDynamicTilt(final double n) {
        float n2;
        if (n <= 8.9408) {
            n2 = 60.0f;
        }
        else if (n > 8.9408 && n <= 29.0576) {
            n2 = (float)this.getLinearMapSection(n, 8.9408, 29.0576, 60.0, 70.0);
        }
        else {
            n2 = 70.0f;
        }
        return n2;
    }
    
    private double getDynamicZoom(double n) {
        if (n <= 8.9408) {
            n = 16.5;
        }
        else if (n > 8.9408 && n <= 20.1168) {
            n = this.getLinearMapSection(n, 8.9408, 20.1168, 16.5, 16.0);
        }
        else if (n > 20.1168 && n <= 29.0576) {
            n = this.getLinearMapSection(n, 20.1168, 29.0576, 16.0, 15.25);
        }
        else if (n > 29.0576 && n <= 33.528) {
            n = this.getLinearMapSection(n, 29.0576, 33.528, 15.25, 14.75);
        }
        else {
            n = 14.75;
        }
        return n;
    }
    
    public static HereMapCameraManager getInstance() {
        return HereMapCameraManager.sInstance;
    }
    
    private double getLinearMapSection(final double n, final double n2, final double n3, final double n4, final double n5) {
        return (n - n2) * (n5 - n4) / (n3 - n2) + n4;
    }
    
    private boolean getNavigationView() {
        boolean b = true;
        if (this.navigationView == null) {
            final UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            this.navigationView = uiStateManager.getNavigationView();
            this.homeScreenView = uiStateManager.getHomescreenView();
            if (this.navigationView == null) {
                b = false;
            }
        }
        return b;
    }
    
    private void goBackFromOverview(final Runnable runnable) {
        if (this.getNavigationView()) {
            this.navigationView.animateBackfromOverview(new Runnable() {
                @Override
                public void run() {
                    if (runnable != null) {
                        HereMapCameraManager.this.handler.post(runnable);
                    }
                }
            });
        }
    }
    
    private void goToOverview(final GeoCoordinate geoCoordinate, final Runnable runnable) {
        if (this.getNavigationView()) {
            if (this.hereNavigationManager.getCurrentRoute() != null) {
                this.navigationView.animateToOverview(geoCoordinate, this.hereNavigationManager.getCurrentRoute(), this.hereNavigationManager.getCurrentMapRoute(), runnable, this.hereNavigationManager.hasArrived());
            }
            else {
                this.navigationView.animateToOverview(geoCoordinate, null, null, runnable, false);
            }
        }
    }
    
    private boolean isOverViewMapVisible() {
        return this.getNavigationView() && this.navigationView.isOverviewMapMode();
    }
    
    private void setInitialZoom() {
        if (!this.initialZoom) {
            HereMapCameraManager.sLogger.v("setInitialZoom:" + this.currentGeoPosition);
            if (this.currentGeoPosition == null) {
                final GeoCoordinate lastGeoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                if (lastGeoCoordinate == null) {
                    return;
                }
                this.currentGeoPosition = new GeoPosition(lastGeoCoordinate);
                this.currentGeoCoordinate = lastGeoCoordinate;
            }
            this.initialZoom = this.setZoom();
            HereMapCameraManager.sLogger.v("setInitialZoom:" + this.initialZoom);
        }
    }
    
    private void zoomToDefault() {
        if (this.init) {
            if (this.isManualZoom()) {
                HereMapCameraManager.sLogger.v("zoomToDefault: manual zoom enabled");
            }
            else if (!this.getNavigationView()) {
                HereMapCameraManager.sLogger.v("zoomToDefault: no view");
            }
            else if (this.navigationView.isOverviewMapMode()) {
                this.goBackFromOverview(new Runnable() {
                    @Override
                    public void run() {
                        HereMapCameraManager.this.animateCamera();
                    }
                });
            }
            else {
                this.animateCamera();
            }
        }
    }
    
    public GeoCoordinate getLastGeoCoordinate() {
        return this.currentGeoCoordinate;
    }
    
    public GeoPosition getLastGeoPosition() {
        return this.currentGeoPosition;
    }
    
    public float getLastTilt() {
        return this.currentDynamicTilt;
    }
    
    public double getLastZoom() {
        double currentDynamicZoom;
        if (this.isManualZoom()) {
            if (this.localPreferences.manualZoomLevel == 10000.0f) {
                currentDynamicZoom = 12.0;
            }
            else {
                currentDynamicZoom = this.localPreferences.manualZoomLevel;
            }
        }
        else {
            currentDynamicZoom = this.currentDynamicZoom;
        }
        return currentDynamicZoom;
    }
    
    public void initialize(final HereMapController mapController, final Bus bus, final HereMapAnimator hereMapAnimator) {
        while (true) {
            boolean isFullAnimationSettingEnabled = true;
            while (true) {
                Label_0150: {
                    synchronized (this) {
                        if (!this.init) {
                            this.mapController = mapController;
                            this.hereMapAnimator = hereMapAnimator;
                            if (hereMapAnimator.getAnimationMode() != HereMapAnimator.AnimationMode.ZOOM_TILT_ANIMATION) {
                                break Label_0150;
                            }
                            this.isFullAnimationSettingEnabled = isFullAnimationSettingEnabled;
                            final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
                            this.localPreferences = currentProfile.getLocalPreferences();
                            HereMapCameraManager.sLogger.v("localPreferences [" + currentProfile.getProfileName() + "] " + this.localPreferences);
                            this.setZoom();
                            this.currentDynamicZoom = 16.5;
                            this.currentDynamicTilt = 60.0f;
                            bus.register(this);
                            this.init = true;
                        }
                        return;
                    }
                }
                isFullAnimationSettingEnabled = false;
                continue;
            }
        }
    }
    
    public boolean isManualZoom() {
        return this.localPreferences != null && Boolean.TRUE.equals(this.localPreferences.manualZoom);
    }
    
    public boolean isOverviewZoomLevel() {
        return this.isManualZoom() && this.localPreferences.manualZoomLevel == 10000.0f;
    }
    
    @Subscribe
    public void onDialZoom(final MapEvents.DialMapZoom dialMapZoom) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    GeoCoordinate geoCoordinate = null;
                Label_0103:
                    while (true) {
                        Label_0089: {
                            try {
                                switch (HereMapCameraManager.this.mapController.getState()) {
                                    case OVERVIEW:
                                    case AR_MODE:
                                        if (HereMapCameraManager.this.currentGeoPosition != null) {
                                            break Label_0089;
                                        }
                                        geoCoordinate = HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
                                        if (geoCoordinate == null) {
                                            HereMapCameraManager.sLogger.i("no location");
                                            break;
                                        }
                                        break Label_0103;
                                }
                                return;
                            }
                            catch (Throwable t) {
                                HereMapCameraManager.sLogger.e("onDialZoom", t);
                                return;
                            }
                        }
                        geoCoordinate = HereMapCameraManager.this.currentGeoPosition.getCoordinate();
                        continue;
                    }
                    if (!HereMapCameraManager.this.animationOn && !HereMapCameraManager.this.goBackfromOverviewOn && !HereMapCameraManager.this.goToOverviewOn && (!HereMapCameraManager.this.isOverViewMapVisible() || dialMapZoom.type != MapEvents.DialMapZoom.Type.OUT)) {
                        final double zoomLevel = HereMapCameraManager.this.mapController.getZoomLevel();
                        double n = 0.0;
                        HereMapCameraManager.this.zoomActionOn = true;
                        HereMapCameraManager.this.handler.removeCallbacks(HereMapCameraManager.this.unlockZoom);
                        switch (dialMapZoom.type) {
                            case IN:
                                if (HereMapCameraManager.this.isOverViewMapVisible()) {
                                    HereMapCameraManager.this.goToOverviewOn = false;
                                    if (!HereMapCameraManager.this.goBackfromOverviewOn) {
                                        HereMapCameraManager.sLogger.v("goBackfromOverview:on");
                                        HereMapCameraManager.this.goBackfromOverviewOn = true;
                                        HereMapCameraManager.this.handler.post((Runnable)new Runnable() {
                                            @Override
                                            public void run() {
                                                HereMapCameraManager.this.goBackFromOverview(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        HereMapCameraManager.sLogger.v("goBackfromOverview:complete");
                                                        HereMapCameraManager.this.goBackfromOverviewOn = false;
                                                    }
                                                });
                                                HereMapCameraManager.this.handler.postDelayed(HereMapCameraManager.this.unlockZoom, 10000L);
                                            }
                                        });
                                    }
                                    return;
                                }
                                else {
                                    if (!HereMapCameraManager.this.goBackfromOverviewOn) {
                                        n = 0.25;
                                        break;
                                    }
                                    return;
                                }
                                break;
                            case OUT:
                                n = -0.25;
                                break;
                        }
                        double zoom;
                        final double n2 = zoom = zoomLevel + n;
                        if (zoomLevel < 16.5) {
                            zoom = n2;
                            if (n2 >= 16.5) {
                                zoom = 16.5;
                            }
                        }
                        int n3;
                        if (zoom <= 16.5 && zoom >= 12.0) {
                            n3 = 1;
                        }
                        else {
                            n3 = 0;
                        }
                        final boolean b = zoomLevel + n < 12.0;
                        if (n3 != 0) {
                            final long elapsedRealtime = SystemClock.elapsedRealtime();
                            if (HereMapCameraManager.this.isFullAnimationSettingEnabled) {
                                HereMapCameraManager.this.hereMapAnimator.setZoom(zoom);
                            }
                            else {
                                HereMapCameraManager.this.mapController.setCenter(geoCoordinate, Map.Animation.NONE, zoom, -1.0f, -1.0f);
                            }
                            HereMapCameraManager.sLogger.v("zoomLevel:" + zoom + " time:" + (SystemClock.elapsedRealtime() - elapsedRealtime));
                        }
                        else if (b) {
                            HereMapCameraManager.this.goBackfromOverviewOn = false;
                            HereMapCameraManager.this.goToOverviewOn = true;
                            HereMapCameraManager.sLogger.v("goToOverviewOn:on");
                            HereMapCameraManager.this.handler.post((Runnable)new Runnable() {
                                @Override
                                public void run() {
                                    HereMapCameraManager.this.goToOverview(geoCoordinate, new Runnable() {
                                        @Override
                                        public void run() {
                                            HereMapCameraManager.this.goToOverviewOn = false;
                                            HereMapCameraManager.sLogger.v("goToOverviewOn:complete");
                                        }
                                    });
                                }
                            });
                            HereMapCameraManager.sLogger.v("zoomLevel: overview");
                        }
                        HereMapCameraManager.this.handler.postDelayed(HereMapCameraManager.this.unlockZoom, 10000L);
                        return;
                    }
                    if (HereMapCameraManager.sLogger.isLoggable(2)) {
                        HereMapCameraManager.sLogger.v("onDialZoom no-op, animationOn: " + HereMapCameraManager.this.animationOn + " goBackfromOverviewOn:" + HereMapCameraManager.this.goBackfromOverviewOn + " goToOverviewOn:" + HereMapCameraManager.this.goToOverviewOn + " overviewMap on-out:" + (HereMapCameraManager.this.isOverViewMapVisible() && dialMapZoom.type == MapEvents.DialMapZoom.Type.OUT));
                    }
                }
            }
        }, 16);
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        this.localPreferences = currentProfile.getLocalPreferences();
        HereMapCameraManager.sLogger.v("driver changed[" + currentProfile.getProfileName() + "] " + this.localPreferences);
        this.setZoom();
    }
    
    public void onGeoPositionChange(final GeoPosition currentGeoPosition) {
        this.currentGeoPosition = currentGeoPosition;
        this.currentGeoCoordinate = currentGeoPosition.getCoordinate();
        if (this.localPreferences.manualZoom == null || !this.localPreferences.manualZoom) {
            final double n = this.speedManager.getObdSpeed();
            double speed;
            if (n != -1.0) {
                speed = SpeedManager.convert(n, this.speedManager.getSpeedUnit(), SpeedManager.SpeedUnit.METERS_PER_SECOND);
            }
            else {
                speed = this.currentGeoPosition.getSpeed();
            }
            if (speed == this.lastRecordedSpeed) {
                if (HereMapCameraManager.sLogger.isLoggable(2)) {
                    HereMapCameraManager.sLogger.v("speed not changed :" + speed);
                }
            }
            else {
                final double lastRecordedSpeed = this.lastRecordedSpeed;
                int n2;
                if (Math.abs(speed - this.lastRecordedSpeed) >= 1.5) {
                    n2 = 1;
                }
                else {
                    n2 = 0;
                }
                this.lastRecordedSpeed = speed;
                if (n2 != 0) {
                    this.handler.removeCallbacks(this.easeOutDynamicZoomTilt);
                    this.currentDynamicZoom = this.getDynamicZoom(speed);
                    this.currentDynamicTilt = this.getDynamicTilt(speed);
                    if (!this.zoomActionOn) {
                        final boolean b = Math.abs(this.mapController.getZoomLevel() - this.currentDynamicZoom) >= 0.25;
                        final boolean b2 = Math.abs(this.mapController.getTilt() - this.currentDynamicTilt) > 5.0f;
                        if (b || b2) {
                            final long n3 = SystemClock.elapsedRealtime() - this.lastDynamicZoomTime;
                            if (n3 < 10000L) {
                                final long n4 = 10000L - n3;
                                this.handler.postDelayed(this.easeOutDynamicZoomTilt, n4);
                                if (HereMapCameraManager.sLogger.isLoggable(2)) {
                                    HereMapCameraManager.sLogger.v("significant too early =" + n3 + " scheduled =" + n4);
                                }
                            }
                            else {
                                HereMapCameraManager.sLogger.v("significant zoom=" + b + " tilt=" + b2 + " current-zoom=" + this.currentDynamicZoom + " current-tilt=" + this.currentDynamicTilt + " speed = " + speed + " lastSpeed=" + lastRecordedSpeed);
                                this.lastDynamicZoomTime = SystemClock.elapsedRealtime();
                                this.animateCamera();
                            }
                        }
                    }
                }
            }
        }
    }
    
    @Subscribe
    public void onLocalPreferencesChanged(final LocalPreferences localPreferences) {
        if (localPreferences.equals(this.localPreferences)) {
            HereMapCameraManager.sLogger.v("localPref not changed:" + localPreferences);
        }
        else {
            HereMapCameraManager.sLogger.v("localPref changed:" + localPreferences);
            this.localPreferences = localPreferences;
            this.setZoom();
        }
    }
    
    @Subscribe
    public void onLocationFixChangeEvent(final MapEvents.LocationFix locationFix) {
        if (this.init && this.mapUIReady) {
            this.setInitialZoom();
        }
    }
    
    @Subscribe
    public void onMapUIReady(final MapEvents.MapUIReady mapUIReady) {
        this.mapUIReady = true;
        this.setInitialZoom();
    }
    
    public boolean setZoom() {
        boolean b;
        if (!this.init) {
            b = false;
        }
        else if (this.currentGeoPosition == null || this.localPreferences == null) {
            b = false;
        }
        else if (!this.getNavigationView()) {
            b = false;
        }
        else {
            final HereMapController.State state = this.mapController.getState();
            if (this.isManualZoom()) {
                HereMapCameraManager.sLogger.v("setZoom: manual: " + state);
                final double n = this.localPreferences.manualZoomLevel;
                if (n == 10000.0) {
                    if (state == HereMapController.State.AR_MODE) {
                        HereMapCameraManager.sLogger.v("setZoom: overview map zoom level, showOverviewMap");
                        this.navigationView.showOverviewMap(this.hereNavigationManager.getCurrentRoute(), this.hereNavigationManager.getCurrentMapRoute(), this.currentGeoPosition.getCoordinate(), this.hereNavigationManager.hasArrived());
                    }
                    else {
                        HereMapCameraManager.sLogger.v("setZoom: overview map zoom level, not in ar mode:" + state + ", set to OVERVIEW");
                        this.mapController.setState(HereMapController.State.OVERVIEW);
                        this.navigationView.showStartMarker();
                    }
                }
                else {
                    HereMapCameraManager.sLogger.v("setZoom: normal zoom level: " + n);
                    double n2 = 0.0;
                    Label_0305: {
                        if (n >= 12.0) {
                            n2 = n;
                            if (n <= 16.5) {
                                break Label_0305;
                            }
                        }
                        n2 = 16.5;
                        HereMapCameraManager.sLogger.w("setZoom: normal zoom level changed from [" + n + "] to [" + 16.5 + "]");
                    }
                    if (this.mapController.getState() == HereMapController.State.OVERVIEW || this.mapController.getState() == HereMapController.State.TRANSITION) {
                        HereMapCameraManager.sLogger.v("setZoom: normal zoom level, showRouteMap");
                        this.navigationView.showRouteMap(this.currentGeoPosition, n2, this.currentDynamicTilt);
                    }
                    else if (this.mapController.getState() == HereMapController.State.AR_MODE) {
                        this.mapController.setCenter(this.currentGeoPosition.getCoordinate(), Map.Animation.NONE, n2, (float)this.currentGeoPosition.getHeading(), this.currentDynamicTilt);
                        HereMapCameraManager.sLogger.v("setZoom: normal zoom level, already in route map, setcenter");
                    }
                    else {
                        HereMapCameraManager.sLogger.v("setZoom: normal zoom level, incorrect state:" + state);
                    }
                }
            }
            else {
                HereMapCameraManager.sLogger.v("setZoom: automatic: " + state);
                if (state == HereMapController.State.OVERVIEW || state == HereMapController.State.TRANSITION) {
                    HereMapCameraManager.sLogger.v("setZoom:automatic showRouteMap");
                    this.navigationView.showRouteMap(this.currentGeoPosition, this.currentDynamicZoom, this.currentDynamicTilt);
                }
                else if (this.mapController.getState() == HereMapController.State.AR_MODE) {
                    HereMapCameraManager.sLogger.v("setZoom:automatic already in route map, setCenter");
                    this.mapController.setCenter(this.currentGeoPosition.getCoordinate(), Map.Animation.NONE, this.currentDynamicZoom, (float)this.currentGeoPosition.getHeading(), this.currentDynamicTilt);
                }
                else {
                    HereMapCameraManager.sLogger.v("setZoom:automatic incorrect state:" + state);
                }
            }
            b = true;
        }
        return b;
    }
}
