package com.navdy.hud.app.manager;

public class PairingManager
{
    private boolean autoPairing;
    
    public boolean isAutoPairing() {
        return this.autoPairing;
    }
    
    public void setAutoPairing(final boolean autoPairing) {
        this.autoPairing = autoPairing;
    }
}
