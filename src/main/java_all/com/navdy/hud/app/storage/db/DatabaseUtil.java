package com.navdy.hud.app.storage.db;

import android.text.TextUtils;
import com.navdy.service.library.log.Logger;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseUtil
{
    public static final String ROW_ID = "rowid";
    
    public static void createIndex(final SQLiteDatabase sqLiteDatabase, final String s, final String s2, final Logger logger) {
        final String string = s + "_" + s2;
        sqLiteDatabase.execSQL("CREATE INDEX IF NOT EXISTS " + string + " ON " + s + "(" + s2 + ");");
        logger.v("createdIndex:" + string);
    }
    
    public static void dropTable(final SQLiteDatabase sqLiteDatabase, final String s, final Logger logger) {
        try {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + s);
                logger.v("dropped table[" + s + "]");
            }
        }
        catch (Throwable t) {
            logger.e(t);
        }
    }
    
    public static class DatabaseNotAvailable extends RuntimeException
    {
    }
}
