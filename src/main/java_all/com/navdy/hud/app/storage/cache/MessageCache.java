package com.navdy.hud.app.storage.cache;

import org.jetbrains.annotations.Nullable;
import com.navdy.service.library.events.MessageStore;
import com.navdy.service.library.events.Ext_NavdyEvent;
import kotlin.jvm.internal.Intrinsics;
import com.squareup.wire.Wire;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;
import com.squareup.wire.Message;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B'\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b¢\u0006\u0002\u0010\tJ\b\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004H\u0016J\u0017\u0010\u0015\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u0014\u001a\u00020\u0004H\u0016¢\u0006\u0002\u0010\u0016J\u001d\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0019J\u0010\u0010\u001a\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0004H\u0016R\u001d\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b" }, d2 = { "Lcom/navdy/hud/app/storage/cache/MessageCache;", "T", "Lcom/squareup/wire/Message;", "Lcom/navdy/hud/app/storage/cache/Cache;", "", "dataCache", "", "type", "Ljava/lang/Class;", "(Lcom/navdy/hud/app/storage/cache/Cache;Ljava/lang/Class;)V", "getDataCache", "()Lcom/navdy/hud/app/storage/cache/Cache;", "getType", "()Ljava/lang/Class;", "wire", "Lcom/squareup/wire/Wire;", "clear", "", "contains", "", "key", "get", "(Ljava/lang/String;)Lcom/squareup/wire/Message;", "put", "message", "(Ljava/lang/String;Lcom/squareup/wire/Message;)V", "remove", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class MessageCache<T extends Message> implements Cache<String, T>
{
    @NotNull
    private final Cache<String, byte[]> dataCache;
    @NotNull
    private final Class<T> type;
    private final Wire wire;
    
    public MessageCache(@NotNull final Cache<String, byte[]> dataCache, @NotNull final Class<T> type) {
        Intrinsics.checkParameterIsNotNull(dataCache, "dataCache");
        Intrinsics.checkParameterIsNotNull(type, "type");
        this.dataCache = dataCache;
        this.type = type;
        this.wire = new Wire((Class<?>[])new Class[] { Ext_NavdyEvent.class });
    }
    
    @Override
    public void clear() {
        this.dataCache.clear();
    }
    
    @Override
    public boolean contains(@NotNull final String s) {
        Intrinsics.checkParameterIsNotNull(s, "key");
        return this.dataCache.contains(s);
    }
    
    @Nullable
    @Override
    public T get(@NotNull final String s) {
        final Message message = null;
        Intrinsics.checkParameterIsNotNull(s, "key");
        final byte[] array = this.dataCache.get(s);
        final Message message2 = null;
        Message removeNulls = message;
        if (array != null) {
            removeNulls = MessageStore.<T>removeNulls((T)this.wire.<T>parseFrom(array, (Class<T>)this.type));
        }
        return (T)removeNulls;
    }
    
    @NotNull
    public final Cache<String, byte[]> getDataCache() {
        return this.dataCache;
    }
    
    @NotNull
    public final Class<T> getType() {
        return this.type;
    }
    
    @Override
    public void put(@NotNull final String s, @NotNull final T t) {
        Intrinsics.checkParameterIsNotNull(s, "key");
        Intrinsics.checkParameterIsNotNull(t, "message");
        final byte[] byteArray = t.toByteArray();
        final Cache<String, byte[]> dataCache = this.dataCache;
        Intrinsics.checkExpressionValueIsNotNull(byteArray, "bytes");
        dataCache.put(s, byteArray);
    }
    
    @Override
    public void remove(@NotNull final String s) {
        Intrinsics.checkParameterIsNotNull(s, "key");
        this.dataCache.remove(s);
    }
}
