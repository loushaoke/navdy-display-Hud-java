package com.navdy.hud.app.storage.cache;

import org.jetbrains.annotations.Nullable;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0012\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0016J\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0011\u001a\u00020\u0002H\u0096\u0002J\u0018\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0003H\u0016J\u0010\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0016" }, d2 = { "Lcom/navdy/hud/app/storage/cache/DiskLruCacheAdapter;", "Lcom/navdy/hud/app/storage/cache/Cache;", "", "", "cacheName", "directory", "maxSize", "", "(Ljava/lang/String;Ljava/lang/String;I)V", "diskLruCache", "Lcom/navdy/hud/app/storage/cache/DiskLruCache;", "getDiskLruCache", "()Lcom/navdy/hud/app/storage/cache/DiskLruCache;", "clear", "", "contains", "", "key", "get", "put", "data", "remove", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class DiskLruCacheAdapter implements Cache<String, byte[]>
{
    @NotNull
    private final DiskLruCache diskLruCache;
    
    public DiskLruCacheAdapter(@NotNull final String s, @NotNull final String s2, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "cacheName");
        Intrinsics.checkParameterIsNotNull(s2, "directory");
        this.diskLruCache = new DiskLruCache(s, s2, n);
    }
    
    @Override
    public void clear() {
        this.diskLruCache.clear();
    }
    
    @Override
    public boolean contains(@NotNull final String s) {
        Intrinsics.checkParameterIsNotNull(s, "key");
        return this.diskLruCache.contains(s);
    }
    
    @Nullable
    @Override
    public byte[] get(@NotNull final String s) {
        Intrinsics.checkParameterIsNotNull(s, "key");
        return this.diskLruCache.get(s);
    }
    
    @NotNull
    public final DiskLruCache getDiskLruCache() {
        return this.diskLruCache;
    }
    
    @Override
    public void put(@NotNull final String s, @NotNull final byte[] array) {
        Intrinsics.checkParameterIsNotNull(s, "key");
        Intrinsics.checkParameterIsNotNull(array, "data");
        this.diskLruCache.put(s, array);
    }
    
    @Override
    public void remove(@NotNull final String s) {
        Intrinsics.checkParameterIsNotNull(s, "key");
        this.diskLruCache.remove(s);
    }
}
