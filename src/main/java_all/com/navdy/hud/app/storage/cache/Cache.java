package com.navdy.hud.app.storage.cache;

import org.jetbrains.annotations.Nullable;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\bf\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003J\b\u0010\u0004\u001a\u00020\u0005H&J\u0015\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00028\u0000H&¢\u0006\u0002\u0010\tJ\u0017\u0010\n\u001a\u0004\u0018\u00018\u00012\u0006\u0010\b\u001a\u00028\u0000H&¢\u0006\u0002\u0010\u000bJ\u001d\u0010\f\u001a\u00020\u00052\u0006\u0010\b\u001a\u00028\u00002\u0006\u0010\r\u001a\u00028\u0001H&¢\u0006\u0002\u0010\u000eJ\u0015\u0010\u000f\u001a\u00020\u00052\u0006\u0010\b\u001a\u00028\u0000H&¢\u0006\u0002\u0010\u0010¨\u0006\u0011" }, d2 = { "Lcom/navdy/hud/app/storage/cache/Cache;", "K", "V", "", "clear", "", "contains", "", "key", "(Ljava/lang/Object;)Z", "get", "(Ljava/lang/Object;)Ljava/lang/Object;", "put", "data", "(Ljava/lang/Object;Ljava/lang/Object;)V", "remove", "(Ljava/lang/Object;)V", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public interface Cache<K, V>
{
    void clear();
    
    boolean contains(final K p0);
    
    @Nullable
    V get(final K p0);
    
    void put(final K p0, final V p1);
    
    void remove(final K p0);
}
