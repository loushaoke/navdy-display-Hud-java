package com.navdy.hud.app.storage.cache;

import android.util.LruCache;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.util.IOUtils;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.task.TaskManager;
import java.io.File;
import com.navdy.service.library.log.Logger;
import android.content.Context;

public class DiskLruCache
{
    private Context appContext;
    private String cacheName;
    private Logger logger;
    private MemoryLruCache lruCache;
    private int maxSize;
    private File rootPath;
    private TaskManager taskManager;
    
    public DiskLruCache(final String cacheName, final String s, final int maxSize) {
        this.logger = new Logger("DLC-" + cacheName);
        this.cacheName = cacheName;
        (this.rootPath = new File(s)).mkdirs();
        this.maxSize = maxSize;
        this.lruCache = new MemoryLruCache(this, maxSize);
        this.taskManager = TaskManager.getInstance();
        this.appContext = HudApplication.getAppContext();
        this.init();
    }
    
    private void addFile(final String s, final byte[] array) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                // 
                This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     1: astore_1       
                //     2: aconst_null    
                //     3: astore_2       
                //     4: aload_1        
                //     5: astore_3       
                //     6: new             Ljava/io/FileOutputStream;
                //     9: astore          4
                //    11: aload_1        
                //    12: astore_3       
                //    13: aload           4
                //    15: aload_0        
                //    16: getfield        com/navdy/hud/app/storage/cache/DiskLruCache$2.this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;
                //    19: aload_0        
                //    20: getfield        com/navdy/hud/app/storage/cache/DiskLruCache$2.val$name:Ljava/lang/String;
                //    23: invokestatic    com/navdy/hud/app/storage/cache/DiskLruCache.access$100:(Lcom/navdy/hud/app/storage/cache/DiskLruCache;Ljava/lang/String;)Ljava/lang/String;
                //    26: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
                //    29: aload           4
                //    31: aload_0        
                //    32: getfield        com/navdy/hud/app/storage/cache/DiskLruCache$2.val$val:[B
                //    35: invokevirtual   java/io/FileOutputStream.write:([B)V
                //    38: aload           4
                //    40: invokestatic    com/navdy/service/library/util/IOUtils.fileSync:(Ljava/io/FileOutputStream;)V
                //    43: aload           4
                //    45: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //    48: return         
                //    49: astore_1       
                //    50: aload_2        
                //    51: astore          4
                //    53: aload           4
                //    55: astore_3       
                //    56: aload_0        
                //    57: getfield        com/navdy/hud/app/storage/cache/DiskLruCache$2.this$0:Lcom/navdy/hud/app/storage/cache/DiskLruCache;
                //    60: invokestatic    com/navdy/hud/app/storage/cache/DiskLruCache.access$200:(Lcom/navdy/hud/app/storage/cache/DiskLruCache;)Lcom/navdy/service/library/log/Logger;
                //    63: aload_1        
                //    64: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
                //    67: aload           4
                //    69: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //    72: goto            48
                //    75: astore          4
                //    77: aload_3        
                //    78: astore_1       
                //    79: aload_1        
                //    80: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //    83: aload           4
                //    85: athrow         
                //    86: astore_3       
                //    87: aload           4
                //    89: astore_1       
                //    90: aload_3        
                //    91: astore          4
                //    93: goto            79
                //    96: astore_1       
                //    97: goto            53
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                 
                //  -----  -----  -----  -----  ---------------------
                //  6      11     49     53     Ljava/lang/Throwable;
                //  6      11     75     79     Any
                //  13     29     49     53     Ljava/lang/Throwable;
                //  13     29     75     79     Any
                //  29     43     96     100    Ljava/lang/Throwable;
                //  29     43     86     96     Any
                //  56     67     75     79     Any
                // 
                // The error that occurred was:
                // 
                // java.util.ConcurrentModificationException
                //     at java.util.ArrayList$Itr.checkForComodification(Unknown Source)
                //     at java.util.ArrayList$Itr.next(Unknown Source)
                //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2863)
                //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2445)
                //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Unknown Source)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        }, 22);
    }
    
    private String getFilePath(final String s) {
        return this.rootPath + File.separator + s;
    }
    
    private void init() {
        this.logger.v("init start");
        final File[] listFiles = this.rootPath.listFiles();
        if (listFiles != null) {
            final ArrayList<Object> list = new ArrayList<Object>();
            for (int i = 0; i < listFiles.length; ++i) {
                if (listFiles[i].isFile()) {
                    list.add(listFiles[i]);
                }
            }
            final int size = list.size();
            this.logger.v("init: files in cache[" + size + "]");
            if (size == 0) {
                this.logger.v("init: cache empty");
            }
            else {
                Collections.<Object>sort(list, (Comparator<? super Object>)new Comparator<File>() {
                    @Override
                    public int compare(final File file, final File file2) {
                        return Long.compare(file.lastModified(), file2.lastModified());
                    }
                });
                for (final File file : list) {
                    final String name = file.getName();
                    this.lruCache.put(name, new FileEntry(name, (int)file.length()));
                }
                this.logger.v("init complete");
            }
        }
    }
    
    private void removeFile(final FileEntry fileEntry) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                if (!IOUtils.deleteFile(DiskLruCache.this.appContext, DiskLruCache.this.getFilePath(fileEntry.name))) {
                    DiskLruCache.this.logger.v("file not deleted [" + fileEntry.name + "]");
                }
            }
        }, 22);
    }
    
    public void clear() {
        synchronized (this) {
            this.lruCache.evictAll();
        }
    }
    
    public boolean contains(final String s) {
        synchronized (this) {
            return this.lruCache.get(s) != null;
        }
    }
    
    public byte[] get(final String s) {
        final byte[] array = null;
        synchronized (this) {
            GenericUtil.checkNotOnMainThread();
            final FileEntry fileEntry = (FileEntry)this.lruCache.get(s);
            byte[] binaryFile;
            if (fileEntry == null) {
                binaryFile = array;
            }
            else {
                this.updateFile(fileEntry);
                try {
                    binaryFile = IOUtils.readBinaryFile(this.getFilePath(s));
                }
                catch (Throwable t) {
                    this.logger.e(t);
                    binaryFile = array;
                }
            }
            return binaryFile;
        }
    }
    
    public void put(final String s, final byte[] array) {
        synchronized (this) {
            final FileEntry fileEntry = new FileEntry(s, array.length);
            this.lruCache.put(s, fileEntry);
            if (!fileEntry.removed) {
                this.addFile(s, array);
            }
        }
    }
    
    public void remove(final String s) {
        synchronized (this) {
            this.lruCache.remove(s);
        }
    }
    
    public void updateFile(final FileEntry fileEntry) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final File file = new File(DiskLruCache.this.getFilePath(fileEntry.name));
                if (!file.setLastModified(System.currentTimeMillis())) {
                    DiskLruCache.this.logger.v("file time not changed [" + file.getAbsolutePath() + "]");
                }
            }
        }, 22);
    }
    
    private static class FileEntry
    {
        String name;
        boolean removed;
        int size;
        
        FileEntry(final String name, final int size) {
            this.name = name;
            this.size = size;
        }
    }
    
    private static class MemoryLruCache extends LruCache<String, FileEntry>
    {
        private DiskLruCache parent;
        
        public MemoryLruCache(final DiskLruCache parent, final int n) {
            super(n);
            this.parent = parent;
        }
        
        protected void entryRemoved(final boolean b, final String s, final FileEntry fileEntry, final FileEntry fileEntry2) {
            if (fileEntry != null) {
                this.parent.removeFile(fileEntry);
                fileEntry.removed = true;
            }
        }
        
        protected int sizeOf(final String s, final FileEntry fileEntry) {
            return fileEntry.size;
        }
    }
}
