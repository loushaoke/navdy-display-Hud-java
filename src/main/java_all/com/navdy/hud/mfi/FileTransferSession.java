package com.navdy.hud.mfi;

import java.io.IOException;
import android.util.Log;

public class FileTransferSession
{
    public static final int DATA_OFFSET = 2;
    private static final int INDEX_OF_COMMAND = 1;
    private static final int INDEX_OF_ID = 0;
    public static final int MESSAGE_SIZE = 2;
    private static final String TAG;
    private boolean mCanceled;
    private byte[] mFileData;
    private int mFileReceivedSoFar;
    private long mFileSize;
    public int mFileTransferIdentifier;
    IIAPFileTransferManager mFileTransferManager;
    private boolean mFinished;
    private boolean mStarted;
    
    static {
        TAG = FileTransferSession.class.getSimpleName();
    }
    
    public FileTransferSession(final int mFileTransferIdentifier, final IIAPFileTransferManager mFileTransferManager) {
        this.mStarted = false;
        this.mFinished = false;
        this.mCanceled = false;
        this.mFileTransferManager = mFileTransferManager;
        this.mFileTransferIdentifier = mFileTransferIdentifier;
    }
    
    private void error(final Throwable t) {
        this.mFinished = true;
        this.sendCommand(FileTransferCommand.COMMAND_FAILURE);
        this.mFileTransferManager.onError(this.mFileTransferIdentifier, t);
    }
    
    private void sendCommand(final FileTransferCommand fileTransferCommand) {
        this.mFileTransferManager.sendMessage(this.mFileTransferIdentifier, new byte[] { (byte)(this.mFileTransferIdentifier & 0xFF), (byte)fileTransferCommand.value });
        Log.d("MFi", "Accessory(F): " + this.mFileTransferIdentifier + " Command : " + fileTransferCommand.name());
    }
    
    public void bProcessFileTransferMessage(final byte[] array) {
        final FileTransferCommand fromInt = FileTransferCommand.fromInt(array[1] & 0xFF);
        if (fromInt != FileTransferCommand.COMMAND_SETUP && !this.mStarted) {
            Log.e(FileTransferSession.TAG, "File transfer is not initiated, bad state " + fromInt);
            this.mFileTransferManager.onError(this.mFileTransferIdentifier, new IllegalStateException("File transfer not started"));
        }
        else {
            switch (fromInt) {
                case COMMAND_SETUP:
                    this.mStarted = true;
                    this.mFileSize = Utils.unpackInt64(array, 2);
                    Log.d("MFi", "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + fromInt.name() + ", File_Size : " + this.mFileSize);
                    if (this.mFileSize > this.mFileTransferManager.getFileTransferLimit()) {
                        Log.e(FileTransferSession.TAG, "File too large, not starting the transfer");
                        this.sendCommand(FileTransferCommand.COMMAND_CANCEL);
                        break;
                    }
                    this.mFileTransferManager.onFileTransferSetupRequest(this.mFileTransferIdentifier, this.mFileSize);
                    if (this.mFileSize == 0L) {
                        this.sendCommand(FileTransferCommand.COMMAND_CANCEL);
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, null);
                        break;
                    }
                    break;
                case COMMAND_FIRST_DATA:
                    try {
                        System.arraycopy(array, 2, this.mFileData, this.mFileReceivedSoFar, array.length - 2);
                        this.mFileReceivedSoFar += array.length - 2;
                        Log.d("MFi", "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + fromInt.name() + ", Received : " + this.mFileReceivedSoFar);
                    }
                    catch (Throwable t) {
                        this.error(t);
                    }
                    break;
                case COMMAND_FIRST_AND_ONLY_DATA:
                    try {
                        System.arraycopy(array, 2, this.mFileData, this.mFileReceivedSoFar, array.length - 2);
                        this.mFileReceivedSoFar += array.length - 2;
                        Log.d("MFi", "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + fromInt.name() + ", Received : " + this.mFileReceivedSoFar);
                        this.sendCommand(FileTransferCommand.COMMAND_SUCCESS);
                        this.mFinished = true;
                        this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, this.mFileData);
                    }
                    catch (Throwable t2) {
                        this.error(t2);
                    }
                    break;
                case COMMAND_DATA:
                    try {
                        System.arraycopy(array, 2, this.mFileData, this.mFileReceivedSoFar, array.length - 2);
                        this.mFileReceivedSoFar += array.length - 2;
                        Log.d("MFi", "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + fromInt.name() + ", Received : " + this.mFileReceivedSoFar);
                    }
                    catch (Throwable t3) {
                        this.error(t3);
                    }
                    break;
                case COMMAND_LAST_DATA:
                    Label_0745: {
                        try {
                            System.arraycopy(array, 2, this.mFileData, this.mFileReceivedSoFar, array.length - 2);
                            this.mFileReceivedSoFar += array.length - 2;
                            Log.d("MFi", "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + fromInt.name() + ", Received : " + this.mFileReceivedSoFar);
                            if (this.mFileReceivedSoFar == this.mFileSize) {
                                break Label_0745;
                            }
                            this.sendCommand(FileTransferCommand.COMMAND_FAILURE);
                            this.mFinished = true;
                            this.mFileTransferManager.onError(this.mFileTransferIdentifier, new IOException("Missed packets"));
                        }
                        catch (Throwable t4) {
                            this.error(t4);
                        }
                        break;
                    }
                    this.sendCommand(FileTransferCommand.COMMAND_SUCCESS);
                    this.mFinished = true;
                    this.mFileTransferManager.onSuccess(this.mFileTransferIdentifier, this.mFileData);
                    break;
                case COMMAND_CANCEL:
                    this.mFinished = true;
                    Log.d("MFi", "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + fromInt.name());
                    this.mFileTransferManager.onCanceled(this.mFileTransferIdentifier);
                    break;
                case COMMAND_PAUSE:
                    Log.d("MFi", "Apple Device(F): " + this.mFileTransferIdentifier + " Command : " + fromInt.name());
                    this.mFileTransferManager.onPaused(this.mFileTransferIdentifier);
                    break;
            }
        }
    }
    
    public void cancel() {
        synchronized (this) {
            this.mCanceled = true;
            if (this.mFileTransferManager != null) {
                this.sendCommand(FileTransferCommand.COMMAND_CANCEL);
                this.mFinished = true;
            }
        }
    }
    
    public boolean isActive() {
        return this.mStarted && !this.mFinished;
    }
    
    public void proceed() {
        synchronized (this) {
            if (!this.mCanceled) {
                this.mFileData = new byte[(int)this.mFileSize];
                if (this.mFileTransferManager != null) {
                    this.sendCommand(FileTransferCommand.COMMAND_START);
                }
            }
        }
    }
    
    public void release() {
    }
    
    enum FileTransferCommand
    {
        COMMAND_CANCEL(2), 
        COMMAND_DATA(0), 
        COMMAND_FAILURE(6), 
        COMMAND_FIRST_AND_ONLY_DATA(192), 
        COMMAND_FIRST_DATA(128), 
        COMMAND_LAST_DATA(64), 
        COMMAND_PAUSE(3), 
        COMMAND_SETUP(4), 
        COMMAND_START(1), 
        COMMAND_SUCCESS(5);
        
        public final int value;
        
        private FileTransferCommand(final int value) {
            this.value = value;
        }
        
        public static FileTransferCommand fromInt(final int n) {
            FileTransferCommand fileTransferCommand;
            if (n >= FileTransferCommand.COMMAND_DATA.value && n <= FileTransferCommand.COMMAND_FAILURE.value) {
                fileTransferCommand = values()[n];
            }
            else if (n == FileTransferCommand.COMMAND_LAST_DATA.value) {
                fileTransferCommand = FileTransferCommand.COMMAND_LAST_DATA;
            }
            else if (n == FileTransferCommand.COMMAND_FIRST_DATA.value) {
                fileTransferCommand = FileTransferCommand.COMMAND_FIRST_DATA;
            }
            else if (n == FileTransferCommand.COMMAND_FIRST_AND_ONLY_DATA.value) {
                fileTransferCommand = FileTransferCommand.COMMAND_FIRST_AND_ONLY_DATA;
            }
            else {
                fileTransferCommand = null;
            }
            return fileTransferCommand;
        }
    }
}
