package com.navdy.hud.mfi;

public class SessionPacket extends Packet
{
    int session;
    
    public SessionPacket(final int session, final byte[] array) {
        super(array);
        this.session = session;
    }
}
