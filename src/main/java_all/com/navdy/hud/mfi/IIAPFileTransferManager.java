package com.navdy.hud.mfi;

public interface IIAPFileTransferManager
{
    public static final int FILE_TRANSFER_SESSION_ID = 2;
    
    void cancel();
    
    void clear();
    
    long getFileTransferLimit();
    
    void onCanceled(final int p0);
    
    void onError(final int p0, final Throwable p1);
    
    void onFileTransferSetupRequest(final int p0, final long p1);
    
    void onFileTransferSetupResponse(final int p0, final boolean p1);
    
    void onPaused(final int p0);
    
    void onSuccess(final int p0, final byte[] p1);
    
    void queue(final byte[] p0);
    
    void sendMessage(final int p0, final byte[] p1);
    
    void setFileTransferLimit(final int p0);
}
