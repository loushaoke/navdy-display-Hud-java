package com.navdy.hud.mfi;

import android.util.Log;
import android.text.TextUtils;

public class IAPMusicManager extends IAPControlMessageProcessor
{
    private static final String TAG;
    public static iAPProcessor.iAPMessage[] mMessages;
    private NowPlayingUpdate aggregatedNowPlayingUpdate;
    private IAPNowPlayingUpdateListener mNowPlayingUpdateListener;
    
    static {
        TAG = IAPMusicManager.class.getSimpleName();
        IAPMusicManager.mMessages = new iAPProcessor.iAPMessage[] { iAPProcessor.iAPMessage.NowPlayingUpdate };
    }
    
    public IAPMusicManager(final iAPProcessor iapProcessor) {
        super(IAPMusicManager.mMessages, iapProcessor);
        this.aggregatedNowPlayingUpdate = new NowPlayingUpdate();
    }
    
    private void mergeNowPlayingUpdate(final NowPlayingUpdate nowPlayingUpdate) {
        if (nowPlayingUpdate.mediaItemTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemTitle = nowPlayingUpdate.mediaItemTitle;
        }
        if (nowPlayingUpdate.mediaItemAlbumTitle != null) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTitle = nowPlayingUpdate.mediaItemAlbumTitle;
        }
        if (nowPlayingUpdate.mediaItemArtist != null) {
            this.aggregatedNowPlayingUpdate.mediaItemArtist = nowPlayingUpdate.mediaItemArtist;
        }
        if (nowPlayingUpdate.mediaItemGenre != null) {
            this.aggregatedNowPlayingUpdate.mediaItemGenre = nowPlayingUpdate.mediaItemGenre;
        }
        if (nowPlayingUpdate.mPlaybackStatus != null) {
            this.aggregatedNowPlayingUpdate.mPlaybackStatus = nowPlayingUpdate.mPlaybackStatus;
        }
        if (nowPlayingUpdate.playbackShuffle != null) {
            this.aggregatedNowPlayingUpdate.playbackShuffle = nowPlayingUpdate.playbackShuffle;
        }
        if (nowPlayingUpdate.playbackRepeat != null) {
            this.aggregatedNowPlayingUpdate.playbackRepeat = nowPlayingUpdate.playbackRepeat;
        }
        if (nowPlayingUpdate.mediaItemPersistentIdentifier != null) {
            this.aggregatedNowPlayingUpdate.mediaItemPersistentIdentifier = nowPlayingUpdate.mediaItemPersistentIdentifier;
        }
        if (nowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds > 0L) {
            this.aggregatedNowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds = nowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds;
        }
        if (nowPlayingUpdate.mediaItemAlbumTrackNumber >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackNumber = nowPlayingUpdate.mediaItemAlbumTrackNumber;
        }
        if (nowPlayingUpdate.mediaItemAlbumTrackCount >= 0) {
            this.aggregatedNowPlayingUpdate.mediaItemAlbumTrackCount = nowPlayingUpdate.mediaItemAlbumTrackCount;
        }
        if (nowPlayingUpdate.mPlaybackElapsedTimeMilliseconds >= 0L) {
            this.aggregatedNowPlayingUpdate.mPlaybackElapsedTimeMilliseconds = nowPlayingUpdate.mPlaybackElapsedTimeMilliseconds;
        }
        if (!TextUtils.isEmpty((CharSequence)nowPlayingUpdate.mAppName)) {
            this.aggregatedNowPlayingUpdate.mAppName = nowPlayingUpdate.mAppName;
        }
        if (!TextUtils.isEmpty((CharSequence)nowPlayingUpdate.mAppBundleId)) {
            this.aggregatedNowPlayingUpdate.mAppBundleId = nowPlayingUpdate.mAppBundleId;
        }
        if (nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier >= 128 && nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier <= 255) {
            this.aggregatedNowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier;
        }
    }
    
    private static NowPlayingUpdate parseNowPlayingUpdate(final iAPProcessor.IAP2Params iap2Params) {
        final NowPlayingUpdate nowPlayingUpdate = new NowPlayingUpdate();
        if (iap2Params.hasParam(NowPlayingUpdatesParameters.MediaItemAttributes)) {
            final iAPProcessor.IAP2Params iap2Params2 = new iAPProcessor.IAP2Params(iap2Params.getBlob(NowPlayingUpdatesParameters.MediaItemAttributes), 0);
            if (iap2Params2.hasParam(MediaItemAttributes.MediaItemPersistentIdentifier.getParam())) {
                nowPlayingUpdate.mediaItemPersistentIdentifier = iap2Params2.getUInt64(MediaItemAttributes.MediaItemPersistentIdentifier.getParam());
            }
            if (iap2Params2.hasParam(MediaItemAttributes.MediaItemTitle.getParam())) {
                nowPlayingUpdate.mediaItemTitle = iap2Params2.getUTF8(MediaItemAttributes.MediaItemTitle.getParam());
            }
            if (iap2Params2.hasParam(MediaItemAttributes.MediaItemAlbumTrackCount.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTrackCount = iap2Params2.getUInt16(MediaItemAttributes.MediaItemAlbumTrackCount.getParam());
            }
            if (iap2Params2.hasParam(MediaItemAttributes.MediaItemAlbumTrackNumber.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTrackNumber = iap2Params2.getUInt16(MediaItemAttributes.MediaItemAlbumTrackNumber.getParam());
            }
            if (iap2Params2.hasParam(MediaItemAttributes.MediaItemArtist.getParam())) {
                nowPlayingUpdate.mediaItemArtist = iap2Params2.getUTF8(MediaItemAttributes.MediaItemArtist.getParam());
            }
            if (iap2Params2.hasParam(MediaItemAttributes.MediaItemAlbumTitle.getParam())) {
                nowPlayingUpdate.mediaItemAlbumTitle = iap2Params2.getUTF8(MediaItemAttributes.MediaItemAlbumTitle.getParam());
            }
            if (iap2Params2.hasParam(MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam())) {
                nowPlayingUpdate.mediaItemArtworkFileTransferIdentifier = iap2Params2.getUInt8(MediaItemAttributes.MediaItemArtworkFileTransferIdentifier.getParam());
            }
            if (iap2Params2.hasParam(MediaItemAttributes.MediaItemGenre.getParam())) {
                nowPlayingUpdate.mediaItemGenre = iap2Params2.getUTF8(MediaItemAttributes.MediaItemGenre.getParam());
            }
            if (iap2Params2.hasParam(MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam())) {
                nowPlayingUpdate.mediaItemPlaybackDurationInMilliseconds = iap2Params2.getUInt32(MediaItemAttributes.MediaItemPlaybackDurationInMilliSeconds.getParam());
            }
        }
        if (iap2Params.hasParam(NowPlayingUpdatesParameters.PlaybackAttributes)) {
            final iAPProcessor.IAP2Params iap2Params3 = new iAPProcessor.IAP2Params(iap2Params.getBlob(NowPlayingUpdatesParameters.PlaybackAttributes), 0);
            if (iap2Params3.hasParam(PlaybackAttributes.PlaybackStatus.getParam())) {
                nowPlayingUpdate.mPlaybackStatus = iap2Params3.<NowPlayingUpdate.PlaybackStatus>getEnum(NowPlayingUpdate.PlaybackStatus.class, PlaybackAttributes.PlaybackStatus.getParam());
            }
            if (iap2Params3.hasParam(PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam())) {
                nowPlayingUpdate.mPlaybackElapsedTimeMilliseconds = iap2Params3.getUInt32(PlaybackAttributes.PlaybackElapsedTimeInMilliseconds.getParam());
            }
            if (iap2Params3.hasParam(PlaybackAttributes.PlaybackAppName.getParam())) {
                nowPlayingUpdate.mAppName = iap2Params3.getUTF8(PlaybackAttributes.PlaybackAppName.getParam());
            }
            if (iap2Params3.hasParam(PlaybackAttributes.PlaybackAppBundleId.getParam())) {
                nowPlayingUpdate.mAppBundleId = iap2Params3.getUTF8(PlaybackAttributes.PlaybackAppBundleId.getParam());
            }
            if (iap2Params3.hasParam(PlaybackAttributes.PlaybackShuffleMode.getParam())) {
                nowPlayingUpdate.playbackShuffle = iap2Params3.<NowPlayingUpdate.PlaybackShuffle>getEnum(NowPlayingUpdate.PlaybackShuffle.class, PlaybackAttributes.PlaybackShuffleMode.getParam());
            }
            if (iap2Params3.hasParam(PlaybackAttributes.PlaybackRepeatMode.getParam())) {
                nowPlayingUpdate.playbackRepeat = iap2Params3.<NowPlayingUpdate.PlaybackRepeat>getEnum(NowPlayingUpdate.PlaybackRepeat.class, PlaybackAttributes.PlaybackRepeatMode.getParam());
            }
        }
        return nowPlayingUpdate;
    }
    
    @Override
    public void bProcessControlMessage(final iAPProcessor.iAPMessage iapMessage, final int n, final byte[] array) {
        final iAPProcessor.IAP2Params parse = iAPProcessor.parse(array);
        switch (iapMessage) {
            case NowPlayingUpdate: {
                final NowPlayingUpdate nowPlayingUpdate = parseNowPlayingUpdate(parse);
                Log.d(IAPMusicManager.TAG, "Parsed " + nowPlayingUpdate);
                this.mergeNowPlayingUpdate(nowPlayingUpdate);
                if (this.mNowPlayingUpdateListener != null) {
                    this.mNowPlayingUpdateListener.onNowPlayingUpdate(this.aggregatedNowPlayingUpdate);
                    break;
                }
                break;
            }
        }
    }
    
    public void onKeyDown(final int n) {
        this.miAPProcessor.onKeyDown(n);
    }
    
    public void onKeyDown(final MediaKey mediaKey) {
        this.miAPProcessor.onKeyDown(mediaKey);
    }
    
    public void onKeyUp(final int n) {
        this.miAPProcessor.onKeyUp(n);
    }
    
    public void onKeyUp(final MediaKey mediaKey) {
        this.miAPProcessor.onKeyUp(mediaKey);
    }
    
    public void setNowPlayingUpdateListener(final IAPNowPlayingUpdateListener mNowPlayingUpdateListener) {
        this.mNowPlayingUpdateListener = mNowPlayingUpdateListener;
    }
    
    public void startNowPlayingUpdates() {
        final int n = 0;
        final iAPProcessor.IAP2SessionMessage iap2SessionMessage = new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.StartNowPlayingUpdates);
        final iAPProcessor.IAP2ParamsCreator iap2ParamsCreator = new iAPProcessor.IAP2ParamsCreator();
        final MediaItemAttributes[] values = MediaItemAttributes.values();
        for (int length = values.length, i = 0; i < length; ++i) {
            iap2ParamsCreator.addNone(values[i].getParam());
        }
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addBlob(NowPlayingUpdatesParameters.MediaItemAttributes, iap2ParamsCreator.toBytes());
        final iAPProcessor.IAP2ParamsCreator iap2ParamsCreator2 = new iAPProcessor.IAP2ParamsCreator();
        final PlaybackAttributes[] values2 = PlaybackAttributes.values();
        for (int length2 = values2.length, j = n; j < length2; ++j) {
            iap2ParamsCreator2.addNone(values2[j].getParam());
        }
        ((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage).addBlob(NowPlayingUpdatesParameters.PlaybackAttributes, iap2ParamsCreator2.toBytes());
        this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)iap2SessionMessage);
    }
    
    public void stopNowPlayingUpdates() {
        this.miAPProcessor.sendControlMessage((iAPProcessor.IAP2ParamsCreator)new iAPProcessor.IAP2SessionMessage(iAPProcessor.iAPMessage.StopNowPlayingUpdates));
    }
    
    public enum MediaItemAttributes
    {
        MediaItemAlbumTitle(6), 
        MediaItemAlbumTrackCount(8), 
        MediaItemAlbumTrackNumber(7), 
        MediaItemArtist(12), 
        MediaItemArtworkFileTransferIdentifier(26), 
        MediaItemGenre(16), 
        MediaItemPersistentIdentifier(0), 
        MediaItemPlaybackDurationInMilliSeconds(4), 
        MediaItemTitle(1);
        
        int paramIndex;
        
        private MediaItemAttributes(final int paramIndex) {
            this.paramIndex = paramIndex;
        }
        
        public int getParam() {
            return this.paramIndex;
        }
    }
    
    public enum MediaKey
    {
        Next, 
        PlayPause, 
        Previous, 
        Siri;
    }
    
    public enum NowPlayingUpdatesParameters
    {
        MediaItemAttributes, 
        PlaybackAttributes;
    }
    
    public enum PlaybackAttributes
    {
        PlaybackAppBundleId(16), 
        PlaybackAppName(7), 
        PlaybackElapsedTimeInMilliseconds(1), 
        PlaybackRepeatMode(6), 
        PlaybackShuffleMode(5), 
        PlaybackStatus(0);
        
        int paramIndex;
        
        private PlaybackAttributes(final int paramIndex) {
            this.paramIndex = paramIndex;
        }
        
        public int getParam() {
            return this.paramIndex;
        }
    }
}
