package com.navdy.hud.mfi;

public interface SessionPacketReceiver
{
    void queue(final SessionPacket p0);
}
