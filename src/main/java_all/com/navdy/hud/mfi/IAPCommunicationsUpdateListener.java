package com.navdy.hud.mfi;

public interface IAPCommunicationsUpdateListener
{
    void onCallStateUpdate(final CallStateUpdate p0);
    
    void onCommunicationUpdate(final CommunicationUpdate p0);
}
