package com.navdy.obd;

import android.os.Parcelable.Creator;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import java.util.List;
import android.os.RemoteException;
import android.os.IInterface;

public interface IObdService extends IInterface
{
    void addListener(final IObdServiceListener p0) throws RemoteException;
    
    void connect(final String p0, final IObdServiceListener p1) throws RemoteException;
    
    void disconnect() throws RemoteException;
    
    String getFirmwareVersion() throws RemoteException;
    
    int getState() throws RemoteException;
    
    String readPid(final int p0) throws RemoteException;
    
    void removeListener(final IObdServiceListener p0) throws RemoteException;
    
    void reset() throws RemoteException;
    
    void scanPids(final List<Pid> p0, final int p1) throws RemoteException;
    
    void scanSupportedPids() throws RemoteException;
    
    void scanVIN() throws RemoteException;
    
    void startRawScan() throws RemoteException;
    
    boolean updateDeviceFirmware(final String p0, final String p1) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IObdService
    {
        private static final String DESCRIPTOR = "com.navdy.obd.IObdService";
        static final int TRANSACTION_addListener = 12;
        static final int TRANSACTION_connect = 1;
        static final int TRANSACTION_disconnect = 2;
        static final int TRANSACTION_getFirmwareVersion = 4;
        static final int TRANSACTION_getState = 3;
        static final int TRANSACTION_readPid = 7;
        static final int TRANSACTION_removeListener = 13;
        static final int TRANSACTION_reset = 6;
        static final int TRANSACTION_scanPids = 10;
        static final int TRANSACTION_scanSupportedPids = 8;
        static final int TRANSACTION_scanVIN = 9;
        static final int TRANSACTION_startRawScan = 11;
        static final int TRANSACTION_updateDeviceFirmware = 5;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.obd.IObdService");
        }
        
        public static IObdService asInterface(final IBinder binder) {
            IObdService obdService;
            if (binder == null) {
                obdService = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.obd.IObdService");
                if (queryLocalInterface != null && queryLocalInterface instanceof IObdService) {
                    obdService = (IObdService)queryLocalInterface;
                }
                else {
                    obdService = new Proxy(binder);
                }
            }
            return obdService;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int state, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
            boolean onTransact = true;
            switch (state) {
                default:
                    onTransact = super.onTransact(state, parcel, parcel2, n);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.obd.IObdService");
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    this.connect(parcel.readString(), IObdServiceListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    break;
                case 2:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    this.disconnect();
                    parcel2.writeNoException();
                    break;
                case 3:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    state = this.getState();
                    parcel2.writeNoException();
                    parcel2.writeInt(state);
                    break;
                case 4: {
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    final String firmwareVersion = this.getFirmwareVersion();
                    parcel2.writeNoException();
                    parcel2.writeString(firmwareVersion);
                    break;
                }
                case 5: {
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    final boolean updateDeviceFirmware = this.updateDeviceFirmware(parcel.readString(), parcel.readString());
                    parcel2.writeNoException();
                    if (updateDeviceFirmware) {
                        state = 1;
                    }
                    else {
                        state = 0;
                    }
                    parcel2.writeInt(state);
                    break;
                }
                case 6:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    this.reset();
                    parcel2.writeNoException();
                    break;
                case 7: {
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    final String pid = this.readPid(parcel.readInt());
                    parcel2.writeNoException();
                    parcel2.writeString(pid);
                    break;
                }
                case 8:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    this.scanSupportedPids();
                    parcel2.writeNoException();
                    break;
                case 9:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    this.scanVIN();
                    parcel2.writeNoException();
                    break;
                case 10:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    this.scanPids(parcel.createTypedArrayList((Parcelable.Creator)Pid.CREATOR), parcel.readInt());
                    parcel2.writeNoException();
                    break;
                case 11:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    this.startRawScan();
                    parcel2.writeNoException();
                    break;
                case 12:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    this.addListener(IObdServiceListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    break;
                case 13:
                    parcel.enforceInterface("com.navdy.obd.IObdService");
                    this.removeListener(IObdServiceListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    break;
            }
            return onTransact;
        }
        
        private static class Proxy implements IObdService
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            @Override
            public void addListener(final IObdServiceListener obdServiceListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    IBinder binder;
                    if (obdServiceListener != null) {
                        binder = obdServiceListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void connect(final String s, final IObdServiceListener obdServiceListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    obtain.writeString(s);
                    IBinder binder;
                    if (obdServiceListener != null) {
                        binder = obdServiceListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void disconnect() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String getFirmwareVersion() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.obd.IObdService";
            }
            
            @Override
            public int getState() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String readPid(final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    obtain.writeInt(n);
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void removeListener(final IObdServiceListener obdServiceListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    IBinder binder;
                    if (obdServiceListener != null) {
                        binder = obdServiceListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void reset() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void scanPids(final List<Pid> list, final int n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    obtain.writeTypedList((List)list);
                    obtain.writeInt(n);
                    this.mRemote.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void scanSupportedPids() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void scanVIN() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void startRawScan() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    this.mRemote.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean updateDeviceFirmware(final String s, final String s2) throws RemoteException {
                boolean b = false;
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.obd.IObdService");
                    obtain.writeString(s);
                    obtain.writeString(s2);
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
