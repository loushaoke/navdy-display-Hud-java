package com.navdy.ancs;

import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import java.util.List;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.os.IInterface;

public interface IAncsService extends IInterface
{
    void addListener(final IAncsServiceListener p0) throws RemoteException;
    
    void connect(final String p0) throws RemoteException;
    
    void connectToDevice(final ParcelUuid p0, final String p1) throws RemoteException;
    
    void connectToService(final ParcelUuid p0) throws RemoteException;
    
    void disconnect() throws RemoteException;
    
    int getState() throws RemoteException;
    
    void performNotificationAction(final int p0, final int p1) throws RemoteException;
    
    void removeListener(final IAncsServiceListener p0) throws RemoteException;
    
    void setNotificationFilter(final List<String> p0, final long p1) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IAncsService
    {
        private static final String DESCRIPTOR = "com.navdy.ancs.IAncsService";
        static final int TRANSACTION_addListener = 1;
        static final int TRANSACTION_connect = 3;
        static final int TRANSACTION_connectToDevice = 9;
        static final int TRANSACTION_connectToService = 4;
        static final int TRANSACTION_disconnect = 5;
        static final int TRANSACTION_getState = 7;
        static final int TRANSACTION_performNotificationAction = 6;
        static final int TRANSACTION_removeListener = 2;
        static final int TRANSACTION_setNotificationFilter = 8;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.ancs.IAncsService");
        }
        
        public static IAncsService asInterface(final IBinder binder) {
            IAncsService ancsService;
            if (binder == null) {
                ancsService = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.ancs.IAncsService");
                if (queryLocalInterface != null && queryLocalInterface instanceof IAncsService) {
                    ancsService = (IAncsService)queryLocalInterface;
                }
                else {
                    ancsService = new Proxy(binder);
                }
            }
            return ancsService;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int state, final Parcel parcel, final Parcel parcel2, final int n) throws RemoteException {
            boolean onTransact = true;
            switch (state) {
                default:
                    onTransact = super.onTransact(state, parcel, parcel2, n);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.ancs.IAncsService");
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.ancs.IAncsService");
                    this.addListener(IAncsServiceListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    break;
                case 2:
                    parcel.enforceInterface("com.navdy.ancs.IAncsService");
                    this.removeListener(IAncsServiceListener.Stub.asInterface(parcel.readStrongBinder()));
                    parcel2.writeNoException();
                    break;
                case 3:
                    parcel.enforceInterface("com.navdy.ancs.IAncsService");
                    this.connect(parcel.readString());
                    parcel2.writeNoException();
                    break;
                case 4: {
                    parcel.enforceInterface("com.navdy.ancs.IAncsService");
                    ParcelUuid parcelUuid;
                    if (parcel.readInt() != 0) {
                        parcelUuid = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        parcelUuid = null;
                    }
                    this.connectToService(parcelUuid);
                    parcel2.writeNoException();
                    break;
                }
                case 5:
                    parcel.enforceInterface("com.navdy.ancs.IAncsService");
                    this.disconnect();
                    parcel2.writeNoException();
                    break;
                case 6:
                    parcel.enforceInterface("com.navdy.ancs.IAncsService");
                    this.performNotificationAction(parcel.readInt(), parcel.readInt());
                    parcel2.writeNoException();
                    break;
                case 7:
                    parcel.enforceInterface("com.navdy.ancs.IAncsService");
                    state = this.getState();
                    parcel2.writeNoException();
                    parcel2.writeInt(state);
                    break;
                case 8:
                    parcel.enforceInterface("com.navdy.ancs.IAncsService");
                    this.setNotificationFilter(parcel.createStringArrayList(), parcel.readLong());
                    parcel2.writeNoException();
                    break;
                case 9: {
                    parcel.enforceInterface("com.navdy.ancs.IAncsService");
                    ParcelUuid parcelUuid2;
                    if (parcel.readInt() != 0) {
                        parcelUuid2 = (ParcelUuid)ParcelUuid.CREATOR.createFromParcel(parcel);
                    }
                    else {
                        parcelUuid2 = null;
                    }
                    this.connectToDevice(parcelUuid2, parcel.readString());
                    parcel2.writeNoException();
                    break;
                }
            }
            return onTransact;
        }
        
        private static class Proxy implements IAncsService
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            @Override
            public void addListener(final IAncsServiceListener ancsServiceListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsService");
                    IBinder binder;
                    if (ancsServiceListener != null) {
                        binder = ancsServiceListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void connect(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsService");
                    obtain.writeString(s);
                    this.mRemote.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void connectToDevice(final ParcelUuid parcelUuid, final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsService");
                    if (parcelUuid != null) {
                        obtain.writeInt(1);
                        parcelUuid.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(s);
                    this.mRemote.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void connectToService(final ParcelUuid parcelUuid) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsService");
                    if (parcelUuid != null) {
                        obtain.writeInt(1);
                        parcelUuid.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    this.mRemote.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void disconnect() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsService");
                    this.mRemote.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.ancs.IAncsService";
            }
            
            @Override
            public int getState() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsService");
                    this.mRemote.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void performNotificationAction(final int n, final int n2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsService");
                    obtain.writeInt(n);
                    obtain.writeInt(n2);
                    this.mRemote.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void removeListener(final IAncsServiceListener ancsServiceListener) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsService");
                    IBinder binder;
                    if (ancsServiceListener != null) {
                        binder = ancsServiceListener.asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    this.mRemote.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void setNotificationFilter(final List<String> list, final long n) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.ancs.IAncsService");
                    obtain.writeStringList((List)list);
                    obtain.writeLong(n);
                    this.mRemote.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
}
