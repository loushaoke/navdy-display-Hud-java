package com.navdy.service.library.util;

import java.io.IOException;

class RuntimeTypeAdapterFactory$1 extends com.google.gson.TypeAdapter {
    final com.navdy.service.library.util.RuntimeTypeAdapterFactory this$0;
    final java.util.Map val$labelToDelegate;
    final java.util.Map val$subtypeToDelegate;
    
    RuntimeTypeAdapterFactory$1(com.navdy.service.library.util.RuntimeTypeAdapterFactory a, java.util.Map a0, java.util.Map a1) {
        super();
        this.this$0 = a;
        this.val$labelToDelegate = a0;
        this.val$subtypeToDelegate = a1;
    }
    
    public Object read(com.google.gson.stream.JsonReader a) {
        com.google.gson.JsonElement a0 = com.google.gson.internal.Streams.parse(a);
        com.google.gson.JsonElement a1 = a0.getAsJsonObject().remove(com.navdy.service.library.util.RuntimeTypeAdapterFactory.access$000(this.this$0));
        if (a1 == null) {
            throw new com.google.gson.JsonParseException(new StringBuilder().append("cannot deserialize ").append(com.navdy.service.library.util.RuntimeTypeAdapterFactory.access$100(this.this$0)).append(" because it does not define a field named ").append(com.navdy.service.library.util.RuntimeTypeAdapterFactory.access$000(this.this$0)).toString());
        }
        String s = a1.getAsString();
        com.google.gson.TypeAdapter a2 = (com.google.gson.TypeAdapter)this.val$labelToDelegate.get(s);
        if (a2 == null) {
            throw new com.google.gson.JsonParseException(new StringBuilder().append("cannot deserialize ").append(com.navdy.service.library.util.RuntimeTypeAdapterFactory.access$100(this.this$0)).append(" subtype named ").append(s).append("; did you forget to register a subtype?").toString());
        }
        return a2.fromJsonTree(a0);
    }
    
    public void write(com.google.gson.stream.JsonWriter a, Object a0) throws IOException {
        Class a1 = a0.getClass();
        String s = (String)com.navdy.service.library.util.RuntimeTypeAdapterFactory.access$200(this.this$0).get(a1);
        com.google.gson.TypeAdapter a2 = (com.google.gson.TypeAdapter)this.val$subtypeToDelegate.get(a1);
        if (a2 == null) {
            throw new com.google.gson.JsonParseException(new StringBuilder().append("cannot serialize ").append(a1.getName()).append("; did you forget to register a subtype?").toString());
        }
        com.google.gson.JsonObject a3 = a2.toJsonTree(a0).getAsJsonObject();
        if (a3.has(com.navdy.service.library.util.RuntimeTypeAdapterFactory.access$000(this.this$0))) {
            throw new com.google.gson.JsonParseException(new StringBuilder().append("cannot serialize ").append(a1.getName()).append(" because it already defines a field named ").append(com.navdy.service.library.util.RuntimeTypeAdapterFactory.access$000(this.this$0)).toString());
        }
        com.google.gson.JsonObject a4 = new com.google.gson.JsonObject();
        a4.add(com.navdy.service.library.util.RuntimeTypeAdapterFactory.access$000(this.this$0), (com.google.gson.JsonElement)new com.google.gson.JsonPrimitive(s));
        Object a5 = a3.entrySet().iterator();
        while(((java.util.Iterator)a5).hasNext()) {
            Object a6 = ((java.util.Iterator)a5).next();
            a4.add((String)((java.util.Map.Entry)a6).getKey(), (com.google.gson.JsonElement)((java.util.Map.Entry)a6).getValue());
        }
        com.google.gson.internal.Streams.write((com.google.gson.JsonElement)a4, a);
    }
}
