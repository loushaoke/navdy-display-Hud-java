package com.navdy.service.library.util;

public class SystemUtils$CpuInfo {
    private java.util.ArrayList plist;
    private int system;
    private int usr;
    
    public SystemUtils$CpuInfo(int i, int i0, java.util.ArrayList a) {
        this.usr = i;
        this.system = i0;
        this.plist = a;
    }
    
    public int getCpuSystem() {
        return this.system;
    }
    
    public int getCpuUser() {
        return this.usr;
    }
    
    public java.util.ArrayList getList() {
        return this.plist;
    }
}
