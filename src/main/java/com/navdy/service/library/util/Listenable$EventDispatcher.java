package com.navdy.service.library.util;

abstract public interface Listenable$EventDispatcher {
    abstract public void dispatchEvent(com.navdy.service.library.util.Listenable arg, com.navdy.service.library.util.Listenable$Listener arg0);
}

/*
abstract public interface Listenable$EventDispatcher<U extends Listenable, T extends Connection.Listener> {
    abstract public void dispatchEvent(U u, T t);
//    abstract public void dispatchEvent(com.navdy.service.library.util.Listenable arg, com.navdy.service.library.util.Listenable$Listener arg0);
}

 */