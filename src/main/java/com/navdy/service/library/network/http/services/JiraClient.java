package com.navdy.service.library.network.http.services;

import android.text.TextUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.http.HttpUtils;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.io.IOException;
import java.util.List;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;

public class JiraClient {
    public static final String ASSIGNEE = "assignee";
    public static final String DESCRIPTION = "description";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String ENVIRONMENT = "environment";
    public static final String FIELDS = "fields";
    public static final String ISSUE_TYPE = "issuetype";
    public static final String ISSUE_TYPE_BUG = "Bug";
    public static final String JIRA_API_SEARCH_ASSIGNABLE_USER = "https://navdyhud.atlassian.net/rest/api/2/user/assignable/search";
    private static final String JIRA_ATTACHMENTS = "/attachments/";
    private static final String JIRA_ISSUE_API_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    private static final String JIRA_ISSUE_EDIT_URL = "https://navdyhud.atlassian.net/rest/api/2/issue/";
    public static final String KEY = "key";
    public static final String NAME = "name";
    public static final String PROJECT = "project";
    public static final String SUMMARY = "summary";
    private static final Logger sLogger = new Logger(JiraClient.class);
    private String encodedCredentials;
    private OkHttpClient mClient;

    public static class Attachment {
        public String filePath = "";
        public String mimeType = "";
    }

    public interface ResultCallback {
        void onError(Throwable th);

        void onSuccess(Object obj);
    }

    public JiraClient(OkHttpClient httpClient) {
        this.mClient = httpClient;
    }

    public void assignTicketForName(final String ticketId, String name, final String emailAddress, final ResultCallback callback) {
        Builder requestBuilder = new Builder().url("https://navdyhud.atlassian.net/rest/api/2/user/assignable/search?issueKey=" + ticketId + "&username=" + name).get();
        if (!TextUtils.isEmpty(this.encodedCredentials)) {
            requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
        }
        this.mClient.newCall(requestBuilder.build()).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                JiraClient.sLogger.e("onFailure ", e);
                if (callback != null) {
                    callback.onError(e);
                }
            }

            public void onResponse(Call call, Response response) {
                try {
                    int responseCode = response.code();
                    if (responseCode == 200) {
                        JSONArray resultsArray = new JSONArray(response.body().string());
                        if (resultsArray.length() > 0) {
                            String userName = "";
                            for (int i = 0; i < resultsArray.length(); i++) {
                                JSONObject userObject = (JSONObject) resultsArray.get(i);
                                if (i == 0) {
                                    userName = userObject.getString("name");
                                }
                                if (userObject.has(JiraClient.EMAIL_ADDRESS)) {
                                    String emailId = userObject.getString(JiraClient.EMAIL_ADDRESS);
                                    JiraClient.sLogger.d("Email Id " + emailId);
                                    if (TextUtils.equals(emailId, emailAddress) && userObject.has("name")) {
                                        userName = userObject.getString("name");
                                        JiraClient.sLogger.d("User name " + userName);
                                        break;
                                    }
                                }
                            }
                            JiraClient.sLogger.d("Assigning the ticket to User " + userName);
                            JiraClient.this.assignTicketToUser(ticketId, userName, callback);
                        }
                    } else if (callback != null) {
                        callback.onError(new IOException("Jira request failed with " + responseCode));
                    }
                } catch (Throwable t) {
                    if (callback != null) {
                        callback.onError(t);
                    }
                }
            }
        });
    }

    public void assignTicketToUser(String ticketId, final String user, final ResultCallback callback) {
        try {
            JSONObject data = new JSONObject();
            JSONObject fields = new JSONObject();
            JSONObject assignee = new JSONObject();
            assignee.put("name", user);
            fields.put("assignee", assignee);
            data.put(FIELDS, fields);
            Builder requestBuilder = new Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue//" + ticketId).put(HttpUtils.getJsonRequestBody(data.toString()));
            if (!TextUtils.isEmpty(this.encodedCredentials)) {
                requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
            }
            this.mClient.newCall(requestBuilder.build()).enqueue(new Callback() {
                public void onFailure(Call call, IOException e) {
                    JiraClient.sLogger.e("onFailure ", e);
                    if (callback != null) {
                        callback.onError(e);
                    }
                }

                public void onResponse(Call call, Response response) {
                    try {
                        int responseCode = response.code();
                        if (responseCode != 200 && responseCode != 204) {
                            JiraClient.sLogger.d("Response data " + response.body().string());
                            if (callback != null) {
                                callback.onError(new IOException("Jira request failed with " + responseCode));
                            }
                        } else if (callback != null) {
                            callback.onSuccess(user);
                        }
                    } catch (Throwable t) {
                        JiraClient.sLogger.e("Exception paring response ", t);
                        if (callback != null) {
                            callback.onError(t);
                        }
                    }
                }
            });
        } catch (Throwable t) {
            callback.onError(t);
        } finally {
            IOUtils.closeStream(null);
        }
    }

    public void submitTicket(String projectName, String issueTypeName, String summary, String description, ResultCallback callback) throws IOException {
        submitTicket(projectName, issueTypeName, summary, description, null, callback);
    }

    public void submitTicket(String projectName, String issueTypeName, String summary, String description, String environment, ResultCallback callback) {
        try {
            JSONObject data = new JSONObject();
            JSONObject fields = new JSONObject();
            JSONObject project = new JSONObject();
            JSONObject issueType = new JSONObject();
            project.put("key", projectName);
            fields.put(PROJECT, project);
            issueType.put("name", issueTypeName);
            fields.put(ISSUE_TYPE, issueType);
            fields.put(SUMMARY, summary);
            if (!TextUtils.isEmpty(environment)) {
                fields.put(ENVIRONMENT, environment);
            }
            fields.put(DESCRIPTION, description);
            data.put(FIELDS, fields);
            Builder requestBuilder = new Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue/").post(HttpUtils.getJsonRequestBody(data.toString()));
            if (!TextUtils.isEmpty(this.encodedCredentials)) {
                requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
            }
            final ResultCallback resultCallback = callback;
            this.mClient.newCall(requestBuilder.build()).enqueue(new Callback() {
                public void onFailure(Call call, IOException e) {
                    if (resultCallback != null) {
                        resultCallback.onError(e);
                    }
                }

                public void onResponse(Call call, Response response) {
                    try {
                        int responseCode = response.code();
                        if (responseCode == 200 || responseCode == 201) {
                            String key = new JSONObject(response.body().string()).getString("key");
                            if (resultCallback != null) {
                                resultCallback.onSuccess(key);
                            }
                        } else if (resultCallback != null) {
                            resultCallback.onError(new IOException("Jira request failed with " + responseCode));
                        }
                    } catch (Throwable t) {
                        if (resultCallback != null) {
                            resultCallback.onError(t);
                        }
                    }
                }
            });
        } catch (Throwable t) {
            callback.onError(t);
        } finally {
            IOUtils.closeStream(null);
        }
    }

    public void setEncodedCredentials(String encodedCredentials) {
        this.encodedCredentials = encodedCredentials;
    }

    public void attachFilesToTicket(final String ticketId, List<Attachment> attachments, final ResultCallback callback) {
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);
        for (Attachment attachment : attachments) {
            File file = new File(attachment.filePath);
            if (!file.exists()) {
                throw new IllegalArgumentException("Attachment " + file.getName() + "File does not exists");
            } else if (file.canRead()) {
                builder.addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse(attachment.mimeType), file));
            } else {
                throw new IllegalArgumentException("Attachment " + file.getName() + "File cannot be read");
            }
        }
        Builder requestBuilder = new Builder().url("https://navdyhud.atlassian.net/rest/api/2/issue/" + ticketId + JIRA_ATTACHMENTS).addHeader("X-Atlassian-Token", "nocheck").post(builder.build());
        if (!TextUtils.isEmpty(this.encodedCredentials)) {
            requestBuilder.addHeader("Authorization", "Basic " + this.encodedCredentials);
        }
        this.mClient.newCall(requestBuilder.build()).enqueue(new Callback() {
            public void onFailure(Call call, IOException e) {
                if (callback != null) {
                    callback.onError(e);
                }
            }

            public void onResponse(Call call, Response response) {
                try {
                    int responseCode = response.code();
                    if ((responseCode == 200 || responseCode == 201) && callback != null) {
                        callback.onSuccess(ticketId);
                    }
                } catch (Throwable t) {
                    if (callback != null) {
                        callback.onError(t);
                    }
                }
            }
        });
    }
}
