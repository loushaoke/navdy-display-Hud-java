package com.navdy.service.library.events.file;

final public class PreviewFileResponse extends com.squareup.wire.Message {
    final public static String DEFAULT_FILENAME = "";
    final public static com.navdy.service.library.events.RequestStatus DEFAULT_STATUS;
    final public static String DEFAULT_STATUS_DETAIL = "";
    final private static long serialVersionUID = 0L;
    final public String filename;
    final public com.navdy.service.library.events.RequestStatus status;
    final public String status_detail;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS;
    }
    
    public PreviewFileResponse(com.navdy.service.library.events.RequestStatus a, String s, String s0) {
        this.status = a;
        this.status_detail = s;
        this.filename = s0;
    }
    
    private PreviewFileResponse(com.navdy.service.library.events.file.PreviewFileResponse$Builder a) {
        this(a.status, a.status_detail, a.filename);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    PreviewFileResponse(com.navdy.service.library.events.file.PreviewFileResponse$Builder a, com.navdy.service.library.events.file.PreviewFileResponse$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.file.PreviewFileResponse) {
                com.navdy.service.library.events.file.PreviewFileResponse a0 = (com.navdy.service.library.events.file.PreviewFileResponse)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.status_detail, a0.status_detail)) {
                        break label1;
                    }
                    if (this.equals(this.filename, a0.filename)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.status_detail == null) ? 0 : this.status_detail.hashCode())) * 37 + ((this.filename == null) ? 0 : this.filename.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
