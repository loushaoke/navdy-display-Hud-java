package com.navdy.service.library.events.file;

final public class FileListResponse$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.file.FileType file_type;
    public java.util.List files;
    public com.navdy.service.library.events.RequestStatus status;
    public String status_detail;
    
    public FileListResponse$Builder() {
    }
    
    public FileListResponse$Builder(com.navdy.service.library.events.file.FileListResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.status_detail = a.status_detail;
            this.file_type = a.file_type;
            this.files = com.navdy.service.library.events.file.FileListResponse.access$000(a.files);
        }
    }
    
    public com.navdy.service.library.events.file.FileListResponse build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.file.FileListResponse(this, (com.navdy.service.library.events.file.FileListResponse$1)null);
    }
    
//    public com.squareup.wire.Message build() {
//        return this.build();
//    }
    
    public com.navdy.service.library.events.file.FileListResponse$Builder file_type(com.navdy.service.library.events.file.FileType a) {
        this.file_type = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileListResponse$Builder files(java.util.List a) {
        this.files = com.navdy.service.library.events.file.FileListResponse$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.file.FileListResponse$Builder status(com.navdy.service.library.events.RequestStatus a) {
        this.status = a;
        return this;
    }
    
    public com.navdy.service.library.events.file.FileListResponse$Builder status_detail(String s) {
        this.status_detail = s;
        return this;
    }
}
