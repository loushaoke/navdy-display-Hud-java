package com.navdy.service.library.device.link;

public class ReaderThread extends com.navdy.service.library.device.link.IOThread {
    final private com.navdy.service.library.device.connection.ConnectionType connectionType;
    private boolean isNetworkLinkReadyByDefault;
    final private com.navdy.service.library.device.link.LinkListener linkListener;
    final private com.navdy.service.library.events.NavdyEventReader mmEventReader;
    private java.io.InputStream mmInStream;
    
    public ReaderThread(com.navdy.service.library.device.connection.ConnectionType a, java.io.InputStream a0, com.navdy.service.library.device.link.LinkListener a1, boolean b) {
        this.isNetworkLinkReadyByDefault = false;
        this.setName("ReaderThread");
        this.logger.d("create ReaderThread");
        this.mmInStream = a0;
        this.mmEventReader = new com.navdy.service.library.events.NavdyEventReader(this.mmInStream);
        this.linkListener = a1;
        this.connectionType = a;
        this.isNetworkLinkReadyByDefault = b;
    }
    
    public void cancel() {
        super.cancel();
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mmInStream);
        this.mmInStream = null;
    }
    
    public void run() {
        this.logger.i("BEGIN");
        com.navdy.service.library.device.connection.Connection$DisconnectCause a = com.navdy.service.library.device.connection.Connection$DisconnectCause.NORMAL;
        this.linkListener.linkEstablished(this.connectionType);
        if (this.isNetworkLinkReadyByDefault) {
            this.linkListener.onNetworkLinkReady();
        }
        while(true) {
            boolean b = this.closing;
            label0: {
                Exception a0 = null;
                if (b) {
                    break label0;
                }
                label1: {
                    try {
                        if (this.logger.isLoggable(3)) {
                            this.logger.v("before read");
                        }
                        byte[] a1 = this.mmEventReader.readBytes();
                        if (a1 != null && a1.length > 524288) {
                            try {
                                throw new RuntimeException(new StringBuilder().append("reader Max packet size exceeded [").append(a1.length).append("] bytes[").append(com.navdy.service.library.util.IOUtils.bytesToHexString(a1, 0, 50)).append("]").toString());
                            } catch(Exception a2) {
                                a0 = a2;
                                break label1;
                            }
                        }
                        if (this.logger.isLoggable(3)) {
                            this.logger.v(new StringBuilder().append("after read len:").append((a1 != null) ? a1.length : -1).toString());
                        }
                        if (a1 != null) {
                            this.linkListener.onNavdyEventReceived(a1);
                            continue;
                        }
                    } catch(Exception a3) {
                        a0 = a3;
                        break label1;
                    }
                    try {
                        if (this.closing) {
                            break label0;
                        }
                        this.logger.e("no event data parsed. assuming disconnect.");
                        a = com.navdy.service.library.device.connection.Connection$DisconnectCause.ABORTED;
                        break label0;
                    } catch(Exception a4) {
                        a0 = a4;
                    }
                }
                if (!this.closing) {
                    this.logger.e(new StringBuilder().append("disconnected: ").append(a0.getMessage()).toString());
                    a = com.navdy.service.library.device.connection.Connection$DisconnectCause.ABORTED;
                }
            }
            this.logger.i(new StringBuilder().append("Signaling connection lost cause:").append(a).toString());
            this.linkListener.linkLost(this.connectionType, a);
            this.logger.i("Exiting thread");
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mmInStream);
            this.mmInStream = null;
            return;
        }
    }
}
