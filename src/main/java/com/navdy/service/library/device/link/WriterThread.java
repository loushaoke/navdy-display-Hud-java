package com.navdy.service.library.device.link;

public class WriterThread extends com.navdy.service.library.device.link.IOThread {
    final private com.navdy.service.library.device.link.WriterThread$EventProcessor mEventProcessor;
    final private java.util.concurrent.LinkedBlockingDeque mEventQueue;
    final private com.navdy.service.library.events.NavdyEventWriter mmEventWriter;
    private java.io.OutputStream mmOutStream;
    
    public WriterThread(java.util.concurrent.LinkedBlockingDeque a, java.io.OutputStream a0, com.navdy.service.library.device.link.WriterThread$EventProcessor a1) {
        this.setName("WriterThread");
        this.logger.d("create WriterThread");
        this.mmOutStream = a0;
        this.mmEventWriter = new com.navdy.service.library.events.NavdyEventWriter(this.mmOutStream);
        this.mEventQueue = a;
        this.mEventProcessor = a1;
    }
    
    public void cancel() {
        super.cancel();
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.mmOutStream);
        this.mmOutStream = null;
    }
    
    public void run() {
        this.logger.i("starting WriterThread loop.");
        while(true) {
            if (!this.closing) {
                com.navdy.service.library.device.link.EventRequest a = null;
                try {
                    a = (com.navdy.service.library.device.link.EventRequest)this.mEventQueue.take();
                } catch(InterruptedException ignoredException) {
                    this.logger.i("WriterThread interrupted");
                    a = null;
                }
                if (!this.closing) {
                    if (a != null) {
                        boolean b = false;
                        byte[] a0 = a.eventData;
                        if (a0.length > 524288) {
                            throw new RuntimeException(new StringBuilder().append("writer Max packet size exceeded [").append(a0.length).append("] bytes[").append(com.navdy.service.library.util.IOUtils.bytesToHexString(a0, 0, 50)).append("]").toString());
                        }
                        if (this.mEventProcessor != null) {
                            a0 = this.mEventProcessor.processEvent(a0);
                        }
                        if (a0 == null) {
                            continue;
                        }
                        try {
                            this.mmEventWriter.write(a.eventData);
                            b = true;
                        } catch(java.io.IOException a1) {
                            if (this.closing) {
                                b = false;
                            } else {
                                this.logger.e(new StringBuilder().append("Error writing event:").append(a1.getMessage()).toString());
                                b = false;
                            }
                        }
                        if (b) {
                            continue;
                        }
                        this.logger.i("send failed");
                        a.callCompletionHandlers(com.navdy.service.library.device.RemoteDevice$PostEventStatus.SEND_FAILED);
                        continue;
                    } else {
                        this.logger.e("WriterThread: unable to retrieve event from queue");
                        continue;
                    }
                }
            }
            this.logger.i("Writer thread ending");
            return;
        }
    }
}
