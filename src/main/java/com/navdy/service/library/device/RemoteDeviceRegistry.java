package com.navdy.service.library.device;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.navdy.service.library.device.connection.ConnectionInfo;

import java.util.List;

public class RemoteDeviceRegistry extends com.navdy.service.library.util.Listenable implements com.navdy.service.library.device.discovery.RemoteDeviceScanner$Listener {
    final public static int MAX_PAIRED_DEVICES = 5;
    final public static String PREFS_FILE_DEVICE_REGISTRY = "DeviceRegistry";
    final public static String PREFS_KEY_DEFAULT_CONNECTION_INFO = "DefaultConnectionInfo";
    final public static String PREFS_KEY_PAIRED_CONNECTION_INFOS = "PairedConnectionInfos";
    final public static com.navdy.service.library.log.Logger sLogger;
    private static com.navdy.service.library.device.RemoteDeviceRegistry sRemoteDeviceRegistry = null;
    protected android.content.Context mContext;
    protected com.google.gson.Gson mGson;
    protected java.util.Set mKnownConnectionInfo;
    protected java.util.HashSet mRemoteDeviceScanners;
    protected android.content.SharedPreferences mSharedPrefs;
    private java.util.ArrayList<com.navdy.service.library.device.connection.ConnectionInfo> pairedConnections;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.RemoteDeviceRegistry.class);
    }
    
    private RemoteDeviceRegistry(android.content.Context a) {
        this.mContext = a;
        this.mGson = new com.google.gson.GsonBuilder().registerTypeAdapterFactory((com.google.gson.TypeAdapterFactory)com.navdy.service.library.device.connection.ConnectionInfo.connectionInfoAdapter).create();
        this.mKnownConnectionInfo = (java.util.Set)new java.util.HashSet();
        this.mRemoteDeviceScanners = new java.util.HashSet();
        this.loadPairedConnections();
    }
    
    public static com.navdy.service.library.device.RemoteDeviceRegistry getInstance(android.content.Context a) {
        if (sRemoteDeviceRegistry == null) {
            sRemoteDeviceRegistry = new com.navdy.service.library.device.RemoteDeviceRegistry(a.getApplicationContext());
        }
        return sRemoteDeviceRegistry;
    }
    
    private void loadPairedConnections() {
        this.mSharedPrefs = this.mContext.getSharedPreferences(PREFS_FILE_DEVICE_REGISTRY, Context.MODE_MULTI_PROCESS);

        java.util.ArrayList<com.navdy.service.library.device.connection.ConnectionInfo> a = new java.util.ArrayList();
        try {
            String s = this.mSharedPrefs.getString(PREFS_KEY_PAIRED_CONNECTION_INFOS, (String)null);
            java.lang.reflect.Type a0 = new TypeToken<List<ConnectionInfo>>() {}.getType();
            if (s != null) {
                java.util.ArrayList<com.navdy.service.library.device.connection.ConnectionInfo> loaded = this.mGson.fromJson(s, a0);
                for (com.navdy.service.library.device.connection.ConnectionInfo c : loaded){
                    if (c == null) { continue; }
                    if (c.getAddress() == null) { continue; }
                    if (c.getDeviceId() == null) { continue; }
                    if (c.getType() == null) { continue; }
                    a.add(c);
                }
            }
        } catch(com.google.gson.JsonParseException a1) {
            sLogger.e("Unable to read connection infos: ", (Throwable)a1);
        }
        sLogger.v(new StringBuilder().append("Read pairing list of:").append(java.util.Arrays.toString(a.toArray())).toString());
        this.pairedConnections = a;
    }
    
    public void addDiscoveredConnectionInfo(com.navdy.service.library.device.connection.ConnectionInfo a) {
        if (this.mKnownConnectionInfo.contains(a)) {
            sLogger.d(new StringBuilder().append("Already contains: ").append(a).toString());
        } else {
            this.mKnownConnectionInfo.add(a);
            this.sendDeviceListUpdate();
        }
    }
    
    public void addPairedConnection(com.navdy.service.library.device.connection.ConnectionInfo a) {
        this.pairedConnections.remove(a);
        if (this.pairedConnections.size() >= 5) {
            this.pairedConnections.remove(4);
        }
        this.pairedConnections.add(0, a);
        this.savePairedConnections();
    }
    
    public void addRemoteDeviceScanner(com.navdy.service.library.device.discovery.RemoteDeviceScanner a) {
        a.addListener((com.navdy.service.library.util.Listenable$Listener)this);
        this.mRemoteDeviceScanners.add(a);
    }
    
    public com.navdy.service.library.device.connection.ConnectionInfo findDevice(com.navdy.service.library.device.NavdyDeviceId a) {
        Object a0 = this.pairedConnections.listIterator();
        while(true) {
            com.navdy.service.library.device.connection.ConnectionInfo a1 = null;
            if (((java.util.Iterator)a0).hasNext()) {
                a1 = (com.navdy.service.library.device.connection.ConnectionInfo)((java.util.Iterator)a0).next();
                if (!a1.getDeviceId().equals(a)) {
                    continue;
                }
            } else {
                a1 = null;
            }
            return a1;
        }
    }
    
    public com.navdy.service.library.device.connection.ConnectionInfo getDefaultConnectionInfo() {
        com.navdy.service.library.device.connection.ConnectionInfo a = null;
        label1: {
            label0: {
                try {
                    String s = this.mSharedPrefs.getString("DefaultConnectionInfo", (String)null);
                    if (s == null) {
                        break label0;
                    }
                    a = (com.navdy.service.library.device.connection.ConnectionInfo)this.mGson.fromJson(s, com.navdy.service.library.device.connection.ConnectionInfo.class);
                } catch(com.google.gson.JsonParseException a0) {
                    sLogger.e("Unable to read connection info: ", (Throwable)a0);
                    a = null;
                }
                break label1;
            }
            a = null;
        }
        return a;
    }
    
    public java.util.Set getKnownConnectionInfo() {
        return this.mKnownConnectionInfo;
    }
    
    public com.navdy.service.library.device.connection.ConnectionInfo getLastPairedDevice() {
        com.navdy.service.library.device.connection.ConnectionInfo a = null;
        try {
            a = (this.pairedConnections.size() <= 0) ? null : (com.navdy.service.library.device.connection.ConnectionInfo)this.pairedConnections.get(0);
        } catch(Throwable ignoredException) {
            a = null;
        }
        return a;
    }
    
    public java.util.List getPairedConnections() {
        return (java.util.List)this.pairedConnections;
    }
    
    public boolean hasPaired() {
        return this.pairedConnections.size() > 0;
    }
    
    public void onDiscovered(com.navdy.service.library.device.discovery.RemoteDeviceScanner a, java.util.List a0) {
        Object a1 = a0.iterator();
        while(((java.util.Iterator)a1).hasNext()) {
            com.navdy.service.library.device.connection.ConnectionInfo a2 = (com.navdy.service.library.device.connection.ConnectionInfo)((java.util.Iterator)a1).next();
            if (a2 != null) {
                this.addDiscoveredConnectionInfo(a2);
                if (!(a2 instanceof com.navdy.service.library.device.connection.BTConnectionInfo) && a2.getDeviceId().getBluetoothAddress() != null) {
                    this.addDiscoveredConnectionInfo((com.navdy.service.library.device.connection.ConnectionInfo)new com.navdy.service.library.device.connection.BTConnectionInfo(a2.getDeviceId(), new com.navdy.service.library.device.connection.ServiceAddress(a2.getDeviceId().getBluetoothAddress(), com.navdy.service.library.device.connection.ConnectionService.NAVDY_PROTO_SERVICE_UUID.toString()), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF));
                }
            } else {
                sLogger.e("missing connection info.");
            }
        }
    }
    
    public void onLost(com.navdy.service.library.device.discovery.RemoteDeviceScanner a, java.util.List a0) {
    }
    
    public void onScanStarted(com.navdy.service.library.device.discovery.RemoteDeviceScanner a) {
        sLogger.e(new StringBuilder().append("Scan started: ").append((a).toString()).toString());
    }
    
    public void onScanStopped(com.navdy.service.library.device.discovery.RemoteDeviceScanner a) {
        sLogger.e(new StringBuilder().append("Scan stopped: ").append((a).toString()).toString());
    }
    
    public void refresh() {
        sLogger.i("refreshing paired connections info");
        this.loadPairedConnections();
    }
    
    public void removePairedConnection(android.bluetooth.BluetoothDevice a) {
        Object a0 = this.pairedConnections.listIterator();
        while(((java.util.Iterator)a0).hasNext()) {
            com.navdy.service.library.device.connection.ConnectionInfo a1 = (com.navdy.service.library.device.connection.ConnectionInfo)((java.util.Iterator)a0).next();
            if (a1 instanceof com.navdy.service.library.device.connection.BTConnectionInfo && a1.getAddress().getAddress().equals(a.getAddress())) {
                ((java.util.Iterator)a0).remove();
            }
        }
        this.savePairedConnections();
    }
    
    public void removePairedConnection(com.navdy.service.library.device.connection.ConnectionInfo a) {
        this.pairedConnections.remove(a);
        this.savePairedConnections();
    }
    
    public void removeRemoteDeviceScanner(com.navdy.service.library.device.discovery.RemoteDeviceScanner a) {
        a.removeListener((com.navdy.service.library.util.Listenable$Listener)this);
        this.mRemoteDeviceScanners.remove(a);
    }

    public void savePairedConnections() {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_PAIRED_CONNECTION_INFOS, this.mGson.toJson(this.pairedConnections, new TypeToken<List<ConnectionInfo>>() {
            }.getType())).commit();
        } catch (Exception e) {
            sLogger.e("Exception saving paired connections", e);
        }
    }
    
    protected void sendDeviceListUpdate() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.RemoteDeviceRegistry$1(this));
    }
    
    public void setDefaultConnectionInfo(com.navdy.service.library.device.connection.ConnectionInfo a) {
        try {
            String s = this.mGson.toJson(a, (java.lang.reflect.Type)com.navdy.service.library.device.connection.ConnectionInfo.class);
            this.mSharedPrefs.edit().putString("DefaultConnectionInfo", s).apply();
        } catch(Exception a0) {
            sLogger.e(new StringBuilder().append("Exception ").append(a0).toString());
        }
    }
    
    public void startScanning() {
        this.mKnownConnectionInfo.clear();
        Object a = this.mRemoteDeviceScanners.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            ((com.navdy.service.library.device.discovery.RemoteDeviceScanner)((java.util.Iterator)a).next()).startScan();
        }
    }
    
    public void stopScanning() {
        Object a = this.mRemoteDeviceScanners.iterator();
        while(((java.util.Iterator)a).hasNext()) {
            ((com.navdy.service.library.device.discovery.RemoteDeviceScanner)((java.util.Iterator)a).next()).stopScan();
        }
    }
}
