package com.navdy.service.library.device.connection;

public class ServiceAddress {
    final private String address;
    final private String protocol;
    final private String service;
    
    public ServiceAddress(String s, String s0) {
        this.address = s;
        this.service = s0;
        this.protocol = "NA";
    }
    
    public ServiceAddress(String s, String s0, String s1) {
        this.address = s;
        this.service = s0;
        this.protocol = s1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (this != a) {
            if (a == null) {
                b = false;
            } else if ((this).getClass() != a.getClass()) {
                b = false;
            } else {
                com.navdy.service.library.device.connection.ServiceAddress a0 = (com.navdy.service.library.device.connection.ServiceAddress)a;
                b = this.address.equals(a0.address) && this.service.equals(a0.service) && this.protocol.equals(a0.protocol);
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public String getProtocol() {
        return this.protocol;
    }
    
    public String getService() {
        return this.service;
    }
    
    public int hashCode() {
        return (this.address.hashCode() * 31 + this.service.hashCode()) * 31 + this.protocol.hashCode();
    }
    
    public String toString() {
        return new StringBuilder().append(this.address).append("/").append(this.service).append("/").append(this.protocol).toString();
    }
}
