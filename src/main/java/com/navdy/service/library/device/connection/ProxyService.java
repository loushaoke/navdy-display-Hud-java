package com.navdy.service.library.device.connection;

abstract public class ProxyService extends Thread {
    public ProxyService() {
    }
    
    abstract public void cancel();
}
