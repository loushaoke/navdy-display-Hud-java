package com.navdy.service.library.device.connection;

class ConnectionService$2 implements Runnable {
    final com.navdy.service.library.device.connection.ConnectionService this$0;
    
    ConnectionService$2(com.navdy.service.library.device.connection.ConnectionService a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (this.this$0.state == com.navdy.service.library.device.connection.ConnectionService$State.RECONNECTING && this.this$0.mRemoteDevice != null && !this.this$0.mRemoteDevice.isConnecting() && !this.this$0.mRemoteDevice.isConnected()) {
            this.this$0.logger.i(new StringBuilder().append("Retrying to connect to ").append(this.this$0.mRemoteDevice.getDeviceId()).toString());
            this.this$0.mRemoteDevice.connect();
        }
    }
}
