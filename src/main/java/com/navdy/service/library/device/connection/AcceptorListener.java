package com.navdy.service.library.device.connection;

public class AcceptorListener extends com.navdy.service.library.device.connection.ConnectionListener {
    private com.navdy.service.library.network.SocketAcceptor acceptor;
    private com.navdy.service.library.device.connection.ConnectionType connectionType;
    
    public AcceptorListener(android.content.Context a, com.navdy.service.library.network.SocketAcceptor a0, com.navdy.service.library.device.connection.ConnectionType a1) {
        super(a, new StringBuilder().append("Acceptor/").append((a0).getClass().getSimpleName()).toString());
        this.acceptor = a0;
        this.connectionType = a1;
    }
    
    static com.navdy.service.library.network.SocketAcceptor access$000(com.navdy.service.library.device.connection.AcceptorListener a) {
        return a.acceptor;
    }
    
    static com.navdy.service.library.device.connection.ConnectionType access$100(com.navdy.service.library.device.connection.AcceptorListener a) {
        return a.connectionType;
    }
    
    protected com.navdy.service.library.device.connection.AcceptorListener$AcceptThread getNewAcceptThread() {
        return new com.navdy.service.library.device.connection.AcceptorListener$AcceptThread(this);
    }
    
//    protected com.navdy.service.library.device.connection.ConnectionListener$AcceptThread getNewAcceptThread() {
//        return this.getNewAcceptThread();
//    }
    
    public com.navdy.service.library.device.connection.ConnectionType getType() {
        return this.connectionType;
    }
    
    public String toString() {
        return new StringBuilder().append((this).getClass().getSimpleName()).append((this.acceptor == null) ? "" : new StringBuilder().append("/").append((this.acceptor).getClass().getSimpleName()).toString()).toString();
    }
}
