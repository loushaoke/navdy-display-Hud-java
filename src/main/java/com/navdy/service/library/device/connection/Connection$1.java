package com.navdy.service.library.device.connection;

final class Connection$1 implements com.navdy.service.library.device.connection.Connection$ConnectionFactory {
    Connection$1() {
    }
    
    public com.navdy.service.library.device.connection.Connection build(android.content.Context a, com.navdy.service.library.device.connection.ConnectionInfo a0) {
        com.navdy.service.library.device.connection.SocketConnection a1 = null;
        switch(com.navdy.service.library.device.connection.Connection$5.$SwitchMap$com$navdy$service$library$device$connection$ConnectionType[a0.getType().ordinal()]) {
            case 5: {
                a1 = new com.navdy.service.library.device.connection.SocketConnection(a0, (com.navdy.service.library.network.SocketFactory)new com.navdy.service.library.network.TCPSocketFactory(a0.getAddress()));
                break;
            }
            case 1: case 2: case 3: case 4: {
                com.navdy.service.library.device.connection.ServiceAddress a2 = a0.getAddress();
                if (a2 == null) {
                    throw new IllegalArgumentException(new StringBuilder().append("Unknown address: ").append(a0.getType()).toString());
                }
                if (a2.getService().equals(com.navdy.service.library.device.connection.ConnectionService.ACCESSORY_IAP2.toString())) {
                    a2 = new com.navdy.service.library.device.connection.ServiceAddress(a2.getAddress(), com.navdy.service.library.device.connection.ConnectionService.DEVICE_IAP2.toString(), a2.getProtocol());
                }
                a1 = new com.navdy.service.library.device.connection.SocketConnection(a0, (com.navdy.service.library.network.SocketFactory)new com.navdy.service.library.network.BTSocketFactory(a2));
                break;
            }
            default: {
                throw new IllegalArgumentException(new StringBuilder().append("Unknown connection class for type: ").append(a0.getType()).toString());
            }
        }
        return a1;
    }
}
