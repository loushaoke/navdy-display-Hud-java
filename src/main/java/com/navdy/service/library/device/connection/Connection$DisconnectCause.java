package com.navdy.service.library.device.connection;


public enum Connection$DisconnectCause {
    UNKNOWN(0),
    NORMAL(1),
    ABORTED(2);

    private int value;
    Connection$DisconnectCause(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class Connection$DisconnectCause extends Enum {
//    final private static com.navdy.service.library.device.connection.Connection$DisconnectCause[] $VALUES;
//    final public static com.navdy.service.library.device.connection.Connection$DisconnectCause ABORTED;
//    final public static com.navdy.service.library.device.connection.Connection$DisconnectCause NORMAL;
//    final public static com.navdy.service.library.device.connection.Connection$DisconnectCause UNKNOWN;
//    
//    static {
//        UNKNOWN = new com.navdy.service.library.device.connection.Connection$DisconnectCause("UNKNOWN", 0);
//        NORMAL = new com.navdy.service.library.device.connection.Connection$DisconnectCause("NORMAL", 1);
//        ABORTED = new com.navdy.service.library.device.connection.Connection$DisconnectCause("ABORTED", 2);
//        com.navdy.service.library.device.connection.Connection$DisconnectCause[] a = new com.navdy.service.library.device.connection.Connection$DisconnectCause[3];
//        a[0] = UNKNOWN;
//        a[1] = NORMAL;
//        a[2] = ABORTED;
//        $VALUES = a;
//    }
//    
//    private Connection$DisconnectCause(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.service.library.device.connection.Connection$DisconnectCause valueOf(String s) {
//        return (com.navdy.service.library.device.connection.Connection$DisconnectCause)Enum.valueOf(com.navdy.service.library.device.connection.Connection$DisconnectCause.class, s);
//    }
//    
//    public static com.navdy.service.library.device.connection.Connection$DisconnectCause[] values() {
//        return $VALUES.clone();
//    }
//}
//