package com.navdy.service.library.device.discovery;

abstract public class RemoteDeviceScanner extends com.navdy.service.library.util.Listenable {
    protected android.content.Context mContext;
    
    RemoteDeviceScanner(android.content.Context a) {
        this.mContext = a;
    }
    
    protected void dispatchOnDiscovered(com.navdy.service.library.device.connection.ConnectionInfo a) {
        com.navdy.service.library.device.connection.ConnectionInfo[] a0 = new com.navdy.service.library.device.connection.ConnectionInfo[1];
        a0[0] = a;
        this.dispatchOnDiscovered(java.util.Arrays.asList((Object[])a0));
    }
    
    protected void dispatchOnDiscovered(java.util.List a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.discovery.RemoteDeviceScanner$3(this, a));
    }
    
    protected void dispatchOnLost(java.util.List a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.discovery.RemoteDeviceScanner$4(this, a));
    }
    
    protected void dispatchOnScanStarted() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.discovery.RemoteDeviceScanner$2(this));
    }
    
    protected void dispatchOnScanStopped() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.discovery.RemoteDeviceScanner$1(this));
    }
    
    abstract public boolean startScan();
    
    
    abstract public boolean stopScan();
}
