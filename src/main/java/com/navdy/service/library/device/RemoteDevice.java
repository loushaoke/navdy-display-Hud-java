package com.navdy.service.library.device;

public class RemoteDevice extends com.navdy.service.library.util.Listenable implements com.navdy.service.library.device.connection.Connection$Listener, com.navdy.service.library.device.link.LinkListener {
    final public static int MAX_PACKET_SIZE = 524288;
    final private static Object lock;
    final public static com.navdy.service.library.device.RemoteDevice$LastSeenDeviceInfo sLastSeenDeviceInfo;
    final public static com.navdy.service.library.log.Logger sLogger;
    protected boolean convertToNavdyEvent;
    public long lastMessageSentTime;
    protected com.navdy.service.library.device.connection.Connection mActiveConnection;
    protected com.navdy.service.library.device.connection.ConnectionInfo mConnectionInfo;
    final protected android.content.Context mContext;
    protected com.navdy.service.library.device.NavdyDeviceId mDeviceId;
    protected com.navdy.service.library.events.DeviceInfo mDeviceInfo;
    final protected java.util.concurrent.LinkedBlockingDeque mEventQueue;
    final protected android.os.Handler mHandler;
    private com.navdy.service.library.device.link.Link mLink;
    private com.navdy.service.library.device.RemoteDevice$LinkStatus mLinkStatus;
    private volatile boolean mNetworkLinkReady;
    protected java.util.concurrent.atomic.AtomicBoolean mNetworkReadyEventDispatched;
    private com.squareup.wire.Wire mWire;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.RemoteDevice.class);
        lock = new Object();
        sLastSeenDeviceInfo = new com.navdy.service.library.device.RemoteDevice$LastSeenDeviceInfo();
    }
    
    public RemoteDevice(android.content.Context a, com.navdy.service.library.device.NavdyDeviceId a0, boolean b) {
        this.mLinkStatus = com.navdy.service.library.device.RemoteDevice$LinkStatus.DISCONNECTED;
        this.mNetworkLinkReady = false;
        this.mNetworkReadyEventDispatched = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.mContext = a;
        this.mDeviceId = a0;
        this.mHandler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.mEventQueue = new java.util.concurrent.LinkedBlockingDeque();
        this.convertToNavdyEvent = b;
        if (b) {
            Class[] a1 = new Class[1];
            a1[0] = com.navdy.service.library.events.Ext_NavdyEvent.class;
            this.mWire = new com.squareup.wire.Wire(a1);
        }
    }
    
    public RemoteDevice(android.content.Context a, com.navdy.service.library.device.connection.ConnectionInfo a0, boolean b) {
        this(a, a0.getDeviceId(), b);
        this.mConnectionInfo = a0;
    }
    
    private void dispatchLocalEvent(com.squareup.wire.Message a) {
        com.navdy.service.library.events.NavdyEvent a0 = com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(a);
        if (this.convertToNavdyEvent) {
            this.dispatchNavdyEvent(a0);
        } else {
            this.dispatchNavdyEvent(a0.toByteArray());
        }
    }
    
    private void dispatchStateChangeEvents(com.navdy.service.library.device.RemoteDevice$LinkStatus a, com.navdy.service.library.device.RemoteDevice$LinkStatus a0) {
        String s = this.getDeviceId().toString();
        switch(com.navdy.service.library.device.RemoteDevice$7.$SwitchMap$com$navdy$service$library$device$RemoteDevice$LinkStatus[a.ordinal()]) {
            case 3: {
                if (a0 != com.navdy.service.library.device.RemoteDevice$LinkStatus.CONNECTED) {
                    this.dispatchLocalEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange(s, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                    break;
                } else {
                    this.dispatchLocalEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange(s, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                    break;
                }
            }
            case 2: {
                if (a0 == com.navdy.service.library.device.RemoteDevice$LinkStatus.CONNECTED) {
                    this.dispatchLocalEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange(s, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                }
                this.dispatchLocalEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange(s, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST));
                break;
            }
            case 1: {
                if (a0 == com.navdy.service.library.device.RemoteDevice$LinkStatus.DISCONNECTED) {
                    this.dispatchLocalEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange(s, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                }
                this.dispatchLocalEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.ConnectionStateChange(s, com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED));
                if (!this.mNetworkLinkReady) {
                    break;
                }
                if (!this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
                    break;
                }
                this.dispatchLocalEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.NetworkLinkReady());
                sLogger.d("dispatchStateChangeEvents: dispatching the Network Link Ready message");
                break;
            }
        }
    }
    
    private void markDisconnected(com.navdy.service.library.device.connection.Connection$DisconnectCause a) {
        this.removeActiveConnection();
        this.dispatchDisconnectEvent(a);
    }
    
    private void setLinkStatus(com.navdy.service.library.device.RemoteDevice$LinkStatus a) {
        if (this.mLinkStatus != a) {
            com.navdy.service.library.device.RemoteDevice$LinkStatus a0 = this.mLinkStatus;
            this.mLinkStatus = a;
            this.dispatchStateChangeEvents(this.mLinkStatus, a0);
        }
    }

    public boolean connect() {
        if (getConnectionStatus() != com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED) {
            return false;
        }
        if (this.mConnectionInfo == null) {
            sLogger.e("can't connect without a connectionInfo");
            return false;
        } else if (setActiveConnection(com.navdy.service.library.device.connection.Connection.instantiateFromConnectionInfo(this.mContext, this.mConnectionInfo))) {
            boolean shouldDispatch;
            synchronized (lock) {
                if (this.mActiveConnection.connect() && getConnectionStatus() == com.navdy.service.library.device.connection.Connection$Status.CONNECTING) {
                    shouldDispatch = true;
                } else {
                    shouldDispatch = false;
                }
            }
            if (shouldDispatch) {
                dispatchConnectingEvent();
                sLogger.i("Starting connection");
                return true;
            }
            sLogger.e("Unable to connect");
            removeActiveConnection();
            return false;
        } else {
            sLogger.e("unable to find connection of type: " + this.mConnectionInfo.getType());
            return false;
        }
    }

    public boolean disconnect() {
        boolean b = false;
        label0: synchronized(this) {
            Object a = null;
            RuntimeException a0 = null;
            label1: if (this.getConnectionStatus() != com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED) {
                com.navdy.service.library.device.connection.Connection a1 = null;
                synchronized(lock) {
                    a1 = this.mActiveConnection;
                    this.mActiveConnection = null;
                    /*monexit(a)*/;
                }
                b = a1 != null && a1.disconnect();
                break label0;
            } else {
                sLogger.e("can't disconnect; already disconnected");
                b = true;
                break label0;
            }
//            while(true) {
//                Throwable a3 = null;
//                try {
//                    /*monexit(a)*/;
//                    a3 = a0;
//                } catch(IllegalMonitorStateException | NullPointerException a4) {
//                    a0 = a4;
//                    continue;
//                }
//                throw a3;
//            }
        }
        /*monexit(this)*/;
        return b;
    }
    
    protected void dispatchConnectEvent() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.RemoteDevice$2(this));
    }
    
    protected void dispatchConnectFailureEvent(com.navdy.service.library.device.connection.Connection$ConnectionFailureCause a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.RemoteDevice$3(this, a));
    }
    
    protected void dispatchConnectingEvent() {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.RemoteDevice$1(this));
    }
    
    protected void dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection$DisconnectCause a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.RemoteDevice$4(this, a));
    }
    
    protected void dispatchNavdyEvent(com.navdy.service.library.events.NavdyEvent a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.RemoteDevice$6(this, a));
    }
    
    protected void dispatchNavdyEvent(byte[] a) {
        this.dispatchToListeners((com.navdy.service.library.util.Listenable$EventDispatcher)new com.navdy.service.library.device.RemoteDevice$5(this, a));
    }
    
    public com.navdy.service.library.device.connection.ConnectionInfo getActiveConnectionInfo() {
        com.navdy.service.library.device.connection.ConnectionInfo a = null;
        synchronized(lock) {
            if (this.mActiveConnection == null) {
                /*monexit(a0)*/;
                a = null;
            } else {
                a = this.mActiveConnection.getConnectionInfo();
                /*monexit(a0)*/;
            }
        }
        return a;
    }
    
    public com.navdy.service.library.device.connection.ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }
    
    public com.navdy.service.library.device.connection.Connection$Status getConnectionStatus() {
        com.navdy.service.library.device.connection.Connection$Status a = null;
        synchronized(lock) {
            if (this.mActiveConnection != null) {
                a = this.mActiveConnection.getStatus();
                /*monexit(a0)*/;
            } else {
                a = com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED;
                /*monexit(a0)*/;
            }
        }
        return a;
    }
    
    public com.navdy.service.library.device.NavdyDeviceId getDeviceId() {
        return this.mDeviceId;
    }
    
    public com.navdy.service.library.events.DeviceInfo getDeviceInfo() {
        return this.mDeviceInfo;
    }
    
    public int getLinkBandwidthLevel() {
        int i = 0;
        com.navdy.service.library.device.link.Link a = this.mLink;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.mLinkStatus == com.navdy.service.library.device.RemoteDevice$LinkStatus.CONNECTED) {
                        break label0;
                    }
                }
                i = -1;
                break label2;
            }
            i = this.mLink.getBandWidthLevel();
        }
        return i;
    }
    
    public com.navdy.service.library.device.RemoteDevice$LinkStatus getLinkStatus() {
        return this.mLinkStatus;
    }
    
    public boolean isConnected() {
        return this.getConnectionStatus() == com.navdy.service.library.device.connection.Connection$Status.CONNECTED;
    }
    
    public boolean isConnecting() {
        return this.getConnectionStatus() == com.navdy.service.library.device.connection.Connection$Status.CONNECTING;
    }
    
    public void linkEstablished(com.navdy.service.library.device.connection.ConnectionType a) {
        synchronized(this) {
            int[] a0 = com.navdy.service.library.device.RemoteDevice$7.$SwitchMap$com$navdy$service$library$device$connection$ConnectionType;
            switch(a0[a.ordinal()]) {
                case 4: {
                    this.setLinkStatus(com.navdy.service.library.device.RemoteDevice$LinkStatus.LINK_ESTABLISHED);
                    if (this.mLink == null) {
                        break;
                    }
                    this.mLink.setBandwidthLevel(1);
                    break;
                }
                case 1: case 2: case 3: {
                    this.setLinkStatus(com.navdy.service.library.device.RemoteDevice$LinkStatus.CONNECTED);
                    if (this.mLink == null) {
                        break;
                    }
                    this.mLink.setBandwidthLevel(1);
                    break;
                }
                default: {
                    sLogger.e(new StringBuilder().append("Unknown connection type: ").append(a).toString());
                }
            }
        }
        /*monexit(this)*/;
    }
    
    public void linkLost(com.navdy.service.library.device.connection.ConnectionType a, com.navdy.service.library.device.connection.Connection$DisconnectCause a0) {
        synchronized(this) {
            if (this.getConnectionStatus() == com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED) {
                a0 = com.navdy.service.library.device.connection.Connection$DisconnectCause.NORMAL;
            }
            this.mNetworkReadyEventDispatched.set(false);
            this.mNetworkLinkReady = false;
            switch(com.navdy.service.library.device.RemoteDevice$7.$SwitchMap$com$navdy$service$library$device$connection$ConnectionType[a.ordinal()]) {
                case 4: {
                    this.setLinkStatus(com.navdy.service.library.device.RemoteDevice$LinkStatus.DISCONNECTED);
                    this.markDisconnected(a0);
                    break;
                }
                case 3: {
                    if (this.getLinkStatus() != com.navdy.service.library.device.RemoteDevice$LinkStatus.CONNECTED) {
                        break;
                    }
                    this.setLinkStatus(com.navdy.service.library.device.RemoteDevice$LinkStatus.LINK_ESTABLISHED);
                    break;
                }
                case 1: case 2: {
                    this.setLinkStatus(com.navdy.service.library.device.RemoteDevice$LinkStatus.DISCONNECTED);
                    this.markDisconnected(a0);
                    break;
                }
                default: {
                    sLogger.e(new StringBuilder().append("Unknown connection type: ").append(a).toString());
                }
            }
        }
        /*monexit(this)*/;
    }
    
    public void onConnected(com.navdy.service.library.device.connection.Connection a) {
        synchronized(lock) {
            if (this.mActiveConnection != a) {
                sLogger.e("Received connect event for unknown connection");
            } else {
                sLogger.i("Connected");
                this.dispatchConnectEvent();
            }
            /*monexit(a0)*/;
        }
    }
    
    public void onConnectionFailed(com.navdy.service.library.device.connection.Connection a, com.navdy.service.library.device.connection.Connection$ConnectionFailureCause a0) {
        com.navdy.service.library.log.Logger a1 = null;
        synchronized(lock) {
            if (this.mActiveConnection == a) {
                this.removeActiveConnection();
            }
            /*monexit(a2)*/;
            a1 = sLogger;
        }
        a1.e(new StringBuilder().append("Connection failed: ").append(a0).toString());
        this.dispatchConnectFailureEvent(a0);
    }
    
    public void onDisconnected(com.navdy.service.library.device.connection.Connection a, com.navdy.service.library.device.connection.Connection$DisconnectCause a0) {
        synchronized(lock) {
            if (this.mActiveConnection == a) {
                this.removeActiveConnection();
            }
            /*monexit(a1)*/;
        }
        com.navdy.service.library.device.connection.ConnectionInfo a6 = a.getConnectionInfo();
        com.navdy.service.library.log.Logger a7 = sLogger;
        StringBuilder a8 = new StringBuilder().append("Disconnected ");
        Object a9 = (a6 == null) ? "unknown" : a6;
        a7.i(a8.append(a9).append(" - ").append(a0).toString());
        this.dispatchDisconnectEvent(a0);
    }
    
    public void onNavdyEventReceived(byte[] a) {
        if (this.convertToNavdyEvent) {
            com.navdy.service.library.events.NavdyEvent a0 = null;
            try {
                a0 = (com.navdy.service.library.events.NavdyEvent)this.mWire.parseFrom(a, com.navdy.service.library.events.NavdyEvent.class);
            } catch(Throwable a1) {
                int i = 0;
                try {
                    i = com.navdy.service.library.events.WireUtil.getEventTypeIndex(a);
                } catch(Throwable ignoredException) {
                    i = -1;
                }
                sLogger.e(new StringBuilder().append("Ignoring invalid navdy event[").append(i).append("]").toString(), a1);
                a0 = null;
            }
            if (a0 != null) {
                this.dispatchNavdyEvent(a0);
            }
        } else {
            this.dispatchNavdyEvent(a);
        }
    }
    
    public void onNetworkLinkReady() {
        this.mNetworkLinkReady = true;
        if (this.getLinkStatus() != com.navdy.service.library.device.RemoteDevice$LinkStatus.CONNECTED) {
            sLogger.d("Network Link is ready, but state is not connected");
        } else if (this.mNetworkReadyEventDispatched.compareAndSet(false, true)) {
            this.dispatchLocalEvent((com.squareup.wire.Message)new com.navdy.service.library.events.connection.NetworkLinkReady());
            sLogger.d("onNetworkLinkReady: dispatching the Network Link Ready message");
        }
    }
    
    public boolean postEvent(com.navdy.service.library.events.NavdyEvent a) {
        return this.postEvent(a, (com.navdy.service.library.device.RemoteDevice$PostEventHandler)null);
    }
    
    public boolean postEvent(com.navdy.service.library.events.NavdyEvent a, com.navdy.service.library.device.RemoteDevice$PostEventHandler a0) {
        return this.postEvent(a.toByteArray(), a0);
    }
    
    public boolean postEvent(com.squareup.wire.Message a) {
        return this.postEvent(com.navdy.service.library.events.NavdyEventUtil.eventFromMessage(a), (com.navdy.service.library.device.RemoteDevice$PostEventHandler)null);
    }
    
    public boolean postEvent(byte[] a) {
        return this.postEvent(a, (com.navdy.service.library.device.RemoteDevice$PostEventHandler)null);
    }
    
    public boolean postEvent(byte[] a, com.navdy.service.library.device.RemoteDevice$PostEventHandler a0) {
        boolean b = this.mEventQueue.add(new com.navdy.service.library.device.link.EventRequest(this.mHandler, a, a0));
        this.lastMessageSentTime = android.os.SystemClock.elapsedRealtime();
        if (sLogger.isLoggable(2)) {
            sLogger.v(new StringBuilder().append("NAVDY-PACKET Event queue size [").append(this.mEventQueue.size()).append("]").toString());
        }
        return b;
    }
    
    public void removeActiveConnection() {
        synchronized(lock) {
            if (this.mActiveConnection != null) {
                this.mActiveConnection.removeListener((com.navdy.service.library.util.Listenable$Listener)this);
                this.mActiveConnection = null;
            }
            /*monexit(a)*/;
        }
    }
    
    public boolean setActiveConnection(com.navdy.service.library.device.connection.Connection a) {
        boolean b = false;
        synchronized(lock) {
            this.mActiveConnection = a;
            if (this.mActiveConnection != null) {
                this.mActiveConnection.addListener((com.navdy.service.library.util.Listenable$Listener)this);
                this.mConnectionInfo = this.mActiveConnection.getConnectionInfo();
            }
            if (this.isConnected()) {
                this.dispatchConnectEvent();
            }
            b = this.mActiveConnection != null;
            /*monexit(a0)*/;
        }
        return b;
    }
    
    public void setDeviceInfo(com.navdy.service.library.events.DeviceInfo a) {
        this.mDeviceInfo = a;
        if (a != null) {
            com.navdy.service.library.device.RemoteDevice$LastSeenDeviceInfo.access$002(sLastSeenDeviceInfo, a);
        }
    }
    
    public void setLinkBandwidthLevel(int i) {
        if (this.mLink != null && this.mLinkStatus == com.navdy.service.library.device.RemoteDevice$LinkStatus.CONNECTED) {
            this.mLink.setBandwidthLevel(i);
        }
    }
    
    public boolean startLink() {
        if (this.mLink != null) {
            throw new IllegalStateException("Link already started");
        }
        sLogger.i("Starting link");
        this.mLink = com.navdy.service.library.device.link.LinkManager.build(this.getConnectionInfo());
        synchronized(lock) {
            com.navdy.service.library.network.SocketAdapter a1 = null;
            try {
                com.navdy.service.library.device.link.Link a2 = this.mLink;
                Object a3 = null;
                if (a2 == null) {
                    a1 = (com.navdy.service.library.network.SocketAdapter)a3;
                } else {
                    com.navdy.service.library.device.connection.Connection a4 = this.mActiveConnection;
                    Object a5 = null;
                    a1 = (com.navdy.service.library.network.SocketAdapter)((a4 == null) ? a5 : this.mActiveConnection.getSocket());
                }
                /*monexit(a0)*/;
            } catch(Throwable a6) {
                sLogger.e("Error Starting link", a6);
                return false;
            }
            return a1 != null && this.mLink.start(a1, this.mEventQueue, (com.navdy.service.library.device.link.LinkListener)this);
        }
    }
    
    public boolean stopLink() {
        sLogger.i("Stopping link");
        if (this.mLink != null) {
            sLogger.i("Closing link");
            this.mNetworkReadyEventDispatched.set(false);
            this.mNetworkLinkReady = false;
            this.mLink.close();
            this.mLink = null;
            this.mEventQueue.clear();
        }
        return true;
    }
}
