package com.navdy.service.library.device.connection.tunnel;

public class Tunnel extends com.navdy.service.library.device.connection.ProxyService {
    final static String TAG;
    final private com.navdy.service.library.network.SocketAcceptor acceptor;
    final private com.navdy.service.library.network.SocketFactory connector;
    final java.util.ArrayList transferThreads;
    
    static {
        TAG = com.navdy.service.library.device.connection.tunnel.Tunnel.class.getSimpleName();
    }
    
    public Tunnel(com.navdy.service.library.network.SocketAcceptor a, com.navdy.service.library.network.SocketFactory a0) {
        this.transferThreads = new java.util.ArrayList();
        this.setName(TAG);
        this.acceptor = a;
        this.connector = a0;
    }
    
    public void cancel() {
        try {
            this.acceptor.close();
        } catch(java.io.IOException a) {
            android.util.Log.e(TAG, new StringBuilder().append("close failed:").append(a.getMessage()).toString());
        }
        Object a0 = ((java.util.ArrayList)this.transferThreads.clone()).iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            ((com.navdy.service.library.device.connection.tunnel.TransferThread)((java.util.Iterator)a0).next()).cancel();
        }
    }
    
    public void run() {
        android.util.Log.d(TAG, String.format("start: %s -> %s", this.acceptor, this.connector));
        while(true) {
            label1: {
                com.navdy.service.library.network.SocketAdapter acceptedSkt;
                com.navdy.service.library.network.SocketAdapter connectedSkt = null;
                java.io.InputStream acceptedInput = null;
                java.io.OutputStream acceptedOutput = null;
                java.io.InputStream connectedInput = null;
                java.io.OutputStream connectedOutput = null;
                label0: {
                    Exception a6;
                    try {
                        acceptedSkt = this.acceptor.accept();
                        break label0;
                    } catch(Exception a7) {
                        a6 = a7;
                    }
                    android.util.Log.e(TAG, "accept failed:" + a6.getMessage());
                    break label1;
                }
                android.util.Log.v(TAG, "accepted connection (" + acceptedSkt + ")");
                try {
                    try {
                        connectedSkt = this.connector.build();
                        connectedSkt.connect();
                        StringBuilder a8 = new StringBuilder();
                        android.util.Log.v(TAG, a8.append("connected (").append(connectedSkt).append(")").toString());
                        android.util.Log.d(TAG, String.format("starting transfer: %s -> %s", acceptedSkt, connectedSkt));
                        acceptedInput = acceptedSkt.getInputStream();
                        connectedOutput = connectedSkt.getOutputStream();
                        com.navdy.service.library.device.connection.tunnel.TransferThread a10 = new com.navdy.service.library.device.connection.tunnel.TransferThread(this, acceptedInput, connectedOutput, true);
                        connectedInput = connectedSkt.getInputStream();
                        acceptedOutput = acceptedSkt.getOutputStream();
                        com.navdy.service.library.device.connection.tunnel.TransferThread a11 = new com.navdy.service.library.device.connection.tunnel.TransferThread(this, connectedInput, acceptedOutput, false);
                        a10.start();
                        a11.start();
                        a10.join();
                        a11.join();
                        acceptedSkt.close();
                        connectedSkt.close();
                        continue;
                    } catch(InterruptedException ignoredException) {
                    }
                } catch(java.io.IOException a12) {
                    android.util.Log.e(TAG, new StringBuilder().append("transfer failed:").append(a12.getMessage()).toString());
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)connectedInput);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)connectedOutput);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)connectedSkt);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)acceptedInput);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)acceptedOutput);
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)acceptedSkt);
                    continue;
                }
            }
            this.cancel();
            return;
        }
    }
}
