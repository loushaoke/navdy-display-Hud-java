package com.navdy.service.library.device.connection;

class Connection$2 implements com.navdy.service.library.device.connection.Connection$EventDispatcher {
    final com.navdy.service.library.device.connection.Connection this$0;
    
    Connection$2(com.navdy.service.library.device.connection.Connection a) {
        super();
        this.this$0 = a;
    }
    
    public void dispatchEvent(com.navdy.service.library.device.connection.Connection a, com.navdy.service.library.device.connection.Connection$Listener a0) {
        a0.onConnected(a);
    }
    
    public void dispatchEvent(com.navdy.service.library.util.Listenable a, com.navdy.service.library.util.Listenable$Listener a0) {
        this.dispatchEvent((com.navdy.service.library.device.connection.Connection)a, (com.navdy.service.library.device.connection.Connection$Listener)a0);
    }
}
