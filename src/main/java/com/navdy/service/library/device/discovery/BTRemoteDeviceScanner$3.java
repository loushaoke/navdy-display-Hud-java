package com.navdy.service.library.device.discovery;

class BTRemoteDeviceScanner$3 extends android.content.BroadcastReceiver {
    final com.navdy.service.library.device.discovery.BTRemoteDeviceScanner this$0;
    
    BTRemoteDeviceScanner$3(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        String s = a0.getAction();
        boolean b = "android.bluetooth.device.action.FOUND".equals(s);
        label0: {
            label1: {
                if (b) {
                    break label1;
                }
                if ("android.bluetooth.device.action.NAME_CHANGED".equals(s)) {
                    break label1;
                }
                if (!"android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(s)) {
                    break label0;
                }
                this.this$0.dispatchOnScanStopped();
                break label0;
            }
            android.bluetooth.BluetoothDevice a1 = (android.bluetooth.BluetoothDevice)a0.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            String s0 = a0.getStringExtra("android.bluetooth.device.extra.NAME");
            String s1 = a1.getAddress();
            com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$400().d(new StringBuilder().append("Scanned: ").append(s1).append(" - name = ").append(s0).toString());
            if (s0 != null && com.navdy.service.library.device.discovery.BTDeviceBroadcaster.isDisplay(s0) && !com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$200(this.this$0).contains(s1)) {
                com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$200(this.this$0).add(s1);
                com.navdy.service.library.device.NavdyDeviceId a2 = new com.navdy.service.library.device.NavdyDeviceId(com.navdy.service.library.device.NavdyDeviceId$Type.BT, s1, s0);
                this.this$0.dispatchOnDiscovered((com.navdy.service.library.device.connection.ConnectionInfo)new com.navdy.service.library.device.connection.BTConnectionInfo(a2, new com.navdy.service.library.device.connection.ServiceAddress(s1, com.navdy.service.library.device.connection.ConnectionService.NAVDY_PROTO_SERVICE_UUID.toString()), com.navdy.service.library.device.connection.ConnectionType.BT_PROTOBUF));
            }
        }
    }
}
