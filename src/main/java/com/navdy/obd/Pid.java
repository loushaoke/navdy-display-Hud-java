package com.navdy.obd;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class Pid implements Parcelable
{
    public static final Parcelable.Creator<Pid> CREATOR;
    public static final int NO_DATA = Integer.MIN_VALUE;
    protected DataType dataType;
    protected int id;
    protected String name;
    protected long timeStamp;
    protected Units units;
    protected double value;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<Pid>() {
            public Pid createFromParcel(final Parcel parcel) {
                return new Pid(parcel);
            }
            
            public Pid[] newArray(final int n) {
                return new Pid[n];
            }
        };
    }
    
    public Pid(final int n) {
        this(n, null, 0.0, DataType.INT, Units.NONE);
    }
    
    public Pid(final int n, final String s) {
        this(n, s, 0.0, DataType.INT, Units.NONE);
    }
    
    public Pid(final int n, final String s, final double n2, final DataType dataType, final Units units) {
        this(n, s, 0.0, DataType.INT, Units.NONE, 0L);
    }
    
    public Pid(final int id, final String name, final double value, final DataType dataType, final Units units, final long timeStamp) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.dataType = dataType;
        this.units = units;
        this.timeStamp = timeStamp;
    }
    
    public Pid(final Parcel parcel) {
        this(parcel.readInt(), parcel.readString(), parcel.readDouble(), DataType.valueOf(parcel.readString()), Units.valueOf(parcel.readString()), parcel.readLong());
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals;
        if (o instanceof Pid) {
            equals = (((Pid)o).id == this.id);
        }
        else {
            equals = super.equals(o);
        }
        return equals;
    }
    
    public DataType getDataType() {
        return this.dataType;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getName() {
        return this.name;
    }
    
    public long getTimeStamp() {
        return this.timeStamp;
    }
    
    public double getValue() {
        return this.value;
    }

    public int getIntValue() {
        return (int) Math.round(getValue());
    }
    
    public void setTimeStamp(final long timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    public void setValue(final double value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return "Pid{id=" + Integer.toHexString(this.id) + '}';
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.id);
        parcel.writeString(this.name);
        parcel.writeDouble(this.value);
        parcel.writeString(this.dataType.name());
        parcel.writeString(this.units.name());
        parcel.writeLong(this.timeStamp);
    }

    public enum DataType {
        INT(0),
        FLOAT(1),
        PERCENTAGE(2),
        BOOL(3);

        private int value;
        DataType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
