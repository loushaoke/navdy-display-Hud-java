package com.navdy.hud.app.analytics;

final class AnalyticsSupport$1 implements Runnable {
    AnalyticsSupport$1() {
    }
    
    public void run() {
        if (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isNetworkAccessAllowed(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.LOCALYTICS)) {
            com.navdy.hud.app.analytics.AnalyticsSupport.access$0001().v("called Localytics.upload");
            com.localytics.android.Localytics.upload();
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.access$200().postDelayed(this, com.navdy.hud.app.analytics.AnalyticsSupport.access$1001());
    }
}
