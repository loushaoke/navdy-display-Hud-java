package com.navdy.hud.app;

import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IEventListener extends IInterface
{
    void onEvent(final byte[] p0) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IEventListener
    {
        private static final String DESCRIPTOR = "com.navdy.hud.app.IEventListener";
        static final int TRANSACTION_onEvent = 1;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.navdy.hud.app.IEventListener");
        }
        
        public static IEventListener asInterface(final IBinder binder) {
            IEventListener eventListener;
            if (binder == null) {
                eventListener = null;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.navdy.hud.app.IEventListener");
                if (queryLocalInterface != null && queryLocalInterface instanceof IEventListener) {
                    eventListener = (IEventListener)queryLocalInterface;
                }
                else {
                    eventListener = new Proxy(binder);
                }
            }
            return eventListener;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            boolean onTransact = true;
            switch (n) {
                default:
                    onTransact = super.onTransact(n, parcel, parcel2, n2);
                    break;
                case 1598968902:
                    parcel2.writeString("com.navdy.hud.app.IEventListener");
                    break;
                case 1:
                    parcel.enforceInterface("com.navdy.hud.app.IEventListener");
                    this.onEvent(parcel.createByteArray());
                    break;
            }
            return onTransact;
        }
        
        private static class Proxy implements IEventListener
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "com.navdy.hud.app.IEventListener";
            }
            
            @Override
            public void onEvent(final byte[] array) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.navdy.hud.app.IEventListener");
                    obtain.writeByteArray(array);
                    this.mRemote.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}
