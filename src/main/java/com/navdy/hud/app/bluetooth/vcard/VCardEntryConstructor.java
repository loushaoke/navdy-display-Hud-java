package com.navdy.hud.app.bluetooth.vcard;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import android.accounts.Account;

public class VCardEntryConstructor implements VCardInterpreter
{
    private static String LOG_TAG;
    private final Account mAccount;
    private VCardEntry mCurrentEntry;
    private final List<VCardEntryHandler> mEntryHandlers;
    private final List<VCardEntry> mEntryStack;
    private final int mVCardType;
    
    static {
        VCardEntryConstructor.LOG_TAG = "vCard";
    }
    
    public VCardEntryConstructor() {
        this(-1073741824, null, null);
    }
    
    public VCardEntryConstructor(final int n) {
        this(n, null, null);
    }
    
    public VCardEntryConstructor(final int n, final Account account) {
        this(n, account, null);
    }
    
    public VCardEntryConstructor(final int mvCardType, final Account mAccount, final String s) {
        this.mEntryStack = new ArrayList<VCardEntry>();
        this.mEntryHandlers = new ArrayList<VCardEntryHandler>();
        this.mVCardType = mvCardType;
        this.mAccount = mAccount;
    }
    
    public void addEntryHandler(final VCardEntryHandler vCardEntryHandler) {
        this.mEntryHandlers.add(vCardEntryHandler);
    }
    
    public void clear() {
        this.mCurrentEntry = null;
        this.mEntryStack.clear();
    }
    
    @Override
    public void onEntryEnded() {
        this.mCurrentEntry.consolidateFields();
        final Iterator<VCardEntryHandler> iterator = this.mEntryHandlers.iterator();
        while (iterator.hasNext()) {
            iterator.next().onEntryCreated(this.mCurrentEntry);
        }
        final int size = this.mEntryStack.size();
        if (size > 1) {
            final VCardEntry mCurrentEntry = this.mEntryStack.get(size - 2);
            mCurrentEntry.addChild(this.mCurrentEntry);
            this.mCurrentEntry = mCurrentEntry;
        }
        else {
            this.mCurrentEntry = null;
        }
        this.mEntryStack.remove(size - 1);
    }
    
    @Override
    public void onEntryStarted() {
        this.mCurrentEntry = new VCardEntry(this.mVCardType, this.mAccount);
        this.mEntryStack.add(this.mCurrentEntry);
    }
    
    @Override
    public void onPropertyCreated(final VCardProperty vCardProperty) {
        this.mCurrentEntry.addProperty(vCardProperty);
    }
    
    @Override
    public void onVCardEnded() {
        final Iterator<VCardEntryHandler> iterator = this.mEntryHandlers.iterator();
        while (iterator.hasNext()) {
            iterator.next().onEnd();
        }
    }
    
    @Override
    public void onVCardStarted() {
        final Iterator<VCardEntryHandler> iterator = this.mEntryHandlers.iterator();
        while (iterator.hasNext()) {
            iterator.next().onStart();
        }
    }
}
