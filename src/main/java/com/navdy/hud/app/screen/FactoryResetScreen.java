package com.navdy.hud.app.screen;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.RecoverySystem;
import android.view.View;

import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.FactoryResetView;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import flow.Layout;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

@Layout(R.layout.screen_factory_reset)
public class FactoryResetScreen extends BaseScreen {
    private static final Logger sLogger = new Logger(FactoryResetScreen.class);

    @dagger.Module(addsTo = com.navdy.hud.app.ui.activity.Main.Module.class, injects = {FactoryResetView.class})
    public class Module {
    }

    @Singleton
    public static class Presenter extends BasePresenter<FactoryResetView> {
        private static final int CANCEL_POSITION = 1;
        private static final List<String> CONFIRMATION_CHOICES = new ArrayList();
        private static final int FACTORY_RESET_POSITION = 0;
        @Inject
        Bus bus;
        private IListener confirmationListener = new IListener() {
            public void executeItem(int pos, int id) {
                switch (pos) {
                    case 0:
                        AnalyticsSupport.recordShutdown(Reason.FACTORY_RESET, false);
                        DialManager.getInstance().requestDialForgetKeys();
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                try {
                                    RecoverySystem.rebootWipeUserData(HudApplication.getAppContext());
                                } catch (Exception e) {
                                    FactoryResetScreen.sLogger.e("Failed to do factory reset", e);
                                    Presenter.this.bus.post(new Builder().screen(Screen.SCREEN_BACK).build());
                                }
                            }
                        }, 1);
                        return;
                    case 1:
                        Presenter.this.bus.post(new Builder().screen(Screen.SCREEN_BACK).build());
                        return;
                    default:
                        return;
                }
            }

            public void itemSelected(int pos, int id) {
            }
        };
        private ConfirmationLayout factoryResetConfirmation;
        private Resources resources;

        static {
            CONFIRMATION_CHOICES.add(HudApplication.getAppContext().getResources().getString(R.string.factory_reset_go));
            CONFIRMATION_CHOICES.add(HudApplication.getAppContext().getResources().getString(R.string.factory_reset_cancel));
        }

        public void onLoad(Bundle savedInstanceState) {
            this.resources = HudApplication.getAppContext().getResources();
            updateView();
            super.onLoad(savedInstanceState);
        }

        public boolean handleKey(CustomKeyEvent event) {
            return this.factoryResetConfirmation.handleKey(event);
        }

        protected void updateView() {
            FactoryResetView view = (FactoryResetView) getView();
            if (view != null) {
                this.factoryResetConfirmation = view.getConfirmation();
                this.factoryResetConfirmation.screenTitle.setText(this.resources.getString(R.string.factory_reset_screen_title));
                this.factoryResetConfirmation.title1.setVisibility(View.GONE);
                this.factoryResetConfirmation.title2.setVisibility(View.GONE);
                this.factoryResetConfirmation.title3.setSingleLine(false);
                this.factoryResetConfirmation.title3.setText(this.resources.getString(R.string.factory_reset_description));
                this.factoryResetConfirmation.screenImage.setImageDrawable(this.resources.getDrawable(R.drawable.icon_settings_factory_reset));
                this.factoryResetConfirmation.setChoices(CONFIRMATION_CHOICES, 1, this.confirmationListener);
            }
        }
    }

    public Screen getScreen() {
        return Screen.SCREEN_FACTORY_RESET;
    }

    public String getMortarScopeName() {
        return getClass().getName();
    }

    public Object getDaggerModule() {
        return new Module();
    }
}
