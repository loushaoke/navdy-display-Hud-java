package com.navdy.hud.app.screen;

import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DashboardWidgetView;
import java.util.List;

public interface IDashboardGaugesManager {

    public enum GaugePositioning {
        LEFT(0),
        RIGHT(1);

        private int value;
        GaugePositioning(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    int getOptionIconResourceForGauge(int i);

    String getOptionLabelForGauge(int i);

    List<Integer> getSmartDashGauges();

    DashboardWidgetPresenter getWidgetPresenterForGauge(int i, boolean z);

    boolean isGaugeWorking(int i);

    void showGauge(int i, DashboardWidgetView dashboardWidgetView);

    void updateGauge(int i, Object obj);

    void updateUserPreference(GaugePositioning gaugePositioning, int i);
}
