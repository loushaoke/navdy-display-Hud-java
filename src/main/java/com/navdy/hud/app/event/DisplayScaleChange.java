package com.navdy.hud.app.event;

import com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat;

public class DisplayScaleChange {
    public final DisplayFormat format;

    public DisplayScaleChange(DisplayFormat format) {
        this.format = format;
    }
}
