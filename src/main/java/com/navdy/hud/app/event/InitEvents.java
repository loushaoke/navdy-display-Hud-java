package com.navdy.hud.app.event;

public class InitEvents {

    public static class BluetoothStateChanged {
        public final boolean enabled;

        public BluetoothStateChanged(boolean enabled) {
            this.enabled = enabled;
        }
    }

    public static class ConnectionServiceStarted {
    }

    public static class InitPhase {
        public final Phase phase;

        public InitPhase(Phase phase) {
            this.phase = phase;
        }
    }

    public enum Phase {
        EARLY(0),
        PRE_USER_INTERACTION(1),
        CONNECTION_SERVICE_STARTED(2),
        POST_START(3),
        LATE(4),
        SWITCHING_LOCALE(5),
        LOCALE_UP_TO_DATE(6);

        private int value;
        Phase(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
