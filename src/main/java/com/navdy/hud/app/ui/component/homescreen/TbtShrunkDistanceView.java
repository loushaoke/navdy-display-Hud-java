package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.R;
import com.navdy.hud.app.view.MainView;
import butterknife.ButterKnife;
import com.navdy.hud.app.util.os.SystemProperties;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import android.widget.ProgressBar;
import butterknife.InjectView;
import android.view.View;
import com.navdy.service.library.log.Logger;
import android.widget.RelativeLayout;

public class TbtShrunkDistanceView extends RelativeLayout
{
    private static final String TBT_SHRUNK_MODE = "persist.sys.tbtdistanceshrunk";
    private static final String TBT_SHRUNK_MODE_DISTANCE_NUMBER = "distance_number";
    private static final String TBT_SHRUNK_MODE_PROGRESS_BAR = "progress_bar";
    private Logger logger;
    private final Mode mode;
    @InjectView(R.id.now_shrunk_icon)
    View nowShrunkIcon;
    @InjectView(R.id.progress_bar_shrunk)
    ProgressBar progressBarShrunk;
    @InjectView(R.id.tbtShrunkDistance)
    TextView tbtShrunkDistance;
    
    public TbtShrunkDistanceView(final Context context) {
        this(context, null);
    }
    
    public TbtShrunkDistanceView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public TbtShrunkDistanceView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        if (SystemProperties.get(TBT_SHRUNK_MODE, TBT_SHRUNK_MODE_PROGRESS_BAR).equals(TBT_SHRUNK_MODE_DISTANCE_NUMBER)) {
            this.mode = Mode.DISTANCE_NUMBER;
        }
        else {
            this.mode = Mode.PROGRESS_BAR;
        }
    }
    
    private void setRegularBackground() {
        this.setBackgroundResource(R.color.black);
    }
    
    private void setTransparentBackground() {
        this.setBackgroundResource(R.color.white_half_transparent);
    }
    
    public void animateProgressBar(final double n) {
        if (this.mode == Mode.PROGRESS_BAR) {
            HomeScreenUtils.getProgressBarAnimator(this.progressBarShrunk, (int)(100.0 * n)).setDuration(250L).start();
        }
    }
    
    public void hideNowIcon() {
        HomeScreenUtils.getFadeOutAndScaleDownAnimator(this.nowShrunkIcon).setDuration(250L).start();
        if (this.mode == Mode.DISTANCE_NUMBER) {
            this.tbtShrunkDistance.setVisibility(View.VISIBLE);
        }
    }
    
    public void hideProgressBar() {
        if (this.mode == Mode.PROGRESS_BAR) {
            HomeScreenUtils.setWidth(this.progressBarShrunk, 0);
            this.setTransparentBackground();
        }
    }
    
    public void initProgressBar() {
        if (this.mode == Mode.PROGRESS_BAR) {
            this.setRegularBackground();
            HomeScreenUtils.setWidth(this.progressBarShrunk, HomeScreenResourceValues.activeRoadShrunkProgressBarWidth);
            this.animateProgressBar(0.0);
        }
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject(this);
        this.setY((float)HomeScreenResourceValues.activeRoadShrunkDistanceY);
        switch (this.mode) {
            default:
                this.tbtShrunkDistance.setVisibility(GONE);
                break;
            case DISTANCE_NUMBER:
                this.progressBarShrunk.setVisibility(GONE);
                break;
        }
    }
    
    public void setDistanceText(final String text) {
        this.tbtShrunkDistance.setText(text);
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case EXPAND:
                this.setX((float)HomeScreenResourceValues.activeRoadShrunkDistanceX);
                break;
            case SHRINK_LEFT:
                this.setX((float)HomeScreenResourceValues.activeRoadShrunkDistanceShrinkLeftX);
                break;
        }
    }
    
    public void showNowIcon() {
        HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowShrunkIcon).setDuration(250L).start();
        if (this.mode == Mode.DISTANCE_NUMBER) {
            this.tbtShrunkDistance.setVisibility(INVISIBLE);
        }
    }

    public enum Mode {
        PROGRESS_BAR(0),
        DISTANCE_NUMBER(1);

        private int value;
        Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
