package com.navdy.hud.app.ui.component.vlist;

import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VerticalModelCache {
    private static final Map<ModelType, CacheEntry> map = new HashMap<>();
    private static final Logger sLogger = new Logger(VerticalModelCache.class);

    private static class CacheEntry {
        ArrayList<Model> items;
        int maxItems;

        CacheEntry(int maxItems, int initialItems) {
            this.maxItems = maxItems;
            this.items = new ArrayList<>(maxItems);
            if (initialItems > 0) {
                for (int i = 0; i < initialItems; i++) {
                    this.items.add(new Model());
                }
            }
        }
    }

    static {
        map.put(ModelType.ICON_BKCOLOR, new CacheEntry(500, 100));
        map.put(ModelType.TWO_ICONS, new CacheEntry(70, 10));
        map.put(ModelType.LOADING_CONTENT, new CacheEntry(500, 100));
    }

    public static void addToCache(ModelType modelType, Model model) {
        if (model != null && modelType != null) {
            CacheEntry entry = map.get(modelType);
            if (entry != null && entry.items.size() < entry.maxItems) {
                model.clear();
                entry.items.add(model);
            }
        }
    }

    public static void addToCache(List<Model> list) {
        if (list != null) {
            for (Model model : list) {
                addToCache(model.type, model);
            }
        }
    }

    public static Model getFromCache(ModelType modelType) {
        if (modelType == null) {
            return null;
        }
        CacheEntry entry = map.get(modelType);
        if (entry == null || entry.items.size() <= 0) {
            return null;
        }
        return entry.items.remove(0);
    }
}
