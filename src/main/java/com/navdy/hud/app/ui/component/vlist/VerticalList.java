package com.navdy.hud.app.ui.component.vlist;

import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import java.util.Iterator;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import java.util.Collection;
import com.navdy.hud.app.util.GenericUtil;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.StaticLayout;
import android.text.Layout;
import com.navdy.hud.app.util.ViewUtil;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.interpolator.FastOutSlowInInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.view.View;
import com.navdy.hud.app.audio.SoundUtils;
import com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import android.animation.Animator;
import android.content.res.Resources;
import android.content.Context;
import com.navdy.hud.app.ui.component.vlist.viewholder.BlankViewHolder;
import com.navdy.hud.app.HudApplication;
import android.os.Looper;
import java.util.HashSet;
import java.util.List;
import android.animation.AnimatorSet;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import android.widget.TextView;

public class VerticalList
{
    private static final Model BLANK_ITEM_BOTTOM;
    private static final Model BLANK_ITEM_TOP;
    private static final FontSize[] FONT_SIZES;
    public static final float ICON_BK_COLOR_SCALE_FACTOR = 0.83f;
    public static final float ICON_SCALE_FACTOR = 0.6f;
    public static final int ITEM_BACK_FADE_IN_DURATION = 100;
    public static final int ITEM_INIT_ANIMATION_DURATION = 100;
    public static final int ITEM_MOVE_ANIMATION_DURATION = 230;
    public static final int ITEM_SCROLL_ANIMATION = 150;
    public static final int ITEM_SELECT_ANIMATION_DURATION = 50;
    public static final int LOADING_ADAPTER_POS = 1;
    private static final int MAX_OFF_SCREEN_VIEWS = 5;
    private static final float MAX_SCROLL_BY = 2.0f;
    private static final float SCROLL_BY_INCREMENT = 0.5f;
    private static final int[] SINGLE_LINE_MAX_LINES;
    private static final float START_SCROLL_BY = 1.0f;
    public static final float TEXT_SCALE_FACTOR_16 = 1.0f;
    public static final float TEXT_SCALE_FACTOR_18 = 1.0f;
    public static final float TEXT_SCALE_FACTOR_22 = 0.81f;
    public static final float TEXT_SCALE_FACTOR_26 = 0.69f;
    private static final float[] TITLE_SIZES;
    private static final int[] TWO_LINE_MAX_LINES;
    public static final TextView fontSizeTextView;
    private static Handler handler;
    private static final Logger sLogger;
    private static final HashMap<FontSize, FontInfo> tit_subt_map;
    private static final HashMap<FontSize, FontInfo> tit_subt_map_2_lines;
    private static final HashMap<FontSize, FontInfo> tit_subt_subt2_map;
    public static final int vlistTitleTextW;
    public static final float vlistTitle_16;
    public static final float vlistTitle_16_16_subtitle_top;
    public static final float vlistTitle_16_16_title_top;
    public static final float vlistTitle_16_top_m_2;
    public static final float vlistTitle_16_top_m_3;
    public static final float vlistTitle_18;
    public static final float vlistTitle_18_16_subtitle_top;
    public static final float vlistTitle_18_16_title_top;
    public static final float vlistTitle_18_top_m_2;
    public static final float vlistTitle_18_top_m_3;
    public static final float vlistTitle_22;
    public static final float vlistTitle_22_16_subtitle_top;
    public static final float vlistTitle_22_16_title_top;
    public static final float vlistTitle_22_18_subtitle_top;
    public static final float vlistTitle_22_18_title_top;
    public static final float vlistTitle_22_top_m_2;
    public static final float vlistTitle_22_top_m_3;
    public static final float vlistTitle_26;
    public static final float vlistTitle_26_16_subtitle_top;
    public static final float vlistTitle_26_16_title_top;
    public static final float vlistTitle_26_18_subtitle_top;
    public static final float vlistTitle_26_18_title_top;
    public static final float vlistTitle_26_top_m_2;
    public static final float vlistTitle_26_top_m_3;
    public static final float vlistsubTitle2_16_top_m_3;
    public static final float vlistsubTitle2_18_top_m_3;
    public static final float vlistsubTitle2_22_top_m_3;
    public static final float vlistsubTitle2_26_top_m_3;
    public static final float vlistsubTitle_16_top_m_2;
    public static final float vlistsubTitle_16_top_m_3;
    public static final float vlistsubTitle_18_top_m_2;
    public static final float vlistsubTitle_18_top_m_3;
    public static final float vlistsubTitle_22_top_m_2;
    public static final float vlistsubTitle_22_top_m_3;
    public static final float vlistsubTitle_26_top_m_2;
    public static final float vlistsubTitle_26_top_m_3;
    private int actualScrollY;
    public VerticalAdapter adapter;
    final int animationDuration;
    volatile boolean bindCallbacks;
    Callback callback;
    ContainerCallback containerCallback;
    private ArrayList<RecyclerView.ViewHolder> copyList;
    private int currentMiddlePosition;
    private volatile float currentScrollBy;
    boolean firstEntryBlank;
    boolean hasScrollableElement;
    private CarouselIndicator indicator;
    private DefaultAnimationListener indicatorAnimationListener;
    private AnimatorSet indicatorAnimatorSet;
    private volatile boolean initialPosChanged;
    private Runnable initialPosCheckRunnable;
    private volatile boolean isScrollBy;
    private volatile boolean isScrolling;
    private volatile boolean isSelectedOperationPending;
    private final ItemSelectionState itemSelectionState;
    private final KeyHandlerState keyHandlerState;
    private int lastScrollState;
    private VerticalLayoutManager layoutManager;
    private volatile boolean lockList;
    private List<Model> modelList;
    boolean reCalcScrollPos;
    private VerticalRecyclerView recyclerView;
    private volatile int scrollByUpReceived;
    int scrollItemEndY;
    int scrollItemHeight;
    int scrollItemIndex;
    int scrollItemStartY;
    private RecyclerView.OnScrollListener scrollListener;
    private volatile int scrollPendingPos;
    public HashSet<Integer> selectedList;
    boolean sendScrollIdleEvent;
    private boolean switchingScrollBoundary;
    private boolean targetFound;
    public int targetPos;
    private boolean twoLineTitles;
    public boolean waitForTarget;
    
    static {
        sLogger = new Logger(VerticalList.class);
        VerticalList.handler = new Handler(Looper.getMainLooper());
        tit_subt_subt2_map = new HashMap<FontSize, FontInfo>();
        tit_subt_map = new HashMap<FontSize, FontInfo>();
        tit_subt_map_2_lines = new HashMap<FontSize, FontInfo>();
        final Context appContext = HudApplication.getAppContext();
        final Resources resources = appContext.getResources();
        vlistTitleTextW = resources.getDimensionPixelSize(R.dimen.vlist_title_text_len);
        vlistTitle_26 = resources.getDimension(R.dimen.vlist_title);
        vlistTitle_22 = resources.getDimension(R.dimen.vlist_title_22);
        vlistTitle_18 = resources.getDimension(R.dimen.vlist_title_18);
        vlistTitle_16 = resources.getDimension(R.dimen.vlist_title_16);
        vlistTitle_16_top_m_3 = resources.getDimension(R.dimen.vlist_16_title_top_m_3);
        vlistsubTitle_16_top_m_3 = resources.getDimension(R.dimen.vlist_16_subtitle_top_m_3);
        vlistsubTitle2_16_top_m_3 = resources.getDimension(R.dimen.vlist_16_subtitle2_top_m_3);
        vlistTitle_18_top_m_3 = resources.getDimension(R.dimen.vlist_18_title_top_m_3);
        vlistsubTitle_18_top_m_3 = resources.getDimension(R.dimen.vlist_18_subtitle_top_m_3);
        vlistsubTitle2_18_top_m_3 = resources.getDimension(R.dimen.vlist_18_subtitle2_top_m_3);
        vlistTitle_22_top_m_3 = resources.getDimension(R.dimen.vlist_22_title_top_m_3);
        vlistsubTitle_22_top_m_3 = resources.getDimension(R.dimen.vlist_22_subtitle_top_m_3);
        vlistsubTitle2_22_top_m_3 = resources.getDimension(R.dimen.vlist_22_subtitle2_top_m_3);
        vlistTitle_26_top_m_3 = resources.getDimension(R.dimen.vlist_26_title_top_m_3);
        vlistsubTitle_26_top_m_3 = resources.getDimension(R.dimen.vlist_26_subtitle_top_m_3);
        vlistsubTitle2_26_top_m_3 = resources.getDimension(R.dimen.vlist_26_subtitle2_top_m_3);
        vlistTitle_16_top_m_2 = resources.getDimension(R.dimen.vlist_16_title_top_m_2);
        vlistsubTitle_16_top_m_2 = resources.getDimension(R.dimen.vlist_16_subtitle_top_m_2);
        vlistTitle_18_top_m_2 = resources.getDimension(R.dimen.vlist_18_title_top_m_2);
        vlistsubTitle_18_top_m_2 = resources.getDimension(R.dimen.vlist_18_subtitle_top_m_2);
        vlistTitle_22_top_m_2 = resources.getDimension(R.dimen.vlist_22_title_top_m_2);
        vlistsubTitle_22_top_m_2 = resources.getDimension(R.dimen.vlist_22_subtitle_top_m_2);
        vlistTitle_26_top_m_2 = resources.getDimension(R.dimen.vlist_26_title_top_m_2);
        vlistsubTitle_26_top_m_2 = resources.getDimension(R.dimen.vlist_26_subtitle_top_m_2);
        vlistTitle_26_18_title_top = resources.getDimension(R.dimen.vlist_26_18_title_top);
        vlistTitle_26_18_subtitle_top = resources.getDimension(R.dimen.vlist_26_18_subtitle_top);
        vlistTitle_22_18_title_top = resources.getDimension(R.dimen.vlist_22_18_title_top);
        vlistTitle_22_18_subtitle_top = resources.getDimension(R.dimen.vlist_22_18_subtitle_top);
        vlistTitle_18_16_title_top = resources.getDimension(R.dimen.vlist_18_16_title_top);
        vlistTitle_18_16_subtitle_top = resources.getDimension(R.dimen.vlist_18_16_subtitle_top);
        vlistTitle_16_16_title_top = resources.getDimension(R.dimen.vlist_16_16_title_top);
        vlistTitle_16_16_subtitle_top = resources.getDimension(R.dimen.vlist_16_16_subtitle_top);
        vlistTitle_26_16_title_top = resources.getDimension(R.dimen.vlist_26_16_title_top);
        vlistTitle_26_16_subtitle_top = resources.getDimension(R.dimen.vlist_26_16_subtitle_top);
        vlistTitle_22_16_title_top = resources.getDimension(R.dimen.vlist_22_16_title_top);
        vlistTitle_22_16_subtitle_top = resources.getDimension(R.dimen.vlist_22_16_subtitle_top);
        final FontInfo fontInfo = new FontInfo();
        fontInfo.titleFontSize = VerticalList.vlistTitle_26;
        fontInfo.titleFontTopMargin = VerticalList.vlistTitle_26_top_m_3;
        fontInfo.titleScale = 0.69f;
        fontInfo.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo.subTitleFontTopMargin = VerticalList.vlistsubTitle_26_top_m_3;
        fontInfo.subTitle2FontSize = VerticalList.vlistTitle_16;
        fontInfo.subTitle2FontTopMargin = VerticalList.vlistsubTitle2_26_top_m_3;
        VerticalList.tit_subt_subt2_map.put(FontSize.FONT_SIZE_26, fontInfo);
        final FontInfo fontInfo2 = new FontInfo();
        fontInfo2.titleFontSize = VerticalList.vlistTitle_22;
        fontInfo2.titleFontTopMargin = VerticalList.vlistTitle_22_top_m_3;
        fontInfo2.titleScale = 0.81f;
        fontInfo2.titleSingleLine = false;
        fontInfo2.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo2.subTitleFontTopMargin = VerticalList.vlistsubTitle_22_top_m_3;
        fontInfo2.subTitle2FontSize = VerticalList.vlistTitle_16;
        fontInfo2.subTitle2FontTopMargin = VerticalList.vlistsubTitle2_22_top_m_3;
        VerticalList.tit_subt_subt2_map.put(FontSize.FONT_SIZE_22, fontInfo2);
        final FontInfo fontInfo3 = new FontInfo();
        fontInfo3.titleFontSize = VerticalList.vlistTitle_22;
        fontInfo3.titleFontTopMargin = VerticalList.vlistTitle_22_top_m_3;
        fontInfo3.titleScale = 0.81f;
        fontInfo3.titleSingleLine = false;
        fontInfo3.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo3.subTitleFontTopMargin = VerticalList.vlistsubTitle_22_top_m_3 + 3.0f;
        fontInfo3.subTitle2FontSize = VerticalList.vlistTitle_16;
        fontInfo3.subTitle2FontTopMargin = VerticalList.vlistsubTitle2_22_top_m_3 + 3.0f;
        VerticalList.tit_subt_subt2_map.put(FontSize.FONT_SIZE_22_2, fontInfo3);
        final FontInfo fontInfo4 = new FontInfo();
        fontInfo4.titleFontSize = VerticalList.vlistTitle_18;
        fontInfo4.titleFontTopMargin = VerticalList.vlistTitle_18_top_m_3;
        fontInfo4.titleScale = 1.0f;
        fontInfo4.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo4.subTitleFontTopMargin = VerticalList.vlistsubTitle_18_top_m_3;
        fontInfo4.subTitle2FontSize = VerticalList.vlistTitle_16;
        fontInfo4.subTitle2FontTopMargin = VerticalList.vlistsubTitle2_18_top_m_3;
        VerticalList.tit_subt_subt2_map.put(FontSize.FONT_SIZE_18, fontInfo4);
        final FontInfo fontInfo5 = new FontInfo();
        fontInfo5.titleFontSize = VerticalList.vlistTitle_18;
        fontInfo5.titleFontTopMargin = VerticalList.vlistTitle_18_top_m_3;
        fontInfo5.titleScale = 1.0f;
        fontInfo5.titleSingleLine = false;
        fontInfo5.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo5.subTitleFontTopMargin = VerticalList.vlistsubTitle_18_top_m_3;
        fontInfo5.subTitle2FontSize = VerticalList.vlistTitle_16;
        fontInfo5.subTitle2FontTopMargin = VerticalList.vlistsubTitle2_18_top_m_3;
        VerticalList.tit_subt_subt2_map.put(FontSize.FONT_SIZE_18_2, fontInfo5);
        final FontInfo fontInfo6 = new FontInfo();
        fontInfo6.titleFontSize = VerticalList.vlistTitle_16;
        fontInfo6.titleFontTopMargin = VerticalList.vlistTitle_16_top_m_3;
        fontInfo6.titleScale = 1.0f;
        fontInfo6.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo6.subTitleFontTopMargin = VerticalList.vlistsubTitle_16_top_m_3;
        fontInfo6.subTitle2FontSize = VerticalList.vlistTitle_16;
        fontInfo6.subTitle2FontTopMargin = VerticalList.vlistsubTitle2_16_top_m_3;
        VerticalList.tit_subt_subt2_map.put(FontSize.FONT_SIZE_16, fontInfo6);
        final FontInfo fontInfo7 = new FontInfo();
        fontInfo7.titleFontSize = VerticalList.vlistTitle_16;
        fontInfo7.titleFontTopMargin = VerticalList.vlistTitle_16_top_m_3;
        fontInfo7.titleScale = 1.0f;
        fontInfo7.titleSingleLine = false;
        fontInfo7.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo7.subTitleFontTopMargin = VerticalList.vlistsubTitle_16_top_m_3;
        fontInfo7.subTitle2FontSize = VerticalList.vlistTitle_16;
        fontInfo7.subTitle2FontTopMargin = VerticalList.vlistsubTitle2_16_top_m_3;
        VerticalList.tit_subt_subt2_map.put(FontSize.FONT_SIZE_16_2, fontInfo7);
        final FontInfo fontInfo8 = new FontInfo();
        fontInfo8.titleFontSize = VerticalList.vlistTitle_26;
        fontInfo8.titleFontTopMargin = VerticalList.vlistTitle_26_top_m_2;
        fontInfo8.titleScale = 0.69f;
        fontInfo8.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo8.subTitleFontTopMargin = VerticalList.vlistsubTitle_26_top_m_2;
        VerticalList.tit_subt_map.put(FontSize.FONT_SIZE_26, fontInfo8);
        final FontInfo fontInfo9 = new FontInfo();
        fontInfo9.titleFontSize = VerticalList.vlistTitle_22;
        fontInfo9.titleFontTopMargin = VerticalList.vlistTitle_22_top_m_2;
        fontInfo9.titleScale = 0.81f;
        fontInfo9.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo9.subTitleFontTopMargin = VerticalList.vlistsubTitle_22_top_m_2;
        VerticalList.tit_subt_map.put(FontSize.FONT_SIZE_22, fontInfo9);
        final FontInfo fontInfo10 = new FontInfo();
        fontInfo10.titleFontSize = VerticalList.vlistTitle_22;
        fontInfo10.titleFontTopMargin = VerticalList.vlistTitle_22_top_m_2 + 3.0f;
        fontInfo10.titleScale = 0.81f;
        fontInfo10.titleSingleLine = false;
        fontInfo10.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo10.subTitleFontTopMargin = VerticalList.vlistsubTitle_22_top_m_2 + 9.0f;
        VerticalList.tit_subt_map.put(FontSize.FONT_SIZE_22_2, fontInfo10);
        final FontInfo fontInfo11 = new FontInfo();
        fontInfo11.titleFontSize = VerticalList.vlistTitle_18;
        fontInfo11.titleFontTopMargin = VerticalList.vlistTitle_18_top_m_2;
        fontInfo11.titleScale = 1.0f;
        fontInfo11.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo11.subTitleFontTopMargin = VerticalList.vlistsubTitle_18_top_m_2;
        VerticalList.tit_subt_map.put(FontSize.FONT_SIZE_18, fontInfo11);
        final FontInfo fontInfo12 = new FontInfo();
        fontInfo12.titleFontSize = VerticalList.vlistTitle_18;
        fontInfo12.titleFontTopMargin = VerticalList.vlistTitle_18_top_m_2 + 1.0f;
        fontInfo12.titleScale = 1.0f;
        fontInfo12.titleSingleLine = false;
        fontInfo12.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo12.subTitleFontTopMargin = VerticalList.vlistsubTitle_18_top_m_2 + 7.0f;
        VerticalList.tit_subt_map.put(FontSize.FONT_SIZE_18_2, fontInfo12);
        final FontInfo fontInfo13 = new FontInfo();
        fontInfo13.titleFontSize = VerticalList.vlistTitle_16;
        fontInfo13.titleFontTopMargin = VerticalList.vlistTitle_16_top_m_2;
        fontInfo13.titleScale = 1.0f;
        fontInfo13.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo13.subTitleFontTopMargin = VerticalList.vlistsubTitle_16_top_m_2;
        VerticalList.tit_subt_map.put(FontSize.FONT_SIZE_16, fontInfo13);
        final FontInfo fontInfo14 = new FontInfo();
        fontInfo14.titleFontSize = VerticalList.vlistTitle_16;
        fontInfo14.titleFontTopMargin = VerticalList.vlistTitle_16_top_m_2 + 1.0f;
        fontInfo14.titleScale = 1.0f;
        fontInfo14.titleSingleLine = false;
        fontInfo14.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo14.subTitleFontTopMargin = VerticalList.vlistsubTitle_16_top_m_2 + 7.0f;
        VerticalList.tit_subt_map.put(FontSize.FONT_SIZE_16_2, fontInfo14);
        final FontInfo fontInfo15 = new FontInfo();
        fontInfo15.titleFontSize = VerticalList.vlistTitle_26;
        fontInfo15.titleFontTopMargin = VerticalList.vlistTitle_26_18_title_top;
        fontInfo15.titleScale = 0.69f;
        fontInfo15.subTitleFontSize = VerticalList.vlistTitle_18;
        fontInfo15.subTitleFontTopMargin = VerticalList.vlistTitle_26_18_subtitle_top;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_26_18, fontInfo15);
        final FontInfo fontInfo16 = new FontInfo();
        fontInfo16.titleFontSize = VerticalList.vlistTitle_22;
        fontInfo16.titleFontTopMargin = VerticalList.vlistTitle_22_18_title_top;
        fontInfo16.titleScale = 0.81f;
        fontInfo16.subTitleFontSize = VerticalList.vlistTitle_18;
        fontInfo16.subTitleFontTopMargin = VerticalList.vlistTitle_22_18_subtitle_top;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_22_18, fontInfo16);
        final FontInfo fontInfo17 = new FontInfo();
        fontInfo17.titleFontSize = VerticalList.vlistTitle_22;
        fontInfo17.titleFontTopMargin = VerticalList.vlistTitle_22_18_title_top;
        fontInfo17.titleScale = 0.81f;
        fontInfo17.titleSingleLine = false;
        fontInfo17.subTitleFontSize = VerticalList.vlistTitle_18;
        fontInfo17.subTitleFontTopMargin = VerticalList.vlistTitle_22_18_subtitle_top;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_22_2_18, fontInfo17);
        final FontInfo fontInfo18 = new FontInfo();
        fontInfo18.titleFontSize = VerticalList.vlistTitle_18;
        fontInfo18.titleFontTopMargin = VerticalList.vlistTitle_18_16_title_top;
        fontInfo18.titleScale = 1.0f;
        fontInfo18.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo18.subTitleFontTopMargin = VerticalList.vlistTitle_18_16_subtitle_top;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_18_16, fontInfo18);
        final FontInfo fontInfo19 = new FontInfo();
        fontInfo19.titleFontSize = VerticalList.vlistTitle_18;
        fontInfo19.titleFontTopMargin = VerticalList.vlistTitle_18_top_m_2 + 1.0f;
        fontInfo19.titleScale = 1.0f;
        fontInfo19.titleSingleLine = false;
        fontInfo19.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo19.subTitleFontTopMargin = VerticalList.vlistsubTitle_18_top_m_2 + 7.0f;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_18_2_16, fontInfo19);
        final FontInfo fontInfo20 = new FontInfo();
        fontInfo20.titleFontSize = VerticalList.vlistTitle_16;
        fontInfo20.titleFontTopMargin = VerticalList.vlistTitle_16_16_title_top;
        fontInfo20.titleScale = 1.0f;
        fontInfo20.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo20.subTitleFontTopMargin = VerticalList.vlistTitle_16_16_subtitle_top;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_16_16, fontInfo20);
        final FontInfo fontInfo21 = new FontInfo();
        fontInfo21.titleFontSize = VerticalList.vlistTitle_16;
        fontInfo21.titleFontTopMargin = VerticalList.vlistTitle_16_16_title_top;
        fontInfo21.titleScale = 1.0f;
        fontInfo21.titleSingleLine = false;
        fontInfo21.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo21.subTitleFontTopMargin = VerticalList.vlistTitle_16_16_subtitle_top;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_16_2_16, fontInfo21);
        final FontInfo fontInfo22 = new FontInfo();
        fontInfo22.titleFontSize = VerticalList.vlistTitle_26;
        fontInfo22.titleFontTopMargin = VerticalList.vlistTitle_26_16_title_top;
        fontInfo22.titleScale = 0.69f;
        fontInfo22.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo22.subTitleFontTopMargin = VerticalList.vlistTitle_26_16_subtitle_top;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_26_16, fontInfo22);
        final FontInfo fontInfo23 = new FontInfo();
        fontInfo23.titleFontSize = VerticalList.vlistTitle_22;
        fontInfo23.titleFontTopMargin = VerticalList.vlistTitle_22_16_title_top;
        fontInfo23.titleScale = 0.81f;
        fontInfo23.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo23.subTitleFontTopMargin = VerticalList.vlistTitle_22_16_subtitle_top;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_22_16, fontInfo23);
        final FontInfo fontInfo24 = new FontInfo();
        fontInfo24.titleFontSize = VerticalList.vlistTitle_22;
        fontInfo24.titleFontTopMargin = VerticalList.vlistTitle_22_top_m_2 + 1.0f;
        fontInfo24.titleScale = 0.81f;
        fontInfo24.titleSingleLine = false;
        fontInfo24.subTitleFontSize = VerticalList.vlistTitle_16;
        fontInfo24.subTitleFontTopMargin = VerticalList.vlistTitle_22_16_subtitle_top + 6.0f;
        VerticalList.tit_subt_map_2_lines.put(FontSize.FONT_SIZE_22_2_16, fontInfo24);
        (fontSizeTextView = new TextView(appContext)).setTextAppearance(appContext, R.style.vlist_title);
        BLANK_ITEM_TOP = BlankViewHolder.buildModel();
        BLANK_ITEM_BOTTOM = BlankViewHolder.buildModel();
        TITLE_SIZES = new float[] { VerticalList.vlistTitle_26, VerticalList.vlistTitle_22, VerticalList.vlistTitle_18, VerticalList.vlistTitle_16 };
        TWO_LINE_MAX_LINES = new int[] { 1, 2, 2, 2 };
        SINGLE_LINE_MAX_LINES = new int[] { 1, 1, 1, 1 };
        FONT_SIZES = new FontSize[] { FontSize.FONT_SIZE_26, FontSize.FONT_SIZE_26, FontSize.FONT_SIZE_22, FontSize.FONT_SIZE_22_2, FontSize.FONT_SIZE_18, FontSize.FONT_SIZE_18_2, FontSize.FONT_SIZE_16, FontSize.FONT_SIZE_16_2 };
    }
    
    public VerticalList(final VerticalRecyclerView verticalRecyclerView, final CarouselIndicator carouselIndicator, final Callback callback, final ContainerCallback containerCallback) {
        this(verticalRecyclerView, carouselIndicator, callback, containerCallback, false);
    }
    
    public VerticalList(final VerticalRecyclerView recyclerView, final CarouselIndicator indicator, final Callback callback, final ContainerCallback containerCallback, final boolean twoLineTitles) {
        this.scrollPendingPos = -1;
        this.indicatorAnimationListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                if (VerticalList.this.containerCallback == null || !VerticalList.this.containerCallback.isFastScrolling()) {
                    VerticalList.this.indicator.setCurrentItem(VerticalList.this.getCurrentPosition());
                }
                VerticalList.this.indicatorAnimatorSet = null;
            }
        };
        this.scrollItemIndex = -1;
        this.scrollItemStartY = -1;
        this.scrollItemEndY = -1;
        this.scrollItemHeight = -1;
        this.initialPosCheckRunnable = new Runnable() {
            @Override
            public void run() {
                if (!VerticalList.this.initialPosChanged) {
                    final int firstCompletelyVisibleItemPosition = VerticalList.this.layoutManager.findFirstCompletelyVisibleItemPosition();
                    final int rawPosition = VerticalList.this.getRawPosition();
                    if (firstCompletelyVisibleItemPosition == 0 && VerticalList.this.adapter.getItemCount() > 3) {
                        VerticalList.sLogger.w("initial scroll did not work, firstV=" + firstCompletelyVisibleItemPosition + " raw=" + rawPosition + " scrollY=" + VerticalList.this.actualScrollY);
                    }
                    else {
                        VerticalList.sLogger.w("initial scroll worked firstV=" + firstCompletelyVisibleItemPosition + " raw=" + rawPosition + " scrollY=" + VerticalList.this.actualScrollY);
                    }
                    VerticalList.this.onItemSelected();
                }
                else {
                    VerticalList.sLogger.v("initial pos changed");
                }
            }
        };
        this.keyHandlerState = new KeyHandlerState();
        this.itemSelectionState = new ItemSelectionState();
        this.copyList = new ArrayList<RecyclerView.ViewHolder>();
        this.scrollListener = new RecyclerView.OnScrollListener() {
            private void moveItemToSelectedState(final VerticalViewHolder verticalViewHolder, final int n) {
                if (verticalViewHolder != null) {
                    if (n != VerticalList.this.currentMiddlePosition || verticalViewHolder.getState() == VerticalViewHolder.State.UNSELECTED) {
                        VerticalList.this.currentMiddlePosition = n;
                        if (!VerticalList.this.isCloseMenuVisible()) {
                            VerticalList.sLogger.v("onScrollStateChanged idle-select pos:" + n);
                            verticalViewHolder.setState(VerticalViewHolder.State.SELECTED, VerticalViewHolder.AnimationType.NONE, 230);
                        }
                        else {
                            VerticalList.sLogger.v("onScrollStateChanged idle-select-un pos:" + n);
                            verticalViewHolder.setState(VerticalViewHolder.State.UNSELECTED, VerticalViewHolder.AnimationType.NONE, 230);
                        }
                    }
                    else if (n == VerticalList.this.currentMiddlePosition && VerticalList.this.isCloseMenuVisible() && verticalViewHolder.getState() == VerticalViewHolder.State.SELECTED) {
                        VerticalList.sLogger.v("onScrollStateChanged idle-select-un-2 pos:" + n);
                        verticalViewHolder.setState(VerticalViewHolder.State.UNSELECTED, VerticalViewHolder.AnimationType.NONE, 230);
                    }
                }
            }
            
            private void moveItemToUnSelectedState(final VerticalViewHolder verticalViewHolder, final int n) {
                if (verticalViewHolder != null && verticalViewHolder.getModelType() != ModelType.BLANK && verticalViewHolder.getState() == VerticalViewHolder.State.SELECTED) {
                    VerticalList.sLogger.v("onScrollStateChanged idle-unselect pos:" + n);
                    verticalViewHolder.setState(VerticalViewHolder.State.UNSELECTED, VerticalViewHolder.AnimationType.NONE, 230);
                }
            }
            
            @Override
            public void onScrollStateChanged(final RecyclerView recyclerView, int n) {
                VerticalList.this.initialPosChanged = true;
                VerticalList.this.lastScrollState = n;
                super.onScrollStateChanged(recyclerView, n);
                if (n == 0) {
                    if (VerticalList.this.reCalcScrollPos) {
                        VerticalList.this.calculateScrollRange();
                    }
                    VerticalList.this.isScrolling = false;
                    VerticalList.this.isScrollBy = false;
                    VerticalList.this.switchingScrollBoundary = false;
                    VerticalList.this.scrollByUpReceived = 0;
                    n = VerticalList.this.getPositionFromScrollY(VerticalList.this.actualScrollY);
                    final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)recyclerView.findViewHolderForAdapterPosition(n);
                    VerticalList.sLogger.v("onScrollStateChanged idle pos:" + n + " current=" + VerticalList.this.currentMiddlePosition + " scrollY=" + VerticalList.this.actualScrollY + " vh=" + (verticalViewHolder != null));
                    this.moveItemToSelectedState(verticalViewHolder, n);
                    n = VerticalList.this.currentMiddlePosition - 1;
                    if (n >= 0) {
                        this.moveItemToUnSelectedState((VerticalViewHolder)recyclerView.findViewHolderForAdapterPosition(n), n);
                    }
                    n = VerticalList.this.currentMiddlePosition + 1;
                    this.moveItemToUnSelectedState((VerticalViewHolder)recyclerView.findViewHolderForAdapterPosition(n), n);
                    VerticalList.this.onItemSelected();
                    if (VerticalList.this.isSelectedOperationPending) {
                        VerticalList.this.isSelectedOperationPending = false;
                        VerticalList.this.select(true);
                    }
                    else if (VerticalList.this.scrollPendingPos != -1) {
                        n = VerticalList.this.scrollPendingPos;
                        VerticalList.this.scrollPendingPos = -1;
                        VerticalList.this.scrollToPosition(n);
                        VerticalList.sLogger.v("onScrolled idle scrollToPos:" + n);
                    }
                    if (VerticalList.this.sendScrollIdleEvent) {
                        VerticalList.this.callback.onScrollIdle();
                    }
                }
            }
            
            @Override
            public void onScrolled(final RecyclerView recyclerView, final int n, final int n2) {
                super.onScrolled(recyclerView, n, n2);
                VerticalList.this.actualScrollY += n2;
                if (VerticalList.this.waitForTarget) {
                    VerticalList.this.targetFound(0);
                }
            }
        };
        this.selectedList = new HashSet<Integer>();
        if (recyclerView == null || indicator == null || callback == null) {
            throw new IllegalArgumentException();
        }
        VerticalList.sLogger.v("ctor");
        this.twoLineTitles = twoLineTitles;
        this.animationDuration = 230;
        final Context context = recyclerView.getContext();
        (this.recyclerView = recyclerView).setItemViewCacheSize(5);
        final RecyclerView.RecycledViewPool recycledViewPool = this.recyclerView.getRecycledViewPool();
        recycledViewPool.setMaxRecycledViews(ModelType.BLANK.ordinal(), 2);
        recycledViewPool.setMaxRecycledViews(ModelType.ICON.ordinal(), 10);
        recycledViewPool.setMaxRecycledViews(ModelType.ICON_BKCOLOR.ordinal(), 10);
        recycledViewPool.setMaxRecycledViews(ModelType.TWO_ICONS.ordinal(), 10);
        recycledViewPool.setMaxRecycledViews(ModelType.TITLE.ordinal(), 1);
        recycledViewPool.setMaxRecycledViews(ModelType.TITLE_SUBTITLE.ordinal(), 1);
        recycledViewPool.setMaxRecycledViews(ModelType.LOADING.ordinal(), 1);
        recycledViewPool.setMaxRecycledViews(ModelType.ICON_OPTIONS.ordinal(), 1);
        recycledViewPool.setMaxRecycledViews(ModelType.SCROLL_CONTENT.ordinal(), 0);
        recycledViewPool.setMaxRecycledViews(ModelType.LOADING_CONTENT.ordinal(), 10);
        this.recyclerView.setOverScrollMode(2);
        this.recyclerView.setVerticalScrollBarEnabled(false);
        this.recyclerView.setHorizontalScrollBarEnabled(false);
        (this.indicator = indicator).setOrientation(CarouselIndicator.Orientation.VERTICAL);
        this.callback = callback;
        this.containerCallback = containerCallback;
        (this.layoutManager = new VerticalLayoutManager(context, this, callback, recyclerView)).setOrientation(1);
        this.recyclerView.setLayoutManager((RecyclerView.LayoutManager)this.layoutManager);
        this.recyclerView.addOnScrollListener(this.scrollListener);
    }
    
    private void addExtraItems(final List<Model> list) {
        if (this.firstEntryBlank) {
            list.add(0, VerticalList.BLANK_ITEM_TOP);
        }
        list.add(VerticalList.BLANK_ITEM_BOTTOM);
    }
    
    private void animate(final int n, final int n2) {
        this.animate(n, n2, true, false, false);
    }
    
    private void animate(final int n, final int highlightIndex, final boolean b, final boolean b2, final boolean b3) {
        if (n != -1) {
            final RecyclerView.ViewHolder viewHolderForAdapterPosition = this.recyclerView.findViewHolderForAdapterPosition(n);
            if (viewHolderForAdapterPosition != null) {
                final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)viewHolderForAdapterPosition;
                if (verticalViewHolder.getState() == VerticalViewHolder.State.SELECTED) {
                    verticalViewHolder.setState(VerticalViewHolder.State.UNSELECTED, VerticalViewHolder.AnimationType.MOVE, 230);
                }
            }
            else {
                VerticalList.sLogger.v("viewHolder {" + n + "} not found");
            }
            if (b2) {
                final RecyclerView.ViewHolder viewHolderForAdapterPosition2 = this.recyclerView.findViewHolderForAdapterPosition(highlightIndex);
                if (viewHolderForAdapterPosition2 != null) {
                    final VerticalViewHolder verticalViewHolder2 = (VerticalViewHolder)viewHolderForAdapterPosition2;
                    if (verticalViewHolder2.getState() == VerticalViewHolder.State.UNSELECTED && !this.isCloseMenuVisible()) {
                        verticalViewHolder2.setState(VerticalViewHolder.State.SELECTED, VerticalViewHolder.AnimationType.MOVE, 230);
                    }
                }
                else if (b3) {
                    final VerticalViewHolder holderForPos = this.adapter.getHolderForPos(highlightIndex);
                    if (holderForPos == null) {
                        VerticalList.sLogger.v("viewHolder-sel {" + highlightIndex + "} not found in adapter");
                        this.adapter.setHighlightIndex(highlightIndex);
                    }
                    else if (holderForPos.getState() == VerticalViewHolder.State.UNSELECTED && !this.isCloseMenuVisible()) {
                        holderForPos.setState(VerticalViewHolder.State.SELECTED, VerticalViewHolder.AnimationType.MOVE, 230);
                    }
                }
                else {
                    VerticalList.sLogger.v("viewHolder-sel {" + highlightIndex + "} not found");
                }
            }
        }
        if (b) {
            this.animateIndicator(this.getCurrentPosition(), this.animationDuration);
        }
    }
    
    private void calculateScrollPos(final int n) {
        if (this.scrollItemHeight != -1 && n == this.scrollItemIndex + 1) {
            this.reCalcScrollPos = false;
            this.actualScrollY = this.scrollItemIndex * VerticalViewHolder.listItemHeight;
            this.actualScrollY += VerticalViewHolder.listItemHeight * 2;
            VerticalList.sLogger.v("onScrollStateChanged(( calculateScrollPos:" + this.actualScrollY + " pos=" + n + " scrollIndex=" + this.scrollItemIndex);
        }
    }
    
    private void calculateScrollRange() {
        final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.scrollItemIndex);
        if (verticalViewHolder instanceof ScrollableViewHolder && verticalViewHolder.layout.getChildCount() > 0) {
            this.scrollItemStartY = VerticalViewHolder.listItemHeight * this.scrollItemIndex;
            this.scrollItemHeight = verticalViewHolder.layout.getChildAt(0).getMeasuredHeight();
            this.scrollItemEndY = this.scrollItemStartY + this.scrollItemHeight - VerticalViewHolder.listItemHeight * 3;
            if (this.reCalcScrollPos) {
                VerticalList.sLogger.v("calculateScrollPos: recalc scrollItemHeight=" + this.scrollItemHeight + " scrollItemStartY=" + this.scrollItemStartY + " scrollItemEndY=" + this.scrollItemEndY);
                this.calculateScrollPos(this.currentMiddlePosition);
            }
        }
        else {
            VerticalList.sLogger.v("calculateScrollRange:" + this.scrollItemIndex + " vh not found");
        }
    }
    
    private int canScrollDown() {
        final int n = -1;
        int n2;
        if (!this.hasScrollableElement) {
            n2 = n;
        }
        else {
            this.calculateScrollRange();
            n2 = n;
            if (this.scrollItemStartY != -1) {
                n2 = n;
                if (this.actualScrollY < this.scrollItemEndY) {
                    n2 = this.scrollItemEndY - this.actualScrollY;
                }
            }
        }
        return n2;
    }
    
    private int canScrollUp() {
        final int n = -1;
        int n2;
        if (!this.hasScrollableElement) {
            n2 = n;
        }
        else {
            this.calculateScrollRange();
            n2 = n;
            if (this.scrollItemStartY != -1) {
                final int n3 = VerticalViewHolder.listItemHeight * this.scrollItemIndex;
                n2 = n;
                if (this.actualScrollY > n3) {
                    n2 = this.actualScrollY - n3;
                }
            }
        }
        return n2;
    }
    
    private void cleanupView(final RecyclerView.RecycledViewPool recycledViewPool, final int n) {
        while (true) {
            final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)recycledViewPool.getRecycledView(n);
            if (verticalViewHolder == null) {
                break;
            }
            verticalViewHolder.clearAnimation();
            verticalViewHolder.layout.removeAllViews();
        }
    }
    
    private void clearViewAnimation(final RecyclerView.RecycledViewPool recycledViewPool, final int n) {
        while (true) {
            final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)recycledViewPool.getRecycledView(n);
            if (verticalViewHolder == null) {
                break;
            }
            verticalViewHolder.clearAnimation();
            this.copyList.add(verticalViewHolder);
        }
    }
    
    private void down(final int n, final int currentMiddlePosition) {
        SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
        this.animate(n, this.currentMiddlePosition = currentMiddlePosition);
        this.isScrolling = true;
        this.layoutManager.smoothScrollToPosition(n, -1);
    }
    
    public static View findCrossFadeImageView(final View view) {
        return view.findViewById(R.id.vlist_image);
    }
    
    public static ImageView findImageView(final View view) {
        return (ImageView)view.findViewById(R.id.big);
    }
    
    public static ImageView findSmallImageView(final View view) {
        return (ImageView)view.findViewById(R.id.small);
    }
    
    private int getExtraItemCount() {
        return this.adapter.getItemCount() - 3;
    }
    
    public static FontInfo getFontInfo(final FontSize fontSize) {
        return VerticalList.tit_subt_map.get(fontSize);
    }
    
    public static Interpolator getInterpolator() {
        return new FastOutSlowInInterpolator();
    }
    
    private int getPositionFromScrollY(int scrollItemIndex) {
        if (!this.hasScrollableElement) {
            scrollItemIndex = scrollItemIndex / VerticalViewHolder.listItemHeight + 1;
        }
        else {
            if (this.scrollItemStartY == -1) {
                this.calculateScrollRange();
            }
            if (scrollItemIndex < this.scrollItemStartY) {
                scrollItemIndex = scrollItemIndex / VerticalViewHolder.listItemHeight + 1;
            }
            else if (scrollItemIndex > this.scrollItemEndY) {
                scrollItemIndex = (scrollItemIndex - this.scrollItemEndY - VerticalViewHolder.listItemHeight * 2) / VerticalViewHolder.listItemHeight + 1 + this.scrollItemIndex;
            }
            else {
                scrollItemIndex = this.scrollItemIndex;
            }
        }
        return scrollItemIndex;
    }
    
    private boolean isCloseMenuVisible() {
        return this.containerCallback != null && this.containerCallback.isCloseMenuVisible();
    }
    
    private boolean isLocked() {
        return this.lockList;
    }
    
    private boolean isOverrideKey(final InputManager.CustomKeyEvent customKeyEvent) {
        final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        return verticalViewHolder != null && verticalViewHolder.handleKey(customKeyEvent);
    }
    
    private boolean isScrollItemAlignedToBottomEdge() {
        boolean b = false;
        if (this.hasScrollableElement && this.actualScrollY == this.scrollItemEndY + VerticalViewHolder.listItemHeight) {
            b = true;
        }
        return b;
    }
    
    private boolean isScrollItemAlignedToTopEdge() {
        boolean b = false;
        if (this.hasScrollableElement && this.actualScrollY == VerticalViewHolder.listItemHeight * this.scrollItemIndex) {
            b = true;
        }
        return b;
    }
    
    private boolean isScrollItemInMiddle() {
        boolean b2;
        final boolean b = b2 = false;
        if (this.hasScrollableElement) {
            if (this.scrollItemIndex <= 0) {
                b2 = b;
            }
            else {
                final int listItemHeight = VerticalViewHolder.listItemHeight;
                final int scrollItemIndex = this.scrollItemIndex;
                b2 = b;
                if (this.actualScrollY == listItemHeight * (scrollItemIndex - 1)) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    private boolean isScrollPosInScrollItem() {
        final boolean b = false;
        boolean b2;
        if (!this.hasScrollableElement) {
            b2 = b;
        }
        else {
            this.calculateScrollRange();
            b2 = b;
            if (this.scrollItemStartY != -1) {
                b2 = b;
                if (this.actualScrollY >= this.scrollItemStartY) {
                    b2 = b;
                    if (this.actualScrollY <= this.scrollItemEndY) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    private void onItemSelected() {
        final int currentPosition = this.getCurrentPosition();
        final Model model = this.adapter.getModel(this.currentMiddlePosition);
        if (model != null) {
            final int n = -1;
            final int n2 = -1;
            int currentSelectionId = n;
            int currentSelection = n2;
            if (model.type == ModelType.ICON_OPTIONS) {
                final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
                currentSelectionId = n;
                currentSelection = n2;
                if (verticalViewHolder instanceof IconOptionsViewHolder) {
                    final IconOptionsViewHolder iconOptionsViewHolder = (IconOptionsViewHolder)verticalViewHolder;
                    currentSelectionId = iconOptionsViewHolder.getCurrentSelectionId();
                    currentSelection = iconOptionsViewHolder.getCurrentSelection();
                }
            }
            this.itemSelectionState.set(model, model.id, currentPosition, currentSelectionId, currentSelection);
            this.callback.onItemSelected(this.itemSelectionState);
        }
    }
    
    private void select(final boolean b) {
        if (!this.isLocked() || b) {
            this.lockList = true;
            if (this.isScrolling) {
                this.isSelectedOperationPending = true;
            }
            else {
                SoundUtils.playSound(SoundUtils.Sound.MENU_SELECT);
                final int rawPosition = this.getRawPosition();
                final Model model = this.modelList.get(rawPosition);
                final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(rawPosition);
                if (verticalViewHolder != null) {
                    verticalViewHolder.select(model, this.getCurrentPosition(), 50);
                }
            }
        }
    }
    
    public static void setFontSize(final Model model, final boolean b) {
        model.fontSizeCheckDone = true;
        model.subTitle_2Lines = false;
        if (model.title == null) {
            model.fontInfo = VerticalList.tit_subt_map.get(FontSize.FONT_SIZE_26);
            model.fontSize = FontSize.FONT_SIZE_26;
        }
        else if (model.subTitle2 != null || model.subTitle == null) {
            setTitleSize(model, b);
        }
        else {
            setTitleSubTitleSize(model, b);
        }
    }
    
    private static void setTitleSize(final Model model, final boolean b) {
        VerticalList.fontSizeTextView.setText(model.title);
        final int[] array = new int[2];
        final TextView fontSizeTextView = VerticalList.fontSizeTextView;
        int[] array2;
        if (b) {
            array2 = VerticalList.TWO_LINE_MAX_LINES;
        }
        else {
            array2 = VerticalList.SINGLE_LINE_MAX_LINES;
        }
        ViewUtil.autosize(fontSizeTextView, array2, VerticalList.vlistTitleTextW, VerticalList.TITLE_SIZES, array);
        final FontSize fontSize = VerticalList.FONT_SIZES[array[0] * 2 + (array[1] - 1)];
        VerticalList.sLogger.d("Setting size for " + model.title + " to " + fontSize);
        if (model.subTitle != null && model.subTitle2 != null) {
            model.fontInfo = VerticalList.tit_subt_subt2_map.get(fontSize);
        }
        else {
            model.fontInfo = VerticalList.tit_subt_map.get(fontSize);
        }
        model.fontSize = fontSize;
    }
    
    private static void setTitleSubTitleSize(final Model model, boolean b) {
        final TextPaint paint = VerticalList.fontSizeTextView.getPaint();
        VerticalList.fontSizeTextView.setTextSize(VerticalList.vlistTitle_18);
        final StaticLayout staticLayout = new StaticLayout(model.subTitle, paint, VerticalList.vlistTitleTextW, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        int lineCount;
        final int n = lineCount = staticLayout.getLineCount();
        if (!TextUtils.isEmpty(model.subTitle2)) {
            ++lineCount;
        }
        if (lineCount > 1) {
            b = false;
        }
        setTitleSize(model, b);
        if (n != 1) {
            model.subTitle_2Lines = true;
            Enum<FontSize> fontSize = null;
            if (staticLayout.getLineCount() == 2) {
                switch (model.fontSize) {
                    case FONT_SIZE_26:
                        fontSize = FontSize.FONT_SIZE_26_18;
                        break;
                    case FONT_SIZE_22:
                        fontSize = FontSize.FONT_SIZE_22_18;
                        break;
                    case FONT_SIZE_22_2:
                        fontSize = FontSize.FONT_SIZE_22_2_16;
                        break;
                    case FONT_SIZE_18:
                        fontSize = FontSize.FONT_SIZE_18_16;
                        break;
                    case FONT_SIZE_18_2:
                        fontSize = FontSize.FONT_SIZE_18_2_16;
                        break;
                    case FONT_SIZE_16:
                        fontSize = FontSize.FONT_SIZE_16_16;
                        break;
                    case FONT_SIZE_16_2:
                        fontSize = FontSize.FONT_SIZE_16_2_16;
                        break;
                }
            }
            else {
                switch (model.fontSize) {
                    case FONT_SIZE_26:
                        fontSize = FontSize.FONT_SIZE_26_16;
                        break;
                    case FONT_SIZE_22:
                        fontSize = FontSize.FONT_SIZE_22_16;
                        break;
                    case FONT_SIZE_22_2:
                        fontSize = FontSize.FONT_SIZE_22_2_16;
                        break;
                    case FONT_SIZE_18:
                        fontSize = FontSize.FONT_SIZE_18_16;
                        break;
                    case FONT_SIZE_18_2:
                        fontSize = FontSize.FONT_SIZE_18_2_16;
                        break;
                    case FONT_SIZE_16:
                        fontSize = FontSize.FONT_SIZE_16_16;
                        break;
                    case FONT_SIZE_16_2:
                        fontSize = FontSize.FONT_SIZE_16_2_16;
                        break;
                }
            }
            if (fontSize != null) {
                model.fontInfo = VerticalList.tit_subt_map_2_lines.get(fontSize);
                model.fontSize = (FontSize)fontSize;
                VerticalList.sLogger.d("Adjusting size for " + model.title + " to " + fontSize);
            }
        }
    }
    
    private void up(final int n, final int currentMiddlePosition) {
        SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
        this.currentMiddlePosition = currentMiddlePosition;
        this.isScrolling = true;
        this.keyHandlerState.keyHandled = true;
        if (this.hasScrollableElement) {
            boolean b = false;
            boolean b2 = false;
            if (this.isScrollItemInMiddle()) {
                this.switchingScrollBoundary = true;
                this.recyclerView.smoothScrollBy(0, -VerticalViewHolder.listItemHeight);
                b = true;
            }
            else if (this.isScrollItemAlignedToTopEdge()) {
                this.isScrollBy = true;
                this.switchingScrollBoundary = true;
                b = true;
                b2 = true;
                this.layoutManager.smoothScrollToPosition(this.currentMiddlePosition - 1, -1);
            }
            else if (this.currentMiddlePosition == this.scrollItemIndex) {
                final float n2 = VerticalViewHolder.listItemHeight * 2;
                this.switchingScrollBoundary = true;
                this.recyclerView.smoothScrollBy(0, (int)(-n2));
                b = true;
            }
            else {
                this.layoutManager.smoothScrollToPosition(n, 1);
            }
            this.animate(n, currentMiddlePosition, true, b, b2);
        }
        else {
            this.animate(n, currentMiddlePosition);
            this.layoutManager.smoothScrollToPosition(n, 1);
        }
    }
    
    private void updateView(final List<Model> list, int n, final boolean firstEntryBlank, final boolean hasScrollableElement, final int scrollItemIndex) {
        if (!GenericUtil.isMainThread()) {
            throw new RuntimeException("updateView can only be called from main thread");
        }
        if (this.isLocked()) {
            VerticalList.sLogger.w("cannot update vlist during lock");
        }
        else {
            final int size = list.size();
            if (size == 0) {
                throw new IllegalArgumentException("empty list now allowed");
            }
            this.clearAllAnimations();
            this.recyclerView.stopScroll();
            VerticalList.sLogger.v("updateView [" + size + "] sel:" + n + " firstEntryBlank:" + firstEntryBlank + " scrollContent:" + hasScrollableElement + " scrollIndex:" + scrollItemIndex);
            this.modelList = new ArrayList<Model>(list);
            int currentItem = n;
            if (size <= 1) {
                currentItem = 0;
            }
            this.firstEntryBlank = firstEntryBlank;
            this.hasScrollableElement = hasScrollableElement;
            this.scrollItemIndex = scrollItemIndex;
            this.scrollItemStartY = -1;
            this.scrollItemEndY = -1;
            this.scrollItemHeight = -1;
            this.addExtraItems(this.modelList);
            this.currentMiddlePosition = currentItem + 1;
            if (hasScrollableElement && firstEntryBlank) {
                ++this.scrollItemIndex;
            }
            int itemCount;
            n = (itemCount = size);
            if (!firstEntryBlank) {
                itemCount = n - 1;
            }
            this.indicator.setItemCount(itemCount);
            this.indicator.setCurrentItem(currentItem);
            this.layoutManager.clear();
            ((RecyclerView.Adapter)(this.adapter = new VerticalAdapter(this.modelList, this))).setHasStableIds(true);
            n = 0;
            if (hasScrollableElement) {
                int[] viewHolderCacheIndex;
                if (list.size() == this.scrollItemIndex) {
                    viewHolderCacheIndex = new int[] { this.scrollItemIndex - 1 };
                    n = 1;
                }
                else {
                    viewHolderCacheIndex = new int[] { this.scrollItemIndex - 1, this.scrollItemIndex + 1 };
                    n = 2;
                }
                this.adapter.setViewHolderCacheIndex(viewHolderCacheIndex);
            }
            this.recyclerView.setAdapter((RecyclerView.Adapter)this.adapter);
            this.initialPosChanged = false;
            final int n2 = this.currentMiddlePosition - 1;
            int n3 = 0;
            int currentMiddlePosition;
            if (!hasScrollableElement) {
                this.actualScrollY = VerticalViewHolder.listItemHeight * currentItem;
                currentMiddlePosition = n2;
            }
            else {
                VerticalList.sLogger.v("updateView initial:" + currentItem + " scrollIndex=" + scrollItemIndex);
                if (currentItem == scrollItemIndex) {
                    currentMiddlePosition = this.currentMiddlePosition;
                    this.actualScrollY = (currentItem + 1) * VerticalViewHolder.listItemHeight;
                    VerticalList.sLogger.v("updateView: scroll index selected:" + this.actualScrollY);
                }
                else if (currentItem < this.scrollItemIndex) {
                    this.actualScrollY = VerticalViewHolder.listItemHeight * currentItem;
                    currentMiddlePosition = n2;
                }
                else {
                    this.reCalcScrollPos = true;
                    VerticalList.sLogger.v("reCalcScrollPos no ht");
                    this.actualScrollY = VerticalViewHolder.listItemHeight * currentItem;
                    currentMiddlePosition = n2;
                    if (currentItem == scrollItemIndex + 1) {
                        n3 = 1;
                        currentMiddlePosition = n2;
                    }
                }
            }
            if (n3 != 0) {
                VerticalList.sLogger.v("updateView special scroll");
                this.layoutManager.scrollToPositionWithOffset(currentMiddlePosition + 1, VerticalViewHolder.listItemHeight);
            }
            else {
                this.layoutManager.scrollToPositionWithOffset(currentMiddlePosition, 0);
            }
            VerticalList.sLogger.v("updateView scroll to " + currentMiddlePosition + " scrollPos=" + this.actualScrollY + " middle=" + this.getCurrentPosition() + " raw=" + this.getRawPosition() + " adapter-cache:" + n + " scrollIndex:" + scrollItemIndex);
            VerticalList.handler.post(this.initialPosCheckRunnable);
        }
    }
    
    public void addCurrentHighlightAnimation(final AnimatorSet.Builder animatorSet$Builder) {
        final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition - 1);
        if (verticalViewHolder != null) {
            animatorSet$Builder.with(ObjectAnimator.ofFloat(verticalViewHolder.layout, View.ALPHA, new float[] { 0.0f }));
        }
        final VerticalViewHolder verticalViewHolder2 = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition + 1);
        if (verticalViewHolder2 != null) {
            animatorSet$Builder.with(ObjectAnimator.ofFloat(verticalViewHolder2.layout, View.ALPHA, new float[] { 0.0f }));
        }
    }
    
    public boolean allowsTwoLineTitles() {
        return this.twoLineTitles;
    }
    
    public void animate(final int n, final boolean b, final int n2, final boolean b2) {
        this.animate(n, b, n2, b2, false);
    }
    
    public void animate(final int n, final boolean b, final int n2, final boolean b2, final boolean b3) {
        int animationDuration = n2;
        if (n2 == -1) {
            animationDuration = this.animationDuration;
        }
        final RecyclerView.ViewHolder viewHolderForAdapterPosition = this.recyclerView.findViewHolderForAdapterPosition(n);
        if (viewHolderForAdapterPosition != null) {
            final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)viewHolderForAdapterPosition;
            if (b) {
                if (VerticalList.sLogger.isLoggable(2)) {
                    VerticalList.sLogger.v("animate:" + n + " selected");
                }
                verticalViewHolder.setState(VerticalViewHolder.State.SELECTED, VerticalViewHolder.AnimationType.MOVE, animationDuration);
                if (verticalViewHolder.hasToolTip() && this.containerCallback != null) {
                    this.containerCallback.showToolTips();
                }
            }
            else {
                if (VerticalList.sLogger.isLoggable(2)) {
                    VerticalList.sLogger.v("animate:" + n + " un-selected");
                }
                verticalViewHolder.setState(VerticalViewHolder.State.UNSELECTED, VerticalViewHolder.AnimationType.MOVE, animationDuration);
                if (verticalViewHolder.hasToolTip() && this.containerCallback != null) {
                    this.containerCallback.hideToolTips();
                }
            }
        }
        else if (VerticalList.sLogger.isLoggable(2)) {
            VerticalList.sLogger.v("animate:" + n + " cannot find viewholder");
        }
    }
    
    public void animateIndicator(final int n, final int n2) {
        if (this.containerCallback == null || !this.containerCallback.isFastScrolling()) {
            if (this.indicatorAnimatorSet != null && this.indicatorAnimatorSet.isRunning()) {
                this.indicatorAnimatorSet.removeAllListeners();
                this.indicatorAnimatorSet.cancel();
                this.indicator.setCurrentItem(this.getCurrentPosition());
            }
            this.indicatorAnimatorSet = this.indicator.getItemMoveAnimator(n, -1);
            if (this.indicatorAnimatorSet != null) {
                this.indicatorAnimatorSet.setDuration((long)n2);
                this.indicatorAnimatorSet.addListener(this.indicatorAnimationListener);
                this.indicatorAnimatorSet.start();
            }
        }
    }
    
    public void callItemSelected(final ItemSelectionState itemSelectionState) {
        if (this.callback != null) {
            this.callback.onItemSelected(itemSelectionState);
        }
    }
    
    public void cancelLoadingAnimation(final int n) {
        int n2 = n;
        if (this.firstEntryBlank) {
            n2 = n + 1;
        }
        final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(n2);
        if (verticalViewHolder != null && verticalViewHolder.getModelType() == ModelType.LOADING) {
            verticalViewHolder.clearAnimation();
        }
    }

    public void clearAllAnimations() {
        int i = this.recyclerView.getChildCount();
        int i0 = 0;
        int i1 = 0;
        while(i1 < i) {
            try {
                VerticalViewHolder a = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(i1);
                if (a != null) {
                    a.clearAnimation();
                    i0 = i0 + 1;
                }
            } catch(Throwable a0) {
                sLogger.e(a0);
            }
            i1 = i1 + 1;
        }
        sLogger.v(new StringBuilder().append("clearAllAnimations current:").append(i0).toString());
        android.support.v7.widget.RecyclerView.RecycledViewPool a1 = this.recyclerView.getRecycledViewPool();
        if (a1 != null) {
            this.copyList.clear();
            this.clearViewAnimation(a1, VerticalList.ModelType.ICON_BKCOLOR.ordinal());
            this.clearViewAnimation(a1, VerticalList.ModelType.TWO_ICONS.ordinal());
            this.clearViewAnimation(a1, VerticalList.ModelType.ICON.ordinal());
            this.clearViewAnimation(a1, VerticalList.ModelType.LOADING.ordinal());
            this.clearViewAnimation(a1, VerticalList.ModelType.LOADING_CONTENT.ordinal());
            this.clearViewAnimation(a1, VerticalList.ModelType.ICON_OPTIONS.ordinal());
            this.cleanupView(a1, VerticalList.ModelType.SCROLL_CONTENT.ordinal());
            int i2 = this.copyList.size();
            if (i2 > 0) {
                Object a2 = this.copyList.iterator();
                while(((Iterator)a2).hasNext()) {
                    a1.putRecycledView((android.support.v7.widget.RecyclerView.ViewHolder)((Iterator)a2).next());
                }
                sLogger.v(new StringBuilder().append("clearAllAnimations pool:").append(i2).toString());
                this.copyList.clear();
            }
        }
    }
    
    public void copyAndPosition(final ImageView imageView, final TextView textView, final TextView textView2, final TextView textView3, final boolean b) {
        final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        if (verticalViewHolder != null) {
            verticalViewHolder.copyAndPosition(imageView, textView, textView2, textView3, b);
        }
    }
    
    public KeyHandlerState down() {
        this.keyHandlerState.clear();
        KeyHandlerState keyHandlerState;
        if (this.isLocked()) {
            VerticalList.sLogger.v("down locked");
            keyHandlerState = this.keyHandlerState;
        }
        else if (this.switchingScrollBoundary) {
            this.keyHandlerState.keyHandled = true;
            keyHandlerState = this.keyHandlerState;
        }
        else {
            if (this.hasScrollableElement) {
                if (this.currentMiddlePosition + 1 == this.scrollItemIndex) {
                    final int currentMiddlePosition = this.currentMiddlePosition;
                    ++this.currentMiddlePosition;
                    SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
                    this.switchingScrollBoundary = true;
                    this.isScrolling = true;
                    this.layoutManager.smoothScrollToPosition(this.currentMiddlePosition, -1);
                    this.animate(currentMiddlePosition, this.currentMiddlePosition, true, true, false);
                    this.keyHandlerState.listMoved = true;
                    this.keyHandlerState.keyHandled = true;
                    keyHandlerState = this.keyHandlerState;
                    return keyHandlerState;
                }
                if (this.isScrollPosInScrollItem()) {
                    final int canScrollDown = this.canScrollDown();
                    if (canScrollDown != -1) {
                        SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
                        if (this.isScrolling) {
                            this.currentScrollBy += 0.5f;
                            if (this.currentScrollBy > 2.0f) {
                                this.currentScrollBy = 2.0f;
                            }
                        }
                        else {
                            this.currentScrollBy = 1.0f;
                        }
                        float n = VerticalViewHolder.scrollDistance * this.currentScrollBy;
                        if (canScrollDown <= n) {
                            n = canScrollDown;
                        }
                        this.isScrolling = true;
                        this.recyclerView.smoothScrollBy(0, (int)n);
                        this.keyHandlerState.keyHandled = true;
                        keyHandlerState = this.keyHandlerState;
                        return keyHandlerState;
                    }
                    if (!this.isBottom()) {
                        SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
                        final float n2 = VerticalViewHolder.listItemHeight * 2;
                        final int currentMiddlePosition2 = this.currentMiddlePosition;
                        final int currentMiddlePosition3 = this.currentMiddlePosition + 1;
                        this.switchingScrollBoundary = true;
                        this.isScrolling = true;
                        this.currentMiddlePosition = currentMiddlePosition3;
                        this.recyclerView.smoothScrollBy(0, (int)n2);
                        this.animate(currentMiddlePosition2, currentMiddlePosition3, true, true, true);
                        this.keyHandlerState.listMoved = true;
                        this.keyHandlerState.keyHandled = true;
                        keyHandlerState = this.keyHandlerState;
                        return keyHandlerState;
                    }
                }
            }
            if (this.isBottom()) {
                VerticalList.sLogger.v("cannot go down:" + this.currentMiddlePosition);
                keyHandlerState = this.keyHandlerState;
            }
            else {
                this.keyHandlerState.keyHandled = true;
                if (this.isOverrideKey(InputManager.CustomKeyEvent.RIGHT)) {
                    SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
                    this.keyHandlerState.listMoved = false;
                    keyHandlerState = this.keyHandlerState;
                }
                else {
                    this.keyHandlerState.listMoved = true;
                    this.down(this.currentMiddlePosition, this.currentMiddlePosition + 1);
                    keyHandlerState = this.keyHandlerState;
                }
            }
        }
        return keyHandlerState;
    }
    
    public Model getCurrentModel() {
        return this.adapter.getModel(this.getRawPosition());
    }
    
    public int getCurrentPosition() {
        return this.currentMiddlePosition - 1;
    }
    
    public VerticalViewHolder getCurrentViewHolder() {
        return this.getViewHolder(this.currentMiddlePosition);
    }
    
    public ItemSelectionState getItemSelectionState() {
        return this.itemSelectionState;
    }
    
    public int getRawPosition() {
        return this.currentMiddlePosition;
    }
    
    public VerticalViewHolder getViewHolder(final int n) {
        VerticalViewHolder verticalViewHolder;
        if (this.adapter.getModel(n) == null) {
            VerticalList.sLogger.i("getViewHolder: invalid pos:" + n);
            verticalViewHolder = null;
        }
        else {
            verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.currentMiddlePosition);
        }
        return verticalViewHolder;
    }
    
    public boolean hasScrollItem() {
        return this.hasScrollableElement;
    }
    
    public boolean isBottom() {
        return this.currentMiddlePosition == this.modelList.size() - 2;
    }
    
    public boolean isFirstEntryBlank() {
        return this.firstEntryBlank;
    }
    
    public boolean isTop() {
        boolean b = true;
        if (this.currentMiddlePosition != 1) {
            b = false;
        }
        return b;
    }
    
    public void lock() {
        this.lockList = true;
    }
    
    public void performSelectAction(final ItemSelectionState itemSelectionState) {
        this.callback.select(itemSelectionState);
    }
    
    public void refreshData(final int n) {
        this.refreshData(n, (Model)null);
    }
    
    public void refreshData(final int n, final Model model) {
        this.refreshData(n, model, false);
    }
    
    public void refreshData(final int n, final Model model, final boolean dontStartFluctuator) {
        int n2 = n;
        if (this.firstEntryBlank) {
            n2 = n + 1;
        }
        final Model model2 = this.adapter.getModel(n2);
        if (model2 == null) {
            VerticalList.sLogger.i("refreshData: invalid model pos:" + n2);
        }
        else {
            if (model == null) {
                model2.needsRebind = true;
                model2.dontStartFluctuator = dontStartFluctuator;
            }
            else {
                model.needsRebind = true;
                this.adapter.updateModel(n2, model);
            }
            ((RecyclerView.Adapter)this.adapter).notifyItemChanged(n2);
        }
    }
    
    public void refreshData(int i, final Model[] array) {
        int n = i;
        if (this.firstEntryBlank) {
            n = i + 1;
        }
        int n2 = 0;
        int n3;
        for (i = 0; i < array.length; ++i) {
            n3 = n + i;
            if (this.adapter.getModel(n3) == null) {
                VerticalList.sLogger.i("refreshData(s): invalid model pos:" + n3);
            }
            else {
                ++n2;
                this.adapter.updateModel(n3, array[i]);
            }
        }
        this.adapter.setInitialState(false);
        ((RecyclerView.Adapter)this.adapter).notifyItemRangeChanged(n, n2);
        VerticalList.sLogger.i("refreshData(s) pos=" + n + " len=" + n2);
    }
    
    public void scrollToPosition(int n) {
        final Model model = this.adapter.getModel(n);
        if (model != null) {
            if (this.isLocked()) {
                VerticalList.sLogger.v("scrollToPos list locked:" + n);
            }
            else if (this.isScrolling) {
                VerticalList.sLogger.v("scrollToPos wait for scroll idle:" + n);
                this.scrollPendingPos = n;
            }
            else {
                VerticalList.sLogger.v("scrollToPos:" + n + " , " + model.title);
                this.currentMiddlePosition = n;
                if ((n = this.currentMiddlePosition - 1) < 0) {
                    n = 0;
                }
                this.actualScrollY = VerticalViewHolder.listItemHeight * n;
                this.adapter.setInitialState(false);
                this.layoutManager.scrollToPositionWithOffset(n, 0);
                VerticalList.sLogger.v("scrollToPos scroll to " + n + " scrollPos=" + this.actualScrollY + " middle=" + this.getCurrentPosition() + " raw=" + this.getRawPosition());
                this.scrollListener.onScrollStateChanged(this.recyclerView, 0);
            }
        }
    }
    
    public void select() {
        this.select(false);
    }
    
    public void setBindCallbacks(final boolean bindCallbacks) {
        this.bindCallbacks = bindCallbacks;
    }
    
    public void setScrollIdleEvent(final boolean sendScrollIdleEvent) {
        this.sendScrollIdleEvent = sendScrollIdleEvent;
    }
    
    public void setViewHolderState(final int n, final VerticalViewHolder.State state, final VerticalViewHolder.AnimationType animationType, final int n2) {
        final VerticalViewHolder viewHolder = this.getViewHolder(n);
        if (viewHolder != null && viewHolder.getState() != state) {
            VerticalList.sLogger.v("setViewHolderState:" + n + ":" + state);
            viewHolder.setState(state, animationType, n2);
        }
    }
    
    public void targetFound(int intValue) {
        if (!this.hasScrollableElement || !this.isScrollPosInScrollItem()) {
            final int positionFromScrollY = this.getPositionFromScrollY(this.actualScrollY + intValue);
            final boolean loggable = VerticalList.sLogger.isLoggable(2);
            if (intValue != 0 || !this.waitForTarget || positionFromScrollY == this.targetPos) {
                this.waitForTarget = false;
                this.targetFound = false;
                this.targetPos = -1;
                final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(positionFromScrollY);
                if (verticalViewHolder == null) {
                    this.waitForTarget = true;
                    this.targetPos = positionFromScrollY;
                }
                else {
                    this.targetFound = true;
                    this.currentMiddlePosition = positionFromScrollY;
                    final HashSet<Integer> selectedList = this.selectedList;
                    this.selectedList = new HashSet<Integer>();
                    if (loggable) {
                        VerticalList.sLogger.v("targetFound selected list size = " + selectedList.size());
                    }
                    if (selectedList.size() > 0) {
                        final Iterator<Integer> iterator = selectedList.iterator();
                        while (iterator.hasNext()) {
                            intValue = iterator.next();
                            final VerticalViewHolder verticalViewHolder2 = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(intValue);
                            if (verticalViewHolder2 != null && verticalViewHolder2.getState() == VerticalViewHolder.State.SELECTED) {
                                verticalViewHolder2.setState(VerticalViewHolder.State.UNSELECTED, VerticalViewHolder.AnimationType.MOVE, 230);
                            }
                        }
                    }
                    if (verticalViewHolder != null && verticalViewHolder.getState() == VerticalViewHolder.State.UNSELECTED && !this.isCloseMenuVisible()) {
                        verticalViewHolder.setState(VerticalViewHolder.State.SELECTED, VerticalViewHolder.AnimationType.MOVE, 230);
                    }
                }
            }
        }
    }
    
    public void unlock() {
        this.unlock(true);
    }
    
    public void unlock(final boolean b) {
        this.lockList = false;
        final VerticalViewHolder verticalViewHolder = (VerticalViewHolder)this.recyclerView.findViewHolderForAdapterPosition(this.getRawPosition());
        if (verticalViewHolder != null) {
            if (b) {
                verticalViewHolder.startFluctuator();
            }
            else {
                verticalViewHolder.clearAnimation();
            }
        }
    }
    
    public KeyHandlerState up() {
        this.keyHandlerState.clear();
        KeyHandlerState keyHandlerState;
        if (this.isLocked()) {
            VerticalList.sLogger.v("up locked");
            keyHandlerState = this.keyHandlerState;
        }
        else if (this.switchingScrollBoundary) {
            this.keyHandlerState.keyHandled = true;
            keyHandlerState = this.keyHandlerState;
        }
        else if (this.reCalcScrollPos && this.isScrolling) {
            this.keyHandlerState.keyHandled = true;
            keyHandlerState = this.keyHandlerState;
        }
        else {
            if (this.hasScrollableElement && this.isScrollPosInScrollItem()) {
                final int canScrollUp = this.canScrollUp();
                if (canScrollUp != -1) {
                    if (this.isScrolling) {
                        this.currentScrollBy += 0.5f;
                        if (this.currentScrollBy > 2.0f) {
                            this.currentScrollBy = 2.0f;
                        }
                    }
                    else {
                        this.currentScrollBy = 1.0f;
                    }
                    float n = VerticalViewHolder.scrollDistance * this.currentScrollBy;
                    if (canScrollUp <= n) {
                        n = canScrollUp;
                    }
                    this.isScrolling = true;
                    this.recyclerView.smoothScrollBy(0, (int)(-n));
                    SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
                    this.keyHandlerState.keyHandled = true;
                    keyHandlerState = this.keyHandlerState;
                    return keyHandlerState;
                }
            }
            if (this.isTop()) {
                if (this.isOverrideKey(InputManager.CustomKeyEvent.LEFT)) {
                    SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
                    this.keyHandlerState.keyHandled = true;
                    this.keyHandlerState.listMoved = false;
                    keyHandlerState = this.keyHandlerState;
                }
                else {
                    if (this.isScrollBy) {
                        ++this.scrollByUpReceived;
                        this.keyHandlerState.keyHandled = true;
                    }
                    if (this.isScrolling) {
                        this.keyHandlerState.keyHandled = true;
                    }
                    VerticalList.sLogger.v("cannot go up:" + this.currentMiddlePosition + " isScrolling:" + this.isScrolling);
                    keyHandlerState = this.keyHandlerState;
                }
            }
            else {
                this.keyHandlerState.keyHandled = true;
                if (this.isOverrideKey(InputManager.CustomKeyEvent.LEFT)) {
                    SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
                    this.keyHandlerState.listMoved = false;
                    keyHandlerState = this.keyHandlerState;
                }
                else {
                    this.keyHandlerState.listMoved = true;
                    this.up(this.currentMiddlePosition, this.currentMiddlePosition - 1);
                    keyHandlerState = this.keyHandlerState;
                }
            }
        }
        return keyHandlerState;
    }
    
    public void updateView(final List<Model> list, final int n, final boolean b) {
        this.updateView(list, n, b, false, -1);
    }
    
    public void updateViewWithScrollableContent(final List<Model> list, final int n, final boolean b) {
        int n2 = 0;
        int n3 = -1;
        int n4 = 0;
        final Iterator<Model> iterator = list.iterator();
        while (iterator.hasNext()) {
            int n5 = n2;
            if (iterator.next().type == ModelType.SCROLL_CONTENT) {
                n5 = n2 + 1;
                n3 = n4;
            }
            ++n4;
            n2 = n5;
        }
        if (list.size() == 1 || n2 == 0 || n2 > 1) {
            throw new RuntimeException("invalid scroll item model size=" + list.size() + " count=" + n2);
        }
        this.updateView(list, n, b, true, n3);
    }
    
    public interface Callback
    {
        void onBindToView(final Model p0, final View p1, final int p2, final ModelState p3);
        
        void onItemSelected(final ItemSelectionState p0);
        
        void onLoad();
        
        void onScrollIdle();
        
        void select(final ItemSelectionState p0);
    }
    
    public interface ContainerCallback
    {
        void hideToolTips();
        
        boolean isCloseMenuVisible();
        
        boolean isFastScrolling();
        
        void showToolTips();
    }

    public enum Direction {
        UP(0),
        DOWN(1);

        private int value;
        Direction(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
    public static class FontInfo
    {
        public float subTitle2FontSize;
        public float subTitle2FontTopMargin;
        public float subTitleFontSize;
        public float subTitleFontTopMargin;
        public float titleFontSize;
        public float titleFontTopMargin;
        public float titleScale;
        public boolean titleSingleLine;
        
        public FontInfo() {
            this.titleSingleLine = true;
        }
    }

    public enum FontSize {
        FONT_SIZE_26(0),
        FONT_SIZE_22(1),
        FONT_SIZE_22_2(2),
        FONT_SIZE_18(3),
        FONT_SIZE_18_2(4),
        FONT_SIZE_16(5),
        FONT_SIZE_16_2(6),
        FONT_SIZE_26_18(7),
        FONT_SIZE_22_18(8),
        FONT_SIZE_22_2_18(9),
        FONT_SIZE_18_16(10),
        FONT_SIZE_18_2_16(11),
        FONT_SIZE_16_16(12),
        FONT_SIZE_16_2_16(13),
        FONT_SIZE_26_16(14),
        FONT_SIZE_22_16(15),
        FONT_SIZE_22_2_16(16);

        private int value;
        FontSize(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
    public static class ItemSelectionState
    {
        public int id;
        public Model model;
        public int pos;
        public int subId;
        public int subPosition;
        
        public ItemSelectionState() {
            this.id = -1;
            this.pos = -1;
            this.subId = -1;
            this.subPosition = -1;
        }
        
        public void set(final Model model, final int id, final int pos, final int subId, final int subPosition) {
            this.model = model;
            this.id = id;
            this.pos = pos;
            this.subId = subId;
            this.subPosition = subPosition;
        }
    }
    
    public static class KeyHandlerState
    {
        public boolean keyHandled;
        public boolean listMoved;
        
        void clear() {
            this.keyHandled = false;
            this.listMoved = false;
        }
    }
    
    public static class Model
    {
        public static final String INITIALS = "INITIAL";
        public static final String SUBTITLE_2_COLOR = "SUBTITLE_2_COLOR";
        public static final String SUBTITLE_COLOR = "SUBTITLE_COLOR";
        public int currentIconSelection;
        boolean dontStartFluctuator;
        public HashMap<String, String> extras;
        public FontInfo fontInfo;
        public FontSize fontSize;
        boolean fontSizeCheckDone;
        public int icon;
        public int iconDeselectedColor;
        public int[] iconDeselectedColors;
        public int iconFluctuatorColor;
        public int[] iconFluctuatorColors;
        public int[] iconIds;
        public int[] iconList;
        public int iconSelectedColor;
        public int[] iconSelectedColors;
        public IconColorImageView.IconShape iconShape;
        public int iconSize;
        public int iconSmall;
        public int id;
        public boolean isEnabled;
        public boolean isOn;
        boolean needsRebind;
        public boolean noImageScaleAnimation;
        public boolean noTextAnimation;
        public int scrollItemLayoutId;
        public Object state;
        public String subTitle;
        public String subTitle2;
        public boolean subTitle2Formatted;
        public boolean subTitleFormatted;
        public boolean subTitle_2Lines;
        public boolean supportsToolTip;
        public String title;
        public ModelType type;
        
        public Model() {
            this.icon = 0;
            this.iconSelectedColor = -1;
            this.iconDeselectedColor = -1;
            this.iconSmall = -1;
            this.iconFluctuatorColor = -1;
            this.scrollItemLayoutId = -1;
            this.iconSize = -1;
        }
        
        public Model(final Model model) {
            this.icon = 0;
            this.iconSelectedColor = -1;
            this.iconDeselectedColor = -1;
            this.iconSmall = -1;
            this.iconFluctuatorColor = -1;
            this.scrollItemLayoutId = -1;
            this.iconSize = -1;
            this.type = model.type;
            this.id = model.id;
            this.icon = model.icon;
            this.iconSelectedColor = model.iconSelectedColor;
            this.iconDeselectedColor = model.iconDeselectedColor;
            this.iconSmall = model.iconSmall;
            this.iconFluctuatorColor = model.iconFluctuatorColor;
            this.title = model.title;
            this.subTitle = model.subTitle;
            this.subTitle2 = model.subTitle2;
            this.subTitle_2Lines = model.subTitle_2Lines;
            this.subTitleFormatted = model.subTitleFormatted;
            this.subTitle2Formatted = model.subTitle2Formatted;
            this.scrollItemLayoutId = model.scrollItemLayoutId;
            this.iconList = model.iconList;
            this.iconIds = model.iconIds;
            this.iconSelectedColors = model.iconSelectedColors;
            this.iconDeselectedColors = model.iconDeselectedColors;
            this.iconFluctuatorColors = model.iconFluctuatorColors;
            this.currentIconSelection = model.currentIconSelection;
            this.supportsToolTip = model.supportsToolTip;
            this.extras = model.extras;
            this.state = model.state;
            this.iconShape = model.iconShape;
            this.needsRebind = model.needsRebind;
            this.dontStartFluctuator = model.dontStartFluctuator;
            this.fontSizeCheckDone = model.fontSizeCheckDone;
            this.fontInfo = model.fontInfo;
            this.fontSize = model.fontSize;
            this.noTextAnimation = model.noTextAnimation;
            this.noImageScaleAnimation = model.noImageScaleAnimation;
            this.iconSize = model.iconSize;
        }
        
        public void clear() {
            this.id = 0;
            this.icon = 0;
            this.iconSelectedColor = -1;
            this.iconDeselectedColor = -1;
            this.iconSmall = -1;
            this.iconFluctuatorColor = -1;
            this.title = null;
            this.subTitle = null;
            this.subTitle2 = null;
            this.subTitle_2Lines = false;
            this.subTitleFormatted = false;
            this.subTitle2Formatted = false;
            this.scrollItemLayoutId = -1;
            this.iconList = null;
            this.iconIds = null;
            this.currentIconSelection = 0;
            this.extras = null;
            this.state = null;
            this.iconShape = null;
            this.needsRebind = false;
            this.dontStartFluctuator = false;
            this.fontSizeCheckDone = false;
            this.fontInfo = null;
            this.fontSize = null;
            this.noTextAnimation = false;
            this.noImageScaleAnimation = false;
            this.iconSize = -1;
        }
    }
    
    public static class ModelState
    {
        public boolean updateImage;
        public boolean updateSmallImage;
        public boolean updateSubTitle;
        public boolean updateSubTitle2;
        public boolean updateTitle;
        
        ModelState() {
            this.reset();
        }
        
        void reset() {
            this.updateImage = true;
            this.updateSmallImage = true;
            this.updateTitle = true;
            this.updateSubTitle = true;
            this.updateSubTitle2 = true;
        }
    }

    public enum ModelType {
        BLANK(0),
        TITLE(1),
        TITLE_SUBTITLE(2),
        ICON_BKCOLOR(3),
        TWO_ICONS(4),
        ICON(5),
        LOADING(6),
        ICON_OPTIONS(7),
        SCROLL_CONTENT(8),
        LOADING_CONTENT(9),
        SWITCH(10);

        private int value;
        ModelType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
