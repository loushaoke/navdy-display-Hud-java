package com.navdy.hud.app.ui.component;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;

public class FluctuatorAnimatorView extends View {
    private ValueAnimator alphaAnimator;
    private int animationDelay;
    private int animationDuration;
    private DefaultAnimationListener animationListener;
    private AnimatorSet animatorSet;
    float currentCircle;
    private float endRadius;
    private int fillColor;
    private boolean fillEnabled;
    private Handler handler;
    private LinearInterpolator interpolator;
    private Paint paint;
    private ValueAnimator radiusAnimator;
    private float startRadius;
    public Runnable startRunnable;
    boolean started;
    private int strokeColor;
    private float strokeWidth;

    public FluctuatorAnimatorView(Context context) {
        this(context, null);
    }

    public FluctuatorAnimatorView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FluctuatorAnimatorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.interpolator = new LinearInterpolator();
        this.handler = new Handler(Looper.getMainLooper());
        this.animationListener = new DefaultAnimationListener() {
            public void onAnimationEnd(Animator animation) {
                FluctuatorAnimatorView.this.handler.postDelayed(FluctuatorAnimatorView.this.startRunnable, (long) FluctuatorAnimatorView.this.animationDelay);
            }
        };
        this.startRunnable = new Runnable() {
            public void run() {
                FluctuatorAnimatorView.this.animatorSet.start();
            }
        };
        this.started = false;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FluctuatorAnimatorView, defStyleAttr, 0);
        if (a != null) {
            this.strokeColor = a.getColor(R.styleable.FluctuatorAnimatorView_fluctuator_stroke_color, -1);
            this.fillColor = a.getColor(R.styleable.FluctuatorAnimatorView_fluctuator_fill_color, -1);
            this.startRadius = a.getDimension(R.styleable.FluctuatorAnimatorView_fluctuator_start_radius, 0.0f);
            this.endRadius = a.getDimension(R.styleable.FluctuatorAnimatorView_fluctuator_end_radius, 0.0f);
            this.strokeWidth = a.getDimension(R.styleable.FluctuatorAnimatorView_fluctuator_stroke_width, 0.0f);
            this.animationDuration = a.getInteger(R.styleable.FluctuatorAnimatorView_fluctuator_duration, 0);
            this.animationDelay = a.getInteger(R.styleable.FluctuatorAnimatorView_fluctuator_duration_delay, 0);
            a.recycle();
        }
        this.paint = new Paint();
        this.paint.setStrokeWidth(this.strokeWidth);
        this.paint.setAntiAlias(true);
        this.animatorSet = new AnimatorSet();
        this.animatorSet.setDuration((long) this.animationDuration);
        this.radiusAnimator = ValueAnimator.ofFloat(this.startRadius, this.endRadius);
        this.alphaAnimator = ValueAnimator.ofFloat(1.0f, 0.0f);
        this.animatorSet.setInterpolator(this.interpolator);
        this.radiusAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                FluctuatorAnimatorView.this.currentCircle = (Float) animation.getAnimatedValue();
                FluctuatorAnimatorView.this.invalidate();
            }
        });
        this.alphaAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                FluctuatorAnimatorView.this.setAlpha((Float) animation.getAnimatedValue());
            }
        });
        this.animatorSet.playTogether(this.radiusAnimator, this.alphaAnimator);
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.fillColor != -1 || this.fillEnabled) {
            this.paint.setStyle(Style.FILL);
            this.paint.setColor(this.fillColor);
            canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), this.currentCircle, this.paint);
        }
        this.paint.setStyle(Style.STROKE);
        this.paint.setColor(this.strokeColor);
        canvas.drawCircle((float) (getWidth() / 2), (float) (getHeight() / 2), this.currentCircle, this.paint);
    }

    public void start() {
        stop();
        this.animatorSet.addListener(this.animationListener);
        this.animatorSet.start();
        this.started = true;
    }

    public void stop() {
        this.handler.removeCallbacks(this.startRunnable);
        if (this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
        }
        this.started = false;
    }

    public void setFillColor(int color) {
        this.fillColor = color;
        this.fillEnabled = true;
    }

    public void setStrokeColor(int color) {
        this.strokeColor = color;
    }

    public boolean isStarted() {
        return this.started;
    }
}
