package com.navdy.hud.app.ui.component.image;

import android.animation.AnimatorSet;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator;
import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import com.navdy.hud.app.R;

public class CrossFadeImageView extends FrameLayout
{
    View big;
    Mode mode;
    View small;
    float smallAlpha;
    boolean smallAlphaSet;
    
    public CrossFadeImageView(final Context context) {
        this(context, null);
    }
    
    public CrossFadeImageView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public CrossFadeImageView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public void inject(final Mode mode) {
        if (this.big == null) {
            this.big = this.findViewById(R.id.big);
            this.small = this.findViewById(R.id.small);
            if ((this.mode = mode) == Mode.BIG) {
                this.big.setAlpha(1.0f);
                this.small.setAlpha(0.0f);
            }
            else {
                if (!this.smallAlphaSet) {
                    this.small.setAlpha(1.0f);
                }
                else {
                    this.small.setAlpha(this.smallAlpha);
                }
                this.big.setAlpha(0.0f);
            }
        }
    }
    
    public View getBig() {
        return this.big;
    }
    
    public AnimatorSet getCrossFadeAnimator() {
        final AnimatorSet set = new AnimatorSet();
        float n;
        float n2;
        float n3;
        float smallAlpha;
        if (this.mode == Mode.BIG) {
            n = 1.0f;
            n2 = 0.0f;
            n3 = 0.0f;
            if (!this.smallAlphaSet) {
                smallAlpha = 1.0f;
            }
            else {
                smallAlpha = this.smallAlpha;
            }
        }
        else {
            n = 0.0f;
            n2 = 1.0f;
            n3 = 1.0f;
            smallAlpha = 0.0f;
        }
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.big, View.ALPHA, n, n2);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                CrossFadeImageView.this.big.setAlpha((float)valueAnimator.getAnimatedValue());
            }
        });
        final AnimatorSet.Builder play = set.play(ofFloat);
        final ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.small, View.ALPHA, n3, smallAlpha);
        ofFloat2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                CrossFadeImageView.this.small.setAlpha((float)valueAnimator.getAnimatedValue());
            }
        });
        play.with(ofFloat2);
        set.addListener(new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                if (CrossFadeImageView.this.mode == Mode.BIG) {
                    CrossFadeImageView.this.mode = Mode.SMALL;
                }
                else {
                    CrossFadeImageView.this.mode = Mode.BIG;
                }
            }
        });
        return set;
    }
    
    public Mode getMode() {
        return this.mode;
    }
    
    public View getSmall() {
        return this.small;
    }
    
    public void setMode(final Mode mode) {
        if (this.mode != mode) {
            if (mode == Mode.BIG) {
                this.big.setAlpha(1.0f);
                this.small.setAlpha(0.0f);
            }
            else {
                if (!this.smallAlphaSet) {
                    this.small.setAlpha(1.0f);
                }
                else {
                    this.small.setAlpha(this.smallAlpha);
                }
                this.big.setAlpha(0.0f);
            }
            this.mode = mode;
        }
    }
    
    public void setSmallAlpha(final float smallAlpha) {
        if (smallAlpha == -1.0f) {
            this.smallAlphaSet = false;
            this.smallAlpha = 0.0f;
        }
        else {
            this.smallAlphaSet = true;
            this.smallAlpha = smallAlpha;
        }
    }

    public enum Mode {
        BIG(0),
        SMALL(1);

        private int value;
        Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
