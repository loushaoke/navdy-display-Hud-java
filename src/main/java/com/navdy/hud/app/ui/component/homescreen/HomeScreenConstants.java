package com.navdy.hud.app.ui.component.homescreen;

import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import android.graphics.PointF;
import com.here.android.mpa.common.ViewRect;
import com.navdy.hud.app.R;

public class HomeScreenConstants
{
    public static final String EMPTY = "";
    public static final int INITIAL_ROUTE_TIMEOUT = 5000;
    public static final int MINUTES_DAY = 1440;
    public static final int MINUTES_HOUR = 60;
    public static final int MIN_RENAVIGATION_DISTANCE_METERS = 500;
    public static final double MIN_RENAVIGATION_DISTANCE_METERS_GAS = 500.0;
    public static final int SELECTION_ROUTE_TIMEOUT = 10000;
    public static final String SPACE = " ";
    public static final String SPEED_KM;
    public static final String SPEED_METERS;
    public static final String SPEED_MPH;
    public static final int TOP_ANIMATION_INITIAL_OUT_INTERVAL = 30000;
    public static final int TOP_ANIMATION_OUT_INTERVAL = 10000;
    public static final ViewRect routeOverviewRect;
    public static final ViewRect routePickerViewRect;
    public static final PointF transformCenterLargeBottom;
    public static final PointF transformCenterLargeMiddle;
    public static final PointF transformCenterPicker;
    public static final PointF transformCenterSmallBottom;
    public static final PointF transformCenterSmallMiddle;
    
    static {
        routePickerViewRect = new ViewRect(HomeScreenResourceValues.routePickerBoxX, HomeScreenResourceValues.routePickerBoxY, HomeScreenResourceValues.routePickerBoxWidth, HomeScreenResourceValues.routePickerBoxHeight);
        routeOverviewRect = new ViewRect(HomeScreenResourceValues.routeOverviewBoxX, HomeScreenResourceValues.routeOverviewBoxY, HomeScreenResourceValues.routeOverviewBoxWidth, HomeScreenResourceValues.routeOverviewBoxHeight);
        Resources resources = HudApplication.getAppContext().getResources();
        SPEED_MPH = resources.getString(R.string.miles_per_hour);
        SPEED_KM = resources.getString(R.string.kilometers_per_hour);
        SPEED_METERS = resources.getString(R.string.meters_per_second);
        transformCenterSmallMiddle = new PointF((float)HomeScreenResourceValues.transformCenterOverviewX, (float)HomeScreenResourceValues.transformCenterMapOnRouteOverviewY);
        transformCenterSmallBottom = new PointF((float)(HomeScreenResourceValues.transformCenterX + HomeScreenResourceValues.transformCenterIconWidth / 2), (float)HomeScreenResourceValues.transformCenterMapOnRouteY);
        transformCenterLargeMiddle = new PointF((float)HomeScreenResourceValues.transformCenterOverviewX, (float)HomeScreenResourceValues.transformCenterOverviewY);
        transformCenterLargeBottom = new PointF((float)(HomeScreenResourceValues.transformCenterX + HomeScreenResourceValues.transformCenterIconWidth / 2), (float)HomeScreenResourceValues.transformCenterOverviewRouteY);
        transformCenterPicker = new PointF((float)HomeScreenResourceValues.transformCenterOverviewX, (float)HomeScreenResourceValues.transformCenterPickerY);
    }
}
