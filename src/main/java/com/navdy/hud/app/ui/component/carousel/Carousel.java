package com.navdy.hud.app.ui.component.carousel;

import android.util.Log;
import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class Carousel {

    public static class InitParams {
        public AnimationStrategy animator;
        public CarouselIndicator carouselIndicator;
        public boolean exitOnDoubleClick;
        public boolean fastScrollAnimation;
        public int imageLytResourceId;
        public int infoLayoutResourceId;
        public List<Model> model;
        public View rootContainer;
        public ViewProcessor viewProcessor;
    }

    public interface Listener {
        void onCurrentItemChanged(int i, int i2);

        void onCurrentItemChanging(int i, int i2, int i3);

        void onExecuteItem(int i, int i2);

        void onExit();
    }

    public static class Model {
        public Object extras;
        public int id;
        public HashMap<Integer, String> infoMap;
        public int largeImageRes;
        public int smallImageColor;
        public int smallImageRes;
    }

    public static class ViewCacheManager {
        private int maxCacheCount;
        private ArrayList<View> middleLeftViewCache = new ArrayList();
        private ArrayList<View> middleRightViewCache = new ArrayList();
        private ArrayList<View> sideViewCache = new ArrayList();

        public ViewCacheManager(int maxCacheCount) {
            this.maxCacheCount = maxCacheCount;
        }

        public View getView(ViewType viewType) {
            View cachedView = null;
            switch (viewType) {
                case SIDE:
                    if (this.sideViewCache.size() > 0) {
                        cachedView = (View) this.sideViewCache.remove(0);
                        break;
                    }
                    break;
                case MIDDLE_LEFT:
                    if (this.middleLeftViewCache.size() > 0) {
                        cachedView = (View) this.middleLeftViewCache.remove(0);
                        break;
                    }
                    break;
                case MIDDLE_RIGHT:
                    if (this.middleRightViewCache.size() > 0) {
                        cachedView = (View) this.middleRightViewCache.remove(0);
                        break;
                    }
                    break;
            }
            if (!(cachedView == null || cachedView.getParent() == null)) {
                Log.e("CAROUSEL", "uhoh");
            }
            return cachedView;
        }

        public void putView(ViewType viewType, View view) {
            if (view == null || view.getParent() != null) {
                Log.e("CAROUSEL", "uhoh");
            }
            switch (viewType) {
                case SIDE:
                    if (this.sideViewCache.size() < this.maxCacheCount) {
                        this.sideViewCache.add(view);
                        return;
                    }
                    return;
                case MIDDLE_LEFT:
                    if (this.middleLeftViewCache.size() < this.maxCacheCount) {
                        this.middleLeftViewCache.add(view);
                        return;
                    }
                    return;
                case MIDDLE_RIGHT:
                    if (this.middleRightViewCache.size() < this.maxCacheCount) {
                        this.middleRightViewCache.add(view);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        public void clearCache() {
            this.sideViewCache.clear();
            this.middleLeftViewCache.clear();
            this.middleRightViewCache.clear();
        }
    }

    public interface ViewProcessor {
        void processInfoView(Model model, View view, int i);

        void processLargeImageView(Model model, View view, int i, int i2, int i3);

        void processSmallImageView(Model model, View view, int i, int i2, int i3);
    }

    public enum ViewType {
        SIDE(0),
        MIDDLE_LEFT(1),
        MIDDLE_RIGHT(2);

        private int value;
        ViewType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
