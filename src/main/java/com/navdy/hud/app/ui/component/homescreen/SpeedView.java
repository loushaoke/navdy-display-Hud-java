package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import butterknife.ButterKnife;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.view.View;
import android.animation.AnimatorSet;

import com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent;
import com.navdy.hud.app.maps.MapEvents$SpeedWarning;
import com.navdy.hud.app.obd.ObdManager;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.maps.MapEvents;
import android.util.AttributeSet;
import android.content.Context;
import butterknife.InjectView;
import android.widget.TextView;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.SpeedManager;
import com.squareup.otto.Bus;
import android.widget.LinearLayout;

public class SpeedView extends LinearLayout
{
    private Bus bus;
    private int lastSpeed;
    private SpeedManager.SpeedUnit lastSpeedUnit;
    private Logger logger;
    private SpeedManager speedManager;
    @InjectView(R.id.speedUnitView)
    TextView speedUnitView;
    @InjectView(R.id.speedView)
    TextView speedView;
    
    public SpeedView(final Context context) {
        this(context, null);
    }
    
    public SpeedView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public SpeedView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.lastSpeed = -1;
    }
    
    private void setTrackingSpeed() {
        int currentSpeed;
        if ((currentSpeed = this.speedManager.getCurrentSpeed()) < 0) {
            currentSpeed = 0;
        }
        final SpeedManager.SpeedUnit speedUnit = this.speedManager.getSpeedUnit();
        if (currentSpeed != this.lastSpeed) {
            this.lastSpeed = currentSpeed;
            this.speedView.setText(String.valueOf(currentSpeed));
        }
        if (speedUnit != this.lastSpeedUnit) {
            this.lastSpeedUnit = speedUnit;
            String text = "";
            switch (speedUnit) {
                case MILES_PER_HOUR:
                    text = HomeScreenConstants.SPEED_MPH;
                    break;
                case KILOMETERS_PER_HOUR:
                    text = HomeScreenConstants.SPEED_KM;
                    break;
                case METERS_PER_SECOND:
                    text = HomeScreenConstants.SPEED_METERS;
                    break;
            }
            this.speedUnitView.setText(text);
        }
    }
    
    @Subscribe
    public void GPSSpeedChangeEvent(final MapEvents$GPSSpeedEvent gpsSpeedEvent) {
        this.setTrackingSpeed();
    }
    
    @Subscribe
    public void ObdPidChangeEvent(final ObdManager.ObdPidChangeEvent obdPidChangeEvent) {
        switch (obdPidChangeEvent.pid.getId()) {
            case 13:
                this.setTrackingSpeed();
                break;
        }
    }
    
    @Subscribe
    public void ObdStateChangeEvent(final ObdManager.ObdConnectionStatusEvent obdConnectionStatusEvent) {
        this.lastSpeed = -1;
        this.lastSpeedUnit = null;
        this.setTrackingSpeed();
    }
    
    public void clearState() {
        this.lastSpeed = -1;
        this.lastSpeedUnit = null;
        this.setTrackingSpeed();
    }
    
    public void getTopAnimator(final AnimatorSet.Builder animatorSet$Builder, final boolean b) {
        if (b) {
            animatorSet$Builder.with(HomeScreenUtils.getTranslationXPositionAnimator(this.speedUnitView, HomeScreenResourceValues.topViewSpeedOut));
            animatorSet$Builder.with(ObjectAnimator.ofFloat(this.speedUnitView, View.ALPHA, 1.0f, 0.0f));
        }
        else {
            animatorSet$Builder.with(HomeScreenUtils.getTranslationXPositionAnimator(this.speedUnitView, 0));
            animatorSet$Builder.with(ObjectAnimator.ofFloat(this.speedUnitView, View.ALPHA, 0.0f, 1.0f));
            this.speedView.setVisibility(View.VISIBLE);
        }
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject(this);
        if (!this.isInEditMode()) {
            this.speedManager = SpeedManager.getInstance();
            (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
        }
    }
    
    @Subscribe
    public void onSpeedDataExpired(final SpeedManager.SpeedDataExpired speedDataExpired) {
        this.setTrackingSpeed();
    }
    
    @Subscribe
    public void onSpeedUnitChanged(final SpeedManager.SpeedUnitChanged speedUnitChanged) {
        this.setTrackingSpeed();
    }
    
    @Subscribe
    public void onSpeedWarningEvent(final MapEvents$SpeedWarning speedWarning) {
        if (speedWarning.exceed) {
            this.speedView.setTextColor(-16711681);
        }
        else {
            this.speedView.setTextColor(-1);
        }
    }
    
    public void resetTopViewsAnimator() {
        this.speedUnitView.setAlpha(1.0f);
        this.speedUnitView.setTranslationX(0.0f);
        this.speedView.setVisibility(View.VISIBLE);
    }
}
