package com.navdy.hud.app.ui.component.homescreen;

import android.widget.ProgressBar;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator;
import android.view.ViewGroup;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import com.navdy.hud.app.HudApplication;

public class HomeScreenUtils
{
    private static final float DENSITY;
    
    static {
        DENSITY = HudApplication.getAppContext().getResources().getDisplayMetrics().density;
    }
    
    public static ObjectAnimator getAlphaAnimator(final View view, final int n) {
        return ObjectAnimator.ofFloat(view, "alpha", new float[] { n });
    }
    
    public static AnimatorSet getFadeInAndScaleUpAnimator(final View view) {
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", new float[] { 0.0f, 1.0f });
        final ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(view, "scaleX", new float[] { 1.0f });
        final ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(view, "scaleY", new float[] { 1.0f });
        final AnimatorSet set = new AnimatorSet();
        set.play((Animator)ofFloat).with((Animator)ofFloat2).with((Animator)ofFloat3);
        return set;
    }
    
    public static AnimatorSet getFadeOutAndScaleDownAnimator(final View view) {
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", new float[] { 0.0f });
        final ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(view, "scaleX", new float[] { 0.9f });
        final ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(view, "scaleY", new float[] { 0.9f });
        final AnimatorSet set = new AnimatorSet();
        set.play((Animator)ofFloat).with((Animator)ofFloat2).with((Animator)ofFloat3);
        return set;
    }
    
    public static AnimatorSet getMarginAnimator(final View view, final int n, final int n2) {
        final ViewGroup.MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        final ValueAnimator ofInt = ValueAnimator.ofInt(new int[] { viewGroup$MarginLayoutParams.leftMargin, n });
        ofInt.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                ((ViewGroup.MarginLayoutParams)view.getLayoutParams()).leftMargin = (int)valueAnimator.getAnimatedValue();
                view.invalidate();
                view.requestLayout();
            }
        });
        final ValueAnimator ofInt2 = ValueAnimator.ofInt(new int[] { viewGroup$MarginLayoutParams.rightMargin, n2 });
        ofInt2.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                ((ViewGroup.MarginLayoutParams)view.getLayoutParams()).rightMargin = (int)valueAnimator.getAnimatedValue();
                view.invalidate();
                view.requestLayout();
            }
        });
        final AnimatorSet set = new AnimatorSet();
        set.playTogether(new Animator[] { ofInt, ofInt2 });
        return set;
    }
    
    public static ValueAnimator getProgressBarAnimator(final ProgressBar progressBar, final int n) {
        final ValueAnimator ofInt = ValueAnimator.ofInt(new int[] { progressBar.getProgress(), n });
        ofInt.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                progressBar.setProgress((int)valueAnimator.getAnimatedValue());
            }
        });
        return ofInt;
    }
    
    public static ObjectAnimator getTranslationXPositionAnimator(final View view, final int n) {
        return ObjectAnimator.ofFloat(view, View.TRANSLATION_X, new float[] { n });
    }
    
    public static ObjectAnimator getTranslationYPositionAnimator(final View view, final int n) {
        return ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, new float[] { n });
    }
    
    public static ValueAnimator getWidthAnimator(final View view, final int n) {
        final ValueAnimator ofInt = ValueAnimator.ofInt(new int[] { ((ViewGroup.MarginLayoutParams)view.getLayoutParams()).width, n });
        ofInt.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                ((ViewGroup.MarginLayoutParams)view.getLayoutParams()).width = (int)valueAnimator.getAnimatedValue();
                view.invalidate();
                view.requestLayout();
            }
        });
        return ofInt;
    }
    
    public static ValueAnimator getWidthAnimator(final View view, final int n, final int n2) {
        final ValueAnimator ofInt = ValueAnimator.ofInt(new int[] { n, n2 });
        ofInt.addUpdateListener((ValueAnimator.AnimatorUpdateListener)new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                HomeScreenUtils.setWidth(view, (int)valueAnimator.getAnimatedValue());
            }
        });
        return ofInt;
    }
    
    public static ObjectAnimator getXPositionAnimator(final View view, final float n) {
        return ObjectAnimator.ofFloat(view, "x", new float[] { n });
    }
    
    public static ObjectAnimator getYPositionAnimator(final View view, final float n) {
        return ObjectAnimator.ofFloat(view, "y", new float[] { n });
    }
    
    public static void setWidth(final View view, final int width) {
        ((ViewGroup.MarginLayoutParams)view.getLayoutParams()).width = width;
        view.invalidate();
        view.requestLayout();
    }
}
