package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.R;
import com.navdy.hud.app.view.DashboardWidgetView;
import android.content.Context;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.view.DashboardWidgetPresenter;

public class ImageWidgetPresenter extends DashboardWidgetPresenter
{
    private String id;
    private Drawable mDrawable;
    
    public ImageWidgetPresenter(final Context context, final int n, final String id) {
        if (n > 0) {
            this.mDrawable = context.getResources().getDrawable(n);
        }
        this.id = id;
    }
    
    @Override
    public Drawable getDrawable() {
        return this.mDrawable;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return this.id;
    }
    
    @Override
    public String getWidgetName() {
        return null;
    }
    
    @Override
    public void setView(final DashboardWidgetView view) {
        if (view != null) {
            view.setContentView(R.layout.smart_dash_widget_layout);
        }
        super.setView(view);
    }
    
    @Override
    protected void updateGauge() {
    }
}
