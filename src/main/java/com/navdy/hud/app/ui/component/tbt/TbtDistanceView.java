package com.navdy.hud.app.ui.component.tbt;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.support.constraint.ConstraintLayout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents$ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import kotlin.Metadata;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import net.hockeyapp.android.LoginActivity;
import org.jetbrains.annotations.NotNull;

import static com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState.*;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 ,2\u00020\u0001:\u0001,B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u001a\u001a\u00020\u001bH\u0002J\u0006\u0010\u001c\u001a\u00020\u001bJ\u001a\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\f2\n\u0010\u001f\u001a\u00060 R\u00020!J\b\u0010\"\u001a\u00020\u001bH\u0014J$\u0010#\u001a\u00020\u001b2\b\u0010$\u001a\u0004\u0018\u00010\u00132\b\u0010%\u001a\u0004\u0018\u00010\u00112\u0006\u0010&\u001a\u00020'H\u0002J\u000e\u0010(\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\fJ\u000e\u0010)\u001a\u00020\u001b2\u0006\u0010*\u001a\u00020+R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;", "Landroid/support/constraint/ConstraintLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "distanceView", "Landroid/widget/TextView;", "initialProgressBarDistance", "lastDistance", "", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "nowView", "Landroid/widget/ImageView;", "progressAnimator", "Landroid/animation/ValueAnimator;", "progressView", "Landroid/widget/ProgressBar;", "cancelProgressAnimator", "", "clear", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet$Builder;", "Landroid/animation/AnimatorSet;", "onFinishInflate", "setDistance", "maneuverState", "text", "distanceInMeters", "", "setMode", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "Companion", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
public final class TbtDistanceView extends ConstraintLayout {
    private static final Resources resources;
    public static final Companion Companion;
    private static final Logger logger = new Logger("TbtDistanceView");
    private static final int fullWidth;
    private static final int mediumWidth;
    private static final int nowHeightFull;
    private static final int nowHeightMedium;
    private static final int nowWidthFull;
    private static final int nowWidthMedium;
    private static final int progressHeightFull;
    private static final int progressHeightMedium;
    private static final float size18;
    private static final float size22;
    private HashMap _$_findViewCache;
    private CustomAnimationMode currentMode;
    private TextView distanceView;
    private int initialProgressBarDistance;
    private String lastDistance;
    private HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private ImageView nowView;
    private ValueAnimator progressAnimator;
    private ProgressBar progressView;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006R\u0014\u0010\u000f\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0006R\u0014\u0010\u0011\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0006R\u0014\u0010\u0013\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0006R\u0014\u0010\u0015\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0006R\u0014\u0010\u0017\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0006R\u0014\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0014\u0010!\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010 \u00a8\u0006#"}, d2 = {"Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView$Companion;", "", "()V", "fullWidth", "", "getFullWidth", "()I", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "mediumWidth", "getMediumWidth", "nowHeightFull", "getNowHeightFull", "nowHeightMedium", "getNowHeightMedium", "nowWidthFull", "getNowWidthFull", "nowWidthMedium", "getNowWidthMedium", "progressHeightFull", "getProgressHeightFull", "progressHeightMedium", "getProgressHeightMedium", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "size18", "", "getSize18", "()F", "size22", "getSize22", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: TbtDistanceView.kt */
    public static final class Companion {
        private Companion() {
        }

        private final Logger getLogger() {
            return TbtDistanceView.logger;
        }

        private final Resources getResources() {
            return TbtDistanceView.resources;
        }

        private final int getFullWidth() {
            return TbtDistanceView.fullWidth;
        }

        private final int getMediumWidth() {
            return TbtDistanceView.mediumWidth;
        }

        private final float getSize22() {
            return TbtDistanceView.size22;
        }

        private final float getSize18() {
            return TbtDistanceView.size18;
        }

        private final int getProgressHeightFull() {
            return TbtDistanceView.progressHeightFull;
        }

        private final int getProgressHeightMedium() {
            return TbtDistanceView.progressHeightMedium;
        }

        private final int getNowWidthFull() {
            return TbtDistanceView.nowWidthFull;
        }

        private final int getNowHeightFull() {
            return TbtDistanceView.nowHeightFull;
        }

        private final int getNowWidthMedium() {
            return TbtDistanceView.nowWidthMedium;
        }

        private final int getNowHeightMedium() {
            return TbtDistanceView.nowHeightMedium;
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View view = (View) this._$_findViewCache.get(i);
        if (view != null) {
            return view;
        }
        view = findViewById(i);
        this._$_findViewCache.put(i, view);
        return view;
    }

    static {
        Resources rsrc = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(rsrc, "HudApplication.getAppContext().resources");
        resources = rsrc;

        Companion = new Companion();
        fullWidth = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_full_w);
        mediumWidth = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_medium_w);
        nowHeightFull = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_h_full);
        nowHeightMedium = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_h_medium);
        nowWidthFull = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_w_full);
        nowWidthMedium = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_now_w_medium);
        progressHeightFull = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_progress_h_full);
        progressHeightMedium = Companion.getResources().getDimensionPixelSize(R.dimen.tbt_distance_progress_h_medium);
        size18 = Companion.getResources().getDimension(R.dimen.tbt_instruction_18);
        size22 = Companion.getResources().getDimension(R.dimen.tbt_instruction_22);
    }

    public TbtDistanceView(@NotNull Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }

    public TbtDistanceView(@NotNull Context context, @NotNull AttributeSet attrs) {
        super(context, attrs);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
    }

    public TbtDistanceView(@NotNull Context context, @NotNull AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(attrs, "attrs");
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        View viewById = findViewById(R.id.distance);
        if (viewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.distanceView = (TextView) viewById;
        viewById = findViewById(R.id.progress);
        if (viewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.ProgressBar");
        }
        this.progressView = (ProgressBar) viewById;
        viewById = findViewById(R.id.now);
        if (viewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
        }
        this.nowView = (ImageView) viewById;
    }

    public final void setMode(@NotNull CustomAnimationMode mode) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        if (!Intrinsics.areEqual(this.currentMode,  mode)) {
            int lytWidth = 0;
            float fontSize = 0.0f;
            int progressSize = 0;
            int nowW = 0;
            int nowH = 0;
            switch (mode) {
                case EXPAND:
                    lytWidth = Companion.getFullWidth();
                    fontSize = Companion.getSize22();
                    progressSize = Companion.getProgressHeightFull();
                    nowW = Companion.getNowWidthFull();
                    nowH = Companion.getNowHeightFull();
                    break;
                case SHRINK_LEFT:
                    lytWidth = Companion.getMediumWidth();
                    fontSize = Companion.getSize18();
                    progressSize = Companion.getProgressHeightMedium();
                    nowW = Companion.getNowWidthMedium();
                    nowH = Companion.getNowHeightMedium();
                    break;
            }
            MarginLayoutParams lytParams = (MarginLayoutParams)getLayoutParams();
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams.width = lytWidth;
            TextView textView = this.distanceView;
            if (textView != null) {
                textView.setTextSize(fontSize);
            }
            ProgressBar progressBar = this.progressView;
            lytParams = progressBar != null ? (MarginLayoutParams)progressBar.getLayoutParams() : null;
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams.height = progressSize;
            ImageView imageView = this.nowView;
            lytParams = imageView != null ? (MarginLayoutParams)imageView.getLayoutParams() : null;
            if (lytParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            lytParams.width = nowW;
            lytParams.height = nowH;
            invalidate();
            requestLayout();
            this.currentMode = mode;
            Companion.getLogger().v("view width = " + lytParams.width);
        }
    }

    public final void getCustomAnimator(@NotNull CustomAnimationMode mode, @NotNull Builder mainBuilder) {
        Intrinsics.checkParameterIsNotNull(mode, LoginActivity.EXTRA_MODE);
        Intrinsics.checkParameterIsNotNull(mainBuilder, "mainBuilder");
    }

    public final void clear() {
        this.lastDistance = null;
        if (this.distanceView != null) {
            this.distanceView.setText("");
        }
        cancelProgressAnimator();
        if (this.progressView != null) {
            this.progressView.setVisibility(View.GONE);
        }
    }

    public final void updateDisplay(@NotNull MapEvents$ManeuverDisplay event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        setDistance(event.maneuverState, event.distanceToPendingRoadText, event.distanceInMeters);
        this.lastManeuverState = event.maneuverState;
    }

    private void setDistance(HereManeuverDisplayBuilder$ManeuverState maneuverState, String lastDistance, long l) {
        if (TextUtils.equals(lastDistance, this.lastDistance) && (this.lastManeuverState == maneuverState)) {
            return;
        }
        if (maneuverState == null) {
            this.cancelProgressAnimator();
            if (this.progressView != null) {
                this.progressView.setVisibility(GONE);
            }
            if (this.distanceView != null) {
                this.distanceView.setText("");
            }
            if (this.nowView != null) {
                this.nowView.setAlpha(0.0f);
                this.nowView.setScaleX(0.9f);
                this.nowView.setScaleY(0.9f);
                this.nowView.setVisibility(GONE);
            }
            this.setVisibility(GONE);
            return;
        }
        this.lastDistance = lastDistance;
        TextView textView = this.distanceView;
        if (textView != null) {
            textView.setText(lastDistance != null ? lastDistance : "");
        }
        boolean transitionToNow = (this.lastManeuverState != NOW) && (maneuverState == NOW);
        if (transitionToNow) {
            if (this.nowView != null) {
                this.nowView.setAlpha(0.0f);
                this.nowView.setVisibility(VISIBLE);
            }
            AnimatorSet animatorSet = HomeScreenUtils.getFadeInAndScaleUpAnimator(this.nowView);
            this.cancelProgressAnimator();
            if (this.progressView != null) {
                this.progressView.setVisibility(GONE);
            }
            if (this.distanceView != null) {
                this.distanceView.setAlpha(0.0f);
            }
            AnimatorSet animatorSet2 = new AnimatorSet();
            animatorSet2.play(animatorSet);
            animatorSet2.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
        } else {
            if (this.nowView != null) {
                this.nowView.setVisibility(GONE);
                this.nowView.setAlpha(0.0f);
            }
            ObjectAnimator animator = HomeScreenUtils.getAlphaAnimator(this.distanceView, 1);
            AnimatorSet set = new AnimatorSet();
            set.play(animator);
            AnimatorSet set2 = set.setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION());
            if (set2 != null) {
                set2.start();
            }
        }
        if ((this.lastManeuverState == SOON) && (maneuverState != SOON)) {
            if (this.progressView != null) {
                this.progressView.setVisibility(GONE);
            }
            this.cancelProgressAnimator();
            this.initialProgressBarDistance = 0;
        } else if ((this.lastManeuverState != SOON) && (maneuverState == SOON)) {
            this.cancelProgressAnimator();
            this.initialProgressBarDistance = (int)l;
            if (this.progressView != null) {
                this.progressView.setProgress(0);
                this.progressView.setVisibility(VISIBLE);
                this.progressView.requestLayout();
            }
        } else if (maneuverState == SOON) {
            double d2 = (double)l / (double)this.initialProgressBarDistance;
            HomeScreenUtils.getProgressBarAnimator(this.progressView, (int)(100.0 * (1.0 - d2))).setDuration(TbtViewContainer.Companion.getMANEUVER_PROGRESS_ANIMATION_DURATION()).start();
        }
        this.setVisibility(VISIBLE);
    }


    private void cancelProgressAnimator() {
        if (this.progressAnimator != null && this.progressAnimator.isRunning()) {
            this.progressAnimator.removeAllListeners();
            this.progressAnimator.cancel();
            this.progressAnimator = null;
        }
    }
}
