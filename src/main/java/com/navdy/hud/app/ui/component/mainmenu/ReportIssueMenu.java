package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.util.ReportIssueService.IssueType;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0000\u0018\u0000 =2\u00020\u0001:\u0002=>B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0007B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\"\u0010\u0011\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0016J\b\u0010\u0015\u001a\u00020\fH\u0016J\u0010\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fH\u0016J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0018\u001a\u00020\fH\u0016J\n\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u001eH\u0016J\b\u0010\u001f\u001a\u00020\u001eH\u0016J\u0018\u0010 \u001a\u00020\u001e2\u0006\u0010!\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\fH\u0016J(\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00102\u0006\u0010%\u001a\u00020&2\u0006\u0010\u0018\u001a\u00020\f2\u0006\u0010'\u001a\u00020(H\u0016J\b\u0010)\u001a\u00020#H\u0016J\b\u0010*\u001a\u00020#H\u0016J\u0010\u0010+\u001a\u00020#2\u0006\u0010,\u001a\u00020-H\u0016J\b\u0010.\u001a\u00020#H\u0016J\u0010\u0010/\u001a\u00020#2\u0006\u00100\u001a\u000201H\u0016J\u0010\u00102\u001a\u00020#2\u0006\u00103\u001a\u000204H\u0002J\u0010\u00105\u001a\u00020\u001e2\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u00106\u001a\u00020#2\u0006\u0010!\u001a\u00020\fH\u0016J\u0010\u00107\u001a\u00020#2\u0006\u00108\u001a\u00020\fH\u0016J\b\u00109\u001a\u00020#H\u0016J\b\u0010:\u001a\u00020#H\u0002J\b\u0010;\u001a\u00020#H\u0016J\u0010\u0010<\u001a\u00020#2\u0006\u0010!\u001a\u00020\fH\u0002R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006?"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V", "type", "Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$ReportIssueMenuType;)V", "backSelection", "", "backSelectionId", "currentItems", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getChildMenu", "args", "", "path", "getInitialSelection", "getItems", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onFastScrollEnd", "onFastScrollStart", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "reportIssue", "issueType", "Lcom/navdy/hud/app/util/ReportIssueService$IssueType;", "selectItem", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showSentToast", "showToolTip", "takeSnapshot", "Companion", "ReportIssueMenuType", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: ReportIssueMenu.kt */
public final class ReportIssueMenu implements IMenu {
    public static final Companion Companion = new Companion();
    private static final String NAV_ISSUE_SENT_TOAST_ID = "nav-issue-sent";
    private static final int TOAST_TIMEOUT = 1000;
    private static final Model back;
    private static final Model driveScore;
    private static final Model etaInaccurate;
    @NotNull
    private static final HashMap<Integer, String> idToTitleMap = MapsKt.hashMapOf(TuplesKt.to(R.id.take_snapshot_maps, "Maps"), TuplesKt.to(R.id.take_snapshot_navigation, "Navigation"), TuplesKt.to(R.id.take_snapshot_smart_dash, "Smart_Dash"), TuplesKt.to(R.id.take_snapshot_drive_score, "Drive_Score"));
    private static final Logger logger = new Logger(ReportIssueMenu.class);

    private static final Model sendReport;

    private static final Model maps;
    private static final Model navigation;
    private static final Model notFastestRoute;
    private static final Model permanentClosure;
    private static final String reportIssue;
    private static final int reportIssueColor;
    private static final ArrayList<Model> reportIssueItems = new ArrayList<>();
    private static final Model roadBlocked;
    private static final Model smartDash;
    private static final ArrayList<Model> takeSnapShotItems = new ArrayList<>();
    private static final String takeSnapshot;
    private static final int takeSnapshotColor;
    private static final String toastSentSuccessfully;
    private static final Model wrongDirection;
    private static final Model wrongRoadName;
    private int backSelection;
    private int backSelectionId;
    private List<Model> currentItems;
    private final IMenu parent;
    protected final Presenter presenter;
    private final ReportIssueMenuType type;
    private final VerticalMenuComponent vscrollComponent;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0013\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082D\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0014\u0010\u0011\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR-\u0010\u0013\u001a\u001e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00040\u0014j\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0004`\u0015\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u000eR\u0014\u0010\u001e\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u000eR\u0014\u0010 \u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u000eR\u0014\u0010\"\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u000eR\u0014\u0010$\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0006R\u0014\u0010&\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b'\u0010\nR\u001a\u0010(\u001a\b\u0012\u0004\u0012\u00020\f0)X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010+R\u0014\u0010,\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010\u000eR\u0014\u0010.\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b/\u0010\u000eR\u001a\u00100\u001a\b\u0012\u0004\u0012\u00020\f0)X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010+R\u0014\u00102\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u0010\u0006R\u0014\u00104\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u0010\nR\u0014\u00106\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b7\u0010\u0006R\u0014\u00108\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b9\u0010\u000eR\u0014\u0010:\u001a\u00020\fX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b;\u0010\u000e\u00a8\u0006<"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/ReportIssueMenu$Companion;", "", "()V", "NAV_ISSUE_SENT_TOAST_ID", "", "getNAV_ISSUE_SENT_TOAST_ID", "()Ljava/lang/String;", "TOAST_TIMEOUT", "", "getTOAST_TIMEOUT", "()I", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "driveScore", "getDriveScore", "etaInaccurate", "getEtaInaccurate", "idToTitleMap", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "getIdToTitleMap", "()Ljava/util/HashMap;", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "maps", "getMaps", "navigation", "getNavigation", "notFastestRoute", "getNotFastestRoute", "permanentClosure", "getPermanentClosure", "reportIssue", "getReportIssue", "reportIssueColor", "getReportIssueColor", "reportIssueItems", "Ljava/util/ArrayList;", "getReportIssueItems", "()Ljava/util/ArrayList;", "roadBlocked", "getRoadBlocked", "smartDash", "getSmartDash", "takeSnapShotItems", "getTakeSnapShotItems", "takeSnapshot", "getTakeSnapshot", "takeSnapshotColor", "getTakeSnapshotColor", "toastSentSuccessfully", "getToastSentSuccessfully", "wrongDirection", "getWrongDirection", "wrongRoadName", "getWrongRoadName", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: ReportIssueMenu.kt */
    public static final class Companion {
        private Companion() {
        }

//        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
//            this();
//        }

        private  Logger getLogger() {
            return ReportIssueMenu.logger;
        }

        @NotNull
        public final HashMap<Integer, String> getIdToTitleMap() {
            return ReportIssueMenu.idToTitleMap;
        }

        private int getTOAST_TIMEOUT() {
            return ReportIssueMenu.TOAST_TIMEOUT;
        }

        private  String getNAV_ISSUE_SENT_TOAST_ID() {
            return ReportIssueMenu.NAV_ISSUE_SENT_TOAST_ID;
        }

        private  Model getBack() {
            return ReportIssueMenu.back;
        }

        private  Model getEtaInaccurate() {
            return ReportIssueMenu.etaInaccurate;
        }

        private  Model getNotFastestRoute() {
            return ReportIssueMenu.notFastestRoute;
        }

        private Model getRoadBlocked() {
            return ReportIssueMenu.roadBlocked;
        }

        private  Model getWrongDirection() {
            return ReportIssueMenu.wrongDirection;
        }

        private  Model getWrongRoadName() {
            return ReportIssueMenu.wrongRoadName;
        }

        private  Model getPermanentClosure() {
            return ReportIssueMenu.permanentClosure;
        }

        private  Model getSendReport() {
            return ReportIssueMenu.sendReport;
        }

        private  Model getMaps() {
            return ReportIssueMenu.maps;
        }

        private  Model getNavigation() {
            return ReportIssueMenu.navigation;
        }

        private  Model getDriveScore() {
            return ReportIssueMenu.driveScore;
        }

        private  Model getSmartDash() {
            return ReportIssueMenu.smartDash;
        }

        private  String getToastSentSuccessfully() {
            return ReportIssueMenu.toastSentSuccessfully;
        }

        private String getReportIssue() {
            return ReportIssueMenu.reportIssue;
        }

        private  int getReportIssueColor() {
            return ReportIssueMenu.reportIssueColor;
        }

        private  String getTakeSnapshot() {
            return ReportIssueMenu.takeSnapshot;
        }

        private  int getTakeSnapshotColor() {
            return ReportIssueMenu.takeSnapshotColor;
        }

        private  ArrayList<Model> getReportIssueItems() {
            return ReportIssueMenu.reportIssueItems;
        }

        private  ArrayList<Model> getTakeSnapShotItems() {
            return ReportIssueMenu.takeSnapShotItems;
        }
    }

    public ReportIssueMenu(@NotNull VerticalMenuComponent vscrollComponent, @NotNull Presenter presenter, @Nullable IMenu parent, @NotNull ReportIssueMenuType type) {
        Intrinsics.checkParameterIsNotNull(vscrollComponent, "vscrollComponent");
        Intrinsics.checkParameterIsNotNull(presenter, "presenter");
        Intrinsics.checkParameterIsNotNull(type, "type");
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.type = type;
    }

//    public /* synthetic */ ReportIssueMenu(VerticalMenuComponent verticalMenuComponent, Presenter presenter, IMenu iMenu, ReportIssueMenuType reportIssueMenuType, int i, DefaultConstructorMarker defaultConstructorMarker) {
//        if ((i & 8) != 0) {
//            reportIssueMenuType = ReportIssueMenuType.NAVIGATION_ISSUES;
//        }
//        this(verticalMenuComponent, presenter, iMenu, reportIssueMenuType);
//    }

    public ReportIssueMenu(@NotNull VerticalMenuComponent vscrollComponent, @NotNull Presenter presenter, @NotNull IMenu parent) {
        this(vscrollComponent, presenter, parent, ReportIssueMenuType.NAVIGATION_ISSUES);
        Intrinsics.checkParameterIsNotNull(vscrollComponent, "vscrollComponent");
        Intrinsics.checkParameterIsNotNull(presenter, "presenter");
        Intrinsics.checkParameterIsNotNull(parent, "parent");
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        String string = resources.getString(R.string.issue_successfully_sent);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026.issue_successfully_sent)");
        toastSentSuccessfully = string;
        string = resources.getString(R.string.report_navigation_issue);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.st\u2026.report_navigation_issue)");
        reportIssue = string;
        string = resources.getString(R.string.take_snapshot);
        Intrinsics.checkExpressionValueIsNotNull(string, "resources.getString(R.string.take_snapshot)");
        takeSnapshot = string;
        reportIssueColor = resources.getColor(R.color.mm_options_report_issue);
        takeSnapshotColor = resources.getColor(R.color.options_dash_purple);
        int bkColorUnselected = resources.getColor(R.color.icon_bk_color_unselected);
        Model buildModel = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, Companion.getReportIssueColor(), MainMenu.bkColorUnselected, Companion.getReportIssueColor(), resources.getString(R.string.back), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026back), null\n            )");
        back = buildModel;
        buildModel = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_eta_inaccurate, R.drawable.icon_eta, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(IssueType.INEFFICIENT_ROUTE_ETA_TRAFFIC.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        etaInaccurate = buildModel;
        buildModel = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_not_fastest_route, R.drawable.icon_bad_route, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(IssueType.INEFFICIENT_ROUTE_SELECTED.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        notFastestRoute = buildModel;
        buildModel = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_road_blocked, R.drawable.icon_road_closed_temp, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(IssueType.ROAD_CLOSED.getTitleStringResource()), resources.getString(IssueType.ROAD_CLOSED.getMessageStringResource()));
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026ngResource)\n            )");
        roadBlocked = buildModel;
        buildModel = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_wrong_direction, R.drawable.icon_bad_map_info, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(IssueType.WRONG_DIRECTION.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        wrongDirection = buildModel;
        buildModel = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_wrong_road_name, R.drawable.icon_wrong_road_name, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(IssueType.ROAD_NAME.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        wrongRoadName = buildModel;
        buildModel = IconBkColorViewHolder.buildModel(R.id.report_issue_menu_permanent_closure, R.drawable.icon_road_closed_perm, Companion.getReportIssueColor(), bkColorUnselected, Companion.getReportIssueColor(), resources.getString(IssueType.ROAD_CLOSED_PERMANENT.getTitleStringResource()), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026urce), null\n            )");
        permanentClosure = buildModel;
        Companion.getReportIssueItems().add(Companion.getBack());
        Companion.getReportIssueItems().add(Companion.getEtaInaccurate());
        Companion.getReportIssueItems().add(Companion.getNotFastestRoute());
        Companion.getReportIssueItems().add(Companion.getRoadBlocked());
        Companion.getReportIssueItems().add(Companion.getWrongDirection());
        Companion.getReportIssueItems().add(Companion.getWrongRoadName());
        Companion.getReportIssueItems().add(Companion.getPermanentClosure());

        buildModel = IconBkColorViewHolder.buildModel(R.id.take_snapshot_send, R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), bkColorUnselected, Companion.getTakeSnapshotColor(), resources.getString(R.string.hockeyapp_crash_dialog_positive_button), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026ing.snapshot_maps), null)");
        sendReport = buildModel;

        buildModel = IconBkColorViewHolder.buildModel(R.id.take_snapshot_maps, R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), bkColorUnselected, Companion.getTakeSnapshotColor(), resources.getString(R.string.snapshot_maps), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026ing.snapshot_maps), null)");
        maps = buildModel;

        buildModel = IconBkColorViewHolder.buildModel(R.id.take_snapshot_navigation, R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), bkColorUnselected, Companion.getTakeSnapshotColor(), resources.getString(R.string.snapshot_navigation), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026apshot_navigation), null)");
        navigation = buildModel;
        buildModel = IconBkColorViewHolder.buildModel(R.id.take_snapshot_smart_dash, R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), bkColorUnselected, Companion.getTakeSnapshotColor(), resources.getString(R.string.snapshot_smart_dash), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026apshot_smart_dash), null)");
        driveScore = buildModel;
        buildModel = IconBkColorViewHolder.buildModel(R.id.take_snapshot_drive_score, R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), bkColorUnselected, Companion.getTakeSnapshotColor(), resources.getString(R.string.snapshot_drive_score), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026pshot_drive_score), null)");
        smartDash = buildModel;

        Companion.getTakeSnapShotItems().add(Companion.getSendReport());
//        Companion.getTakeSnapShotItems().add(Companion.getMaps());
//        Companion.getTakeSnapShotItems().add(Companion.getNavigation());
//        Companion.getTakeSnapShotItems().add(Companion.getDriveScore());
//        Companion.getTakeSnapShotItems().add(Companion.getSmartDash());
    }

    @Nullable
    public List<Model> getItems() {
        switch (this.type) {
            case NAVIGATION_ISSUES:
                this.currentItems = Companion.getReportIssueItems();
                break;
            case SNAP_SHOT:
                this.currentItems = Companion.getTakeSnapShotItems();
                break;
            default:
                return null;
        }
        return this.currentItems;
    }

    public int getInitialSelection() {
        switch (this.type) {
            case NAVIGATION_ISSUES:
                return 1;
            case SNAP_SHOT:
                return 0;
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    @Nullable
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        switch (this.type) {
            case NAVIGATION_ISSUES:
                this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_options_report_issue_2, Companion.getReportIssueColor(), null, 1.0f);
                this.vscrollComponent.selectedText.setText(Companion.getReportIssue());
                return;
            case SNAP_SHOT:
                this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_options_report_issue_2, Companion.getTakeSnapshotColor(), null, 1.0f);
                this.vscrollComponent.selectedText.setText(Companion.getTakeSnapshot());
                return;
            default:
        }
    }

    public boolean selectItem(@NotNull ItemSelectionState selection) {
        Intrinsics.checkParameterIsNotNull(selection, "selection");
        Companion.getLogger().v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.menu_back:
                Companion.getLogger().v("back");
                AnalyticsSupport.recordMenuSelection("report-issue-back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId, true);
                break;
            case R.id.report_issue_menu_eta_inaccurate:
                Companion.getLogger().v("ETA inaccurate");
                AnalyticsSupport.recordMenuSelection("report-issue-eta-inaccurate");
                reportIssue(IssueType.INEFFICIENT_ROUTE_ETA_TRAFFIC);
                break;
            case R.id.report_issue_menu_not_fastest_route:
                Companion.getLogger().v("Not the fastest route");
                AnalyticsSupport.recordMenuSelection("report-issue-not-fastest-route");
                reportIssue(IssueType.INEFFICIENT_ROUTE_SELECTED);
                break;
            case R.id.report_issue_menu_permanent_closure:
                Companion.getLogger().v("Permanent closure");
                AnalyticsSupport.recordMenuSelection("report-issue-permanent-closure");
                reportIssue(IssueType.ROAD_CLOSED_PERMANENT);
                break;
            case R.id.report_issue_menu_road_blocked:
                Companion.getLogger().v("Road blocked");
                AnalyticsSupport.recordMenuSelection("report-issue-road-blocked");
                reportIssue(IssueType.ROAD_CLOSED);
                break;
            case R.id.report_issue_menu_wrong_direction:
                Companion.getLogger().v("Wrong direction");
                AnalyticsSupport.recordMenuSelection("report-issue-wrong-direction");
                reportIssue(IssueType.WRONG_DIRECTION);
                break;
            case R.id.report_issue_menu_wrong_road_name:
                Companion.getLogger().v("Wrong road name");
                AnalyticsSupport.recordMenuSelection("report-issue-wrong-road-name");
                reportIssue(IssueType.ROAD_NAME);
                break;
            case R.id.take_snapshot_drive_score:
            case R.id.take_snapshot_maps:
            case R.id.take_snapshot_navigation:
            case R.id.take_snapshot_smart_dash:
            case R.id.take_snapshot_send:
                takeSnapshot(selection.id);
                break;
        }
        return false;
    }

    private void reportIssue(IssueType issueType) {
        this.presenter.performSelectionAnimation(new ReportIssueMenu$reportIssue$1(this, issueType));
    }

    private  void takeSnapshot(int id) {
        this.presenter.performSelectionAnimation(new ReportIssueMenu$takeSnapshot$1(this, id));
    }

    protected final void showSentToast() {
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, Companion.getTOAST_TIMEOUT());
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_report_issue);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, Companion.getToastSentSuccessfully());
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        bundle.putString(ToastPresenter.EXTRA_TTS, TTSUtils.TTS_NAV_STOPPED);
        bundle.putBoolean(ToastPresenter.EXTRA_NO_START_DELAY, true);
        bundle.putString(ToastPresenter.EXTRA_SHOW_SCREEN_ID, Screen.SCREEN_BACK.name());
        ToastManager.getInstance().addToast(new ToastManager$ToastParams(Companion.getNAV_ISSUE_SENT_TOAST_ID(), bundle, null, true, false));
    }

    @NotNull
    public Menu getType() {
        return Menu.REPORT_ISSUE;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    @Nullable
    public Model getModelfromPos(int pos) {
        if (this.currentItems != null) {
            List list = this.currentItems;
            if (list.size() > pos) {
                list = this.currentItems;
                return (Model) list.get(pos);
            }
        }
        return null;
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(@NotNull Model model, @NotNull View view, int pos, @NotNull ModelState state) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(state, "state");
    }

    @Nullable
    public IMenu getChildMenu(@NotNull IMenu parent, @NotNull String args, @NotNull String path) {
        Intrinsics.checkParameterIsNotNull(parent, "parent");
        Intrinsics.checkParameterIsNotNull(args, "args");
        Intrinsics.checkParameterIsNotNull(path, "path");
        return null;
    }

    public void onUnload(@NotNull MenuLevel level) {
        Intrinsics.checkParameterIsNotNull(level, "level");
    }

    public void onItemSelected(@NotNull ItemSelectionState selection) {
        Intrinsics.checkParameterIsNotNull(selection, "selection");
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public enum ReportIssueMenuType {
        NAVIGATION_ISSUES(0),
        SNAP_SHOT(1);

        private int value;
        ReportIssueMenuType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}

