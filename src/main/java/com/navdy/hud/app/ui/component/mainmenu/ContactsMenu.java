package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;

import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.Iterator;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.contacts.FavoriteContactsManager;
import java.util.ArrayList;
import java.util.HashMap;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import android.text.TextUtils;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.squareup.picasso.Picasso;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.squareup.picasso.Target;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import java.io.File;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import com.makeramen.RoundedTransformationBuilder;
import android.os.Looper;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import java.util.List;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.squareup.picasso.Transformation;
import android.content.res.Resources;
import android.os.Handler;
import com.navdy.hud.app.ui.component.vlist.VerticalList;

class ContactsMenu implements IMenu
{
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final float CONTACT_PHOTO_OPACITY = 0.6f;
    private static final VerticalList.Model back;
    private static final int backColor = resources.getColor(R.color.mm_back);
    private static final int contactColor = resources.getColor(R.color.mm_contacts);
    private static final String contactStr = resources.getString(R.string.carousel_menu_contacts);
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final int noContactColor = resources.getColor(R.color.icon_user_bg_4);
    private static final String recentContactStr = resources.getString(R.string.carousel_menu_recent_contacts_title);
    private static final VerticalList.Model recentContacts;
    private static final Transformation roundTransformation = new RoundedTransformationBuilder().oval(true).build();
    private static final Logger sLogger = new Logger(ContactsMenu.class);
    private int backSelection;
    private int backSelectionId;
    private Bus bus;
    private List<VerticalList.Model> cachedList;
    private IMenu parent;
    private MainMenuScreen2.Presenter presenter;
    private RecentContactsMenu recentContactsMenu;
    private List<VerticalList.Model> returnToCacheList;
    private VerticalMenuComponent vscrollComponent;
    
    static {
        String title = resources.getString(R.string.back);
        int fluctuatorColor = backColor;
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = recentContactStr;
        fluctuatorColor = contactColor;
        recentContacts = IconBkColorViewHolder.buildModel(R.id.contacts_menu_recent, R.drawable.icon_place_recent_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }
    
    ContactsMenu(final Bus bus, final VerticalMenuComponent vscrollComponent, final MainMenuScreen2.Presenter presenter, final IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }
    
    private VerticalList.Model getContactsNotAllowed() {
        final Resources resources = HudApplication.getAppContext().getResources();
        String s;
        if (RemoteDeviceManager.getInstance().isRemoteDeviceIos()) {
            s = resources.getString(R.string.allow_contact_access_iphone_title);
        }
        else {
            s = resources.getString(R.string.allow_contact_access_android_title);
        }
        return IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, ContactsMenu.contactColor, MainMenu.bkColorUnselected, ContactsMenu.contactColor, s, null);
    }
    
    private void setImage(final File file, final int n, final int n2, final int n3) {
        PicassoUtil.getInstance().load(file).resize(n, n2).transform(ContactsMenu.roundTransformation).into(new Target() {
            @Override
            public void onBitmapFailed(final Drawable drawable) {
            }
            
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, final Picasso.LoadedFrom loadedFrom) {
                ContactsMenu.this.presenter.refreshDataforPos(n3);
            }
            
            @Override
            public void onPrepareLoad(final Drawable drawable) {
            }
        });
    }

    VerticalList.Model buildModel(final int n, final String name, String formattedNumber, final String numberTypeStr, int resourceColor, final NumberType numberType, final String initials) {
        final ContactImageHelper instance = ContactImageHelper.getInstance();
        VerticalList.Model model;
        if (TextUtils.isEmpty(name)) {
            model = IconsTwoViewHolder.buildModel(n, R.drawable.icon_user_bg_4, R.drawable.icon_user_numberonly, ContactsMenu.noContactColor, -1, formattedNumber, null);
        }
        else {
            final int resourceId = instance.getResourceId(resourceColor);
            resourceColor = instance.getResourceColor(resourceColor);
            if (numberType != NumberType.OTHER) {
                formattedNumber = numberTypeStr;
            }
            model = IconsTwoViewHolder.buildModel(n, resourceId, R.drawable.icon_user_grey, resourceColor, -1, name, formattedNumber);
        }
        (model.extras = new HashMap<>()).put("INITIAL", initials);
        return model;
    }
    
    @Override
    public IMenu getChildMenu(final IMenu menu, final String s, final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        int n;
        if (this.cachedList == null) {
            n = 0;
        }
        else if (this.cachedList.size() >= 3) {
            n = 2;
        }
        else {
            n = 1;
        }
        return n;
    }
    
    @Override
    public List<VerticalList.Model> getItems() {
        List<VerticalList.Model> cachedList;
        if (this.cachedList != null) {
            cachedList = this.cachedList;
        }
        else {
            cachedList = new ArrayList<>();
            this.returnToCacheList = new ArrayList<>();
            final FavoriteContactsManager instance = FavoriteContactsManager.getInstance();
            if (instance.isAllowedToReceiveContacts()) {
                cachedList.add(ContactsMenu.back);
                cachedList.add(ContactsMenu.recentContacts);
                int n = 0;
                final List<Contact> favoriteContacts = instance.getFavoriteContacts();
                if (favoriteContacts != null) {
                    ContactsMenu.sLogger.v("favorite contacts:" + favoriteContacts.size());
                    for (final Contact state : favoriteContacts) {
                        String numberStr = (TextUtils.isEmpty(state.formattedNumber)) ? state.number:state.formattedNumber;

                        final VerticalList.Model buildModel = this.buildModel(n, state.name, numberStr, state.numberTypeStr, state.defaultImageIndex, state.numberType, state.initials);
                        buildModel.state = state;
                        cachedList.add(buildModel);
                        this.returnToCacheList.add(buildModel);
                        ++n;
                    }
                }
                else {
                    ContactsMenu.sLogger.v("no favorite contacts");
                }
                this.cachedList = cachedList;
            }
            else {
                cachedList.add(this.getContactsNotAllowed());
            }
        }
        return cachedList;
    }
    
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        VerticalList.Model model;
        if (this.cachedList != null && this.cachedList.size() > n) {
            model = this.cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @Override
    public Menu getType() {
        return Menu.CONTACTS;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return true;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
        if (model.state != null) {
            final InitialsImageView initialsImageView = (InitialsImageView)VerticalList.findImageView(view);
            final InitialsImageView initialsImageView2 = (InitialsImageView)VerticalList.findSmallImageView(view);
            final CrossFadeImageView crossFadeImageView = (CrossFadeImageView)VerticalList.findCrossFadeImageView(view);
            initialsImageView.setTag(null);
            String s;
            if (model.state instanceof RecentCall) {
                s = ((RecentCall)model.state).number;
            }
            else {
                s = ((Contact)model.state).number;
            }
            final File imagePath = PhoneImageDownloader.getInstance().getImagePath(s, PhotoType.PHOTO_CONTACT);
            final Bitmap bitmapfromCache = PicassoUtil.getBitmapfromCache(imagePath);
            if (bitmapfromCache != null) {
                initialsImageView.setTag(null);
                initialsImageView.setInitials(null, InitialsImageView.Style.DEFAULT);
                initialsImageView.setImageBitmap(bitmapfromCache);
                initialsImageView2.setInitials(null, InitialsImageView.Style.DEFAULT);
                initialsImageView2.setImageBitmap(bitmapfromCache);
                initialsImageView2.setAlpha(CONTACT_PHOTO_OPACITY);
                crossFadeImageView.setSmallAlpha(0.6f);
                modelState.updateImage = false;
                modelState.updateSmallImage = false;
            }
            else {
                initialsImageView.setTag(model.state);
                TaskManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (imagePath.exists()) {
                            ContactsMenu.handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (initialsImageView.getTag() == model.state) {
                                        ContactsMenu.this.setImage(imagePath, VerticalViewHolder.selectedIconSize, VerticalViewHolder.selectedIconSize, n);
                                    }
                                }
                            });
                        }
                    }
                }, 1);
            }
        }
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(final MenuLevel menuLevel) {
        switch (menuLevel) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    ContactsMenu.sLogger.v("cm:unload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.recentContactsMenu != null) {
                    this.recentContactsMenu.onUnload(MenuLevel.CLOSE);
                    break;
                }
                break;
        }
    }
    
    @Override
    public boolean selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
        ContactsMenu.sLogger.v("select id:" + itemSelectionState.id + " pos:" + itemSelectionState.pos);
        switch (itemSelectionState.id) {
            default: {
                ContactsMenu.sLogger.v("contact options");
                AnalyticsSupport.recordMenuSelection("contact_options");
                final Contact contact = (Contact)this.cachedList.get(itemSelectionState.pos).state;
                final ArrayList<Contact> list = new ArrayList<>(1);
                list.add(contact);
                this.presenter.loadMenu(new ContactOptionsMenu(list, this.vscrollComponent, this.presenter, this, this.bus), MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
            }
            case R.id.menu_back:
                ContactsMenu.sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            case R.id.contacts_menu_recent:
                ContactsMenu.sLogger.v("recent contacts");
                AnalyticsSupport.recordMenuSelection("recent_contacts");
                if (this.recentContactsMenu == null) {
                    this.recentContactsMenu = new RecentContactsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.recentContactsMenu, MenuLevel.SUB_LEVEL, itemSelectionState.pos, 0);
                break;
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int backSelectionId) {
        this.backSelectionId = backSelectionId;
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_contacts_2, ContactsMenu.contactColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(ContactsMenu.contactStr);
    }
    
    @Override
    public void showToolTip() {
    }
}
