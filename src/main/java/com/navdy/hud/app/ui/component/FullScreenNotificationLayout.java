package com.navdy.hud.app.ui.component;

import android.content.res.TypedArray;
import android.view.ViewPropertyAnimator;
import java.util.Iterator;
import com.navdy.hud.app.R;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import java.util.List;
import android.widget.RelativeLayout;

public class FullScreenNotificationLayout extends RelativeLayout
{
    private static final long ANIM_DURATION = 800L;
    private boolean autoPopup;
    private List<View> bodyViews;
    private float directionalTranslation;
    private View footer;
    private int footerId;
    private View title;
    private int titleId;

    public FullScreenNotificationLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        float translationY = getResources().getDimension(R.dimen.text_fade_anim_translationy);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FullScreenNotificationLayout);
        if (ta != null) {
            try {
                this.titleId = ta.getResourceId(R.styleable.FullScreenNotificationLayout_notification_title, 0);
                this.footerId = ta.getResourceId(R.styleable.FullScreenNotificationLayout_notification_footer, 0);
                if (AnimationDirection.values()[ta.getInt(R.styleable.FullScreenNotificationLayout_notification_anim_direction, 0)] != AnimationDirection.UP) {
                    translationY = -translationY;
                }
                this.directionalTranslation = translationY;
                this.autoPopup = ta.getBoolean(R.styleable.FullScreenNotificationLayout_auto_popup, true);
            } finally {
                ta.recycle();
            }
        }
    }
    
    private void animateInSecondStage(final Runnable runnable) {
        if (this.title != null) {
            if (runnable != null) {
                this.getInTitleFooterAnimation(this.title).withEndAction(runnable);
            }
            else {
                this.getInTitleFooterAnimation(this.title);
            }
        }
        if (this.footer != null) {
            if (this.title == null) {
                if (runnable != null) {
                    this.getInTitleFooterAnimation(this.footer).withEndAction(runnable);
                }
                else {
                    this.getInTitleFooterAnimation(this.footer);
                }
            }
            else {
                this.getInTitleFooterAnimation(this.footer);
            }
        }
    }
    
    private void animateOutSecondStage(final Runnable runnable) {
        int n = 0;
        for (final View view : this.bodyViews) {
            if (n == 0) {
                n = 1;
                if (runnable != null) {
                    this.getOutBodyAnimation(view).withEndAction(runnable);
                }
                else {
                    this.getOutBodyAnimation(view);
                }
            }
            else {
                this.getOutBodyAnimation(view);
            }
        }
    }
    
    private ViewPropertyAnimator getInBodyAnimation(final View view) {
        return view.animate().alpha(1.0f).translationY(0.0f).setDuration(800L);
    }
    
    private ViewPropertyAnimator getInTitleFooterAnimation(final View view) {
        return view.animate().alpha(1.0f).setDuration(800L);
    }
    
    private ViewPropertyAnimator getOutBodyAnimation(final View view) {
        return view.animate().alpha(0.0f).translationY(this.directionalTranslation).setDuration(800L);
    }
    
    private ViewPropertyAnimator getOutTitleFooterAnimation(final View view) {
        return view.animate().alpha(0.0f).setDuration(800L);
    }
    
    private void initialize() {
        this.title = this.findViewById(this.titleId);
        this.footer = this.findViewById(this.footerId);
        this.title.setAlpha(0.0f);
        this.footer.setAlpha(0.0f);
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child.getId() != this.titleId && child.getId() != this.footerId) {
                child.setAlpha(0.0f);
                child.setTranslationY(this.directionalTranslation);
                this.bodyViews.add(child);
            }
        }
    }
    
    public void animateIn() {
        this.animateIn(null);
    }
    
    public void animateIn(final Runnable runnable) {
        int n = 0;
        for (final View view : this.bodyViews) {
            if (n == 0) {
                n = 1;
                if (this.title == null && this.footer == null) {
                    if (runnable != null) {
                        this.getInBodyAnimation(view).withEndAction(runnable);
                    }
                    else {
                        this.getInBodyAnimation(view);
                    }
                }
                else {
                    this.getInBodyAnimation(view).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            FullScreenNotificationLayout.this.animateInSecondStage(runnable);
                        }
                    });
                }
            }
            else {
                this.getInBodyAnimation(view);
            }
        }
        if (n == 0) {
            this.animateInSecondStage(runnable);
        }
    }
    
    public void animateOut(final Runnable runnable) {
        if (this.title == null && this.footer == null) {
            this.animateOutSecondStage(runnable);
        }
        else {
            if (this.title != null) {
                this.getOutTitleFooterAnimation(this.title).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        FullScreenNotificationLayout.this.animateOutSecondStage(runnable);
                    }
                });
            }
            if (this.footer != null) {
                if (this.title == null) {
                    this.getOutTitleFooterAnimation(this.footer).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            FullScreenNotificationLayout.this.animateOutSecondStage(runnable);
                        }
                    });
                }
                else {
                    this.getOutTitleFooterAnimation(this.footer);
                }
            }
        }
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.autoPopup) {
            this.animateIn();
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.initialize();
    }

    public enum AnimationDirection {
        UP(0),
        DOWN(1);

        private int value;
        AnimationDirection(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
