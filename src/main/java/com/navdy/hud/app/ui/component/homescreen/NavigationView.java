package com.navdy.hud.app.ui.component.homescreen;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.PointF;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.here.android.mpa.common.CopyrightLogoPosition;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.Map.OnTransformListener;
import com.here.android.mpa.mapping.Map.PixelResult;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.MapState;
import com.here.android.mpa.mapping.MapView;
import com.here.android.mpa.mapping.PositionIndicator;
import com.here.android.mpa.routing.Route;
import com.navdy.hud.app.R;
import com.navdy.hud.app.config.SettingsManager.SettingsChanged;
import com.navdy.hud.app.device.gps.GpsUtils;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.framework.network.NetworkStateManager$HudNetworkInitialized;
import com.navdy.hud.app.gesture.GestureDetector.GestureListener;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents$DialMapZoom;
import com.navdy.hud.app.maps.MapEvents$DialMapZoom$Type;
import com.navdy.hud.app.maps.MapEvents$LocationFix;
import com.navdy.hud.app.maps.MapEvents$MapEngineInitialize;
import com.navdy.hud.app.maps.MapEvents$MapEngineReady;
import com.navdy.hud.app.maps.MapEvents$MapUIReady;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereLocationFixManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapCameraManager;
import com.navdy.hud.app.maps.here.HereMapController;
import com.navdy.hud.app.maps.here.HereMapController$State;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.here.HereMapUtil$SavedRouteData;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.FeatureUtil.Feature;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;

public class NavigationView extends FrameLayout implements IInputHandler, GestureListener, IHomeScreenLifecycle {
    private static final int MAP_MASK_OFFSET = 1;
    private static final MapEvents$DialMapZoom ZOOM_IN = new MapEvents$DialMapZoom(MapEvents$DialMapZoom$Type.IN);
    private static final MapEvents$DialMapZoom ZOOM_OUT = new MapEvents$DialMapZoom(MapEvents$DialMapZoom$Type.OUT);
    private Bus bus;
    private Runnable cleanupMapOverviewRunnable;
    private DriverProfileManager driverProfileManager;
    private MapMarker endMarker;
    private OnTransformListener fluctuatorPosListener;
    private Handler handler;
    private HereLocationFixManager hereLocationFixManager;
    private HereMapsManager hereMapsManager;
    private HomeScreenView homeScreenView;
    private boolean initComplete;
    private boolean isRenderingEnabled;
    private Logger logger;
    private HereMapController mapController;
    @InjectView(R.id.mapIconIndicator)
    ImageView mapIconIndicator;
    private List<MapObject> mapMarkerList;
    @InjectView(R.id.map_mask)
    ImageView mapMask;
    @InjectView(R.id.mapView)
    MapView mapView;
    private boolean networkCheckRequired;
    @InjectView(R.id.noLocationContainer)
    NoLocationView noLocationView;
    private ArrayList<MapObject> overviewMapRouteObjects;
    private boolean paused;
    private Runnable resetMapOverviewRunnable;
    private Image selectedDestinationImage;
    private boolean setInitialCenter;
    private Runnable showMapIconIndicatorRunnable;
    @InjectView(R.id.startDestinationFluctuator)
    FluctuatorAnimatorView startDestinationFluctuatorView;
    private MapMarker startMarker;
    private UIStateManager uiStateManager;
    private Image unselectedDestinationImage;

    public NavigationView(Context context) {
        this(context, null);
    }

    public NavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.logger = new Logger(NavigationView.class);
        this.handler = new Handler();
        this.overviewMapRouteObjects = new ArrayList<>();
        this.mapMarkerList = new ArrayList<>();
        this.fluctuatorPosListener = new OnTransformListener() {
            public void onMapTransformStart() {
            }

            public void onMapTransformEnd(MapState mapState) {
                NavigationView.this.mapController.removeTransformListener(this);
                NavigationView.this.positionFluctuator();
            }
        };
        this.showMapIconIndicatorRunnable = new Runnable() {
            public void run() {
                NavigationView.this.mapIconIndicator.setVisibility(VISIBLE);
            }
        };
        this.resetMapOverviewRunnable = new Runnable() {
            public void run() {
                NavigationView.this.logger.v("resetMapOverview: " + NavigationView.this.mapController.getState());
                NavigationView.this.cleanupMapOverview();
                if (HereMapsManager.getInstance().isInitialized()) {
                    HereMapCameraManager hereMapCameraManager = HereMapCameraManager.getInstance();
                    if (hereMapCameraManager.isOverviewZoomLevel()) {
                        NavigationView.this.showOverviewMap(null, null, hereMapCameraManager.getLastGeoCoordinate(), false);
                    }
                }
            }
        };
        this.cleanupMapOverviewRunnable = new Runnable() {
            public void run() {
                NavigationView.this.logger.v("cleanupMapOverview");
                if (NavigationView.this.startMarker != null) {
                    NavigationView.this.mapController.removeMapObject(NavigationView.this.startMarker);
                }
                if (NavigationView.this.endMarker != null) {
                    NavigationView.this.mapController.removeMapObject(NavigationView.this.endMarker);
                }
                NavigationView.this.cleanupMapOverviewRoutes();
                NavigationView.this.cleanupFluctuator();
            }
        };
        if (!isInEditMode()) {
            this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
            this.hereMapsManager = HereMapsManager.getInstance();
            this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject(this);
        this.mapMask.setX(0.0f);
        this.bus = RemoteDeviceManager.getInstance().getBus();
        this.driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
        initViews();
    }

    public void init(HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }

    @Subscribe
    public void onGeoPositionChange(GeoPosition geoPosition) {
        if (this.startMarker != null) {
            switch (this.mapController.getState()) {
                case OVERVIEW:
                    this.startMarker.setCoordinate(geoPosition.getCoordinate());
                    return;
                case ROUTE_PICKER:
                    this.startMarker.setCoordinate(geoPosition.getCoordinate());
                    return;
                default:
            }
        }
    }

    public HereMapController getMapController() {
        return this.mapController;
    }

    private void initViews() {
        if (!isInEditMode()) {
            if (this.hereLocationFixManager != null) {
                this.hereLocationFixManager.hasLocationFix();
            }
            this.logger.v("first layout: location fix[" + false + "] initComplete[" + this.initComplete + "]");
            this.noLocationView.showLocationUI();

        }
    }

    public void setView(CustomAnimationMode mode) {
        this.logger.v("setview: " + mode);
        setViewMapIconIndicator(mode);
        setViewMap(mode);
        setViewMapMask(mode);
        this.noLocationView.setView(mode);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.hereMapsManager.isInitializing()) {
            this.logger.v("engine initializing, wait");
        } else {
            handleEngineInit();
        }
    }

    @Subscribe
    public void onEngineInitialized(MapEvents$MapEngineInitialize event) {
        handleEngineInit();
    }

    private synchronized void handleEngineInit() {
        if (!this.hereMapsManager.isRenderingAllowed()) {
            this.logger.v("onMapEngineInitializationCompleted: Maps Rendering not allowed, Quiet mode On!");
        } else if (this.hereMapsManager.isInitialized()) {
            this.logger.v("engine initialized");
            onMapEngineInitializationCompleted();
        } else {
            this.logger.e("onCreate() : Cannot initialize map engine: " + this.hereMapsManager.getError());
            this.noLocationView.hideLocationUI();
        }
    }

    public synchronized void enableRendering() {
        if (this.isRenderingEnabled) {
            this.logger.v("enableRendering:Maps Rendering already enabled");
        } else if (this.initComplete) {
            this.hereMapsManager.initNavigation();
            this.mapController.setState(HereMapController$State.AR_MODE);
            this.hereMapsManager.installPositionListener();
            setTransformCenter(HereMapController$State.AR_MODE);
            this.mapView.setMap(this.mapController.getMap());
            this.logger.v("enableRendering:Maps Rendering enabled");
            this.isRenderingEnabled = true;
        } else {
            this.logger.v("enableRendering:Maps Rendering init-engine not complete yet");
        }
    }

    @Subscribe
    public void onWakeup(Wakeup event) {
        handleEngineInit();
    }

    private synchronized void onMapEngineInitializationCompleted() {
        if (!this.initComplete) {
            this.mapController = this.hereMapsManager.getMapController();
            this.initComplete = true;
            this.logger.v("onMapEngineInitializationCompleted: Maps Rendering allowed");
            enableRendering();
            this.mapView.setCopyrightLogoPosition(CopyrightLogoPosition.TOP_RIGHT);
            this.mapView.onResume();
            this.hereMapsManager.setMapView(this.mapView, this);
            try {
                Image image = new Image();
                image.setImageResource(R.drawable.icon_driver_position);
                GeoCoordinate geoCoordinate = this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
                if (geoCoordinate == null) {
                    geoCoordinate = new GeoCoordinate(37.802086d, -122.419015d);
                }
                this.startMarker = new MapMarker(geoCoordinate, image);
            } catch (Throwable t) {
                this.logger.e(t);
            }
            this.logger.v("view rendered");
            if (this.noLocationView.isAcquiringLocation() && this.initComplete && setMapCenter()) {
                this.noLocationView.hideLocationUI();
            }
            this.bus.post(new MapEvents$MapUIReady());
        }
    }

    @Subscribe
    public void onLocationFixChangeEvent(MapEvents$LocationFix event) {
        this.logger.v("location fix event locationAvailable[" + event.locationAvailable + "] phone[" + event.usingPhoneLocation + "] gps [" + event.usingLocalGpsLocation + "]");
        if (isAcquiringLocation() && this.initComplete && setMapCenter()) {
            this.noLocationView.hideLocationUI();
        }
    }

    public void layoutMap() {
        setTransformCenter();
    }

    public void adjustMaplayoutHeight(int height) {
        int y;
        if (height == HomeScreenResourceValues.activeMapHeight) {
            y = HomeScreenResourceValues.iconIndicatorActiveY;
        } else {
            y = HomeScreenResourceValues.iconIndicatorOpenY;
        }
        this.mapIconIndicator.setY(((float) y) - (((float) HomeScreenResourceValues.transformCenterIconHeight) * 0.6f));
        ((MarginLayoutParams) this.mapMask.getLayoutParams()).height = height;
        layoutMap();
        invalidate();
        requestLayout();
    }

    public void updateLayoutForMode(NavigationMode navigationMode) {
        adjustMaplayoutHeight(this.homeScreenView.isNavigationActive() ? HomeScreenResourceValues.activeMapHeight : HomeScreenResourceValues.openMapHeight);
    }

    private void setTransformCenter() {
        if (this.mapController == null) {
            this.logger.v("setTransformCenter:no map");
        } else {
            setTransformCenter(this.mapController.getState());
        }
    }

    private void setTransformCenter(HereMapController$State state) {
        if (this.mapController == null) {
            this.logger.v("setTransformCenter:no map");
            return;
        }
        switch (state) {
            case OVERVIEW:
                if (this.homeScreenView.isNavigationActive()) {
                    this.logger.v("setTransformCenter:overview:nav");
                    this.mapController.setTransformCenter(HomeScreenConstants.transformCenterSmallMiddle);
                    return;
                }
                this.logger.v("setTransformCenter:overview:open");
                this.mapController.setTransformCenter(HomeScreenConstants.transformCenterLargeMiddle);
                return;
            case ROUTE_PICKER:
                this.logger.v("setTransformCenter:routeSearch");
                this.mapController.setTransformCenter(HomeScreenConstants.transformCenterPicker);
                return;
            case AR_MODE:
                if (this.homeScreenView.isNavigationActive()) {
                    this.logger.v("setTransformCenter:ar:nav");
                    this.mapController.setTransformCenter(HomeScreenConstants.transformCenterSmallBottom);
                    return;
                }
                this.logger.v("setTransformCenter:ar:open");
                this.mapController.setTransformCenter(HomeScreenConstants.transformCenterLargeBottom);
                return;
            default:
        }
    }

    public void animateToOverview(GeoCoordinate geoCoordinate, Route route, MapRoute currentMapRoute, Runnable endAction, boolean isArrived) {
        switchToOverviewMode(geoCoordinate, route, currentMapRoute, endAction, isArrived, true, -1.0d);
    }

    private void switchToOverviewMode(GeoCoordinate geoCoordinate, Route route, MapRoute currentMapRoute, Runnable endAction, boolean isArrived, boolean animate, double zoomLevel) {
        this.logger.v("switchToOverviewMode:" + animate);
        this.mapController.setState(HereMapController$State.OVERVIEW);
        cleanupFluctuator();
        this.mapIconIndicator.setVisibility(INVISIBLE);
        final Route route2 = route;
        final Runnable runnable = endAction;
        final GeoCoordinate geoCoordinate2 = geoCoordinate;
        final boolean z = animate;
        final double d = zoomLevel;
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                long l1 = SystemClock.elapsedRealtime();
                boolean isTrafficEnabled = DriverProfileHelper.getInstance().getDriverProfileManager().isTrafficEnabled();
                if (route2 == null) {
                    NavigationView.this.logger.v("switchToOverviewMode:no route traffic=" + isTrafficEnabled);
                    NavigationView.this.setTransformCenter(HereMapController$State.OVERVIEW);
                    NavigationView.this.mapController.addTransformListener(new OnTransformListener() {
                        public void onMapTransformStart() {
                        }

                        public void onMapTransformEnd(MapState mapState) {
                            NavigationView.this.logger.v("switchToOverviewMode:no route  tranform end");
                            NavigationView.this.mapController.removeTransformListener(this);
                            NavigationView.this.handler.post(runnable);
                        }
                    });
                    NavigationView.this.mapController.setCenterForState(HereMapController$State.OVERVIEW, geoCoordinate2, z ? Animation.BOW : Animation.NONE, d, 0.0f, 0.0f);
                    if (NavigationView.this.startMarker != null) {
                        NavigationView.this.startMarker.setCoordinate(geoCoordinate2);
                        NavigationView.this.mapController.addMapObject(NavigationView.this.startMarker);
                        return;
                    }
                    return;
                }
                NavigationView.this.logger.v("switchToOverviewMode:route traffic=" + isTrafficEnabled);
                if (NavigationView.this.startMarker != null) {
                    NavigationView.this.startMarker.setCoordinate(geoCoordinate2);
                    NavigationView.this.mapController.addMapObject(NavigationView.this.startMarker);
                }
                NavigationView.this.logger.v("switchToOverviewMode:map:executeSynchronized");
                NavigationView.this.mapController.setTilt(0.0f);
                if (runnable != null) {
                    NavigationView.this.mapController.addTransformListener(new OnTransformListener() {
                        public void onMapTransformStart() {
                        }

                        public void onMapTransformEnd(MapState mapState) {
                            NavigationView.this.logger.v("switchToOverviewMode:map tranform end");
                            NavigationView.this.mapController.removeTransformListener(this);
                            NavigationView.this.handler.post(runnable);
                        }
                    });
                }
                NavigationView.this.setTransformCenter(HereMapController$State.OVERVIEW);
                NavigationView.this.mapController.zoomTo(route2.getBoundingBox(), HomeScreenConstants.routeOverviewRect, z ? Animation.BOW : Animation.NONE, 0.0f);
                NavigationView.this.logger.v("switchToOverviewMode took [" + (SystemClock.elapsedRealtime() - l1) + "]");
            }
        }, 17);
    }

    public void animateBackfromOverview(final Runnable endAction) {
        this.logger.v("animateBackfromOverview");
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                NavigationView.this.setMapToArMode(true, false, false, null, -1.0d, -1.0d, false);
                NavigationView.this.logger.v("animateBackfromOverview: add transform");
                NavigationView.this.mapController.addTransformListener(NavigationView.this.animateBackfromOverviewMap(endAction));
            }
        }, 17);
    }

    public void switchToRouteSearchMode(GeoCoordinate from, GeoCoordinate to, GeoBoundingBox boundingBox, Route route, boolean startFluctuator, List<GeoCoordinate> markers, int destinationIcon) {
        Image image;
        this.logger.v("switchToRouteSearchMode: start[" + from + "] end [" + to + "] x=" + getMapViewX());
        if (HereMapsManager.getInstance().isInitialized()) {
            HereNavigationManager.getInstance().getSafetySpotListener().setSafetySpotsEnabledOnMap(false);
        }
        setMapMaskVisibility(4);
        setMapViewX(-HomeScreenResourceValues.transformCenterMoveX);
        this.homeScreenView.navigationViewsContainer.setVisibility(INVISIBLE);
        this.homeScreenView.openMapRoadInfoContainer.setVisibility(INVISIBLE);
        if (this.homeScreenView.timeContainer.getVisibility() == VISIBLE) {
            this.homeScreenView.timeContainer.setVisibility(INVISIBLE);
        } else {
            this.homeScreenView.activeEtaContainer.setVisibility(INVISIBLE);
        }
        this.mapController.setState(HereMapController$State.ROUTE_PICKER);
        this.mapIconIndicator.setVisibility(INVISIBLE);
        cleanupMapOverview();
        try {
            setTransformCenter(HereMapController$State.ROUTE_PICKER);
            GeoCoordinate center = from;
            if (center == null) {
                center = this.hereLocationFixManager.getLastGeoCoordinate();
            }
            this.mapController.setCenterForState(HereMapController$State.ROUTE_PICKER, center, Animation.NONE, 15.5d, 0.0f, 0.0f);
        } catch (Throwable t) {
            this.logger.e(t);
        }
        HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
        if (hereNavigationManager.removeCurrentRoute()) {
            this.logger.v("map-route- switchToRouteSearchMode: nav route removed");
        } else {
            this.logger.v("map-route- switchToRouteSearchMode: nav route not removed, does not exist");
        }
        MapMarker destMarker = hereNavigationManager.getDestinationMarker();
        if (destMarker != null) {
            this.mapController.removeMapObject(destMarker);
            this.logger.v("switchToRouteSearchMode: dest removed");
        }
        try {
            if (this.startMarker == null) {
                image = new Image();
                image.setImageResource(R.drawable.icon_driver_position);
                this.startMarker = new MapMarker();
                this.startMarker.setIcon(image);
            }
            if (from != null) {
                this.startMarker.setCoordinate(from);
                this.mapController.addMapObject(this.startMarker);
            }
        } catch (Throwable t2) {
            this.logger.e(t2);
        }
        try {
            if (this.endMarker == null) {
                image = new Image();
                image.setImageResource(R.drawable.icon_pin_dot_destination);
                this.endMarker = new MapMarker();
                this.endMarker.setIcon(image);
            }
            if (to != null) {
                this.endMarker.setCoordinate(to);
                this.mapController.addMapObject(this.endMarker);
            }
        } catch (Throwable t22) {
            this.logger.e(t22);
        }
        if (this.mapMarkerList.size() > 0) {
            this.mapController.removeMapObjects(this.mapMarkerList);
            this.mapMarkerList = new ArrayList<>();
        }
        if (markers != null) {
            int len = markers.size();
            try {
                this.selectedDestinationImage = new Image();
                Image image2 = this.selectedDestinationImage;
                if (destinationIcon == -1) {
                    destinationIcon = R.drawable.icon_pin_dot_destination_blue;
                }
                image2.setImageResource(destinationIcon);
            } catch (Throwable t222) {
                this.logger.e(t222);
            }
            if (this.unselectedDestinationImage == null) {
                try {
                    this.unselectedDestinationImage = new Image();
                    this.unselectedDestinationImage.setImageResource(R.drawable.icon_destination_pin_unselected);
                } catch (Throwable t2222) {
                    this.logger.e(t2222);
                }
            }
            for (int i = 0; i < len; i++) {
                GeoCoordinate g = markers.get(i);
                if (g != null) {
                    MapObject mapMarker = new MapMarker(g, this.unselectedDestinationImage);
                    this.mapController.addMapObject(mapMarker);
                    this.mapMarkerList.add(mapMarker);
                } else {
                    this.mapMarkerList.add(null);
                }
            }
            this.logger.v("addded dest marker:" + len);
        }
        zoomToBoundBox(boundingBox, route, startFluctuator, true);
    }

    public void zoomToBoundBox(GeoBoundingBox boundingBox, Route route, boolean startFluctuator, boolean cleanExistingRoutes) {
        HereMapController$State state = this.mapController.getState();
        if (state != HereMapController$State.ROUTE_PICKER) {
            this.logger.i("zoomToBoundBox: incorrect state:" + state);
            return;
        }
        try {
            cleanupFluctuator();
            if (startFluctuator) {
                startFluctuator();
            }
            if (boundingBox != null) {
                if (cleanExistingRoutes) {
                    cleanupMapOverviewRoutes();
                }
                if (route != null) {
                    MapRoute mapRoute = new MapRoute(route);
                    mapRoute.setTrafficEnabled(true);
                    addMapOverviewRoutes(mapRoute);
                }
                this.mapController.zoomTo(boundingBox, HomeScreenConstants.routePickerViewRect, Animation.BOW, -1.0f);
                this.logger.v("zoomed to bbox");
                return;
            }
            this.logger.v("zoomed to bbox: null");
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    public void changeMarkerSelection(int selected, int unselected) {
        HereMapController$State state = this.mapController.getState();
        if (state != HereMapController$State.ROUTE_PICKER) {
            this.logger.i("changeMarkerSelection: incorrect state:" + state);
            return;
        }
        int len = this.mapMarkerList.size();
        if (len > 0) {
            MapMarker old;
            MapMarker marker;
            this.logger.i("changeMarkerSelection {" + selected + HereManeuverDisplayBuilder.COMMA + unselected + "}");
            if (unselected >= 0 && unselected < len) {
                old = (MapMarker) this.mapMarkerList.get(unselected);
                if (old != null) {
                    marker = new MapMarker(old.getCoordinate(), this.unselectedDestinationImage);
                    this.mapMarkerList.set(unselected, marker);
                    this.mapController.removeMapObject(old);
                    this.mapController.addMapObject(marker);
                }
            }
            if (selected >= 0 && selected < len) {
                old = (MapMarker) this.mapMarkerList.get(selected);
                if (old != null) {
                    marker = new MapMarker(old.getCoordinate(), this.selectedDestinationImage);
                    this.mapMarkerList.set(selected, marker);
                    this.mapController.removeMapObject(old);
                    this.mapController.addMapObject(marker);
                }
            }
        }
    }

    public void switchBackfromRouteSearchMode(boolean addCurrentRoute) {
        HereMapController$State state = this.mapController.getState();
        if (state != HereMapController$State.ROUTE_PICKER) {
            this.logger.v("switchBackfromRouteSearchMode: invalid state:" + state);
            return;
        }
        this.logger.v("map-route- switchBackfromRouteSearchMode:" + addCurrentRoute);
        if (HereMapsManager.getInstance().isInitialized()) {
            HereNavigationManager.getInstance().getSafetySpotListener().setSafetySpotsEnabledOnMap(true);
        }
        int len = this.mapMarkerList.size();
        if (len > 0) {
            this.logger.v("switchBackfromRouteSearchMode remove map markers:" + len);
            this.mapController.removeMapObjects(this.mapMarkerList);
            this.mapMarkerList = new ArrayList<>();
        }
        setMapMaskVisibility(0);
        setMapViewX(0);
        this.homeScreenView.navigationViewsContainer.setVisibility(VISIBLE);
        if (!this.homeScreenView.isNavigationActive()) {
            this.homeScreenView.openMapRoadInfoContainer.setVisibility(VISIBLE);
            if (this.homeScreenView.getDisplayMode() == DisplayMode.MAP) {
                this.homeScreenView.timeContainer.setVisibility(VISIBLE);
            }
        } else if (this.homeScreenView.getDisplayMode() == DisplayMode.MAP) {
            this.homeScreenView.activeEtaContainer.setVisibility(VISIBLE);
        }
        this.mapController.setState(HereMapController$State.TRANSITION);
        if (!DriverProfileHelper.getInstance().getDriverProfileManager().isManualZoom()) {
            setMapToArMode(false, true, true, null, -1.0d, -1.0d, false);
            this.mapIconIndicator.setVisibility(VISIBLE);
        }
        if (this.startMarker != null) {
            this.mapController.removeMapObject(this.startMarker);
        }
        if (this.endMarker != null) {
            this.mapController.removeMapObject(this.endMarker);
        }
        this.logger.v("map-route- switchBackfromRouteSearchMode cleanup routes");
        cleanupMapOverviewRoutes();
        HereNavigationManager hereNavigationManager = HereNavigationManager.getInstance();
        Route navRoute = hereNavigationManager.getCurrentRoute();
        if (!(navRoute == null || !hereNavigationManager.isNavigationModeOn() || hereNavigationManager.hasArrived())) {
            if (addCurrentRoute) {
                MapRoute mapRoute = new MapRoute(navRoute);
                mapRoute.setTrafficEnabled(true);
                hereNavigationManager.addCurrentRoute(mapRoute);
                this.logger.v("map-route- switchBackfromRouteSearchMode: nav route added");
            } else {
                this.logger.v("map-route- switchBackfromRouteSearchMode: nav route not added back");
            }
        }
        MapMarker destinationMarker = HereNavigationManager.getInstance().getDestinationMarker();
        if (destinationMarker != null) {
            this.logger.v("switchBackfromRouteSearchMode: destination marker added");
            this.mapController.addMapObject(destinationMarker);
        }
        cleanupFluctuator();
        HereMapCameraManager.getInstance().setZoom();
    }

    public void showStartMarker() {
        this.logger.v("showStartMarker");
        if (this.startMarker != null) {
            this.mapController.addMapObject(this.startMarker);
        }
    }

    public void resetMapOverview() {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            this.resetMapOverviewRunnable.run();
        } else {
            this.handler.post(this.resetMapOverviewRunnable);
        }
    }

    public void cleanupMapOverview() {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            this.cleanupMapOverviewRunnable.run();
        } else {
            this.handler.post(this.cleanupMapOverviewRunnable);
        }
    }

    public void addMapOverviewRoutes(MapRoute mapRoute) {
        this.logger.v("addMapOverviewRoutes:" + mapRoute);
        this.mapController.addMapObject(mapRoute);
        this.overviewMapRouteObjects.add(mapRoute);
    }

    public void cleanupMapOverviewRoutes() {
        this.logger.v("cleanupMapOverviewRoutes");
        if (this.overviewMapRouteObjects.size() > 0) {
            List<MapObject> copy = new ArrayList(this.overviewMapRouteObjects);
            for (MapObject mapObject : copy) {
                this.logger.v("cleanupMapOverviewRoutes:" + mapObject);
            }
            this.mapController.removeMapObjects(copy);
            this.overviewMapRouteObjects.clear();
        }
    }

    private boolean setMapCenter() {
        if (this.setInitialCenter) {
            return true;
        }
        if (this.initComplete) {
            if (this.hereLocationFixManager == null) {
                this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
            }
            GeoCoordinate center = this.hereLocationFixManager.getLastGeoCoordinate();
            if (center == null) {
                return false;
            }
            this.setInitialCenter = true;
            this.mapController.setCenter(center, Animation.NONE, -1.0d, -1.0f, -1.0f);
            this.logger.w("setMapCenter:setcenterInit done " + center);
            if (RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(Feature.FUEL_ROUTING)) {
                FuelRoutingManager.getInstance().markAvailable();
            }
            updateMapIndicator();
            if (NetworkStateManager.getInstance().isNetworkStateInitialized()) {
                this.logger.v("checkPrevRoute:n/w is initialized");
                startPreviousRoute(center);
                this.logger.v("send map engine ready event-1");
                this.bus.post(new RemoteEvent(new NavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ENGINE_READY, null, null, null, null)));
                this.bus.post(new MapEvents$MapEngineReady());
                return true;
            }
            this.networkCheckRequired = true;
            this.logger.v("checkPrevRoute:n/w is initialized, defer and wait");
            return true;
        }
        this.logger.w("setMapCenter:init not complete yet, deferring");
        return false;
    }

    private void updateMapIndicator() {
        final boolean showRawLocation = GpsUtils.isDebugRawGpsPosEnabled();
        this.mapIconIndicator.setAlpha(showRawLocation ? 0.4f : 1.0f);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                PositionIndicator positionIndicator = NavigationView.this.mapController.getPositionIndicator();
                try {
                    if (showRawLocation) {
                        Image image = new Image();
                        image.setImageResource(R.drawable.here_pos_indicator);
                        positionIndicator.setMarker(image);
                    }
                    positionIndicator.setVisible(showRawLocation);
                } catch (Throwable t) {
                    NavigationView.this.logger.e(t);
                }
            }
        }, 2);
        if (showRawLocation) {
            this.hereLocationFixManager.addMarkers(this.mapController);
        } else {
            this.hereLocationFixManager.removeMarkers(this.mapController);
        }
    }

    @Subscribe
    public void onSettingsChange(SettingsChanged event) {
        if (event.setting == GpsUtils.SHOW_RAW_GPS) {
            updateMapIndicator();
        }
    }

    @Subscribe
    public void onHudNetworkInit(NetworkStateManager$HudNetworkInitialized event) {
        this.logger.v("checkPrevRoute:hud n/w is initialized: " + this.networkCheckRequired);
        if (this.networkCheckRequired) {
            this.networkCheckRequired = false;
            if (this.hereLocationFixManager == null) {
                this.hereLocationFixManager = this.hereMapsManager.getLocationFixManager();
            }
            startPreviousRoute(this.hereLocationFixManager.getLastGeoCoordinate());
            this.logger.v("send map engine ready event-2");
            this.bus.post(new RemoteEvent(new NavigationSessionStatusEvent(NavigationSessionState.NAV_SESSION_ENGINE_READY, null, null, null, null)));
        }
    }

    public void switchScreen() {
        BaseScreen screen = this.uiStateManager.getCurrentScreen();
        if (screen != null) {
            HomeScreenView view = this.uiStateManager.getHomescreenView();
            if (view != null) {
                Main.saveHomeScreenPreference(view.globalPreferences, DisplayMode.MAP.ordinal());
                view.setDisplayMode(DisplayMode.MAP);
            }
            if (screen.getScreen() != Screen.SCREEN_HOME) {
                this.logger.v("current screen is not map:" + screen.getScreen());
                Main main = this.uiStateManager.getRootScreen();
                if (main.isNotificationViewShowing() || main.isNotificationExpanding()) {
                    this.logger.v("switching, but collpasing notif first");
                    this.homeScreenView.setShowCollapsedNotification(true);
                } else {
                    this.logger.v("switched to map");
                }
                this.bus.post(new Builder().screen(Screen.SCREEN_HYBRID_MAP).build());
                return;
            }
            this.logger.v("current screen is map no switch reqd");
            return;
        }
        this.logger.w("current screen is null");
    }

    public void showOverviewMap(Route route, MapRoute currentMapRoute, GeoCoordinate geoCoordinate, boolean isArrived) {
        this.logger.v("showOverviewMap");
        switchToOverviewMode(geoCoordinate, route, currentMapRoute, null, isArrived, false, 12.0d);
        this.mapIconIndicator.setVisibility(INVISIBLE);
    }

    public void showRouteMap(GeoPosition geoPosition, double zoom, float tilt) {
        this.logger.v("showRouteMap");
        setMapToArMode(false, true, false, geoPosition, zoom, (double) tilt, true);
        this.mapIconIndicator.setVisibility(VISIBLE);
    }

    public void setMapToArMode(boolean animate, boolean changeState, boolean useLastZoom, GeoPosition geoPosition, double zoom, double tilt, boolean cleanupMapOverview) {
        if (GenericUtil.isMainThread()) {
            final boolean z = animate;
            final boolean z2 = changeState;
            final boolean z3 = useLastZoom;
            final GeoPosition geoPosition2 = geoPosition;
            final double d = zoom;
            final double d2 = tilt;
            final boolean z4 = cleanupMapOverview;
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    NavigationView.this.setMapToArModeInternal(z, z2, z3, geoPosition2, d, (float) d2, z4);
                }
            }, 17);
            return;
        }
        setMapToArModeInternal(animate, changeState, useLastZoom, geoPosition, zoom, (float) tilt, cleanupMapOverview);
    }

    private void setMapToArModeInternal(boolean animate, boolean changeState, boolean useLastZoom, GeoPosition geoPosition, double zoomLevel, float tiltLevel, boolean cleanupMapOverview) {
        GeoCoordinate center;
        float heading;
        float tilt;
        double zoom;
        GenericUtil.checkNotOnMainThread();
        this.logger.v("setMapToArModeInternal animate:" + animate + " changeState:" + changeState);
        HereMapCameraManager hereMapCameraManager = HereMapCameraManager.getInstance();
        if (geoPosition == null) {
            geoPosition = hereMapCameraManager.getLastGeoPosition();
        }
        if (cleanupMapOverview) {
            cleanupMapOverview();
        }
        if (geoPosition != null) {
            center = geoPosition.getCoordinate();
            heading = (float) geoPosition.getHeading();
            if (tiltLevel == -1.0f) {
                tilt = hereMapCameraManager.getLastTilt();
            } else {
                tilt = tiltLevel;
            }
            if (useLastZoom) {
                zoom = hereMapCameraManager.getLastZoom();
            } else if (zoomLevel == -1.0d) {
                zoom = 12.0d;
            } else {
                zoom = zoomLevel;
            }
        } else {
            center = this.mapController.getCenter();
            heading = -1.0f;
            if (tiltLevel == -1.0f) {
                tilt = 60.0f;
            } else {
                tilt = tiltLevel;
            }
            if (zoomLevel == -1.0d) {
                zoom = 16.5d;
            } else {
                zoom = zoomLevel;
            }
        }
        setTransformCenter(HereMapController$State.AR_MODE);
        this.mapController.setCenterForState(this.mapController.getState(), center, animate ? Animation.BOW : Animation.NONE, zoom, heading, tilt);
        if (changeState) {
            this.mapController.setState(HereMapController$State.AR_MODE);
        } else {
            this.mapController.setState(HereMapController$State.TRANSITION);
        }
    }

    public ImageView getMapIconIndicatorView() {
        return this.mapIconIndicator;
    }

    private void setViewMapIconIndicator(CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                this.mapIconIndicator.setX((float) HomeScreenResourceValues.iconIndicatorX);
                return;
            case SHRINK_LEFT:
                this.mapIconIndicator.setX((float) HomeScreenResourceValues.iconIndicatorShrinkLeft_X);
                return;
            default:
        }
    }

    private void setViewMap(CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                this.mapView.setX((float) HomeScreenResourceValues.mapViewX);
                return;
            case SHRINK_LEFT:
                this.mapView.setX((float) HomeScreenResourceValues.mapViewShrinkLeftX);
                return;
            default:
        }
    }

    private void setViewMapMask(CustomAnimationMode mode) {
        MarginLayoutParams lytParams = (MarginLayoutParams) this.mapMask.getLayoutParams();
        switch (mode) {
            case EXPAND:
                this.mapMask.setX((float) HomeScreenResourceValues.mapMaskX);
                lytParams.width = HomeScreenResourceValues.fullWidth;
                return;
            case SHRINK_LEFT:
                this.mapMask.setX((float) HomeScreenResourceValues.mapMaskShrinkLeftX);
                lytParams.width = HomeScreenResourceValues.mapMaskShrinkWidth;
                return;
            default:
        }
    }

    public void clearState() {
    }

    public void getCustomAnimator(CustomAnimationMode mode, AnimatorSet.Builder mainBuilder) {
        this.logger.v("getCustomAnimator: " + mode);
        ObjectAnimator mapMaskXAnimator;
        ValueAnimator mapMaskWidthAnimator;
        switch (mode) {
            case EXPAND:
                mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this.mapIconIndicator, (float) HomeScreenResourceValues.iconIndicatorX));
                mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this.mapView, (float) HomeScreenResourceValues.mapViewX));
                mapMaskXAnimator = HomeScreenUtils.getXPositionAnimator(this.mapMask, (float) HomeScreenResourceValues.mapMaskX);
                mapMaskWidthAnimator = HomeScreenUtils.getWidthAnimator(this.mapMask, this.mapMask.getMeasuredWidth() + 1, HomeScreenResourceValues.fullWidth + 1);
                mainBuilder.with(mapMaskXAnimator);
                mainBuilder.with(mapMaskWidthAnimator);
                if (isAcquiringLocation()) {
                    this.noLocationView.getCustomAnimator(CustomAnimationMode.EXPAND, mainBuilder);
                    return;
                } else {
                    this.noLocationView.setView(CustomAnimationMode.EXPAND);
                    return;
                }
            case SHRINK_LEFT:
            case SHRINK_MODE:
                int iconIndicatorX;
                int mapViewX;
                int mapMaskX;
                int mapMaskWidth;
                if (mode == CustomAnimationMode.SHRINK_LEFT) {
                    iconIndicatorX = HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
                    mapViewX = HomeScreenResourceValues.mapViewShrinkLeftX;
                    mapMaskX = HomeScreenResourceValues.mapMaskShrinkLeftX;
                    mapMaskWidth = HomeScreenResourceValues.mapMaskShrinkWidth;
                } else {
                    iconIndicatorX = HomeScreenResourceValues.iconIndicatorShrinkModeX;
                    mapViewX = HomeScreenResourceValues.mapViewShrinkModeX;
                    mapMaskX = 0;
                    mapMaskWidth = HomeScreenResourceValues.mapMaskShrinkWidth;
                }
                mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this.mapIconIndicator, (float) iconIndicatorX));
                mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this.mapView, (float) mapViewX));
                mapMaskXAnimator = HomeScreenUtils.getXPositionAnimator(this.mapMask, (float) mapMaskX);
                mapMaskWidthAnimator = HomeScreenUtils.getWidthAnimator(this.mapMask, mapMaskWidth);
                mainBuilder.with(mapMaskXAnimator);
                mainBuilder.with(mapMaskWidthAnimator);
                if (mode != CustomAnimationMode.SHRINK_LEFT) {
                    return;
                }
                if (isAcquiringLocation()) {
                    this.noLocationView.getCustomAnimator(CustomAnimationMode.SHRINK_LEFT, mainBuilder);
                    return;
                } else {
                    this.noLocationView.setView(CustomAnimationMode.SHRINK_LEFT);
                    return;
                }
            default:
        }
    }

    public void getTopAnimator(AnimatorSet.Builder builder, boolean out) {
    }

    public void resetTopViewsAnimator() {
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        switch (event) {
            case RIGHT:
                this.bus.post(ZOOM_IN);
                return true;
            case LEFT:
                this.bus.post(ZOOM_OUT);
                return true;
            default:
                return false;
        }
    }

    private OnTransformListener animateBackfromOverviewMap(final Runnable endAction) {
        return new OnTransformListener() {
            public void onMapTransformStart() {
            }

            public void onMapTransformEnd(MapState mapState) {
                NavigationView.this.logger.v("animateBackfromOverview: transform end");
                NavigationView.this.mapController.removeTransformListener(this);
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        NavigationView.this.handler.post(NavigationView.this.showMapIconIndicatorRunnable);
                        if (NavigationView.this.startMarker != null) {
                            NavigationView.this.mapController.removeMapObject(NavigationView.this.startMarker);
                        }
                        MapRoute mapRoute = HereNavigationManager.getInstance().getCurrentMapRoute();
                        NavigationView.this.logger.v("animateBackfromOverview traffic=" + NavigationView.this.driverProfileManager.isTrafficEnabled());
                        NavigationView.this.mapController.setState(HereMapController$State.AR_MODE);
                        if (endAction != null) {
                            endAction.run();
                        }
                    }
                }, 17);
            }
        };
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public boolean isOverviewMapMode() {
        if (this.mapController == null) {
            return false;
        }
        switch (this.mapController.getState()) {
            case OVERVIEW:
            case ROUTE_PICKER:
                return true;
            default:
                return false;
        }
    }

    public boolean isAcquiringLocation() {
        return this.noLocationView.isAcquiringLocation();
    }

    public void cleanupFluctuator() {
        this.logger.v("cleanupFluctuator");
        this.mapController.removeTransformListener(this.fluctuatorPosListener);
        this.startDestinationFluctuatorView.stop();
        this.startDestinationFluctuatorView.setVisibility(GONE);
    }

    public void startFluctuator() {
        this.mapController.addTransformListener(getFluctuatorTransformListener());
    }

    private void positionFluctuator() {
        try {
            if (this.startMarker != null) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        PixelResult result = NavigationView.this.mapController.projectToPixel(NavigationView.this.startMarker.getCoordinate());
                        if (result != null) {
                            PointF pointF = result.getResult();
                            if (pointF != null) {
                                final int x = ((int) pointF.x) - (HomeScreenResourceValues.startFluctuatorDimension / 2);
                                final int y = ((int) pointF.y) - (HomeScreenResourceValues.startFluctuatorDimension / 2);
                                NavigationView.this.handler.post(new Runnable() {
                                    public void run() {
                                        HereMapController$State state = NavigationView.this.mapController.getState();
                                        if (state == HereMapController$State.ROUTE_PICKER) {
                                            NavigationView.this.startDestinationFluctuatorView.setX((float) (x - HomeScreenResourceValues.transformCenterMoveX));
                                            NavigationView.this.startDestinationFluctuatorView.setY((float) y);
                                            NavigationView.this.startDestinationFluctuatorView.setVisibility(VISIBLE);
                                            NavigationView.this.startDestinationFluctuatorView.start();
                                            return;
                                        }
                                        NavigationView.this.logger.v("fluctuator state from route search mode:" + state);
                                    }
                                });
                            }
                        }
                    }
                }, 3);
            }
        } catch (Throwable t) {
            this.logger.e(t);
        }
    }

    public OnTransformListener getFluctuatorTransformListener() {
        return this.fluctuatorPosListener;
    }

    private void startPreviousRoute(final GeoCoordinate center) {
        TaskManager.getInstance().execute(new Runnable() {
            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                try {
                    NavigationView.this.logger.v("check if prev route was active");
                    HereMapUtil$SavedRouteData savedRouteData = HereMapUtil.getSavedRouteData(NavigationView.this.logger);
                    NavigationRouteRequest request = savedRouteData.navigationRouteRequest;
                    if (request == null) {
                        NavigationView.this.logger.v("no saved route info");
                        DestinationsManager.getInstance().launchSuggestedDestination();
                    } else {
                        NavigationView.this.logger.v("saved route found");
                        HereRouteManager.printRouteRequest(request);
                        if (NavigationView.this.homeScreenView.isNavigationActive()) {
                            NavigationView.this.logger.v("navigation is already active or route being calculated,ignore the saved route");
                            DestinationsManager.getInstance().clearSuggestedDestination();
                            HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                            return;
                        }
                        GeoCoordinate target = new GeoCoordinate(request.destination.latitude, request.destination.longitude, 0.0d);
                        double distance = center.distanceTo(target);
                        NavigationView.this.logger.v("distance remaining to destination is [" + distance + "] current[" + center + "] target [" + target + "]");
                        boolean isGasRoute = request.routeAttributes.contains(RouteAttribute.ROUTE_ATTRIBUTE_GAS);
                        if (isGasRoute && distance >= 0.0d && distance <= 500.0d) {
                            NavigationView.this.logger.v("gas route and distance to destination is <= 500.0, don't navigate");
                            NavigationView.this.logger.v("remove gas route and look for non gas route");
                            HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                            savedRouteData = HereMapUtil.getSavedRouteData(NavigationView.this.logger);
                            request = savedRouteData.navigationRouteRequest;
                            if (request == null) {
                                NavigationView.this.logger.v("no non gas route");
                                DestinationsManager.getInstance().launchSuggestedDestination();
                                HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                                return;
                            }
                            isGasRoute = false;
                            distance = center.distanceTo(new GeoCoordinate(request.destination.latitude, request.destination.longitude, 0.0d));
                        }
                        if (isGasRoute || distance < 0.0d || distance > 500.0d) {
                            DestinationsManager.getInstance().clearSuggestedDestination();
                            request = new NavigationRouteRequest.Builder(request).originDisplay(Boolean.TRUE).cancelCurrent(Boolean.FALSE).build();
                            NavigationView.this.logger.v("sleep before starting navigation");
                            GenericUtil.sleep(2000);
                            NavigationView.this.logger.v("slept");
                            if (savedRouteData.isGasRoute && RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(Feature.FUEL_ROUTING)) {
                                NavigationView.this.logger.v("start saveroute navigation (gas route)");
                            } else {
                                NavigationView.this.logger.v("start saveroute navigation");
                            }
                            NavigationView.this.bus.post(request);
                            NavigationView.this.bus.post(new RemoteEvent(request));
                        } else {
                            NavigationView.this.logger.v("distance to destination is <= 500, don't navigate");
                            DestinationsManager.getInstance().launchSuggestedDestination();
                            HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                            return;
                        }
                    }
                    HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                } catch (Throwable th) {
                    HereMapUtil.removeRouteInfo(NavigationView.this.logger, false);
                }
            }
        }, 3);
    }

    public void onPause() {
        if (!this.paused) {
            this.logger.v("::onPause");
            this.paused = true;
            this.hereMapsManager.stopMapRendering();
        }
    }

    public void onResume() {
        if (this.paused) {
            this.logger.v("::onResume");
            this.paused = false;
            this.hereMapsManager.startMapRendering();
        }
    }

    public void setMapMaskVisibility(int visibility) {
        this.mapMask.setVisibility(visibility);
    }

    public void setMapViewX(int x) {
        this.mapView.setX((float) x);
    }

    public int getMapViewX() {
        return (int) this.mapView.getX();
    }
}
