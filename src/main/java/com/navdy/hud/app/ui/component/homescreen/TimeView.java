package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.R;
import com.navdy.hud.app.view.MainView;
import com.squareup.otto.Subscribe;
import android.content.Intent;
import android.content.BroadcastReceiver;
import com.navdy.hud.app.HudApplication;
import android.content.IntentFilter;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.view.View;
import butterknife.ButterKnife;
import java.util.Date;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import com.squareup.otto.Bus;
import butterknife.InjectView;
import android.widget.TextView;
import android.widget.RelativeLayout;

public class TimeView extends RelativeLayout
{
    public static final int CLOCK_UPDATE_INTERVAL = 30000;
    @InjectView(R.id.txt_ampm)
    TextView ampmTextView;
    private Bus bus;
    @InjectView(R.id.txt_day)
    TextView dayTextView;
    private Handler handler;
    private Logger logger;
    private Runnable runnable;
    private StringBuilder stringBuilder;
    private TimeHelper timeHelper;
    @InjectView(R.id.txt_time)
    TextView timeTextView;
    
    public TimeView(final Context context) {
        this(context, null);
    }
    
    public TimeView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public TimeView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.stringBuilder = new StringBuilder();
    }
    
    private void updateTime() {
        this.dayTextView.setText((CharSequence)this.timeHelper.getDay());
        this.timeTextView.setText((CharSequence)this.timeHelper.formatTime(new Date(), this.stringBuilder));
        this.ampmTextView.setText((CharSequence)this.stringBuilder.toString());
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.handler != null) {
            this.handler.removeCallbacks(this.runnable);
        }
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        if (!this.isInEditMode()) {
            this.timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
            this.updateTime();
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            HudApplication.getAppContext().registerReceiver((BroadcastReceiver)new BroadcastReceiver() {
                public void onReceive(final Context context, final Intent intent) {
                    if ("android.intent.action.TIMEZONE_CHANGED".equals(intent.getAction())) {
                        TimeView.this.logger.v("timezone change broadcast");
                        TimeView.this.timeHelper.updateLocale();
                    }
                }
            }, intentFilter);
            this.postDelayed(this.runnable = new Runnable() {
                @Override
                public void run() {
                    TimeView.this.updateTime();
                    TimeView.this.postDelayed(TimeView.this.runnable, 30000L);
                }
            }, 30000L);
            (this.bus = RemoteDeviceManager.getInstance().getBus()).register(this);
        }
    }
    
    @Subscribe
    public void onTimeSettingsChange(final TimeHelper.UpdateClock updateClock) {
        this.updateTime();
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case EXPAND:
                this.setX((float)HomeScreenResourceValues.timeX);
                break;
            case SHRINK_LEFT:
                this.setX((float)HomeScreenResourceValues.timeShrinkLeftX);
                break;
        }
    }
}
