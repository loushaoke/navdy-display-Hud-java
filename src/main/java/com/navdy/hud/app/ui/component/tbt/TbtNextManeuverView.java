package com.navdy.hud.app.ui.component.tbt;

import com.navdy.hud.app.R;
import com.navdy.hud.app.maps.MapEvents;
import kotlin.TypeCastException;
import android.animation.AnimatorSet;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import android.widget.TextView;
import android.widget.ImageView;

import com.navdy.hud.app.maps.MapEvents$ManeuverDisplay;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState;
import com.navdy.hud.app.view.MainView;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;
import kotlin.Metadata;
import android.widget.LinearLayout;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 !2\u00020\u0001:\u0001!B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0006\u0010\u0014\u001a\u00020\u0015J\u001a\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\f2\n\u0010\u0018\u001a\u00060\u0019R\u00020\u001aJ\b\u0010\u001b\u001a\u00020\u0015H\u0014J\u000e\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\fJ\u000e\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u001fJ\b\u0010 \u001a\u00020\u0015H\u0002R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\"" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "lastManeuverState", "Lcom/navdy/hud/app/maps/here/HereManeuverDisplayBuilder$ManeuverState;", "lastNextTurnIconId", "nextManeuverIcon", "Landroid/widget/ImageView;", "nextManeuverText", "Landroid/widget/TextView;", "clear", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet.Builder;", "Landroid/animation/AnimatorSet;", "onFinishInflate", "setMode", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "updateIcon", "Companion", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class TbtNextManeuverView extends LinearLayout
{
    public static final Companion Companion;
    private static final Logger logger;
    private HashMap _$_findViewCache;
    private MainView.CustomAnimationMode currentMode;
    private HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private int lastNextTurnIconId;
    private ImageView nextManeuverIcon;
    private TextView nextManeuverText;
    
    static {
        Companion = new Companion();
        logger = new Logger("TbtNextManeuverView");
    }
    
    public TbtNextManeuverView(@NotNull final Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.lastNextTurnIconId = -1;
    }
    
    public TbtNextManeuverView(@NotNull final Context context, @NotNull final AttributeSet set) {
        super(context, set);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        this.lastNextTurnIconId = -1;
    }
    
    public TbtNextManeuverView(@NotNull final Context context, @NotNull final AttributeSet set, final int n) {
        super(context, set, n);
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        this.lastNextTurnIconId = -1;
    }
    
    @NotNull
    public static  /* synthetic */ Logger access$getLogger$cp() {
        return TbtNextManeuverView.logger;
    }
    
    private  void updateIcon() {
        if (this.lastNextTurnIconId != -1) {
            final ImageView nextManeuverIcon = this.nextManeuverIcon;
            if (nextManeuverIcon != null) {
                nextManeuverIcon.setImageResource(this.lastNextTurnIconId);
            }
            if (Intrinsics.areEqual(this.currentMode, MainView.CustomAnimationMode.SHRINK_LEFT)) {
                this.setVisibility(GONE);
            }
            else {
                this.setVisibility(View.VISIBLE);
            }
        }
        else {
            final ImageView nextManeuverIcon2 = this.nextManeuverIcon;
            if (nextManeuverIcon2 != null) {
                nextManeuverIcon2.setImageDrawable(null);
            }
            this.setVisibility(GONE);
        }
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = (View)this._$_findViewCache.get(n)) == null) {
            viewById = this.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    public final void clear() {
        this.lastNextTurnIconId = -1;
        final ImageView nextManeuverIcon = this.nextManeuverIcon;
        if (nextManeuverIcon != null) {
            nextManeuverIcon.setImageDrawable(null);
        }
        this.setVisibility(GONE);
    }
    
    public final void getCustomAnimator(@NotNull final MainView.CustomAnimationMode customAnimationMode, @NotNull final AnimatorSet.Builder animatorSet$Builder) {
        Intrinsics.checkParameterIsNotNull(customAnimationMode, "mode");
        Intrinsics.checkParameterIsNotNull(animatorSet$Builder, "mainBuilder");
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        final View viewById = this.findViewById(R.id.nextManeuverText);
        if (viewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
        }
        this.nextManeuverText = (TextView)viewById;
        final View viewById2 = this.findViewById(R.id.nextManeuverIcon);
        if (viewById2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
        }
        this.nextManeuverIcon = (ImageView)viewById2;
    }
    
    public final void setMode(@NotNull final MainView.CustomAnimationMode currentMode) {
        Intrinsics.checkParameterIsNotNull(currentMode, "mode");
        if (!Intrinsics.areEqual(this.currentMode, currentMode)) {
            this.currentMode = currentMode;
            if (Intrinsics.areEqual(currentMode, MainView.CustomAnimationMode.EXPAND)) {
                this.updateIcon();
            }
            else {
                this.setVisibility(GONE);
            }
        }
    }
    
    public final void updateDisplay(@NotNull final MapEvents$ManeuverDisplay maneuverDisplay) {
        Intrinsics.checkParameterIsNotNull(maneuverDisplay, "event");
        if (this.lastNextTurnIconId != maneuverDisplay.nextTurnIconId) {
            this.lastNextTurnIconId = maneuverDisplay.nextTurnIconId;
            this.lastManeuverState = maneuverDisplay.maneuverState;
            this.updateIcon();
        }
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView$Companion;", "", "()V", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private  Logger getLogger() {
            return TbtNextManeuverView.access$getLogger$cp();
        }
    }
}
