package com.navdy.hud.app.ui.component;

import android.text.TextUtils;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable;
import java.util.Iterator;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.navdy.hud.app.audio.SoundUtils;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.graphics.Typeface;
import android.content.res.TypedArray;
import com.navdy.hud.app.R;
import android.animation.ObjectAnimator;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.ViewGroup;
import android.animation.ValueAnimator;
import android.animation.Animator;
import java.util.LinkedList;
import android.util.AttributeSet;
import android.content.Context;
import java.util.Queue;
import android.animation.Animator;
import android.os.Handler;
import android.animation.ValueAnimator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import android.view.View;
import java.util.List;
import android.widget.LinearLayout;
import android.animation.AnimatorSet;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

public class ChoiceLayout extends FrameLayout
{
    private static final int ANIMATION_EXECUTE_DURATION = 200;
    private static final int ANIMATION_SLIDE_DURATION = 100;
    private static final Object ANIMATION_TAG;
    private static final String EMPTY = "";
    private static final int KEY_UP_DOWN_DURATION = 50;
    private static int MIN_HIGHLIGHT_VIEW_WIDTH;
    private static int choiceHighlightColor;
    private static int choiceHighlightMargin;
    private static int choiceTextPaddingBottomDefault;
    private static int choiceTextPaddingDefault;
    private static int choiceTextSizeDefault;
    private static int textDeselectionColor;
    private static int textSelectionColor;
    private ViewTreeObserver.OnGlobalLayoutListener animationLayoutListener;
    private AnimatorSet animatorSet;
    protected LinearLayout choiceContainer;
    private int choiceTextPaddingBottom;
    private int choiceTextPaddingLeft;
    private int choiceTextPaddingRight;
    private int choiceTextPaddingTop;
    private int choiceTextSize;
    protected List<Choice> choices;
    private Mode currentMode;
    private View currentSelectionView;
    private DefaultAnimationListener executeAnimationListener;
    private ValueAnimator.AnimatorUpdateListener executeUpdateListener;
    private View executeView;
    private boolean executing;
    private Handler handler;
    private boolean highlightPersistent;
    protected View highlightView;
    private Animator.AnimatorListener highlightViewListener;
    private int highlightVisibility;
    private boolean initialized;
    private IListener listener;
    private View oldSelectionView;
    private Queue<Operation> operationQueue;
    private boolean operationRunning;
    private boolean pressedDown;
    private int selectedItem;
    private DefaultAnimationListener upDownAnimationListener;
    private ValueAnimator.AnimatorUpdateListener upDownUpdateListener;
    
    static {
        ANIMATION_TAG = new Object();
        ChoiceLayout.textSelectionColor = -1;
    }
    
    public ChoiceLayout(final Context context) {
        this(context, null);
    }
    
    public ChoiceLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ChoiceLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.highlightPersistent = false;
        this.highlightVisibility = 0;
        this.listener = null;
        this.selectedItem = -1;
        this.handler = new Handler();
        this.operationQueue = new LinkedList<Operation>();
        this.upDownAnimationListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                super.onAnimationEnd(animator);
                ChoiceLayout.this.runQueuedOperation();
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                super.onAnimationStart(animator);
            }
        };
        this.upDownUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                ((MarginLayoutParams)ChoiceLayout.this.highlightView.getLayoutParams()).topMargin = (int)valueAnimator.getAnimatedValue();
                ChoiceLayout.this.highlightView.invalidate();
                ChoiceLayout.this.highlightView.requestLayout();
            }
        };
        this.executeAnimationListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                if (ChoiceLayout.this.executeView != null) {
                    if (ChoiceLayout.this.executeView.getTag() != null) {
                        ChoiceLayout.this.executeView.setTag(null);
                        if (ChoiceLayout.this.executeView instanceof TextView) {
                            ((TextView)ChoiceLayout.this.executeView).setTextColor(ChoiceLayout.choiceHighlightColor);
                        }
                        else if (ChoiceLayout.this.executeView instanceof ImageView) {
                            ((ImageView)ChoiceLayout.this.executeView).setColorFilter(ChoiceLayout.choiceHighlightColor);
                        }
                        final ValueAnimator ofInt = ValueAnimator.ofInt(0, ChoiceLayout.choiceHighlightMargin);
                        ofInt.addUpdateListener(ChoiceLayout.this.executeUpdateListener);
                        ofInt.addListener(ChoiceLayout.this.executeAnimationListener);
                        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(ChoiceLayout.this.highlightView, "alpha", 1.0f, 0.5f);
                        ChoiceLayout.this.animatorSet = new AnimatorSet();
                        ChoiceLayout.this.animatorSet.setDuration(200L);
                        ChoiceLayout.this.animatorSet.playTogether(ofInt, ofFloat);
                        ChoiceLayout.this.animatorSet.start();
                    }
                    else {
                        if (!ChoiceLayout.this.isHighlightPersistent()) {
                            ChoiceLayout.this.highlightView.setVisibility(INVISIBLE);
                        }
                        ChoiceLayout.this.highlightView.setAlpha(1.0f);
                        ((ViewGroup.MarginLayoutParams)ChoiceLayout.this.highlightView.getLayoutParams()).topMargin = ChoiceLayout.choiceHighlightMargin;
                        ChoiceLayout.this.executing = false;
                        ChoiceLayout.this.animatorSet = null;
                        ChoiceLayout.this.clearOperationQueue();
                        if (ChoiceLayout.this.selectedItem != -1) {
                            ChoiceLayout.this.callListener(ChoiceLayout.this.selectedItem);
                        }
                        if (ChoiceLayout.this.executeView instanceof TextView) {
                            ((TextView)ChoiceLayout.this.currentSelectionView).setTextColor(ChoiceLayout.textSelectionColor);
                        }
                        else if (ChoiceLayout.this.executeView instanceof ImageView) {
                            ((ImageView)ChoiceLayout.this.currentSelectionView).setColorFilter(ChoiceLayout.textSelectionColor);
                        }
                    }
                }
            }
        };
        this.executeUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                if (ChoiceLayout.this.executeView != null) {
                    ((MarginLayoutParams)ChoiceLayout.this.highlightView.getLayoutParams()).topMargin = (int)valueAnimator.getAnimatedValue();
                    ChoiceLayout.this.highlightView.invalidate();
                    ChoiceLayout.this.highlightView.requestLayout();
                }
            }
        };
        this.animationLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                final View child = ChoiceLayout.this.choiceContainer.getChildAt(ChoiceLayout.this.selectedItem);
                if (child != null) {
                    child.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    ChoiceLayout.this.selectInitialItem(ChoiceLayout.this.selectedItem);
                }
            }
        };
        this.highlightViewListener = new DefaultAnimationListener() {
            @Override
            public void onAnimationEnd(final Animator animator) {
                if (ChoiceLayout.this.oldSelectionView != null) {
                    if (ChoiceLayout.this.currentMode == Mode.LABEL) {
                        ((TextView)ChoiceLayout.this.oldSelectionView).setTextColor(ChoiceLayout.textDeselectionColor);
                    }
                    else {
                        ((ImageView)ChoiceLayout.this.oldSelectionView).setColorFilter(ChoiceLayout.textDeselectionColor);
                    }
                }
                Integer n = null;
                final Integer n2 = null;
                if (ChoiceLayout.this.currentSelectionView != null) {
                    final Object tag = ChoiceLayout.this.currentSelectionView.getTag();
                    n = n2;
                    if (tag instanceof Integer) {
                        n = (Integer)tag;
                    }
                    if (ChoiceLayout.this.currentMode == Mode.LABEL) {
                        ((TextView)ChoiceLayout.this.currentSelectionView).setTextColor(ChoiceLayout.textSelectionColor);
                    }
                    else {
                        ((ImageView)ChoiceLayout.this.currentSelectionView).setColorFilter(ChoiceLayout.textSelectionColor);
                    }
                }
                ChoiceLayout.this.animatorSet = null;
                if (n != null) {
                    ChoiceLayout.this.callSelectListener(n);
                }
                ChoiceLayout.this.runQueuedOperation();
            }
        };
        this.init();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ChoiceLayout, n, 0);
        if (obtainStyledAttributes != null) {
            this.choiceTextPaddingLeft = (int)obtainStyledAttributes.getDimension(R.styleable.ChoiceLayout_choice_text_padding_left, (float)ChoiceLayout.choiceTextPaddingDefault);
            this.choiceTextPaddingRight = (int)obtainStyledAttributes.getDimension(R.styleable.ChoiceLayout_choice_text_padding_right, (float)ChoiceLayout.choiceTextPaddingDefault);
            this.choiceTextPaddingBottom = (int)obtainStyledAttributes.getDimension(R.styleable.ChoiceLayout_choice_text_padding_bottom, (float)ChoiceLayout.choiceTextPaddingBottomDefault);
            this.choiceTextPaddingTop = (int)obtainStyledAttributes.getDimension(R.styleable.ChoiceLayout_choice_text_padding_top, (float)ChoiceLayout.choiceTextPaddingDefault);
            this.choiceTextSize = (int)obtainStyledAttributes.getDimension(R.styleable.ChoiceLayout_choice_text_size, (float)ChoiceLayout.choiceTextSizeDefault);
            obtainStyledAttributes.recycle();
        }
    }
    
    private TextView buildChoice(final String text, final int n) {
        final TextView textView = new TextView(this.getContext());
        textView.setGravity(17);
        textView.setSingleLine(true);
        textView.setText(text);
        textView.setIncludeFontPadding(false);
        textView.setTextSize(0, (float)this.choiceTextSize);
        textView.setTypeface(null, 1);
        textView.setTextColor(ChoiceLayout.textDeselectionColor);
        return textView;
    }
    
    private void buildChoices() {
        this.choiceContainer.removeAllViews();
        this.currentSelectionView = null;
        this.oldSelectionView = null;
        this.highlightView.setVisibility(INVISIBLE);
        if (this.choices != null) {
            for (int size = this.choices.size(), i = 0; i < size; ++i) {
                final Choice choice = this.choices.get(i);
                Object buildChoice;
                if (this.currentMode == Mode.LABEL) {
                    buildChoice = this.buildChoice("", i);
                    ((TextView)buildChoice).setText(choice.label);
                }
                else {
                    buildChoice = new ImageView(this.getContext());
                    ((ImageView)buildChoice).setScaleType(ImageView.ScaleType.FIT_CENTER);
                    ((ImageView)buildChoice).setImageResource(choice.icon.resIdUnSelected);
                    ((ImageView)buildChoice).setColorFilter(ChoiceLayout.textDeselectionColor);
                }
                final LinearLayout.LayoutParams linearLayout$LayoutParams = new LinearLayout.LayoutParams(-2, -2);
                linearLayout$LayoutParams.setMargins(this.choiceTextPaddingLeft, 0, this.choiceTextPaddingRight, 0);
                this.choiceContainer.addView((View)buildChoice, linearLayout$LayoutParams);
            }
        }
        this.invalidate();
        this.requestLayout();
    }
    
    private void callListener(final int n) {
        if (this.listener != null) {
            SoundUtils.playSound(SoundUtils.Sound.MENU_SELECT);
            if (this.choices != null && n >= 0 && n < this.choices.size()) {
                this.listener.executeItem(n, this.choices.get(n).id);
            }
        }
    }
    
    private void callSelectListener(final int n) {
        if (this.listener != null && this.choices != null && n >= 0 && n < this.choices.size()) {
            this.listener.itemSelected(n, this.choices.get(n).id);
        }
    }
    
    private boolean executeSelectedItemInternal() {
        boolean b = true;
        if (!this.initialized || this.executing) {
            b = false;
        }
        else {
            if (this.selectedItem != -1 && this.getItemCount() > 0) {
                this.executing = true;
                if (this.highlightVisibility == 0) {
                    this.executeView = this.choiceContainer.getChildAt(this.selectedItem);
                    if (this.executeView != null) {
                        this.executeView.setTag(ChoiceLayout.ANIMATION_TAG);
                        final ValueAnimator ofInt = ValueAnimator.ofInt(ChoiceLayout.choiceHighlightMargin, 0);
                        ofInt.addListener(this.executeAnimationListener);
                        ofInt.addUpdateListener(this.executeUpdateListener);
                        (this.animatorSet = new AnimatorSet()).setDuration(200L);
                        this.animatorSet.play(ofInt);
                        this.animatorSet.start();
                        return b;
                    }
                    this.callListener(this.selectedItem);
                    this.executing = false;
                }
                else {
                    this.callListener(this.selectedItem);
                    this.executing = false;
                }
            }
            b = false;
        }
        return b;
    }
    
    private int getItemCount() {
        int size;
        if (this.choices != null) {
            size = this.choices.size();
        }
        else {
            size = 0;
        }
        return size;
    }
    
    private float getXPositionForHighlight(final View view) {
        final int n = view.getMeasuredWidth() - (view.getPaddingLeft() + view.getPaddingRight());
        final int n2 = view.getLeft() + view.getPaddingLeft();
        float n3;
        if (n >= ChoiceLayout.MIN_HIGHLIGHT_VIEW_WIDTH) {
            n3 = n2;
        }
        else {
            n3 = n2 - (ChoiceLayout.MIN_HIGHLIGHT_VIEW_WIDTH - n) / 2;
        }
        return n3;
    }
    
    private void init() {
        LayoutInflater.from(this.getContext()).inflate(R.layout.choices_lyt, this, true);
        this.invalidate();
        this.choiceContainer = (LinearLayout)this.findViewById(R.id.choiceContainer);
        this.highlightView = this.findViewById(R.id.choiceHighlight);
        if (ChoiceLayout.textSelectionColor == -1) {
            final Resources resources = this.getResources();
            ChoiceLayout.MIN_HIGHLIGHT_VIEW_WIDTH = resources.getDimensionPixelSize(R.dimen.min_highlight_view_width);
            ChoiceLayout.textSelectionColor = resources.getColor(R.color.choice_selection);
            ChoiceLayout.textDeselectionColor = resources.getColor(R.color.choice_deselection);
            ChoiceLayout.choiceTextPaddingDefault = (int)resources.getDimension(R.dimen.choices_lyt_item_padding);
            ChoiceLayout.choiceTextPaddingBottomDefault = (int)resources.getDimension(R.dimen.choices_lyt_item_padding_bottom);
            ChoiceLayout.choiceTextSizeDefault = (int)resources.getDimension(R.dimen.choices_lyt_text_size);
            ChoiceLayout.choiceHighlightColor = resources.getColor(R.color.cyan);
            ChoiceLayout.choiceHighlightMargin = (int)resources.getDimension(R.dimen.choices_lyt_highlight_margin);
        }
    }
    
    private boolean keyDownSelectedItemInternal() {
        boolean b;
        if (!this.initialized || this.pressedDown) {
            b = false;
        }
        else {
            if (this.selectedItem != -1 && this.getItemCount() > 0) {
                if (this.currentMode == Mode.LABEL) {
                    ((TextView)this.currentSelectionView).setTextColor(ChoiceLayout.choiceHighlightColor);
                }
                else {
                    ((ImageView)this.currentSelectionView).setColorFilter(ChoiceLayout.choiceHighlightColor);
                }
                if (this.highlightVisibility == 0) {
                    this.pressedDown = true;
                    if (this.choiceContainer.getChildAt(this.selectedItem) != null) {
                        final ValueAnimator ofInt = ValueAnimator.ofInt(ChoiceLayout.choiceHighlightMargin, 0);
                        ofInt.addListener(this.upDownAnimationListener);
                        ofInt.addUpdateListener(this.upDownUpdateListener);
                        (this.animatorSet = new AnimatorSet()).setDuration(50L);
                        this.animatorSet.play(ofInt);
                        this.animatorSet.start();
                        b = true;
                        return b;
                    }
                }
            }
            b = true;
        }
        return b;
    }
    
    private boolean keyUpSelectedItemInternal() {
        boolean b;
        if (!this.initialized || !this.pressedDown) {
            b = false;
        }
        else {
            if (this.selectedItem != -1 && this.getItemCount() > 0) {
                if (this.currentMode == Mode.LABEL) {
                    ((TextView)this.currentSelectionView).setTextColor(ChoiceLayout.textSelectionColor);
                }
                else {
                    ((ImageView)this.currentSelectionView).setColorFilter(ChoiceLayout.textSelectionColor);
                }
                if (this.highlightVisibility == 0) {
                    this.pressedDown = false;
                    if (this.choiceContainer.getChildAt(this.selectedItem) != null) {
                        final ValueAnimator ofInt = ValueAnimator.ofInt(0, ChoiceLayout.choiceHighlightMargin);
                        ofInt.addListener(this.upDownAnimationListener);
                        ofInt.addUpdateListener(this.upDownUpdateListener);
                        (this.animatorSet = new AnimatorSet()).setDuration(50L);
                        this.animatorSet.play(ofInt);
                        this.animatorSet.start();
                        b = true;
                        return b;
                    }
                }
            }
            b = true;
        }
        return b;
    }
    
    private boolean moveSelectionLeftInternal() {
        boolean setSelectedItem;
        final boolean b = setSelectedItem = false;
        if (this.initialized) {
            if (this.executing) {
                setSelectedItem = b;
            }
            else {
                setSelectedItem = b;
                if (this.selectedItem != -1) {
                    setSelectedItem = this.setSelectedItem(this.selectedItem - 1);
                }
            }
        }
        return setSelectedItem;
    }
    
    private boolean moveSelectionRightInternal() {
        boolean setSelectedItem;
        final boolean b = setSelectedItem = false;
        if (this.initialized) {
            if (this.executing) {
                setSelectedItem = b;
            }
            else {
                setSelectedItem = b;
                if (this.selectedItem != -1) {
                    setSelectedItem = this.setSelectedItem(this.selectedItem + 1);
                }
            }
        }
        return setSelectedItem;
    }
    
    private void runOperation(final Operation operation, final boolean b) {
        if (!b && this.operationRunning) {
            this.operationQueue.add(operation);
        }
        else {
            this.operationRunning = true;
            switch (operation) {
                case MOVE_LEFT:
                    if (!this.moveSelectionLeftInternal()) {
                        this.runQueuedOperation();
                        break;
                    }
                    break;
                case MOVE_RIGHT:
                    if (!this.moveSelectionRightInternal()) {
                        this.runQueuedOperation();
                        break;
                    }
                    break;
                case EXECUTE:
                    if (!this.executeSelectedItemInternal()) {
                        this.runQueuedOperation();
                        break;
                    }
                    break;
                case KEY_DOWN:
                    if (!this.keyDownSelectedItemInternal()) {
                        this.runQueuedOperation();
                        break;
                    }
                    break;
                case KEY_UP:
                    if (!this.keyUpSelectedItemInternal()) {
                        this.runQueuedOperation();
                        break;
                    }
                    break;
            }
        }
    }
    
    private void runQueuedOperation() {
        if (this.operationQueue.size() > 0) {
            this.handler.post(new Runnable() {
                final /* synthetic */ Operation val$operation = ChoiceLayout.this.operationQueue.remove();
                
                @Override
                public void run() {
                    ChoiceLayout.this.runOperation(this.val$operation, true);
                }
            });
        }
        else {
            this.operationRunning = false;
        }
    }
    
    private void selectInitialItem(final int n) {
        if (n != -1) {
            final View child = this.choiceContainer.getChildAt(n);
            if (child.getMeasuredWidth() == 0) {
                child.getViewTreeObserver().addOnGlobalLayoutListener(this.animationLayoutListener);
            }
            else {
                final int n2 = (int)this.getXPositionForHighlight(child);
                ((FrameLayout.LayoutParams)this.highlightView.getLayoutParams()).width = Math.max(child.getMeasuredWidth() - (child.getPaddingLeft() + child.getPaddingRight()), ChoiceLayout.MIN_HIGHLIGHT_VIEW_WIDTH);
                this.highlightView.setX((float)n2);
                this.highlightView.setVisibility(this.highlightVisibility);
                this.highlightView.invalidate();
                this.highlightView.requestLayout();
                (this.currentSelectionView = child).setTag(n);
                if (this.currentMode == Mode.LABEL) {
                    ((TextView)this.currentSelectionView).setTextColor(ChoiceLayout.textSelectionColor);
                }
                else {
                    ((ImageView)this.currentSelectionView).setColorFilter(ChoiceLayout.textSelectionColor);
                }
                this.initialized = true;
            }
        }
    }
    
    public void clearOperationQueue() {
        if (this.animatorSet != null && this.animatorSet.isRunning()) {
            this.animatorSet.removeAllListeners();
            this.animatorSet.cancel();
            this.animatorSet = null;
        }
        this.operationQueue.clear();
        this.operationRunning = false;
    }
    
    public void executeSelectedItem(final boolean b) {
        this.runOperation(Operation.EXECUTE, false);
    }
    
    public List<Choice> getChoices() {
        return this.choices;
    }
    
    @Nullable
    public Choice getSelectedItem() {
        Choice choice;
        if (this.choices != null && this.selectedItem >= 0 && this.selectedItem < this.choices.size()) {
            choice = this.choices.get(this.selectedItem);
        }
        else {
            choice = null;
        }
        return choice;
    }
    
    public int getSelectedItemIndex() {
        return this.selectedItem;
    }
    
    public boolean hasChoiceId(final int n) {
        for (Choice choice : this.choices) {
            if (choice.id == n) {
                return true;
            }
        }
        return false;
    }
    
    protected boolean isHighlightPersistent() {
        return this.highlightPersistent;
    }
    
    public void keyDownSelectedItem() {
        this.runOperation(Operation.KEY_DOWN, false);
    }
    
    public void keyUpSelectedItem() {
        this.runOperation(Operation.KEY_UP, false);
    }
    
    public void moveSelectionLeft() {
        this.runOperation(Operation.MOVE_LEFT, false);
    }
    
    public void moveSelectionRight() {
        this.runOperation(Operation.MOVE_RIGHT, false);
    }
    
    public void resetAnimationElements() {
        if (this.highlightVisibility == 0 && this.highlightView != null) {
            this.highlightView.setVisibility(View.VISIBLE);
        }
        if (this.executeView != null) {
            if (this.executeView instanceof TextView) {
                ((TextView)this.executeView).setTextColor(ChoiceLayout.textSelectionColor);
                this.executeView = null;
            }
            else if (this.executeView instanceof ImageView) {
                ((ImageView)this.executeView).setColorFilter(ChoiceLayout.textSelectionColor);
                this.executeView = null;
            }
        }
    }
    
    public void setChoices(final Mode currentMode, final List<Choice> choices, final int selectedItem, final IListener listener) {
        this.currentMode = currentMode;
        this.choices = choices;
        this.listener = listener;
        this.initialized = false;
        final Operation operation = this.operationQueue.peek();
        this.executeView = null;
        this.clearOperationQueue();
        this.executing = false;
        this.selectedItem = -1;
        if (choices != null && choices.size() > 0) {
            if (selectedItem >= 0 && selectedItem < choices.size()) {
                this.selectedItem = selectedItem;
            }
            else {
                this.selectedItem = 0;
            }
        }
        this.buildChoices();
        this.selectInitialItem(this.selectedItem);
        if (operation == Operation.KEY_UP) {
            ((FrameLayout.LayoutParams)this.highlightView.getLayoutParams()).topMargin = ChoiceLayout.choiceHighlightMargin;
        }
    }
    
    public void setHighlightPersistent(final boolean highlightPersistent) {
        this.highlightPersistent = highlightPersistent;
    }
    
    public void setHighlightVisibility(final int highlightVisibility) {
        this.highlightVisibility = highlightVisibility;
        this.invalidate();
    }
    
    public void setItemPadding(final int n) {
        this.choiceTextPaddingLeft = n;
        this.choiceTextPaddingRight = n;
    }
    
    public void setLabelTextSize(final int choiceTextSize) {
        this.choiceTextSize = choiceTextSize;
    }
    
    protected boolean setSelectedItem(int selectedItem) {
        final boolean b = false;
        boolean b2;
        if (this.selectedItem == selectedItem) {
            b2 = b;
        }
        else {
            final int itemCount = this.getItemCount();
            b2 = b;
            if (selectedItem >= 0) {
                b2 = b;
                if (selectedItem < itemCount) {
                    this.selectedItem = selectedItem;
                    final View child = this.choiceContainer.getChildAt(selectedItem);
                    this.oldSelectionView = this.currentSelectionView;
                    (this.currentSelectionView = child).setTag(selectedItem);
                    selectedItem = Math.max(child.getMeasuredWidth(), ChoiceLayout.MIN_HIGHLIGHT_VIEW_WIDTH);
                    b2 = b;
                    if (selectedItem != 0) {
                        final int n = selectedItem - (child.getPaddingLeft() + child.getPaddingRight());
                        if (this.oldSelectionView != null) {
                            selectedItem = Math.max(this.oldSelectionView.getMeasuredWidth() - (this.oldSelectionView.getPaddingLeft() + this.oldSelectionView.getPaddingRight()), ChoiceLayout.MIN_HIGHLIGHT_VIEW_WIDTH);
                        }
                        else {
                            selectedItem = n;
                        }
                        this.animatorSet = new AnimatorSet();
                        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.highlightView, "x", this.getXPositionForHighlight(child));
                        final ValueAnimator ofInt = ValueAnimator.ofInt(selectedItem, n);
                        ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                                ((LayoutParams)ChoiceLayout.this.highlightView.getLayoutParams()).width = (int)valueAnimator.getAnimatedValue();
                                ChoiceLayout.this.highlightView.invalidate();
                                ChoiceLayout.this.highlightView.requestLayout();
                            }
                        });
                        this.highlightView.setVisibility(this.highlightVisibility);
                        this.highlightView.invalidate();
                        this.highlightView.requestLayout();
                        this.animatorSet.playTogether(ofFloat, ofInt);
                        this.animatorSet.setDuration(100L);
                        this.animatorSet.addListener(this.highlightViewListener);
                        this.animatorSet.start();
                        SoundUtils.playSound(SoundUtils.Sound.MENU_MOVE);
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    public static class Choice implements Parcelable
    {
        public static final Parcelable.Creator<Choice> CREATOR;
        public Icon icon;
        public int id;
        public String label;
        
        static {
            CREATOR = new Creator<Choice>() {
                public Choice createFromParcel(final Parcel parcel) {
                    return new Choice(parcel);
                }
                
                public Choice[] newArray(final int n) {
                    return new Choice[n];
                }
            };
        }
        
        public Choice(final Parcel parcel) {
            this.label = parcel.readString();
            if (TextUtils.isEmpty(this.label)) {
                this.label = null;
            }
            final int int1 = parcel.readInt();
            final int int2 = parcel.readInt();
            if (int1 != -1 && int2 != -1) {
                this.icon = new Icon(int1, int2);
            }
            this.id = parcel.readInt();
        }
        
        public Choice(final Icon icon, final int id) {
            this.icon = icon;
            this.id = id;
        }
        
        public Choice(final String label, final int id) {
            this.label = label;
            this.id = id;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, int n) {
            final int n2 = -1;
            String label;
            if (this.label != null) {
                label = this.label;
            }
            else {
                label = "";
            }
            parcel.writeString(label);
            if (this.icon != null) {
                n = this.icon.resIdSelected;
            }
            else {
                n = -1;
            }
            parcel.writeInt(n);
            n = n2;
            if (this.icon != null) {
                n = this.icon.resIdUnSelected;
            }
            parcel.writeInt(n);
            parcel.writeInt(this.id);
        }
    }
    
    public interface IListener
    {
        void executeItem(final int p0, final int p1);
        
        void itemSelected(final int p0, final int p1);
    }
    
    public static class Icon
    {
        public int resIdSelected;
        public int resIdUnSelected;
        
        public Icon(final int n) {
            this.resIdSelected = n;
            this.resIdUnSelected = n;
        }
        
        public Icon(final int resIdSelected, final int resIdUnSelected) {
            this.resIdSelected = resIdSelected;
            this.resIdUnSelected = resIdUnSelected;
        }
    }

    public enum Mode {
        LABEL(0),
        ICON(1);

        private int value;
        Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public enum Operation {
        MOVE_LEFT(0),
        MOVE_RIGHT(1),
        EXECUTE(2),
        KEY_DOWN(3),
        KEY_UP(4);

        private int value;
        Operation(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
