package com.navdy.hud.app.manager;

import com.navdy.hud.app.event.DriverProfileUpdated;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.analytics.RawSpeed;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import android.os.Looper;
import mortar.Mortar;
import com.navdy.hud.app.HudApplication;
import android.os.Handler;
import com.navdy.hud.app.profile.DriverProfileManager;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class SpeedManager
{
    private static final double EPSILON = 1.0E-5;
    private static final long GPS_SPEED_EXPIRY_INTERVAL = 5000L;
    private static final double MAX_VALID_METERS_PER_SEC = 89.41333333333333;
    private static final double METERS_PER_KILOMETER = 1000.0;
    private static final double METERS_PER_MILE = 1609.44;
    private static final int SECONDS_PER_HOUR = 3600;
    public static final int SPEED_NOT_AVAILABLE = -1;
    private static final Logger sLogger;
    private static final SpeedManager singleton;
    @Inject
    Bus bus;
    @Inject
    DriverProfileManager driverProfileManager;
    private volatile int gpsSpeed;
    private Runnable gpsSpeedExpiryRunnable;
    private Handler handler;
    private volatile int obdSpeed;
    private volatile float rawGpsSpeed;
    private volatile long rawGpsSpeedTimeStamp;
    private volatile int rawObdSpeed;
    private volatile long rawObdSpeedTimeStamp;
    private SpeedUnit speedUnit;
    
    static {
        sLogger = new Logger(SpeedManager.class);
        singleton = new SpeedManager();
    }
    
    public SpeedManager() {
        this.obdSpeed = -1;
        this.rawObdSpeed = -1;
        this.rawObdSpeedTimeStamp = 0L;
        this.rawGpsSpeed = -1.0f;
        this.rawGpsSpeedTimeStamp = 0L;
        this.speedUnit = SpeedUnit.MILES_PER_HOUR;
        Mortar.inject(HudApplication.getAppContext(), this);
        this.handler = new Handler(Looper.getMainLooper());
        this.bus.register(this);
        this.setUnit();
    }
    
    private static boolean almostEqual(final float n, final float n2) {
        return Math.abs(n - n2) < 1.0E-5;
    }
    
    public static int convert(final double n, final SpeedUnit speedUnit) {
        return convert(n, SpeedUnit.METERS_PER_SECOND, speedUnit);
    }
    
    public static int convert(final double n, final SpeedUnit currentSpeedUnit, final SpeedUnit desiredSpeedUnit) {
        return Math.round(convertWithPrecision(n, currentSpeedUnit, desiredSpeedUnit));
    }
    
    public static float convertWithPrecision(double n, final SpeedUnit currentSpeedUnit, final SpeedUnit desiredSpeedUnit) {
        float n2;
        if (n == 0.0 || n == -1.0) {
            n2 = (float)n;
        }
        else if (currentSpeedUnit == desiredSpeedUnit) {
            n2 = (float)n;
        }
        else {
            switch (currentSpeedUnit) {
                default:
                    n = 0.0;
                    break;
                case MILES_PER_HOUR:
                    n = n * 1609.44 / 3600.0;
                    break;
                case KILOMETERS_PER_HOUR:
                    n = n * 1000.0 / 3600.0;
                    break;
                case METERS_PER_SECOND:
                    break;
            }
            switch (desiredSpeedUnit) {
                default:
                    n2 = 0.0f;
                    break;
                case MILES_PER_HOUR:
                    n2 = (float)(n * 3600.0 / 1609.44);
                    break;
                case KILOMETERS_PER_HOUR:
                    n2 = (float)(n * 3600.0 / 1000.0);
                    break;
                case METERS_PER_SECOND:
                    n2 = (float)n;
                    break;
            }
        }
        return n2;
    }
    
    public static SpeedManager getInstance() {
        return SpeedManager.singleton;
    }
    
    private void setUnit() {
        SpeedUnit speedUnit;
        if (this.driverProfileManager.getCurrentProfile().getUnitSystem() == DriverProfilePreferences.UnitSystem.UNIT_SYSTEM_METRIC) {
            speedUnit = SpeedUnit.KILOMETERS_PER_HOUR;
        }
        else {
            speedUnit = SpeedUnit.MILES_PER_HOUR;
        }
        if (this.speedUnit != speedUnit) {
            SpeedManager.sLogger.i("ondriverprofileupdate speed unit has changed to " + speedUnit.name());
            this.setSpeedUnit(speedUnit);
            this.bus.post(new SpeedUnitChanged());
        }
        else {
            SpeedManager.sLogger.v("ondriverprofileupdated speed unit is still " + speedUnit.name());
        }
    }
    
    private boolean updateSpeed(final float rawGpsSpeed, final long n) {
        final boolean b = false;
        synchronized (this) {
            boolean b2;
            if (almostEqual(this.rawGpsSpeed, rawGpsSpeed)) {
                this.rawGpsSpeedTimeStamp = n;
                b2 = b;
            }
            else {
                b2 = b;
                if (rawGpsSpeed >= 0.0f) {
                    b2 = b;
                    if (rawGpsSpeed <= 89.41333333333333) {
                        this.rawGpsSpeedTimeStamp = n;
                        this.rawGpsSpeed = rawGpsSpeed;
                        this.gpsSpeed = convert(rawGpsSpeed, SpeedUnit.METERS_PER_SECOND, this.speedUnit);
                        b2 = true;
                    }
                }
            }
            return b2;
        }
    }
    
    public int getCurrentSpeed() {
        synchronized (this) {
            int n = this.getObdSpeed();
            if (n == -1) {
                n = this.getGpsSpeed();
            }
            return n;
        }
    }
    
    public RawSpeed getCurrentSpeedInMetersPerSecond() {
        synchronized (this) {
            RawSpeed rawSpeed;
            if (this.rawObdSpeed != -1) {
                rawSpeed = new RawSpeed(convertWithPrecision(this.rawObdSpeed, SpeedUnit.KILOMETERS_PER_HOUR, SpeedUnit.METERS_PER_SECOND), this.rawObdSpeedTimeStamp);
            }
            else {
                rawSpeed = new RawSpeed(this.rawGpsSpeed, this.rawGpsSpeedTimeStamp);
            }
            return rawSpeed;
        }
    }
    
    public int getGpsSpeed() {
        synchronized (this) {
            return this.gpsSpeed;
        }
    }
    
    public float getGpsSpeedInMetersPerSecond() {
        synchronized (this) {
            return this.rawGpsSpeed;
        }
    }
    
    public int getObdSpeed() {
        return this.obdSpeed;
    }
    
    public int getRawObdSpeed() {
        return this.rawObdSpeed;
    }
    
    public SpeedUnit getSpeedUnit() {
        return this.speedUnit;
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        this.setUnit();
    }
    
    @Subscribe
    public void onDriverProfileUpdated(final DriverProfileUpdated driverProfileUpdated) {
        if (driverProfileUpdated.state == DriverProfileUpdated.State.UPDATED) {
            this.setUnit();
        }
    }
    
    public boolean setGpsSpeed(final float n, final long n2) {
        synchronized (this) {
            if (this.gpsSpeedExpiryRunnable == null) {
                this.gpsSpeedExpiryRunnable = new Runnable() {
                    @Override
                    public void run() {
                        SpeedManager.sLogger.d("Gps RawSpeed data expired, resetting to zero");
                        SpeedManager.this.gpsSpeed = -1;
                        HudApplication.getApplication().getBus().post(new SpeedDataExpired());
                    }
                };
            }
            this.handler.removeCallbacks(this.gpsSpeedExpiryRunnable);
            this.handler.postDelayed(this.gpsSpeedExpiryRunnable, 5000L);
            return this.updateSpeed(n, n2);
        }
    }

    public synchronized void setObdSpeed(int kph, long timeStamp) {
        if (kph < -1 || kph > 256) {
            sLogger.w("Invalid OBD speed ignored:" + kph);
        } else {
            this.rawObdSpeed = kph;
            this.rawObdSpeedTimeStamp = timeStamp;
            this.obdSpeed = convert((double) kph, SpeedUnit.KILOMETERS_PER_HOUR, this.speedUnit);
        }
    }

    
    public void setSpeedUnit(final SpeedUnit speedUnit) {
        if (speedUnit != this.speedUnit) {
            this.gpsSpeed = convert(this.gpsSpeed, this.speedUnit, speedUnit);
            this.obdSpeed = convert(this.obdSpeed, this.speedUnit, speedUnit);
            this.speedUnit = speedUnit;
        }
    }
    
    public static class SpeedDataExpired
    {
        public SpeedDataSource source;
        
        public SpeedDataExpired() {
            this.source = SpeedDataSource.GPS;
        }
    }

    public enum SpeedDataSource {
        GPS(0),
        OBD(1);

        private int value;
        SpeedDataSource(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public enum SpeedUnit {
        MILES_PER_HOUR(0),
        KILOMETERS_PER_HOUR(1),
        METERS_PER_SECOND(2);

        private int value;
        SpeedUnit(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public static class SpeedUnitChanged
    {
    }
}
