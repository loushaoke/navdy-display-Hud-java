package com.navdy.hud.app.util;


public enum CrashReportService$CrashType {
    BLUETOOTH_DISCONNECTED(0),
    OBD_RESET(1),
    GENERIC_NON_FATAL_CRASH(2);

    private int value;
    CrashReportService$CrashType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class CrashReportService$CrashType extends Enum {
//    final private static com.navdy.hud.app.util.CrashReportService$CrashType[] $VALUES;
//    final public static com.navdy.hud.app.util.CrashReportService$CrashType BLUETOOTH_DISCONNECTED;
//    final public static com.navdy.hud.app.util.CrashReportService$CrashType GENERIC_NON_FATAL_CRASH;
//    final public static com.navdy.hud.app.util.CrashReportService$CrashType OBD_RESET;
//    
//    static {
//        BLUETOOTH_DISCONNECTED = new com.navdy.hud.app.util.CrashReportService$CrashType("BLUETOOTH_DISCONNECTED", 0);
//        OBD_RESET = new com.navdy.hud.app.util.CrashReportService$CrashType("OBD_RESET", 1);
//        GENERIC_NON_FATAL_CRASH = new com.navdy.hud.app.util.CrashReportService$CrashType("GENERIC_NON_FATAL_CRASH", 2);
//        com.navdy.hud.app.util.CrashReportService$CrashType[] a = new com.navdy.hud.app.util.CrashReportService$CrashType[3];
//        a[0] = BLUETOOTH_DISCONNECTED;
//        a[1] = OBD_RESET;
//        a[2] = GENERIC_NON_FATAL_CRASH;
//        $VALUES = a;
//    }
//    
//    private CrashReportService$CrashType(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.util.CrashReportService$CrashType valueOf(String s) {
//        return (com.navdy.hud.app.util.CrashReportService$CrashType)Enum.valueOf(com.navdy.hud.app.util.CrashReportService$CrashType.class, s);
//    }
//    
//    public static com.navdy.hud.app.util.CrashReportService$CrashType[] values() {
//        return $VALUES.clone();
//    }
//}
//