package com.navdy.hud.app.util;

import static com.navdy.hud.app.BuildConfig.VERSION_NAME;

public class DeviceUtil {
    private final static String BUILD_TYPE_USER = "user";
    final public static String TELEMETRY_FILE_NAME = "telemetry";
    private static boolean hudDevice;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.DeviceUtil.class);
        boolean b = android.os.Build.MODEL.equalsIgnoreCase("NAVDY_HUD-MX6DL");
        label2: {
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!android.os.Build.MODEL.equalsIgnoreCase("Display")) {
                        break label0;
                    }
                }
                hudDevice = true;
                sLogger.i("Running on Navdy Device:" + android.os.Build.MODEL);
                break label2;
            }
            sLogger.i("Not running on Navdy Device:" + android.os.Build.MODEL);
        }
    }
    
    public DeviceUtil() {
    }
    
    public static void copyHEREMapsDataInfo(String s) {
        java.io.File a = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
        boolean b = a.exists();
        label0: {
            label1: if (b) {
                java.io.File a0 = new java.io.File(a, "meta.json");
                if (a0.exists()) {
                    String s0 = s + java.io.File.separator + "HERE_meta.json";
                    try {
                        com.navdy.service.library.util.IOUtils.copyFile(a0.getAbsolutePath(), s0);
                    } catch(java.io.IOException ignoredException) {
                        break label1;
                    }
                    break label0;
                } else {
                    sLogger.d("Meta json file not found");
                    break label0;
                }
            } else {
                sLogger.d("Here maps data directory on the maps partition does not exists");
                break label0;
            }
            sLogger.e("Error copying the HERE maps data meta json file");
        }
    }
    
    public static String getCurrentHereSdkTimestamp() {
        return /* folderName  */ "00a584764c918d799f376cc41e46674ba558f8b4";
    }
    
    public static String getCurrentHereSdkVersion() {
        return com.here.android.mpa.common.Version.getSdkVersion();
    }
    
    public static String getHEREMapsDataInfo() {
        java.io.FileInputStream inputStream = null;
        java.io.File a1 = new java.io.File(com.navdy.hud.app.storage.PathManager.getInstance().getHereMapsDataDirectory());
        boolean b = a1.exists();
        String s;
        label2:
        {
            java.io.IOException a3;
            label9:
            {
                java.io.IOException a4;
                label3:
                {
                    if (b) {
                        java.io.File a5 = new java.io.File(a1, "meta.json");
                        if (a5.exists()) {
                            label6:
                            {
                                Exception a6;
                                label8:
                                {
                                    label7:
                                    {
                                        try {
                                            inputStream = new java.io.FileInputStream(a5);
                                            break label7;
                                        } catch (Exception a7) {
                                            a6 = a7;
                                        }
                                        break label8;
                                    }
                                    try {
                                        s = com.navdy.service.library.util.IOUtils.convertInputStreamToString(inputStream, "UTF-8");
                                        break label6;
                                    } catch (Exception a8) {
                                        a6 = a8;
                                    }

                                }
                                sLogger.e("Error reading the Meta data,", a6);
                                break label9;
                            }
                            try {
                                inputStream.close();
                            } catch (java.io.IOException a12) {
                                a4 = a12;
                                break label3;
                            }
                            break label2;
                        } else {
                            sLogger.d("Meta json file not found");
                            s = null;
                            break label2;
                        }
                    } else {
                        sLogger.d("Here maps data directory on the maps partition does not exists");
                        s = null;
                        break label2;
                    }
                }
                sLogger.e("Error closing the FileInputStream", a4);
                break label2;
            }
            s = null;
            if (inputStream == null) {
                break label2;
            }
            label1:
            {
                try {
                    inputStream.close();
                } catch (java.io.IOException a13) {
                    a3 = a13;
                    break label1;
                }
                s = null;
                break label2;
            }
            sLogger.e("Error closing the FileInputStream", a3);
            s = null;
        }
        return s;
    }
    
    public static boolean isNavdyDevice() {
        return hudDevice;
    }

    static boolean isTaggedRelease() {
        return ! java.util.regex.Pattern.compile(".*-[0-9]+-g.*").matcher(VERSION_NAME).matches();
    }

    public static boolean isUserBuild() {
        return BUILD_TYPE_USER.equals(android.os.Build.TYPE);
    }

    /* Deprecated Method
    public static boolean supportsCamera() {
        android.hardware.Camera camera = null;
        int i = android.hardware.Camera.getNumberOfCameras();
        sLogger.v("Found " + i + " cameras");
        Boolean hasCamera;
        try {
            camera = android.hardware.Camera.open(0);
            hasCamera = Boolean.TRUE;
        } catch(Exception a4) {
            sLogger.e("Failed to open camera", a4);
            hasCamera = Boolean.FALSE;
        }
        if (camera != null) {
            camera.release();
        }
        return hasCamera;
    }
*/

    public static void takeDeviceScreenShot(String s) {
        label0: {
            java.io.IOException a;
            try {
                try {
                    Runtime.getRuntime().exec("screencap -p " + s).waitFor();
                    break label0;
                } catch(java.io.IOException a0) {
                    a = a0;
                }
            } catch(InterruptedException a1) {
                sLogger.e("Error while taking screen shot", a1);
                break label0;
            }
            sLogger.e("Error while taking screen shot", a);
        }
    }
}
