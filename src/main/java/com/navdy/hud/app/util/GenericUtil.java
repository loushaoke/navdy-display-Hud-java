package com.navdy.hud.app.util;

import com.navdy.hud.app.R;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.text.TextUtils;
import android.content.Context;
import java.lang.reflect.Method;
import com.navdy.hud.app.HudApplication;
import android.view.inputmethod.InputMethodManager;
import android.os.Looper;
import com.navdy.service.library.log.Logger;

public class GenericUtil
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(GenericUtil.class);
    }
    
    public static void checkNotOnMainThread() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new RuntimeException("cannot call on mainthread");
        }
    }
    
    public static void clearInputMethodManagerFocusLeak() {
        final InputMethodManager inputMethodManager = (InputMethodManager)HudApplication.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            final Method declaredMethod = InputMethodManager.class.getDeclaredMethod("finishInputLocked", (Class<?>[])new Class[0]);
            declaredMethod.setAccessible(true);
            GenericUtil.sLogger.v("calling finishInputLocked");
            declaredMethod.invoke(inputMethodManager);
            GenericUtil.sLogger.v("called finishInputLocked");
        }
        catch (Throwable t) {
            GenericUtil.sLogger.v("finishInputLocked", t);
        }
    }
    
    public static int getDrawableResourceIdFromName(final String s) {
        final Context appContext = HudApplication.getAppContext();
        return appContext.getResources().getIdentifier(s, "drawable", appContext.getPackageName());
    }
    
    public static String getErrorMessage(final Throwable t) {
        String s;
        if (t == null) {
            s = HudApplication.getAppContext().getResources().getString(R.string.operation_failed);
        }
        else if (TextUtils.isEmpty(s = t.getMessage())) {
            s = t.toString();
        }
        return s;
    }
    
    public static int getIdsResourceIdFromName(final String s) {
        final Context appContext = HudApplication.getAppContext();
        return appContext.getResources().getIdentifier(s, "id", appContext.getPackageName());
    }
    
    public static int getStringResourceIdFromName(final String s) {
        final Context appContext = HudApplication.getAppContext();
        return appContext.getResources().getIdentifier(s, "string", appContext.getPackageName());
    }
    
    public static int indexOf(final byte[] array, final byte[] array2, int i, int n) {
        if (array2.length == 0 || n - i + 1 < array2.length) {
            n = -1;
        }
        else {
            int n2 = array.length - array2.length;
            if (n < n2) {
                n2 = n;
            }
            while (i <= n2) {
                final boolean b = true;
                n = 0;
                boolean b2;
                while (true) {
                    b2 = b;
                    if (n >= array2.length) {
                        break;
                    }
                    if (array[i + n] != array2[n]) {
                        b2 = false;
                        break;
                    }
                    ++n;
                }
                n = i;
                if (b2) {
                    return n;
                }
                ++i;
            }
            n = -1;
        }
        return n;
    }
    
    public static boolean isClientiOS() {
        final DeviceInfo remoteDeviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        return remoteDeviceInfo != null && remoteDeviceInfo.platform == DeviceInfo.Platform.PLATFORM_iOS;
    }
    
    public static boolean isMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
    
    public static String normalizeToFilename(String replaceAll) {
        if (!TextUtils.isEmpty(replaceAll)) {
            replaceAll = replaceAll.replaceAll("[^a-zA-Z0-9._]+", "");
        }
        return replaceAll;
    }
    
    public static String removePunctuation(String replaceAll) {
        if (!TextUtils.isEmpty(replaceAll)) {
            replaceAll = replaceAll.replaceAll("\\p{Punct}+", " ");
        }
        return replaceAll;
    }
    
    public static void sleep(final int n) {

//        final long n2 = n;
//        try {
//            Thread.sleep(n2);
//        }

        try {
            Thread.sleep((long) n);
        }
        catch (Throwable ignored) {}
    }
}
