package com.navdy.hud.app.util;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.framework.network.NetworkBandwidthController$Component;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.util.OTAUpdateService.OTAFailedException;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.LogUtils;
import java.io.File;
import java.io.FilenameFilter;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.CrashManagerListener;
import net.hockeyapp.android.ExceptionHandler;
import net.hockeyapp.android.metrics.MetricsManager;

import java.lang.String;
import java.security.SecureRandom;

public final class CrashReporter {
    private static final int ANR_LOG_LIMIT = 32768;
    private static final String ANR_PATH = "/data/anr/traces.txt";
    // keep it all the same for now for release, then change it depending on git tag status
    private static final String HOCKEY_APP_ID = "94c05da80f9f4219a02cad7b7604b330";
    private static final String HOCKEY_APP_DEVELOP_ID = "49b87814edb942dea24362a20278abe4";
    private static final int KERNEL_CRASH_INFO_LIMIT = 32768;
    private static final String LOG_ANR_MARKER = "Wrote stack traces to '/data/anr/traces.txt'";
    private static final String LOG_BEGIN_MARKER = "--------- beginning of main";
    private static final int PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL = ((int) TimeUnit.MINUTES.toMillis(2));
    private static final int PERIOD_DELIVERY_CHECK_INTERVAL = ((int) TimeUnit.MINUTES.toMillis(5));
    private static final Pattern START_OF_OOPS = Pattern.compile("(Unable to handle)|(Internal error:)");
    private static final int SYSTEM_LOG_LIMIT = 32768;
    private static final String TOMBSTONE_FILE_PATTERN = "tombstone_light_";
    private static final String TOMBSTONE_PATH = "/data/tombstones";
    private static final Pattern USER_REBOOT = Pattern.compile("(reboot: Restarting system with command)|(do_powerctl:)");
    private static DriverProfileManager driverProfileManager;
    private static final CrashReporter sInstance = new CrashReporter();
    private static final Logger sLogger = new Logger(CrashReporter.class);
    private static volatile boolean stopCrashReporting;

    private static final String userId; // Build.SERIAL;
    static {
        String _userId = SystemProperties.get("persist.sys.userId", null);
        if (_userId == null || _userId.equals("")) {
            _userId = UUID.randomUUID().toString();
            SystemProperties.set("persist.sys.userId", _userId);
        }
        userId = _userId;
    }

    private NavdyCrashListener crashManagerListener = new NavdyCrashListener();

    private Runnable deliveryRunnable = (Runnable) new Runnable() {
        public void run() {
            label0: {
                Throwable a;
                label1: {
                    boolean b;
                    try {
                        b = CrashReporter.this.checkForCrashes();
                    } catch(Throwable a0) {
                        a = a0;
                        break label1;
                    }
                    if (!b) {
                        break label0;
                    }
                    CrashReporter.this.handler.postDelayed(CrashReporter.this.periodicRunnable, (long) PERIOD_DELIVERY_CHECK_INTERVAL);
                    break label0;
                }
                try {
                    sLogger.e(a);
                } catch(Throwable a1) {
                    CrashReporter.this.handler.postDelayed(CrashReporter.this.periodicRunnable, (long) PERIOD_DELIVERY_CHECK_INTERVAL);
                    throw a1;
                }
                CrashReporter.this.handler.postDelayed(CrashReporter.this.periodicRunnable, (long) PERIOD_DELIVERY_CHECK_INTERVAL);
            }
        }
    };

    private Handler handler = new Handler(Looper.getMainLooper());

    private boolean installed;
    private OTAFailedException otaFailure = null;
    private Runnable periodicRunnable = new Runnable() {
        public void run() {
            TaskManager.getInstance().execute(CrashReporter.this.deliveryRunnable, 1);
        }
    };

    static class AnrException extends Exception {
        AnrException(String str) {
            super(str);
        }
    }

    public static class CrashException extends Exception {
        CrashException(String detailMessage) {
            super(detailMessage);
        }

        public Pattern getLocationPattern() {
            return null;
        }

        public String filter(String msg) {
            return msg;
        }

        public String getCrashLocation() {
            return extractCrashLocation(getLocalizedMessage());
        }

        public String toString() {
            String name = getClass().getSimpleName();
            String msg = getLocalizedMessage();
            return name + ": " + getCrashLocation() + (msg != null ? filter(msg) : "");
        }

        String extractCrashLocation(String msg) {
            String crashLocation = "";
            Pattern pattern = getLocationPattern();
            if (pattern == null || msg == null) {
                return crashLocation;
            }
            Matcher match = pattern.matcher(msg);
            if (!match.find()) {
                return crashLocation;
            }
            if (match.groupCount() < 1) {
                return match.group();
            }
            for (int i = 1; i <= match.groupCount(); i++) {
                if (match.group(i) != null) {
                    return match.group(i);
                }
            }
            return crashLocation;
        }
    }

    public static class KernelCrashException extends CrashException {
        private static final Pattern stackMatcher = Pattern.compile("PC is at\\s+([^\n]+\n)");

        KernelCrashException(String str) {
            super(str);
        }

        public Pattern getLocationPattern() {
            return stackMatcher;
        }
    }

    public static String getUserID() {
        return CrashReporter.userId;
    }

    public static class NavdyCrashListener extends CrashManagerListener {
        public Throwable exception = null;

        public boolean shouldAutoUploadCrashes() {
            return true;
        }

        public String getUserID() {
            return CrashReporter.userId;
        }

        public String getContact() {
            return CrashReporter.driverProfileManager.getLastUserEmail();
        }

        public String getDescription() {
            if (this.exception instanceof TombstoneException) {
                return ((TombstoneException)this.exception).description;
            }
            if (this.exception instanceof AnrException) {
                return LogUtils.systemLogStr(ANR_LOG_LIMIT, CrashReporter.LOG_ANR_MARKER);
            }
            if (this.exception instanceof KernelCrashException) {
                return LogUtils.systemLogStr(KERNEL_CRASH_INFO_LIMIT, CrashReporter.LOG_BEGIN_MARKER);
            }
            if (!(this.exception instanceof OTAFailedException)) {
                return LogUtils.systemLogStr(SYSTEM_LOG_LIMIT, null);
            }
            OTAFailedException e = (OTAFailedException) this.exception;
            return e.last_install + GlanceConstants.NEWLINE + e.last_log;
        }

        public void saveException(Throwable t) {
            this.exception = t;
            ExceptionHandler.saveException(t, Thread.currentThread(), this);
            this.exception = null;
        }
    }

    public static class TombstoneException extends CrashException {
        private static final Pattern stackMatcher = Pattern.compile("Abort message: ('[^']+'\n)|#00 pc \\w+\\s+([^\n]+\n)");
        public final String description;

        TombstoneException(String str) {
            super("");
            this.description = str;
        }

        public String getCrashLocation() {
            return extractCrashLocation(this.description);
        }

        public Pattern getLocationPattern() {
            return stackMatcher;
        }
    }

    public static boolean isEnabled() {
        return DeviceUtil.isNavdyDevice() && !HudApplication.isDeveloperBuild();
    }

    public static CrashReporter getInstance() {
        return sInstance;
    }

    private CrashReporter() {
        this.handler.postDelayed(this.periodicRunnable, (long) PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL);
        driverProfileManager = DriverProfileHelper.getInstance().getDriverProfileManager();
    }

    public synchronized void installCrashHandler(Context context) {
        if (!this.installed) {
            UncaughtExceptionHandler defaultUncaughtHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (defaultUncaughtHandler != null) {
                sLogger.v("default uncaught handler:" + defaultUncaughtHandler.getClass().getName());
            } else {
                sLogger.v("default uncaught handler is null");
            }
//            setHockeyAppCrashUploading(false);

            String appId = DeviceUtil.isTaggedRelease() ? HOCKEY_APP_ID : HOCKEY_APP_DEVELOP_ID;
            CrashManager.register(context, appId, this.crashManagerListener);
            MetricsManager.register(HudApplication.getApplication(), appId);
            MetricsManager.trackEvent("Startup");
            sLogger.v("enabled metrics manager");
//            setHockeyAppCrashUploading(true);
            final UncaughtExceptionHandler hockeyUncaughtHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (hockeyUncaughtHandler != null) {
                sLogger.v("hockey uncaught handler:" + hockeyUncaughtHandler.getClass().getName());
            } else {
                sLogger.v("default hockey handler is null");
            }
            Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable exception) {
                    CrashReporter.this.handleUncaughtException(thread, exception, hockeyUncaughtHandler);
                }
            });
            this.installed = true;
        }
    }

    public void reportNonFatalException(Throwable throwable) {
        if (this.installed) {
            try {
                saveHockeyAppException(throwable);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public void log(String msg) {
    }

    private void handleUncaughtException(Thread thread, Throwable exception, UncaughtExceptionHandler exceptionHandler) {
        if (stopCrashReporting) {
            sLogger.i("FATAL-reporting-turned-off", exception);
            return;
        }
        stopCrashReporting = true;
        String tag = "FATAL-CRASH";
        Log.e(tag, tag + " Uncaught exception - " + thread.getName() + GlanceConstants.COLON_SEPARATOR + thread.getId() + " ui thread id:" + Looper.getMainLooper().getThread().getId(), exception);
        sLogger.i("closing logger");
        Logger.close();
        if (exceptionHandler != null) {
            exceptionHandler.uncaughtException(thread, exception);
        }
    }

    public void checkForKernelCrashes(final String[] kernelCrashFiles) {
        if (kernelCrashFiles != null && kernelCrashFiles.length != 0) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    try {
                        CrashReporter.sLogger.v("checking for kernel crashes");
                        File file = null;
                        for (String str2 : kernelCrashFiles) {
                            File f = new File(str2);
                            if (f.exists()) {
                                file = f;
                                CrashReporter.sLogger.v("found file:" + f.getAbsolutePath());
                                break;
                            }
                        }
                        if (file != null) {
                            String str = CrashReporter.this.filterKernelCrash(file);
                            if (!TextUtils.isEmpty(str)) {
                                CrashReporter.this.reportNonFatalException(new KernelCrashException(str));
                                CrashReporter.sLogger.v("kernel crash created");
                            }
                            CrashReporter.this.cleanupKernelCrashes(kernelCrashFiles);
                            return;
                        }
                        CrashReporter.sLogger.v("no kernel crash files");
                    } catch (Throwable t) {
                        CrashReporter.sLogger.e(t);
                    }
                }
            }, 1);
        }
    }

    private String filterKernelCrash(File file) {
        try {
            String str = IOUtils.convertFileToString(file.getAbsolutePath());
            if (str == null) {
                return null;
            }
            if (USER_REBOOT.matcher(str).find()) {
                sLogger.v("Ignoring user reboot log");
                return null;
            }
            Matcher match = START_OF_OOPS.matcher(str);
            int start = str.length() - KERNEL_CRASH_INFO_LIMIT;
            if (match.find() && match.start() < start) {
                start = match.start();
            }
            start = Math.max(0, start);
            return str.substring(start, Math.min(str.length() - start, KERNEL_CRASH_INFO_LIMIT));
        } catch (Throwable t) {
            sLogger.e(t);
            return null;
        }
    }

    private void cleanupKernelCrashes(String[] kernelCrashFiles) {
        for (String crashFile : kernelCrashFiles) {
            File f = new File(crashFile);
            if (f.exists()) {
                sLogger.v("Trying to delete " + crashFile);
                if (!f.delete()) {
                    sLogger.e("Unable to delete kernel crash file: " + crashFile);
                }
            }
        }
    }

    public void checkForTombstones() {
        GenericUtil.checkNotOnMainThread();
        try {
            sLogger.v("checking for tombstones");
            File f = new File(TOMBSTONE_PATH);
            if (f.exists() && f.isDirectory()) {
                String[] list = f.list(new FilenameFilter() {
                    public boolean accept(File dir, String filename) {
                        return filename.contains(CrashReporter.TOMBSTONE_FILE_PATTERN);
                    }
                });
                if (list == null || list.length == 0) {
                    sLogger.v("no tombstones");
                    return;
                }
                for (String str : list) {
                    File tombstoneFile = new File(TOMBSTONE_PATH + File.separator + str);
                    if (tombstoneFile.exists()) {
                        String fileName = tombstoneFile.getAbsolutePath();
                        String str2 = IOUtils.convertFileToString(fileName);
                        if (!TextUtils.isEmpty(str2)) {
                            reportNonFatalException(new TombstoneException(str2));
                            sLogger.v("tombstone exception logged file[" + fileName + "] size[" + str2.length() + "]");
                        }
                        sLogger.v("tombstone deleted: " + tombstoneFile.delete());
                    }
                }
                return;
            }
            sLogger.v("no tombstones");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void stopCrashReporting(boolean b) {
        setCrashReporting(b);
    }

    private static void setCrashReporting(boolean b) {
        stopCrashReporting = b;
    }

    private boolean checkForCrashes() {
        if (NetworkBandwidthController.getInstance().isNetworkAccessAllowed(NetworkBandwidthController$Component.HOCKEY)) {
            try {
                CrashManager.submitStackTraces(new WeakReference(HudApplication.getAppContext()), this.crashManagerListener);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
        return true;
    }

    public void checkForAnr() {
        GenericUtil.checkNotOnMainThread();
        try {
            sLogger.v("checking for anr's");
            if (new File(ANR_PATH).exists()) {
                String str = IOUtils.convertFileToString(ANR_PATH);
                if (!TextUtils.isEmpty(str)) {
                    reportNonFatalException(new AnrException(str));
                    sLogger.v("anr crash created size[" + str.length() + "]");
                }
                cleanupAnrFiles();
                return;
            }
            sLogger.v("no anr files");
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public synchronized void reportOTAFailure(OTAFailedException failure) {
        if (this.installed) {
            reportNonFatalException(failure);
        } else {
            if (this.otaFailure != null) {
                sLogger.e("multiple OTAFailedException reports");
            }
            this.otaFailure = failure;
        }
    }

    public synchronized void checkForOTAFailure() {
        if (this.otaFailure != null) {
            reportNonFatalException(this.otaFailure);
            this.otaFailure = null;
        }
    }

    private void cleanupAnrFiles() {
        File f = new File(ANR_PATH);
        if (f.exists()) {
            sLogger.v("delete /data/anr/traces.txt :" + f.delete());
        }
    }

    private void saveHockeyAppException(final Throwable throwable) {
        try {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    try {
                        CrashReporter.this.crashManagerListener.saveException(throwable);
                    } catch (Throwable t) {
                        CrashReporter.sLogger.e(t);
                    }
                }
            }, 1);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public boolean isInstalled() {
        return this.installed;
    }

    private void setHockeyAppCrashUploading(boolean enable) {
        try {
            sLogger.v("setHockeyAppCrashUploading: try to enabled:" + enable);
            Field f = CrashManager.class.getDeclaredField("submitting");
            f.setAccessible(true);
            f.set(null, !enable);
            sLogger.v("setHockeyAppCrashUploading: enabled=" + enable);
        } catch (Throwable t) {
            sLogger.e("setHockeyAppCrashUploading", t);
        }
    }
}
