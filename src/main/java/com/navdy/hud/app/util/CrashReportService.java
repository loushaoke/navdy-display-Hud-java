package com.navdy.hud.app.util;

public class CrashReportService extends android.app.IntentService {
    final private static String ACTION_ADD_REPORT = "AddReport";
    final private static String ACTION_DUMP_CRASH_REPORT = "DumpCrashReport";
    private static String CRASH_INFO_TEXT_FILE_NAME;
    private static String CRASH_LOG_TEXT_FILE_NAME;
    final private static java.text.SimpleDateFormat DATE_FORMAT;
    final public static String EXTRA_CRASH_TYPE = "EXTRA_CRASH_TYPE";
    final public static String EXTRA_FILE_PATH = "EXTRA_FILE_PATH";
    private static int LOG_FILE_SIZE = 0;
    final private static int MAX_NON_FATAL_CRASH_REPORTS_OUT_STANDING = 10;
    final private static java.text.SimpleDateFormat TIME_FORMAT_FOR_REPORT;
    private static boolean mIsInitialized;
    private static String sCrashReportsFolder;
    private final static java.util.concurrent.PriorityBlockingQueue sCrashReportsToSend;
    private static com.navdy.service.library.log.Logger sLogger;
    private static int sOutStandingCrashReportsToBeCleared;
    
    static {
        sCrashReportsToSend = new java.util.concurrent.PriorityBlockingQueue(10, new CrashReportService$FilesModifiedTimeComparator());
        sOutStandingCrashReportsToBeCleared = 0;
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.CrashReportService.class);
        CRASH_INFO_TEXT_FILE_NAME = "info.txt";
        CRASH_LOG_TEXT_FILE_NAME = "log.txt";
        LOG_FILE_SIZE = 51200;
        mIsInitialized = false;
        DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH:mm:ss.SSS", java.util.Locale.US);
        TIME_FORMAT_FOR_REPORT = new java.text.SimpleDateFormat("dd MM yyyy' 'HH:mm:ss.SSS", java.util.Locale.US);
    }
    
    public CrashReportService() {
        super("CrashReportService");
    }
    
    private void addReport(java.io.File a) {
        synchronized(sCrashReportsToSend) {
            sCrashReportsToSend.add(a);
            if (sCrashReportsToSend.size() > 10) {
                if ((java.io.File)sCrashReportsToSend.poll() != null) {
                    com.navdy.service.library.util.IOUtils.deleteFile(
                            com.navdy.hud.app.HudApplication.getAppContext(),
                            ((java.io.File)sCrashReportsToSend.poll()).getAbsolutePath()
                    );
                }
                if (sOutStandingCrashReportsToBeCleared > 0) {
                    sOutStandingCrashReportsToBeCleared = sOutStandingCrashReportsToBeCleared - 1;
                }
            }
        }
    }
    
    public static void addSnapshotAsync(String s) {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.CrashReportService.class);
        a.setAction("AddReport");
        a.putExtra(EXTRA_FILE_PATH, s);
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    public static void clearCrashReports() {
        synchronized(sCrashReportsToSend) {
            while(sOutStandingCrashReportsToBeCleared > 0) {
                java.io.File a0 = (java.io.File)sCrashReportsToSend.poll();
                if (a0 != null) {
                    com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a0.getAbsolutePath());
                    sOutStandingCrashReportsToBeCleared = sOutStandingCrashReportsToBeCleared - 1;
                }
            }
            /*monexit(a)*/;
        }
    }
    
    public static void compressCrashReportsToZip(java.io.File[] a, String s) {
        Throwable a0 = null;
        java.io.File a1 = new java.io.File(s);
        if (a1.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), s);
        }
        try {
            a1.createNewFile();
        } catch(java.io.IOException a2) {
            sLogger.e("Failed to create new file at the specified output file path " + s, (Throwable)a2);
        }
        int i = (a == null) ? 0 : (a.length <= 0) ? 0 : a.length;
        label0: synchronized(sCrashReportsToSend) {
            java.io.File[] a5 = null;
            int i0 = 0;
            label1: {
                int i1 = 0;
                int i2 = 0;
                try {
                    a5 = new java.io.File[i + sCrashReportsToSend.size()];
                    if (a == null) {
                        i0 = 0;
                        break label1;
                    }
                    i1 = a.length;
                    i2 = 0;
                    i0 = 0;
                } catch(Throwable a6) {
                    break label0;
                }
                while(i2 < i1) {
                    a5[i0] = a[i2];
                    i2 = i2 + 1;
                    i0 = i0 + 1;
                }
            }
            //try {
                sOutStandingCrashReportsToBeCleared = sCrashReportsToSend.size();
                Object a7 = sCrashReportsToSend.iterator();
                while(((java.util.Iterator)a7).hasNext()) {
                    a5[i0] = (java.io.File)((java.util.Iterator)a7).next();
                    i0 = i0 + 1;
                }
                /*monexit(a3)*/;
            //} catch(Throwable a8) {
            //    a0 = a8;
            //    break label0;
            //}
            com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.hud.app.HudApplication.getAppContext(), a5, s);
        }
    }
    
    private void dumpCrashReport(String s) {
        StringBuilder a = new StringBuilder();
        java.util.Date a0 = new java.util.Date(System.currentTimeMillis());
        String s0 = TIME_FORMAT_FOR_REPORT.format(a0);
        java.io.File a1 = new java.io.File(sCrashReportsFolder + java.io.File.separator + CRASH_INFO_TEXT_FILE_NAME);
        if (a1.exists()) {
            com.navdy.service.library.util.IOUtils.deleteFile((android.content.Context)this, a1.getAbsolutePath());
        }
        java.io.File a2 = a1.getParentFile();
        if (!a2.exists()) {
            a2.mkdirs();
        }
        label0: {
            java.io.FileWriter a3 = null;
            Throwable a4 = null;
            label2: {
                label4: {
                    try {
                        a1.createNewFile();
                        a3 = new java.io.FileWriter(a1);
                        break label4;
                    } catch(Throwable a5) {
                        a4 = a5;
                    }
                    a3 = null;
                    break label2;
                }
                label1: {
                    {
                        java.io.File a6 = null;
                        java.io.File[] a7 = null;
                        java.io.File[] a8 = null;
                        try {
                            a3.write("Crash Type : " + s + "\n");
                            a3.write("Report Time : " + s0 + " , " + a.toString() + "\n");
                            a3.write(com.navdy.hud.app.util.os.PropsFileUpdater.readProps());
                            com.navdy.service.library.events.DeviceInfo a9 = com.navdy.service.library.device.RemoteDevice.sLastSeenDeviceInfo.getDeviceInfo();
                            if (a9 != null) {
                                a3.write(com.navdy.hud.app.util.CrashReportService.printDeviceInfo(a9));
                            }
                            a3.flush();
                            a3.close();
                            a6 = new java.io.File(sCrashReportsFolder + java.io.File.separator + "temp");
                            if (a6.exists()) {
                                com.navdy.service.library.util.IOUtils.deleteDirectory((android.content.Context)this, a6);
                            }
                            a6.mkdirs();
                            com.navdy.service.library.util.LogUtils.copySnapshotSystemLogs(a6.getAbsolutePath());
                            a7 = a6.listFiles();
                        } catch(Throwable a10) {
                            a4 = a10;
                            break label2;
                        }
                        label3: {
                            int i = 0;
                            int i0 = 0;
                            int i1 = 0;
                            try {
                                a8 = new java.io.File[(a7 == null) ? 1 : a7.length + 1];
                                a8[0] = a1;
                                if (a7 == null) {
                                    break label3;
                                }
                                i = a7.length;
                                i0 = 0;
                                i1 = 1;
                            } catch(NegativeArraySizeException | NullPointerException | OutOfMemoryError a11) {
                                a4 = a11;
                                break label2;
                            }
                            while(i0 < i) {
                                a8[i1] = a7[i0];
                                i0 = i0 + 1;
                                i1 = i1 + 1;
                            }
                        }
                        try {
                            String s1 = DATE_FORMAT.format(a0) + "_" + s + ".zip";
                            java.io.File a12 = new java.io.File(sCrashReportsFolder + java.io.File.separator + s1);
                            com.navdy.service.library.util.IOUtils.compressFilesToZip(com.navdy.hud.app.HudApplication.getAppContext(), a8, a12.getAbsolutePath());
                            com.navdy.service.library.util.IOUtils.deleteFile(this, a1.getAbsolutePath());
                            com.navdy.service.library.util.IOUtils.deleteDirectory(this, a6);
                            this.addReport(a12);
                            break label1;
                        } catch(Throwable a13) {
                            a4 = a13;
                        }
                    }
                    break label2;
                }
                try {
                    a3.close();
                    break label0;
                } catch(java.io.IOException ignoredException) {
                    sLogger.d("Error closing the File Writer");
                    break label0;
                }
            }
            if (a4 != null) {
                sLogger.e("Exception while creating the crash report ", a4);
            }
            try {
                if (a3 != null) {
                    a3.close();
                }
            } catch(java.io.IOException ignoredException1) {
                sLogger.d("Error closing the File Writer");
            }
        }
    }
    
    public static void dumpCrashReportAsync(com.navdy.hud.app.util.CrashReportService$CrashType a) {
        android.content.Intent a0 = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.util.CrashReportService.class);
        a0.setAction("DumpCrashReport");
        a0.putExtra(EXTRA_CRASH_TYPE, a.name());
        com.navdy.hud.app.HudApplication.getAppContext().startService(a0);
    }
    
    public static com.navdy.hud.app.util.CrashReportService$CrashType getCrashTypeForId(int i) {
        com.navdy.hud.app.util.CrashReportService$CrashType a = null;
        com.navdy.hud.app.util.CrashReportService$CrashType[] a0 = com.navdy.hud.app.util.CrashReportService$CrashType.values();
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < a0.length) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = a0[i];
        }
        return a;
    }
    
    public static void populateFilesQueue(java.io.File[] a, java.util.concurrent.PriorityBlockingQueue a0, int i) {
        if (a != null) {
            int i0 = a.length;
            int i1 = 0;
            while(i1 < i0) {
                java.io.File a1 = a[i1];
                if (a1.isFile()) {
                    sLogger.d("File " + a1.getName());
                    a0.add(a1);
                    if (a0.size() == i) {
                        java.io.File a2 = (java.io.File)a0.poll();
                        sLogger.d(new StringBuilder().append("Deleting the old file ").append(a2.getName()).toString());
                        com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a2.getAbsolutePath());
                    }
                }
                i1 = i1 + 1;
            }
        }
    }
    
    public static String printDeviceInfo(com.navdy.service.library.events.DeviceInfo a) {
        return "\nDevice Information\n" +
                "-----------------------\n\n" +
                "Platform :" + a.platform.name() + "\n" +
                "Name :" + a.deviceName + "\n" +
                "System Version :" + a.systemVersion + "\n" +
                "Model :" + a.model + "\n" +
                "Device ID :" + a.deviceId + "\n" +
                "Make :" + a.deviceMake + "\n" +
                "Build Type :" + a.buildType + "\n" +
                "API level :" + a.systemApiLevel + "\n" +
                "Protocol version :" + a.protocolVersion + "\n" +
                "Force Full Update Flag :" + a.forceFullUpdate + "\n";
    }
    
    public void onCreate() {
        super.onCreate();
        if (!mIsInitialized) {
            sCrashReportsFolder = com.navdy.hud.app.storage.PathManager.getInstance().getNonFatalCrashReportDir();
            java.io.File[] a = new java.io.File(sCrashReportsFolder).listFiles();
            sLogger.d(new StringBuilder().append("Number of Fatal crash reports :").append((a == null) ? 0 : a.length).toString());
            if (a != null) {
                com.navdy.hud.app.util.CrashReportService.populateFilesQueue(a, sCrashReportsToSend, 10);
            }
            mIsInitialized = true;
        }
    }
    
    protected void onHandleIntent(android.content.Intent a) {
        try {
            sLogger.d(new StringBuilder().append("onHandleIntent ").append(a.toString()).toString());
            if ("DumpCrashReport".equals(a.getAction())) {
                String s = a.getStringExtra(EXTRA_CRASH_TYPE);
                if (s == null) {
                    sLogger.e("Missing crash type");
                } else {
                    sLogger.d(new StringBuilder().append("Dumping a crash ").append(s).toString());
                    this.dumpCrashReport(s);
                }
            } else if ("AddReport".equals(a.getAction())) {
                String s0 = a.getStringExtra(EXTRA_FILE_PATH);
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    java.io.File a0 = new java.io.File(s0);
                    if (a0.exists() && a0.isFile()) {
                        this.addReport(a0);
                    }
                }
            }
        } catch(Throwable a1) {
            sLogger.e("Exception while handling intent ", a1);
        }
    }
}
