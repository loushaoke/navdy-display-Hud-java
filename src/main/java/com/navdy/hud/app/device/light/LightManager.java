package com.navdy.hud.app.device.light;

import android.util.SparseArray;

public class LightManager {
    public static final int FRONT_LED = 0;
    private static final String LED_DEVICE = "/sys/class/leds/as3668";
    private static final LightManager sInstance = new LightManager();
    private SparseArray<ILight> mLights = new SparseArray();

    public static LightManager getInstance() {
        return sInstance;
    }

    private LightManager() {
        LED frontLED = new LED(LED_DEVICE);
        if (frontLED.isAvailable()) {
            this.mLights.append(0, frontLED);
        }
    }

    public ILight getLight(int id) {
        return (ILight) this.mLights.get(id);
    }
}
