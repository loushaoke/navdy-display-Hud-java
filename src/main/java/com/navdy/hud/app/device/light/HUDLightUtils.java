package com.navdy.hud.app.device.light;

import android.content.Context;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.service.library.log.Logger;

public class HUDLightUtils
{
    public static LED.Settings gestureDetectedLedSettings;
    public static LED.Settings pairingLedSettings;
    public static final Logger sLogger;
    public static LED.Settings snapShotCollectionEnd;
    public static LED.Settings snapshotCollectionStart;
    
    static {
        sLogger = new Logger(HUDLightUtils.class);
        final int color = HudApplication.getAppContext().getResources().getColor(R.color.led_snapshot);
        HUDLightUtils.snapshotCollectionStart = new LED.Settings.Builder().setName("SnapshotStart").setColor(color).setIsBlinking(false).build();
        HUDLightUtils.snapShotCollectionEnd = new LED.Settings.Builder().setName("SnapshotEnd").setColor(color).setIsBlinking(true).setBlinkInfinite(false).setBlinkPulseCount(3).setBlinkFrequency(LED.BlinkFrequency.HIGH).build();
        HUDLightUtils.gestureDetectedLedSettings = new LED.Settings.Builder().setName("GestureDetected").setColor(HudApplication.getAppContext().getResources().getColor(R.color.led_gesture_color)).setIsBlinking(true).setBlinkInfinite(false).setBlinkPulseCount(2).setBlinkFrequency(LED.BlinkFrequency.HIGH).build();
        HUDLightUtils.pairingLedSettings = new LED.Settings.Builder().setName("Pairing").setColor(HudApplication.getAppContext().getResources().getColor(R.color.led_dial_pairing_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(LED.BlinkFrequency.LOW).build();
    }
    
    public static void dialActionDetected(final Context context, final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("Front LED, showing dial action detected");
            led.startActivityBlink();
        }
    }
    
    public static void removeSettings(final LED.Settings settings) {
        if (settings != null) {
            final LED led = (LED)LightManager.getInstance().getLight(0);
            if (led != null) {
                led.removeSetting(settings);
            }
        }
    }
    
    public static void resetFrontLED(final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("resetFrontLED called");
            led.reset();
        }
    }
    
    public static void showError(final Context context, final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("Front LED, showing error, not blinking");
            led.pushSetting(new LED.Settings.Builder().setColor(context.getResources().getColor(R.color.led_error_color)).setIsBlinking(false).build());
        }
    }
    
    public static void showGestureDetected(final Context context, final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("Front LED, showing gesture detected");
            led.pushSetting(HUDLightUtils.gestureDetectedLedSettings);
            led.removeSetting(HUDLightUtils.gestureDetectedLedSettings);
        }
    }
    
    public static LED.Settings showGestureDetectionEnabled(final Context context, final LightManager lightManager, final String s) {
        final LED led = (LED)lightManager.getLight(0);
        LightSettings build;
        if (led != null) {
            HUDLightUtils.sLogger.d("Front LED, showing gesture detection enabled");
            build = new LED.Settings.Builder().setName("GestureDetectionEnabled-" + s).setColor(context.getResources().getColor(R.color.led_gesture_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(LED.BlinkFrequency.LOW).build();
            led.pushSetting(build);
        }
        else {
            build = null;
        }
        return (LED.Settings)build;
    }
    
    public static void showGestureNotRecognized(final Context context, final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("Front LED, showing gesture not recognized");
            led.pushSetting(new LED.Settings.Builder().setColor(context.getResources().getColor(R.color.led_gesture_not_recognized_color)).setIsBlinking(false).setBlinkFrequency(LED.BlinkFrequency.LOW).build());
        }
    }
    
    public static void showPairing(final Context context, final LightManager lightManager, final boolean b) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            if (b) {
                HUDLightUtils.sLogger.d("Front LED, showing dial pairing");
                led.pushSetting(HUDLightUtils.pairingLedSettings);
            }
            else {
                HUDLightUtils.sLogger.d("Front LED, stop showing dial pairing");
                led.removeSetting(HUDLightUtils.pairingLedSettings);
            }
        }
    }
    
    public static void showShutDown(final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("Front LED, showing shutdown");
            led.pushSetting(new LED.Settings.Builder().setColor(LED.DEFAULT_COLOR).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(LED.BlinkFrequency.LOW).build());
        }
    }
    
    public static void showSnapshotCollection(final Context context, final LightManager lightManager, final boolean b) {
        LED.Settings settings;
        if (b) {
            settings = HUDLightUtils.snapShotCollectionEnd;
        }
        else {
            settings = HUDLightUtils.snapshotCollectionStart;
        }
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            led.pushSetting(settings);
            if (b) {
                led.removeSetting(HUDLightUtils.snapShotCollectionEnd);
                led.removeSetting(HUDLightUtils.snapshotCollectionStart);
            }
        }
    }
    
    public static void showUSBPowerOn(final Context context, final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("Front LED, showing USB power on, not blinking");
            led.pushSetting(new LED.Settings.Builder().setColor(context.getResources().getColor(R.color.led_usb_power_color)).setIsBlinking(false).build());
        }
    }
    
    public static void showUSBPowerShutDown(final Context context, final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("Front LED, showing USB power shutdown");
            led.pushSetting(new LED.Settings.Builder().setColor(context.getResources().getColor(R.color.led_usb_power_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(LED.BlinkFrequency.LOW).build());
        }
    }
    
    public static void showUSBTransfer(final Context context, final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("Front LED, showing USB transfer");
            led.pushSetting(new LED.Settings.Builder().setColor(context.getResources().getColor(R.color.led_usb_power_color)).setIsBlinking(true).setBlinkInfinite(true).setBlinkFrequency(LED.BlinkFrequency.HIGH).build());
        }
    }
    
    public static void turnOffFrontLED(final LightManager lightManager) {
        final LED led = (LED)lightManager.getLight(0);
        if (led != null) {
            HUDLightUtils.sLogger.d("turnOffFrontLED called");
            led.turnOff();
        }
    }
}
