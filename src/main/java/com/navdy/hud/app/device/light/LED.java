package com.navdy.hud.app.device.light;

import android.graphics.Color;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class LED implements ILight {
    private static final String BLINK_ATTRIBUTE = "blink";
    public static final int CLEAR_COLOR = 0;
    private static final String COLOR_ATTRIBUTE = "color";
    public static final int DEFAULT_COLOR = Color.parseColor("#ff000000");
    private static final Settings DEFAULT_SETTINGS = new LED.Settings.Builder().setName("DEFAULT").setColor(DEFAULT_COLOR).setIsBlinking(false).build();
    private static final String SLOPE_DOWN_ATTRUTE = "slope_down";
    private static final String SLOPE_UP_ATTRIBUTE = "slope_up";
    private static final Logger sLogger = new Logger(LED.class);
    private AtomicBoolean activityBlinkRunning = new AtomicBoolean(false);
    private String blinkPath;
    private String colorPath;
    private LinkedList<LightSettings> mLightSettingsStack;
    private ExecutorService mSerialExecutor;
    private String slopeDownPath;
    private String slopeUpPath;

    enum BlinkFrequency {
        LOW(1000),
        HIGH(100);
        
        private int blinkDelay;

        public int getBlinkDelay() {
            return this.blinkDelay;
        }

        private BlinkFrequency(int delay) {
            this.blinkDelay = delay;
        }
    }

    public static class Settings extends LightSettings {
        private boolean activityBlink;
        private BlinkFrequency blinkFrequency;
        private boolean blinkInfinite;
        private int blinkPulseCount;
        private String name;

        public static class Builder {
            private boolean activityBlink = false;
            private BlinkFrequency blinkFrequency = BlinkFrequency.LOW;
            private boolean blinkInfinite = true;
            private int blinkPulseCount = 0;
            private int color = LED.DEFAULT_COLOR;
            private boolean isBlinking = true;
            private boolean isTurnedOn = true;
            private String name = "UNKNOWN";

            public Builder setColor(int color) {
                this.color = color;
                return this;
            }

            public Builder setIsTurnedOn(boolean isTurnedOn) {
                this.isTurnedOn = isTurnedOn;
                return this;
            }

            public Builder setIsBlinking(boolean isBlinking) {
                this.isBlinking = isBlinking;
                return this;
            }

            public Builder setBlinkFrequency(BlinkFrequency blinkFrequency) {
                this.blinkFrequency = blinkFrequency;
                return this;
            }

            public Builder setBlinkInfinite(boolean blinkInfinite) {
                this.blinkInfinite = blinkInfinite;
                return this;
            }

            public Builder setBlinkPulseCount(int blinkPulseCount) {
                this.blinkPulseCount = blinkPulseCount;
                return this;
            }

            public Builder setActivityBlink(boolean activityBlink) {
                this.activityBlink = activityBlink;
                return this;
            }

            public Builder setName(String name) {
                this.name = name;
                return this;
            }

            public Settings build() {
                return new Settings(this.color, this.isTurnedOn, this.isBlinking, this.blinkFrequency, this.blinkInfinite, this.blinkPulseCount, this.activityBlink, this.name);
            }
        }

        public Settings(int color, boolean isTurnedOn, boolean isBlinking, BlinkFrequency blinkFrequency, boolean blinkInfinite, int blinkPulseCount, boolean activityBlink, String name) {
            super(color, isTurnedOn, isBlinking);
            this.blinkFrequency = blinkFrequency;
            this.blinkInfinite = blinkInfinite;
            this.blinkPulseCount = blinkPulseCount;
            this.activityBlink = activityBlink;
            this.name = name;
        }

        public BlinkFrequency getBlinkFrequency() {
            return this.blinkFrequency;
        }

        public boolean isBlinkInfinite() {
            return this.blinkInfinite;
        }

        public int getBlinkPulseCount() {
            return this.blinkPulseCount;
        }

        public boolean getActivityBlink() {
            return this.activityBlink;
        }
    }

    public LED(String ledDevice) {
        if (new File(ledDevice).exists()) {
            this.colorPath = ledDevice + File.separator + COLOR_ATTRIBUTE;
            this.slopeUpPath = ledDevice + File.separator + SLOPE_UP_ATTRIBUTE;
            this.slopeDownPath = ledDevice + File.separator + SLOPE_DOWN_ATTRUTE;
            this.blinkPath = ledDevice + File.separator + BLINK_ATTRIBUTE;
            this.mLightSettingsStack = new LinkedList();
            this.mSerialExecutor = Executors.newSingleThreadExecutor();
            this.mLightSettingsStack.push(DEFAULT_SETTINGS);
            return;
        }
        sLogger.w("Unable to open led at " + ledDevice);
    }

    public boolean isAvailable() {
        return this.mLightSettingsStack != null;
    }

    public synchronized void turnOn() {
        stopBlinking();
        setColor(DEFAULT_COLOR);
    }

    public synchronized void turnOff() {
        stopBlinking();
        setColor(0);
    }

    public static void writeToSysfs(String data, String filepath) {
        sLogger.d("writing '" + data + "' to sysfs file '" + filepath + "'");
        writeToKernelDevice(data, filepath);
    }

    private void setColor(int color) {
        writeToKernelDevice(String.format("%08x", new Object[]{Integer.valueOf(color)}), this.colorPath);
    }

    private void startBlinking() {
        writeToKernelDevice(ToastPresenter.EXTRA_MAIN_TITLE, this.blinkPath);
    }

    private void startBlinking(int pulseCount, int color, BlinkFrequency frequency) {
        stopBlinking();
        int blinkDelay = frequency.blinkDelay;
        if (pulseCount > 0) {
            for (int i = 0; i < pulseCount; i++) {
                try {
                    setColor(color);
                    Thread.sleep((long) (blinkDelay / 2));
                    setColor(0);
                    Thread.sleep((long) (blinkDelay / 2));
                } catch (InterruptedException e) {
                    sLogger.e("Interrupted exception while blinking");
                }
            }
        }
    }

    private void stopBlinking() {
        if (sLogger.isLoggable(3)) {
            sLogger.d("Stop blinking");
        }
        writeToKernelDevice("0", this.blinkPath);
    }

    private void activityBlink() {
        try {
            stopBlinking();
            setBlinkFrequency(BlinkFrequency.HIGH);
            startBlinking();
            Thread.sleep((long) (BlinkFrequency.HIGH.blinkDelay / 2));
        } catch (InterruptedException e) {
            sLogger.e("Interrupted exception while activity blinking");
        } catch (Throwable th) {
            this.activityBlinkRunning.set(false);
        }
        stopBlinking();
        this.activityBlinkRunning.set(false);
    }

    public synchronized void pushSetting(LightSettings settings) {
        this.mLightSettingsStack.push(settings);
        applySettings(settings);
    }

    public synchronized void popSetting() {
        if (this.mLightSettingsStack.size() > 1) {
            LightSettings lightSettings = (LightSettings) this.mLightSettingsStack.pop();
            if (lightSettings != null) {
                Settings lightSettings2 = (Settings) lightSettings;
            }
        }
        applySettings((LightSettings) this.mLightSettingsStack.peek());
    }

    public synchronized void removeSetting(LightSettings settings) {
        if (this.mLightSettingsStack.peek() == settings) {
            popSetting();
        } else {
            this.mLightSettingsStack.remove(settings);
        }
    }

    public synchronized void reset() {
        this.mLightSettingsStack.clear();
        pushSetting(DEFAULT_SETTINGS);
    }

    private void setBlinkFrequency(BlinkFrequency frequency) {
        if (frequency != null) {
            writeToKernelDevice(String.valueOf(frequency.getBlinkDelay()), this.slopeUpPath);
            writeToKernelDevice(String.valueOf(frequency.getBlinkDelay()), this.slopeDownPath);
        }
    }

    private void applySettings(final LightSettings settings) {
        this.mSerialExecutor.execute(new Runnable() {
            public void run() {
                if (settings instanceof LED.Settings) {
                    LED.Settings ledSettings = (LED.Settings)settings;
                    if (ledSettings.getActivityBlink()) {
                        LED.this.activityBlink();
                    } else if (ledSettings.isTurnedOn()) {
                        LED.this.setColor(ledSettings.getColor());
                        if (!ledSettings.isBlinking()) {
                            LED.this.stopBlinking();
                        } else if (ledSettings.isBlinkInfinite()) {
                            LED.this.setBlinkFrequency(ledSettings.getBlinkFrequency());
                            LED.this.startBlinking();
                        } else {
                            LED.this.startBlinking(ledSettings.getBlinkPulseCount(), ledSettings.getColor(), ledSettings.getBlinkFrequency());
                        }
                    } else {
                        LED.this.turnOff();
                    }
                }
            }
        });
    }

    private static void writeToKernelDevice(String data, String kernelFilePath) {
        try {
            try {
                FileWriter writer = new FileWriter(new File(kernelFilePath));
                writer.write(data);
                writer.flush();
                writer.close();
            } catch (IOException e) {
                sLogger.e("Error writing to the sysfsfile: " + e.toString());
            }
        } catch (Exception e2) {
            sLogger.e("Exception while trying to write to the sysfs ", e2);
        }
    }

    public void startActivityBlink() {
        if (this.activityBlinkRunning.compareAndSet(false, true)) {
            Settings settings = new LED.Settings.Builder().setName("ActivityBlink").setActivityBlink(true).build();
            pushSetting(settings);
            removeSetting(settings);
        }
    }
}
