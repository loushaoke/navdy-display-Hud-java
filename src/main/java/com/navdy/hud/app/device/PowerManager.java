package com.navdy.hud.app.device;

import android.annotation.SuppressLint;
import android.content.Context;

import com.squareup.otto.Subscribe;

public class PowerManager {
    final public static String BOOT_POWER_MODE;
    final private static String COOLING_DEVICE = "/sys/devices/virtual/thermal/cooling_device0/cur_state";
    final private static long COOLING_MONITOR_INTERVAL;
    final private static long DEFAULT_INSTANT_ON_TIMEOUT;
    final private static String INSTANT_ON = "persist.sys.instanton";
    final private static long INSTANT_ON_TIMEOUT;
    final private static String INSTANT_ON_TIMEOUT_PROPERTRY = "persist.sys.instanton.timeout";
    final public static String LAST_LOW_VOLTAGE_EVENT = "last_low_voltage_event";
    final public static String NORMAL_MODE = "normal";
    final public static String POWER_MODE_PROPERTY = "sys.power.mode";
    final public static String QUIET_MODE = "quiet";
    final public static long RECHARGE_TIME;
    final private static com.navdy.service.library.log.Logger sLogger;
    private boolean awake;
    private com.squareup.otto.Bus bus;
    private Runnable checkCoolingState;
    private android.os.Handler handler;
    private long lastLowVoltage;
    private java.lang.reflect.Method mIPowerManagerShutdownMethod;
    private Object mPowerManager;
    private String oldCoolingState;
    private android.os.PowerManager powerManager;
    private android.content.SharedPreferences preferences;
    private Runnable quietModeTimeout;
    private com.navdy.hud.app.device.PowerManager$RunState runState;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.PowerManager.class);
        BOOT_POWER_MODE = com.navdy.hud.app.util.os.SystemProperties.get("sys.power.mode");
        COOLING_MONITOR_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(15L);
        DEFAULT_INSTANT_ON_TIMEOUT = java.util.concurrent.TimeUnit.HOURS.toMillis(4L);
        INSTANT_ON_TIMEOUT = com.navdy.hud.app.util.os.SystemProperties.getLong("persist.sys.instanton.timeout", DEFAULT_INSTANT_ON_TIMEOUT);
        RECHARGE_TIME = java.util.concurrent.TimeUnit.DAYS.toMillis(7L);
    }
    
    public PowerManager(com.squareup.otto.Bus a, android.content.Context a0, android.content.SharedPreferences a1) {
        this.awake = !"quiet".equals(BOOT_POWER_MODE);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.checkCoolingState = (Runnable)new com.navdy.hud.app.device.PowerManager$1(this);
        this.quietModeTimeout = (Runnable)new com.navdy.hud.app.device.PowerManager$2(this);
        this.mIPowerManagerShutdownMethod = null;
        this.mPowerManager = null;
        this.bus = a;
        a.register(this);
        this.powerManager = (android.os.PowerManager)a0.getSystemService(Context.POWER_SERVICE);
        this.preferences = a1;
        this.lastLowVoltage = a1.getLong("last_low_voltage_event", -1L);
        this.setupShutdown();
        com.navdy.hud.app.util.os.SystemProperties.set("sys.power.mode", (this.inQuietMode()) ? "quiet" : "normal");
        if (!this.inQuietMode()) {
            this.startOverheatMonitoring(com.navdy.hud.app.device.PowerManager$RunState.Booting);
        }
        sLogger.i(new StringBuilder().append("quietMode:").append(this.inQuietMode()).toString());
    }
    
    static boolean access$000(com.navdy.hud.app.device.PowerManager a) {
        return a.updateCoolingState();
    }
    
    static Runnable access$100(com.navdy.hud.app.device.PowerManager a) {
        return a.checkCoolingState;
    }
    
    static long access$200() {
        return COOLING_MONITOR_INTERVAL;
    }
    
    static android.os.Handler access$300(com.navdy.hud.app.device.PowerManager a) {
        return a.handler;
    }
    
    static com.navdy.service.library.log.Logger access$400() {
        return sLogger;
    }
    
    static com.squareup.otto.Bus access$500(com.navdy.hud.app.device.PowerManager a) {
        return a.bus;
    }
    
    private void androidSleep() {
        sLogger.d("androidSleep()");
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
        try {
            Class[] a = new Class[1];
            a[0] = Long.TYPE;
            java.lang.reflect.Method a0 = android.os.PowerManager.class.getMethod("goToSleep", a);
            android.os.PowerManager a1 = this.powerManager;
            Object[] a2 = new Object[1];
            a2[0] = Long.valueOf(android.os.SystemClock.uptimeMillis());
            a0.invoke(a1, a2);
        } catch(Exception a3) {
            sLogger.e(new StringBuilder().append("error invoking PowerManager.goToSleep(): ").append(a3).toString());
        }
    }
    
    private void androidWakeup() {
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        try {
            Class[] a = new Class[1];
            a[0] = Long.TYPE;
            java.lang.reflect.Method a0 = android.os.PowerManager.class.getMethod("wakeUp", a);
            android.os.PowerManager a1 = this.powerManager;
            Object[] a2 = new Object[1];
            a2[0] = Long.valueOf(android.os.SystemClock.uptimeMillis());
            a0.invoke(a1, a2);
        } catch(Exception a3) {
            sLogger.e(new StringBuilder().append("error invoking PowerManager.wakeUp(): ").append(a3).toString());
        }
    }
    
    public static boolean isAwake() {
        return !com.navdy.hud.app.util.os.SystemProperties.get("sys.power.mode", "").equals("quiet");
    }
    
    private boolean isCoolingActive(String s) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (s == null) {
                        break label1;
                    }
                    if (!s.equals("0")) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void setupShutdown() {
        label1: {
            label0: {
                Exception a = null;
                try {
                    Class a0 = Class.forName("android.os.ServiceManager");
                    Class[] a1 = new Class[1];
                    a1[0] = String.class;
                    java.lang.reflect.Method a2 = a0.getDeclaredMethod("getService", a1);
                    Object[] a3 = new Object[1];
                    a3[0] = "power";
                    Object a4 = a2.invoke(null, a3);
                    Class a5 = Class.forName("android.os.IPowerManager$Stub");
                    Class[] a6 = new Class[1];
                    a6[0] = android.os.IBinder.class;
                    java.lang.reflect.Method a7 = a5.getDeclaredMethod("asInterface", a6);
                    Object[] a8 = new Object[1];
                    a8[0] = a4;
                    this.mPowerManager = a7.invoke(null, a8);
                    break label0;
                } catch(Exception a9) {
                    a = a9;
                }
                sLogger.e("exception invoking IPowerManager.Stub.asInterface()", (Throwable)a);
                break label1;
            }
            try {
                Class a10 = Class.forName("android.os.IPowerManager");
                Class[] a11 = new Class[2];
                a11[0] = Boolean.TYPE;
                a11[1] = Boolean.TYPE;
                this.mIPowerManagerShutdownMethod = a10.getDeclaredMethod("shutdown", a11);
            } catch(Exception a12) {
                sLogger.e("exception getting IPowerManager.shutdown() method", (Throwable)a12);
            }
        }
    }
    
    private void startOverheatMonitoring(com.navdy.hud.app.device.PowerManager$RunState a) {
        this.runState = a;
        this.handler.removeCallbacks(this.checkCoolingState);
        this.handler.post(this.checkCoolingState);
    }
    
    private boolean updateCoolingState() {
        boolean b = false;
        try {
            String s = com.navdy.service.library.util.IOUtils.convertFileToString("/sys/devices/virtual/thermal/cooling_device0/cur_state").trim();
            if (!s.equals(this.oldCoolingState)) {
                sLogger.i(new StringBuilder().append("Cooling state changed from:").append(this.oldCoolingState).append(" to:").append(s).toString());
                if (!this.isCoolingActive(this.oldCoolingState) && this.isCoolingActive(s)) {
                    com.navdy.hud.app.analytics.AnalyticsSupport.recordCpuOverheat(this.runState.name());
                }
                this.oldCoolingState = s;
            }
            this.runState = com.navdy.hud.app.device.PowerManager$RunState.Running;
            b = true;
        } catch(java.io.IOException a) {
            sLogger.e("Failed to read cooling device state - stopping monitoring", (Throwable)a);
            b = false;
        }
        return b;
    }

    @SuppressLint({"ApplySharedPref"})
    public void androidShutdown(com.navdy.hud.app.event.Shutdown.Reason a, boolean b) {
        sLogger.d(new StringBuilder().append("androidShutdown: ").append(a).append(" forceFullShutdown:").append(b).toString());
        com.navdy.hud.app.device.dial.DialManager.getInstance().requestDialReboot(false);
        com.navdy.hud.app.event.Shutdown.Reason a0 = com.navdy.hud.app.event.Shutdown.Reason.LOW_VOLTAGE;
        label3: {
            label4: {
                if (a == a0) {
                    break label4;
                }
                if (a != com.navdy.hud.app.event.Shutdown.Reason.CRITICAL_VOLTAGE) {
                    break label3;
                }
            }
            this.preferences.edit().putLong("last_low_voltage_event", System.currentTimeMillis()).commit();
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.recordShutdown(a, !b);
        com.navdy.hud.app.device.light.LED.writeToSysfs("0", "/sys/dlpc/led_enable");
        Object a1 = this.mPowerManager;
        label2: {
            label0: {
                label1: {
                    if (a1 == null) {
                        break label1;
                    }
                    if (this.mIPowerManagerShutdownMethod != null) {
                        break label0;
                    }
                }
                sLogger.e("shutdown was not properly initialized");
                break label2;
            }
            try {
                if (b) {
                    java.lang.reflect.Method a2 = this.mIPowerManagerShutdownMethod;
                    Object a3 = this.mPowerManager;
                    Object[] a4 = new Object[2];
                    a4[0] = Boolean.FALSE;
                    a4[1] = Boolean.FALSE;
                    a2.invoke(a3, a4);
                } else {
                    this.powerManager.reboot("quiet");
                }
            } catch(Exception a5) {
                sLogger.e("exception invoking IPowerManager.shutdown()", (Throwable)a5);
            }
        }
    }
    
    public void enterSleepMode() {
        this.handler.postDelayed(this.quietModeTimeout, INSTANT_ON_TIMEOUT);
        com.navdy.hud.app.device.light.HUDLightUtils.turnOffFrontLED(com.navdy.hud.app.device.light.LightManager.getInstance());
        this.androidSleep();
    }
    
    public boolean inQuietMode() {
        return !this.awake;
    }

    @Subscribe
    public void onKey(android.view.KeyEvent a) {
        this.wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason.DIAL);
    }
    
    public boolean quietModeEnabled() {
        boolean b = false;
        boolean b0 = false;
        long j = System.currentTimeMillis() - this.lastLowVoltage;
        long j0 = this.lastLowVoltage;
        int i = (j0 < -1L) ? -  1 : (j0 == -1L) ? 0 : 1;
        label4: {
            label2: {
                label3: {
                    if (i == 0) {
                        break label3;
                    }
                    if (j < RECHARGE_TIME) {
                        break label2;
                    }
                }
                b = false;
                break label4;
            }
            b = true;
        }
        if (b) {
            sLogger.d(new StringBuilder().append("disabling quiet mode since we had a low voltage event ").append(java.util.concurrent.TimeUnit.MILLISECONDS.toHours(j)).append(" hours ago").toString());
        }
        boolean b1 = com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.instanton", true);
        label0: {
            label1: {
                if (!b1) {
                    break label1;
                }
                if (!b) {
                    b0 = true;
                    break label0;
                }
            }
            b0 = false;
        }
        return b0;
    }
    
    public void wakeUp(com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason a) {
        if (!this.awake) {
            sLogger.d("waking up");
            this.awake = true;
            com.navdy.hud.app.util.os.SystemProperties.set("sys.power.mode", "normal");
            this.handler.removeCallbacks(this.quietModeTimeout);
            com.navdy.hud.app.device.light.HUDLightUtils.resetFrontLED(com.navdy.hud.app.device.light.LightManager.getInstance());
            com.navdy.hud.app.device.light.LED.writeToSysfs("1", "/sys/dlpc/led_enable");
            this.androidWakeup();
            this.startOverheatMonitoring(com.navdy.hud.app.device.PowerManager$RunState.Waking);
            this.bus.post(new com.navdy.hud.app.event.Wakeup(a));
        }
    }
}
