package com.navdy.hud.app.debug;

import java.io.IOException;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "run"}, k = 3, mv = {1, 1, 6})
/* compiled from: AsyncBufferedFileWriter.kt */
final class AsyncBufferedFileWriter$write$1 implements Runnable {
    final /* synthetic */ String $data;
    final /* synthetic */ boolean $forceToDisk;
    final /* synthetic */ AsyncBufferedFileWriter this$0;

    AsyncBufferedFileWriter$write$1(AsyncBufferedFileWriter asyncBufferedFileWriter, String str, boolean z) {
        this.this$0 = asyncBufferedFileWriter;
        this.$data = str;
        this.$forceToDisk = z;
    }

    public final void run() {
        try {
            this.this$0.getBufferedWriter().write(this.$data);
            if (this.$forceToDisk) {
                this.this$0.getBufferedWriter().flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
