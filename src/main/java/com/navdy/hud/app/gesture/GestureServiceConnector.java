package com.navdy.hud.app.gesture;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;
import android.os.SystemClock;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.State;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.input.Gesture;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;

public class GestureServiceConnector {
    private static final String AUTO_EXPOSURE_OFF = "autoexpo off";
    private static final String AUTO_EXPOSURE_ON = "autoexpo on";
    private static final String CALIBRATE_MASK = "calibrate-mask";
    private static final String CALIBRATE_MASK_FORCE = "calibrate-mask-force";
    private static final String CALIBRATE_SAVE_SNAPSHOT_ON = "calibrate-save-snapshot on";
    private static final String DEV_SOCKET_SWIPED = "/dev/socket/swiped";
    private static final String EXPOSURE = "exposure";
    private static final String FORMAT = "format";
    private static final String GAIN = "gain";
    public static final String GESTURE_ENABLED = "gesture.enabled";
    public static final String GESTURE_LEFT = "gesture left";
    public static final String GESTURE_RIGHT = "gesture right";
    public static final String GESTURE_VERSION = "persist.sys.gesture.version";
    public static final String LEFT = "left";
    public static final char LEFT_FIRST_CHAR = 'l';
    public static final int MAX_RECORD_TIME = 6000;
    private static final int MINIMUM_INTERVAL_BETWEEN_SNAPSHOTS = 5000;
    private static final String NOTIFY = "notify";
    private static final int PID_CHECK_INTERVAL = 10000;
    public static final String RECORDING_SAVED = "recording-saved:";
    public static final String RECORD_SAVE = "record save";
    public static final String RIGHT = "right";
    public static final char RIGHT_FIRST_CHAR = 'r';
    public static final String SNAPSHOT = "snapshot ";
    public static final String SNAPSHOT_PATH = "/data/misc/swiped/camera.png";
    private static final String SOCKET_NAME = "swiped";
    private static final int SO_TIMEOUT = 2000;
    public static final String SWIPED_DISABLE_CALIBRATION = "calibration pause";
    public static final String SWIPED_ENABLE_CALIBRATION = "calibration resume";
    private static final String SWIPED_PROCESS_NAME = "/system/xbin/swiped";
    public static final String SWIPED_RECORD_MODE_ONE_SHOT = "record-mode oneshot";
    public static final String SWIPED_RECORD_MODE_ROLLING = "record-mode rolling";
    public static final String SWIPED_START_RECORDING = "record on";
    public static final String SWIPED_STOP_RECORDING = "record off";
    public static final String SWIPE_PROGRESS = "swipe-progress:";
    public static final int SWIPE_PROGRESS_COMMAND_DATA_OFFSET = SWIPE_PROGRESS.length();
    public static final String SWIPE_PROGRESS_LEFT = "swipe-progress: left,";
    public static final String SWIPE_PROGRESS_RIGHT = "swipe-progress: right,";
    public static final String SWIPE_PROGRESS_UNKNOWN = "swipe-progress: unknown,";
    public static final String UNKNOWN = "unknown";
    private static final CharsetEncoder encoder = Charset.defaultCharset().newEncoder();
    private static final Logger sLogger = new Logger(GestureServiceConnector.class);
    private static final byte[] temp = new byte[1024];
    private Bus bus;
    private long lastSnapshotTime = 0;
    private PowerManager powerManager;
    private String recordingPath;
    private volatile boolean running;
    private boolean shuttingDown;
    private volatile Thread swipedReader;
    private volatile LocalSocket swipedSocket;
    private String vin = null;

    public static class ConnectedEvent {
    }

    public enum Error {
        CANNOT_CONNECT(0),
        COMMUNICATION_LOST(1),
        SWIPED_RESTARTED(2);

        private int value;
        Error(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public enum GestureDirection {
        UNKNOWN(0),
        LEFT(1),
        RIGHT(2);

        private int value;
        GestureDirection(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    public static class GestureProgress {
        public GestureDirection direction;
        public float progress;

        public GestureProgress(GestureDirection direction, float progress) {
            this.direction = direction;
            this.progress = progress;
        }
    }

    public static class RecordingSaved {
        public String path;

        public RecordingSaved(String path) {
            this.path = path;
        }
    }

    public static class TakeSnapshot {
    }

    public GestureServiceConnector(Bus bus, PowerManager powerManager) {
        this.bus = bus;
        this.powerManager = powerManager;
        bus.register(this);
    }

    public synchronized void start() {
        GenericUtil.checkNotOnMainThread();
        if (DeviceUtil.isNavdyDevice()) {
            if (this.running) {
                sLogger.v("swipedconnector already running");
            } else {
                closeSocket();
                closeThread();
                if (this.powerManager.inQuietMode()) {
                    sLogger.v("Not starting gesture engine due to quiet mode");
                } else {
                    this.running = true;
                    SystemProperties.set(GESTURE_ENABLED, SystemProperties.get(GESTURE_VERSION, DeviceUtil.isUserBuild() ? ToastPresenter.EXTRA_MAIN_TITLE : "beta"));
                    this.swipedReader = new Thread(new Runnable() {
                        public void run() {
                            while (GestureServiceConnector.this.running) {
                                try {
                                    GestureServiceConnector.sLogger.v("creating socket");
                                    GestureServiceConnector.this.swipedSocket = new LocalSocket(1);
                                    GestureServiceConnector.sLogger.v("bind socket");
                                    GestureServiceConnector.this.swipedSocket.bind(new LocalSocketAddress(""));
                                    GestureServiceConnector.sLogger.v("calling communicateWithSwipeDaemon");
                                    Error error = GestureServiceConnector.this.communicateWithSwipeDaemon(GestureServiceConnector.this.swipedSocket);
                                    GestureServiceConnector.sLogger.v("called communicateWithSwipeDaemon");
                                    if (GestureServiceConnector.this.running) {
                                        GestureServiceConnector.this.closeSocket();
                                        switch (error) {
                                            case CANNOT_CONNECT:
                                                GestureServiceConnector.sLogger.v("communicateWithSwipeDaemon  cannot connect");
                                                Thread.sleep(5000);
                                                break;
                                            case COMMUNICATION_LOST:
                                                GestureServiceConnector.sLogger.v("communicateWithSwipeDaemon  communication lost, restart");
                                                GestureServiceConnector.this.reStart();
                                                GestureServiceConnector.sLogger.v("swipedconnector connect thread exit");
                                                return;
                                            case SWIPED_RESTARTED:
                                                GestureServiceConnector.sLogger.v("communicateWithSwipeDaemon  swiped restarted, restart");
                                                GestureServiceConnector.this.reStart();
                                                GestureServiceConnector.sLogger.v("swipedconnector connect thread exit");
                                                return;
                                        }
                                    }
                                    GestureServiceConnector.sLogger.v("swipedconnector connect thread exit");
                                } catch (Throwable th) {
                                    GestureServiceConnector.sLogger.v("swipedconnector connect thread exit");
                                }
                            }
                        }
                    });
                    this.swipedReader.setName("SwipedReaderThread");
                    this.swipedReader.start();
                    sLogger.v("swipedconnector connect thread started");
                }
            }
        }
    }

    public synchronized void stop() {
        GenericUtil.checkNotOnMainThread();
        if (DeviceUtil.isNavdyDevice()) {
            if (this.running) {
                this.running = false;
                SystemProperties.set(GESTURE_ENABLED, "0");
                closeSocket();
                closeThread();
            }
        }
    }

    private void reStart() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                GestureServiceConnector.this.stop();
                GenericUtil.sleep(2000);
                GestureServiceConnector.this.start();
            }
        }, 1);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private Error communicateWithSwipeDaemon(LocalSocket sock) {
        boolean connected = false;
        try {
            sock.connect(new LocalSocketAddress(SOCKET_NAME, Namespace.RESERVED));
            sendCommand(NOTIFY);
            connected = true;
            sendCommand(SWIPED_ENABLE_CALIBRATION);
            sendCommand(SWIPED_STOP_RECORDING);
            sendCommand(SWIPED_RECORD_MODE_ROLLING);
        } catch (Throwable th) {
            sLogger.d("socket connect to filesystem namespace failed", th);
        }
        if (!this.running) {
            sLogger.v("swipedconnector is not running");
            return Error.CANNOT_CONNECT;
        } else if (connected) {
            int swipedPid = SystemUtils.getNativeProcessId(SWIPED_PROCESS_NAME);
            sLogger.v("swipedconnector connected");
            this.bus.post(new ConnectedEvent());
            InputStream ins;
            byte[] buf;
            try {
                ins = sock.getInputStream();
                sLogger.d("read starting");
                buf = new byte[1000];
                sLogger.v("setting timeout");
                this.swipedSocket.setSoTimeout(2000);
                sLogger.v("set timeout");
            } catch (Throwable th) {
                sLogger.d("failed to configure socket", th);
                return Error.CANNOT_CONNECT;
            }
            long lastPidCheckTime = SystemClock.elapsedRealtime();
            sLogger.v("swipedconnector pid  [" + swipedPid + "]");
            while (this.running) {
                try {
                    int br = ins.read(buf);
                    if (br < 0) {
                        sLogger.v("swipedconnector closed the socket");
                        return Error.COMMUNICATION_LOST;
                    } else if (br > 1) {
                        String str = new String(buf, 0, br - 1);
                        switch (str.hashCode()) {
                            case -1253681019:
                                if (str.equals(GESTURE_RIGHT)) {
                                    this.bus.post(new GestureEvent(Gesture.GESTURE_SWIPE_RIGHT, 0, 0));
                                    break;
                                }
                            case 2037586046:
                                if (str.equals(GESTURE_LEFT)) {
                                    this.bus.post(new GestureEvent(Gesture.GESTURE_SWIPE_LEFT, 0, 0));
                                    break;
                                }
                            default:
                                if (!str.startsWith(SWIPE_PROGRESS) || str.length() <= SWIPE_PROGRESS_COMMAND_DATA_OFFSET + 1) {
                                    if (!str.startsWith(RECORDING_SAVED)) {
                                        if (!str.startsWith("swiped event: ")) {
                                            break;
                                        }
                                        String[] parts = str.split(" ");
                                        if (parts.length < 3) {
                                            break;
                                        }
                                        String eventName = parts[2];
                                        List<String> args = new ArrayList();
                                        for (int i = 3; i < parts.length; i++) {
                                            String[] parts2 = parts[i].split("=");
                                            String key = parts2[0];
                                            String value = parts2[1];
                                            args.add(key);
                                            args.add(value);
                                        }
                                        AnalyticsSupport.recordSwipedCalibration(eventName, (String[]) args.toArray(new String[args.size()]));
                                        break;
                                    }
                                    this.recordingPath = str.substring(RECORDING_SAVED.length() + 1);
                                    sLogger.i("recording saved at " + this.recordingPath);
                                    synchronized (this) {
                                        notify();
                                    }
                                    this.bus.post(new RecordingSaved(this.recordingPath));
                                    break;
                                }
                                switch (str.charAt(SWIPE_PROGRESS_COMMAND_DATA_OFFSET + 1)) {
                                    case 'l':
                                        try {
                                            this.bus.post(new GestureProgress(GestureDirection.LEFT, Float.valueOf(str.substring(SWIPE_PROGRESS_LEFT.length()))));
                                            break;
                                        } catch (Throwable t) {
                                            sLogger.e("Error parsing the progress " + str, t);
                                            break;
                                        }
                                    case 'r':
                                        try {
                                            this.bus.post(new GestureProgress(GestureDirection.RIGHT, Float.valueOf(str.substring(SWIPE_PROGRESS_RIGHT.length()))));
                                            break;
                                        } catch (Throwable t2) {
                                            sLogger.e("Error parsing the progress " + str, t2);
                                            break;
                                        }
                                    default:
                                        break;
                                }
                                break;
                        }
                    } else {
                        continue;
                    }
                } catch (IOException io) {
                    if (this.running) {
                        if ("Try again".equals(io.getMessage())) {
                            long clockTime = SystemClock.elapsedRealtime();
                            if (clockTime - lastPidCheckTime > 10000) {
                                int pid = SystemUtils.getNativeProcessId(SWIPED_PROCESS_NAME);
                                if (pid != swipedPid) {
                                    sLogger.e("swipedconnector pid has changed from [" + swipedPid + "] to [" + pid + "]");
                                    return Error.SWIPED_RESTARTED;
                                }
                                lastPidCheckTime = clockTime;
                            } else {
                                continue;
                            }
                        }
                    }
                } catch (Throwable e) {
                    sLogger.e("swipedconnector", e);
                }
            }
            return Error.COMMUNICATION_LOST;
        } else {
            sLogger.v("swipedconnector could not connect");
            return Error.CANNOT_CONNECT;
        }
    }

    public synchronized void sendCommand(String command) throws IOException {
        if (this.swipedSocket != null) {
            ByteBuffer out = ByteBuffer.wrap(temp);
            encoder.encode(CharBuffer.wrap(command), out, true);
            out.put((byte) 0);
            this.swipedSocket.getOutputStream().write(temp, 0, out.position());
        }
    }

    public void sendCommandAsync(final String command) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    GestureServiceConnector.this.sendCommand(command);
                } catch (Throwable t) {
                    GestureServiceConnector.sLogger.e(t);
                }
            }
        }, 10);
    }

    public void sendCommand(Object... args) throws IOException {
        if (args != null && args.length > 0) {
            StringBuilder builder = new StringBuilder();
            for (Object arg : args) {
                if (arg != null) {
                    if (arg == args[args.length - 1]) {
                        builder.append(arg.toString());
                    } else {
                        builder.append(arg.toString() + " ");
                    }
                }
            }
            sendCommand(builder.toString());
        }
    }

    public void takeSnapShot(String absolutePath) throws IOException {
        long time = SystemClock.elapsedRealtime();
        if (time - this.lastSnapshotTime > 5000) {
            this.lastSnapshotTime = time;
            sendCommand(SNAPSHOT + absolutePath);
        }
    }

    public void setRecordMode(boolean bl) {
        sLogger.d("setRecordMode " + bl);
        String string = bl ? SWIPED_RECORD_MODE_ONE_SHOT : SWIPED_RECORD_MODE_ROLLING;
        try {
            this.sendCommand(string);
        }
        catch (IOException iOException) {
            sLogger.e("Exception while setting the record mode ", iOException);
        }
    }

    public void setCalibrationEnabled(boolean bl) {
        String string = bl ? SWIPED_ENABLE_CALIBRATION : SWIPED_DISABLE_CALIBRATION;
        try {
            this.sendCommand(string);
        }
        catch (IOException iOException) {
            sLogger.e("Exception while setting calibration state ", iOException);
        }
    }


    public void startRecordingVideo() {
        try {
            sLogger.d("startRecordingVideo");
            sendCommand(SWIPED_START_RECORDING);
        } catch (IOException e) {
        }
    }

    public void stopRecordingVideo() {
        sLogger.d("stopRecordingVideo");
        try {
            sendCommand(SWIPED_STOP_RECORDING);
        } catch (IOException e) {
        }
    }

    public synchronized String dumpRecording() {
        String str;
        try {
            sendCommand(RECORD_SAVE);
            wait(6000);
            str = this.recordingPath;
        } catch (IOException e) {
            str = null;
            return str;
        } catch (InterruptedException e2) {
            str = null;
            return str;
        }
        return str;
    }

    public boolean isRunning() {
        return this.swipedSocket != null;
    }

    public void setDiscreteMode(boolean discreteMode) {
    }

    public void enablePreview(boolean on) {
    }

    @Subscribe
    public void ObdStateChangeEvent(ObdConnectionStatusEvent event) {
        if (DeviceUtil.isNavdyDevice() && event.connected) {
            String newVin = ObdManager.getInstance().getVin();
            if (newVin != null && !newVin.equals(this.vin)) {
                this.vin = newVin;
                sendCommandAsync("set-id " + this.vin);
            }
        }
    }

    @Subscribe
    public void onWakeup(Wakeup event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                GestureServiceConnector.this.start();
            }
        }, 1);
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged event) {
        if (DeviceUtil.isNavdyDevice()) {
            InputPreferences inputPreferences = DriverProfileHelper.getInstance().getCurrentProfile().getInputPreferences();
            if (inputPreferences != null) {
                onInputPreferenceUpdate(inputPreferences);
            }
        }
    }

    @Subscribe
    public void onTakeSnapshot(TakeSnapshot takeSnapshot) {
        if (DeviceUtil.isNavdyDevice()) {
            sLogger.d("Request to take snap shot");
            try {
                takeSnapShot(SNAPSHOT_PATH);
            } catch (IOException e) {
                sLogger.e("Exception while taking the snapshot ", e);
            }
        }
    }

    @Subscribe
    public void onInputPreferenceUpdate(final InputPreferences event) {
        if (DeviceUtil.isNavdyDevice() && !this.shuttingDown && !this.powerManager.inQuietMode()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    if (event.use_gestures.booleanValue()) {
                        if (GestureServiceConnector.this.swipedSocket != null) {
                            GestureServiceConnector.sLogger.v("already running");
                        } else {
                            GestureServiceConnector.this.start();
                        }
                    } else if (GestureServiceConnector.this.swipedSocket == null) {
                        GestureServiceConnector.sLogger.v("not running");
                    } else {
                        GestureServiceConnector.this.stop();
                    }
                }
            }, 1);
        }
    }

    @Subscribe
    public void onShutdown(Shutdown event) {
        if (event.state == State.CONFIRMED) {
            this.shuttingDown = true;
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    GestureServiceConnector.this.stop();
                }
            }, 1);
        }
    }

    private void closeSocket() {
        if (this.swipedSocket != null) {
            InputStream ip = null;
            try {
                ip = this.swipedSocket.getInputStream();
            } catch (Throwable th) {
            }
            IOUtils.closeStream(ip);
            OutputStream op = null;
            try {
                op = this.swipedSocket.getOutputStream();
            } catch (Throwable th2) {
            }
            IOUtils.closeStream(op);
            IOUtils.closeStream(this.swipedSocket);
            this.swipedSocket = null;
            sLogger.v("swipedconnector socket closed");
        }
    }

    private void closeThread() {
        if (this.swipedReader != null) {
            if (this.swipedReader.isAlive()) {
                sLogger.v("swipedconnector thread alive");
                this.swipedReader.interrupt();
                sLogger.v("swipedconnector thread waiting");
                try {
                    this.swipedReader.join();
                } catch (Throwable th) {
                }
                sLogger.v("swipedconnector thread waited");
            }
            this.swipedReader = null;
        }
    }
}
