package com.navdy.hud.app.service;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.net.LocalSocketAddress.Namespace;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.ArrayMap;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.app.device.light.LED;
import com.navdy.hud.app.event.DrivingStateChange;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.event.Shutdown.Reason;
import com.navdy.hud.app.event.Wakeup;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdConnectionStatusEvent;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.hudcontrol.AccelerateShutdown;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import mortar.Mortar;

public class ShutdownMonitor implements Runnable {
    private static final long ACCELERATED_TIMEOUT = TimeUnit.SECONDS.toMillis(30);
    private static final long ACTIVE_USE_TIMEOUT = TimeUnit.MINUTES.toMillis(3);
    private static final long BOOT_MODE_TIMEOUT = TimeUnit.MINUTES.toMillis(7);
    private static final float MOVEMENT_THRESHOLD = 60.0f;
    private static final long OBD_ACTIVE_TIMEOUT = TimeUnit.MINUTES.toMillis(20);
    private static final long OBD_DISCONNECT_DELAY = TimeUnit.SECONDS.toMillis(30);
    private static final long OBD_DISCONNECT_TIMEOUT = TimeUnit.SECONDS.toMillis(5);
    private static final long PENDING_SHUTDOWN_TIMEOUT = TimeUnit.MINUTES.toMillis(5);
    private static final long POWER_DISCONECT_SMOOTHING_TIMEOUT = TimeUnit.SECONDS.toMillis(3);
    private static long REPORT_INTERVAL = TimeUnit.MINUTES.toMillis(1);
    private static final long SAMPLING_INTERVAL = TimeUnit.SECONDS.toMillis(15);
    private static final String SHUTDOWND_REQUEST_CLICK = "powerclick";
    private static final String SHUTDOWND_REQUEST_DOUBLE_CLICK = "powerdoubleclick";
    private static final String SHUTDOWND_REQUEST_LONG_PRESS = "powerlongpress";
    private static final String SHUTDOWND_REQUEST_POWER_STATE = "powerstate";
    private static String SHUTDOWN_OVERRIDE_SETTING = "persist.sys.noautoshutdown";
    private static final String SOCKET_NAME = "shutdownd";
    private static String TEMPORARY_SHUTDOWN_OVERRIDE_SETTING = "sys.powerctl.noautoshutdown";
    private static ShutdownMonitor sInstance = null;
    private static final Logger sLogger = new Logger(ShutdownMonitor.class);
    @Inject
    Bus bus;
    private String lastWakeReason;
    private long mAccelerateTime = 0;
    private long mDialLastActivityTimeMsecs = 0;
    private DialManager mDialManager;
    private boolean mDriving = false;
    private Handler mHandler;
    private boolean mInactivityShutdownDisabled = false;
    @Inject
    InputManager mInputManager;
    private boolean mIsDialConnected = false;
    private long mLastInputEventTimeMsecs = 0;
    private long mLastMovementReport = 0;
    private long mLastMovementTimeMsecs = 0;
    private LocationListener mLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            final Location copy = new Location(location);
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    String provider = copy.getProvider();
                    if (provider == null) {
                        provider = "unknown";
                    }
                    Location movementBase = (Location) ShutdownMonitor.this.mMovementBases.get(provider);
                    if (movementBase == null) {
                        ShutdownMonitor.this.mMovementBases.put(provider, copy);
                        ShutdownMonitor.this.onMovement();
                    } else if (movementBase.distanceTo(copy) > 60.0f) {
                        ShutdownMonitor.this.mMovementBases.put(provider, copy);
                        ShutdownMonitor.this.onMovement();
                        long curTime = ShutdownMonitor.this.getCurrentTimeMsecs();
                        if (ShutdownMonitor.this.mLastMovementReport == 0 || curTime - ShutdownMonitor.this.mLastMovementReport >= ShutdownMonitor.REPORT_INTERVAL) {
                            ShutdownMonitor.this.mLastMovementReport = curTime;
                            ShutdownMonitor.sLogger.i("Moved significant distance - based on " + provider + " provider");
                        }
                    }
                }
            }, 1);
        }

        public void onStatusChanged(String var1, int var2, Bundle var3) {
        }

        public void onProviderEnabled(String var1) {
        }

        public void onProviderDisabled(String var1) {
        }
    };
    private boolean mMainPowerOn;
    private MonitorState mMonitorState = MonitorState.STATE_BOOT;
    private ArrayMap<String, Location> mMovementBases = new ArrayMap();
    private long mObdDisconnectTimeMsecs = 0;
    private ObdManager mObdManager;
    private long mRemoteDeviceConnectTimeMsecs = 0;
    private boolean mScreenDimmingDisabled = false;
    private LocalSocket mSocket;
    private InputStream mSocketInputStream;
    private OutputStream mSocketOutputStream;
    private long mStateChangeTime = 0;
    private boolean mUsbPowerOn = false;
    private double maxVoltage = 0.0d;
    private final NotificationReceiver notifyReceiver = new NotificationReceiver();
    private Runnable powerLossRunnable = null;
    @Inject
    PowerManager powerManager;

    public enum MonitorState {
        STATE_BOOT(0),
        STATE_OBD_ACTIVE(1),
        STATE_ACTIVE_USE(2),
        STATE_PENDING_SHUTDOWN(3),
        STATE_SHUTDOWN_PROMPT(4),
        STATE_QUIET_MODE(5);

        private int value;
        MonitorState(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    private class NotificationReceiver {

        private class ShutdownRunnable implements Runnable {
            private final Shutdown event;

            ShutdownRunnable(Shutdown r) {
                this.event = r;
            }

            public void run() {
                boolean fullShutdown;
                boolean charging;
                Reason reason = this.event.reason;
                HudApplication.getApplication().setShutdownReason(reason);
                if (ShutdownMonitor.this.powerManager.quietModeEnabled()) {
                    fullShutdown = false;
                } else {
                    fullShutdown = true;
                }
                double batteryVoltage = ShutdownMonitor.this.mObdManager.getBatteryVoltage();
                boolean autoOnEnabled = ShutdownMonitor.this.mObdManager.getObdDeviceConfigurationManager().isAutoOnEnabled();
                if (batteryVoltage >= 13.100000381469727d) {
                    charging = true;
                } else {
                    charging = false;
                }
                switch (reason) {
                    case POWER_LOSS:
                    case CRITICAL_VOLTAGE:
                    case LOW_VOLTAGE:
                    case TIMEOUT:
                    case HIGH_TEMPERATURE:
                        fullShutdown = true;
                        break;
                }
                ShutdownMonitor.sLogger.i("Shutting down, auto-on:" + autoOnEnabled + " voltage:" + batteryVoltage + " charging:" + charging);
                if (!autoOnEnabled || charging) {
                    fullShutdown = true;
                } else {
                    if (ShutdownMonitor.this.mObdManager.isSleeping()) {
                        ShutdownMonitor.this.mObdManager.wakeup();
                        GenericUtil.sleep(1000);
                    }
                    ShutdownMonitor.this.mObdManager.sleep(fullShutdown);
                }
                ShutdownMonitor.this.powerManager.androidShutdown(reason, fullShutdown);
            }
        }

        private NotificationReceiver() {
        }

//        /* synthetic */ NotificationReceiver(ShutdownMonitor x0, AnonymousClass1 x1) {
//            this();
//        }

        private void triggerShutdown(Shutdown event) {
            TaskManager.getInstance().execute(new ShutdownRunnable(event), 1);
        }

        @Subscribe
        public void onShutdown(final Shutdown event) {
            switch (event.state) {
                case CANCELED:
                    ShutdownMonitor.this.enterActiveUseState();
                    return;
                case CONFIRMED:
                    ShutdownMonitor.this.mHandler.post(new Runnable() {
                        public void run() {
                            NotificationReceiver.this.triggerShutdown(event);
                        }
                    });
                    return;
                default:
                    return;
            }
        }

        @Subscribe
        public void onWakeup(Wakeup event) {
            ShutdownMonitor.this.enterBootState();
        }

        @Subscribe
        public void ObdStateChangeEvent(ObdConnectionStatusEvent event) {
            if (event.connected) {
                ShutdownMonitor.this.mObdDisconnectTimeMsecs = 0;
                ShutdownMonitor.this.mAccelerateTime = 0;
                return;
            }
            ShutdownMonitor.this.mObdDisconnectTimeMsecs = ShutdownMonitor.this.getCurrentTimeMsecs();
            ShutdownMonitor.this.mHandler.removeCallbacks(ShutdownMonitor.this);
            ShutdownMonitor.this.mHandler.postDelayed(ShutdownMonitor.this, ShutdownMonitor.OBD_DISCONNECT_TIMEOUT);
        }

        @Subscribe
        public void onConnectionStateChange(ConnectionStateChange event) {
            if (event.state == ConnectionState.CONNECTION_VERIFIED) {
                ShutdownMonitor.this.mRemoteDeviceConnectTimeMsecs = ShutdownMonitor.this.getCurrentTimeMsecs();
                ShutdownMonitor.this.mAccelerateTime = 0;
                ShutdownMonitor.this.onWakeEvent();
            } else if (event.state == ConnectionState.CONNECTION_DISCONNECTED) {
                ShutdownMonitor.this.mRemoteDeviceConnectTimeMsecs = 0;
            }
        }

        @Subscribe
        public void onAccelerateShutdown(AccelerateShutdown event) {
            ShutdownMonitor.sLogger.v(String.format("received AccelerateShutdown, reason = %s", new Object[]{event.reason}));
            ShutdownMonitor.this.mAccelerateTime = ShutdownMonitor.this.getCurrentTimeMsecs();
        }

        @Subscribe
        public void onDrivingStateChange(DrivingStateChange event) {
            ShutdownMonitor.this.mDriving = event.driving;
            if (ShutdownMonitor.this.mDriving) {
                ShutdownMonitor.this.onMovement();
            }
        }
    }

    public static synchronized ShutdownMonitor getInstance() {
        ShutdownMonitor shutdownMonitor;
        synchronized (ShutdownMonitor.class) {
            if (sInstance == null) {
                sInstance = new ShutdownMonitor();
                sInstance.init();
            }
            shutdownMonitor = sInstance;
        }
        return shutdownMonitor;
    }

    private long getCurrentTimeMsecs() {
        return SystemClock.elapsedRealtime();
    }

    private synchronized void establishSocketConnection() {
        this.mSocket = new LocalSocket();
        try {
            this.mSocket.connect(new LocalSocketAddress(SOCKET_NAME, Namespace.RESERVED));
            this.mSocketOutputStream = this.mSocket.getOutputStream();
            this.mSocketInputStream = this.mSocket.getInputStream();
        } catch (Exception e) {
            sLogger.e("exception while attempting to connect to shutdownd socket", e);
            closeSocketConnection();
        }
    }

    private synchronized void closeSocketConnection() {
        IOUtils.closeStream(this.mSocketInputStream);
        IOUtils.closeStream(this.mSocketOutputStream);
        IOUtils.closeStream(this.mSocket);
        this.mSocket = null;
        this.mSocketInputStream = null;
        this.mSocketOutputStream = null;
    }

    private synchronized OutputStream getSocketOutputStream() {
        if (this.mSocketOutputStream == null) {
            establishSocketConnection();
        }
        return this.mSocketOutputStream;
    }

    private synchronized InputStream getSocketInputStream() {
        if (this.mSocketInputStream == null) {
            establishSocketConnection();
        }
        return this.mSocketInputStream;
    }

    private ShutdownMonitor() {
        if (DeviceUtil.isNavdyDevice()) {
            long curTimeMsecs = getCurrentTimeMsecs();
            this.mHandler = new Handler(Looper.getMainLooper());
            this.mDialManager = DialManager.getInstance();
            this.mObdManager = ObdManager.getInstance();
            this.mLastInputEventTimeMsecs = curTimeMsecs;
            this.mDialLastActivityTimeMsecs = curTimeMsecs;
            this.mObdDisconnectTimeMsecs = 0;
            this.mLastMovementTimeMsecs = 0;
            this.mRemoteDeviceConnectTimeMsecs = 0;
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    ShutdownMonitor.this.establishSocketConnection();
                    final ShutdownMonitor a = ShutdownMonitor.this;
                    new Thread("ShutdownMonitor MessageReceiver thread") {
                        final ShutdownMonitor this$0 = a;

                        public void run() {
                            byte[] a = new byte[100];
                            Charset a0 = Charset.forName("US-ASCII");
                            int i = 0;
                            String s = "";
                            while(true) {
                                int i0 = 0;
                                Exception a1 = null;
                                try {
                                    i0 = -1;
                                    InputStream a2 = this.this$0.getSocketInputStream();
                                    if (a2 == null) {
                                        i0 = -1;
                                    } else {
                                        i0 = -1;
                                        i = 0;
                                        i0 = a2.read(a);
                                        if (i0 >= 0) {
                                            i = 0;
                                        } else {
                                            i = 0;
                                            sLogger.e("end of file reading shutdownd socket");
                                            this.this$0.closeSocketConnection();
                                            i = 0;
                                        }
                                    }
                                } catch(Exception a3) {
                                    sLogger.e("exception reading shutdownd socket", (Throwable)a3);
                                    this.this$0.closeSocketConnection();
                                }
                                label0: {
                                    if (i0 >= 0) {
                                        try {
                                            s = new StringBuilder().append(s).append(new String(a, 0, i0, a0)).toString();
                                        } catch(Exception a4) {
                                            a1 = a4;
                                            break label0;
                                        }
                                        while(true) {
                                            String[] a5 = null;
                                            int i1 = 0;
                                            try {
                                                if (!s.contains((CharSequence)"\n")) {
                                                    break;
                                                }
                                                a5 = s.split("\n", 2);
                                            } catch(Exception a6) {
                                                a1 = a6;
                                                break label0;
                                            }
                                            String s0 = a5[0];
                                            try {
                                                i1 = a5.length;
                                            } catch(NullPointerException a7) {
                                                a1 = a7;
                                                break label0;
                                            }
                                            s = (i1 <= 1) ? "" : a5[1];
                                            try {
                                                sLogger.v("got command[" + s0 + "]");
                                                if ("powerclick".equals(s0)) {
                                                    this.this$0.mInputManager.injectKey(CustomKeyEvent.POWER_BUTTON_CLICK);
                                                } else if ("powerdoubleclick".equals(s0)) {
                                                    this.this$0.mInputManager.injectKey(CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK);
                                                } else if ("powerlongpress".equals(s0)) {
                                                    this.this$0.mInputManager.injectKey(CustomKeyEvent.POWER_BUTTON_LONG_PRESS);
                                                } else if (s0.startsWith("powerstate")) {
                                                    String s1 = s0.substring("powerstate".length() + 1).trim();
                                                    sLogger.d("State : " + s1);
                                                    int i2 = s1.charAt(0);
                                                    boolean b = i2 != 48;
                                                    int i3 = s1.charAt(1);
                                                    boolean b0 = i3 != 48;
                                                    sLogger.d("USB on : " + b + " , Main on : " + b0);
                                                    label1: {
                                                        label3: {
                                                            label4: {
                                                                if (b) {
                                                                    break label4;
                                                                }
                                                                if (!b0) {
                                                                    break label3;
                                                                }
                                                            }
                                                            if (this.this$0.mMainPowerOn) {
                                                                break label1;
                                                            }
                                                            if (!this.this$0.mUsbPowerOn && this.this$0.powerLossRunnable != null) {
                                                                this.this$0.mHandler.removeCallbacks(this.this$0.powerLossRunnable);
                                                                this.this$0.powerLossRunnable = null;
                                                            }
                                                            this.this$0.mObdManager.updateListener();
                                                            break label1;
                                                        }
                                                        boolean b1 = this.this$0.mMainPowerOn;
                                                        label2: {
                                                            if (b1) {
                                                                break label2;
                                                            }
                                                            if (!this.this$0.mUsbPowerOn) {
                                                                break label1;
                                                            }
                                                        }
                                                        final Thread a2 = this;
                                                        this.this$0.powerLossRunnable = new Runnable() {

                                                            public void run() {
                                                                if (!mMainPowerOn && !mUsbPowerOn) {
                                                                    ShutdownMonitor.this.bus.post(new Shutdown(Reason.POWER_LOSS));
                                                                }
                                                            }
                                                        };
                                                        this.this$0.mHandler.postDelayed(this.this$0.powerLossRunnable, POWER_DISCONECT_SMOOTHING_TIMEOUT);
                                                    }
                                                    this.this$0.mMainPowerOn = b0;
                                                    this.this$0.mUsbPowerOn = b;
                                                } else {
                                                    sLogger.e("invalid command from shutdownd socket: " + s0);
                                                }
                                            } catch(Exception a8) {
                                                a1 = a8;
                                                break label0;
                                            }
                                        }
                                    } else {
                                        String s2 = null;
                                        i = i + 1;
                                        if (i > 3) {
                                            sLogger.e("MessageReceiver thread terminating");
                                            return;
                                        }
                                        try {
                                            long j = (long)(i * 10000);
                                            s2 = s;
                                            Thread.sleep(j);
                                        } catch(InterruptedException ignoredException) {
                                            s = s2;
                                        }
                                    }
                                    continue;
                                }
                                sLogger.e("exception while processing data from shutdownd", (Throwable)a1);
                                s = "";
                            }
                        }
                    }.start();
                    ShutdownMonitor.this.mHandler.postDelayed(ShutdownMonitor.this, ShutdownMonitor.SAMPLING_INTERVAL);
                }
            }, 1);
            return;
        }
        sLogger.w("not a Navdy device, ShutdownMonitor will not run");
    }

    private boolean canDoShutdown() {
        boolean shutdownDisabled;
        if (SystemProperties.getBoolean(SHUTDOWN_OVERRIDE_SETTING, false)) {
            sLogger.w("SHUTDOWN_OVERRIDE_SETTING, can't shutdown");
            shutdownDisabled = true;
        } else if (SystemProperties.getBoolean(TEMPORARY_SHUTDOWN_OVERRIDE_SETTING, false)) {
            sLogger.w("TEMPORARY_SHUTDOWN_OVERRIDE_SETTING, can't shutdown");
            shutdownDisabled = true;
        } else if (this.mUsbPowerOn) {
            sLogger.w("mUsbPowerOn, can't shutdown");
            shutdownDisabled = true;
        } else {
            shutdownDisabled = false;
        }
        return !shutdownDisabled;
    }

    private void init() {
        Mortar.inject(HudApplication.getAppContext(), this);
        Looper looper = Looper.getMainLooper();
        LocationManager locationManager = (LocationManager) HudApplication.getAppContext().getSystemService("location");
        try {
            locationManager.requestLocationUpdates("gps", 0, 0.0f, this.mLocationListener, looper);
            locationManager.requestLocationUpdates("network", 0, 0.0f, this.mLocationListener, looper);
        } catch (IllegalArgumentException e) {
            sLogger.e("failed to register with GPS or Network provider");
        }
        this.bus.register(this.notifyReceiver);
        if (this.powerManager.inQuietMode()) {
            enterQuiteModeState();
        } else {
            enterBootState();
        }
    }

    private void onMovement() {
        this.mLastMovementTimeMsecs = getCurrentTimeMsecs();
        this.mAccelerateTime = 0;
        onWakeEvent();
    }

    private void onWakeEvent() {
        if (this.mMonitorState == MonitorState.STATE_SHUTDOWN_PROMPT || this.mMonitorState == MonitorState.STATE_PENDING_SHUTDOWN) {
            enterActiveUseState();
        }
    }

    public void recordInputEvent() {
        this.mLastInputEventTimeMsecs = getCurrentTimeMsecs();
        if (this.mMonitorState == MonitorState.STATE_PENDING_SHUTDOWN) {
            enterActiveUseState();
        }
    }

    private void updateStats() {
        long curTimeMsecs = getCurrentTimeMsecs();
        boolean connected = this.mDialManager.isDialConnected();
        if (connected && !this.mIsDialConnected) {
            this.mDialLastActivityTimeMsecs = curTimeMsecs;
        }
        this.mIsDialConnected = connected;
    }

    private long checkObdConnection() {
        return this.mObdDisconnectTimeMsecs == 0 ? 0 : getCurrentTimeMsecs() - this.mObdDisconnectTimeMsecs;
    }

    private void setMonitorState(MonitorState state) {
        switch (this.mMonitorState) {
            case STATE_PENDING_SHUTDOWN:
                leavePendingShutdownState();
                break;
            case STATE_SHUTDOWN_PROMPT:
                leaveShutdownPromptState();
                break;
        }
        sLogger.v(String.format("setting monitor state to %s", new Object[]{state}));
        this.mMonitorState = state;
        this.mStateChangeTime = getCurrentTimeMsecs();
    }

    private void enterBootState() {
        setMonitorState(MonitorState.STATE_BOOT);
    }

    private void enterObdActiveState() {
        setMonitorState(MonitorState.STATE_OBD_ACTIVE);
    }

    private void enterActiveUseState() {
        setMonitorState(MonitorState.STATE_ACTIVE_USE);
    }

    private void enterPendingShutdownState() {
        if (canDoShutdown()) {
            LED.writeToSysfs("0", "/sys/dlpc/led_enable");
            setMonitorState(MonitorState.STATE_PENDING_SHUTDOWN);
            return;
        }
        sLogger.v("pending shutdown state disabled by property setting");
    }

    private void leavePendingShutdownState() {
        LED.writeToSysfs(ToastPresenter.EXTRA_MAIN_TITLE, "/sys/dlpc/led_enable");
    }

    private void enterShutdownPromptState(Reason reason) {
        this.bus.post(new Shutdown(reason));
        setMonitorState(MonitorState.STATE_SHUTDOWN_PROMPT);
    }

    private void enterShutdownPromptStateIfNotDisabled(Reason reason) {
        boolean shutdownAllowed = canDoShutdown();
        if (this.mInactivityShutdownDisabled || !shutdownAllowed) {
            sLogger.v(String.format("shutdown prompt state disabled, shutdownAllowed = %b, mInactivityShutdownDisabled = %b, mUsbPowerOn = %b", shutdownAllowed, this.mInactivityShutdownDisabled, this.mUsbPowerOn));
            return;
        }
        enterShutdownPromptState(reason);
    }

    private void leaveShutdownPromptState() {
        this.mAccelerateTime = 0;
    }

    private void enterQuiteModeState() {
        setMonitorState(MonitorState.STATE_QUIET_MODE);
    }

    public void run() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                ShutdownMonitor.this.updateStats();
                ShutdownMonitor.this.mHandler.postDelayed(ShutdownMonitor.this, ShutdownMonitor.this.evaluateState());
            }
        }, 10);
    }

    public void disableInactivityShutdown(boolean disable) {
        this.mInactivityShutdownDisabled = disable;
    }

    public void disableScreenDim(boolean disable) {
        this.mScreenDimmingDisabled = disable;
        recordInputEvent();
        Logger logger = sLogger;
        String str = "screen dimming %s";
        Object[] objArr = new Object[1];
        objArr[0] = disable ? "disabled" : "enabled";
        logger.v(String.format(str, objArr));
    }

    private long evaluateState() {
        long curTime = getCurrentTimeMsecs();
        long nextTimeout = SAMPLING_INTERVAL;
        double curVoltage = this.mObdManager.getBatteryVoltage();
        if (curVoltage > this.maxVoltage) {
            this.maxVoltage = curVoltage;
        }
        switch (this.mMonitorState) {
            case STATE_PENDING_SHUTDOWN:
                long wakeEventTime = lastWakeEventTime();
                if (this.mObdManager.isConnected()) {
                    enterObdActiveState();
                    return nextTimeout;
                } else if (wakeEventTime > this.mStateChangeTime) {
                    sLogger.i("Wake reason:" + this.lastWakeReason);
                    enterActiveUseState();
                    return nextTimeout;
                } else if (this.mAccelerateTime == 0 && curTime - wakeEventTime < PENDING_SHUTDOWN_TIMEOUT) {
                    return nextTimeout;
                } else {
                    enterShutdownPromptStateIfNotDisabled(Reason.INACTIVITY);
                    return nextTimeout;
                }
            case STATE_BOOT:
                if (this.mObdManager.isConnected()) {
                    enterObdActiveState();
                    return nextTimeout;
                } else if (curTime - this.mStateChangeTime <= BOOT_MODE_TIMEOUT) {
                    return nextTimeout;
                } else {
                    enterActiveUseState();
                    return nextTimeout;
                }
            case STATE_OBD_ACTIVE:
                long obdDiscMsec = checkObdConnection();
                if (obdDiscMsec >= OBD_DISCONNECT_TIMEOUT) {
                    sLogger.i(String.format("OBD disconnected, driving = %b, battery voltage = %5.1f, max voltage = %5.1f", new Object[]{Boolean.valueOf(this.mDriving), Double.valueOf(curVoltage), Double.valueOf(this.maxVoltage)}));
                    if (!this.mDriving && curVoltage != -1.0d && curVoltage <= 12.899999618530273d) {
                        enterShutdownPromptStateIfNotDisabled(Reason.ENGINE_OFF);
                        return nextTimeout;
                    } else if (obdDiscMsec < OBD_DISCONNECT_DELAY) {
                        return Math.min(TimeUnit.SECONDS.toMillis(5), OBD_DISCONNECT_DELAY - obdDiscMsec);
                    } else {
                        if (this.mDriving) {
                            enterActiveUseState();
                            return nextTimeout;
                        }
                        enterPendingShutdownState();
                        return nextTimeout;
                    }
                } else if (curTime - lastWakeEventTime() < OBD_ACTIVE_TIMEOUT || this.mDriving) {
                    return nextTimeout;
                } else {
                    enterShutdownPromptStateIfNotDisabled(Reason.ENGINE_OFF);
                    return nextTimeout;
                }
            case STATE_ACTIVE_USE:
                if (this.mObdManager.isConnected()) {
                    enterObdActiveState();
                    return nextTimeout;
                } else if (this.mAccelerateTime != 0) {
                    long wt = lastWakeEventTime();
                    if (curTime - wt < ACCELERATED_TIMEOUT || this.mDriving) {
                        return (ACCELERATED_TIMEOUT + wt) - curTime;
                    }
                    enterShutdownPromptStateIfNotDisabled(Reason.ACCELERATE_SHUTDOWN);
                    return nextTimeout;
                } else if (this.mScreenDimmingDisabled || curTime - lastWakeEventTime() <= ACTIVE_USE_TIMEOUT || this.mDriving) {
                    return nextTimeout;
                } else {
                    sLogger.i("Active use timeout - last wake event was " + this.lastWakeReason);
                    enterPendingShutdownState();
                    return nextTimeout;
                }
            default:
                return nextTimeout;
        }
    }

    private long lastWakeEventTime() {
        long lastDownloadTime;
        updateStats();
        long currentTime = getCurrentTimeMsecs();
        long lastWakeEventTime = this.mStateChangeTime;
        this.lastWakeReason = "stateChange";
        if (this.mDialLastActivityTimeMsecs > lastWakeEventTime) {
            lastWakeEventTime = this.mDialLastActivityTimeMsecs;
            this.lastWakeReason = "dial";
        }
        if (this.mLastInputEventTimeMsecs > lastWakeEventTime) {
            lastWakeEventTime = this.mLastInputEventTimeMsecs;
            this.lastWakeReason = "input";
        }
        if (this.mRemoteDeviceConnectTimeMsecs > lastWakeEventTime) {
            lastWakeEventTime = this.mRemoteDeviceConnectTimeMsecs;
            this.lastWakeReason = GpsConstants.USING_PHONE_LOCATION;
        }
        if (this.mLastMovementTimeMsecs > lastWakeEventTime) {
            lastWakeEventTime = this.mLastMovementTimeMsecs;
            this.lastWakeReason = "movement";
        }
        DriveRecorder odr = this.mObdManager.getDriveRecorder();
        if (odr != null && odr.isDemoPlaying()) {
            lastWakeEventTime = currentTime;
            this.lastWakeReason = "recording";
        }
        String downloading = SystemProperties.get(FileTransferHandler.OTA_DOWLOADING_PROPERTY, "0");
        try {
            lastDownloadTime = ((long) Integer.parseInt(downloading)) * 1000;
        } catch (NumberFormatException e) {
            sLogger.e(String.format("%s property has invalid value: %s", new Object[]{FileTransferHandler.OTA_DOWLOADING_PROPERTY, downloading}));
            lastDownloadTime = 0;
        }
        if (lastDownloadTime <= lastWakeEventTime) {
            return lastWakeEventTime;
        }
        lastWakeEventTime = lastDownloadTime;
        this.lastWakeReason = "ota";
        return lastWakeEventTime;
    }
}
