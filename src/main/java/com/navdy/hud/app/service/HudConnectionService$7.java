package com.navdy.hud.app.service;

class HudConnectionService$7 {
    final static int[] $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State;
    final static int[] $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType;
    final static int[] $SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action;
    final static int[] $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
    
    static {
        $SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action = new int[com.navdy.service.library.events.connection.ConnectionRequest.Action.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action;
        com.navdy.service.library.events.connection.ConnectionRequest.Action a0 = com.navdy.service.library.events.connection.ConnectionRequest.Action.CONNECTION_SELECT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action[com.navdy.service.library.events.connection.ConnectionRequest.Action.CONNECTION_START_SEARCH.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionRequest$Action[com.navdy.service.library.events.connection.ConnectionRequest.Action.CONNECTION_STOP_SEARCH.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType = new int[com.navdy.service.library.events.NavdyEvent.MessageType.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType;
        com.navdy.service.library.events.NavdyEvent.MessageType a2 = com.navdy.service.library.events.NavdyEvent.MessageType.Coordinate;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.StartDriveRecordingEvent.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.StopDriveRecordingEvent.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.DriveRecordingsRequest.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.StartDrivePlaybackEvent.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.StopDrivePlaybackEvent.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.FileTransferRequest.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.FileTransferStatus.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException9) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.FileTransferData.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException10) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.ConnectionStateChange.ordinal()] = 10;
        } catch(NoSuchFieldError ignoredException11) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$NavdyEvent$MessageType[com.navdy.service.library.events.NavdyEvent.MessageType.AudioStatus.ordinal()] = 11;
        } catch(NoSuchFieldError ignoredException12) {
        }
        $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState = new int[com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.values().length];
        int[] a3 = $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState;
        com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState a4 = com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED;
        try {
            a3[a4.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException13) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException14) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException15) {
        }
        $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State = new int[com.navdy.service.library.device.connection.ConnectionService$State.values().length];
        int[] a5 = $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State;
        com.navdy.service.library.device.connection.ConnectionService$State a6 = com.navdy.service.library.device.connection.ConnectionService$State.START;
        try {
            a5[a6.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException16) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.IDLE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException17) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.SEARCHING.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException18) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.CONNECTED.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException19) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.CONNECTING.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException20) {
        }
        try {
            $SwitchMap$com$navdy$service$library$device$connection$ConnectionService$State[com.navdy.service.library.device.connection.ConnectionService$State.RECONNECTING.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException21) {
        }
    }
}
