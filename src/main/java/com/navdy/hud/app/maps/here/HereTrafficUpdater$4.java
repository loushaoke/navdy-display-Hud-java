package com.navdy.hud.app.maps.here;

class HereTrafficUpdater$4 implements com.here.android.mpa.guidance.TrafficUpdater.GetEventsListener {
    final com.navdy.hud.app.maps.here.HereTrafficUpdater this$0;
    
    HereTrafficUpdater$4(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {

        super();
        this.this$0 = a;
    }
    
    public void onComplete(java.util.List a, com.here.android.mpa.guidance.TrafficUpdater.Error a0) {
        if (com.navdy.hud.app.maps.here.HereTrafficUpdater.access$200(this.this$0) != null) {
            label0: if (a0 != com.here.android.mpa.guidance.TrafficUpdater.Error.NONE) {
                com.navdy.hud.app.maps.here.HereTrafficUpdater.access$400(this.this$0).i(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficUpdater.access$300(this.this$0)).append(" error ").append(a0.name()).append(" while getting TrafficEvents.").toString());
            } else {
                com.navdy.hud.app.maps.here.HereTrafficUpdater$State a1 = com.navdy.hud.app.maps.here.HereTrafficUpdater.access$800(this.this$0);
                com.navdy.hud.app.maps.here.HereTrafficUpdater$State a2 = com.navdy.hud.app.maps.here.HereTrafficUpdater$State.ACTIVE;
                label1: {
                    if (a1 == a2) {
                        break label1;
                    }
                    if (com.navdy.hud.app.maps.here.HereTrafficUpdater.access$800(this.this$0) != com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_DISTANCE) {
                        break label0;
                    }
                }
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereTrafficUpdater$4$1(this, a), 2);
            }
            com.navdy.hud.app.maps.here.HereTrafficUpdater.access$700(this.this$0);
        } else {
            com.navdy.hud.app.maps.here.HereTrafficUpdater.access$400(this.this$0).w(new StringBuilder().append(com.navdy.hud.app.maps.here.HereTrafficUpdater.access$300(this.this$0)).append("onGetEventsListener triggered and currentRoute is null").toString());
        }
    }
}
