package com.navdy.hud.app.maps.here;

class HereNavigationManager$9$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager$9 this$1;
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$nextRoute;
    
    HereNavigationManager$9$1(com.navdy.hud.app.maps.here.HereNavigationManager$9 a, com.navdy.service.library.events.navigation.NavigationRouteRequest a0) {

        super();
        this.this$1 = a;
        this.val$nextRoute = a0;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereNavigationManager.access$500(this.this$1.this$0).post(this.val$nextRoute);
        com.navdy.hud.app.maps.here.HereNavigationManager.access$500(this.this$1.this$0).post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)this.val$nextRoute));
    }
}
