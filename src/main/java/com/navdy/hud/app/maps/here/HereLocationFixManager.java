package com.navdy.hud.app.maps.here;
import android.content.Context;
import com.squareup.otto.Subscribe;

import com.navdy.hud.app.R;

public class HereLocationFixManager {
    final private static java.text.SimpleDateFormat DATE_FORMAT;
    final private static byte[] RECORD_MARKER;
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.location.LocationListener androidGpsLocationListener;
    final private com.squareup.otto.Bus bus;
    private Runnable checkLocationFix;
    private long counter;
    private android.location.LocationListener debugTTSLocationListener;
    private boolean firstLocationFixCheck;
    private com.here.android.mpa.common.GeoPosition geoPosition;
    private android.os.Handler handler;
    private volatile boolean hereGpsSignal;
    private volatile boolean isRecording;
    private android.location.Location lastAndroidLocation;
    private long lastAndroidLocationPostTime;
    private long lastAndroidLocationTime;
    private com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch lastGpsSwitchEvent;
    private long lastHerelocationUpdateTime;
    private volatile long lastLocationTime;
    private volatile boolean locationFix;
    final private android.location.LocationListener locationListener;
    private com.here.android.mpa.common.PositioningManager.LocationMethod locationMethod;
    private com.here.android.mpa.mapping.MapMarker rawLocationMarker;
    private boolean rawLocationMarkerAdded;
    private java.io.FileOutputStream recordingFile;
    private volatile String recordingLabel;
    final private com.navdy.hud.app.manager.SpeedManager speedManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereLocationFixManager.class);
        DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH_mm_ss.SSS", java.util.Locale.US);
        RECORD_MARKER = "<< Marker >>\n".getBytes();
    }
    
    HereLocationFixManager(com.squareup.otto.Bus a) {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.firstLocationFixCheck = true;
        this.checkLocationFix = (Runnable)new com.navdy.hud.app.maps.here.HereLocationFixManager$1(this);
        this.locationListener = (android.location.LocationListener)new com.navdy.hud.app.maps.here.HereLocationFixManager$2(this);
        this.androidGpsLocationListener = null;
        this.debugTTSLocationListener = null;
        this.bus = a;
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        android.location.LocationManager a0 = (android.location.LocationManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);
        android.os.Looper a1 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getBkLocationReceiverLooper();
        if (com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled() && com.navdy.hud.app.util.DeviceUtil.isNavdyDevice() && a0.getProvider("gps") != null) {
            this.debugTTSLocationListener = (android.location.LocationListener)new com.navdy.hud.app.maps.here.HereLocationFixManager$3(this, a0, android.os.SystemClock.elapsedRealtime());
            a0.requestLocationUpdates("gps", 0L, 0.0f, this.debugTTSLocationListener, a1);
        }
        android.location.LocationProvider a2 = a0.getProvider("NAVDY_GPS_PROVIDER");
        android.location.Location a3 = null;
        if (a2 != null) {
            a3 = a0.getLastKnownLocation("NAVDY_GPS_PROVIDER");
            a0.requestLocationUpdates("NAVDY_GPS_PROVIDER", 0L, 0.0f, this.locationListener, a1);
            sLogger.v("installed gps listener");
        }
        if (a0.getProvider("gps") != null) {
            this.androidGpsLocationListener = (android.location.LocationListener)new com.navdy.hud.app.maps.here.HereLocationFixManager$4(this);
            a0.requestLocationUpdates("gps", 0L, 0.0f, this.androidGpsLocationListener, a1);
        }
        sLogger.v("installed gps listener");
        if (a0.getProvider("network") != null) {
            if (a3 == null) {
                a3 = a0.getLastKnownLocation("network");
            }
            a0.requestLocationUpdates("network", 0L, 0.0f, this.locationListener, a1);
            sLogger.v("installed n/w listener");
        }
        sLogger.v(new StringBuilder().append("got last location from location manager:").append(a3).toString());
        if (!this.locationFix && a3 != null) {
            sLogger.v("sending to map");
            this.setMaptoLocation(a3);
        }
        this.bus.register(this);
        this.handler.postDelayed(this.checkLocationFix, 1000L);
        com.navdy.service.library.events.debug.StartDriveRecordingEvent a4 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().getDriverRecordingEvent();
        if (a4 != null) {
            this.onStartDriveRecording(a4);
        }
        sLogger.v("initialized");
    }
    
    static long access$000(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.lastAndroidLocationTime;
    }
    
    static long access$002(com.navdy.hud.app.maps.here.HereLocationFixManager a, long j) {
        a.lastAndroidLocationTime = j;
        return j;
    }
    
    static boolean access$100(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.locationFix;
    }
    
    static boolean access$1000(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.isRecording;
    }
    
    static boolean access$1002(com.navdy.hud.app.maps.here.HereLocationFixManager a, boolean b) {
        a.isRecording = b;
        return b;
    }
    
    static boolean access$102(com.navdy.hud.app.maps.here.HereLocationFixManager a, boolean b) {
        a.locationFix = b;
        return b;
    }
    
    static void access$1100(com.navdy.hud.app.maps.here.HereLocationFixManager a, android.location.Location a0) {
        a.recordRawLocation(a0);
    }
    
    static long access$1200(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.lastAndroidLocationPostTime;
    }
    
    static long access$1202(com.navdy.hud.app.maps.here.HereLocationFixManager a, long j) {
        a.lastAndroidLocationPostTime = j;
        return j;
    }
    
    static void access$1300(com.navdy.hud.app.maps.here.HereLocationFixManager a, android.location.Location a0) {
        a.sendLocation(a0);
    }
    
    static String access$1400(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.recordingLabel;
    }
    
    static String access$1402(com.navdy.hud.app.maps.here.HereLocationFixManager a, String s) {
        a.recordingLabel = s;
        return s;
    }
    
    static java.io.FileOutputStream access$1500(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.recordingFile;
    }
    
    static java.io.FileOutputStream access$1502(com.navdy.hud.app.maps.here.HereLocationFixManager a, java.io.FileOutputStream a0) {
        a.recordingFile = a0;
        return a0;
    }
    
    static byte[] access$1600() {
        return RECORD_MARKER;
    }
    
    static com.navdy.service.library.log.Logger access$200() {
        return sLogger;
    }
    
    static com.squareup.otto.Bus access$300(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.bus;
    }
    
    static android.location.Location access$400(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.lastAndroidLocation;
    }
    
    static android.location.Location access$402(com.navdy.hud.app.maps.here.HereLocationFixManager a, android.location.Location a0) {
        a.lastAndroidLocation = a0;
        return a0;
    }
    
    static boolean access$500(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.firstLocationFixCheck;
    }
    
    static boolean access$502(com.navdy.hud.app.maps.here.HereLocationFixManager a, boolean b) {
        a.firstLocationFixCheck = b;
        return b;
    }
    
    static Runnable access$600(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.checkLocationFix;
    }
    
    static android.os.Handler access$700(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.handler;
    }
    
    static com.here.android.mpa.mapping.MapMarker access$800(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.rawLocationMarker;
    }
    
    static boolean access$900(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        return a.rawLocationMarkerAdded;
    }
    
    private void recordHereLocation(com.here.android.mpa.common.GeoPosition a, com.here.android.mpa.common.PositioningManager.LocationMethod a0, boolean b) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereLocationFixManager$8(this, a, a0, b), 9);
    }
    
    private void recordRawLocation(android.location.Location a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereLocationFixManager$7(this, a), 9);
    }
    
    private void sendLocation(android.location.Location a) {
        if (this.speedManager.setGpsSpeed(a.getSpeed(), a.getElapsedRealtimeNanos() / 1000000L)) {
            this.bus.post(new com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent());
        }
        this.lastLocationTime = android.os.SystemClock.elapsedRealtime();
        this.bus.post(a);
    }
    
    public void addMarkers(com.navdy.hud.app.maps.here.HereMapController a) {
        boolean b = com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled();
        label0: {
            Throwable a0 = null;
            if (!b) {
                break label0;
            }
            try {
                if (this.rawLocationMarker == null) {
                    this.rawLocationMarker = new com.here.android.mpa.mapping.MapMarker();
                    com.here.android.mpa.common.Image a1 = new com.here.android.mpa.common.Image();
                    a1.setImageResource(R.drawable.icon_position_raw_gps);
                    this.rawLocationMarker.setIcon(a1);
                    sLogger.v("marker created");
                }
                if (this.rawLocationMarkerAdded) {
                    break label0;
                }
                a.addMapObject((com.here.android.mpa.mapping.MapObject)this.rawLocationMarker);
                this.rawLocationMarkerAdded = true;
                sLogger.v("marker added");
                break label0;
            } catch(Throwable a2) {
                a0 = a2;
            }
            sLogger.e(a0);
        }
    }
    
    public com.here.android.mpa.common.GeoCoordinate getLastGeoCoordinate() {
        return (this.geoPosition != null) ? this.geoPosition.getCoordinate() : null;
    }
    
    public long getLastLocationTime() {
        return this.lastLocationTime;
    }
    
    public com.navdy.hud.app.maps.MapEvents$LocationFix getLocationFixEvent() {
        com.navdy.hud.app.maps.MapEvents$LocationFix a = null;
        if (this.locationFix) {
            boolean b = false;
            boolean b0 = false;
            if (this.lastAndroidLocation == null) {
                b = false;
                b0 = false;
            } else if (android.text.TextUtils.equals((CharSequence)this.lastAndroidLocation.getProvider(), (CharSequence)"NAVDY_GPS_PROVIDER")) {
                b = true;
                b0 = false;
            } else {
                b = false;
                b0 = true;
            }
            a = new com.navdy.hud.app.maps.MapEvents$LocationFix(true, b0, b);
        } else {
            a = new com.navdy.hud.app.maps.MapEvents$LocationFix(false, false, false);
        }
        return a;
    }
    
    public boolean hasLocationFix() {
        return this.locationFix;
    }
    
    public boolean isRecording() {
        return this.isRecording;
    }
    
    @Subscribe
    public void onGpsEventSwitch(com.navdy.hud.app.device.gps.GpsUtils.GpsSwitch a) {
        sLogger.v(new StringBuilder().append("Gps-Switch phone=").append(a.usingPhone).append(" ublox=").append(a.usingUblox).toString());
        this.lastGpsSwitchEvent = a;
    }
    
    @Subscribe
    public void onHereGpsEvent(com.navdy.hud.app.maps.MapEvents$GpsStatusChange a) {
        this.hereGpsSignal = a.connected;
        sLogger.v(new StringBuilder().append("here gps event connected [").append(this.hereGpsSignal).append("]").toString());
    }

    public void onHerePositionUpdated(com.here.android.mpa.common.PositioningManager.LocationMethod a, com.here.android.mpa.common.GeoPosition a0, boolean b) {
        if (sLogger.isLoggable(2)) {
            com.navdy.service.library.log.Logger a1 = sLogger;
            StringBuilder a2 = new StringBuilder().append("onHerePositionUpdated [");
            long j = this.counter + 1L;
            this.counter = j;
            a1.v(a2.append(j).append("] geoPosition[").append(a0.getCoordinate()).append("] method [").append(a.name()).append("] valid[").append(a0.isValid()).append("]").toString());
        }
        if (a != null && a0 != null) {
            this.locationMethod = a;
            this.geoPosition = a0;
            this.lastHerelocationUpdateTime = android.os.SystemClock.elapsedRealtime();
            if (this.isRecording) {
                this.recordHereLocation(a0, a, b);
            }
        }
    }
    
    @Subscribe
    public void onStartDriveRecording(com.navdy.service.library.events.debug.StartDriveRecordingEvent a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereLocationFixManager$5(this, a), 9);
    }
    
    @Subscribe
    public void onStopDriveRecording(com.navdy.service.library.events.debug.StopDriveRecordingEvent a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereLocationFixManager$6(this), 9);
    }
    
    public void recordMarker() {
        if (this.isRecording) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereLocationFixManager$9(this), 9);
        }
    }
    
    public void removeMarkers(com.navdy.hud.app.maps.here.HereMapController a) {
        if (com.navdy.hud.app.device.gps.GpsUtils.isDebugRawGpsPosEnabled() && this.rawLocationMarker != null && this.rawLocationMarkerAdded) {
            a.removeMapObject((com.here.android.mpa.mapping.MapObject)this.rawLocationMarker);
            this.rawLocationMarkerAdded = false;
            sLogger.v("marker removed");
        }
    }
    
    public void setMaptoLocation(android.location.Location a) {
        try {
            if (!this.locationFix && this.geoPosition == null) {
                sLogger.v(new StringBuilder().append("marking location fix:").append(a).toString());
                this.geoPosition = new com.here.android.mpa.common.GeoPosition(new com.here.android.mpa.common.GeoCoordinate(a.getLatitude(), a.getLongitude(), a.getAltitude()));
                this.locationMethod = com.here.android.mpa.common.PositioningManager.LocationMethod.NETWORK;
                this.lastHerelocationUpdateTime = android.os.SystemClock.elapsedRealtime();
                this.locationFix = true;
                this.bus.post(new com.navdy.hud.app.maps.MapEvents$LocationFix(true, true, false));
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    public void shutdown() {
        android.location.LocationManager a = (android.location.LocationManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("location");
        a.removeUpdates(this.locationListener);
        if (this.androidGpsLocationListener != null) {
            a.removeUpdates(this.androidGpsLocationListener);
        }
        if (this.debugTTSLocationListener != null) {
            a.removeUpdates(this.debugTTSLocationListener);
        }
    }
}
