package com.navdy.hud.app.maps.notification;

class RouteTimeUpdateNotification$1 implements com.navdy.hud.app.ui.component.ChoiceLayout2.IListener {
    final com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification this$0;
    
    RouteTimeUpdateNotification$1(com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void executeItem(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection a) {
        if (this.this$0.controller != null) {
            switch(a.id) {
                case 103: {
                    if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$000(this.this$0) == null) {
                        break;
                    }
                    com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$300(this.this$0);
                    break;
                }
                case 102: {
                    if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$000(this.this$0) == null) {
                        if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$200(this.this$0) != null) {
                            com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$202(this.this$0, (com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent)null);
                        }
                    } else {
                        com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$100(this.this$0).post(com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction.DISMISS);
                        com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$002(this.this$0, (com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent)null);
                    }
                    this.this$0.dismissNotification();
                    break;
                }
                case 101: {
                    if (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$000(this.this$0) != null) {
                        com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$100(this.this$0).post(com.navdy.hud.app.maps.MapEvents$TrafficRerouteAction.REROUTE);
                        com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification.access$002(this.this$0, (com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent)null);
                    }
                    this.this$0.dismissNotification();
                    break;
                }
                case 5: {
                    if (this.this$0.controller == null) {
                        break;
                    }
                    if (!this.this$0.controller.isExpandedWithStack()) {
                        break;
                    }
                    this.this$0.controller.collapseNotification(false, false);
                    break;
                }
            }
        }
    }
    
    public void itemSelected(com.navdy.hud.app.ui.component.ChoiceLayout2.Selection a) {
    }
}
