package com.navdy.hud.app.maps;

public enum MapEvents$DisplayTrafficIncident$Type {
    BLOCKING(0),
    VERY_HIGH(1),
    HIGH(2),
    NORMAL(3),
    FAILED(4),
    INACTIVE(5);

    private int value;
    MapEvents$DisplayTrafficIncident$Type(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MapEvents$DisplayTrafficIncident$Type extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type BLOCKING;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type FAILED;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type HIGH;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type INACTIVE;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type NORMAL;
//    final public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type VERY_HIGH;
//
//    static {
//        BLOCKING = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type("BLOCKING", 0);
//        VERY_HIGH = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type("VERY_HIGH", 1);
//        HIGH = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type("HIGH", 2);
//        NORMAL = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type("NORMAL", 3);
//        FAILED = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type("FAILED", 4);
//        INACTIVE = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type("INACTIVE", 5);
//        com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type[] a = new com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type[6];
//        a[0] = BLOCKING;
//        a[1] = VERY_HIGH;
//        a[2] = HIGH;
//        a[3] = NORMAL;
//        a[4] = FAILED;
//        a[5] = INACTIVE;
//        $VALUES = a;
//    }
//
//    private MapEvents$DisplayTrafficIncident$Type(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type[] values() {
//        return $VALUES.clone();
//    }
//}
