package com.navdy.hud.app.maps.widget;

class TrafficIncidentWidgetPresenter$2 {
    final static int[] $SwitchMap$com$navdy$hud$app$maps$MapEvents$DisplayTrafficIncident$Type;
    
    static {
        $SwitchMap$com$navdy$hud$app$maps$MapEvents$DisplayTrafficIncident$Type = new int[com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.values().length];
        com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type a0 = com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.NORMAL;
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$DisplayTrafficIncident$Type[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$DisplayTrafficIncident$Type[com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.INACTIVE.ordinal()] = 2;
        } catch(NoSuchFieldError ignored) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$maps$MapEvents$DisplayTrafficIncident$Type[com.navdy.hud.app.maps.MapEvents$DisplayTrafficIncident$Type.FAILED.ordinal()] = 3;
        } catch(NoSuchFieldError ignored) {
        }
    }
}
