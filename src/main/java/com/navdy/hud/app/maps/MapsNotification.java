package com.navdy.hud.app.maps;
import com.navdy.hud.app.R;

public class MapsNotification {
    final private static String MAP_ENGINE_NOT_INIT_ID = "maps-no-init";
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.MapsNotification.class);
    }
    
    public MapsNotification() {
    }
    
    public static void showMapsEngineNotInitializedToast() {
        sLogger.d("maps engine not init toast");
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        android.os.Bundle a0 = new android.os.Bundle();
        a0.putInt("13", 2000);
        a0.putInt("8", R.drawable.icon_mm_map);
        com.here.android.mpa.common.OnEngineInitListener.Error a1 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getError();
        String s = (a1 != null) ? a1.name() : "";
        a0.putString("4", a.getString(R.string.map_engine_not_ready));
        a0.putString("6", s);
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("maps-no-init", a0, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false));
    }
}
