package com.navdy.hud.app.maps.notification;
import android.view.View;

import com.navdy.hud.app.R;

public class RouteTimeUpdateNotification extends com.navdy.hud.app.maps.notification.BaseTrafficNotification {
    final private static int TAG_DISMISS = 102;
    final private static int TAG_GO = 101;
    final private static int TAG_READ = 103;
    private static StringBuilder ampmMarker;
    private static int badTrafficColor;
    private static String delay;
    private static java.util.List delayChoices;
    private static String done;
    private static String fasterRoute;
    private static String fasterRouteAvailable;
    private static String go;
    private static String hour;
    private static String hours;
    private static String hr;
    private static String min;
    private static String minute;
    private static String minutes;
    private static int normalTrafficColor;
    private static java.util.List reRouteChoices;
    private static android.content.res.Resources resources;
    private static String save;
    private static int trafficRerouteColor;
    private static String trafficUpdate;
    private static String via;
    private com.squareup.otto.Bus bus;
    private com.navdy.service.library.events.audio.CancelSpeechRequest cancelSpeechRequest;
    private com.navdy.hud.app.ui.component.ChoiceLayout2.IListener choiceListener;
    private android.view.ViewGroup container;
    private String distanceDiffStr;
    private String distanceStr;
    private String etaStr;
    private String etaUnitStr;
    private android.view.ViewGroup extendedContainer;
    private com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent fasterRouteEvent;
    private String id;
    private com.navdy.hud.app.ui.component.image.ColorImageView mainImage;
    private android.widget.TextView mainTitle;
    private android.widget.ImageView sideImage;
    private android.widget.TextView subTitle;
    private android.widget.TextView text1;
    private android.widget.TextView text2;
    private android.widget.TextView text3;
    private String timeStr;
    private String timeStrExpanded;
    private String timeUnitStr;
    private com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent trafficDelayEvent;
    private String ttsMessage;
    private com.navdy.hud.app.framework.notifications.NotificationType type;
    
    static {
        delayChoices = (java.util.List)new java.util.ArrayList(1);
        reRouteChoices = (java.util.List)new java.util.ArrayList(2);
        ampmMarker = new StringBuilder();
    }
    
    public RouteTimeUpdateNotification(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.squareup.otto.Bus a0) {
        this.etaStr = "";
        this.etaUnitStr = "";
        this.timeStr = "";
        this.timeUnitStr = "";
        this.timeStrExpanded = "";
        this.distanceStr = "";
        this.distanceDiffStr = "";
        this.choiceListener = (com.navdy.hud.app.ui.component.ChoiceLayout2.IListener)new com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification$1(this);
        this.id = s;
        this.type = a;
        this.cancelSpeechRequest = new com.navdy.service.library.events.audio.CancelSpeechRequest(s);
        if (trafficUpdate == null) {
            resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            trafficUpdate = resources.getString(R.string.traffic_notification_delay_title);
            fasterRoute = resources.getString(R.string.traffic_reroute_faster_route);
            delay = resources.getString(R.string.traffic_notification_delay);
            done = resources.getString(R.string.done);
            min = resources.getString(R.string.min);
            hr = resources.getString(R.string.hr);
            fasterRouteAvailable = resources.getString(R.string.traffic_faster_route_available);
            hour = resources.getString(R.string.hour);
            hours = resources.getString(R.string.hours);
            minute = resources.getString(R.string.minute);
            minutes = resources.getString(R.string.minutes);
            save = resources.getString(R.string.traffic_notification_save);
            go = resources.getString(R.string.traffic_reroute_go);
            via = resources.getString(R.string.traffic_reroute_via);
            normalTrafficColor = resources.getColor(R.color.traffic_good);
            badTrafficColor = resources.getColor(R.color.traffic_bad);
            trafficRerouteColor = resources.getColor(R.color.grey_4a);
            int i = resources.getColor(R.color.glance_dismiss);
            int i0 = resources.getColor(R.color.glance_ok_blue);
            int i1 = resources.getColor(R.color.glance_ok_go);
            String s0 = resources.getString(R.string.dismiss);
            delayChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(102, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, s0, i));
            reRouteChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(103, R.drawable.icon_glances_read, i0, R.drawable.icon_glances_read, -16777216, com.navdy.hud.app.framework.glance.GlanceConstants.read, i0));
            reRouteChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(101, R.drawable.icon_glances_ok_strong, i1, R.drawable.icon_glances_ok_strong, -16777216, go, i1));
            reRouteChoices.add(new com.navdy.hud.app.ui.component.ChoiceLayout2.Choice(102, R.drawable.icon_glances_dismiss, i, R.drawable.icon_glances_dismiss, -16777216, s0, i));
        }
        this.bus = a0;
        a0.register(this);
    }
    
    static com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent access$000(com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification a) {
        return a.fasterRouteEvent;
    }
    
    static com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent access$002(com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification a, com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent a0) {
        a.fasterRouteEvent = a0;
        return a0;
    }
    
    static com.squareup.otto.Bus access$100(com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification a) {
        return a.bus;
    }
    
    static com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent access$200(com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification a) {
        return a.trafficDelayEvent;
    }
    
    static com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent access$202(com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification a, com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent a0) {
        a.trafficDelayEvent = a0;
        return a0;
    }
    
    static void access$300(com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification a) {
        a.switchToExpandedMode();
    }
    
    private void buildData() {
        if (this.fasterRouteEvent != null) {
            java.util.Date a = null;
            if (this.fasterRouteEvent.etaDifference != 0L) {
                long j = java.util.concurrent.TimeUnit.SECONDS.toMinutes(this.fasterRouteEvent.etaDifference);
                if (j <= 99L) {
                    this.timeStr = String.valueOf(j);
                    this.timeUnitStr = min;
                    if (j <= 1L) {
                        this.timeStrExpanded = minute;
                    } else {
                        this.timeStrExpanded = minutes;
                    }
                } else {
                    long j0 = java.util.concurrent.TimeUnit.SECONDS.toHours(this.fasterRouteEvent.etaDifference);
                    this.timeStr = String.valueOf(j0);
                    this.timeUnitStr = hr;
                    if (j0 <= 1L) {
                        this.timeStrExpanded = hour;
                    } else {
                        this.timeStrExpanded = hours;
                    }
                }
            }
            if (this.fasterRouteEvent.newEta == 0L) {
                long j1 = this.fasterRouteEvent.currentEta;
                int i = (j1 < 0L) ? -1 : (j1 == 0L) ? 0 : 1;
                a = null;
                if (i != 0) {
                    a = new java.util.Date(this.fasterRouteEvent.currentEta - this.fasterRouteEvent.etaDifference * 1000L);
                }
            } else {
                a = new java.util.Date(this.fasterRouteEvent.newEta);
            }
            if (a != null) {
                ampmMarker.setLength(0);
                this.etaStr = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper().formatTime12Hour(a, ampmMarker, false);
                this.etaUnitStr = ampmMarker.toString();
            }
            long j2 = this.fasterRouteEvent.distanceDifference;
            if (j2 <= 0L) {
                this.distanceDiffStr = resources.getString(R.string.shorter);
                j2 = -j2;
            } else {
                this.distanceDiffStr = resources.getString(R.string.longer);
            }
            com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a0 = new com.navdy.hud.app.maps.MapEvents$ManeuverDisplay();
            com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.setNavigationDistance(j2, a0, true, true);
            this.distanceStr = a0.distanceToPendingRoadText;
            if (this.fasterRouteEvent.etaDifference == 0L) {
                this.ttsMessage = resources.getString(R.string.traffic_faster_route_tts_eta_only);
            } else {
                android.content.res.Resources a1 = resources;
                Object[] a2 = new Object[2];
                a2[0] = this.timeStr;
                a2[1] = this.timeStrExpanded;
                this.ttsMessage = a1.getString(R.string.traffic_faster_route_tts, a2);
            }
        }
    }
    
    private void cancelTts() {
        if (this.controller != null && this.controller.isTtsOn()) {
            this.logger.v(new StringBuilder().append("tts-cancelled [").append(this.id).append("]").toString());
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)this.cancelSpeechRequest));
        }
    }
    
    private void cleanupView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType a, android.view.ViewGroup a0) {
        android.view.ViewGroup a1 = (android.view.ViewGroup)a0.getParent();
        if (a1 != null) {
            a1.removeView((android.view.View)a0);
        }
        switch(com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification$2.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[a.ordinal()]) {
            case 2: {
                a0.setAlpha(1f);
                break;
            }
            case 1: {
                this.mainTitle.setAlpha(1f);
                this.mainTitle.setVisibility(View.VISIBLE);
                this.subTitle.setAlpha(1f);
                this.subTitle.setVisibility(View.VISIBLE);
                this.choiceLayout.setAlpha(1f);
                this.choiceLayout.setVisibility(View.VISIBLE);
                this.choiceLayout.setTag(null);
                this.sideImage.setImageResource(0);
                a0.setAlpha(1f);
                break;
            }
        }
        com.navdy.hud.app.framework.glance.GlanceViewCache.putView(a, (android.view.View)a0);
    }
    
    private void sendtts() {
        if (this.controller != null && this.controller.isTtsOn() && !android.text.TextUtils.isEmpty((CharSequence)this.ttsMessage)) {
            com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(this.ttsMessage, com.navdy.service.library.events.audio.SpeechRequest.Category.SPEECH_REROUTE, this.id);
        }
    }
    
    private void switchToExpandedMode() {
        this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
        if (!this.controller.isExpandedWithStack()) {
            this.controller.expandNotification(true);
        }
    }
    
    private void updateState() {
        if (this.controller != null) {
            this.logger.v("updateState");
            if (this.trafficDelayEvent == null && this.fasterRouteEvent != null) {
                this.mainTitle.setText((CharSequence)fasterRoute);
                if (this.fasterRouteEvent.via == null) {
                    this.subTitle.setText((CharSequence)"");
                } else {
                    this.subTitle.setText((CharSequence)new StringBuilder().append(via).append(" ").append(this.fasterRouteEvent.via).toString());
                }
                this.mainImage.setColor(trafficRerouteColor);
                this.sideImage.setImageResource(R.drawable.icon_badge_route);
                this.buildData();
                if (this.fasterRouteEvent.etaDifference != 0L) {
                    this.text1.setText((CharSequence)save);
                    ((android.view.ViewGroup.MarginLayoutParams)this.text2.getLayoutParams()).topMargin = com.navdy.hud.app.framework.glance.GlanceConstants.calendarNormalMargin;
                    this.text2.setTextAppearance(com.navdy.hud.app.HudApplication.getAppContext(), R.style.small_glance_sign_text_1);
                    this.text2.setText((CharSequence)this.timeStr);
                    this.text3.setText((CharSequence)this.timeUnitStr);
                }
                if (this.controller.isExpandedWithStack()) {
                    this.choiceLayout.setChoices(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.backChoice);
                } else {
                    this.choiceLayout.setChoices(reRouteChoices, 1, this.choiceListener, 0.5f);
                }
                if (this.extendedContainer != null && this.controller.isExpandedWithStack()) {
                    this.setExpandedContent(com.navdy.hud.app.HudApplication.getAppContext());
                }
            }
        }
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    public boolean expandNotification() {
        boolean b = false;
        if (this.controller != null) {
            this.switchToExpandedMode();
            b = true;
        } else {
            b = false;
        }
        return b;
    }
    
    public int getColor() {
        return (this.fasterRouteEvent == null) ? com.navdy.hud.app.framework.glance.GlanceConstants.colorTrafficDelay : com.navdy.hud.app.framework.glance.GlanceConstants.colorFasterRoute;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        this.extendedContainer = (android.view.ViewGroup)com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_MULTI_TEXT, a);
        this.setExpandedContent(a);
        return this.extendedContainer;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return this.id;
    }
    
    public int getTimeout() {
        return 0;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return this.type;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.container == null) {
            this.container = (android.view.ViewGroup)com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_SIGN, a);
            this.mainTitle = (android.widget.TextView)this.container.findViewById(R.id.mainTitle);
            this.subTitle = (android.widget.TextView)this.container.findViewById(R.id.subTitle);
            this.mainImage = (com.navdy.hud.app.ui.component.image.ColorImageView)this.container.findViewById(R.id.mainImage);
            this.sideImage = (android.widget.ImageView)this.container.findViewById(R.id.sideImage);
            this.text1 = (android.widget.TextView)this.container.findViewById(R.id.text1);
            this.text2 = (android.widget.TextView)this.container.findViewById(R.id.text2);
            this.text3 = (android.widget.TextView)this.container.findViewById(R.id.text3);
            this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)this.container.findViewById(R.id.choiceLayout);
        }
        return this.container;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        android.animation.AnimatorSet a = new android.animation.AnimatorSet();
        if (b) {
            android.animation.Animator[] a0 = new android.animation.Animator[3];
            android.widget.TextView a1 = this.mainTitle;
            android.util.Property a2 = android.view.View.ALPHA;
            float[] a3 = new float[2];
            a3[0] = 0.0f;
            a3[1] = 1f;
            a0[0] = android.animation.ObjectAnimator.ofFloat(a1, a2, a3);
            android.widget.TextView a4 = this.subTitle;
            android.util.Property a5 = android.view.View.ALPHA;
            float[] a6 = new float[2];
            a6[0] = 0.0f;
            a6[1] = 1f;
            a0[1] = android.animation.ObjectAnimator.ofFloat(a4, a5, a6);
            com.navdy.hud.app.ui.component.ChoiceLayout2 a7 = this.choiceLayout;
            android.util.Property a8 = android.view.View.ALPHA;
            float[] a9 = new float[2];
            a9[0] = 0.0f;
            a9[1] = 1f;
            a0[2] = android.animation.ObjectAnimator.ofFloat(a7, a8, a9);
            a.playTogether(a0);
        } else {
            android.animation.Animator[] a10 = new android.animation.Animator[3];
            android.widget.TextView a11 = this.mainTitle;
            android.util.Property a12 = android.view.View.ALPHA;
            float[] a13 = new float[2];
            a13[0] = 1f;
            a13[1] = 0.0f;
            a10[0] = android.animation.ObjectAnimator.ofFloat(a11, a12, a13);
            android.widget.TextView a14 = this.subTitle;
            android.util.Property a15 = android.view.View.ALPHA;
            float[] a16 = new float[2];
            a16[0] = 1f;
            a16[1] = 0.0f;
            a10[1] = android.animation.ObjectAnimator.ofFloat(a14, a15, a16);
            com.navdy.hud.app.ui.component.ChoiceLayout2 a17 = this.choiceLayout;
            android.util.Property a18 = android.view.View.ALPHA;
            float[] a19 = new float[2];
            a19[0] = 1f;
            a19[1] = 0.0f;
            a10[2] = android.animation.ObjectAnimator.ofFloat(a17, a18, a19);
            a.playTogether(a10);
        }
        return a;
    }
    
    public boolean isAlive() {
        return true;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode a) {
        if (a != com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE) {
            this.sendtts();
        } else if (this.controller != null) {
            this.cancelTts();
            if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isNotificationMarkedForRemoval(this.id)) {
                this.dismissNotification();
            } else {
                this.updateState();
            }
        }
    }
    
    public void onExpandedNotificationSwitched() {
        if (this.controller != null && this.controller.isExpandedWithStack()) {
            this.sendtts();
        }
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager.Mode a) {
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        super.onStart(a);
        this.updateState();
        if (a.isExpandedWithStack()) {
            this.mainTitle.setAlpha(0.0f);
            this.subTitle.setAlpha(0.0f);
            this.choiceLayout.setAlpha(0.0f);
        } else {
            this.container.setTranslationX(0.0f);
            this.container.setTranslationY(0.0f);
            this.container.setAlpha(1f);
            this.mainTitle.setAlpha(1f);
            this.subTitle.setAlpha(1f);
            this.choiceLayout.setAlpha(1f);
        }
    }
    
    public void onStop() {
        this.cancelTts();
        super.onStop();
        if (this.container != null) {
            this.cleanupView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_SIGN, this.container);
            this.container = null;
        }
        if (this.extendedContainer != null) {
            this.cleanupView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_MULTI_TEXT, this.extendedContainer);
            this.extendedContainer = null;
        }
    }
    
    public void onUpdate() {
        this.updateState();
    }
    
    public void setDelayEvent(com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent a) {
        this.trafficDelayEvent = a;
    }
    
    void setExpandedContent(android.content.Context a) {
        android.widget.TextView a0 = (android.widget.TextView)this.extendedContainer.findViewById(R.id.title1);
        a0.setTextAppearance(a, R.style.glance_title_1);
        android.widget.TextView a1 = (android.widget.TextView)this.extendedContainer.findViewById(R.id.title2);
        a1.setTextAppearance(a, R.style.glance_title_2);
        if (this.fasterRouteEvent != null) {
            String s = null;
            a0.setText((CharSequence)fasterRouteAvailable);
            if (this.timeStr == null) {
                this.buildData();
            }
            if (this.fasterRouteEvent.etaDifference == 0L) {
                android.content.res.Resources a2 = resources;
                Object[] a3 = new Object[5];
                a3[0] = this.fasterRouteEvent.additionalVia;
                a3[1] = this.etaStr;
                a3[2] = this.etaUnitStr;
                a3[3] = this.distanceStr;
                a3[4] = this.distanceDiffStr;
                s = a2.getString(R.string.traffic_reroute_msg_eta_only, a3);
            } else {
                android.content.res.Resources a4 = resources;
                Object[] a5 = new Object[7];
                a5[0] = this.timeStr;
                a5[1] = this.timeStrExpanded;
                a5[2] = this.fasterRouteEvent.additionalVia;
                a5[3] = this.etaStr;
                a5[4] = this.etaUnitStr;
                a5[5] = this.distanceStr;
                a5[6] = this.distanceDiffStr;
                s = a4.getString(R.string.traffic_reroute_msg, a5);
            }
            a1.setText((CharSequence)s);
        }
        a0.setVisibility(View.VISIBLE);
        a1.setVisibility(View.VISIBLE);
    }
    
    public void setFasterRouteEvent(com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent a) {
        this.fasterRouteEvent = a;
    }
}
