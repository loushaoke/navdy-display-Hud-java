package com.navdy.hud.app.maps.here;

class HereNavigationEventListener$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationEventListener this$0;
    
    HereNavigationEventListener$1(com.navdy.hud.app.maps.here.HereNavigationEventListener a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.here.android.mpa.guidance.NavigationManager.NavigationMode a = com.navdy.hud.app.maps.here.HereNavigationEventListener.access$000(this.this$0).getHereNavigationState();
        com.navdy.hud.app.maps.here.HereNavController$State a0 = com.navdy.hud.app.maps.here.HereNavigationEventListener.access$000(this.this$0).getNavigationState();
        com.navdy.hud.app.maps.here.HereNavigationEventListener.access$200(this.this$0).i(new StringBuilder().append(com.navdy.hud.app.maps.here.HereNavigationEventListener.access$100(this.this$0)).append(" onNavigationModeChanged HERE mode=").append(a).append(" Navdy=").append(a0).toString());
    }
}
