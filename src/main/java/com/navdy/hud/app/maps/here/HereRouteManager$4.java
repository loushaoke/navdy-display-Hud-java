package com.navdy.hud.app.maps.here;

final class HereRouteManager$4 implements com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener {
    final com.here.android.mpa.common.GeoCoordinate val$endPoint;
    final com.navdy.service.library.events.navigation.NavigationRouteRequest val$request;
    final Runnable val$runnable;
    final com.here.android.mpa.common.GeoCoordinate val$startPoint;
    final java.util.List val$waypoints;
    
    HereRouteManager$4(com.navdy.service.library.events.navigation.NavigationRouteRequest a, Runnable a0, com.here.android.mpa.common.GeoCoordinate a1, java.util.List a2, com.here.android.mpa.common.GeoCoordinate a3) {

        super();
        this.val$request = a;
        this.val$runnable = a0;
        this.val$startPoint = a1;
        this.val$waypoints = a2;
        this.val$endPoint = a3;
    }
    
    public void error(com.here.android.mpa.routing.RoutingError a, Throwable a0) {
        com.navdy.hud.app.maps.here.HereRouteManager.access$1400().removeCallbacks(this.val$runnable);
        if (a == null) {
            com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("got error for traffic", a0);
        } else {
            com.navdy.hud.app.maps.here.HereRouteManager.access$100().v(new StringBuilder().append("got error for traffic [").append(a.name()).append("]").toString());
        }
        label2: synchronized(com.navdy.hud.app.maps.here.HereRouteManager.access$400()) {
            label1: try {
                boolean b = com.navdy.hud.app.maps.here.HereRouteManager.access$1500();
                label0: {
                    if (b) {
                        break label0;
                    }
                    /*monexit(a1)*/;
                    break label1;
                }
                com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("user cancelled routing");
                com.navdy.hud.app.maps.here.HereRouteManager.access$300(com.navdy.service.library.events.RequestStatus.REQUEST_CANCELLED, this.val$request, (String)null);
                /*monexit(a1)*/;
                break label2;
            } catch(Throwable a2) {
                Throwable a3 = a2;

            }
            com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("launching no traffic");
            com.navdy.hud.app.maps.here.HereRouteManager.access$1600(this.val$request, this.val$startPoint, this.val$waypoints, this.val$endPoint, false);
        }
    }
    
    public void postSuccess(java.util.ArrayList a) {
        com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("got result for traffic");
        com.navdy.hud.app.maps.here.HereRouteManager.access$1300(this.val$request, a, true);
    }
    
    public void preSuccess() {
        synchronized(com.navdy.hud.app.maps.here.HereRouteManager.access$400()) {
            com.navdy.hud.app.maps.here.HereRouteManager.access$1702((com.navdy.hud.app.maps.here.HereRouteCalculator)null);
            /*monexit(a)*/;
        }
        com.navdy.hud.app.maps.here.HereRouteManager.access$1400().removeCallbacks(this.val$runnable);
        com.navdy.hud.app.maps.here.HereRouteManager.access$100().v("route successfully calculated, post calc in progress");
    }
    
    public void progress(int i) {
    }
}
