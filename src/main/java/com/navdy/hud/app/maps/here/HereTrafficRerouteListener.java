package com.navdy.hud.app.maps.here;

import com.here.android.mpa.routing.RouteResult;

class HereTrafficRerouteListener extends com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener {
    final static int MIN_INTERVAL_REROUTE_FIRST = 90;
    final static long NOTIF_FROM_HERE_THRESHOLD;
    final static long REROUTE_POINT_THRESHOLD;
    final static int ROUTE_CHECK_INITIAL_INTERVAL;
    final static int ROUTE_CHECK_INTERVAL;
    final static int ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH;
    final static int WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN;
    final private com.squareup.otto.Bus bus;
    private com.here.android.mpa.routing.Route currentProposedRoute;
    private long fasterBy;
    final private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private long lastTrafficNotifFromHere;
    final private com.navdy.service.library.log.Logger logger;
    final private com.navdy.hud.app.maps.here.HereNavController navController;
    private com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager navdyTrafficRerouteManager;
    private boolean notifFromHere;
    private com.here.android.mpa.common.GeoCoordinate placeCalculated;
    private Runnable startRunnable;
    private Runnable stopRunnable;
    final private String tag;
    private long timeCalculated;
    private boolean trafficRerouteListenerRunning;
    final private boolean verbose;
    private String viaString;
    
    static {
        WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(60L);
        ROUTE_CHECK_INITIAL_INTERVAL = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(1L);
        ROUTE_CHECK_INTERVAL = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(5L);
        ROUTE_CHECK_INTERVAL_LIMIT_BANDWIDTH = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(30L);
        REROUTE_POINT_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toSeconds(20L);
        NOTIF_FROM_HERE_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toMillis(1L);
    }
    
    HereTrafficRerouteListener(String s, boolean b, com.navdy.hud.app.maps.here.HereNavController a, com.navdy.hud.app.maps.here.HereNavigationManager a0, com.squareup.otto.Bus a1) {
        this.logger = new com.navdy.service.library.log.Logger("HereTrafficReroute");
        this.startRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficRerouteListener$1(this);
        this.stopRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficRerouteListener$2(this);
        this.logger.v("HereTrafficRerouteListener:ctor");
        this.tag = s;
        this.verbose = b;
        this.navController = a;
        this.hereNavigationManager = a0;
        this.bus = a1;
    }
    
    static com.navdy.hud.app.maps.here.HereNavController access$000(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a) {
        return a.navController;
    }
    
    static com.navdy.service.library.log.Logger access$100(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a) {
        return a.logger;
    }
    
    static com.here.android.mpa.routing.Route access$200(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a) {
        return a.currentProposedRoute;
    }
    
    static com.navdy.hud.app.maps.here.HereNavigationManager access$300(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a) {
        return a.hereNavigationManager;
    }
    
    static void access$400(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a) {
        a.clearProposedRoute();
    }
    
    static String access$500(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a) {
        return a.tag;
    }
    
    static boolean access$600(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a, java.util.List a0, java.util.List a1, java.util.List a2) {
        return a.isFasterRouteDivergePointCloseBy(a0, a1, a2);
    }
    
    static com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager access$700(com.navdy.hud.app.maps.here.HereTrafficRerouteListener a) {
        return a.navdyTrafficRerouteManager;
    }
    
    private void clearProposedRoute() {
        this.logger.v("clearProposedRoute");
        if (this.currentProposedRoute != null) {
            this.logger.v("clearProposedRoute:cleared");
            com.navdy.hud.app.maps.notification.TrafficNotificationManager.getInstance().removeFasterRouteNotifiation();
            this.currentProposedRoute = null;
            this.timeCalculated = 0L;
            this.placeCalculated = null;
            this.notifFromHere = false;
            this.viaString = null;
            this.fasterBy = 0L;
        }
    }
    
    private boolean isFasterRouteDivergePointCloseBy(java.util.List a, java.util.List a0, java.util.List a1) {
        boolean b = false;
        com.here.android.mpa.routing.Maneuver a2 = this.navController.getNextManeuver();
        com.here.android.mpa.routing.Maneuver a3 = (com.here.android.mpa.routing.Maneuver)a0.get(0);
        com.here.android.mpa.routing.Maneuver a4 = (a1.size() <= 1) ? (com.here.android.mpa.routing.Maneuver)a1.get(0) : (com.here.android.mpa.routing.Maneuver)a1.get(1);
        com.here.android.mpa.routing.Maneuver a5 = (a0.size() <= 1) ? a3 : (com.here.android.mpa.routing.Maneuver)a0.get(1);
        this.logger.v(new StringBuilder().append(this.tag).append(" diverge point road faster [").append(com.navdy.hud.app.maps.here.HereMapUtil.getRoadName(a4)).append(",").append(com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName(a4)).append("]  slower [").append(com.navdy.hud.app.maps.here.HereMapUtil.getRoadName(a5)).append(",").append(com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName(a5)).append("]").toString());
        boolean b0 = com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual(a2, a3);
        label5: {
            long j = 0L;
            long j0 = 0L;
            label0: {
                label1: {
                    label3: {
                        Object a6 = null;
                        int i = 0;
                        boolean b1 = false;
                        label4: {
                            if (!b0) {
                                break label4;
                            }
                            this.logger.i(new StringBuilder().append(this.tag).append("first change and current are same").toString());
                            break label3;
                        }
                        if (a2 != null) {
                            int i0 = a.size();
                            a6 = a;
                            i = 0;
                            while(true) {
                                if (i >= i0) {
                                    i = -1;
                                    break;
                                } else {
                                    if (!com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver)((java.util.List)a6).get(i), a2)) {
                                        i = i + 1;
                                        continue;
                                    }
                                    break;
                                }
                            }
                        } else {
                            this.logger.i(new StringBuilder().append(this.tag).append("no current maneuver").toString());
                            a6 = a;
                            i = -1;
                        }
                        label2: {
                            if (i != -1) {
                                break label2;
                            }
                            this.logger.i(new StringBuilder().append(this.tag).append("offset not found").toString());
                            break label3;
                        }
                        this.logger.v(new StringBuilder().append(this.tag).append("reroute offset=").append(i).toString());
                        com.navdy.hud.app.maps.here.HereMapUtil$LongContainer a7 = new com.navdy.hud.app.maps.here.HereMapUtil$LongContainer();
                        int i1 = ((java.util.List)a6).size();
                        j = 0L;
                        j0 = 0L;
                        while(true) {
                            if (i >= i1) {
                                b1 = false;
                                break;
                            } else {
                                com.here.android.mpa.routing.Maneuver a8 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a6).get(i);
                                if (!com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual(a8, a3)) {
                                    j = j + com.navdy.hud.app.maps.here.HereMapUtil.getManeuverDriveTime(a8, a7);
                                    j0 = j0 + a7.val;
                                    i = i + 1;
                                    continue;
                                }
                                this.logger.v(new StringBuilder().append(this.tag).append("reroute diverge:").append(i).append(",").append(i1).toString());
                                b1 = true;
                                break;
                            }
                        }
                        if (!b1) {
                            break label1;
                        }
                        if (j >= REROUTE_POINT_THRESHOLD) {
                            break label0;
                        }
                        this.logger.v(new StringBuilder().append(this.tag).append("reroute point threshold [").append(j).append("] distance[").append(j0).append("]").toString());
                    }
                    b = true;
                    break label5;
                }
                this.logger.i(new StringBuilder().append(this.tag).append("reroute threshold slow maneuver not found").toString());
                b = true;
                break label5;
            }
            this.logger.i(new StringBuilder().append(this.tag).append("reroute threshold [").append(j).append("] is > ").append(REROUTE_POINT_THRESHOLD).append(" dist=").append(j0).toString());
            this.dismissReroute();
            b = false;
        }
        return b;
    }
    
    public void confirmReroute() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereTrafficRerouteListener$3(this), 20);
    }
    
    public void dismissReroute() {
        this.clearProposedRoute();
    }
    
    void fasterRouteNotFound(boolean b, long j, String s, String s0) {
        String s1 = b ? "[Here Traffic]" : "[Navdy Calc]";
        this.logger.v(new StringBuilder().append(this.tag).append(s1).append(" new route threshold not met:").append(this.getRerouteMinDuration()).toString());
        if (j > 0L && com.navdy.hud.app.framework.voice.TTSUtils.isDebugTTSEnabled()) {
            com.navdy.hud.app.framework.voice.TTSUtils.debugShowFasterRouteToast(new StringBuilder().append(s1).append(" Faster route below threshold").toString(), new StringBuilder().append("via[").append(s).append("] faster by[").append(j).append("] seconds current via[").append(s0).append("]").toString());
        }
    }
    
    public com.here.android.mpa.routing.Route getCurrentProposedRoute() {
        return this.currentProposedRoute;
    }
    
    public long getCurrentProposedRouteTime() {
        return this.timeCalculated;
    }
    
    public long getFasterBy() {
        return this.fasterBy;
    }
    
    public int getRerouteMinDuration() {
        int i = 0;
        if (this.hereNavigationManager.getCurrentTrafficRerouteCount() >= 1) {
            int i0 = (int)(android.os.SystemClock.elapsedRealtime() - this.hereNavigationManager.getLastTrafficRerouteTime()) / 1000;
            i = (int)(90.0 + 1.5 * (double)Math.max(360 - i0, 0));
            this.logger.v(new StringBuilder().append("getRerouteMinDuration:").append(i).append(" secslast:").append(i0).toString());
        } else {
            this.logger.v("getRerouteMinDuration:90");
            i = 90;
        }
        return i;
    }
    
    public String getViaString() {
        return this.viaString;
    }
    
    public void handleFasterRoute(com.here.android.mpa.routing.Route a, boolean b) {
        if (a != null) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereTrafficRerouteListener$4(this, b, a), 2);
        } else {
            this.logger.e(new StringBuilder().append("null faster route:").append(b).toString());
        }
    }
    
    public boolean isMoreRouteNotificationComplete() {
        boolean b = false;
        label1: if (this.hereNavigationManager.isShownFirstManeuver()) {
            if (com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isCurrentNotificationId("navdy#route#calc#notif")) {
                this.logger.v("isMoreRouteNotificationComplete:route calc notif on");
                b = false;
            } else {
                com.navdy.hud.app.screen.BaseScreen a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
                label0: {
                    if (a == null) {
                        break label0;
                    }
                    if (a.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_DESTINATION_PICKER) {
                        break label0;
                    }
                    android.os.Bundle a0 = a.getArguments();
                    if (a0 == null) {
                        break label0;
                    }
                    if (!a0.getBoolean("ROUTE_PICKER")) {
                        break label0;
                    }
                    this.logger.v("isMoreRouteNotificationComplete:user looking at route picker");
                    b = false;
                    break label1;
                }
                if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getRouteCalcEventHandler().hasMoreRoutes()) {
                    this.logger.v("isMoreRouteNotificationComplete:user has not seen more routes");
                    b = false;
                } else {
                    long j = android.os.SystemClock.elapsedRealtime() - this.hereNavigationManager.getFirstManeuverShowntime();
                    if (j >= (long)WAIT_INTERVAL_AFTER_FIRST_MANEUVER_SHOWN) {
                        b = true;
                    } else {
                        this.logger.v(new StringBuilder().append("isMoreRouteNotificationComplete:first maneuver just shown:").append(j).toString());
                        b = false;
                    }
                }
            }
        } else {
            this.logger.v("isMoreRouteNotificationComplete:not shown first maneuver");
            b = false;
        }
        return b;
    }
    
    public void onTrafficRerouteBegin(com.here.android.mpa.guidance.TrafficNotification a) {
        this.logger.v(new StringBuilder().append(this.tag).append(" reroute-begin:").toString());
        if (this.verbose) {
            com.navdy.hud.app.maps.here.HereMapUtil.printTrafficNotification(a, this.tag);
        }
    }
    
    public void onTrafficRerouteFailed(com.here.android.mpa.guidance.TrafficNotification a) {
        this.logger.e(new StringBuilder().append(this.tag).append(" reroute-failed").toString());
        com.navdy.hud.app.maps.here.HereMapUtil.printTrafficNotification(a, this.tag);
    }
    
    public void onTrafficRerouteState(com.here.android.mpa.guidance.NavigationManager.TrafficRerouteListener.TrafficEnabledRoutingState a) {
    }

    public void onTrafficRerouted(RouteResult routeResult) {

        com.here.android.mpa.routing.Route route = routeResult.getRoute();
        long j = android.os.SystemClock.elapsedRealtime();
        long j0 = j - this.lastTrafficNotifFromHere;
        if (j0 >= NOTIF_FROM_HERE_THRESHOLD) {
            if (this.isMoreRouteNotificationComplete()) {
                this.lastTrafficNotifFromHere = j;
                this.handleFasterRoute(route, true);
            } else {
                this.logger.v("onTrafficRerouted: more route notif in progress");
            }
        } else {
            this.logger.i(this.tag + "here traffic notif came in " + j0);
        }
    }
    
    void setFasterRoute(com.here.android.mpa.routing.Route a, boolean b, String s, long j, String s0, long j0, long j1, long j2) {
        this.hereNavigationManager.incrCurrentTrafficRerouteCount();
        this.hereNavigationManager.setLastTrafficRerouteTime(android.os.SystemClock.elapsedRealtime());
        this.currentProposedRoute = a;
        this.timeCalculated = android.os.SystemClock.elapsedRealtime();
        this.notifFromHere = b;
        this.viaString = s;
        this.fasterBy = j;
        this.placeCalculated = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager().getLastGeoCoordinate();
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent(s0, s0, j, j0, j1, j2));
        this.logger.v("faster reroute event sent");
    }
    
    public void setNavdyTrafficRerouteManager(com.navdy.hud.app.maps.here.NavdyTrafficRerouteManager a) {
        this.navdyTrafficRerouteManager = a;
    }
    
    public boolean shouldCalculateFasterRoute() {
        boolean b = false;
        long j = android.os.SystemClock.elapsedRealtime() - this.timeCalculated;
        this.logger.v(new StringBuilder().append("elapsed=").append(j).toString());
        if (j >= (long)(this.hereNavigationManager.getRerouteInterval() - 15000)) {
            if (this.isMoreRouteNotificationComplete()) {
                b = true;
            } else {
                this.logger.v("shouldCalculateFasterRoute: more route notif in progress");
                b = false;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public void start() {
        if (!this.trafficRerouteListenerRunning) {
            this.trafficRerouteListenerRunning = true;
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.startRunnable, 15);
            this.logger.v("added traffic reroute listener");
        }
    }
    
    public void stop() {
        if (this.trafficRerouteListenerRunning) {
            this.trafficRerouteListenerRunning = false;
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.stopRunnable, 15);
            this.logger.v("removed traffic reroute listener");
        }
    }
}
