package com.navdy.hud.app.maps.here;

class HereMapCameraManager$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapCameraManager this$0;
    
    HereMapCameraManager$3(com.navdy.hud.app.maps.here.HereMapCameraManager a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        label2: if (com.navdy.hud.app.maps.here.HereMapCameraManager.access$200(this.this$0)) {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v("easeout user zoom action is on");
        } else {
            boolean b = Math.abs(com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$0).getZoomLevel() - com.navdy.hud.app.maps.here.HereMapCameraManager.access$900(this.this$0)) >= 0.25;
            boolean b0 = Math.abs(com.navdy.hud.app.maps.here.HereMapCameraManager.access$300(this.this$0).getTilt() - com.navdy.hud.app.maps.here.HereMapCameraManager.access$1000(this.this$0)) > 5f;
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!b0) {
                        break label0;
                    }
                }
                com.navdy.hud.app.maps.here.HereMapCameraManager.access$1102(this.this$0, android.os.SystemClock.elapsedRealtime());
                com.navdy.hud.app.maps.here.HereMapCameraManager.access$1200(this.this$0);
                com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v(new StringBuilder().append("easeout zoom=").append(b).append(" tilt=").append(b0).append(" current-zoom=").append(com.navdy.hud.app.maps.here.HereMapCameraManager.access$900(this.this$0)).append(" current-tilt=").append(com.navdy.hud.app.maps.here.HereMapCameraManager.access$1000(this.this$0)).toString());
                break label2;
            }
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$400().v(new StringBuilder().append("easeout not significant current-zoom=").append(com.navdy.hud.app.maps.here.HereMapCameraManager.access$900(this.this$0)).append(" current-tilt=").append(com.navdy.hud.app.maps.here.HereMapCameraManager.access$1000(this.this$0)).toString());
        }
    }
}
