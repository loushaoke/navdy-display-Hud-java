package com.navdy.hud.app.maps.here;

class HereMapAnimator$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapAnimator this$0;
    final com.here.android.mpa.common.GeoPosition val$geoPosition;
    
    HereMapAnimator$2(com.navdy.hud.app.maps.here.HereMapAnimator a, com.here.android.mpa.common.GeoPosition a0) {
        super();
        this.this$0 = a;
        this.val$geoPosition = a0;
    }
    
    public void run() {
        Object a = null;
        Throwable a0 = null;
        long j = (com.navdy.hud.app.maps.here.HereMapAnimator.access$200(this.this$0) <= 0L) ? 0L : android.os.SystemClock.elapsedRealtime() - com.navdy.hud.app.maps.here.HereMapAnimator.access$200(this.this$0);
        boolean b = com.navdy.hud.app.maps.here.HereMapAnimator.access$300(this.this$0, this.val$geoPosition);
        label0: {
            if (b) {
                com.here.android.mpa.common.GeoPosition a1 = null;
                com.navdy.hud.app.maps.here.HereMapAnimator.access$202(this.this$0, android.os.SystemClock.elapsedRealtime());
                synchronized(com.navdy.hud.app.maps.here.HereMapAnimator.access$500()) {
                    com.navdy.hud.app.maps.here.HereMapAnimator.access$602(this.this$0, j);
                    a1 = com.navdy.hud.app.maps.here.HereMapAnimator.access$700(this.this$0);
                    com.navdy.hud.app.maps.here.HereMapAnimator.access$702(this.this$0, this.val$geoPosition);
                    /*monexit(a)*/;
                }
                if (a1 == null && com.navdy.hud.app.maps.here.HereMapAnimator.access$800(this.this$0)) {
                    com.navdy.hud.app.maps.here.HereMapAnimator.access$400().v("initial setCenter");
                    com.navdy.hud.app.maps.here.HereMapAnimator.access$900(this.this$0).setCenter(com.navdy.hud.app.maps.here.HereMapAnimator.access$700(this.this$0).getCoordinate(), com.here.android.mpa.mapping.Map.Animation.NONE, -1.0, (float)com.navdy.hud.app.maps.here.HereMapAnimator.access$700(this.this$0).getHeading(), -1f);
                }
            } else {
                com.navdy.hud.app.maps.here.HereMapAnimator.access$400().v(new StringBuilder().append("filtering out spurious heading: ").append(this.val$geoPosition.getHeading()).append(" speed:").append(this.val$geoPosition.getSpeed()).toString());
            }
            return;
        }

    }
}
