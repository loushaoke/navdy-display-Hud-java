package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

import java.io.IOException;

public class HereMapUtil {
    final private static String ACTIVE_GAS_ROUTE_INFO_REQUEST = "gasrouterequest.bin";
    final private static String ACTIVE_ROUTE_INFO_DEVICE_ID = "routerequest.device";
    final private static String ACTIVE_ROUTE_INFO_REQUEST = "routerequest.bin";
    final private static int EQUALITY_DISTANCE_VARIATION = 500;
    final private static long INVALID_ETA_TIME_DIFF = 360000L;
    final public static double MILES_TO_METERS = 1609.34;
    final public static int SHORT_DISTANCE_IN_METERS = 20;
    final private static long STALE_ROUTE_TIME;
    final public static String TBT_ISO3_LANG_CODE;
    final private static boolean VERBOSE = false;
    final private static String day;
    final private static String hr;
    private static String lastKnownDeviceId;
    final private static String min;
    final private static String min_short;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static StringBuilder signPostBuilder;
    final private static StringBuilder signPostBuilder2;
    final private static java.util.HashSet signPostSet;
    final private static android.text.style.TextAppearanceSpan sp_20_b_1;
    final private static android.text.style.TextAppearanceSpan sp_20_b_2;
    final private static android.text.style.TextAppearanceSpan sp_24_1;
    final private static android.text.style.TextAppearanceSpan sp_24_2;
    final private static android.text.style.TextAppearanceSpan sp_26_1;
    final private static android.text.style.TextAppearanceSpan sp_26_2;
    final private static StringBuilder stringBuilder;
    final private static StringBuilder trafficEventBuilder;
    final private static com.squareup.wire.Wire wire;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapUtil.class);
        STALE_ROUTE_TIME = java.util.concurrent.TimeUnit.HOURS.toMillis(3L);
        stringBuilder = new StringBuilder();
        TBT_ISO3_LANG_CODE = new java.util.Locale("en", "US").getISO3Language().toUpperCase();
        signPostBuilder = new StringBuilder();
        signPostBuilder2 = new StringBuilder();
        signPostSet = new java.util.HashSet();
        trafficEventBuilder = new StringBuilder();
        Class[] a = new Class[1];
        a[0] = com.navdy.service.library.events.Ext_NavdyEvent.class;
        wire = new com.squareup.wire.Wire(a);
        android.content.Context a0 = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.res.Resources a1 = a0.getResources();
        sp_20_b_1 = new android.text.style.TextAppearanceSpan(a0, R.style.route_duration_20_b);
        sp_20_b_2 = new android.text.style.TextAppearanceSpan(a0, R.style.route_duration_20_b);
        sp_24_1 = new android.text.style.TextAppearanceSpan(a0, R.style.route_duration_24);
        sp_24_2 = new android.text.style.TextAppearanceSpan(a0, R.style.route_duration_24);
        sp_26_1 = new android.text.style.TextAppearanceSpan(a0, R.style.route_duration_26);
        sp_26_2 = new android.text.style.TextAppearanceSpan(a0, R.style.route_duration_26);
        min = a1.getString(R.string.route_time_min);
        min_short = a1.getString(R.string.route_time_min_short);
        hr = a1.getString(R.string.route_time_hr);
        day = a1.getString(R.string.route_time_day);
    }
    
    public HereMapUtil() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public static boolean areManeuverEqual(com.here.android.mpa.routing.Maneuver a, com.here.android.mpa.routing.Maneuver a0) {
        boolean b = false;
        label0: {
            Throwable a1 = null;
            label1: {
                label2: {
                    if (a == null) {
                        break label2;
                    }
                    if (a0 != null) {
                        break label1;
                    }
                }
                b = false;
                break label0;
            }
            b = a.getRoadElements().equals(a0.getRoadElements());
            if (b) {
                break label0;
            }
            try {
                if (a.getTurn() != a0.getTurn()) {
                    break label0;
                }
                if (!android.text.TextUtils.equals((CharSequence)a.getNextRoadNumber(), (CharSequence)a0.getNextRoadNumber())) {
                    break label0;
                }
                if (!android.text.TextUtils.equals((CharSequence)a.getNextRoadName(), (CharSequence)a0.getNextRoadName())) {
                    break label0;
                }
                if (!android.text.TextUtils.equals((CharSequence)a.getRoadNumber(), (CharSequence)a0.getRoadNumber())) {
                    break label0;
                }
                if (!android.text.TextUtils.equals((CharSequence)a.getRoadName(), (CharSequence)a0.getRoadName())) {
                    break label0;
                }
                if (a.getAction() != a0.getAction()) {
                    break label0;
                }
                if (Math.abs(a.getDistanceToNextManeuver() - a0.getDistanceToNextManeuver()) > 500) {
                    break label0;
                }
                com.navdy.service.library.log.Logger a2 = sLogger;
                b = true;
                if (a2.isLoggable(2)) {
                    b = true;
                    int i = Math.abs(a.getDistanceToNextManeuver() - a0.getDistanceToNextManeuver());
                    sLogger.v(new StringBuilder().append("areManeuverEqual: distance diff=").append(i).append(" allowance=").append(500).toString());
                    b = true;
                    break label0;
                } else {
                    b = true;
                    break label0;
                }
            } catch(Throwable a3) {
                a1 = a3;
            }
            sLogger.e(a1);
        }
        return b;
    }
    
    static boolean areManeuversEqual(java.util.List a, int i, java.util.List a0, int i0, java.util.List a1) {
        int i1 = a.size() - i;
        int i2 = a0.size() - i0;
        Object a2 = a;
        Object a3 = a0;
        Object a4 = a1;
        while(true) {
            boolean b = false;
            label1: {
                label2: {
                    label3: {
                        if (i1 <= 0) {
                            break label3;
                        }
                        if (i2 > 0) {
                            break label2;
                        }
                    }
                    label0: {
                        if (i1 > 0) {
                            break label0;
                        }
                        if (i2 > 0) {
                            break label0;
                        }
                        b = true;
                        break label1;
                    }
                    if (i1 > 0 && a4 != null) {
                        ((java.util.List)a4).add(((java.util.List)a2).get(i));
                    }
                    b = false;
                    break label1;
                }
                com.here.android.mpa.routing.Maneuver a5 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a2).get(i);
                com.here.android.mpa.routing.Maneuver a6 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a3).get(i0);
                if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual(a5, a6)) {
                    if (sLogger.isLoggable(2)) {
                        sLogger.v("maneuver are equal");
                        com.navdy.hud.app.maps.here.HereMapUtil.printManeuverDetails(a5, "[maneuver-1]");
                        com.navdy.hud.app.maps.here.HereMapUtil.printManeuverDetails(a6, "[maneuver-2]");
                    }
                    i1 = i1 + -1;
                    i2 = i2 + -1;
                    int i3 = i0 + 1;
                    int i4 = i + 1;
                    i0 = i3;
                    i = i4;
                    continue;
                }
                sLogger.v("maneuver are NOT equal");
                com.navdy.hud.app.maps.here.HereMapUtil.printManeuverDetails(a5, "[maneuver-1]");
                com.navdy.hud.app.maps.here.HereMapUtil.printManeuverDetails(a6, "[maneuver-2]");
                if (a4 == null) {
                    b = false;
                } else {
                    ((java.util.List)a4).add(a5);
                    b = false;
                }
            }
            return b;
        }
    }
    
    public static String concatText(String s, String s0, boolean b, boolean b0) {
        String s1 = null;
        synchronized(stringBuilder) {
            StringBuilder a0 = stringBuilder;
            boolean b1 = false;
            a0.setLength(0);
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                b0 = false;
                b1 = false;
            } else {
                stringBuilder.append(s);
                b1 = true;
            }
            if (!android.text.TextUtils.isEmpty((CharSequence)s0) && !b0) {
                if (b1) {
                    stringBuilder.append(" ");
                    if (b) {
                        stringBuilder.append("(");
                    }
                } else {
                    b = false;
                }
                stringBuilder.append(s0);
                if (b) {
                    stringBuilder.append(")");
                }
            }
            s1 = stringBuilder.toString();
            /*monexit(a)*/;
        }
        return s1;
    }
    
    public static String convertDateToEta(java.util.Date a) {
        String s = null;
        if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a)) {
            long j = a.getTime() - System.currentTimeMillis();
            int i = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
            s = null;
            if (i > 0) {
                long j0 = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(j);
                if (j0 >= 60L) {
                    long j1 = java.util.concurrent.TimeUnit.MILLISECONDS.toHours(j);
                    long j2 = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(j - java.util.concurrent.TimeUnit.HOURS.toMillis(j1));
                    if (j1 >= 24L) {
                        long j3 = java.util.concurrent.TimeUnit.MILLISECONDS.toDays(j);
                        long j4 = java.util.concurrent.TimeUnit.MILLISECONDS.toHours(j - java.util.concurrent.TimeUnit.DAYS.toMillis(j3));
                        s = new StringBuilder().append(String.valueOf(j3)).append(" ").append(day).append(" ").append(String.valueOf(j4)).append(" ").append(hr).toString();
                    } else {
                        s = new StringBuilder().append(String.valueOf(j1)).append(" ").append(hr).append(" ").append(String.valueOf(j2)).append(" ").append(min_short).toString();
                    }
                } else {
                    s = new StringBuilder().append(String.valueOf(j0)).append(" ").append(min).toString();
                }
            }
        } else {
            s = null;
        }
        return s;
    }
    
    public static int findManeuverIndex(java.util.List a, com.here.android.mpa.routing.Maneuver a0) {
        int i = 0;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 != null) {
                        break label0;
                    }
                }
                i = -1;
                break label2;
            }
            int i0 = a.size();
            Object a1 = a;
            i = 0;
            while(true) {
                if (i >= i0) {
                    i = -1;
                    break;
                } else {
                    if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver)((java.util.List)a1).get(i), a0)) {
                        break;
                    }
                    i = i + 1;
                }
            }
        }
        return i;
    }
    
    private static void formatString(android.text.SpannableStringBuilder a, String s, android.text.style.TextAppearanceSpan a0, String s0, android.text.style.TextAppearanceSpan a1, String s1, android.text.style.TextAppearanceSpan a2, String s2, android.text.style.TextAppearanceSpan a3, boolean b) {
        if (s != null) {
            a.append((CharSequence)s);
            a.setSpan(a0, 0, a.length(), 33);
        }
        if (s0 != null) {
            if (b) {
                a.append((CharSequence)" ");
            }
            int i = a.length();
            a.append((CharSequence)s0);
            a.setSpan(a1, i, a.length(), 33);
        }
        if (s1 != null) {
            int i0 = a.length();
            a.append((CharSequence)" ");
            a.append((CharSequence)s1);
            a.setSpan(a2, i0, a.length(), 33);
        }
        if (s2 != null) {
            if (b) {
                a.append((CharSequence)" ");
            }
            int i1 = a.length();
            a.append((CharSequence)s2);
            a.setSpan(a3, i1, a.length(), 33);
        }
    }
    
    public static void generateRouteIcons(String s, String s0, com.here.android.mpa.routing.Route a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        try {
            android.content.Context a0 = com.navdy.hud.app.HudApplication.getAppContext();
            String s1 = new StringBuilder().append(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath()).append(java.io.File.separator).append("routeicons").append(java.io.File.separator).append(s).append("_").append(com.navdy.hud.app.util.GenericUtil.normalizeToFilename(s0)).toString();
            sLogger.v(new StringBuilder().append("generateRouteIcons: ").append(s1).toString());
            java.io.File a1 = new java.io.File(s1);
            if (a1.exists()) {
                com.navdy.service.library.util.IOUtils.deleteDirectory(a0, a1);
            }
            a1.mkdirs();
            Object a2 = a.getManeuvers().iterator();
            int i = 0;
            while(((java.util.Iterator)a2).hasNext()) {
                com.here.android.mpa.routing.Maneuver a3 = (com.here.android.mpa.routing.Maneuver)((java.util.Iterator)a2).next();
                com.here.android.mpa.common.Image a4 = a3.getNextRoadImage();
                if (a4 != null) {
                    int i0 = (int)a4.getWidth();
                    int i1 = (int)a4.getHeight();
                    sLogger.v(new StringBuilder().append("generateRouteIcons: w=").append(i0).append(" h=").append(i1).toString());
                    android.graphics.Bitmap a5 = a4.getBitmap(i0 * 3, i1 * 3);
                    if (a5 != null) {
                        String s2 = new StringBuilder().append("man_").append(com.navdy.hud.app.util.GenericUtil.normalizeToFilename(com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName(a3))).append("_").append(i0).append("_").append(i1).append(".png").toString();
                        java.io.ByteArrayOutputStream a6 = new java.io.ByteArrayOutputStream();
                        a5.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, (java.io.OutputStream)a6);
                        com.navdy.service.library.util.IOUtils.copyFile(new StringBuilder().append(s1).append(java.io.File.separator).append(s2).toString(), a6.toByteArray());
                        i = i + 1;
                        sLogger.v("icon generated");
                    }
                }
            }
            sLogger.v(new StringBuilder().append("total icons generated:").append(i).toString());
        } catch(Throwable a7) {
            sLogger.e(a7);
        }
    }
    
    public static java.util.List getAheadRouteElements(com.here.android.mpa.routing.Route a, com.here.android.mpa.common.RoadElement a0, com.here.android.mpa.routing.Maneuver a1, com.here.android.mpa.routing.Maneuver a2) {
        java.util.ArrayList a3 = null;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a0 == null) {
                        break label1;
                    }
                    if (a2 != null) {
                        break label0;
                    }
                }
                sLogger.i("getAheadRouteElements:invalid arg");
                a3 = null;
                break label2;
            }
            java.util.List a4 = a.getManeuvers();
            int i = com.navdy.hud.app.maps.here.HereMapUtil.findManeuverIndex(a4, a2);
            if (i != -1) {
                Object a5 = null;
                sLogger.i(new StringBuilder().append("getAheadRouteElements:found maneuver index:").append(i).append(" total:").append(a4.size()).toString());
                a3 = new java.util.ArrayList();
                if (a1 == null) {
                    a5 = a4;
                } else {
                    java.util.List a6 = a1.getRouteElements();
                    if (a6 == null) {
                        a5 = a4;
                    } else if (a6.size() <= 0) {
                        a5 = a4;
                    } else {
                        int i0 = a6.size();
                        Object a7 = a6;
                        a5 = a4;
                        boolean b = false;
                        int i1 = 0;
                        while(i1 < i0) {
                            com.here.android.mpa.routing.RouteElement a8 = (com.here.android.mpa.routing.RouteElement)((java.util.List)a7).get(i1);
                            if (b) {
                                ((java.util.List)a3).add(a8);
                            } else {
                                com.here.android.mpa.common.RoadElement a9 = a8.getRoadElement();
                                if (a9 != null && a9.equals(a0)) {
                                    b = true;
                                }
                            }
                            i1 = i1 + 1;
                        }
                    }
                }
                int i2 = ((java.util.List)a5).size();
                while(i < i2) {
                    ((java.util.List)a3).addAll((java.util.Collection)((com.here.android.mpa.routing.Maneuver)((java.util.List)a5).get(i)).getRouteElements());
                    i = i + 1;
                }
            } else {
                sLogger.i("getAheadRouteElements:maneuver index not found");
                a3 = null;
            }
        }
        return (java.util.List)a3;
    }
    
    public static String getAllRoadNames(java.util.List a, int i) {
        String s = null;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (i >= 0) {
                        break label0;
                    }
                }
                s = "";
                break label2;
            }
            int i0 = a.size();
            if (i < i0) {
                StringBuilder a0 = new StringBuilder();
                Object a1 = a;
                while(i < i0) {
                    a0.append("[");
                    a0.append(com.navdy.hud.app.maps.here.HereMapUtil.getRoadName((com.here.android.mpa.routing.Maneuver)((java.util.List)a1).get(i)));
                    a0.append("]");
                    a0.append(" ");
                    i = i + 1;
                }
                s = a0.toString();
            } else {
                s = "";
            }
        }
        return s;
    }
    
    public static String getCurrentEtaString() {
        String s = null;
        try {
            if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                boolean b = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn();
                s = null;
                if (b) {
                    com.navdy.hud.app.ui.component.homescreen.HomeScreenView a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                    s = null;
                    if (a != null) {
                        s = a.getEtaView().getLastEtaDate();
                    }
                }
            } else {
                s = null;
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
            s = null;
        }
        return s;
    }
    
    public static String getCurrentEtaStringWithDestination() {
        String s = null;
        try {
            if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                com.navdy.hud.app.maps.here.HereNavigationManager a = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
                if (a.isNavigationModeOn()) {
                    String s0 = a.getDestinationLabel();
                    if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        s0 = a.getDestinationStreetAddress();
                        if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                            s0 = null;
                        }
                    }
                    android.content.res.Resources a0 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
                    if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
                        if (s0 == null) {
                            s = a0.getString(R.string.mm_active_trip_arrived_no_label);
                        } else {
                            Object[] a1 = new Object[1];
                            a1[0] = s0;
                            s = a0.getString(R.string.mm_active_trip_arrived, a1);
                        }
                    } else {
                        s = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentEtaString();
                        if (s != null) {
                            if (s0 != null) {
                                android.content.Context a2 = com.navdy.hud.app.HudApplication.getAppContext();
                                Object[] a3 = new Object[2];
                                a3[0] = s;
                                a3[1] = s0;
                                s = a2.getString(R.string.mm_active_trip_eta, a3);
                            }
                        } else {
                            s = null;
                        }
                    }
                } else {
                    s = null;
                }
            } else {
                s = null;
            }
        } catch(Throwable a4) {
            sLogger.e(a4);
            s = null;
        }
        return s;
    }
    
    public static com.here.android.mpa.common.RoadElement getCurrentRoadElement() {
        com.here.android.mpa.common.PositioningManager a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getPositioningManager();
        return (a != null) ? a.getRoadElement() : null;
    }
    
    public static String getCurrentRoadName() {
        com.here.android.mpa.common.PositioningManager a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getPositioningManager();
        String s = null;
        if (a != null) {
            com.here.android.mpa.common.RoadElement a0 = a.getRoadElement();
            s = null;
            if (a0 != null) {
                s = com.navdy.hud.app.maps.here.HereMapUtil.getRoadName(a0);
            }
        }
        return s;
    }
    
    public static float getCurrentSpeedLimit() {
        com.here.android.mpa.common.RoadElement a = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
        return (a == null) ? 0.0f : a.getSpeedLimit();
    }
    
    static java.util.List getDifferentManeuver(com.here.android.mpa.routing.Route a, com.here.android.mpa.routing.Route a0, int i, java.util.List a1) {
        java.util.ArrayList a2 = new java.util.ArrayList();
        java.util.List a3 = a.getManeuvers();
        java.util.List a4 = a0.getManeuvers();
        int i0 = a3.size();
        int i1 = a4.size();
        int i2 = (i0 <= i1) ? i0 + -1 : i1 - 1;
        Object a5 = a3;
        int i3 = 0;
        Object a6 = a1;
        Object a7 = a4;
        while(true) {
            label0: if (i3 <= i2) {
                com.here.android.mpa.routing.Maneuver a8 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a5).get(i3);
                com.here.android.mpa.routing.Maneuver a9 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a7).get(i3);
                if (sLogger.isLoggable(2)) {
                    com.navdy.hud.app.maps.here.HereMapUtil.printManeuverDetails(a8, "getDifferentManeuver-1");
                    com.navdy.hud.app.maps.here.HereMapUtil.printManeuverDetails(a9, "getDifferentManeuver-2");
                }
                boolean b = com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual(a8, a9);
                label1: {
                    if (b) {
                        break label1;
                    }
                    String s = com.navdy.hud.app.maps.here.HereMapUtil.getNextRoadName(a8);
                    if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                        i3 = i3 + 1;
                        continue;
                    }
                    if (sLogger.isLoggable(2)) {
                        sLogger.v(new StringBuilder().append("getDifferentManeuver:diff:").append(s).toString());
                    }
                    ((java.util.List)a2).add(a8);
                    ((java.util.List)a6).add(a9);
                    i = i + -1;
                    if (i == 0) {
                        break label0;
                    }
                }
                i3 = i3 + 1;
                continue;
            }
            return (java.util.List)a2;
        }
    }
    
    public static long getDistanceToRoadElement(com.here.android.mpa.common.RoadElement a, com.here.android.mpa.common.RoadElement a0, java.util.List a1) {
        long j = 0L;
        label0: {
            label3: {
                label4: {
                    if (a == null) {
                        break label4;
                    }
                    if (a0 == null) {
                        break label4;
                    }
                    if (a1 != null) {
                        break label3;
                    }
                }
                j = -1L;
                break label0;
            }
            int i = a1.size();
            Object a2 = a1;
            j = -1L;
            boolean b = false;
            int i0 = 0;
            while(true) {
                if (i0 >= i) {
                    j = -1L;
                    break;
                } else {
                    java.util.List a3 = ((com.here.android.mpa.routing.Maneuver)((java.util.List)a2).get(i0)).getRoadElements();
                    int i1 = a3.size();
                    Object a4 = a3;
                    int i2 = 0;
                    while(i2 < i1) {
                        com.here.android.mpa.common.RoadElement a5 = (com.here.android.mpa.common.RoadElement)((java.util.List)a4).get(i2);
                        label1: {
                            label2: {
                                if (b) {
                                    break label2;
                                }
                                if (!a5.equals(a)) {
                                    break label1;
                                }
                                j = (long)a5.getGeometryLength();
                                b = true;
                                break label1;
                            }
                            if (a5.equals(a0)) {
                                break label0;
                            }
                            j = j + (long)a5.getGeometryLength();
                        }
                        i2 = i2 + 1;
                    }
                    i0 = i0 + 1;
                }
            }
        }
        return j;
    }
    
    public static android.text.SpannableStringBuilder getEtaString(com.here.android.mpa.routing.Route a, String s, boolean b) {
        android.text.SpannableStringBuilder a0 = new android.text.SpannableStringBuilder();
        if (a != null) {
            long j = 0L;
            java.util.Date a1 = com.navdy.hud.app.maps.here.HereMapUtil.getRouteTtaDate(a);
            if (a1 == null) {
                sLogger.i(new StringBuilder().append("tta date is null for route:").append(s).toString());
                j = 0L;
            } else {
                j = a1.getTime() - System.currentTimeMillis();
            }
            if (j > 0L) {
                long j0 = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(j);
                if (j0 >= 60L) {
                    long j1 = java.util.concurrent.TimeUnit.MILLISECONDS.toHours(j);
                    long j2 = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(j - java.util.concurrent.TimeUnit.HOURS.toMillis(j1));
                    if (j1 >= 10L) {
                        if (j1 >= 24L) {
                            long j3 = java.util.concurrent.TimeUnit.MILLISECONDS.toDays(j);
                            long j4 = java.util.concurrent.TimeUnit.MILLISECONDS.toHours(j - java.util.concurrent.TimeUnit.DAYS.toMillis(j3));
                            if (j3 != 1L) {
                                com.navdy.hud.app.maps.here.HereMapUtil.formatString(a0, String.valueOf(j3), sp_24_1, day, sp_20_b_1, String.valueOf(j4), sp_24_2, hr, sp_20_b_2, b);
                            } else {
                                com.navdy.hud.app.maps.here.HereMapUtil.formatString(a0, String.valueOf(j3), sp_26_1, day, sp_20_b_1, String.valueOf(j4), sp_26_2, hr, sp_20_b_2, b);
                            }
                        } else {
                            com.navdy.hud.app.maps.here.HereMapUtil.formatString(a0, String.valueOf(j1), sp_24_1, hr, sp_20_b_1, String.valueOf(j2), sp_24_2, min_short, sp_20_b_2, b);
                        }
                    } else {
                        com.navdy.hud.app.maps.here.HereMapUtil.formatString(a0, String.valueOf(j1), sp_26_1, hr, sp_20_b_1, String.valueOf(j2), sp_26_2, min_short, sp_20_b_2, b);
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereMapUtil.formatString(a0, String.valueOf(j0), sp_26_1, min, sp_20_b_1, (String)null, (android.text.style.TextAppearanceSpan)null, (String)null, (android.text.style.TextAppearanceSpan)null, true);
                }
            }
        }
        return a0;
    }
    
    public static com.here.android.mpa.routing.Maneuver getLastEqualManeuver(com.here.android.mpa.routing.Route a, com.here.android.mpa.routing.Route a0) {
        java.util.List a1 = a.getManeuvers();
        java.util.List a2 = a0.getManeuvers();
        int i = (a1.size() >= a2.size()) ? a2.size() - 1 : a1.size() - 1;
        Object a3 = a1;
        Object a4 = a2;
        int i0 = 0;
        while(true) {
            com.here.android.mpa.routing.Maneuver a5 = null;
            if (i0 <= i) {
                boolean b = com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual((com.here.android.mpa.routing.Maneuver)((java.util.List)a3).get(i0), (com.here.android.mpa.routing.Maneuver)((java.util.List)a4).get(i0));
                label0: {
                    label1: {
                        if (!b) {
                            break label1;
                        }
                        if (i0 > 0) {
                            break label0;
                        }
                    }
                    i0 = i0 + 1;
                    continue;
                }
                a5 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a3).get(i0 - 1);
            }
            return a5;
        }
    }
    
    public static com.navdy.service.library.device.NavdyDeviceId getLastKnownDeviceId() {
        com.navdy.service.library.device.NavdyDeviceId a = null;
        String s = lastKnownDeviceId;
        label0: {
            Throwable a0 = null;
            if (s == null) {
                a = null;
                break label0;
            } else {
                try {
                    a = new com.navdy.service.library.device.NavdyDeviceId(lastKnownDeviceId);
                    lastKnownDeviceId = null;
                    break label0;
                } catch(Throwable a1) {
                    a0 = a1;
                }
            }
            lastKnownDeviceId = null;
            sLogger.e(a0);
            a = null;
        }
        return a;
    }
    
    public static com.here.android.mpa.routing.Maneuver getLastManeuver(java.util.List a) {
        com.here.android.mpa.routing.Maneuver a0 = null;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.size() > 1) {
                        break label0;
                    }
                }
                a0 = null;
                break label2;
            }
            a0 = (com.here.android.mpa.routing.Maneuver)a.get(a.size() - 2);
        }
        return a0;
    }
    
    public static long getManeuverDriveTime(com.here.android.mpa.routing.Maneuver a, com.navdy.hud.app.maps.here.HereMapUtil$LongContainer a0) {
        long j = 0L;
        label2: if (a != null) {
            java.util.List a1 = a.getRoadElements();
            label0: {
                label1: {
                    if (a1 == null) {
                        break label1;
                    }
                    if (a1.size() != 0) {
                        break label0;
                    }
                }
                j = 0L;
                break label2;
            }
            if (a0 != null) {
                a0.val = 0L;
            }
            java.util.Iterator a2 = a1.iterator();
            j = 0L;
            Object a3 = a2;
            while(((java.util.Iterator)a3).hasNext()) {
                com.here.android.mpa.common.RoadElement a4 = (com.here.android.mpa.common.RoadElement)((java.util.Iterator)a3).next();
                if (a4 != null && a4.getDefaultSpeed() > 0.0f) {
                    double d = a4.getGeometryLength();
                    j = j + Math.round(d / (double)a4.getDefaultSpeed());
                    if (a0 != null) {
                        a0.val = (long)((double)a0.val + d);
                    }
                }
            }
        } else {
            j = 0L;
        }
        return j;
    }
    
    public static String getNextRoadName(com.here.android.mpa.routing.Maneuver a) {
        boolean b = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a);
        return com.navdy.hud.app.maps.here.HereMapUtil.concatText(a.getNextRoadNumber(), a.getNextRoadName(), true, b);
    }
    
    public static String getRoadName(com.here.android.mpa.common.RoadElement a) {
        String s = null;
        if (a != null) {
            boolean b = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a);
            s = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a.getRouteName(), a.getRoadName(), true, b);
        } else {
            s = null;
        }
        return s;
    }
    
    public static String getRoadName(com.here.android.mpa.routing.Maneuver a) {
        boolean b = com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway(a);
        return com.navdy.hud.app.maps.here.HereMapUtil.concatText(a.getRoadNumber(), a.getRoadName(), true, b);
    }
    
    public static String getRoadName(java.util.List a) {
        String s = null;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.size() != 0) {
                        break label0;
                    }
                }
                s = "";
                break label2;
            }
            Object a0 = a.iterator();
            while(true) {
                if (((java.util.Iterator)a0).hasNext()) {
                    com.here.android.mpa.common.RoadElement a1 = (com.here.android.mpa.common.RoadElement)((java.util.Iterator)a0).next();
                    s = a1.getRoadName();
                    if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                        break;
                    }
                    s = a1.getRouteName();
                    {
                        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                            continue;
                        }
                        break;
                    }
                } else {
                    s = "";
                    break;
                }
            }
        }
        return s;
    }
    
    public static java.util.Date getRouteTtaDate(com.here.android.mpa.routing.Route a) {
        java.util.Date a0 = null;
        label0: {
            Throwable a1 = null;
            if (a != null) {
                try {
                    com.here.android.mpa.routing.RouteTta a2 = a.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, 268435455);
                    if (a2 != null && a2.getDuration() == 0) {
                        a2 = null;
                    }
                    if (a2 == null) {
                        a2 = a.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.DISABLED, 268435455);
                    }
                    a0 = null;
                    if (a2 == null) {
                        break label0;
                    }
                    int i = a2.getDuration();
                    a0 = new java.util.Date(System.currentTimeMillis() + java.util.concurrent.TimeUnit.SECONDS.toMillis((long)i));
                    break label0;
                } catch(Throwable a3) {
                    a1 = a3;
                }
            } else {
                a0 = null;
                break label0;
            }
            sLogger.e(a1);
            a0 = null;
        }
        return a0;
    }
    
    public static com.navdy.hud.app.maps.here.HereMapUtil$SavedRouteData getSavedRouteData(com.navdy.service.library.log.Logger a) {
        com.navdy.hud.app.maps.here.HereMapUtil$SavedRouteData a0 = null;
        synchronized(com.navdy.hud.app.maps.here.HereMapUtil.class) {
            boolean b = false;
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            com.navdy.service.library.events.navigation.NavigationRouteRequest a1 = com.navdy.hud.app.maps.here.HereMapUtil.getSavedRouteRequest(a);
            label2: {
                label0: {
                    label1: {
                        if (a1 == null) {
                            break label1;
                        }
                        if (a1.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                            break label0;
                        }
                    }
                    b = false;
                    break label2;
                }
                b = true;
            }
            a0 = new com.navdy.hud.app.maps.here.HereMapUtil$SavedRouteData(a1, b);
        }
        /*monexit(com.navdy.hud.app.maps.here.HereMapUtil.class)*/;
        return a0;
    }
    
    private static com.navdy.service.library.events.navigation.NavigationRouteRequest getSavedRouteRequest(com.navdy.service.library.log.Logger a) {
        com.navdy.service.library.events.navigation.NavigationRouteRequest a0 = null;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        a.v("getSavedRouteRequest");
        lastKnownDeviceId = null;
        label0: {
            java.io.FileInputStream a1 = null;
            Throwable a2 = null;
            label4: try {
                boolean b = false;
                String s = com.navdy.hud.app.storage.PathManager.getInstance().getActiveRouteInfoDir();
                java.io.File a3 = new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append("gasrouterequest.bin").toString());
                if (a3.exists()) {
                    if (com.navdy.hud.app.maps.here.HereMapUtil.isRouteFileStale(a3)) {
                        long j = a3.lastModified();
                        boolean b0 = com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a3.getAbsolutePath());
                        a.v(new StringBuilder().append("getSavedRouteRequest: gas route is stale file-time =").append(j).append(" now=").append(System.currentTimeMillis()).append(" deleted=").append(b0).toString());
                        b = false;
                    } else {
                        a.v(new StringBuilder().append("getSavedRouteRequest: gas route is good, file-time =").append(a3.lastModified()).append(" now=").append(System.currentTimeMillis()).toString());
                        b = true;
                    }
                } else {
                    b = false;
                }
                label6: {
                    if (b) {
                        break label6;
                    }
                    a.v("getSavedRouteRequest: no saved gas route");
                    a3 = new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append("routerequest.bin").toString());
                    boolean b1 = a3.exists();
                    label7: {
                        if (b1) {
                            break label7;
                        }
                        a.v("getSavedRouteRequest: no saved route");
                        a0 = null;
                        break label0;
                    }
                    boolean b2 = com.navdy.hud.app.maps.here.HereMapUtil.isRouteFileStale(a3);
                    label5: {
                        if (b2) {
                            break label5;
                        }
                        a.v(new StringBuilder().append("getSavedRouteRequest: route is good file-time =").append(a3.lastModified()).append(" now=").append(System.currentTimeMillis()).toString());
                        break label6;
                    }
                    long j0 = a3.lastModified();
                    boolean b3 = com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a3.getAbsolutePath());
                    a.v(new StringBuilder().append("getSavedRouteRequest: route is stale file-time =").append(j0).append(" now=").append(System.currentTimeMillis()).append(" deleted=").append(b3).toString());
                    a0 = null;
                    break label0;
                }
                a1 = new java.io.FileInputStream(a3);
                com.squareup.wire.Wire a4 = wire;
                label3: {
                    try {
                        a0 = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder((com.navdy.service.library.events.navigation.NavigationRouteRequest)a4.parseFrom((java.io.InputStream)a1, com.navdy.service.library.events.navigation.NavigationRouteRequest.class)).requestId(java.util.UUID.randomUUID().toString()).build();
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                        break label3;
                    } catch(Throwable a5) {
                        a2 = a5;
                    }
                    break label4;
                }
                java.io.File a6 = new java.io.File(new StringBuilder().append(s).append(java.io.File.separator).append("routerequest.device").toString());
                if (a6.exists()) {
                    String s0 = com.navdy.service.library.util.IOUtils.convertFileToString(a6.getAbsolutePath());
                    com.navdy.service.library.device.NavdyDeviceId a7 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
                    label1: {
                        label2: {
                            if (a7 == null) {
                                break label2;
                            }
                            if (!a7.equals(new com.navdy.service.library.device.NavdyDeviceId(s0))) {
                                break label1;
                            }
                        }
                        lastKnownDeviceId = s0;
                        a.v("getSavedRouteRequest: returning route");
                        break label0;
                    }
                    a.v(new StringBuilder().append("getSavedRouteRequest: device id is different now[").append(a7).append("] stored[").append(s0).append("]").toString());
                    com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(a, true);
                    a0 = null;
                    break label0;
                } else {
                    a.v("getSavedRouteRequest: no saved device id");
                    break label0;
                }
            } catch(Throwable a8) {
                a2 = a8;
                a1 = null;
            }
            sLogger.e(a2);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
            com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(a, true);
            lastKnownDeviceId = null;
            a0 = null;
        }
        return a0;
    }
    
    public static String getSignpostText(com.here.android.mpa.routing.Signpost a, com.here.android.mpa.routing.Maneuver a0, com.navdy.hud.app.maps.here.HereMapUtil$SignPostInfo a1, java.util.ArrayList a2) {
        StringBuilder a3 = null;
        Throwable a4 = null;
        label0: {
            String s = null;
            if (a == null) {
                s = null;
            } else {
                synchronized(signPostBuilder) {
                    StringBuilder a5 = signPostBuilder;
                    boolean b = false;
                    boolean b0 = false;
                    a5.setLength(0);
                    signPostBuilder2.setLength(0);
                    signPostSet.clear();
                    String s0 = a.getExitNumber();
                    String s1 = a.getExitText();
                    if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        b = false;
                        b0 = false;
                    } else {
                        if (a0.getAction() == com.here.android.mpa.routing.Maneuver.Action.LEAVE_HIGHWAY) {
                            b0 = false;
                            b = true;
                        } else {
                            signPostBuilder.append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.EXIT_NUMBER);
                            signPostBuilder.append(" ");
                            b0 = true;
                            b = false;
                        }
                        signPostBuilder.append(s0);
                        if (a2 != null) {
                            a2.add(s0);
                        }
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                        if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(signPostBuilder)) {
                            signPostBuilder.append(" ");
                        }
                        signPostBuilder.append(s1);
                        if (a2 != null) {
                            a2.add(s1);
                        }
                    }
                    if (b) {
                        signPostBuilder.append(" ");
                    }
                    Object a6 = a.getExitDirections().iterator();
                    while(((java.util.Iterator)a6).hasNext()) {
                        com.here.android.mpa.routing.Signpost.LocalizedLabel a7 = (com.here.android.mpa.routing.Signpost.LocalizedLabel)((java.util.Iterator)a6).next();
                        if (a7.getLanguage().equalsIgnoreCase(TBT_ISO3_LANG_CODE)) {
                            String s2 = a7.getRouteName();
                            String s3 = a7.getRouteDirection();
                            String s4 = a7.getText();
                            boolean b1 = android.text.TextUtils.isEmpty((CharSequence)s2);
                            boolean b2 = b0;
                            if (!b1) {
                                if (a1 != null) {
                                    a1.hasNumberInSignPost = true;
                                }
                                b2 = b0;
                                if (b0) {
                                    if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(signPostBuilder)) {
                                        signPostBuilder.append("/");
                                        b2 = false;
                                    } else {
                                        b2 = false;
                                    }
                                }
                                if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(signPostBuilder)) {
                                    signPostBuilder.append("/");
                                }
                                signPostBuilder.append(s2);
                            }
                            if (!android.text.TextUtils.isEmpty((CharSequence)s3)) {
                                if (a1 != null && a1.hasNumberInSignPost) {
                                    signPostBuilder.append(" ");
                                }
                                if (a2 != null && !android.text.TextUtils.isEmpty((CharSequence)s2)) {
                                    a2.add(new StringBuilder().append(s2).append(" ").append(s3).toString());
                                }
                                signPostBuilder.append(s3);
                            }
                            boolean b3 = android.text.TextUtils.isEmpty((CharSequence)s4);
                            b0 = b2;
                            if (b3) {
                                b0 = b2;
                            } else if (!signPostSet.contains(s4)) {
                                if (a1 != null) {
                                    a1.hasNameInSignPost = true;
                                }
                                if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(signPostBuilder2)) {
                                    signPostBuilder2.append("/");
                                }
                                signPostBuilder2.append(s4);
                                signPostSet.add(s4);
                            }
                        }
                    }
                    if (signPostBuilder2.length() > 0) {
                        if (com.navdy.hud.app.maps.here.HereMapUtil.needSeparator(signPostBuilder)) {
                            signPostBuilder.append("/");
                        }
                        signPostBuilder.append(signPostBuilder2.toString());
                    }
                    s = signPostBuilder.toString();
                    /*monexit(a3)*/;
                }
            }
            return s;
        }
//        while(true) {
//            try {
//                /*monexit(a3)*/;
//            } catch(IllegalMonitorStateException | NullPointerException a9) {
//                Throwable a10 = a9;
//                a4 = a10;
//                continue;
//            }
//            throw a4;
//        }
//        return null;
    }
    
    public static boolean hasSameSignpostAndToRoad(com.here.android.mpa.routing.Maneuver a, com.here.android.mpa.routing.Maneuver a0) {
        boolean b = false;
        label0: {
            label4: {
                label5: {
                    if (a == null) {
                        break label5;
                    }
                    if (a0 != null) {
                        break label4;
                    }
                }
                b = false;
                break label0;
            }
            String s = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a.getNextRoadNumber(), a.getNextRoadName(), true, false);
            String s0 = com.navdy.hud.app.maps.here.HereMapUtil.concatText(a0.getNextRoadNumber(), a0.getNextRoadName(), true, false);
            boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
            label2: {
                label3: {
                    if (b0) {
                        break label3;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        break label3;
                    }
                    if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)s0)) {
                        break label2;
                    }
                }
                b = false;
                break label0;
            }
            String s1 = com.navdy.hud.app.maps.here.HereMapUtil.getSignpostText(a.getSignpost(), a, (com.navdy.hud.app.maps.here.HereMapUtil$SignPostInfo)null, (java.util.ArrayList)null);
            String s2 = com.navdy.hud.app.maps.here.HereMapUtil.getSignpostText(a0.getSignpost(), a0, (com.navdy.hud.app.maps.here.HereMapUtil$SignPostInfo)null, (java.util.ArrayList)null);
            boolean b1 = android.text.TextUtils.isEmpty((CharSequence)s1);
            label1: {
                if (b1) {
                    break label1;
                }
                if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
                    break label1;
                }
                if (android.text.TextUtils.equals((CharSequence)s1, (CharSequence)s2)) {
                    b = true;
                    break label0;
                }
            }
            b = false;
        }
        return b;
    }
    
    public static boolean isCoordinateEqual(com.here.android.mpa.common.GeoCoordinate a, com.here.android.mpa.common.GeoCoordinate a0) {
        boolean b = false;
        double d = a.getLatitude();
        double d0 = a.getLongitude();
        double d1 = a0.getLatitude();
        double d2 = a0.getLongitude();
        int i = (d > d1) ? 1 : (d == d1) ? 0 : -1;
        label2: {
            label0: {
                label1: {
                    if (i != 0) {
                        break label1;
                    }
                    if (d0 == d2) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static boolean isCurrentRoadHighway(com.here.android.mpa.common.RoadElement a) {
        return a != null && a.getAttributes().contains(com.here.android.mpa.common.RoadElement.Attribute.HIGHWAY);
    }
    
    public static boolean isCurrentRoadHighway(com.here.android.mpa.routing.Maneuver a) {
        boolean b = false;
        if (a != null) {
            if (com.navdy.hud.app.maps.here.HereMapUtil.isManeuverActionHighway(a.getAction())) {
                b = true;
            } else {
                java.util.List a0 = a.getRoadElements();
                b = a0 != null && a0.size() != 0 && com.navdy.hud.app.maps.here.HereMapUtil.isCurrentRoadHighway((com.here.android.mpa.common.RoadElement)a0.get(0));
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public static boolean isGeoCoordinateEquals(com.here.android.mpa.common.GeoCoordinate a, com.here.android.mpa.common.GeoCoordinate a0) {
        boolean b = false;
        double d = a.getLatitude();
        double d0 = a0.getLatitude();
        int i = (d > d0) ? 1 : (d == d0) ? 0 : -1;
        label2: {
            label0: {
                label1: {
                    if (i != 0) {
                        break label1;
                    }
                    if (a.getLongitude() == a0.getLongitude()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static boolean isHighwayAction(com.here.android.mpa.routing.Maneuver a) {
        boolean b = false;
        if (a != null) {
            com.here.android.mpa.routing.Maneuver.Action a0 = a.getAction();
            if (a0 == null) {
                b = false;
            } else {
                switch(a0) {
                    case END: case ENTER_HIGHWAY: case ENTER_HIGHWAY_FROM_LEFT: {
                        b = true;
                        break;
                    }
                    default: {
                        b = false;
                    }
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public static boolean isInTunnel(com.here.android.mpa.common.RoadElement a) {
        boolean b = false;
        label2: {
            java.util.EnumSet a0 = null;
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    a0 = a.getAttributes();
                    if (a0 != null) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = a0.contains(com.here.android.mpa.common.RoadElement.Attribute.TUNNEL);
        }
        return b;
    }
    
    public static boolean isManeuverActionHighway(com.here.android.mpa.routing.Maneuver.Action a) {
        boolean b = false;
        if (a != null) {
            switch(com.navdy.hud.app.maps.here.HereMapUtil$2.$SwitchMap$com$here$android$mpa$routing$Maneuver$Action[a.ordinal()]) {
                case 1: case 2: case 3: case 4: case 5: {
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public static boolean isManeuverShort(com.here.android.mpa.routing.Maneuver a) {
        return a.getDistanceToNextManeuver() <= 20;
    }
    
    static boolean isRouteFileStale(java.io.File a) {
        boolean b = false;
        long j = a.lastModified();
        long j0 = System.currentTimeMillis() - j;
        long j1 = STALE_ROUTE_TIME;
        int i = (j0 < j1) ? -1 : (j0 == j1) ? 0 : 1;
        label2: {
            label0: {
                label1: {
                    if (i >= 0) {
                        break label1;
                    }
                    if (j0 >= 0L) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static boolean isSavedRoute(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a.routeAttributes == null) {
                        break label1;
                    }
                    if (a.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static boolean isStartManeuver(com.here.android.mpa.routing.Maneuver a) {
        return a != null && a.getIcon() == com.here.android.mpa.routing.Maneuver.Icon.START;
    }
    
    public static boolean isValidEtaDate(java.util.Date a) {
        boolean b = false;
        label2: if (a != null) {
            int i = a.getYear();
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (i > 71) {
                        break label0;
                    }
                }
                sLogger.e(new StringBuilder().append("invalid eta date[").append(a).append("]").toString());
                b = false;
                break label2;
            }
            long j = System.currentTimeMillis() - a.getTime();
            if (j < 360000L) {
                if (j > 0L) {
                    sLogger.i(new StringBuilder().append("eta is behind timediff[").append(j).append("]").toString());
                }
                b = true;
            } else {
                sLogger.e(new StringBuilder().append("invalid eta timediff[").append(j).append("]").toString());
                b = false;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    static boolean isValidNavigationState(com.navdy.service.library.events.navigation.NavigationSessionState a) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (a == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STARTED) {
                        break label0;
                    }
                    if (a == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_PAUSED) {
                        break label0;
                    }
                    if (a == com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public static void loadImage(com.here.android.mpa.common.Image a, int i, int i0, com.navdy.hud.app.maps.here.HereMapUtil$IImageLoadCallback a0) {
        if (a != null && a0 != null && i > 0 && i0 > 0) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapUtil$1(a, i, i0, a0), 1);
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public static boolean needSeparator(StringBuilder a) {
        boolean b = false;
        if (a != null) {
            int i = a.length();
            if (i == 0) {
                b = false;
            } else {
                int i0 = a.charAt(i - 1);
                b = i0 != 47 && i0 != 32;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    public static String parseStreetAddress(String s) {
        if (s != null) {
            int i = s.indexOf(",");
            if (i >= 0) {
                s = s.substring(0, i);
            }
        } else {
            s = null;
        }
        return s;
    }
    
    public static void printManeuverDetails(com.here.android.mpa.routing.Maneuver a, String s) {
        sLogger.v(new StringBuilder().append(s).append(" turn[").append(a.getTurn().name()).append("] to[").append(a.getNextRoadNumber()).append(" ").append(a.getNextRoadName()).append("] distance[").append(a.getDistanceToNextManeuver()).append(" meters] action[").append(a.getAction()).append("] current[").append(a.getRoadNumber()).append(" ").append(a.getRoadName()).append("] distanceFromStart[").append(a.getDistanceFromStart()).append(" meters]").toString());
    }
    
    public static void printRouteDetails(com.here.android.mpa.routing.Route a, String s, com.navdy.service.library.events.navigation.NavigationRouteRequest a0, StringBuilder a1) {
        com.navdy.hud.app.maps.here.HereMapUtil.printRouteDetails(a, s, a0, a1, (com.here.android.mpa.routing.Maneuver)null);
    }
    
    public static void printRouteDetails(com.here.android.mpa.routing.Route a, String s, com.navdy.service.library.events.navigation.NavigationRouteRequest a0, StringBuilder a1, com.here.android.mpa.routing.Maneuver a2) {
        label0: if (a != null) {
            long j = android.os.SystemClock.elapsedRealtime();
            com.here.android.mpa.routing.RouteTta a3 = a.getTta(com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL, 268435455);
            String s0 = new StringBuilder().append("[Route] start[").append(a.getStart()).append("] end[").append(a.getDestination()).append("] length[").append(a.getLength()).append(" meters] tta[").append(a3.getDuration()).append(" seconds]").toString();
            if (a1 != null) {
                a1.append(new StringBuilder().append(s0).append("\n").toString());
            } else {
                sLogger.v(s0);
            }
            java.util.List a4 = a.getManeuvers();
            label1: {
                if (a2 == null) {
                    break label1;
                }
                int i = com.navdy.hud.app.maps.here.HereMapUtil.findManeuverIndex(a4, a2);
                if (i < 0) {
                    break label0;
                }
                a4 = a4.subList(i, a4.size());
            }
            int i0 = a4.size();
            com.here.android.mpa.routing.Maneuver a5 = null;
            com.here.android.mpa.routing.Maneuver a6 = null;
            Object a7 = a4;
            int i1 = 0;
            while(i1 < i0) {
                if (a5 != null) {
                    a6 = a5;
                }
                a5 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a7).get(i1);
                int i2 = i0 - 1;
                com.here.android.mpa.routing.Maneuver a8 = null;
                if (i1 < i2) {
                    a8 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a7).get(i1 + 1);
                }
                com.here.android.mpa.routing.Maneuver.Icon a9 = a5.getIcon();
                String s1 = (a9 == null) ? "" : a9.name();
                com.here.android.mpa.routing.Maneuver.TrafficDirection a10 = a5.getTrafficDirection();
                String s2 = (a10 == null) ? "" : a10.name();
                String s3 = new StringBuilder().append("    [Maneuver-Raw] turn[").append(a5.getTurn().name()).append("] to[").append(a5.getNextRoadNumber()).append(" ").append(a5.getNextRoadName()).append("] distance[").append(a5.getDistanceToNextManeuver()).append(" meters] action[").append(a5.getAction()).append("] current[").append(a5.getRoadNumber()).append(" ").append(a5.getRoadName()).append("] traffic-directon[").append(s2).append("] angle[").append(a5.getAngle()).append("] orientation[").append(a5.getMapOrientation()).append("] distanceFromStart[").append(a5.getDistanceFromStart()).append(" meters] Icon[").append(s1).append("]").toString();
                if (a1 == null) {
                    sLogger.v(s3);
                } else {
                    a1.append(new StringBuilder().append(s3).append("\n").toString());
                }
                com.here.android.mpa.routing.Signpost a11 = a5.getSignpost();
                if (a11 != null) {
                    String s4 = new StringBuilder().append("    [Maneuver-Raw-SignPost] exitNumber[").append(a11.getExitNumber()).append("] exitText[").append(a11.getExitText()).append("]").toString();
                    if (a1 == null) {
                        sLogger.v(s4);
                    } else {
                        a1.append(s4);
                    }
                    Object a12 = a11.getExitDirections().iterator();
                    while(((java.util.Iterator)a12).hasNext()) {
                        com.here.android.mpa.routing.Signpost.LocalizedLabel a13 = (com.here.android.mpa.routing.Signpost.LocalizedLabel)((java.util.Iterator)a12).next();
                        String s5 = new StringBuilder().append("     [Maneuver-Raw-SignPost-Label]  text[").append(a13.getText()).append("] routeDirection[").append(a13.getRouteDirection()).append("] routeName[").append(a13.getRouteName()).append("]").toString();
                        if (a1 == null) {
                            sLogger.v(s5);
                        } else {
                            a1.append(new StringBuilder().append(s5).append("\n").toString());
                        }
                    }
                }
                com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a14 = (i1 != 0) ? com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverDisplay(a5, a8 == null, (long)a5.getDistanceToNextManeuver(), s, a6, a0, a8, true, false, (com.navdy.hud.app.maps.MapEvents$DestinationDirection)null) : com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getStartManeuverDisplay(a5, a8);
                sLogger.v(new StringBuilder().append("[Maneuver-Display] ").append(a14.toString()).toString());
                String s6 = new StringBuilder().append("[Maneuver-Display-short] ").append(a14.pendingTurn).append(" ").append(a14.pendingRoad).append(" ( ").append(a14.distanceToPendingRoadText).append(" )").toString();
                if (a1 == null) {
                    sLogger.v(s6);
                } else {
                    a1.append(new StringBuilder().append(s6).append("\n").toString());
                }
                i1 = i1 + 1;
            }
            long j0 = android.os.SystemClock.elapsedRealtime();
            sLogger.v(new StringBuilder().append("Time to print route:").append(j0 - j).toString());
        }
    }
    
    static void printTrafficEvent(com.here.android.mpa.mapping.TrafficEvent a, String s, com.here.android.mpa.routing.Route a0) {
        synchronized(trafficEventBuilder) {
            StringBuilder a2 = trafficEventBuilder;
            a2.setLength(0);
            trafficEventBuilder.append(s);
            trafficEventBuilder.append(":: ");
            trafficEventBuilder.append(new StringBuilder().append("severity:").append(a.getSeverity().name()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", short-text:").append(a.getShortText()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", text:").append(a.getEventText()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", isActive:").append(a.isActive()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", isFlow:").append(a.isFlow()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", isIncident:").append(a.isIncident()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", isVisible:").append(a.isVisible()).toString());
            StringBuilder a3 = trafficEventBuilder;
            StringBuilder a4 = new StringBuilder().append(", isOnRoute:");
            Object a5 = (a0 != null) ? Boolean.valueOf(a.isOnRoute(a0)) : "n/a";
            a3.append(a4.append(a5).toString());
            trafficEventBuilder.append(new StringBuilder().append(", isReroutable:").append(a.isReroutable()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", affected len:").append(a.getAffectedLength()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", offRoute icon:").append(a.getIconOffRoute()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", onRoute icon:").append(a.getIconOnRoute()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", firstAffectedStreet:").append(a.getFirstAffectedStreet()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", activateDate:").append(a.getActivationDate()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", updateDate:").append(a.getUpdateDate()).toString());
            trafficEventBuilder.append(new StringBuilder().append(", now:").append(new java.util.Date()).toString());
            java.util.List a6 = a.getAffectedStreets();
            trafficEventBuilder.append(", affectedStreet: ");
            if (a6 != null) {
                Object a7 = a6.iterator();
                while(((java.util.Iterator)a7).hasNext()) {
                    String s0 = (String)((java.util.Iterator)a7).next();
                    trafficEventBuilder.append(s0);
                    trafficEventBuilder.append(", ");
                }
            }
            sLogger.v(trafficEventBuilder.toString());
            trafficEventBuilder.setLength(0);
            /*monexit(a1)*/;
        }
    }
    
    static void printTrafficNotification(com.here.android.mpa.guidance.TrafficNotification a, String s) {
        java.util.List a0 = a.getInfoList();
        if (a0 != null) {
            Object a1 = a0.iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                com.here.android.mpa.guidance.TrafficNotificationInfo a2 = (com.here.android.mpa.guidance.TrafficNotificationInfo)((java.util.Iterator)a1).next();
                sLogger.v(new StringBuilder().append(s).append(" type:").append(a2.getType().name()).toString());
                sLogger.v(new StringBuilder().append(s).append(" distance:").append(a2.getDistanceInMeters()).toString());
//                com.navdy.hud.app.maps.here.HereMapUtil.printTrafficEvent(a2.getEvent(), s, (com.here.android.mpa.routing.Route)null);
                sLogger.v(a2.toString());
            }
        }
    }
    
    public static void removeRouteInfo(com.navdy.service.library.log.Logger a, boolean b) {
        synchronized(com.navdy.hud.app.maps.here.HereMapUtil.class) {
            com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfoFiles(a, b);
        }
        /*monexit(com.navdy.hud.app.maps.here.HereMapUtil.class)*/;
    }
    
    private static void removeRouteInfoFiles(com.navdy.service.library.log.Logger a, boolean b) {
        label0: try {
            android.content.Context a0 = com.navdy.hud.app.HudApplication.getAppContext();
            String s = com.navdy.hud.app.storage.PathManager.getInstance().getActiveRouteInfoDir();
            boolean b0 = com.navdy.service.library.util.IOUtils.deleteFile(a0, new StringBuilder().append(s).append(java.io.File.separator).append("gasrouterequest.bin").toString());
            a.v(new StringBuilder().append("removeRouteInfo delete gasRoute[").append(b0).append("]").toString());
            label1: {
                if (!b0) {
                    break label1;
                }
                if (!b) {
                    break label0;
                }
            }
            boolean b1 = com.navdy.service.library.util.IOUtils.deleteFile(a0, new StringBuilder().append(s).append(java.io.File.separator).append("routerequest.bin").toString());
            boolean b2 = com.navdy.service.library.util.IOUtils.deleteFile(a0, new StringBuilder().append(s).append(java.io.File.separator).append("routerequest.device").toString());
            a.v(new StringBuilder().append("removeRouteInfo delete route[").append(b1).append("] delete id[").append(b2).append("]").toString());
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
    
    static float roundToIntegerStep(int i, float f) {
        return (float)(i * Math.round(f / (float)i));
    }
    
    public static double roundToN(double d, int i) {
        return (double)(long)((double)i * d) / (double)i;
    }
    
    public static void saveGasRouteInfo(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.navdy.service.library.device.NavdyDeviceId a0, com.navdy.service.library.log.Logger a1) {
        com.navdy.hud.app.maps.here.HereMapUtil.saveRouteInfoInternal(a, a0, a1, "gasrouterequest.bin");
    }
    
    public static void saveRouteInfo(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.navdy.service.library.device.NavdyDeviceId a0, com.navdy.service.library.log.Logger a1) {
        com.navdy.hud.app.maps.here.HereMapUtil.saveRouteInfoInternal(a, a0, a1, "routerequest.bin");
    }
    
    private static void saveRouteInfoInternal(com.navdy.service.library.events.navigation.NavigationRouteRequest a, com.navdy.service.library.device.NavdyDeviceId a0, com.navdy.service.library.log.Logger a1, String s) {
        label0: synchronized(com.navdy.hud.app.maps.here.HereMapUtil.class) {
        lastKnownDeviceId = null;
            java.io.FileOutputStream a2 = null;
            Throwable a3 = null;
            label1: try {
                label3: {
                    Throwable a4 = null;
                    if (a0 != null) {
                        break label3;
                    }
                    sLogger.v("saveRouteInfo: null deviceid");
                    label4: {
                        com.navdy.service.library.device.NavdyDeviceId a5 = null;
                        label2: {
                            {
                                com.navdy.service.library.log.Logger a6 = null;
//                                try {
                                    a5 = new com.navdy.service.library.device.NavdyDeviceId(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getConnectionHandler().getLastConnectedDeviceInfo().deviceId);
                                    a6 = sLogger;
//                                } catch(Throwable a7) {
//                                    a4 = a7;
//                                    break label4;
//                                }
//                                try {
                                    a6.v(new StringBuilder().append("saveRouteInfo: last deviceid found:").append(a5).toString());
                                    a0 = a5;
                                    break label3;
//                                } catch(Throwable a8) {
//                                    a4 = a8;
//                                    break label2;
//                                }
                            }
                        }
//                        a0 = a5;
                    }
//                    sLogger.e("saveRouteInfo", a4);
                }
                a1.v(new StringBuilder().append("saveRouteInfo request[").append(a).append("] deviceId[").append(a0).append("]").toString());
                if (a.destination != null) {
                    byte[] a9 = null;
                    java.util.ArrayList a10 = new java.util.ArrayList((java.util.Collection)a.routeAttributes);
                    if (!((java.util.List)a10).contains(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE)) {
                        ((java.util.List)a10).add(com.navdy.service.library.events.navigation.NavigationRouteRequest.RouteAttribute.ROUTE_ATTRIBUTE_SAVED_ROUTE);
                    }
                    com.navdy.service.library.events.navigation.NavigationRouteRequest a11 = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder(a).routeAttributes((java.util.List)a10).build();
                    sLogger.v("saveRouteInfo added saved route attribute");
                    String s0 = com.navdy.hud.app.storage.PathManager.getInstance().getActiveRouteInfoDir();
                    a2 = new java.io.FileOutputStream(new StringBuilder().append(s0).append(java.io.File.separator).append(s).toString());
//                    try {
                        a9 = a11.toByteArray();
                        a2.write(a9);
                        com.navdy.service.library.util.IOUtils.fileSync(a2);
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
//                    } catch(Throwable a12) {
//                        a3 = a12;
//                        break label1;
//                    }
                    a1.v(new StringBuilder().append("saveRouteInfo: route info size[").append(a9.length).append("]").toString());
                    if (a0 == null) {
                        a1.v("saveRouteInfo: device id not written:null");
                        break label0;
                    } else {
                        byte[] a13 = null;
                        a2 = new java.io.FileOutputStream(new StringBuilder().append(s0).append(java.io.File.separator).append("routerequest.device").toString());
//                        try {
                            a13 = a0.toString().getBytes();
                            a2.write(a13);
                            com.navdy.service.library.util.IOUtils.fileSync(a2);
                            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
//                        } catch(Throwable a14) {
//                            a3 = a14;
//                            break label1;
//                        }
                        lastKnownDeviceId = a0.toString();
                        a1.v(new StringBuilder().append("saveRouteInfo: device id size[").append(a13.length).append("]").toString());
                        break label0;
                    }
                } else {
                    a1.w("route does not have destination");
                    com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfoFiles(sLogger, true);
                    break label0;
                }
            //} catch(Throwable a15) {
            //    a2 = null;
            //    a3 = a15;
            //}
            //try {
//                sLogger.e(a3);
            } catch(IOException a16) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfoFiles(sLogger, true);
                sLogger.e("saveRouteInfo", a16);
                /*monexit(com.navdy.hud.app.maps.here.HereMapUtil.class)*/;
            //    throw a16;
            }
        }
        /*monexit(com.navdy.hud.app.maps.here.HereMapUtil.class)*/;
    }
}
