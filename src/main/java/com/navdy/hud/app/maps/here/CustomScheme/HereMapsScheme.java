package com.navdy.hud.app.maps.here.CustomScheme;

import com.here.android.mpa.mapping.customization.CustomizableScheme;
import com.here.android.mpa.mapping.customization.SchemeColorProperty;
import com.here.android.mpa.mapping.customization.ZoomRange;
import com.navdy.hud.app.storage.PathManager;
import com.nokia.maps.CustomizableSchemeImpl;
import com.nokia.maps.MapImpl;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.constructor.ConstructorException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class HereMapsScheme {
    private final static String scheme_path = PathManager.getInstance().getMapsPartitionPath() + "/map_scheme.yml";
    private final static String error_log_path = PathManager.getInstance().getMapsPartitionPath() + "/map_scheme_error.txt";
    private CustomSchemeDetail details;
    final protected static com.navdy.service.library.log.Logger sLogger;

    final public String TrackingSchemeName;
    final public String EnrouteSchemeName;

    static {
        sLogger = new com.navdy.service.library.log.Logger(HereMapsScheme.class);
    }
    private final static String template_content = "" +
            "# Map scheme template. Copy to 'map_scheme.yml' to activate\n" +
            "# Entries should be named as per COLOR entries in 'Field and Description' column \n" +
            "#  in any of the class pages linked to from: https://developer.here.com/documentation/android-premium/api_reference_java/com/here/android/mpa/mapping/customization/CustomizableVariables.html \n" +
            "#  eg. https://developer.here.com/documentation/android-premium/api_reference_java/com/here/android/mpa/mapping/customization/CustomizableVariables.Street.html\n" +
            "\n" +
            "\n" +
            "Tracking:\n" +
            "  SchemeName: navdy.trackingScheme\n" +
            "  BaseScheme: carnav.traffic.night\n" +
            "  Colours:\n" +
            "    Street.CATEGORY0_COLOR: 0xFF623B69\n" +
            "    Street.CATEGORY0_OUTLINECOLOR: 0xFF9b5fa5\n" +
            "    Street.CATEGORY1_COLOR: 0xFF17586b\n" +
            "    Street.CATEGORY1_OUTLINECOLOR: 0xFF2d8da9\n" +
            "    Street.CATEGORY2_COLOR: 0xFF78752C\n" +
            "    Street.CATEGORY2_OUTLINECOLOR: 0xFFbdb848\n" +
            "    Street.CATEGORY3_COLOR: 0xFF4e4e4e\n" +
            "    Street.CATEGORY4_COLOR: 0xFF616161\n" +
            "\n" +
            "\n" +
            "Enroute:\n" +
            "  SchemeName: navdy.enrouteScheme\n" +
            "  BaseScheme: navdy.trackingScheme\n" +
            "  Colours:\n" +
            "    Street.CATEGORY0_COLOR: 0xFF623B69\n" +
            "  ZoomMin: 0\n" +
            "  ZoomMax: 20\n" +
            "\n";

    private void WriteTemplate() {
        try {
            FileWriter writer = new FileWriter(scheme_path, false);
            writer.write(template_content);
            writer.close();
        } catch (IOException e) {
            sLogger.e("Failed to write scheme template file", e);
        }
    }

    private void WriteToFile(String data, String path) {
        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(data);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public HereMapsScheme(com.here.android.mpa.mapping.Map map) {
        File schemeFile = new File(scheme_path);
        if (!schemeFile.exists()) {
            WriteTemplate();
        }
        try {
            if (!schemeFile.exists()) {
                details = null;
            } else {
                FileInputStream fileInputStream = null;
                fileInputStream = new FileInputStream(schemeFile);
                Yaml yaml = new Yaml(new Constructor(CustomSchemeDetail.class));
                this.details = yaml.load(fileInputStream);
            }
        } catch (FileNotFoundException | ConstructorException e) {
            sLogger.e("Failed to load scheme file", e);
            WriteToFile(String.valueOf(e), error_log_path);
            this.details = null;
        }

        if (details == null) {
            WriteTemplate();
            TrackingSchemeName = null;
            EnrouteSchemeName = null;
            return;
        }

        List<Scheme> schemes = new ArrayList<Scheme>();
        if (details.Enroute.BaseScheme.equals(details.Tracking.SchemeName)) {
            schemes.add(details.Tracking);
            schemes.add(details.Enroute);
        } else {
            schemes.add(details.Enroute);
            schemes.add(details.Tracking);
        }

        TrackingSchemeName = details.Tracking.SchemeName;
        EnrouteSchemeName = details.Enroute.SchemeName;

        for (Scheme scheme: schemes) {
            CustomizableScheme newScheme  = getCustomizableScheme(map, scheme.SchemeName, scheme.BaseScheme);
            if (newScheme != null) {
                for (Map.Entry<String, String> entry : scheme.Colours.entrySet()) {
                    String categoryName = entry.getKey();
                    String scolor = entry.getValue().trim();
                    if (scolor.startsWith("0x")) {
                        scolor = scolor.substring(2);
                    }
                    Long colour = Long.parseLong(scolor, 16);
                    SchemeColorProperty prop = getCategoryProperty(categoryName);
                    if (prop != null) {
                        newScheme.setVariableValue(prop, colour.intValue(),
                                new ZoomRange(scheme.ZoomMin, scheme.ZoomMax));
                    }
                }
            }
        }
    }

    private SchemeColorProperty getCategoryProperty(String categoryName) {
        try {
//            Class.forName("com.here.android.mpa.mapping.customization.CustomizableVariables$Street")
//
//            com.here.android.mpa.mapping.customization.CustomizableVariables.class.getClasses()
            String[] parts = categoryName.split("\\.");
            Class<?> category = Class.forName("com.here.android.mpa.mapping.customization.CustomizableVariables$" + parts[0]);
            Field f_SchemeColorProperty = category.getDeclaredField(parts[1]);
            return (SchemeColorProperty)f_SchemeColorProperty.get(null);

        } catch (NoSuchFieldException | IllegalAccessException | ClassNotFoundException | IndexOutOfBoundsException e) {
            sLogger.e("Failed to find category entry: " + categoryName, e);
        }
        return null;
    }

    private CustomizableScheme getCustomizableScheme(com.here.android.mpa.mapping.Map map, String customName, String templateScheme) {
        Field f_mapImpl = null;
        try {
            f_mapImpl = com.here.android.mpa.mapping.Map.class.getDeclaredField("a");
            f_mapImpl.setAccessible(true);
            MapImpl mapImpl = (MapImpl)f_mapImpl.get(map);

            Method m_createCustomizableSchemeNative = MapImpl.class.getDeclaredMethod("createCustomizableSchemeNative", String.class, String.class);
            m_createCustomizableSchemeNative.setAccessible(true);
            CustomizableSchemeImpl var = (CustomizableSchemeImpl)m_createCustomizableSchemeNative.invoke(mapImpl, customName, templateScheme);

            Method m_implToCSI = CustomizableSchemeImpl.class.getDeclaredMethod("a", CustomizableSchemeImpl.class);
            m_implToCSI.setAccessible(true);
            return (CustomizableScheme)m_implToCSI.invoke(null, var);
        } catch (NoSuchFieldException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            sLogger.e("Failed to get Customizable Scheme " + customName + " from " + templateScheme, e);
        }
        return null;
    }



}
