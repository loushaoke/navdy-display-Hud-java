package com.navdy.hud.app.maps.here;

final class HereMapUtil$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapUtil$IImageLoadCallback val$callback;
    final int val$height;
    final com.here.android.mpa.common.Image val$image;
    final int val$width;
    
    HereMapUtil$1(com.here.android.mpa.common.Image a, int i, int i0, com.navdy.hud.app.maps.here.HereMapUtil$IImageLoadCallback a0) {

        super();
        this.val$image = a;
        this.val$width = i;
        this.val$height = i0;
        this.val$callback = a0;
    }
    
    public void run() {
        try {
            android.graphics.Bitmap a = this.val$image.getBitmap(this.val$width, this.val$height);
            this.val$callback.result(this.val$image, a);
        } catch(Throwable a0) {
            com.navdy.hud.app.maps.here.HereMapUtil.access$000().e(a0);
            this.val$callback.result(this.val$image, (android.graphics.Bitmap)null);
        }
    }
}
