package com.navdy.hud.app.maps;
import com.navdy.hud.app.R;
import javax.inject.Inject;
import com.squareup.otto.Subscribe;

final public class MapsEventHandler {
    final private static boolean VERBOSE = false;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.maps.MapsEventHandler sSingleton;
    @Inject
    com.squareup.otto.Bus bus;
    @Inject
    com.navdy.hud.app.service.ConnectionHandler connectionHandler;
    @Inject
    com.navdy.hud.app.profile.DriverProfileManager mDriverProfileManager;
    @Inject
    protected android.content.SharedPreferences sharedPreferences;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.MapsEventHandler.class);
        sSingleton = new com.navdy.hud.app.maps.MapsEventHandler();
    }
    
    private MapsEventHandler() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.bus.register(this);
    }
    
    final public static com.navdy.hud.app.maps.MapsEventHandler getInstance() {
        return sSingleton;
    }
    
    private void handleConnectionStateChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            boolean b = false;
            if (a.state != com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_VERIFIED) {
                b = false;
            } else {
                this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_READY, (String)null, (String)null, (com.navdy.service.library.events.navigation.NavigationRouteResult)null, (String)null)));
                b = true;
            }
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().handleConnectionState(b);
        } else {
            sLogger.v("handleConnectionStateChange:engine not initiazed");
        }
    }
    
    private void handleNavigationRequest(com.navdy.service.library.events.navigation.NavigationSessionRequest a) {
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().handleNavigationSessionRequest(a);
        } else {
            this.sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.NavigationSessionResponse(com.navdy.service.library.events.RequestStatus.REQUEST_NOT_READY, com.navdy.hud.app.HudApplication.getAppContext().getString(R.string.map_engine_not_ready), a.newState, a.routeId));
        }
    }
    
    public com.squareup.otto.Bus getBus() {
        return this.bus;
    }
    
    public com.navdy.service.library.events.preferences.NavigationPreferences getNavigationPreferences() {
        return this.mDriverProfileManager.getCurrentProfile().getNavigationPreferences();
    }
    
    public android.content.SharedPreferences getSharedPreferences() {
        return this.sharedPreferences;
    }
    
    @Subscribe
    public void onAutoCompleteRequest(com.navdy.service.library.events.places.AutoCompleteRequest a) {
        com.navdy.hud.app.maps.here.HerePlacesManager.handleAutoCompleteRequest(a);
    }
    
    @Subscribe
    public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        this.handleConnectionStateChange(a);
    }
    
    @Subscribe
    public void onGetNavigationSessionState(com.navdy.service.library.events.navigation.GetNavigationSessionState a) {
        if (com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().postNavigationSessionStatusEvent(true);
        } else {
            sLogger.i("onGetNavigationSessionState:engine not ready");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY, (String)null, (String)null, (com.navdy.service.library.events.navigation.NavigationRouteResult)null, (String)null)));
        }
    }
    
    @Subscribe
    public void onGetRouteManeuverRequest(com.navdy.service.library.events.navigation.RouteManeuverRequest a) {
        try {
            com.navdy.hud.app.maps.here.HereRouteManager.handleRouteManeuverRequest(a);
        } catch(Throwable a0) {
            this.sendEventToClient((com.squareup.wire.Message)new com.navdy.service.library.events.navigation.RouteManeuverResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR, a0.toString(), (java.util.List)null));
            sLogger.e(a0);
        }
    }
    
    @Subscribe
    public void onNavigationRequest(com.navdy.service.library.events.navigation.NavigationSessionRequest a) {
        this.handleNavigationRequest(a);
    }
    
    @Subscribe
    public void onPlaceSearchRequest(com.navdy.service.library.events.places.PlacesSearchRequest a) {
        com.navdy.hud.app.maps.here.HerePlacesManager.handlePlacesSearchRequest(a);
    }
    
    @Subscribe
    public void onRouteCancelRequest(com.navdy.service.library.events.navigation.NavigationRouteCancelRequest a) {
        com.navdy.hud.app.maps.here.HereRouteManager.handleRouteCancelRequest(a, false);
    }
    
    @Subscribe
    public void onRouteSearchRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        com.navdy.hud.app.maps.here.HereRouteManager.handleRouteRequest(a);
    }
    
    public void sendEventToClient(com.squareup.wire.Message a) {
        try {
            com.navdy.service.library.device.RemoteDevice a0 = this.connectionHandler.getRemoteDevice();
            if (a0 != null) {
                a0.postEvent(a);
            }
        } catch(Throwable a1) {
            sLogger.e(a1);
        }
    }
}
