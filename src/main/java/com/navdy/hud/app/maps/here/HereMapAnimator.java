package com.navdy.hud.app.maps.here;

public class HereMapAnimator {
    final private static int DRASTIC_HEADING_CHANGE = 45;
    final private static int INVALID_STEP = -1;
    final private static int INVALID_TILT = -1;
    final private static int INVALID_ZOOM = -1;
    final private static int MAX_DIFFERENCE_HEADING_FILTER = 120;
    final private static int MAX_HEADING = 360;
    final private static long MAX_INTERVAL_HEADING_UPDATE = 3000L;
    final private static int MIN_HEADING = 0;
    final private static double MIN_SPEED_FILTER = 0.44704;
    final private static double MPH_TO_MS = 0.44704;
    final private static long REFRESH_TIME_THROTTLE;
    final private static Object geoPositionLock;
    final private static com.navdy.service.library.log.Logger logger;
    final private static Object tiltLock;
    final private static Object zoomLock;
    private com.here.android.mpa.common.GeoPosition currentGeoPosition;
    private float currentTilt;
    private double currentZoom;
    private long geoPositionUpdateInterval;
    private long lastGeoPositionUpdateTime;
    private volatile float lastHeading;
    private long lastPreDrawTime;
    private long lastTiltUpdateTime;
    private long lastZoomUpdateTime;
    private com.navdy.hud.app.maps.here.HereMapController mapController;
    private com.here.android.mpa.mapping.OnMapRenderListener mapRenderListener;
    private com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode mode;
    private com.navdy.hud.app.maps.here.HereNavController navController;
    private long preDrawFinishTime;
    private volatile boolean renderingEnabled;
    private long tiltUpdateInterval;
    private long zoomUpdateInterval;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapAnimator.class);
        REFRESH_TIME_THROTTLE = (long)(1000 / com.navdy.hud.app.maps.MapSettings.getMapFps());
        geoPositionLock = new Object();
        zoomLock = new Object();
        tiltLock = new Object();
    }
    
    HereMapAnimator(com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode a, com.navdy.hud.app.maps.here.HereMapController a0, com.navdy.hud.app.maps.here.HereNavController a1) {
        this.mode = com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.NONE;
        this.lastHeading = -1f;
        this.renderingEnabled = true;
        this.mode = a;
        if (a != com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.NONE) {
            logger.v("animation throttle = " + REFRESH_TIME_THROTTLE);
            this.mapRenderListener = new HereMapAnimator$1(this);
        }
        this.mapController = a0;
        this.navController = a1;
        this.currentGeoPosition = null;
        this.currentZoom = a0.getZoomLevel();
        this.currentTilt = a0.getTilt();
    }
    
    static void access$000(com.navdy.hud.app.maps.here.HereMapAnimator a) {
        a.onPreDraw();
    }
    
    static void access$100(com.navdy.hud.app.maps.here.HereMapAnimator a, boolean b, long j) {
        a.onPostDraw(b, j);
    }
    
    static long access$1000(com.navdy.hud.app.maps.here.HereMapAnimator a) {
        return a.lastZoomUpdateTime;
    }
    
    static long access$1002(com.navdy.hud.app.maps.here.HereMapAnimator a, long j) {
        a.lastZoomUpdateTime = j;
        return j;
    }
    
    static boolean access$1100(com.navdy.hud.app.maps.here.HereMapAnimator a, double d) {
        return a.isValidZoom(d);
    }
    
    static Object access$1200() {
        return zoomLock;
    }
    
    static long access$1302(com.navdy.hud.app.maps.here.HereMapAnimator a, long j) {
        a.zoomUpdateInterval = j;
        return j;
    }
    
    static double access$1402(com.navdy.hud.app.maps.here.HereMapAnimator a, double d) {
        a.currentZoom = d;
        return d;
    }
    
    static long access$1500(com.navdy.hud.app.maps.here.HereMapAnimator a) {
        return a.lastTiltUpdateTime;
    }
    
    static long access$1502(com.navdy.hud.app.maps.here.HereMapAnimator a, long j) {
        a.lastTiltUpdateTime = j;
        return j;
    }
    
    static boolean access$1600(com.navdy.hud.app.maps.here.HereMapAnimator a, float f) {
        return a.isValidTilt(f);
    }
    
    static Object access$1700() {
        return tiltLock;
    }
    
    static long access$1802(com.navdy.hud.app.maps.here.HereMapAnimator a, long j) {
        a.tiltUpdateInterval = j;
        return j;
    }
    
    static float access$1902(com.navdy.hud.app.maps.here.HereMapAnimator a, float f) {
        a.currentTilt = f;
        return f;
    }
    
    static long access$200(com.navdy.hud.app.maps.here.HereMapAnimator a) {
        return a.lastGeoPositionUpdateTime;
    }
    
    static com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode access$2000(com.navdy.hud.app.maps.here.HereMapAnimator a) {
        return a.mode;
    }
    
    static long access$202(com.navdy.hud.app.maps.here.HereMapAnimator a, long j) {
        a.lastGeoPositionUpdateTime = j;
        return j;
    }
    
    static boolean access$300(com.navdy.hud.app.maps.here.HereMapAnimator a, com.here.android.mpa.common.GeoPosition a0) {
        return a.isValidGeoPosition(a0);
    }
    
    static com.navdy.service.library.log.Logger access$400() {
        return logger;
    }
    
    static Object access$500() {
        return geoPositionLock;
    }
    
    static long access$602(com.navdy.hud.app.maps.here.HereMapAnimator a, long j) {
        a.geoPositionUpdateInterval = j;
        return j;
    }
    
    static com.here.android.mpa.common.GeoPosition access$700(com.navdy.hud.app.maps.here.HereMapAnimator a) {
        return a.currentGeoPosition;
    }
    
    static com.here.android.mpa.common.GeoPosition access$702(com.navdy.hud.app.maps.here.HereMapAnimator a, com.here.android.mpa.common.GeoPosition a0) {
        a.currentGeoPosition = a0;
        return a0;
    }
    
    static boolean access$800(com.navdy.hud.app.maps.here.HereMapAnimator a) {
        return a.renderingEnabled;
    }
    
    static com.navdy.hud.app.maps.here.HereMapController access$900(com.navdy.hud.app.maps.here.HereMapAnimator a) {
        return a.mapController;
    }
    
    private com.here.android.mpa.common.GeoCoordinate getNewCenter(com.here.android.mpa.common.GeoPosition a, double d) {
        com.here.android.mpa.common.GeoCoordinate a0 = this.mapController.getCenter();
        if (a != null && d != -1.0) {
            com.here.android.mpa.common.GeoCoordinate a1 = a.getCoordinate();
            double d0 = a1.getLatitude();
            double d1 = a0.getLatitude();
            double d2 = a1.getLongitude();
            double d3 = a0.getLongitude();
            a0 = new com.here.android.mpa.common.GeoCoordinate(a0.getLatitude() + (d0 - d1) * d, a0.getLongitude() + (d2 - d3) * d);
        }
        return a0;
    }
    
    private float getNewHeading(com.here.android.mpa.common.GeoPosition a, double d) {
        float f = this.mapController.getOrientation();
        if (a != null && d != -1.0) {
            double d0 = this.getOrientationDiff((double)(float)a.getHeading(), (double)f);
            double d1 = (double)f + d * d0;
            if (d1 > 360.0) {
                d1 = d1 - 360.0;
            } else if (d1 < 0.0) {
                d1 = d1 + 360.0;
            }
            f = (float)d1;
        }
        return f;
    }
    
    private float getNewTilt(float f, double d) {
        float f0;
        if (this.mode != com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.ZOOM_TILT_ANIMATION) {
            f0 = -1f;
        } else if (f == -1f) {
            f0 = -1f;
        } else if (d != -1.0) {
            float f1 = this.mapController.getTilt();
            f0 = (float)((double)f1 + (double)(f - f1) * d);
            logger.v("animatorTilt: " + f0);
        } else {
            f0 = -1f;
        }
        return f0;
    }
    
    private double getNewZoomLevel(double d, double d0) {
        double d1;
        if (this.mode != com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode.ZOOM_TILT_ANIMATION) {
            d1 = -1.0;
        } else if (d == -1.0) {
            d1 = -1.0;
        } else if (d0 != -1.0) {
            double d2 = this.mapController.getZoomLevel();
            d1 = d2 + (d - d2) * d0;
            logger.v("animatorZoom: " + d1);
        } else {
            d1 = -1.0;
        }
        return d1;
    }
    
    private double getOrientationDiff(double d, double d0) {
        double d1 = d - d0;
        if (d1 > 180.0) {
            d1 = d1 - 360.0;
        } else if (d1 < -180.0) {
            d1 = d1 + 360.0;
        }
        return d1;
    }
    
    private boolean isValidGeoPosition(com.here.android.mpa.common.GeoPosition a) {
        com.here.android.mpa.common.GeoPosition a0;
        long j;
        boolean b;
        synchronized(geoPositionLock) {
            a0 = this.currentGeoPosition;
            j = this.geoPositionUpdateInterval;
            /*monexit(a1)*/
        }
        if (a0 != null) {
            double d = this.getOrientationDiff(a.getHeading(), a0.getHeading());
            b = a.getSpeed() >= 0.44704 || Math.abs(d) <= 120.0 || j >= 3000L;
        } else {
            b = true;
        }
        return b;
    }
    
    private boolean isValidTilt(float f) {
        boolean b;
        int i = Float.compare(f, 0.0f);
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (f < 90f) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private boolean isValidZoom(double d) {
        boolean b;
        int i = Double.compare(d, 0.0);
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (d <= 20.0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void onPostDraw(boolean b, long j) {
        long j0 = android.os.SystemClock.elapsedRealtime() - this.preDrawFinishTime;
        long j1 = REFRESH_TIME_THROTTLE - j0;
        if (logger.isLoggable(2)) {
            logger.v("last render took " + j0 + " ms");
        }
        int i = Long.compare(j1, 0L);
        label0: {
            InterruptedException a;
            if (i <= 0) {
                if (!logger.isLoggable(2)) {
                    break label0;
                }
                logger.v("render thread could not sleep");
                break label0;
            } else {
                if (logger.isLoggable(2)) {
                    logger.v("sleeping render thread for " + j1 + " ms");
                }
                try {
                    Thread.sleep(j1);
                    break label0;
                } catch(InterruptedException a0) {
                    a = a0;
                }
            }
            logger.e(a);
        }
    }
    
    private void onPreDraw() {
        Object a;

        com.here.android.mpa.common.GeoPosition a1 = this.currentGeoPosition;
        {
            Object a2;
            {
                {
                    label1:
                    if (a1 != null) {
                        long j = android.os.SystemClock.elapsedRealtime();
                        if (this.lastPreDrawTime <= 0L) {
                            this.lastPreDrawTime = j;
                        } else {
                            long j0;
                            com.here.android.mpa.common.GeoPosition a6;
                            long j1;
                            double d;
                            long j2;
                            float f;
                            boolean b;
                            boolean b0;
                            boolean b1;
                            double d0;
                            double d1;
                            double d2;
                            long j3 = j - this.lastPreDrawTime;
                            this.lastPreDrawTime = j;
                            synchronized (geoPositionLock) {
                                j0 = this.geoPositionUpdateInterval;
                                a6 = this.currentGeoPosition;
                                /*monexit(a4)*/
                                a2 = zoomLock;
                            }
                            synchronized (a2) {
                                j1 = this.zoomUpdateInterval;
                                d = this.currentZoom;
                                /*monexit(a2)*/
                                a = tiltLock;
                            }
                            synchronized (a) {
                                j2 = this.tiltUpdateInterval;
                                f = this.currentTilt;
                                /*monexit(a)*/
                            }
                            label10:
                            {
                                label8:
                                {
                                    label9:
                                    {
                                        if (a6 == null) {
                                            break label9;
                                        }
                                        if (j0 > 0L) {
                                            break label8;
                                        }
                                    }
                                    b = false;
                                    break label10;
                                }
                                b = true;
                            }
                            int i = Double.compare(d, 0.0);
                            label7:
                            {
                                label5:
                                {
                                    label6:
                                    {
                                        if (i < 0) {
                                            break label6;
                                        }
                                        if (j1 > 0L) {
                                            break label5;
                                        }
                                    }
                                    b0 = false;
                                    break label7;
                                }
                                b0 = true;
                            }
                            int i0 = Float.compare(f, 0.0f);
                            label4:
                            {
                                label2:
                                {
                                    label3:
                                    {
                                        if (i0 < 0) {
                                            break label3;
                                        }
                                        if (j2 > 0L) {
                                            break label2;
                                        }
                                    }
                                    b1 = false;
                                    break label4;
                                }
                                b1 = true;
                            }
                            if (b) {
                                d0 = (double) j3 / (double) j0;
                            } else {
                                a6 = null;
                                d0 = -1.0;
                            }
                            if (b0) {
                                d1 = (double) j3 / (double) j1;
                            } else {
                                d = -1.0;
                                d1 = -1.0;
                            }
                            if (b1) {
                                d2 = (double) j3 / (double) j2;
                            } else {
                                f = -1f;
                                d2 = -1.0;
                            }
                            label0:
                            {
                                if (b) {
                                    break label0;
                                }
                                if (b0) {
                                    break label0;
                                }
                                if (b1) {
                                    break label0;
                                }
                                this.preDrawFinishTime = android.os.SystemClock.elapsedRealtime();
                                break label1;
                            }
                            double d3 = Math.max(d0, Math.max(d1, d2));
                            if (d3 > 1.0) {
                                d3 = 1.0;
                            }
                            com.here.android.mpa.common.GeoCoordinate a11 = this.getNewCenter(a6, d3);
                            float f0 = this.getNewHeading(a6, d3);
                            double d4 = this.getNewZoomLevel(d, d3);
                            float f1 = this.getNewTilt(f, d3);
                            if (this.renderingEnabled && this.mapController.getState() == HereMapController$State.AR_MODE) {
                                this.mapController.setCenter(a11, com.here.android.mpa.mapping.Map.Animation.NONE, d4, f0, f1);
                            }
                            this.preDrawFinishTime = android.os.SystemClock.elapsedRealtime();
                            if (logger.isLoggable(2)) {
                                logger.v("predraw logic took " + (this.preDrawFinishTime - j) + " ms");
                            }
                        }
                    }
                }
            }

        }
    }
    
    public void clearState() {
        logger.v("clearState");
        this.lastHeading = -1f;
    }
    
    public com.navdy.hud.app.maps.here.HereMapAnimator$AnimationMode getAnimationMode() {
        return this.mode;
    }
    
    public com.here.android.mpa.mapping.OnMapRenderListener getMapRenderListener() {
        return this.mapRenderListener;
    }
    
    public void setGeoPosition(com.here.android.mpa.common.GeoPosition a) {
        label1: if (com.navdy.hud.app.maps.here.HereMapAnimator$6.$SwitchMap$com$navdy$hud$app$maps$here$HereMapAnimator$AnimationMode[this.mode.ordinal()] != 0) {
            boolean b;
            double d = a.getHeading();
            float f = this.lastHeading;
            int i = Float.compare(f, -1f);
            label2: {
                if (i == 0) {
                    b = false;
                    break label2;
                }
                double d0 = this.getOrientationDiff(d, (double)this.lastHeading);
                double d1 = a.getSpeed();
                int i0 = Double.compare(d1, 0.44704);
                label0: {
                    if (i0 > 0) {
                        break label0;
                    }
                    if (!(Math.abs(d0) >= 120.0)) {
                        break label0;
                    }
                    logger.v("filtering out spurious heading last: " + this.lastHeading + " new:" + d + " speed:" + d1);
                    break label1;
                }
                b = Math.abs(d0) >= 45.0;
            }
            this.currentGeoPosition = a;
            this.lastHeading = (float)d;
            if (this.renderingEnabled && this.mapController.getState() == com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE) {
                this.mapController.setCenter(a.getCoordinate(), b ? com.here.android.mpa.mapping.Map.Animation.BOW : com.here.android.mpa.mapping.Map.Animation.NONE, -1.0, this.lastHeading, -1f);
            }
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapAnimator$2(this, a), 17);
        }
    }
    
    void setTilt(float f) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapAnimator$4(this, f), 17);
    }
    
    void setZoom(double d) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapAnimator$3(this, d), 17);
    }
    
    public void startMapRendering() {
        logger.v("startMapRendering: rendering enabled");
        this.renderingEnabled = true;
        com.navdy.service.library.task.TaskManager.getInstance().execute(new HereMapAnimator$5(this), 17);
    }
    
    public void stopMapRendering() {
        logger.v("stopMapRendering: rendering disabled");
        this.renderingEnabled = false;
    }
}
