package com.navdy.hud.app.maps.util;

final public class RouteUtils$Bounds {
    public double height;
    public double width;
    
    RouteUtils$Bounds(double d, double d0) {
        this.width = d;
        this.height = d0;
    }
}
