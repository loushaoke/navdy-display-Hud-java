package com.navdy.hud.app.maps;

public class MapEvents$TrafficRerouteEvent {
    public String additionalVia;
    public long currentEta;
    public long distanceDifference;
    public long etaDifference;
    public long newEta;
    public String via;
    
    public MapEvents$TrafficRerouteEvent(String s, String s0, long j, long j0, long j1, long j2) {
        this.via = s;
        this.additionalVia = s0;
        this.etaDifference = j;
        this.currentEta = j0;
        this.distanceDifference = j1;
        this.newEta = j2;
    }
}
