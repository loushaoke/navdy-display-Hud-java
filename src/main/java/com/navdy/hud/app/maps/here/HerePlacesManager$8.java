package com.navdy.hud.app.maps.here;

final class HerePlacesManager$8 implements com.here.android.mpa.search.ResultListener {
    final java.util.concurrent.atomic.AtomicInteger val$counter;
    final com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener val$listener;
    final java.util.List val$places;
    
    HerePlacesManager$8(java.util.concurrent.atomic.AtomicInteger a, java.util.List a0, com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener a1) {

        super();
        this.val$counter = a;
        this.val$places = a0;
        this.val$listener = a1;
    }
    
    public void onCompleted(com.here.android.mpa.search.Place a, com.here.android.mpa.search.ErrorCode a0) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HerePlacesManager$8$1(this, a0, a), 2);
    }
    
    public void onCompleted(Object a, com.here.android.mpa.search.ErrorCode a0) {
        this.onCompleted((com.here.android.mpa.search.Place)a, a0);
    }
}
