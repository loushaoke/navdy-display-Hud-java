package com.navdy.hud.app.maps.here;

class HerePlacesManager$7$1$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HerePlacesManager$7$1 this$1;
    final com.here.android.mpa.search.ErrorCode val$error;
    final com.here.android.mpa.search.DiscoveryResultPage val$results;
    
    HerePlacesManager$7$1$1(com.navdy.hud.app.maps.here.HerePlacesManager$7$1 a, com.here.android.mpa.search.ErrorCode a0, com.here.android.mpa.search.DiscoveryResultPage a1) {

        super();
        this.this$1 = a;
        this.val$error = a0;
        this.val$results = a1;
    }
    
    public void run() {
        label6: {
            Throwable a = null;
            label0: {
                label5: {
                    label2: {
                        label4: try {
                            com.here.android.mpa.search.ErrorCode a0 = this.val$error;
                            com.here.android.mpa.search.ErrorCode a1 = com.here.android.mpa.search.ErrorCode.NONE;
                            label3: {
                                if (a0 == a1) {
                                    break label3;
                                }
                                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e(new StringBuilder().append("Error in nearby categories response: ").append(this.val$error.name()).toString());
                                com.navdy.hud.app.maps.here.HerePlacesManager.access$1000();
                                this.this$1.this$0.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.RESPONSE_ERROR);
                                break label4;
                            }
                            java.util.List a2 = this.val$results.getItems();
                            java.util.ArrayList a3 = new java.util.ArrayList();
                            Object a4 = a2.iterator();
                            while(((java.util.Iterator)a4).hasNext()) {
                                com.here.android.mpa.search.DiscoveryResult a5 = (com.here.android.mpa.search.DiscoveryResult)((java.util.Iterator)a4).next();
                                if (com.navdy.hud.app.maps.here.HerePlacesManager.access$000().isLoggable(2)) {
                                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("Found a nearby category item: ").append(a5.getTitle()).toString());
                                }
                                if (a5.getResultType() == com.here.android.mpa.search.DiscoveryResult.ResultType.PLACE) {
                                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("Discovered a place: ").append(a5.getTitle()).toString());
                                    ((java.util.List)a3).add((com.here.android.mpa.search.PlaceLink)a5);
                                }
                            }
                            int i = ((java.util.List)a3).size();
                            label1: {
                                if (i == 0) {
                                    break label1;
                                }
                                com.navdy.hud.app.maps.here.HerePlacesManager.access$1100((java.util.List)a3, this.this$1.this$0.val$listener);
                                break label2;
                            }
                            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e("no places links found");
                            com.navdy.hud.app.maps.here.HerePlacesManager.access$1000();
                            this.this$1.this$0.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.RESPONSE_ERROR);
                            break label5;
                        } catch(Throwable a6) {
                            a = a6;
                            break label0;
                        }
                        long j = android.os.SystemClock.elapsedRealtime();
                        com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest- time-2 =").append(j - this.this$1.val$l1).toString());
                        break label6;
                    }
                    long j0 = android.os.SystemClock.elapsedRealtime();
                    com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest- time-2 =").append(j0 - this.this$1.val$l1).toString());
                    break label6;
                }
                long j1 = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest- time-2 =").append(j1 - this.this$1.val$l1).toString());
                break label6;
            }
            try {
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().e("HERE internal DiscoveryRequest.execute callback exception: ", a);
                com.navdy.hud.app.maps.here.HerePlacesManager.access$1000();
                this.this$1.this$0.val$listener.onError(com.navdy.hud.app.maps.here.HerePlacesManager$Error.UNKNOWN_ERROR);
            } catch(Throwable a7) {
                long j2 = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest- time-2 =").append(j2 - this.this$1.val$l1).toString());
                throw a7;
            }
            long j3 = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HerePlacesManager.access$000().v(new StringBuilder().append("handleCategoriesRequest- time-2 =").append(j3 - this.this$1.val$l1).toString());
        }
    }
}
