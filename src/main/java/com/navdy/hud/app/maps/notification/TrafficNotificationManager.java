package com.navdy.hud.app.maps.notification;
import com.navdy.hud.app.R;
import com.squareup.otto.Subscribe;

public class TrafficNotificationManager {
    final private static com.navdy.hud.app.maps.notification.TrafficNotificationManager sInstance;
    final public static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private android.os.Handler handler;
    final private int junctionViewH;
    final private int junctionViewInflateH;
    final private int junctionViewInflateW;
    final private int junctionViewW;
    private com.navdy.hud.app.maps.MapEvents$DisplayJunction lastJunctionEvent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.notification.TrafficNotificationManager.class);
        sInstance = new com.navdy.hud.app.maps.notification.TrafficNotificationManager();
    }
    
    private TrafficNotificationManager() {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.junctionViewW = a.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_w);
        this.junctionViewH = a.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_h);
        this.junctionViewInflateW = a.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_inflate_w);
        this.junctionViewInflateH = a.getDimensionPixelSize(R.dimen.traffic_junc_notif_img_inflate_h);
        this.bus.register(this);
    }
    
    static int access$000(com.navdy.hud.app.maps.notification.TrafficNotificationManager a) {
        return a.junctionViewInflateW;
    }
    
    static int access$100(com.navdy.hud.app.maps.notification.TrafficNotificationManager a) {
        return a.junctionViewInflateH;
    }
    
    static com.navdy.hud.app.maps.MapEvents$DisplayJunction access$200(com.navdy.hud.app.maps.notification.TrafficNotificationManager a) {
        return a.lastJunctionEvent;
    }
    
    static int access$300(com.navdy.hud.app.maps.notification.TrafficNotificationManager a) {
        return a.junctionViewW;
    }
    
    static int access$400(com.navdy.hud.app.maps.notification.TrafficNotificationManager a) {
        return a.junctionViewH;
    }
    
    static android.os.Handler access$500(com.navdy.hud.app.maps.notification.TrafficNotificationManager a) {
        return a.handler;
    }
    
    public static com.navdy.hud.app.maps.notification.TrafficNotificationManager getInstance() {
        return sInstance;
    }
    
    private void removeJunctionView() {
        this.handler.post((Runnable)new com.navdy.hud.app.maps.notification.TrafficNotificationManager$2(this));
    }
    
    @Subscribe
    public void onDisplayJunction(com.navdy.hud.app.maps.MapEvents$DisplayJunction a) {
        try {
            sLogger.v("onDisplayJunction");
            if (a.junction != null) {
                this.removeJunctionView();
                this.lastJunctionEvent = a;
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.notification.TrafficNotificationManager$1(this, a), 10);
            } else {
                sLogger.v("no junction image");
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    @Subscribe
    public void onHideJunctionSignPost(com.navdy.hud.app.maps.MapEvents$HideSignPostJunction a) {
        sLogger.v("onHideJunctionSignPost");
        this.lastJunctionEvent = null;
        this.removeJunctionView();
    }
    
    @Subscribe
    public void onLiveTrafficDismiss(com.navdy.hud.app.maps.MapEvents$LiveTrafficDismissEvent a) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#traffic#event#notif");
    }
    
    @Subscribe
    public void onLiveTrafficEvent(com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent a) {
        if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
            boolean b = false;
            com.navdy.hud.app.framework.notifications.NotificationManager a0 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            boolean b0 = a0.isNotificationPresent("navdy#traffic#reroute#notif");
            label2: {
                label0: {
                    label1: {
                        if (b0) {
                            break label1;
                        }
                        if (a0.isNotificationPresent("navdy#traffic#delay#notif")) {
                            break label1;
                        }
                        if (!a0.isNotificationPresent("navdy#traffic#jam#notif")) {
                            break label0;
                        }
                    }
                    b = true;
                    break label2;
                }
                b = false;
            }
            if (b) {
                sLogger.v(new StringBuilder().append("higher priority notification active ignore:").append(a.type.name()).toString());
            } else {
                com.navdy.hud.app.maps.notification.TrafficEventNotification a1 = (com.navdy.hud.app.maps.notification.TrafficEventNotification)a0.getNotification("navdy#traffic#event#notif");
                if (a1 == null) {
                    a1 = new com.navdy.hud.app.maps.notification.TrafficEventNotification(this.bus);
                }
                a1.setTrafficEvent(a);
                a0.addNotification((com.navdy.hud.app.framework.notifications.INotification)a1);
            }
        } else {
            sLogger.v(new StringBuilder().append("traffic notification disabled:").append(a).toString());
        }
    }
    
    @Subscribe
    public void onTrafficDelayDismiss(com.navdy.hud.app.maps.MapEvents$TrafficDelayDismissEvent a) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#traffic#delay#notif");
    }
    
    @Subscribe
    public void onTrafficDelayEvent(com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent a) {
    }
    
    @Subscribe
    public void onTrafficJamProgress(com.navdy.hud.app.maps.MapEvents$TrafficJamProgressEvent a) {
        if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
            com.navdy.hud.app.framework.notifications.NotificationManager a0 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            if (a0.isNotificationPresent("navdy#traffic#reroute#notif")) {
                sLogger.v(new StringBuilder().append("reroute notification active ignore jam event:").append(a.remainingTime).toString());
            } else {
                a0.removeNotification("navdy#traffic#event#notif");
                a0.removeNotification("navdy#traffic#delay#notif");
                com.navdy.hud.app.maps.notification.TrafficJamNotification a1 = (com.navdy.hud.app.maps.notification.TrafficJamNotification)a0.getNotification("navdy#traffic#jam#notif");
                if (a1 == null) {
                    a1 = new com.navdy.hud.app.maps.notification.TrafficJamNotification(this.bus, a);
                }
                a1.setTrafficEvent(a);
                a0.addNotification((com.navdy.hud.app.framework.notifications.INotification)a1);
            }
        } else {
            sLogger.v(new StringBuilder().append("traffic notification disabled:").append(a).toString());
        }
    }
    
    @Subscribe
    public void onTrafficJamProgressDismiss(com.navdy.hud.app.maps.MapEvents$TrafficJamDismissEvent a) {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#traffic#jam#notif");
    }
    
    @Subscribe
    public void onTrafficReroutePrompt(com.navdy.hud.app.maps.MapEvents$TrafficRerouteEvent a) {
        if (com.navdy.hud.app.framework.glance.GlanceHelper.isTrafficNotificationEnabled()) {
            com.navdy.hud.app.framework.notifications.NotificationManager a0 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
            a0.removeNotification("navdy#traffic#event#notif");
            a0.removeNotification("navdy#traffic#delay#notif");
            a0.removeNotification("navdy#traffic#jam#notif");
            com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification a1 = (com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification)a0.getNotification("navdy#traffic#reroute#notif");
            if (a1 == null) {
                a1 = new com.navdy.hud.app.maps.notification.RouteTimeUpdateNotification("navdy#traffic#reroute#notif", com.navdy.hud.app.framework.notifications.NotificationType.FASTER_ROUTE, this.bus);
            }
            a1.setFasterRouteEvent(a);
            a0.addNotification((com.navdy.hud.app.framework.notifications.INotification)a1);
        } else {
            sLogger.v(new StringBuilder().append("traffic notification disabled:").append(a).toString());
        }
    }
    
    @Subscribe
    public void onTrafficReroutePromptDismiss(com.navdy.hud.app.maps.MapEvents$TrafficRerouteDismissEvent a) {
        this.removeFasterRouteNotifiation();
    }
    
    public void removeFasterRouteNotifiation() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#traffic#reroute#notif");
    }
}
