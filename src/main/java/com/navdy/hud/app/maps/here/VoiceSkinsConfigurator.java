package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;

class VoiceSkinsConfigurator {
    final private static com.navdy.hud.app.util.PackagedResource resource;
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.VoiceSkinsConfigurator.class);
        resource = new com.navdy.hud.app.util.PackagedResource("voiceskins", com.navdy.hud.app.storage.PathManager.getInstance().getHereVoiceSkinsPath());
    }
    
    VoiceSkinsConfigurator() {
    }
    
    static void updateVoiceSkins() {
        try {
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            com.navdy.service.library.util.IOUtils.checkIntegrity(a, com.navdy.hud.app.storage.PathManager.getInstance().getHereVoiceSkinsPath(), R.raw.here_voices_integrity);
            sLogger.i("Voice skins update: starting");
            long j = android.os.SystemClock.elapsedRealtime();
            resource.updateFromResources(a, R.raw.here_voices, R.raw.here_voices_md5, true);
            long j0 = android.os.SystemClock.elapsedRealtime();
            sLogger.i(new StringBuilder().append("Voice skins update: time took ").append(j0 - j).append(" ms").toString());
        } catch(Throwable a0) {
            sLogger.e("Voice skins error", a0);
        }
    }
}
