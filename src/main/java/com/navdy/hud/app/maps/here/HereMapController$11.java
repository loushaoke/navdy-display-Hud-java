package com.navdy.hud.app.maps.here;

class HereMapController$11 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapController this$0;
    final com.here.android.mpa.mapping.Map.OnTransformListener val$listener;
    
    HereMapController$11(com.navdy.hud.app.maps.here.HereMapController a, com.here.android.mpa.mapping.Map.OnTransformListener a0) {

        super();
        this.this$0 = a;
        this.val$listener = a0;
    }
    
    public void run() {
        this.this$0.map.removeTransformListener(this.val$listener);
    }
}
