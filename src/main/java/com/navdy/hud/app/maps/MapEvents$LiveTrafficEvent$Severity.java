package com.navdy.hud.app.maps;

public enum MapEvents$LiveTrafficEvent$Severity {
    HIGH(0),
    VERY_HIGH(1);

    public int value;
    MapEvents$LiveTrafficEvent$Severity(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class MapEvents$LiveTrafficEvent$Severity extends Enum {
//    final private static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity[] $VALUES;
//    final public static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity HIGH;
//    final public static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity VERY_HIGH;
//    final public int value;
//
//    static {
//        HIGH = new com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity("HIGH", 0, 0);
//        VERY_HIGH = new com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity("VERY_HIGH", 1, 1);
//        com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity[] a = new com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity[2];
//        a[0] = HIGH;
//        a[1] = VERY_HIGH;
//        $VALUES = a;
//    }
//
//    private MapEvents$LiveTrafficEvent$Severity(String s, int i, int i0) {
//        super(s, i);
//        this.value = i0;
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity valueOf(String s) {
//        return (com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity)Enum.valueOf(com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity[] values() {
//        return $VALUES.clone();
//    }
//}
