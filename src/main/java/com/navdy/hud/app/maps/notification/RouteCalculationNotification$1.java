package com.navdy.hud.app.maps.notification;

class RouteCalculationNotification$1 implements Runnable {
    final com.navdy.hud.app.maps.notification.RouteCalculationNotification this$0;
    
    RouteCalculationNotification$1(com.navdy.hud.app.maps.notification.RouteCalculationNotification a) {

        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$000(this.this$0) != null && com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$100(this.this$0) != null) {
            com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$200().v("nav lookup expired");
            com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$102(this.this$0, (com.navdy.service.library.events.places.DestinationSelectedRequest)null);
            com.navdy.hud.app.maps.notification.RouteCalculationNotification.access$300(this.this$0);
        }
    }
}
