package com.navdy.hud.app.maps.here;

class HereLocationFixManager$4 implements android.location.LocationListener {
    final com.navdy.hud.app.maps.here.HereLocationFixManager this$0;
    
    HereLocationFixManager$4(com.navdy.hud.app.maps.here.HereLocationFixManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onLocationChanged(android.location.Location a) {
        if (com.navdy.hud.app.maps.here.HereLocationFixManager.access$1000(this.this$0) && !com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$1100(this.this$0, a);
        }
        com.navdy.hud.app.maps.here.HereLocationFixManager.access$1300(this.this$0, a);
    }
    
    public void onProviderDisabled(String s) {
    }
    
    public void onProviderEnabled(String s) {
    }
    
    public void onStatusChanged(String s, int i, android.os.Bundle a) {
    }
}
