package com.navdy.hud.app.maps.here;

class HerePositionUpdateListener$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HerePositionUpdateListener this$0;
    final com.here.android.mpa.common.GeoPosition val$geoPosition;
    
    HerePositionUpdateListener$1(com.navdy.hud.app.maps.here.HerePositionUpdateListener a, com.here.android.mpa.common.GeoPosition a0) {

        super();
        this.this$0 = a;
        this.val$geoPosition = a0;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HerePositionUpdateListener.access$000(this.this$0, this.val$geoPosition);
    }
}
