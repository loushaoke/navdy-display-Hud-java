package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.util.DeviceUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;


public class HereOfflineMapsVersion {
    final static com.navdy.service.library.log.Logger sLogger;
    private Boolean updateAvail = false;
    private ArrayList packages = new ArrayList<String>();
    private String version = "unknown";
    private String rawDate = "";
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereOfflineMapsVersion.class);
    }
    
    public HereOfflineMapsVersion() {
//        try {
//            this.rawDate = "";
//            this.date = this.parseMapDate(this.rawDate);
//            this.version = a.getString("here_map_version");
//
//            JSONArray a0 = a.getJSONArray("map_packages");
//            int i = a0.length();
//
//            this.packages = new ArrayList<>(i);
//
//            int i0 = 0;
//            while(i0 < i) {
//                this.packages.add(a0.getString(i0));
//                i0 = i0 + 1;
//            }
//        } catch(Exception a1) {
//            sLogger.e("Failed to parse off-line maps version info", a1);
//        }
    }


    
//    private java.util.Date parseMapDate(String s) {
//        java.util.Date a;
//
//        StringTokenizer a0 = new StringTokenizer(s, ".");
//
//        int i = -1;
//        int i0 = -1;
//        int i1 = -1;
//        while(a0.hasMoreElements()) {
//            String s0 = a0.nextToken();
//            if (i != -1) {
//                if (i0 != -1) {
//                    i1 = Integer.parseInt(s0);
//                } else {
//                    i0 = Integer.parseInt(s0);
//                }
//            } else {
//                i = Integer.parseInt(s0);
//            }
//        }
//        label2: {
//            label0: {
//                label1: {
//                    if (i == -1) {
//                        break label1;
//                    }
//                    if (i0 == -1) {
//                        break label1;
//                    }
//                    if (i1 != -1) {
//                        break label0;
//                    }
//                }
//                a = null;
//                break label2;
//            }
//            java.util.Calendar a1 = java.util.Calendar.getInstance();
////            a1.set(1, i);
////            a1.set(2, i0 - 1);
////            a1.set(5, i1);
//
//            a1.set(Calendar.YEAR, i);
//            a1.set(Calendar.MONTH, i0 - 1);
//            a1.set(Calendar.DATE, i1);
//
//            a = a1.getTime();
//        }
//        return a;
//    }
    
    public Boolean updateAvailable() {
        return this.updateAvail;
    }

    public java.util.List getPackages() {
        return this.packages;
    }
    
    public String getRawDate() {
        return this.rawDate;
    }
    
    public String getVersion() {
        return this.version;
    }

    public void setVersion(String v) {
        this.version = v;
    }

    public void setUpdateAvailable(Boolean a) {
        if (a != null) {
            this.updateAvail = a;
        }
    }

    public void addPackage(String p) {
        packages.add(p);
    }

}
