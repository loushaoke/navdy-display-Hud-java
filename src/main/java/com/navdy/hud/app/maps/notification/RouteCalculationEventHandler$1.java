package com.navdy.hud.app.maps.notification;

class RouteCalculationEventHandler$1 implements com.navdy.hud.app.ui.framework.INotificationAnimationListener {
    final com.navdy.hud.app.maps.notification.RouteCalculationEventHandler this$0;
    
    RouteCalculationEventHandler$1(com.navdy.hud.app.maps.notification.RouteCalculationEventHandler a) {
        super();
        this.this$0 = a;
    }
    
    public void onStart(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager.Mode a0) {
        if (a0 == com.navdy.hud.app.ui.framework.UIStateManager.Mode.COLLAPSE && "navdy#route#calc#notif".equals(s)) {
            com.navdy.hud.app.maps.notification.RouteCalculationNotification a1 = (com.navdy.hud.app.maps.notification.RouteCalculationNotification)com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification("navdy#route#calc#notif");
            if (a1 != null) {
                com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.access$000().v("hideStartTrip");
                a1.hideStartTrip();
            }
        }
    }
    
    public void onStop(String s, com.navdy.hud.app.framework.notifications.NotificationType a, com.navdy.hud.app.ui.framework.UIStateManager.Mode a0) {
        if (a0 == com.navdy.hud.app.ui.framework.UIStateManager.Mode.EXPAND && "navdy#route#calc#notif".equals(s)) {
            com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.access$000().v("showStartTrip: check");
            com.navdy.hud.app.maps.notification.RouteCalculationNotification a1 = (com.navdy.hud.app.maps.notification.RouteCalculationNotification)com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().getNotification("navdy#route#calc#notif");
            if (a1 != null && a1.isStarting()) {
                com.navdy.hud.app.maps.notification.RouteCalculationEventHandler.access$000().v("showStartTrip");
                a1.showStartTrip();
            }
        }
    }
}
