package com.navdy.hud.app.maps.here;

public enum HereTrafficUpdater$State {
    STOPPED(0),
    ACTIVE(1),
    TRACK_DISTANCE(2),
    TRACK_JAM(3);

    private int value;
    HereTrafficUpdater$State(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class HereTrafficUpdater$State extends Enum {
//    final private static com.navdy.hud.app.maps.here.HereTrafficUpdater$State[] $VALUES;
//    final public static com.navdy.hud.app.maps.here.HereTrafficUpdater$State ACTIVE;
//    final public static com.navdy.hud.app.maps.here.HereTrafficUpdater$State STOPPED;
//    final public static com.navdy.hud.app.maps.here.HereTrafficUpdater$State TRACK_DISTANCE;
//    final public static com.navdy.hud.app.maps.here.HereTrafficUpdater$State TRACK_JAM;
//
//    static {
//        STOPPED = new com.navdy.hud.app.maps.here.HereTrafficUpdater$State("STOPPED", 0);
//        ACTIVE = new com.navdy.hud.app.maps.here.HereTrafficUpdater$State("ACTIVE", 1);
//        TRACK_DISTANCE = new com.navdy.hud.app.maps.here.HereTrafficUpdater$State("TRACK_DISTANCE", 2);
//        TRACK_JAM = new com.navdy.hud.app.maps.here.HereTrafficUpdater$State("TRACK_JAM", 3);
//        com.navdy.hud.app.maps.here.HereTrafficUpdater$State[] a = new com.navdy.hud.app.maps.here.HereTrafficUpdater$State[4];
//        a[0] = STOPPED;
//        a[1] = ACTIVE;
//        a[2] = TRACK_DISTANCE;
//        a[3] = TRACK_JAM;
//        $VALUES = a;
//    }
//
//    private HereTrafficUpdater$State(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.maps.here.HereTrafficUpdater$State valueOf(String s) {
//        return (com.navdy.hud.app.maps.here.HereTrafficUpdater$State)Enum.valueOf(com.navdy.hud.app.maps.here.HereTrafficUpdater$State.class, s);
//    }
//
//    public static com.navdy.hud.app.maps.here.HereTrafficUpdater$State[] values() {
//        return $VALUES.clone();
//    }
//}
