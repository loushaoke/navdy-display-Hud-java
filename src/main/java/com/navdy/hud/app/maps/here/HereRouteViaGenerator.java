package com.navdy.hud.app.maps.here;

public class HereRouteViaGenerator {
    final private static String COMMA = ", ";
    final private static String EMPTY = "";
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static StringBuilder viaBuilder;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRouteViaGenerator.class);
        viaBuilder = new StringBuilder();
    }
    
    public HereRouteViaGenerator() {
    }
    
    private static java.util.List buildDistanceListDesc(java.util.List a, java.util.List a0, java.util.concurrent.atomic.AtomicInteger a1, String s) {
        java.util.HashMap a2 = new java.util.HashMap();
        java.util.ArrayList a3 = new java.util.ArrayList();
        label0: {
            label3: {
                label2: {
                    if (a == null) {
                        break label2;
                    }
                    Object a4 = a.iterator();
                    int i = 0;
                    while(((java.util.Iterator)a4).hasNext()) {
                        Object a5 = ((com.here.android.mpa.routing.RouteResult)((java.util.Iterator)a4).next()).getRoute().getRouteElements().getElements().iterator();
                        int i0 = 1;
                        while(((java.util.Iterator)a5).hasNext()) {
                            com.navdy.hud.app.maps.here.HereRouteViaGenerator.processRoadElement(((com.here.android.mpa.routing.RouteElement)((java.util.Iterator)a5).next()).getRoadElement(), a2, i, a1);
                            int i1 = i0 % 5;
                            label1: {
                                if (i1 != 0) {
                                    break label1;
                                }
                                if (s == null) {
                                    break label1;
                                }
                                if (com.navdy.hud.app.maps.here.HereRouteViaGenerator.isRouteCancelled(s)) {
                                    break label0;
                                }
                            }
                            i0 = i0 + 1;
                        }
                        a3.addAll(a2.values());
                        a2.clear();
                        i = i + 1;
                    }
                    break label3;
                }
                Object a6 = a0.iterator();
                while(((java.util.Iterator)a6).hasNext()) {
                    Object a7 = ((com.here.android.mpa.routing.Maneuver)((java.util.Iterator)a6).next()).getRoadElements().iterator();
                    while(((java.util.Iterator)a7).hasNext()) {
                        com.navdy.hud.app.maps.here.HereRouteViaGenerator.processRoadElement((com.here.android.mpa.common.RoadElement)((java.util.Iterator)a7).next(), a2, 0, a1);
                    }
                }
                a3.addAll(a2.values());
            }
            java.util.Collections.sort((java.util.List)a3);
        }
        return (java.util.List)a3;
    }
    
    public static String[] generateVia(java.util.List a, String s) {
        sLogger.v(new StringBuilder().append("generating via for ").append(a.size()).append(" routes").toString());
        java.util.concurrent.atomic.AtomicInteger a0 = new java.util.concurrent.atomic.AtomicInteger(0);
        long j = android.os.SystemClock.elapsedRealtime();
        int i = a.size();
        String[] a1 = new String[i];
        double[] a2 = new double[i];
        label1: {
            Throwable a3 = null;
            com.navdy.service.library.log.Logger a4 = null;
            label2: {
                label7: try {
                    Object a5 = null;
                    java.util.List a6 = com.navdy.hud.app.maps.here.HereRouteViaGenerator.buildDistanceListDesc(a, (java.util.List)null, a0, s);
                    if (sLogger.isLoggable(2)) {
                        java.util.Iterator a7 = a6.iterator();
                        a5 = a6;
                        Object a8 = a7;
                        while(((java.util.Iterator)a8).hasNext()) {
                            com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer a9 = (com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer)((java.util.Iterator)a8).next();
                            sLogger.v(new StringBuilder().append("[").append(a9.index).append("] road[").append(a9.via).append("] distance[").append(a9.val).append("]").toString());
                        }
                    } else {
                        a5 = a6;
                    }
                    java.util.HashSet a10 = new java.util.HashSet();
                    int i0 = 0;
                    while(((java.util.List)a5).size() > 0 && i0 != a1.length) {
                        if (com.navdy.hud.app.maps.here.HereRouteViaGenerator.isRouteCancelled(s)) {
                            break label7;
                        }
                        int i1 = 0;
                        int i2 = -1;
                        label3: while(true) {
                            com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer a11 = (com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer)((java.util.List)a5).get(i1);
                            label6: {
                                label4: {
                                    label5: {
                                        if (i2 != -1) {
                                            break label5;
                                        }
                                        i2 = a11.index;
                                        break label4;
                                    }
                                    if (i2 == a11.index) {
                                        break label4;
                                    }
                                    sLogger.i("no more roadelements,go back");
                                    com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer a12 = (com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer)((java.util.List)a5).get(i1 - 1);
                                    a1[i0] = a12.via;
                                    a2[i0] = a12.val;
                                    break label6;
                                }
                                if (a10.contains(a11.via)) {
                                    sLogger.v(new StringBuilder().append("already seen[").append(a11.via).append("]").toString());
                                    if (i1 + 1 != ((java.util.List)a5).size()) {
                                        i1 = i1 + 1;
                                        continue label3;
                                    }
                                    sLogger.i("no more roadelements,use current");
                                } else {
                                    a1[a11.index] = a11.via;
                                    a2[a11.index] = a11.val;
                                    a10.add(a11.via);
                                }
                            }
                            Object a13 = ((java.util.List)a5).iterator();
                            while(((java.util.Iterator)a13).hasNext()) {
                                if (((com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer)((java.util.Iterator)a13).next()).index == i0) {
                                    ((java.util.Iterator)a13).remove();
                                }
                            }
                            i0 = i0 + 1;
                            break;
                        }
                    }
                } catch(Throwable a14) {
                    a3 = a14;
                    break label2;
                }
                sLogger.v(new StringBuilder().append("via algo took[").append(android.os.SystemClock.elapsedRealtime() - j).append("] len[").append(a1.length).append("]").toString());
                int i3 = 0;
                while(i3 < a1.length) {
                    sLogger.v(new StringBuilder().append("via[").append(i3 + 1).append("] [").append(a1[i3]).append("][").append(a2[i3]).append("]").toString());
                    i3 = i3 + 1;
                }
                break label1;
            }
            label0: {
                try {
                    sLogger.e(a3);
                    a4 = sLogger;
                    break label0;
                } catch(Throwable ignoredException) {
                }
                sLogger.v(new StringBuilder().append("via algo took[").append(android.os.SystemClock.elapsedRealtime() - j).append("] len[").append(a1.length).append("]").toString());
                int i4 = 0;
                while(i4 < a1.length) {
                    sLogger.v(new StringBuilder().append("via[").append(i4 + 1).append("] [").append(a1[i4]).append("][").append(a2[i4]).append("]").toString());
                    i4 = i4 + 1;
                }
                break label1;
            }
            a4.v(new StringBuilder().append("via algo took[").append(android.os.SystemClock.elapsedRealtime() - j).append("] len[").append(a1.length).append("]").toString());
            int i5 = 0;
            while(i5 < a1.length) {
                sLogger.v(new StringBuilder().append("via[").append(i5 + 1).append("] [").append(a1[i5]).append("][").append(a2[i5]).append("]").toString());
                i5 = i5 + 1;
            }
        }
        return a1;
    }
    
    private static String getDisplayString(com.here.android.mpa.common.RoadElement a) {
        String s = null;
        if (a != null) {
            s = a.getRouteName();
            if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                s = a.getRoadName();
            }
        } else {
            s = null;
        }
        return s;
    }
    
    public static String getViaString(com.here.android.mpa.routing.Route a) {
        return com.navdy.hud.app.maps.here.HereRouteViaGenerator.getViaString(a, 1);
    }
    
    public static String getViaString(com.here.android.mpa.routing.Route a, int i) {
        String s = null;
        java.util.concurrent.atomic.AtomicInteger a0 = new java.util.concurrent.atomic.AtomicInteger(0);
        synchronized(viaBuilder) {
            StringBuilder a2 = viaBuilder;
            a2.setLength(0);
            if (a != null) {
                java.util.List a3 = com.navdy.hud.app.maps.here.HereRouteViaGenerator.buildDistanceListDesc((java.util.List)null, a.getManeuvers(), a0, (String)null);
                if (a3 != null && a3.size() > 0) {
                    int i0 = a3.size();
                    Object a4 = a3;
                    int i1 = 0;
                    while(i1 < i0) {
                        if (viaBuilder.length() > 0) {
                            viaBuilder.append(", ");
                        }
                        viaBuilder.append(((com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer)((java.util.List)a4).get(i1)).via);
                        int i2 = i + -1;
                        if (i2 == 0) {
                            break;
                        }
                        i1 = i1 + 1;
                        i = i2;
                    }
                }
            }
            s = viaBuilder.toString();
            viaBuilder.setLength(0);
            sLogger.v(new StringBuilder().append("getViaString[").append(s).append("]").toString());
            /*monexit(a1)*/;
        }
        return s;
    }
    
    private static boolean isRouteCancelled(String s) {
        boolean b = false;
        if (s != null) {
            String s0 = com.navdy.hud.app.maps.here.HereRouteManager.getActiveRouteCalcId();
            if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)s0)) {
                b = false;
            } else {
                sLogger.v(new StringBuilder().append("route request [").append(s).append("] is not active anymore, current [").append(s0).append("]").toString());
                b = true;
            }
        } else {
            b = false;
        }
        return b;
    }
    
    private static void processRoadElement(com.here.android.mpa.common.RoadElement a, java.util.HashMap a0, int i, java.util.concurrent.atomic.AtomicInteger a1) {
        String s = com.navdy.hud.app.maps.here.HereRouteViaGenerator.getDisplayString(a);
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer a2 = (com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer)a0.get(s);
            if (a2 != null) {
                a2.val = a2.val + a.getGeometryLength();
            } else {
                a0.put(s, new com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer(a.getGeometryLength(), i, (long)a1.getAndIncrement(), s));
            }
        }
    }
}
