package com.navdy.hud.app.maps.notification;
import com.navdy.hud.app.R;

class TrafficNotificationManager$1$1$1 implements Runnable {
    final com.navdy.hud.app.maps.notification.TrafficNotificationManager$1$1 this$2;
    final android.graphics.Bitmap val$combined;
    
    TrafficNotificationManager$1$1$1(com.navdy.hud.app.maps.notification.TrafficNotificationManager$1$1 a, android.graphics.Bitmap a0) {

        super();
        this.this$2 = a;
        this.val$combined = a0;
    }
    
    public void run() {
        try {
            com.navdy.hud.app.ui.component.homescreen.HomeScreenView a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
            android.view.View a0 = android.view.LayoutInflater.from(a.getContext()).inflate(R.layout.junction_view_lyt, (android.view.ViewGroup)null);
            android.widget.ImageView a1 = (android.widget.ImageView)a0.findViewById(R.id.image);
            android.widget.FrameLayout.LayoutParams a2 = new android.widget.FrameLayout.LayoutParams(com.navdy.hud.app.maps.notification.TrafficNotificationManager.access$300(this.this$2.this$1.this$0), com.navdy.hud.app.maps.notification.TrafficNotificationManager.access$400(this.this$2.this$1.this$0));
            a2.gravity = 83;
            a0.setLayoutParams((android.view.ViewGroup.LayoutParams)a2);
            a1.setImageBitmap(this.val$combined);
            a.injectRightSection(a0);
            com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.i("junction view mode injected");
        } catch(Throwable a3) {
            com.navdy.hud.app.maps.notification.TrafficNotificationManager.sLogger.e("junction view mode", a3);
        }
    }
}
