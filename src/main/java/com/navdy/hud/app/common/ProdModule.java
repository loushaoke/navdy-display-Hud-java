package com.navdy.hud.app.common;

import com.navdy.hud.app.analytics.AnalyticsSupport.NotificationReceiver;
import com.navdy.hud.app.analytics.AnalyticsSupport.TemperatureReporter;
import com.navdy.hud.app.framework.voice.VoiceSearchHandler;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import android.preference.PreferenceManager;
import com.navdy.hud.app.config.SettingsManager;
import com.navdy.hud.app.manager.PairingManager;
import com.navdy.hud.app.manager.MusicManager;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.storage.cache.Cache;
import com.navdy.hud.app.storage.cache.DiskLruCacheAdapter;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.service.library.network.http.HttpManager;
import com.navdy.service.library.network.http.IHttpManager;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.util.FeatureUtil;
import android.content.SharedPreferences;
import com.navdy.hud.app.storage.PathManager;
import com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.profile.DriverProfileManager;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.util.MusicArtworkCache;
import javax.inject.Singleton;
import dagger.Provides;
import com.navdy.hud.app.ancs.AncsServiceConnector;
import com.squareup.otto.Bus;
import android.content.Context;
import com.navdy.hud.app.view.EngineTemperaturePresenter;
import com.navdy.hud.app.service.ObdCANBusDataUploadService;
import com.navdy.hud.app.service.GestureVideosSyncService;
import com.navdy.hud.app.manager.UpdateReminderManager;
import com.navdy.hud.app.ui.component.mainmenu.MusicMenu2;
import com.navdy.hud.app.view.MusicWidgetPresenter;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.obd.CarMDVinDecoder;
import com.navdy.hud.app.util.ReportIssueService;
import com.navdy.hud.app.service.ShutdownMonitor;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.debug.DebugReceiver;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.maps.MapSchemeController;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapsEventHandler;
import com.navdy.hud.app.util.OTAUpdateService;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.maps.here.HereRegionManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.service.ConnectionServiceProxy;
import dagger.Module;
import com.navdy.hud.app.R;

@Module(injects = {
            ConnectionServiceProxy.class,
            HereMapsManager.class,
            HereRegionManager.class,
            ObdManager.class,
            OTAUpdateService.class,
            MapsEventHandler.class,
            RemoteDeviceManager.class,
            MapSchemeController.class,
            DriverProfileHelper.class,
            NotificationManager.class,
            TripManager.class,
            Main.Presenter.DeferredServices.class,
            DriverProfileHelper.class,
            DebugReceiver.class,
            NetworkStateManager.class,
            ShutdownMonitor.class,
            ReportIssueService.class,
            CarMDVinDecoder.class,
            DriveRecorder.class,
            HudApplication.class,
            SpeedManager.class,
            MusicWidgetPresenter.class,
            MusicMenu2.class,
            UpdateReminderManager.class,
            TemperatureReporter.class,
            NotificationReceiver.class,
            GestureVideosSyncService.class,
            ObdCANBusDataUploadService.class,
            EngineTemperaturePresenter.class },
        library = true)
public class ProdModule
{
    private static final int MUSIC_DATA_CACHE_SIZE = 2000000;
    private static final String MUSIC_RESPONSE_CACHE = "MUSIC_RESPONSE_CACHE";
    private static final String PREF_NAME = "App";
    private Context context;
    
    public ProdModule(final Context context) {
        this.context = context;
    }
    
    @Provides
    @Singleton
    AncsServiceConnector provideAncsServiceConnector(final Bus bus) {
        return new AncsServiceConnector(this.context, bus);
    }
    
    @Provides
    @Singleton
    MusicArtworkCache provideArtworkCache() {
        return new MusicArtworkCache();
    }
    
    @Provides
    @Singleton
    Bus provideBus() {
        return new MainThreadBus();
    }
    
    @Provides
    @Singleton
    CallManager provideCallManager(final Bus bus) {
        return new CallManager(bus, this.context);
    }
    
    @Provides
    @Singleton
    ConnectionHandler provideConnectionHandler(final ConnectionServiceProxy connectionServiceProxy, final PowerManager powerManager, final DriverProfileManager driverProfileManager, final UIStateManager uiStateManager, final TimeHelper timeHelper) {
        return new ConnectionHandler(this.context, connectionServiceProxy, powerManager, driverProfileManager, uiStateManager, timeHelper);
    }
    
    @Provides
    @Singleton
    ConnectionServiceProxy provideConnectionServiceProxy(final Bus bus) {
        return new ConnectionServiceProxy(this.context, bus);
    }
    
    @Provides
    @Singleton
    DialSimulatorMessagesHandler provideDialSimulatorMessagesHandler() {
        return new DialSimulatorMessagesHandler(this.context);
    }
    
    @Provides
    @Singleton
    DriverProfileManager provideDriverProfileManager(final Bus bus, final PathManager pathManager, final TimeHelper timeHelper) {
        return new DriverProfileManager(bus, pathManager, timeHelper);
    }
    
    @Provides
    @Singleton
    FeatureUtil provideFeatureUtil(final SharedPreferences sharedPreferences) {
        return new FeatureUtil(sharedPreferences);
    }
    
    @Provides
    @Singleton
    GestureServiceConnector provideGestureServiceConnector(final Bus bus, final PowerManager powerManager) {
        return new GestureServiceConnector(bus, powerManager);
    }
    
    @Provides
    @Singleton
    IHttpManager provideHttpManager() {
        return new HttpManager();
    }
    
    @Provides
    @Singleton
    InputManager provideInputManager(final Bus bus, final PowerManager powerManager, final UIStateManager uiStateManager) {
        return new InputManager(bus, powerManager, uiStateManager);
    }
    
    @Provides
    @Singleton
    MessageCache<MusicCollectionResponse> provideMusicCollectionResponseMessageCache(final PathManager pathManager) {
        return new MessageCache<>(new DiskLruCacheAdapter(MUSIC_RESPONSE_CACHE, pathManager.getMusicDiskCacheFolder(), MUSIC_DATA_CACHE_SIZE), MusicCollectionResponse.class);
    }
    
    @Provides
    @Singleton
    MusicManager provideMusicManager(final MessageCache<MusicCollectionResponse> messageCache, final Bus bus, final UIStateManager uiStateManager, final PandoraManager pandoraManager, final MusicArtworkCache musicArtworkCache) {
        return new MusicManager(messageCache, bus, this.context.getResources(), uiStateManager, pandoraManager, musicArtworkCache);
    }
    
    @Provides
    @Singleton
    DriveRecorder provideObdDataReceorder(final Bus bus, final ConnectionHandler connectionHandler) {
        return new DriveRecorder(bus, connectionHandler);
    }
    
    @Provides
    @Singleton
    PairingManager providePairingManager() {
        return new PairingManager();
    }
    
    @Provides
    @Singleton
    PandoraManager providePandoraManager(final Bus bus) {
        return new PandoraManager(bus, this.context.getResources());
    }
    
    @Provides
    @Singleton
    PathManager providePathManager() {
        return PathManager.getInstance();
    }
    
    @Provides
    @Singleton
    PowerManager providePowerManager(final Bus bus, final SharedPreferences sharedPreferences) {
        return new PowerManager(bus, this.context, sharedPreferences);
    }
    
    @Provides
    @Singleton
    SettingsManager provideSettingsManager(final Bus bus, final SharedPreferences sharedPreferences) {
        return new SettingsManager(bus, sharedPreferences);
    }
    
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        PreferenceManager.setDefaultValues(this.context, PREF_NAME, 0, R.xml.developer_preferences, true);
        return this.context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }
    
    @Provides
    @Singleton
    TelemetryDataManager provideTelemetryDataManager(final UIStateManager uiStateManager, final PowerManager powerManager, final Bus bus, final SharedPreferences sharedPreferences, final TripManager tripManager) {
        return new TelemetryDataManager(uiStateManager, powerManager, bus, sharedPreferences, tripManager);
    }
    
    @Provides
    @Singleton
    TimeHelper provideTimeHelper(final Bus bus) {
        return new TimeHelper(this.context, bus);
    }
    
    @Provides
    @Singleton
    TripManager provideTripManager(final Bus bus, final SharedPreferences sharedPreferences) {
        return new TripManager(bus, sharedPreferences);
    }
    
    @Provides
    @Singleton
    UIStateManager provideUIStateManager() {
        return new UIStateManager();
    }
    
    @Provides
    @Singleton
    VoiceSearchHandler provideVoiceSearchHandler(final Bus bus, final FeatureUtil featureUtil) {
        return new VoiceSearchHandler(this.context, bus, featureUtil);
    }
}
