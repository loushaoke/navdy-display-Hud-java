package com.navdy.hud.app.framework.notifications;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.icu.text.IDNA;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.glance.GlanceViewCache;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

final public class NotificationManager {
    final private static int EXPAND_VIEW_ANIMATION_DURATION = 250;
    final public static int EXPAND_VIEW_ANIMATION_START_DELAY_DURATION = 250;
    final private static int EXPAND_VIEW_ELEMENT_ANIMATION_DURATION = 100;
    final private static int EXPAND_VIEW_QUICK_DURATION = 0;
    final public static String EXTRA_VOICE_SEARCH_NOTIFICATION = "extra_voice_search_notification";
    final private static com.navdy.hud.app.framework.notifications.NotificationManager$NotificationChange NOTIFICATION_CHANGE;
    final private static com.navdy.hud.app.framework.notifications.NotificationManager sInstance;
    final static com.navdy.service.library.log.Logger sLogger;
    private java.util.HashSet NOTIFS_NOT_ALLOWED_ON_DISCONNECT;
    private java.util.HashSet NOTIFS_REMOVED_ON_DISCONNECT;
    int animationTranslation;
    @Inject
    com.squareup.otto.Bus bus;
    private java.util.Comparator comparator;
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info currentNotification;
    private boolean deleteAllGlances;
    private boolean disconnected;
    private boolean enable;
    private boolean enablePhoneCalls;
    private android.view.View expandedNotifCoverView;
    private android.widget.FrameLayout expandedNotifView;
    private boolean expandedWithStack;
    private com.navdy.hud.app.device.light.LED.Settings gestureEnabledSettings;
    private android.os.Handler handler;
    private Runnable ignoreScrollRunnable;
    private volatile boolean isAnimating;
    private volatile boolean isCollapsed;
    private volatile boolean isExpanded;
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info lastStackCurrentNotification;
    private final Object lockObj;
    private com.navdy.hud.app.ui.activity.Main mainScreen;
    private com.navdy.hud.app.ui.framework.INotificationAnimationListener notifAnimationListener;
    private com.navdy.hud.app.ui.component.carousel.CarouselIndicator notifIndicator;
    private com.navdy.hud.app.ui.component.carousel.ProgressIndicator notifScrollIndicator;
    private com.navdy.hud.app.view.NotificationView notifView;
    private com.navdy.hud.app.framework.notifications.INotificationController notificationController;
    private long notificationCounter;
    private java.util.HashMap notificationIdMap;
    private java.util.TreeMap notificationPriorityMap;
    private java.util.ArrayList notificationSavedList;
    private com.navdy.service.library.device.NavdyDeviceId notificationSavedListDeviceId;
    private java.util.Queue operationQueue;
    private boolean operationRunning;
    com.navdy.hud.app.framework.notifications.INotification pendingNotification;
    private com.navdy.service.library.events.ui.Screen pendingScreen;
    private android.os.Bundle pendingScreenArgs;
    private Object pendingScreenArgs2;
    private android.os.Bundle pendingShutdownArgs;
    private android.content.res.Resources resources;
    private com.navdy.hud.app.framework.notifications.IProgressUpdate scrollProgress;
    private com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState scrollState;
    private boolean showOn;
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info stackCurrentNotification;
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info stagedNotification;
    private boolean ttsOn;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.notifications.NotificationManager.class);
        NOTIFICATION_CHANGE = new com.navdy.hud.app.framework.notifications.NotificationManager$NotificationChange();
        sInstance = new com.navdy.hud.app.framework.notifications.NotificationManager();
    }
    
    private NotificationManager() {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.operationQueue = (java.util.Queue)new java.util.LinkedList();
        this.enable = true;
        this.enablePhoneCalls = true;
        this.scrollState = com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.NONE;
        this.ignoreScrollRunnable = (Runnable)new com.navdy.hud.app.framework.notifications.NotificationManager$1(this);
        this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.notificationController = (com.navdy.hud.app.framework.notifications.INotificationController)new com.navdy.hud.app.framework.notifications.NotificationManager$2(this);
        this.NOTIFS_REMOVED_ON_DISCONNECT = new java.util.HashSet();
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT = new java.util.HashSet();
        this.comparator = (java.util.Comparator)new com.navdy.hud.app.framework.notifications.NotificationManager$3(this);
        this.lockObj = new Object();
        this.notificationPriorityMap = new java.util.TreeMap(this.comparator);
        this.notificationIdMap = new java.util.HashMap();
        this.notificationSavedList = new java.util.ArrayList();
        this.disconnected = true;
        this.pendingShutdownArgs = null;
        this.notifAnimationListener = (com.navdy.hud.app.ui.framework.INotificationAnimationListener)new com.navdy.hud.app.framework.notifications.NotificationManager$4(this);
        this.scrollProgress = (com.navdy.hud.app.framework.notifications.IProgressUpdate)new com.navdy.hud.app.framework.notifications.NotificationManager$5(this);
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.uiStateManager.addNotificationAnimationListener(this.notifAnimationListener);
        this.NOTIFS_REMOVED_ON_DISCONNECT.add("navdy#phone#call#notif");
        this.NOTIFS_REMOVED_ON_DISCONNECT.add("navdy#music#notif");
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.add("navdy#phone#call#notif");
        this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.add("navdy#music#notif");
        this.animationTranslation = (int)com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimension(R.dimen.glance_anim_translation);
        this.bus.register(this);
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState access$002(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState a0) {
        a.scrollState = a0;
        return a0;
    }
    
    static com.navdy.hud.app.view.NotificationView access$100(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.getNotificationView();
    }
    
    static boolean access$1000(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.showOn;
    }
    
    static boolean access$1002(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b) {
        a.showOn = b;
        return b;
    }
    
    static boolean access$1100(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.expandedWithStack;
    }
    
    static boolean access$1102(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b) {
        a.expandedWithStack = b;
        return b;
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager$Info access$1200(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.stackCurrentNotification;
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager$Info access$1202(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0) {
        a.stackCurrentNotification = a0;
        return a0;
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager$Info access$1302(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0) {
        a.lastStackCurrentNotification = a0;
        return a0;
    }
    
    static boolean access$1400(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.isPhoneNotifPresentAndNotAlive();
    }
    
    static int access$1500(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0) {
        return a.getNotificationIndex(a0);
    }
    
    static void access$1600(com.navdy.hud.app.framework.notifications.NotificationManager a, int i) {
        a.displayScrollingIndicator(i);
    }
    
    static void access$1700(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b, boolean b0, boolean b1) {
        a.collapseNotificationInternal(b, b0, b1);
    }
    
    static boolean access$1802(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b) {
        a.isCollapsed = b;
        return b;
    }
    
    static com.navdy.hud.app.framework.notifications.INotificationController access$1900(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.notificationController;
    }
    
    static boolean access$200(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.operationRunning;
    }
    
    static boolean access$2002(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b) {
        a.isExpanded = b;
        return b;
    }
    
    static com.navdy.hud.app.device.light.LED.Settings access$2100(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.gestureEnabledSettings;
    }
    
    static com.navdy.hud.app.device.light.LED.Settings access$2102(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.device.light.LED.Settings a0) {
        a.gestureEnabledSettings = a0;
        return a0;
    }
    
    static boolean access$2200(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.deleteAllGlances;
    }
    
    static boolean access$2202(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b) {
        a.deleteAllGlances = b;
        return b;
    }
    
    static Object access$2300(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.lockObj;
    }
    
    static void access$2400(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        a.removeAllNotification();
    }

    static void access$2600(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0, boolean b) {
        a.removeNotificationfromStack(a0, b);
    }
    
    static void access$2700(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b) {
        a.showNotification(b);
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager$Info access$2800(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.stagedNotification;
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager$Info access$2802(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0) {
        a.stagedNotification = a0;
        return a0;
    }
    
    static com.navdy.service.library.events.ui.Screen access$2900(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.pendingScreen;
    }
    
    static com.navdy.service.library.events.ui.Screen access$2902(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.service.library.events.ui.Screen a0) {
        a.pendingScreen = a0;
        return a0;
    }
    
    static java.util.Queue access$300(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.operationQueue;
    }
    
    static boolean access$3000(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.service.library.events.ui.Screen a0) {
        return a.isScreenHighPriority(a0);
    }
    
    static android.os.Bundle access$3100(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.pendingScreenArgs;
    }
    
    static android.os.Bundle access$3102(com.navdy.hud.app.framework.notifications.NotificationManager a, android.os.Bundle a0) {
        a.pendingScreenArgs = a0;
        return a0;
    }
    
    static Object access$3200(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.pendingScreenArgs2;
    }
    
    static Object access$3202(com.navdy.hud.app.framework.notifications.NotificationManager a, Object a0) {
        a.pendingScreenArgs2 = a0;
        return a0;
    }
    
    static java.util.TreeMap access$3300(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.notificationPriorityMap;
    }
    
    static android.os.Bundle access$3400(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.pendingShutdownArgs;
    }
    
    static android.os.Bundle access$3402(com.navdy.hud.app.framework.notifications.NotificationManager a, android.os.Bundle a0) {
        a.pendingShutdownArgs = a0;
        return a0;
    }
    
    static com.navdy.hud.app.view.NotificationView access$3500(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.notifView;
    }
    
    static com.navdy.hud.app.ui.component.carousel.ProgressIndicator access$3600(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.notifScrollIndicator;
    }
    
    static void access$3700(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        a.startScrollThresholdTimer();
    }
    
    static void access$3800(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.INotification a0, java.util.EnumSet a1) {
        a.addNotificationInternal(a0, a1);
    }
    
    static com.navdy.hud.app.ui.component.carousel.CarouselIndicator access$3900(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.notifIndicator;
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager$Info access$400(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.currentNotification;
    }
    
    static android.widget.FrameLayout access$4000(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.expandedNotifView;
    }
    
    static com.navdy.hud.app.framework.notifications.NotificationManager$Info access$402(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.NotificationManager$Info a0) {
        a.currentNotification = a0;
        return a0;
    }
    
    static void access$4100(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        a.runQueuedOperation();
    }
    
    static void access$4200(com.navdy.hud.app.framework.notifications.NotificationManager a, android.view.View a0) {
        a.cleanupView(a0);
    }
    
    static void access$4300(com.navdy.hud.app.framework.notifications.NotificationManager a, com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation a0, boolean b, boolean b0, boolean b1, boolean b2, String s, boolean b3) {
        a.runOperation(a0, b, b0, b1, b2, s, b3);
    }
    
    static void access$4400(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        a.hideNotificationInternal();
    }
    
    static void access$4500(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        a.hideScrollingIndicator();
    }
    
    static void access$4600(com.navdy.hud.app.framework.notifications.NotificationManager a, android.graphics.RectF a0, com.navdy.hud.app.framework.notifications.IScrollEvent a1) {
        a.setNotifScrollIndicatorXY(a0, a1);
    }
    
    static boolean access$500(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.isAnimating;
    }
    
    static boolean access$502(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b) {
        a.isAnimating = b;
        return b;
    }
    
    static android.widget.FrameLayout access$600(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.getExpandedNotificationView();
    }
    
    static android.view.View access$700(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.getExpandedNotificationCoverView();
    }
    
    static com.navdy.hud.app.ui.component.carousel.CarouselIndicator access$800(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.getNotificationIndicator();
    }
    
    static boolean access$900(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        return a.ttsOn;
    }
    
    static boolean access$902(com.navdy.hud.app.framework.notifications.NotificationManager a, boolean b) {
        a.ttsOn = b;
        return b;
    }
    
    private void addNotificationInternal(com.navdy.hud.app.framework.notifications.INotification a, java.util.EnumSet a0) {
        Object a1 = null;
        Throwable a2 = null;
        com.navdy.hud.app.ui.activity.Main a3 = this.mainScreen;
        label0: {
            label1: {
                label18: {
                    if (a3 != null) {
                        break label18;
                    }
                    this.getNotificationView();
                    if (this.mainScreen == null) {
                        break label1;
                    }
                }
                String s = a.getId();
                if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                    sLogger.w("notification id is null");
                } else {
                    com.navdy.hud.app.framework.notifications.NotificationType a4 = a.getType();
                    if (a4 != null) {
                        sLogger.v(new StringBuilder().append("addNotif type[").append(a.getType()).append("] id[").append(s).append("]").toString());
                        synchronized(this.lockObj) {
                            if (this.currentNotification != null && !this.isNotificationViewShowing()) {
                                com.navdy.service.library.log.Logger a5 = sLogger;
                                StringBuilder a6 = new StringBuilder().append("addNotif current-notification marked null:");
                                Object a7 = (this.currentNotification.notification == null) ? "unk" : this.currentNotification.notification.getType();
                                a5.v(a6.append(a7).toString());
                                this.currentNotification = null;
                            }
                            com.navdy.hud.app.framework.notifications.NotificationManager$Info a8 = this.currentNotification;
                            label17: {
                                if (a8 == null) {
                                    break label17;
                                }
                                if (a.canAddToStackIfCurrentExists()) {
                                    break label17;
                                }
                                if (android.text.TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)a.getId())) {
                                    if (this.currentNotification.startCalled) {
                                        sLogger.v("addNotif updated");
                                        this.currentNotification.notification.onUpdate();
                                    } else {
                                        sLogger.v("addNotif cannot be added current rule-1");
                                    }
                                } else {
                                    sLogger.v("addNotif cannot be added current rule-2");
                                }
                                /*monexit(a1)*/;
                                break label1;
                            }
                            label12: {
                                if (a0 == null) {
                                    break label12;
                                }
                                com.navdy.hud.app.framework.notifications.NotificationManager$Info a9 = this.currentNotification;
                                label16: {
                                    if (a9 == null) {
                                        break label16;
                                    }
                                    sLogger.v("addNotif screen constraint failed, current notification active");
                                    /*monexit(a1)*/;
                                    break label1;
                                }
                                com.navdy.hud.app.screen.BaseScreen a10 = this.uiStateManager.getCurrentScreen();
                                label14: {
                                    label15: {
                                        if (a10 == null) {
                                            break label15;
                                        }
                                        if (a0.contains(a10.getScreen())) {
                                            break label14;
                                        }
                                    }
                                    sLogger.v(new StringBuilder().append("addNotif screen constraint failed, current screen:").append(a10).toString());
                                    /*monexit(a1)*/;
                                    break label1;
                                }
                                boolean b = this.mainScreen.isNotificationExpanding();
                                label13: {
                                    if (b) {
                                        break label13;
                                    }
                                    if (this.mainScreen.isNotificationCollapsing()) {
                                        break label13;
                                    }
                                    if (this.mainScreen.isNotificationViewShowing()) {
                                        break label13;
                                    }
                                    if (!this.mainScreen.isScreenAnimating()) {
                                        break label12;
                                    }
                                }
                                sLogger.v("addNotif screen constraint failed, mainscreen animating");
                                /*monexit(a1)*/;
                                break label1;
                            }
                            com.navdy.hud.app.framework.notifications.NotificationManager$Info a11 = this.currentNotification;
                            label10: {
                                label11: {
                                    if (a11 == null) {
                                        break label11;
                                    }
                                    if (android.text.TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)a.getId())) {
                                        break label10;
                                    }
                                }
                                com.navdy.hud.app.framework.notifications.NotificationManager$Info a12 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationIdMap.get(s);
                                if (a12 == null) {
                                    com.navdy.hud.app.framework.notifications.NotificationManager$Info a13 = new com.navdy.hud.app.framework.notifications.NotificationManager$Info();
                                    a13.notification = a;
                                    a13.priority = a4.getPriority();
                                    long j = this.notificationCounter + 1L;
                                    this.notificationCounter = j;
                                    a13.uniqueCounter = j;
                                    this.notificationIdMap.put(s, a13);
                                    this.notificationPriorityMap.put(a13, a13);
                                    sLogger.v("addNotif added to stack");
                                    com.navdy.hud.app.framework.notifications.NotificationManager$Info a14 = this.currentNotification;
                                    label6: {
                                        label2: {
                                            label5: {
                                                label4: {
                                                    label9: {
                                                        if (a14 != null) {
                                                            break label9;
                                                        }
                                                        sLogger.v(new StringBuilder().append("addNotif make current [").append(s).append("]").toString());
                                                        this.currentNotification = a13;
                                                        if (this.isExpanded()) {
                                                            this.currentNotification.pushBackDuetoHighPriority = true;
                                                            this.hideNotification();
                                                            break label4;
                                                        } else {
                                                            if (this.showNotification()) {
                                                                break label4;
                                                            }
                                                            sLogger.v("addNotif not making current, disabled");
                                                            this.currentNotification.pushBackDuetoHighPriority = true;
                                                            this.currentNotification = null;
                                                            break label4;
                                                        }
                                                    }
                                                    boolean b0 = a13.priority > this.currentNotification.priority;
                                                    boolean b1 = this.currentNotification.notification.isPurgeable();
                                                    label8: {
                                                        if (b0) {
                                                            break label8;
                                                        }
                                                        if (b1) {
                                                            break label8;
                                                        }
                                                        sLogger.v(new StringBuilder().append("addNotif lower priority than current[").append(this.currentNotification.notification.getType()).append("] update color").toString());
                                                        a13.pushBackDuetoHighPriority = true;
                                                        this.setNotificationColor();
                                                        if (this.isAnimating) {
                                                            break label4;
                                                        }
                                                        if (this.isExpanded) {
                                                            break label4;
                                                        }
                                                        if (this.isNotificationViewShowing()) {
                                                            break label4;
                                                        }
                                                        sLogger.v("add notif lower priority, but view hidden");
                                                        this.showNotification();
                                                        break label4;
                                                    }
                                                    sLogger.v(new StringBuilder().append("addNotif high priority[").append(b0).append("] purgeable[").append(b1).append("]").toString());
                                                    com.navdy.hud.app.framework.notifications.NotificationManager$Info a15 = this.stagedNotification;
                                                    label7: {
                                                        if (a15 != null) {
                                                            break label7;
                                                        }
                                                        if (this.currentNotification.notification.isAlive()) {
                                                            this.currentNotification.pushBackDuetoHighPriority = true;
                                                        } else {
                                                            this.currentNotification.removed = true;
                                                            sLogger.v(new StringBuilder().append("addNotif current notification not alive anymore [").append(this.currentNotification.notification.getType()).append("]").toString());
                                                        }
                                                        this.stagedNotification = a13;
                                                        if (this.isAnimating) {
                                                            sLogger.v("addNotif anim in progress");
                                                            break label4;
                                                        } else if (this.isExpanded) {
                                                            sLogger.v("addNotif hideNotif");
                                                            this.hideNotification();
                                                            break label4;
                                                        } else {
                                                            if (this.currentNotification.startCalled) {
                                                                sLogger.v("addNotif showNotif");
                                                            } else {
                                                                sLogger.v("addNotif swapping notif");
                                                                this.stagedNotification = null;
                                                                this.currentNotification = a13;
                                                            }
                                                            this.showNotification();
                                                            break label4;
                                                        }
                                                    }
                                                    if (android.text.TextUtils.equals((CharSequence)this.stagedNotification.notification.getId(), (CharSequence)a13.notification.getId())) {
                                                        break label6;
                                                    }
                                                    if (a13.priority <= this.stagedNotification.priority) {
                                                        break label5;
                                                    }
                                                    sLogger.v(new StringBuilder().append("addNotif replacing staged notif since it high priority than staged [").append(this.stagedNotification.notification.getId()).append("]").toString());
                                                    this.stagedNotification.pushBackDuetoHighPriority = true;
                                                    this.stagedNotification = a13;
                                                    boolean b2 = this.isAnimating;
                                                    label3: {
                                                        if (!b2) {
                                                            break label3;
                                                        }
                                                        sLogger.v("addNotif anim in progress");
                                                        break label4;
                                                    }
                                                    if (this.isExpanded) {
                                                        break label2;
                                                    }
                                                    sLogger.v("addNotif-1 showNotif");
                                                    this.showNotification();
                                                }
                                                this.postNotificationChange();
                                                this.updateExpandedIndicator();
                                                /*monexit(a1)*/;
                                                break label1;
                                            }
                                            sLogger.v(new StringBuilder().append("addNotif not replacing staged notif staged [").append(this.stagedNotification.notification.getId()).append("]").toString());
                                            /*monexit(a1)*/;
                                            break label1;
                                        }
                                        sLogger.v("addNotif-1 hideNotif");
                                        this.hideNotification();
                                        /*monexit(a1)*/;
                                        break label1;
                                    }
                                    sLogger.v("addNotif stage notif already added");
                                    /*monexit(a1)*/;
                                    break label1;
                                } else {
                                    if (this.currentNotification != null) {
                                        sLogger.v("addNotif already exist on stack, no-op");
                                    } else {
                                        sLogger.v("addNotif already exist on stack, making it current");
                                        this.currentNotification = a12;
                                        this.showNotification();
                                    }
                                    /*monexit(a1)*/;
                                    break label1;
                                }
                            }
                            if (this.currentNotification.removed) {
                                sLogger.v("addNotif notif was marked to remove,resurrecting it back");
                                this.currentNotification.removed = false;
                                this.currentNotification.resurrected = true;
                            } else if (this.currentNotification.startCalled) {
                                sLogger.v("addNotif already on screen,update");
                                a.onUpdate();
                            } else {
                                sLogger.v("addNotif not on screen yet");
                                if (!this.mainScreen.isNotificationViewShowing()) {
                                    this.showNotification();
                                }
                            }
//                            /*monexit(a1)*/;
                        }
                    } else {
                        sLogger.w("notification type is null");
                    }
                }
            }
            return;
        }
    }
    
    private void animateOutExpandedView(boolean b, com.navdy.hud.app.framework.notifications.NotificationManager$Info a, boolean b0) {
        boolean b1 = this.isExpanded();
        label0: {
            label1: {
                if (b1) {
                    break label1;
                }
                if (!this.isExpandedNotificationVisible()) {
                    break label0;
                }
            }
            this.runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.COLLAPSE_VIEW, false, b, b0, false, (String)null, false);
        }
    }
    
    private void animateOutExpandedView(boolean b, boolean b0) {
        this.animateOutExpandedView(b, this.currentNotification, b0);
    }
    
    private void cleanupView(android.view.View a) {
        com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType a0 = (com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType)a.getTag(R.id.glance_view_type);
        a.setTag(R.id.glance_view_type, null);
        a.setTag(R.id.glance_can_delete, null);
        if (a0 != null) {
            switch(com.navdy.hud.app.framework.notifications.NotificationManager$16.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceViewCache$ViewType[a0.ordinal()]) {
                case 2: {
                    android.widget.TextView a1 = (android.widget.TextView)a.findViewById(R.id.mainTitle);
                    a1.setAlpha(1f);
                    a1.setVisibility(View.VISIBLE);
                    android.widget.TextView a2 = (android.widget.TextView)a.findViewById(R.id.subTitle);
                    a2.setAlpha(1f);
                    a2.setVisibility(View.VISIBLE);
                    android.view.View a3 = a.findViewById(R.id.choiceLayout);
                    if (a3 != null) {
                        a3.setAlpha(1f);
                        a3.setVisibility(View.VISIBLE);
                        a3.setTag(null);
                        a3.setTag(R.id.message_prev_choice, null);
                        a3.setTag(R.id.message_secondary_screen, null);
                    }
                    com.navdy.hud.app.ui.component.image.InitialsImageView a4 = (com.navdy.hud.app.ui.component.image.InitialsImageView)a.findViewById(R.id.mainImage);
                    a4.setImage(0, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView.Style.DEFAULT);
                    a4.setTag(null);
                    ((android.widget.ImageView)a.findViewById(R.id.sideImage)).setImageResource(0);
                    a.setAlpha(1f);
                    com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE, a);
                    break;
                }
                case 1: {
                    ((android.widget.ImageView)a.findViewById(R.id.imageView)).setImageResource(0);
                    com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_IMAGE, a);
                    break;
                }
            }
        }
    }
    
    protected void cleanupViews(com.navdy.hud.app.view.NotificationView a) {
        sLogger.v("*** cleaning up expanded view");
        this.expandedWithStack = false;
        android.widget.FrameLayout a0 = this.getExpandedNotificationView();
        android.view.View a1 = this.getExpandedViewChild();
        if (a1 != null) {
            ((android.view.ViewGroup)a0).removeView(a1);
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(a1)) {
                a1.setTag(null);
                a1.setAlpha(1f);
                com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_TEXT, a1);
            }
            sLogger.v("*** removed expanded view");
        }
        android.view.ViewGroup a2 = this.notifView.getNotificationContainer();
        if (a2.getChildCount() > 0) {
            android.view.View a3 = a2.getChildAt(0);
            a2.removeView(a3);
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(a3)) {
                a3.setTag(null);
                this.cleanupView(a3);
            }
            sLogger.v("*** removed notif view");
        }
        if (this.stackCurrentNotification != null && this.stackCurrentNotification.startCalled) {
            this.stackCurrentNotification.startCalled = false;
            this.stackCurrentNotification.notification.onStop();
        }
        this.stackCurrentNotification = null;
        this.lastStackCurrentNotification = null;
        ((android.view.ViewGroup)a0).setVisibility(View.GONE);
        com.navdy.hud.app.ui.component.carousel.CarouselIndicator a4 = this.getNotificationIndicator();
        if (a4 != null) {
            a4.setVisibility(View.GONE);
            this.hideScrollingIndicator();
        }
        this.setNotificationColor();
        a.showNextNotificationColor();
    }
    
    private void clearAllGlances() {
        sLogger.v("glances turned off");
        boolean b = this.isExpanded();
        {
            label3:
            {
                label1:
                {
                    label2:
                    {
                        if (b) {
                            break label2;
                        }
                        if (!this.isExpandedNotificationVisible()) {
                            break label1;
                        }
                    }
                    sLogger.v("glances turned off: expanded mode");
                    this.deleteAllGlances = true;
                    this.clearOperationQueue();
                    this.collapseExpandedNotification(true, true);
                    break label3;
                }
                if (this.isNotificationViewShowing()) {
                    sLogger.v("glances turned off: notif showing");
                    this.deleteAllGlances = true;
                    this.collapseNotification();
                } else {
                    sLogger.v("glances turned off: notif not showing");
                    synchronized (this.lockObj) {
                        this.removeAllNotification();
                        /*monexit(a)*/
                        ;
                    }
                }
            }
        }
    }
    
    private void collapseExpandedViewInternal(boolean b, boolean b0) {
        sLogger.v("collapseExpandedViewInternal");
        this.isAnimating = true;
        this.clearOperationQueue();
        com.navdy.hud.app.framework.notifications.NotificationManager$12 a = new com.navdy.hud.app.framework.notifications.NotificationManager$12(this, b);
        android.widget.FrameLayout a0 = this.getExpandedNotificationView();
        android.view.View a1 = this.getExpandedViewChild();
        com.navdy.hud.app.framework.notifications.NotificationManager$13 a2 = new com.navdy.hud.app.framework.notifications.NotificationManager$13(this, a1, (android.view.ViewGroup)a0, (Runnable)a);
        boolean b1 = this.showOn;
        label3: {
            label0: {
                label1: {
                    label2: {
                        if (b1) {
                            break label2;
                        }
                        if (!this.isExpandedNotificationVisible()) {
                            break label1;
                        }
                    }
                    if (a1 != null) {
                        break label0;
                    }
                }
                ((Runnable)a2).run();
                break label3;
            }
            com.navdy.hud.app.framework.notifications.NotificationAnimator.animateChildViewOut(a1, b0 ? 0 : 250, (Runnable)a2);
        }
    }
    
    private void collapseNotificationInternal(boolean b, boolean b0, boolean b1) {
        sLogger.v("collapseNotificationInternal");
        label0: if (this.getNotificationView() != null) {
            boolean b2 = this.isExpanded();
            label1: {
                if (b2) {
                    break label1;
                }
                if (!this.isExpandedNotificationVisible()) {
                    break label0;
                }
            }
            if (b1) {
                this.collapseExpandedViewInternal(b, b0);
            } else {
                this.animateOutExpandedView(b, this.currentNotification, b0);
            }
        }
    }
    
    private void deleteAllGlances() {
        synchronized(this.lockObj) {
            if (this.deleteAllGlances) {
                this.animateOutExpandedView(true, true);
            }
            /*monexit(a)*/;
        }
    }
    
    private void displayScrollingIndicator(int i) {
        label2: if (this.isExpanded() && this.showOn) {
            com.navdy.hud.app.framework.notifications.NotificationManager$Info a = this.stackCurrentNotification;
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.stackCurrentNotification.notification.supportScroll()) {
                        break label0;
                    }
                }
                this.hideScrollingIndicator();
                break label2;
            }
            com.navdy.hud.app.framework.notifications.INotification a0 = this.stackCurrentNotification.notification;
            this.notifScrollIndicator.setBackgroundColor(this.stackCurrentNotification.notification.getColor());
            this.notifIndicator.getViewTreeObserver().addOnGlobalLayoutListener((android.view.ViewTreeObserver.OnGlobalLayoutListener)new com.navdy.hud.app.framework.notifications.NotificationManager$15(this, i, (com.navdy.hud.app.framework.notifications.IScrollEvent)a0));
        }
    }
    
    private void executeItemInternal() {
        boolean b = sLogger.isLoggable(2);
        if (this.isExpanded()) {
            if (this.stackCurrentNotification == null) {
                if (b) {
                    sLogger.v("executeItemInternal: expanded check for delete:");
                }
                android.view.View a = (this.showOn) ? this.getExpandedViewChild() : this.notifView.getCurrentNotificationViewChild();
                if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(a)) {
                    if (a.getTag(R.id.glance_can_delete) == null) {
                        if (b) {
                            sLogger.v("executeItemInternal: delete all");
                        }
                        this.deleteAllGlances = true;
                        this.clearOperationQueue();
                        com.navdy.hud.app.framework.glance.GlanceHelper.showGlancesDeletedToast();
                        sLogger.v("executeItemInternal: show delete all toast");
                    } else {
                        sLogger.v("executeItemInternal: no glance to delete");
                        this.runQueuedOperation();
                    }
                }
            } else {
                if (b) {
                    sLogger.v(new StringBuilder().append("executeItemInternal: expanded :").append(this.stackCurrentNotification).toString());
                }
                this.clearOperationQueue();
                this.stackCurrentNotification.notification.onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT);
            }
        } else if (this.isExpandedNotificationVisible()) {
            if (b) {
                sLogger.v(new StringBuilder().append("executeItemInternal: expandedNoStack :").append(this.currentNotification).toString());
            }
            if (this.currentNotification == null) {
                this.runQueuedOperation();
            } else {
                this.clearOperationQueue();
                this.currentNotification.notification.onKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.SELECT);
            }
        }
    }
    
    private android.view.View getExpandedNotificationCoverView() {
        android.view.View a = null;
        if (this.expandedNotifCoverView == null) {
            if (this.mainScreen == null) {
                this.getMainScreen();
            }
            if (this.mainScreen != null) {
                this.expandedNotifCoverView = this.mainScreen.getExpandedNotificationCoverView();
            }
            a = this.expandedNotifCoverView;
        } else {
            a = this.expandedNotifCoverView;
        }
        return a;
    }
    
    private android.widget.FrameLayout getExpandedNotificationView() {
        android.widget.FrameLayout a = null;
        if (this.expandedNotifView == null) {
            if (this.mainScreen == null) {
                this.getMainScreen();
            }
            if (this.mainScreen != null) {
                this.expandedNotifView = this.mainScreen.getExpandedNotificationView();
            }
            a = this.expandedNotifView;
        } else {
            a = this.expandedNotifView;
        }
        return a;
    }
    
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info getHigherNotification(com.navdy.hud.app.framework.notifications.NotificationManager$Info a) {
        while(true) {
            com.navdy.hud.app.framework.notifications.NotificationManager$Info a0 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationPriorityMap.higherKey(a);
            if (a0 != null) {
                if (a0.notification instanceof com.navdy.hud.app.framework.phonecall.CallNotification && !a0.notification.isAlive()) {
                    a = a0;
                    continue;
                }
            } else {
                a0 = null;
            }
            return a0;
        }
    }
    
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info getHighestNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager$Info a = null;
        synchronized(this.lockObj) {
            a = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationPriorityMap.lastKey();
            /*monexit(a0)*/;
        }
        return a;
    }
    
    public static com.navdy.hud.app.framework.notifications.NotificationManager getInstance() {
        return sInstance;
    }
    
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info getLowerNotification(com.navdy.hud.app.framework.notifications.NotificationManager$Info a) {
        while(true) {
            com.navdy.hud.app.framework.notifications.NotificationManager$Info a0 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationPriorityMap.lowerKey(a);
            if (a0 != null) {
                if (a0.notification instanceof com.navdy.hud.app.framework.phonecall.CallNotification && !a0.notification.isAlive()) {
                    a = a0;
                    continue;
                }
            } else {
                a0 = null;
            }
            return a0;
        }
    }
    
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info getLowestNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager$Info a = null;
        synchronized(this.lockObj) {
            a = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationPriorityMap.firstKey();
            /*monexit(a0)*/;
        }
        return a;
    }
    
    private com.navdy.hud.app.ui.activity.Main getMainScreen() {
        if (this.mainScreen == null) {
            this.mainScreen = this.uiStateManager.getRootScreen();
        }
        return this.mainScreen;
    }
    
    private int getNonRemovableNotifCount() {
        int i = (this.getNotification("navdy#phone#call#notif") == null) ? 0 : 1;
        if (this.getNotification("navdy#traffic#reroute#notif") != null) {
            i = i + 1;
        }
        return i;
    }
    
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info getNotificationByIndex(int i) {
        com.navdy.hud.app.framework.notifications.NotificationManager$Info a = null;
        label2: synchronized(this.lockObj) {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.notificationPriorityMap.size()) {
                        break label0;
                    }
                }
                /*monexit(a0)*/;
                a = null;
                break label2;
            }
            Object a1 = this.notificationPriorityMap.descendingKeySet().iterator();
            int i0 = 0;
            while(true) {
                if (((java.util.Iterator)a1).hasNext()) {
                    a = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)((java.util.Iterator)a1).next();
                    if (i0 != i) {
                        i0 = i0 + 1;
                        continue;
                    }
                    /*monexit(a0)*/;
                    break;
                } else {
                    /*monexit(a0)*/;
                    a = null;
                    break;
                }
            }
        }
        return a;
    }
    
    private com.navdy.hud.app.framework.notifications.NotificationManager$Info getNotificationFromId(String s) {
        com.navdy.hud.app.framework.notifications.NotificationManager$Info a = null;
        synchronized(this.lockObj) {
            a = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationIdMap.get(s);
            /*monexit(a0)*/;
        }
        return a;
    }
    
    private int getNotificationIndex(com.navdy.hud.app.framework.notifications.NotificationManager$Info a) {
        int i = 0;
        synchronized(this.lockObj) {
            Object a1 = this.notificationPriorityMap.descendingKeySet().iterator();
            i = -1;
            while(true) {
                if (((java.util.Iterator)a1).hasNext()) {
                    i = i + 1;
                    if (((java.util.Iterator)a1).next() != a) {
                        continue;
                    }
                    /*monexit(a0)*/;
                    break;
                } else {
                    /*monexit(a0)*/;
                    i = -1;
                    break;
                }
            }
        }
        return i;
    }
    
    private com.navdy.hud.app.ui.component.carousel.CarouselIndicator getNotificationIndicator() {
        com.navdy.hud.app.ui.component.carousel.CarouselIndicator a = null;
        if (this.notifIndicator == null) {
            if (this.mainScreen == null) {
                this.getMainScreen();
            }
            if (this.mainScreen != null) {
                this.notifIndicator = this.mainScreen.getNotificationIndicator();
                this.notifScrollIndicator = this.mainScreen.getNotificationScrollIndicator();
            }
            a = this.notifIndicator;
        } else {
            a = this.notifIndicator;
        }
        return a;
    }
    
    private com.navdy.hud.app.view.NotificationView getNotificationView() {
        com.navdy.hud.app.view.NotificationView a = null;
        if (this.notifView == null) {
            if (this.mainScreen == null) {
                this.getMainScreen();
            }
            if (this.mainScreen != null) {
                this.notifView = this.mainScreen.getNotificationView();
            }
            a = this.notifView;
        } else {
            a = this.notifView;
        }
        return a;
    }

    private void handleExpandedMove_new(InputManager.CustomKeyEvent event, boolean gesture) {
        sLogger.v("handleExpandedMove key:" + event + " gesture:" + gesture);
        com.navdy.hud.app.framework.notifications.NotificationManager$Info info = null;
        boolean prev = false;
        boolean deleteAll = false;
        boolean runQueuedOperation = false;
        int currentItem = this.notifIndicator.getCurrentItem();
        int indicatorCount = this.notifIndicator.getItemCount();
        try {
            View view;
            switch (event) {
                case LEFT:
                    if (currentItem == 0) {
                        runQueuedOperation = true;
                        if (sLogger.isLoggable(2)) {
                            sLogger.v("handleExpandedMove: cannot go up");
                        }
                        runQueuedOperation();
                        return;
                    }
                    if (currentItem == 1) {
                        deleteAll = true;
                    } else {
                        synchronized (this.lockObj) {
                            if (this.stackCurrentNotification != null) {
                                info = getHigherNotification(this.stackCurrentNotification);
                                if (info == null) {
                                    deleteAll = true;
                                    if (sLogger.isLoggable(2)) {
                                        sLogger.v("handleExpandedMove: cannot go up, no notification left");
                                    }
                                }
                            } else {
                                info = this.lastStackCurrentNotification;
                                this.lastStackCurrentNotification = null;
                                if (info == null) {
                                    sLogger.v("handleExpandedMove: no prev notification");
                                    if (currentItem == indicatorCount - 1) {
                                        info = getLowestNotification();
                                        sLogger.v("handleExpandedMove: got lowest:" + info);
                                    }
                                }
                            }
                        }
                    }
                    prev = true;
                    break;
                case RIGHT:
                    if (currentItem == indicatorCount - 1) {
                        if (gesture) {
                            if (this.showOn) {
                                view = getExpandedViewChild();
                            } else {
                                view = this.notifView.getCurrentNotificationViewChild();
                            }
                            if (view.getTag(R.id.glance_can_delete) != null) {
                                sLogger.v("no glance to delete:right");
                                runQueuedOperation = true;
                            } else {
                                this.deleteAllGlances = true;
                                clearOperationQueue();
                                GlanceHelper.showGlancesDeletedToast();
                                if (sLogger.isLoggable(2)) {
                                    sLogger.v("handleExpandedMove: delete all glances");
                                }
                            }
                        } else {
                            if (sLogger.isLoggable(2)) {
                                sLogger.v("handleExpandedMove: cannot go down");
                            }
                            runQueuedOperation = true;
                        }
                        if (runQueuedOperation) {
                            runQueuedOperation();
                            return;
                        }
                        return;
                    } else if (currentItem == indicatorCount - 2) {
                        deleteAll = true;
                    } else {
                        synchronized (this.lockObj) {
                            if (this.stackCurrentNotification != null) {
                                info = getLowerNotification(this.stackCurrentNotification);
                            } else {
                                info = this.lastStackCurrentNotification;
                                this.lastStackCurrentNotification = null;
                                if (info == null) {
                                    sLogger.v("handleExpandedMove: no next notification");
                                    if (currentItem == 0) {
                                        info = getHighestNotification();
                                        sLogger.v("handleExpandedMove: got highest:" + info);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case SELECT:
                    if (this.stackCurrentNotification != null) {
                        this.stackCurrentNotification.notification.onKey(event);
                        return;
                    }
                    if (this.showOn) {
                        view = getExpandedViewChild();
                    } else {
                        view = this.notifView.getCurrentNotificationViewChild();
                    }
                    if (GlanceHelper.isDeleteAllView(view)) {
                        if (view.getTag(R.id.glance_can_delete) != null) {
                            sLogger.v("no glance to delete:select");
                            runQueuedOperation();
                            return;
                        }
                        this.deleteAllGlances = true;
                        GlanceHelper.showGlancesDeletedToast();
                        return;
                    } else {
                        return;
                    }
            }

            Context uiContext = this.notifView.getContext();
            final boolean previous = prev;
            if (deleteAll) {
                synchronized (this.lockObj) {
                    View s;
                    View l;
                    int c;
                    final com.navdy.hud.app.framework.notifications.NotificationManager$Info notifInfo = info;
                    if (info != null) {
                        if (sLogger.isLoggable(2)) {
                            sLogger.v("handleExpandedMove: going to " + notifInfo.notification.getId());
                        }
                        s = notifInfo.notification.getView(uiContext);
                        if (this.showOn) {
                            l = notifInfo.notification.getExpandedView(uiContext, null);
                        } else {
                            l = null;
                        }
                        notifInfo.startCalled = true;
                        notifInfo.notification.onStart(this.notificationController);
                        c = notifInfo.notification.getColor();
                    } else {
                        ImageView imageView;
                        if (sLogger.isLoggable(2)) {
                            sLogger.v("handleExpandedMove: going to delete all");
                        }
                        if (this.showOn) {
                            s = GlanceViewCache.getView(GlanceViewCache.ViewType.SMALL_IMAGE, uiContext);
                            s.setTag(R.id.glance_view_type, GlanceViewCache.ViewType.SMALL_IMAGE);
                            imageView = (ImageView) s.findViewById(R.id.imageView);
                        } else {
                            s = GlanceViewCache.getView(GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE, uiContext);
                            s.setTag(R.id.glance_view_type, GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE);
                            imageView = (ImageView) s.findViewById(R.id.mainImage);
                            TextView mainTitle = (TextView) s.findViewById(R.id.mainTitle);
                            mainTitle.setText(GlanceConstants.glancesDismissAll);
                            mainTitle.setAlpha(1.0f);
                            mainTitle.setVisibility(View.VISIBLE);
                            TextView subTitle = (TextView) s.findViewById(R.id.subTitle);
                            int count = getNotificationCount() - getNonRemovableNotifCount();
                            if (count == 0) {
                                s.setTag(R.id.glance_can_delete, 1);
                            } else {
                                s.setTag(R.id.glance_can_delete, null);
                            }
                            subTitle.setText(this.resources.getString(R.string.count, new Object[]{Integer.valueOf(count)}));
                            subTitle.setAlpha(1.0f);
                            subTitle.setVisibility(View.VISIBLE);
                            View choiceLayout = s.findViewById(R.id.choiceLayout);
                            if (choiceLayout != null) {
                                choiceLayout.setVisibility(View.INVISIBLE);
                            }
                        }
                        s.setTag(GlanceConstants.DELETE_ALL_VIEW_TAG);
                        imageView.setImageResource(R.drawable.icon_dismiss_all_glances);
                        if (this.showOn) {
                            l = GlanceViewCache.getView(GlanceViewCache.ViewType.BIG_TEXT, uiContext);
                            l.setTag(GlanceConstants.DELETE_ALL_VIEW_TAG);
                            updateDeleteAllGlanceCount((TextView) l.findViewById(R.id.textView), l);
                        } else {
                            l = null;
                        }
                        c = GlanceConstants.colorDeleteAll;
                    }
                    final View small = s;
                    final View large = l;
                    final int color = c;
                    if (this.stackCurrentNotification != null) {
                        AnimatorSet set = this.stackCurrentNotification.notification.getViewSwitchAnimation(false);
                        if (set != null) {
                            set.addListener(new DefaultAnimationListener() {
                                public void onAnimationEnd(Animator animation) {
                                    NotificationManager.this.viewSwitchAnimation(small, large, notifInfo, previous, color);
                                }
                            });
                            set.setDuration(100);
                            set.start();
                        } else {
                            viewSwitchAnimation(small, large, notifInfo, previous, color);
                        }
                    } else {
                        viewSwitchAnimation(small, large, notifInfo, previous, color);
                    }
                }
            } else {
                if (sLogger.isLoggable(2)) {
                    sLogger.v("handleExpandedMove: no-op");
                }
                runQueuedOperation = true;
            }
            if (runQueuedOperation) {
                runQueuedOperation();
                return;
            }
            return;

        } catch (Throwable th) {
            if (runQueuedOperation) {
                runQueuedOperation();
            }
        }
        if (runQueuedOperation) {
            runQueuedOperation();
        }
    }

    private void handleExpandedMove(com.navdy.hud.app.manager.InputManager.CustomKeyEvent keyEvent, boolean gesture) {
        sLogger.v("handleExpandedMove key:" + keyEvent + " gesture:" + gesture);
        int currentItem = this.notifIndicator.getCurrentItem();
        int itemCount = this.notifIndicator.getItemCount();
        int[] a0 = com.navdy.hud.app.framework.notifications.NotificationManager$16.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent;
        label0: {
            boolean b0 = false;
            label9: {
                boolean b1 = false;
                label1: {
                    label8: {
                        boolean doRunQueuedOperation = false;
                        label5: try {
                            com.navdy.hud.app.framework.notifications.NotificationManager$Info info = null;
                            boolean b3 = false;
                            boolean b4 = false;
                            doRunQueuedOperation = false;
                            label10: {
                                com.navdy.hud.app.framework.notifications.NotificationManager$Info a2 = null;
                                android.view.View a3 = null;
                                label14: {
                                    label12: {
                                        {
                                            label13:
                                            {
                                                switch (keyEvent) {
                                                    case SELECT: {
                                                        doRunQueuedOperation = false;
                                                        a2 = this.stackCurrentNotification;
                                                        break label14;
                                                    }
                                                    case RIGHT: {
                                                        if (currentItem == itemCount - 1) {
                                                            break label13;
                                                        }
                                                        if (currentItem != itemCount - 2) {
                                                            doRunQueuedOperation = false;
                                                            synchronized (this.lockObj) {
                                                                if (this.stackCurrentNotification == null) {
                                                                    info = this.lastStackCurrentNotification;
                                                                    this.lastStackCurrentNotification = null;
                                                                    if (info == null) {
                                                                        sLogger.v("handleExpandedMove: no next notification");
                                                                        if (currentItem == 0) {
                                                                            info = this.getHighestNotification();
                                                                            sLogger.v("handleExpandedMove: got highest:" + info);
                                                                        }
                                                                    }
                                                                } else {
                                                                    info = this.getLowerNotification(this.stackCurrentNotification);
                                                                }
                                                                /*monexit(a6)*/
                                                                ;
                                                            }
                                                            b3 = false;
                                                            b4 = false;
                                                            break label10;
                                                        } else {
                                                            info = null;
                                                            b3 = false;
                                                            b4 = true;
                                                            break label10;
                                                        }
                                                    }
                                                    case LEFT: {
                                                        if (currentItem == 0) {
                                                            break label12;
                                                        }
                                                        if (currentItem != 1) {
                                                            doRunQueuedOperation = false;
                                                            synchronized (this.lockObj) {
                                                                if (this.stackCurrentNotification == null) {
                                                                    info = this.lastStackCurrentNotification;
                                                                    this.lastStackCurrentNotification = null;
                                                                    if (info != null) {
                                                                        b4 = false;
                                                                    } else {
                                                                        sLogger.v("handleExpandedMove: no prev notification");
                                                                        if (currentItem != itemCount - 1) {
                                                                            b4 = false;
                                                                        } else {
                                                                            info = this.getLowestNotification();
                                                                            sLogger.v("handleExpandedMove: got lowest:" + info);
                                                                            b4 = false;
                                                                        }
                                                                    }
                                                                } else {
                                                                    info = this.getHigherNotification(this.stackCurrentNotification);
                                                                    if (info != null) {
                                                                        b4 = false;
                                                                    } else if (sLogger.isLoggable(2)) {
                                                                        sLogger.v("handleExpandedMove: cannot go up, no notification left");
                                                                        b4 = true;
                                                                    } else {
                                                                        b4 = true;
                                                                    }
                                                                }
                                                                /*monexit(a4)*/
                                                                ;
                                                            }
                                                        } else {
                                                            info = null;
                                                            b4 = true;
                                                        }
                                                        b3 = true;
                                                        break label10;
                                                    }
                                                    default: {
                                                        info = null;
                                                        b3 = false;
                                                        b4 = false;
                                                        break label10;
                                                    }
                                                }
                                            }
                                            if (gesture) {
                                                View a12 = null;
                                                doRunQueuedOperation = false;
                                                if (this.showOn) {
                                                    doRunQueuedOperation = false;
                                                    a12 = this.getExpandedViewChild();
                                                } else {
                                                    doRunQueuedOperation = false;
                                                    a12 = this.notifView.getCurrentNotificationViewChild();
                                                }
                                                doRunQueuedOperation = false;
                                                if (a12.getTag(R.id.glance_can_delete) == null) {
                                                    doRunQueuedOperation = false;
                                                    this.deleteAllGlances = true;
                                                    this.clearOperationQueue();
                                                    GlanceHelper.showGlancesDeletedToast();
                                                    if (sLogger.isLoggable(2)) {
                                                        doRunQueuedOperation = false;
                                                        sLogger.v("handleExpandedMove: delete all glances");
                                                        b0 = false;
                                                        break label9;
                                                    } else {
                                                        b0 = false;
                                                        break label9;
                                                    }
                                                } else {
                                                    doRunQueuedOperation = false;
                                                    sLogger.v("no glance to delete:right");
                                                    b0 = true;
                                                    break label9;
                                                }
                                            } else {
                                                doRunQueuedOperation = false;
                                                if (sLogger.isLoggable(2)) {
                                                    doRunQueuedOperation = false;
                                                    sLogger.v("handleExpandedMove: cannot go down");
                                                }
                                                b0 = true;
                                                break label9;
                                            }
                                        }

                                    }
                                    doRunQueuedOperation = true;
                                    if (!sLogger.isLoggable(2)) {
                                        break label8;
                                    }
                                    doRunQueuedOperation = true;
                                    sLogger.v("handleExpandedMove: cannot go up");
                                    break label8;
                                }
                                label7: {
                                    if (a2 == null) {
                                        break label7;
                                    }
                                    doRunQueuedOperation = false;
                                    this.stackCurrentNotification.notification.onKey(keyEvent);
                                    break label0;
                                }
                                doRunQueuedOperation = false;
                                if (this.showOn) {
                                    doRunQueuedOperation = false;
                                    a3 = this.getExpandedViewChild();
                                } else {
                                    doRunQueuedOperation = false;
                                    a3 = this.notifView.getCurrentNotificationViewChild();
                                }
                                doRunQueuedOperation = false;
                                boolean b5 = com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(a3);
                                label6: {
                                    if (b5) {
                                        break label6;
                                    }
                                    break label0;
                                }
                                doRunQueuedOperation = false;
                                Object a21 = a3.getTag(R.id.glance_can_delete);
                                label4: {
                                    if (a21 == null) {
                                        break label4;
                                    }
                                    doRunQueuedOperation = false;
                                    sLogger.v("no glance to delete:select");
                                    break label5;
                                }
                                doRunQueuedOperation = false;
                                this.deleteAllGlances = true;
                                com.navdy.hud.app.framework.glance.GlanceHelper.showGlancesDeletedToast();
                                break label0;
                            }
                            doRunQueuedOperation = false;
                            android.content.Context a23 = this.notifView.getContext();
                            label2: {
                                label3: {
                                    if (info != null) {
                                        break label3;
                                    }
                                    if (!b4) {
                                        break label2;
                                    }
                                }
                                doRunQueuedOperation = false;
                                synchronized(this.lockObj) {
                                    android.view.View a25 = null;
                                    android.view.View a26 = null;
                                    int i2 = 0;
                                    if (info == null) {
                                        android.widget.ImageView a27 = null;
                                        if (sLogger.isLoggable(2)) {
                                            sLogger.v("handleExpandedMove: going to delete all");
                                        }
                                        if (this.showOn) {
                                            a25 = com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_IMAGE, a23);
                                            a25.setTag(R.id.glance_view_type, com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_IMAGE);
                                            a27 = (android.widget.ImageView)a25.findViewById(R.id.imageView);
                                        } else {
                                            a25 = com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE, a23);
                                            a25.setTag(R.id.glance_view_type, com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.SMALL_GLANCE_MESSAGE);
                                            a27 = (android.widget.ImageView)a25.findViewById(R.id.mainImage);
                                            android.widget.TextView a28 = (android.widget.TextView)a25.findViewById(R.id.mainTitle);
                                            a28.setText((CharSequence)com.navdy.hud.app.framework.glance.GlanceConstants.glancesDismissAll);
                                            a28.setAlpha(1f);
                                            a28.setVisibility(View.VISIBLE);
                                            android.widget.TextView a29 = (android.widget.TextView)a25.findViewById(R.id.subTitle);
                                            int i3 = this.getNotificationCount() - this.getNonRemovableNotifCount();
                                            if (i3 != 0) {
                                                a25.setTag(R.id.glance_can_delete, null);
                                            } else {
                                                a25.setTag(R.id.glance_can_delete, 1);
                                            }
                                            android.content.res.Resources a30 = this.resources;
                                            a29.setText(a30.getString(R.string.count, i3));
                                            a29.setAlpha(1f);
                                            a29.setVisibility(View.VISIBLE);
                                            android.view.View a32 = a25.findViewById(R.id.choiceLayout);
                                            if (a32 != null) {
                                                a32.setVisibility(View.INVISIBLE);
                                            }
                                        }
                                        a25.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.DELETE_ALL_VIEW_TAG);
                                        a27.setImageResource(R.drawable.icon_dismiss_all_glances);
                                        if (this.showOn) {
                                            a26 = com.navdy.hud.app.framework.glance.GlanceViewCache.getView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_TEXT, a23);
                                            a26.setTag(com.navdy.hud.app.framework.glance.GlanceConstants.DELETE_ALL_VIEW_TAG);
                                            this.updateDeleteAllGlanceCount((android.widget.TextView)a26.findViewById(R.id.textView), a26);
                                        } else {
                                            a26 = null;
                                        }
                                        i2 = com.navdy.hud.app.framework.glance.GlanceConstants.colorDeleteAll;
                                    } else {
                                        if (sLogger.isLoggable(2)) {
                                            sLogger.v("handleExpandedMove: going to " + info.notification.getId());
                                        }
                                        a25 = info.notification.getView(a23);
                                        a26 = (this.showOn) ? info.notification.getExpandedView(a23, null) : null;
                                        info.startCalled = true;
                                        info.notification.onStart(this.notificationController);
                                        i2 = info.notification.getColor();
                                    }
                                    if (this.stackCurrentNotification == null) {
                                        this.viewSwitchAnimation(a25, a26, info, b3, i2);
                                    } else {
                                        android.animation.AnimatorSet animation = this.stackCurrentNotification.notification.getViewSwitchAnimation(false);
                                        if (animation == null) {
                                            this.viewSwitchAnimation(a25, a26, info, b3, i2);
                                        } else {
                                            animation.addListener(new NotificationManager$14(this, a25, a26, info, b3, i2));
                                            animation.setDuration(100L);
                                            animation.start();
                                        }
                                    }
                                    /*monexit(a24)*/;
                                    b1 = false;
                                    break label1;
                                }
                            }
                            doRunQueuedOperation = false;
                            if (sLogger.isLoggable(2)) {
                                doRunQueuedOperation = false;
                                sLogger.v("handleExpandedMove: no-op");
                            }
                            b1 = true;
                            break label1;
                        } catch(Throwable a40) {
                            if (doRunQueuedOperation) {
                                this.runQueuedOperation();
                            }
                            sLogger.e("Error in NotificationManager.handleExpandedMove", a40);
//                            throw a40;
                        }
                        this.runQueuedOperation();
                        break label0;
                    }
                    this.runQueuedOperation();
                    break label0;
                }
                if (!b1) {
                    break label0;
                }
                this.runQueuedOperation();
                break label0;
            }
            if (b0) {
                this.runQueuedOperation();
            }
        }
    }
    
    private void handleRemoveNotificationInExpandedMode(String s, boolean b) {
        if (this.isExpanded() && !this.deleteAllGlances) {
            this.runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.REMOVE_NOTIFICATION, false, false, false, false, s, b);
        }
    }
    
    private void handleRemoveNotificationInExpandedModeInternal(String s, boolean b) {
        if (this.isNotifCurrent(s)) {
            sLogger.v(new StringBuilder().append("notif-end: is current:").append(s).toString());
            if (b) {
                this.collapseNotificationInternal(false, false, true);
            }
        } else if (this.getNotificationCount() != 1) {
            sLogger.v(new StringBuilder().append("remove-end: not current, remove:").append(s).toString());
            this.removeNotificationfromStack(this.getNotificationFromId(s), true);
            if (this.lastStackCurrentNotification != null && android.text.TextUtils.equals((CharSequence)this.lastStackCurrentNotification.notification.getId(), (CharSequence)s)) {
                if (this.stackCurrentNotification != null) {
                    this.lastStackCurrentNotification = null;
                } else {
                    this.lastStackCurrentNotification = this.getLowerNotification(this.lastStackCurrentNotification);
                    sLogger.v(new StringBuilder().append("after remove notif is:").append((this.lastStackCurrentNotification != null) ? this.lastStackCurrentNotification.notification.getId() : "null").toString());
                }
            }
            if (this.currentNotification != null && android.text.TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)s)) {
                this.currentNotification = this.stackCurrentNotification;
                sLogger.v(new StringBuilder().append("after remove current notif set:").append((this.currentNotification != null) ? this.currentNotification.notification.getId() : "null").toString());
            }
            this.postNotificationChange();
            this.updateExpandedIndicatorInternal();
        } else {
            sLogger.v("remove-end: no more glance");
            this.collapseExpandedViewInternal(true, true);
        }
    }
    
    private void hideNotificationInternal() {
        com.navdy.hud.app.view.NotificationView a = this.getNotificationView();
        if (a.border.isVisible()) {
            sLogger.v("hideNotification:border visible");
            a.border.stopTimeout(true, (Runnable)new com.navdy.hud.app.framework.notifications.NotificationManager$8(this));
        } else {
            sLogger.v("hideNotification:screen_back");
            this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_BACK, (android.os.Bundle)null, null, true));
        }
    }
    
    private void hideScrollingIndicator() {
        if (this.notifScrollIndicator != null) {
            ((android.view.View)this.notifScrollIndicator.getParent()).setVisibility(View.GONE);
        }
    }
    
    private boolean isDeleteAllIndex(int i, int i0) {
        boolean b = false;
        label2: {
            label0: {
                label1: {
                    if (i == 0) {
                        break label1;
                    }
                    if (i != i0 - 1) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    private boolean isNotifCurrent(String s) {
        boolean b = false;
        boolean b0 = this.notificationController.isExpandedWithStack();
        label0: {
            label3: {
                label4: {
                    if (!b0) {
                        break label4;
                    }
                    if (this.expandedWithStack) {
                        break label3;
                    }
                }
                com.navdy.hud.app.framework.notifications.NotificationManager$Info a = this.currentNotification;
                label2: {
                    if (a == null) {
                        break label2;
                    }
                    if (!android.text.TextUtils.equals((CharSequence)s, (CharSequence)this.currentNotification.notification.getId())) {
                        break label2;
                    }
                    if (this.currentNotification.startCalled) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
                break label0;
            }
            com.navdy.hud.app.framework.notifications.NotificationManager$Info a0 = this.stackCurrentNotification;
            label1: {
                if (a0 == null) {
                    break label1;
                }
                if (!android.text.TextUtils.equals((CharSequence)s, (CharSequence)this.stackCurrentNotification.notification.getId())) {
                    break label1;
                }
                if (this.stackCurrentNotification.startCalled) {
                    b = true;
                    break label0;
                }
            }
            b = false;
        }
        return b;
    }
    
    private boolean isPhoneNotifPresentAndNotAlive() {
        boolean b = false;
        com.navdy.hud.app.framework.phonecall.CallNotification a = (com.navdy.hud.app.framework.phonecall.CallNotification)this.getNotification("navdy#phone#call#notif");
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (!a.isAlive()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private boolean isScreenHighPriority(com.navdy.service.library.events.ui.Screen a) {
        return com.navdy.hud.app.framework.notifications.NotificationManager$16.$SwitchMap$com$navdy$service$library$events$ui$Screen[a.ordinal()] != 0;
    }
    
    private void moveNextInternal(boolean b) {
        boolean b0 = sLogger.isLoggable(2);
        if (b0) {
            sLogger.v(new StringBuilder().append("moveNextInternal:").append(b).toString());
        }
        if (this.isExpanded()) {
            this.handleExpandedMove(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT, b);
        } else if (this.isExpandedNotificationVisible()) {
            int i = this.notifIndicator.getCurrentItem();
            if (i != this.notifIndicator.getItemCount() - 1) {
                int i0 = i + 1;
                if (b0) {
                    sLogger.v(new StringBuilder().append("moveNextInternal:going to ").append(i0).toString());
                }
                this.viewSwitchAnimation((android.view.View)null, this.currentNotification.notification.getExpandedView(this.notifView.getContext(), Integer.valueOf(i0)), (com.navdy.hud.app.framework.notifications.NotificationManager$Info)null, false, this.currentNotification.notification.getExpandedViewIndicatorColor());
            } else {
                this.runQueuedOperation();
                if (b0) {
                    sLogger.v("moveNextInternal:cannot go next");
                }
            }
        }
    }
    
    private void movePrevInternal(boolean b) {
        boolean b0 = sLogger.isLoggable(2);
        if (b0) {
            sLogger.v(new StringBuilder().append("movePrevInternal:").append(b).toString());
        }
        if (this.isExpanded()) {
            this.handleExpandedMove(com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT, b);
        } else if (this.isExpandedNotificationVisible()) {
            int i = this.notifIndicator.getCurrentItem();
            this.notifIndicator.getItemCount();
            if (i != 0) {
                int i0 = i + -1;
                if (b0) {
                    sLogger.v(new StringBuilder().append("movePrevInternal: going to ").append(i0).toString());
                }
                this.viewSwitchAnimation((android.view.View)null, this.currentNotification.notification.getExpandedView(this.notifView.getContext(), Integer.valueOf(i0)), (com.navdy.hud.app.framework.notifications.NotificationManager$Info)null, true, com.navdy.hud.app.framework.glance.GlanceConstants.colorWhite);
            } else {
                this.runQueuedOperation();
                if (b0) {
                    sLogger.v("movePrevInternal: cannot go prev");
                }
            }
        }
    }
    
    private void removeAllNotification() {
        this.expandedWithStack = false;
        synchronized(this.lockObj) {
            if (this.currentNotification != null && this.currentNotification.startCalled && com.navdy.hud.app.framework.notifications.NotificationHelper.isNotificationRemovable(this.currentNotification.notification.getId())) {
                sLogger.v(new StringBuilder().append("removeAllNotification calling stop: ").append(this.currentNotification.notification.getId()).toString());
                this.currentNotification.startCalled = false;
                this.currentNotification.notification.onStop();
            }
            int i = this.notificationIdMap.size();
            sLogger.v(new StringBuilder().append("removeAllNotification:").append(i).toString());
            com.navdy.hud.app.framework.notifications.NotificationManager$Info a0 = null;
            if (i > 0) {
                java.util.Iterator a1 = this.notificationIdMap.keySet().iterator();
                a0 = null;
                Object a2 = a1;
                while(true) {
                    boolean b = ((java.util.Iterator)a2).hasNext();
                    com.navdy.hud.app.framework.notifications.NotificationManager$Info a3 = a0;
                    if (!b) {
                        break;
                    }
                    String s = (String)((java.util.Iterator)a2).next();
                    a0 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationIdMap.get(s);
                    if (com.navdy.hud.app.framework.notifications.NotificationHelper.isNotificationRemovable(s)) {
                        this.notificationPriorityMap.remove(a0);
                        ((java.util.Iterator)a2).remove();
                        sLogger.v(new StringBuilder().append("removeAllNotification removed:").append(s).toString());
                        a0 = a3;
                    } else {
                        sLogger.v(new StringBuilder().append("removeAllNotification left:").append(s).toString());
                    }
                }
            }
            this.stackCurrentNotification = null;
            this.lastStackCurrentNotification = null;
            this.currentNotification = a0;
            this.postNotificationChange();
            /*monexit(a)*/;
        }
    }
    
    private void removeNotificationfromStack(com.navdy.hud.app.framework.notifications.NotificationManager$Info a, boolean b) {
        if (a != null) {
            if (this.notificationIdMap.remove(a.notification.getId()) == null) {
                sLogger.v("removeNotification, not found in id map");
            }
            if (this.notificationPriorityMap.remove(a) == null) {
                sLogger.v("removeNotification, not found in priority map");
            }
            if (a.startCalled) {
                a.startCalled = false;
                a.notification.onStop();
            }
            com.navdy.hud.app.framework.notifications.NotificationManager$Info a0 = this.stagedNotification;
            String s = null;
            if (a0 != null) {
                s = this.stagedNotification.notification.getId();
                if (android.text.TextUtils.equals((CharSequence)this.stagedNotification.notification.getId(), (CharSequence)a.notification.getId())) {
                    this.stagedNotification = null;
                    sLogger.v("cancelling staged notification, getting removed");
                }
            }
            this.postNotificationChange();
            sLogger.v(new StringBuilder().append("removed notification from stack [").append(a.notification.getId()).append("] staged[").append(s).append("]").toString());
            if (!b) {
                this.updateExpandedIndicator();
            }
        }
    }
    
    private void restoreStack() {
        label4: synchronized(this.lockObj) {
            label3: {
                com.navdy.service.library.device.NavdyDeviceId a0 = null;
                int i = this.notificationSavedList.size();
                sLogger.v(new StringBuilder().append("restoreStack:").append(i).toString());
                label0: {
                    label1: {
                        label2: {
                            if (i <= 0) {
                                break label2;
                            }
                            a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
                            if (a0 == null) {
                                break label1;
                            }
                            if (!a0.equals(this.notificationSavedListDeviceId)) {
                                break label0;
                            }
                            Object a1 = this.notificationSavedList.iterator();
                            while(((java.util.Iterator)a1).hasNext()) {
                                com.navdy.hud.app.framework.notifications.NotificationManager$Info a2 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)((java.util.Iterator)a1).next();
                                String s = a2.notification.getId();
                                sLogger.w(new StringBuilder().append("restoreStack restored:").append(s).toString());
                                this.notificationIdMap.put(s, a2);
                                this.notificationPriorityMap.put(a2, a2);
                            }
                            com.navdy.hud.app.framework.glance.GlanceHandler.getInstance().restoreState();
                        }
                        /*monexit(a)*/;
                        break label3;
                    }
                    sLogger.w("restoreStack no device id");
                    this.notificationSavedList.clear();
                    /*monexit(a)*/;
                    break label4;
                }
                sLogger.w(new StringBuilder().append("restoreStack device id does not match current[").append(a0).append("] saved[").append(this.notificationSavedListDeviceId).append("]").toString());
                this.notificationSavedList.clear();
                com.navdy.hud.app.framework.glance.GlanceHandler.getInstance().clearState();
                /*monexit(a)*/;
                break label4;
            }
            this.uiStateManager.enableNotificationColor(true);
        }
    }
    
    private void runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation a, boolean b, boolean b0, boolean b1, boolean b2, String s, boolean b3) {
        label0: {
            label2: {
                label3: {
                    if (b) {
                        break label3;
                    }
                    if (this.operationRunning) {
                        break label2;
                    }
                }
                sLogger.v(new StringBuilder().append("run operation=").append(a).toString());
                this.operationRunning = true;
                switch(com.navdy.hud.app.framework.notifications.NotificationManager$16.$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation[a.ordinal()]) {
                    case 6: {
                        this.handleRemoveNotificationInExpandedModeInternal(s, b3);
                        break label0;
                    }
                    case 5: {
                        this.updateExpandedIndicatorInternal();
                        break label0;
                    }
                    case 4: {
                        this.collapseExpandedViewInternal(b0, b1);
                        break label0;
                    }
                    case 3: {
                        this.executeItemInternal();
                        if (!this.deleteAllGlances) {
                            break label0;
                        }
                        com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Delete_All", (com.navdy.hud.app.framework.notifications.INotification)null, b2 ? "swipe" : "dial");
                        break label0;
                    }
                    case 2: {
                        this.movePrevInternal(b2);
                        com.navdy.hud.app.analytics.AnalyticsSupport.setGlanceNavigationSource(b2 ? "swipe" : "dial");
                        break label0;
                    }
                    case 1: {
                        this.moveNextInternal(b2);
                        if (this.deleteAllGlances) {
                            com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Delete_All", (com.navdy.hud.app.framework.notifications.INotification)null, b2 ? "swipe" : "dial");
                            break label0;
                        } else {
                            com.navdy.hud.app.analytics.AnalyticsSupport.setGlanceNavigationSource(b2 ? "swipe" : "dial");
                            break label0;
                        }
                    }
                    default: {
                        break label0;
                    }
                }
            }
            int i = this.operationQueue.size();
            label1: {
                if (i < 3) {
                    break label1;
                }
                switch(com.navdy.hud.app.framework.notifications.NotificationManager$16.$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationAnimator$Operation[a.ordinal()]) {
                    default: {
                        break;
                    }
                    case 1: case 2: {
                        break label0;
                    }
                }
            }
            this.operationQueue.add(new com.navdy.hud.app.framework.notifications.NotificationAnimator.OperationInfo(a, b0, b1, b2, s, b3));
        }
    }
    
    private void runQueuedOperation() {
        if (this.operationQueue.size() <= 0) {
            sLogger.v("operationRunning=false");
            this.operationRunning = false;
        } else {
            com.navdy.hud.app.framework.notifications.NotificationAnimator.OperationInfo a = (com.navdy.hud.app.framework.notifications.NotificationAnimator.OperationInfo)this.operationQueue.remove();
            this.handler.post((Runnable)new com.navdy.hud.app.framework.notifications.NotificationManager$11(this, a));
        }
    }
    
    private void saveStack() {
        synchronized(this.lockObj) {
            com.navdy.hud.app.framework.toast.ToastManager a0 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            a0.dismissCurrentToast("incomingcall#toast");
            a0.clearPendingToast("incomingcall#toast");
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager().clearCallStack();
            this.notificationSavedList.clear();
            int i = this.notificationIdMap.size();
            sLogger.v(new StringBuilder().append("saveStack:").append(i).toString());
            if (i > 0) {
                Object a1 = this.notificationIdMap.keySet().iterator();
                while(((java.util.Iterator)a1).hasNext()) {
                    String s = (String)((java.util.Iterator)a1).next();
                    com.navdy.hud.app.framework.notifications.NotificationManager$Info a2 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationIdMap.get(s);
                    if (this.NOTIFS_REMOVED_ON_DISCONNECT.contains(s)) {
                        this.notificationPriorityMap.remove(a2);
                        ((java.util.Iterator)a1).remove();
                        sLogger.v(new StringBuilder().append("saveStack removed:").append(s).toString());
                    } else if (com.navdy.hud.app.framework.notifications.NotificationType.GLANCE != a2.notification.getType()) {
                        sLogger.v(new StringBuilder().append("saveStack left:").append(s).toString());
                    } else {
                        sLogger.v(new StringBuilder().append("saveStack saved:").append(s).toString());
                        this.notificationSavedList.add(a2);
                        ((java.util.Iterator)a1).remove();
                        this.notificationPriorityMap.remove(a2);
                    }
                }
            }
            this.currentNotification = null;
            this.stagedNotification = null;
            com.navdy.hud.app.framework.glance.GlanceHandler.getInstance().saveState();
            /*monexit(a)*/;
        }
        this.uiStateManager.enableNotificationColor(true);
    }
    
    private void setNotifScrollIndicatorXY(android.graphics.RectF a, com.navdy.hud.app.framework.notifications.IScrollEvent a0) {
        a0.setListener(this.scrollProgress);
        this.notifScrollIndicator.setCurrentItem(1);
        android.view.View a1 = (android.view.View)this.notifScrollIndicator.getParent();
        a1.setX(this.notifIndicator.getX() + (float)com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorLeftPadding);
        a1.setY(this.notifIndicator.getY() + a.top + (float)(com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorCircleSize / 2) - (float)(com.navdy.hud.app.framework.glance.GlanceConstants.scrollingIndicatorHeight / 2));
        a1.setVisibility(View.VISIBLE);
    }
    
    private void showNotification(boolean b) {
        sLogger.v(new StringBuilder().append("show notification isExpanded:").append(this.isExpanded).append(" anim:").append(this.isAnimating).append(" ignore:").append(b).toString());
        if (!this.isExpanded && !this.isAnimating) {
            this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_NOTIFICATION, (android.os.Bundle)null, null, b));
        }
    }
    
    private void startScrollThresholdTimer() {
        this.scrollState = com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.NOT_HANDLED;
        this.handler.removeCallbacks(this.ignoreScrollRunnable);
        this.handler.postDelayed(this.ignoreScrollRunnable, 750L);
    }
    
    private void updateDeleteAllGlanceCount(android.widget.TextView a, android.view.View a0) {
        int i = this.getNotificationCount() - this.getNonRemovableNotifCount();
        if (i != 0) {
            if (this.showOn) {
                android.content.res.Resources a1 = this.resources;
                Object[] a2 = new Object[1];
                a2[0] = Integer.valueOf(i);
                a.setText((CharSequence)a1.getString(R.string.glances_dismiss_all, a2));
            } else {
                android.content.res.Resources a3 = this.resources;
                Object[] a4 = new Object[1];
                a4[0] = Integer.valueOf(i);
                a.setText((CharSequence)a3.getString(R.string.count, a4));
            }
            a0.setTag(R.id.glance_can_delete, null);
        } else {
            a.setText((CharSequence)com.navdy.hud.app.framework.glance.GlanceConstants.glancesCannotDelete);
            a0.setTag(R.id.glance_can_delete, Integer.valueOf(1));
        }
    }
    
    private void updateExpandedIndicator() {
        if (this.isExpanded() && !this.deleteAllGlances) {
            if (this.isAnimating()) {
                sLogger.v("updateExpandedIndicator animating no-op");
            } else {
                this.runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.UPDATE_INDICATOR, false, false, false, false, (String)null, false);
            }
        }
    }
    
    private void updateExpandedIndicatorInternal() {
        android.widget.FrameLayout a = this.getExpandedNotificationView();
        label3: if (this.isExpanded() && ((android.view.View)a).getTag() != null) {
            android.view.View a0 = null;
            int i = this.notifIndicator.getItemCount();
            int i0 = this.notifIndicator.getCurrentItem();
            boolean b = this.isDeleteAllIndex(i0, i);
            int i1 = this.getNotificationCount();
            if (this.isPhoneNotifPresentAndNotAlive()) {
                i1 = i1 - 1;
            }
            int i2 = i1 + 2;
            int i3 = com.navdy.hud.app.framework.glance.GlanceConstants.colorDeleteAll;
            if (i0 >= i2) {
                i0 = i2 - 1;
            }
            if (this.stackCurrentNotification != null) {
                i0 = this.getNotificationIndex(this.stackCurrentNotification);
                if (i0 != -1) {
                    i3 = this.stackCurrentNotification.notification.getColor();
                    i0 = i0 + 1;
                }
            }
            boolean b0 = this.isDeleteAllIndex(i0, i2);
            label1: {
                label2: {
                    if (!b0) {
                        break label2;
                    }
                    i3 = com.navdy.hud.app.framework.glance.GlanceConstants.colorDeleteAll;
                    break label1;
                }
                if (!b) {
                    break label1;
                }
                com.navdy.hud.app.framework.notifications.NotificationManager$Info a1 = this.getNotificationByIndex(i1 - 1);
                label0: {
                    if (a1 != null) {
                        break label0;
                    }
                    i3 = com.navdy.hud.app.framework.glance.GlanceConstants.colorDeleteAll;
                    i0 = i2 - 1;
                    break label1;
                }
                this.lastStackCurrentNotification = a1;
                this.updateExpandedIndicator(i2, i2 - 1, a1.notification.getColor());
                this.movePrevInternal(false);
                break label3;
            }
            this.updateExpandedIndicator(i2, i0, i3);
            if (this.showOn) {
                a0 = this.getExpandedViewChild();
            } else {
                if (this.notifView == null) {
                    this.getNotificationView();
                }
                a0 = this.notifView.getCurrentNotificationViewChild();
            }
            if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(a0)) {
                this.updateDeleteAllGlanceCount((this.showOn) ? (android.widget.TextView)a0.findViewById(R.id.textView) : (android.widget.TextView)a0.findViewById(R.id.subTitle), a0);
            }
            this.runQueuedOperation();
        }
    }
    
    public void addNotification(com.navdy.hud.app.framework.notifications.INotification a) {
        this.addNotification(a, (java.util.EnumSet)null);
    }
    
    public void addNotification(com.navdy.hud.app.framework.notifications.INotification a, java.util.EnumSet a0) {
        label1: if (a != null) {
            if (this.uiStateManager.getRootScreen() != null) {
                boolean b = this.disconnected;
                label0: {
                    if (!b) {
                        break label0;
                    }
                    if (!this.NOTIFS_NOT_ALLOWED_ON_DISCONNECT.contains(a.getId())) {
                        break label0;
                    }
                    sLogger.i(new StringBuilder().append("notification not allowed during disconnect[").append(a.getId()).append("]").toString());
                    break label1;
                }
                if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
                    this.addNotificationInternal(a, a0);
                } else {
                    this.handler.post((Runnable)new com.navdy.hud.app.framework.notifications.NotificationManager$6(this, a, a0));
                }
            } else {
                sLogger.v(new StringBuilder().append("notification not accepted, Main screen not on [").append(a.getId()).append("]").toString());
            }
        }
    }
    
    public void addPendingNotification(com.navdy.hud.app.framework.notifications.INotification a) {
        this.pendingNotification = a;
        sLogger.v(new StringBuilder().append("addPendingNotification:").append(this.pendingNotification).toString());
    }
    
    public void clearOperationQueue() {
        sLogger.v(new StringBuilder().append("clearOperationQueue:").append(this.operationQueue.size()).toString());
        this.operationQueue.clear();
        this.operationRunning = false;
    }
    
    public void collapseExpandedNotification(boolean b, boolean b0) {
        this.notificationController.collapseNotification(b, b0);
    }
    
    public boolean collapseNotification() {
        boolean b = false;
        com.navdy.hud.app.ui.activity.Main a = this.mainScreen;
        label1: {
            label0: {
                if (a != null) {
                    break label0;
                }
                this.getNotificationView();
                if (this.mainScreen != null) {
                    break label0;
                }
                b = false;
                break label1;
            }
            if (this.mainScreen.isNotificationViewShowing()) {
                if (this.mainScreen.isNotificationCollapsing()) {
                    b = false;
                } else if (this.mainScreen.isNotificationExpanding()) {
                    b = false;
                } else {
                    this.hideNotification();
                    b = true;
                }
            } else {
                b = false;
            }
        }
        return b;
    }
    
    public void currentNotificationTimeout() {
        if (this.currentNotification != null) {
            sLogger.v("currentNotificationTimeout");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordGlanceAction("Glance_Dismiss", this.currentNotification.notification, "timeout");
            this.removeNotification(this.currentNotification.notification.getId(), false);
        }
    }
    
    public void enableNotifications(boolean b) {
        sLogger.v(new StringBuilder().append("notification enabled[").append(b).append("]").toString());
        this.enable = b;
        this.enablePhoneNotifications(b);
    }
    
    public void enablePhoneNotifications(boolean b) {
        sLogger.v(new StringBuilder().append("notification enabled phone[").append(b).append("]").toString());
        this.enablePhoneCalls = b;
    }
    
    public void executeItem() {
        boolean b = this.isExpanded();
        label0: {
            label1: {
                if (b) {
                    break label1;
                }
                if (!this.isExpandedNotificationVisible()) {
                    break label0;
                }
            }
            if (this.deleteAllGlances) {
                sLogger.v("executeItem: no-op deleteGlances set");
            } else {
                this.runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.SELECT, false, false, false, false, (String)null, false);
            }
        }
    }
    
    public boolean expandNotification() {
        boolean b = false;
        com.navdy.hud.app.ui.activity.Main a = this.mainScreen;
        label1: {
            label0: {
                if (a != null) {
                    break label0;
                }
                this.getNotificationView();
                if (this.mainScreen != null) {
                    break label0;
                }
                b = false;
                break label1;
            }
            if (this.mainScreen.isNotificationViewShowing()) {
                b = false;
            } else if (this.mainScreen.isNotificationExpanding()) {
                b = false;
            } else if (this.makeNotificationCurrent(true)) {
                this.showNotification();
                b = true;
            } else {
                b = false;
            }
        }
        return b;
    }
    
    public com.navdy.hud.app.framework.notifications.INotification getCurrentNotification() {
        return (this.currentNotification != null) ? this.currentNotification.notification : null;
    }
    
    public int getExpandedIndicatorCount() {
        return this.notifIndicator.getItemCount();
    }
    
    public int getExpandedIndicatorCurrentItem() {
        return this.notifIndicator.getCurrentItem();
    }
    
    public android.view.View getExpandedViewChild() {
        android.widget.FrameLayout a = this.getExpandedNotificationView();
        android.view.View a0 = null;
        if (a != null) {
            int i = a.getChildCount();
            a0 = null;
            if (i != 0) {
                a0 = a.getChildAt(0);
            }
        }
        return a0;
    }
    
    public com.navdy.hud.app.framework.notifications.INotification getNotification(String s) {
        Object a = null;
        Throwable a0 = null;
        boolean b = android.text.TextUtils.isEmpty((CharSequence)s);
        label0: {
            com.navdy.hud.app.framework.notifications.INotification a1 = null;
            if (b) {
                a1 = null;
            } else {
                synchronized(this.lockObj) {
                    com.navdy.hud.app.framework.notifications.NotificationManager$Info a2 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationIdMap.get(s);
                    if (a2 == null) {
                        /*monexit(a)*/;
                        a1 = null;
                    } else {
                        a1 = a2.notification;
                        /*monexit(a)*/;
                    }
                }
            }
            return a1;
        }
    }
    
    public int getNotificationColor() {
        int i = 0;
        synchronized(this.lockObj) {
            if (this.notificationPriorityMap.size() <= 0) {
                /*monexit(a)*/;
                i = -1;
            } else {
                com.navdy.hud.app.framework.notifications.NotificationManager$Info a0 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationPriorityMap.lastKey();
                if (a0.removed) {
                    /*monexit(a)*/;
                    i = -1;
                } else {
                    i = a0.notification.getColor();
                    /*monexit(a)*/;
                }
            }
        }
        return i;
    }
    
    public int getNotificationCount() {
        int i = 0;
        synchronized(this.lockObj) {
            i = this.notificationIdMap.size();
            /*monexit(a)*/;
        }
        return i;
    }
    
    public String getTopNotificationId() {
        String s = null;
        synchronized(this.lockObj) {
            if (this.currentNotification == null) {
                if (this.notificationPriorityMap.size() <= 0) {
                    /*monexit(a)*/;
                    s = null;
                } else {
                    s = ((com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationPriorityMap.lastKey()).notification.getId();
                    /*monexit(a)*/;
                }
            } else {
                s = this.currentNotification.notification.getId();
                /*monexit(a)*/;
            }
        }
        return s;
    }
    
    public android.content.Context getUIContext() {
        return this.getNotificationView().getContext();
    }
    
    public void handleConnect() {
        sLogger.v("client connected: restore stack");
        this.disconnected = false;
        this.restoreStack();
        this.notificationSavedListDeviceId = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getDeviceId();
    }
    
    public void handleDisconnect(boolean b) {
        if (!this.disconnected) {
            sLogger.v(new StringBuilder().append("client disconnected: save stack :").append(b).toString());
            this.disconnected = true;
            this.saveStack();
            boolean b0 = this.isExpanded();
            label2: {
                label0: {
                    label1: {
                        if (b0) {
                            break label1;
                        }
                        if (!this.isExpandedNotificationVisible()) {
                            break label0;
                        }
                    }
                    this.collapseExpandedNotification(true, true);
                    break label2;
                }
                this.collapseNotification();
            }
            com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getCallManager().disconnect();
        }
    }
    
    public boolean handleKey(com.navdy.hud.app.manager.InputManager.CustomKeyEvent a) {
        boolean b = false;
        this.getNotificationView();
        sLogger.v(new StringBuilder().append("handleKey: running=").append(this.operationRunning).append(" qsize:").append(this.operationQueue.size()).append(" animating:").append(this.isAnimating).toString());
        label0: if (this.isAnimating) {
            sLogger.v("handleKey: animating no-op");
            b = false;
        } else {
            boolean b0 = this.operationRunning;
            label2: {
                boolean b1 = false;
                if (b0) {
                    break label2;
                }
                if (!this.isExpanded()) {
                    break label2;
                }
                if (this.stackCurrentNotification == null) {
                    break label2;
                }
                if (!this.showOn) {
                    break label2;
                }
                if (!this.stackCurrentNotification.notification.supportScroll()) {
                    break label2;
                }
                if (!this.stackCurrentNotification.startCalled) {
                    break label2;
                }
                com.navdy.hud.app.manager.InputManager.CustomKeyEvent a0 = com.navdy.hud.app.manager.InputManager.CustomKeyEvent.LEFT;
                label5: {
                    label3: {
                        label4: {
                            if (a == a0) {
                                break label4;
                            }
                            if (a != com.navdy.hud.app.manager.InputManager.CustomKeyEvent.RIGHT) {
                                break label3;
                            }
                        }
                        b1 = true;
                        break label5;
                    }
                    b1 = false;
                }
                if (!b1) {
                    break label2;
                }
                boolean b2 = this.stackCurrentNotification.notification.onKey(a);
                label1: {
                    if (!b2) {
                        break label1;
                    }
                    this.scrollState = com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.HANDLED;
                    this.handler.removeCallbacks(this.ignoreScrollRunnable);
                    b = true;
                    break label0;
                }
                switch(com.navdy.hud.app.framework.notifications.NotificationManager$16.$SwitchMap$com$navdy$hud$app$framework$notifications$NotificationManager$ScrollState[this.scrollState.ordinal()]) {
                    case 4: {
                        this.startScrollThresholdTimer();
                        b = true;
                        break label0;
                    }
                    case 3: {
                        b = true;
                        break label0;
                    }
                }
            }
            this.scrollState = com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.NONE;
            switch(com.navdy.hud.app.framework.notifications.NotificationManager$16.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 4: {
                    sLogger.v("power button click: show shutdown");
                    this.pendingShutdownArgs = com.navdy.hud.app.event.Shutdown.Reason.POWER_BUTTON.asBundle();
                    this.collapseExpandedNotification(true, true);
                    b = true;
                    break;
                }
                case 3: {
                    this.executeItem();
                    b = true;
                    break;
                }
                case 2: {
                    this.moveNext(false);
                    b = true;
                    break;
                }
                case 1: {
                    this.movePrevious(false);
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        }
        return b;
    }
    
    public void hideConnectionToast(boolean b) {
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        if (b) {
            a.dismissCurrentToast("disconnection#toast");
        } else {
            a.dismissCurrentToast("connection#toast");
        }
    }
    
    public void hideNotification() {
        if (this.isExpanded && !this.isAnimating) {
            if (this.isExpanded()) {
                sLogger.v("hideNotification:expand hide");
                this.animateOutExpandedView(true, this.currentNotification, false);
            } else {
                this.hideNotificationInternal();
            }
        }
    }
    
    public void hideNotificationCoverView() {
        android.view.View a = this.getExpandedNotificationCoverView();
        if (a != null && a.getVisibility() == View.VISIBLE) {
            sLogger.v("hideNotificationCoverView");
            a.setVisibility(View.GONE);
        }
    }
    
    public boolean isAnimating() {
        return this.isAnimating;
    }
    
    public boolean isCurrentItemDeleteAll() {
        boolean b = false;
        if (this.notifIndicator != null) {
            int i = this.notifIndicator.getItemCount();
            b = this.isDeleteAllIndex(this.notifIndicator.getCurrentItem(), i);
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean isCurrentNotificationId(String s) {
        return this.currentNotification != null && android.text.TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)s);
    }
    
    public boolean isExpanded() {
        return this.expandedWithStack;
    }
    
    public boolean isExpandedNotificationVisible() {
        android.widget.FrameLayout a = this.getExpandedNotificationView();
        return a != null && ((android.view.View)a).getVisibility() == View.VISIBLE;
    }
    
    public boolean isNotificationMarkedForRemoval(String s) {
        boolean b = false;
        synchronized(this.lockObj) {
            com.navdy.hud.app.framework.notifications.NotificationManager$Info a0 = this.getNotificationFromId(s);
            if (a0 == null) {
                /*monexit(a)*/;
                b = false;
            } else {
                b = a0.removed;
                /*monexit(a)*/;
            }
        }
        return b;
    }
    
    public boolean isNotificationPresent(String s) {
        boolean b = false;
        synchronized(this.lockObj) {
            b = this.notificationIdMap.containsKey(s);
            /*monexit(a)*/;
        }
        return b;
    }
    
    public boolean isNotificationViewShowing() {
        boolean b = false;
        com.navdy.hud.app.ui.activity.Main a = this.mainScreen;
        label0: {
            label2: {
                if (a != null) {
                    break label2;
                }
                this.getNotificationView();
                if (this.mainScreen == null) {
                    b = false;
                    break label0;
                }
            }
            boolean b0 = this.mainScreen.isNotificationViewShowing();
            label1: {
                if (b0) {
                    break label1;
                }
                if (!this.mainScreen.isNotificationExpanding()) {
                    b = false;
                    break label0;
                }
            }
            b = true;
        }
        return b;
    }
    
    public boolean isNotificationsEnabled() {
        return this.enable;
    }
    
    public boolean isPhoneNotificationsEnabled() {
        return this.enablePhoneCalls;
    }
    
    public boolean makeNotificationCurrent(boolean b) {
        boolean b0 = false;
        synchronized(this.lockObj) {
            if (this.currentNotification == null) {
                if (this.notificationPriorityMap.size() <= 0) {
                    /*monexit(a)*/;
                    b0 = false;
                } else {
                    if (b) {
                        sLogger.v("make all visible");
                        Object a0 = this.notificationPriorityMap.keySet().iterator();
                        while(((java.util.Iterator)a0).hasNext()) {
                            ((com.navdy.hud.app.framework.notifications.NotificationManager$Info)((java.util.Iterator)a0).next()).pushBackDuetoHighPriority = true;
                        }
                    }
                    this.currentNotification = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationPriorityMap.lastKey();
                    /*monexit(a)*/;
                    b0 = true;
                }
            } else if (this.notificationPriorityMap.size() != 0) {
                com.navdy.hud.app.framework.notifications.NotificationManager$Info a1 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationPriorityMap.lastKey();
                if (a1 != null && a1 != this.currentNotification) {
                    sLogger.v(new StringBuilder().append("mknc:current[").append(this.currentNotification.notification.getId()).append("] is not highest[").append(a1.notification.getId()).append("]").toString());
                    if (this.currentNotification.startCalled) {
                        this.currentNotification.startCalled = false;
                        this.currentNotification.notification.onStop();
                        sLogger.v("mknc: stop called");
                    }
                    this.currentNotification = a1;
                }
                /*monexit(a)*/;
                b0 = true;
            } else {
                if (this.currentNotification.startCalled) {
                    this.currentNotification.notification.onStop();
                }
                this.currentNotification = null;
                /*monexit(a)*/;
                b0 = false;
            }
        }
        return b0;
    }
    
    public void moveNext(boolean b) {
        boolean b0 = this.isExpanded();
        label0: {
            label1: {
                if (b0) {
                    break label1;
                }
                if (!this.isExpandedNotificationVisible()) {
                    break label0;
                }
            }
            if (this.deleteAllGlances) {
                sLogger.v("moveNext: no-op deleteGlances set");
            } else {
                this.runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.MOVE_NEXT, false, false, false, b, (String)null, false);
            }
        }
    }
    
    public void movePrevious(boolean b) {
        boolean b0 = this.isExpanded();
        label0: {
            label1: {
                if (b0) {
                    break label1;
                }
                if (!this.isExpandedNotificationVisible()) {
                    break label0;
                }
            }
            if (this.deleteAllGlances) {
                sLogger.v("movePrevious: no-op deleteGlances set");
            } else {
                this.runOperation(com.navdy.hud.app.framework.notifications.NotificationAnimator.Operation.MOVE_PREV, false, false, false, b, (String)null, false);
            }
        }
    }

    @Subscribe
    public void onCallAccepted(com.navdy.hud.app.framework.phonecall.CallManager.CallAccepted a) {
        sLogger.v("phone-start");
        this.updateExpandedIndicator();
    }

    @Subscribe
    public void onCallEnded(com.navdy.hud.app.framework.phonecall.CallManager.CallEnded a) {
        if (this.notificationController.isExpandedWithStack()) {
            this.handleRemoveNotificationInExpandedMode("navdy#phone#call#notif", false);
        } else {
            sLogger.v("phone-end");
        }
    }

    @Subscribe
    public void onClearGlances(com.navdy.service.library.events.glances.ClearGlances a) {
        this.clearAllGlances();
    }

    @Subscribe
    public void onNotificationPreference(com.navdy.service.library.events.preferences.NotificationPreferences a) {
        if (!a.enabled.booleanValue()) {
            this.clearAllGlances();
        }
    }
    
    @Subscribe
    public void onShowToast(com.navdy.hud.app.framework.toast.ToastManager$ShowToast a) {
        if (this.deleteAllGlances && android.text.TextUtils.equals((CharSequence)a.name, (CharSequence)"glance-deleted")) {
            sLogger.v("showToast: called deleteAllGlances");
            this.deleteAllGlances();
        }
    }
    
    public void postNotificationChange() {
        this.bus.post(NOTIFICATION_CHANGE);
    }
    
    public void printPriorityMap() {
        synchronized(this.lockObj) {
            if (this.stackCurrentNotification != null) {
                sLogger.v(new StringBuilder().append("notif-debug-map stack current = ").append(this.stackCurrentNotification.notification.getId()).toString());
            } else {
                sLogger.v("notif-debug-map stack current = null");
            }
            if (this.currentNotification != null) {
                sLogger.v(new StringBuilder().append("notif-debug-map current = ").append(this.currentNotification.notification.getId()).toString());
            } else {
                sLogger.v("notif-debug-map current = null");
            }
            java.util.Iterator a0 = this.notificationPriorityMap.keySet().iterator();
            sLogger.v("notif-debug-map-start");
            Object a1 = a0;
            while(((java.util.Iterator)a1).hasNext()) {
                com.navdy.hud.app.framework.notifications.NotificationManager$Info a2 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)((java.util.Iterator)a1).next();
                sLogger.v(new StringBuilder().append("notif-debug [").append(a2.notification.getId()).append("] priority[").append(a2.priority).append("] counter [").append(a2.uniqueCounter).append("]").toString());
            }
            sLogger.v("notif-debug-map-stop");
            /*monexit(a)*/;
        }
    }
    
    public void removeNotification(String s) {
        if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
            this.removeNotification(s, true);
        } else {
            this.handler.post((Runnable)new com.navdy.hud.app.framework.notifications.NotificationManager$7(this, s));
        }
    }
    
    public void removeNotification(String s, boolean b) {
        this.removeNotification(s, b, (com.navdy.service.library.events.ui.Screen)null, (android.os.Bundle)null, null);
    }
    
    public void removeNotification(String s, boolean b, com.navdy.service.library.events.ui.Screen a, android.os.Bundle a0, Object a1) {
        Object a2 = null;
        Throwable a3 = null;
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
        label0: {
            label5: if (!b0) {
                synchronized(this.lockObj) {
                    this.pendingScreen = a;
                    this.pendingScreenArgs = a0;
                    this.pendingScreenArgs2 = a1;
                    com.navdy.hud.app.framework.notifications.NotificationManager$Info a4 = (com.navdy.hud.app.framework.notifications.NotificationManager$Info)this.notificationIdMap.get(s);
                    if (a4 != null) {
                        boolean b1 = this.notificationController.isExpandedWithStack();
                        label4: {
                            if (!b1) {
                                break label4;
                            }
                            if (com.navdy.hud.app.framework.notifications.NotificationHelper.isNotificationRemovable(s)) {
                                break label4;
                            }
                            sLogger.v("removeNotification, handleRemoveNotificationInExpandedMode");
                            a4.removed = true;
                            this.handleRemoveNotificationInExpandedMode(s, true);
                            /*monexit(a2)*/;
                            break label5;
                        }
                        a4.removed = b;
                        if (a4.removed && a4.resurrected) {
                            sLogger.v(new StringBuilder().append("removeNotification [").append(s).append("] resurrected:false").toString());
                            a4.resurrected = false;
                        }
                        if (!a4.removed && !a4.notification.isAlive()) {
                            sLogger.v(new StringBuilder().append("removeNotification [").append(s).append("] is not alive").toString());
                            a4.removed = true;
                        }
                        if (!a4.removed) {
                            sLogger.v(new StringBuilder().append("removeNotification [").append(s).append("] is still alive").toString());
                        }
                        com.navdy.hud.app.framework.notifications.NotificationManager$Info a5 = this.currentNotification;
                        label3: {
                            label1: {
                                label2: {
                                    if (a5 == null) {
                                        break label2;
                                    }
                                    if (android.text.TextUtils.equals((CharSequence)this.currentNotification.notification.getId(), (CharSequence)a4.notification.getId())) {
                                        break label1;
                                    }
                                }
                                sLogger.v("bkgnd notification being removed");
                                this.removeNotificationfromStack(a4, false);
                                this.setNotificationColor();
                                break label3;
                            }
                            if (this.isExpanded && !this.isAnimating) {
                                this.hideNotification();
                            }
                        }
                        /*monexit(a2)*/;
                    } else {
                        /*monexit(a2)*/;
                    }
                }
            }
            return;
        }
    }
    
    public void setExpandedStack(boolean b) {
        this.expandedWithStack = b;
    }
    
    public void setNotificationColor() {
        Object a = null;
        Throwable a0 = null;
        com.navdy.hud.app.view.NotificationView a1 = this.getNotificationView();
        label0: {
            if (a1 != null) {
                if (this.currentNotification == null) {
                    a1.resetNextNotificationColor();
                } else {
                    synchronized(this.lockObj) {
                        com.navdy.hud.app.framework.notifications.NotificationManager$Info a2 = this.getLowerNotification(this.currentNotification);
                        if (a2 != null) {
                            a1.setNextNotificationColor(a2.notification.getColor());
                        } else {
                            a1.resetNextNotificationColor();
                        }
                        /*monexit(a)*/;
                    }
                }
            }
            return;
        }
    }
    
    public void showHideToast(boolean b) {
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        if (this.uiStateManager.isWelcomeScreenOn()) {
            sLogger.v("welcome screen on, no disconnect notification");
            this.hideConnectionToast(b);
        } else if (b) {
            if (!a.isCurrentToast("connection#toast")) {
                com.navdy.hud.app.framework.connection.ConnectionNotification.showConnectedToast();
            }
        } else if (!a.isCurrentToast("disconnection#toast")) {
            com.navdy.hud.app.framework.connection.ConnectionNotification.showDisconnectedToast(false);
        }
    }
    
    public boolean showNotification() {
        boolean b = false;
        boolean b0 = this.enable;
        label2: {
            label1: {
                if (b0) {
                    break label1;
                }
                boolean b1 = this.enablePhoneCalls;
                label0: {
                    if (!b1) {
                        break label0;
                    }
                    if (!this.isCurrentNotificationId("navdy#phone#call#notif")) {
                        break label0;
                    }
                    sLogger.v("notification only phone-notif allowed");
                    break label1;
                }
                sLogger.v("notification not enabled don't show notification");
                b = false;
                break label2;
            }
            if (this.uiStateManager.isWelcomeScreenOn()) {
                sLogger.v("welcome screen on, don't show notification");
                b = false;
            } else {
                this.showNotification(false);
                b = true;
            }
        }
        return b;
    }
    
    public void updateExpandedIndicator(int i, int i0, int i1) {
        this.notifIndicator.setItemCount(i);
        this.notifIndicator.setCurrentItem(i0, i1);
        this.displayScrollingIndicator(i0);
        sLogger.v(new StringBuilder().append("updateExpandedIndicator count =").append(i).append(" currentitem=").append(i0).toString());
    }
    
    public void viewSwitchAnimation(android.view.View a, android.view.View a0, com.navdy.hud.app.framework.notifications.NotificationManager$Info info, boolean b, int i) {
        android.view.View a2 = this.getExpandedViewChild();
        int i0 = this.notifIndicator.getCurrentItem();
        int i1 = b ? i0 + -1 : i0 + 1;
        com.navdy.hud.app.framework.notifications.NotificationManager$9 a3 = new com.navdy.hud.app.framework.notifications.NotificationManager$9(this, i1, i, info, a2);
        if (a != null) {
            android.view.View a4 = this.notifView.getCurrentNotificationViewChild();
            android.view.ViewGroup a5 = this.notifView.getNotificationContainer();
            com.navdy.hud.app.framework.notifications.NotificationAnimator.animateNotifViews(a, a4, a5, b ? -this.animationTranslation : this.animationTranslation, 250, 250, (Runnable)new com.navdy.hud.app.framework.notifications.NotificationManager$10(this, info, a4, a5, a0, (Runnable)a3));
        }
        if (a0 != null) {
            com.navdy.hud.app.framework.notifications.NotificationAnimator.animateExpandedViews(a0, a2, (android.view.ViewGroup)this.expandedNotifView, b ? -this.animationTranslation : this.animationTranslation, 250, 250, (Runnable)a3);
        }
        if (this.isExpanded()) {
            this.hideScrollingIndicator();
            if (this.stackCurrentNotification != null && this.stackCurrentNotification.notification.supportScroll()) {
                ((com.navdy.hud.app.framework.notifications.IScrollEvent)this.stackCurrentNotification.notification).setListener((com.navdy.hud.app.framework.notifications.IProgressUpdate)null);
            }
        }
        android.animation.AnimatorSet a6 = this.notifIndicator.getItemMoveAnimator(i1, i);
        if (a6 != null) {
            a6.start();
        }
    }
}
