package com.navdy.hud.app.framework.phonecall;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus.BatteryStatus;
import com.navdy.service.library.log.Logger;

import java.util.Arrays;

public class PhoneBatteryNotification {
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final String EMPTY = "";
    private static final String ID_BATTERY_EXTREMELY_LOW = "phone-bat-exlow";
    private static final String ID_BATTERY_LOW = "phone-bat-low";
    private static final String ID_NO_NETWORK = "phone-no-network";
    private static final int PHONE_BATTERY_TIMEOUT = 2000;
    private static final int PHONE_BATTERY_TIMEOUT_EXTREMELY_LOW = 20000;
    private static final int PHONE_NO_NETWORK_TIMEOUT = 2000;
    private static final String noNetwork = resources.getString(R.string.phone_no_network);
    private static final Logger sLogger = new Logger(PhoneBatteryNotification.class);
    private static final String unknown = resources.getString(R.string.question_mark);

    public static void showBatteryToast(PhoneBatteryStatus phoneBatteryStatus) {
        sLogger.v("[PhoneBatteryStatus] status[" + phoneBatteryStatus.status + "] charging[" + phoneBatteryStatus.charging + "] level[" + phoneBatteryStatus.level + "]");
        if (phoneBatteryStatus.status == BatteryStatus.BATTERY_OK || Boolean.TRUE.equals(phoneBatteryStatus.charging)) {
            sLogger.v("phone battery is ok/charging");
            dismissAllBatteryToasts();
            return;
        }
        Bundle bundle = new Bundle();
        String level = String.valueOf(phoneBatteryStatus.level != null ? phoneBatteryStatus.level : unknown);
        String title = "";
        int icon = 0;
        String id = null;
        String tts = null;
        ToastManager toastManager = ToastManager.getInstance();
        switch (phoneBatteryStatus.status) {
            case BATTERY_EXTREMELY_LOW:
                title = resources.getString(R.string.phone_ex_low_battery, new Object[]{level});
                icon = R.drawable.icon_toast_phone_battery_very_low;
                bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, PHONE_BATTERY_TIMEOUT_EXTREMELY_LOW);
                id = ID_BATTERY_EXTREMELY_LOW;
                bundle.putBoolean(ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
                tts = TTSUtils.TTS_PHONE_BATTERY_VERY_LOW;
                toastManager.dismissCurrentToast(ID_BATTERY_LOW);
                toastManager.clearPendingToast(new String[] {ID_BATTERY_LOW, ID_BATTERY_EXTREMELY_LOW});
                break;
            case BATTERY_LOW:
                title = resources.getString(R.string.phone_low_battery, new Object[]{level});
                icon = R.drawable.icon_toast_phone_battery_low;
                bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 2000);
                id = ID_BATTERY_LOW;
                tts = TTSUtils.TTS_PHONE_BATTERY_LOW;
                toastManager.dismissCurrentToast(ID_BATTERY_EXTREMELY_LOW);
                toastManager.clearPendingToast(new String[] {ID_BATTERY_LOW, ID_BATTERY_EXTREMELY_LOW});
                break;
        }
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, icon);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, title);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        bundle.putString(ToastPresenter.EXTRA_TTS, tts);
        if (!TextUtils.equals(id, toastManager.getCurrentToastId())) {
            ToastManager.getInstance().addToast(new ToastManager$ToastParams(id, bundle, null, false, false));
        }
    }

    private static void dismissAllBatteryToasts() {
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.dismissCurrentToast(new String[] {ID_BATTERY_LOW, ID_BATTERY_EXTREMELY_LOW});
        toastManager.clearPendingToast(new String[] {ID_BATTERY_LOW, ID_BATTERY_EXTREMELY_LOW});
    }

    public static void dismissAllToasts() {
        dismissAllBatteryToasts();
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.dismissCurrentToast(ID_NO_NETWORK);
        toastManager.clearPendingToast(ID_NO_NETWORK);
    }
}
