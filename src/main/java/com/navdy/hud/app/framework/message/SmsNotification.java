package com.navdy.hud.app.framework.message;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceHandler;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.manager.InputManager;
import android.animation.AnimatorSet;
import butterknife.ButterKnife;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.view.View;
import android.content.Context;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import android.text.TextUtils;
import com.navdy.hud.app.util.PhoneUtil;
import com.makeramen.RoundedTransformationBuilder;
import java.util.ArrayList;
import android.os.Looper;
import com.navdy.hud.app.HudApplication;
import java.util.concurrent.TimeUnit;
import android.widget.TextView;
import android.widget.ImageView;
import com.squareup.picasso.Transformation;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.framework.notifications.INotificationController;
import butterknife.InjectView;
import com.navdy.service.library.log.Logger;
import android.content.res.Resources;
import android.os.Handler;
import java.util.List;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.framework.notifications.INotification;

public class SmsNotification implements INotification, ChoiceLayout2.IListener, ContactUtil.IContactCallback
{
    private static final int NOTIFICATION_TIMEOUT;
    private static final int TIMEOUT;
    private static final List<ChoiceLayout2.Choice> choicesDismiss;
    private static final List<ChoiceLayout2.Choice> choicesRetryAndDismiss;
    private static final Handler handler;
    private static final Resources resources;
    private static final Logger sLogger;
    @InjectView(R.id.choice_layout)
    ChoiceLayout2 choiceLayout;
    private INotificationController controller;
    private final String message;
    private final Mode mode;
    private final String name;
    private final String notifId;
    @InjectView(R.id.notification_user_image)
    InitialsImageView notificationUserImage;
    private final String number;
    private final String rawNumber;
    private boolean retrySendingMessage;
    private Transformation roundTransformation;
    @InjectView(R.id.badge)
    ImageView sideImage;
    @InjectView(R.id.subtitle)
    TextView subtitle;
    @InjectView(R.id.title)
    TextView title;
    
    static {
        sLogger = new Logger(SmsNotification.class);
        TIMEOUT = (int)TimeUnit.SECONDS.toMillis(10L);
        NOTIFICATION_TIMEOUT = (int)TimeUnit.SECONDS.toMillis(5L);
        resources = HudApplication.getAppContext().getResources();
        handler = new Handler(Looper.getMainLooper());
        final String string = SmsNotification.resources.getString(R.string.dismiss);
        final int color = SmsNotification.resources.getColor(R.color.glance_dismiss);
        final int color2 = SmsNotification.resources.getColor(R.color.glance_ok_blue);
        (choicesRetryAndDismiss = new ArrayList<ChoiceLayout2.Choice>()).add(new ChoiceLayout2.Choice(R.id.retry, R.drawable.icon_glances_retry, color2, R.drawable.icon_glances_retry, -16777216, SmsNotification.resources.getString(R.string.retry), color2));
        SmsNotification.choicesRetryAndDismiss.add(new ChoiceLayout2.Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, string, color));
        (choicesDismiss = new ArrayList<ChoiceLayout2.Choice>()).add(new ChoiceLayout2.Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, string, color));
    }
    
    public SmsNotification(final Mode mode, final String s, final String s2, final String message, final String name) {
        this.roundTransformation = new RoundedTransformationBuilder().oval(true).build();
        this.mode = mode;
        this.notifId = "navdy#sms#notif#" + s;
        this.number = PhoneUtil.formatPhoneNumber(s2);
        this.rawNumber = PhoneUtil.convertToE164Format(s2);
        this.message = message;
        this.name = name;
    }
    
    private String getContactName() {
        String s;
        if (!TextUtils.isEmpty((CharSequence)this.name)) {
            s = this.name;
        }
        else if (!TextUtils.isEmpty((CharSequence)this.number)) {
            s = this.number;
        }
        else {
            s = "";
        }
        return s;
    }
    
    private void setUI() {
        if (!TextUtils.isEmpty((CharSequence)this.name)) {
            this.subtitle.setText((CharSequence)this.name);
        }
        else if (!TextUtils.isEmpty((CharSequence)this.number)) {
            this.subtitle.setText((CharSequence)PhoneUtil.formatPhoneNumber(this.number));
        }
        else {
            this.subtitle.setText((CharSequence)"");
        }
        ContactUtil.setContactPhoto(this.name, this.number, false, this.notificationUserImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
        switch (this.mode) {
            case Failed:
                this.title.setText((CharSequence)SmsNotification.resources.getString(R.string.reply_failed));
                this.subtitle.setText((CharSequence)this.getContactName());
                this.sideImage.setImageResource(R.drawable.icon_msg_failed);
                this.choiceLayout.setChoices(SmsNotification.choicesRetryAndDismiss, 0, (ChoiceLayout2.IListener)this);
                break;
            case Success:
                this.title.setText((CharSequence)SmsNotification.resources.getString(R.string.reply_sent));
                this.subtitle.setText((CharSequence)this.getContactName());
                this.sideImage.setImageResource(R.drawable.icon_msg_success);
                this.choiceLayout.setChoices(SmsNotification.choicesDismiss, 0, (ChoiceLayout2.IListener)this);
                break;
        }
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public void executeItem(final ChoiceLayout2.Selection selection) {
        switch (selection.id) {
            case R.id.retry:
                this.retrySendingMessage = true;
                NotificationManager.getInstance().removeNotification(this.notifId);
                break;
            case R.id.dismiss:
                NotificationManager.getInstance().removeNotification(this.notifId);
                break;
        }
    }
    
    @Override
    public boolean expandNotification() {
        return false;
    }
    
    @Override
    public int getColor() {
        return 0;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return this.notifId;
    }
    
    @Override
    public int getTimeout() {
        int n = 0;
        switch (this.mode) {
            default:
                n = SmsNotification.NOTIFICATION_TIMEOUT;
                break;
            case Failed:
                n = SmsNotification.TIMEOUT;
                break;
        }
        return n;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.SMS;
    }
    
    @Override
    public View getView(final Context context) {
        final View inflate = LayoutInflater.from(context).inflate(R.layout.notification_sms, (ViewGroup)null);
        ButterKnife.inject(this, inflate);
        return inflate;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    @Override
    public boolean isAlive() {
        return false;
    }
    
    @Override
    public boolean isContextValid() {
        return this.controller != null;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }
    
    @Override
    public void itemSelected(final ChoiceLayout2.Selection selection) {
    }
    
    @Override
    public InputManager.IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onClick() {
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Override
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean b = false;
        if (this.controller != null) {
            switch (customKeyEvent) {
                case LEFT:
                    this.choiceLayout.moveSelectionLeft();
                    this.controller.resetTimeout();
                    b = true;
                    break;
                case RIGHT:
                    this.choiceLayout.moveSelectionRight();
                    this.controller.resetTimeout();
                    b = true;
                    break;
                case SELECT:
                    this.choiceLayout.executeSelectedItem();
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        this.controller = controller;
        this.setUI();
    }
    
    @Override
    public void onStop() {
        this.controller = null;
        if (this.retrySendingMessage) {
            SmsNotification.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    final GlanceHandler instance = GlanceHandler.getInstance();
                    if (!instance.sendMessage(SmsNotification.this.rawNumber, SmsNotification.this.message, SmsNotification.this.name)) {
                        SmsNotification.sLogger.v("retry message not sent");
                        instance.sendSmsFailedNotification(SmsNotification.this.number, SmsNotification.this.message, SmsNotification.this.name);
                    }
                    else {
                        SmsNotification.sLogger.v("retry message sent");
                        instance.sendSmsSuccessNotification(SmsNotification.this.notifId, SmsNotification.this.number, SmsNotification.this.message, SmsNotification.this.name);
                    }
                }
            });
        }
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public void onUpdate() {
    }
    
    @Override
    public boolean supportScroll() {
        return false;
    }

    public enum Mode {
        Success(0),
        Failed(1);

        private int value;
        Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
