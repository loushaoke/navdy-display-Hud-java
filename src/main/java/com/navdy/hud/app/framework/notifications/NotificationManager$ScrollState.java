package com.navdy.hud.app.framework.notifications;


public enum NotificationManager$ScrollState {
    NONE("NONE", 0),
    HANDLED("HANDLED", 1),
    NOT_HANDLED("NOT_HANDLED", 2),
    LIMIT_REACHED("LIMIT_REACHED", 3);

    private String value;
    NotificationManager$ScrollState(String s, int i) {
        this.value = s;
    }
    public String getValue() {
        return value;
    }
}

//final class NotificationManager$ScrollState extends Enum {
//    final private static com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState[] $VALUES;
//    final public static com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState HANDLED;
//    final public static com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState LIMIT_REACHED;
//    final public static com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState NONE;
//    final public static com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState NOT_HANDLED;
//
//    static {
//        NONE = new com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState("NONE", 0);
//        HANDLED = new com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState("HANDLED", 1);
//        NOT_HANDLED = new com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState("NOT_HANDLED", 2);
//        LIMIT_REACHED = new com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState("LIMIT_REACHED", 3);
//        com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState[] a = new com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState[4];
//        a[0] = NONE;
//        a[1] = HANDLED;
//        a[2] = NOT_HANDLED;
//        a[3] = LIMIT_REACHED;
//        $VALUES = a;
//    }
//
//    private NotificationManager$ScrollState(String s, int i) {
//        super(s, i);
//    }
//
//    public static com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState valueOf(String s) {
//        return (com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState)Enum.valueOf(com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.class, s);
//    }
//
//    public static com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState[] values() {
//        return $VALUES.clone();
//    }
//}
