package com.navdy.hud.app.framework.toast;

class ToastManager$1 implements Runnable {
    final com.navdy.hud.app.framework.toast.ToastManager this$0;
    
    ToastManager$1(com.navdy.hud.app.framework.toast.ToastManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        synchronized(com.navdy.hud.app.framework.toast.ToastManager.access$000(this.this$0)) {
            if (com.navdy.hud.app.framework.toast.ToastManager.access$000(this.this$0).size() != 0) {
                if (com.navdy.hud.app.framework.toast.ToastManager.access$200(this.this$0)) {
                    com.navdy.hud.app.framework.toast.ToastManager.access$100().v("cannot display toast screen still on");
                    /*monexit(a)*/;
                } else if (com.navdy.hud.app.framework.toast.ToastManager.access$300(this.this$0)) {
                    com.navdy.hud.app.framework.toast.ToastManager.access$100().v("toasts disabled");
                    /*monexit(a)*/;
                } else {
                    com.navdy.hud.app.framework.toast.ToastManager$ToastInfo a0 = (com.navdy.hud.app.framework.toast.ToastManager$ToastInfo)com.navdy.hud.app.framework.toast.ToastManager.access$000(this.this$0).remove();
                    com.navdy.hud.app.framework.toast.ToastManager.access$402(this.this$0, a0);
                    com.navdy.hud.app.framework.toast.ToastManager.access$500(this.this$0, a0);
                    /*monexit(a)*/;
                }
            } else {
                com.navdy.hud.app.framework.toast.ToastManager.access$100().v("no pending toast");
                /*monexit(a)*/;
            }
        }
    }
}
