package com.navdy.hud.app.framework.recentcall;

import android.text.TextUtils;
import android.util.LongSparseArray;

import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.recentcall.RecentCall.CallType;
import com.navdy.hud.app.framework.recentcall.RecentCall.Category;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.callcontrol.CallEvent;
import com.navdy.service.library.events.contacts.Contact;
import com.navdy.service.library.events.contacts.ContactRequest;
import com.navdy.service.library.events.contacts.ContactResponse;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.navdy.hud.app.framework.contacts.PhoneImageDownloader.*;
import static com.navdy.service.library.events.photo.PhotoType.*;

public class RecentCallManager {
    private static final RecentCallChanged RECENT_CALL_CHANGED = new RecentCallChanged();
    private static final RecentCallManager sInstance = new RecentCallManager();
    private static final Logger sLogger = new Logger(RecentCallManager.class);
    private Bus bus = RemoteDeviceManager.getInstance().getBus();
    private final HashMap<String, RecentCall> contactRequestMap = new HashMap<>();
    private LongSparseArray<RecentCall> numberMap = new LongSparseArray<>();
    private volatile List<RecentCall> recentCalls;
    private final HashMap<String, List<Contact>> tempContactLookupMap = new HashMap<>();

    public static class ContactFound {
        public List<Contact> contact;
        public String identifier;

        public ContactFound(String identifier, List<Contact> contact) {
            this.identifier = identifier;
            this.contact = contact;
        }
    }

    public static class RecentCallChanged {
    }

    public static RecentCallManager getInstance() {
        return sInstance;
    }

    private RecentCallManager() {
        this.bus.register(this);
    }

    public List<RecentCall> getRecentCalls() {
        return this.recentCalls;
    }

    public int getRecentCallsSize() {
        if (this.recentCalls != null) {
            return this.recentCalls.size();
        }
        return 0;
    }

    public void setRecentCalls(List<RecentCall> recentCalls) {
        this.recentCalls = recentCalls;
        buildMap();
        this.bus.post(RECENT_CALL_CHANGED);
    }

    public void clearRecentCalls() {
        setRecentCalls(null);
    }

    public void buildRecentCalls() {
        synchronized (this.contactRequestMap) {
            this.contactRequestMap.clear();
        }
        if (GenericUtil.isMainThread()) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    RecentCallManager.this.load();
                }
            }, 1);
        } else {
            load();
        }
    }

    private void load() {
        try {
            String id = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
            this.recentCalls = RecentCallPersistenceHelper.getRecentsCalls(id);
            sLogger.v("load recentCall id[" + id + "] calls[" + this.recentCalls.size() + "]");
        } catch (Throwable t) {
            this.recentCalls = null;
            sLogger.e(t);
        } finally {
            buildMap();
            this.bus.post(RECENT_CALL_CHANGED);
        }
    }

    @Subscribe
    public void onCallEvent(CallEvent event) {
        if (TextUtils.isEmpty(event.number)) {
            sLogger.e("call event does not have a number, " + event.contact_name);
            return;
        }
        CallType callType;
        if (!event.incoming) {
            callType = CallType.OUTGOING;
        } else if (event.answered) {
            callType = CallType.INCOMING;
        } else {
            callType = CallType.MISSED;
        }
        handleNewCall(new RecentCall(event.contact_name, Category.PHONE_CALL, event.number, NumberType.OTHER, new Date(), callType, -1, 0));
    }

    public boolean handleNewCall(RecentCall recentCall) {
        sLogger.v("handle new call:" + recentCall.number);
        if (ContactUtil.isValidNumber(recentCall.number)) {
            this.storeContactInfo(recentCall);
            return true;
        }
        String string = null;
        synchronized(this.tempContactLookupMap) {
            List list = this.tempContactLookupMap.get(recentCall.number);
            if (list != null) {
                if (list.size() > 1) {
                    return false;
                }
                string = ((Contact) list.get(0)).number;
            }
            if (!TextUtils.isEmpty(string)) {
                recentCall.number = string;
                this.storeContactInfo(recentCall);
                return true;
            }
            this.requestContactInfo(recentCall);
            return false;
        }
    }


    private void requestContactInfo(RecentCall call) {
        sLogger.v("number not available for [" + call.number + "]");
        synchronized (this.contactRequestMap) {
            if (this.contactRequestMap.containsKey(call.number)) {
                sLogger.v("contact [" + call.number + "] request already pending");
                return;
            }
            this.contactRequestMap.put(call.number, call);
            sLogger.v("contact [" + call.number + "] request sent");
            this.bus.post(new RemoteEvent(new ContactRequest(call.number)));
        }
    }

    @Subscribe
    public void onContactResponse(final ContactResponse event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    if (TextUtils.isEmpty(event.identifier)) {
                        RecentCallManager.sLogger.w("invalid contact repsonse");
                        return;
                    }
                    List<RecentCall> callList = new ArrayList<>();
                    List<Contact> contactList = new ArrayList<>();
                    synchronized (RecentCallManager.this.contactRequestMap) {
                        RecentCall call = RecentCallManager.this.contactRequestMap.remove(event.identifier);
                        if (call == null) {
                            RecentCallManager.sLogger.i("identifer not found[" + event.identifier + "]");
                            return;
                        } else if (event.status != RequestStatus.REQUEST_SUCCESS) {
                            RecentCallManager.sLogger.i("request failed for [" + event.identifier + "] status[" + event.status + "]");
                        } else if (event.contacts == null || event.contacts.size() == 0) {
                            RecentCallManager.sLogger.i("no contact returned for [" + event.identifier + "] status[" + event.status + "]");
                            return;
                        } else {
                            RecentCallManager.sLogger.v("contacts returned [" + event.contacts.size() + "]");
                            for (Contact contact: event.contacts) {

                                if (TextUtils.isEmpty(contact.number) || !ContactUtil.isValidNumber(contact.number)) {
                                    RecentCallManager.sLogger.i("no number for  [" + event.identifier + "] status[" + event.status + "]");
                                } else {
                                    RecentCall copy = new RecentCall(call);
                                    if (ContactUtil.isDisplayNameValid(contact.name, contact.number, ContactUtil.getPhoneNumber(contact.number))) {
                                        copy.name = contact.name;
                                    } else {
                                        copy.name = null;
                                    }
                                    copy.number = contact.number;
                                    copy.numberType = ContactUtil.getNumberType(contact.numberType);
                                    copy.validateArguments();

                                    callList.add(copy);
                                    contactList.add(contact);
                                }
                            }
                        }
                    }
                    if (callList.size() == 0) {
                        return;
                    }
                    RecentCallManager.sLogger.v("contact found [" + event.identifier + "] downloading photo");
                    synchronized(RecentCallManager.this.tempContactLookupMap) {
                        RecentCallManager.this.tempContactLookupMap.put(event.identifier, contactList);
                    }
                    RecentCallManager.this.bus.post(new ContactFound(event.identifier, contactList));
                    PhoneImageDownloader a16 = PhoneImageDownloader.getInstance();
                    Object a17 = ((java.util.List)callList).iterator();
                    while(((java.util.Iterator)a17).hasNext()) {
                        RecentCall a18 = (RecentCall)((java.util.Iterator)a17).next();
                        a16.clearPhotoCheckEntry(a18.number, PHOTO_CONTACT);
                        a16.submitDownload(a18.number, Priority.HIGH, PHOTO_CONTACT, a18.name);
                    }
                    if (callList.size() != 1) {
                        return;
                    }
                    RecentCallManager.this.storeContactInfo(callList);

                } catch (Throwable t) {
                    RecentCallManager.sLogger.e(t);
                }
            }
        }, 1);
    }

    public void clearContactLookupMap() {
        synchronized (this.tempContactLookupMap) {
            this.tempContactLookupMap.clear();
        }
    }

    public List<Contact> getContactsFromId(String id) {
        List<Contact> list;
        synchronized (this.tempContactLookupMap) {
            list = this.tempContactLookupMap.get(id);
        }
        return list;
    }

    private void storeContactInfo(RecentCall call) {
        List<RecentCall> list = new ArrayList<>();
        list.add(call);
        storeContactInfo(list);
    }

    private void storeContactInfo(final List<RecentCall> calls) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                RecentCallPersistenceHelper.storeRecentCalls(DriverProfileHelper.getInstance().getCurrentProfile().getProfileName(), calls, false);
                for (RecentCall c : calls) {
                    RecentCallManager.sLogger.v("added contact [" + c.number + "]");
                }
            }
        }, 1);
    }

    public int getDefaultContactImage(long number) {
        RecentCall call = this.numberMap.get(number);
        if (call == null) {
            return -1;
        }
        return call.defaultImageIndex;
    }

    private void buildMap() {
        this.numberMap.clear();
        if (this.recentCalls != null && this.recentCalls.size() != 0) {
            for (RecentCall call : this.recentCalls) {
                this.numberMap.put(call.numericNumber, call);
            }
        }
    }
}
