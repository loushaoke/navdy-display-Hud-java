package com.navdy.hud.app.framework.notifications;

class NotificationManager$13 implements Runnable {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    final android.view.View val$child;
    final android.view.ViewGroup val$parent;
    final Runnable val$runnable;
    
    NotificationManager$13(com.navdy.hud.app.framework.notifications.NotificationManager a, android.view.View a0, android.view.ViewGroup a1, Runnable a2) {
        super();
        this.this$0 = a;
        this.val$child = a0;
        this.val$parent = a1;
        this.val$runnable = a2;
    }
    
    public void run() {
        com.navdy.hud.app.framework.notifications.NotificationManager.access$4500(this.this$0);
        if (this.val$child != null) {
            this.val$parent.removeView(this.val$child);
        }
        if (com.navdy.hud.app.framework.glance.GlanceHelper.isDeleteAllView(this.val$child)) {
            this.val$child.setTag(null);
            this.val$child.setAlpha(1f);
            com.navdy.hud.app.framework.glance.GlanceViewCache.putView(com.navdy.hud.app.framework.glance.GlanceViewCache.ViewType.BIG_TEXT, this.val$child);
        }
        int i = this.this$0.uiStateManager.getMainPanelWidth();
        int i0 = this.this$0.uiStateManager.getSidePanelWidth();
        com.navdy.hud.app.framework.notifications.NotificationAnimator.animateExpandedViewOut(com.navdy.hud.app.framework.notifications.NotificationManager.access$3500(this.this$0), i - i0, (android.view.View)com.navdy.hud.app.framework.notifications.NotificationManager.access$4000(this.this$0), (android.view.View)com.navdy.hud.app.framework.notifications.NotificationManager.access$3900(this.this$0), this.val$runnable, com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0), com.navdy.hud.app.framework.notifications.NotificationManager.access$1000(this.this$0), this.this$0.isExpandedNotificationVisible());
    }
}
