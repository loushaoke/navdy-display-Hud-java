package com.navdy.hud.app.framework.phonecall;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.event.DeviceInfoAvailable;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.callcontrol.CallAction;
import com.navdy.service.library.events.callcontrol.CallEvent;
import com.navdy.service.library.events.callcontrol.PhoneBatteryStatus;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.callcontrol.PhoneStatusRequest;
import com.navdy.service.library.events.callcontrol.PhoneStatusResponse;
import com.navdy.service.library.events.callcontrol.TelephonyRequest;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.Stack;

public class CallManager {
    private static final CallAccepted CALL_ACCEPTED = new CallAccepted();
    private static final CallEnded CALL_ENDED = new CallEnded();
    private static final int NO_RESPONSE_THRESHOLD = 10000;
    private static final PhoneStatusRequest PHONE_STATUS_REQUEST = new PhoneStatusRequest();
    private static final Logger sLogger = new Logger(CallManager.class);
    boolean answered = false;
    private Bus bus;
    boolean callMuted;
    private CallNotification callNotification;
    Stack<Info> callStack = new Stack();
    long callStartMs;
    String callUUID;
    String contact = null;
    boolean dialSet;
    long duration;
    private Handler handler = new Handler(Looper.getMainLooper());
    boolean incomingCall = false;
    CallAction lastCallAction;
    private Runnable noResponseCheckRunnable = new Runnable() {
        @Override
        public void run() {
            CallManager.sLogger.v("no response for call action:" + CallManager.this.lastCallAction);
            if (CallManager.this.lastCallAction != null && CallManager.this.isCallActionResponseRequired(CallManager.this.lastCallAction)) {
                switch (CallManager.this.lastCallAction) {
                    case CALL_ACCEPT:
                        CallManager.this.state = CallNotificationState.FAILED;
                        break;
                    case CALL_END:
                        CallManager.this.state = CallNotificationState.IDLE;
                        break;
                    case CALL_DIAL:
                        CallManager.this.state = CallNotificationState.FAILED;
                        break;
                }
                CallManager.this.userRejectedCall = false;
                CallManager.this.userCancelledCall = false;
                CallManager.this.dialSet = false;
                if (CallManager.this.isNotificationAllowed()) {
                    CallManager.this.sendNotification();
                }
            }
        }
    };
    String normalizedNumber = null;
    String number = null;
    PhoneStatus phoneStatus = PhoneStatus.PHONE_IDLE;
    boolean putOnStack = false;
    CallNotificationState state = CallNotificationState.IDLE;
    boolean userCancelledCall;
    boolean userRejectedCall;

    public static class CallAccepted {
    }

    public static class CallEnded {
    }

    public enum CallNotificationState {
        IDLE(0),
        RINGING(1),
        MISSED(2),
        ENDED(3),
        FAILED(4),
        DIALING(5),
        IN_PROGRESS(6),
        REJECTED(7),
        CANCELLED(8);

        private int value;
        CallNotificationState(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }

    private static class Info {
        boolean answered;
        boolean callMuted;
        long callStartMs;
        String callUUID;
        String contact;
        long duration;
        boolean incomingCall;
        String normalizedNumber;
        String number;
        boolean putOnStack;
        boolean userCancelledCall;
        boolean userRejectedCall;

        Info(String number, String normalizedNumber, String contact, boolean incomingCall, boolean answered, long callStartMs, String callUUID, long duration, boolean userRejectedCall, boolean userCancelledCall, boolean putOnStack, boolean callMuted) {
            this.number = number;
            this.normalizedNumber = normalizedNumber;
            this.contact = contact;
            this.incomingCall = incomingCall;
            this.answered = answered;
            this.callStartMs = callStartMs;
            this.callUUID = callUUID;
            this.duration = duration;
            this.userRejectedCall = userRejectedCall;
            this.userCancelledCall = userCancelledCall;
            this.putOnStack = putOnStack;
            this.callMuted = callMuted;
        }
    }

    public CallManager(Bus bus, Context context) {
        this.bus = bus;
        this.bus.register(this);
        this.callNotification = new CallNotification(this, bus);
    }

    @Subscribe
    public void onPhoneEvent(PhoneEvent event) {
        this.handler.removeCallbacks(this.noResponseCheckRunnable);
        this.lastCallAction = null;
        if (event.status == this.phoneStatus) {
            if (this.dialSet && event.status == PhoneStatus.PHONE_DIALING) {
                this.dialSet = false;
            } else if (TextUtils.equals(PhoneUtil.normalizeNumber(event.number), this.normalizedNumber)) {
                return;
            }
        }
        sLogger.v("phoneEvent:" + event);
        DeviceInfo deviceInfo = RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
        PhoneStatus lastStatus = this.phoneStatus;
        this.phoneStatus = event.status;
        long nowMs = SystemClock.elapsedRealtime();
        switch (event.status) {
            case PHONE_IDLE:
                sLogger.v("IDLE: putOnStack=" + this.putOnStack + " lastStatus=" + lastStatus);
                this.callUUID = null;
                this.callMuted = false;
                if (lastStatus == PhoneStatus.PHONE_RINGING || lastStatus == PhoneStatus.PHONE_DIALING) {
                    this.duration = 0;
                    boolean rejected = false;
                    if (this.userRejectedCall) {
                        this.state = CallNotificationState.REJECTED;
                        rejected = true;
                    } else if (this.userCancelledCall) {
                        this.state = CallNotificationState.CANCELLED;
                    } else if (this.answered) {
                        this.state = CallNotificationState.ENDED;
                    } else {
                        this.state = CallNotificationState.MISSED;
                    }
                    CallNotification.clearToast();
                    this.userRejectedCall = false;
                    this.userCancelledCall = false;
                    if (this.putOnStack) {
                        removeNotification();
                    } else if (!rejected) {
                        INotification notification = NotificationManager.getInstance().getCurrentNotification();
                        if (notification == null || TextUtils.equals(notification.getId(), NotificationId.PHONE_CALL_NOTIFICATION_ID)) {
                            removeNotification();
                        } else if (isNotificationAllowed()) {
                            sendNotification();
                        }
                    }
                } else if (lastStatus == PhoneStatus.PHONE_OFFHOOK) {
                    if (deviceInfo == null || deviceInfo.platform != Platform.PLATFORM_iOS || TextUtils.isEmpty(event.number) || TextUtils.equals(PhoneUtil.normalizeNumber(event.number), this.normalizedNumber)) {
                        this.duration = toSeconds(nowMs - this.callStartMs);
                        this.state = CallNotificationState.ENDED;
                        if (this.userRejectedCall || this.userCancelledCall || this.putOnStack) {
                            removeNotification();
                        } else if (isNotificationAllowed()) {
                            sendNotification();
                        }
                        this.userRejectedCall = false;
                        this.userCancelledCall = false;
                    } else {
                        sLogger.d("Received end for a different call than the current one");
                        return;
                    }
                }
                this.putOnStack = false;
                this.bus.post(CALL_ENDED);
                if (isNotificationAllowed() && this.number != null) {
                    this.bus.post(new CallEvent(Boolean.valueOf(this.incomingCall), Boolean.valueOf(this.answered), this.number, this.contact, Long.valueOf(toSeconds(this.callStartMs)), Long.valueOf(this.duration)));
                    break;
                }
                break;
            case PHONE_RINGING:
                if (lastStatus == PhoneStatus.PHONE_OFFHOOK) {
                    sLogger.v("we are in a call");
                    if (deviceInfo == null || deviceInfo.platform != Platform.PLATFORM_iOS) {
                        sLogger.w("stack not implemented on android");
                        return;
                    }
                    this.callStack.push(new Info(this.number, this.normalizedNumber, this.contact, this.incomingCall, this.answered, this.callStartMs, this.callUUID, this.duration, this.userRejectedCall, this.userCancelledCall, true, this.callMuted));
                    sLogger.v("stack pushed [" + this.contact + " , " + this.number + HereManeuverDisplayBuilder.COMMA + this.normalizedNumber + "]");
                }
                this.callStartMs = nowMs;
                this.incomingCall = true;
                this.answered = false;
                this.number = event.number;
                this.normalizedNumber = PhoneUtil.normalizeNumber(event.number);
                this.contact = event.contact_name;
                this.callMuted = false;
                if (!TextUtils.isEmpty(event.callUUID)) {
                    this.callUUID = event.callUUID;
                }
                this.state = CallNotificationState.RINGING;
                if (!isNotificationAllowed()) {
                    sLogger.v("incoming phone call disabled");
                } else {
                    this.callNotification.showIncomingCallToast();
                }
                break;
            case PHONE_DIALING:
                this.callStartMs = nowMs;
                this.incomingCall = false;
                this.answered = false;
                this.number = event.number;
                this.normalizedNumber = PhoneUtil.normalizeNumber(event.number);
                this.contact = event.contact_name;
                if (!TextUtils.isEmpty(event.callUUID)) {
                    this.callUUID = event.callUUID;
                }
                this.state = CallNotificationState.DIALING;
                if (isNotificationAllowed()) {
                    sendNotification();
                    break;
                }
                break;
            case PHONE_OFFHOOK:
                CallNotification.clearToast();
                this.callStartMs = nowMs;
                this.answered = true;
                if (!TextUtils.isEmpty(event.callUUID)) {
                    this.callUUID = event.callUUID;
                }
                if (TextUtils.isEmpty(this.normalizedNumber)) {
                    this.normalizedNumber = PhoneUtil.normalizeNumber(event.number);
                }
                if (TextUtils.isEmpty(this.contact)) {
                    this.contact = event.contact_name;
                }
                if (TextUtils.isEmpty(this.number)) {
                    this.number = event.number;
                }
                this.state = CallNotificationState.IN_PROGRESS;
                if (isNotificationAllowed()) {
                    sendNotification();
                    this.bus.post(CALL_ACCEPTED);
                    break;
                }
                break;
            case PHONE_DISCONNECTING:
                if (this.state == CallNotificationState.DIALING) {
                    this.state = CallNotificationState.FAILED;
                    if (isNotificationAllowed()) {
                        sendNotification();
                        break;
                    }
                }
                break;
        }
        sLogger.v("state:" + this.state);
    }

    public void clearCallStack() {
        this.callStack.clear();
    }

    private long toSeconds(long ms) {
        return (500 + ms) / 1000;
    }

    void sendCallAction(CallAction callAction, String number) {
        try {
            if (this.state == CallNotificationState.RINGING && callAction == CallAction.CALL_REJECT) {
                this.userRejectedCall = true;
            } else if ((this.state == CallNotificationState.DIALING || this.state == CallNotificationState.IN_PROGRESS) && callAction == CallAction.CALL_END) {
                this.userCancelledCall = true;
            }
            switch (callAction) {
                case CALL_MUTE:
                    this.callMuted = true;
                    break;
                case CALL_UNMUTE:
                    this.callMuted = false;
                    break;
            }
            if (isCallActionResponseRequired(callAction)) {
                this.handler.removeCallbacks(this.noResponseCheckRunnable);
                this.handler.postDelayed(this.noResponseCheckRunnable, 10000);
            }
            this.lastCallAction = callAction;
            this.bus.post(new RemoteEvent(new TelephonyRequest(callAction, number, this.callUUID)));
            int size = this.callStack.size();
            sLogger.v("call stack size=" + size + " action=" + callAction.name());
            if (size <= 0) {
                return;
            }
            if (callAction == CallAction.CALL_REJECT) {
                restoreInfo((Info) this.callStack.pop());
            } else if (callAction == CallAction.CALL_END) {
                restoreInfo((Info) this.callStack.pop());
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    void restoreInfo(Info info) {
        this.bus.post(new CallEvent(Boolean.valueOf(this.incomingCall), Boolean.valueOf(this.answered), this.number, this.contact, Long.valueOf(toSeconds(this.callStartMs)), Long.valueOf(this.duration)));
        this.number = info.number;
        this.normalizedNumber = info.normalizedNumber;
        this.contact = info.contact;
        this.incomingCall = info.incomingCall;
        this.answered = info.answered;
        this.callStartMs = info.callStartMs;
        this.callUUID = info.callUUID;
        this.duration = info.duration;
        this.userRejectedCall = info.userRejectedCall;
        this.userCancelledCall = info.userCancelledCall;
        this.putOnStack = info.putOnStack;
        this.callMuted = info.callMuted;
        sLogger.v("stack restored [" + this.contact + " , " + this.number + " , " + this.normalizedNumber + "]");
        if (!isIos()) {
            this.handler.postDelayed(new Runnable() {
                public void run() {
                    CallManager.this.bus.post(new RemoteEvent(CallManager.PHONE_STATUS_REQUEST));
                }
            }, 1000);
        } else if (this.answered) {
            this.phoneStatus = PhoneStatus.PHONE_OFFHOOK;
        }
    }

    void sendNotification() {
        NotificationManager.getInstance().addNotification(this.callNotification);
    }

    void removeNotification() {
        NotificationManager.getInstance().removeNotification(this.callNotification.getId());
    }

    public PhoneStatus getPhoneStatus() {
        return this.phoneStatus;
    }

    public void dial(String number, String callUUID, String contact) {
        if (number == null) {
            sLogger.e("null number passed contact[" + contact + "]");
            return;
        }
        this.state = CallNotificationState.DIALING;
        this.phoneStatus = PhoneStatus.PHONE_DIALING;
        this.dialSet = true;
        this.incomingCall = false;
        this.number = number;
        this.normalizedNumber = PhoneUtil.normalizeNumber(number);
        this.contact = contact;
        sendNotification();
        sendCallAction(CallAction.CALL_DIAL, number);
    }

    @Subscribe
    public void onDeviceInfoAvailable(DeviceInfoAvailable deviceInfoAvailable) {
        if (deviceInfoAvailable.deviceInfo != null && deviceInfoAvailable.deviceInfo.platform == Platform.PLATFORM_Android) {
            this.bus.post(new RemoteEvent(new PhoneStatusRequest()));
        }
    }

    private boolean isIos() {
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        return Platform.PLATFORM_iOS.equals(remoteDeviceManager.isRemoteDeviceConnected() ? remoteDeviceManager.getRemoteDevicePlatform() : null);
    }

    @Subscribe
    public void onPhoneStatusResponse(PhoneStatusResponse response) {
        sLogger.v("onPhoneStatusResponse [" + response.status + "] [" + response.callStatus + "]");
        if (response.status == RequestStatus.REQUEST_SUCCESS && response.callStatus != null) {
            if (this.state == CallNotificationState.IDLE || this.state == CallNotificationState.IN_PROGRESS) {
                sLogger.v("onPhoneStatusResponse fwding event");
                onPhoneEvent(response.callStatus);
            }
        }
    }

    @Subscribe
    public void onPhoneBatteryEvent(PhoneBatteryStatus event) {
        sLogger.v("[Battery-phone] status[" + event.status + "] level[" + event.level + "] charging[" + event.charging + "]");
        PhoneBatteryNotification.showBatteryToast(event);
    }

    private boolean isCallActionResponseRequired(CallAction callAction) {
        if (callAction == null) {
            return false;
        }
        switch (callAction) {
            case CALL_ACCEPT:
            case CALL_END:
            case CALL_DIAL:
                return true;
            default:
                return false;
        }
    }

    public boolean isCallInProgress() {
        if (this.phoneStatus == PhoneStatus.PHONE_OFFHOOK || this.phoneStatus == PhoneStatus.PHONE_DIALING || this.phoneStatus == PhoneStatus.PHONE_RINGING) {
            return true;
        }
        return false;
    }

    public void disconnect() {
        sLogger.v("got disconnect event, setting to IDLE");
        this.phoneStatus = PhoneStatus.PHONE_IDLE;
        this.state = CallNotificationState.IDLE;
        this.incomingCall = false;
        this.number = null;
        this.normalizedNumber = null;
        this.contact = null;
    }

    private boolean isNotificationAllowed() {
        if ((!this.incomingCall || (this.incomingCall && GlanceHelper.isPhoneNotificationEnabled())) && NotificationManager.getInstance().isPhoneNotificationsEnabled()) {
            return true;
        }
        return false;
    }

    public int getCurrentCallDuration() {
        if (this.callStartMs <= 0) {
            return -1;
        }
        return (int) ((SystemClock.elapsedRealtime() - this.callStartMs) / 1000);
    }
}
