package com.navdy.hud.app.framework.music;

import android.os.Bundle;

import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import android.view.KeyEvent;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.TitleSubtitleViewHolder;
import java.util.ArrayList;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.service.library.events.ui.ShowScreen;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.content.res.Resources;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.HudApplication;
import java.util.List;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.service.library.events.ui.Screen;
import flow.Flow;
import com.navdy.service.library.log.Logger;
import flow.Layout;
import com.navdy.hud.app.screen.BaseScreen;

@Layout(R.layout.screen_music_details)
public class MusicDetailsScreen extends BaseScreen
{
    private static final Logger sLogger;

    static {
        sLogger = new Logger(MusicDetailsScreen.class);
    }

    @Override
    public int getAnimationIn(final Flow.Direction direction) {
        return -1;
    }

    @Override
    public int getAnimationOut(final Flow.Direction direction) {
        return -1;
    }

    @Override
    public Object getDaggerModule() {
        return new Module();
    }

    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }

    @Override
    public Screen getScreen() {
        return Screen.SCREEN_MUSIC_DETAILS;
    }

    @dagger.Module(addsTo = Main.Module.class, injects = { MusicDetailsView.class })
    public class Module
    {
    }

    @Singleton
    public static class Presenter extends BasePresenter<MusicDetailsView>
    {
        private static final int MUSIC_CONTROL_TRAY_POS = 0;
        private static final int music_selected_color;
        private static final int music_unselected_color;
        private static final String[] subTitles;
        private static final VerticalList.Model thumbsDown;
        private static final VerticalList.Model thumbsUp;
        private static final String[] titles;
        private boolean animateIn;
        @Inject
        Bus bus;
        private boolean closed;
        private int currentCounter;
        private int currentTitleSelection;
        private List<VerticalList.Model> data;
        private boolean handledSelection;
        private boolean keyPressed;
        private boolean registered;
        private int thumbPos;

        static {
            final Resources resources = HudApplication.getAppContext().getResources();
            music_selected_color = resources.getColor(R.color.music_details_sel);
            music_unselected_color = resources.getColor(R.color.icon_bk_color_unselected);
            titles = new String[] { "The Mother We Both Share", "Tomorrow, Tomorrow, I love you", "Who Let the Dogs out" };
            subTitles = new String[] { "Churches Your Vibe, Your Tribe Vol. 1", "Old Classics, Vol 2", "woof woof" };
            thumbsUp = IconBkColorViewHolder.buildModel(R.id.music_thumbsup, R.drawable.icon_thumbs_up, Presenter.music_selected_color, Presenter.music_unselected_color, Presenter.music_selected_color, "Like", null);
            thumbsDown = IconBkColorViewHolder.buildModel(R.id.music_thumbsdown, R.drawable.icon_thumbs_dn, Presenter.music_selected_color, Presenter.music_unselected_color, Presenter.music_selected_color, "Dislike", null);
        }

        void close() {
            if (this.closed) {
                MusicDetailsScreen.sLogger.v("already closed");
            }
            else {
                this.closed = true;
                MusicDetailsScreen.sLogger.v("close");
                AnalyticsSupport.recordMenuSelection("exit");
                final MusicDetailsView musicDetailsView = this.getView();
                if (musicDetailsView != null) {
                    musicDetailsView.vmenuComponent.animateOut(new DefaultAnimationListener() {
                        @Override
                        public void onAnimationEnd(final Animator animator) {
                            MusicDetailsScreen.sLogger.v("post back");
                            Presenter.this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
                        }
                    });
                }
            }
        }

        void init(final MusicDetailsView musicDetailsView) {
            musicDetailsView.vmenuComponent.setOverrideDefaultKeyEvents(InputManager.CustomKeyEvent.LONG_PRESS, true);
            final ArrayList<VerticalList.Model> data = new ArrayList<>();
            data.add(TitleSubtitleViewHolder.buildModel(Presenter.titles[this.currentTitleSelection], Presenter.subTitles[this.currentTitleSelection]));
            ++this.currentTitleSelection;
            final int[] array = { Presenter.music_selected_color, Presenter.music_selected_color, Presenter.music_selected_color };
            data.add(IconOptionsViewHolder.buildModel(R.id.music_controls, new int[] { R.drawable.icon_prev_music, R.drawable.icon_play_music, R.drawable.icon_next_music }, new int[] { R.id.music_rewind, R.id.music_play, R.id.music_forward }, array, new int[] { Presenter.music_unselected_color, Presenter.music_unselected_color, Presenter.music_unselected_color }, array, 1, false));
            final int n = 2 + 1;
            data.add(IconBkColorViewHolder.buildModel(2, R.drawable.icon_playlist, Presenter.music_selected_color, Presenter.music_unselected_color, Presenter.music_selected_color, "Playlist/Album name", null));
            final int n2 = n + 1;
            data.add(IconBkColorViewHolder.buildModel(n, R.drawable.icon_heart, Presenter.music_selected_color, Presenter.music_unselected_color, Presenter.music_selected_color, "Love", null));
            this.thumbPos = data.size();
            data.add(Presenter.thumbsUp);
            final int n3 = n2 + 1;
            data.add(IconBkColorViewHolder.buildModel(n2, R.drawable.icon_shuffle, Presenter.music_selected_color, Presenter.music_unselected_color, Presenter.music_selected_color, "Shuffle", null));
            final int n4 = n3 + 1;
            data.add(IconBkColorViewHolder.buildModel(n3, R.drawable.icon_stations, Presenter.music_selected_color, Presenter.music_unselected_color, Presenter.music_selected_color, "Stations", null));
            data.add(IconBkColorViewHolder.buildModel(n4, R.drawable.icon_bookmark, Presenter.music_selected_color, Presenter.music_unselected_color, Presenter.music_selected_color, "Bookmark", null));
            this.data = data;
            musicDetailsView.vmenuComponent.updateView(this.data, MUSIC_CONTROL_TRAY_POS, false);
        }

        boolean isClosed() {
            return this.closed;
        }

        boolean isItemClickable(final VerticalList.ItemSelectionState itemSelectionState) {
            return true;
        }

        @Subscribe
        public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
            switch (connectionStateChange.state) {
                case CONNECTION_DISCONNECTED:
                    MusicDetailsScreen.sLogger.v("disconnected");
                    this.close();
                    break;
            }
        }

        @Subscribe
        public void onKeyEvent(final KeyEvent keyEvent) {
            if (InputManager.isCenterKey(keyEvent.getKeyCode())) {
                final MusicDetailsView musicDetailsView = this.getView();
                if (musicDetailsView != null && !musicDetailsView.vmenuComponent.isCloseMenuVisible() && musicDetailsView.vmenuComponent.verticalList.getCurrentPosition() == 0) {
                    final VerticalViewHolder currentViewHolder = musicDetailsView.vmenuComponent.verticalList.getCurrentViewHolder();
                    if (currentViewHolder != null && currentViewHolder instanceof IconOptionsViewHolder) {
                        final IconOptionsViewHolder iconOptionsViewHolder = (IconOptionsViewHolder)currentViewHolder;
                        final int currentSelection = iconOptionsViewHolder.getCurrentSelection();
                        if (currentSelection != 1) {
                            int n;
                            if (currentSelection == 2) {
                                n = 1;
                            }
                            else {
                                n = 0;
                            }
                            if (keyEvent.getAction() == 0) {
                                if (!this.keyPressed) {
                                    this.keyPressed = true;
                                    iconOptionsViewHolder.selectionDown();
                                }
                                final int currentCounter = this.currentCounter;
                                if (n != 0) {
                                    ++this.currentCounter;
                                }
                                else if (this.currentCounter > 1) {
                                    --this.currentCounter;
                                }
                                musicDetailsView.counter.setText(String.valueOf(currentCounter));
                            }
                            else if (keyEvent.getAction() == 1 && this.keyPressed) {
                                this.keyPressed = false;
                                iconOptionsViewHolder.selectionUp();
                                this.reset(musicDetailsView);
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void onLoad(final Bundle bundle) {
            this.currentCounter = 1;
            this.currentTitleSelection = 0;
            this.animateIn = false;
            this.keyPressed = false;
            this.reset(null);
            this.bus.register(this);
            this.registered = true;
            this.updateView();
            super.onLoad(bundle);
        }

        public void onUnload() {
            if (this.registered) {
                this.registered = false;
                this.bus.unregister(this);
            }
            this.reset(null);
            super.onUnload();
        }

        void reset(final MusicDetailsView musicDetailsView) {
            this.closed = false;
            this.handledSelection = false;
            if (musicDetailsView != null) {
                musicDetailsView.vmenuComponent.unlock();
            }
        }

        void resetSelectedItem() {
            MusicDetailsScreen.sLogger.v("resetSelectedItem");
            this.handledSelection = false;
            if (!this.animateIn) {
                this.animateIn = true;
                final MusicDetailsView musicDetailsView = this.getView();
                if (musicDetailsView != null) {
                    musicDetailsView.vmenuComponent.animateIn(null);
                }
            }
        }

        void selectItem(final VerticalList.ItemSelectionState itemSelectionState) {
            MusicDetailsScreen.sLogger.v("select id:" + itemSelectionState.id + "," + itemSelectionState.pos + " subid=" + itemSelectionState.subId);
            if (this.handledSelection) {
                MusicDetailsScreen.sLogger.v("already handled [" + itemSelectionState.id + "], " + itemSelectionState.pos);
            }
            else {
                final MusicDetailsView musicDetailsView = this.getView();
                Label_0164: {
                    if (musicDetailsView != null) {
                        switch (itemSelectionState.id) {
                            default:
                                this.close();
                                break;
                            case R.id.music_controls:
                                switch (itemSelectionState.subId) {
                                    default:
                                        break Label_0164;
                                    case R.id.music_forward:
                                        this.reset(musicDetailsView);
                                        break Label_0164;
                                    case R.id.music_play: {
                                        this.reset(musicDetailsView);
                                        if (this.currentTitleSelection == Presenter.titles.length) {
                                            this.currentTitleSelection = 0;
                                        }
                                        final VerticalList.Model buildModel = TitleSubtitleViewHolder.buildModel(Presenter.titles[this.currentTitleSelection], Presenter.subTitles[this.currentTitleSelection]);
                                        ++this.currentTitleSelection;
                                        musicDetailsView.vmenuComponent.verticalList.refreshData(itemSelectionState.pos, buildModel);
                                        break Label_0164;
                                    }
                                    case R.id.music_rewind:
                                        this.reset(musicDetailsView);
                                        break Label_0164;
                                    case R.id.music_pause:
                                        this.reset(musicDetailsView);
                                        break Label_0164;
                                }
//                                break;
                            case R.id.music_thumbsdown:
                                MusicDetailsScreen.sLogger.v("thumbsup");
                                this.reset(musicDetailsView);
                                musicDetailsView.vmenuComponent.verticalList.refreshData(this.thumbPos, Presenter.thumbsUp);
                                break;
                            case R.id.music_thumbsup:
                                MusicDetailsScreen.sLogger.v("thumbsdown");
                                this.reset(musicDetailsView);
                                musicDetailsView.vmenuComponent.verticalList.refreshData(this.thumbPos, Presenter.thumbsDown);
                                break;
                        }
                    }
                }
            }
        }

        protected void updateView() {
            final MusicDetailsView musicDetailsView = this.getView();
            if (musicDetailsView != null) {
                this.init(musicDetailsView);
            }
        }
    }
}
