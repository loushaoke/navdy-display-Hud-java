package com.navdy.hud.app.framework.network;

public class NetworkStatCache {
    private com.navdy.hud.app.framework.network.DnsCache dnsCache;
    private java.util.HashMap networkStatMap;
    private java.util.HashMap networkStatMapBoot;

    NetworkStatCache(com.navdy.hud.app.framework.network.DnsCache a) {
        this.networkStatMap = new java.util.HashMap();
        this.networkStatMapBoot = new java.util.HashMap();
        this.dnsCache = a;
    }

    private void dumpBootStat(com.navdy.service.library.log.Logger a) {
        if (this.networkStatMapBoot.size() != 0) {
            java.util.Iterator a0 = this.networkStatMapBoot.values().iterator();
            a.v("dump-boot total endpoints:" + this.networkStatMapBoot.size());
            while(a0.hasNext()) {
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a2 = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo) a0.next();
                String s = this.dnsCache.getHostnamefromIP(a2.destIP);
                if (s == null) {
                    s = a2.destIP;
                }
                a.v("dump-boot  dest[" + s + "] tx[" + a2.txBytes + "] rx[" + a2.rxBytes + "]");
            }
        }
    }

    private void dumpSessionStat(com.navdy.service.library.log.Logger a, boolean b) {
        if (this.networkStatMap.size() != 0) {
            java.util.Iterator a0 = this.networkStatMap.keySet().iterator();
            a.v("dump-session total connections:" + this.networkStatMap.size());
            while(a0.hasNext()) {
                int i = (Integer) a0.next();
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a2 = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo)this.networkStatMap.get(Integer.valueOf(i));
                String s = this.dnsCache.getHostnamefromIP(a2.destIP);
                if (s == null) {
                    s = a2.destIP;
                }
                a.v("dump-session fd[" + i + "] dest[" + s + "] tx[" + a2.txBytes + "] rx[" + a2.rxBytes + "]");
            }
            if (b) {
                this.clear();
            }
        }
    }

    public void addStat(String s, int i, int i0, int i1) {
        synchronized(this) {
            com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo)this.networkStatMap.get(Integer.valueOf(i1));
            if (a != null) {
                a.txBytes = a.txBytes + i;
                a.rxBytes = a.rxBytes + i0;
            } else {
                java.util.HashMap a0 = this.networkStatMap;
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a1 = new com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo(i, i0, s);
                a0.put(i1, a1);
            }
            if (s != null) {
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a2 = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo)this.networkStatMapBoot.get(s);
                if (a2 != null) {
                    a2.txBytes = a2.txBytes + i;
                    a2.rxBytes = a2.rxBytes + i0;
                } else {
                    this.networkStatMapBoot.put(s, new com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo(i, i0, s));
                }
            }
        }
        /*monexit(this)*/
    }

    public void clear() {
        synchronized(this) {
            this.networkStatMap.clear();
        }
        /*monexit(this)*/
    }

    public void dump(com.navdy.service.library.log.Logger a, boolean b) {
        synchronized(this) {
            this.dumpSessionStat(a, b);
            this.dumpBootStat(a);
        }
        /*monexit(this)*/
    }

    public java.util.List getBootStat() {
        java.util.List a = null;
        synchronized(this) {
            a = this.getStat(this.networkStatMapBoot.values().iterator());
        }
        /*monexit(this)*/
        return a;
    }

    public java.util.List getSessionStat() {
        java.util.List a = null;
        synchronized(this) {
            a = this.getStat(this.networkStatMap.values().iterator());
        }
        /*monexit(this)*/
        return a;
    }

    private java.util.List getStat(java.util.Iterator a) {
        java.util.ArrayList a0;
        synchronized(this) {
            a0 = new java.util.ArrayList();
            while(a.hasNext()) {
                com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo a2 = (com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo) a.next();
                String s = this.dnsCache.getHostnamefromIP(a2.destIP);
                if (s == null) {
                    s = a2.destIP;
                }
                a0.add(new com.navdy.hud.app.framework.network.NetworkStatCache$NetworkStatCacheInfo(a2.txBytes, a2.rxBytes, s));
            }
        }
        /*monexit(this)*/
        return a0;
    }
}
