package com.navdy.hud.app.framework.voice;

import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction;
import com.navdy.service.library.events.input.Gesture;
import android.view.animation.Interpolator;
import com.navdy.hud.app.manager.MusicManager;
import com.squareup.wire.Wire;
import com.navdy.hud.app.profile.HudLocale;
import android.text.TextUtils;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.manager.InputManager;
import android.content.res.Resources;
import java.util.ArrayList;
import butterknife.ButterKnife;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.content.Context;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.animation.AnimatorSet.Builder;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.framework.DriverProfileHelper;
import android.view.LayoutInflater;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.animation.AnimatorInflater;
import android.animation.TimeInterpolator;
import android.view.animation.BounceInterpolator;
import android.animation.ObjectAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.Animator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.HudApplication;
import java.util.concurrent.TimeUnit;
import android.widget.TextView;
import com.navdy.service.library.events.destination.Destination;
import java.util.List;
import java.util.Locale;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.os.Handler;
import com.navdy.hud.app.framework.notifications.INotificationController;
import butterknife.InjectView;
import android.animation.AnimatorSet;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.events.audio.VoiceSearchResponse;
import java.util.HashMap;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.framework.notifications.INotification;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;

public class VoiceSearchNotification implements INotification, ChoiceLayout2.IListener, VoiceSearchHandler.VoiceSearchUserInterface
{
    public static final int ANIMATION_DURATION = 300;
    private static final HashMap<VoiceSearchResponse.VoiceSearchError, Integer> BADGE_ICON_MAPPING;
    private static final HashMap<VoiceSearchResponse.VoiceSearchError, Integer> ERROR_TEXT_MAPPING;
    public static final int INITIALIZATION_DELAY_MILLIS;
    public static final int INPUT_DELAY_MILLIS;
    public static final int RESULT_SELECTION_TIMEOUT = 10000;
    public static final int SEARCH_DELAY_MILLIS;
    private static final HashMap<VoiceSearchStateInternal, Integer> STATE_NAME_MAPPING;
    private static final Logger sLogger;
    public AnimatorSet animatorSet;
    @InjectView(R.id.choiceLayout)
    public ChoiceLayout2 choiceLayout;
    private INotificationController controller;
    private VoiceSearchResponse.VoiceSearchError error;
    private Handler handler;
    @InjectView(R.id.image_voice_search_active)
    public ImageView imageActive;
    @InjectView(R.id.image_voice_search_inactive)
    public ImageView imageInactive;
    @InjectView(R.id.img_processing_badge)
    public ImageView imageProcessing;
    @InjectView(R.id.img_status_badge)
    public ImageView imageStatus;
    private boolean isListeningOverBluetooth;
    private ViewGroup layout;
    private AnimatorSet listeningFeedbackAnimation;
    @InjectView(R.id.voice_search_listening_feedback)
    public View listeningFeedbackView;
    private Locale locale;
    private AnimatorSet mainImageAnimation;
    private boolean multipleResultsMode;
    private AnimatorSet processingImageAnimation;
    private volatile boolean responseFromClientTimedOut;
    private List<Destination> results;
    @InjectView(R.id.results_count)
    public TextView resultsCount;
    @InjectView(R.id.results_count_container)
    public ViewGroup resultsCountContainer;
    private AnimatorSet statusImageAnimation;
    @InjectView(R.id.subStatus)
    public TextView subStatus;
    private List<ChoiceLayout2.Choice> voiceSearchActiveChoices;
    private List<ChoiceLayout2.Choice> voiceSearchFailedChoices;
    private VoiceSearchHandler voiceSearchHandler;
    private VoiceSearchStateInternal voiceSearchState;
    private List<ChoiceLayout2.Choice> voiceSearchSuccessResultsChoices;
    private Runnable waitingTimeoutRunnable;
    private List<String> words;
    
    static {
        sLogger = new Logger(VoiceSearchNotification.class);
        INITIALIZATION_DELAY_MILLIS = (int)TimeUnit.SECONDS.toMillis(30L);
        SEARCH_DELAY_MILLIS = (int)TimeUnit.MINUTES.toMillis(2L);
        INPUT_DELAY_MILLIS = (int)TimeUnit.MINUTES.toMillis(2L);
        HudApplication.getAppContext().getResources();
        (ERROR_TEXT_MAPPING = new HashMap<VoiceSearchResponse.VoiceSearchError, Integer>()).put(VoiceSearchResponse.VoiceSearchError.FAILED_TO_START, R.string.voice_search_failed);
        VoiceSearchNotification.ERROR_TEXT_MAPPING.put(VoiceSearchResponse.VoiceSearchError.OFFLINE, R.string.voice_search_offline);
        VoiceSearchNotification.ERROR_TEXT_MAPPING.put(VoiceSearchResponse.VoiceSearchError.NEED_PERMISSION, R.string.voice_search_need_permission);
        VoiceSearchNotification.ERROR_TEXT_MAPPING.put(VoiceSearchResponse.VoiceSearchError.NO_WORDS_RECOGNIZED, R.string.voice_search_failed);
        VoiceSearchNotification.ERROR_TEXT_MAPPING.put(VoiceSearchResponse.VoiceSearchError.NOT_AVAILABLE, R.string.voice_search_failed);
        VoiceSearchNotification.ERROR_TEXT_MAPPING.put(VoiceSearchResponse.VoiceSearchError.NO_RESULTS_FOUND, R.string.voice_search_no_results_found);
        VoiceSearchNotification.ERROR_TEXT_MAPPING.put(VoiceSearchResponse.VoiceSearchError.AMBIENT_NOISE, R.string.voice_search_ambient_noise);
        (BADGE_ICON_MAPPING = new HashMap<VoiceSearchResponse.VoiceSearchError, Integer>()).put(VoiceSearchResponse.VoiceSearchError.FAILED_TO_START, R.drawable.icon_badge_alert);
        VoiceSearchNotification.BADGE_ICON_MAPPING.put(VoiceSearchResponse.VoiceSearchError.OFFLINE, R.drawable.icon_badge_alert);
        VoiceSearchNotification.BADGE_ICON_MAPPING.put(VoiceSearchResponse.VoiceSearchError.NEED_PERMISSION, R.drawable.icon_badge_alert);
        VoiceSearchNotification.BADGE_ICON_MAPPING.put(VoiceSearchResponse.VoiceSearchError.NO_RESULTS_FOUND, R.drawable.icon_badge_voice_search);
        VoiceSearchNotification.BADGE_ICON_MAPPING.put(VoiceSearchResponse.VoiceSearchError.NOT_AVAILABLE, R.drawable.icon_badge_alert);
        VoiceSearchNotification.BADGE_ICON_MAPPING.put(VoiceSearchResponse.VoiceSearchError.AMBIENT_NOISE, R.drawable.icon_badge_alert);
        (STATE_NAME_MAPPING = new HashMap<VoiceSearchStateInternal, Integer>()).put(VoiceSearchStateInternal.STARTING, R.string.voice_search_wait);
        VoiceSearchNotification.STATE_NAME_MAPPING.put(VoiceSearchStateInternal.LISTENING, R.string.voice_search_speak_now);
        VoiceSearchNotification.STATE_NAME_MAPPING.put(VoiceSearchStateInternal.FAILED, R.string.voice_search_failed);
        VoiceSearchNotification.STATE_NAME_MAPPING.put(VoiceSearchStateInternal.SEARCHING, R.string.voice_search_searching);
    }
    
    public VoiceSearchNotification() {
        this.multipleResultsMode = false;
        this.voiceSearchState = VoiceSearchStateInternal.IDLE;
        this.waitingTimeoutRunnable = new Runnable() {
            @Override
            public void run() {
                if (VoiceSearchNotification.this.controller != null) {
                    VoiceSearchNotification.this.multipleResultsMode = false;
                    VoiceSearchNotification.this.error = null;
                    VoiceSearchNotification.this.responseFromClientTimedOut = true;
                    VoiceSearchNotification.this.updateUI(VoiceSearchStateInternal.FAILED);
                }
            }
        };
    }
    
    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification("navdy#voicesearch#notif");
    }
    
    public static AnimatorSet getBadgeAnimation(final View view, final boolean b) {
        view.setAlpha(1.0f);
        final AnimatorSet set = new AnimatorSet();
        set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            @Override
            public void onAnimationStart(final Animator animator) {
                view.setVisibility(View.VISIBLE);
                super.onAnimationStart(animator);
            }
        });
        if (b) {
            view.setTranslationY(-10.0f);
            final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "translationY", new float[] { -10.0f, 0.0f });
            set.setDuration(200L);
            set.play((Animator)ofFloat);
            set.setInterpolator((TimeInterpolator)new BounceInterpolator());
        }
        else {
            view.setTranslationY(0.0f);
            final ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(view, "alpha", new float[] { 1.0f, 0.0f });
            set.setDuration(200L);
            set.play((Animator)ofFloat2);
        }
        return set;
    }
    
    public static AnimatorSet getListeningFeedbackDefaultAnimation(final View target) {
        final AnimatorSet set = (AnimatorSet)AnimatorInflater.loadAnimator(target.getContext(), R.animator.listening_feedback_animation);
        set.setTarget(target);
        set.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
            private boolean canceled = false;
            
            @Override
            public void onAnimationCancel(final Animator animator) {
                this.canceled = true;
            }
            
            @Override
            public void onAnimationEnd(final Animator animator) {
                if (!this.canceled) {
                    set.start();
                }
            }
            
            @Override
            public void onAnimationStart(final Animator animator) {
                this.canceled = false;
            }
        });
        return set;
    }
    
    public static Animator getProcessingBadgeAnimation(final View view) {
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, View.ROTATION, new float[] { 360.0f });
        ((Animator)ofFloat).setDuration(300L);
        ((Animator)ofFloat).setInterpolator((TimeInterpolator)new AccelerateDecelerateInterpolator());
        return (Animator)ofFloat;
    }
    
    private void setChoices(final VoiceSearchStateInternal voiceSearchStateInternal) {
        switch (voiceSearchStateInternal) {
            default:
                this.choiceLayout.setVisibility(GONE);
                break;
            case LISTENING:
            case SEARCHING:
                this.choiceLayout.setVisibility(View.VISIBLE);
                this.choiceLayout.setChoices(this.voiceSearchActiveChoices, 0, (ChoiceLayout2.IListener)this);
                break;
            case FAILED:
                this.choiceLayout.setVisibility(View.VISIBLE);
                this.choiceLayout.setChoices(this.voiceSearchFailedChoices, 0, (ChoiceLayout2.IListener)this);
                break;
            case RESULTS:
                if (this.results != null && this.results.size() > 1) {
                    this.choiceLayout.setVisibility(View.VISIBLE);
                    this.choiceLayout.setChoices(this.voiceSearchSuccessResultsChoices, 0, (ChoiceLayout2.IListener)this);
                    break;
                }
                break;
        }
    }
    
    private void showHideVoiceSearchTipsIfNeeded(final boolean b) {
        VoiceSearchNotification.sLogger.d("showHideVoiceSearchTipsIfNeeded, show : " + b);
        final Main rootScreen = RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
        if (b) {
            VoiceSearchNotification.sLogger.d("Should show voice search tips :" + this.voiceSearchHandler.shouldShowVoiceSearchTipsToUser());
            if (this.voiceSearchHandler.shouldShowVoiceSearchTipsToUser()) {
                final ViewGroup viewGroup = (ViewGroup)LayoutInflater.from(HudApplication.getAppContext()).inflate(R.layout.voice_search_tips_layout, rootScreen.getNotificationExtensionViewHolder(), false);
                final VoiceSearchTipsView voiceSearchTipsView = (VoiceSearchTipsView)viewGroup.findViewById(R.id.voice_search_tips);
                voiceSearchTipsView.setListeningOverBluetooth(this.isListeningOverBluetooth);
                if (this.locale != null) {
                    voiceSearchTipsView.setVoiceSearchLocale(this.locale);
                }
                else {
                    voiceSearchTipsView.setVoiceSearchLocale(DriverProfileHelper.getInstance().getCurrentProfile().getLocale());
                }
                rootScreen.addNotificationExtensionView((View)viewGroup);
                this.voiceSearchHandler.showedVoiceSearchTipsToUser();
            }
        }
        else {
            VoiceSearchNotification.sLogger.d("Remove notification extension");
            rootScreen.removeNotificationExtensionView();
        }
    }
    
    private void startVoiceSearch() {
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        this.responseFromClientTimedOut = false;
        this.updateUI(VoiceSearchStateInternal.STARTING);
        this.voiceSearchHandler.startVoiceSearch();
        this.handler.postDelayed(this.waitingTimeoutRunnable, (long)VoiceSearchNotification.INITIALIZATION_DELAY_MILLIS);
    }
    
    private void updateUI(final VoiceSearchStateInternal voiceSearchStateInternal) {
        this.updateUI(voiceSearchStateInternal, this.error, this.results);
    }
    
    private void updateUI(final VoiceSearchStateInternal voiceSearchStateInternal, final VoiceSearchResponse.VoiceSearchError voiceSearchError, final List<Destination> list) {
        VoiceSearchNotification.sLogger.d("updateUI newState :" + voiceSearchStateInternal + " , Error :" + voiceSearchError);
        boolean b;
        if (this.voiceSearchState != voiceSearchStateInternal) {
            b = true;
        }
        else {
            b = false;
        }
        if (b) {
            VoiceSearchNotification.sLogger.d("State Changed From : " + this.voiceSearchState + " , New state : " + voiceSearchStateInternal);
        }
        this.voiceSearchState = voiceSearchStateInternal;
        final AnimatorSet set = new AnimatorSet();
        final Builder Builder = null;
        if (b) {
            this.setChoices(voiceSearchStateInternal);
        }
        Label_0177: {
            switch (voiceSearchStateInternal) {
                default:
                    if (b) {
                        this.subStatus.setText((int)VoiceSearchNotification.STATE_NAME_MAPPING.get(voiceSearchStateInternal));
                        break;
                    }
                    break;
                case FAILED:
                    if (voiceSearchError == null || this.responseFromClientTimedOut) {
                        this.subStatus.setText(this.voiceSearchState.getStatusTextResId());
                        break;
                    }
                    switch (voiceSearchError) {
                        default: {
                            Integer n;
                            if (voiceSearchError == null) {
                                n = null;
                            }
                            else {
                                n = VoiceSearchNotification.ERROR_TEXT_MAPPING.get(voiceSearchError);
                            }
                            Integer value = n;
                            if (n == null) {
                                value = R.string.voice_search_failed;
                                VoiceSearchNotification.sLogger.e(String.format("unhandled error response: %s", voiceSearchError));
                            }
                            this.subStatus.setText((int)value);
                            break Label_0177;
                        }
                        case NO_RESULTS_FOUND:
                            this.subStatus.setText((CharSequence)"");
                            break Label_0177;
                    }
                case IDLE:
                    if (b) {
                        this.subStatus.setText((CharSequence)"");
                        break;
                    }
                    break;
                case RESULTS:
                    this.subStatus.setText((CharSequence)"");
                    break;
            }
        }
        Object o = null;
        if (b) {
            this.resultsCountContainer.setVisibility(INVISIBLE);
            if (this.mainImageAnimation != null) {
                this.mainImageAnimation.removeAllListeners();
                this.mainImageAnimation.cancel();
            }
            this.mainImageAnimation = new AnimatorSet();
            final AnimatorSet mainImageAnimation = this.mainImageAnimation;
            switch (voiceSearchStateInternal) {
                default:
                    this.imageInactive.setVisibility(View.VISIBLE);
                    if (this.imageActive.getAlpha() > 0.0f) {
                        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.imageActive, "alpha", new float[] { 1.0f, 0.0f });
                        ofFloat.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                            @Override
                            public void onAnimationEnd(final Animator animator) {
                                VoiceSearchNotification.this.imageActive.setAlpha(0.0f);
                            }
                        });
                        mainImageAnimation.play((Animator)ofFloat);
                        break;
                    }
                    break;
                case LISTENING: {
                    final ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.imageActive, "alpha", new float[] { 0.0f, 1.0f });
                    ofFloat2.addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                        @Override
                        public void onAnimationEnd(final Animator animator) {
                            VoiceSearchNotification.this.imageActive.setAlpha(1.0f);
                        }
                    });
                    mainImageAnimation.play((Animator)ofFloat2);
                    break;
                }
            }
            o = mainImageAnimation;
            switch (voiceSearchStateInternal) {
                default:
                    o = mainImageAnimation;
                    break;
                case RESULTS:
                    if (list != null && list.size() > 0) {
                        this.resultsCount.setText((CharSequence)Integer.toString(list.size()));
                    }
                    this.resultsCountContainer.setVisibility(View.VISIBLE);
                    this.imageInactive.setVisibility(INVISIBLE);
                    o = mainImageAnimation;
                case IDLE:
                    break;
                case FAILED:
                    o = mainImageAnimation;
                    if (voiceSearchError == VoiceSearchResponse.VoiceSearchError.NO_RESULTS_FOUND) {
                        this.resultsCountContainer.setVisibility(View.VISIBLE);
                        this.imageInactive.setVisibility(INVISIBLE);
                        o = mainImageAnimation;
                        break;
                    }
                    break;
            }
        }
        AnimatorSet processingImageAnimation;
        final AnimatorSet set2 = processingImageAnimation = null;
        if (b) {
            if (this.statusImageAnimation != null) {
                this.statusImageAnimation.removeAllListeners();
                this.statusImageAnimation.cancel();
                this.statusImageAnimation = null;
            }
            this.imageStatus.setVisibility(INVISIBLE);
            if (this.processingImageAnimation != null) {
                this.processingImageAnimation.cancel();
            }
            this.imageProcessing.setVisibility(INVISIBLE);
            this.statusImageAnimation = new AnimatorSet();
            processingImageAnimation = set2;
            switch (voiceSearchStateInternal) {
                default:
                    processingImageAnimation = set2;
                    break;
                case RESULTS:
                    this.imageStatus.setImageResource(R.drawable.icon_badge_voice_search);
                    this.statusImageAnimation.play((Animator)getBadgeAnimation((View)this.imageStatus, true));
                    processingImageAnimation = set2;
                    break;
                case LISTENING:
                    if (this.isListeningOverBluetooth) {
                        this.imageStatus.setImageResource(R.drawable.icon_badge_b_t);
                    }
                    else {
                        this.imageStatus.setImageResource(R.drawable.icon_badge_phone);
                    }
                    this.statusImageAnimation.play((Animator)getBadgeAnimation((View)this.imageStatus, true));
                    processingImageAnimation = set2;
                    break;
                case STARTING:
                case SEARCHING:
                    this.imageProcessing.setVisibility(View.VISIBLE);
                    if (this.processingImageAnimation == null) {
                        (this.processingImageAnimation = new AnimatorSet()).addListener((Animator.AnimatorListener)new DefaultAnimationListener() {
                            private boolean canceled = false;
                            
                            @Override
                            public void onAnimationCancel(final Animator animator) {
                                this.canceled = true;
                            }
                            
                            @Override
                            public void onAnimationEnd(final Animator animator) {
                                if (!this.canceled) {
                                    VoiceSearchNotification.this.processingImageAnimation.setStartDelay(33L);
                                    VoiceSearchNotification.this.processingImageAnimation.start();
                                }
                            }
                            
                            @Override
                            public void onAnimationStart(final Animator animator) {
                                VoiceSearchNotification.this.imageProcessing.setRotation(0.0f);
                                this.canceled = false;
                            }
                        });
                        this.processingImageAnimation.play(getProcessingBadgeAnimation((View)this.imageProcessing));
                    }
                    processingImageAnimation = this.processingImageAnimation;
                case IDLE:
                    break;
                case FAILED:
                    if (voiceSearchError != null) {
                        if (VoiceSearchResponse.VoiceSearchError.NO_WORDS_RECOGNIZED == voiceSearchError) {
                            if (this.isListeningOverBluetooth) {
                                this.imageStatus.setImageResource(R.drawable.icon_badge_b_t);
                            }
                            else {
                                this.imageStatus.setImageResource(R.drawable.icon_badge_phone);
                            }
                        }
                        else {
                            Integer n2;
                            if (voiceSearchError == null) {
                                n2 = null;
                            }
                            else {
                                n2 = VoiceSearchNotification.BADGE_ICON_MAPPING.get(voiceSearchError);
                            }
                            Integer value2 = n2;
                            if (n2 == null) {
                                value2 = R.drawable.icon_badge_alert;
                                VoiceSearchNotification.sLogger.e(String.format("no icon for error: %s", voiceSearchError));
                            }
                            this.imageStatus.setImageResource((int)value2);
                        }
                        this.statusImageAnimation.play((Animator)getBadgeAnimation((View)this.imageStatus, true));
                        processingImageAnimation = set2;
                        break;
                    }
                    this.imageStatus.setImageDrawable((Drawable)null);
                    processingImageAnimation = set2;
                    break;
            }
        }
        Builder play = Builder;
        if (o != null) {
            play = set.play((Animator)o);
        }
        if (b && this.statusImageAnimation != null) {
            if (play != null) {
                play.before((Animator)this.statusImageAnimation);
            }
            else {
                set.play((Animator)this.statusImageAnimation);
            }
        }
        if (b && processingImageAnimation != null) {
            this.processingImageAnimation.start();
        }
        set.setDuration(300L);
        set.start();
        if (b) {
            if (this.voiceSearchState == VoiceSearchStateInternal.LISTENING) {
                this.listeningFeedbackView.setVisibility(View.VISIBLE);
                if (this.listeningFeedbackAnimation == null) {
                    this.listeningFeedbackAnimation = getListeningFeedbackDefaultAnimation(this.listeningFeedbackView);
                }
                this.listeningFeedbackAnimation.start();
            }
            else {
                this.listeningFeedbackView.setVisibility(GONE);
                if (this.listeningFeedbackAnimation != null && this.listeningFeedbackAnimation.isRunning()) {
                    this.listeningFeedbackAnimation.cancel();
                }
            }
        }
        if (b && voiceSearchStateInternal == VoiceSearchStateInternal.FAILED && (voiceSearchError == VoiceSearchResponse.VoiceSearchError.NO_RESULTS_FOUND || voiceSearchError == VoiceSearchResponse.VoiceSearchError.NO_WORDS_RECOGNIZED)) {
            VoiceSearchNotification.sLogger.d("VoiceSearch error , error :" + voiceSearchError + " , showing voice search");
            this.showHideVoiceSearchTipsIfNeeded(true);
        }
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public void close() {
        this.dismissNotification();
    }
    
    @Override
    public void executeItem(final ChoiceLayout2.Selection selection) {
        switch (selection.id) {
            case R.id.dismiss:
                AnalyticsSupport.recordRouteSelectionCancelled();
                this.dismissNotification();
                break;
            case R.id.retry:
                AnalyticsSupport.recordVoiceSearchRetry();
                this.showHideVoiceSearchTipsIfNeeded(false);
                this.startVoiceSearch();
                break;
            case R.id.list:
                AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(AnalyticsSupport$VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST);
                this.voiceSearchHandler.showResults();
                break;
            case R.id.go:
                AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(AnalyticsSupport$VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_GO);
                this.voiceSearchHandler.go();
                this.dismissNotification();
                break;
        }
    }
    
    @Override
    public boolean expandNotification() {
        return false;
    }
    
    @Override
    public int getColor() {
        return 0;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return "navdy#voicesearch#notif";
    }
    
    @Override
    public int getTimeout() {
        return 0;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.VOICE_SEARCH;
    }
    
    @Override
    public View getView(final Context context) {
        if (this.layout == null) {
            this.layout = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.notification_voice_search, (ViewGroup)null);
        }
        ButterKnife.inject(this, (View)this.layout);
        final Resources resources = context.getResources();
        final int color = resources.getColor(R.color.glance_dismiss);
        final int color2 = resources.getColor(R.color.glance_ok_blue);
        final int color3 = resources.getColor(R.color.glance_ok_go);
        final int color4 = resources.getColor(R.color.glance_ok_blue);
        final ChoiceLayout2.Choice choice = new ChoiceLayout2.Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, resources.getString(R.string.cancel), color);
        (this.voiceSearchFailedChoices = new ArrayList<ChoiceLayout2.Choice>()).add(new ChoiceLayout2.Choice(R.id.retry, R.drawable.icon_glances_retry, color2, R.drawable.icon_glances_retry, -16777216, resources.getString(R.string.retry), color2));
        this.voiceSearchFailedChoices.add(choice);
        (this.voiceSearchActiveChoices = new ArrayList<ChoiceLayout2.Choice>()).add(choice);
        (this.voiceSearchSuccessResultsChoices = new ArrayList<ChoiceLayout2.Choice>()).add(new ChoiceLayout2.Choice(R.id.list, R.drawable.icon_glances_read, color4, R.drawable.icon_glances_read, -16777216, resources.getString(R.string.view), color4));
        this.voiceSearchSuccessResultsChoices.add(new ChoiceLayout2.Choice(R.id.go, R.drawable.icon_go, color3, R.drawable.icon_go, -16777216, resources.getString(R.string.go), color3));
        this.choiceLayout.setChoices(this.voiceSearchActiveChoices, 0, (ChoiceLayout2.IListener)this);
        this.choiceLayout.setVisibility(View.VISIBLE);
        return (View)this.layout;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    @Override
    public boolean isAlive() {
        return false;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }
    
    @Override
    public void itemSelected(final ChoiceLayout2.Selection selection) {
    }
    
    @Override
    public InputManager.IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onClick() {
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        boolean b = true;
        if (this.multipleResultsMode) {
            switch (gestureEvent.gesture) {
                case GESTURE_SWIPE_LEFT:
                    this.voiceSearchHandler.showResults();
                    return b;
                case GESTURE_SWIPE_RIGHT:
                    this.voiceSearchHandler.go();
                    this.dismissNotification();
                    return b;
            }
        }
        b = false;
        return b;
    }
    
    @Override
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean b = true;
        switch (customKeyEvent) {
            default:
                b = false;
                break;
            case LEFT:
                this.choiceLayout.moveSelectionLeft();
                break;
            case RIGHT:
                this.choiceLayout.moveSelectionRight();
                break;
            case SELECT:
                this.choiceLayout.executeSelectedItem();
                break;
        }
        return b;
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        this.handler = new Handler();
        if (this.layout != null) {
            (this.voiceSearchHandler = RemoteDeviceManager.getInstance().getVoiceSearchHandler()).setVoiceSearchUserInterface((VoiceSearchHandler.VoiceSearchUserInterface)this);
            this.controller = controller;
            this.error = null;
            this.isListeningOverBluetooth = false;
            this.words = null;
            this.imageStatus.setImageDrawable((Drawable)null);
            AnalyticsSupport.recordVoiceSearchEntered();
            this.startVoiceSearch();
        }
    }
    
    @Override
    public void onStop() {
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        this.controller = null;
        this.voiceSearchHandler.stopVoiceSearch();
        this.voiceSearchHandler.setVoiceSearchUserInterface(null);
        if (this.layout != null) {
            if (this.processingImageAnimation != null) {
                this.processingImageAnimation.removeAllListeners();
                this.processingImageAnimation.cancel();
                this.processingImageAnimation = null;
            }
            if (this.listeningFeedbackAnimation != null) {
                this.listeningFeedbackAnimation.removeAllListeners();
                this.listeningFeedbackAnimation.cancel();
                this.listeningFeedbackAnimation = null;
            }
        }
        ToastManager.getInstance().disableToasts(false);
        VoiceSearchNotification.sLogger.v("startVoiceSearch: enabled toast");
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public void onUpdate() {
    }
    
    @Override
    public void onVoiceSearchResponse(final VoiceSearchResponse voiceSearchResponse) {
        VoiceSearchNotification.sLogger.d("onVoiceSearchResponse:" + voiceSearchResponse);
        this.handler.removeCallbacks(this.waitingTimeoutRunnable);
        if (this.responseFromClientTimedOut) {
            VoiceSearchNotification.sLogger.d("Not processing further response as the client did not respond in time");
        }
        else {
            AnalyticsSupport.recordVoiceSearchResult(voiceSearchResponse);
            final MusicManager musicManager = RemoteDeviceManager.getInstance().getMusicManager();
            switch (voiceSearchResponse.state) {
                case VOICE_SEARCH_STARTING:
                    this.multipleResultsMode = false;
                    this.error = voiceSearchResponse.error;
                    if (!TextUtils.isEmpty((CharSequence)voiceSearchResponse.locale)) {
                        this.locale = HudLocale.getLocaleForID(voiceSearchResponse.locale);
                    }
                    this.updateUI(VoiceSearchStateInternal.STARTING);
                    this.handler.postDelayed(this.waitingTimeoutRunnable, (long)VoiceSearchNotification.INITIALIZATION_DELAY_MILLIS);
                    break;
                case VOICE_SEARCH_SEARCHING:
                    if (musicManager != null) {
                        musicManager.acceptResumes();
                    }
                    this.multipleResultsMode = false;
                    this.error = voiceSearchResponse.error;
                    this.updateUI(VoiceSearchStateInternal.SEARCHING);
                    this.handler.postDelayed(this.waitingTimeoutRunnable, (long)VoiceSearchNotification.SEARCH_DELAY_MILLIS);
                    break;
                case VOICE_SEARCH_LISTENING:
                    this.multipleResultsMode = false;
                    this.isListeningOverBluetooth = Wire.<Boolean>get(voiceSearchResponse.listeningOverBluetooth, false);
                    this.error = voiceSearchResponse.error;
                    this.updateUI(VoiceSearchStateInternal.LISTENING);
                    this.handler.postDelayed(this.waitingTimeoutRunnable, (long)VoiceSearchNotification.INPUT_DELAY_MILLIS);
                    break;
                case VOICE_SEARCH_ERROR:
                    if (musicManager != null) {
                        musicManager.acceptResumes();
                    }
                    this.multipleResultsMode = false;
                    this.error = voiceSearchResponse.error;
                    VoiceSearchNotification.sLogger.d("Error " + voiceSearchResponse.error);
                    this.updateUI(VoiceSearchStateInternal.FAILED);
                    break;
                case VOICE_SEARCH_SUCCESS: {
                    this.error = voiceSearchResponse.error;
                    if (voiceSearchResponse.results == null) {
                        this.multipleResultsMode = false;
                        this.updateUI(VoiceSearchStateInternal.IDLE);
                        this.dismissNotification();
                        break;
                    }
                    final boolean b = false;
                    VoiceSearchNotification.sLogger.v("onVoiceSearchResponse: size=" + voiceSearchResponse.results.size());
                    int n = b ? 1 : 0;
                    if (voiceSearchResponse.results.size() == 1) {
                        final Destination destination = voiceSearchResponse.results.get(0);
                        n = (b ? 1 : 0);
                        if (destination.favorite_type != null) {
                            switch (destination.favorite_type) {
                                default:
                                    n = (b ? 1 : 0);
                                    break;
                                case FAVORITE_HOME:
                                case FAVORITE_WORK:
                                case FAVORITE_CUSTOM:
                                    n = 1;
                                    break;
                                case FAVORITE_NONE:
                                    n = (b ? 1 : 0);
                                    if (destination.last_navigated_to == null) {
                                        break;
                                    }
                                    n = (b ? 1 : 0);
                                    if (destination.last_navigated_to > 0L) {
                                        n = 1;
                                        break;
                                    }
                                    break;
                            }
                        }
                    }
                    if (n == 0) {
                        VoiceSearchNotification.sLogger.v("onVoiceSearchResponse: show picker");
                        AnalyticsSupport.recordVoiceSearchAdditionalResultsAction(AnalyticsSupport$VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST);
                        this.voiceSearchHandler.showResults();
                        break;
                    }
                    VoiceSearchNotification.sLogger.v("onVoiceSearchResponse: start trip");
                    this.multipleResultsMode = false;
                    this.voiceSearchHandler.go();
                    this.dismissNotification();
                    break;
                }
            }
        }
    }
    
    @Override
    public boolean supportScroll() {
        return false;
    }
    
    static class SinusoidalInterpolator implements Interpolator
    {
        public float getInterpolation(final float n) {
            return (float)(Math.sin(n * 3.141592653589793 - 1.5707963267948966) * 0.5 + 0.5);
        }
    }
    
    enum VoiceSearchStateInternal
    {
        IDLE(-1),
        FAILED(R.string.voice_search_failed),
        LISTENING(R.string.voice_search_listening),
        RESULTS(R.string.voice_search_more_results), 
        SEARCHING(R.string.voice_search_searching), 
        STARTING(R.string.voice_search_starting);
        
        private int statusTextResId;
        
        private VoiceSearchStateInternal(final int statusTextResId) {
            this.statusTextResId = statusTextResId;
        }
        
        public int getStatusTextResId() {
            return this.statusTextResId;
        }
    }
}
