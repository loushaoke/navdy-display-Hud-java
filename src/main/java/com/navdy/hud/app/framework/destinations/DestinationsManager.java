package com.navdy.hud.app.framework.destinations;
import com.navdy.hud.app.R;
import com.squareup.otto.Subscribe;

public class DestinationsManager {
    final private static com.navdy.hud.app.framework.destinations.DestinationsManager$FavoriteDestinationsChanged FAVORITE_DESTINATIONS_CHANGED;
    final public static String FAVORITE_DESTINATIONS_FILENAME = "favoriteDestinations.pb";
    final private static char[] INITIALS_ARRAY;
    final public static int MAX_DESTINATIONS = 30;
    final private static int MAX_INITIALS = 4;
    final private static com.navdy.hud.app.framework.destinations.DestinationsManager$RecentDestinationsChanged RECENT_DESTINATIONS_CHANGED;
    final private static com.navdy.hud.app.framework.destinations.DestinationsManager$SuggestedDestinationsChanged SUGGESTED_DESTINATIONS_CHANGED;
    final private static com.navdy.hud.app.framework.destinations.DestinationsManager sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.squareup.otto.Bus bus;
    private volatile java.util.List favoriteDestinations;
    private com.navdy.service.library.events.places.FavoriteDestinationsUpdate lastUpdate;
    private volatile java.util.List recentDestinations;
    android.content.res.Resources resources;
    private com.navdy.service.library.events.places.SuggestedDestination suggestedDestination;
    private volatile java.util.List suggestedDestinations;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.destinations.DestinationsManager.class);
        FAVORITE_DESTINATIONS_CHANGED = new com.navdy.hud.app.framework.destinations.DestinationsManager$FavoriteDestinationsChanged();
        RECENT_DESTINATIONS_CHANGED = new com.navdy.hud.app.framework.destinations.DestinationsManager$RecentDestinationsChanged();
        SUGGESTED_DESTINATIONS_CHANGED = new com.navdy.hud.app.framework.destinations.DestinationsManager$SuggestedDestinationsChanged();
        INITIALS_ARRAY = new char[4];
        sInstance = new com.navdy.hud.app.framework.destinations.DestinationsManager();
    }
    
    private DestinationsManager() {
        this.resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.bus.register(this);
    }
    
    static void access$000(com.navdy.hud.app.framework.destinations.DestinationsManager a) {
        a.requestDestinations();
    }
    
    static void access$100(com.navdy.hud.app.framework.destinations.DestinationsManager a, com.navdy.service.library.events.places.FavoriteDestinationsUpdate a0) {
        a.parseDestinations(a0);
    }
    
    static com.navdy.service.library.log.Logger access$200() {
        return sLogger;
    }
    
    static void access$300(com.navdy.hud.app.framework.destinations.DestinationsManager a, com.navdy.service.library.events.destination.RecommendedDestinationsUpdate a0) {
        a.parseDestinations(a0);
    }
    
    static com.navdy.service.library.events.places.SuggestedDestination access$402(com.navdy.hud.app.framework.destinations.DestinationsManager a, com.navdy.service.library.events.places.SuggestedDestination a0) {
        a.suggestedDestination = a0;
        return a0;
    }
    
    static void access$500(com.navdy.hud.app.framework.destinations.DestinationsManager a) {
        a.load();
    }
    
    private void buildDestinations() {
        if (com.navdy.hud.app.util.GenericUtil.isMainThread()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(new DestinationsManager$5(this), 10);
        } else {
            this.load();
        }
    }
    
    private void clearDestinations() {
        this.lastUpdate = null;
        this.setFavoriteDestinations(null);
        this.setRecentDestinations(null);
        this.setSuggestedDestinations(null);
    }
    
    private static String getDestinationInitials(String s) {
        String s0;
        label0: try {
            if (android.text.TextUtils.isEmpty(s)) {
                s0 = null;
            } else {
                boolean b;
                String s1 = com.navdy.hud.app.util.GenericUtil.removePunctuation(s).toUpperCase().trim();
                int i = s1.indexOf(" ");
                s0 = null;
                if (i >= 0) {
                    s0 = s1.substring(0, i).trim();
                }
                String s2 = (s0 == null) ? s1 : s0;
                try {
                    Integer.parseInt(s2);
                    if (s0 != null) {
                        b = true;
                    } else {
                        b = true;
                        s0 = s1;
                    }
                } catch(Throwable ignoredException) {
                    b = false;
                }
                if (b) {
                    if (s0.length() <= 4) {
                        break label0;
                    }
                    s0 = null;
                } else if (s0 != null) {
                synchronized(INITIALS_ARRAY) {
                        java.util.StringTokenizer a1 = new java.util.StringTokenizer(s1, " ");
                        int i0 = 0;
                        while(a1.hasMoreElements()) {
                            String s3 = (String)a1.nextElement();
                            int i1 = i0 + 1;
                            int i2 = s3.charAt(0);
                            INITIALS_ARRAY[i0] = (char)i2;
                            {
                                label1: {
                                    label2: {
                                        if (i1 != 4) {
                                            break label2;
                                        }
                                        if (a1.hasMoreElements()) {
                                            break label1;
                                        }
                                    }
                                    i0 = i1;
                                    continue;
                                }
                                /*monexit(a)*/
                                s0 = null;
                                break label0;
                            }
                        }
                        s0 = new String(INITIALS_ARRAY, 0, i0);
                        /*monexit(a)*/
                }
                } else {
                    s0 = s1.substring(0, 1);
                }
            }

        } catch(Throwable a6) {
            sLogger.e(a6);
            s0 = null;
        }
        return s0;
    }
    
    private com.navdy.service.library.events.destination.Destination.FavoriteType getDestinationType(com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a) {
        com.navdy.service.library.events.destination.Destination.FavoriteType a0;
        switch(com.navdy.hud.app.framework.destinations.DestinationsManager$6.$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[a.ordinal()]) {
            case 6: {
                a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CUSTOM;
                break;
            }
            case 3: {
                a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_WORK;
                break;
            }
            case 2: {
                a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_HOME;
                break;
            }
            default: {
                a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE;
            }
        }
        return a0;
    }
    
    public static String getInitials(String s, com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a) {
        String s0;
        label0: {
            label1: {
                if (a == null) {
                    break label1;
                }
                switch(com.navdy.hud.app.framework.destinations.DestinationsManager$6.$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[a.ordinal()]) {
                    case 4: {
                        s0 = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(s);
                        break label0;
                    }
                    case 1: case 6: {
                        s0 = com.navdy.hud.app.framework.destinations.DestinationsManager.getDestinationInitials(s);
                        break label0;
                    }
                }
            }
            s0 = null;
        }
        return s0;
    }
    
    public static com.navdy.hud.app.framework.destinations.DestinationsManager getInstance() {
        return sInstance;
    }
    
    public static com.navdy.service.library.events.navigation.NavigationRouteRequest getNavigationRouteRequestForDestination(com.navdy.service.library.events.destination.Destination a) {
        com.navdy.service.library.events.location.Coordinate a0;
        com.navdy.service.library.events.location.Coordinate a1;
        com.navdy.service.library.events.location.LatLong a2 = a.navigation_position;
        label2: {
            label0: {
                label1: {
                    if (a2 == null) {
                        break label1;
                    }
                    if (a.navigation_position.latitude != 0.0) {
                        break label0;
                    }
                    if (a.navigation_position.longitude != 0.0) {
                        break label0;
                    }
                }
                a0 = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(a.display_position.latitude).longitude(a.display_position.longitude).build();
                a1 = null;
                break label2;
            }
            a0 = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(a.display_position.latitude).longitude(a.display_position.longitude).build();
            a1 = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(a.navigation_position.latitude).longitude(a.navigation_position.longitude).build();
        }
        com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder a3 = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder().destination(a0).label(a.destination_title).streetAddress(a.full_address).destination_identifier(a.identifier).originDisplay(Boolean.TRUE).geoCodeStreetAddress(Boolean.FALSE).autoNavigate(Boolean.TRUE).requestId(java.util.UUID.randomUUID().toString());
        if (a1 != null) {
            a3.destinationDisplay(a1);
        }
        return a3.build();
    }
    
    private com.navdy.hud.app.framework.destinations.Destination$PlaceCategory getPlaceCategory(com.navdy.service.library.events.destination.Destination a) {
        com.navdy.hud.app.framework.destinations.Destination$PlaceCategory a0 = null;
        boolean b = Boolean.TRUE.equals(a.is_recommendation);
        boolean b0 = a.suggestion_type != null && a.suggestion_type == com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT;
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                if (!b0) {
                    break label0;
                }
                a0 = com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.SUGGESTED_RECENT;
                break label1;
            }
            a0 = b ? com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.SUGGESTED : b0 ? com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.RECENT : null;
        }
        return a0;
    }
    
    private String getRecentTimeLabel(com.navdy.service.library.events.destination.Destination a) {
        com.navdy.service.library.events.destination.Destination.SuggestionType a0 = a.suggestion_type;
        String s = null;
        if (a0 != null) {
            com.navdy.service.library.events.destination.Destination.SuggestionType a1 = a.suggestion_type;
            com.navdy.service.library.events.destination.Destination.SuggestionType a2 = com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT;
            s = null;
            if (a1 == a2) {
                Long a3 = a.last_navigated_to;
                s = null;
                if (a3 != null) {
                    long j = a.last_navigated_to;
                    int i = Long.compare(j, 0L);
                    s = null;
                    if (i > 0) {
                        s = com.navdy.hud.app.util.DateUtil.getDateLabel(new java.util.Date(a.last_navigated_to));
                    }
                }
            }
        }
        return s;
    }
    
    private int getSuggestionLabelColor(com.navdy.service.library.events.destination.Destination a, com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a0, com.navdy.hud.app.framework.destinations.Destination$PlaceCategory a1) {
        int i;
        if (Boolean.TRUE.equals(a.is_recommendation)) {
            android.content.res.Resources a2 = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            if (a1 != com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.SUGGESTED_RECENT) {
                switch(com.navdy.hud.app.framework.destinations.DestinationsManager$6.$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[a0.ordinal()]) {
                    case 5: {
                        i = a2.getColor(R.color.suggested_dest_cal);
                        break;
                    }
                    case 4: {
                        i = a2.getColor(R.color.suggested_dest_contact);
                        break;
                    }
                    case 3: {
                        i = a2.getColor(R.color.suggested_dest_work);
                        break;
                    }
                    case 2: {
                        i = a2.getColor(R.color.suggested_dest_home);
                        break;
                    }
                    default: {
                        i = a2.getColor(R.color.suggested_dest_fav);
                    }
                }
            } else {
                i = a2.getColor(R.color.suggested_dest_work);
            }
        } else {
            i = -1;
        }
        return i;
    }
    
    private void handleDefaultDestination(com.navdy.hud.app.framework.destinations.Destination a, boolean b) {
        sLogger.v("handleDefaultDestination [" + a.toString() + "] lookup=" + b);
        label5: {
            com.navdy.service.library.events.location.Coordinate a0;
            com.navdy.service.library.events.location.Coordinate a1;
            label3: {
                if (!b) {
                    break label3;
                }
                boolean b0 = com.navdy.hud.app.util.RemoteCapabilitiesUtil.supportsNavigationCoordinateLookup();
                label4: {
                    if (b0) {
                        break label4;
                    }
                    sLogger.v("handleDefaultDestination client does not support nav lookup");
                    break label3;
                }
                double d = a.navigationPositionLatitude;
                int i = Double.compare(d, 0.0);
                label2: {
                    if (i == 0) {
                        break label2;
                    }
                    if (a.navigationPositionLongitude == 0.0) {
                        break label2;
                    }
                    sLogger.v("handleDefaultDestination nav coordinate exists already");
                    break label3;
                }
                sLogger.v("handleDefaultDestination do nav lookup");
                com.navdy.hud.app.maps.here.HereRouteManager.startNavigationLookup(a);
                break label5;
            }
            double d0 = a.navigationPositionLatitude;
            int i0 = Double.compare(d0, 0.0);
            label1: {
                label0: {
                    if (i0 != 0) {
                        break label0;
                    }
                    if (a.navigationPositionLongitude != 0.0) {
                        break label0;
                    }
                    a0 = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(a.displayPositionLatitude).longitude(a.displayPositionLongitude).build();
                    a1 = null;
                    break label1;
                }
                a0 = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(a.navigationPositionLatitude).longitude(a.navigationPositionLongitude).build();
                a1 = new com.navdy.service.library.events.location.Coordinate.Builder().latitude(a.displayPositionLatitude).longitude(a.displayPositionLongitude).build();
            }
            com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder a2 = new com.navdy.service.library.events.navigation.NavigationRouteRequest.Builder().destination(a0).label(a.destinationTitle).streetAddress(a.fullAddress).destination_identifier(a.identifier).requestDestination(this.transformToProtoDestination(a)).originDisplay(Boolean.TRUE).geoCodeStreetAddress(Boolean.FALSE).destinationType(this.getDestinationType(a.favoriteDestinationType)).requestId(java.util.UUID.randomUUID().toString());
            if (a1 != null) {
                a2.destinationDisplay(a1);
            }
            com.navdy.service.library.events.navigation.NavigationRouteRequest a3 = a2.build();
            sLogger.v("launched navigation route request");
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent(a3));
            this.bus.post(a3);
        }
    }
    
    private void handleFindGasDestination() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(com.navdy.hud.app.framework.fuel.FuelRoutingManager.LOW_FUEL_ID);
        com.navdy.hud.app.framework.fuel.FuelRoutingManager a0 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
        a0.showFindingGasStationToast();
        a0.findGasStations(-1, false);
    }
    
    private void load() {
        try {
            this.parseDestinations(new com.navdy.service.library.events.MessageStore(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getPreferencesDirectory()).readMessage("favoriteDestinations.pb", com.navdy.service.library.events.places.FavoriteDestinationsUpdate.class));
        } catch(Throwable a) {
            this.setFavoriteDestinations(null);
            sLogger.e(a);
        }
    }
    
    private void parseDestinations(com.navdy.service.library.events.destination.RecommendedDestinationsUpdate a) {
        sLogger.v("parsing RecommendedDestinationsUpdate");
        if (a.destinations == null) {
            sLogger.w("rec-destination list returned is null");
        } else {
            java.util.List a0 = this.transform(a.destinations);
            java.util.ArrayList a1 = new java.util.ArrayList();
            java.util.ArrayList a2 = new java.util.ArrayList();
            if (a0.size() > 0) {
                Object a3 = a0.iterator();
                while(((java.util.Iterator)a3).hasNext()) {
                    com.navdy.hud.app.framework.destinations.Destination a4 = (com.navdy.hud.app.framework.destinations.Destination)((java.util.Iterator)a3).next();
                    if (a4.recommendation) {
                        a2.add(a4);
                    } else {
                        a1.add(a4);
                    }
                }
            }
            this.setRecentDestinations(a1);
            this.setSuggestedDestinations(a2);
        }
    }
    
    private void parseDestinations(com.navdy.service.library.events.places.FavoriteDestinationsUpdate a) {
        java.util.List a0;
        sLogger.v("parsing FavoriteDestinationsUpdate");
        this.lastUpdate = a;
        label1: {
            label0: {
                if (a == null) {
                    break label0;
                }
                if (a.destinations == null) {
                    break label0;
                }
                new com.navdy.service.library.events.MessageStore(com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getPreferencesDirectory()).writeMessage(a, "favoriteDestinations.pb");
                a0 = this.transform(a.destinations);
                break label1;
            }
            sLogger.w("fav-destination list returned is null");
            a0 = null;
        }
        this.setFavoriteDestinations(a0);
    }
    
    private void requestDestinations() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        long j = (this.lastUpdate == null) ? 0L : this.lastUpdate.serial_number;
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.FavoriteDestinationsRequest(j)));
        this.bus.post(new com.navdy.hud.app.event.RemoteEvent(new com.navdy.service.library.events.destination.RecommendedDestinationsRequest(0L)));
        sLogger.v("sent favorite destinations request with current version=" + j);
        sLogger.v("sent recommended destinations request with current version");
    }
    
    private void setFavoriteDestinations(java.util.List a) {
        if (a != this.favoriteDestinations) {
            this.favoriteDestinations = a;
            this.bus.post(FAVORITE_DESTINATIONS_CHANGED);
        }
    }
    
    private java.util.List transform(java.util.List a) {
        java.util.ArrayList a0 = new java.util.ArrayList();
        Object a1 = a.iterator();
        while(true) {
            if (((java.util.Iterator)a1).hasNext()) {
                a0.add(this.transformToInternalDestination((com.navdy.service.library.events.destination.Destination)((java.util.Iterator)a1).next()));
                if (a0.size() != 30) {
                    continue;
                }
                sLogger.v("exceeded max size");
            }
            return a0;
        }
    }
    
    public void clearSuggestedDestination() {
        sLogger.v("clearSuggestedDestination");
        com.navdy.hud.app.framework.destinations.DestinationSuggestionToast.dismissSuggestionToast();
        this.suggestedDestination = null;
    }
    
    public java.util.List getFavoriteDestinations() {
        return this.favoriteDestinations;
    }
    
    public com.navdy.hud.app.framework.destinations.DestinationsManager$GasDestination getGasDestination() {
        boolean b = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getFeatureUtil().isFeatureEnabled(com.navdy.hud.app.util.FeatureUtil.Feature.FUEL_ROUTING);
        com.navdy.hud.app.framework.destinations.DestinationsManager$GasDestination a = null;
        if (b) {
            boolean b0 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance().isAvailable();
            a = null;
            if (b0) {
                com.navdy.hud.app.framework.fuel.FuelRoutingManager a0 = com.navdy.hud.app.framework.fuel.FuelRoutingManager.getInstance();
                if (a0.isBusy()) {
                    sLogger.v("fuel route manager is busy");
                    a = null;
                } else {
                    boolean b1;
                    sLogger.v("fuel route manager is not busy");
                    com.navdy.hud.app.obd.ObdManager a1 = com.navdy.hud.app.obd.ObdManager.getInstance();
                    boolean b2 = a1.isConnected() && a1.getFuelLevel() != -1;
                    com.navdy.hud.app.framework.destinations.Destination a2 = com.navdy.hud.app.framework.destinations.Destination.getGasDestination();
                    b1 = b2 && a0.getFuelGlanceDismissTime() > 0L;
                    a = new com.navdy.hud.app.framework.destinations.DestinationsManager$GasDestination();
                    a.destination = a2;
                    a.showFirst = b1;
                }
            }
        }
        return a;
    }
    
    public java.util.List getRecentDestinations() {
        return this.recentDestinations;
    }
    
    public java.util.List getSuggestedDestinations() {
        java.util.ArrayList a = new java.util.ArrayList();
        if (this.suggestedDestinations != null) {
            a.addAll(this.suggestedDestinations);
        }
        return a;
    }
    
    public void goToSuggestedDestination() {
        if (this.suggestedDestination != null) {
            com.navdy.service.library.events.destination.Destination a = this.suggestedDestination.destination;
            if (a != null) {
                if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                    sLogger.e("gotToSuggestedDestination: Cannot navigate as the user is already navigating");
                } else {
                    com.navdy.service.library.events.navigation.NavigationRouteRequest a0 = com.navdy.hud.app.framework.destinations.DestinationsManager.getNavigationRouteRequestForDestination(a);
                    sLogger.v("launched navigation route request");
                    this.bus.post(new com.navdy.hud.app.event.RemoteEvent(a0));
                    this.bus.post(a0);
                }
            } else {
                sLogger.e("gotToSuggestedDestination : Destination in Suggested destination is null, cannot to navigate");
            }
        } else {
            sLogger.e("gotToSuggestedDestination : Suggested destination is null, cannot to navigate");
        }
    }
    
    public boolean launchSuggestedDestination() {
        boolean b;
        if (this.suggestedDestination == null) {
            sLogger.v("launchSuggestedDestination: no suggested destination available");
            b = false;
        } else {
            sLogger.v("launchSuggestedDestination: suggested destination toast");
            com.navdy.hud.app.framework.destinations.DestinationSuggestionToast.showSuggestion(this.suggestedDestination);
            b = true;
        }
        return b;
    }
    
    @Subscribe
    public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (a.state == com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
            this.clearDestinations();
        }
    }
    
    @Subscribe
    public void onDestinationSuggestion(com.navdy.service.library.events.places.SuggestedDestination a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(new DestinationsManager$4(this, a), 3);
    }
    
    @Subscribe
    public void onDeviceSyncRequired(com.navdy.hud.app.service.ConnectionHandler.DeviceSyncEvent a) {

            sLogger.v("syncDestinations");
        com.navdy.service.library.task.TaskManager.getInstance().execute(new DestinationsManager$1(this), 10);
    }
    
    @Subscribe
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.buildDestinations();
    }
    
    @Subscribe
    public void onFavoriteDestinationsUpdate(com.navdy.service.library.events.places.FavoriteDestinationsUpdate a) {
        sLogger.v("received FavoriteDestinationsUpdate: " + a);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new DestinationsManager$2(this, a), 1);
    }
    
    @Subscribe
    public void onRecommendedDestinationsUpdate(com.navdy.service.library.events.destination.RecommendedDestinationsUpdate a) {
        sLogger.v("received RecommendedDestinationsUpdate:" + a);
        com.navdy.service.library.task.TaskManager.getInstance().execute(new DestinationsManager$3(this, a), 1);
    }
    
    public void requestNavigation(com.navdy.hud.app.framework.destinations.Destination a) {
        switch(com.navdy.hud.app.framework.destinations.DestinationsManager$6.$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$DestinationType[a.destinationType.ordinal()]) {
            case 2: {
                this.handleFindGasDestination();
                break;
            }
            case 1: {
                this.handleDefaultDestination(a, false);
                break;
            }
        }
    }
    
    public void requestNavigationWithNavLookup(com.navdy.hud.app.framework.destinations.Destination a) {
        this.handleDefaultDestination(a, true);
    }
    
    public void setRecentDestinations(java.util.List a) {
        this.recentDestinations = a;
        this.bus.post(RECENT_DESTINATIONS_CHANGED);
    }
    
    public void setSuggestedDestinations(java.util.List a) {
        this.suggestedDestinations = a;
        this.bus.post(SUGGESTED_DESTINATIONS_CHANGED);
    }
    
    public com.navdy.hud.app.framework.destinations.Destination transformToInternalDestination(com.navdy.service.library.events.destination.Destination a) {
        com.navdy.hud.app.framework.destinations.Destination a0;
        if (a != null) {
            int i = 0;
            com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a1;
            boolean b = Boolean.TRUE.equals(a.is_recommendation);
            com.navdy.service.library.events.destination.Destination.SuggestionType a2 = a.suggestion_type;
            label1: {
                label0: {
                    if (a2 == null) {
                        break label0;
                    }
                    if (a.suggestion_type != com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_CALENDAR) {
                        break label0;
                    }
                    i = this.resources.getColor(R.color.suggested_dest_cal);
                    a1 = com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.FAVORITE_CALENDAR;
                    b = true;
                    break label1;
                }
                a1 = com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.buildFromValue(a.favorite_type.getValue());
                i = -1;
            }
            String s = com.navdy.hud.app.framework.destinations.DestinationsManager.getInitials(a.destination_title, a1);
            com.navdy.hud.app.framework.destinations.Destination$PlaceCategory a3 = this.getPlaceCategory(a);
            com.navdy.hud.app.framework.destinations.Destination$PlaceCategory a4 = com.navdy.hud.app.framework.destinations.Destination$PlaceCategory.RECENT;
            String s0 = null;
            if (a3 == a4) {
                s0 = this.getRecentTimeLabel(a);
            }
            if (b) {
                i = this.getSuggestionLabelColor(a, a1, a3);
            }
            a0 = new com.navdy.hud.app.framework.destinations.Destination((a.navigation_position == null) ? 0.0 : a.navigation_position.latitude, (a.navigation_position == null) ? 0.0 : a.navigation_position.longitude, (a.display_position == null) ? 0.0 : a.display_position.latitude, (a.display_position == null) ? 0.0 : a.display_position.longitude, a.full_address, a.destination_title, a.destination_subtitle, a.identifier, a1, com.navdy.hud.app.framework.destinations.Destination$DestinationType.DEFAULT, s, a3, s0, i, b, (a.destinationIcon == null) ? 0 : a.destinationIcon, (a.destinationIconBkColor == null) ? 0 : a.destinationIconBkColor, a.place_id, a.place_type, a.destinationDistance, com.navdy.hud.app.framework.contacts.ContactUtil.fromPhoneNumbers(a.phoneNumbers), com.navdy.hud.app.framework.contacts.ContactUtil.fromContacts(a.contacts));
        } else {
            a0 = null;
        }
        return a0;
    }
    
    public com.navdy.service.library.events.destination.Destination transformToProtoDestination(com.navdy.hud.app.framework.destinations.Destination a) {
        com.navdy.service.library.events.destination.Destination.FavoriteType a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_NONE;
        com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a1 = a.favoriteDestinationType;
        com.navdy.service.library.events.destination.Destination.SuggestionType a2 = null;
        if (a1 != null) {
            if (com.navdy.hud.app.framework.destinations.DestinationsManager$6.$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[a.favoriteDestinationType.ordinal()] == 5) {
                a2 = com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_CALENDAR;
            } else {
                int i = com.navdy.hud.app.framework.destinations.DestinationsManager$6.$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$FavoriteDestinationType[a.favoriteDestinationType.ordinal()];
                a2 = null;
                switch(i) {
                    case 6: {
                        a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CUSTOM;
                        a2 = null;
                        break;
                    }
                    case 4: {
                        a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_CONTACT;
                        a2 = null;
                        break;
                    }
                    case 3: {
                        a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_WORK;
                        a2 = null;
                        break;
                    }
                    case 2: {
                        a0 = com.navdy.service.library.events.destination.Destination.FavoriteType.FAVORITE_HOME;
                        a2 = null;
                        break;
                    }
                    default: {
                        a2 = null;
                        break;
                    }
                    case 1: case 5: {
                    }
                }
            }
        }
        com.navdy.hud.app.framework.destinations.Destination$PlaceCategory a3 = a.placeCategory;
        Boolean a4 = null;
        if (a3 != null) {
            switch(com.navdy.hud.app.framework.destinations.DestinationsManager$6.$SwitchMap$com$navdy$hud$app$framework$destinations$Destination$PlaceCategory[a.placeCategory.ordinal()]) {
                case 2: {
                    a4 = Boolean.TRUE;
                    a2 = com.navdy.service.library.events.destination.Destination.SuggestionType.SUGGESTION_RECENT;
                    break;
                }
                case 1: {
                    a4 = Boolean.TRUE;
                    break;
                }
                default: {
                    a4 = null;
                }
            }
        }
        com.navdy.service.library.events.places.PlaceType a5 = com.navdy.service.library.events.places.PlaceType.PLACE_TYPE_UNKNOWN;
        if (a.placeType != null) {
            a5 = a.placeType;
        }
        com.navdy.service.library.events.destination.Destination.Builder a6 = new com.navdy.service.library.events.destination.Destination.Builder().navigation_position(new com.navdy.service.library.events.location.LatLong(a.navigationPositionLatitude, a.navigationPositionLongitude)).display_position(new com.navdy.service.library.events.location.LatLong(a.displayPositionLatitude, a.displayPositionLongitude)).full_address(a.fullAddress).destination_title(a.destinationTitle).destination_subtitle(a.destinationSubtitle).identifier(a.identifier).is_recommendation(a4).place_type(a5).suggestion_type(a2).place_id(a.destinationPlaceId).favorite_type(a0).contacts(com.navdy.hud.app.framework.contacts.ContactUtil.toContacts(a.contacts)).phoneNumbers(com.navdy.hud.app.framework.contacts.ContactUtil.toPhoneNumbers(a.phoneNumbers));
        if (a.destinationIcon != 0) {
            a6.destinationIcon(a.destinationIcon);
        }
        if (a.destinationIconBkColor != 0) {
            a6.destinationIconBkColor(a.destinationIconBkColor);
        }
        if (a.distanceStr != null) {
            a6.destinationDistance(a.distanceStr);
        }
        return a6.build();
    }
}
