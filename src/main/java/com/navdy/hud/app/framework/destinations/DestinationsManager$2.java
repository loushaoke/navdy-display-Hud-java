package com.navdy.hud.app.framework.destinations;

class DestinationsManager$2 implements Runnable {
    final com.navdy.hud.app.framework.destinations.DestinationsManager this$0;
    final com.navdy.service.library.events.places.FavoriteDestinationsUpdate val$response;
    
    DestinationsManager$2(com.navdy.hud.app.framework.destinations.DestinationsManager a, com.navdy.service.library.events.places.FavoriteDestinationsUpdate a0) {
        super();
        this.this$0 = a;
        this.val$response = a0;
    }
    
    public void run() {
        try {
            if (this.val$response.status != com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS) {
                if (this.val$response.status != com.navdy.service.library.events.RequestStatus.REQUEST_VERSION_IS_CURRENT) {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.access$200().e(new StringBuilder().append("sent fav-destination response error: ").append(this.val$response.status).toString());
                } else {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.access$200().v(new StringBuilder().append("fav-destination response: version").append(this.val$response.serial_number).append(" is current").toString());
                }
            } else {
                com.navdy.hud.app.framework.destinations.DestinationsManager.access$100(this.this$0, this.val$response);
            }
        } catch(Throwable a) {
            com.navdy.hud.app.framework.destinations.DestinationsManager.access$200().e(a);
        }
    }
}
