package com.navdy.hud.app.framework.notifications;

class NotificationManager$2 implements com.navdy.hud.app.framework.notifications.INotificationController {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    
    NotificationManager$2(com.navdy.hud.app.framework.notifications.NotificationManager a) {
        super();
        this.this$0 = a;
    }
    
    public void collapseNotification(boolean b, boolean b0) {
        com.navdy.hud.app.framework.notifications.NotificationManager.access$1700(this.this$0, b, b0, false);
    }
    
    public void expandNotification(boolean b) {
        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("expandNotification  running=").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$200(this.this$0)).append(" qsize:").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$300(this.this$0).size()).toString());
        com.navdy.hud.app.view.NotificationView a = com.navdy.hud.app.framework.notifications.NotificationManager.access$100(this.this$0);
        label2: {
            label5: {
                label6: {
                    if (a == null) {
                        break label6;
                    }
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0) != null) {
                        break label5;
                    }
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.i("cannot display expand notif:no current notif");
                break label2;
            }
            if (com.navdy.hud.app.framework.notifications.NotificationManager.access$500(this.this$0)) {
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.i("animation in progress, ignore expand");
            } else if (this.isExpanded()) {
                com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("already expanded");
            } else {
                android.view.View a0 = null;
                android.widget.FrameLayout a1 = com.navdy.hud.app.framework.notifications.NotificationManager.access$600(this.this$0);
                android.view.View a2 = com.navdy.hud.app.framework.notifications.NotificationManager.access$700(this.this$0);
                com.navdy.hud.app.ui.component.carousel.CarouselIndicator a3 = com.navdy.hud.app.framework.notifications.NotificationManager.access$800(this.this$0);
                com.navdy.hud.app.framework.notifications.NotificationManager.access$002(this.this$0, com.navdy.hud.app.framework.notifications.NotificationManager$ScrollState.NONE);
                if (a1.getChildCount() > 0) {
                    a1.removeAllViews();
                }
                if (b) {
                    com.navdy.service.library.events.preferences.NotificationPreferences a4 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNotificationPreferences();
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$902(this.this$0, a4.readAloud.booleanValue());
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1002(this.this$0, a4.showContent.booleanValue());
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("expanded with[").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getId()).append("] tts=").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$900(this.this$0)).append(" show=").append(com.navdy.hud.app.framework.notifications.NotificationManager.access$1000(this.this$0)).toString());
                }
                label3: {
                    label4: {
                        if (!b) {
                            break label4;
                        }
                        a0 = null;
                        if (!b) {
                            break label3;
                        }
                        boolean b0 = com.navdy.hud.app.framework.notifications.NotificationManager.access$1000(this.this$0);
                        a0 = null;
                        if (!b0) {
                            break label3;
                        }
                    }
                    a0 = com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getExpandedView(a.getContext(), null);
                    if (a0 != null) {
                        break label3;
                    }
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.w("cannot expand, view is null");
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$902(this.this$0, false);
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1002(this.this$0, false);
                    break label2;
                }
                com.navdy.hud.app.framework.notifications.NotificationManager.access$1102(this.this$0, b);
                if (b) {
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1202(this.this$0, com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0));
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1302(this.this$0, (com.navdy.hud.app.framework.notifications.NotificationManager$Info)null);
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.isLoggable(2)) {
                        this.this$0.printPriorityMap();
                    }
                    a1.setTag(Boolean.valueOf(true));
                } else {
                    a1.setTag(null);
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1202(this.this$0, (com.navdy.hud.app.framework.notifications.NotificationManager$Info)null);
                    com.navdy.hud.app.framework.notifications.NotificationManager.access$1302(this.this$0, (com.navdy.hud.app.framework.notifications.NotificationManager$Info)null);
                }
                a.border.stopTimeout(true, (Runnable)null);
                a.hideNextNotificationColor();
                com.navdy.hud.app.framework.notifications.INotification a5 = com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification;
                if (b) {
                    int i = this.this$0.getNotificationCount();
                    if (com.navdy.hud.app.framework.notifications.NotificationManager.access$1400(this.this$0)) {
                        i = i - 1;
                    }
                    int i0 = com.navdy.hud.app.framework.notifications.NotificationManager.access$1500(this.this$0, com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0));
                    com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v(new StringBuilder().append("expandNotification index:").append(i0).toString());
                    int i1 = (i0 != -1) ? i0 + 1 : 0;
                    int i2 = i + 2;
                    if (i1 >= i2) {
                        i1 = i2 - 1;
                    }
                    this.this$0.updateExpandedIndicator(i2, i1, com.navdy.hud.app.framework.notifications.NotificationManager.access$400(this.this$0).notification.getColor());
                }
                com.navdy.hud.app.framework.notifications.NotificationManager$2$1 a6 = new com.navdy.hud.app.framework.notifications.NotificationManager$2$1(this, a5, a3);
                boolean b1 = com.navdy.hud.app.framework.notifications.NotificationManager.access$1100(this.this$0);
                label0: {
                    label1: {
                        if (!b1) {
                            break label1;
                        }
                        if (!com.navdy.hud.app.framework.notifications.NotificationManager.access$1100(this.this$0)) {
                            break label0;
                        }
                        if (!com.navdy.hud.app.framework.notifications.NotificationManager.access$1000(this.this$0)) {
                            break label0;
                        }
                    }
                    a.animate().x(0.0f).withEndAction((Runnable)new com.navdy.hud.app.framework.notifications.NotificationManager$2$3(this, a0, a1, (Runnable)a6)).withStartAction((Runnable)new com.navdy.hud.app.framework.notifications.NotificationManager$2$2(this, a2, a1)).start();
                    break label2;
                }
                ((Runnable)a6).run();
            }
        }
    }
    
    public android.content.Context getUIContext() {
        return this.this$0.getUIContext();
    }
    
    public boolean isExpanded() {
        return this.this$0.isExpandedNotificationVisible();
    }
    
    public boolean isExpandedWithStack() {
        return this.this$0.isExpanded();
    }
    
    public boolean isShowOn() {
        return com.navdy.hud.app.framework.notifications.NotificationManager.access$1000(this.this$0);
    }
    
    public boolean isTtsOn() {
        return com.navdy.hud.app.framework.notifications.NotificationManager.access$900(this.this$0);
    }
    
    public void moveNext(boolean b) {
        this.this$0.moveNext(b);
    }
    
    public void movePrevious(boolean b) {
        this.this$0.movePrevious(b);
    }
    
    public void resetTimeout() {
        com.navdy.hud.app.view.NotificationView a = com.navdy.hud.app.framework.notifications.NotificationManager.access$100(this.this$0);
        if (a != null) {
            a.border.resetTimeout();
        }
    }
    
    public void startTimeout(int i) {
        com.navdy.hud.app.view.NotificationView a = com.navdy.hud.app.framework.notifications.NotificationManager.access$100(this.this$0);
        if (a != null) {
            a.border.startTimeout(i);
        }
    }
    
    public void stopTimeout(boolean b) {
        com.navdy.hud.app.view.NotificationView a = com.navdy.hud.app.framework.notifications.NotificationManager.access$100(this.this$0);
        if (a != null) {
            a.border.stopTimeout(b, (Runnable)null);
        }
    }
}
