package com.navdy.hud.app.framework.contacts;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.util.PhoneUtil;
import com.navdy.service.library.events.contacts.PhoneNumber;
import com.navdy.service.library.log.Logger;
import java.util.Locale;

public class Contact implements Parcelable {
    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
    private static final Logger sLogger = new Logger(Contact.class);
    public int defaultImageIndex;
    public String firstName;
    public String formattedNumber;
    public String initials;
    public String name;
    public String number;
    public NumberType numberType;
    public String numberTypeStr;
    public long numericNumber;

    public Contact(String name, String number, NumberType numberType, int defaultImageIndex, long numericNumber) {
        this.name = name;
        this.number = number;
        this.numberType = numberType;
        this.defaultImageIndex = defaultImageIndex;
        this.numericNumber = numericNumber;
        init(numericNumber);
    }

    public Contact(PhoneNumber phoneNumber) {
        this.number = phoneNumber.number;
        this.numberType = ContactUtil.getNumberType(phoneNumber.numberType);
        this.defaultImageIndex = ContactImageHelper.getInstance().getContactImageIndex(this.number);
        this.numberTypeStr = phoneNumber.customType;
        init(0);
    }

    public Contact(com.navdy.service.library.events.contacts.Contact contact) {
        this.name = contact.name;
        this.number = contact.number;
        this.numberType = ContactUtil.getNumberType(contact.numberType);
        this.defaultImageIndex = ContactImageHelper.getInstance().getContactImageIndex(this.number);
        this.numberTypeStr = contact.label;
        init(0);
    }

    public Contact(Parcel in) {
        this.name = in.readString();
        this.number = in.readString();
        int i = in.readInt();
        if (i != -1) {
            this.numberType = NumberType.values()[i];
        }
        this.defaultImageIndex = in.readInt();
        this.numericNumber = in.readLong();
        this.firstName = in.readString();
        this.initials = in.readString();
        this.numberTypeStr = in.readString();
        this.formattedNumber = in.readString();
    }

    private void init(long numericNumber) {
        if (!TextUtils.isEmpty(this.name)) {
            this.firstName = ContactUtil.getFirstName(this.name);
            this.initials = ContactUtil.getInitials(this.name);
        }
        if (this.numberTypeStr == null) {
            this.numberTypeStr = ContactUtil.getPhoneType(this.numberType);
        }
        this.formattedNumber = PhoneUtil.formatPhoneNumber(this.number);
        Locale currentLocale = DriverProfileHelper.getInstance().getCurrentLocale();
        if (!TextUtils.isEmpty(this.number)) {
            if (numericNumber > 0) {
                this.numericNumber = numericNumber;
                return;
            }
            try {
                this.numericNumber = PhoneNumberUtil.getInstance().parse(this.number, currentLocale.getCountry()).getNationalNumber();
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.number);
        dest.writeInt(this.numberType != null ? this.numberType.ordinal() : -1);
        dest.writeInt(this.defaultImageIndex);
        dest.writeLong(this.numericNumber);
        dest.writeString(this.firstName);
        dest.writeString(this.initials);
        dest.writeString(this.numberTypeStr);
        dest.writeString(this.formattedNumber);
    }
}