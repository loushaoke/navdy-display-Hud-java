package com.navdy.hud.app.framework.glance;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager$ShowToast;
import com.navdy.service.library.events.audio.SpeechRequestStatus;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.framework.recentcall.RecentCallManager;
import com.squareup.otto.Subscribe;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.hud.app.manager.InputManager;
import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2;
import com.navdy.hud.app.framework.contacts.NumberType;
import java.util.ArrayList;
import android.os.Bundle;
import android.graphics.drawable.AnimationDrawable;
import com.navdy.hud.app.util.PhoneUtil;
import android.view.ViewGroup.MarginLayoutParams;
import android.text.style.AbsoluteSizeSpan;
import android.text.SpannableStringBuilder;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.CalendarConstants;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.service.library.events.audio.SpeechRequest;
import android.view.LayoutInflater;
import android.content.Context;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.text.TextUtils;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.hud.app.framework.fuel.FuelRoutingManager;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.service.library.events.glances.FuelConstants;
import com.makeramen.RoundedTransformationBuilder;
import android.os.Looper;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.view.ObservableScrollView;
import com.squareup.picasso.Transformation;
import com.navdy.hud.app.framework.notifications.IProgressUpdate;
import java.util.Date;
import android.widget.TextView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.service.library.events.glances.GlanceEvent;
import android.view.ViewGroup;
import java.util.Map;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import java.util.List;
import com.navdy.service.library.events.audio.CancelSpeechRequest;
import com.squareup.otto.Bus;
import android.view.View;
import android.widget.ImageView;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.notifications.IScrollEvent;
import com.navdy.hud.app.framework.notifications.INotification;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;

public class GlanceNotification implements INotification, IScrollEvent, ContactUtil.IContactCallback
{
    private static final float IMAGE_SCALE = 0.5f;
    private static Handler handler;
    private static final Logger sLogger;
    private boolean alive;
    private int appIcon;
    private ImageView audioFeedback;
    private View bottomScrub;
    private Bus bus;
    private CancelSpeechRequest cancelSpeechRequest;
    private List<String> cannedReplyMessages;
    private ChoiceLayout2 choiceLayout;
    private ChoiceLayout2.IListener choiceListener;
    private final int color;
    private ColorImageView colorImageView;
    private List<Contact> contacts;
    private INotificationController controller;
    private final Map<String, String> data;
    private final GlanceApp glanceApp;
    private ViewGroup glanceContainer;
    private final GlanceEvent glanceEvent;
    private ViewGroup glanceExtendedContainer;
    private boolean hasFuelLevelInfo;
    private final String id;
    private boolean initialReplyMode;
    private GlanceViewCache.ViewType largeType;
    private InitialsImageView mainImage;
    private TextView mainTitle;
    private String messageStr;
    private String number;
    private boolean onBottom;
    private boolean onTop;
    private Mode operationMode;
    private boolean photoCheckRequired;
    private final Date postTime;
    private IProgressUpdate progressUpdate;
    private ViewGroup replyExitView;
    private ViewGroup replyMsgView;
    private Transformation roundTransformation;
    private ObservableScrollView.IScrollListener scrollListener;
    private ObservableScrollView scrollView;
    private ImageView sideImage;
    private GlanceViewCache.ViewType smallType;
    private String source;
    private StringBuilder stringBuilder1;
    private StringBuilder stringBuilder2;
    private TextView subTitle;
    private final boolean supportsScroll;
    private TextView text1;
    private TextView text2;
    private TextView text3;
    private TimeHelper timeHelper;
    private View topScrub;
    private String ttsMessage;
    private boolean ttsSent;
    private Runnable updateTimeRunnable;
    
    static {
        sLogger = new Logger(GlanceNotification.class);
        GlanceNotification.handler = new Handler(Looper.getMainLooper());
    }
    
    public GlanceNotification(final GlanceEvent glanceEvent, final GlanceApp glanceApp, final GlanceViewCache.ViewType largeType, final Map<String, String> data) {
        this.stringBuilder1 = new StringBuilder();
        this.stringBuilder2 = new StringBuilder();
        this.roundTransformation = new RoundedTransformationBuilder().oval(true).build();
        this.alive = true;
        final GlanceNotification a = this;
        this.updateTimeRunnable = (Runnable) new Runnable() {
            final GlanceNotification this$0 = a;

            public void run() {
                INotificationController a = GlanceNotification.this.controller;
                label5: {
                    Throwable a0 = null;
                    if (a == null) {
                        break label5;
                    }
                    label0: {
                        label4: {
                            label1: {
                                label3: try {
                                    boolean b = false;
                                    if (GlanceNotification.this.glanceExtendedContainer == null) {
                                        break label4;
                                    }
                                    TextView a1 = (TextView) GlanceNotification.this.glanceExtendedContainer.findViewById(R.id.title);
                                    switch(com.navdy.hud.app.framework.glance.GlanceNotification$5.$SwitchMap$com$navdy$hud$app$framework$glance$GlanceApp[GlanceNotification.this.glanceApp.ordinal()]) {
                                        case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17: case 18: case 19: {
                                            a1.setText((CharSequence) GlanceHelper.getTimeStr(System.currentTimeMillis(), GlanceNotification.this.postTime, GlanceNotification.this.timeHelper));
                                            break label3;
                                        }
                                        case 8: {
                                            b = GlanceNotification.this.hasFuelLevelInfo;
                                            break;
                                        }
                                        case 1: case 2: case 3: {
                                            GlanceNotification.this.setMainImage();
                                            break label3;
                                        }
                                        default: {
                                            break label3;
                                        }
                                    }
                                    label2: {
                                        if (!b) {
                                            break label2;
                                        }
                                        int i = ObdManager.getInstance().getFuelLevel();
                                        if (i > Integer.parseInt(GlanceNotification.this.data.get(FuelConstants.FUEL_LEVEL.name()))) {
                                            break label1;
                                        }
                                        GlanceNotification.this.data.put(FuelConstants.FUEL_LEVEL.name(), String.valueOf(i));
                                    }
                                    FuelRoutingManager a2 = FuelRoutingManager.getInstance();
                                    if (a2 != null && a2.getCurrentGasStation() != null) {
                                        double d = (double)Math.round(a2.getCurrentGasStation().getLocation().getCoordinate().distanceTo(HereMapsManager.getInstance().getLastGeoPosition().getCoordinate()) * 10.0 / 1609.34) / 10.0;
                                        GlanceNotification.this.data.put(FuelConstants.GAS_STATION_DISTANCE.name(), String.valueOf(d));
                                    }
                                    GlanceNotification.this.setTitle();
                                    GlanceNotification.this.setExpandedContent((View) GlanceNotification.this.glanceExtendedContainer);
                                } catch(Throwable a3) {
                                    a0 = a3;
                                    break label0;
                                }
                                GlanceNotification.handler.postDelayed(GlanceNotification.this.updateTimeRunnable, 30000L);
                                break label5;
                            }
                            GlanceNotification.handler.postDelayed(GlanceNotification.this.updateTimeRunnable, 30000L);
                            break label5;
                        }
                        GlanceNotification.handler.postDelayed(GlanceNotification.this.updateTimeRunnable, 30000L);
                        break label5;
                    }
                    try {
                        GlanceNotification.this.sLogger.e(a0);
                    } catch(Throwable a4) {
                        GlanceNotification.handler.postDelayed(GlanceNotification.this.updateTimeRunnable, 30000L);
                        throw a4;
                    }
                    GlanceNotification.handler.postDelayed(GlanceNotification.this.updateTimeRunnable, 30000L);
                }
            }
        };
        this.choiceListener = new ChoiceLayout2.IListener() {
            @Override
            public void executeItem(final ChoiceLayout2.Selection selection) {
                if (GlanceNotification.this.controller != null) {
                    switch (selection.id) {
                        case 1:
                            GlanceNotification.this.switchToReplyScreen();
                            break;
                        case 2:
                            GlanceNotification.this.switchToMode(Mode.READ, null);
                            break;
                        case 6:
                            GlanceNotification.this.dismissNotification();
                            break;
                        case 3:
                            GlanceNotification.this.call();
                            break;
                        case 4:
                            GlanceNotification.this.switchToMode(Mode.REPLY, GlanceNotification.this.number);
                            break;
                        case 5:
                            if (GlanceNotification.this.choiceLayout.getTag(R.id.message_secondary_screen) != null) {
                                GlanceNotification.this.controller.collapseNotification(false, false);
                                break;
                            }
                            GlanceNotification.this.revertChoice();
                            GlanceNotification.this.controller.startTimeout(GlanceNotification.this.getTimeout());
                            break;
                        case 7: {
                            final FuelRoutingManager instance = FuelRoutingManager.getInstance();
                            if (instance != null) {
                                instance.routeToGasStation();
                            }
                            GlanceNotification.this.dismissNotification();
                            break;
                        }
                        case 8: {
                            final FuelRoutingManager instance2 = FuelRoutingManager.getInstance();
                            if (instance2 != null) {
                                instance2.dismissGasRoute();
                            }
                            GlanceNotification.this.dismissNotification();
                            break;
                        }
                    }
                }
            }
            
            @Override
            public void itemSelected(final ChoiceLayout2.Selection selection) {
                if (GlanceNotification.this.controller != null) {
                    GlanceNotification.this.controller.resetTimeout();
                }
            }
        };
        this.scrollListener = new ObservableScrollView.IScrollListener() {
            @Override
            public void onBottom() {
                if (GlanceNotification.this.controller != null && GlanceNotification.this.supportsScroll && GlanceNotification.this.scrollView != null) {
                    GlanceNotification.this.onTop = false;
                    GlanceNotification.this.onBottom = true;
                    if (GlanceNotification.this.progressUpdate != null) {
                        GlanceNotification.this.progressUpdate.onPosChange(100);
                    }
                }
            }
            
            @Override
            public void onScroll(int height, final int n, int bottom, final int n2) {
                if (GlanceNotification.this.controller != null && GlanceNotification.this.supportsScroll && GlanceNotification.this.scrollView != null) {
                    GlanceNotification.this.topScrub.setVisibility(View.VISIBLE);
                    GlanceNotification.this.onTop = false;
                    GlanceNotification.this.onBottom = false;
                    if (GlanceNotification.this.progressUpdate != null) {
                        bottom = GlanceNotification.this.scrollView.getChildAt(0).getBottom();
                        height = GlanceNotification.this.scrollView.getHeight();
                        GlanceNotification.this.progressUpdate.onPosChange((int)(n * 100.0 / (bottom - height)));
                    }
                }
            }
            
            @Override
            public void onTop() {
                if (GlanceNotification.this.controller != null && GlanceNotification.this.supportsScroll && GlanceNotification.this.scrollView != null) {
                    GlanceNotification.this.topScrub.setVisibility(GONE);
                    GlanceNotification.this.onTop = true;
                    GlanceNotification.this.onBottom = false;
                    if (GlanceNotification.this.progressUpdate != null) {
                        GlanceNotification.this.progressUpdate.onPosChange(1);
                    }
                }
            }
        };
        this.glanceEvent = glanceEvent;
        this.glanceApp = glanceApp;
        this.id = GlanceHelper.getNotificationId(this.glanceEvent);
        this.color = this.glanceApp.getColor();
        this.appIcon = this.glanceApp.getSideIcon();
        if (data == null) {
            this.data = GlanceHelper.buildDataMap(glanceEvent);
        }
        else {
            this.data = data;
        }
        switch (glanceApp) {
            case GENERIC: {
                final String s = this.data.get(GenericConstants.GENERIC_SIDE_ICON.name());
                if (TextUtils.isEmpty(s)) {
                    break;
                }
                final int icon = GlanceHelper.getIcon(s);
                if (icon != -1) {
                    this.appIcon = icon;
                    break;
                }
                break;
            }
            case FUEL:
                this.hasFuelLevelInfo = this.data.containsKey(FuelConstants.FUEL_LEVEL);
                break;
        }
        this.photoCheckRequired = GlanceHelper.isPhotoCheckRequired(this.glanceApp);
        this.postTime = new Date(glanceEvent.postTime);
        this.timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
        this.smallType = GlanceHelper.getSmallViewType(this.glanceApp);
        if (largeType == null) {
            this.largeType = GlanceHelper.getLargeViewType(this.glanceApp);
        }
        else {
            this.largeType = largeType;
        }
        this.ttsMessage = GlanceHelper.getTtsMessage(this.glanceApp, this.data, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
        this.messageStr = GlanceHelper.getGlanceMessage(this.glanceApp, this.data);
        this.cancelSpeechRequest = new CancelSpeechRequest(this.id);
        this.supportsScroll = GlanceViewCache.supportScroll(this.largeType);
        this.bus = RemoteDeviceManager.getInstance().getBus();
    }
    
    public GlanceNotification(final GlanceEvent glanceEvent, final GlanceApp glanceApp, final Map<String, String> map) {
        this(glanceEvent, glanceApp, null, map);
    }
    
    private void call() {
        this.dismissNotification();
        RemoteDeviceManager.getInstance().getCallManager().dial(this.number, null, this.source);
    }
    
    private void cancelTts() {
        if (this.controller != null && this.controller.isTtsOn() && this.ttsSent) {
            GlanceNotification.sLogger.v("tts-cancelled [" + this.id + "]");
            this.bus.post(new RemoteEvent(this.cancelSpeechRequest));
            this.stopAudioAnimation(this.ttsSent = false);
        }
    }
    
    private void cleanupView(final GlanceViewCache.ViewType viewType, final ViewGroup viewGroup) {
        final ViewGroup viewGroup2 = (ViewGroup)viewGroup.getParent();
        if (viewGroup2 != null) {
            viewGroup2.removeView(viewGroup);
        }
        switch (viewType) {
            case SMALL_GLANCE_MESSAGE:
                this.mainTitle.setAlpha(1.0f);
                this.mainTitle.setVisibility(View.VISIBLE);
                this.subTitle.setAlpha(1.0f);
                this.subTitle.setVisibility(View.VISIBLE);
                this.choiceLayout.setAlpha(1.0f);
                this.choiceLayout.setVisibility(View.VISIBLE);
                this.choiceLayout.setTag(null);
                this.choiceLayout.setTag(R.id.message_prev_choice, null);
                this.choiceLayout.setTag(R.id.message_secondary_screen, null);
                this.mainImage.setImage(0, null, InitialsImageView.Style.DEFAULT);
                this.mainImage.setTag(null);
                this.sideImage.setImageResource(0);
                viewGroup.setAlpha(1.0f);
                break;
            case BIG_GLANCE_MESSAGE:
                viewGroup.setAlpha(1.0f);
                if (this.scrollView != null) {
                    this.scrollView.fullScroll(33);
                    this.onTop = true;
                    this.onBottom = false;
                    break;
                }
                break;
            case BIG_GLANCE_MESSAGE_SINGLE:
                ((TextView)viewGroup.findViewById(R.id.title)).setTextAppearance(HudApplication.getAppContext(), R.style.glance_message_title);
            case BIG_MULTI_TEXT:
            case BIG_CALENDAR:
                viewGroup.setAlpha(1.0f);
                break;
            case SMALL_SIGN:
                this.mainTitle.setAlpha(1.0f);
                this.mainTitle.setVisibility(View.VISIBLE);
                this.subTitle.setAlpha(1.0f);
                this.subTitle.setVisibility(View.VISIBLE);
                this.choiceLayout.setAlpha(1.0f);
                this.choiceLayout.setVisibility(View.VISIBLE);
                this.choiceLayout.setTag(null);
                this.choiceLayout.setTag(R.id.message_prev_choice, null);
                this.choiceLayout.setTag(R.id.message_secondary_screen, null);
                this.sideImage.setImageResource(0);
                viewGroup.setAlpha(1.0f);
                break;
        }
        GlanceViewCache.putView(viewType, viewGroup);
    }
    
    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification(this.id);
    }
    
    private List<String> getCannedReplyMessages() {
        if (this.cannedReplyMessages == null) {
            this.cannedReplyMessages = GlanceConstants.getCannedMessages();
        }
        return this.cannedReplyMessages;
    }
    
    private ViewGroup getReplyView(final int n, final Context context) {
        ViewGroup viewGroup;
        if (n == 0) {
            if (this.replyExitView == null) {
                this.replyExitView = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.glance_reply_exit, null, false);
            }
            else {
                this.removeParent(this.replyExitView);
            }
            viewGroup = this.replyExitView;
        }
        else {
            if (this.replyMsgView == null) {
                this.replyMsgView = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.glance_large_message_single, null, false);
            }
            else {
                this.removeParent(this.replyMsgView);
            }
            final List<String> cannedReplyMessages = this.getCannedReplyMessages();
            int size = n;
            if (n > cannedReplyMessages.size()) {
                size = cannedReplyMessages.size();
            }
            ((TextView)this.replyMsgView.findViewById(R.id.title)).setText(GlanceConstants.message);
            ((TextView)this.replyMsgView.findViewById(R.id.message)).setText(cannedReplyMessages.get(size - 1));
            viewGroup = this.replyMsgView;
        }
        return viewGroup;
    }
    
    private boolean hasReplyAction() {
        return this.glanceEvent.actions != null && this.glanceEvent.actions.contains(GlanceEvent.GlanceActions.REPLY);
    }
    
    private void removeParent(final View view) {
        if (view != null) {
            final ViewGroup viewGroup = (ViewGroup)view.getParent();
            if (viewGroup != null) {
                viewGroup.removeView(view);
                GlanceNotification.sLogger.v("reply view removed");
            }
        }
    }
    
    private void reply(final String s, final String s2, final String s3) {
        this.alive = false;
        final GlanceHandler instance = GlanceHandler.getInstance();
        if (instance.sendMessage(s2, s, s3)) {
            instance.sendSmsSuccessNotification(this.id, s2, s, s3);
        }
        else {
            instance.sendSmsFailedNotification(s2, s, s3);
        }
    }
    
    private void revertChoice() {
        List<ChoiceLayout2.Choice> tag = null;
        int n2;
        final int n = n2 = 0;
        if (this.choiceLayout.getTag(R.id.message_secondary_screen) != null) {
            final Object tag2 = this.choiceLayout.getTag(R.id.message_prev_choice);
            this.choiceLayout.setTag(R.id.message_prev_choice, null);
            this.choiceLayout.setTag(R.id.message_secondary_screen, null);
            if (tag2 == GlanceConstants.numberAndReplyBack_1) {
                tag = GlanceConstants.numberAndReplyBack_1;
                n2 = 1;
            }
            else if (tag2 == GlanceConstants.numberAndNoReplyBack) {
                tag = GlanceConstants.numberAndNoReplyBack;
                n2 = 1;
            }
            else if (tag2 == GlanceConstants.noMessage) {
                tag = GlanceConstants.noMessage;
                n2 = n;
            }
            else {
                if (tag2 != GlanceConstants.noNumberandNoReplyBack) {
                    this.choiceLayout.setTag(null);
                    this.setChoices();
                    return;
                }
                tag = GlanceConstants.noNumberandNoReplyBack;
                n2 = n;
            }
        }
        this.choiceLayout.setAlpha(1.0f);
        this.choiceLayout.setVisibility(View.VISIBLE);
        this.choiceLayout.setChoices(tag, n2, this.choiceListener, 0.5f);
        this.choiceLayout.setTag(tag);
    }
    
    private void sendTts() {
        if (this.controller != null && this.controller.isTtsOn() && !this.ttsSent) {
            GlanceNotification.sLogger.v("tts-send [" + this.id + "]");
            this.bus.post(new LocalSpeechRequest(new SpeechRequest.Builder().words(this.ttsMessage).category(SpeechRequest.Category.SPEECH_MESSAGE_READ_OUT).id(this.id).sendStatus(true).build()));
            this.ttsSent = true;
        }
    }
    
    private void setChoices() {
        if (this.controller.isExpandedWithStack()) {
            this.switchToMode(Mode.READ, null);
        }
        else {
            List<ChoiceLayout2.Choice> tag = null;
            final int n = 0;
            final boolean b = false;
            int n2 = 0;
            switch (this.glanceApp) {
                default: {
                    n2 = n;
                    if (this.choiceLayout.getTag() != null) {
                        break;
                    }
                    boolean b2 = b;
                    if (this.hasReplyAction()) {
                        b2 = true;
                    }
                    if (this.number != null || this.contacts != null) {
                        if (b2) {
                            tag = GlanceConstants.numberAndReplyBack_1;
                            n2 = 1;
                            break;
                        }
                        tag = GlanceConstants.numberAndNoReplyBack;
                        n2 = 1;
                        break;
                    }
                    else {
                        if (GlanceHelper.isCalendarApp(this.glanceApp)) {
                            tag = GlanceConstants.calendarOptions;
                            n2 = n;
                            break;
                        }
                        tag = GlanceConstants.noNumberandNoReplyBack;
                        n2 = n;
                        break;
                    }
                }
                case FUEL:
                    if (this.data.get(FuelConstants.NO_ROUTE.name()) == null) {
                        tag = GlanceConstants.fuelChoices;
                    }
                    else {
                        tag = GlanceConstants.fuelChoicesNoRoute;
                    }
                    n2 = 0;
                    break;
            }
            this.choiceLayout.setAlpha(1.0f);
            this.choiceLayout.setVisibility(View.VISIBLE);
            this.choiceLayout.setChoices(tag, n2, this.choiceListener, 0.5f);
            this.choiceLayout.setTag(tag);
        }
    }
    
    private void setExpandedContent(final View view) {
        if (this.glanceExtendedContainer != null) {
            switch (this.glanceApp) {
                case GOOGLE_CALENDAR:
                case APPLE_CALENDAR:
                case GENERIC_CALENDAR: {
                    final TextView textView = (TextView)view.findViewById(R.id.text1);
                    final String text = this.data.get(CalendarConstants.CALENDAR_TIME_STR.name());
                    if (!TextUtils.isEmpty(text)) {
                        textView.setText(text);
                        textView.setVisibility(View.VISIBLE);
                    }
                    else {
                        textView.setVisibility(GONE);
                    }
                    final TextView textView2 = (TextView)view.findViewById(R.id.text2);
                    final String text2 = this.data.get(CalendarConstants.CALENDAR_TITLE.name());
                    if (!TextUtils.isEmpty(text2)) {
                        textView2.setText(text2);
                        textView2.setVisibility(View.VISIBLE);
                    }
                    else {
                        textView2.setVisibility(GONE);
                    }
                    final ViewGroup viewGroup = (ViewGroup)view.findViewById(R.id.locationContainer);
                    final String text3 = this.data.get(CalendarConstants.CALENDAR_LOCATION.name());
                    if (!TextUtils.isEmpty(text3)) {
                        ((TextView)view.findViewById(R.id.text3)).setText(text3);
                        viewGroup.setVisibility(View.VISIBLE);
                        break;
                    }
                    viewGroup.setVisibility(GONE);
                    break;
                }
                case FUEL: {
                    final TextView textView3 = (TextView)view.findViewById(R.id.title1);
                    textView3.setTextAppearance(view.getContext(), R.style.glance_title_1);
                    final TextView textView4 = (TextView)view.findViewById(R.id.title2);
                    textView4.setTextAppearance(view.getContext(), R.style.glance_title_2);
                    textView3.setText(this.data.get(FuelConstants.GAS_STATION_NAME.name()));
                    if (this.data.get(FuelConstants.NO_ROUTE.name()) == null) {
                        textView4.setText(view.getContext().getResources().getString(R.string.gas_station_detail, this.data.get(FuelConstants.GAS_STATION_ADDRESS.name()), this.data.get(FuelConstants.GAS_STATION_DISTANCE.name())));
                    }
                    else {
                        textView4.setText(view.getContext().getResources().getString(R.string.i_cannot_add_gasstation));
                    }
                    textView3.setVisibility(View.VISIBLE);
                    textView4.setVisibility(View.VISIBLE);
                    break;
                }
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                case GOOGLE_HANGOUT:
                case SLACK:
                case WHATS_APP:
                case FACEBOOK_MESSENGER:
                case FACEBOOK:
                case GENERIC_MESSAGE:
                case TWITTER:
                case GENERIC_SOCIAL:
                case IMESSAGE:
                case SMS:
                case GENERIC: {
                    final TextView textView5 = (TextView)view.findViewById(R.id.title);
                    final TextView textView6 = (TextView)view.findViewById(R.id.message);
                    switch (this.glanceApp) {
                        default:
                            textView5.setText(GlanceHelper.getTimeStr(System.currentTimeMillis(), this.postTime, this.timeHelper));
                            break;
                        case GOOGLE_MAIL:
                        case APPLE_MAIL:
                        case GENERIC_MAIL:
                        case GOOGLE_INBOX: {
                            textView5.setTextColor(GlanceConstants.colorWhite);
                            this.subTitle.setVisibility(INVISIBLE);
                            String text4;
                            if ((text4 = this.data.get(EmailConstants.EMAIL_SUBJECT.name())) == null) {
                                text4 = "";
                            }
                            textView5.setText(text4);
                            break;
                        }
                    }
                    textView6.setText(this.messageStr);
                    break;
                }
            }
        }
    }

    private void setMainImage() {
        if (GlanceHelper.isCalendarApp(this.glanceApp)) {
            this.colorImageView.setColor(this.glanceApp.getColor());
            this.text1.setText(GlanceConstants.starts);
            long millis = 0;
            String str = this.data.get(CalendarConstants.CALENDAR_TIME.name());
            if (str != null) {
                try {
                    millis = Long.parseLong(str);
                } catch (Throwable t) {
                    sLogger.e(t);
                }
            }
            if (millis > 0) {
                int margin;
                String data = GlanceHelper.getCalendarTime(millis, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
                str = this.stringBuilder1.toString();
                if (TextUtils.isEmpty(str)) {
                    String pmMarker = this.stringBuilder2.toString();
                    if (TextUtils.isEmpty(pmMarker)) {
                        margin = GlanceConstants.calendarNowMargin;
                        this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_2);
                        this.text2.setText(data);
                        this.text3.setText("");
                    } else {
                        margin = GlanceConstants.calendarTimeMargin;
                        this.text3.setText("");
                        this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_3);
                        SpannableStringBuilder spannable = new SpannableStringBuilder();
                        spannable.append(data);
                        int len = spannable.length();
                        spannable.append(pmMarker);
                        spannable.setSpan(new AbsoluteSizeSpan(GlanceConstants.calendarpmMarker), len, spannable.length(), 33);
                        this.text2.setText(spannable);
                    }
                } else {
                    margin = GlanceConstants.calendarNormalMargin;
                    this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_1);
                    this.text2.setText(data);
                    this.text3.setText(str);
                }
                ((MarginLayoutParams) this.text2.getLayoutParams()).topMargin = margin;
            } else {
                this.text1.setText("");
                this.text2.setTextAppearance(HudApplication.getAppContext(), R.style.small_glance_sign_text_2);
                this.text2.setText(GlanceConstants.questionMark);
                this.text3.setText("");
                ((MarginLayoutParams) this.text2.getLayoutParams()).topMargin = GlanceConstants.calendarNowMargin;
            }
            this.ttsMessage = GlanceHelper.getTtsMessage(this.glanceApp, this.data, this.stringBuilder1, this.stringBuilder2, this.timeHelper);
            this.stringBuilder1.setLength(0);
            this.stringBuilder2.setLength(0);
            return;
        }
        int image = this.glanceApp.getMainIcon();
        if (this.glanceApp == GlanceApp.GENERIC) {
            String iconStr = this.data.get(GenericConstants.GENERIC_MAIN_ICON.name());
            if (!TextUtils.isEmpty(iconStr)) {
                int nIcon = GlanceHelper.getIcon(iconStr);
                if (nIcon != -1) {
                    image = nIcon;
                }
            }
        }
        ContactImageHelper contactImageHelper;
        if (image != -1) {
            this.mainImage.setImage(image, null, InitialsImageView.Style.DEFAULT);
        } else if (this.photoCheckRequired) {
            if (this.contacts == null || this.contacts.size() <= 1) {
                ContactUtil.setContactPhoto(this.source, this.number, this.photoCheckRequired, this.mainImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
            } else {
                contactImageHelper = ContactImageHelper.getInstance();
                this.mainImage.setImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(this.source)), ContactUtil.getInitials(this.source), InitialsImageView.Style.LARGE);
            }
            this.mainImage.requestLayout();
        } else if (this.glanceApp.isDefaultIconBasedOnId()) {
            contactImageHelper = ContactImageHelper.getInstance();
            this.mainImage.setImage(contactImageHelper.getResourceId(contactImageHelper.getContactImageIndex(this.source)), ContactUtil.getInitials(this.source), InitialsImageView.Style.LARGE);
        }
    }
    private void setSideImage() {
        this.sideImage.setImageResource(this.appIcon);
    }
    
    private void setSubTitle() {
        String text;
        final String s = text = GlanceHelper.getSubTitle(this.glanceApp, this.data);
        if (TextUtils.isEmpty(s)) {
            text = s;
            if (this.number != null) {
                text = s;
                if (!TextUtils.equals(this.number, this.source)) {
                    text = PhoneUtil.formatPhoneNumber(this.number);
                }
            }
        }
        this.subTitle.setText(text);
    }
    
    private void setTitle() {
        this.source = GlanceHelper.getTitle(this.glanceApp, this.data);
        if (this.number == null) {
            this.number = GlanceHelper.getNumber(this.glanceApp, this.data);
        }
        boolean b = false;
        if (this.number != null) {
            b = b;
            if (TextUtils.equals(this.source, this.number)) {
                b = true;
            }
        }
        if (b) {
            this.mainTitle.setText(PhoneUtil.formatPhoneNumber(this.source));
        }
        else {
            this.mainTitle.setText(this.source);
        }
    }
    
    private void startAudioAnimation() {
        if (this.audioFeedback != null) {
            this.audioFeedback.animate().cancel();
            this.audioFeedback.setAlpha(1.0f);
            this.audioFeedback.setVisibility(View.VISIBLE);
            this.audioFeedback.setImageResource(R.drawable.audio_loop);
            ((AnimationDrawable)this.audioFeedback.getDrawable()).start();
        }
    }
    
    private void stopAudioAnimation(final boolean b) {
        if (this.audioFeedback != null && this.audioFeedback.getVisibility() == View.VISIBLE) {
            final AnimationDrawable animationDrawable = (AnimationDrawable)this.audioFeedback.getDrawable();
            if (animationDrawable.isRunning()) {
                animationDrawable.stop();
                if (b) {
                    this.audioFeedback.animate().alpha(0.0f).setDuration(300L).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            GlanceNotification.this.audioFeedback.setVisibility(GONE);
                            GlanceNotification.this.audioFeedback.setAlpha(1.0f);
                        }
                    });
                }
                else {
                    this.audioFeedback.setVisibility(GONE);
                }
            }
        }
    }
    
    private void switchToMode(final Mode mode, final String s) {
        if (this.controller != null) {
            this.controller.stopTimeout(true);
            this.choiceLayout.setTag(R.id.message_prev_choice, this.choiceLayout.getTag());
            this.choiceLayout.setTag(R.id.message_secondary_screen, 1);
            if (mode == Mode.READ) {
                this.operationMode = mode;
                this.choiceLayout.setChoices(GlanceConstants.backChoice2, 0, this.choiceListener, 0.5f);
                this.choiceLayout.setTag(GlanceConstants.backChoice);
                if (!this.controller.isExpandedWithStack()) {
                    this.controller.expandNotification(true);
                }
            }
            else {
                this.operationMode = mode;
                this.choiceLayout.setChoices(null, 0, null, 0.5f);
                this.choiceLayout.setTag(GlanceConstants.backChoice);
                if (!this.controller.isExpandedWithStack()) {
                    this.initialReplyMode = true;
                    this.controller.expandNotification(false);
                }
            }
        }
    }
    
    private void switchToReplyScreen() {
        final Bundle bundle = new Bundle();
        final ArrayList<Contact> list = new ArrayList<Contact>();
        if (this.contacts == null) {
            list.add(new Contact(this.source, this.number, NumberType.OTHER, ContactImageHelper.getInstance().getContactImageIndex(this.number), 0L));
        }
        else {
            list.addAll(this.contacts);
        }
        bundle.putParcelableArrayList("CONTACTS", list);
        bundle.putString("NOTIF_ID", this.id);
        bundle.putInt("MENU_MODE", MainMenuScreen2.MenuMode.REPLY_PICKER.ordinal());
        NotificationManager.getInstance().removeNotification(this.id, false, Screen.SCREEN_MAIN_MENU, bundle, null);
    }
    
    private void updateState() {
        if (RemoteDeviceManager.getInstance().getRemoteDeviceInfo() == null) {
            this.dismissNotification();
        }
        else {
            this.setTitle();
            this.setSubTitle();
            this.setSideImage();
            this.setMainImage();
            this.setChoices();
            if (this.controller.isExpandedWithStack()) {
                this.mainTitle.setAlpha(0.0f);
                this.subTitle.setAlpha(0.0f);
                this.choiceLayout.setAlpha(0.0f);
            }
        }
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public boolean expandNotification() {
        boolean b;
        if (this.choiceLayout.getTag(R.id.message_secondary_screen) == null) {
            this.switchToMode(Mode.READ, null);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    @Override
    public int getColor() {
        return this.color;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        ViewGroup viewGroup;
        if (this.operationMode == Mode.READ || this.operationMode == null) {
            this.glanceExtendedContainer = (ViewGroup)GlanceViewCache.getView(this.largeType, context);
            if (this.supportsScroll) {
                (this.topScrub = this.glanceExtendedContainer.findViewById(R.id.topScrub)).setVisibility(GONE);
                (this.bottomScrub = this.glanceExtendedContainer.findViewById(R.id.bottomScrub)).setVisibility(View.VISIBLE);
                (this.scrollView = (ObservableScrollView)this.glanceExtendedContainer.findViewById(R.id.scrollView)).setScrollListener(this.scrollListener);
            }
            this.setExpandedContent(this.glanceExtendedContainer);
            viewGroup = this.glanceExtendedContainer;
        }
        else {
            final NotificationManager instance = NotificationManager.getInstance();
            int intValue;
            if (this.initialReplyMode) {
                this.initialReplyMode = false;
                intValue = 1;
            }
            else {
                intValue = (int)o;
            }
            final View expandedViewChild = instance.getExpandedViewChild();
            if (expandedViewChild != null) {
                this.removeParent(expandedViewChild);
            }
            viewGroup = this.getReplyView(intValue, context);
        }
        return viewGroup;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return GlanceConstants.colorWhite;
    }
    
    public GlanceApp getGlanceApp() {
        return this.glanceApp;
    }
    
    @Override
    public String getId() {
        return this.id;
    }
    
    @Override
    public int getTimeout() {
        return 25000;
    }
    
    @Override
    public NotificationType getType() {
        return this.glanceApp.notificationType;
    }
    
    @Override
    public View getView(final Context context) {
        this.glanceContainer = (ViewGroup)GlanceViewCache.getView(this.smallType, context);
        this.mainTitle = (TextView)this.glanceContainer.findViewById(R.id.mainTitle);
        this.subTitle = (TextView)this.glanceContainer.findViewById(R.id.subTitle);
        this.sideImage = (ImageView)this.glanceContainer.findViewById(R.id.sideImage);
        this.choiceLayout = (ChoiceLayout2)this.glanceContainer.findViewById(R.id.choiceLayout);
        this.audioFeedback = (ImageView)this.glanceContainer.findViewById(R.id.audioFeedback);
        if (GlanceHelper.isCalendarApp(this.glanceApp)) {
            this.colorImageView = (ColorImageView)this.glanceContainer.findViewById(R.id.mainImage);
            this.text1 = (TextView)this.glanceContainer.findViewById(R.id.text1);
            this.text2 = (TextView)this.glanceContainer.findViewById(R.id.text2);
            this.text3 = (TextView)this.glanceContainer.findViewById(R.id.text3);
        }
        else {
            this.mainImage = (InitialsImageView)this.glanceContainer.findViewById(R.id.mainImage);
        }
        return this.glanceContainer;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        final AnimatorSet set = new AnimatorSet();
        if (!b) {
            set.playTogether(ObjectAnimator.ofFloat(this.mainTitle, View.ALPHA, 1.0f, 0.0f), ObjectAnimator.ofFloat(this.subTitle, View.ALPHA, 1.0f, 0.0f), ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, 1.0f, 0.0f));
        }
        else {
            set.playTogether(ObjectAnimator.ofFloat(this.mainTitle, View.ALPHA, 0.0f, 1.0f), ObjectAnimator.ofFloat(this.subTitle, View.ALPHA, 0.0f, 1.0f), ObjectAnimator.ofFloat(this.choiceLayout, View.ALPHA, 0.0f, 1.0f));
        }
        return set;
    }
    
    @Override
    public boolean isAlive() {
        return this.alive;
    }

    public boolean isContextValid() {
        return this.controller != null;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }

    public boolean isShowingReadOption() {
        if (this.choiceLayout != null) {
            Object tag = this.choiceLayout.getTag();
            return tag == GlanceConstants.noNumberandNoReplyBack || tag == GlanceConstants.numberAndNoReplyBack || tag == GlanceConstants.numberAndReplyBack_1;
        }
        return false;
    }
    
    @Override
    public InputManager.IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onClick() {
    }
    
    @Subscribe
    public void onConnectionStateChange(final ConnectionStateChange connectionStateChange) {
        if (connectionStateChange.state == ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED) {
            this.stopAudioAnimation(true);
        }
    }
    
    @Subscribe
    public void onContactFound(final RecentCallManager.ContactFound contactFound) {
        if (this.controller != null) {
            final Object tag = this.choiceLayout.getTag();
            if (tag != null && tag == GlanceConstants.noNumberandNoReplyBack && contactFound.contact != null && TextUtils.equals(contactFound.identifier, this.source)) {
                for (final com.navdy.service.library.events.contacts.Contact contact : contactFound.contact) {
                    if (this.contacts == null) {
                        this.contacts = new ArrayList<Contact>();
                    }
                    final Contact contact2 = new Contact(contact);
                    contact2.setName(this.source);
                    this.contacts.add(contact2);
                    if (contactFound.contact.size() == 1) {
                        this.number = contact.number;
                    }
                }
                GlanceNotification.sLogger.v("contact info found:" + contactFound.identifier + " number:" + this.number + " size:" + contactFound.contact.size());
                this.setSubTitle();
                if (!this.hasReplyAction()) {
                    this.choiceLayout.setChoices(GlanceConstants.numberAndNoReplyBack, 1, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(GlanceConstants.numberAndNoReplyBack);
                }
                else {
                    this.choiceLayout.setChoices(GlanceConstants.numberAndReplyBack_1, 1, this.choiceListener, 0.5f);
                    this.choiceLayout.setTag(GlanceConstants.numberAndReplyBack_1);
                }
                if (this.contacts != null && this.contacts.size() > 1) {
                    final ContactImageHelper instance = ContactImageHelper.getInstance();
                    this.mainImage.setImage(instance.getResourceId(instance.getContactImageIndex(this.source)), ContactUtil.getInitials(this.source), InitialsImageView.Style.LARGE);
                }
                else {
                    ContactUtil.setContactPhoto(this.source, contactFound.identifier, false, this.mainImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
                }
            }
        }
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
        if (mode == UIStateManager.Mode.COLLAPSE) {
            this.cancelTts();
            this.operationMode = null;
            if (this.choiceLayout.getTag(R.id.message_secondary_screen) != null && this.controller != null) {
                this.revertChoice();
                this.controller.startTimeout(this.getTimeout());
            }
            this.removeParent(this.replyMsgView);
            this.replyMsgView = null;
            this.removeParent(this.replyExitView);
            this.replyExitView = null;
            this.initialReplyMode = false;
            switch (this.glanceApp) {
                case GOOGLE_MAIL:
                case APPLE_MAIL:
                case GENERIC_MAIL:
                case GOOGLE_INBOX:
                    this.subTitle.setVisibility(View.VISIBLE);
                    break;
            }
        }
        else if (this.operationMode == Mode.READ || this.operationMode == null) {
            this.sendTts();
        }
        else {
            NotificationManager.getInstance().updateExpandedIndicator(this.getCannedReplyMessages().size() + 1, 1, GlanceConstants.colorWhite);
        }
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
        if (this.controller != null && this.controller.isExpandedWithStack()) {
            this.sendTts();
        }
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Override
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean b = false;
        if (this.controller == null) {
            b = true;
        }
        else {
            if (this.supportsScroll && this.scrollView != null && this.controller.isExpanded()) {
                switch (customKeyEvent) {
                    case LEFT:
                        b = this.scrollView.arrowScroll(33);
                        if (this.onTop) {
                            b = false;
                            return b;
                        }
                        return b;
                    case RIGHT:
                        b = this.scrollView.arrowScroll(130);
                        if (this.onBottom) {
                            b = false;
                            return b;
                        }
                        return b;
                }
            }
            if (this.operationMode == Mode.REPLY) {
                final int expandedIndicatorCurrentItem = NotificationManager.getInstance().getExpandedIndicatorCurrentItem();
                switch (customKeyEvent) {
                    default:
                        b = true;
                        break;
                    case LEFT:
                        b = false;
                        break;
                    case RIGHT:
                        b = false;
                        break;
                    case SELECT:
                        if (expandedIndicatorCurrentItem == 0) {
                            this.controller.collapseNotification(false, false);
                        }
                        else {
                            this.reply(this.getCannedReplyMessages().get(expandedIndicatorCurrentItem - 1), this.number, this.source);
                        }
                        b = true;
                        break;
                }
            }
            else {
                if (!false) {
                    switch (customKeyEvent) {
                        case LEFT:
                            this.choiceLayout.moveSelectionLeft();
                            b = true;
                            return b;
                        case RIGHT:
                            this.choiceLayout.moveSelectionRight();
                            b = true;
                            return b;
                        case SELECT: {
                            final ChoiceLayout2.Choice currentSelectedChoice = this.choiceLayout.getCurrentSelectedChoice();
                            if (currentSelectedChoice != null) {
                                switch (currentSelectedChoice.getId()) {
                                    case 2:
                                        AnalyticsSupport.recordGlanceAction("Glance_Open_Full", this, "dial");
                                        break;
                                    case 5:
                                        AnalyticsSupport.recordGlanceAction("Glance_Open_Mini", this, "dial");
                                        break;
                                    case 6:
                                        AnalyticsSupport.recordGlanceAction("Glance_Dismiss", this, "dial");
                                        break;
                                }
                            }
                            this.choiceLayout.executeSelectedItem();
                            b = true;
                            return b;
                        }
                    }
                }
                b = false;
            }
        }
        return b;
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Subscribe
    public void onPhotoDownload(final PhoneImageDownloader.PhotoDownloadStatus photoDownloadStatus) {
        if (this.controller != null && this.photoCheckRequired && photoDownloadStatus.photoType == PhotoType.PHOTO_CONTACT && (TextUtils.equals(photoDownloadStatus.sourceIdentifier, this.source) || (this.number != null && TextUtils.equals(photoDownloadStatus.sourceIdentifier, this.number))) && (!photoDownloadStatus.alreadyDownloaded || this.mainImage.getTag() == null)) {
            GlanceNotification.sLogger.v("photo available");
            ContactUtil.setContactPhoto(this.source, this.number, false, this.mainImage, this.roundTransformation, (ContactUtil.IContactCallback)this);
        }
    }
    
    @Subscribe
    public void onShowToast(final ToastManager$ShowToast showToast) {
        if (TextUtils.equals(showToast.name, this.id + "message-sent-toast")) {
            GlanceNotification.sLogger.v("reply toast:animateOutExpandedView");
            NotificationManager.getInstance().collapseExpandedNotification(true, true);
        }
    }
    
    @Subscribe
    public void onSpeechRequestNotification(final SpeechRequestStatus speechRequestStatus) {
        if (this.audioFeedback != null && !TextUtils.isEmpty(this.id) && TextUtils.equals(this.id, speechRequestStatus.id) && speechRequestStatus.status != null) {
            switch (speechRequestStatus.status) {
                case SPEECH_REQUEST_STARTING:
                    this.startAudioAnimation();
                    break;
                case SPEECH_REQUEST_STOPPED:
                    this.stopAudioAnimation(true);
                    break;
            }
        }
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        GlanceNotification.sLogger.v("start called:" + System.identityHashCode(this) + " expanded:" + controller.isExpandedWithStack());
        this.bus.register(this);
        this.controller = controller;
        if (this.choiceLayout != null) {
            this.choiceLayout.setTag(null);
            this.choiceLayout.setTag(R.id.message_prev_choice, null);
            this.choiceLayout.setTag(R.id.message_secondary_screen, null);
        }
        GlanceNotification.handler.postDelayed(this.updateTimeRunnable, 30000L);
        this.updateState();
    }
    
    @Override
    public void onStop() {
        GlanceNotification.sLogger.v("stop called:" + System.identityHashCode(this));
        GlanceNotification.handler.removeCallbacks(this.updateTimeRunnable);
        this.bus.unregister(this);
        this.cancelTts();
        if (this.choiceLayout != null) {
            this.choiceLayout.clear();
        }
        if (this.supportsScroll && this.scrollView != null) {
            this.scrollView.setScrollListener(null);
        }
        this.controller = null;
        if (this.glanceContainer != null) {
            this.cleanupView(this.smallType, this.glanceContainer);
            this.glanceContainer = null;
        }
        if (this.glanceExtendedContainer != null) {
            this.cleanupView(this.largeType, this.glanceExtendedContainer);
            this.glanceExtendedContainer = null;
        }
        this.removeParent(this.replyMsgView);
        this.replyMsgView = null;
        this.removeParent(this.replyExitView);
        this.replyExitView = null;
        this.operationMode = null;
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public void onUpdate() {
    }
    
    public void setContactList(final List<Contact> contacts) {
        this.contacts = contacts;
    }
    
    @Override
    public void setListener(final IProgressUpdate progressUpdate) {
        this.progressUpdate = progressUpdate;
    }
    
    @Override
    public boolean supportScroll() {
        return this.supportsScroll;
    }

    public enum Mode {
        REPLY(0),
        READ(1);

        private int value;
        Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
