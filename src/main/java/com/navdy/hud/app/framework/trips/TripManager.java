package com.navdy.hud.app.framework.trips;

import android.content.SharedPreferences;
import android.location.Location;
import android.os.SystemClock;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.navdy.hud.app.analytics.TelemetrySession;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.util.MapUtils;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.events.TripUpdate.Builder;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TripManager {
    public static final int MAX_SPEED_METERS_PER_SECOND_THRESHOLD = 200;
    private static final long MIN_TIME_THRESHOLD = 1000;
    public static final String PREFERENCE_TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY = "trip_manager_total_distance_travelled_with_navdy";
    public static final long TOTAL_DISTANCE_PERSIST_INTERVAL = 30000;
    private static final Logger sLogger = new Logger(TripManager.class);
    private final Bus bus;
    private int currentDistanceTraveledInMeters;
    private int currentSequenceNumber;
    private volatile State currentState;
    private long currentTripNumber;
    private long currentTripStartTime;
    private boolean isClientConnected;
    private GeoCoordinate lastCoords;
    private Location lastLocation;
    private long lastTotalDistancePersistTime = 0;
    private long lastTotalDistancePersistedValue = 0;
    private long lastTrackingTime;
    private Builder lastTripUpdateBuilder;
    private StringBuilder logBuilder;
    SharedPreferences preferences;
    private final SpeedManager speedManager;
    private int totalDistanceTravelledInMeters;

    public static class FinishedTripRouteEvent {
    }

    public static class NewTripEvent {
    }

    public enum State {
        STARTED,
        STOPPED
    }

    public static class TrackingEvent {
        private final String chosenDestinationId;
        private final int distanceToDestination;
        private final int estimatedTimeRemaining;
        private final GeoPosition geoPosition;

        public TrackingEvent(GeoPosition geoPosition) {
            this.geoPosition = geoPosition;
            this.chosenDestinationId = null;
            this.estimatedTimeRemaining = -1;
            this.distanceToDestination = -1;
        }

        public TrackingEvent(GeoPosition geoPosition, String chosenDestinationId, int estimatedTimeRemaining, int distanceToDestination) {
            this.geoPosition = geoPosition;
            this.chosenDestinationId = chosenDestinationId;
            this.estimatedTimeRemaining = estimatedTimeRemaining;
            this.distanceToDestination = distanceToDestination;
        }

        public boolean isRouteTracking() {
            if (TripManager.sLogger.isLoggable(2)) {
                TripManager.sLogger.v("[routeTracking] chosenDestinationId=" + this.chosenDestinationId + "; estimatedTimeRemaining=" + this.estimatedTimeRemaining + "; distanceToDestination=" + this.distanceToDestination);
            }
            return this.chosenDestinationId != null && this.estimatedTimeRemaining >= 0 && this.distanceToDestination >= 0;
        }
    }

    @Inject
    public TripManager(Bus bus, SharedPreferences preferences) {
        this.bus = bus;
        this.speedManager = SpeedManager.getInstance();
        this.preferences = preferences;
        this.currentState = State.STOPPED;
        this.logBuilder = new StringBuilder();
        this.isClientConnected = false;
        this.totalDistanceTravelledInMeters = 0;
    }

    public int getCurrentDistanceTraveled() {
        return this.currentDistanceTraveledInMeters;
    }

    public long getCurrentTripStartTime() {
        return this.currentTripStartTime;
    }

    public int getTotalDistanceTravelled() {
        return this.totalDistanceTravelledInMeters;
    }

    @Subscribe
    public void onLocation(Location location) {
        this.lastLocation = location;
    }

    @Subscribe
    public void onTrack(final TrackingEvent event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (TripManager.this.currentState == State.STOPPED) {
                    TripManager.this.createNewTrip();
                    TripManager.this.currentState = State.STARTED;
                }
                GeoCoordinate coords = event.geoPosition.getCoordinate();
                double latitude = coords.getLatitude();
                double longitude = coords.getLongitude();
                double altitude = coords.getAltitude();
                if (MapUtils.sanitizeCoords(coords) == null) {
                    TripManager.sLogger.v("filtering out bad coords: " + latitude + ", " + longitude + ", " + altitude);
                    return;
                }
                if (TripManager.this.lastCoords != null) {
                    long currentTime = SystemClock.elapsedRealtime();
                    long timeElapsed = currentTime - TripManager.this.lastTrackingTime;
                    if (timeElapsed >= 1000) {
                        TripManager.this.lastTrackingTime = currentTime;
                        int dist = (int) event.geoPosition.getCoordinate().distanceTo(TripManager.this.lastCoords);
                        if (((double) dist) / (((double) timeElapsed) / 1000.0d) <= 200.0d) {
                            TripManager.this.currentDistanceTraveledInMeters = TripManager.this.currentDistanceTraveledInMeters + dist;
                            TripManager.this.totalDistanceTravelledInMeters = TripManager.this.totalDistanceTravelledInMeters + dist;
                            if (TripManager.this.lastTotalDistancePersistTime == 0 || currentTime - TripManager.this.lastTotalDistancePersistTime > 30000) {
                                TripManager.this.lastTotalDistancePersistTime = currentTime;
                                long newValue = TripManager.this.getTotalDistanceTravelledWithNavdy();
                                TripManager.this.preferences.edit().putLong(TripManager.PREFERENCE_TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY, newValue).apply();
                                TripManager.this.lastTotalDistancePersistedValue = (long) TripManager.this.totalDistanceTravelledInMeters;
                            }
                        } else {
                            TripManager.sLogger.d("Unusual jump between co ordinates, Last lat :" + TripManager.this.lastCoords.getLatitude() + ", " + "Lon :" + TripManager.this.lastCoords.getLongitude() + ", " + "Current Lat:" + event.geoPosition.getCoordinate().getLatitude() + ", " + "Lon :" + event.geoPosition.getCoordinate().getLongitude());
                        }
                    } else {
                        return;
                    }
                }
                TripManager.this.lastCoords = event.geoPosition.getCoordinate();
                if (TripManager.this.isClientConnected) {
                    TripManager.this.currentSequenceNumber = TripManager.this.currentSequenceNumber + 1;
                    TelemetrySession telemetrySession = TelemetrySession.INSTANCE;
                    Builder updateBuilder = new Builder().trip_number(Long.valueOf(TripManager.this.currentTripNumber)).sequence_number(Integer.valueOf(TripManager.this.currentSequenceNumber)).timestamp(Long.valueOf(System.currentTimeMillis())).distance_traveled(Integer.valueOf(TripManager.this.currentDistanceTraveledInMeters)).current_position(new LatLong(Double.valueOf(latitude), Double.valueOf(longitude))).elevation(Double.valueOf(event.geoPosition.getCoordinate().getAltitude())).bearing(Float.valueOf((float) event.geoPosition.getHeading())).gps_speed(Float.valueOf((float) event.geoPosition.getSpeed())).obd_speed(Integer.valueOf(TripManager.this.speedManager.getObdSpeed())).hard_acceleration_count(Integer.valueOf(telemetrySession.getSessionHardAccelerationCount())).hard_breaking_count(Integer.valueOf(telemetrySession.getSessionHardBrakingCount())).high_g_count(Integer.valueOf(telemetrySession.getSessionHighGCount())).speeding_ratio(Double.valueOf((double) telemetrySession.getSessionSpeedingPercentage())).excessive_speeding_ratio(Double.valueOf((double) telemetrySession.getSessionExcessiveSpeedingPercentage())).meters_traveled_since_boot(Integer.valueOf((int) telemetrySession.getSessionDistance())).horizontal_accuracy(Float.valueOf((event.geoPosition.getLatitudeAccuracy() + event.geoPosition.getLongitudeAccuracy()) / 2.0f)).elevation_accuracy(Float.valueOf(event.geoPosition.getAltitudeAccuracy())).last_raw_coordinate(TripManager.this.toCoordinate(TripManager.this.lastLocation));
                    if (TripManager.sLogger.isLoggable(2)) {
                        TripManager.this.logBuilder.setLength(0);
                        TripManager.this.logBuilder.append("TripUpdate: tripNumber=" + TripManager.this.currentTripNumber + "; sequenceNumber=" + TripManager.this.currentSequenceNumber + "; timestamp=" + System.currentTimeMillis() + "; distanceTraveled=" + TripManager.this.currentDistanceTraveledInMeters + "; currentPosition=" + latitude + HereManeuverDisplayBuilder.COMMA + longitude + "; elevation=" + event.geoPosition.getCoordinate().getAltitude() + "; bearing=" + event.geoPosition.getHeading() + "; gpsSpeed=" + event.geoPosition.getSpeed() + "; obdSpeed=" + TripManager.this.speedManager.getObdSpeed());
                    }
                    if (event.isRouteTracking()) {
                        updateBuilder.chosen_destination_id(event.chosenDestinationId).estimated_time_remaining(Integer.valueOf(event.estimatedTimeRemaining)).distance_to_destination(Integer.valueOf(event.distanceToDestination));
                        if (TripManager.sLogger.isLoggable(2)) {
                            TripManager.this.logBuilder.append("; chosenDestinationId=" + event.chosenDestinationId + "; estimatedTimeRemaining=" + event.estimatedTimeRemaining + "; distanceToDestination=" + event.distanceToDestination);
                        }
                    }
                    if (TripManager.sLogger.isLoggable(2)) {
                        TripManager.sLogger.v(TripManager.this.logBuilder.toString());
                    }
                    TripManager.this.lastTripUpdateBuilder = updateBuilder;
                    if (TripManager.this.currentState != State.STOPPED) {
                        TripManager.this.bus.post(new RemoteEvent(TripManager.this.lastTripUpdateBuilder.build()));
                    }
                }
            }
        }, 8);
    }

    private Coordinate toCoordinate(Location lastLocation) {
        if (lastLocation == null) {
            return null;
        }
        return new Coordinate.Builder().accuracy(Float.valueOf(lastLocation.getAccuracy())).altitude(Double.valueOf(lastLocation.getAltitude())).bearing(Float.valueOf(lastLocation.getBearing())).latitude(Double.valueOf(lastLocation.getLatitude())).longitude(Double.valueOf(lastLocation.getLongitude())).provider(lastLocation.getProvider()).speed(Float.valueOf(lastLocation.getSpeed())).timestamp(Long.valueOf(lastLocation.getTime())).build();
    }

    @Subscribe
    public void onNewTrip(NewTripEvent event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                TripManager.this.currentState = State.STOPPED;
            }
        }, 8);
    }

    @Subscribe
    public void onFinishedTripRouteEvent(FinishedTripRouteEvent event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (TripManager.this.lastTripUpdateBuilder != null && TripManager.this.lastTripUpdateBuilder.chosen_destination_id != null) {
                    TripManager.this.lastTripUpdateBuilder.arrived_at_destination_id(TripManager.this.lastTripUpdateBuilder.chosen_destination_id);
                    if (TripManager.this.currentState != State.STOPPED) {
                        TripManager.this.bus.post(new RemoteEvent(TripManager.this.lastTripUpdateBuilder.build()));
                    }
                }
            }
        }, 8);
    }

    @Subscribe
    public void onConnectionStateChange(ConnectionStateChange event) {
        if (event.state == ConnectionState.CONNECTION_DISCONNECTED) {
            this.isClientConnected = false;
        } else if (event.state == ConnectionState.CONNECTION_VERIFIED) {
            this.isClientConnected = true;
        }
    }

    private void createNewTrip() {
        GenericUtil.checkNotOnMainThread();
        this.currentTripNumber = System.currentTimeMillis();
        this.currentSequenceNumber = 0;
        this.currentDistanceTraveledInMeters = 0;
        this.currentTripStartTime = System.currentTimeMillis();
    }

    public long getTotalDistanceTravelledWithNavdy() {
        return this.preferences.getLong(PREFERENCE_TOTAL_DISTANCE_TRAVELLED_WITH_NAVDY, 0) + (((long) this.totalDistanceTravelledInMeters) - this.lastTotalDistancePersistedValue);
    }
}
