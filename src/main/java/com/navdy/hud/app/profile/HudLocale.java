package com.navdy.hud.app.profile;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout.LayoutParams;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.view.ToastView;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;

public class HudLocale {
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String DEFAULT_LOCALE_ID = "en_US";
    private static final String LOCALE_SEPARATOR = "_";
    private static final String LOCALE_TOAST_ID = "#locale#toast";
    private static String MAP_ENGINE_PROCESS_NAME = "global.Here.Map.Service.v3";
    private static final String SELECTED_LOCALE = "Locale.Helper.Selected.Language";
    private static final String TAG = "[HUD-locale]";
    private static final HashSet<String> hudLanguages = new HashSet();
    final private static String OVERRIDE_SYSTEM_LOCALE = "persist.sys.override_locale";

    static {
        hudLanguages.add(DEFAULT_LANGUAGE);
        hudLanguages.add("fr");
        hudLanguages.add("de");
        hudLanguages.add("it");
        hudLanguages.add("es");
    }

    public static boolean isLocaleSupported(Locale locale) {
        if (locale == null) {
            return false;
        }
        return hudLanguages.contains(getBaseLanguage(locale.getLanguage()));
    }

    public static Context onAttach(Context context) {
        return setLocale(context, getCurrentLocale(context));
    }

    private static Context setLocale(Context context, Locale locale) {
        String languageTag = com.navdy.hud.app.util.os.SystemProperties.get(OVERRIDE_SYSTEM_LOCALE, locale.toLanguageTag());
        Locale finalLocale = Locale.forLanguageTag(languageTag);
        Log.e(TAG, "setLocale [" + finalLocale + "]");
        if (VERSION.SDK_INT >= 24) {
            return updateResources(context, finalLocale);
        }
        return updateResourcesLegacy(context, finalLocale);
    }

    @TargetApi(24)
    private static Context updateResources(Context context, Locale locale) {
        Locale.setDefault(locale);
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private static Context updateResourcesLegacy(Context context, Locale locale) {
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    public static Locale getCurrentLocale(Context context) {
        try {
            String localeId = PreferenceManager.getDefaultSharedPreferences(context).getString(SELECTED_LOCALE, "en_US");
            if (localeId.equals(DEFAULT_LANGUAGE)) {
                localeId = "en_US";
            }
            return getLocaleForID(localeId);
        } catch (Throwable t) {
            Log.e(TAG, "getCurrentLocale", t);
            return getLocaleForID("en_US");
        }
    }

    private static void setCurrentLocale(Context context, Locale locale) {
        Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(SELECTED_LOCALE, locale.toString());
        editor.commit();
    }

    public static boolean switchLocale(Context context, Locale locale) {
        try {
            GenericUtil.checkNotOnMainThread();
            if (isLocaleSupported(locale)) {
                Locale currentLocale = getCurrentLocale(context);
                if (locale.equals(currentLocale)) {
                    Log.e(TAG, "switchLocale language not changed:" + locale);
                    return false;
                }
                setCurrentLocale(context, locale);
                Log.e(TAG, "switchLocale changed language from [" + currentLocale + "] to [" + locale + "], restarting hud app");
                return true;
            }
            Log.e(TAG, "switchLocale hud does not support locale:" + locale);
            return false;
        } catch (Throwable t) {
            Log.e(TAG, "switchLocale", t);
            return false;
        }
    }

    public static void showLocaleNotSupportedToast(String language) {
        dismissToast();
        Resources resources = HudApplication.getAppContext().getResources();
        Bundle bundle = new Bundle();
        bundle.putBoolean(ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_not_supported);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.locale_not_supported, new Object[]{language}));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        ToastManager.getInstance().addToast(new ToastManager$ToastParams(LOCALE_TOAST_ID, bundle, null, true, true));
    }

    public static void dismissToast() {
        ToastManager.getInstance().dismissToast(LOCALE_TOAST_ID);
    }

    public static void showLocaleChangeToast() {
        ToastManager toastManager = ToastManager.getInstance();
        final Resources resources = HudApplication.getAppContext().getResources();
        Bundle bundle = new Bundle();
        toastManager.clearAllPendingToast();
        toastManager.disableToasts(false);
        toastManager.dismissCurrentToast(LOCALE_TOAST_ID);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_sm_spinner);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.updating_language));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        toastManager.addToast(new ToastManager$ToastParams(LOCALE_TOAST_ID, bundle, new IToastCallback() {
            ObjectAnimator animator;

            public void onStart(ToastView view) {
                ConfirmationLayout lyt = view.getView();
                MarginLayoutParams layoutParams = (MarginLayoutParams) lyt.screenImage.getLayoutParams();
                layoutParams.width = resources.getDimensionPixelSize(R.dimen.locale_change_icon_size);
                layoutParams.height = layoutParams.width;
                lyt.title1.setVisibility(View.GONE);
                lyt.title3.setVisibility(View.GONE);
                lyt.title4.setVisibility(View.GONE);
                ((LayoutParams) lyt.screenImage.getLayoutParams()).gravity = 17;
                lyt.findViewById(R.id.infoContainer).setPadding(0, 0, 0, 0);
                if (this.animator == null) {
                    this.animator = ObjectAnimator.ofFloat(lyt.screenImage, View.ROTATION, new float[]{360.0f});
                    this.animator.setDuration(500);
                    this.animator.setInterpolator(new AccelerateDecelerateInterpolator());
                    final ObjectAnimator anim = this.animator;
                    this.animator.addListener(new DefaultAnimationListener() {
                        public void onAnimationEnd(Animator animation) {
                            if (anim != null) {
                                anim.setStartDelay(33);
                                anim.start();
                            }
                        }
                    });
                }
                if (!this.animator.isRunning()) {
                    this.animator.start();
                }
            }

            public void onStop() {
                if (this.animator != null) {
                    this.animator.removeAllListeners();
                    this.animator.cancel();
                }
            }

            public boolean onKey(CustomKeyEvent event) {
                return false;
            }

            public void executeChoiceItem(int pos, int id) {
            }
        }, true, true));
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            public void run() {
                HudLocale.killSelfAndMapEngine();
            }
        }, 2000);
    }

    private static void killSelfAndMapEngine() {
        for (RunningAppProcessInfo processInfo : ((ActivityManager) Objects.requireNonNull(HudApplication.getAppContext().getSystemService(Context.ACTIVITY_SERVICE))).getRunningAppProcesses()) {
            if (processInfo.processName.equals(MAP_ENGINE_PROCESS_NAME)) {
                Process.killProcess(processInfo.pid);
                break;
            }
        }
        System.exit(0);
    }

    public static String getBaseLanguage(String language) {
        int index = 0;
        while (index < language.length()) {
            char c = language.charAt(index);
            if (c == '_' || c == '-') {
                break;
            }
            index++;
        }
        if (index <= 0 || index >= language.length()) {
            return language;
        }
        return language.substring(0, index);
    }

    public static Locale getLocaleForID(String localeID) {
        if (TextUtils.isEmpty(localeID)) {
            return null;
        }
        int index = localeID.indexOf("_");
        if (index < 0) {
            return new Locale(localeID);
        }
        String language = localeID.substring(0, index);
        localeID = localeID.substring(index + 1);
        index = localeID.indexOf("_");
        if (index > 0) {
            return new Locale(language, localeID.substring(0, index), localeID.substring(index + 1));
        }
        return new Locale(language, localeID);
    }
}
