package com.navdy.hud.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.notifications.INotification;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.gesture.GestureDetector.GestureListener;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.presenter.NotificationPresenter;
import com.navdy.hud.app.ui.component.ShrinkingBorderView;
import com.navdy.hud.app.ui.component.ShrinkingBorderView.IListener;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import javax.inject.Inject;
import mortar.Mortar;

public class NotificationView extends RelativeLayout implements IInputHandler, GestureListener {
    private static final int CLICK_COUNT = 2;
    private static final Logger sLogger = new Logger(NotificationView.class);
    @InjectView(R.id.border)
    public ShrinkingBorderView border;
    private IListener borderListener;
    private MultipleClickGestureDetector clickGestureDetector;
    private IMultipleClickKeyGesture clickGestureListener;
    @InjectView(R.id.customNotificationContainer)
    FrameLayout customNotificationContainer;
    private int defaultColor;
    private boolean initialized;
    @InjectView(R.id.nextNotificationColorView)
    public ColorImageView nextNotificationColorView;
    private INotification notification;
    private NotificationManager notificationManager;
    @Inject
    NotificationPresenter presenter;

    public NotificationView(Context context) {
        this(context, null);
    }

    public NotificationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.clickGestureListener = new IMultipleClickKeyGesture() {
            public void onMultipleClick(int count) {
                NotificationView.this.takeNotificationAction(false);
            }

            public boolean onGesture(GestureEvent event) {
                return false;
            }

            public boolean onKey(CustomKeyEvent event) {
                if (NotificationView.this.notification == null) {
                    return false;
                }
                if (NotificationView.this.notificationManager.isExpanded() || NotificationView.this.notificationManager.isExpandedNotificationVisible()) {
                    return NotificationView.this.notificationManager.handleKey(event);
                }
                return NotificationView.this.notification.onKey(event);
            }

            public IInputHandler nextHandler() {
                return null;
            }
        };
        this.borderListener = new IListener() {
            public void timeout() {
                if (NotificationView.this.initialized) {
                    NotificationManager.getInstance().currentNotificationTimeout();
                }
            }
        };
        if (!isInEditMode()) {
            Mortar.inject(context, this);
            this.notificationManager = NotificationManager.getInstance();
            this.clickGestureDetector = new MultipleClickGestureDetector(2, this.clickGestureListener);
        }
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.defaultColor = getContext().getResources().getColor(R.color.black);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.border.setListener(this.borderListener);
        resetNextNotificationColor();
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        if (!this.initialized || this.notification == null || event == null || event.gesture == null) {
            return false;
        }
        if (this.notificationManager.isAnimating()) {
            sLogger.v("ignore gesture, animation in progress");
            return true;
        }
        switch (event.gesture) {
            case GESTURE_SWIPE_LEFT:
                if (!this.notificationManager.isExpanded() && !this.notificationManager.isExpandedNotificationVisible()) {
                    if (!this.notification.onGesture(event)) {
                        AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_FULL, this.notification, "swipe");
                        sLogger.v("gesture expanded:" + this.notification.expandNotification());
                        break;
                    }
                    sLogger.v("gesture handled by notif:" + this.notification.getType());
                    break;
                } else if (this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible()) {
                    this.notificationManager.moveNext(true);
                    break;
                }
                break;
            case GESTURE_SWIPE_RIGHT:
                boolean handled = false;
                if (!(this.notification == null || this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible())) {
                    handled = this.notification.onGesture(event);
                }
                if (!handled) {
                    takeNotificationAction(true);
                    break;
                }
                sLogger.v("gesture handled by notif:" + this.notification.getType());
                break;
        }
        return true;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.initialized) {
            return this.clickGestureDetector.onKey(event);
        }
        return false;
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public void clearNotification() {
        this.customNotificationContainer.removeAllViews();
    }

    public void addCustomView(INotification notification, View view) {
        removeCustomView();
        this.customNotificationContainer.addView(view, new LayoutParams(-1, -1));
        this.notification = notification;
        this.initialized = true;
    }

    public void removeCustomView() {
        this.customNotificationContainer.removeAllViews();
        this.border.stopTimeout(false, null);
        this.initialized = false;
    }

    public void switchNotfication(INotification notification) {
        this.notification = notification;
    }

    public void setNextNotificationColor(int color) {
        this.nextNotificationColorView.setColor(color);
    }

    public void resetNextNotificationColor() {
        this.nextNotificationColorView.setColor(this.defaultColor);
    }

    public void showNextNotificationColor() {
        this.nextNotificationColorView.setVisibility(View.VISIBLE);
    }

    public void hideNextNotificationColor() {
        this.nextNotificationColorView.setVisibility(INVISIBLE);
    }

    private void takeNotificationAction(boolean quickly) {
        if (this.notification == null) {
            return;
        }
        if (this.notificationManager.isExpanded() || this.notificationManager.isExpandedNotificationVisible()) {
            boolean completely = false;
            if (this.notificationManager.isCurrentItemDeleteAll()) {
                AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DISMISS, null, quickly ? "swipe" : "doubleclick");
                completely = true;
                quickly = true;
            } else {
                AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_OPEN_MINI, this.notification, quickly ? "swipe" : "doubleclick");
            }
            this.notificationManager.collapseExpandedNotification(completely, quickly);
            return;
        }
        AnalyticsSupport.recordGlanceAction(AnalyticsSupport.ANALYTICS_EVENT_GLANCE_DISMISS, this.notification, quickly ? "swipe" : "doubleclick");
        this.notificationManager.collapseNotification();
    }

    public View getCurrentNotificationViewChild() {
        if (this.customNotificationContainer.getChildCount() > 0) {
            return this.customNotificationContainer.getChildAt(0);
        }
        return null;
    }

    public ViewGroup getNotificationContainer() {
        return this.customNotificationContainer;
    }
}
