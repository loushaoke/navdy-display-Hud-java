package com.navdy.hud.app.view;

public abstract class GaugeViewPresenter extends DashboardWidgetPresenter {
    protected GaugeView mGaugeView;

    public void setView(DashboardWidgetView dashboardWidgetView) {
        super.setView(dashboardWidgetView);
        if (dashboardWidgetView == null) {
            this.mGaugeView = null;
        } else if (dashboardWidgetView instanceof GaugeView) {
            this.mGaugeView = (GaugeView) dashboardWidgetView;
        } else {
            throw new IllegalArgumentException("The view has to be of type GaugeView");
        }
    }
}
