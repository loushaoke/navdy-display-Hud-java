package com.navdy.hud.app.view;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;

public class EmptyGaugePresenter extends DashboardWidgetPresenter {
    public static final int ALPHA_ANIMATION_DURATION = 2000;
    public static final int ANIMATION_DELAY = 1000;
    private String emptyGaugeName;
    private Runnable mAnimationRunnable;
    @InjectView(R.id.txt_empty_gauge)
    TextView mEmptyGaugeText;
    private Handler mHandler = new Handler();
    private boolean mPlayedAnimation = false;
    private ValueAnimator mTextAlphaAnimator = new ValueAnimator();

    public EmptyGaugePresenter(Context context) {
        this.mTextAlphaAnimator.setFloatValues(new float[]{1.0f, 0.0f});
        this.mTextAlphaAnimator.setDuration(2000);
        this.emptyGaugeName = context.getString(R.string.widget_empty);
        this.mTextAlphaAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                float alphaValue = ((Float) animation.getAnimatedValue()).floatValue();
                if (EmptyGaugePresenter.this.mEmptyGaugeText != null) {
                    EmptyGaugePresenter.this.mEmptyGaugeText.setAlpha(alphaValue);
                }
            }
        });
        this.mAnimationRunnable = new Runnable() {
            public void run() {
                EmptyGaugePresenter.this.mTextAlphaAnimator.start();
            }
        };
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.empty_gauge_layout);
            ButterKnife.inject( this, (View) dashboardWidgetView);
            if (arguments != null) {
                if (!arguments.getBoolean(DashboardWidgetPresenter.EXTRA_IS_ACTIVE, false)) {
                    this.mPlayedAnimation = false;
                    this.mTextAlphaAnimator.end();
                    this.mHandler.removeCallbacks(this.mAnimationRunnable);
                    this.mEmptyGaugeText.setAlpha(1.0f);
                } else if (!this.mPlayedAnimation) {
                    this.mPlayedAnimation = true;
                    this.mTextAlphaAnimator.end();
                    this.mHandler.removeCallbacks(this.mAnimationRunnable);
                    this.mEmptyGaugeText.setAlpha(1.0f);
                    this.mHandler.postDelayed(this.mAnimationRunnable, 1000);
                }
            }
            super.setView(dashboardWidgetView, arguments);
            return;
        }
        this.mPlayedAnimation = false;
        super.setView(dashboardWidgetView, arguments);
    }

    public Drawable getDrawable() {
        return null;
    }

    protected void updateGauge() {
    }

    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.EMPTY_WIDGET_ID;
    }

    public String getWidgetName() {
        return this.emptyGaugeName;
    }
}
