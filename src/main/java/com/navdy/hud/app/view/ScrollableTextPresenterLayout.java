package com.navdy.hud.app.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.GestureLearningScreen.Presenter;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator;
import com.navdy.hud.app.ui.component.carousel.CarouselIndicator.Orientation;
import com.navdy.hud.app.ui.component.carousel.ProgressIndicator;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.view.ObservableScrollView.IScrollListener;
import com.navdy.service.library.events.input.GestureEvent;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class ScrollableTextPresenterLayout extends RelativeLayout implements IListener, IScrollListener, IInputHandler {
    public static final int TAG_BACK = 0;
    @InjectView(R.id.bottomScrub)
    View bottomScrub;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    boolean mContentNeedsScrolling;
    private boolean mContentReachedBottom;
    private boolean mContentReachedTop;
    private int mCount;
    private int mCurrentItem;
    @InjectView(R.id.mainImage)
    ImageView mMainImageView;
    @InjectView(R.id.mainTitle)
    TextView mMainTitleText;
    @InjectView(R.id.message)
    TextView mMessageText;
    @InjectView(R.id.notifIndicator)
    CarouselIndicator mNotificationIndicator;
    @InjectView(R.id.notifScrollIndicator)
    ProgressIndicator mNotificationScrollIndicator;
    @InjectView(R.id.scrollView)
    ObservableScrollView mObservableScrollView;
    @Inject
    Presenter mPresenter;
    ArrayList<CharSequence> mTextContents;
    @InjectView(R.id.title)
    TextView mTitleText;
    private int scrollColor;
    @InjectView(R.id.topScrub)
    View topScrub;

    public ScrollableTextPresenterLayout(Context context) {
        this(context, null);
    }

    public ScrollableTextPresenterLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScrollableTextPresenterLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mTextContents = new ArrayList();
        this.mContentNeedsScrolling = false;
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.mChoiceLayout.setHighlightPersistent(true);
        this.scrollColor = getResources().getColor(R.color.scroll_option_color);
        this.mObservableScrollView.setScrollListener(this);
        List<Choice> list = new ArrayList();
        list.add(new Choice(getContext().getString(R.string.back), 0));
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
        this.mMainImageView.setImageResource(R.drawable.icon_settings_learning_to_gesture);
        this.mMainTitleText.setText(R.string.gesture_tips_title);
        this.mNotificationScrollIndicator.setOrientation(Orientation.VERTICAL);
        this.mNotificationScrollIndicator.setProperties(GlanceConstants.scrollingRoundSize, GlanceConstants.scrollingIndicatorProgressSize, 0, 0, GlanceConstants.colorWhite, GlanceConstants.colorWhite, true, GlanceConstants.scrollingIndicatorPadding, -1, -1);
        this.mNotificationScrollIndicator.setItemCount(100);
        this.mNotificationIndicator.setOrientation(Orientation.VERTICAL);
        this.mNotificationScrollIndicator.setBackgroundColor(this.scrollColor);
    }

    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == 0) {
            setCurrentItem(this.mCurrentItem, false, false);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void setTextContents(CharSequence[] textContents) {
        this.mTextContents.clear();
        for (CharSequence text : textContents) {
            this.mTextContents.add(text);
        }
        this.mCount = this.mTextContents.size();
        this.mNotificationIndicator.setItemCount(this.mCount);
        setCurrentItem(0, false, false);
    }

    private void setCurrentItem(int item, boolean animate, boolean previous) {
        if (item >= 0 && item < this.mCount) {
            this.mCurrentItem = item;
            CharSequence text = (CharSequence) this.mTextContents.get(this.mCurrentItem);
            this.mContentNeedsScrolling = GlanceHelper.needsScrollLayout(text.toString());
            this.mObservableScrollView.fullScroll(33);
            this.mMessageText.setText(text);
            this.mMessageText.setVisibility(View.VISIBLE);
            this.topScrub.setVisibility(GONE);
            this.bottomScrub.setVisibility(View.VISIBLE);
            this.mContentReachedTop = true;
            this.mContentReachedBottom = false;
            if (animate) {
                AnimatorSet animatorSet = this.mNotificationIndicator.getItemMoveAnimator(this.mCurrentItem, this.scrollColor);
                if (animatorSet != null) {
                    animatorSet.addListener(new DefaultAnimationListener() {
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            ScrollableTextPresenterLayout.this.displayScrollIndicator(false);
                        }
                    });
                    animatorSet.start();
                    return;
                }
                displayScrollIndicator(false);
                return;
            }
            displayScrollIndicator(true);
        }
    }

    private void displayScrollIndicator(boolean initial) {
        if (this.mCount > 0) {
            this.mNotificationIndicator.setVisibility(View.VISIBLE);
        }
        this.mNotificationIndicator.setCurrentItem(this.mCurrentItem, this.scrollColor);
        if (!this.mContentNeedsScrolling) {
            ((View) this.mNotificationScrollIndicator.getParent()).setVisibility(GONE);
        } else if (initial) {
            this.mNotificationIndicator.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    ScrollableTextPresenterLayout.this.layoutScrollIndicator();
                    ScrollableTextPresenterLayout.this.mNotificationIndicator.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        } else {
            layoutScrollIndicator();
        }
    }

    private void layoutScrollIndicator() {
        RectF rectF = this.mNotificationIndicator.getItemPos(this.mCurrentItem);
        if (rectF == null) {
            return;
        }
        if (rectF.left != 0.0f || rectF.top != 0.0f) {
            this.mNotificationScrollIndicator.setCurrentItem(1);
            View parent = (View) this.mNotificationScrollIndicator.getParent();
            parent.setX(this.mNotificationIndicator.getX() + ((float) GlanceConstants.scrollingIndicatorLeftPadding));
            parent.setY(((this.mNotificationIndicator.getY() + rectF.top) + ((float) (GlanceConstants.scrollingIndicatorCircleSize / 2))) - ((float) (GlanceConstants.scrollingIndicatorHeight / 2)));
            parent.setVisibility(View.VISIBLE);
        }
    }

    public void onTop() {
        if (this.mContentNeedsScrolling) {
            this.topScrub.setVisibility(GONE);
            this.mContentReachedTop = true;
            this.mContentReachedBottom = false;
            onPosChange(1);
        }
    }

    public void onBottom() {
        if (this.mContentNeedsScrolling) {
            this.mContentReachedTop = false;
            this.mContentReachedBottom = true;
            onPosChange(100);
        }
    }

    public void onScroll(int l, int t, int oldl, int oldt) {
        if (this.mContentNeedsScrolling) {
            this.topScrub.setVisibility(View.VISIBLE);
            this.mContentReachedTop = false;
            this.mContentReachedBottom = false;
            onPosChange((int) ((((double) t) * 100.0d) / ((double) (this.mObservableScrollView.getChildAt(0).getBottom() - this.mObservableScrollView.getHeight()))));
        }
    }

    public void onPosChange(int pos) {
        if (pos <= 0) {
            pos = 1;
        } else if (pos > 100) {
            pos = 100;
        }
        if (this.mNotificationScrollIndicator.getCurrentItem() != pos) {
            if (pos == 1 || pos == 100) {
                this.mNotificationScrollIndicator.setCurrentItem(pos);
            } else {
                this.mNotificationScrollIndicator.setCurrentItem(pos);
            }
        }
    }

    public boolean onGesture(GestureEvent event) {
        return false;
    }

    public boolean onKey(CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                handleLeft();
                return true;
            case RIGHT:
                handleRight();
                return true;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                return true;
            default:
                return false;
        }
    }

    private void handleLeft() {
        boolean handled = false;
        if (this.mContentNeedsScrolling) {
            handled = this.mObservableScrollView.arrowScroll(33) && !this.mContentReachedTop;
        }
        if (!handled && this.mCurrentItem > 0) {
            setCurrentItem(this.mCurrentItem - 1, true, true);
        }
    }

    private void handleRight() {
        boolean handled = false;
        if (this.mContentNeedsScrolling) {
            handled = this.mObservableScrollView.arrowScroll(130) && !this.mContentReachedBottom;
        }
        if (!handled && this.mCurrentItem < this.mCount - 1) {
            setCurrentItem(this.mCurrentItem + 1, true, false);
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public void executeItem(int pos, int id) {
        switch (id) {
            case 0:
                this.mPresenter.hideTips();
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }
}
