package com.navdy.hud.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.media.TransportMediator;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.View.MeasureSpec;
import com.glympse.android.lib.StaticConfig;
import com.navdy.hud.app.R;
import com.navdy.hud.app.util.CustomDimension;
import com.navdy.service.library.log.Logger;

public class Gauge extends View implements SerialValueAnimator.SerialValueAnimatorAdapter {
    private static final int ANIMATION_DURATION = 100;
    private static final int DEFAULT_SHADOW_THICKNESS_DP = 0;
    private static final float DEFAULT_SUB_TEXT_SIZE = 40.0f;
    private static final float DEFAULT_TEXT_SIZE = 90.0f;
    private static final int DEFAULT_THICKNESS_DP = 40;
    private static final int DEFAULT_TIC_LENGTH_DP = 4;
    private static final int SHADOW_START_ANGLE = 2;
    private static final int TIC_STYLE_CIRCLE = 2;
    private static final int TIC_STYLE_LINE = 1;
    private static final int TIC_STYLE_NONE = 0;
    private static final Logger sLogger = new Logger(Gauge.class);
    private boolean AntialiasSetting;
    private boolean mAnimateValues;
    protected int mBackgroundColor;
    private Paint mBackgroundPaint;
    protected int mBackgroundThickness;
    protected CustomDimension mBackgroundThicknessAttribute;
    protected String mCenterSubtext;
    protected String mCenterText;
    protected int mEndColor;
    protected int mMaxValue;
    protected int mMinValue;
    private SerialValueAnimator mSerialValueAnimator;
    protected int mShadowColor;
    protected int mShadowThickness;
    protected CustomDimension mShadowThicknessAttribute;
    protected int mStartAngle;
    protected int mStartColor;
    protected float mSubTextSize;
    protected CustomDimension mSubTextSizeAttribute;
    protected int mSweepAngle;
    protected int mTextColor;
    protected float mTextSize;
    protected CustomDimension mTextSizeAttribute;
    protected int mThickness;
    protected CustomDimension mThicknessAttribute;
    protected int mTicColor;
    private int mTicInterval;
    private int mTicLength;
    protected int mTicPadding;
    private Paint mTicPaint;
    protected int mTicStyle;
    protected int mValue;
    protected int mWarningColor;
    protected int mWarningValue;

    public Gauge(Context context) {
        this(context, null);
    }

    public Gauge(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Gauge(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mTicColor = -1;
        this.AntialiasSetting = true;
        this.mAnimateValues = true;
        initFromAttributes(context, attrs);
        initDrawingTools();
        this.mSerialValueAnimator = new SerialValueAnimator(this, 100);
    }

    private int convertDpToPx(int dp) {
        return (int) TypedValue.applyDimension(1, (float) dp, getResources().getDisplayMetrics());
    }

    private void initFromAttributes(Context context, AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Gauge, 0, 0);
        int defaultThickness = convertDpToPx(40);
        int defaultShadowThickness = convertDpToPx(0);
        int defaultTicLength = convertDpToPx(4);
        try {
            this.mMinValue = a.getInteger(R.styleable.Gauge_gaugeMinValue, 0);
            this.mValue = a.getInteger(R.styleable.Gauge_gaugeValue, 0);
            this.mMaxValue = a.getInteger(R.styleable.Gauge_gaugeMaxValue, StaticConfig.MEMORY_IMAGE_CACHE_SIZE_THRESHOLD);
            this.mStartAngle = a.getInteger(R.styleable.Gauge_gaugeStartAngle, 150);
            this.mSweepAngle = a.getInteger(R.styleable.Gauge_gaugeSweepAngle, StaticConfig.PLACE_SEARCH_DISTANCE_FILTER);
            this.mTicPadding = a.getDimensionPixelOffset(R.styleable.Gauge_gaugeTicPadding, 0);
            this.mThicknessAttribute = CustomDimension.getDimension(this, a, R.styleable.Gauge_gaugeThickness, (float) defaultThickness);
            if (CustomDimension.hasDimension(this, a, R.styleable.Gauge_gaugeBackgroundThickness)) {
                this.mBackgroundThicknessAttribute = CustomDimension.getDimension(this, a, R.styleable.Gauge_gaugeBackgroundThickness, (float) defaultThickness);
            } else {
                this.mBackgroundThicknessAttribute = CustomDimension.getDimension(this, a, R.styleable.Gauge_gaugeThickness, (float) defaultThickness);
            }
            this.mShadowThicknessAttribute = CustomDimension.getDimension(this, a, R.styleable.Gauge_gaugeShadowThickness, (float) defaultShadowThickness);
            this.mTextSizeAttribute = CustomDimension.getDimension(this, a, R.styleable.Gauge_gaugeTextSize, DEFAULT_TEXT_SIZE);
            this.mSubTextSizeAttribute = CustomDimension.getDimension(this, a, R.styleable.Gauge_gaugeSubTextSize, DEFAULT_SUB_TEXT_SIZE);
            this.mTicStyle = a.getInteger(R.styleable.Gauge_gaugeTicStyle, 0);
            this.mTicLength = a.getDimensionPixelSize(R.styleable.Gauge_gaugeTicLength, defaultTicLength);
            this.mTicInterval = a.getInteger(R.styleable.Gauge_gaugeTicInterval, 10);
            this.mBackgroundColor = a.getColor(R.styleable.Gauge_gaugeBackgroundColor, -7829368);
            this.mWarningColor = a.getColor(R.styleable.Gauge_gaugeWarningColor, SupportMenu.CATEGORY_MASK);
            this.mTextColor = a.getColor(R.styleable.Gauge_gaugeTextColor, -1);
            this.mWarningValue = a.getInteger(R.styleable.Gauge_gaugeWarningValue, 75);
            this.mStartColor = a.getColor(R.styleable.Gauge_gaugeStartColor, -16777216);
            this.mEndColor = a.getColor(R.styleable.Gauge_gaugeEndColor, -1);
            this.mShadowColor = a.getColor(R.styleable.Gauge_gaugeShadowColor, -7829368);
            this.mCenterText = a.getString(R.styleable.Gauge_gaugeCenterText);
            this.mCenterSubtext = a.getString(R.styleable.Gauge_gaugeCenterSubText);
            if (this.mMaxValue < this.mMinValue) {
                this.mMaxValue = this.mMinValue + 1;
            }
            if (this.mSweepAngle <= 0) {
                this.mSweepAngle = 1;
            }
            if (this.mTicInterval <= 0) {
                this.mTicInterval = 1;
            }
        } finally {
            a.recycle();
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int chosenDimension = Math.min(chooseDimension(MeasureSpec.getMode(widthMeasureSpec), MeasureSpec.getSize(widthMeasureSpec)), chooseDimension(MeasureSpec.getMode(heightMeasureSpec), MeasureSpec.getSize(heightMeasureSpec)));
        setMeasuredDimension(chosenDimension, chosenDimension);
    }

    private int chooseDimension(int mode, int size) {
        return (mode == Integer.MIN_VALUE || mode == 1073741824) ? size : getPreferredSize();
    }

    private int getPreferredSize() {
        return 300;
    }

    public void setValue(int value) {
        if (value < this.mMinValue) {
            value = this.mMinValue;
        }
        if (value > this.mMaxValue) {
            value = this.mMaxValue;
        }
        if (this.mAnimateValues) {
            this.mSerialValueAnimator.setValue((float) value);
            return;
        }
        this.mValue = value;
        invalidate();
    }

    public void setMaxValue(int mMaxValue) {
        this.mMaxValue = mMaxValue;
    }

    public void setCenterText(String text) {
        this.mCenterText = text;
        invalidate();
    }

    public void setCenterSubtext(String text) {
        this.mCenterSubtext = text;
        invalidate();
    }

    private void evaluateDimensions() {
        int smallSide = Math.min(getWidth(), getHeight());
        this.mThickness = (int) this.mThicknessAttribute.getSize(this, (float) smallSide, 0.0f);
        this.mShadowThickness = (int) this.mShadowThicknessAttribute.getSize(this, (float) smallSide, 0.0f);
        if (this.mThickness <= this.mShadowThickness) {
            sLogger.e("Shadow is set to be bigger than gauge's thickness - removing shadow");
            this.mShadowThickness = 0;
        }
        this.mBackgroundThickness = (int) this.mBackgroundThicknessAttribute.getSize(this, (float) smallSide, 0.0f);
        this.mTextSize = this.mTextSizeAttribute.getSize(this, (float) smallSide, 0.0f);
        this.mSubTextSize = this.mSubTextSizeAttribute.getSize(this, (float) smallSide, 0.0f);
    }

    protected void initDrawingTools() {
        evaluateDimensions();
        this.mBackgroundPaint = new Paint();
        this.mBackgroundPaint.setStrokeWidth((float) this.mBackgroundThickness);
        this.mBackgroundPaint.setAntiAlias(true);
        this.mBackgroundPaint.setStrokeCap(Cap.BUTT);
        this.mBackgroundPaint.setStyle(Style.STROKE);
        this.mTicPaint = new Paint();
        this.mTicPaint.setColor(this.mTicColor);
        this.mTicPaint.setStyle(this.mTicStyle == 1 ? Style.STROKE : Style.FILL);
        this.mTicPaint.setStrokeWidth(0.0f);
        this.mTicPaint.setAntiAlias(this.AntialiasSetting);
    }

    public float getRadius() {
        return getRadius(this.mThickness);
    }

    private float getRadius(int thickness) {
        return ((float) (Math.min(getHeight(), getWidth()) - thickness)) / 2.0f;
    }

    protected void drawBackground(Canvas canvas) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius = getRadius();
        Paint paint = this.mBackgroundPaint;
        RectF rect = new RectF();
        rect.set((width / 2.0f) - radius, (height / 2.0f) - radius, (width / 2.0f) + radius, (height / 2.0f) + radius);
        paint.setColor(this.mBackgroundColor);
        float backgroundSweep = (float) this.mSweepAngle;
        if (this.mWarningValue != 0) {
            float warningSweep = deltaToAngle(this.mMaxValue - this.mWarningValue);
            backgroundSweep -= warningSweep;
            paint.setColor(this.mWarningColor);
            canvas.drawArc(rect, valueToAngle(this.mWarningValue), warningSweep, false, paint);
        }
        paint.setColor(this.mBackgroundColor);
        canvas.drawArc(rect, (float) this.mStartAngle, backgroundSweep, false, paint);
    }

    private float valueToAngle(int value) {
        return ((float) this.mStartAngle) + ((((float) this.mSweepAngle) * ((float) (value - this.mMinValue))) / ((float) (this.mMaxValue - this.mMinValue)));
    }

    protected float deltaToAngle(int deltaValue) {
        return (((float) this.mSweepAngle) * ((float) deltaValue)) / ((float) (this.mMaxValue - this.mMinValue));
    }

    private void drawTics(Canvas canvas) {
        if (this.mTicStyle != 0) {
            float centerX = (float) Math.round(((float) getWidth()) / 2.0f);
            float centerY = (float) Math.round(((float) getHeight()) / 2.0f);
            float radius = getRadius();
            canvas.save(1);
            canvas.rotate((float) this.mStartAngle, centerX, centerY);
            float tickSweep = deltaToAngle(this.mTicInterval);
            float maxAngle = (float) (this.mStartAngle + this.mSweepAngle);
            float alphaSweep = tickSweep / 2.0f;
            float valueAngle = valueToAngle(this.mValue);
            for (float tickAngle = (float) this.mStartAngle; tickAngle <= maxAngle; tickAngle += tickSweep) {
                int alpha = 128;
                if (valueAngle > tickAngle) {
                    alpha = 255;
                } else if (valueAngle > tickAngle - alphaSweep) {
                    alpha = 128 + Math.round((((float) TransportMediator.KEYCODE_MEDIA_PAUSE) * (valueAngle - (tickAngle - alphaSweep))) / alphaSweep);
                }
                this.mTicPaint.setAlpha(alpha);
                if (this.mTicStyle == 1) {
                    canvas.drawLine((((centerX + radius) - (((float) this.mThickness) / 2.0f)) - ((float) this.mTicLength)) - ((float) this.mTicPadding), centerY, ((centerX + radius) - (((float) this.mThickness) / 2.0f)) - ((float) this.mTicPadding), centerY, this.mTicPaint);
                } else if (this.mTicStyle == 2) {
                    canvas.drawCircle((((centerX + radius) - (((float) this.mThickness) / 2.0f)) - (((float) this.mTicLength) / 2.0f)) - ((float) this.mTicPadding), centerY, ((float) this.mTicLength) / 2.0f, this.mTicPaint);
                }
                canvas.rotate(tickSweep, centerX, centerY);
            }
            canvas.restore();
        }
    }

    private void drawText(Canvas canvas) {
        if (this.mCenterText != null || this.mCenterSubtext != null) {
            Paint paint = new Paint();
            paint.setColor(this.mTextColor);
            float mainTextSize = this.mTextSize;
            float subTextSize = this.mSubTextSize;
            paint.setTextSize(mainTextSize);
            paint.setTextAlign(Align.CENTER);
            float centerX = ((float) getWidth()) / 2.0f;
            float centerY = ((float) getHeight()) / 2.0f;
            if (this.mCenterText != null) {
                canvas.drawText(this.mCenterText, centerX, centerY, paint);
            }
            if (this.mCenterSubtext != null) {
                paint.setTextSize(subTextSize);
                canvas.drawText(this.mCenterSubtext, centerX, (centerY + subTextSize) + 10.0f, paint);
            }
        }
    }

    public void drawIndicator(Canvas canvas) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius = getRadius();
        Paint paint = new Paint();
        paint.setColor(this.mStartColor);
        paint.setStrokeWidth((float) this.mThickness);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Cap.BUTT);
        paint.setStyle(Style.STROKE);
        float sweep = deltaToAngle(this.mValue - this.mMinValue);
        if (this.mStartColor != this.mEndColor) {
            paint.setShader(new SweepGradient(width / 2.0f, height / 2.0f, new int[]{this.mStartColor, this.mStartColor, this.mEndColor, this.mEndColor}, new float[]{0.0f, ((float) this.mStartAngle) / 360.0f, (((float) this.mStartAngle) + sweep) / 360.0f, 1.0f}));
        }
        float rectLeft = (width / 2.0f) - radius;
        float rectTop = (height / 2.0f) - radius;
        float rectRight = (width / 2.0f) + radius;
        float rectBottom = (height / 2.0f) + radius;
        float radiusDeltaForShadow = 0.0f;
        float angleDelta = 0.0f;
        if (this.mShadowThickness > 0) {
            Paint shadowPaint = new Paint(paint);
            shadowPaint.setColor(this.mShadowColor);
            canvas.drawArc(new RectF(rectLeft, rectTop, rectRight, rectBottom), (float) this.mStartAngle, sweep, false, shadowPaint);
            int newThickness = this.mThickness - this.mShadowThickness;
            paint.setStrokeWidth((float) newThickness);
            radiusDeltaForShadow = radius - getRadius(newThickness);
            angleDelta = 2.0f;
        }
        if (sweep > angleDelta) {
            RectF rect = new RectF();
            rect.set(rectLeft + radiusDeltaForShadow, rectTop + radiusDeltaForShadow, rectRight - radiusDeltaForShadow, rectBottom - radiusDeltaForShadow);
            canvas.drawArc(rect, ((float) this.mStartAngle) + angleDelta, sweep - angleDelta, false, paint);
        }
    }

    public void drawIndicator(Canvas canvas, int fromValue, int toValue) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius = getRadius();
        Paint paint = new Paint();
        paint.setColor(this.mStartColor);
        paint.setStrokeWidth((float) this.mThickness);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Cap.BUTT);
        paint.setStyle(Style.STROKE);
        float sweep = deltaToAngle(toValue - fromValue);
        float startAngle = ((float) this.mStartAngle) + deltaToAngle(fromValue - this.mMinValue);
        if (this.mStartColor != this.mEndColor) {
            paint.setShader(new SweepGradient(width / 2.0f, height / 2.0f, new int[]{this.mStartColor, this.mStartColor, this.mEndColor, this.mEndColor}, new float[]{0.0f, startAngle / 360.0f, (startAngle + sweep) / 360.0f, 1.0f}));
        }
        float rectLeft = (width / 2.0f) - radius;
        float rectTop = (height / 2.0f) - radius;
        float rectRight = (width / 2.0f) + radius;
        float rectBottom = (height / 2.0f) + radius;
        float radiusDeltaForShadow = 0.0f;
        float angleDelta = 0.0f;
        if (this.mShadowThickness > 0) {
            Paint shadowPaint = new Paint(paint);
            shadowPaint.setColor(this.mShadowColor);
            canvas.drawArc(new RectF(rectLeft, rectTop, rectRight, rectBottom), startAngle, sweep, false, shadowPaint);
            int newThickness = this.mThickness - this.mShadowThickness;
            paint.setStrokeWidth((float) newThickness);
            radiusDeltaForShadow = radius - getRadius(newThickness);
            angleDelta = 2.0f;
        }
        if (sweep > angleDelta) {
            RectF rect = new RectF();
            rect.set(rectLeft + radiusDeltaForShadow, rectTop + radiusDeltaForShadow, rectRight - radiusDeltaForShadow, rectBottom - radiusDeltaForShadow);
            canvas.drawArc(rect, startAngle + angleDelta, sweep - angleDelta, false, paint);
        }
    }

    protected void onDraw(Canvas canvas) {
        drawBackground(canvas);
        drawTics(canvas);
        drawText(canvas);
        drawGauge(canvas);
    }

    protected void drawGauge(Canvas canvas) {
        drawIndicator(canvas, this.mMinValue, this.mValue);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        initDrawingTools();
    }

    public void setBackgroundColor(int color) {
        this.mBackgroundColor = color;
        invalidate();
    }

    public void clearAnimationQueue() {
        this.mSerialValueAnimator.release();
    }

    public float getValue() {
        return (float) this.mValue;
    }

    public void setValue(float newValue) {
        this.mValue = (int) newValue;
        invalidate();
    }

    public void animationComplete(float newValue) {
    }

    public void setAnimated(boolean animated) {
        this.mAnimateValues = animated;
    }
}
