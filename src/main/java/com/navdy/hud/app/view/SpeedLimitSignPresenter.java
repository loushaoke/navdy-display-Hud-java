package com.navdy.hud.app.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnitChanged;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.squareup.otto.Subscribe;

import static android.view.View.GONE;

public class SpeedLimitSignPresenter extends DashboardWidgetPresenter {
    private int speedLimit;
    @InjectView(R.id.speed_limit_sign)
    protected SpeedLimitSignView speedLimitSignView;
    private String speedLimitSignWidgetName;
    @InjectView(R.id.txt_speed_limit_unavailable)
    protected TextView speedLimitUnavailableText;
    private SpeedManager speedManager = SpeedManager.getInstance();

    public SpeedLimitSignPresenter(Context context) {
        this.speedLimitSignWidgetName = context.getResources().getString(R.string.widget_speed_limit);
    }

    public void setSpeedLimit(int speedLimit) {
        this.speedLimit = speedLimit;
        reDraw();
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView((int) R.layout.speed_limit_sign_gauge);
            ButterKnife.inject( this, (View) dashboardWidgetView);
        }
        super.setView(dashboardWidgetView, arguments);
    }

    protected boolean isRegisteringToBusRequired() {
        return true;
    }

    public Drawable getDrawable() {
        return null;
    }

    protected void updateGauge() {
        if (this.speedLimit > 0) {
            this.speedLimitUnavailableText.setVisibility(GONE);
            this.speedLimitSignView.setVisibility(View.VISIBLE);
            this.speedLimitSignView.setSpeedLimitUnit(this.speedManager.getSpeedUnit());
            this.speedLimitSignView.setSpeedLimit(this.speedLimit);
            return;
        }
        this.speedLimitUnavailableText.setVisibility(View.VISIBLE);
        this.speedLimitSignView.setVisibility(GONE);
    }

    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID;
    }

    public String getWidgetName() {
        return this.speedLimitSignWidgetName;
    }

    @Subscribe
    public void onSpeedUnitChanged(SpeedUnitChanged unitChanged) {
        reDraw();
    }
}
