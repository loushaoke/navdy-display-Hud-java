package com.navdy.hud.app.storage;

import com.navdy.service.library.events.file.FileType;

import java.io.File;

public class PathManager implements com.navdy.service.library.file.IFileTransferAuthority {
    final private static String ACTIVE_ROUTE_DIR = ".activeroute";
    final private static String CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR = "car_md_cache";
    private static String CRASH_INFO_TEXT_FILE_NAME;
    final private static String DATA_LOGS_DIR = "DataLogs";
    final private static String DRIVER_PROFILES_DIR = "DriverProfiles";
    final private static String GESTURE_VIDEOS_SYNC_FOLDER = "gesture_videos";
    final private static String HERE_MAPS_CONFIG_BASE_PATH = "/.here-maps";
    final private static java.util.regex.Pattern HERE_MAPS_CONFIG_DIRS_PATTERN;
    final private static String HERE_MAPS_CONFIG_DIRS_REGEX = "[0-9]{10}";
    final private static String HERE_MAP_DATA_DIR = ".here-maps";
    final public static String HERE_MAP_META_JSON_FILE = "meta.json";
    final private static String HUD_DATABASE_DIR = "/.db";
    final private static String IMAGE_DISK_CACHE_FILES_DIR = "img_disk_cache";
    final private static String KERNEL_CRASH_CONSOLE_RAMOOPS = "/sys/fs/pstore/console-ramoops";
    final private static String KERNEL_CRASH_CONSOLE_RAMOOPS_0 = "/sys/fs/pstore/console-ramoops-0";
    final private static String KERNEL_CRASH_DMESG_RAMOOPS = "/sys/fs/pstore/dmesg-ramoops-0";
    final private static String[] KERNEL_CRASH_FILES;
    final private static String LOGS_FOLDER = "templogs";
    final private static String MUSIC_LIBRARY_DISK_CACHE_FILES_DIR = "music_disk_cache";
    final private static String NAVIGATION_FILES_DIR = "navigation_issues";
    final private static String NON_FATAL_CRASH_REPORT_DIR = ".logs/snapshot/";
    final private static String SYSTEM_CACHE_DIR = "/cache";
    final private static String TEMP_FILE_TIMESTAMP_FORMAT = "'display_log'_yyyy_MM_dd-HH_mm_ss'.zip'";
    final private static String TIMESTAMP_MWCONFIG_LATEST;
    private static java.text.SimpleDateFormat format;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.storage.PathManager sSingleton;
    private String activeRouteInfoDir;
    private String carMdResponseDiskCacheFolder;
    private String databaseDir;
    private String driverProfilesDir;
    private String gestureVideosSyncFolder;
    private java.io.File hereMapsConfigDirs;
    private String nonFatalCrashReportedDir;
    private String hereMapsDataDirectory;
    private String hereVoiceSkinsPath;
    private String imageDiskCacheFolder;
    private volatile boolean initialized;
    final private String mapsPartitionPath;
    private String musicDiskCacheFolder;
    private String navigationIssuesFolder;
    private String tempLogsFolder;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.storage.PathManager.class);
        CRASH_INFO_TEXT_FILE_NAME = "info.txt";
        HERE_MAPS_CONFIG_DIRS_PATTERN = java.util.regex.Pattern.compile("[0-9]{10}");
        TIMESTAMP_MWCONFIG_LATEST = com.navdy.hud.app.util.DeviceUtil.getCurrentHereSdkTimestamp();
        String[] a = new String[3];
        a[0] = "/sys/fs/pstore/console-ramoops";
        a[1] = "/sys/fs/pstore/console-ramoops-0";
        a[2] = "/sys/fs/pstore/dmesg-ramoops-0";
        KERNEL_CRASH_FILES = a;
        format = new java.text.SimpleDateFormat("'display_log'_yyyy_MM_dd-HH_mm_ss'.zip'");
        sSingleton = new com.navdy.hud.app.storage.PathManager();
    }
    
    private PathManager() {
        String externalStorage = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
        String appFiles = com.navdy.hud.app.HudApplication.getAppContext().getFilesDir().getAbsolutePath();
        String appCache = com.navdy.hud.app.HudApplication.getAppContext().getCacheDir().getAbsolutePath();
        this.tempLogsFolder = appCache + File.separator + "templogs";
        this.driverProfilesDir = appFiles + File.separator + DRIVER_PROFILES_DIR;
        this.mapsPartitionPath = com.navdy.hud.app.util.os.SystemProperties.get("ro.maps_partition", externalStorage);
        this.hereMapsConfigDirs = new java.io.File(this.mapsPartitionPath + File.separator + HERE_MAPS_CONFIG_BASE_PATH);
        this.nonFatalCrashReportedDir = this.mapsPartitionPath + File.separator + NON_FATAL_CRASH_REPORT_DIR;
        this.databaseDir = appFiles + File.separator + "/.db";
        this.activeRouteInfoDir = appFiles + File.separator + ACTIVE_ROUTE_DIR;
        this.navigationIssuesFolder = appFiles + File.separator + "navigation_issues";
        this.carMdResponseDiskCacheFolder = appFiles + File.separator + CAR_MD_RESPONSE_DISK_CACHE_FILES_DIR;
        this.imageDiskCacheFolder = appFiles + File.separator + "img_disk_cache";
        this.musicDiskCacheFolder = appFiles + File.separator + "music_disk_cache";
        this.gestureVideosSyncFolder = this.mapsPartitionPath + File.separator + GESTURE_VIDEOS_SYNC_FOLDER;
        java.io.File a = new java.io.File(this.mapsPartitionPath + File.separator + ".here-maps/voices-download");
        sLogger.v("voiceSkins path=" + a);
        this.hereVoiceSkinsPath = a.getAbsolutePath();
        this.hereMapsDataDirectory = this.mapsPartitionPath + File.separator + ".here-maps";
    }
    
    static java.util.regex.Pattern access$000() {
        return HERE_MAPS_CONFIG_DIRS_PATTERN;
    }
    
    private void collectGpsLog(String s) {
        try {
            android.content.Intent a = new android.content.Intent("GPS_COLLECT_LOGS");
            a.putExtra("logPath", s);
            com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(a, android.os.Process.myUserHandle());
            com.navdy.hud.app.util.GenericUtil.sleep(3000);
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    public static String generateTempFileName() {
        String s;
        synchronized(com.navdy.hud.app.storage.PathManager.class) {
            s = format.format(System.currentTimeMillis());
        }
        return s;
    }
    
    public static com.navdy.hud.app.storage.PathManager getInstance() {
        return sSingleton;
    }
    
    private void init() {
        if (!this.initialized) {
            this.initialized = true;
            new java.io.File(this.tempLogsFolder).mkdirs();
            new java.io.File(this.databaseDir).mkdirs();
            new java.io.File(this.driverProfilesDir).mkdirs();
            new java.io.File(this.activeRouteInfoDir).mkdirs();
            new java.io.File(this.navigationIssuesFolder).mkdirs();
            new java.io.File(this.carMdResponseDiskCacheFolder).mkdirs();
            new java.io.File(this.nonFatalCrashReportedDir).mkdirs();
            new java.io.File(this.imageDiskCacheFolder).mkdirs();
            new java.io.File(this.musicDiskCacheFolder).mkdirs();
            new java.io.File(this.hereVoiceSkinsPath).mkdirs();
            new java.io.File(this.gestureVideosSyncFolder).mkdirs();
        }
    }
    
    public void collectEnvironmentInfo(String s) {
        label3: {
            java.io.FileWriter a = null;
            Throwable a0 = null;
            label1: {
                label2: {
                    try {
                        a = new java.io.FileWriter(new java.io.File(s + File.separator + CRASH_INFO_TEXT_FILE_NAME));
                        break label2;
                    } catch(Throwable a1) {
                        a0 = a1;
                    }
                    a = null;
                    break label1;
                }
                label0: {
                    try {
                        a.write(com.navdy.hud.app.util.os.PropsFileUpdater.readProps());
                        break label0;
                    } catch(Throwable a2) {
                        a0 = a2;
                    }
                    break label1;
                }
                com.navdy.service.library.util.IOUtils.closeStream(a);
                break label3;
            }
            try {
                sLogger.e(a0);
            } catch(Throwable a3) {
                com.navdy.service.library.util.IOUtils.closeStream(a);
                throw a3;
            }
            com.navdy.service.library.util.IOUtils.closeStream(a);
        }
    }
    
    public String getActiveRouteInfoDir() {
        if (!this.initialized) {
            this.init();
        }
        return this.activeRouteInfoDir;
    }
    
    public String getCarMdResponseDiskCacheFolder() {
        if (!this.initialized) {
            this.init();
        }
        return this.carMdResponseDiskCacheFolder;
    }
    
    public String getDatabaseDir() {
        if (!this.initialized) {
            this.init();
        }
        return this.databaseDir;
    }
    
    public String getDirectoryForFileType(com.navdy.service.library.events.file.FileType a) {
        String s;
        if (!this.initialized) {
            this.init();
        }
        switch(a) {
            case FILE_TYPE_LOGS: {
                return this.tempLogsFolder;
            }
            case FILE_TYPE_OTA: {
                return SYSTEM_CACHE_DIR;
            }
            default: {
                return null;
            }
        }
    }
    
    public String getDriverProfilesDir() {
        if (!this.initialized) {
            this.init();
        }
        return this.driverProfilesDir;
    }
    
    public String getFileToSend(com.navdy.service.library.events.file.FileType a) {
        String s;
        com.navdy.service.library.events.file.FileType a0 = com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS;
        label0: {
            label1: {
                label2: {
                    boolean b;
                    if (a != a0) {
                        break label2;
                    }
                    String s0 = this.getDirectoryForFileType(a);
                    String s1 = s0 + File.separator + "stage";
                    java.io.File a1 = new java.io.File(s1);
                    com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), a1);
                    com.navdy.service.library.util.IOUtils.createDirectory(a1);
                    try {
                        com.navdy.service.library.util.LogUtils.copyComprehensiveSystemLogs(s1);
                        this.collectEnvironmentInfo(s1);
                        this.collectGpsLog(s1);
                        com.navdy.hud.app.util.DeviceUtil.copyHEREMapsDataInfo(s1);
                        com.navdy.hud.app.util.DeviceUtil.takeDeviceScreenShot(a1.getAbsolutePath() + File.separator + "HUDScreenShot.png");
                        java.util.List a2 = com.navdy.hud.app.util.ReportIssueService.getLatestDriveLogFiles(1);
                        if (a2 != null && a2.size() > 0) {
                            Object a3 = a2.iterator();
                            while(((java.util.Iterator)a3).hasNext()) {
                                java.io.File a4 = (java.io.File)((java.util.Iterator)a3).next();
                                com.navdy.service.library.util.IOUtils.copyFile(a4.getAbsolutePath(), a1.getAbsolutePath() + File.separator + a4.getName());
                            }
                        }
                        java.io.File[] a5 = a1.listFiles();
                        String s2 = com.navdy.hud.app.storage.PathManager.generateTempFileName();
                        s = s0 + File.separator + s2;
                        com.navdy.hud.app.util.CrashReportService.compressCrashReportsToZip(a5, s);
                        b = new java.io.File(s).exists();
                    } catch(Throwable ignoredException) {
                        break label1;
                    }
                    if (b) {
                        break label0;
                    }
                }
                s = null;
                break label0;
            }
            s = null;
        }
        return s;
    }
    
    public String getGestureVideosSyncFolder() {
        if (!this.initialized) {
            this.init();
        }
        return this.gestureVideosSyncFolder;
    }
    
    public java.util.List getHereMapsConfigDirs() {
        if (!this.initialized) {
            this.init();
        }
        java.util.ArrayList a = new java.util.ArrayList();
        java.io.File[] a0 = this.hereMapsConfigDirs.listFiles(new PathManager$1(this));
        if (a0 != null) {
            int i = a0.length;
            int i0 = 0;
            while(i0 < i) {
                a.add(a0[i0].getAbsolutePath());
                i0 = i0 + 1;
            }
        }
        return a;
    }
    
    public String getHereMapsDataDirectory() {
        if (!this.initialized) {
            this.init();
        }
        return this.hereMapsDataDirectory;
    }
    
    public String getHereVoiceSkinsPath() {
        if (!this.initialized) {
            this.init();
        }
        return this.hereVoiceSkinsPath;
    }
    
    public String getImageDiskCacheFolder() {
        if (!this.initialized) {
            this.init();
        }
        return this.imageDiskCacheFolder;
    }
    
    public String[] getKernelCrashFiles() {
        if (!this.initialized) {
            this.init();
        }
        return KERNEL_CRASH_FILES;
    }

    public String getLatestHereMapsConfigPath() {
        if (!this.initialized) {
            this.init();
        }
        return this.hereMapsConfigDirs.getAbsolutePath() + File.separator + TIMESTAMP_MWCONFIG_LATEST;
    }
    
    public String getMapsPartitionPath() {
        if (!this.initialized) {
            this.init();
        }
        return this.mapsPartitionPath;
    }
    
    public String getMusicDiskCacheFolder() {
        if (!this.initialized) {
            this.init();
        }
        return this.musicDiskCacheFolder;
    }
    
    public String getNavigationIssuesDir() {
        if (!this.initialized) {
            this.init();
        }
        return this.navigationIssuesFolder;
    }
    
    public String getNonFatalCrashReportDir() {
        if (!this.initialized) {
            this.init();
        }
        return this.nonFatalCrashReportedDir;
    }

    public boolean isFileTypeAllowed(FileType var1) {
        switch(var1) {
            case FILE_TYPE_OTA:
            case FILE_TYPE_LOGS:
            case FILE_TYPE_PERF_TEST:
                return true;
            default:
                return false;
        }
    }
    
    public void onFileSent(com.navdy.service.library.events.file.FileType a) {
        if (a == com.navdy.service.library.events.file.FileType.FILE_TYPE_LOGS) {
            com.navdy.hud.app.util.CrashReportService.clearCrashReports();
        }
    }
}
