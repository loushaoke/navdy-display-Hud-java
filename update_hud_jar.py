#!/usr/bin/env python3

import os
from pathlib import Path
import ruamel.std.zipfile as zipfile

hudjar = Path(__file__).parent / 'libs' / 'Hud.jar'
src = Path(__file__).parent / 'src' / 'main' / 'java'

with zipfile.ZipFile(hudjar, 'r') as jar:
    filelist = jar.namelist()

dellist = []
for root, dirs, files in os.walk(src):
    for f in files:
        fpath = Path(root) / f
        rel = os.path.relpath(fpath, src)
        rel = rel.lstrip('.').replace('\\', '/')
        rel = rel.replace('.java', '.class')
        if rel in filelist:
            dellist.append(rel)

print(dellist)
if dellist:
    zipfile.delete_from_zip_file(hudjar, file_names=dellist)
