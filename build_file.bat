@echo off
pushd %~dp0

if "%1" == "" (
    echo Usage: %0 filename.java second.java ...
    exit /b
)

set "TOOLS=%CD%\..\..\.."
set "smali=%CD%\..\smali_classes30"

set "PATH=%PATH%;%TOOLS%\dex-tools-2.1-SNAPSHOT;%TOOLS%\dex-tools-2.1-SNAPSHOT\bin;C:\Program Files (x86)\Android\android-sdk\build-tools\25.0.3;C:\Users\corona\AppData\Local\Android\sdk\build-tools\26.0.2;F:\Android\sdk\build-tools\25.0.1"

:: mkdir build\intermediates\classes
:: for %%C in (..\build\apk\classes*.dex) do (
:: call d2j-dex2jar --force %%C -o build\classes.jar
:: )

call d2j-dex2jar --force ..\..\Hud.apk -o build\classes.jar

:again
rem if %1 is blank, we are finished
if not "%1" == "" (
    echo Compiling file %1...

    :: src\main\java
    javac -cp build\classes.jar -cp "build\intermediates\classes;build\*;F:\Android\sdk\platforms\android-25\*;C:\Users\corona\AppData\Local\Android\sdk\platforms\android-25\*" -g -d build\intermediates\classes "%1"
    
    if %errorlevel% neq 0 exit /b %errorlevel%

    rem - shift the arguments and examine %1 again
    shift
    goto again
)


pushd build\intermediates\classes\
for /r %%C in (*.class) do (
java -Xmx1500m -cp ".;%TOOLS%\helper;%TOOLS%\dex\*" com.cooltrickshome.helper.RunProgramDex class2Dex %%C classes.dex 
::java -Xmx1500m -cp .;%TOOLS%\helper;%TOOLS%\dex\* com.cooltrickshome.helper.RunProgramDex dex2Smali %smali%\classes.dex  %smali%\out 
java -Xmx1500m -jar %TOOLS%\baksmali-2.2.2.jar d --use-locals --output %smali% classes.dex 
)
popd
pause
